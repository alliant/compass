&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* sys/tempfiledata.p
   Manage TEMPorary FILEs including creation and deletion
   D.Sinclair
   5.2.2013
   */

{tt/tempfile.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 12.48
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* Shared temp-file management to attempt deletion upon application exit */

subscribe to "AddTempFile" anywhere.
subscribe to "GetTempFile" anywhere.
subscribe to "DeleteTempFile" anywhere.
subscribe to "GetTempFiles" anywhere.

subscribe to "AddActionTempFiles" anywhere.
subscribe to "DeleteActionTempFiles" anywhere.

subscribe to "DeleteAllTempFiles" anywhere.

subscribe to "OpenTempFile" anywhere.

subscribe to "QUIT" anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-AddActionTempFiles) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AddActionTempFiles Procedure 
PROCEDURE AddActionTempFiles :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAction as char.

run AddTempFile in this-procedure (pAction + "1", search(pAction + ".bat")).
run AddTempFile in this-procedure (pAction + "2", search(pAction + "-srv.dat")).
run AddTempFile in this-procedure (pAction + "3", search(pAction + "-log.dat")).
run AddTempFile in this-procedure (pAction + "4", search(pAction + "-ini.dat")).
run AddTempFile in this-procedure (pAction + "5", search(pAction + "-url.dat")).
run AddTempFile in this-procedure (pAction + "6", search(pAction + "-trc.dat")).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddTempFile) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AddTempFile Procedure 
PROCEDURE AddTempFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pKey as char.
 def input parameter pFile as char.

 def buffer tempfile for tempfile.

 if pKey = ? or pFile = ? 
  then return.

 find tempfile
   where tempfile.id = pKey
     and tempfile.fname = pFile no-error.
 if not available tempfile 
  then
   do: create tempfile.
       tempfile.id = pKey.
       tempfile.fname = pFile.
       tempfile.created = now.
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteActionTempFiles) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteActionTempFiles Procedure 
PROCEDURE DeleteActionTempFiles :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAction as char.
def var std-lo as logical no-undo.

run DeleteTempFile (pAction + "1", output std-lo).
run DeleteTempFile (pAction + "2", output std-lo).
run DeleteTempFile (pAction + "3", output std-lo).
run DeleteTempFile (pAction + "4", output std-lo).
run DeleteTempFile (pAction + "5", output std-lo).
run DeleteTempFile (pAction + "6", output std-lo).
run DeleteTempFile (pAction + "7", output std-lo).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteAllTempFiles) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteAllTempFiles Procedure 
PROCEDURE DeleteAllTempFiles :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 for each tempfile:
  if search(tempfile.fname) <> ? 
   then os-delete value(tempfile.fname).
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteTempFile) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteTempFile Procedure 
PROCEDURE DeleteTempFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pKey as char.
 def output parameter pSuccess as logical init false.

 def buffer tempfile for tempfile.

 for each tempfile
   where tempfile.id = pKey:
  os-delete value(tempfile.fname).
  if os-error <> 0 
   then next.
  delete tempfile.
  pSuccess = true.
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetTempFile) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetTempFile Procedure 
PROCEDURE GetTempFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pKey as char.
 def output parameter pFile as char.

 find last tempfile
   where tempfile.id = pKey no-error.
 if available tempfile 
  then pFile = tempfile.fname.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetTempFiles) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetTempFiles Procedure 
PROCEDURE GetTempFiles :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter table for tempfile.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-OpenTempFile) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenTempFile Procedure 
PROCEDURE OpenTempFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pKey as char.

 def buffer tempfile for tempfile.

 find first tempfile
   where tempfile.id = pKey no-error.
 if available tempfile 
  then run util/openfile.p (tempfile.fname).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-QUIT) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE QUIT Procedure 
PROCEDURE QUIT :
/*------------------------------------------------------------------------------
  Purpose:     Run just before the application quits
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 run DeleteAllTempFiles in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF
