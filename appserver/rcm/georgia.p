&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : rcm/georgia.p                 
    Purpose     : State specific rate calculator

    Syntax      :

    Description :

    Author(s)   :Anjly and Archana
    Created     :
    Notes       :
    @Modified   :
    Date        Name    Comments    
    07/27/2018  Anjly   Testing bug fixes
    09/05/2018  Archana Modified for new framework
    09/28/2018  Anjly   testing bug fix
    11/05/2018  Anubha  UAT Bug Fixes
    04/08/2019  Anjly   Fixed formatting issue and added check on premium amount.
    08/11/2020  AC      Remove progress error in case any string is passed from client 
                        in the variable that expect any integer and consider that value as 0         

  ----------------------------------------------------------------------*/  
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/nadef.i}
{tt\ratelog.i}
{tt\ratelog.i   &tablealias=ttTempRateLog}

define input parameter            parameters    as character no-undo.

define output parameter           premiumOutput  as character no-undo.
define output parameter           debugparameter as character no-undo.
define output parameter table for rateLog.

/*------------------debugging-------------------*/
debugparameter =  "1.parameters in definition=" + parameters.
/*----------------------------------------------*/
    
/* General */
&scoped-define Georgia                    "GA"
&scoped-define Residential                "R"
&scoped-define Multifamily                "M"
&scoped-define ConstructionResidential    "CR"
&scoped-define ConstructionMultifamily    "CM"
                                       
/* Log Calculation for*/                     
&scoped-define OwnersCalc                 "Owner"
&scoped-define LendersCalc                "Loan"
&scoped-define SecondLendersCalc          "Second Loan"
&scoped-define SimultaneousCalc           "Simultaneous" 

/* UseCase Types */                     
&scoped-define Owners                     "O"
&scoped-define Lenders                    "L"
&scoped-define SecondLenders              "S"
                                         
/* Rate Type */                          
&scoped-define Basic                      "B"
&scoped-define Simultaneous               "S" 
                                         
/* Card Selection criteria */            
&scoped-define General                    "G"
&scoped-define Simo                       "Sim"
                                         
/* UI codes for cards*/                  
&scoped-define Original                   "O"
&scoped-define SecondMortgage             "SM"
&scoped-define Homeowners                 "H"
&scoped-define Construction               "C"
&scoped-define JrLimitedCoverage          "JR"
&scoped-define none                       "none"

/* card names */
&scoped-define OwnersOriginal             "OwnersOriginal"                 
&scoped-define LoanOriginalShortForm      "LoanOriginalShortForm"                 
&scoped-define HomeownersOriginal         "HomeownersOriginal"  
&scoped-define SecondLoanOriginal         "SecondLoanOriginal"   
&scoped-define JrLimitedLoan              "JrLimitedLoan"                     
&scoped-define CommercialOwners           "CommercialOwners"            
&scoped-define CommercialLoan             "CommercialLoan" 
&scoped-define CommercialConstructionLoan "CommercialConstructionLoan"                  
&scoped-define ResidentialConstructionLoan "ResidentialConstructionLoan"             
&scoped-define SimoLoan                   "SimoLoan"                         
&scoped-define SimoSecondLoan             "SimoSecondLoan"
&scoped-define SimoResidentialConstruction "SimoResidentialConstruction"
&scoped-define SimoCommercialConstruction "SimoCommercialConstruction"
&scoped-define SimoCommercialOwners       "SimoCommercialOwners"
&scoped-define SimoCommercialLoan         "SimoCommercialLoan"
&scoped-define CPL                        "CPL"


/* Parameters - parse the input parameter and store the information into these variables. */
define variable rateType                  as character initial "rateType".
define variable coverageAmount            as character initial "coverageAmount".                    
define variable loanRateType              as character initial "loanRateType".
define variable loanCoverageAmount        as character initial "loanCoverageAmount". 
define variable secondLoanRateType        as character initial "secondLoanRateType".
define variable secondLoanCoverageAmount  as character initial "secondLoanCoverageAmount".  
define variable propertyType              as character initial "propertyType".
define variable Version                   as character initial "Version".
define variable simultaneous              as character initial "simultaneous".
define variable region                    as character initial "region".                                      
define variable CPL                       as character initial "CPL".      

/* derived variables based on input parameters */
define variable ca                        as integer   no-undo.
define variable loanCa                    as integer   no-undo.
define variable secondLoanCa              as integer   no-undo.
define variable iVersion                  as integer   no-undo.
define variable cardName                  as character no-undo.
define variable caseType                  as character no-undo.
define variable lSimultaneous             as logical   no-undo.
define variable attrID                    as character no-undo.
define variable refRule                   as logical   no-undo.
define variable refRange                  as logical   no-undo.
define variable err                       as logical   no-undo.
define variable iCPL                      as integer   no-undo.

/* basic variables */
define variable iLogSeq                   as integer   no-undo.
define variable iCntParameters            as integer   no-undo.
define variable fCardID                   as integer   no-undo.
define variable premium                   as decimal   no-undo.
define variable cardSetID                 as integer   no-undo.
define variable premiumLoanTemp           as decimal   no-undo.
define variable effectiveDate             as character   no-undo.

/* variables to create output parameter   */
define variable premiumBasic              as decimal   no-undo.
define variable premiumSimo               as decimal   no-undo.
define variable premiumLoan               as decimal   no-undo.
define variable premiumScndLoan           as decimal   no-undo.
define variable premiumOwner              as decimal   no-undo.

define variable errorStatus               as logical   no-undo.
define variable errorMsg                  as character no-undo.
define variable premiumCPL                as decimal   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-CalculateBasicPremium) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD CalculateBasicPremium Procedure 
FUNCTION CalculateBasicPremium RETURNS DECIMAL
  (  pCardName as char , pAmount as int,pAttrID as char , pCaseType as char, pGroupID as int  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CreateLog) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD CreateLog Procedure 
FUNCTION CreateLog RETURNS LOGICAL
  ( pNote as char , pActionCalculation as char, pCardID as int, pTypeAmount as char, pTypeCase as char, pCalculationOn as char, pGroupID as int)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 10.33
         WIDTH              = 57.2.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/*------------------debugging-------------------*/
debugparameter = debugparameter + "2.enter in main block".
/*------------------------------------------------*/

run ExtractParameters.

if iVersion = 0 then 
  for first rateState  where rateState.stateID  = {&Georgia}
                         and rateState.active   = true  :
    cardSetID     = rateState.cardSetID.
    effectiveDate =  if rateState.effectiveDate = ? then "" else  string(rateState.effectiveDate).
  end.
else
  for first rateState  where rateState.stateID  = {&Georgia}
                          and rateState.version = iVersion  :
    cardSetID     = rateState.cardSetID.
    effectiveDate =  if rateState.effectiveDate = ? then "" else  string(rateState.effectiveDate).
  end.
if cardSetID = 0 then
do:
  CreateLog("No Such RateState record found." ,"", 0, "", "", "",0).
  premiumOutput = "servererrmsg="           + "No Such RateState record found."  + 
                  ",success="               + "N"                                +
                  ",cardSetID="             + string(cardSetID)                  + 
                  ",manualeffectiveDate="   + string(effectiveDate) .
  return .
end.

run ServerValidations(output errorStatus,
                      output errorMsg).

if errorStatus = yes then
do:
  premiumOutput = "servererrmsg="           + errorMsg           + 
                  ",success="               + "N"                +
                  ",cardSetID="             + string(cardSetID)  + 
                  ",manualeffectiveDate="   + string(effectiveDate) .                                          
  return .
end.

/*------------------debugging-------------------*/
debugparameter = debugparameter + "7.after server validation".
/*---------------------------------------------------*/

/*----if not get any of case like basic reissue or simo then return-----*/
if caseType = "" then
do:
  premiumOutput = "servererrmsg="           + "Case type not define for given input" + 
                  ",success="               + "N"                                    +
                  ",cardSetID="             + string(cardSetID)                      +
                  ",manualeffectiveDate="   + string(effectiveDate).
  return .
end.
else if caseType = {&Owners} or  caseType = {&Lenders} then                                           
do:  
  /*----------------Basic Calculation for Owner/Lender------------------------------------------------------------------------*/                                                                                             
  if caseType = {&Owners} then
  do:     
    run GetCardNameandAttribute({&General},propertyType,casetype,RateType,      
                                  output cardName,
                                  output attrID,
                                  output err ).                       
    if err then
      return .                                                                                                                   
    premiumOwner = CalculateBasicPremium(cardName,ca,attrID,{&OwnersCalc},1).           
  end.                                                                                                              
  else if caseType = {&lenders} then                                                                                
  do:                                                                                                               
    run GetCardNameandAttribute({&General},propertyType,casetype,loanRateType,
                                  output cardName,
                                  output attrID,
                                  output err ). 
    if err then
      return .
    premiumLoan = CalculateBasicPremium(cardName,loanCa,attrID,{&LendersCalc},1). 
  end.  
end.
/*-----------------------simultaneous Calculation-------------------------------*/ 
else if caseType = {&simultaneous} then       
do:  
  /*-----------------------simultaneous Calculation residential policies--------*/ 
  if propertyType = {&Residential} then
  do:
    if ca > loanCa then
    do:
      run GetCardNameandAttribute({&General},propertyType,{&Owners},RateType,
                                   output cardName,
                                   output attrID,
                                   output err ). 
      if err then
        return .

      premiumBasic = CalculateBasicPremium(cardName,ca,attrID,{&OwnersCalc},1). 
      run GetCardNameandAttribute({&General},propertyType,{&Lenders},loanRateType,
                                  output cardName,
                                  output attrID,
                                  output err ). 
      if err then
        return .   

      premiumLoan  = CalculateBasicPremium(cardName,loanCa,attrID,{&LendersCalc},2). 
      premiumOwner = premiumBasic - premiumLoan.
    end.
    else 
    do:
      run GetCardNameandAttribute({&General},propertyType,{&Owners},RateType,
                                  output cardName,
                                  output attrID,
                                  output err ). 
      if err then
        return .
      premiumBasic = CalculateBasicPremium(cardName,ca,attrID,{&OwnersCalc},1). 
                        
      run GetCardNameandAttribute({&General},propertyType,{&Lenders},loanRateType,
                                  output cardName,
                                  output attrID,
                                  output err ). 
      if err then
        return .
      premiumLoanTemp = CalculateBasicPremium(cardName,ca,attrID,{&OwnersCalc},1). 
      premiumOwner    = premiumBasic - premiumLoanTemp. 
      run GetCardNameandAttribute({&General},propertyType,{&Lenders},loanRateType,
                                  output cardName,
                                  output attrID,
                                  output err ). 
      if err then
        return .
      premiumLoan = CalculateBasicPremium(cardName,loanCa,attrID,{&LendersCalc},2).
    end.

    run GetCardNameandAttribute({&Simo},propertyType,{&Lenders},loanRateType,               
                                  output cardName,
                                  output attrID,              
                                  output err ).                                                   
    if err then                                                                                                                                         
      return .
    premiumSimo  = CalculateBasicPremium(cardName,loanCa,attrID,{&SimultaneousCalc},1). 
    premiumOwner = premiumOwner + premiumSimo.


    if secondloanCa ne 0 then
    do:
      run GetCardNameandAttribute({&Simo},propertyType,{&SecondLenders},secondLoanRateType,
                                  output cardName,
                                  output attrID,
                                  output err ). 
      if err then
        return .
      premiumScndLoan = CalculateBasicPremium(cardName,secondloanCa,attrID,{&SecondLendersCalc},3). 
    end.

    if ca > loanCa then

      CreateLog("Total premium amount for owner" + " = "  + (if premiumBasic      ne 0 then  trim(string(premiumBasic, "$>>>,>>>,>>9.99")) + " - "   else "") 
                                                          + (if premiumLoan       ne 0 then  trim(string(premiumLoan, "$>>>,>>>,>>9.99"))  + " + "   else "") 
                                                          + (if premiumSimo       ne 0 then  trim(string(premiumSimo, "$>>>,>>>,>>9.99"))  else "")  
                                                          +  " = " + trim(string((premiumBasic - premiumLoan + premiumSimo), "$>>>,>>>,>>9.99")),trim(string((premiumBasic - premiumLoan + premiumSimo), ">>>,>>>,>>9.99")), 0, "",{&OwnersCalc}, "",1) .

    else 
      CreateLog("Total premium amount for owner" + " = "  + (if premiumBasic      ne 0 then  trim(string(premiumBasic, "$>>>,>>>,>>9.99"))    + " - "   else "") 
                                                          + (if premiumLoanTemp   ne 0 then  trim(string(premiumLoanTemp, "$>>>,>>>,>>9.99")) + " + "   else "") 
                                                          + (if premiumSimo       ne 0 then  trim(string(premiumSimo, "$>>>,>>>,>>9.99"))  else "") 
                                                          + " = " + trim(string((premiumBasic - premiumLoanTemp + premiumSimo), "$>>>,>>>,>>9.99")),trim(string((premiumBasic - premiumLoanTemp + premiumSimo), ">>>,>>>,>>9.99")), 0, "",{&OwnersCalc}, "",1) .

  end.
  /*-----------------------simultaneous Calculation commercial policies----------------*/ 
  else if propertyType = {&Multifamily} then
  do:
    if ca > loanCa then
    do:
      run GetCardNameandAttribute({&General},propertyType,{&Owners},RateType,
                                  output cardName,
                                  output attrID,
                                  output err ). 
      if err then
        return .
      premiumOwner = CalculateBasicPremium(cardName,ca,attrID,{&OwnersCalc},1). 
      /*-----------calculate for sim card----------------------*/
      run GetCardNameandAttribute({&Simo},propertyType,{&Lenders},loanRateType,
                                  output cardName,
                                  output attrID,
                                  output err ). 
      if err then
        return .
      premiumLoan = CalculateBasicPremium(cardName,loanCa,attrID,{&LendersCalc},2).
    end.
    else 
    do:
      run GetCardNameandAttribute({&Simo},propertyType,{&Owners},RateType,
                                  output cardName,
                                  output attrID,
                                  output err ). 
      if err then
        return .
      
      premiumOwner = CalculateBasicPremium(cardName,ca,attrID,{&OwnersCalc},1). 
      run GetCardNameandAttribute({&General},propertyType,{&Lenders},loanRateType,
                                  output cardName,
                                  output attrID,
                                  output err ). 
      if err then
        return .
      
      premiumLoan = CalculateBasicPremium(cardName,loanCa,attrID,{&LendersCalc},2).
    end.

  end.
  /*-----------------------simultaneous Calculation construction policy-------------*/ 
  else if propertyType = {&ConstructionResidential} or propertyType = {&ConstructionMultifamily} then
  do:
    if ca ge loanCa then
    do:                                                                                                        
      run GetCardNameandAttribute({&General},propertyType,{&Owners},RateType,                                  
                                  output cardName,
                                  output attrID,
                                  output err ). 
      if err then
        return .
      
      premiumOwner = CalculateBasicPremium(cardName,ca,attrID,{&OwnersCalc},1). 
                        
      run GetCardNameandAttribute({&Simo},propertyType,{&Lenders},loanRateType,
                                  output cardName,
                                  output attrID,
                                  output err ). 
      if err then
        return .
      
      premiumLoan = CalculateBasicPremium(cardName,loanCa,attrID,{&LendersCalc},2).
    end.                
    else 
    do:
      run GetCardNameandAttribute({&General},propertyType,{&Owners},RateType,
                                  output cardName,
                                  output attrID,
                                  output err ). 
      if err then
        return .
      
      premiumOwner = CalculateBasicPremium(cardName,ca,attrID,{&OwnersCalc},1). 
       /*-----------calculate for sim card----------------------*/
      run GetCardNameandAttribute({&General},propertyType,{&Lenders},loanRateType,
                                  output cardName,
                                  output attrID,
                                  output err ). 
      if err then
        return .
      premiumLoan = CalculateBasicPremium(cardName,loanCa,attrID,{&LendersCalc},2).   
    end.
  end.
end.        
  if iCPL ne 0 then
  do:
    premiumCPL = CalculateBasicPremium({&CPL},loanCa,"",{&CPL},4). 
    CreateLog("Calculated premium for " + string(iCPL) + " CPL is " + string(iCPL) + " X " + trim(string(premiumCPL, "$>>>,>>>,>>9.99")) + " = " + trim(string((iCPL * premiumCPL), "$>>>,>>>,>>9.99")),trim(string((iCPL * premiumCPL), ">>>,>>>,>>9.99")), 0,"",{&CPL},"",4).
    premiumCPL = iCPL * premiumCPL. 
  end.

 if premiumLoan = ? or premiumOwner = ? or premiumSimo = ? or premiumScndLoan = ? then return.
 CreateLog("Total premium amount = " +  trim(string((premiumLoan + premiumOwner + premiumScndLoan + premiumCPL), "$>>>,>>>,>>9.99")), trim(string((premiumLoan + premiumOwner + premiumScndLoan + premiumCPL), ">>>,>>>,>>9.99")), 0, "","Grand Total", "",0) .
 premiumOutput = "premiumOwner="            + string(premiumOwner)          + 
                 ",premiumLoan="            + string(premiumLoan )          +                  
                 ",premiumScnd="            + string(premiumScndLoan)       +
                 ",premiumCPL="             + string(premiumCPL)            +
                 ",success="                + "Y"                           +
                 ",cardSetID="              + string(cardSetID)             +
                 ",manualeffectiveDate="    + string(effectiveDate).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-ExtractParameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExtractParameters Procedure 
PROCEDURE ExtractParameters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*-----------------------------input variables--------------------------------------*/
do iCntParameters = 1 to num-entries(parameters,","):

  case entry(1,entry(iCntParameters,parameters,","),"^"):
    
    when rateType  then
      rateType = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
    
    when coverageAmount  then
      coverageAmount = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
    
    when loanRateType then
      loanRateType = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
   
    when loanCoverageAmount  then
      loanCoverageAmount = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
    
    when secondLoanRateType  then
      secondLoanRateType = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
    
    when secondLoanCoverageAmount  then
      secondLoanCoverageAmount = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
  
    when propertyType  then
      propertyType = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
   
    when Version  then
      Version = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
    
    when simultaneous  then
      simultaneous = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
    
    when region  then
      region = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
  
    when CPL  then                            
      CPL = entry(2,entry(iCntParameters,parameters,","),"^") no-error.                                                                                                                        
  
  end case.
end.
/*------------------debugging-------------------*/
debugparameter = debugparameter + "3.coverageAmount=" + coverageAmount
                                + "4.loanCoverageAmount=" + loanCoverageAmount.
/*---------------------------------------------------*/

lSimultaneous = logical(simultaneous) no-error.
/*----------------------------------------derived variables------------------------------------------------*/
if (coverageAmount     ne "coverageAmount"     ) and
   (loanCoverageAmount eq "loanCoverageAmount" or  loanCoverageAmount eq "0") and
   (rateType ne "rateType" AND rateType ne {&none}) then
  casetype = {&Owners}.
else if (coverageAmount     eq "coverageAmount"     or  coverageAmount     eq "0" )and
        (loanCoverageAmount ne "loanCoverageAmount" ) and
        (loanRateType ne "loanRateType" AND loanRateType ne {&none})  then
  casetype = {&Lenders}.
else if (coverageAmount     ne "coverageAmount"     and  coverageAmount    ne "0" and  coverageAmount    ne "") and 
        (loanCoverageAmount ne "loanCoverageAmount" and loanCoverageAmount ne "0" and loanCoverageAmount ne "") and
         lSimultaneous      eq true                                                then
  casetype = {&simultaneous}.

/*------------------debugging-------------------*/
debugparameter = debugparameter + "6.casetype=" + casetype.
/*---------------------------------------------------*/

/*---------------------------------------datatype Conversion-----------------------------------------*/
assign
  ca = 0
  loanCa = 0
  secondloanCa = 0.

if coverageAmount ne "coverageAmount" then
  ca = int(coverageAmount) no-error.

if loanCoverageAmount ne "loanCoverageAmount" then
  loanCa = int(loanCoverageAmount) no-error.

if secondLoanCoverageAmount ne "secondLoanCoverageAmount" then
  secondloanCa = int(secondLoanCoverageAmount) no-error.

if Version ne "version" then
  iVersion = int(Version) no-error.
else
  iVersion = 0.

if CPL ne "CPL" then
  iCPL = int(CPL) no-error.
else
  iCPL = 0.

if propertyType = {&Multifamily}  then
   loanCa = loanCa + secondloanCa.


parameters = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetCardNameandAttribute) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCardNameandAttribute Procedure 
PROCEDURE GetCardNameandAttribute :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter pRateCase     as character.
define input parameter pPropertyType as character.
define input parameter pCaseType     as character.
define input parameter pRateType     as character.

define output parameter pCardName    as character.
define output parameter pAttrID      as character.
define output parameter pError       as logical.    

{lib/nacard.i &rateType = pRateType , &cardName = pCardName , &attrID = pAttrID}  
/*------------------debugging-------------------*/
debugparameter = debugparameter + "8.pRateCase=" + pRateCase
                                + "9.pCaseType=" + pCaseType
                                + "10.pRateType=" + pRateType.
/*---------------------------------------------------*/

case pRateCase  :                                                          
  when {&General} then                                                     
  do:                                                                      
    case pPropertyType :                                                   
      when {&Residential} then                                                             
      do:                                                                  
        case pCaseType  :                                                  
          when {&Owners} then                                                
          do:                                                              
            case pRateType : 

              when {&Original} then                                        
                assign                                                     
                  pCardName = {&OwnersOriginal}                            
                  pAttrID   =  "". 
                                                                           
              when {&Homeowners} then                                      
                assign                                                     
                  pCardName = {&HomeownersOriginal}                                 
                  pAttrID   =  "".  
              
              otherwise
                  pError = true.

            end case.                                                      
          end.                                                             
          when {&Lenders} then                                             
          do:                                                              
            case pRateType :   

              when {&Original} then                                        
                assign                                                     
                  pCardName = {&LoanOriginalShortForm}                              
                  pAttrID   =  "".    

              when {&SecondMortgage} then                                  
                assign                                                     
                  pCardName = {&SecondLoanOriginal}                                
                  pAttrID   =  "".                                        

              when {&JrLimitedCoverage} then                               
                assign                                                     
                  pCardName = {&JrLimitedLoan}                             
                  pAttrID   =  "".   
               
               otherwise
                  pError = true.

            end case.                                                      
          end.  

          otherwise
              pError = true.

        end case.                                                          
      end.                                                                 
      when {&Multifamily} then                                             
      do:                                                                  
        case pCaseType :                                                   
          when {&Owners} then                                              
          do:                                                              
            case pRateType :   

              when {&Original} then                                        
                assign                                                     
                  pCardName = {&CommercialOwners}                          
                  pAttrID   =  "". 
               
              otherwise
                  pError = true.

            end case.                                                      
          end.                                                             
          when {&Lenders} then                                             
          do:                                                              
            case pRateType :   

              when {&Original} then                                        
                assign                                                     
                  pCardName = {&CommercialLoan}                            
                  pAttrID   =  "".  
               
              otherwise
                  pError = true.

            end case.                                                      
          end. 

          otherwise
              pError = true.
        end case.                                                          
      end.      

      when {&ConstructionResidential} then                                            
      do:                                                                  
        case pCaseType :  

          when {&Owners} then                                                 
          do:                                                   
            case pRateType :   

              when {&Original} then                                        
                assign                                                     
                  pCardName = {&OwnersOriginal}                            
                  pAttrID   =  "".  
           
              otherwise
                  pError = true.

            end case.  
          end.

          when {&Lenders} then                                             
          do:                                                              
            case pRateType :  

              when {&Construction} then                                    
                assign                                                     
                  pCardName = {&ResidentialConstructionLoan}                          
                  pAttrID   =  "". 
               
              otherwise
                  pError = true.

            end case.                                                      
          end. 

          otherwise
            pError = true.

        end case.
      end.     

      when {&ConstructionMultifamily} then                                            
      do:                                                                  
        case pCaseType :  

          when {&Owners} then                                              
          do:                                                              
            case pRateType : 

              when {&Original} then                                        
                assign                                                     
                  pCardName = {&CommercialOwners}                            
                  pAttrID   =  "".   
               
              otherwise
                  pError = true.

            end case.  
          end.  

          when {&Lenders} then                                             
          do:                                                              
            case pRateType :   

              when {&Construction} then                                    
                assign                                                     
                  pCardName = {&CommercialConstructionLoan}                          
                  pAttrID   =  "".  
               
              otherwise
                  pError = true.

            end case.                                                      
          end.    

          otherwise
            pError = true.

        end case.
      end.

      otherwise
        pError = true.

    end case.
  end.

  when {&Simo} then
  do:
    case pPropertyType:

      when {&Residential} then
      do:
        case pCaseType :

          when {&Lenders} then     
            assign 
              pCardName = {&SimoLoan}             
              pAttrID   =  "".     

          when {&SecondLenders} then  
            assign 
              pCardName = {&SimoSecondLoan}             
              pAttrID   =  "".  

          otherwise
              pError = true.

        end case. 
      end.
      when {&Multifamily} then
      do:
        case pCaseType :

          when {&Owners} then
            assign 
              pCardName = {&SimoCommercialOwners}             
              pAttrID   =  "".   

          when {&Lenders} then
            assign 
              pCardName = {&SimoCommercialLoan}             
              pAttrID   =  "". 

          otherwise
              pError = true.

        end case.
      end.

      when {&ConstructionResidential} then
        assign 
          pCardName = {&SimoResidentialConstruction}             
          pAttrID   =  "".      

      when {&ConstructionMultifamily} then
        assign 
          pCardName = {&SimoCommercialConstruction}             
          pAttrID   =  "".   

      otherwise
          pError = true.

    end case.
  end.

  otherwise
      pError = true.

end case.

debugparameter = debugparameter + "11.pCardName=" + pCardName
                                + "13.pAttrID="   + pAttrID
                                + "15.pError="    + string(pError).

if pError then
  CreateLog("Rate Card does not exist for provided UI Input.","", 0, "","" , "",0) .

premiumOutput = "servererrmsg=" + "Rate Card does not exist for provided UI Input."  + 
                ",success="         + "N"              +
                ",cardSetID="       + string(cardSetID) + 
                ",manualeffectiveDate="   + string(effectiveDate) .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ServerValidations) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ServerValidations Procedure 
PROCEDURE ServerValidations :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define output parameter errorStatus as logical   no-undo.
define output parameter errormsg    as character no-undo.                                                                                                                                

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-CalculateBasicPremium) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION CalculateBasicPremium Procedure 
FUNCTION CalculateBasicPremium RETURNS DECIMAL
  (  pCardName as char , pAmount as int,pAttrID as char , pCaseType as char, pGroupID as int  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
define variable ppremium   as decimal no-undo.
define variable pError     as logical no-undo.
define variable hRateCard  as handle.

{lib/nacheck.i &cardName = pCardName}
/*------------------debugging-------------------*/
debugparameter = debugparameter + "16.pCardName=" + pCardName
                                + "18.pAttrID="   + pAttrID
                                + "20.pAmount="   + string(pAmount)
                                + "21.pCaseType=" + string(pCaseType)
                                + "22.pGroupID="  + string(pGroupID).
/*---------------------------------------------------------*/

run  rcm\ratecard.p persistent set hRateCard (cardSetID,
                                              pCardName,
                                              region,
                                              output fCardID).
/*------------------debugging-------------------*/
debugparameter = debugparameter + "23.fCardID=" + string(fCardID).
/*---------------------------------------------------------*/

if fCardID = 0 then
do:
  run GetLog in hRateCard(output table ttTempRateLog).
  for each ttTempRateLog:
    CreateLog(ttTempRateLog.action ,ttTempRateLog.actioncalculation , ttTempRateLog.cardID,pCardName,pCaseType, ttTempRateLog.calculationOn,pGroupID).
  end.
    premiumOutput = "servererrmsg="         + "Rate card not found with ID " + string(fCardID) + 
                  ",success="               + "N"              +
                  ",cardSetID="             + string(cardSetID) + 
                  ",manualeffectiveDate="   + string(effectiveDate) .
  delete procedure hRateCard.
  return ?.
end.

ppremium = dynamic-function("GetRate" in hRateCard, pAmount, pAttrID, output pError).

/* The calculated permium amount should never be equal to coverage amount. So if thats 
   the case, then assign zero to premium amount. */ 
if ppremium = pAmount then ppremium = 0.

run GetLog in hRateCard(output table ttTempRateLog).
for each ttTempRateLog:
  CreateLog(ttTempRateLog.action ,ttTempRateLog.actioncalculation, ttTempRateLog.cardID,pCardName,pCaseType, ttTempRateLog.calculationOn,pGroupID).
end.

if pError or ppremium = ? then
do:
  premiumOutput = "servererrmsg="           + "Calculation error"  + 
                  ",success="               + "N"              +
                  ",cardSetID="             + string(cardSetID) + 
                  ",manualeffectiveDate="   + string(effectiveDate) .
  delete procedure hRateCard.
  return ?.
end.

delete procedure hRateCard.
CreateLog("Policy premium amount = " + trim(string(ppremium, "$>>>,>>>,>>9.99")),trim(string(ppremium, ">>>,>>>,>>9.99")),0, pCardName,pCaseType,"",pGroupID).
RETURN ppremium.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CreateLog) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION CreateLog Procedure 
FUNCTION CreateLog RETURNS LOGICAL
  ( pNote as char , pActionCalculation as char, pCardID as int, pTypeAmount as char, pTypeCase as char, pCalculationOn as char, pGroupID as int) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define buffer ratelog for ratelog.
 
  iLogSeq = iLogSeq + 1.
  create ratelog.
  assign
    ratelog.seq = iLogSeq
    ratelog.action = pNote
    rateLog.actionCalculation = pActionCalculation
    ratelog.cardID = pCardID
    ratelog.typeofamount = pTypeAmount
    ratelog.calculationFor = pTypeCase
    ratelog.calculationOn = pCalculationOn
    ratelog.groupID =   pGroupID.
 
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

