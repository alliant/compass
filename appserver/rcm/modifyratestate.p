/*------------------------------------------------------------------------
@name modifyratestate.p
@action ratestateModify
@description Modifies an existing ratestate
@throws 3000;Create failed
@throws 3005;No XML file attached
@author Anubha Jain
@version 1.0
@created 11/06/2018
@Modification:
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable hParser    as handle  no-undo.
define variable iSeq       as integer no-undo.
define variable iVersion   as integer no-undo.
define variable iCardSetID as integer no-undo.

{tt/ratestate.i &tablealias=tempRateState}
{tt/ratestate.i &tablealias=ttratestate}    
{tt/file.i}
{lib/xmlencode.i}
{lib/std-def.i}

if not web-context:is-xml or not valid-handle(web-context:x-document) then
do: 
  pResponse:fault("3005", "No XML file attached.").
  return.
end.
  
create sax-reader hParser.
hParser:handler = this-procedure.
hParser:set-input-source("handle", web-context).
hParser:suppress-namespace-processing = true.
hParser:sax-parse() no-error.
std-in = hParser:parse-status.
delete object hParser.
   
std-lo = false.

TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:
  for first tempratestate:
    for first ratestate exclusive-lock
     where ratestate.cardsetID = tempratestate.cardsetID:
      buffer-copy tempratestate except tempratestate.cardsetID to ratestate.
      assign ratestate.lastModifiedDate = now
             ratestate.lastModifiedBy   = pRequest:Uid.
      validate ratestate.
      create ttratestate.
      buffer-copy ratestate to ttratestate.
      release ratestate.
    end.
  end.
  std-lo = true.
end.

if not std-lo
 then
  do: pResponse:fault("3000", "Create").
      return.
  end.
pResponse:success("2000", "ratestate modify").
pResponse:setParameter("ratestate", table ttratestate). 

/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable parseCode   as character  no-undo.
  define variable parseStatus as logical    no-undo.
  define variable parseMsg    as character  no-undo.
  define variable oldFormat   as character  no-undo.

 assign
    oldFormat = session:date-format
    session:date-format = "ymd"
    . 

  {lib/srvstartelement.i}

  if qName = "ratestate"
  then
    do: 
      create tempratestate.
      do i = 1 TO  attributes:num-items:
        fldvalue = attributes:get-value-by-index(i).
        case attributes:get-qname-by-index(i):	
          when "cardSetID"           then tempratestate.cardSetID             = integer(fldvalue).
          when "version"             then tempratestate.version               = integer(fldvalue).
          when "active"              then tempratestate.active                = logical(fldvalue).
          when "stateID"             then tempratestate.stateID               = fldvalue.
          when "effectiveDate"       then tempratestate.effectiveDate         = datetime(fldvalue).
          when "createdBy"           then tempratestate.createdBy             = fldvalue.
          when "dateApproved"        then tempratestate.dateApproved          = datetime(fldvalue).
          when "approvedBy"          then tempratestate.approvedBy            = fldvalue.
          when "dateFiled"           then tempratestate.dateFiled             = datetime(fldvalue).
          when "filingMethod"        then tempratestate.filingMethod          = fldvalue.
          when "dateStateApproved"   then tempratestate.dateStateApproved     = datetime(fldvalue).
          when "description"         then tempratestate.description           = fldvalue.
          when "comments"            then tempratestate.comments              = fldvalue.
          when "lastModifiedDate"    then tempratestate.lastModifiedDate      = datetime(fldvalue).
          when "lastModifiedBy"      then tempratestate.lastModifiedBy        = fldvalue.
          when "execBusinessLogic"   then tempratestate.execBusinessLogic     = fldvalue.
          when "execDataModel"       then tempratestate.execDataModel         = fldvalue.
          when "execUserInterface"   then tempratestate.execUserInterface     = fldvalue.
          when "configUserInterface" then tempratestate.configUserInterface   = fldvalue.
        end case.
      end.
  end. 
  session:date-format = oldFormat.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

