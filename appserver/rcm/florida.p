&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : rcm/florida.p
    Purpose     : State specific rate calculator

    Syntax      :

    Description :

    Author(s)   : Vikas and Archana
    Created     :
    Notes       :
    @Modified    :
    Date        Name    Comments
    07/27/2018  Vikas   Testing bug fixes
    09/28/2018  Archana Modified for new framework
    03/11/2019  Vikas   Modified for UAT comments
    03/20/2019  Anjly   Fixed formating 
    03/25/2019  Vikas   Modified for UAT comments
    04/08/2019  Anjly   Fixed formatting issue and added check on premium amount.
    05/07/2019  Anjly   Testing bug fix
    05/07/2019  Anjly   Modify to create endorsement 9,Navi as premium specific 
                        as per UAT comments
    09/12/2019  Sachin  Testing bug fix  
    09/26/2019  Anubha  Added "Owner's Reissue" in Simultaneous case (UAT feedback) 
    08/11/2020  AC      Remove progress error in case any string is passed from client 
                        in the variable that expect any integer and consider that value as 0         

  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib\std-def.i}
{lib\nadef.i}
{tt\ratelog.i}
{tt\ratelog.i &tablealias=ttTempRateLog}

define input parameter            parameters     as character no-undo.

define output parameter           premiumOutput  as character no-undo.
define output parameter           debugparameter as character no-undo.
define output parameter table for rateLog.

    /*------------------debugging-------------------*/
   debugparameter =  "1.parameters in definition=" + parameters.
   /*----------------------------------------------*/
    
/* General */
&scoped-define Florida      "FL"
&scoped-define Residential  "R"
&scoped-define null         "null"

/* UseCase Types */
&scoped-define Owners       "O"
&scoped-define Lenders      "L"

/* Rate Type */
&scoped-define Basic        "B"
&scoped-define Reissue      "Re"
&scoped-define Simultaneous "S" 

/* Card Selection criteria */
&scoped-define General      "G"
&scoped-define Excess       "X"
&scoped-define Simo         "Sim"
&scoped-define SimoExcess   "SimX"

/* Log Calculation for*/                     
&scoped-define OwnersPolicy             "Owner Policy"
&scoped-define LendersPolicy            "Loan Policy"
&scoped-define SimultaneousCalc         "Simultaneous" 
&scoped-define OwnersEndors             "Owner endorsement"
&scoped-define LendersEndors            "Loan endorsement"
/* Log Typeofamount*/ 
&scoped-define Face                     "Face"
&scoped-define Excess                   "Excess"
&scoped-define Endorsements             "Endorsements" 

/* UI codes for RateType*/                  
&scoped-define Original                 "O"
&scoped-define NewHomePurchaseDiscount  "NH"
&scoped-define ContractPurchaser        "CP"
&scoped-define SubstitutionRefinance    "SR"
&scoped-define none                     "none"

&scoped-define LE3YEARS                 "3"
&scoped-define LE4YEARS                 "4"
&scoped-define LE5YEARS                 "5"
&scoped-define LE10YEARS                "10"
&scoped-define GT10YEARS                "Over10"

/* card names */
&scoped-define OwnersOriginal                     "OwnersOriginal"
&scoped-define ReissueCard                        "Reissue"
&scoped-define ReissueExcess                      "ReissueExcess"
&scoped-define LoanOriginal                       "LoanOriginal"
&scoped-define OwnerNewHomePurchaseDiscount       "NewHomePurchaseDiscount"
&scoped-define OwnerNewHomePurchaseDiscountExcess "NewHomePurchaseDiscountExcess"
&scoped-define OwnerContractPurchaser             "ContractPurchaser"
&scoped-define SubstitutionLoan                   "SubstitutionLoan"
&scoped-define SubstitutionLoanExcess             "SubstitutionLoanExcess"
&scoped-define SimoLoan                           "SimoLoan"
&scoped-define SimoLoanExcess                     "SimoLoanExcess"

&scoped-define PremiumSpecificRates               "9,Navi"


/* Parameters - parse the input parameter and store the information into these variables. */
define variable rateType                  as character initial "rateType".
define variable coverageAmount            as character initial "coverageAmount".                    
define variable reissueCoverageAmount     as character initial "reissueCoverageAmount".
define variable loanRateType              as character initial "loanRateType".
define variable loanReissueCoverageAmount as character initial "loanReissueCoverageAmount".
define variable loanCoverageAmount        as character initial "loanCoverageAmount". 
define variable ownerEndors               as character initial "ownerEndors".
define variable loanEndors                as character initial "loanEndors".
define variable loanPriorEffectiveDate    as character initial "loanPriorEffectiveDate".
define variable secondLoanEndors          as character initial "secondLoanEndors".
define variable propertyType              as character initial "propertyType".
define variable Version                   as character initial "Version".
define variable simultaneous              as character initial "simultaneous".
define variable region                    as character initial "region".
                                                                                  
/* derived variables based on input parameters */
define variable ca                        as integer   no-undo.
define variable reissueCa                 as integer   no-undo.
define variable loanCa                    as integer   no-undo.
define variable loanReissueCa             as integer   no-undo.
define variable iVersion                  as integer   no-undo.
define variable selectedEndors            as character no-undo.
define variable rateCase                  as character no-undo.
define variable cardName                  as character no-undo.
define variable caseType                  as character no-undo.
define variable lSimultaneous             as logical   no-undo.
define variable dtloanPriorEffectiveDate  as date      no-undo .
define variable premiumSpecificOwnerEndors as character no-undo.
define variable premiumSpecificLoanEndors  as character no-undo.
define variable otherOwnerEndors           as character no-undo.
define variable otherLoanEndors            as character no-undo.

/* basic variables */
define variable iLogSeq                   as integer   no-undo.
define variable iCntParameters            as integer   no-undo.
define variable endorsPremium             as integer   no-undo.
define variable fCardID                   as integer   no-undo.
define variable hRateCard                 as handle    no-undo.
define variable hExcessAmountCalc         as handle    no-undo.
define variable hEndCalc                  as handle    no-undo.
define variable cardSetID                 as integer   no-undo.
define variable attrID                    as character no-undo.
define variable err                       as logical   no-undo.
define variable effectiveDate             as character no-undo.

/* variables to create output parameter   */
define variable premiumBasic              as decimal   no-undo.
define variable premiumExcess             as decimal   no-undo.  

define variable premiumOwner              as decimal   no-undo.            
define variable premiumLoan               as decimal   no-undo.            

define variable premiumOEndors            as decimal   no-undo.            
define variable cPremiumOEndorsDetail     as character no-undo.
define variable cOwnerEndorsDetail        as character no-undo.
define variable premiumLEndors            as decimal   no-undo.           
define variable cPremiumLEndorsDetail     as character no-undo.
define variable cLenderEndorsDetail       as character no-undo.

define variable errorStatus               as logical   no-undo.
define variable errorMsg                  as character no-undo.

define variable dtEffectiveDate           as date      no-undo.
define variable deNumDays                 as integer   no-undo.
define variable deAgeOfOriginalLoan       as integer   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-CalculateBasicPremium) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD CalculateBasicPremium Procedure 
FUNCTION CalculateBasicPremium RETURNS DECIMAL
  (  pCardName as char ,pAttrID as char , pAmount as int, pCaseType as char, pGroupID as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CalculateEndors) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD CalculateEndors Procedure 
FUNCTION CalculateEndors RETURNS DECIMAL
  ( pAmount as decimal , pGroupName as char,pCaseType as char,pGroupID as int , pSelectedEndorsement as char, pPropertyType as char,output premiumEndorsDetail as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CalculateExcessPremium) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD CalculateExcessPremium Procedure 
FUNCTION CalculateExcessPremium RETURNS DECIMAL
  (  pCardName as char ,pAttrID as char , pExcessAmount as int, pAmount as int, pCaseType as char, pGroupID as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CreateLog) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD CreateLog Procedure 
FUNCTION CreateLog RETURNS LOGICAL
  ( pNote as char , pActionCalculation as character,  pCardID as int, pTypeAmount as char, pTypeCase as char, pCalculationOn as char, pGroupID as int)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-RemoveItemFromList) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD RemoveItemFromList Procedure 
FUNCTION RemoveItemFromList returns character
    (pclist as character,
     pcremoveitem as character,
     pcdelimiter as character) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 10.14
         WIDTH              = 55.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/*------------------debugging-------------------*/
debugparameter = debugparameter + "2.enter in main block".
/*------------------------------------------------*/

run ExtractParameters.

if iVersion = 0 then 
  for first rateState where rateState.stateID = {&Florida}
                         and rateState.active  = true:
    cardSetID     = rateState.cardSetID.
    effectiveDate = if rateState.effectiveDate = ? then "" else string(rateState.effectiveDate).
  end.
else
  for first rateState where rateState.stateID = {&Florida}
                         and rateState.version = iVersion:
    cardSetID     = rateState.cardSetID.
    effectiveDate = if rateState.effectiveDate = ? then "" else string(rateState.effectiveDate).
  end.
if cardSetID = 0 then 
do:    
  CreateLog("No Such RateState record found." ,"", 0, "", "","",0).
  premiumOutput = "servererrmsg="           + "No Such RateState record found." + 
                  ",success="               + "N"                               +
                  ",cardSetID="             + string(cardSetID)             +
                  ",manualeffectiveDate="   + string(effectiveDate) .
  return .
end.

run ServerValidations(output errorStatus,
                      output errorMsg).
if errorStatus = yes then
do:
  premiumOutput = "servererrmsg="           + errorMsg             + 
                  ",success="               + "N"                  +
                  ",cardSetID="             + string(cardSetID)    + 
                  ",manualeffectiveDate="   + string(effectiveDate) .
  return .
end.

 /*------------------debugging-------------------*/
debugparameter = debugparameter + "7.after server validation".
/*---------------------------------------------------*/
/*----if not get any of case like basic reissue or simo then return-----*/
if caseType = "" then
do:
    premiumOutput = "servererrmsg="     + "Case type not defined for given input" + 
                  ",success="           + "N"                                    +
                  ",cardSetID="         + string(cardSetID)                      +
                  ",manualeffectiveDate="     + string(effectiveDate).                                                                
  return .
end.
if caseType = {&Owners} or  caseType = {&Lenders} then                                           
do:  
  /*----------------Basic Calculation for Owner/Lender------------------------------------------------------------------------*/
  if rateCase = {&Basic} then                                                                    
  do:                                                                                              
    if caseType = {&Owners} then 
    do: 
      run GetCardNameandAttribute({&General},caseType,ratetype,      
                                  output cardName,
                                  output attrID,
                                  output err ).
      premiumOwner       = CalculateBasicPremium(cardName,attrID,ca,{&OwnersPolicy},1).
    end.                
    else if caseType = {&lenders} then 
    do:
      run GetCardNameandAttribute({&General},caseType,loanRateType,      
                                  output cardName,
                                  output attrID,
                                  output err ).
      premiumLoan       = CalculateBasicPremium(cardName,attrID,loanCa,{&LendersPolicy},2). 
    end.    
  end.  /*-----end of basic case---------*/

  /*------------------------Reissue Calculation for Owner/Lender-------------------------------------------------------------------*/
  if rateCase = {&Reissue} then                                                                  
  do:      
    /*--------------Reissue Face amount and newHomeOwnerPurchase calculation  if prior amount is 0 or greater than original amount----------*/
    if caseType = {&Owners} then
    do:
      run GetCardNameandAttribute({&General},caseType,ratetype,      
                                   output cardName,
                                   output attrID,
                                   output err ). 
     
      if (reissueCa > ca) or (cardName = {&OwnerNewHomePurchaseDiscount}) then
        premiumBasic       = CalculateBasicPremium(cardName,attrID,ca,{&OwnersPolicy},1). 
      else if (reissueCa > 0) then
        premiumBasic       = CalculateBasicPremium(cardName,attrID,reissueCa,{&OwnersPolicy},1).             
    end.             
    else if caseType = {&lenders} then
    do:
      run GetCardNameandAttribute({&General},caseType,LoanRateType,      
                                   output cardName,
                                   output attrID,
                                   output err ). 
      if (loanReissueCa > loanCa) then
        premiumBasic     = CalculateBasicPremium(cardName,attrID,loanCa,{&LendersPolicy},2). 
      else if (loanReissueCa > 0) then
        premiumBasic     = CalculateBasicPremium(cardName,attrID,loanReissueCa,{&LendersPolicy},2). 
    end.
    /*---------------------------------Reissue excess amount and newHomeOwnerPurchase calculation for prior amount less than original amount----------------------*/
    if (ca > reissueCa or loanCa > loanReissueCa) or cardName = {&OwnerNewHomePurchaseDiscount} then
    do:
      if caseType = {&Owners} then 
      do:
        run GetCardNameandAttribute({&Excess},caseType,ratetype,
                                    output cardName,
                                    output attrID,
                                    output err ). 

        /*----------------in case of owner differentiating reissue and newHomeOwnerPurchase case--------------------------------*/
        if (cardName = {&OwnerNewHomePurchaseDiscountExcess} and reissueCa > 0)  then
        do:
            premiumExcess   = CalculateBasicPremium(cardName,attrID,reissueCa,{&OwnersPolicy},1).
            premiumOwner = premiumBasic - premiumExcess.
            CreateLog("Total policy premium amount " + trim(string(premiumBasic, "$>>>,>>>,>>9.99")) + " - " + trim(string(premiumExcess, "$>>>,>>>,>>9.99")) + " = " + trim(if premiumOwner < 0 then string(premiumOwner) else string(premiumOwner,"$>>>,>>>,>>9.99")), trim(if premiumOwner < 0 then string(premiumOwner) else string(premiumOwner,">>>,>>>,>>9.99")),0, "",{&OwnersPolicy},"",1).
        end.
        else if (cardName ne {&OwnerNewHomePurchaseDiscountExcess})  then
        do:
            premiumExcess   = CalculateExcessPremium(cardName,attrID,ca,reissueCa,{&OwnersPolicy},1).                
            CreateLog("Total policy premium amount " + trim(string(premiumBasic, "$>>>,>>>,>>9.99")) + " + " + trim(string(premiumExcess, "$>>>,>>>,>>9.99")) + " = " + trim(string((premiumBasic + premiumExcess), "$>>>,>>>,>>9.99")), trim(string((premiumBasic + premiumExcess), ">>>,>>>,>>9.99")),0, "",{&OwnersPolicy},"",1).
            premiumOwner = premiumBasic + premiumExcess.
        end.  
      end.
      else if caseType = {&lenders} then
      do:
        run GetCardNameandAttribute({&Excess},caseType,loanRateType,      
                                    output cardName,
                                    output attrID,
                                    output err ). 
        premiumExcess   = CalculateExcessPremium(cardName,attrID,loanCa, loanReissueCa,{&LendersPolicy},2).            
        CreateLog("Total policy premium amount " + trim(string(premiumBasic, "$>>>,>>>,>>9.99")) + " + " + trim(string(premiumExcess, "$>>>,>>>,>>9.99")) + " = " + trim(string((premiumBasic + premiumExcess), "$>>>,>>>,>>9.99")),trim(string((premiumBasic + premiumExcess), ">>>,>>>,>>9.99")),0, "",{&LendersPolicy},"",2).
        premiumLoan = premiumBasic + premiumExcess.      
      end. 
    end.
    else if caseType = {&owners} 
     then
      premiumOwner = premiumBasic.
    else 
      premiumLoan  = premiumBasic.   
    
    if caseType = {&Owners} and premiumOwner < 100 
     then
      do:
         premiumOwner = 100.
         CreateLog("Applying minimum rule $100",string(premiumOwner,">>>,>>>,>>9.99"),100, "",{&OwnersPolicy},"", 1 ).
      end. 
      
    if caseType = {&Lenders} and premiumLoan < 100
     then
      do:
         premiumLoan = 100.
         CreateLog("Applying minimum rule $100",string(premiumLoan,">>>,>>>,>>9.99"),100, "",{&LendersPolicy},"", 2 ).
      end.
  end. /*-------end of 'reissue' ratecase--------*/
end. /*-------end of individual owner and lender--------*/

/*-----------------------simultaneous Calculation = Owner & Lender & Second Lender--------------------------------------------------*/
else if caseType = {&simultaneous} then
do:
  /*--------------------Owner's Coverage Amount & Endorsements Calculations--------------------------------------------------*/
  run GetCardNameandAttribute({&General},{&Owners},ratetype,      
                              output cardName,
                              output attrID,
                              output err ). 

  if rateCase = {&Basic}          
   then 
    premiumOwner = CalculateBasicPremium(cardName,attrID,ca,{&OwnersPolicy},1).
  else if rateCase = {&Reissue}       /* Calculating premium for Owner's Reissue in Simultaneous scenario*/
   then
    do: 
       if (reissueCa ne 0) 
        then 
         premiumOwner = CalculateBasicPremium(cardName,attrID,min(ca,reissueCa),{&OwnersPolicy},1).
        else
         premiumOwner = CalculateBasicPremium(cardName,attrID,ca,{&OwnersPolicy},1). 

       if (reissueCa ne 0) and (ca > reissueCa) 
        then
         do:
            run GetCardNameandAttribute({&Excess},{&Owners},ratetype,      
                                         output cardName,
                                         output attrID,
                                         output err ).  
            premiumExcess   = CalculateExcessPremium(cardName,attrID,ca,reissueCa,{&OwnersPolicy},1).       
            
            premiumOwner    = premiumOwner + premiumExcess.
            premiumExcess   = 0.
         end.                 
       
       if premiumOwner < 100 
        then
         do:
            premiumOwner = 100.
            CreateLog("Applying minimum rule $100",string(premiumOwner,">>>,>>>,>>9.99"),100, "",{&OwnersPolicy},"", 1 ).
         end. 
    end.   
  /*--------------------Lender's Face Amount & Excess Amount &  Endorsements Calculations----------------------------*/
  run GetCardNameandAttribute({&Simo},{&Lenders},{&Original},      
                            output cardName,
                            output attrID,
                            output err ). 
  premiumLoan           = CalculateBasicPremium(cardName,attrID,loanCa,{&LendersPolicy},2).
  /*------------------------------------loan excess amount calculation---------------------------------------*/
  if loanCa > ca then
  do:
    run GetCardNameandAttribute({&SimoExcess},{&Lenders},{&Original},      
                            output cardName,
                            output attrID,
                            output err ). 
    premiumExcess       = CalculateExcessPremium(cardName,attrID,loanCa,ca,{&LendersPolicy},2).
    premiumLoan         = premiumLoan + premiumExcess.
  end.   
end.

 /*-------------------------------------Endorsement Calculations----------------------------------------------------------------*/
 

if premiumSpecificOwnerEndors ne "" then
  premiumOEndors    = CalculateEndors(premiumOwner + premiumLoan,{&OwnersEndors},{&Owners},1,premiumSpecificOwnerEndors,propertyType,output cPremiumOEndorsDetail).
  
if  otherOwnerEndors ne "" then
  premiumOEndors    = premiumOEndors + CalculateEndors(ca,{&OwnersEndors},{&Owners},1,otherOwnerEndors,propertyType,output cOwnerEndorsDetail).
    
if premiumSpecificOwnerEndors ne "" or otherOwnerEndors ne ""  then
 CreateLog("Total owner endorsements premium amount is " + trim(string(decimal(premiumOEndors),"$>>>,>>>,>>9.99")),trim(string(decimal(premiumOEndors),">>>,>>>,>>9.99")), 0, {&endorsements},{&OwnersEndors},"",1).

 
if premiumSpecificLoanEndors ne "" then
  premiumLEndors    = CalculateEndors(premiumOwner + premiumLoan,{&LendersEndors},{&Lenders},2,premiumSpecificLoanEndors,propertyType,output cPremiumLEndorsDetail).
  
if  otherLoanEndors ne ""  then
  premiumLEndors    = premiumLEndors + CalculateEndors(loanCa,{&LendersEndors},{&Lenders},2,otherLoanEndors,propertyType,output cLenderEndorsDetail).
  
if premiumSpecificLoanEndors ne "" or  otherLoanEndors ne ""  then
  CreateLog("Total lender endorsements premium amount is " + trim(string(decimal(premiumLEndors),"$>>>,>>>,>>9.99")),trim(string(decimal(premiumLEndors),">>>,>>>,>>9.99")), 0, {&endorsements},{&LendersEndors},"",2).

if premiumOwner = ? or premiumLoan = ? or premiumExcess = ? or premiumOEndors = ? or premiumLEndors = ? then return.

CreateLog("Total premium amount "   + trim(trim((if premiumOwner      ne 0 then  trim(string(premiumOwner, "$>>>,>>>,>>9.99"))            else "") 
                                                + (if premiumOEndors    ne 0 then  " + " + trim(string(premiumOEndors, "$>>>,>>>,>>9.99"))  else "")  
                                                + (if premiumLoan       ne 0 then  " + " + trim(string(premiumLoan, "$>>>,>>>,>>9.99"))     else "") 
                                                + (if premiumLEndors    ne 0 then  " + " + trim(string(premiumLEndors, "$>>>,>>>,>>9.99"))  else "") ," "),"+")
                                                + " = " + trim(string(premiumOwner + premiumLoan  + premiumOEndors + premiumLEndors, "$>>>,>>>,>>9.99")),
                                                string(premiumOwner + premiumLoan  + premiumOEndors + premiumLEndors, ">>>,>>>,>>9.99"), 0,"","Grand Total","",0). 
                                                
    premiumOutput = "premiumOwner="          + string(premiumOwner)                    + 
                    ",premiumLoan="          + string(premiumLoan)                     + 
                    ",premiumOEndors="       + string(premiumOEndors)                  +
                    ",premiumOEndorsDetail=" + trim((cPremiumOEndorsDetail + "|" + cOwnerEndorsDetail),"|")            +                     
                    ",premiumLEndors="       + string(premiumLEndors)                  +           
                    ",premiumLEndorsDetail=" + trim((cPremiumLEndorsDetail + "|" + cLenderEndorsDetail),"|")           +
                    ",success="              + "y"                                     +
                    ",cardSetID="            + string(cardSetID)                       +
                    ",manualeffectiveDate="        + string(effectiveDate).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-ExtractParameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExtractParameters Procedure 
PROCEDURE ExtractParameters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define variable iCountEndors as integer no-undo.

/*-----------------------------input variables--------------------------------------*/
do iCntParameters = 1 to num-entries(parameters,","):

case entry(1,entry(iCntParameters,parameters,","),"^"):
  
  when rateType  then
       rateType = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
  
  when coverageAmount  then
       coverageAmount = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
  
  when reissueCoverageAmount  then
       reissueCoverageAmount = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
  
  when loanRateType then
       loanRateType = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
 
  when loanCoverageAmount  then
       loanCoverageAmount = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
  
  when loanReissueCoverageAmount  then
  loanReissueCoverageAmount     = entry(2,entry(iCntParameters,parameters,","),"^") no-error.  
 
  when ownerEndors  then
  ownerEndors = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
  
  when loanEndors then
  loanEndors = entry(2,entry(iCntParameters,parameters,","),"^") no-error.

  when loanPriorEffectiveDate then
    loanPriorEffectiveDate = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
 
  when propertyType  then
    propertyType = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
 
  when Version  then
  Version = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
  
  when simultaneous  then
  simultaneous = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
  
  when region  then
    region = entry(2,entry(iCntParameters,parameters,","),"^") no-error.

end case.
end.
/*------------------debugging-------------------*/
debugparameter = debugparameter + "3.coverageAmount=" + coverageAmount
                                + "4.loanCoverageAmount=" + loanCoverageAmount.
/*---------------------------------------------------*/

lSimultaneous = logical(simultaneous) no-error.
dtloanPriorEffectiveDate = date(loanPriorEffectiveDate) no-error.

/*----------------------------------------derived variables------------------------------------------------*/
if (coverageAmount     ne "coverageAmount"     ) and
   (loanCoverageAmount eq "loanCoverageAmount" or  loanCoverageAmount eq "0")and
   (rateType ne "rateType" and rateType ne {&none}) then
  casetype = {&Owners}.
else if (coverageAmount     eq "coverageAmount"     or  coverageAmount     eq "0" )and
        (loanCoverageAmount ne "loanCoverageAmount" )and
        (loanRateType ne "loanRateType" and loanRateType ne {&none}) then
  casetype = {&Lenders}.
else if (coverageAmount     ne "coverageAmount"     and  coverageAmount    ne "0") and
        (loanCoverageAmount ne "loanCoverageAmount" and loanCoverageAmount ne "0") and
         lSimultaneous      eq true                                                then
  casetype = {&simultaneous}.

if (reissueCoverageAmount ne "reissueCoverageAmount" and reissueCoverageAmount ne "0")
  or (loanReissueCoverageAmount ne "loanReissueCoverageAmount" and loanReissueCoverageAmount ne "0") then
  rateCase = {&Reissue}.
else 
  rateCase = {&Basic}. 

 /*------------------debugging-------------------*/
debugparameter = debugparameter + "5.rateCase=" + rateCase
                                + "6.casetype=" + casetype.
/*---------------------------------------------------*/

if dtloanPriorEffectiveDate = ? then
  deAgeOfOriginalLoan = 0.
else
  deAgeOfOriginalLoan = truncate((today - dtloanPriorEffectiveDate) / 365, 0).
  deNumDays  = (today - dtloanPriorEffectiveDate) modulo 365.
  deAgeOfOriginalLoan = if deNumDays > 0 then (deAgeOfOriginalLoan + 1) else deAgeOfOriginalLoan.


/*---------------------------------------datatype Conversion-----------------------------------------*/
assign
  ca = 0
  loanCa = 0  
  reissueCa = 0.

if coverageAmount ne "coverageAmount" then
  ca = int(coverageAmount) no-error.

if reissueCoverageAmount ne "reissueCoverageAmount" then
  reissueCa = int(reissueCoverageAmount) no-error.

if loanCoverageAmount ne "loanCoverageAmount" then
  loanCa = int(loanCoverageAmount) no-error.

if loanReissueCoverageAmount ne "loanReissueCoverageAmount" then
  loanReissueCa = int(loanReissueCoverageAmount) no-error.

if Version ne "version" then
  iVersion = int(Version) no-error.
else
  iVersion = 0.
  
  
  
 assign
    otherOwnerEndors      = ownerEndors
    otherLoanEndors       = loanEndors. 
  
  
 do iCountEndors = 1 to num-entries(ownerEndors , "|"): 
   if lookup(entry(1,entry(iCountEndors,ownerEndors,"|"),"-"), {&PremiumSpecificRates}) > 0 then
   do:
     premiumSpecificOwnerEndors = premiumSpecificOwnerEndors + "|" + entry(1,entry(iCountEndors,ownerEndors,"|"),"-") + "-" + entry(2,entry(iCountEndors,ownerEndors,"|"),"-").
     otherOwnerEndors = removeItemFromList(otherOwnerEndors,entry(iCountEndors,ownerEndors,"|"),"|" ).
   end.
 end.
 premiumSpecificOwnerEndors = trim(premiumSpecificOwnerEndors,"|").

 
 do iCountEndors = 1 to num-entries(loanEndors , "|"): 
   if lookup(entry(1,entry(iCountEndors,loanEndors,"|"),"-"), {&PremiumSpecificRates}) > 0 then
   do:
     premiumSpecificLoanEndors = premiumSpecificLoanEndors + "|" + entry(1,entry(iCountEndors,loanEndors,"|"),"-") + "-" + entry(2,entry(iCountEndors,loanEndors,"|"),"-").
     otherLoanEndors = removeItemFromList(otherLoanEndors,entry(iCountEndors,loanEndors,"|"),"|" ).
   end.
 end.
 premiumSpecificLoanEndors = trim(premiumSpecificLoanEndors,"|").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetCardNameandAttribute) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCardNameandAttribute Procedure 
PROCEDURE GetCardNameandAttribute :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter pRateCase as character.
define input parameter pCaseType as character.
define input parameter pRateType as character.

define output parameter pCardName     as character.
define output parameter pAttrID       as character.
define output parameter pError        as logical.
                                    
{lib/nacard.i &rateType = pRateType , &cardName = pCardName , &attrID = pAttrID}   
/*------------------debugging-------------------*/
/* debugparameter = debugparameter + "8.pRateCase=" + pRateCase   */
/*                                 + "9.pCaseType=" + pCaseType   */
/*                                 + "10.pRateType=" + pRateType. */
/*---------------------------------------------------*/


case pRateCase :                    
/*-----------------------case for basic and reissue rate cards------------------*/                                   
  when {&General} then                
  do:                                 
    case pRateType:
      when {&Original} then
      do:
        if pCaseType = {&Owners} then 
          assign
            pCardName = {&OwnersOriginal}
            pAttrID   =  "". 
  
        else if pCaseType = {&Lenders} then
          assign
            pCardName = {&LoanOriginal}
            pAttrID   =  "". 
      end.   
  
      when {&Reissue} then
      do:
        if pCaseType = {&Owners} then
          assign
            pCardName = {&ReissueCard}
            pAttrID   =  "". 
  
        else if pCaseType = {&Lenders} then
          assign
            pCardName = {&ReissueCard}
            pAttrID   =  "".  
      end.
     
      when {&SubstitutionRefinance} then
      do:
        assign
          pCardName = {&SubstitutionLoan}.

        if deAgeOfOriginalLoan <= 3 then
            pAttrID   =  {&LE3YEARS}. 
        else if deAgeOfOriginalLoan <= 4 then
            pAttrID   =   {&LE4YEARS}.
        else if deAgeOfOriginalLoan <= 5 then
            pAttrID   =   {&LE5YEARS}.
        else if deAgeOfOriginalLoan <= 10 then
            pAttrID   =   {&LE10YEARS}.
        else
            pAttrID   =   {&GT10YEARS}.       
      end.     
  
      when {&NewHomePurchaseDiscount} then
       assign
       pCardName = {&OwnerNewHomePurchaseDiscount}
       pAttrID   =  "".
      
      when {&ContractPurchaser} then
       assign
        pCardName = {&OwnerContractPurchaser}
        pAttrID   =  "".
       
      otherwise
        pError = true.
  
    end case.
  end.

/*--------------------case for excess amount of reissue cards--------------------*/
  when {&Excess} then
  do:
    case pRateType:
      when {&Reissue} then
      do:
        if pCaseType = {&Owners} then
          assign
            pCardName = {&ReissueExcess}
            pAttrID   =  "". 
  
        else if pCaseType = {&Lenders} then
          assign
            pCardName = {&ReissueExcess}
            pAttrID   =  "".  
      end.      
  
      when {&NewHomePurchaseDiscount} then
        assign
          pCardName = {&OwnerNewHomePurchaseDiscountExcess}
          pAttrID   =  "".

      when {&SubstitutionRefinance} then
        assign
          pCardName = {&SubstitutionLoanExcess}
          pAttrID = "".
  
      otherwise
        pError = true.
  
    end case.
  end.
/*-----------------------case for lenders simultaneous rate cards--------------------------*/
  when {&Simo} then
  do:
    case pRateType:
      when {&Original} then
      do:
        if pCaseType = {&Lenders} then   
          assign
            pCardName = {&SimoLoan}
            pAttrID   =  "".    
      end.    
      otherwise
        pError = true.
    end case.
  end.
/*----------------case for excess amount of simultaneous rate cards--------------------------------*/
  when {&SimoExcess} then
  do:
    case pRateType:
      when {&Original} then
      do:
        if pCaseType = {&Lenders} then   
          assign
            pCardName = {&SimoLoanExcess}
            pAttrID   =  "".    
      end.    
      otherwise
        pError = true.
    end case.
  end.
end case.

 debugparameter = debugparameter + "11.pCardName=" + pCardName    
                                 + "12.pAttrID=" + pAttrID        
                                 + "13.pError=" + string(pError). 

if pError then
  CreateLog("Rate Card does not exist for provided UI Input.","", 0, "","","",0 ) .

premiumOutput = "servererrmsg="       + "Rate Card does not exist for provided UI Input." + 
                ",success="           + "N"                                                 +
                ",cardSetID="       + string(cardSetID)                                     + 
                ",manualeffectiveDate="   + string(effectiveDate) .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ServerValidations) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ServerValidations Procedure 
PROCEDURE ServerValidations :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define output parameter errorStatus as logical   no-undo.
define output parameter errormsg    as character no-undo.

{lib/navalidate.i &rateType = rateType , &loanRateType = loanRateType , &errorMsg = errorMsg , &errorStatus = errorStatus , &ownerEndors = ownerEndors , &loanEndors = loanEndors}   

if dtloanPriorEffectiveDate > today then
do:    
  errorMsg =  "Prior Policy Date can not be future date.".
  errorStatus = true.
  return.
end.     

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-CalculateBasicPremium) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION CalculateBasicPremium Procedure 
FUNCTION CalculateBasicPremium RETURNS DECIMAL
  (  pCardName as char ,pAttrID as char , pAmount as int, pCaseType as char, pGroupID as int ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
define variable ppremium as decimal no-undo.
define variable pError as logical no-undo.

{lib/nacheck.i &cardName = pCardName}
/*------------------debugging-------------------*/
 debugparameter = debugparameter + "14.pCardName=" + pCardName         
                                 + "15.pAttrID="   + pAttrID           
                                 + "16.pAmount="   + string(pAmount)   
                                 + "17.pCaseType=" + string(pCaseType) 
                                 + "18.pGroupID="  + string(pGroupID). 
/*---------------------------------------------------------*/

run rcm\ratecard.p persistent set hRateCard (cardSetID,
                                                   pCardName,
                                                   region,
                                                   output fCardID).
if fCardID = 0 then
do:
  run GetLog in hRateCard(output table ttTempRateLog).
  for each ttTempRateLog:
    CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation , ttTempRateLog.cardID,{&Face},pCaseType,ttTempRateLog.calculationOn,pGroupID).
  end.
    premiumOutput = "servererrmsg="           + "Rate card not found with ID " + string(fCardID) + 
                    ",success="               + "N"                                              +
                    ",cardSetID="             + string(cardSetID)                                + 
                    ",manualeffectiveDate="   + string(effectiveDate) .
  delete procedure hRateCard.
  return ?.
end.

ppremium = dynamic-function("GetRate" in hRateCard, pAmount,pAttrID, output pError).

/* The calculated permium amount should never be equal to coverage amount. So if thats 
   the case, then assign zero to premium amount. */
if ppremium = pAmount then ppremium = 0.

if pError or ppremium = ? then
do:
  run GetLog in hRateCard(output table ttTempRateLog).
  for each ttTempRateLog:
    CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation, ttTempRateLog.cardID,{&face},pCaseType,ttTempRateLog.calculationOn,pGroupID).
  end.
    premiumOutput = "servererrmsg="           + "Calculation error"  + 
                    ",success="               + "N"                  +
                    ",cardSetID="             + string(cardSetID)    + 
                    ",manualeffectiveDate="   + string(effectiveDate) .
    delete procedure hRateCard.
  return ?.
end.

run GetLog in hRateCard(output table ttTempRateLog).
for each ttTempRateLog:
  CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation, ttTempRateLog.cardID,{&face},pCaseType,ttTempRateLog.calculationOn,pGroupID).
end.
  delete procedure hRateCard.
CreateLog("Total policy premium for face amount " + trim(string(pAmount,"$>>>,>>>,>>9.99")) +  " is " + trim(string(ppremium,"$>>>,>>>,>>9.99")),trim(string(ppremium,">>>,>>>,>>9.99")),0, {&face},pCaseType,"",pGroupID).
RETURN ppremium.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CalculateEndors) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION CalculateEndors Procedure 
FUNCTION CalculateEndors RETURNS DECIMAL
  ( pAmount as decimal , pGroupName as char,pCaseType as char,pGroupID as int , pSelectedEndorsement as char, pPropertyType as char,output premiumEndorsDetail as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
    
------------------------------------------------------------------------------*/
define variable premium          as decimal   no-undo.
define variable temptotalPremium as decimal   no-undo .
define variable proposedrate     as character no-undo .
define variable iCountEndors     as integer   no-undo.
define variable attribute        as character no-undo.
define variable pError           as logical   no-undo.
define variable validproposedrate as logical  no-undo.
/*------------------debugging-------------------*/
 debugparameter = debugparameter + "19.pAmount=" + string(pAmount)                           
                                 + "20.pGroupName=" + string(pGroupName)                     
                                 + "21.pCaseType=" + pCaseType                               
                                 + "22.pGroupID=" + string(pGroupID)                         
                                 + "23.pSelectedEndorsement=" + string(pSelectedEndorsement) 
                                 + "24.pPropertyType=" + string(pPropertyType).              
 Endors-List-block:
 do iCountEndors = 1 to num-entries(pSelectedEndorsement , "|"): 
   /*---if the endorsement is valid for both policy type (L|O) and independent of property type (R\M) 
   and have different rates based on owner or loan 
   then specifying attribute based on Case type (L\O)------*/
   if lookup(entry(1,entry(iCountEndors,pSelectedEndorsement,"|"),"-"), {&PremiumSpecificRates}) > 0 then
     attribute = "".
   else
       attribute = pPropertyType.
   run  rcm\ratecard.p persistent set hEndCalc (cardSetID,
                                                entry(1,entry(iCountEndors,pSelectedEndorsement,"|"),"-"),   /*cardname*/ 
                                                region,
                                                output fCardID).
    /*------------------debugging-------------------*/
debugparameter = debugparameter + "30.fCardID=" + string(entry(1,entry(iCountEndors,pSelectedEndorsement,"|"),"-")) + string(fCardID).
/*---------------------------------------------------------*/
   if fCardID = 0 then
   do:
     run GetLog in hEndCalc (output table ttTempRateLog).
     for each ttTempRateLog:
       CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation , ttTempRateLog.cardID,{&face},pGroupName,ttTempRateLog.calculationOn,pGroupID).
     end.
       premiumOutput = "servererrmsg="           + "Rate card not found with ID " + string(fCardID) + 
                       ",success="               + "N"                                              +
                       ",cardSetID="             + string(cardSetID)                                + 
                       ",manualeffectiveDate="   + string(effectiveDate) .
     delete procedure hEndCalc.
     next Endors-List-block.
   end.
   
   premium = dynamic-function("GetRate" in hEndCalc, pAmount, attribute, output pError).
   
   /* The calculated permium amount should never be equal to coverage amount. So if thats 
   the case, then assign zero to premium amount. */
   if premium = pAmount then premium = 0.
   
   if pError then
   do:
     run GetLog in hEndCalc(output table ttTempRateLog).
     for each ttTempRateLog:
       CreateLog(ttTempRateLog.action ,ttTempRateLog.actionCalculation, ttTempRateLog.cardID,{&face},pGroupName,ttTempRateLog.calculationOn,pGroupID).
     end.
     premiumOutput = "servererrmsg="           + "Calculation error" + 
                     ",success="               + "N"                 +
                     ",cardSetID="             + string(cardSetID)   + 
                     ",manualeffectiveDate="   + string(effectiveDate) .
     delete procedure hEndCalc.
     return ?.
   end.

   validproposedrate = false.
   if entry(2,entry(iCountEndors,pSelectedEndorsement,"|"),"-") ne "proposedRate" and entry(2,entry(iCountEndors,pSelectedEndorsement,"|"),"-") ne {&null} then
   do:
     proposedrate =  entry(2,entry(iCountEndors,pSelectedEndorsement,"|"),"-").
     /*------------------debugging-------------------*/
     debugparameter = debugparameter + "31.proposedrate=" + proposedrate.
    /*---------------------------------------------------------*/
     validproposedrate = dynamic-function("validateRate" in hEndCalc, proposedrate,attribute
                                          , output pError).
   end.

   premiumEndorsDetail = premiumEndorsDetail + "|" + entry(1,entry(iCountEndors,pSelectedEndorsement,"|"),"-") + "-" + if validproposedrate then proposedrate else string(premium).
   temptotalPremium = temptotalPremium + if validproposedrate then decimal(proposedrate) else premium.

   run GetLog in hEndCalc(output table ttTempRateLog).
   for each ttTempRateLog:
     CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation, ttTempRateLog.cardID, {&endorsements},pGroupName,ttTempRateLog.calculationOn,pGroupID).
   end.

   if validproposedrate then
     CreateLog("Modified endorsements premium amount after validate rate is " + trim(string(decimal(proposedrate),"$>>>,>>>,>>9.99")),trim(string(decimal(proposedrate),">>>,>>>,>>9.99")), 0, {&endorsements},pGroupName,"",pGroupID).
   delete procedure hEndCalc.

 end.  

 premiumEndorsDetail = trim(premiumEndorsDetail,"|").

 return temptotalPremium.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CalculateExcessPremium) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION CalculateExcessPremium Procedure 
FUNCTION CalculateExcessPremium RETURNS DECIMAL
  (  pCardName as char ,pAttrID as char , pExcessAmount as int, pAmount as int, pCaseType as char, pGroupID as int ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

define variable ppremium            as decimal no-undo.
define variable origAmountPremium   as decimal no-undo.
define variable excessAmountPremium as decimal no-undo.
define variable pError              as logical no-undo.

/*------------------debugging-------------------*/
 debugparameter = debugparameter + "25.pCardName=" + string(pCardName)         
                                 + "26.pAttrID=" + string(pAttrID)             
                                 + "27.pExcessAmount=" + string(pExcessAmount) 
                                 + "28.pAmount=" + string(pAmount)             
                                 + "29.pCaseType=" + string(pCaseType)         
                                 + "30.pGroupID=" + string(pGroupID).          
/*---------------------------------------------------------*/
run  rcm\ratecard.p persistent set hExcessAmountCalc (cardSetID,
                                                      pCardName,
                                                      region,
                                                      output fCardID).
                                                        
if fCardID = 0 then
do:
  run GetLog in hExcessAmountCalc(output table ttTempRateLog).
  for each ttTempRateLog:
    CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation , ttTempRateLog.cardID,{&face},pCaseType,ttTempRateLog.calculationOn,pGroupID).
  end.
    premiumOutput = "servererrmsg="           + "Rate card not found with ID " + string(fCardID) + 
                    ",success="               + "N"                                              +
                    ",cardSetID="             + string(cardSetID)                                + 
                    ",manualeffectiveDate="   + string(effectiveDate) .
  delete procedure hExcessAmountCalc.
  return ?.
end.

origAmountPremium = dynamic-function("GetRate" in hExcessAmountCalc, pAmount, pAttrID, output pError).

/* The calculated permium amount should never be equal to coverage amount. So if thats 
   the case, then assign zero to premium amount. */
if origAmountPremium = pAmount then origAmountPremium = 0.

run GetLog in hExcessAmountCalc(output table ttTempRateLog).
for each ttTempRateLog:
  CreateLog(ttTempRateLog.action ,ttTempRateLog.actionCalculation, ttTempRateLog.cardID,{&excess},pCaseType,ttTempRateLog.calculationOn,pGroupID).
end.
if pError or origAmountPremium = ? then
do:
  premiumOutput = "servererrmsg="             + "Calculation error" + 
                    ",success="               + "N"               +
                    ",cardSetID="             + string(cardSetID) +
                    ",manualeffectiveDate="   + string(effectiveDate).
    delete procedure hExcessAmountCalc.
  return ?.
end.

CreateLog("Calculated premium for amount " + trim(string(pAmount, "$>>>,>>>,>>9.99")) + " is " + trim(string(origAmountPremium, "$>>>,>>>,>>9.99")),trim(string(origAmountPremium, ">>>,>>>,>>9.99")), 0, {&excess},pCaseType,"",pGroupID).

excessAmountPremium = dynamic-function("GetRate" in hExcessAmountCalc, pExcessAmount, pAttrID, output pError).


/* The calculated permium amount should never be equal to coverage amount. So if thats 
   the case, then assign zero to premium amount. */
if excessAmountPremium = pExcessAmount then excessAmountPremium = 0.

run GetLog in hExcessAmountCalc(output table ttTempRateLog).
for each ttTempRateLog:
  CreateLog(ttTempRateLog.action ,ttTempRateLog.actionCalculation, ttTempRateLog.cardID,{&excess},pCaseType,ttTempRateLog.calculationOn,pGroupID).
end.
if pError or excessAmountPremium = ?  then
do:
  premiumOutput = "servererrmsg="           + "Calculation error" + 
                  ",success="               + "N"                 +
                  ",cardSetID="             + string(cardSetID)   + 
                  ",manualeffectiveDate="   + string(effectiveDate) .
    delete procedure hExcessAmountCalc.
  return ?.
end.
delete procedure hExcessAmountCalc.

CreateLog("Calculated premium for amount " + trim(string(pExcessAmount, "$>>>,>>>,>>9.99")) + " = " + trim(string(excessAmountPremium, "$>>>,>>>,>>9.99")),trim(string(excessAmountPremium, ">>>,>>>,>>9.99")), 0, {&excess},pCaseType,"",pGroupID).
ppremium = excessAmountPremium - origAmountPremium.
if ppremium < 0 then
do:
  CreateLog("Premium for excess amount " + trim(string(excessAmountPremium, "$>>>,>>>,>>9.99")) + " - " + trim(string(origAmountPremium, "$>>>,>>>,>>9.99")) + " is negative. ","" ,fCardID, {&excess},pCaseType,"",pGroupID).
  return ?. 
end.

CreateLog("Premium for excess amount " + trim(string(excessAmountPremium, "$>>>,>>>,>>9.99")) + " - " + trim(string(origAmountPremium, "$>>>,>>>,>>9.99")) + " = " + trim(string( excessAmountPremium - origAmountPremium, "$>>>,>>>,>>9.99"))
          ,trim(if (excessAmountPremium - origAmountPremium) >= 0  then trim(string((excessAmountPremium - origAmountPremium), ">>>,>>>,>>9.99")) else trim(string((excessAmountPremium - origAmountPremium), ">>>,>>>,>>9.99")))
          ,fCardID
          ,{&excess}
          ,pCaseType
          ,""
          ,pGroupID).

return ppremium.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CreateLog) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION CreateLog Procedure 
FUNCTION CreateLog RETURNS LOGICAL
  ( pNote as char , pActionCalculation as character,  pCardID as int, pTypeAmount as char, pTypeCase as char, pCalculationOn as char, pGroupID as int) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 define buffer ratelog for ratelog.

 iLogSeq = iLogSeq + 1.
 create ratelog.
 assign
   ratelog.seq = iLogSeq
   ratelog.action = pNote
   rateLog.actionCalculation = pActionCalculation
   ratelog.cardID = pCardID
   ratelog.typeofamount = pTypeAmount
   ratelog.calculationFor = pTypeCase
   ratelog.calculationOn = pCalculationOn
   ratelog.groupID =   pGroupID.

 return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-RemoveItemFromList) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION RemoveItemFromList Procedure 
FUNCTION RemoveItemFromList returns character
    (pclist as character,
     pcremoveitem as character,
     pcdelimiter as character):
 
define variable lipos as integer no-undo.

lipos = lookup(pcremoveitem,pclist,pcdelimiter).

if lipos > 0 then
do:
  assign entry(lipos,pclist,pcdelimiter) = "".
  if lipos = 1 then
    pclist = substring(pclist,2).
  else if lipos = num-entries(pclist,pcdelimiter) then
    pclist = substring(pclist,1,length(pclist) - 1).
  else
    pclist = replace(pclist,pcdelimiter + pcdelimiter,pcdelimiter).
end.

return pclist.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

