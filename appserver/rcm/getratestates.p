/*------------------------------------------------------------------------
@name getratestates.p
@action ratestatesGet
@description Returns all rate states
@param StateID;char;

@returns The ratestate table

@throws 3001;char;The State is invalid

@success 2005;char;The count of the records returned

@author Archana Gupta
@version 1.0
@created 03/06/2018
@modified
01-25-2022 Shefali Task:89648 Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"
----------------------------------------------------------------------*/

define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable pcStateID as char no-undo.

/* stored procedure variables */
define variable hTable as handle no-undo.

{lib/std-def.i}
{lib/callsp-defs.i }
{tt/ratestate.i &tableAlias="ttratestate"}

pRequest:getParameter("StateID", output pcStateID).
if pcStateID = ""
 then pcStateID = "ALL".

if pResponse:isFault()
 then return.

{lib/callsp.i  &name=spGetRateStates &load-into=ttRateState &params="input pcStateID" &noCount=true &noresponse=true}

std-in = 0.
for each ttRateState exclusive-lock:
  std-in = std-in + 1.
end.


if std-in > 0
 then pResponse:setParameter("rateState", table ttRateState).

pResponse:success("2005", string(std-in) {&msg-add} "States").
