&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*-----------------------------------------------------------------------------
    File        : rcm/newmexico.p
    Purpose     : State specific rate calculator

    Syntax      :

    Description :

    Author(s)   : S Chandu
    Created     : 08/29/2023
    Notes       :
    Modification :
    Date          Name        Comments
    04/18/24      S Chandu    Task 112308 - Modified attribute to 'LE2TO3YEARS' to avoid 
                              duplicate attributes.
    05/13/24      Vignesh R   Task 112559 - newmexicov2.p created on top of newmexico.p
                              for version 2 changes.
  ---------------------------------------------------------------------------*/

/*******************************  Definitions  *******************************/
{lib\std-def.i}
{lib\rcm-std-def.i}
{lib\nadef.i}
{tt\ratelog.i}
{tt\ratelog.i &tablealias=ttTempRateLog}

define input parameter            parameters     as character no-undo.

define output parameter           premiumOutput  as character no-undo.
define output parameter           debugparameter as character no-undo.
define output parameter table for rateLog.

/* Debugging */
debugparameter =  "1.parameters in definition=" + parameters.

/* RateTypes for UI Codes */
&scoped-define UiOwnersOriginal                     "O"    /* Owner's Original */
&scoped-define UiOwnersReissue                     "Re"   /* Owner's Reissue */

&scoped-define UiLoan                              "Lo"   /* Loan Policy */
&scoped-define UiSubstitutionRate                  "SR"   /* SubstitutionRate Loan */
&scoped-define UiLoanTwoYearLimitationConstruction "LC"   /* Loan Policy Two-Year Limitation for Construction */

&scoped-define uiSecondMortgage                    "SM"   /* Second Mortgage */


/* Attributes */
&scoped-define ExcessSecondLoan        "Ex-SL"
&scoped-define Loan-Residential        "L-R"

&scoped-define LE1YEARS                "0To1"
&scoped-define LE2YEARS                "1To2"
&scoped-define LE2TO3YEARS             "2To3"                                     
&scoped-define GT3YEARS                "GT3"

&scoped-define LE3YEARS                "0To3"
&scoped-define LE5YEARS                "3To5"
&scoped-define LE10YEARS               "5To10"                                     
&scoped-define GT10YEARS               "GT10"

&scoped-define ResiOwnerExtended       "R-Ex"
&scoped-define ComOwnerExtended        "M-EX" 
&scoped-define ResiLoanExtended        "L-R-Ex"
&scoped-define ComLoanExtended         "M-R-Ex"
&scoped-define Others                  "OT"

&scoped-define ExtendedCoverageSpecificEndors  ""
&scoped-define ExtendedCoverageRateType        ""
&scoped-define NASpecificEndors  "14,15,17,24,24.1,61,DO"


/* Scenarios */
&scoped-define sOwner                          "O"
&scoped-define sOwnersReissue                  "OR"
&scoped-define sOwnerLoan                      "OL"
&scoped-define sOwnerReissueLoan               "ORL"
&scoped-define sOwnerLoanScndLoan              "OLSL"
&scoped-define sOwnerReissueLoanScndLoan       "ORLSL"

&scoped-define sLoan                        "L"
&scoped-define sLoanReissue                 "LR"
&scoped-define sLoanScndLoan                "LSL"


/* Card Names used 'c' as prefix */
&scoped-define cOwnersOriginal              "OwnersOriginal"
&scoped-define cOwnersReissue               "OwnersReissue"
&scoped-define cownersReissueExcess         "OwnersReissueExcess"

&scoped-define cLoanOriginal                "LoanOriginal"
&scoped-define cSubstitutionRate            "SubstitutionRate"
&scoped-define cSubstitutionRateExcess      "SubstitutionRateExcess"
&scoped-define cTwoYearLimitedConstruction  "LoanTwoYearLimitationForConstruction"

&scoped-define cSimoLoan                   "SimoLoan"
&scoped-define cSimoLoanExcess             "SimoLoanExcess"
&scoped-define cSimoSecondLoan             "SimoSecondLoan"
&scoped-define cSimoSecondLoanExcess       "SimoSecondLoanExcess"
&scoped-define cCommitment                 "COMMITMENT"


/* Parameters - parse the input parameter and store the information into these variables */
define variable rateType                  as character no-undo initial "rateType".
define variable coverageAmount            as character no-undo initial "coverageAmount".                    
define variable reissueCoverageAmount     as character no-undo initial "reissueCoverageAmount".
define variable loanRateType              as character no-undo initial "loanRateType".
define variable loanCoverageAmount        as character no-undo initial "loanCoverageAmount".
define variable loanReissueCoverageAmount as character no-undo initial "loanReissueCoverageAmount". 
define variable secondLoanRateType        as character no-undo initial "secondLoanRateType".
define variable secondLoanCoverageAmount  as character no-undo initial "secondLoanCoverageAmount".  
define variable ownerEndors               as character no-undo initial "ownerEndors".
define variable loanEndors                as character no-undo initial "loanEndors".
define variable secondLoanEndors          as character no-undo initial "secondLoanEndors".
define variable propertyType              as character no-undo initial "propertyType".
define variable Version                   as character no-undo initial "Version".
define variable simultaneous              as character no-undo initial "simultaneous".                                                                                
define variable prioreffectiveDate        as character no-undo initial "prioreffectiveDate".    
define variable loanPriorEffectiveDate    as character no-undo initial "loanPriorEffectiveDate".      
define variable lenderCPL                 as character no-undo initial "lenderCPL". 
define variable buyerCPL                  as character no-undo initial "buyerCPL". 
define variable sellerCPL                 as character no-undo initial "sellerCPL". 

/* Derived variables based on input parameters */
define variable iCovgAmt                  as integer   no-undo.
define variable iReissueCovgAmt           as integer   no-undo. 
define variable iLoanCovgAmt              as integer   no-undo.
define variable iLoanReissueCovgAmt       as integer   no-undo.   /* In this logic iLoanReissueCovgAmt variable is used to take the number of parcels value */
define variable iSecondLoanCovgAmt        as integer   no-undo.
define variable iVersion                  as integer   no-undo.
define variable lSimultaneous             as logical   no-undo.
define variable dtPriorPolicyDate         as date      no-undo.
define variable dtPriorLoanPolicyDate     as date      no-undo.
define variable iAgeOfOriginalPolicy      as integer   no-undo.
define variable iAgeofLoanPolicy          as integer   no-undo. 
define variable iLenderCPL                as integer   no-undo.
define variable iBuyerCPL                 as integer   no-undo.
define variable iSellerCPL                as integer   no-undo.

/* Basic variables */
define variable cScenarioCode             as character no-undo.
define variable cScenarioName             as character no-undo.
define variable cScenarioDesc             as character no-undo.
define variable cCardAttrib               as character no-undo.
define variable cEffectiveDate            as character no-undo.
define variable region                    as character no-undo.
define variable iLogSeq                   as integer   no-undo.
define variable iCardSetID                as integer   no-undo.
define variable cScenarioMsg              as character no-undo.

/* Variables to create output parameter */
define variable cPremiumOEndorsDetail     as character no-undo. 
define variable cPremiumLEndorsDetail     as character no-undo.
define variable cPremiumSEndorsDetail     as character no-undo.
define variable cErrorMsg                 as character no-undo.
define variable dePremiumOwner            as decimal   no-undo.   
define variable dePremiumLoan             as decimal   no-undo.  
define variable dePremiumScnd             as decimal   no-undo.
define variable dePremiumOEndors          as decimal   no-undo.          
define variable dePremiumLEndors          as decimal   no-undo.
define variable dePremiumSEndors          as decimal   no-undo.
define variable lErrorStatus              as logical   no-undo.
define variable dePremiumCPL              as decimal   no-undo.
define variable deLPremiumCPL             as decimal   no-undo.
define variable deBPremiumCPL             as decimal   no-undo.
define variable deSPremiumCPL             as decimal   no-undo.

define variable deGrandTotal              as decimal   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-calculateBasicPremium) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD calculateBasicPremium Procedure 
FUNCTION calculateBasicPremium RETURNS DECIMAL
  (  pCardName as character ,pAttrID as character , pAmount as decimal, pCaseType as character, pGroupID as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-calculateEndors) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD calculateEndors Procedure 
FUNCTION calculateEndors RETURNS decimal
  ( pAmount as integer , pGroupName as character,pCaseType as character,pGroupID as integer , pSelectedEndorsement as character, pPropertyType as character, pRateType as character, output premiumEndorsDetail as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-calculateExcessPremium) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD calculateExcessPremium Procedure 
FUNCTION calculateExcessPremium RETURNS DECIMAL
  (pCardName as character ,pAttrID as character , pExcessAmount as integer, pAmount as integer, pCaseType as character, pGroupID as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createLog) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createLog Procedure 
FUNCTION createLog RETURNS LOGICAL
  ( pNote as character , pActionCalculation as character,  pCardID as integer, pTypeAmount as character, pTypeCase as character, pCalculationOn as character, pGroupID as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 18.43
         WIDTH              = 55.6.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* Debugging */
debugparameter = debugparameter + "2.enter in main block".

/* Extract user input values */
run extractParameters.

/* Check the UI Provided version number - If it's zero then find an
active version of this state otherwise specified version */
if iVersion = 0 
 then 
  for first rateState  where rateState.stateID = {&NewMexico}
                         and rateState.active  = true:
    assign
        iCardSetID     = rateState.cardSetID.
        cEffectiveDate = if rateState.effectiveDate = ? then "" 
                           else string(rateState.effectiveDate)
        .
  end.
 else
  for first rateState  where rateState.stateID = {&NewMexico}
                         and rateState.version = iVersion:
    assign
        iCardSetID     = rateState.cardSetID.
        cEffectiveDate = if rateState.effectiveDate = ? then "" 
                          else string(rateState.effectiveDate)
        .
  end.


/* Debugging */
debugparameter = debugparameter + "31.cardSetID="     + string(iCardSetID) 
                                + "32.effectiveDate=" + cEffectiveDate.

  
if iCardSetID = 0 
 then 
  do:
    CreateLog("No Such RateState record found." ,"", 0, "", "",{&GroupError},0).
    premiumOutput = "servererrmsg="         + "No Such RateState record found for version " + string(iVersion) + " ." +
                    ",success="             + "N"                                                                     +
                    ",cardSetID="           + string(iCardSetID)                                                      +
                    ",manualeffectiveDate=" + cEffectiveDate.
    return.
  end.


/* Server Validations */
run ServerValidations(output lErrorStatus,
                      output cErrorMsg).

/* Debugging */
debugparameter = debugparameter + "34.lErrorStatus=" + string(lErrorStatus) 
                                + "35.cErrorMsg="    + cErrorMsg.

if lErrorStatus
 then
  do:
    premiumOutput = "servererrmsg="         + cErrorMsg          +                  
                    ",success="             + "N"                +
                    ",cardSetID="           + string(iCardSetID) +
                    ",manualeffectiveDate=" + cEffectiveDate.
    return.
  end.

/* Debugging */
debugparameter = debugparameter + "33.after server validations" .  


/* Descide the scenario */
run decideScenario.

/* Debugging */
debugparameter = debugparameter + "34.lErrorStatus=" + string(lErrorStatus) 
                                + "35.cErrorMsg="    + cErrorMsg.
if lErrorStatus
 then
  do:
    premiumOutput = "servererrmsg="         + cErrorMsg          +                  
                    ",success="             + "N"                +
                    ",cardSetID="           + string(iCardSetID) +
                    ",manualeffectiveDate=" + cEffectiveDate.
    return.
  end.

/* Debugging */
debugparameter = debugparameter + "32.after amount validations".

/* Excecute the Scenario */
case cScenarioCode:
  
  /* residential */
 when {&Residential}{&sOwner} /* R"O */ 
  then
   run ResiOwner(input  rateType,
                 input  iCovgAmt,
                 output dePremiumOwner).

 when {&Residential}{&sLoan} /* R"L */ 
  then
   run ResiLoan(input  loanRateType,
                input  iLoanCovgAmt,
                output dePremiumLoan).

 when {&Residential}{&sOwnersReissue} /* R"OR */ 
  then
   run ResiOwnerReissue(input  rateType,
                        input  iCovgAmt,
                        input  iReissueCovgAmt,
                        output dePremiumOwner).

 when {&Residential}{&sLoanReissue} /* R"LR*/ 
  then
   run ResiLoanReissue(input  loanRateType,
                       input  iLoanCovgAmt,
                       input  loanReissueCoverageAmount,
                       output dePremiumLoan).

 when {&Residential}{&sOwnerLoan} /* R"OL */
  then
   run ResiOwnerLoan(input  rateType,
                     input  loanRateType,
                     input  iCovgAmt,
                     input  iLoanCovgAmt,
                     output dePremiumOwner,
                     output dePremiumLoan).

 when {&Residential}{&sOwnerReissueLoan} /* R"ORL */ 
  then
   run ResiOwnerReissueLoan(input  rateType,
                            input  loanRateType,
                            input  iCovgAmt,
                            input  iReissueCovgamt,
                            input  iLoanCovgAmt,
                            output dePremiumOwner,
                            output dePremiumLoan).


 when {&Residential}{&sOwnerLoanScndLoan} /* R"OLSL */
  then
   run ResiOwnerLoanScndLoan(input  rateType,
                             input  loanRateType,
                             input  secondLoanRateType,
                             input  iCovgAmt,
                             input  iLoanCovgAmt,
                             input  iSecondLoanCovgAmt,                                                  
                             output dePremiumOwner,                                                                                                            
                             output dePremiumLoan,                                                                                                             
                             output dePremiumScnd). 


 when {&Residential}{&sOwnerReissueLoanScndLoan} /* R"ORLSL */ 
  then                                                                                                          
   run ResiOwnerReissueLoanScndLoan(input  rateType,
                                    input  loanRateType,
                                    input  secondLoanRateType,
                                    input  iCovgAmt,
                                    input  iReissueCovgamt,
                                    input  iLoanCovgAmt,
                                    input  iSecondLoanCovgAmt,
                                    output dePremiumOwner,                                                                                                             
                                    output dePremiumLoan,  
                                    output dePremiumScnd).                                                                                                          

 
 when {&Residential}{&sLoanScndLoan} /* R"LSL*/   
  then 
   run ResiLoanScndLoan(input  loanRateType,
                        input  secondLoanRateType,
                        input  iLoanCovgAmt,
                        input  iSecondLoanCovgAmt,                                                                                                            
                        output dePremiumLoan,  
                        output dePremiumScnd). 

end case.

if cScenarioName <> ""
 then
  publish "doScenario" (input  iCardSetID,
                        input  cScenarioName,
                        input  cScenarioDesc,
                        input  region,       
                        input  propertyType,
                        input  cCardAttrib,
                        output cScenarioMsg).

/* Debugging */
debugparameter = debugparameter + "33.cScenarioName=" + cScenarioName
                                + "34.cScenarioDesc=" + cScenarioDesc
                                + "35.iCardSetID="    + string(iCardSetID)
                                + "36.cCardAttrib="   + cCardAttrib
                                + "37.cScenarioMsg="  + cScenarioMsg.



/* Endorsement Calculations */
if ownerEndors ne "" 
 then
  dePremiumOEndors = CalculateEndors(input  iCovgAmt,
                                     input  {&OwnersEndors},
                                     input  {&Owners},
                                     input  1,
                                     input  ownerEndors,
                                     input  propertyType,
                                     input  rateType, 
                                     output cPremiumOEndorsDetail).
  
if loanEndors ne "" 
 then
  dePremiumLEndors = CalculateEndors(input  iLoanCovgAmt,
                                     input  {&LendersEndors},
                                     input  {&Lenders},
                                     input  2,
                                     input  loanEndors,
                                     input  propertyType, 
                                     input  loanRateType, 
                                     output cPremiumLEndorsDetail).
  
if secondLoanEndors ne "" 
 then
  dePremiumSEndors = CalculateEndors(input  iSecondLoanCovgAmt,
                                     input  {&SecondLendersEndors},
                                     input  {&SecondLenders},
                                     input  3,
                                     input  secondLoanEndors,
                                     input  propertyType, 
                                     input  secondLoanRateType, 
                                     output cPremiumSEndorsDetail).
 

/* COMMITMENT Calculations */
run calcCommitment.


if dePremiumOwner   = ? or dePremiumLoan    = ? or dePremiumScnd = ? or 
   dePremiumOEndors = ? or dePremiumLEndors = ? or dePremiumSEndors = ? or 
   dePremiumCPL = ?     
 then
  assign
      dePremiumOwner   = 0 
      dePremiumLoan    = 0
      dePremiumScnd    = 0
      dePremiumOEndors = 0
      dePremiumLEndors = 0 
      dePremiumSEndors = 0
      dePremiumCPL     = 0
      .

CreateLog("Total premium amount " + trim(trim((if dePremiumOwner   ne 0 then         trim(string(dePremiumOwner,"$>>>,>>>,>>9.99"))     else "")
                                            + (if dePremiumOEndors ne 0 then " + " + trim(string(dePremiumOEndors,"$>>>,>>>,>>9.99"))   else "")
                                            + (if dePremiumCPL     ne 0 then " + " + trim(string(dePremiumCPL,"$>>>,>>>,>>9.99"))       else "")
                                            + (if dePremiumLoan    ne 0 then " + " + trim(string(dePremiumLoan,"$>>>,>>>,>>9.99"))      else "")
                                            + (if dePremiumLEndors ne 0 then " + " + trim(string(dePremiumLEndors,"$>>>,>>>,>>9.99"))   else "")
                                            + (if dePremiumScnd    ne 0 then " + " + trim(string(dePremiumScnd,"$>>>,>>>,>>9.99"))      else "")
                                            + (if dePremiumSEndors ne 0 then " + " + trim(string(dePremiumSEndors,"$>>>,>>>,>>9.99"))   else "")," "),"+")
                                            + " = " + trim(string(dePremiumOwner + dePremiumLoan + dePremiumOEndors + dePremiumLEndors + dePremiumScnd  + dePremiumSEndors + dePremiumCPL, "$>>>,>>>,>>9.99")),
                                              string(dePremiumOwner + dePremiumLoan  + dePremiumOEndors + dePremiumLEndors + dePremiumScnd  + dePremiumSEndors + dePremiumCPL , ">>>,>>>,>>9.99"), 0,"",{&GrandTotal},{&GrandTotal} ,0).

deGrandTotal =  dePremiumOwner + dePremiumOEndors + dePremiumLoan + dePremiumLEndors + dePremiumScnd +  dePremiumSEndors + dePremiumCPL.                                        
                                              
CreateLog("Round rule with value 1 adjusted from $" +  trim(string(deGrandTotal, ">>>,>>>,>>9.99")) + " to $ " + trim(string(round(deGrandTotal, 0), ">>>,>>>,>>9.99")), trim(string(round(deGrandTotal, 0), ">>>,>>>,>>9.99")), 0, "", {&GrandTotal},{&GrandTotal},0).

CreateLog("Total Premium amount is $" + trim(string(round(deGrandTotal, 0), ">>>,>>>,>>9.99")), trim(string(round(deGrandTotal, 0), ">>>,>>>,>>9.99")), 0, "", {&GrandTotal},{&GrandTotal},0).                                              


  
/* Set Output Parameters */
premiumOutput = "premiumOwner="          + string(dePremiumOwner)        + 
                ",premiumLoan="          + string(dePremiumLoan)         + 
                ",premiumLenderCPL="     + string(dePremiumCPL)          + 
                ",premiumOEndors="       + string(dePremiumOEndors)      + 
                ",premiumLEndors="       + string(dePremiumLEndors)      + 
                ",premiumOEndorsDetail=" + string(cPremiumOEndorsDetail) + 
                ",premiumLEndorsDetail=" + string(cPremiumLEndorsDetail) + 
                ",premiumScnd="          + string(dePremiumScnd)         + 
                ",premiumSEndors="       + string(dePremiumSEndors)      +
                ",premiumSEndorsDetail=" + string(cPremiumSEndorsDetail) +
                ",success="              + "y"                           +
                ",cardSetID="            + string(iCardSetID)            +
                ",manualeffectiveDate="  + string(cEffectiveDate).

/* Debugging */
debugparameter = debugparameter + "38.premiumOutput=" + premiumOutput.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-calcCommitment) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calcCommitment Procedure 
PROCEDURE calcCommitment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:     using same variables of  CPL's for commitment calculations 
------------------------------------------------------------------------------*/
  assign
      dePremiumCPL = 0
      deLPremiumCPL = 0
      deBPremiumCPL = 0
      deSPremiumCPL = 0
      .

  if iLenderCPL ne 0 
   then
    do: 
      deLPremiumCPL = CalculateBasicPremium({&cCommitment},"",0,{&cCommitment},4). 
      CreateLog("Calculated premium for " + string(iLenderCPL) + " Commitment is " + string(iLenderCPL) + " X " + trim(string(deLPremiumCPL, "$>>>,>>>,>>9.99")) + " = " + trim(string((iLenderCPL * deLPremiumCPL), "$>>>,>>>,>>9.99")),trim(string((iLenderCPL * deLPremiumCPL), ">>>,>>>,>>9.99")), 0,"",{&cCommitment},"",4).
      deLPremiumCPL = iLenderCPL * deLPremiumCPL. 
    end.

  if iBuyerCPL ne 0 
   then
    do: 
      deBPremiumCPL = CalculateBasicPremium({&cCommitment},"",0,{&cCommitment},4). 
      CreateLog("Calculated premium for " + string(iBuyerCPL) + " Commitment is " + string(iBuyerCPL) + " X " + trim(string(deBPremiumCPL, "$>>>,>>>,>>9.99")) + " = " + trim(string((iBuyerCPL * deBPremiumCPL), "$>>>,>>>,>>9.99")),trim(string((iBuyerCPL * deBPremiumCPL), ">>>,>>>,>>9.99")), 0,"",{&cCommitment},"",4).
      deBPremiumCPL = iBuyerCPL * deBPremiumCPL. 
    end.

  if iSellerCPL ne 0 
   then 
    do: 
      deSPremiumCPL = CalculateBasicPremium({&cCommitment},"",0,{&cCommitment},4). 
      CreateLog("Calculated premium for " + string(iSellerCPL) + " Commitment is " + string(iSellerCPL) + " X " + trim(string(deSPremiumCPL, "$>>>,>>>,>>9.99")) + " = " + trim(string((iSellerCPL * deSPremiumCPL), "$>>>,>>>,>>9.99")),trim(string((iSellerCPL * deSPremiumCPL), ">>>,>>>,>>9.99")), 0,"",{&cCommitment},"",4).
      deSPremiumCPL = iSellerCPL * deSPremiumCPL. 
    end.

  dePremiumCPL = deLPremiumCPL + deBPremiumCPL + deSPremiumCPL.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-decideScenario) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE decideScenario Procedure 
PROCEDURE decideScenario :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  case propertyType:
   when {&Residential} 
    then
     do:
       if not lSimultaneous 
        then
         do:
           if rateType = {&NAUI} or
              (coverageAmount    ne "" and
              rateType           ne "" and
              loanCoverageAmount eq "")       
            then
             do:
               if rateType = {&UiOwnersReissue}
                then
                 cScenarioCode = {&Residential}{&sOwnersReissue}. /* R"OR */
                else
                 cScenarioCode = {&Residential}{&sOwner}. /* R"O */
             end. 
            else if loanRateType = {&NAUI} or
                    (loanCoverageAmount    ne "" and
                    loanRateType           ne "" and
                    coverageAmount         eq "")  
             then
              do:
                if loanRateType = {&UiSubstitutionRate}
                 then
                  cScenarioCode = {&Residential}{&sLoanReissue}. /* R"LR */
                 else
                  cScenarioCode = {&Residential}{&sLoan}. /* R"L */
               end.
              
         end.  
        else 
         do:
           if coverageAmount eq ""
            then
             do:
               if secondLoanRateType = {&uiSecondMortgage}
                 then
                  cScenarioCode = {&Residential}{&sLoanScndLoan}. /* R"LSL */
             end.
            else if secondLoanCoverageAmount eq "" 
             then  
              do:
                if rateType = {&UiOwnersReissue} 
                 then
                  cScenarioCode = {&Residential}{&sOwnerReissueLoan}. /* R"ORL */                                     
                 else                     
                  cScenarioCode = {&Residential}{&sOwnerLoan}. /* R"OL */                 
              end.
             else 
              do:
                if rateType = {&UiOwnersReissue} 
                 then
                  cScenarioCode =  if secondLoanRateType = {&uiSecondMortgage} 
                                    then {&Residential}{&sOwnerReissueLoanScndLoan} else "". /* R"ORLSL */                   
                 else
                  cScenarioCode =  if secondLoanRateType = {&uiSecondMortgage} 
                                    then {&Residential}{&sOwnerLoanScndLoan} else "". /* R"OLSL */  
              end.
         end.  
     end.

   otherwise                                                                                                                                   
    do:                                                                                                                                        
      premiumOutput = "servererrmsg="           + "Scenario not defined for given input" +                                                         
                      ",success="               + "N"                                    +
                      ",cardSetID="             + string(iCardSetID)                     +
                      ",manualeffectiveDate="   + cEffectiveDate.                                                                
      return.
    end.
  end case.
  
  /*------------------debugging-------------------*/
  debugparameter = debugparameter + "27.scenario=" + cScenarioCode.    
 /*---------------------------------------------------*/ 
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-extractParameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE extractParameters Procedure 
PROCEDURE extractParameters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cErrorMsgs     as character no-undo.
  define variable iCntParameters as integer   no-undo.
  define variable iCntError      as integer   no-undo.
  define variable iNumDays       as integer   no-undo.
  define variable iNoOfLoanDays  as integer   no-undo.
  /*-----------------------------input variables--------------------------------------*/
  do iCntParameters = 1 to num-entries(parameters,","):
  
    case entry(1,entry(iCntParameters,parameters,","),"^"):
    
      when rateType  
       then
        rateType                   = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
      when coverageAmount  
       then
        coverageAmount             = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
      when loanRateType
       then
        loanRateType               = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
      when loanCoverageAmount  
       then
        loanCoverageAmount         = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
        
      when reissueCoverageAmount 
       then
        reissueCoverageAmount      = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
      when loanReissueCoverageAmount 
       then
        loanReissueCoverageAmount  = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
        
      when secondLoanRateType  
       then
        secondLoanRateType         = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
        
      when secondLoanCoverageAmount  
       then
        secondLoanCoverageAmount   = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
      when ownerEndors  
       then
        ownerEndors                = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
      when loanEndors 
       then
        loanEndors                 = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
      when secondLoanEndors 
       then
        secondLoanEndors           = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
      when priorEffectiveDate 
       then
        priorEffectiveDate         = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
 
      when propertyType  
       then
        propertyType               = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
      when Version  
       then
        Version                    = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
      when simultaneous  
       then
        simultaneous               = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
          
      when loanPriorEffectiveDate 
       then 
        loanPriorEffectiveDate     = entry(2,entry(iCntParameters,parameters,","),"^") no-error.    
 
      when lenderCPL  
       then                            
        lenderCPL                  = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
 
      when buyerCPL 
       then                            
        buyerCPL                   = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
 
      when sellerCPL  
       then                            
        sellerCPL                  = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
 
    end case.  
  end.
  
  /* Debugging */
  debugparameter = debugparameter + "3.rateType="                  + rateType
                                  + "4.coverageAmount="            + coverageAmount            
                                  + "5.reissueCoverageAmount="     + reissueCoverageAmount     
                                  + "6.loanRateType="              + loanRateType              
                                  + "7.loanCoverageAmount="        + loanCoverageAmount        
                                  + "8.loanReissueCoverageAmount=" + loanReissueCoverageAmount 
                                  + "9.secondLoanRateType="        + secondLoanRateType        
                                  + "10.secondLoanCoverageAmount=" + secondLoanCoverageAmount  
                                  + "11.ownerEndors="              + ownerEndors               
                                  + "12.loanEndors="               + loanEndors               
                                  + "13.secondLoanEndors="         + secondLoanEndors                
                                  + "14.prioreffectiveDate="       + prioreffectiveDate 
                                  + "15.loanPriorEffectiveDate="   + loanPriorEffectiveDate
                                  + "16.propertyType="             + propertyType              
                                  + "17.simultaneous="             + simultaneous
                                  + "18.version="                  + version
                                  + "19.lenderCPL="                + lenderCPL
                                  + "20.buyerCPL="                 + buyerCPL
                                  + "21.sellerCPL="                + sellerCPL.
 

  if coverageAmount             eq "coverageAmount"               or coverageAmount               eq "0" 
   then
    coverageAmount = "".  

  if reissueCoverageAmount      eq "reissueCoverageAmount"        or reissueCoverageAmount        eq "0" 
   then
    reissueCoverageAmount = "".  

  if loanCoverageAmount         eq "loanCoverageAmount"           or loanCoverageAmount           eq "0" 
   then
    loanCoverageAmount = "".  

  if loanReissueCoverageAmount  eq "loanReissueCoverageAmount"    or loanReissueCoverageAmount    eq "0" 
   then
    loanReissueCoverageAmount = "". 

  if secondLoanCoverageAmount   eq "secondLoanCoverageAmount"     or secondLoanCoverageAmount     eq "0" 
   then
    secondLoanCoverageAmount = "".

  if rateType           eq "rateType"          
   then
    rateType = "".

  if loanRateType       eq "loanRateType"      
   then
    loanRateType = "".

  if secondLoanRateType eq "secondLoanRateType" 
   then
    secondLoanRateType = "".  

  if priorEffectiveDate eq "priorEffectiveDate" or priorEffectiveDate eq "?"    
   then
    priorEffectiveDate = "".

  if propertyType       eq "propertyType"     
   then
    propertyType = "".

  if ownerEndors        eq "ownerEndors"      
   then
    ownerEndors = "".

  if loanEndors         eq "loanEndors"       
   then
    loanEndors = "".

  if secondLoanEndors   eq "secondLoanEndors" 
   then
    secondLoanEndors = "".

  if Version            eq "Version"   or Version   eq "0" 
   then
    Version = "".

  if loanPriorEffectiveDate eq "loanPriorEffectiveDate" or loanPriorEffectiveDate eq "?"    
   then
    loanPriorEffectiveDate = "".

  if lenderCPL       eq "lenderCPL" or lenderCPL  eq "0" 
   then
    lenderCPL = "".

  if buyerCPL        eq "buyerCPL"  or buyerCPL   eq "0" 
   then
    buyerCPL = "".

  if sellerCPL       eq "sellerCPL" or sellerCPL  eq "0" 
   then
    sellerCPL = "".

  
  /* Datatype conversion */
  assign
      iCovgAmt            = 0
      iLoanCovgAmt        = 0
      iSecondLoanCovgAmt  = 0
      iReissueCovgAmt     = 0
      iLoanReissueCovgAmt = 0
      iVersion            = 0
      iLenderCPL          = 0
      iBuyerCPL           = 0
      iSellerCPL          = 0
      .
  assign
      iCovgAmt                   = integer(coverageAmount)
      iReissueCovgAmt            = integer(reissueCoverageAmount)
      iLoanCovgAmt               = integer(loanCoverageAmount)
      iLoanReissueCovgAmt        = integer(loanReissueCoverageAmount)   /* In this logic iLoanReissueCovgAmt variable is used to take the number of parcels value */
      iSecondLoanCovgAmt         = integer(secondLoanCoverageAmount)
      iVersion                   = integer(Version)                    
      lSimultaneous              = logical(simultaneous) 
      dtPriorPolicyDate          = date(prioreffectiveDate)
      dtPriorLoanPolicyDate      = date(loanPriorEffectiveDate)
      iLenderCPL                 = integer(lenderCPL)
      iBuyerCPL                  = integer(buyerCPL)
      iSellerCPL                 = integer(sellerCPL)
      no-error.
  
  if error-status:error                               
   then 
    do:
      if error-status:num-messages > 0 
       then
        do iCntError = 1 to error-status:num-messages:
         cErrorMsgs = cErrorMsgs + "::" + error-status:get-message(iCntError).
        end.
       else
        cErrorMsgs =  "Error occured in parsing.".         
       
      CreateLog(cErrorMsgs ,"", 0, "",{&GroupError},{&GroupError},0 ).  
    end.

  if dtPriorPolicyDate = ? 
   then
    iAgeOfOriginalPolicy = 0.
   else
    assign
        iAgeOfOriginalPolicy = truncate((today - dtPriorPolicyDate) / 365, 0)      
        iNumDays             = (today - dtPriorPolicyDate) modulo 365
        iAgeOfOriginalPolicy = if iNumDays > 0 then (iAgeOfOriginalPolicy + 1) else iAgeOfOriginalPolicy
        .

  if dtPriorLoanPolicyDate = ?
   then
     iAgeofLoanPolicy = 0.
   else
    assign
        iAgeofLoanPolicy = truncate((today - dtPriorLoanPolicyDate) / 365, 0)
        iNoOfLoanDays    = (today - dtPriorLoanPolicyDate) modulo 365
        iAgeofLoanPolicy = if iNoOfLoanDays > 0 then (iAgeofLoanPolicy + 1) else iAgeofLoanPolicy
        .               

  /* Debugging */
  debugparameter = debugparameter + "21.covgAmt="              + string(iCovgAmt) 
                                  + "22.reissueCovgAmt="       + string(iReissueCovgAmt)
                                  + "23.loanCovgAmt="          + string(iLoanCovgAmt)  
                                  + "24.reissueloanCovgAmt="   + string(iLoanReissueCovgAmt) 
                                  + "25.secondloanCovgAmt="    + string(iSecondloanCovgAmt)
                                  + "26.iVersion="             + string(iVersion)       
                                  + "27.lSimultaneous="        + string(lSimultaneous)
                                  + "28.priorPolicyDate="      + if dtPriorPolicyDate = ? then "" else string(dtPriorPolicyDate) 
                                  + "29.iNumDays="             + string(iNumDays)
                                  + "30.iAgeOfOriginalPolicy=" + string(iAgeOfOriginalPolicy) 
                                  + "31.LoanPriorPolicyDate="  + if dtPriorLoanPolicyDate = ? then "" else string(dtPriorLoanPolicyDate)
                                  + "32.iNoOfLoanDays="        + string(iNoofLoanDays)
                                  + "33.iAgeofLoanPolicy="     + string(iAgeofLoanPolicy).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-resiLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resiLoan Procedure 
PROCEDURE resiLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcRateType     as character no-undo.
  define input  parameter ipiAmount       as integer   no-undo.
  define output parameter opdePremium     as decimal   no-undo.

  define variable cCardName               as character no-undo.
  define variable cAttribute              as character no-undo.
                                         
  {lib/nacard.i &rateType = ipcRateType , &cardName = cCardName , &attrID = cAttribute}   
  
  CreateLog("Scenario Executed: Residential Loan","", 0, "",{&Scenario},{&Scenario},0 ).   

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcRateType:
   when {&UiLoan} 
    then
     assign
         cCardName      = {&cLoanOriginal}
         cAttribute     = ""
         cScenarioName  = "First Mortgage Loan"
         cScenarioDesc  = "Original First Mortgage Title insurance"
         .
   when {&UiLoanTwoYearLimitationConstruction} 
    then
     assign
         cCardName      = {&cTwoYearLimitedConstruction}
         cAttribute     = ""
         cScenarioName  = "Two-Year Limitation For Construction"
         cScenarioDesc  = "Loan Policy With Two-Year Limitation For Construction"
         .
   
   otherwise
    do:
      CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
      opdePremium = ?.
      return.
    end.
  end case.
  
  opdePremium = CalculateBasicPremium(cCardName,cAttribute,ipiAmount,{&LendersPolicy},1). 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-resiLoanReissue) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resiLoanReissue Procedure 
PROCEDURE resiLoanReissue :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcRateType                  as character no-undo.
  define input  parameter ipiLoanCoverageAmount        as integer   no-undo.
  define input  parameter ipiLoanReissueCoverageAmount as integer   no-undo.
  define output parameter opdePremium                  as decimal   no-undo.

  define variable cCardName                  as character no-undo.
  define variable cExcessCardName            as character no-undo.
  define variable cAttribute                 as character no-undo.
  define variable cExcessAttribute           as character no-undo.
  define variable opdeExcessPremium          as decimal   no-undo.

  CreateLog("Scenario Executed: Residential Loan Reissue","", 0, "",{&Scenario},{&Scenario},0 ). 

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcRateType:
   when {&UiSubstitutionRate} 
    then
     assign
         cCardName        = {&cSubstitutionRate}
         cExcessCardName  = {&cSubstitutionRateExcess}
         cAttribute       = ""
         cExcessAttribute = ""
         cScenarioName    = "Substitution Rate/Reissue First Mortgage"
         cScenarioDesc    = "First Mortgage Policy issued on a property which was earlier insured as owner,loan"
         . 

   otherwise
    do:
      CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
      opdePremium = ?.
      return.
    end.
  end case.
   
 if iAgeofLoanPolicy <= 3 
  then
   cAttribute = {&LE3YEARS}. 
 else if iAgeofLoanPolicy <= 5
  then
   cAttribute = {&LE5YEARS}.
  else if iAgeofLoanPolicy <= 10
   then
    cAttribute = {&LE10YEARS}.
   else
    cAttribute = {&GT10YEARS}.

  if ipiLoanCoverageAmount > ipiLoanReissueCoverageAmount 
   then
    do:
      opdePremium       = CalculateBasicPremium(cCardName,cAttribute,ipiLoanReissueCoverageAmount,{&LendersPolicy}, 1).
      opdeExcessPremium = CalculateExcessPremium(cExcesscardName,cExcessAttribute,ipiLoanCoverageAmount, ipiLoanReissueCoverageAmount, {&LendersPolicy}, 1).
    end.
   else
    opdePremium = CalculateBasicPremium(cCardName,cAttribute,ipiLoanCoverageAmount,{&LendersPolicy},1). 

  if opdeExcessPremium > 0
   then  
    assign
        cScenarioName   = cScenarioName + " w/Excess Amount"
        cScenarioDesc   = cScenarioDesc + " involving Excess Amount"
        . 

  opdePremium = opdePremium + opdeExcessPremium.

  CreateLog("Calculated premium for Face amount and Excess amount is " + trim(string(opdePremium,"$>>>,>>>,>>9.99")),trim(string(opdePremium,">>>,>>>,>>9.99")),0, "",{&LendersPolicy},{&PremiumResult}, 1).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-resiLoanScndLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resiLoanScndLoan Procedure 
PROCEDURE resiLoanScndLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcLoanRateType     as character no-undo.
  define input  parameter ipcScndLoanRateType as character no-undo.
  define input  parameter ipiLoanAmount       as integer   no-undo.
  define input  parameter ipiScndLoanAmount   as integer   no-undo.
  define output parameter opdePremiumLoan     as decimal   no-undo.
  define output parameter opdePremiumScndLoan as decimal   no-undo.

  define variable cLoanCardName               as character no-undo.
  define variable cSimoScndLoanCardName       as character no-undo. 
  define variable cExcessScndLoanCardName     as character no-undo.
  define variable cLoanAttribute              as character no-undo.
  define variable cScndLoanAttribute          as character no-undo.
  define variable cScndLoanAttributeExcess    as character no-undo.
  define variable dePremiumSimoScndLoan       as decimal   no-undo.
  define variable dePremiumExcessScndLoan     as decimal   no-undo.

  CreateLog("Scenario Executed: Residential Loan-SecondLoan","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcLoanRateType:
   when {&UiLoan} 
    then
     assign
         cLoanCardName  = {&cLoanOriginal}
         cLoanAttribute = ""
         cScenarioName  = "First Mortgage Loan"
         cScenarioDesc  = "Original First Mortgage Title insurance"   
         .
   otherwise
    do:
      CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
      assign
          opdePremiumLoan        = ?
          opdePremiumScndLoan    = ?.  
      return.
    end.
  end case.

  case ipcScndLoanRateType:
   when {&uiSecondMortgage} 
    then
     assign
         cSimoScndLoanCardName    = {&cSimoSecondLoan}
         cExcessScndLoanCardName  = {&cSimoSecondLoanExcess}
         cScndLoanAttribute       = ""
         cScndLoanAttributeExcess = ""
         cScenarioName            = cScenarioName + " - Second Mortgage"
         cScenarioDesc            = cScenarioDesc + " and Second Mortgage Policy"
         .       
   otherwise
    do:
      CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcScndLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .      
      assign
          opdePremiumLoan        = ?
          opdePremiumScndLoan    = ?.  
      return. 
    end.
  end case.

  opdePremiumLoan  = CalculateBasicPremium(cLoanCardName,cLoanAttribute,ipiLoanAmount,{&LendersPolicy},1). 

  if ipiScndLoanAmount > ipiLoanAmount 
   then
    do:
      dePremiumSimoScndLoan   = CalculateBasicPremium(cSimoScndLoanCardName,cScndLoanAttribute,ipiLoanAmount,{&SecondLendersPolicy},2).
      dePremiumExcessScndLoan = CalculateExcessPremium(cExcessScndLoanCardName,cScndLoanAttributeExcess,ipiScndLoanAmount,ipiLoanAmount,{&SecondLendersPolicy},2). 
    end.
   else
    dePremiumSimoScndLoan = CalculateBasicPremium(cSimoScndLoanCardName,cScndLoanAttribute,ipiScndLoanAmount,{&SecondLendersPolicy},2). 
    
  opdePremiumScndLoan = dePremiumSimoScndLoan + dePremiumExcessScndLoan.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-resiOwner) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resiOwner Procedure 
PROCEDURE resiOwner :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcRateType      as character no-undo.
  define input  parameter ipiAmount        as integer   no-undo.
  define output parameter opdePremium      as decimal   no-undo.

  define variable cCardName                as character no-undo.
  define variable cAttribute               as character no-undo.

  {lib/nacard.i &rateType = ipcRateType , &cardName = cCardName , &attrID = cAttribute} 
  
  CreateLog("Scenario Executed: Residential Owner","", 0, "",{&Scenario},{&Scenario},0 ).   

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcRateType:
   when {&UiOwnersOriginal} 
    then
     assign
         cCardName       = {&cOwnersOriginal}
         cAttribute      = ""
         cScenarioName   = "Owner's Original"
         cScenarioDesc   = "Owner's Original policy insuring a fee simple estate"
         .   
   otherwise
    do:
      CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcRateType + ".","", 0, "","",{&CardSelectionError},0 ) .  
      opdePremium = ?.
      return.
    end.
  end case.
  
  opdePremium = CalculateBasicPremium(cCardName,cAttribute,ipiAmount,{&OwnersPolicy},1). 
                 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-resiOwnerLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resiOwnerLoan Procedure 
PROCEDURE resiOwnerLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcOwnerRateType as character no-undo.
  define input  parameter ipcLoanRateType  as character no-undo.
  define input  parameter ipiOwnerAmount   as integer   no-undo.
  define input  parameter ipiLoanAmount    as integer   no-undo.
  define output parameter opdePremiumOwner as decimal   no-undo.
  define output parameter opdePremiumLoan  as decimal   no-undo.

  define variable cOwnerCardName           as character no-undo.
  define variable cSimoCardName            as character no-undo.
  define variable cExcessCardName          as character no-undo.
  define variable cOwnerAttribute          as character no-undo.
  define variable cLoanAttribute           as character no-undo.
  define variable cLoanAttributeExcess     as character no-undo.
  define variable dePremiumSimo            as decimal   no-undo.
  define variable dePremiumExcess          as decimal   no-undo.

  CreateLog("Scenario Executed: Residential Owner-Loan","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */   
  case ipcOwnerRateType:
   when {&UiOwnersOriginal} 
    then
     assign
         cOwnerCardName  = {&cOwnersOriginal}
         cOwnerAttribute = ""
         cScenarioName   = "Owner's Original"
         cScenarioDesc   = "Owner's Original policy insuring a fee simple estate"
         .   
   otherwise
    do:
      CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcOwnerRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
      assign
          opdePremiumOwner = ?
          opdePremiumLoan  = ?.  
      return.
    end.
  end case.

 
  case ipcLoanRateType:
   when {&UiLoan} 
    then
     assign
         cSimoCardName        = {&cSimoLoan}
         cExcessCardName      = {&cSimoLoanExcess}
         cLoanAttribute       = ""
         cLoanAttributeExcess = "" 
         cScenarioName        = cScenarioName + " - First Mortgage Original"
         cScenarioDesc        = cScenarioDesc + " and First Mortgage Original Policy"
         .
 
   otherwise
    do:
      CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
      assign
          opdePremiumOwner = ?
          opdePremiumLoan  = ?.  
      return.
    end.
  end case.
      
  opdePremiumOwner = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiOwnerAmount,{&OwnersPolicy},1). 
  
  if ipiLoanAmount > ipiOwnerAmount
   then
    do:
      dePremiumSimo   = CalculateBasicPremium(cSimoCardName,cLoanAttribute,ipiOwnerAmount,{&LendersPolicy},2).
      dePremiumExcess = CalculateExcessPremium(cExcessCardName,cLoanAttributeExcess,ipiLoanAmount,ipiOwnerAmount,{&LendersPolicy},2).
      assign
          cScenarioName        = cScenarioName + " w/Excess Amount"
          cScenarioDesc        = cScenarioDesc + " involving Excess Amount"
          .
    end.
   else
    dePremiumSimo = CalculateBasicPremium(cSimoCardName,cLoanAttribute,ipiLoanAmount,{&LendersPolicy},2). 

  opdePremiumLoan = dePremiumSimo + dePremiumExcess.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-resiOwnerLoanScndLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resiOwnerLoanScndLoan Procedure 
PROCEDURE resiOwnerLoanScndLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter  ipcOwnerRateType    as character no-undo.
  define input parameter  ipcLoanRateType     as character no-undo.
  define input parameter  ipcScndLoanRateType as character no-undo.
  define input parameter  ipiOwnerAmount      as integer   no-undo.
  define input parameter  ipiLoanAmount       as integer   no-undo.
  define input parameter  ipiScndLoanAmount   as integer   no-undo.
  define output parameter opdePremiumOwner    as decimal   no-undo.
  define output parameter opdePremiumLoan     as decimal   no-undo.
  define output parameter opdePremiumScndLoan as decimal   no-undo.

  define variable cOwnerCardName              as character no-undo.
  define variable cSimoLoanCardName           as character no-undo.
  define variable cExcessLoanCardName         as character no-undo.
  define variable cSimoScndLoanCardName       as character no-undo.
  define variable cExcessScndLoanCardName     as character no-undo.
  define variable cOwnerAttribute             as character no-undo.
  define variable cLoanAttribute              as character no-undo.
  define variable cLoanAttributeExcess        as character no-undo.
  define variable cScndLoanAttribute          as character no-undo.
  define variable cScndLoanAttributeExcess    as character no-undo.
  define variable dePremiumSimoLoan           as decimal   no-undo.
  define variable dePremiumSimoScndLoan       as decimal   no-undo.
  define variable dePremiumLoanExcess         as decimal   no-undo.
  define variable dePremiumScndLoanExcess     as decimal   no-undo.
  
  CreateLog("Scenario Executed: Residential Owner-Loan-SecondLoan","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcOwnerRateType:
   when {&UiOwnersOriginal} 
    then
     assign
         cOwnerCardName  = {&cOwnersOriginal}
         cOwnerAttribute = ""
         cScenarioName   = "Owner's Original"
         cScenarioDesc   = "Simultaneous issue of Owner's Original"
         . 
   otherwise
    do:
      CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcOwnerRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
      assign 
          opdePremiumOwner    = ?
          opdePremiumLoan     = ?
          opdePremiumScndLoan = ?
          .        
      return.      
    end.
  end case.

  case ipcLoanRateType:
   when {&UiLoan} 
    then
     assign
         cSimoLoanCardName    = {&cSimoLoan}
         cExcessLoanCardName  = {&cSimoLoanExcess}
         cLoanAttribute       = ""
         cLoanAttributeExcess = "" 
         cScenarioName        = cScenarioName + " - First Mortgage Original"
         cScenarioDesc        = cScenarioDesc + " and First Mortgage Original Policy"
         .
  

   otherwise
    do:
      CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
      assign 
          opdePremiumOwner    = ?
          opdePremiumLoan     = ?
          opdePremiumScndLoan = ?
          .        
      return.  
    end.
  end case.
      
  case ipcScndLoanRateType:
   when {&uiSecondMortgage}
    then
     assign
         cSimoScndLoanCardName    = {&cSimoSecondLoan}
         cExcessScndLoanCardName  = {&cSimoSecondLoanExcess}
         cScndLoanAttribute       = ""
         cScndLoanAttributeExcess = ""
         cScenarioName            = cScenarioName + " - Second Mortgage Original"
         cScenarioDesc            = cScenarioDesc + " and Second Mortgage Original"
         .    
   otherwise
    do:
      CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcScndLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
      assign 
          opdePremiumOwner    = ?
          opdePremiumLoan     = ?
          opdePremiumScndLoan = ?
          .        
      return.  
    end.
  end case.
  
  opdePremiumOwner      = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiOwnerAmount,{&OwnersPolicy},1).
  dePremiumSimoLoan     = CalculateBasicPremium(cSimoLoanCardName,cLoanAttribute,min(ipiOwnerAmount,ipiLoanAmount),{&LendersPolicy},2).
  dePremiumSimoScndLoan = CalculateBasicPremium(cSimoScndLoanCardName,cScndLoanAttribute,min(ipiOwnerAmount,ipiScndLoanAmount),{&SecondLendersPolicy},3).
  
  if ipiLoanAmount le ipiOwnerAmount and (ipiLoanAmount + ipiScndLoanAmount) > ipiOwnerAmount  
   then 
    depremiumScndLoanExcess = CalculateExcessPremium(cExcessScndLoanCardName,cScndLoanAttributeExcess,ipiLoanAmount + ipiScndLoanAmount , ipiOwnerAmount,{&SecondLendersPolicy},3 ).
   else if ipiLoanAmount > ipiOwnerAmount  
    then
     do:
       dePremiumLoanExcess     = CalculateExcessPremium(cExcessLoanCardName,cLoanAttributeExcess,ipiLoanAmount ,ipiOwnerAmount,{&LendersPolicy},2 ).
       dePremiumScndLoanExcess = CalculateExcessPremium(cExcessScndLoanCardName,cScndLoanAttributeExcess,ipiScndLoanAmount + ipiLoanAmount ,ipiLoanAmount,{&SecondLendersPolicy},3).
     end.

    
  if dePremiumLoanExcess > 0 or dePremiumScndLoanExcess > 0
   then  
    assign
        cScenarioName    = cScenarioName + " w/Excess Amount"
        cScenarioDesc    = cScenarioDesc + " involving Excess Amount"
        .

  assign
      opdePremiumLoan     = dePremiumSimoLoan     + dePremiumLoanExcess
      opdePremiumScndLoan = dePremiumSimoScndLoan + dePremiumScndLoanExcess
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-resiOwnerReissue) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resiOwnerReissue Procedure 
PROCEDURE resiOwnerReissue :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcRateType              as character no-undo.
  define input  parameter ipiCoverageAmount        as integer   no-undo.
  define input  parameter ipiReissueCoverageAmount as integer   no-undo.
  define output parameter opdePremium              as decimal   no-undo.

  define variable cCardName                   as character no-undo.
  define variable cExcessCardName             as character no-undo.
  define variable cAttribute                  as character no-undo.
  define variable cExcessAttribute            as character no-undo.
  define variable opdeExcessPremium           as decimal   no-undo.

  CreateLog("Scenario Executed: Residential OwnerReissue","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcRateType:
   when {&UiOwnersReissue}
    then
     do:
       assign
           cCardName        = {&cOwnersReissue}
           cExcessCardName  = {&cOwnersReissueExcess}
           cAttribute       = ""
           cExcessAttribute = ""
           cScenarioName    = "Owner's Reissue"
           cScenarioDesc    = "Purchaser/lessee of real estate whose title has been insured by either an owners, loan or leasehold policy within 5 years prior to the application for new policy"
           .
     end.
   otherwise
    do:
      CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
      opdePremium = ?.       
      return.
    end.
  end case.

  if iAgeOfOriginalPolicy <= 1 
   then
    cAttribute = {&LE1YEARS}.         
   else if iAgeOfOriginalPolicy <= 2
    then
     cAttribute = {&LE2YEARS}.         
    else if iAgeOfOriginalPolicy <= 3
     then
      cAttribute = {&LE2TO3YEARS}. 
     else
      cAttribute = {&GT3YEARS}.
  
  
  if ipiCoverageAmount > ipiReissueCoverageAmount 
   then
    do:
      opdePremium       = CalculateBasicPremium(cCardName,cAttribute,ipiReissueCoverageAmount,{&OwnersPolicy},1). 
      opdeExcessPremium = CalculateExcessPremium(cExcessCardName,cExcessAttribute,ipiCoverageAmount, ipiReissueCoverageAmount, {&OwnersPolicy},1).
    end.
   else
    opdePremium  = CalculateBasicPremium(cCardName,cAttribute,ipiCoverageAmount,{&OwnersPolicy},1).

  if opdeExcessPremium > 0
   then  
    assign
        cScenarioName   = cScenarioName + " w/Excess Amount"
        cScenarioDesc   = cScenarioDesc + " involving Excess Amount"
        . 
  
  opdePremium = opdePremium + opdeExcessPremium.
 

  CreateLog("Calculated premium for Face amount and Excess amount is " + trim(string(opdePremium,"$>>>,>>>,>>9.99")),trim(string(opdePremium,">>>,>>>,>>9.99")),0, "", {&OwnersPolicy}, {&PremiumResult},1).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-resiOwnerReissueLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resiOwnerReissueLoan Procedure 
PROCEDURE resiOwnerReissueLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcOwnerRateType              as character no-undo.
  define input  parameter ipcLoanRateType               as character no-undo.
  define input  parameter ipiOwnerCoverageAmount        as integer   no-undo.
  define input  parameter ipiOwnerReissueCoverageAmount as integer   no-undo.
  define input  parameter ipiLoanAmount                 as integer   no-undo.
  define output parameter opdePremiumOwner              as decimal   no-undo.
  define output parameter opdePremiumLoan               as decimal   no-undo.

  define variable cOwnerCardName              as character no-undo.
  define variable cOwnerExcessCardName        as character no-undo.
  define variable cOwnerExcessAttribute       as character no-undo.
  define variable cSimoCardName               as character no-undo.
  define variable cLoanExcessCardName         as character no-undo.
  define variable cOwnerAttribute             as character no-undo.
  define variable cLoanAttribute              as character no-undo.
  define variable cLoanAttributeExcess        as character no-undo.
  define variable dePremiumSimo               as decimal   no-undo.
  define variable dePremiumSimoExcess         as decimal   no-undo.
  define variable opdeOwnerExcessPremium      as decimal   no-undo.

  CreateLog("Scenario Executed: Residential Owner Reissue-Loan","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */  
  case ipcOwnerRateType:
   when {&UiOwnersReissue} 
    then
     do:
       assign
           cOwnerCardName        = {&cOwnersReissue} 
           cOwnerExcessCardName  = {&cOwnersReissueExcess}
           cOwnerAttribute       = ""
           cOwnerExcessAttribute = ""
           cScenarioName         = "Owner's Reissue"
           cScenarioDesc         = "Simultaneous issue of Owner's Reissue"
           .       
     end.
   otherwise
    do:
      CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcOwnerRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
      assign
          opdePremiumOwner  = ?
          opdePremiumLoan   = ?. 
      return.
    end.
  end case.

  if iAgeOfOriginalPolicy <= 1 
   then
    cOwnerAttribute = {&LE1YEARS}. 
   else if iAgeOfOriginalPolicy <= 2
    then
     cOwnerAttribute = {&LE2YEARS}.
    else if iAgeOfOriginalPolicy <= 3
     then
      cOwnerAttribute = {&LE2TO3YEARS}.
     else
      cOwnerAttribute = {&GT3YEARS}.

  case ipcLoanRateType:
   when {&UiLoan} 
    then
     assign
         cSimoCardName        = {&cSimoLoan}
         cLoanExcessCardName  = {&cSimoLoanExcess}
         cLoanAttribute       = ""
         cLoanAttributeExcess = "" 
         cScenarioName        = cScenarioName + " - First Mortgage Original"
         cScenarioDesc        = cScenarioDesc + " and First Mortgage Original Policy"
         .
   otherwise
    do:
      CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
      assign
          opdePremiumOwner  = ?
          opdePremiumLoan   = ?. 
      return.
    end.
  end case.

  
  if ipiOwnerCoverageAmount > ipiOwnerReissueCoverageAmount 
   then
    do:
      opdePremiumOwner       = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiOwnerReissueCoverageAmount,{&OwnersPolicy},1). 
      opdeOwnerExcessPremium = CalculateExcessPremium(cOwnerExcessCardName,cOwnerExcessAttribute,ipiOwnerCoverageAmount, ipiOwnerReissueCoverageAmount, {&OwnersPolicy},1).
    end.
   else
    opdePremiumOwner  = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiOwnerCoverageAmount,{&OwnersPolicy},1).
  
  opdePremiumOwner = opdePremiumOwner + opdeOwnerExcessPremium.
 
  CreateLog("Calculated premium for Face amount and Excess amount is " + trim(string(opdePremiumOwner,"$>>>,>>>,>>9.99")),trim(string(opdePremiumOwner,">>>,>>>,>>9.99")),0, "",{&OwnersPolicy},{&PremiumResult},1).
 
  if ipiLoanAmount > ipiOwnerCoverageAmount 
   then
    do:
      dePremiumSimo       = CalculateBasicPremium(cSimoCardName,cLoanAttribute,ipiOwnerCoverageAmount,{&LendersPolicy},2).
      dePremiumSimoExcess = CalculateExcessPremium(cLoanExcessCardName,cLoanAttributeExcess,ipiLoanAmount,ipiOwnerCoverageAmount,{&LendersPolicy},2). 
    end.
   else
    dePremiumSimo = CalculateBasicPremium(cSimoCardName,cLoanAttribute,ipiLoanAmount,{&LendersPolicy},2). 

  if opdeOwnerExcessPremium > 0 or dePremiumSimoExcess > 0
   then  
    assign
        cScenarioName   = cScenarioName + " w/Excess Amount"
        cScenarioDesc   = cScenarioDesc + " involving Excess Amount"
        . 

  opdePremiumLoan = dePremiumSimo + dePremiumSimoExcess.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-resiOwnerReissueLoanScndLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resiOwnerReissueLoanScndLoan Procedure 
PROCEDURE resiOwnerReissueLoanScndLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter  ipcOwnerRateType              as character no-undo.
  define input  parameter  ipcLoanRateType               as character no-undo.
  define input  parameter  ipcScndLoanRateType           as character no-undo.
  define input  parameter  ipiOwnerCoverageAmount        as integer   no-undo.
  define input  parameter  ipiOwnerReissueCoverageAmount as integer   no-undo.
  define input  parameter  ipiLoanAmount                 as integer   no-undo.
  define input  parameter  ipiScndLoanAmount             as integer   no-undo.
  define output parameter  opdePremiumOwner              as decimal   no-undo.
  define output parameter  opdePremiumLoan               as decimal   no-undo.
  define output parameter  opdePremiumScndLoan           as decimal   no-undo.

  define variable cOwnerCardName               as character no-undo.
  define variable cSimoLoanCardName            as character no-undo.
  define variable cExcessLoanCardName          as character no-undo.
  define variable cSimoScndLoanCardName        as character no-undo.
  define variable cExcessScndLoanCardName      as character no-undo.
  define variable cOwnerAttribute              as character no-undo.
  define variable cLoanAttribute               as character no-undo.
  define variable cLoanAttributeExcess         as character no-undo.
  define variable cScndLoanAttribute           as character no-undo.
  define variable cScndLoanAttributeExcess     as character no-undo.
  define variable depremiumLoanExcess          as decimal   no-undo.
  define variable depremiumScndLoanExcess      as decimal   no-undo.
  define variable dePremiumSimoLoan            as decimal   no-undo.
  define variable dePremiumSimoScndLoan        as decimal   no-undo.
  define variable opdeOwnerExcessPremium       as decimal   no-undo.
  define variable cOwnerExcesscardName         as character no-undo.
  define variable cOwnerExcessAttribute        as character no-undo.
  
  CreateLog("Scenario Executed: Residential Owner Reissue-Loan-SecondLoan","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcOwnerRateType:
   when {&UiOwnersReissue} 
    then
     assign
         cOwnerCardName        = {&cOwnersReissue} 
         cOwnerExcessCardName  = {&cOwnersReissueExcess}
         cOwnerAttribute       = ""
         cOwnerExcessAttribute = ""
         cScenarioName         = "Owner's Reissue"
         cScenarioDesc         = "Simultaneous issue of Owner's Reissue"
         .

   otherwise
    do:
      CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcOwnerRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
      assign 
          opdePremiumOwner     = ?
          opdePremiumLoan      = ?
          opdePremiumScndLoan  = ?.        
      return.
    end.
  end case.

  if iAgeOfOriginalPolicy <= 1 
   then
    cOwnerAttribute = {&LE1YEARS}. 
   else if iAgeOfOriginalPolicy <= 2
    then
     cOwnerAttribute = {&LE2YEARS}.
    else if iAgeOfOriginalPolicy <= 3
     then
      cOwnerAttribute = {&LE2TO3YEARS}.
     else
      cOwnerAttribute = {&GT3YEARS}.

  case ipcLoanRateType:
   when {&UiLoan} 
    then
     assign
         cSimoLoanCardName    = {&cSimoLoan}
         cExcessLoanCardName  = {&cSimoLoanExcess}
         cLoanAttribute       = ""
         cLoanAttributeExcess = "" 
         cScenarioName        = cScenarioName + " - First Mortgage Original"
         cScenarioDesc        = cScenarioDesc + " and First Mortgage Original Policy"
         .

   otherwise
    do:
      CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
      assign 
          opdePremiumOwner     = ?
          opdePremiumLoan      = ?
          opdePremiumScndLoan  = ?.        
      return.
    end.
  end case.
          
  case ipcScndLoanRateType:
   when {&uiSecondMortgage} 
    then
     assign
         cSimoScndLoanCardName    = {&cSimoSecondLoan}
         cExcessScndLoanCardName  = {&cSimoSecondLoanExcess}
         cScndLoanAttribute       = ""
         cScndLoanAttributeExcess = "" 
         cScenarioName            = cScenarioName + " - Second Mortgage Original"
         cScenarioDesc            = cScenarioDesc + " and Second Mortgage Original"
         .               
   otherwise
    do:
      CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcScndLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
      assign 
          opdePremiumOwner     = ?
          opdePremiumLoan      = ?
          opdePremiumScndLoan  = ?.        
      return.
    end.
  end case.

  if ipiOwnerCoverageAmount > ipiOwnerReissueCoverageAmount 
   then
    do:
      opdePremiumOwner       = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiOwnerReissueCoverageAmount,{&OwnersPolicy},1). 
      opdeOwnerExcessPremium = CalculateExcessPremium(cOwnerExcessCardName,cOwnerExcessAttribute,ipiOwnerCoverageAmount, ipiOwnerReissueCoverageAmount, {&OwnersPolicy},1).
    end.
   else
    opdePremiumOwner  = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiOwnerCoverageAmount,{&OwnersPolicy},1).
  
  opdePremiumOwner = opdePremiumOwner + opdeOwnerExcessPremium. 

  CreateLog("Calculated premium for Face amount and Excess amount is " + trim(string(opdePremiumOwner,"$>>>,>>>,>>9.99")),trim(string(opdePremiumOwner,">>>,>>>,>>9.99")),0, "",{&OwnersPolicy},{&PremiumResult},1).

  dePremiumSimoLoan     = CalculateBasicPremium(cSimoLoanCardName,cLoanAttribute,min(ipiOwnerCoverageAmount,ipiLoanAmount),{&LendersPolicy},2).
  dePremiumSimoScndLoan = CalculateBasicPremium(cSimoScndLoanCardName,cScndLoanAttribute,min(ipiOwnerCoverageAmount,ipiScndLoanAmount),{&SecondLendersPolicy},3).

  if ipiLoanAmount le ipiOwnerCoverageAmount and (ipiLoanAmount + ipiScndLoanAmount) > ipiOwnerCoverageAmount 
   then
    depremiumScndLoanExcess = CalculateExcessPremium(cExcessScndLoanCardName,cScndLoanAttributeExcess,ipiLoanAmount + ipiScndLoanAmount , ipiOwnerCoverageAmount,{&SecondLendersPolicy},3 ).
   else if ipiLoanAmount > ipiOwnerCoverageAmount  
    then
     do:
       dePremiumLoanExcess     = CalculateExcessPremium(cExcessLoanCardName,cLoanAttributeExcess,ipiLoanAmount ,ipiOwnerCoverageAmount,{&LendersPolicy},2 ).
       depremiumScndLoanExcess = CalculateExcessPremium(cExcessScndLoanCardName,cScndLoanAttributeExcess,ipiScndLoanAmount + ipiLoanAmount ,ipiLoanAmount,{&SecondLendersPolicy},3).
     end.
     
  if opdeOwnerExcessPremium > 0 or dePremiumLoanExcess > 0 or dePremiumScndLoanExcess > 0
   then  
    assign
        cScenarioName   = cScenarioName + " w/Excess Amount"
        cScenarioDesc   = cScenarioDesc + " involving Excess Amount"
        . 
 
  assign
      opdePremiumLoan     = dePremiumSimoLoan     + dePremiumLoanExcess
      opdePremiumScndLoan = dePremiumSimoScndLoan + depremiumScndLoanExcess
      .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-serverValidations) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE serverValidations Procedure 
PROCEDURE serverValidations :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter oplErrorStatus as logical   no-undo.
  define output parameter opcErrorMsg    as character no-undo.
    
  {lib/navalidate.i &rateType = rateType , &loanRateType = loanRateType , &errorMsg = opcErrorMsg , &errorStatus = oplErrorStatus , &ownerEndors = ownerEndors , &loanEndors = loanEndors}   

  if lSimultaneous    
   then
    do:    
      /* Invalid simultaneous scenario of Owner policies + Second Loan policies */
      if coverageAmount           ne ""  and
         loanCoverageAmount       eq ""  and
         secondLoanCoverageAmount ne ""
       then
        assign 
            opcErrorMsg    =  "Owner and Second Loan policies cannot be issued simultaneously without First Loan policies."
            oplErrorStatus = true
            .       
    end.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-calculateBasicPremium) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION calculateBasicPremium Procedure 
FUNCTION calculateBasicPremium RETURNS DECIMAL
  (  pCardName as character ,pAttrID as character , pAmount as decimal, pCaseType as character, pGroupID as integer ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable dePremium as decimal no-undo.
  define variable lError    as logical no-undo.
  define variable hRateCard as handle  no-undo.
  define variable iCardID   as integer no-undo.

  {lib/nacheck.i &cardName = pCardName}
  
  run rcm\ratecard.p persistent set hRateCard (iCardSetID,
                                               pCardName,
                                               region,
                                               output iCardID).
  if iCardID = 0 
   then
    do:
      run GetLog in hRateCard(output table ttTempRateLog).
      for each ttTempRateLog:
        CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation , ttTempRateLog.cardID,{&face},pCaseType,ttTempRateLog.calculationOn,pGroupID).
      end.
      premiumOutput = "servererrmsg="           + "Rate card not found with ID " + string(iCardID) + 
                      ",success="               + "N"                                              +
                      ",cardSetID="             + string(iCardSetID)                               + 
                      ",manualeffectiveDate="   + cEffectiveDate.
      delete procedure hRateCard.
      return ?.
    end.
  
  dePremium = dynamic-function("GetRate" in hRateCard, pAmount,pAttrID, output lError).


  if lError or dePremium = ?
   then
    do:
      run GetLog in hRateCard(output table ttTempRateLog).
      for each ttTempRateLog:
        CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation, ttTempRateLog.cardID,{&face},pCaseType,ttTempRateLog.calculationOn,pGroupID).
      end.
      premiumOutput = "servererrmsg="         + "Calculation error" + 
                      ",success="             + "N"                 +
                      ",cardSetID="           + string(iCardSetID)  +
                      ",manualeffectiveDate=" + cEffectiveDate.
      delete procedure hRateCard.
      return ?.
    end.
    
  run GetLog in hRateCard(output table ttTempRateLog).

  for each ttTempRateLog:
    CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation, ttTempRateLog.cardID,{&face},pCaseType,ttTempRateLog.calculationOn,pGroupID).
  end.

  delete procedure hRateCard.

  /* CadrdId and attribute list used to create ratescenariocard table */
  cCardAttrib = trim((cCardAttrib + "|" + string(iCardID) + "-" + pAttrID), "|").


  CreateLog("Total policy premium for face amount " + trim(string(pAmount,"$>>>,>>>,>>9.99")) +  " is " + trim(string(dePremium,"$>>>,>>>,>>9.99")),trim(string(dePremium,">>>,>>>,>>9.99")),0, {&face},pCaseType,{&PremiumResult},pGroupID).

  return dePremium.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-calculateEndors) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION calculateEndors Procedure 
FUNCTION calculateEndors RETURNS decimal
  ( pAmount as integer , pGroupName as character,pCaseType as character,pGroupID as integer , pSelectedEndorsement as character, pPropertyType as character, pRateType as character, output premiumEndorsDetail as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
    
------------------------------------------------------------------------------*/
  define variable dePremium          as decimal   no-undo.
  define variable deTempTotalPremium as decimal   no-undo.
  define variable deProposedRate     as character no-undo.
  define variable iCountEndors       as integer   no-undo.
  define variable cAttribute         as character no-undo.
  define variable lError             as logical   no-undo.
  define variable lValidProposedRate as logical   no-undo.  
  define variable hEndCalc           as handle    no-undo.
  define variable iCardID            as integer   no-undo.
                                     
  Endors-List-block:
  do iCountEndors = 1 to num-entries(pSelectedEndorsement, "|"): 
    
    run  rcm\ratecard.p persistent set hEndCalc (iCardSetID,
                                                 entry(1,entry(iCountEndors,pSelectedEndorsement,"|"),"-"),   /*cardname*/ 
                                                 region,
                                                 output iCardID).
    if iCardID = 0 
     then
      do:
        run GetLog in hEndCalc (output table ttTempRateLog).
        for each ttTempRateLog:
          CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation , ttTempRateLog.cardID,{&face},pGroupName,ttTempRateLog.calculationOn,pGroupID).
        end.
        premiumOutput = "servererrmsg="         + "Rate card not found with ID " + string(iCardID) + 
                        ",success="             + "N"                            +
                        ",cardSetID="           + string(iCardSetID)             +
                        ",manualeffectiveDate=" + cEffectiveDate.
        delete procedure hEndCalc.
        next Endors-List-block.
      end.
    
    if pPropertyType = {&Residential} 
     then 
      do:
        if pCaseType eq {&owners} 
         then 
          do: 
            if lookup(pRateType,{&ExtendedCoverageRateType}) > 0 and 
               lookup(entry(1,entry(iCountEndors,pSelectedEndorsement,"|"),"-"),{&ExtendedCoverageSpecificEndors}) > 0 
             then
              cAttribute = {&ResiOwnerExtended}.
            else
            cAttribute = {&Residential}.
          end.
         else if (pCaseType eq {&Lenders} or pCaseType eq {&SecondLenders})
          then 
           do:
             if lookup(pRateType,{&ExtendedCoverageRateType}) > 0 and 
                lookup(entry(1,entry(iCountEndors,pSelectedEndorsement,"|"),"-"),{&ExtendedCoverageSpecificEndors}) > 0 
              then
               cAttribute = {&ResiLoanExtended}.
             else
             cAttribute = {&Loan-Residential}.
           end.  
      end. 

      /* new OT type refers to NONE property type and specific Endorsements type */
     if pRateType = 'NA' and lookup(entry(1,entry(iCountEndors,pSelectedEndorsement,"|"),"-"),{&NASpecificEndors}) > 0
      then
       cAttribute = {&Others}.  
  
    dePremium = dynamic-function("GetRate" in hEndCalc, pAmount, cAttribute, output lError).

    if dePremium = pAmount or dePremium = ? 
     then
      dePremium = 0.

    if lError 
     then
      do:
        run GetLog in hEndCalc(output table ttTempRateLog).
        for each ttTempRateLog:
          CreateLog(ttTempRateLog.action ,ttTempRateLog.actionCalculation, ttTempRateLog.cardID,{&face},pGroupName,ttTempRateLog.calculationOn,pGroupID).
        end.
        PremiumOutput = "servererrmsg="           + "Calculation error" + 
                        ",success="               + "N"                 +
                        ",cardSetID="             + string(iCardSetID)  +
                        ",manualeffectiveDate="   + cEffectiveDate.
        delete procedure hEndCalc.
        return ?.
      end.
    
    lValidProposedRate = false.

    if entry(2,entry(iCountEndors,pSelectedEndorsement,"|"),"-") ne "ProposedRate" and 
       entry(2,entry(iCountEndors,pSelectedEndorsement,"|"),"-") ne {&null} 
     then
      assign
          deProposedRate     =  entry(2,entry(iCountEndors,pSelectedEndorsement,"|"),"-")
          lValidProposedRate = dynamic-function("validateRate" in hEndCalc, deProposedRate,cAttribute, output lError)
          .
  
    premiumEndorsDetail = premiumEndorsDetail + "|" + entry(1,entry(iCountEndors,pSelectedEndorsement,"|"),"-") + "-" + if lValidProposedRate then deProposedRate else string(dePremium).
    deTempTotalPremium  = deTempTotalPremium + if lValidProposedRate then decimal(deProposedRate) else dePremium.
  
    run GetLog in hEndCalc(output table ttTempRateLog).
    for each ttTempRateLog:
      CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation, ttTempRateLog.cardID, {&endorsements},pGroupName,ttTempRateLog.calculationOn,pGroupID).
    end.
  
    if lValidProposedRate 
     then
      CreateLog("Modified endorsements Premium amount after validate rate is " + trim(string(decimal(deProposedRate),"$>>>,>>>,>>9.99")),trim(string(decimal(deProposedRate),">>>,>>>,>>9.99")), 0, {&endorsements},pGroupName,"",pGroupID).
    delete procedure hEndCalc.
    cAttribute = "".
  end.
  
  CreateLog("Total endorsements Premium amount is " + trim(string(decimal(deTempTotalPremium),"$>>>,>>>,>>9.99")),trim(string(decimal(deTempTotalPremium),">>>,>>>,>>9.99")), 0, {&endorsements},pGroupName,{&PremiumResult},pGroupID).
  
  premiumEndorsDetail = trim(premiumEndorsDetail,"|").
  
  return deTempTotalPremium.   /* Function return value. */
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-calculateExcessPremium) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION calculateExcessPremium Procedure 
FUNCTION calculateExcessPremium RETURNS DECIMAL
  (pCardName as character ,pAttrID as character , pExcessAmount as integer, pAmount as integer, pCaseType as character, pGroupID as integer ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  define variable dePremium             as decimal no-undo.
  define variable deOrigAmountPremium   as decimal no-undo.
  define variable deExcessAmountPremium as decimal no-undo.
  define variable lError                as logical no-undo.
  define variable hExcessAmountCalc     as handle  no-undo.
  define variable iCardID               as integer no-undo.
  
  run  rcm\ratecard.p persistent set hExcessAmountCalc (iCardSetID,
                                                        pCardName,
                                                        region,
                                                        output iCardID).
  if iCardID = 0 
   then
    do:
      run GetLog in hExcessAmountCalc(output table ttTempRateLog).
      for each ttTempRateLog:
        CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation , ttTempRateLog.cardID,{&face},pCaseType,ttTempRateLog.calculationOn,pGroupID).
      end.
      premiumOutput = "servererrmsg="           + "Rate card not found with ID " + string(iCardID) + 
                      ",success="               + "N"                                              +
                      ",cardSetID="             + string(iCardSetID)                               + 
                      ",manualeffectiveDate="   + cEffectiveDate.
      delete procedure hExcessAmountCalc.
      return ?.
    end.
              
  deOrigAmountPremium = dynamic-function("GetRate" in hExcessAmountCalc, pAmount, pAttrID, output lError).

  run GetLog in hExcessAmountCalc(output table ttTempRateLog).

  for each ttTempRateLog:
    CreateLog(ttTempRateLog.action ,ttTempRateLog.actionCalculation, ttTempRateLog.cardID,{&excess},pCaseType,ttTempRateLog.calculationOn,pGroupID).
  end.

  if lError or deOrigAmountPremium = ? 
   then
    do:
      premiumOutput = "servererrmsg="           + "Calculation error" + 
                      ",success="               + "N"                 +
                      ",cardSetID="             + string(iCardSetID)  + 
                      ",manualeffectiveDate="   + cEffectiveDate.
      delete procedure hExcessAmountCalc.
      return ?.
    end.
  
  CreateLog("Calculated premium for amount " + trim(string(pAmount, "$>>>,>>>,>>9.99")) + " is " + trim(string(deOrigAmountPremium, "$>>>,>>>,>>9.99")),trim(string(deOrigAmountPremium, ">>>,>>>,>>9.99")), 0, {&excess},pCaseType,"",pGroupID).
  
  deExcessAmountPremium = dynamic-function("GetRate" in hExcessAmountCalc, pExcessAmount, pAttrID, output lError).

  run GetLog in hExcessAmountCalc(output table ttTempRateLog).

  for each ttTempRateLog:
    CreateLog(ttTempRateLog.action ,ttTempRateLog.actionCalculation, ttTempRateLog.cardID,{&excess},pCaseType,ttTempRateLog.calculationOn,pGroupID).
  end.

  if lError or deExcessAmountPremium = ?  
   then
    do:
      premiumOutput = "servererrmsg="         + "Calculation error" + 
                      ",success="             + "N"                 +
                      ",cardSetID="           + string(iCardSetID)  + 
                      ",manualeffectiveDate=" + cEffectiveDate.
      delete procedure hExcessAmountCalc.
      return ?.
    end.

  delete procedure hExcessAmountCalc.
  
  CreateLog("Calculated premium for amount " + trim(string(pExcessAmount, "$>>>,>>>,>>9.99")) + " = " + trim(string(deExcessAmountPremium, "$>>>,>>>,>>9.99")),trim(string(deExcessAmountPremium, ">>>,>>>,>>9.99")), 0, {&excess},pCaseType,"",pGroupID).

  dePremium = deExcessAmountPremium - deOrigAmountPremium.   
 
  /* CadrdId and attribute list used to create ratescenariocard table */    
  cCardAttrib = trim((cCardAttrib + "|" + string(iCardID) + "-" + pAttrID), "|").

  if dePremium < 0 
   then
    do:
      CreateLog("Premium for excess amount " + trim(string(deExcessAmountPremium, "$>>>,>>>,>>9.99")) + " - " + trim(string(deOrigAmountPremium, "$>>>,>>>,>>9.99")) + " is negative. ","" ,iCardID, {&excess},pCaseType,{&GroupError},pGroupID).
      return ?. 
    end.
  
  CreateLog("Premium for excess amount " + trim(string(deExcessAmountPremium, "$>>>,>>>,>>9.99")) + " - " + trim(string(deOrigAmountPremium, "$>>>,>>>,>>9.99")) + " = " + trim(string( deExcessAmountPremium - deOrigAmountPremium, "$>>>,>>>,>>9.99"))
            ,trim(if (deExcessAmountPremium - deOrigAmountPremium) >= 0  then trim(string((deExcessAmountPremium - deOrigAmountPremium), ">>>,>>>,>>9.99")) else trim(string((deExcessAmountPremium - deOrigAmountPremium), ">>>,>>>,>>9.99")))
            ,iCardID
            ,{&excess}
            ,pCaseType
            ,{&PremiumResult}
            ,pGroupID).
  
  return dePremium.   /* Function return value. */
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createLog) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createLog Procedure 
FUNCTION createLog RETURNS LOGICAL
  ( pNote as character , pActionCalculation as character,  pCardID as integer, pTypeAmount as character, pTypeCase as character, pCalculationOn as character, pGroupID as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define buffer ratelog for ratelog.
 
  iLogSeq = iLogSeq + 1.
  
  create ratelog.
  assign
      ratelog.seq               = iLogSeq
      ratelog.action            = pNote
      rateLog.actionCalculation = pActionCalculation
      ratelog.cardID            = pCardID
      ratelog.typeofamount      = pTypeAmount
      ratelog.calculationFor    = pTypeCase
      ratelog.calculationOn     = pCalculationOn
      ratelog.groupID           = pGroupID
      .
 
  return true.   /* Function return value. */
 
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

