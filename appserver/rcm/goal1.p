
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable pbatchid as int.
define variable pyearStartRange as int.
define variable pyearEndRange as int.
define variable pseq as int.

define temp-table calcParameters
fields scenarioID as int
fields scenarioName as char
fields version as char
fields region as char
fields propertyType as char
fields simultaneous as char
fields coverageAmount as char
fields ratetype as char
fields reissueCoverageAmount as char
fields PriorEffectiveDate as char
fields ownerEndors as char
fields loanCoverageAmount as char
fields loanRateType as char
fields loanReissueCoverageAmount as char
fields loanPriorEffectiveDate as char
fields loanEndors as char
fields lenderCPL as char
fields buyerCPL as char
fields sellerCPL as char
fields ratetypecode as char
fields loanRateTypecode as char
fields secondloanRateType as char
fields secondloanCoverageAmount as char
fields secondLoanEndors as char
.


define temp-table calcOutputParameters
fields scenarioID as int
fields scenarioName as char
fields ttpremiumOwner as char
fields ttpremiumLoan as char
fields ttpremiumOEndors as char
fields ttpremiumLEndors as char
fields ttpremiumOEndorsDetail as char
fields ttpremiumLEndorsDetail as char
fields ttpremiumScnd as char
fields ttpremiumSEndors as char
fields ttpremiumSEndorsDetail as char
fields ttpremiumLenderCPL as char
fields ttpremiumBuyerCPL as char
fields ttpremiumSellerCPL as char
fields ttsuccess as char
fields tteffectiveDate as char
fields ttlenderCPL as char
fields ttbuyerCPL as char
fields ttsellerCPL as char
fields ttratetypecode as char
fields ttloanRateTypecode as char
fields ttsecondloanRateType as char
fields ttsecondloanCoverageAmount as char
fields ttsecondLoanEndors as char
.
define buffer bufbatchform for batchform.


pRequest:getParameter("BatchID", output pbatchid).
pRequest:getParameter("Seq", output pseq).
pRequest:getParameter("YearStartRange", output pyearStartRange).
pRequest:getParameter("YearEndRange", output pyearEndRange).


for each  bufbatchform no-lock where 
                               bufbatchform.batchid = (if pbatchid = 0 then bufbatchform.batchid  else pbatchid ) and
                               bufbatchform.seq = (if pseq = 0 then bufbatchform.seq  else pseq )and 
                               bufbatchform.formtype = "p":

/*   will update later
   iCountofpolicy = 0.
 for each batchform where batchform.formtype = "p" and batchform.batchid = bufbatchform.batchid:
   iCountofpolicy = iCountofpolicy + 1.
 end.
 
 /*general cases*/
 
  if iCountofpolicy = 1 and bufbatchform.formid = "O" then  */
  
  if  bufbatchform.formid = "O" then
  do:
     /*owner scenarios*/
     /* 1. Owner Orignal*/
     create calcParameters.
     assign 
           calcParameters.scenarioID                = 1 
           calcParameters.scenarioName              = "Residential Owner Original"
           calcParameters.version                   = "1"
           calcParameters.region                    = "Area1"
           calcParameters.propertyType              = "R"
           calcParameters.simultaneous              = "no"
           calcParameters.coverageAmount            = string(bufbatchform.liabilityAmount)
           calcParameters.ratetype                  = "O"
           calcParameters.reissueCoverageAmount     = "reissueCoverageAmount"
           calcParameters.PriorEffectiveDate        = "PriorEffectiveDate"
           calcParameters.ownerEndors               = "ownerEndors"
           calcParameters.loanCoverageAmount        = "loanCoverageAmount"
           calcParameters.loanRateType              = "loanRateType"
           calcParameters.loanReissueCoverageAmount = "loanReissueCoverageAmount"
           calcParameters.loanPriorEffectiveDate    = "loanPriorEffectiveDate"
           calcParameters.loanEndors                = "loanEndors"
           calcParameters.lenderCPL                 = "lenderCPL"
           calcParameters.buyerCPL                  = "buyerCPL"
           calcParameters.sellerCPL                 = "sellerCPL"
           calcParameters.ratetypecode              = "ratetypecode"
           calcParameters.loanRateTypecode          = "loanRateTypecode"
           calcParameters.secondloanRateType        = "secondloanRateType"
           calcParameters.secondloanCoverageAmount  = "secondloanCoverageAmount"
           calcParameters.secondLoanEndors          = "secondLoanEndors" 
           .
     /* 2. Owner Shortterm in daterange 1 to 3 Years*/
     create calcParameters.
     assign 
           calcParameters.scenarioID                = 2
           calcParameters.scenarioName              = "Residential Owner Short Term wirthin 1 to 3 Years"
           calcParameters.version                   = "1"
           calcParameters.region                    = "Area1"
           calcParameters.propertyType              = "R"
           calcParameters.simultaneous              = "no"
           calcParameters.coverageAmount            = string(bufbatchform.liabilityAmount)
           calcParameters.ratetype                  = "ST"
           calcParameters.reissueCoverageAmount     = "reissueCoverageAmount"
           calcParameters.PriorEffectiveDate        = string(date(1,1,2018))
           calcParameters.ownerEndors               = "ownerEndors"
           calcParameters.loanCoverageAmount        = "loanCoverageAmount"
           calcParameters.loanRateType              = "loanRateType"
           calcParameters.loanReissueCoverageAmount = "loanReissueCoverageAmount"
           calcParameters.loanPriorEffectiveDate    = "loanPriorEffectiveDate"
           calcParameters.loanEndors                = "loanEndors"
           calcParameters.lenderCPL                 = "lenderCPL"
           calcParameters.buyerCPL                  = "buyerCPL"
           calcParameters.sellerCPL                 = "sellerCPL"
           calcParameters.ratetypecode              = "ratetypecode"
           calcParameters.loanRateTypecode          = "loanRateTypecode"
           calcParameters.secondloanRateType        = "secondloanRateType"
           calcParameters.secondloanCoverageAmount  = "secondloanCoverageAmount"
           calcParameters.secondLoanEndors          = "secondLoanEndors" 
           .
           
     /* 3. Owner Shortterm in daterange 4 to 6 Years */
     create calcParameters.
     assign 
           calcParameters.scenarioID                = 3
           calcParameters.scenarioName              = "Residential Owner Short Term wirthin 4 to 6 Years"
           calcParameters.version                   = "1"
           calcParameters.region                    = "Area1"
           calcParameters.propertyType              = "R"
           calcParameters.simultaneous              = "no"
           calcParameters.coverageAmount            = string(bufbatchform.liabilityAmount)
           calcParameters.ratetype                  = "ST"
           calcParameters.reissueCoverageAmount     = "reissueCoverageAmount"
           calcParameters.PriorEffectiveDate        = string(date(1,1,2016))
           calcParameters.ownerEndors               = "ownerEndors"
           calcParameters.loanCoverageAmount        = "loanCoverageAmount"
           calcParameters.loanRateType              = "loanRateType"
           calcParameters.loanReissueCoverageAmount = "loanReissueCoverageAmount"
           calcParameters.loanPriorEffectiveDate    = "loanPriorEffectiveDate"
           calcParameters.loanEndors                = "loanEndors"
           calcParameters.lenderCPL                 = "lenderCPL"
           calcParameters.buyerCPL                  = "buyerCPL"
           calcParameters.sellerCPL                 = "sellerCPL"
           calcParameters.ratetypecode              = "ratetypecode"
           calcParameters.loanRateTypecode          = "loanRateTypecode"
           calcParameters.secondloanRateType        = "secondloanRateType"
           calcParameters.secondloanCoverageAmount  = "secondloanCoverageAmount"
           calcParameters.secondLoanEndors          = "secondLoanEndors" 
           .
   
   
     /* 3. Owner Shortterm in daterange more then 6 Years */
     create calcParameters.
     assign
           calcParameters.scenarioID                = 4
           calcParameters.scenarioName              = "Residential Owner Short Term more then 6 Years"
           calcParameters.version                   = "1"
           calcParameters.region                    = "Area1"
           calcParameters.propertyType              = "R"
           calcParameters.simultaneous              = "no"
           calcParameters.coverageAmount            = string(bufbatchform.liabilityAmount)
           calcParameters.ratetype                  = "ST"
           calcParameters.reissueCoverageAmount     = "reissueCoverageAmount"
           calcParameters.PriorEffectiveDate        = string(date(1,1,2001))
           calcParameters.ownerEndors               = "ownerEndors"
           calcParameters.loanCoverageAmount        = "loanCoverageAmount"
           calcParameters.loanRateType              = "loanRateType"
           calcParameters.loanReissueCoverageAmount = "loanReissueCoverageAmount"
           calcParameters.loanPriorEffectiveDate    = "loanPriorEffectiveDate"
           calcParameters.loanEndors                = "loanEndors"
           calcParameters.lenderCPL                 = "lenderCPL"
           calcParameters.buyerCPL                  = "buyerCPL"
           calcParameters.sellerCPL                 = "sellerCPL"
           calcParameters.ratetypecode              = "ratetypecode"
           calcParameters.loanRateTypecode          = "loanRateTypecode"
           calcParameters.secondloanRateType        = "secondloanRateType"
           calcParameters.secondloanCoverageAmount  = "secondloanCoverageAmount"
           calcParameters.secondLoanEndors          = "secondLoanEndors" 
           .  
  
   end.
  
end.


run rcm\Goal2.p(input table calcParameters, 
                output table calcOutputParameters).
                
                
pResponse:setParameter("calcParameters", table calcParameters).
pResponse:setParameter("calcOutputParameters", table calcOutputParameters).
 
 
 

