&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : rcm/kansas.p
    Purpose     : State specific rate calculator

    Syntax      :

    Description :

    Author(s)   : Anubha Jain
    Created     : 08/13/2019
    Notes       :
    Modification :
    Date        Name    Comments
    10/15/2019  Anubha  Added logic for Rate Scenario 
    10/17/2019  Anubha  Modified code for Rate Scenario 
    10/24/2019  Anubha  Modified code for Rate Scenario 
    11/01/2019  Anubha  Modified code for Rate Scenario 
    12/13/2019  Anubha  Fixed testing issues 
    12/24/2019  Anubha  Fixed testing issues 
    01/06/2020  Anubha  Fixed testing issue
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib\std-def.i}
{lib\rcm-std-def.i}
{lib\nadef.i}
{tt\ratelog.i}
{tt\ratelog.i &tablealias=ttTempRateLog}

define input parameter            parameters     as character no-undo.

define output parameter           premiumOutput  as character no-undo.
define output parameter           debugparameter as character no-undo.
define output parameter table for rateLog.

/*------------------debuging-------------------*/
debugparameter =  "1.parameters in definition=" + parameters.
/*----------------------------------------------*/


/*-----------------------UI codes for RateType---------------------------------*/   
&scoped-define UiBuilder                  "B"
&scoped-define UiDeveloper                "D"
&scoped-define UiHomeowners               "H"
&scoped-define UiJuniorLoanCoverage       "JR"
&scoped-define UiLoanOriginal             "LO"
&scoped-define UiLoanSubstitution         "SL"
&scoped-define UiOriginal                 "O"
&scoped-define UiReissue                  "Re"
&scoped-define UiSecondLoan               "SM"

/*-------------------------------Scenarios----------------------------------------*/
&scoped-define Loan                              "L"
&scoped-define LoanReissue                       "LR"
&scoped-define LoanReissueScndLoan               "LRSL"
&scoped-define LoanReissueScndLoanExcess         "LRSLEx"
&scoped-define LoanScndLoan                      "LSL"
&scoped-define LoanScndLoanExcess                "LSLEx"
&scoped-define Owner                             "O"
&scoped-define OwnerLoan                         "OL"
&scoped-define OwnerJuniorLoan                   "OJrL"
&scoped-define OwnerLoanScndLoan                 "OLSL"
&scoped-define OwnerLoanJuniorScndLoan           "OLJrSL"
&scoped-define OwnersReissue                     "OR"
&scoped-define OwnerReissueLoan                  "ORL"
&scoped-define OwnerReissueLoanScndLoan          "ORLSL"
&scoped-define OwnerReissueLoanJuniorScndLoan    "ORLJrSL"

/*-----------------------------Card names-----------------------------------------*/
&scoped-define Builder                    "Builder"
&scoped-define Developer                  "Developer"
&scoped-define Homeowner                  "HomeOwnersOriginal"
&scoped-define JuniorLoan                 "LoanJrLimited"  
&scoped-define LoanOriginal               "LoanOriginal"
&scoped-define LoanReissue                "LoanReissue"
&scoped-define LoanReissueExcess          "LoanReissueExcess"
&scoped-define LoanSubstitution           "LoanSubstitution"
&scoped-define OwnersOriginal             "OwnersOriginal"
&scoped-define OwnerReissue               "OwnersReissue"
&scoped-define OwnerReissueExcess         "OwnersReissueExcess"
&scoped-define SimoBuilder                "SimoBuilder"
&scoped-define SimoBuilderExcess          "SimoBuilderExcess"
&scoped-define SimoDeveloper              "SimoDeveloper"
&scoped-define SimoLoan                   "SimoLoan"
&scoped-define SimoLoanExcess             "SimoLoanExcess"
&scoped-define SimoLoanJrExcess           "SimoLoanJrExcess"
&scoped-define SimoSecondLoan             "SimoSecondLoan"
&scoped-define SimoSecondLoanExcess       "SimoSecondLoanExcess"
&scoped-define SimoSecondLoanJrExcess     "SimoSecondLoanJrExcess"

/*------------------------------Attribute-----------------------------------------*/
&scoped-define PostCalcAdjust             "PostCalcAdjust"

/*---Parameters - parse the input parameter and store the information into these variables.---*/
define variable rateType                  as character no-undo initial "rateType".
define variable coverageAmount            as character no-undo initial "coverageAmount".                    
define variable reissueCoverageAmount     as character no-undo initial "reissueCoverageAmount".
define variable loanRateType              as character no-undo initial "loanRateType".
define variable loanCoverageAmount        as character no-undo initial "loanCoverageAmount".
define variable loanReissueCoverageAmount as character no-undo initial "loanReissueCoverageAmount". 
define variable secondLoanRateType        as character no-undo initial "secondLoanRateType".
define variable secondLoanCoverageAmount  as character no-undo initial "secondLoanCoverageAmount".  
define variable ownerEndors               as character no-undo initial "ownerEndors".
define variable loanEndors                as character no-undo initial "loanEndors".
define variable secondLoanEndors          as character no-undo initial "secondLoanEndors".
define variable propertyType              as character no-undo initial "propertyType".
define variable Version                   as character no-undo initial "Version".
define variable simultaneous              as character no-undo initial "simultaneous".                                                                                

/*--------------------derived variables based on input parameters-----------------------------*/
define variable iCovgAmt                  as integer   no-undo.
define variable iReissueCovgAmt           as integer   no-undo.
define variable iLoanCovgAmt              as integer   no-undo.
define variable iLoanReissueCovgAmt       as integer   no-undo.
define variable iSecondLoanCovgAmt        as integer   no-undo.
define variable iVersion                  as integer   no-undo.
define variable lSimultaneous             as logical   no-undo.

/*-------------------------------basic variables--------------------------------------------*/
define variable cScenarioCode             as character no-undo.
define variable cScenarioName             as character no-undo.
define variable cScenarioDesc             as character no-undo.
define variable cCardAttrib               as character no-undo.
define variable cEffectiveDate            as character no-undo.
define variable region                    as character no-undo.
define variable iLogSeq                   as integer   no-undo.
define variable iCardSetID                as integer   no-undo.
define variable cScenarioMsg              as character no-undo.

/*----------------------------variables to create output parameter--------------------------*/
define variable cPremiumOEndorsDetail     as character no-undo. 
define variable cPremiumLEndorsDetail     as character no-undo.
define variable cPremiumSEndorsDetail     as character no-undo.
define variable cErrorMsg                 as character no-undo.
define variable dePremiumOwner            as decimal   no-undo.   
define variable dePremiumLoan             as decimal   no-undo.  
define variable dePremiumScnd             as decimal   no-undo.
define variable dePremiumOEndors          as decimal   no-undo.          
define variable dePremiumLEndors          as decimal   no-undo.
define variable dePremiumSEndors          as decimal   no-undo.
define variable lErrorStatus              as logical   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-CalculateBasicPremium) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD CalculateBasicPremium Procedure 
FUNCTION CalculateBasicPremium RETURNS DECIMAL
  (  pCardName as character ,pAttrID as character , pAmount as decimal, pCaseType as character, pGroupID as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CalculateEndors) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD CalculateEndors Procedure 
FUNCTION CalculateEndors RETURNS decimal
  ( pAmount as integer , pGroupName as character,pCaseType as character,pGroupID as integer , pSelectedEndorsement as character, pPropertyType as character,output premiumEndorsDetail as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CalculateExcessPremium) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD CalculateExcessPremium Procedure 
FUNCTION CalculateExcessPremium RETURNS DECIMAL
  (pCardName as character ,pAttrID as character , pExcessAmount as integer, pAmount as integer, pCaseType as character, pGroupID as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CreateLog) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD CreateLog Procedure 
FUNCTION CreateLog RETURNS LOGICAL
  ( pNote as character , pActionCalculation as character,  pCardID as integer, pTypeAmount as character, pTypeCase as character, pCalculationOn as character, pGroupID as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 18.29
         WIDTH              = 55.6.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/*------------------debugging-------------------*/
debugparameter = debugparameter + "2.enter in main block".
/*----------------------------------------------*/
                         
/*----------fetch user input values------------*/
run ExtractParameters.

/*---check UI provided version number- if it's zero then find an active version of this state else specified version---*/
if iVersion = 0 
 then 
  for first rateState  where rateState.stateID = {&Kansas}
                         and rateState.active  = true:
    assign
        iCardSetID     =  rateState.cardSetID.
        cEffectiveDate =  if rateState.effectiveDate = ? then "" else string(rateState.effectiveDate)
        .
  end.
 else
  for first rateState  where rateState.stateID = {&Kansas}
                         and rateState.version = iVersion:
    assign
        iCardSetID     =  rateState.cardSetID.
        cEffectiveDate =  if rateState.effectiveDate = ? then "" else string(rateState.effectiveDate)
        .
  end.
    
/*------------------debugging-------------------*/
debugparameter = debugparameter + "24.cardSetID="       + string(iCardSetID) 
                                + "25.effectiveDate="   + cEffectiveDate.    
/*----------------------------------------------*/  
  
if iCardSetID = 0 
 then 
  do:
     CreateLog("No Such RateState record found." ,"", 0, "", "",{&GroupError},0).
     premiumOutput = "servererrmsg="         + "No Such RateState record found for version " + string(iVersion) + " ." +
                     ",success="             + "N"                                                                     +
                     ",cardSetID="           + string(iCardSetID)                                                      +
                     ",manualeffectiveDate=" + cEffectiveDate.
     return.
  end.

/*-----Necessary server validations and validation of user input-----*/
run ServerValidations(output lErrorStatus,
                      output cErrorMsg).

if lErrorStatus
 then
  do:
     premiumOutput = "servererrmsg="         + cErrorMsg          +                  
                     ",success="             + "N"                +
                     ",cardSetID="           + string(iCardSetID) +
                     ",manualeffectiveDate=" + cEffectiveDate.
     return.
  end.

/*------------------debugging-------------------*/
debugparameter = debugparameter + "26.after server validations" .    
/*----------------------------------------------*/ 

/*----------Decide the scenario to execute------------*/
run DecideScenario.


/*-----Validating policy amount----*/
run validatePolicyAmount (output lErrorStatus,
                          output cErrorMsg).
                          
 /*------------------debugging-------------------*/
debugparameter = debugparameter + "30.lErrorStatus="  + string(lErrorStatus) 
                                + "31.cErrorMsg="     + cErrorMsg.    
/*----------------------------------------------*/ 

if lErrorStatus
 then
  do:
     premiumOutput = "servererrmsg="         + cErrorMsg          +                  
                     ",success="             + "N"                +
                     ",cardSetID="           + string(iCardSetID) +
                     ",manualeffectiveDate=" + cEffectiveDate.
     return.
  end.

/*------------------debugging-------------------*/
debugparameter = debugparameter + "32.after amount validations" . 
/*----------------------------------------------*/ 

/*-----------Execute the scenario------------------*/

case cScenarioCode:

 when {&Residential}{&Owner} /* R"O */
  then
   run ResiOwner(rateType,iCovgAmt,
                 output dePremiumOwner).  

                 
 when {&Residential}{&Loan} /* R"L */
  then
   run ResiLoan(loanRateType,iLoanCovgAmt,
                output dePremiumLoan).
 
 
 when {&Residential}{&OwnerLoan} /* R"OL */
  then
   run ResiOwnerLoan(rateType,loanRateType,iCovgAmt,iLoanCovgAmt,
                     output dePremiumOwner,
                     output dePremiumLoan).
                     
 when {&Residential}{&OwnerJuniorLoan} /* R"OJrL */
  then
   run ResiOwnerJrLoan(rateType,loanRateType,iCovgAmt,iLoanCovgAmt,
                       output dePremiumOwner,
                       output dePremiumLoan).

 when {&Residential}{&OwnerLoanScndLoan} /* R"OLSL */
  then
   run ResiOwnerLoanScndLoan(rateType,loanRateType,secondLoanRateType,iCovgAmt,iLoanCovgAmt,iSecondLoanCovgAmt,                                                  
                             output dePremiumOwner,                                                                                                            
                             output dePremiumLoan,                                                                                                             
                             output dePremiumScnd). 
 
 when {&Residential}{&OwnerLoanJuniorScndLoan} /* R"OLJrSL */
  then
   run ResiOwnerLoanJrScndLoan(rateType,loanRateType,secondLoanRateType,iCovgAmt,iLoanCovgAmt,iSecondLoanCovgAmt,                                                  
                               output dePremiumOwner,                                                                                                            
                               output dePremiumLoan,                                                                                                             
                               output dePremiumScnd). 
      
 when {&Residential}{&OwnersReissue} /* R"OR */
  then
   run ResiOwnerReissue(rateType,iCovgAmt,iReissueCovgAmt,
                        output dePremiumOwner).
                      
                      
 when {&Residential}{&LoanReissue} /* R"LR*/
  then
   run ResiLoanReissue(loanRateType,iLoanCovgAmt,iLoanReissueCovgAmt,
                       output dePremiumLoan).
      
      
 when {&Residential}{&OwnerReissueLoan} /* R"ORL */ 
  then
   run ResiOwnerReissueLoan(rateType,loanRateType,iCovgAmt,iReissueCovgAmt,iLoanCovgAmt,
                            output dePremiumOwner,
                            output dePremiumLoan).
           
           
 when {&Residential}{&OwnerReissueLoanScndLoan} /* R"ORLSL */ 
  then                                                                                                          
   run ResiOwnerReissueLoanScndLoan(rateType,loanRateType,secondLoanRateType,iCovgAmt,iReissueCovgAmt,iLoanCovgAmt,iSecondLoanCovgAmt,
                                    output dePremiumOwner,                                                                                                             
                                    output dePremiumLoan,  
                                    output dePremiumScnd).
 
 when {&Residential}{&OwnerReissueLoanJuniorScndLoan} /* R"ORLJrSL */ 
  then                                                                                                          
   run ResiOwnerReissueLoanJrScndLoan(rateType,loanRateType,secondLoanRateType,iCovgAmt,iReissueCovgAmt,iLoanCovgAmt,iSecondLoanCovgAmt,
                                          output dePremiumOwner,                                                                                                             
                                          output dePremiumLoan,  
                                          output dePremiumScnd).
           
 when {&Residential}{&LoanScndLoan} /* R"LSL */   
  then 
   run ResiLoanScndLoan(loanRateType,secondLoanRateType,iLoanCovgAmt,iSecondLoanCovgAmt,                                                                                                            
                        output dePremiumLoan,  
                        output dePremiumScnd). 
          
          
 when {&Residential}{&LoanReissueScndLoan} /* R"LRSL */           
  then                                      
   run ResiLoanReissueScndLoan(loanRateType,secondLoanRateType,iLoanCovgAmt,iLoanReissueCovgAmt,iSecondLoanCovgAmt,                                                                                                            
                               output dePremiumLoan,  
                               output dePremiumScnd).             
                  
                  
 when {&Residential}{&LoanScndLoanExcess} /* R"LSLEx*/   
  then 
   run ResiLoanScndLoanExcess(loanRateType,secondLoanRateType,iLoanCovgAmt,iSecondLoanCovgAmt,                                                                                                            
                              output dePremiumLoan,  
                              output dePremiumScnd). 
                           
                           
 when {&Residential}{&LoanReissueScndLoanExcess} /* R"LRSLEx*/           
  then                                      
   run ResiLoanReissueScndLoanExcess(loanRateType,secondLoanRateType,iLoanCovgAmt,iLoanReissueCovgAmt,iSecondLoanCovgAmt,                                                                                                            
                                     output dePremiumLoan,  
                                     output dePremiumScnd).   

                                     
 when {&UiBuilder}{&Owner} /* B"O */           
  then                                        
   run BuilderOwner(rateType,iCovgAmt, 
                    output dePremiumOwner). 
                    
                    
 when {&UiBuilder}{&Loan} /* B"L */           
  then                                      
   run BuilderLoan(loanRateType,iLoanCovgAmt,   
                   output dePremiumLoan). 
                
                 
 when {&UiBuilder}{&OwnerLoan} /* B"OL */             
  then                                        
   run BuilderOwnerLoan(rateType,loanRateType,iCovgAmt,iLoanCovgAmt,
                        output dePremiumOwner,
                        output dePremiumLoan).
                        
                        
 when {&UiDeveloper}{&Owner} /* D"O */           
  then                                      
   run DeveloperOwner(rateType,iCovgAmt,   
                      output dePremiumOwner). 
                      
                      
 when {&UiDeveloper}{&Loan} /* D"L */           
  then                                        
   run DeveloperLoan(loanRateType,iLoanCovgAmt, 
                     output dePremiumLoan).   
                     
                     
 when {&UiDeveloper}{&OwnerLoan} /* D"OL */           
  then                                      
   run DeveloperOwnerLoan(rateType,loanRateType,iCovgAmt,iLoanCovgAmt,
                          output dePremiumOwner,
                          output dePremiumLoan).
 otherwise                                                                                                                                   
  do:                                                                                                                                        
     premiumOutput = "servererrmsg="           + "Scenario not defined for given input" +                                                         
                     ",success="               + "N"                                    +
                     ",cardSetID="             + string(iCardSetID)                     +
                     ",manualeffectiveDate="   + cEffectiveDate.                                                                
     return.
  end.
  
end case.

if cScenarioName <> ""
 then
  publish "doScenario" (input  iCardSetID,
                        input  cScenarioName,
                        input  cScenarioDesc,
                        input  region,       
                        input  propertyType,
                        input  cCardAttrib,
                        output cScenarioMsg).

  
/*------------------debugging-------------------*/
debugparameter = debugparameter + "33.cScenarioName="    + cScenarioName
                                + "34.cScenarioDesc="    + cScenarioDesc
                                + "35.iCardSetID="       + string(iCardSetID)
                                + "36.cCardAttrib="      + cCardAttrib
                                + "37.cScenarioMsg="     + cScenarioMsg
                                .
/*----------------------------------------------*/
  
/*-----------------------------endorsement calculation------------------------------*/
if ownerEndors ne "" 
 then
  dePremiumOEndors        = CalculateEndors(iCovgAmt,{&OwnersEndors},{&Owners},1,ownerEndors,propertyType,output cPremiumOEndorsDetail).
  
if loanEndors ne "" 
 then
  dePremiumLEndors        = CalculateEndors(iLoanCovgAmt,{&LendersEndors},{&Lenders},2,loanEndors,propertyType,output cPremiumLEndorsDetail).
  
if secondLoanEndors ne "" 
 then
  dePremiumSEndors        = CalculateEndors(iSecondLoanCovgAmt,{&SecondLendersEndors},{&SecondLenders},3,secondLoanEndors,propertyType,output cPremiumSEndorsDetail).
 
if dePremiumOwner   = ?   or dePremiumLoan    = ?  or dePremiumScnd = ?  or 
   dePremiumOEndors = ?   or dePremiumLEndors = ?  or dePremiumSEndors = ?    
 then
  assign
      dePremiumOwner     = 0 
      dePremiumLoan      = 0
      dePremiumScnd      = 0
      dePremiumOEndors   = 0
      dePremiumLEndors   = 0 
      dePremiumSEndors   = 0
      .
      
CreateLog("Total premium amount " + trim(trim((if dePremiumOwner      ne 0 then           trim(string(dePremiumOwner,"$>>>,>>>,>>9.99"))     else "")
                                            + (if dePremiumOEndors    ne 0 then  " + " +  trim(string(dePremiumOEndors,"$>>>,>>>,>>9.99"))   else "")
                                            + (if dePremiumLoan       ne 0 then  " + " +  trim(string(dePremiumLoan,"$>>>,>>>,>>9.99"))      else "")
                                            + (if dePremiumLEndors    ne 0 then  " + " +  trim(string(dePremiumLEndors,"$>>>,>>>,>>9.99"))   else "")
                                            + (if dePremiumScnd       ne 0 then  " + " +  trim(string(dePremiumScnd,"$>>>,>>>,>>9.99"))      else "")
                                            + (if dePremiumSEndors    ne 0 then  " + " +  trim(string(dePremiumSEndors,"$>>>,>>>,>>9.99"))   else "")," "),"+")
                                            + " = " + trim(string(dePremiumOwner + dePremiumLoan + dePremiumOEndors + dePremiumLEndors + dePremiumScnd  + dePremiumSEndors, "$>>>,>>>,>>9.99")),
                                              string(dePremiumOwner + dePremiumLoan  + dePremiumOEndors + dePremiumLEndors + dePremiumScnd  + dePremiumSEndors , ">>>,>>>,>>9.99"), 0,"",{&GrandTotal},{&GrandTotal} ,0).
  
/*------------------------Set OUTPUT parameter-----------------------------*/
premiumOutput = "premiumOwner="            + string(dePremiumOwner)         + 
                ",premiumLoan="            + string(dePremiumLoan)          + 
                ",premiumOEndors="         + string(dePremiumOEndors)       + 
                ",premiumLEndors="         + string(dePremiumLEndors)       + 
                ",premiumOEndorsDetail="   + string(cPremiumOEndorsDetail)  + 
                ",premiumLEndorsDetail="   + string(cPremiumLEndorsDetail)  + 
                ",premiumScnd="            + string(dePremiumScnd)          + 
                ",premiumSEndors="         + string(dePremiumSEndors)       +
                ",premiumSEndorsDetail="   + string(cPremiumSEndorsDetail)  +
                ",success="                + "y"                            +
                ",cardSetID="              + string(iCardSetID)             +
                ",manualeffectiveDate="    + string(cEffectiveDate).

/*------------------debugging-------------------*/
debugparameter = debugparameter + "38.premiumOutput=" + premiumOutput .    
/*----------------------------------------------*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-BuilderLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE BuilderLoan Procedure 
PROCEDURE BuilderLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcRateType as character no-undo.
  define input  parameter ipiAmount   as integer   no-undo.
  define output parameter opdePremium as decimal   no-undo.

  define variable cCardName           as character no-undo.
  define variable cAttribute          as character no-undo.
  
  {lib/nacard.i &rateType = ipcRateType , &cardName = cCardName , &attrID = cAttribute}   
  
  CreateLog("Scenario Executed: Builder Loan","", 0, "",{&Scenario},{&Scenario},0 ).   

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcRateType:
   when {&UiBuilder} 
    then
     assign
         cCardName      = {&Builder}
         cAttribute     = ""
         cScenarioName  = "Builder Loan"
         cScenarioDesc  = "Builder/Developer insures interest in subdivision commercial property."
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremium = ?.
       return.
    end.
  end case.
  
  opdePremium = CalculateBasicPremium(cCardName,cAttribute,ipiAmount,{&LendersPolicy},1). 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-BuilderOwner) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE BuilderOwner Procedure 
PROCEDURE BuilderOwner :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcRateType  as character no-undo.
  define input  parameter ipiAmount    as integer   no-undo.
  define output parameter opdePremium  as decimal   no-undo.

  define variable cCardName            as character no-undo.
  define variable cAttribute           as character no-undo.
    
  {lib/nacard.i &rateType = ipcRateType , &cardName = cCardName , &attrID = cAttribute}   
  
  CreateLog("Scenario Executed: Builder Owner","", 0, "",{&Scenario},{&Scenario},0 ).   

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcRateType:
   when {&UiBuilder} 
    then
     assign
         cCardName       = {&Builder}
         cAttribute      = ""
         cScenarioName   = "Builder Owner"
         cScenarioDesc   = "Builder/Developer insures interest in subdivision commercial property"
         .

   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcRateType + ".","", 0, "","",{&CardSelectionError},0 ) .  
       opdePremium = ?.
       return.
    end.
  end case.
  
  opdePremium = CalculateBasicPremium(cCardName,cAttribute,ipiAmount,{&OwnersPolicy},1). 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-BuilderOwnerLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE BuilderOwnerLoan Procedure 
PROCEDURE BuilderOwnerLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcOwnerRateType as character no-undo.
  define input  parameter ipcLoanRateType  as character no-undo.
  define input  parameter ipiOwnerAmount   as integer   no-undo.
  define input  parameter ipiLoanAmount    as integer   no-undo.
  define output parameter opdePremiumOwner as decimal   no-undo.
  define output parameter opdePremiumLoan  as decimal   no-undo.

  define variable cOwnerCardName           as character no-undo.
  define variable cSimoCardName            as character no-undo.
  define variable cExcessCardName          as character no-undo.
  define variable cOwnerAttribute          as character no-undo.
  define variable cLoanAttribute           as character no-undo.
  define variable dePremiumSimo            as decimal   no-undo.
  define variable dePremiumExcess          as decimal   no-undo.

  CreateLog("Scenario Executed: Builder Owner-Loan","", 0, "",{&Scenario},{&Scenario},0 ). 

  /* This is core business logic which identifies CARDS to be used in each scenario. */   
  case ipcOwnerRateType:
   when {&UiBuilder} 
    then
     assign
         cOwnerCardName  = {&Builder}
         cOwnerAttribute = ""
         cScenarioName   = "Builder Owner"
         cScenarioDesc   = "Simultaneous Issue of Builder Policy"
         . 
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcOwnerRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner = ?.
       opdePremiumLoan  = ?. 
       return.
    end.
  end case.

  case ipcLoanRateType:
   when {&UiBuilder} 
    then
     assign
         cSimoCardName        = {&SimoBuilder}
         cExcessCardName      = {&SimoBuilderExcess}
         cLoanAttribute       = ""
         cScenarioName        = cScenarioName + " - Loan"
         .     
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner = ?.
       opdePremiumLoan  = ?.  
       return.
    end.
  end case.

  opdePremiumOwner = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiOwnerAmount,{&OwnersPolicy},1). 

  if ipiLoanAmount > ipiOwnerAmount 
   then
    do:
       dePremiumSimo   = CalculateBasicPremium(cSimoCardName,cLoanAttribute,ipiOwnerAmount,{&LendersPolicy},2).
       dePremiumExcess = CalculateExcessPremium(cExcessCardName,cLoanAttribute,ipiLoanAmount,ipiOwnerAmount,{&LendersPolicy},2). 
    end.
   else
    dePremiumSimo = CalculateBasicPremium(cSimoCardName,cLoanAttribute,ipiLoanAmount,{&LendersPolicy},2). 

  opdePremiumLoan = dePremiumSimo + dePremiumExcess.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DecideScenario) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DecideScenario Procedure 
PROCEDURE DecideScenario :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  case propertyType:

   when {&Residential} 
    then
     do:
        if not lSimultaneous 
         then
          do:
             if  rateType = {&NAUI} or
                 (coverageAmount    ne "" and
                 rateType           ne "" and
                 loanCoverageAmount eq "")       
              then
               do:
                  if reissueCoverageAmount ne ""  
                   then
                    cScenarioCode = {&Residential}{&OwnersReissue}. /* R"OR */
                   else
                    cScenarioCode = {&Residential}{&Owner}. /* R"O */
               end. 
              else if loanRateType = {&NAUI} or
                      (loanCoverageAmount    ne "" and
                      loanRateType           ne "" and
                      coverageAmount         eq "")  
               then
                do:
                   if loanReissueCoverageAmount ne ""  
                    then
                     cScenarioCode = {&Residential}{&LoanReissue}. /* R"LR */
                    else
                     cScenarioCode = {&Residential}{&Loan}. /* R"L */
                end.  
          end.
         else 
          do:
             if coverageAmount eq ""
              then
               do:
                  if loanReissueCoverageAmount ne ""
                   then
                    do:
                       if secondLoanRateType = {&UiJuniorLoanCoverage}
                        then
                         cScenarioCode = {&Residential}{&LoanReissueScndLoan}. /* R"LRSL */
                        else
                         cScenarioCode = {&Residential}{&LoanReissueScndLoanExcess}. /* R"LRSLEx */
                    end.
                    else if secondLoanRateType = {&UiJuniorLoanCoverage}
                     then
                      cScenarioCode = {&Residential}{&LoanScndLoan}. /* R"LSL*/
                     else
                      cScenarioCode = {&Residential}{&LoanScndLoanExcess}. /* R"LSLEx */
               end.
              else if secondLoanCoverageAmount eq "" 
               then  
                do:
                   if reissueCoverageAmount ne "" 
                    then
                     cScenarioCode = {&Residential}{&OwnerReissueLoan}. /* R"ORL */
                    else                     
                     cScenarioCode = if loanRateType = {&UiJuniorLoanCoverage} then {&Residential}{&OwnerJuniorLoan} else {&Residential}{&OwnerLoan}. /* R"OJrL or R"OL */                  
                end.
               else 
                do:
                   if reissueCoverageAmount ne "" 
                    then
                     cScenarioCode =  if secondLoanRateType = {&UiJuniorLoanCoverage} 
                                         then {&Residential}{&OwnerReissueLoanJuniorScndLoan} else {&Residential}{&OwnerReissueLoanScndLoan}. /* R"ORLJrSL  or R"ORLSL */
                    else
                     cScenarioCode =  if secondLoanRateType = {&UiJuniorLoanCoverage} 
                                         then {&Residential}{&OwnerLoanJuniorScndLoan} else {&Residential}{&OwnerLoanScndLoan}. /* R"OLJrSL  or R"OLSL */  
                end.
          end.
     end.
               
   when {&UiBuilder}
    then
     do:
        if not lSimultaneous 
         then
          do:
             if  rateType = {&NAUI} or
                 (coverageAmount    ne "" and
                 rateType           ne "" and
                 loanCoverageAmount eq "")       
              then
               cScenarioCode = {&UiBuilder}{&Owner}. /* B"O */  
              else if loanRateType = {&NAUI} or
                      (loanCoverageAmount    ne "" and
                      loanRateType           ne "" and
                      coverageAmount         eq "")  
               then
                cScenarioCode = {&UiBuilder}{&Loan}. /* B"L */ 
          end.
         else
          cScenarioCode = {&UiBuilder}{&OwnerLoan}. /* B"OL */ 
     end.
      
   when {&UiDeveloper}
    then
     do:
        if not lSimultaneous 
         then
          do:
             if  rateType = {&NAUI} or
                 (coverageAmount    ne "" and
                 rateType           ne "" and
                 loanCoverageAmount eq "")       
              then
               cScenarioCode = {&UiDeveloper}{&Owner}. /* D"O */  
              else if loanRateType = {&NAUI} or
                      (loanCoverageAmount    ne "" and
                      loanRateType           ne "" and
                      coverageAmount         eq "")  
               then
                cScenarioCode = {&UiDeveloper}{&Loan}. /* D"L */ 
          end.
         else
          cScenarioCode = {&UiDeveloper}{&OwnerLoan}. /* D"OL */     
      end. 
   otherwise                                                                                                                                   
    do:                                                                                                                                        
       premiumOutput = "servererrmsg="           + "Scenario not defined for given input" +                                                         
                       ",success="               + "N"                                    +
                       ",cardSetID="             + string(iCardSetID)                     +
                       ",manualeffectiveDate="   + cEffectiveDate.                                                                
       return.
    end.
  end case.
  
  /*------------------debugging-------------------*/
  debugparameter = debugparameter + "27.scenario=" + cScenarioCode.    
 /*---------------------------------------------------*/ 
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeveloperLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeveloperLoan Procedure 
PROCEDURE DeveloperLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcRateType     as character no-undo.
  define input  parameter ipiAmount       as integer   no-undo.
  define output parameter opdePremium     as decimal   no-undo.

  define variable cCardName               as character no-undo.
  define variable cAttribute              as character no-undo.

  {lib/nacard.i &rateType = ipcRateType , &cardName = cCardName , &attrID = cAttribute}   
  
  CreateLog("Scenario Executed: Developer Loan","", 0, "",{&Scenario},{&Scenario},0 ).   

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcRateType:
   when {&UiDeveloper} 
    then
     assign
         cCardName      = {&Developer}
         cAttribute     = ""
         cScenarioName  = "Developer Loan"
         cScenarioDesc  = "Builder/Developer insures interest in subdivision commercial property."
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremium = ?.
       return.
    end.
  end case.
  
  opdePremium = CalculateBasicPremium(cCardName,cAttribute,ipiAmount,{&LendersPolicy},1). 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeveloperOwner) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeveloperOwner Procedure 
PROCEDURE DeveloperOwner :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcRateType      as character no-undo.
  define input  parameter ipiAmount        as integer   no-undo.
  define output parameter opdePremium      as decimal   no-undo.

  define variable cCardName                as character no-undo.
  define variable cAttribute               as character no-undo.
    
  {lib/nacard.i &rateType = ipcRateType , &cardName = cCardName , &attrID = cAttribute}   
  
  CreateLog("Scenario Executed: Developer Owner","", 0, "",{&Scenario},{&Scenario},0 ).   

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcRateType:
   when {&UiDeveloper} 
    then
     assign
         cCardName       = {&Developer}
         cAttribute      = ""
         cScenarioName  = "Developer Owner"
         cScenarioDesc  = "Builder/Developer insures interest in subdivision commercial property."
         .    

   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcRateType + ".","", 0, "","",{&CardSelectionError},0 ) .  
       opdePremium = ?.
       return.
    end.
  end case.
  
  opdePremium = CalculateBasicPremium(cCardName,cAttribute,ipiAmount,{&OwnersPolicy},1). 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeveloperOwnerLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeveloperOwnerLoan Procedure 
PROCEDURE DeveloperOwnerLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcOwnerRateType as character no-undo.
  define input  parameter ipcLoanRateType  as character no-undo.
  define input  parameter ipiOwnerAmount   as integer   no-undo.
  define input  parameter ipiLoanAmount    as integer   no-undo.
  define output parameter opdePremiumOwner as decimal   no-undo.
  define output parameter opdePremiumLoan  as decimal   no-undo.

  define variable cOwnerCardName           as character no-undo.
  define variable cSimoCardName            as character no-undo.
  define variable cOwnerAttribute          as character no-undo.
  define variable cSimoAttribute           as character no-undo.

  CreateLog("Scenario Executed: Developer Owner-Loan","", 0, "",{&Scenario},{&Scenario},0 ). 

  /* This is core business logic which identifies CARDS to be used in each scenario. */   
  case ipcOwnerRateType:
   when {&UiDeveloper} 
    then
     assign
         cOwnerCardName  = {&Developer}
         cOwnerAttribute = ""
         cScenarioName  = "Developer Owner"
         cScenarioDesc  = "Simultaneous Issue of Developer Policy"
         . 
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcOwnerRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner = ?.
       opdePremiumLoan  = ?.  
       return.
    end.
  end case.

  case ipcLoanRateType:
   when {&UiDeveloper} 
    then
     assign
         cSimoCardName  = {&SimoDeveloper}
         cSimoAttribute = ""
         cScenarioName  = cScenarioName + " - Loan"
         .    
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner = ?.
       opdePremiumLoan  = ?.  
       return.
    end.
  end case.

  opdePremiumOwner = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiOwnerAmount,{&OwnersPolicy},1). 
  opdePremiumLoan  = CalculateBasicPremium(cSimoCardName,cSimoAttribute,ipiLoanAmount,{&LendersPolicy},2). 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ExtractParameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExtractParameters Procedure 
PROCEDURE ExtractParameters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cErrorMsgs     as character no-undo.
  define variable iCntParameters as integer   no-undo.
  define variable iCntError      as integer   no-undo.
  
  /*-----------------------------input variables--------------------------------------*/
  do iCntParameters = 1 to num-entries(parameters,","):
  
   case entry(1,entry(iCntParameters,parameters,","),"^"):
    
    when rateType  
     then
      rateType                   = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
    when coverageAmount  
     then
      coverageAmount             = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
    when loanRateType
     then
      loanRateType               = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
    when loanCoverageAmount  
     then
      loanCoverageAmount         = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
        
    when reissueCoverageAmount 
     then
      reissueCoverageAmount      = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
    when loanReissueCoverageAmount 
     then
      loanReissueCoverageAmount  = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
    when secondLoanRateType  
     then
      secondLoanRateType         = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
    when secondLoanCoverageAmount  
     then
      secondLoanCoverageAmount   = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
    when ownerEndors  
     then
      ownerEndors                = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
    when loanEndors 
     then
      loanEndors                 = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
    when secondLoanEndors 
     then
      secondLoanEndors           = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
    when propertyType  
     then
      propertyType               = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
    when Version  
     then
      Version                    = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
      
    when simultaneous  
     then
      simultaneous               = entry(2,entry(iCntParameters,parameters,","),"^") no-error.
        
   end case.  
  end.
  
  /*------------------debugging-------------------*/
  debugparameter = debugparameter + "3.rateType="                  + rateType
                                  + "4.coverageAmount="            + coverageAmount            
                                  + "5.reissueCoverageAmount="     + reissueCoverageAmount     
                                  + "6.loanRateType="              + loanRateType              
                                  + "7.loanCoverageAmount="        + loanCoverageAmount        
                                  + "8.loanReissueCoverageAmount=" + loanReissueCoverageAmount 
                                  + "9.secondLoanRateType="        + secondLoanRateType        
                                  + "10.secondLoanCoverageAmount=" + secondLoanCoverageAmount  
                                  + "11.ownerEndors="              + ownerEndors               
                                  + "12.loanEndors="               + loanEndors                
                                  + "13.secondLoanEndors="         + secondLoanEndors                
                                  + "14.propertyType="             + propertyType              
                                  + "15.simultaneous="             + simultaneous
                                  + "16.version="                  + version.
  /*---------------------------------------------------*/

  if coverageAmount             eq "coverageAmount"               or coverageAmount               eq "0" 
   then
    coverageAmount = "".    
  if reissueCoverageAmount      eq "reissueCoverageAmount"        or reissueCoverageAmount        eq "0" 
   then
    reissueCoverageAmount = "".      
  if loanCoverageAmount         eq "loanCoverageAmount"           or loanCoverageAmount           eq "0" 
   then
    loanCoverageAmount = "".  
  if loanReissueCoverageAmount  eq "loanReissueCoverageAmount"    or loanReissueCoverageAmount    eq "0" 
   then
    loanReissueCoverageAmount = "". 
  if secondLoanCoverageAmount   eq "secondLoanCoverageAmount"     or secondLoanCoverageAmount     eq "0" 
   then
    secondLoanCoverageAmount = "".
  
  if rateType           eq "rateType"          
   then
    rateType = "".
  if loanRateType       eq "loanRateType"      
   then
    loanRateType = "".
  if secondLoanRateType eq "secondLoanRateType" 
   then
    secondLoanRateType = "".   
  if propertyType       eq "propertyType"     
   then
    propertyType = "".
  if ownerEndors        eq "ownerEndors"      
   then
    ownerEndors = "".
  if loanEndors         eq "loanEndors"       
   then
    loanEndors = "".
  if secondLoanEndors   eq "secondLoanEndors" 
   then
    secondLoanEndors = "".
  if Version            eq "Version"   or Version   eq "0" 
   then
    Version = "".
  
  /*---------------------------------------datatype Conversion-----------------------------------------*/
  assign
      iCovgAmt            = 0
      iLoanCovgAmt        = 0
      iSecondLoanCovgAmt  = 0
      iReissueCovgAmt     = 0
      iLoanReissueCovgAmt = 0
      iVersion            = 0
      .
  assign
      iCovgAmt                   = integer(coverageAmount)
      iReissueCovgAmt            = integer(reissueCoverageAmount)
      iLoanCovgAmt               = integer(loanCoverageAmount)
      iLoanReissueCovgAmt        = integer(loanReissueCoverageAmount)
      iSecondLoanCovgAmt         = integer(secondLoanCoverageAmount)
      iVersion                   = integer(Version)                    
      lSimultaneous              = logical(simultaneous) 
      no-error.
  
  if error-status:error                               
   then 
    do:
       if error-status:num-messages > 0 
        then
         do iCntError = 1 to error-status:num-messages:
          cErrorMsgs = cErrorMsgs + "::" + error-status:get-message(iCntError).
         end.
        else
         cErrorMsgs =  "Error occured in parsing.".         
       
       CreateLog(cErrorMsgs ,"", 0, "",{&GroupError},{&GroupError},0 ).  
    end.

  /*------------------debugging-------------------*/
  debugparameter = debugparameter + "17.covgAmt="                    + string(iCovgAmt)                  
                                  + "18.reissueCovgAmt="             + string(iReissueCovgAmt)
                                  + "19.loanCovgAmt="                + string(iLoanCovgAmt)  
                                  + "20.reissueloanCovgAmt="         + string(iLoanReissueCovgAmt) 
                                  + "21.secondloanCovgAmt="          + string(iSecondloanCovgAmt)        
                                  + "22.iVersion="                   + string(iVersion)                      
                                  + "23.lSimultaneous="              + string(lSimultaneous).
  /*---------------------------------------------------*/  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ResiLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResiLoan Procedure 
PROCEDURE ResiLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcRateType     as character no-undo.
  define input  parameter ipiAmount       as integer   no-undo.
  define output parameter opdePremium     as decimal   no-undo.

  define variable cCardName               as character no-undo.
  define variable cAttribute              as character no-undo.
                                         
  {lib/nacard.i &rateType = ipcRateType , &cardName = cCardName , &attrID = cAttribute}   
  
  CreateLog("Scenario Executed: Residential Loan","", 0, "",{&Scenario},{&Scenario},0 ).   

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcRateType:
   when {&UiLoanOriginal} 
    then
     assign
         cCardName      = {&LoanOriginal}
         cAttribute     = ""
         cScenarioName  = "First Mortgage Loan"
         cScenarioDesc  = "Original First Mortgage Title insurance"
         .
   when {&UiLoanSubstitution} 
    then
     assign
         cCardName      = {&LoanSubstitution}
         cAttribute     = ""
         cScenarioName  = "Substitution Rate First Mortgage"
         cScenarioDesc  = "Mortgage insured in First lien position to same Lender for same Owner and property"
         .
   when {&UiJuniorLoanCoverage} 
    then
     assign
         cCardName      = {&JuniorLoan}
         cAttribute     = ""
         cScenarioName  = "Limited Coverage Junior Loan"
         cScenarioDesc  = "Insures secondary lender against damages/loss"
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremium = ?.
       return.
    end.
  end case.
  
  opdePremium = CalculateBasicPremium(cCardName,cAttribute,ipiAmount,{&LendersPolicy},1). 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ResiLoanReissue) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResiLoanReissue Procedure 
PROCEDURE ResiLoanReissue :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcRateType        as character no-undo.
  define input  parameter ipiAmount          as integer   no-undo.
  define input  parameter ipiPriorAmount     as integer   no-undo.
  define output parameter opdePremium        as decimal   no-undo.

  define variable cCardName                  as character no-undo.
  define variable cExcessCardName            as character no-undo.
  define variable cAttribute                 as character no-undo.
  define variable dePremiumBasic             as decimal   no-undo.
  define variable dePremiumExcess            as decimal   no-undo.

  CreateLog("Scenario Executed: Residential Loan Reissue","", 0, "",{&Scenario},{&Scenario},0 ). 

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcRateType:
   when {&UiReissue} 
    then
     assign
         cCardName               = {&LoanReissue}
         cExcessCardName         = {&LoanReissueExcess}
         cAttribute              = ""
         cScenarioName           = "Reissue/Refinance First Mortgage"
         cScenarioDesc           = "First Mortgage Policy issued on a property which was earlier insured as owner,loan or leasehold  as owner, loan or leasehold within 10 years prior to such application"
         
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremium = ?.
       return.
    end.
  end case.

  if ipiPriorAmount < ipiAmount 
   then
    do:
       dePremiumBasic  = CalculateBasicPremium(cCardName,cAttribute,ipiPriorAmount,{&LendersPolicy},1).
       dePremiumExcess = CalculateExcessPremium(cExcessCardName,cAttribute,ipiAmount,ipiPriorAmount,{&LendersPolicy},1).
    end.
   else
    dePremiumBasic = CalculateBasicPremium(cCardName,cAttribute,ipiAmount,{&LendersPolicy},1). 

  opdePremium = dePremiumBasic + dePremiumExcess.

  CreateLog("Calculated premium for Face amount and Excess amount is " + trim(string(opdePremium,"$>>>,>>>,>>9.99")),trim(string(opdePremium,">>>,>>>,>>9.99")),0, "",{&LendersPolicy},{&PremiumResult},0).

  /*------------------Apply rules required to adjust premium-----------------------*/
  opdePremium  = CalculateBasicPremium(cCardName,{&PostCalcAdjust},opdePremium,{&LendersPolicy},1).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ResiLoanReissueScndLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResiLoanReissueScndLoan Procedure 
PROCEDURE ResiLoanReissueScndLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcLoanRateType     as character no-undo.
  define input  parameter ipcScndLoanRateType as character no-undo.
  define input  parameter ipiLoanAmount       as integer   no-undo.
  define input  parameter ipiLoanPriorAmount  as integer   no-undo.
  define input  parameter ipiScndLoanAmount   as integer   no-undo.
  define output parameter opdePremiumLoan     as decimal   no-undo.
  define output parameter opdePremiumScndLoan as decimal   no-undo.

  define variable cLoanCardName               as character no-undo.
  define variable cScndLoanCardName           as character no-undo.
  define variable cLoanExcessCardName         as character no-undo.  
  define variable cLoanAttribute              as character no-undo.
  define variable cScndLoanAttribute          as character no-undo.
  define variable dePremiumLoan               as decimal   no-undo.
  define variable dePremiumLoanExcess         as decimal   no-undo.

  CreateLog("Scenario Executed: Residential Loan Reissue-SecondLoan","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcLoanRateType:
   when {&UiReissue} 
    then
     assign
         cLoanCardName          = {&OwnerReissue}
         cLoanExcessCardName    = {&LoanReissueExcess}
         cLoanAttribute         = ""
         cScenarioName          = "First Mortgage Reissue"
         cScenarioDesc          = "Simultaneous Issue of First Mortgage Reissue"
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumLoan        = ?.
       opdePremiumScndLoan    = ?.  
       return.
    end.
  end case.

  case ipcScndLoanRateType:
    when {&UiJuniorLoanCoverage} 
     then
      assign
         cScndLoanCardName        = {&JuniorLoan}
         cScndLoanAttribute       = ""
         cScenarioName            = cScenarioName + " - Junior Loan"
	 cScenarioDesc            = cScenarioDesc + " and Junior Loan Coverage"
         .  
    otherwise
     do:
        CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcScndLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .      
        opdePremiumLoan      = ?.
        opdePremiumScndLoan  = ?.  
        return.  
     end.
  end case.

  if ipiLoanAmount > ipiLoanPriorAmount
   then
    do:
       dePremiumLoan       = CalculateBasicPremium(cLoanCardName,cLoanAttribute,ipiLoanPriorAmount,{&LendersPolicy},1). 
       dePremiumLoanExcess = CalculateExcessPremium(cLoanExcessCardName,cLoanAttribute,ipiLoanAmount,ipiLoanPriorAmount,{&LendersPolicy},1).
    end.
   else
    dePremiumLoan = CalculateBasicPremium(cLoanCardName,cLoanAttribute,ipiLoanAmount,{&LendersPolicy},1). 
  
  opdePremiumLoan  = dePremiumLoan + dePremiumLoanExcess.              
  
  CreateLog("Calculated premium for Face amount and Excess amount is " + trim(string(opdePremiumLoan,"$>>>,>>>,>>9.99")),trim(string(opdePremiumLoan,">>>,>>>,>>9.99")),0, "",{&LendersPolicy},{&PremiumResult},0).
  
  /*------------------Apply rules required to adjust premium-----------------------*/
  opdePremiumLoan  = CalculateBasicPremium(cLoanCardName,{&PostCalcAdjust},opdePremiumLoan,{&LendersPolicy},1).
    
  opdePremiumScndLoan = CalculateBasicPremium(cScndLoanCardName,cScndLoanAttribute,ipiScndLoanAmount,{&SecondLendersPolicy},2).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ResiLoanReissueScndLoanExcess) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResiLoanReissueScndLoanExcess Procedure 
PROCEDURE ResiLoanReissueScndLoanExcess :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcLoanRateType     as character no-undo.
  define input  parameter ipcScndLoanRateType as character no-undo.
  define input  parameter ipiLoanAmount       as integer   no-undo.
  define input  parameter ipiLoanPriorAmount  as integer   no-undo.
  define input  parameter ipiScndLoanAmount   as integer   no-undo.
  define output parameter opdePremiumLoan     as decimal   no-undo.
  define output parameter opdePremiumScndLoan as decimal   no-undo.

  define variable cLoanCardName               as character no-undo.
  define variable cSimoScndLoanCardName       as character no-undo.
  define variable cLoanExcessCardName         as character no-undo.  
  define variable cExcessScndLoanCardName     as character no-undo. 
  define variable cLoanAttribute              as character no-undo.
  define variable cScndLoanAttribute          as character no-undo.
  define variable dePremiumLoan               as decimal   no-undo.
  define variable dePremiumLoanExcess         as decimal   no-undo.
  define variable dePremiumSimoScndLoan       as decimal   no-undo.
  define variable dePremiumExcessScndLoan     as decimal   no-undo.

  CreateLog("Scenario Executed: Residential Loan Reissue-SecondLoan Excess","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcLoanRateType:
   when {&UiReissue} 
    then
     assign
         cLoanCardName          = {&OwnerReissue}
         cLoanExcessCardName    = {&LoanReissueExcess}
         cLoanAttribute         = ""
         cScenarioName          = "First Mortgage Reissue"
         cScenarioDesc          = "Simultaneous Issue of First Mortgage Reissue"
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumLoan        = ?.
       opdePremiumScndLoan    = ?.  
       return.
    end.
  end case.

  case ipcScndLoanRateType:
    when {&UiSecondLoan} 
     then
      assign
          cSimoScndLoanCardName    = {&SimoSecondLoan}
          cExcessScndLoanCardName  = {&SimoSecondLoanExcess}
          cScndLoanAttribute       = ""
          cScenarioName            = cScenarioName + " - Second Mortgage"
          cScenarioDesc            = cScenarioDesc + " and Second Mortgage Policy involving Excess amount"
          .  
    otherwise
     do:
        CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcScndLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .      
        opdePremiumLoan      = ?.
        opdePremiumScndLoan  = ?.  
        return.  
     end.
  end case.

  if ipiLoanAmount > ipiLoanPriorAmount
   then
    do:
       dePremiumLoan       = CalculateBasicPremium(cLoanCardName,cLoanAttribute,ipiLoanPriorAmount,{&LendersPolicy},1). 
       dePremiumLoanExcess = CalculateExcessPremium(cLoanExcessCardName,cLoanAttribute,ipiLoanAmount,ipiLoanPriorAmount,{&LendersPolicy},1).
    end.
   else
    dePremiumLoan = CalculateBasicPremium(cLoanCardName,cLoanAttribute,ipiLoanAmount,{&LendersPolicy},1). 
  
  opdePremiumLoan  = dePremiumLoan + dePremiumLoanExcess.              
  
  CreateLog("Calculated premium for Face amount and Excess amount is " + trim(string(opdePremiumLoan,"$>>>,>>>,>>9.99")),trim(string(opdePremiumLoan,">>>,>>>,>>9.99")),0, "",{&LendersPolicy},{&PremiumResult},0).
  
  /*------------------Apply rules required to adjust premium-----------------------*/
  opdePremiumLoan  = CalculateBasicPremium(cLoanCardName,{&PostCalcAdjust},opdePremiumLoan,{&LendersPolicy},1).

  if ipiScndLoanAmount > ipiLoanAmount 
   then
    do:
       dePremiumSimoScndLoan   = CalculateBasicPremium(cSimoScndLoanCardName,cScndLoanAttribute,ipiLoanAmount,{&SecondLendersPolicy},2).
       dePremiumExcessScndLoan = CalculateExcessPremium(cExcessScndLoanCardName,cScndLoanAttribute,ipiScndLoanAmount,ipiLoanAmount,{&SecondLendersPolicy},2). 
    end.
   else
    dePremiumSimoScndLoan = CalculateBasicPremium(cSimoScndLoanCardName,cScndLoanAttribute,ipiScndLoanAmount,{&SecondLendersPolicy},2). 
    
  opdePremiumScndLoan = dePremiumSimoScndLoan + dePremiumExcessScndLoan.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ResiLoanScndLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResiLoanScndLoan Procedure 
PROCEDURE ResiLoanScndLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcLoanRateType     as character no-undo.
  define input  parameter ipcScndLoanRateType as character no-undo.
  define input  parameter ipiLoanAmount       as integer   no-undo.
  define input  parameter ipiScndLoanAmount   as integer   no-undo.
  define output parameter opdePremiumLoan     as decimal   no-undo.
  define output parameter opdePremiumScndLoan as decimal   no-undo.

  define variable cLoanCardName               as character no-undo.
  define variable cScndLoanCardName           as character no-undo.
  define variable cLoanAttribute              as character no-undo.
  define variable cScndLoanAttribute          as character no-undo.

  CreateLog("Scenario Executed: Residential Loan-SecondLoan","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcLoanRateType:
   when {&UiLoanOriginal} 
    then
      assign
          cLoanCardName  = {&LoanOriginal}
          cLoanAttribute = ""
          cScenarioName  = "First Mortgage"
          cScenarioDesc  = "Simultaneous Issue of First Mortgage"
          .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumLoan        = ?.
       opdePremiumScndLoan    = ?.  
       return.
    end.
  end case.

  case ipcScndLoanRateType:
   when {&UiJuniorLoanCoverage} 
    then
     assign
         cScndLoanCardName        = {&JuniorLoan}
         cScndLoanAttribute       = ""
         cScenarioName            = cScenarioName + " - Junior Loan"
	 cScenarioDesc            = cScenarioDesc + " and Junior Loan Coverage"
         .   
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcScndLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .      
       opdePremiumLoan      = ?.
       opdePremiumScndLoan  = ?.  
       return.  
    end.
  end case.

  opdePremiumLoan     = CalculateBasicPremium(cLoanCardName,cLoanAttribute,ipiLoanAmount,{&LendersPolicy},1). 
    
  opdePremiumScndLoan = CalculateBasicPremium(cScndLoanCardName,cScndLoanAttribute,ipiScndLoanAmount,{&SecondLendersPolicy},2).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ResiLoanScndLoanExcess) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResiLoanScndLoanExcess Procedure 
PROCEDURE ResiLoanScndLoanExcess :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcLoanRateType     as character no-undo.
  define input  parameter ipcScndLoanRateType as character no-undo.
  define input  parameter ipiLoanAmount       as integer   no-undo.
  define input  parameter ipiScndLoanAmount   as integer   no-undo.
  define output parameter opdePremiumLoan     as decimal   no-undo.
  define output parameter opdePremiumScndLoan as decimal   no-undo.

  define variable cLoanCardName               as character no-undo.
  define variable cSimoScndLoanCardName       as character no-undo. 
  define variable cExcessScndLoanCardName     as character no-undo.
  define variable cLoanAttribute              as character no-undo.
  define variable cScndLoanAttribute          as character no-undo.
  define variable dePremiumSimoScndLoan       as decimal   no-undo.
  define variable dePremiumExcessScndLoan     as decimal   no-undo.

  CreateLog("Scenario Executed: Residential Loan-SecondLoan Excess","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcLoanRateType:
   when {&UiLoanOriginal} 
    then
      assign
          cLoanCardName  = {&LoanOriginal}
          cLoanAttribute = ""
          cScenarioName  = "First Mortgage"
          cScenarioDesc  = "Simultaneous Issue of First Mortgage"
          .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumLoan        = ?.
       opdePremiumScndLoan    = ?.  
       return.
    end.
  end case.

  case ipcScndLoanRateType:
  when {&UiSecondLoan} 
    then
     assign
         cSimoScndLoanCardName    = {&SimoSecondLoan}
         cExcessScndLoanCardName  = {&SimoSecondLoanExcess}
         cScndLoanAttribute       = ""
         cScenarioName            = cScenarioName + " - Second Mortgage"
	 cScenarioDesc            = cScenarioDesc + " and Second Mortgage Policy involving Excess Amount"
         .       
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcScndLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .      
       opdePremiumLoan      = ?.
       opdePremiumScndLoan  = ?.  
       return.  
    end.
  end case.

  opdePremiumLoan  = CalculateBasicPremium(cLoanCardName,cLoanAttribute,ipiLoanAmount,{&LendersPolicy},1). 

  if ipiScndLoanAmount > ipiLoanAmount 
   then
    do:
       dePremiumSimoScndLoan   = CalculateBasicPremium(cSimoScndLoanCardName,cScndLoanAttribute,ipiLoanAmount,{&SecondLendersPolicy},2).
       dePremiumExcessScndLoan = CalculateExcessPremium(cExcessScndLoanCardName,cScndLoanAttribute,ipiScndLoanAmount,ipiLoanAmount,{&SecondLendersPolicy},2). 
    end.
   else
    dePremiumSimoScndLoan = CalculateBasicPremium(cSimoScndLoanCardName,cScndLoanAttribute,ipiScndLoanAmount,{&SecondLendersPolicy},2). 
    
  opdePremiumScndLoan = dePremiumSimoScndLoan + dePremiumExcessScndLoan.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ResiOwner) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResiOwner Procedure 
PROCEDURE ResiOwner :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcRateType      as character no-undo.
  define input  parameter ipiAmount        as integer   no-undo.
  define output parameter opdePremium      as decimal   no-undo.

  define variable cCardName                as character no-undo.
  define variable cAttribute               as character no-undo.
  
   
  {lib/nacard.i &rateType = ipcRateType , &cardName = cCardName , &attrID = cAttribute} 
  
  CreateLog("Scenario Executed: Residential Owner","", 0, "",{&Scenario},{&Scenario},0 ).   

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcRateType:
   when {&UiOriginal} 
    then
     assign
         cCardName       = {&OwnersOriginal}
         cAttribute      = ""
         cScenarioName   = "Owner's Original"
         cScenarioDesc   = "Owner's Original policy insuring a fee simple estate"
         .    
   when {&UiHomeowners} 
    then
     assign
         cCardName       = {&HomeOwner}
         cAttribute      = ""
         cScenarioName   = "Homeowner's Policy"
         cScenarioDesc   = "Homeowner�s Policy of Title Insurance on a one-to-four family residence"
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcRateType + ".","", 0, "","",{&CardSelectionError},0 ) .  
       opdePremium = ?.
       return.
    end.
  end case.
  
  opdePremium = CalculateBasicPremium(cCardName,cAttribute,ipiAmount,{&OwnersPolicy},1). 
 
                     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ResiOwnerJrLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResiOwnerJrLoan Procedure 
PROCEDURE ResiOwnerJrLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcOwnerRateType as character no-undo.
  define input  parameter ipcLoanRateType  as character no-undo.
  define input  parameter ipiOwnerAmount   as integer   no-undo.
  define input  parameter ipiLoanAmount    as integer   no-undo.
  define output parameter opdePremiumOwner as decimal   no-undo.
  define output parameter opdePremiumLoan  as decimal   no-undo.

  define variable cOwnerCardName           as character no-undo.
  define variable cSimoCardName            as character no-undo.
  define variable cExcessCardName          as character no-undo.
  define variable cOwnerAttribute          as character no-undo.
  define variable cLoanAttribute           as character no-undo.
  define variable dePremiumSimo            as decimal   no-undo.
  define variable dePremiumExcess          as decimal   no-undo.

  CreateLog("Scenario Executed: Residential Owner-Junior Loan","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */   
  case ipcOwnerRateType:
   when {&UiOriginal} 
    then
     assign
         cOwnerCardName  = {&OwnersOriginal}
         cOwnerAttribute = ""
         cScenarioName   = "Owner's Original"
         cScenarioDesc   = "Simultaneous issue of Owner's Original"
         . 
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcOwnerRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner = ?.
       opdePremiumLoan  = ?.  
       return.
    end.
  end case.

  case ipcLoanRateType: 
   when {&UiJuniorLoanCoverage} 
    then
     assign
         cSimoCardName        = {&SimoLoan}
         cExcessCardName      = {&SimoLoanJrExcess}
         cLoanAttribute       = ""
         cScenarioName        = cScenarioName + " - Junior Loan"
         cScenarioDesc        = cScenarioDesc + " and Junior Loan Policy"
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner = ?.
       opdePremiumLoan  = ?.  
       return.
    end.
  end case.

  opdePremiumOwner = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiOwnerAmount,{&OwnersPolicy},1). 
  
  if ipiLoanAmount > ipiOwnerAmount 
   then
    do:  
       dePremiumSimo   = CalculateBasicPremium(cSimoCardName,cLoanAttribute,ipiOwnerAmount,{&LendersPolicy},2).
       
       if (ipiLoanAmount - ipiOwnerAmount) > {&KansasJuniorFixAmtLimit}
        then 
         dePremiumExcess = CalculateExcessPremium(cExcessCardName,cLoanAttribute,ipiLoanAmount,ipiOwnerAmount,{&LendersPolicy},2). 
        else
         dePremiumExcess = CalculateBasicPremium(cExcessCardName,cLoanAttribute,(ipiLoanAmount - ipiOwnerAmount),{&LendersPolicy},2). 
       
       assign
         cScenarioName        = cScenarioName + " w/Excess Amount"
         cScenarioDesc        = cScenarioDesc + " involving Excess Amount"
         .
    end.
   else
    dePremiumSimo = CalculateBasicPremium(cSimoCardName,cLoanAttribute,ipiLoanAmount,{&LendersPolicy},2). 

  opdePremiumLoan = dePremiumSimo + dePremiumExcess.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ResiOwnerLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResiOwnerLoan Procedure 
PROCEDURE ResiOwnerLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcOwnerRateType as character no-undo.
  define input  parameter ipcLoanRateType  as character no-undo.
  define input  parameter ipiOwnerAmount   as integer   no-undo.
  define input  parameter ipiLoanAmount    as integer   no-undo.
  define output parameter opdePremiumOwner as decimal   no-undo.
  define output parameter opdePremiumLoan  as decimal   no-undo.

  define variable cOwnerCardName           as character no-undo.
  define variable cSimoCardName            as character no-undo.
  define variable cExcessCardName          as character no-undo.
  define variable cOwnerAttribute          as character no-undo.
  define variable cLoanAttribute           as character no-undo.
  define variable dePremiumSimo            as decimal   no-undo.
  define variable dePremiumExcess          as decimal   no-undo.

  CreateLog("Scenario Executed: Residential Owner-Loan","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */   
  case ipcOwnerRateType:
   when {&UiOriginal} 
    then
     assign
         cOwnerCardName  = {&OwnersOriginal}
         cOwnerAttribute = ""
         cScenarioName   = "Owner's Original"
         cScenarioDesc   = "Simultaneous issue of Owner's Original"
         . 
   when {&UiHomeowners} 
    then
     assign
         cOwnerCardName  = {&HomeOwner}
         cOwnerAttribute = ""
         cScenarioName   = "Homeowners"
         cScenarioDesc   = "Simultaneous issue of Homeowners"
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcOwnerRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner = ?.
       opdePremiumLoan  = ?.  
       return.
    end.
  end case.

  case ipcLoanRateType:
   when {&UiLoanOriginal} 
    then
     assign
         cSimoCardName        = {&SimoLoan}
         cExcessCardName      = {&SimoLoanExcess}
         cLoanAttribute       = ""
         cScenarioName        = cScenarioName + " - First Mortgage Original"
         cScenarioDesc        = cScenarioDesc + " and First Mortgage Original Policy"
         .     
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner = ?.
       opdePremiumLoan  = ?.  
       return.
    end.
  end case.

  opdePremiumOwner = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiOwnerAmount,{&OwnersPolicy},1). 

  if ipiLoanAmount > ipiOwnerAmount 
   then
    do:
       dePremiumSimo   = CalculateBasicPremium(cSimoCardName,cLoanAttribute,ipiOwnerAmount,{&LendersPolicy},2).
       dePremiumExcess = CalculateExcessPremium(cExcessCardName,cLoanAttribute,ipiLoanAmount,ipiOwnerAmount,{&LendersPolicy},2). 
       assign
         cScenarioName        = cScenarioName + " w/Excess Amount"
         cScenarioDesc        = cScenarioDesc + " involving Excess Amount"
         .
    end.
   else
    dePremiumSimo = CalculateBasicPremium(cSimoCardName,cLoanAttribute,ipiLoanAmount,{&LendersPolicy},2). 

  opdePremiumLoan = dePremiumSimo + dePremiumExcess.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ResiOwnerLoanJrScndLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResiOwnerLoanJrScndLoan Procedure 
PROCEDURE ResiOwnerLoanJrScndLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter  ipcOwnerRateType    as character no-undo.
  define input parameter  ipcLoanRateType     as character no-undo.
  define input parameter  ipcScndLoanRateType as character no-undo.
  define input parameter  ipiOwnerAmount      as integer   no-undo.
  define input parameter  ipiLoanAmount       as integer   no-undo.
  define input parameter  ipiScndLoanAmount   as integer   no-undo.
  define output parameter opdePremiumOwner    as decimal   no-undo.
  define output parameter opdePremiumLoan     as decimal   no-undo.
  define output parameter opdePremiumScndLoan as decimal   no-undo.

  define variable cOwnerCardName              as character no-undo.
  define variable cSimoLoanCardName           as character no-undo.
  define variable cExcessLoanCardName         as character no-undo.
  define variable cSimoScndLoanCardName       as character no-undo.
  define variable cExcessScndLoanCardName     as character no-undo.
  define variable cOwnerAttribute             as character no-undo.
  define variable cLoanAttribute              as character no-undo.
  define variable cScndLoanAttribute          as character no-undo.
  define variable dePremiumSimoLoan           as decimal   no-undo.
  define variable dePremiumSimoScndLoan       as decimal   no-undo.
  define variable dePremiumLoanExcess         as decimal   no-undo.
  define variable dePremiumScndLoanExcess     as decimal   no-undo.
  define variable lSuccess                    as logical   no-undo.

  CreateLog("Scenario Executed: Residential Owner-Loan-Junior SecondLoan","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcOwnerRateType:

   when {&UiHomeowners} 
    then
     assign
         cOwnerCardName     = {&HomeOwner}
         cOwnerAttribute    = ""
         cScenarioName      = "Homeowners"
         cScenarioDesc      = "Simultaneous issue of Homeowners"
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcOwnerRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner     = ?.
       opdePremiumLoan      = ?.  
       opdePremiumScndLoan  = ?.        
       return.      
    end.
  end case.

  case ipcLoanRateType:
   when {&UiLoanOriginal} 
    then
     assign
         cSimoLoanCardName    = {&SimoLoan}
         cExcessLoanCardName  = {&SimoLoanExcess}
         cLoanAttribute       = ""
         cScenarioName        = cScenarioName + " - First Mortgage Original"
         cScenarioDesc        = cScenarioDesc + " and First Mortgage Original"   
         
         .       
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner     = ?.
       opdePremiumLoan      = ?. 
       opdePremiumScndLoan  = ?.        
       return.
    end.
  end case.
      
  case ipcScndLoanRateType:
   
   when {&UiJuniorLoanCoverage} 
    then
     assign
         cSimoScndLoanCardName    = {&SimoSecondLoan}
         cExcessScndLoanCardName  = {&SimoSecondLoanJrExcess}
         cScndLoanAttribute       = ""
         cScenarioName            = cScenarioName + " - Junior Loan Original"
         cScenarioDesc            = cScenarioDesc + " and Junior Loan Original"
         .   
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcScndLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner     = ?.
       opdePremiumLoan      = ?. 
       opdePremiumScndLoan  = ?.        
       return.  
    end.
  end case.

  opdePremiumOwner      = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiOwnerAmount,{&OwnersPolicy},1).
  dePremiumSimoLoan     = CalculateBasicPremium(cSimoLoanCardName,cLoanAttribute,min(ipiOwnerAmount,ipiLoanAmount),{&LendersPolicy},2).
  dePremiumSimoScndLoan = CalculateBasicPremium(cSimoScndLoanCardName,cScndLoanAttribute,min(ipiOwnerAmount,ipiScndLoanAmount),{&SecondLendersPolicy},3).

  if ipiLoanAmount le ipiOwnerAmount and (ipiLoanAmount + ipiScndLoanAmount) > ipiOwnerAmount  
   then
    do:
       if ((ipiLoanAmount + ipiScndLoanAmount) - ipiOwnerAmount) > {&KansasJuniorFixAmtLimit}
        then 
         depremiumScndLoanExcess = CalculateExcessPremium(cExcessScndLoanCardName,cScndLoanAttribute,ipiLoanAmount + ipiScndLoanAmount , ipiOwnerAmount,{&SecondLendersPolicy},3 ). 
        else
         depremiumScndLoanExcess = CalculateBasicPremium(cExcessScndLoanCardName,cScndLoanAttribute,((ipiLoanAmount + ipiScndLoanAmount) - ipiOwnerAmount),{&SecondLendersPolicy},3 ).
    end.
   else if ipiLoanAmount > ipiOwnerAmount  
    then
     do:
        dePremiumLoanExcess     = CalculateExcessPremium(cExcessLoanCardName,cLoanAttribute,ipiLoanAmount ,ipiOwnerAmount,{&LendersPolicy},2 ).
        
        if ipiScndLoanAmount > {&KansasJuniorFixAmtLimit}
         then 
          depremiumScndLoanExcess = CalculateExcessPremium(cExcessScndLoanCardName,cScndLoanAttribute,ipiScndLoanAmount + ipiLoanAmount ,ipiLoanAmount,{&SecondLendersPolicy},3 ). 
         else
          depremiumScndLoanExcess = CalculateBasicPremium(cExcessScndLoanCardName,cScndLoanAttribute,ipiScndLoanAmount,{&SecondLendersPolicy},3 ).
     end.
     
  if dePremiumLoanExcess > 0 or dePremiumScndLoanExcess > 0
   then  
    assign
        cScenarioName    = cScenarioName + " w/Excess Amount"
        cScenarioDesc    = cScenarioDesc + " involving Excess Amount"
        .

  assign
      opdePremiumLoan     = dePremiumSimoLoan     + dePremiumLoanExcess
      opdePremiumScndLoan = dePremiumSimoScndLoan + dePremiumScndLoanExcess
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ResiOwnerLoanScndLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResiOwnerLoanScndLoan Procedure 
PROCEDURE ResiOwnerLoanScndLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter  ipcOwnerRateType    as character no-undo.
  define input parameter  ipcLoanRateType     as character no-undo.
  define input parameter  ipcScndLoanRateType as character no-undo.
  define input parameter  ipiOwnerAmount      as integer   no-undo.
  define input parameter  ipiLoanAmount       as integer   no-undo.
  define input parameter  ipiScndLoanAmount   as integer   no-undo.
  define output parameter opdePremiumOwner    as decimal   no-undo.
  define output parameter opdePremiumLoan     as decimal   no-undo.
  define output parameter opdePremiumScndLoan as decimal   no-undo.

  define variable cOwnerCardName              as character no-undo.
  define variable cSimoLoanCardName           as character no-undo.
  define variable cExcessLoanCardName         as character no-undo.
  define variable cSimoScndLoanCardName       as character no-undo.
  define variable cExcessScndLoanCardName     as character no-undo.
  define variable cOwnerAttribute             as character no-undo.
  define variable cLoanAttribute              as character no-undo.
  define variable cScndLoanAttribute          as character no-undo.
  define variable dePremiumSimoLoan           as decimal   no-undo.
  define variable dePremiumSimoScndLoan       as decimal   no-undo.
  define variable dePremiumLoanExcess         as decimal   no-undo.
  define variable dePremiumScndLoanExcess     as decimal   no-undo.
  define variable lSuccess                    as logical   no-undo.

  CreateLog("Scenario Executed: Residential Owner-Loan-SecondLoan","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcOwnerRateType:
   when {&UiOriginal} 
    then
     assign
         cOwnerCardName     = {&OwnersOriginal}
         cOwnerAttribute    = ""
         cScenarioName      = "Owner's Original"
         cScenarioDesc      = "Simultaneous issue of Owner's Original"
         .
   when {&UiHomeowners} 
    then
     assign
         cOwnerCardName     = {&HomeOwner}
         cOwnerAttribute    = ""
         cScenarioName      = "Homeowners"
         cScenarioDesc      = "Simultaneous issue of Homeowners"
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcOwnerRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner     = ?.
       opdePremiumLoan      = ?.  
       opdePremiumScndLoan  = ?.        
       return.      
    end.
  end case.

  case ipcLoanRateType:
   when {&UiLoanOriginal} 
    then
     assign
         cSimoLoanCardName    = {&SimoLoan}
         cExcessLoanCardName  = {&SimoLoanExcess}
         cLoanAttribute       = ""
         cScenarioName        = cScenarioName + " - First Mortgage Original"
         cScenarioDesc        = cScenarioDesc + " and First Mortgage Original"   
         
         .       
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner     = ?.
       opdePremiumLoan      = ?. 
       opdePremiumScndLoan  = ?.        
       return.
    end.
  end case.
      
  case ipcScndLoanRateType:
   when {&UiSecondLoan} 
    then
     assign
         cSimoScndLoanCardName    = {&SimoSecondLoan}
         cExcessScndLoanCardName  = {&SimoSecondLoanExcess}
         cScndLoanAttribute       = ""
         cScenarioName            = cScenarioName + " - Second Mortgage Original"
         cScenarioDesc            = cScenarioDesc + " and Second Mortgage Original"
         .    
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcScndLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner     = ?.
       opdePremiumLoan      = ?. 
       opdePremiumScndLoan  = ?.        
       return.  
    end.
  end case.

  opdePremiumOwner      = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiOwnerAmount,{&OwnersPolicy},1).
  dePremiumSimoLoan     = CalculateBasicPremium(cSimoLoanCardName,cLoanAttribute,min(ipiOwnerAmount,ipiLoanAmount),{&LendersPolicy},2).
  dePremiumSimoScndLoan = CalculateBasicPremium(cSimoScndLoanCardName,cScndLoanAttribute,min(ipiOwnerAmount,ipiScndLoanAmount),{&SecondLendersPolicy},3).

  if ipiLoanAmount le ipiOwnerAmount and (ipiLoanAmount + ipiScndLoanAmount) > ipiOwnerAmount  
   then
    depremiumScndLoanExcess    = CalculateExcessPremium(cExcessScndLoanCardName,cScndLoanAttribute,ipiLoanAmount + ipiScndLoanAmount , ipiOwnerAmount,{&SecondLendersPolicy},3 ).
   else if ipiLoanAmount > ipiOwnerAmount  
    then
     do:
        dePremiumLoanExcess     = CalculateExcessPremium(cExcessLoanCardName,cLoanAttribute,ipiLoanAmount ,ipiOwnerAmount,{&LendersPolicy},2 ).
        dePremiumScndLoanExcess = CalculateExcessPremium(cExcessScndLoanCardName,cScndLoanAttribute,ipiScndLoanAmount + ipiLoanAmount ,ipiLoanAmount,{&SecondLendersPolicy},3).
     end.
     
  if dePremiumLoanExcess > 0 or dePremiumScndLoanExcess > 0
   then  
    assign
        cScenarioName    = cScenarioName + " w/Excess Amount"
        cScenarioDesc    = cScenarioDesc + " involving Excess Amount"
        .

  assign
      opdePremiumLoan     = dePremiumSimoLoan     + dePremiumLoanExcess
      opdePremiumScndLoan = dePremiumSimoScndLoan + dePremiumScndLoanExcess
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ResiOwnerReissue) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResiOwnerReissue Procedure 
PROCEDURE ResiOwnerReissue :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcRateType         as character no-undo.
  define input  parameter ipiAmount           as integer   no-undo.
  define input  parameter ipiPriorAmount      as integer   no-undo.
  define output parameter opdePremium         as decimal   no-undo.

  define variable cCardName                   as character no-undo.
  define variable cExcessCardName             as character no-undo.
  define variable cAttribute                  as character no-undo.
  define variable dePremiumBasic              as decimal   no-undo.
  define variable dePremiumExcess             as decimal   no-undo.

  CreateLog("Scenario Executed: Residential OwnerReissue","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcRateType:
   when {&UiReissue}
    then
     assign
         cCardName               = {&OwnerReissue}
         cExcessCardName         = {&OwnerReissueExcess}
         cAttribute              = ""
         cScenarioName           = "Owner's Reissue"
         cScenarioDesc           = "Purchaser/lessee of real estate whose title has been insured by either an owners, loan or leasehold policy within 10 years prior to the application for new policy"
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremium = ?.       
       return.
    end.
  end case.

  if ipiPriorAmount < ipiAmount 
   then
    do:
       dePremiumBasic  = CalculateBasicPremium(cCardName,cAttribute,ipiPriorAmount,{&OwnersPolicy},1).
       dePremiumExcess = CalculateExcessPremium(cExcessCardName,cAttribute,ipiAmount,ipiPriorAmount,{&OwnersPolicy},1).
    end.
   else
    dePremiumBasic = CalculateBasicPremium(cCardName,cAttribute,ipiAmount,{&OwnersPolicy},1). 

  opdePremium = dePremiumBasic + dePremiumExcess.

  CreateLog("Calculated premium for Face amount and Excess amount is " + trim(string(opdePremium,"$>>>,>>>,>>9.99")),trim(string(opdePremium,">>>,>>>,>>9.99")),0, "",{&OwnersPolicy},{&PremiumResult},0).
      
  /*------------------Apply rules required to adjust premium-----------------------*/
  opdePremium  = CalculateBasicPremium(cCardName,{&PostCalcAdjust},opdePremium,{&OwnersPolicy},1).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ResiOwnerReissueLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResiOwnerReissueLoan Procedure 
PROCEDURE ResiOwnerReissueLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcOwnerRateType    as character no-undo.
  define input  parameter ipcLoanRateType     as character no-undo.
  define input  parameter ipiOwnerAmount      as integer   no-undo.
  define input  parameter ipiPriorOwnerAmount as integer   no-undo.
  define input  parameter ipiLoanAmount       as integer   no-undo.
  define output parameter opdePremiumOwner    as decimal   no-undo.
  define output parameter opdePremiumLoan     as decimal   no-undo.

  define variable cOwnerCardName              as character no-undo.
  define variable cOwnerExcessCardName        as character no-undo.
  define variable cSimoCardName               as character no-undo.
  define variable cLoanExcessCardName         as character no-undo.
  define variable cOwnerAttribute             as character no-undo.
  define variable cLoanAttribute              as character no-undo.
  define variable dePremiumBasic              as decimal   no-undo.
  define variable dePremiumExcess             as decimal   no-undo.
  define variable dePremiumSimo               as decimal   no-undo.
  define variable dePremiumSimoExcess         as decimal   no-undo.

  CreateLog("Scenario Executed: Residential Owner Reissue-Loan","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */  
  case ipcOwnerRateType:
   when {&UiReissue} 
    then
     assign
         cOwnerCardName          = {&OwnerReissue}
         cOwnerExcessCardName    = {&OwnerReissueExcess}
         cOwnerAttribute         = ""
         cScenarioName           = "Owner's Reissue"
         cScenarioDesc           = "Simultaneous issue of Owner's Reissue"
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcOwnerRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner     = ?.
       opdePremiumLoan      = ?. 
       return.
    end.
  end case.

  case ipcLoanRateType:
   when {&UiLoanOriginal} 
    then
     assign
         cSimoCardName       = {&SimoLoan}
         cLoanExcessCardName = {&SimoLoanExcess}
         cLoanAttribute      = ""
         cScenarioName       = cScenarioName + " - First Mortgage Original"
         cScenarioDesc       = cScenarioDesc + " and First Mortgage Original"
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner     = ?.
       opdePremiumLoan      = ?.       
       return.
    end.
  end case.

  if ipiPriorOwnerAmount < ipiOwnerAmount 
   then
    do:
       dePremiumBasic  = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiPriorOwnerAmount,{&OwnersPolicy},1).
       dePremiumExcess = CalculateExcessPremium(cOwnerExcessCardName,cOwnerAttribute,ipiOwnerAmount,ipiPriorOwnerAmount,{&OwnersPolicy},1).    
    end.
   else
    dePremiumBasic = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiOwnerAmount,{&OwnersPolicy},1). 

  opdePremiumOwner = dePremiumBasic + dePremiumExcess.
  
  CreateLog("Calculated premium for Face amount and Excess amount is " + trim(string(opdePremiumOwner,"$>>>,>>>,>>9.99")),trim(string(opdePremiumOwner,">>>,>>>,>>9.99")),0, "",{&OwnersPolicy},{&PremiumResult},0).

  /*------------------Apply rules required to adjust premium--------------------*/
  opdePremiumOwner  = CalculateBasicPremium(cOwnerCardName,{&PostCalcAdjust},opdePremiumOwner,{&OwnersPolicy},1). 

  if ipiLoanAmount > ipiOwnerAmount 
   then
    do:
       dePremiumSimo       = CalculateBasicPremium(cSimoCardName,cLoanAttribute,ipiOwnerAmount,{&LendersPolicy},2).
       dePremiumSimoExcess = CalculateExcessPremium(cLoanExcessCardName,cLoanAttribute,ipiLoanAmount,ipiOwnerAmount,{&LendersPolicy},2). 
             
       assign
           cScenarioName   = cScenarioName + " w/Excess Amount"
           cScenarioDesc   = cScenarioDesc + " involving Excess Amount"
           .
    end.
   else
    dePremiumSimo = CalculateBasicPremium(cSimoCardName,cLoanAttribute,ipiLoanAmount,{&LendersPolicy},2). 

  opdePremiumLoan = dePremiumSimo + dePremiumSimoExcess.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ResiOwnerReissueLoanJrScndLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResiOwnerReissueLoanJrScndLoan Procedure 
PROCEDURE ResiOwnerReissueLoanJrScndLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter  ipcOwnerRateType    as character no-undo.
  define input  parameter  ipcLoanRateType     as character no-undo.
  define input  parameter  ipcScndLoanRateType as character no-undo.
  define input  parameter  ipiOwnerAmount      as integer   no-undo.
  define input  parameter  ipiPriorOwnerAmount as integer   no-undo.
  define input  parameter  ipiLoanAmount       as integer   no-undo.
  define input  parameter  ipiScndLoanAmount   as integer   no-undo.
  define output parameter  opdePremiumOwner    as decimal   no-undo.
  define output parameter  opdePremiumLoan     as decimal   no-undo.
  define output parameter  opdePremiumScndLoan as decimal   no-undo.

  define variable cOwnerCardName               as character no-undo.
  define variable cOwnerExcessCardName         as character no-undo.
  define variable cSimoLoanCardName            as character no-undo.
  define variable cExcessLoanCardName          as character no-undo.
  define variable cSimoScndLoanCardName        as character no-undo.
  define variable cExcessScndLoanCardName      as character no-undo.
  define variable cOwnerAttribute              as character no-undo.
  define variable cLoanAttribute               as character no-undo.
  define variable cScndLoanAttribute           as character no-undo.
  define variable depremiumLoanExcess          as decimal   no-undo.
  define variable depremiumScndLoanExcess      as decimal   no-undo.
  define variable dePremiumBasic               as decimal   no-undo.
  define variable dePremiumExcess              as decimal   no-undo.
  define variable dePremiumSimoLoan            as decimal   no-undo.
  define variable dePremiumSimoScndLoan        as decimal   no-undo.
  
  CreateLog("Scenario Executed: Residential Owner Reissue-Loan-Junior SecondLoan","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcOwnerRateType:
   when {&UiReissue} 
    then
     assign
         cOwnerCardName          = {&OwnerReissue}
         cOwnerExcessCardName    = {&OwnerReissueExcess}
         cOwnerAttribute         = ""
         cScenarioName           = "Owner's Reissue"
         cScenarioDesc           = "Simultaneous issue of Owner's Reissue"
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcOwnerRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner     = ?.
       opdePremiumLoan      = ?. 
       opdePremiumScndLoan  = ?.        
       return.
    end.
  end case.

  case ipcLoanRateType:
   when {&UiLoanOriginal} 
    then
     assign
         cSimoLoanCardName    = {&SimoLoan}
         cExcessLoanCardName  = {&SimoLoanExcess}
         cLoanAttribute       = ""
         cScenarioName        = cScenarioName + " - First Mortgage Original"
         cScenarioDesc        = cScenarioDesc + " and First Mortgage Original"
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner     = ?.
       opdePremiumLoan      = ?. 
       opdePremiumScndLoan  = ?.        
       return.
    end.
  end case.
          
  case ipcScndLoanRateType:
   when {&UiJuniorLoanCoverage} 
    then
     assign
         cSimoScndLoanCardName    = {&SimoSecondLoan}
         cExcessScndLoanCardName  = {&SimoSecondLoanJrExcess}
         cScndLoanAttribute       = ""
         cScenarioName            = cScenarioName + " - Junior Loan Original"
         cScenarioDesc            = cScenarioDesc + " and Junior Loan Original"
         .  
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcScndLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner     = ?.
       opdePremiumLoan      = ?. 
       opdePremiumScndLoan  = ?.        
       return.  
    end.
  end case.

  if ipiPriorOwnerAmount < ipiOwnerAmount 
   then
    do:
       dePremiumBasic  = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiPriorOwnerAmount,{&OwnersPolicy},1).
       dePremiumExcess = CalculateExcessPremium(cOwnerExcessCardName,cOwnerAttribute,ipiOwnerAmount,ipiPriorOwnerAmount,{&OwnersPolicy},1).
    end.
   else
    dePremiumBasic = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiOwnerAmount,{&OwnersPolicy},1). 
  
  opdePremiumOwner = dePremiumBasic + dePremiumExcess.
  
  CreateLog("Calculated premium for Face amount and Excess amount is " + trim(string(opdePremiumOwner,"$>>>,>>>,>>9.99")),trim(string(opdePremiumOwner,">>>,>>>,>>9.99")),0, "",{&OwnersPolicy},{&PremiumResult},0).

  /*------------------Apply rules required to adjust premium---------------------*/
  opdePremiumOwner  = CalculateBasicPremium(cOwnerCardName,{&PostCalcAdjust},opdePremiumOwner,{&OwnersPolicy},1). 
  dePremiumSimoLoan     = CalculateBasicPremium(cSimoLoanCardName,cLoanAttribute,min(ipiOwnerAmount,ipiLoanAmount),{&LendersPolicy},2).
  dePremiumSimoScndLoan = CalculateBasicPremium(cSimoScndLoanCardName,cScndLoanAttribute,min(ipiOwnerAmount,ipiScndLoanAmount),{&SecondLendersPolicy},3).

  if ipiLoanAmount le ipiOwnerAmount and (ipiLoanAmount + ipiScndLoanAmount) > ipiOwnerAmount 
   then
    do:
       if ((ipiLoanAmount + ipiScndLoanAmount) - ipiOwnerAmount) > {&KansasJuniorFixAmtLimit}
        then 
         depremiumScndLoanExcess = CalculateExcessPremium(cExcessScndLoanCardName,cScndLoanAttribute,ipiLoanAmount + ipiScndLoanAmount , ipiOwnerAmount,{&SecondLendersPolicy},3 ). 
        else
         depremiumScndLoanExcess = CalculateBasicPremium(cExcessScndLoanCardName,cScndLoanAttribute,((ipiLoanAmount + ipiScndLoanAmount) - ipiOwnerAmount),{&SecondLendersPolicy},3 ).
    end.
   else if ipiLoanAmount > ipiOwnerAmount  
    then
     do:
        dePremiumLoanExcess     = CalculateExcessPremium(cExcessLoanCardName,cLoanAttribute,ipiLoanAmount ,ipiOwnerAmount,{&LendersPolicy},2 ).
        
        if ipiScndLoanAmount > {&KansasJuniorFixAmtLimit}
         then 
          depremiumScndLoanExcess = CalculateExcessPremium(cExcessScndLoanCardName,cScndLoanAttribute,ipiScndLoanAmount + ipiLoanAmount ,ipiLoanAmount,{&SecondLendersPolicy},3 ). 
         else
          depremiumScndLoanExcess = CalculateBasicPremium(cExcessScndLoanCardName,cScndLoanAttribute,ipiScndLoanAmount,{&SecondLendersPolicy},3 ).
     end.
     
  if dePremiumLoanExcess > 0 or dePremiumScndLoanExcess > 0
   then  
    assign
        cScenarioName   = cScenarioName + " w/Excess Amount"
        cScenarioDesc   = cScenarioDesc + " involving Excess Amount"
        . 
 
  assign
      opdePremiumLoan     = dePremiumSimoLoan     + dePremiumLoanExcess
      opdePremiumScndLoan = dePremiumSimoScndLoan + depremiumScndLoanExcess
      .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ResiOwnerReissueLoanScndLoan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResiOwnerReissueLoanScndLoan Procedure 
PROCEDURE ResiOwnerReissueLoanScndLoan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter  ipcOwnerRateType    as character no-undo.
  define input  parameter  ipcLoanRateType     as character no-undo.
  define input  parameter  ipcScndLoanRateType as character no-undo.
  define input  parameter  ipiOwnerAmount      as integer   no-undo.
  define input  parameter  ipiPriorOwnerAmount as integer   no-undo.
  define input  parameter  ipiLoanAmount       as integer   no-undo.
  define input  parameter  ipiScndLoanAmount   as integer   no-undo.
  define output parameter  opdePremiumOwner    as decimal   no-undo.
  define output parameter  opdePremiumLoan     as decimal   no-undo.
  define output parameter  opdePremiumScndLoan as decimal   no-undo.

  define variable cOwnerCardName               as character no-undo.
  define variable cOwnerExcessCardName         as character no-undo.
  define variable cSimoLoanCardName            as character no-undo.
  define variable cExcessLoanCardName          as character no-undo.
  define variable cSimoScndLoanCardName        as character no-undo.
  define variable cExcessScndLoanCardName      as character no-undo.
  define variable cOwnerAttribute              as character no-undo.
  define variable cLoanAttribute               as character no-undo.
  define variable cScndLoanAttribute           as character no-undo.
  define variable depremiumLoanExcess          as decimal   no-undo.
  define variable depremiumScndLoanExcess      as decimal   no-undo.
  define variable dePremiumBasic               as decimal   no-undo.
  define variable dePremiumExcess              as decimal   no-undo.
  define variable dePremiumSimoLoan            as decimal   no-undo.
  define variable dePremiumSimoScndLoan        as decimal   no-undo.
  
  CreateLog("Scenario Executed: Residential Owner Reissue-Loan-SecondLoan","", 0, "",{&Scenario},{&Scenario},0 ) . 

  /* This is core business logic which identifies CARDS to be used in each scenario. */
  case ipcOwnerRateType:
   when {&UiReissue} 
    then
     assign
         cOwnerCardName          = {&OwnerReissue}
         cOwnerExcessCardName    = {&OwnerReissueExcess}
         cOwnerAttribute         = ""
         cScenarioName           = "Owner's Reissue"
         cScenarioDesc           = "Simultaneous issue of Owner's Reissue"
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcOwnerRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner     = ?.
       opdePremiumLoan      = ?. 
       opdePremiumScndLoan  = ?.        
       return.
    end.
  end case.

  case ipcLoanRateType:
   when {&UiLoanOriginal} 
    then
     assign
         cSimoLoanCardName    = {&SimoLoan}
         cExcessLoanCardName  = {&SimoLoanExcess}
         cLoanAttribute       = ""
         cScenarioName        = cScenarioName + " - First Mortgage Original"
         cScenarioDesc        = cScenarioDesc + " and First Mortgage Original"
         .
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner     = ?.
       opdePremiumLoan      = ?. 
       opdePremiumScndLoan  = ?.        
       return.
    end.
  end case.
          
  case ipcScndLoanRateType:
   when {&UiSecondLoan} 
    then
     assign
         cSimoScndLoanCardName    = {&SimoSecondLoan}
         cExcessScndLoanCardName  = {&SimoSecondLoanExcess}
         cScndLoanAttribute       = ""
         cScenarioName            = cScenarioName + " - Second Mortgage Original"
         cScenarioDesc            = cScenarioDesc + " and Second Mortgage Original"
         .  
   otherwise
    do:
       CreateLog("Rate Card does not exist for provided UI Input RateType - " + ipcScndLoanRateType + ".","", 0, "","",{&CardSelectionError},0 ) .
       opdePremiumOwner     = ?.
       opdePremiumLoan      = ?. 
       opdePremiumScndLoan  = ?.        
       return.  
    end.
  end case.

  if ipiPriorOwnerAmount < ipiOwnerAmount 
   then
    do:
       dePremiumBasic  = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiPriorOwnerAmount,{&OwnersPolicy},1).
       dePremiumExcess = CalculateExcessPremium(cOwnerExcessCardName,cOwnerAttribute,ipiOwnerAmount,ipiPriorOwnerAmount,{&OwnersPolicy},1).
    end.
   else
    dePremiumBasic = CalculateBasicPremium(cOwnerCardName,cOwnerAttribute,ipiOwnerAmount,{&OwnersPolicy},1). 
  
  opdePremiumOwner = dePremiumBasic + dePremiumExcess.
  
  CreateLog("Calculated premium for Face amount and Excess amount is " + trim(string(opdePremiumOwner,"$>>>,>>>,>>9.99")),trim(string(opdePremiumOwner,">>>,>>>,>>9.99")),0, "",{&OwnersPolicy},{&PremiumResult},0).

  /*------------------Apply rules required to adjust premium---------------------*/
  opdePremiumOwner  = CalculateBasicPremium(cOwnerCardName,{&PostCalcAdjust},opdePremiumOwner,{&OwnersPolicy},1). 

  dePremiumSimoLoan     = CalculateBasicPremium(cSimoLoanCardName,cLoanAttribute,min(ipiOwnerAmount,ipiLoanAmount),{&LendersPolicy},2).
  dePremiumSimoScndLoan = CalculateBasicPremium(cSimoScndLoanCardName,cScndLoanAttribute,min(ipiOwnerAmount,ipiScndLoanAmount),{&SecondLendersPolicy},3).

  if ipiLoanAmount le ipiOwnerAmount and (ipiLoanAmount + ipiScndLoanAmount) > ipiOwnerAmount 
   then
    depremiumScndLoanExcess = CalculateExcessPremium(cExcessScndLoanCardName,cScndLoanAttribute,ipiLoanAmount + ipiScndLoanAmount , ipiOwnerAmount,{&SecondLendersPolicy},3 ).
   else if ipiLoanAmount > ipiOwnerAmount  
    then
     do:
        dePremiumLoanExcess     = CalculateExcessPremium(cExcessLoanCardName,cLoanAttribute,ipiLoanAmount ,ipiOwnerAmount,{&LendersPolicy},2 ).
        depremiumScndLoanExcess = CalculateExcessPremium(cExcessScndLoanCardName,cScndLoanAttribute,ipiScndLoanAmount + ipiLoanAmount ,ipiLoanAmount,{&SecondLendersPolicy},3).
     end.
     
  if dePremiumLoanExcess > 0 or dePremiumScndLoanExcess > 0
   then  
    assign
        cScenarioName   = cScenarioName + " w/Excess Amount"
        cScenarioDesc   = cScenarioDesc + " involving Excess Amount"
        . 
 
  assign
      opdePremiumLoan     = dePremiumSimoLoan     + dePremiumLoanExcess
      opdePremiumScndLoan = dePremiumSimoScndLoan + depremiumScndLoanExcess
      .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ServerValidations) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ServerValidations Procedure 
PROCEDURE ServerValidations :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter oplErrorStatus as logical   no-undo.
  define output parameter opcErrorMsg    as character no-undo.
    
  {lib/navalidate.i &rateType = rateType , &loanRateType = loanRateType , &errorMsg = opcErrorMsg , &errorStatus = oplErrorStatus , &ownerEndors = ownerEndors , &loanEndors = loanEndors}   

  if lSimultaneous    
   then
    do:
       /* Invalid simultaneous scenario of Owner policies + Second Loan policies */
       if  coverageAmount           ne ""  and
           loanCoverageAmount       eq ""  and
           secondLoanCoverageAmount ne ""
        then
         assign 
             opcErrorMsg    =  "Owner and Second Loan policies cannot be issued simultaneously without First Loan policies."
             oplErrorStatus = true
             .    
             
       /* Invalid simultaneous scenario of Junior Loan policy + Second Loan policies */
       if  coverageAmount           eq ""  and
           (loanCoverageAmount      ne ""  and  loanRateType = {&UiJuniorLoanCoverage}) and
           secondLoanCoverageAmount ne ""
        then
         assign 
             opcErrorMsg    =  "Limited Coverage Junior Loan cannot be issued simultaneously with Second Loan policies."
             oplErrorStatus = true
             .   
             
       /* Invalid simultaneous scenario of Reissue Loan policy with Owner policies */
       else if  coverageAmount      ne ""  and
                loanCoverageAmount  ne ""  and  loanRateType = {&UiReissue}
        then
         assign 
             opcErrorMsg    =  "Loan Reissue policy cannot be issued simultaneously with Owner policies."
             oplErrorStatus = true
             .    
       /* Invalid scenario of Owner policies  + Limited Coverage Junior Loan + Second Loan policies */
       else if  coverageAmount      ne ""  and
           (loanCoverageAmount      ne ""  and loanRateType = {&UiJuniorLoanCoverage}) and
           secondLoanCoverageAmount ne ""
        then
         assign 
             opcErrorMsg    =  "Limited Coverage Junior Loan cannot be issued simultaneously with Owner and Second Loan policies."
             oplErrorStatus = true
             .
       /* Invalid scenario of Owners Reissue/Homeowners + Limited Coverage Junior Loan */
       else if (coverageAmount           ne ""  and rateType <> {&UiOriginal})              and
               (loanCoverageAmount       ne ""  and loanRateType = {&UiJuniorLoanCoverage}) and
                secondLoanCoverageAmount eq ""
        then      
         assign 
             opcErrorMsg    =  "Limited Coverage Junior Loan cannot be issued simultaneously with Owner's Reissue or Homeowners policy."
             oplErrorStatus = true
             .
      /* Invalid scenario of Owners Original + First Mortgage + Limited Coverage Junior Loan */
      else if   (coverageAmount           ne ""  and rateType           = {&UiOriginal})           and
                (loanCoverageAmount       ne ""  and loanRateType       = {&UiLoanOriginal})       and
                (secondLoanCoverageAmount ne ""  and secondLoanRateType = {&UiJuniorLoanCoverage})
       then      
        assign 
            opcErrorMsg    =  "Limited Coverage Junior Second Loan cannot be issued simultaneously with Owner's Original and First Mortgage Original policy."
            oplErrorStatus = true
            .
    end.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-validatePolicyAmount) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validatePolicyAmount Procedure 
PROCEDURE validatePolicyAmount :
/*------------------------------------------------------------------------------
  Purpose:     Validating the entered coverage amount for a policy.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter oplErrorStatus as logical   no-undo.
  define output parameter opcErrorMsg    as character no-undo.
  
  define variable iAmount                as integer   no-undo.
 
 /*------------------debugging-------------------*/
  debugparameter = debugparameter + "28.Validating the amount".
  /*----------------------------------------------*/ 
  
  /* Deciding the amount to validate for particular rate scenario*/ 
  case cScenarioCode:
   
    when {&Residential}{&Loan}  or when {&Residential}{&OwnerJuniorLoan} /* R"L or R"OJrL */
     then
      if loanRateType = {&UiJuniorLoanCoverage} 
       then
        assign
            iAmount = iLoanCovgAmt
            . 
            
    when {&Residential}{&LoanScndLoan}
       or when {&Residential}{&LoanReissueScndLoan} 
       or when {&Residential}{&OwnerLoanJuniorScndLoan} 
       or when {&Residential}{&OwnerReissueLoanJuniorScndLoan} /* R"LSL  or  R"LRSL or R"OLJrSL or R"ORLJrSL */   
     then 
      if secondLoanRateType = {&UiJuniorLoanCoverage} 
       then
        assign
            iAmount = iSecondLoanCovgAmt
            .          
          
  end case.

  /*------------------debugging-------------------*/
   debugparameter = debugparameter + "29.iAmount=" + string(iAmount).
  /*----------------------------------------------*/ 
  
  /* Validating the amount*/
  if {&KansasJuniorMaxLimit} <> 0 
    and iAmount <> 0 
    and iAmount > {&KansasJuniorMaxLimit}
   then
    assign 
        opcErrorMsg    = "Maximum liability of a Junior Loan Policy shall be " +  string({&KansasJuniorMaxLimit})
        oplErrorStatus = true
        .  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-CalculateBasicPremium) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION CalculateBasicPremium Procedure 
FUNCTION CalculateBasicPremium RETURNS DECIMAL
  (  pCardName as character ,pAttrID as character , pAmount as decimal, pCaseType as character, pGroupID as integer ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable dePremium as decimal no-undo.
  define variable lError    as logical no-undo.
  define variable hRateCard as handle  no-undo.
  define variable iCardID   as integer no-undo.

  {lib/nacheck.i &cardName = pCardName}
  
  run rcm\ratecard.p persistent set hRateCard (iCardSetID,
                                               pCardName,
                                               region,
                                               output iCardID).
  if iCardID = 0 
   then
    do:
       run GetLog in hRateCard(output table ttTempRateLog).
       for each ttTempRateLog:
         CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation , ttTempRateLog.cardID,{&face},pCaseType,ttTempRateLog.calculationOn,pGroupID).
       end.
       premiumOutput = "servererrmsg="           + "Rate card not found with ID " + string(iCardID) + 
                       ",success="               + "N"                                              +
                       ",cardSetID="             + string(iCardSetID)                               + 
                       ",manualeffectiveDate="   + cEffectiveDate.
       delete procedure hRateCard.
       return ?.
    end.
  
  dePremium = dynamic-function("GetRate" in hRateCard, pAmount,pAttrID, output lError).


  if lError or dePremium = ?
   then
    do:
       run GetLog in hRateCard(output table ttTempRateLog).
       for each ttTempRateLog:
         CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation, ttTempRateLog.cardID,{&face},pCaseType,ttTempRateLog.calculationOn,pGroupID).
       end.
       premiumOutput = "servererrmsg="         + "Calculation error" + 
                       ",success="             + "N"                 +
                       ",cardSetID="           + string(iCardSetID)  +
                       ",manualeffectiveDate=" + cEffectiveDate.
       delete procedure hRateCard.
       return ?.
    end.
    

    
  run GetLog in hRateCard(output table ttTempRateLog).

  for each ttTempRateLog:
    CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation, ttTempRateLog.cardID,{&face},pCaseType,ttTempRateLog.calculationOn,pGroupID).
  end.

  delete procedure hRateCard.


/* CadrdId and attribute list used to create ratescenariocard table */
  cCardAttrib = trim((cCardAttrib + "|" + string(iCardID) + "-" + pAttrID), "|").


  CreateLog("Total policy premium for face amount " + trim(string(pAmount,"$>>>,>>>,>>9.99")) +  " is " + trim(string(dePremium,"$>>>,>>>,>>9.99")),trim(string(dePremium,">>>,>>>,>>9.99")),0, {&face},pCaseType,{&PremiumResult},pGroupID).

  return dePremium.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CalculateEndors) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION CalculateEndors Procedure 
FUNCTION CalculateEndors RETURNS decimal
  ( pAmount as integer , pGroupName as character,pCaseType as character,pGroupID as integer , pSelectedEndorsement as character, pPropertyType as character,output premiumEndorsDetail as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
    
------------------------------------------------------------------------------*/
  define variable dePremium          as decimal   no-undo.
  define variable deTempTotalPremium as decimal   no-undo.
  define variable deProposedRate     as character no-undo.
  define variable iCountEndors       as integer   no-undo.
  define variable cAttribute         as character no-undo.
  define variable lError             as logical   no-undo.
  define variable lValidProposedRate as logical   no-undo.  
  define variable hEndCalc           as handle    no-undo.
  define variable iCardID            as integer   no-undo.
                                      
  Endors-List-block:
  do iCountEndors = 1 to num-entries(pSelectedEndorsement , "|"): 
    
    run  rcm\ratecard.p persistent set hEndCalc (iCardSetID,
                                                 entry(1,entry(iCountEndors,pSelectedEndorsement,"|"),"-"),   /*cardname*/ 
                                                 region,
                                                 output iCardID).
    if iCardID = 0 
     then
      do:
        run GetLog in hEndCalc (output table ttTempRateLog).
        for each ttTempRateLog:
          CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation , ttTempRateLog.cardID,{&face},pGroupName,ttTempRateLog.calculationOn,pGroupID).
        end.
        premiumOutput = "servererrmsg="         + "Rate card not found with ID " + string(iCardID) + 
                        ",success="             + "N"                            +
                        ",cardSetID="           + string(iCardSetID)             +
                        ",manualeffectiveDate=" + cEffectiveDate.
        delete procedure hEndCalc.
        next Endors-List-block.
      end.

    dePremium = dynamic-function("GetRate" in hEndCalc, pAmount, cAttribute, output lError).

    if dePremium = pAmount or dePremium = ? 
     then
      dePremium = 0.

    if lError 
     then
      do:
        run GetLog in hEndCalc(output table ttTempRateLog).
        for each ttTempRateLog:
          CreateLog(ttTempRateLog.action ,ttTempRateLog.actionCalculation, ttTempRateLog.cardID,{&face},pGroupName,ttTempRateLog.calculationOn,pGroupID).
        end.
        PremiumOutput = "servererrmsg="           + "Calculation error" + 
                        ",success="               + "N"                 +
                        ",cardSetID="             + string(iCardSetID)  +
                        ",manualeffectiveDate="   + cEffectiveDate.
        delete procedure hEndCalc.
        return ?.
      end.
    
    lValidProposedRate = false.

    if entry(2,entry(iCountEndors,pSelectedEndorsement,"|"),"-") ne "ProposedRate" and 
       entry(2,entry(iCountEndors,pSelectedEndorsement,"|"),"-") ne {&null} 
     then
      assign
        deProposedRate =  entry(2,entry(iCountEndors,pSelectedEndorsement,"|"),"-")
        lValidProposedRate = dynamic-function("validateRate" in hEndCalc, deProposedRate,cAttribute, output lError)
        .
  
    premiumEndorsDetail = premiumEndorsDetail + "|" + entry(1,entry(iCountEndors,pSelectedEndorsement,"|"),"-") + "-" + if lValidProposedRate then deProposedRate else string(dePremium).
    deTempTotalPremium = deTempTotalPremium + if lValidProposedRate then decimal(deProposedRate) else dePremium.
  
    run GetLog in hEndCalc(output table ttTempRateLog).
    for each ttTempRateLog:
      CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation, ttTempRateLog.cardID, {&endorsements},pGroupName,ttTempRateLog.calculationOn,pGroupID).
    end.
  
    if lValidProposedRate 
     then
      CreateLog("Modified endorsements Premium amount after validate rate is " + trim(string(decimal(deProposedRate),"$>>>,>>>,>>9.99")),trim(string(decimal(deProposedRate),">>>,>>>,>>9.99")), 0, {&endorsements},pGroupName,"",pGroupID).
    delete procedure hEndCalc.
    cAttribute = "".
  end.  
  CreateLog("Total endorsements Premium amount is " + trim(string(decimal(deTempTotalPremium),"$>>>,>>>,>>9.99")),trim(string(decimal(deTempTotalPremium),">>>,>>>,>>9.99")), 0, {&endorsements},pGroupName,{&PremiumResult},pGroupID).
  
  premiumEndorsDetail = trim(premiumEndorsDetail,"|").
  
  return deTempTotalPremium.   /* Function return value. */
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CalculateExcessPremium) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION CalculateExcessPremium Procedure 
FUNCTION CalculateExcessPremium RETURNS DECIMAL
  (pCardName as character ,pAttrID as character , pExcessAmount as integer, pAmount as integer, pCaseType as character, pGroupID as integer ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  define variable dePremium             as decimal no-undo.
  define variable deOrigAmountPremium   as decimal no-undo.
  define variable deExcessAmountPremium as decimal no-undo.
  define variable lError                as logical no-undo.
  define variable hExcessAmountCalc     as handle  no-undo.
  define variable iCardID               as integer no-undo.
  
  run  rcm\ratecard.p persistent set hExcessAmountCalc (iCardSetID,
                                                        pCardName,
                                                        region,
                                                        output iCardID).
                                                                  
  if iCardID = 0 
   then
    do:
      run GetLog in hExcessAmountCalc(output table ttTempRateLog).
      for each ttTempRateLog:
        CreateLog(ttTempRateLog.action , ttTempRateLog.actionCalculation , ttTempRateLog.cardID,{&face},pCaseType,ttTempRateLog.calculationOn,pGroupID).
      end.
      premiumOutput = "servererrmsg="           + "Rate card not found with ID " + string(iCardID) + 
                      ",success="               + "N"                                              +
                      ",cardSetID="             + string(iCardSetID)                               + 
                      ",manualeffectiveDate="   + cEffectiveDate.
      delete procedure hExcessAmountCalc.
      return ?.
    end.
              
  deOrigAmountPremium = dynamic-function("GetRate" in hExcessAmountCalc, pAmount, pAttrID, output lError).

  run GetLog in hExcessAmountCalc(output table ttTempRateLog).

  for each ttTempRateLog:
    CreateLog(ttTempRateLog.action ,ttTempRateLog.actionCalculation, ttTempRateLog.cardID,{&excess},pCaseType,ttTempRateLog.calculationOn,pGroupID).
  end.

  if lError or deOrigAmountPremium = ? 
   then
    do:
      premiumOutput = "servererrmsg="           + "Calculation error" + 
                      ",success="               + "N"                 +
                      ",cardSetID="             + string(iCardSetID)  + 
                      ",manualeffectiveDate="   + cEffectiveDate.
      delete procedure hExcessAmountCalc.
      return ?.
    end.
  
  CreateLog("Calculated premium for amount " + trim(string(pAmount, "$>>>,>>>,>>9.99")) + " is " + trim(string(deOrigAmountPremium, "$>>>,>>>,>>9.99")),trim(string(deOrigAmountPremium, ">>>,>>>,>>9.99")), 0, {&excess},pCaseType,"",pGroupID).
  
  deExcessAmountPremium = dynamic-function("GetRate" in hExcessAmountCalc, pExcessAmount, pAttrID, output lError).


  run GetLog in hExcessAmountCalc(output table ttTempRateLog).

  for each ttTempRateLog:
    CreateLog(ttTempRateLog.action ,ttTempRateLog.actionCalculation, ttTempRateLog.cardID,{&excess},pCaseType,ttTempRateLog.calculationOn,pGroupID).
  end.

  if lError or deExcessAmountPremium = ?  
   then
    do:
      premiumOutput = "servererrmsg="           + "Calculation error" + 
                      ",success="               + "N"                 +
                      ",cardSetID="             + string(iCardSetID)  + 
                      ",manualeffectiveDate="   + cEffectiveDate.
      delete procedure hExcessAmountCalc.
      return ?.
    end.

  delete procedure hExcessAmountCalc.
  
  CreateLog("Calculated premium for amount " + trim(string(pExcessAmount, "$>>>,>>>,>>9.99")) + " = " + trim(string(deExcessAmountPremium, "$>>>,>>>,>>9.99")),trim(string(deExcessAmountPremium, ">>>,>>>,>>9.99")), 0, {&excess},pCaseType,"",pGroupID).

  dePremium = deExcessAmountPremium - deOrigAmountPremium. 
  
 
  /* CadrdId and attribute list used to create ratescenariocard table */    
  cCardAttrib = trim((cCardAttrib + "|" + string(iCardID) + "-" + pAttrID), "|").


  if dePremium < 0 
   then
    do:
      CreateLog("Premium for excess amount " + trim(string(deExcessAmountPremium, "$>>>,>>>,>>9.99")) + " - " + trim(string(deOrigAmountPremium, "$>>>,>>>,>>9.99")) + " is negative. ","" ,iCardID, {&excess},pCaseType,{&GroupError},pGroupID).
      return ?. 
    end.
  
  CreateLog("Premium for excess amount " + trim(string(deExcessAmountPremium, "$>>>,>>>,>>9.99")) + " - " + trim(string(deOrigAmountPremium, "$>>>,>>>,>>9.99")) + " = " + trim(string( deExcessAmountPremium - deOrigAmountPremium, "$>>>,>>>,>>9.99"))
            ,trim(if (deExcessAmountPremium - deOrigAmountPremium) >= 0  then trim(string((deExcessAmountPremium - deOrigAmountPremium), ">>>,>>>,>>9.99")) else trim(string((deExcessAmountPremium - deOrigAmountPremium), ">>>,>>>,>>9.99")))
            ,iCardID
            ,{&excess}
            ,pCaseType
            ,{&PremiumResult}
            ,pGroupID).
  
  return dePremium.   /* Function return value. */
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CreateLog) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION CreateLog Procedure 
FUNCTION CreateLog RETURNS LOGICAL
  ( pNote as character , pActionCalculation as character,  pCardID as integer, pTypeAmount as character, pTypeCase as character, pCalculationOn as character, pGroupID as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define buffer ratelog for ratelog.
 
  iLogSeq = iLogSeq + 1.
  
  create ratelog.
  assign
    ratelog.seq               = iLogSeq
    ratelog.action            = pNote
    rateLog.actionCalculation = pActionCalculation
    ratelog.cardID            = pCardID
    ratelog.typeofamount      = pTypeAmount
    ratelog.calculationFor    = pTypeCase
    ratelog.calculationOn     = pCalculationOn
    ratelog.groupID           = pGroupID
    .
 
  return true.   /* Function return value. */
 
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

