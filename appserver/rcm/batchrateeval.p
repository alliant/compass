
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

{tt\rateUI.i}
{tt\batchProcessed.i}
{tt\calcParameters.i}
{tt\calcOutputParameters.i}
{tt\batchProcessed.i  &tableAlias=batchreject}


define buffer bufbatchform for batchform.
define buffer bfPolicyBatchform for batchform.

define variable pbatchid        as integer no-undo.
define variable pyearStartRange as integer no-undo.
define variable pyearEndRange   as integer no-undo.
define variable pseq            as integer no-undo.
define variable pRegion         as character no-undo.
define variable pVersion        as character no-undo.
define variable pPeriodID       as integer no-undo.

define variable ownerShortTerm    as character no-undo initial "01/01/18,01/01/15,01/01/08".
define variable LoanShortTerm     as character no-undo initial "01/01/18,01/01/08".
define variable cFormIDTOExclude  as character no-undo initial "Hold Open,C1,TGFG,107.2,108.8,110.219.2l,110.3,140.1c,LG,101.1,104,107.3,129.9".

define variable icount         as integer no-undo.
define variable iCountofpolicy as integer no-undo.
define variable iCountVersion  as integer no-undo.
define variable icountSimo     as integer no-undo.

define variable simoscenario   as character no-undo.

define variable ownerliability as decimal no-undo.
define variable Loanliability  as decimal no-undo.
define variable scndLiability  as decimal no-undo.
define variable tempLiability  as decimal no-undo.

define variable endorsList            as character no-undo.
define variable ownerendorsList       as character no-undo.
define variable loanendorsList        as character no-undo.
define variable scndloanendorsList    as character no-undo.

define variable cPolicyIDList           as character no-undo.
define variable batchFileTotalPremium   as decimal no-undo.
define variable batchFileFormIds        as character no-undo.
define variable iScenarioID             as integer no-undo.
define variable iCreatedScenario        as integer no-undo.
define variable loaded                  as logical no-undo.
define variable policyFomID             as character no-undo.
define variable fileIdToSkipInBatch     as character no-undo.
define variable cAgentID                as character no-undo.

iScenarioID = 0.
 
pRequest:getParameter("BatchID",        output pbatchid).
pRequest:getParameter("Seq",            output pseq).
pRequest:getParameter("Version",        output pVersion). 
pRequest:getParameter("YearStartRange", output pyearStartRange).
pRequest:getParameter("YearEndRange",   output pyearEndRange).
pRequest:getParameter("PeriodID",       output pPeriodID).

/* Dataset definitions         */
define dataset RCUI
 for developerComments, proposedRateConfig, region, propertyType, loanRateType, ownerRateType,scndLoanRateType,endorsement
    data-relation for propertyType, loanRateType relation-fields (propertyTypecode, propertyTypecode)     nested   foreign-key-hidden        
    data-relation for propertyType, ownerRateType relation-fields (propertyTypecode, propertyTypecode)    nested   foreign-key-hidden        
    data-relation for propertyType, scndLoanRateType relation-fields (propertyTypecode, propertyTypecode) nested   foreign-key-hidden .

loaded = dataset RCUI:read-json("file", search("D:\temp\coloradorateui.json"), "empty").

for each batch no-lock where batch.periodID = pPeriodID and
                             batch.stateid  = "CO":
fileIdToSkipInBatch = "".
cAgentID = batch.agentID.
LOOP-BATCH-FORMS:
for each  bufbatchform no-lock where bufbatchform.batchid = (if pbatchid = 0 then batch.batchid  else pbatchid ) and
                                     bufbatchform.seq = (if pseq = 0 then bufbatchform.seq  else pseq ) break by batchid by fileid by policyID:
                                     
  /* skip all the batch form for which rate card is not defined */                                    
  if lookup(bufbatchform.formid,cFormIDTOExclude,",") > 0 
   then
    do:
       create batchreject.
                assign
                      batchreject.scenarioID      = 0         
                      batchreject.batchID         = bufbatchform.batchID
                      batchreject.FileID          = bufbatchform.fileID
                      batchreject.PolicyID        = string(bufbatchform.policyid)
                      batchreject.batchPremium    = bufbatchform.grosspremium
                      batchreject.premiumMatched  = no
                      batchreject.comments        = "File contain formid that is in excluded list."
                      batchreject.formIDs         = bufbatchform.formID
                      batchreject.agentID         = cAgentID

                      .
       fileIdToSkipInBatch = bufbatchform.fileid. 
        next LOOP-BATCH-FORMS.
    end.
    else if fileIdToSkipInBatch = bufbatchform.fileid  and  fileIdToSkipInBatch ne ""
     then
      do:
         create batchreject.
                assign
                      batchreject.scenarioID      = 0         
                      batchreject.batchID         = bufbatchform.batchID
                      batchreject.FileID          = bufbatchform.fileID
                      batchreject.PolicyID        = string(bufbatchform.policyid)
                      batchreject.batchPremium    = bufbatchform.grosspremium
                      batchreject.premiumMatched  = no
                      batchreject.comments        = "File contain formid that is in excluded list."
                      batchreject.formIDs         = bufbatchform.formID
                      batchreject.agentID         = cAgentID
                      .
         next LOOP-BATCH-FORMS.
      end.
   /*   for each batchform no-lock where batchform.batchid = bufbatchform.batchid and batchform.fileid = bufbatchform.fileid and
                                       (batchform.formtype = "P" or batchform.formtype = "E") break by batchform.policyid:
                                       
          /* skip all the batch form for which rate card is not defined */                                    
          if lookup(batchform.formid,cFormIDTOExclude,",") > 0 
           then
            next. */ 
          /*will also be first of fileid*/
          if first(bufbatchform.policyid)
           then
             assign
                   iCountofpolicy         = 0
                   simoscenario           = ""
                   ownerliability         = 0
                   Loanliability          = 0
                   scndLiability          = 0
                   tempLiability          = 0
                   endorsList             = ""
                   ownerendorsList        = ""
                   loanendorsList         = ""
                   scndloanendorsList     = "" 
                   batchFileTotalPremium  = 0
                   batchFileFormIds       = ""
                   cPolicyIDList          = "".
                   
           if first-of(bufbatchform.policyid) 
            then
             assign
                   endorsList = ""
                   policyFomID = "".
             
           /* count the number of policies in the file to decide which case to execute general case with one policy simo with 2 policie or simo with 3 policies 
              and also get the formid of policy so that type of scenario (oll/ol/ll) can be decided*/  
           if bufbatchform.formtype = "P" 
            then
             do:
                for first county no-lock where stateid = "CO" and trim(county.description, "county") = bufbatchform.countyID:
                  pRegion = county.region. 
                end.
                if pRegion = " " or pRegion = ? or pRegion = "?" 
                 then
                  for first agent no-lock where agent.agentid = batch.agentid:
                    pRegion = agent.region. 
                  end. 
                if pRegion = ? or pRegion = "?" 
                 then 
                  pRegion = "".
                assign
                      iCountofpolicy = iCountofpolicy + 1
                      policyFomID    = bufbatchform.formid
                      tempLiability  = bufbatchform.liabilityAmount . 
             end.
            
           /*get the list of endosement that is related to one policy*/
           if bufbatchform.formtype = "E" 
            then
             endorsList = endorsList + bufbatchform.formID + "-" + "null" + "|".
             
           /* calculte the total premium of file */  
           batchFileTotalPremium = batchFileTotalPremium + bufbatchform.grosspremium.   
           
           /* formIDs calculated in file*/
           batchFileFormIds = batchFileFormIds + "," + bufbatchform.formid.

          if last-of(bufbatchform.policyid) and iCountofpolicy <= 3
           then
            do:
               endorsList = trim(endorsList,"|").
               
               /* Get the list of policies with form ID in the batch */
               cPolicyIDList = cPolicyIDList + "," + string(bufbatchform.policyID) + "-" + policyFomID .
               
               /* get the liabilti and endorsement list based on formID */
               if policyFomID = "O" and iCountofpolicy <> 0
                then
                 assign
                  simoscenario       = simoscenario + "O"
                  OwnerEndorsList    = endorsList
                  ownerliability     = tempLiability.
                else if iCountofpolicy <> 0
                 then
                  do:
                     simoscenario = simoscenario + "L".
                     if simoscenario = "L"  or simoscenario = "OL" or simoscenario = "LO" 
                      then
                       assign
                             loanEndorsList    = endorsList
                             loanliability     = tempLiability.
                      else
                       assign
                             scndloanendorsList    = endorsList
                             scndLiability         = tempLiability.
                  end.
                else if iCountofpolicy = 0
                 then
                  for first bfPolicyBatchform no-lock where bfPolicyBatchform.policyid = bufbatchform.policyid and bfPolicyBatchform.formtype = "P": 
                    if endorsList <> ""
                     then
                      do:
                         if bfPolicyBatchform.formID = "O" 
                          then
                           OwnerEndorsList = endorsList.
                          else if bfPolicyBatchform.formID = "L" or bfPolicyBatchform.formID = "MG"
                           then
                            do:
                               if loanEndorsList = ""
                                then
                                 loanEndorsList = endorsList.
                               else 
                                 scndloanendorsList = endorsList.
                            end.
                      end.
                  end.
               end.
               
           /* if the first loan liabilty is more than secon loan the swap the liabilty ans endorsement list as the first loan policy will always have less liability*/
           /*will also be last of fileid*/
           if last(bufbatchform.policyid) and loanliability > scndLiability and
                  (simoscenario = "LL" or simoscenario = "LLO" or simoscenario = "LOL" or simoscenario = "OLL" ) 
            then
             do:
                assign
                       tempLiability      = loanliability
                       loanliability      = scndLiability
                       scndLiability      = tempLiability
                       endorsList         = loanEndorsList
                       loanEndorsList     = scndloanendorsList
                       scndloanendorsList = endorsList
                       .              
             end.                 
 /*       end. /* for each batchform */   */                               
 /*  end. /* first of fileid*/ */

   
 /* if it is the last recode of file in the batch then go create a parameters that can be passed to calculator to calculate*/  
 if last-of (bufbatchform.fileid)
  then
   do:
  case iCountofpolicy:
  
  /*********************When the policy count is 0 there may be ony endorsement in the batch********************/
  when 0 
    then
     do:
           /*---------Owner Endorsement calculation-------------------*/
           if OwnerendorsList ne ""
             then            
            do:
               run  createOwnerEndorsParameter( (if bufbatchform.residential then "R" else "C") + "|" + OwnerendorsList,                   /* pScenarioname,  */
                                                entry(iCountVersion,pVersion,"," ),                                                        /*pVersion,        */
                                                pRegion,                                                                                   /* pRegion,        */
                                                (if bufbatchform.residential then "R" else "C"),                                           /* pPropertytype,  */
                                                string(ownerliability),                                                                    /* pCoverageamount,*/ 
                                                OwnerendorsList,                                                                           /* pRatetype,      */
                                                output iCreatedScenario).                                                               
                run createBatchProcessed(bufbatchform.batchID,OwnerendorsList,bufbatchform.fileID,iCreatedScenario,batchFileTotalPremium,
                                         batchFileFormIds,entry(iCountVersion,pVersion,"," ), "E",cAgentID).                           
             end.

            /*---------Loan Endorsement calculation-------------------*/
            if loanendorsList ne ""
             then            
             do:
                run  createLoanEndorsParameter( (if bufbatchform.residential then "R" else "C") + "|" + loanendorsList,                    /* pScenarioname,  */
                                                 entry(iCountVersion,pVersion,"," ),                                                       /*pVersion,        */
                                                 pRegion,                                                                                  /* pRegion,        */
                                                 (if bufbatchform.residential then "R" else "C"),                                          /* pPropertytype,  */
                                                 string(loanliability),                                                                    /* pCoverageamount,*/ 
                                                 loanendorsList,                                                                           /* pRatetype,      */
                                                 output iCreatedScenario).                                                               
                run createBatchProcessed(bufbatchform.batchID,loanendorsList,bufbatchform.fileID,iCreatedScenario,batchFileTotalPremium,
                                         batchFileFormIds,entry(iCountVersion,pVersion,"," ), "E",cAgentID).                           
             end.
            /*---------Second Loan Endorsement calculation-------------------*/
            if scndloanendorsList ne ""
             then
             do:
                run  createLoanEndorsParameter( (if bufbatchform.residential then "R" else "C") + "|" + scndloanendorsList,                    /* pScenarioname,  */
                                                 entry(iCountVersion,pVersion,"," ),                                                           /*pVersion,        */
                                                 pRegion,                                                                                      /* pRegion,        */
                                                 (if bufbatchform.residential then "R" else "C"),                                              /* pPropertytype,  */
                                                 string(scndLiability),                                                                        /* pCoverageamount,*/ 
                                                 scndloanendorsList,                                                                           /* pRatetype,      */
                                                 output iCreatedScenario).                                                               
                run createBatchProcessed(bufbatchform.batchID,scndloanendorsList,bufbatchform.fileID,iCreatedScenario,batchFileTotalPremium,
                                         batchFileFormIds,entry(iCountVersion,pVersion,"," ), "E",cAgentID).                           
             end.

  
   end. /* case 0 end.*/
 
 /*********************************************************************General*****************************************************************************/
   when 1 
    then
     do:
        /*----------------------------------------------------------O-----------------------------------------------------------------------*/
        if  simoscenario = "O" 
          then 
           do iCountVersion = 1 to num-entries(pVersion, ","):
              for each ownerRateType where ownerRateType.propertyTypecode = if bufbatchform.residential then "R" else "C":
                 if ownerRateType.priorPolicyDateFlag 
                  then
                   do icount = 1 to num-entries(ownerShortTerm, ","):
                      run  createOwnerParameter( ownerRateType.propertyTypecode + "|" + ownerRateType.description + "|" + entry(icount,ownerShortTerm, ","), /* pScenarioname,  */
                                                entry(iCountVersion,pVersion,"," ),                                                   /*pVersion,        */
                                                 pRegion,                                                                             /* pRegion,        */
                                                 ownerRateType.propertyTypecode,                                                      /* pPropertytype,  */
                                                 string(bufbatchform.liabilityAmount),                                                /* pCoverageamount,*/ 
                                                 ownerRateType.ownerRateTypeCode,                                                     /* pRatetype,      */
                                                 entry(icount,ownerShortTerm, ",") ,                                                  /* pEffectivedate) */
                                                 endorsList,                                                                          /*endorsList*/
                                                 output iCreatedScenario).                                                 
                      run createBatchProcessed(bufbatchform.batchID,cPolicyIDList,bufbatchform.fileID,iCreatedScenario,batchFileTotalPremium,
                                               batchFileFormIds,entry(iCountVersion,pVersion,"," ),"P",cAgentID).                           
                   end.
                  else 
                   do:
                      run  createOwnerParameter( ownerRateType.propertyTypecode + "|" + ownerRateType.description,                     /* pScenarioname,  */
                                                 entry(iCountVersion,pVersion,"," ),                                                   /*pVersion,        */
                                                 pRegion,                                                                              /* pRegion,        */
                                                 ownerRateType.propertyTypecode,                                                       /* pPropertytype,  */
                                                 string(bufbatchform.liabilityAmount),                                                 /* pCoverageamount,*/ 
                                                 ownerRateType.ownerRateTypeCode,                                                      /* pRatetype,      */
                                                 "PriorEffectiveDate",                                                                 /* pEffectivedate) */
                                                 endorsList,                                                                           /*endorsList*/
                                                 output iCreatedScenario).                                                               
                      run createBatchProcessed(bufbatchform.batchID,cPolicyIDList,bufbatchform.fileID,iCreatedScenario,batchFileTotalPremium,
                                               batchFileFormIds,entry(iCountVersion,pVersion,"," ), "P",cAgentID).                           
                   end.
              end.
          end.
        /*----------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        
        /*-----------------------------------------------------------------------L and Both ------------------------------------------------------------------------------*/
        if simoscenario = "L" 
         then  
         do iCountVersion = 1 to num-entries(pVersion, ","):
             for each loanRateType where loanRateType.propertyTypecode = (if bufbatchform.residential then "R" else "C") or
                                         loanRateType.propertyTypecode = (if bufbatchform.residential then "RB" else "CB"):
                if loanRateType.priorPolicyDateFlag 
                 then
                  do icount = 1 to num-entries(ownerShortTerm, ","):
                     run  createLoanParameter( loanRateType.propertyTypecode + "|" + loanRateType.description + "|" + entry(icount,ownerShortTerm, ","),     /* pScenarioname,  */
                                               entry(iCountVersion,pVersion,"," ),                                                               /*pVersion,        */
                                               pRegion,                                                                                          /* pRegion,        */
                                               loanRateType.propertyTypecode,                                                                    /* pPropertytype,  */
                                               string(bufbatchform.liabilityAmount),                                                             /* pCoverageamount,*/ 
                                               loanRateType.loanRateTypeCode,                                                                    /* pRatetype,      */
                                               entry(icount,ownerShortTerm, ","),                                                                /* pEffectivedate) */
                                               endorsList,                                                                                       /*endorsList*/
                                               output iCreatedScenario).                                                             
                    run createBatchProcessed(bufbatchform.batchID,cPolicyIDList,bufbatchform.fileID,iCreatedScenario,batchFileTotalPremium,
                                             batchFileFormIds,entry(iCountVersion,pVersion,"," ), "P",cAgentID).                           
                  end.
                 else 
                  do:
                     run  createLoanParameter( loanRateType.propertyTypecode + "|" + loanRateType.description,                                  /* pScenarioname,  */
                                               entry(iCountVersion,pVersion,"," ),                                                              /*pVersion,        */
                                               pRegion,                                                                                         /* pRegion,        */
                                               loanRateType.propertyTypecode,                                                                   /* pPropertytype,  */
                                               string(bufbatchform.liabilityAmount),                                                            /* pCoverageamount,*/ 
                                               loanRateType.loanRateTypeCode,                                                                   /* pRatetype,      */
                                               "loanPriorEffectiveDate",                                                                        /* pEffectivedate) */
                                                endorsList,                                                                                     /*endorsList*/
                                               output iCreatedScenario). 
                     run createBatchProcessed(bufbatchform.batchID,cPolicyIDList,bufbatchform.fileID,iCreatedScenario,batchFileTotalPremium,
                                              batchFileFormIds,entry(iCountVersion,pVersion,"," ), "P",cAgentID).                           
                  end.
             end.           
         end.
        /*----------------------------------------------------------------------------------------------------------------------------------------------------------------*/            
     end. /* case 1 end.*/
  /******************************************************************2 Simo***************************************************************************/   
  when 2 then
   do:      
      if last-of (bufbatchform.fileid) 
        then
        do iCountVersion = 1 to num-entries(pVersion, ","):
          /*-----------------------------------------------------------------OL or LO---------------------------------------------------------------*/
           if simoscenario = "OL" or simoscenario = "LO"
             then
            for each ownerRateType where ownerRateType.propertyTypecode = (if bufbatchform.residential then "R" else "C") and ownerRateType.simo = true:
               for each loanRateType where loanRateType.propertyTypecode = (if bufbatchform.residential then "R" else "C")  and loanRateType.simo = true:
                  if loanRateType.priorPolicyDateFlag  and
                     ownerRateType.priorPolicyDateFlag
                   then
                    do icount = 1 to num-entries(ownerShortTerm, ","):
                       do icountSimo = 1 to num-entries(loanShortTerm, ","):
                          run  createOwnerLoanParameter( ownerRateType.propertyTypecode + "|" + ownerRateType.description + "|" + entry(icount,ownerShortTerm, ",") + "|" + loanRateType.description + "|" + entry(icountSimo,loanShortTerm, ","), /* pScenarioname,  */
                                                         entry(iCountVersion,pVersion,"," ),                                                  /*pVersion,        */
                                                         pRegion,                                                                             /* pRegion,        */
                                                         ownerRateType.propertyTypecode,                                                      /* pPropertytype,  */
                                                         string(ownerliability),                                                              /* pCoverageamount,*/ 
                                                         ownerRateType.ownerRateTypeCode,                                                     /* pRatetype,      */
                                                         entry(icount,ownerShortTerm, ",")  ,                                                 /* pEffectivedate) */
                                                         string(loanliability),                                                               /* pCoverageamount,*/ 
                                                         loanRateType.loanRateTypeCode,                                                       /* pRatetype,      */
                                                         entry(icountSimo,loanShortTerm, ","),                                                /* pEffectivedate) */
                                                         ownerendorsList,                                                                     /* ownerendorsList */
                                                         loanendorsList,                                                                      /* loanendorsList */
                                                         output iCreatedScenario).                                               
                          run createBatchProcessed(bufbatchform.batchID,cPolicyIDList,bufbatchform.fileID,iCreatedScenario,batchFileTotalPremium,
                                                   trim(batchFileFormIds,","),entry(iCountVersion,pVersion,"," ), "P",cAgentID).                           
                       end.
                    end.
                   else if loanRateType.priorPolicyDateFlag and
                       not ownerRateType.priorPolicyDateFlag
                    then
                     do icountSimo = 1 to num-entries(loanShortTerm, ","):
                        run  createOwnerLoanParameter( ownerRateType.propertyTypecode + "|" + ownerRateType.description + "|" + loanRateType.description + "|" + entry(icountSimo,loanShortTerm, ","), /* pScenarioname,  */
                                                       entry(iCountVersion,pVersion,"," ),                                                  /*pVersion,        */
                                                       pRegion,                                                                             /* pRegion,        */
                                                       ownerRateType.propertyTypecode,                                                      /* pPropertytype,  */
                                                       string(ownerliability),                                                              /* pCoverageamount,*/ 
                                                       ownerRateType.ownerRateTypeCode,                                                     /* pRatetype,      */
                                                       "PriorEffectiveDate"  ,                                                              /* pEffectivedate) */
                                                       string(loanliability),                                                               /* pCoverageamount,*/ 
                                                       loanRateType.loanRateTypeCode,                                                       /* pRatetype,      */
                                                       entry(icountSimo,loanShortTerm, ","),                                                /* pEffectivedate) */
                                                       ownerendorsList,                                                                     /* ownerendorsList */
                                                       loanendorsList,                                                                      /* loanendorsList */
                                                       output iCreatedScenario).                                               
                        run createBatchProcessed(bufbatchform.batchID,cPolicyIDList,bufbatchform.fileID,iCreatedScenario,batchFileTotalPremium,
                                                 trim(batchFileFormIds,","),entry(iCountVersion,pVersion,"," ), "P",cAgentID).                           
                     end.
                    else if not loanRateType.priorPolicyDateFlag and
                            ownerRateType.priorPolicyDateFlag
                     then
                      do icount = 1 to num-entries(ownerShortTerm, ","):
                         run  createOwnerLoanParameter( ownerRateType.propertyTypecode + "|" + ownerRateType.description + "|" + entry(icount,ownerShortTerm, ",") + "|" + loanRateType.description,  /* pScenarioname,  */
                                                        entry(iCountVersion,pVersion,"," ),                                                  /*pVersion,        */
                                                        pRegion,                                                                             /* pRegion,        */
                                                        ownerRateType.propertyTypecode,                                                      /* pPropertytype,  */
                                                        string(ownerliability),                                                              /* pCoverageamount,*/ 
                                                        ownerRateType.ownerRateTypeCode,                                                     /* pRatetype,      */
                                                        entry(icount,ownerShortTerm, ","),                                                   /* pEffectivedate) */
                                                        string(loanliability),                                                               /* pCoverageamount,*/ 
                                                        loanRateType.loanRateTypeCode,                                                       /* pRatetype,      */
                                                        "loanPriorEffectiveDate",                                                            /* pEffectivedate) */
                                                        ownerendorsList,                                                                     /* ownerendorsList */
                                                        loanendorsList,                                                                      /* loanendorsList */
                                                        output iCreatedScenario).                                                          
                         run createBatchProcessed(bufbatchform.batchID,cPolicyIDList,bufbatchform.fileID,iCreatedScenario,batchFileTotalPremium,
                                                  trim(batchFileFormIds,","),entry(iCountVersion,pVersion,"," ), "P",cAgentID).                                                                        
                      end.
                    else 
                     do:
                        run  createOwnerLoanParameter( ownerRateType.propertyTypecode + "|" + ownerRateType.description + "|" +  loanRateType.description ,                        /* pScenarioname,  */
                                                       entry(iCountVersion,pVersion,"," ),                                                  /*pVersion,        */
                                                       pRegion,                                                                             /* pRegion,        */
                                                       ownerRateType.propertyTypecode,                                                      /* pPropertytype,  */
                                                       string(ownerliability),                                                              /* pCoverageamount,*/ 
                                                       ownerRateType.ownerRateTypeCode,                                                     /* pRatetype,      */
                                                       "PriorEffectiveDate",                                                                /* pEffectivedate) */
                                                       string(loanliability),                                                               /* pCoverageamount,*/ 
                                                       loanRateType.loanRateTypeCode,                                                       /* pRatetype,      */
                                                       "loanPriorEffectiveDate",                                                            /* pEffectivedate) */
                                                       ownerendorsList,                                                                     /* ownerendorsList */
                                                       loanendorsList,                                                                      /* loanendorsList */
                                                       output iCreatedScenario).                                                          
                        run createBatchProcessed(bufbatchform.batchID,cPolicyIDList,bufbatchform.fileID,iCreatedScenario,batchFileTotalPremium,
                                                 trim(batchFileFormIds,","),entry(iCountVersion,pVersion,"," ), "P",cAgentID).                           
                     end.
               end.
            end. 
           /*----------------------------------------------------------------------------------------------------------------------------------------------------*/
            
           /*----------------------------------------------------------LL---------------------------------------------------------------------------------*/ 
          if simoscenario = "LL"
             then
            for each loanRateType where loanRateType.propertyTypecode = (if bufbatchform.residential then "R" else "C")  and loanRateType.simo = true:
               for each scndLoanRateType where scndLoanRateType.propertyTypecode = (if bufbatchform.residential then "R" else "C") and scndLoanRateType.simo = true:
                   if loanRateType.priorPolicyDateFlag 
                    then
                     do icountSimo = 1 to num-entries(loanShortTerm, ","):
                        run  createLoanLoanParameter( loanRateType.propertyTypecode + "|" + loanRateType.description + "|" + scndLoanRateType.description + "|" + entry(icountSimo,loanShortTerm, ","), /* pScenarioname,  */
                                                       entry(iCountVersion,pVersion,"," ),                                                  /*pVersion,        */
                                                       pRegion,                                                                             /* pRegion,        */
                                                       loanRateType.propertyTypecode,                                                       /* pPropertytype,  */
                                                       string(loanliability),                                                               /* pCoverageamount,*/ 
                                                       loanRateType.loanRateTypeCode,                                                       /* pRatetype,      */
                                                       entry(icountSimo,loanShortTerm, ",")  ,                                              /* pEffectivedate) */
                                                       string(scndLiability),                                                               /* pCoverageamount,*/ 
                                                       scndLoanRateType.scndLoanRateTypecode,                                               /* scndLoanRateTypecode) */
                                                       loanendorsList,                                                                      /* loanendorsList */
                                                       scndloanendorsList,                                                                  /* scndloanendorsList */
                                                       output iCreatedScenario).                                              
                        run createBatchProcessed(bufbatchform.batchID,cPolicyIDList,bufbatchform.fileID,iCreatedScenario,batchFileTotalPremium,
                                                 trim(batchFileFormIds,","),entry(iCountVersion,pVersion,"," ), "P",cAgentID).                           
                     end.
                    else 
                     do:
                        run  createLoanLoanParameter( loanRateType.propertyTypecode + "|" + loanRateType.description + "|" +  scndLoanRateType.description ,                        /* pScenarioname,  */
                                                       entry(iCountVersion,pVersion,"," ),                                                  /*pVersion,        */
                                                       pRegion,                                                                             /* pRegion,        */
                                                       loanRateType.propertyTypecode,                                                       /* pPropertytype,  */
                                                       string(loanliability),                                                               /* pCoverageamount,*/ 
                                                       loanRateType.loanRateTypeCode,                                                       /* pRatetype,      */
                                                       "loanPriorEffectiveDate",                                                            /* pEffectivedate) */
                                                       string(scndLiability),                                                               /* pCoverageamount,*/ 
                                                       scndLoanRateType.scndLoanRateTypecode,                                               /* scndLoanRateTypecode) */ 
                                                       loanendorsList,                                                                      /* loanendorsList */
                                                       scndloanendorsList,                                                                  /* scndloanendorsList */
                                                       output iCreatedScenario).                                             
                        run createBatchProcessed(bufbatchform.batchID,cPolicyIDList,bufbatchform.fileID,iCreatedScenario,batchFileTotalPremium,
                                                 trim(batchFileFormIds,","),entry(iCountVersion,pVersion,"," ), "P",cAgentID).                           
                     end.
               end.
            end. 
                      
        end. /*end of lastof file*/
   end.   /* case 2 end */
   /*************************************************************************3 Simo*********************************************************************************/
  when 3 then
   do:   
      if last-of (bufbatchform.fileid) 
        then
        do iCountVersion = 1 to num-entries(pVersion, ","):
          /*-----------------------------------------------------------------OL or LO---------------------------------------------------------------*/
           if simoscenario = "OLL" or simoscenario = "LOL" or simoscenario = "LLO"
             then
              for each ownerRateType where ownerRateType.propertyTypecode = (if bufbatchform.residential then "R" else "C") and ownerRateType.simo = true:
                 for each loanRateType where loanRateType.propertyTypecode = (if bufbatchform.residential then "R" else "C")  and loanRateType.simo = true:
                     for each scndLoanRateType where scndLoanRateType.propertyTypecode = (if bufbatchform.residential then "R" else "C") and scndLoanRateType.simo = true:
                         if loanRateType.priorPolicyDateFlag  and
                            ownerRateType.priorPolicyDateFlag
                          then
                           do icount = 1 to num-entries(ownerShortTerm, ","):
                              do icountSimo = 1 to num-entries(loanShortTerm, ","):
                                 run  createOwnerLoanLoanParameter( ownerRateType.propertyTypecode + "|" + ownerRateType.description + "|" + entry(icount,ownerShortTerm, ",") + "|" + loanRateType.description + "|" + entry(icountSimo,loanShortTerm, ",") + "|" + scndLoanRateType.description, /* pScenarioname,  */
                                                                entry(iCountVersion,pVersion,"," ),                                                  /*pVersion,        */
                                                                pRegion,                                                                             /* pRegion,        */
                                                                ownerRateType.propertyTypecode,                                                      /* pPropertytype,  */
                                                                string(ownerliability),                                                              /* pCoverageamount,*/ 
                                                                ownerRateType.ownerRateTypeCode,                                                     /* pRatetype,      */
                                                                entry(icount,ownerShortTerm, ",")  ,                                                 /* pEffectivedate) */
                                                                string(loanliability),                                                               /* pCoverageamount,*/ 
                                                                loanRateType.loanRateTypeCode,                                                       /* pRatetype,      */
                                                                entry(icountSimo,loanShortTerm, ","),                                                /* pEffectivedate) */   
                                                                string(scndLiability),                                                               /* pCoverageamount,*/ 
                                                                scndLoanRateType.scndLoanRateTypecode,                                               /* scndLoanRateTypecode) */
                                                                ownerendorsList,                                                                     /* ownerendorsList */
                                                                loanendorsList,                                                                      /* loanendorsList */
                                                                scndloanendorsList,                                                                  /* scndloanendorsList */
                                                                output iCreatedScenario).                                               
                                 run createBatchProcessed(bufbatchform.batchID,cPolicyIDList,bufbatchform.fileID,iCreatedScenario,batchFileTotalPremium,
                                                          trim(batchFileFormIds,","),entry(iCountVersion,pVersion,"," ), "P",cAgentID).                           
                              end.
                           end.
                         else if loanRateType.priorPolicyDateFlag and
                                 not ownerRateType.priorPolicyDateFlag
                          then
                           do icountSimo = 1 to num-entries(loanShortTerm, ","):
                              run  createOwnerLoanLoanParameter( ownerRateType.propertyTypecode + "|" + ownerRateType.description + "|" + loanRateType.description + "|" + entry(icountSimo,loanShortTerm, ",") + "|" + scndLoanRateType.description, /* pScenarioname,  */
                                                             entry(iCountVersion,pVersion,"," ),                                                  /*pVersion,        */
                                                             pRegion,                                                                             /* pRegion,        */
                                                             ownerRateType.propertyTypecode,                                                      /* pPropertytype,  */
                                                             string(ownerliability),                                                              /* pCoverageamount,*/ 
                                                             ownerRateType.ownerRateTypeCode,                                                     /* pRatetype,      */
                                                             "PriorEffectiveDate"  ,                                                              /* pEffectivedate) */
                                                             string(loanliability),                                                               /* pCoverageamount,*/ 
                                                             loanRateType.loanRateTypeCode,                                                       /* pRatetype,      */
                                                             entry(icountSimo,loanShortTerm, ","),                                                /* pEffectivedate) */ 
                                                             string(scndLiability),                                                               /* pCoverageamount,*/ 
                                                             scndLoanRateType.scndLoanRateTypecode,                                               /* scndLoanRateTypecode) */
                                                             ownerendorsList,                                                                     /* ownerendorsList */
                                                             loanendorsList,                                                                      /* loanendorsList */
                                                             scndloanendorsList,                                                                  /* scndloanendorsList */
                                                             output iCreatedScenario).                                               
                              run createBatchProcessed(bufbatchform.batchID,cPolicyIDList,bufbatchform.fileID,iCreatedScenario,batchFileTotalPremium,
                                                       trim(batchFileFormIds,","),entry(iCountVersion,pVersion,"," ), "P",cAgentID).                           
                           end.
                         else if not loanRateType.priorPolicyDateFlag and
                                 ownerRateType.priorPolicyDateFlag
                          then
                           do icount = 1 to num-entries(ownerShortTerm, ","):
                              run  createOwnerLoanLoanParameter( ownerRateType.propertyTypecode + "|" + ownerRateType.description + "|" + entry(icount,ownerShortTerm, ",") + "|" + loanRateType.description + "|" + scndLoanRateType.description,  /* pScenarioname,  */
                                                             entry(iCountVersion,pVersion,"," ),                                                  /*pVersion,        */
                                                             pRegion,                                                                             /* pRegion,        */
                                                             ownerRateType.propertyTypecode,                                                      /* pPropertytype,  */
                                                             string(ownerliability),                                                              /* pCoverageamount,*/ 
                                                             ownerRateType.ownerRateTypeCode,                                                     /* pRatetype,      */
                                                             entry(icount,ownerShortTerm, ","),                                                   /* pEffectivedate) */
                                                             string(loanliability),                                                               /* pCoverageamount,*/ 
                                                             loanRateType.loanRateTypeCode,                                                       /* pRatetype,      */
                                                             "loanPriorEffectiveDate",                                                            /* pEffectivedate) */   
                                                             string(scndLiability),                                                               /* pCoverageamount,*/ 
                                                             scndLoanRateType.scndLoanRateTypecode,                                               /* scndLoanRateTypecode) */
                                                             ownerendorsList,                                                                     /* ownerendorsList */
                                                             loanendorsList,                                                                      /* loanendorsList */
                                                             scndloanendorsList,                                                                  /* scndloanendorsList */
                                                             output iCreatedScenario).                                                          
                              run createBatchProcessed(bufbatchform.batchID,cPolicyIDList,bufbatchform.fileID,iCreatedScenario,batchFileTotalPremium,
                                                       trim(batchFileFormIds,","),entry(iCountVersion,pVersion,"," ), "P",cAgentID).                                                                        
                           end.
                         else 
                          do:
                             run  createOwnerLoanLoanParameter( ownerRateType.propertyTypecode + "|" + ownerRateType.description + "|" +  loanRateType.description + "|" + scndLoanRateType.description,                        /* pScenarioname,  */
                                                            entry(iCountVersion,pVersion,"," ),                                                  /*pVersion,        */
                                                            pRegion,                                                                             /* pRegion,        */
                                                            ownerRateType.propertyTypecode,                                                      /* pPropertytype,  */
                                                            string(ownerliability),                                                              /* pCoverageamount,*/ 
                                                            ownerRateType.ownerRateTypeCode,                                                     /* pRatetype,      */
                                                            "PriorEffectiveDate",                                                                /* pEffectivedate) */
                                                            string(loanliability),                                                               /* pCoverageamount,*/ 
                                                            loanRateType.loanRateTypeCode,                                                       /* pRatetype,      */
                                                            "loanPriorEffectiveDate",                                                            /* pEffectivedate) */ 
                                                            string(scndLiability),                                                               /* pCoverageamount,*/ 
                                                            scndLoanRateType.scndLoanRateTypecode,                                               /* scndLoanRateTypecode) */
                                                            ownerendorsList,                                                                     /* ownerendorsList */
                                                            loanendorsList,                                                                      /* loanendorsList */
                                                            scndloanendorsList,                                                                   /* scndloanendorsList */
                                                            output iCreatedScenario).                                                          
                            run createBatchProcessed(bufbatchform.batchID,cPolicyIDList,bufbatchform.fileID,iCreatedScenario,batchFileTotalPremium,
                                                     trim(batchFileFormIds,","),entry(iCountVersion,pVersion,"," ), "P",cAgentID).                           
                          end.
                     end.    
                  end.
               end.                 
           /*----------------------------------------------------------------------------------------------------------------------------------------------------*/
           /*--------------------------------------------------------------------OOO/OOL/LOO/OlO--------------------------------------------------------------------*/
           else if simoscenario = "OOO" or simoscenario = "OOl" or simoscenario = "LOO" or simoscenario = "OLO"
            then
             do:
                create batchreject.
                assign
                      batchreject.scenarioID      = 0
                      batchreject.batchID         = bufbatchform.batchID
                      batchreject.FileID          = bufbatchform.fileID
                      batchreject.PolicyID        = cPolicyIDList
                      batchreject.batchPremium    = batchFileTotalPremium
                      batchreject.premiumMatched  = no
                      batchreject.comments        = "Scenario " + simoscenario + " not possible for calculator. Reason - Two Owner policies in one file." 
                      batchreject.formIDs         = trim(batchFileFormIds,",")
                      batchreject.agentID         = cAgentID
                      .
             end.
           /*---------------------------------------------------------------------------------------------------------------------------------------------------*/
           
           /*--------------------------------------------------------------------------LLL--------------------------------------------------------------------*/
           else if simoscenario = "LLL"
            then
             do: 
                create batchreject.
                assign
                      batchreject.scenarioID      = 0         
                      batchreject.batchID         = bufbatchform.batchID
                      batchreject.FileID          = bufbatchform.fileID
                      batchreject.PolicyID        = cPolicyIDList
                      batchreject.batchPremium    = batchFileTotalPremium
                      batchreject.premiumMatched  = no
                      batchreject.comments        = "Scenario " + simoscenario + " not possible for calculator. Reason - 3 Loan policies in One file."
                      batchreject.formIDs         = trim(batchFileFormIds,",")
                      batchreject.agentID         = cAgentID

                      .
             end.
           /*---------------------------------------------------------------------------------------------------------------------------------------------------*/
        end. /*end of lastof file*/   
   end.
   
   /************************************************************more than 3 policies*****************************************************************************/
   otherwise
   do:
      create batchreject.
      assign
             batchreject.scenarioID      = 0    
             batchreject.batchID         = bufbatchform.batchID
             batchreject.FileID          = bufbatchform.fileID
             batchreject.PolicyID        = string(bufbatchform.policyID)
             batchreject.batchPremium    = bufbatchform.grosspremium
             batchreject.premiumMatched  = no
             batchreject.comments        = "Scenario not possible for calculator. Reason - More then 3 policies in one file."
             batchreject.formIDs         =  bufbatchform.formID
             batchreject.agentID         = cAgentID


             .
   end.  
  
 end case.
 end. /*last of bufbatchform file id */ 
end. /*end of buf batch form*/
end. /*end of batch*/


run rcm\calculateRate.p(input table calcParameters, 
                        output table calcOutputParameters).
                        
                        
                   
pResponse:setParameter("batchProcessed", table batchProcessed).               
pResponse:setParameter("calcParameters", table calcParameters).
pResponse:setParameter("calcOutputParameters", table calcOutputParameters).
pResponse:setParameter("batchreject", table batchreject).               


procedure createBatchProcessed:
define input parameter pBatchID         as integer no-undo.
define input parameter pPolicyID        as character no-undo.
define input parameter pFileID          as character no-undo.
define input parameter pScenaarioID     as integer no-undo.
define input parameter pBatchPremium    as decimal no-undo.
define input parameter pFormIDs         as character no-undo.
define input parameter pversion         as character no-undo.
define input parameter pFormType        as character no-undo.
define input parameter pAgentID         as character no-undo.


create batchProcessed.
assign
      batchProcessed.scenarioID   = pScenaarioID
      batchProcessed.versionExec  = pversion
      batchProcessed.batchID      = pBatchID
      batchProcessed.FileID       = pFileID
      batchProcessed.PolicyID     = pPolicyID
      batchProcessed.batchPremium = pBatchPremium
      batchProcessed.formIDs      = pFormIDs
      batchProcessed.formType     = pFormType
      batchProcessed.agentID      = pAgentID
.
end.


procedure createOwnerParameter:
define input parameter pScenarioname       as character no-undo.
define input parameter pVersion            as character no-undo.
define input parameter pRegion             as character no-undo.
define input parameter pPropertytype       as character no-undo.
define input parameter pCoverageamount     as character no-undo.
define input parameter pRatetype           as character no-undo.
define input parameter pEffectivedate      as character no-undo.
define input parameter pownerEndors        as character no-undo.
define output parameter pScenarioID        as integer no-undo.
iScenarioID = iScenarioID + 1 .
pScenarioID = iScenarioID.
 create calcParameters.
      assign 
           calcParameters.scenarioID                = iScenarioID 
           calcParameters.scenarioName              = pScenarioname
           calcParameters.version                   = pVersion
           calcParameters.region                    = pRegion
           calcParameters.propertyType              = pPropertytype
           calcParameters.simultaneous              = "no"
           calcParameters.coverageAmount            = pCoverageamount
           calcParameters.ratetype                  = pRatetype
           calcParameters.reissueCoverageAmount     = "reissueCoverageAmount"
           calcParameters.PriorEffectiveDate        = pEffectivedate
           calcParameters.ownerEndors               = pownerEndors
           calcParameters.loanCoverageAmount        = "loanCoverageAmount"
           calcParameters.loanRateType              = "loanRateType"
           calcParameters.loanReissueCoverageAmount = "loanReissueCoverageAmount"
           calcParameters.loanPriorEffectiveDate    = "loanPriorEffectiveDate"
           calcParameters.loanEndors                = "loanEndors"
           calcParameters.lenderCPL                 = "lenderCPL"
           calcParameters.buyerCPL                  = "buyerCPL"
           calcParameters.sellerCPL                 = "sellerCPL"
           calcParameters.ratetypecode              = "ratetypecode"
           calcParameters.loanRateTypecode          = "loanRateTypecode"
           calcParameters.secondloanRateType        = "secondloanRateType"
           calcParameters.secondloanCoverageAmount  = "secondloanCoverageAmount"
           calcParameters.secondLoanEndors          = "secondLoanEndors" 
           . 
           
end.
procedure createLoanParameter:
define input parameter pScenarioname       as character no-undo.
define input parameter pVersion            as character no-undo.
define input parameter pRegion             as character no-undo.
define input parameter pPropertytype       as character no-undo.
define input parameter pLoanCoverageamount as character no-undo.
define input parameter pLoanRatetype       as character no-undo.
define input parameter pLoanEffectivedate  as character no-undo.
define input parameter ploanEndors         as character no-undo.
define output parameter pScenarioID        as integer   no-undo.
iScenarioID = iScenarioID + 1 .
pScenarioID = iScenarioID.
 create calcParameters.
      assign 
           calcParameters.scenarioID                = iScenarioID 
           calcParameters.scenarioName              = pScenarioname
           calcParameters.version                   = pVersion
           calcParameters.region                    = pRegion
           calcParameters.propertyType              = pPropertytype
           calcParameters.simultaneous              = "no"
           calcParameters.coverageAmount            = "coverageAmount"
           calcParameters.ratetype                  = "ratetype"
           calcParameters.reissueCoverageAmount     = "reissueCoverageAmount"
           calcParameters.PriorEffectiveDate        = "PriorEffectiveDate"
           calcParameters.ownerEndors               = "ownerEndors"
           calcParameters.loanCoverageAmount        = pLoanCoverageamount
           calcParameters.loanRateType              = pLoanRatetype
           calcParameters.loanReissueCoverageAmount = "loanReissueCoverageAmount"
           calcParameters.loanPriorEffectiveDate    = pLoanEffectivedate
           calcParameters.loanEndors                = ploanEndors
           calcParameters.lenderCPL                 = "lenderCPL"
           calcParameters.buyerCPL                  = "buyerCPL"
           calcParameters.sellerCPL                 = "sellerCPL"
           calcParameters.ratetypecode              = "ratetypecode"
           calcParameters.loanRateTypecode          = "loanRateTypecode"
           calcParameters.secondloanRateType        = "secondloanRateType"
           calcParameters.secondloanCoverageAmount  = "secondloanCoverageAmount"
           calcParameters.secondLoanEndors          = "secondLoanEndors" 
           . 
           
end.


procedure createOwnerLoanParameter:
define input parameter pScenarioname       as character no-undo.
define input parameter pVersion            as character no-undo.
define input parameter pRegion             as character no-undo.
define input parameter pPropertytype       as character no-undo.
define input parameter pCoverageamount     as character no-undo.
define input parameter pRatetype           as character no-undo.
define input parameter pEffectivedate      as character no-undo.
define input parameter pLoanCoverageamount as character no-undo.
define input parameter pLoanRatetype       as character no-undo.
define input parameter pLoanEffectivedate  as character no-undo.
define input parameter pownerEndors        as character no-undo.
define input parameter ploanEndors         as character no-undo.
define output parameter pScenarioID        as integer   no-undo.
iScenarioID = iScenarioID + 1 .
pScenarioID = iScenarioID. create calcParameters.
 
      assign 
           calcParameters.scenarioID                = iScenarioID 
           calcParameters.scenarioName              = pScenarioname
           calcParameters.version                   = pVersion
           calcParameters.region                    = pRegion
           calcParameters.propertyType              = pPropertytype
           calcParameters.simultaneous              = "yes"
           calcParameters.coverageAmount            = pCoverageamount
           calcParameters.ratetype                  = pRatetype
           calcParameters.reissueCoverageAmount     = "reissueCoverageAmount"
           calcParameters.PriorEffectiveDate        = pEffectivedate
           calcParameters.ownerEndors               = pownerEndors
           calcParameters.loanCoverageAmount        = pLoanCoverageamount
           calcParameters.loanRateType              = pLoanRatetype
           calcParameters.loanReissueCoverageAmount = "loanReissueCoverageAmount"
           calcParameters.loanPriorEffectiveDate    = pLoanEffectivedate
           calcParameters.loanEndors                = ploanEndors
           calcParameters.lenderCPL                 = "lenderCPL"
           calcParameters.buyerCPL                  = "buyerCPL"
           calcParameters.sellerCPL                 = "sellerCPL"
           calcParameters.ratetypecode              = "ratetypecode"
           calcParameters.loanRateTypecode          = "loanRateTypecode"
           calcParameters.secondloanRateType        = "secondloanRateType"
           calcParameters.secondloanCoverageAmount  = "secondloanCoverageAmount"
           calcParameters.secondLoanEndors          = "secondLoanEndors" 
           . 
           
end.
   
procedure createLoanLoanParameter:
define input parameter pScenarioname       as character no-undo.
define input parameter pVersion            as character no-undo.
define input parameter pRegion             as character no-undo.
define input parameter pPropertytype       as character no-undo.
define input parameter pLoanCoverageamount as character no-undo.
define input parameter pLoanRatetype       as character no-undo.
define input parameter pLoanEffectivedate  as character no-undo.
define input parameter pScndCoverageamount as character no-undo.
define input parameter pScndRatetype       as character no-undo.
define input parameter ploanEndors         as character no-undo.
define input parameter psecondLoanEndors   as character no-undo.
define output parameter pScenarioID        as integer   no-undo.
iScenarioID = iScenarioID + 1 .
pScenarioID = iScenarioID. create calcParameters.
      assign 
           calcParameters.scenarioID                = iScenarioID 
           calcParameters.scenarioName              = pScenarioname
           calcParameters.version                   = pVersion
           calcParameters.region                    = pRegion
           calcParameters.propertyType              = pPropertytype
           calcParameters.simultaneous              = "yes"
           calcParameters.coverageAmount            = "coverageAmount"
           calcParameters.ratetype                  = "ratetype"
           calcParameters.reissueCoverageAmount     = "reissueCoverageAmount"
           calcParameters.PriorEffectiveDate        = "PriorEffectiveDate"
           calcParameters.ownerEndors               = "ownerEndors"
           calcParameters.loanCoverageAmount        = pLoanCoverageamount
           calcParameters.loanRateType              = pLoanRatetype
           calcParameters.loanReissueCoverageAmount = "loanReissueCoverageAmount"
           calcParameters.loanPriorEffectiveDate    = pLoanEffectivedate
           calcParameters.loanEndors                = ploanEndors
           calcParameters.lenderCPL                 = "lenderCPL"
           calcParameters.buyerCPL                  = "buyerCPL"
           calcParameters.sellerCPL                 = "sellerCPL"
           calcParameters.ratetypecode              = "ratetypecode"
           calcParameters.loanRateTypecode          = "loanRateTypecode"
           calcParameters.secondloanRateType        = pScndRatetype
           calcParameters.secondloanCoverageAmount  = pScndCoverageamount
           calcParameters.secondLoanEndors          = psecondLoanEndors 
           . 
           
end.

procedure createOwnerLoanLoanParameter:
define input parameter pScenarioname        as character no-undo.
define input parameter pVersion             as character no-undo.
define input parameter pRegion              as character no-undo.
define input parameter pPropertytype        as character no-undo.
define input parameter pCoverageamount      as character no-undo.
define input parameter pRatetype            as character no-undo.
define input parameter pEffectivedate       as character no-undo.
define input parameter pLoanCoverageamount  as character no-undo.
define input parameter pLoanRatetype        as character no-undo.
define input parameter pLoanEffectivedate   as character no-undo.
define input parameter pScndCoverageamount  as character no-undo.
define input parameter pScndRatetype        as character no-undo.
define input parameter pownerEndors         as character no-undo.
define input parameter ploanEndors          as character no-undo.
define input parameter psecondLoanEndors    as character no-undo.
define output parameter pScenarioID         as integer no-undo.
iScenarioID = iScenarioID + 1 .
pScenarioID = iScenarioID. create calcParameters.
      assign 
           calcParameters.scenarioID                = iScenarioID 
           calcParameters.scenarioName              = pScenarioname
           calcParameters.version                   = pVersion
           calcParameters.region                    = pRegion
           calcParameters.propertyType              = pPropertytype
           calcParameters.simultaneous              = "yes"
           calcParameters.coverageAmount            = pCoverageamount
           calcParameters.ratetype                  = pRatetype
           calcParameters.reissueCoverageAmount     = "reissueCoverageAmount"
           calcParameters.PriorEffectiveDate        = pEffectivedate
           calcParameters.ownerEndors               = pownerEndors
           calcParameters.loanCoverageAmount        = pLoanCoverageamount
           calcParameters.loanRateType              = pLoanRatetype
           calcParameters.loanReissueCoverageAmount = "loanReissueCoverageAmount"
           calcParameters.loanPriorEffectiveDate    = pLoanEffectivedate
           calcParameters.loanEndors                = ploanEndors
           calcParameters.lenderCPL                 = "lenderCPL"
           calcParameters.buyerCPL                  = "buyerCPL"
           calcParameters.sellerCPL                 = "sellerCPL"
           calcParameters.ratetypecode              = "ratetypecode"
           calcParameters.loanRateTypecode          = "loanRateTypecode"
           calcParameters.secondloanRateType        = pScndRatetype
           calcParameters.secondloanCoverageAmount  = pScndCoverageamount
           calcParameters.secondLoanEndors          = psecondLoanEndors 
           . 
           
end.
 
procedure createOwnerEndorsParameter:
define input parameter pScenarioname      as character no-undo.
define input parameter pVersion           as character no-undo.
define input parameter pRegion            as character no-undo.
define input parameter pPropertytype      as character no-undo.
define input parameter pCoverageAmount    as character no-undo.
define input parameter pownerEndors       as character no-undo.
define output parameter pScenarioID       as integer no-undo.
iScenarioID = iScenarioID + 1 .
pScenarioID = iScenarioID. create calcParameters.
      assign 
           calcParameters.scenarioID                = iScenarioID 
           calcParameters.scenarioName              = pScenarioname
           calcParameters.version                   = pVersion
           calcParameters.region                    = pRegion
           calcParameters.propertyType              = pPropertytype
           calcParameters.simultaneous              = "no"
           calcParameters.coverageAmount            = pcoverageAmount
           calcParameters.ratetype                  = "N/A"
           calcParameters.reissueCoverageAmount     = "reissueCoverageAmount"
           calcParameters.PriorEffectiveDate        = "PriorEffectiveDate"
           calcParameters.ownerEndors               = pownerEndors
           calcParameters.loanCoverageAmount        = "loanCoverageAmount"
           calcParameters.loanRateType              = "loanRateType"
           calcParameters.loanReissueCoverageAmount = "loanReissueCoverageAmount"
           calcParameters.loanPriorEffectiveDate    = "loanPriorEffectiveDate"
           calcParameters.loanEndors                = "loanEndors"
           calcParameters.lenderCPL                 = "lenderCPL"
           calcParameters.buyerCPL                  = "buyerCPL"
           calcParameters.sellerCPL                 = "sellerCPL"
           calcParameters.ratetypecode              = "ratetypecode"
           calcParameters.loanRateTypecode          = "loanRateTypecode"
           calcParameters.secondloanRateType        = "secondloanRateType"
           calcParameters.secondloanCoverageAmount  = "secondloanCoverageAmount"
           calcParameters.secondLoanEndors          = "secondLoanEndors" 
           . 
           
end.
procedure createLoanEndorsParameter:
define input parameter pScenarioname          as character no-undo.
define input parameter pVersion               as character no-undo.
define input parameter pRegion                as character no-undo.
define input parameter pPropertytype          as character no-undo.
define input parameter ploanCoverageAmount    as character no-undo.
define input parameter ploanEndors            as character no-undo.
define output parameter pScenarioID           as integer no-undo.
iScenarioID = iScenarioID + 1 .
pScenarioID = iScenarioID. create calcParameters.
      assign 
           calcParameters.scenarioID                = iScenarioID 
           calcParameters.scenarioName              = pScenarioname
           calcParameters.version                   = pVersion
           calcParameters.region                    = pRegion
           calcParameters.propertyType              = pPropertytype
           calcParameters.simultaneous              = "no"
           calcParameters.coverageAmount            = "coverageAmount"
           calcParameters.ratetype                  = "ratetype"
           calcParameters.reissueCoverageAmount     = "reissueCoverageAmount"
           calcParameters.PriorEffectiveDate        = "PriorEffectiveDate"
           calcParameters.ownerEndors               = "ownerEndors"
           calcParameters.loanCoverageAmount        = ploanCoverageAmount
           calcParameters.loanRateType              = "N/A"
           calcParameters.loanReissueCoverageAmount = "loanReissueCoverageAmount"
           calcParameters.loanPriorEffectiveDate    = "loanPriorEffectiveDate"
           calcParameters.loanEndors                = ploanEndors
           calcParameters.lenderCPL                 = "lenderCPL"
           calcParameters.buyerCPL                  = "buyerCPL"
           calcParameters.sellerCPL                 = "sellerCPL"
           calcParameters.ratetypecode              = "ratetypecode"
           calcParameters.loanRateTypecode          = "loanRateTypecode"
           calcParameters.secondloanRateType        = "secondloanRateType"
           calcParameters.secondloanCoverageAmount  = "secondloanCoverageAmount"
           calcParameters.secondLoanEndors          = "secondLoanEndors" 
           . 
           
end.


  
 
 
 

