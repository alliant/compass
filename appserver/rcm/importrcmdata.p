/*------------------------------------------------------------------------
@name importrcmdata.p
@description Loads ratestate along with ratecard
             and its child tables in the database.
@action RCMDataImport             
@throws  3005: Import file does not exist
         3000: RCMDataImport is failed
         2000: RCMDataImport was successful
@author  Anubha
@version 1.0
@created 12/24/2019
@Modified    :
  Date        Name    Comments
 01/03/2020   Anubha  Fixed testing issues
 01/06/2020   Anubha  Fixed testing issue
 03/09/21     SA      Remove hardcoded reference of temp folder code.
 03/08/2022   SR      Task 89909 - Modified nextkey.i
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

/* Import streams */
define stream stRate.
define stream stCard.
define stream stTable.
define stream stRule.
define stream stScenario.
define stream stScenCard.

/* Variable to store input CardSetID  received in request */
define variable piCardSetID       as integer   no-undo.

/* Variables for keeping NEXT primary KEY value */
define variable iCardSetID        as integer   no-undo.
define variable iCardID           as integer   no-undo.
define variable iTableID          as integer   no-undo.
define variable iRuleID           as integer   no-undo.
define variable iScenarioID       as integer   no-undo.

/* Variable for keeping NEXT version of ratestate */
define variable iVersion          as integer   no-undo.

/* Variables for import files full-path */
define variable cFileRateState    as character no-undo.
define variable cFileRateCard     as character no-undo.
define variable cFileRateRule     as character no-undo.
define variable cFileRateTable    as character no-undo.
define variable cFileScenario     as character no-undo.
define variable cFileScenarioCard as character no-undo.

{lib/std-def.i}  
{lib/nextkey-def.i}

define temp-table ttratestate like ratestate.
define temp-table ttratecard  like ratecard.
define temp-table ttraterule  like raterule.
define temp-table ttratetable like ratetable.
define temp-table ttratescenario     like ratescenario.
define temp-table ttratescenariocard like ratescenariocard.

pRequest:getParameter("CardSetID", output piCardSetID).

if piCardSetID = 0 
 then
  do:
     pResponse:fault("3066", "CardSetID cannot be zero.").
     return.
  end.
/* GetSystemParameter provides the temporary directory location  */
publish "GetSystemParameter" ("TempFolder", output std-ch).
if std-ch = ? or std-ch = ""
 then
  std-ch = os-getenv("TEMP").
if std-ch = ? or std-ch = ""  
 then  
  std-ch = os-getenv("TMP").  
  
if std-ch = ? or std-ch = ""  
 then  
  std-ch = "d:\temp\".  
assign
    cFileRateState    =  std-ch + "ratestate"        + string(piCardSetID)  + ".csv"
    cFileRateCard     =  std-ch + "ratecard"         + string(piCardSetID)  + ".csv"
    cFileRateTable    =  std-ch + "ratetable"        + string(piCardSetID)  + ".csv"
    cFileRateRule     =  std-ch + "raterule"         + string(piCardSetID)  + ".csv"
    cFileScenario     =  std-ch + "ratescenario"     + string(piCardSetID)  + ".csv"
    cFileScenarioCard =  std-ch + "ratescenariocard" + string(piCardSetID)  + ".csv"
    .
    
empty temp-table ttratestate.
empty temp-table ttratecard.
empty temp-table ttratetable.
empty temp-table ttraterule.
empty temp-table ttratescenario.
empty temp-table ttratescenariocard.

/* Import for RateState */
file-info:file-name = cFileRateState.

if file-info:file-type ne "FRW"
 then
  do:
     pResponse:fault("3005", "Import directory or file for Ratestate does not exist or not have write permissions").
     return.
  end.
 else
  do:
     input stream stRate from value(cFileRateState).
     
     repeat:
       create ttratestate.
       import stream stRate delimiter "^" ttratestate.  
     end.
     
     input stream stRate close.
  end.

  
/* Import for RateCards, RateRule, RateTable, RateScenario and RateScenarioCards*/  
file-info:file-name = cFileRateCard.

if file-info:file-type ne "FRW"
 then
  do:
     pResponse:fault("3005", "Import directory or file for RateCard does not exist or not have write permissions").
     return.
  end.
 else
  do:
     /* Import for RateCard*/ 
     input stream stCard from value(cFileRateCard).
     
     repeat:
       create ttratecard.
       import stream stCard delimiter "^" ttratecard. 
     end.
     
     input stream stCard close.
          
          
     /* Import for RateTable*/     
     file-info:file-name = cFileRateTable.

     if file-info:file-type eq "FRW"
      then
       do:
          input stream stTable  from value(cFileRateTable).
         
          repeat:
            create ttratetable.
            import stream stTable delimiter "^" ttratetable.  
          end.

          input stream stTable close.
       end.
      else
       pResponse:setParameter("rateTable Import", "Import directory or file for RateTable does not exist or not have write permissions").
     
     /* Import for RateRule*/
     file-info:file-name = cFileRateRule.     
     
     if file-info:file-type eq "FRW"
      then
       do:
          input stream stRule  from value(cFileRateRule).
         
          repeat:
            create ttraterule.
            import stream stRule delimiter "^" ttraterule.  
          end.

          input stream stRule close.
       end.
      else
       pResponse:setParameter("rateRule Import", "Import directory or file for RateRule does not exist or not have write permissions").
       
     
     /* Import for RateScenario*/
     file-info:file-name = cFileScenario.     
     
     if file-info:file-type eq "FRW"
      then
       do:
          input stream stScenario  from value(cFileScenario).
         
          repeat:
            create ttratescenario.
            import stream stScenario delimiter "^" ttratescenario.  
          end.

          input stream stScenario close.
          
          /* Import for RateScenarioCards*/
          file-info:file-name = cFileScenarioCard. 
          
          if file-info:file-type eq "FRW"
           then
            do:
               input stream stScenCard  from value(cFileScenarioCard).
         
               repeat:
                 create ttratescenariocard.
                 import stream stScenCard delimiter "^" ttratescenariocard.  
               end.

               input stream stScenCard close.
            end.
           else
            pResponse:setParameter("rateScenarioCards Import", "Import directory or file for RateScenarioCard does not exist or not have write permissions").
       end.
      else
       pResponse:setParameter("rateScenario Import", "Import directory or file for RateScenario does not exist or not have write permissions").
  end.

/* Reset the value of this handle */
file-info:file-name = ?.

pResponse:setParameter("ratestate", table ttratestate).
pResponse:setParameter("ratecard",  table ttratecard).
pResponse:setParameter("raterule",  table ttraterule).
pResponse:setParameter("ratetable", table ttratetable).
pResponse:setParameter("ratescenario",table ttratescenario).
pResponse:setParameter("ratescenariocard", table ttratescenariocard).
  
std-lo = false.

/* Load data from temp-table to database.*/
TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:
  
  for first ttratestate where ttratestate.cardSetID = piCardSetID:
    
    {lib/nextkey.i &type='ratestate' &var=iCardSetID &err="undo TRX-BLOCK, leave TRX-BLOCK."}
     
    for first ratestate no-lock
      where ratestate.stateID = ttratestate.stateID by ratestate.version desc:
       iVersion = ratestate.version + 1.
    end.
    
    if can-find(first ratestate where ratestate.stateID = ttratestate.stateID
                                  and ratestate.active  = yes) 
     then
      ttratestate.active = no.

     
    if iVersion = 0 
     then 
      iVersion = 1.
    
    /* ratestate table */
    create ratestate.
    buffer-copy ttratestate except cardsetID version lastModifiedDate lastModifiedBy to ratestate.
    assign 
        ratestate.version          = iVersion
        ratestate.cardsetID        = iCardSetID
        .          
     
    for each ttratecard where ttratecard.cardSetID = ttratestate.cardSetID:

      {lib/nextkey.i &type='ratecard' &var=iCardID &err="undo TRX-BLOCK, leave TRX-BLOCK."}
      
      /* ratecard table */
      create ratecard.
      buffer-copy ttratecard except cardID cardsetID lastModifiedDate lastModifiedBy to ratecard.
      assign
          ratecard.cardID    = icardID
          ratecard.cardsetID = iCardSetID
          .
       
      /* Replacing exported card ID in ratescenariocard temp-table with the new generated card ID for the ratecard */ 
      for each ttratescenariocard where ttratescenariocard.cardID = ttratecard.cardID:
        ttratescenariocard.cardID = iCardID.       
      end.

      for each ttratetable where ttratetable.cardID = ttratecard.cardID:
       
        {lib/nextkey.i &type='ratetable' &var=iTableID &err="undo TRX-BLOCK, leave TRX-BLOCK."}
         
        /* ratetable table */ 
        create rateTable.
        buffer-copy ttratetable except cardID rateTableID lastModifiedDate lastModifiedBy to rateTable.
        assign 
            rateTable.cardID           = icardID
            rateTable.rateTableID      = iTableID
            rateTable.lastModifiedDate = now
            rateTable.lastModifiedBy   = pRequest:UID
            .
        
        validate rateTable.
        release  rateTable.
        
      end. 
       
      for each ttraterule where ttraterule.cardID = ttratecard.cardID:
       
        {lib/nextkey.i &type='raterule' &var=iRuleID &err="undo TRX-BLOCK, leave TRX-BLOCK."}
        
        /* raterule table */ 
        create raterule.
        buffer-copy ttraterule except cardID rateRuleID lastModifiedDate lastModifiedBy to raterule.
        assign
            raterule.cardID           = icardID
            raterule.rateRuleID       = iRuleID
            raterule.lastModifiedDate = now
            raterule.lastModifiedBy   = pRequest:UID
            .
        
        validate rateRule.
        release  rateRule.
        
      end.
      
      assign
          ratecard.lastModifiedDate = now
          ratecard.lastModifiedBy   = pRequest:UID
          .
          
      validate ratecard.
      release  ratecard.
    end. /* for each ttratecard */
    
    for each ttratescenario where ttratescenario.cardSetID = ttratestate.cardSetID:
      
      {lib/nextkey.i &type='ratescenario' &var=iScenarioID &err="undo TRX-BLOCK, leave TRX-BLOCK."}
      
      /* ratescenario table */
      create ratescenario.
      buffer-copy ttratescenario except scenarioID cardSetID createdDate to ratescenario.
      assign
          ratescenario.scenarioID  = iScenarioID
          ratescenario.cardSetID   = iCardSetID
          ratescenario.createdDate = now
          .
          
      for each ttratescenariocard where ttratescenariocard.scenarioID = ttratescenario.scenarioID:
        
        /* ratescenariocard table */
        create ratescenariocard.
        buffer-copy ttratescenariocard except scenarioID createdDate to ratescenariocard.
        assign
            ratescenariocard.scenarioID  = iScenarioID
            ratescenariocard.createdDate = now
            .   
            
        validate ratescenariocard.
        release  ratescenariocard.
      end.
      
      validate ratescenario.
      release  ratescenario.
    
    end. /* for each ttratescenario */
     
    assign
        ratestate.lastModifiedDate = now
        ratestate.lastModifiedBy   = pRequest:UID
        .
            
    validate ratestate.
    release  ratestate. 

  end.  /* for first ttratestate */
   
  std-lo = true.
end.

if pResponse:isFault() 
 then
  return.

if not std-lo
 then
  do:
     pResponse:fault("3000", "RCMDataImport").
     return.
  end.


pResponse:success("2000", "RCMDataImport").
