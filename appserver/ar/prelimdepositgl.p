/*------------------------------------------------------------------------
@name prelimdepositgl.p
@action depositGlPrelim 

@returns glpayment
@author Anjly
@version 1.0
@created 07/16/2020
Modification:
Date          Name      Description
03-24-2021    SA        Edit field phase
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable piDepositID   as integer   no-undo.
define variable iCount        as integer   no-undo.

{lib/std-def.i}
{lib/ar-def.i}

{tt/ledgerreport.i &tableAlias="glpaymentdetail"}
{tt/ledgerreport.i &tableAlias="gldeposit"}
{tt/ledgerreport.i &tableAlias="tempglpayment"}

pRequest:getParameter("depositID",   output piDepositID).

if pResponse:isFault()
 then return.
 
for each arpmt fields(arPmtID) where arpmt.depositID = piDepositID:
    run ar\getgl.p(input {&PostPayment},
                          input arpmt.arPmtID,
						  input 0,
                          input ?,
                          input 0,
                          output table tempglpayment).
                                
     for each tempglpayment:
       create glpaymentdetail.
       buffer-copy  tempglpayment to glpaymentdetail.
     end.
     empty temp-table tempglpayment.
 end.

run ar\getgl.p(input {&PostDeposit},
                      input piDepositID,
					  input 0,
                      input ?,
                      input 0,
                      output table gldeposit).
                    
pResponse:setParameter("glpaymentdetail", table glpaymentdetail).
pResponse:setParameter("gldeposit", table gldeposit).
 
pResponse:success("2000", "glpayment").


