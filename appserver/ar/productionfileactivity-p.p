/*------------------------------------------------------------------------
@name productionfileactivity-p.p
@description Get data of all production files activity

@returns Success;int;3005
@author S Chandu
@version 1.0 11/08/2024
@notes
@modified 
Date          Name          Description

----------------------------------------------------------------------*/
def input parameter pRequest  AS framework.IRequestable.
def input parameter pResponse AS framework.IRespondable.

{tt/fileAr.i &tableAlias="ttFileAR"}
{lib/std-def.i}
{lib/callsp-defs.i }

/* Variables for parameter */
define input parameter ipcAgentList     as character no-undo.
define input parameter ipdtStartDate    as datetime  no-undo.
define input parameter ipdtEndDate      as date      no-undo.
define output parameter table for ttFileAR.
define output parameter oplSuccess      as logical   no-undo.
define output parameter opcMsg          as character no-undo.

 
{lib\callsp.i 
      &name=spGetProductionFileActivity 
      &load-into=ttFileAR 
      &params="input ipcAgentList, input ipdtStartDate, input ipdtEndDate"
      &noResponse=true &nocount=true}
if not csp-lSucess and csp-cErr > ''
 then
  do:
    pResponse:fault("3005", csp-cErr).
    return.    
  end.

assign oplSuccess =  true.


