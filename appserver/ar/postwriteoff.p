/*------------------------------------------------------------------------
@name ar/postwriteoff.p
@action writeoffPost
@description writeoff generated from artran table are posted to arTran table and
             create a "W" type record for posted writeoff

@author Shefali 
@version 1.0
@created 05/04/2021                           
@Modified    :
Date        Name     Comments   

07/26/2021  SB       Task 84781 Error message should appear if Write Off is not posted successfully.
09/09/2021  SB       Task 86366 GL Account Validation Check
03/04/2022  SD       Task 89909 - Modified nextkey.i
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable dtpostdate       as date      no-undo.
define variable totamt           as decimal   no-undo.
define variable iArTranID        as integer   no-undo.
define variable iSeq             as integer   no-undo.
define variable lViewReport      as logical   no-undo.
define variable iwriteoffID      as integer   no-undo.
define variable iwoffID          as integer   no-undo.
define variable iLedgerID        as integer   no-undo.
define variable lLocked          as logical   no-undo.
define variable lSuccess         as logical   no-undo.
define variable cErrorMsg        as character no-undo.
define variable cAgentsList      as character no-undo.
define variable cFilesList       as character no-undo.
define variable cFileNumber      as character no-undo.
define variable iCount           as integer   no-undo.
define variable iRptCount        as integer   no-undo.
define variable cFileName        as character no-undo.
define variable cDestinationList as character no-undo.

define buffer bfArTran for artran.

/* Standard lib files */
{lib/std-def.i}
{lib/nextkey-def.i}
{lib/ar-def.i}
{lib/get-os-error.i}
{lib/add-delimiter.i}
{lib/normalizefileid.i}
{lib/arc-repository-procedures.i}

/* Temp-table definition */
{tt/ledgerreport.i &tableAlias="glwriteoff"}
{tt/ledgerreport.i &tableAlias="glwriteoffreport"} /* for pdf generation */
{tt/artran.i       &tableAlias="ttARtran"}
{tt/repository.i}

empty temp-table ttARtran.

pRequest:getParameter("artran", input-output table ttARtran).
pRequest:getParameter("ViewReport", output lViewReport ).
pRequest:getParameter("postdate", output dtpostdate).

if dtpostdate = ?
 then
  do:    
    pResponse:fault("3005", "Post date date cannot be blank").
    return.    
  end.

if date(dtpostdate) > today
 then
  do:    
    pResponse:fault("3005", "Post date cannot be in the future").
    return.    
  end.

for first period no-lock 
  where period.periodMonth = month(date(dtpostdate))  
    and period.periodYear = year(date(dtpostdate)):                                 
  std-lo = period.active.        
end.

if not std-lo
 then
  do:
    pResponse:fault("3020", "Period" {&msg-add} "Closed").
    return.
  end. 
  
for each ttARtran no-lock 
  break by ttARtran.entityID by ttARtran.fileID:
   if first-of(ttARtran.entityID) 
    then 
     do:   
       if can-find(first agent where agent.agentID       = ttARtran.entityID 
                                and (agent.ARGLRef       = "" 
                                or agent.ARGLRef         = ?
                                or agent.arwriteoffglref = ""
                                or agent.arwriteoffglref = ?))
         then
          do:
            if cAgentsList = "" 
             then 
              cAgentsList = ttARtran.entityID.  
             else
              cAgentsList = cAgentsList + "," + ttARtran.entityID.
          end.
     end.
     
   if not can-find(first artran where artran.artranID = ttARtran.artranID and artran.remainingAmt = ttARtran.remainingAmt)
   then
    do: 
      if cFilesList = "" 
       then 
        cFilesList = ttARtran.fileId.  
       else
        cFilesList = cFilesList + "," + ttARtran.fileId.
    end.
end.
  
if cAgentsList ne "" 
 then
  do:
    pResponse:fault("3005", "GL Accounts are not set for Agent(s) " + cAgentsList).
    return.
  end.

if cFilesList ne "" 
 then
  do:
    pResponse:fault("3005", "Write-off for files(s) " + cFilesList + " failed as write-off amount and file amount is not same").
    return.
  end.

/* When posting is locked by any user */
{lib/islocked.i &obj='arPosting' &id='' &exclude-fault=TRUE}
if std-lo
 then
  do: 
    pResponse:fault("3081", "A/R Posting" {&msg-add} std-ch).
    return.
  end.
  
run util/newsyslock.p ("arPosting", "", 0, pRequest:uid, output lLocked).
if not lLocked
 then
  do: 
    pResponse:fault("3000", "Lock").
    return.
  end.
         
TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:
  for each ttARtran no-lock 
    break by ttARtran.entityID by ttARtran.fileID:
    
    if first-of(ttARtran.entityID) 
     then
      do:
        totamt = 0.
        for first artran exclusive-lock 
          where arTran.entity    = {&Agent}  
            and arTran.entityID  = ttARtran.entityID  
            and arTran.fileID    = ttARtran.fileID 
            and artran.transtype = {&FILE} 
            and arTran.seq       = 0:
        
 		  iwoffID = 0.
          {lib/nextkey.i &type='arwriteoff' &var=iwoffID &err="undo TRX-BLOCK, leave TRX-BLOCK."}
          
          if can-find(first arwriteoff where arwriteoffID = iwoffID)
           then 
            do:
              pResponse:fault("3005", "Duplicate value for key arwriteoff in database.").
              undo TRX-BLOCK, leave TRX-BLOCK.
            end.
            
          /*create a new record in writeoff table*/   
          create arwriteoff.
          assign 
              arwriteoff.arWriteoffID   = iwoffID
              arwriteoff.Entity         = {&Agent}
              arwriteoff.EntityID       = arTran.EntityID
              arwriteoff.tranDate       = date(dtpostdate) /*Post Date as selected by users*/
              arwriteoff.CreatedBy      = pRequest:uID
              arwriteoff.createdDate    = now
              arwriteoff.postBy         = pRequest:uID
              arwriteoff.postDate       = now
              arwriteoff.Void           = arTran.Void
              arwriteoff.VoidDate       = arTran.VoidDate
              arwriteoff.VoidBy         = arTran.VoidBy
              arwriteoff.LedgerID       = arTran.LedgerID
              arwriteoff.voidLedgerID   = arTran.voidLedgerID
              arwriteoff.type           = if ttartran.remainingamt > 0 then {&Debit} else {&Credit}
              arwriteoff.source         = {&FILE}
              .
                  
        end.
         
        iArTranID = 0.
        {lib/nextkey.i &type='artran' &var=iArTranID &err="undo TRX-BLOCK, leave TRX-BLOCK."}
         
        /*-----------------create artran for writeoff record of seq 0 --------------------*/
         
        create artran.
        assign
            artran.artranID     = iArTranID
            artran.tranID       = string(arwriteoff.arwriteoffID)
            artran.tranDate     = arwriteoff.tranDate  /*Post Date as selected by users */ 
            artran.type         = {&Writeoff}
            artran.transtype    = ""
            artran.entity       = arwriteoff.entity
            artran.entityID     = arwriteoff.entityID
            artran.remainingAmt = 0
            artran.void         = false
            artran.voidDate     = ?
            artran.voidBy       = ""
            artran.notes        = "Writeoff Posted."
            artran.createdBy    = pRequest:uID
            artran.createdDate  = now
            artran.postBy       = pRequest:uID
            artran.postDate     = now
            artran.fullypaid    = YES
            artran.reference    = string(arwriteoff.arwriteoffID)
            iwriteoffID         = iArTranID
            artran.seq          = 0
            .
      end. 
    
    totamt = totamt + ttARtran.remainingamt.
    if last-of(ttARtran.entityID) 
     then
      do:
        for last arwriteoff exclusive-lock where arwriteoff.entityID = ttARtran.entityID :
          arwriteoff.tranamount = totamt.
          find last artran exclusive-lock 
            where artran.type   = {&Writeoff} 
              and artran.seq    = 0          
              and artran.tranID = string(arwriteoff.arwriteoffID) no-error.
          if avail(artran) 
           then
            artran.tranAmt        = arwriteoff.tranamount.
           
        end.
      end.
  end. /* for each ttARtran no-lock */
   
  for each ttARtran no-lock 
    by ttARtran.entityID by ttARtran.fileID:
    for last arwriteoff no-lock
      where arwriteoff.entityID = ttARtran.entityID:
      for last artran fields(artranID tranID) no-lock 
        where artran.entityID = arwriteoff.entityID 
          and artran.type = {&Writeoff} 
          and artran.tranID = string(arwriteoff.arWriteoffID) 
          and artran.seq    = 0:
        assign 
            iwriteoffID = integer(artran.arTranID)
            iwoffID     = integer(artran.tranID)
            .
      end.
    end.
       
    for last artran exclusive-lock
      where artran.entity    = {&Agent}               
        and artran.entityID  = ttARtran.entityID 
        and artran.fileID    = ttARtran.fileID   
        and artran.transtype = {&File}
        by seq:
      assign 
          cFileNumber = artran.filenumber
          iSeq        = artran.seq + 1
          .         
    end.

    for first artran exclusive-lock
      where artran.entity    = {&Agent}               
        and artran.entityID  = ttARtran.entityID 
        and artran.fileID    = ttARtran.fileID   
        and artran.transtype = {&File} 
        and seq              = 0:
      artran.remainingAmt = 0.
    end.

    iArTranID = 0.
    {lib/nextkey.i &type='artran' &var=iArTranID &err="undo TRX-BLOCK, leave TRX-BLOCK."} 
   
 
    /*-----------------create the apply recod of writeoff-------------------------*/
    create artran.
    assign
        artran.artranID     = iArTranID
        artran.seq          = iSeq
        artran.sourcetype   = {&Writeoff}
        artran.type         = {&Apply}
        artran.transtype    = {&File}
        artran.entity       = {&Agent}
        artran.entityID     = ttartran.entityID
        artran.sourceID     = string(iwriteoffID)
        artran.fileNumber   = cFileNumber 
        artran.tranAmt      = (-1 * ttARtran.remainingAmt)
        artran.remainingAmt = 0
        artran.notes        = "Writeoff applied."
        artran.createdBy    = pRequest:uID
        artran.createdDate  = now
        artran.postBy       = pRequest:uID
        artran.postDate     = now
        artran.fullyPaid    = yes
        artran.fileID       = ttartran.fileID
        artran.reference    = string(iwoffID)
        artran.tranDate     = date(dtpostdate)
        .
  end. /*for each ttARtran no-lock */
   
  for each ttARtran no-lock 
    break by ttARtran.entityID by ttARtran.fileID:
    if first-of(ttARtran.entityID) 
     then 
      do:
    
        for last arwriteoff exclusive-lock where arwriteoff.entityID = ttARtran.entityID:
          /*-------Writeoff entry to Ledger-----------*/      
          run ar/getgl.p (input {&PostWriteOff},
                          input arwriteoff.arWriteoffID,
		     	   	      input 0,
                          input date(arwriteoff.tranDate),
                          input 0,
                          output table glwriteoff).

          run ar/ledgerentry.p(input {&Writeoff},
                               input table glwriteoff,
                               input pRequest:uid,
                               output iLedgerID). 
                           
          empty temp-table  glwriteoff. 
                                  
          run ar/postgl.p(input {&PostWriteOff},
                          input iLedgerID,
                          output table glwriteoff).
      
          /* Temp-table storing GL payment records used to generate gl writeoff report */
          for each glwriteoff:
            create glwriteoffreport.
            buffer-copy glwriteoff to glwriteoffreport.
            iRptCount = iRptCount + 1.        
          end.
            
          assign
              arwriteoff.postBy    = pRequest:uID                                                                                         
              arwriteoff.postDate  = now
              arwriteoff.ledgerID  = iLedgerID
              arwriteoff.tranDate  = date(dtpostdate)
              .
         
          std-in = std-in + 1.
        end. /* for last arwriteoff */ 
      end.
  end. 
  std-lo = true.   
end. 

/* Release lock on posting */
run util/deletesyslock.p ("arPosting", "", 0, output lLocked).

/* when lock is not released */  
if not lLocked
 then 
  do:
    pResponse:fault("3000", "A/R Posting lock release").  
    return.
  end.
  
if pResponse:isFault()
 then
  return.
  
/* Sending generated pdf gl report to output destination setup in sysdest */
if iRptCount > 0
 then
  do:                              
    /* Getting list of system destination set for an agent */
    {lib/get-sysdest.i &entityType='C' &entityID='' &action=pRequest:actionid &outdest=cDestinationList}
    
    if cDestinationList = ""
     then
      run createSysAppLog (input pRequest:actionID,
                           input "System destination not set",
                           input "Default agent type system destination is not set for the action " + pRequest:actionID).
                           
     else
      do std-in = 1 to num-entries(cDestinationList, {&msg-dlm}):
        std-ch = entry(std-in, cDestinationList, {&msg-dlm}). /* Destination name:value pair */
        case entry(1, std-ch, "="):
          when "E" then
           do:
             /* Generate pdf gl report for the posted  write-offs */
             run util/arwriteoffpdf.p (input {&agentdefault},
                                      input table glwriteoffreport,
                                      output cFileName).
             /* send the email */
             run util/attachmail.p ("no-reply@alliantnational.com",   /* from loggedIN user*/
                                    entry(2, std-ch, "="),            /* To loggedIN use*/
                                    "",                               /* CC */
                                    "Posted Write-off GL Report", /* Subject */
                                    "",                               /* Body */
                                    cFileName).                       /* Attachment */ 
                                    
             /* Delete the generated report from temporary dir */
             os-delete value(cFileName) no-error.                       
           end.                         
           
          when "F" then
           do:
             /* Generate pdf gl report for the posted  write-offs */
             run util/arwriteoffpdf.p (input {&agentdefault},
                                      input table glwriteoffreport,
                                      output cFileName).
                                      
             os-copy value(cFileName) value(entry(2, std-ch, "=")).
             if os-error > 0
              then
               run createSysAppLog (input pRequest:actionID,
                                    input "System destination failed",
                                    input "Report " + cFileName + " not copied to " + entry(2, std-ch, "=") + ". The error is: " + getError(os-error)).
                                    
             /* Delete the generated report from temporary dir */
             os-delete value(cFileName) no-error.                                      
           end.
          when "S" then           
           if entry(2, std-ch, "=") = "A" /* Store to Arc Repository */
            then  
             do:               
               /* Generate pdf gl report for the posted write-off's */
               run util/arwriteoffpdf.p (input {&agentdefault},
                                        input table glwriteoffreport,
                                        output cFileName).
                                        
            
               
               run UploadFile in this-procedure (pRequest:uid, true, "AR", string(iLedgerID), "Reports", cFileName, output table repository, output lSuccess, output cErrorMsg).
             
               if not lSuccess
                then       
                 run createSysAppLog (input pRequest:actionID,
                                      input "System destination failed",
                                      input "Report " + cFileName + " not saved to Arc Repository. The error is: " + cErrorMsg).
                                      
               /* Delete the generated report from temporary dir */
               os-delete value(cFileName) no-error. 
             end.               
        end case. 
      end. /* do std-in = 1 to num-entries(cDestinationList, {&msg-dlm}): */ 
  end.
  
if not std-lo 
 then
  do:
     pResponse:fault("3002", " Write-off").
     return.
  end.
  
if iRptCount > 0 and lViewReport
 then
  pResponse:setParameter("glwriteoff", table glwriteoffreport).
  
pResponse:success("2010", string(std-in) + " Write-offs").

PROCEDURE createSysAppLog:
  define input parameter pcAction   as character no-undo. 
  define input parameter pcName     as character no-undo. 
  define input parameter pcErrorMsg as character no-undo.
  
  run util/newsysapplog.p (input  pRequest:clientID,   /* AppCode: ARM */
                           input  "C",                 /* EntityType: C (Company) */
                           input  "",                  /* EntityID */
                           input  pcAction,            /* ObjAction */
                           input  1,                   /* ObjID: Unique keys */
                           input  pcName,              /* ObjName */
                           input  pcErrorMsg,          /* ObjDesc: Error desc */
                           input  '',                  /* ObjRef: bej@bellsouth.net */
                           input  pRequest:uid,        /* Uid */                     
                           output std-lo).             /* Success */
end PROCEDURE.






























