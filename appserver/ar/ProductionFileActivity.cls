/*-----------------------------------------------------------------------
@name  ProductionFileActivity.cls
@action ProductionFileActivity
@description  Get production file actvity reports

@author  Chandu 
@Modified :
Date        Name          Comments    
11/20/2024  K.R           Modified to add notify requestor
----------------------------------------------------------------------*/
USING Progress.Json.ObjectModel.ObjectModelParser.
USING progress.Json.ObjectModel.*.

class ar.ProductionFileActivity inherits framework.ActionBase:

  /*Temp-Table */
  define temp-table AgentList
       fields agentID as character.

  {lib/ar-def.i}
  {lib/callsp-defs.i }
  {tt/fileAr.i &tableAlias="ttFileAR"}

  constructor public ProductionFileActivity ():
    super().
  end constructor.

  destructor public ProductionFileActivity ( ):
  end destructor.

  {framework/sysmailparameter.i &tableAlias=mailparameter}

  /* Global Variables*/
  define variable pResponse         as framework.IRespondable no-undo.
  define variable pRequest          as framework.IRequestable no-undo.
  
  /* Variables for parameter */
  define variable ipcAgentID           as character no-undo.
  define variable ipcAgentList         as character no-undo.
  define variable ipdtStartDate        as datetime  no-undo.
  define variable ipdtEndDate          as date      no-undo.
  define variable iplPrintEmptyFile    as logical   no-undo.
  
  define variable opcFileName          as character no-undo.
  define variable chErrorMsg           as character no-undo.
  define variable chAgentID            as character no-undo.
  define variable chAgentName          as character no-undo.
  define variable chAgentAddr          as character no-undo.
  define variable chAgentManager       as character no-undo.
  define variable chAgentManagerName   as character no-undo.

  method public override void act (input pActRequest  as framework.IRequestable,
                                   input pActResponse as framework.IRespondable):

    {lib/std-def.i}

    assign 
        pResponse = pActResponse
        pRequest  = pActRequest
        .

 
    /* local variables */
    define variable dBalForward          as decimal   no-undo.
    define variable iErrorCode           as integer   no-undo.
    define variable iDestCount           as integer   no-undo.
    define variable hTable               as handle    no-undo.
    
    define variable oplSuccess           as logical   no-undo.
    define variable opcMsg               as character no-undo.

    /*dataset handle */
    define variable dsHandle             as handle no-undo.
  
    
    pRequest:getParameter("AgentList",     output ipcAgentList).
    pRequest:getParameter("StartDate",     output ipdtStartDate).
    pRequest:getParameter("EndDate",       output ipdtEndDate).
    pRequest:getParameter("PrintEmptyFile", output iplPrintEmptyFile).

    /* dataset creation */
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer AgentList:handle).
	
    /* 'View' option or 'do it' option */
    if ipcAgentList = '' or ipcAgentList = ?
     then
      do:
        ipcAgentList = ''.
        empty temp-table AgentList.
    
        pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
        if std-ch > ''
         then
          do:
            pResponse:fault("3005", "Data Parsing failed. Reason: " + std-ch).
            return.
          end.
    
        /* Creating agent Id List*/
        for each AgentList no-lock:
         ipcAgentList = ipcAgentList + "," + AgentList.agentID.
        end.
        ipcAgentList = trim(ipcAgentList, ",").
    end.

    if pResponse:isFault()
     then
      return.  

    run ar/productionfileactivity-p.p(input pRequest,
                                      input pResponse,
                                      input ipcAgentList,
                                      input ipdtStartDate,
                                      input ipdtEndDate,
                                      output table ttFileAR,
                                      output oplSuccess,
                                      output opcMsg
                                       ).
     

    if oplSuccess and can-find(first ttFileAR) then
     do:
 
     end.
    for each ttFileAR:
        std-in = std-in + 1.
    end.

    for first agent fields(agentID name addr1 addr2 city state zip) no-lock
      where agent.agentID = trim(string(ipcAgentList),","):  
      assign
          chAgentID   = agent.agentID
          chAgentName = agent.name
          chAgentAddr = (if agent.addr1  <> "" then agent.addr1  + ";" else "") +
                        (if agent.addr2  <> "" then agent.addr2  + ";" else "") +
                        agent.city + ", " + agent.state + " " + agent.zip.

       
      for first agentManager fields(UID) no-lock
        where agentmanager.agentID   = agent.agentID
          and agentmanager.stat      = "A"
          and agentmanager.isPrimary = true:
        chAgentManager =  agentmanager.UID.
        
        for first sysuser fields(name) no-lock
          where sysuser.UID = agentmanager.UID:
          chAgentManagerName = sysuser.name.
        end.   
      end.
    end. 
  
    if (can-find(first ttFileAR) or iplPrintEmptyFile)
     then
      do:
       generateFile().
       
       if chErrorMsg <> ""
        then 
         do:
           pResponse:fault("3005", chErrorMsg).
           return.
         end.  
       else
        tContentFile = opcFileName.
      end.

 
    
    pResponse:success("2005", string(std-in) {&msg-add} "fileactivity").
      
  end method.

  method public override logical emailContent(input pAddr as character,
                                              input pSubject as character,
                                              input pTemplate as character,
                                              output pMsg as character  ):


    define variable iCounter         as integer   no-undo.
    define variable cAttachment      as character no-undo.
    define variable cDefaultTemplate as character no-undo.
    define variable cFrom            as character no-undo. 
    define variable lMailSent        as logical   no-undo.
    define variable cErrorPDF        as character no-undo.
    define variable cErrorCSV        as character no-undo.

    file-info:filename = opcFileName.
    if file-info:full-pathname = ?
     then 
      do:
        cErrorPDF = "Production Activity CSV not found for agent: " + chAgentID.
        run util/sysmail.p("Emailing file failed",
                               cErrorPDF).
        return false.
      end.

      SendEMail(pAddr,pSubject,pTemplate, opcFileName , output pMsg).


    if pMsg > "" 
     then return false.
    else return true.
  end.

  method private character GetSysDestintion (input pcEntityType as character,
                                             input pcEntityID   as character,
                                             input pcAction     as character):

    define variable cAllDestination as character no-undo.

    if not can-find(first sysdest where sysdest.entityType = pcEntityType
                                    and sysdest.entityID   = pcEntityID        
                                    and sysdest.action     = pcAction) 
     then
       for each sysdest no-lock
         where sysdest.entityType = pcEntityType
           and sysdest.entityID   = ""
           and sysdest.action     = pcAction:
         cAllDestination = cAllDestination + sysdest.destType + "=" + sysdest.destName + "^".
       end.
     else
       for each sysdest no-lock
         where sysdest.entityType = pcEntityType
           and sysdest.entityID    = pcEntityID
           and sysdest.action     = pcAction:

         cAllDestination = cAllDestination + sysdest.destType + "=" + sysdest.destName + "^".

       end.
    return trim(cAllDestination,"^").
  end method.

   method private void SendEMail (input pAddr       as character,
                                  input pSubject    as character,
                                  input pTemplate   as character,
                                  input pAttachment as character,
                                  output pMsg       as character
                                  ):

    define variable lSuccess         as logical   no-undo.
    define variable cDefaultTemplate as character no-undo.
    define variable cFrom            as character no-undo.

    if pAddr <> ""
     then
     do:
      if not validEmailAddr(pAddr)
       then
        do:
          pResponse:fault("3005",  "Email Address '" + pAddr + "' is invalid for AgentID '" + chAgentID + "'").
          return.
        end.
     end.

    pTemplate = search(pTemplate).
    file-info:filename = pTemplate.
    if file-info:full-pathname = ?
     then
      do:
        publish "GetSystemParameter" from (SESSION:first-procedure) ("emailDefaultTemplate", output cDefaultTemplate).
        pTemplate = search(cDefaultTemplate).
      end.
    if pSubject = "" or pSubject = ? 
     then pSubject = "Production Activity from Alliant National".

    publish "GetSystemParameter" from (SESSION:first-procedure) ("emailDefaultFrom", output cFrom).

    find first mailparameter where mailparameter.fieldName = "action" no-error.
    if not avail mailparameter then
     do:
        create mailparameter.
        assign 
            mailparameter.fieldname  = "action".
     end.
    assign mailparameter.fieldValue = "Production Activity".
    
     run framework/sendsyshtmlemail.p(input pTemplate,
                                      input cFrom,
                                      input pAddr,
                                      input pSubject,
                                      input pAttachment,
                                      input table mailparameter,
                                      output lSuccess).
    
      if not lSuccess
       then 
        do: 
          pMsg =  "Email failed to " + pAddr + " for Production Activity " + opcFileName.
           run util/sysmail.p ("Emailing file failed",
                               "Action:" + pRequest:ActionID + "<br>" + pMsg).
        end.
  end method.

  method public override logical storeContent(input pUID as character,
                                              output pMsg as character):

    define variable cErrorPDF as character no-undo.

    file-info:filename = opcFileName.
    if file-info:full-pathname = ?
     then
      do:
        cErrorPDF = "Production Activity CSV not found".
        run util/sysmail.p("Saving production Activty file to repository failed",
                           cErrorPDF).
      end.
    else
      StoreProductionActivity(pUID, opcFileName, output cErrorPDF).  

    pMsg = cErrorPDF.

    if trim(pMsg,",") > "" 
     then return false.
    else return true.

  end method.

  method private void StoreProductionActivity (input pUID                     as character,
                                               input cProductionActivityFile  as character,
                                               output pMsg                    as character):


    define variable cStorageEntity              as character no-undo.
    define variable cStorageID                  as character no-undo.
    define variable cStorageCategory            as character no-undo.
    define variable lProductionActivityStored   as logical   no-undo.
    define variable iCount                      as integer   no-undo.
    
    do iCount = 1 to num-entries(ipcAgentList,","):
      assign 
          cStorageEntity   = "Production"
          cStorageID       = entry(iCount,ipcAgentList,",")
          cStorageCategory = "FileActivity".
	  
      run util\UploadFile.p (pUID, true, cStorageEntity, cStorageID, cStorageCategory, cProductionActivityFile, output lProductionActivityStored, output pMsg).
	  
      if not lProductionActivityStored 
       then
        do:
          pMsg = "Saving file " + cProductionActivityFile + " to repository failed with error " + pMsg.
          run util/sysmail.p ("Saving file to repository failed",
                              "Action:" + pRequest:actionID + "<br>" + pMsg).
        end.
    end.
  end method.

  method public override void queue (input pRequest as framework.IRequestable, input pResponse as framework.IRespondable):
  
    define variable cDestination       as character  no-undo.
    define variable cParamterList      as character  no-undo.
    define variable dsHandle           as handle     no-undo.
    define variable std-ch             as character  no-undo.
    define variable iCount             as integer    no-undo.
    define variable cAllDestination    as character  no-undo.
    define variable cALLParamterList   as character  no-undo.
    define variable cAgentList         as character  no-undo.

    define variable cBoolenDestination as character no-undo.
    define variable cEmailAddrList     as character no-undo.
    define variable cPrinterAddress    as character no-undo.

    /*To send email alert when destination is not set or have invalid destinations for agents*/
    define variable tToEmail           as character no-undo.
    define variable lNoSysDest         as logical   no-undo.
    define variable cFaultAgentID      as character no-undo.
    define variable cAlertMsg          as character no-undo.

    /*To store destination values*/
    define variable iDestCount         as integer   no-undo.
    define variable cDestKeyVal        as character no-undo.
    define variable cDestKey           as character no-undo.
    define variable cDestVal           as character no-undo.
    define variable cDestEmail         as character no-undo.
    define variable cDestPrinter       as character no-undo.
    define variable cDestFileStored    as character no-undo.
    define variable cDestStorage       as character no-undo.
    define variable cURLStorage        as character no-undo.

    /*To store fault dest Agents and destinations*/
    define variable cfltEmailAgents    as character no-undo.
    define variable cfltEmails         as character no-undo.
    define variable cfltPrinterAgents  as character no-undo.
    define variable cfltPrinters       as character no-undo.
    define variable cfltFolderAgents   as character no-undo.
    define variable cfltFolders        as character no-undo.
    define variable cfltStorageAgents  as character no-undo.
    define variable lInvalidDest       as logical   no-undo.
    define variable cinvalidDestAgents as character no-undo.
    
    pRequest:getParameter("StartDate",     output ipdtStartDate).
    pRequest:getParameter("EndDate",       output ipdtEndDate).
    pRequest:getParameter("PrintEmptyFile", output iplPrintEmptyFile).

    cBoolenDestination = pRequest:GetDestination().

    /* dataset creation */
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer AgentList:handle).
	
    empty temp-table AgentList.
    
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > ''
     then
      do:
        pResponse:fault("3005", "Data Parsing failed. Reason: " + std-ch).
        return.
      end.
    
    /* Creating agent Id List*/
    for each AgentList no-lock:
      assign cAgentList = cAgentList + AgentList.agentID + ",".
    end.
    cAgentList = trim(cAgentList, ",").

    for each AgentList no-lock:

      assign
        cDestEmail      = ""
        cDestPrinter    = ""
        cDestFileStored = ""
        cDestStorage    = ""
        .
		
      /*Configured option is selected*/
      if cBoolenDestination = "C" then
       do:
         assign cDestination = GetSysDestintion("G", AgentList.agentID, pRequest:actionId).
	     
         /*no destination is set*/
         if cDestination = "" or cDestination = ?
          then
           do:
             lNoSysDest    = true.
             cFaultAgentID = cFaultAgentID + (if cFaultAgentID > "" then "," else "") + AgentList.agentID. /* Agents whose destination not set */
             next.
           end.
         else
          do:
            assign cDestination = GetSysDestintion("G", AgentList.agentID, pRequest:actionId).
	     
            /*Check configured destination is valid or not*/
            cDestination = trim(cDestination,"^").
	        
            do iDestCount = 1 to num-entries(cDestination,"^"):
              assign
                   cDestKeyVal = entry(iDestCount, cDestination, "^")
                   cDestKey    = entry(1, cDestKeyVal, "=")
                   cDestVal    = entry(2, cDestKeyVal, "=")
                   .
              
              case cDestKey:
                when 'E' then cDestEmail      = cDestVal.
                when 'P' then cDestPrinter    = cDestVal.
                when 'F' then cDestFileStored = cDestVal.
                when 'S' then cDestStorage    = cDestVal.  
              end case.
            end.
		    
            if cDestEmail <> "" and not(validEmailAddr(cDestEmail))
             then 
              assign
                  cfltEmailAgents = cfltEmailAgents + (if cfltEmailAgents > "" then "," else "") + AgentList.agentID
                  cfltEmails        = cfltEmails + (if cfltEmails > "" then "," else "") + cDestEmail
                  .
		    
            if cDestPrinter <> "" and not can-find(first sysprop  
                where sysprop.appCode     = {&printappcode}     
                  and sysprop.objAction   = {&printobjAction}   
                  and sysprop.objProperty = {&printProperty}    
                  and sysprop.objValue    = cDestPrinter)     
             then
              assign
                  cfltPrinterAgents = cfltPrinterAgents + (if cfltPrinterAgents > "" then "," else "") + AgentList.agentID
                  cfltPrinters        = cfltPrinters + (if cfltPrinters > "" then "," else "") + cDestPrinter
                  .
             
            if cDestFileStored <> ""
             then
              do:
                file-info:file-name = cDestFileStored.
                if file-info:full-pathname = ?
                 then
                  assign
                      cfltFolderAgents = cfltFolderAgents + (if cfltFolderAgents > "" then "," else "") + AgentList.agentID
                      cfltFolders        = cfltFolders + (if cfltFolders > "" then "," else "") + cDestFileStored
                      .
              end.
              
            if cDestStorage <> ""  
             then
              do:
                publish "GetSystemParameter" from (session:first-procedure) ("ARCRepositoryUpload", OUTPUT cURLStorage).
                
                if cURLStorage = "" or cURLStorage = ?
                 then
                  cfltStorageAgents = cfltStorageAgents + (if cfltStorageAgents > "" then "," else "") + AgentList.agentID. 
                  
              end.
		    
            if lookup(AgentList.agentID,cfltEmailAgents)   > 0   
            or lookup(AgentList.agentID,cfltPrinterAgents) > 0
            or lookup(AgentList.agentID,cfltFolderAgents)  > 0 
            or lookup(AgentList.agentID,cfltStorageAgents) > 0
             then
              assign
                  lInvalidDest        = true
                  cinvalidDestAgents = cinvalidDestAgents + (if cinvalidDestAgents > "" then "," else "") + AgentList.agentID
                  .
            else
             do:
               if cAllDestination ne "" then 
                 assign cAllDestination = cAllDestination + "|" +  cDestination. 
                else
                 assign cAllDestination = cDestination.
            end.
          end.
       end.
       else
        do:
		  /*Validate the supplied email ID or Printers*/
		  if pRequest:getPrinter() ne "" then 
           for each sysprop fields(objValue) no-lock 
             where sysprop.appCode     = {&printappcode}     
               and sysprop.objAction   = {&printobjAction}   
               and sysprop.objProperty = {&printProperty}    
               and sysprop.objID       = pRequest:getPrinter():    
             cPrinterAddress = sysprop.objValue.
           end.
		   
		  if pRequest:getDistributionEmailList() = "" and cPrinterAddress = ""
           then
            pResponse:fault("3001", "Destination ").
			
		  if pRequest:getPrinter() ne "" and cPrinterAddress = ""
           then
            pResponse:fault("3001", "Printer ").
			
		  if pRequest:getDistributionEmailList() <> "" and not(validEmailAddr(pRequest:getDistributionEmailList()))
           then 
            pResponse:fault("3001", "Email ID " ).
			
          if pResponse:isFault()
           then 
            return.
	    
          if pRequest:getDistributionEmailList() ne "" then
           do:
             if cAllDestination ne "" then 
              assign cAllDestination = cAllDestination + "|E=" + pRequest:getDistributionEmailList(). 
               else
              assign cAllDestination = "E=" + pRequest:getDistributionEmailList().
           end.
	    
          if cPrinterAddress ne "" then
           do:			     
             if cAllDestination ne "" then 
              assign cAllDestination = cAllDestination + "|P=" + pRequest:getPrinter(). 
               else
              assign cAllDestination = "P=" + pRequest:getPrinter().
           end.
        end.

      if not lookup(AgentList.agentID,cinvalidDestAgents) > 0
	   then
	    do:
          /*set the parameter list*/
          assign cParamterList = "AgentList=" + AgentList.agentID + "^StartDate=" + string(ipdtStartDate) + "^EndDate=" + string(ipdtEndDate) + "^PrintEmptyFile=" + string(iplPrintEmptyFile).
           
          if cALLParamterList ne "" then 
            assign cALLParamterList = cALLParamterList + "|" + cParamterList. 
           else 
            assign cALLParamterList = cParamterList.
	      
          iCount =  iCount + 1.
	   end.
    end.

     /*Sent mail when no destination is set*/
    if lNoSysDest or lInvalidDest 
     then
      do:
        publish "GetSystemParameter" from (session:first-procedure) ("AlertsTo", output tToEmail) .
        if not can-do(tToEmail,pRequest:uid)
         then
          tToEmail  = tToEmail + ";" + pRequest:uid.

        assign
            tToEmail  = trim(tToEmail,",;")
            cAlertMsg = (if cFaultAgentID eq "" then "" 
                              else 
                               ("Default as well as specific system destination is not set for the action " + pRequest:actionId + " and Agent ID(s) '" + cFaultAgentID + "'. <br>"))
                            + (if cfltEmailAgents eq "" then "" 
                               else ("Email address(es) '" + cfltEmails + "' are invalid for Agent ID(s) '" + cfltEmailAgents + "' respectively. <br>")) 
                            + (if cfltPrinterAgents eq "" then "" 
                               else ("Printer location(s) '" + cfltPrinters + "' are invalid for Agent ID(s) '" + cfltPrinterAgents + "' respectively. <br>"))
                            + (if cfltFolderAgents eq "" then "" 
                               else ("File storage path(s) '" + cfltFolders + "' are invalid for Agent ID(s) '" + cfltFolderAgents + "' respectively. <br>"))
                            + (if cfltStorageAgents eq "" then "" 
                               else ("ARC repository URL(s) are invalid for Agent ID(s) '" + cfltStorageAgents + "'" + ". <br>"))
                            + "<br>Unable to send output for action 'Production Activity' (" + pRequest:actionId + ").".

        run util/simplemail.p (input "",
                               input tToEmail,
                               input "Unable to generate Production Activity Report",
                               input cAlertMsg).
		
        if num-entries(cAgentList) = num-entries(cfaultAgentID) +  num-entries(cinvalidDestAgents)
         then
          do:
            pResponse:fault("3005", "Action " + pRequest:actionId + " failed due to no system destination available for the Agents, please check email for further details."). 
            return. 
          end.
    end.
	  
    run util/queueaction-p.p(pRequest:actionId, pRequest:uid, pRequest:GetNotifyRequestor (), cALLParamterList, cAllDestination, "TPS", "", "").

    pResponse:success("2013", string(iCount) {&msg-add} "Queue Item").
  end.

  method private logical validEmailAddr (input pEmailAddr as character):

    def var tBadEmailChars as char init "/?<>\:*|~"~~~'~`!#$%^&~{~}[];," no-undo.
    def var i              as int                                        no-undo.
    def var tEmailOK       as log                                        no-undo init yes.
    def var tEmail         as character                                  no-undo.
    def var loop           as integer                                    no-undo.
                                                                     
    do loop = 1 to num-entries(pEmailAddr, ";"):
      if not tEmailOK
       then next.
      tEmail = trim(entry(loop, pEmailAddr, ";")).
   
      do i = 1 to length(tEmail):
       if index(tBadEmailChars, substring(tEmail,i,1)) > 0 then
        do:
          tEmailOK = no.
          leave.
        end.
      end.
   
      if tEmailOK then
       if num-entries(tEmail, "@") <> 2 then
        tEmailOK = no. /*eamil address must contain '@'*/
         
      if tEmailOK then
       if num-entries(tEmail, ".") < 2 then
        tEmailOK = no.
      
      if tEmailOK then
       if num-entries(tEmail, " ") > 1 then
        tEmailOK = no.
      
      if tEmailOK then
       do i = 1 to num-entries(tEmail, "@"):
        if entry(i, tEmail, "@") = "" or entry(i, tEmail, "@") = "?" then
        do:
          tEmailOK = no.
          leave.
        end.
       end.
      
      if tEmailOK then
      do i = 1 to num-entries(pEmailAddr, "."):
        if entry(i, pEmailAddr, ".") = "" or entry(i, pEmailAddr, ".") = "?" then
        do:
          tEmailOK = no.
          leave.
        end.
      end.
    end.
    
    RETURN tEmailOK.
  end method.

  method private void generateFile():


    run util/productionfileactivitycsv.p(input ipdtStartDate,
                                         input ipdtEndDate,
                                         input chAgentID,
                                         input chAgentName,
                                         input chAgentAddr,
                                         input chAgentManager, 
                                         input chAgentManagerName,
                                         input table ttFileAR,
                                         output opcFileName,
                                         output chErrorMsg).

   

  end method.
 
end class.


