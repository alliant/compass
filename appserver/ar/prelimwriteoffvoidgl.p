/*------------------------------------------------------------------------
@name prelimwriteoffvoidgl.p
@action writeoffVoidGlPrelim 

@returns ledgerReport
@author Shefali
@version 1.0
@created 06/11/2021
Modification:
Date          name      Description
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable iarwriteoffID    as character no-undo.

define buffer bfartran for artran.

{lib/std-def.i}
{lib/ar-def.i}

{tt/ledgerreport.i}

pRequest:getParameter("WriteoffID", output iarwriteoffID).

if pResponse:isFault()
 then return.
 
std-lo = false.
find last arwriteoff where arwriteoff.arWriteoffID = integer(iarwriteoffID) no-lock.
for each artran no-lock where artran.reference = string(arwriteoff.arWriteoffID) and artran.sourceType = {&writeoff} and artran.entityID =  arwriteoff.entityID:
  for first agent fields(name ARcashGLRef ARcashGLDesc ARGLRef ARGLDesc arWriteoffGLRef arWriteoffGLDesc) no-lock where agent.agentID = arwriteoff.entityID:
    for first bfartran no-lock where bfartran.entity    = {&Agent}        and
                                     bfartran.entityID  = artran.entityID and
                                     bfartran.fileID    = artran.fileID   and
                                     bfartran.transtype = {&File}         and
                                     bfartran.seq       = 0:
        /* create temp-table record for ledger report */
      create ledgerReport.
      assign 
          ledgerReport.actionID         = string(iarwriteoffID)           
          ledgerReport.agentID          = agent.agentID 
          ledgerReport.agentName        = agent.name 
          ledgerReport.fileNumber       = artran.fileID
          ledgerReport.glDesc           = if arwriteoff.TYPE = {&Debit} then agent.ARwriteoffGLDesc  else agent.ARGLDesc
          ledgerReport.debit            = 0.00
          ledgerReport.credit           = if arwriteoff.TYPE = {&Debit} then ((-1) * artran.tranamt) else artran.tranamt
          ledgerReport.transAmt         = (-1) * artran.tranamt
          ledgerReport.fileDate         = bfartran.trandate
          ledgerreport.ledgerID         = arWriteOff.ledgerID
          ledgerreport.transDate        = arWriteOff.tranDate.
       
      for first ledger fields(accountID) no-lock where ledger.ledgerID = arwriteoff.ledgerID and ledger.debitAmount <> 0:
        ledgerReport.glRef  = ledger.accountID.
      end.
      if ledgerReport.glRef = '' or ledgerReport.glRef = ? 
       then
        ledgerReport.glRef  = if arwriteoff.TYPE = {&Debit} then agent.arWriteoffGLRef else agent.arGLRef.

      /* create temp-table record for ledger report */
      create ledgerReport.
      assign 
          ledgerReport.actionID         = string(iarwriteoffID)           
          ledgerReport.agentID          = agent.agentID 
          ledgerReport.agentName        = agent.name 
          ledgerReport.fileNumber       = artran.fileID
          ledgerReport.glDesc           = if arwriteoff.TYPE = {&Debit} then agent.ARGLDesc          else agent.ARwriteoffGLDesc 
          ledgerReport.debit            = if arwriteoff.TYPE = {&Debit} then ((-1) * artran.tranamt) else artran.tranamt
          ledgerReport.credit           = 0.00
          ledgerReport.transAmt         = (-1) * artran.tranamt
          ledgerReport.fileDate         = bfartran.trandate
          ledgerreport.ledgerID         = arWriteOff.ledgerID
          ledgerreport.transDate        = arWriteOff.tranDate.

      for first ledger fields(accountID) no-lock where ledger.ledgerID = arwriteoff.ledgerID and ledger.creditAmount <> 0:
        ledgerReport.glRef  = ledger.accountID.
      end.
      if ledgerReport.glRef = '' or ledgerReport.glRef = ? 
       then
        ledgerReport.glRef  = if arwriteoff.TYPE = {&Debit} then agent.arGLRef else agent.arWriteoffGLRef .
    end.
  end.
  std-lo = true.
end.
 
if not std-lo 
 then
  do:
     pResponse:fault("1001", "Prelim ledger report").
     return.
  end.

pResponse:setParameter("ledgerReport", table ledgerReport). 
pResponse:success("2000", "Prelim ledger report").  

