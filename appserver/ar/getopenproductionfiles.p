/*---------------------------------------------------------------------------
@name ar/getopenproductionfiles.p
@action openProductionFilesGet
@description return arTRan(filear) table for Production Files
             
@returns The artran(filear) dataset
@success 2005;char;The count of the records returned

@author Sagar R Koralli
@version 1.0
@created 05/10/2024
@modified

-----------------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.
 
define variable pcEntityID      as character no-undo.
define variable pcFileID        as character no-undo.
define variable piReferenceID   as integer   no-undo.


{lib/std-def.i}
{lib/ar-def.i}
{lib/callsp-defs.i}

{tt/artran.i &tableAlias="ttArTran"}
{tt/artran.i &tableAlias="tArTran"}

pRequest:getParameter("EntityID",     output pcEntityID).
pRequest:getParameter("FileID",       output pcFileID).
pRequest:getParameter("ReferenceID",  output piReferenceID).

if pcEntityID > "" and not can-find(first agent where agent.agentID = pcEntityID)
 then
  do:
    pResponse:fault("3066", "Agent").
    return.
  end.  

if pcFileID > "" and not can-find(first filear where filear.agentID    = pcEntityID
                                                 and filear.fileID     = pcFileID
                                                 and filear.type       = {&File}
                                                 and filear.sourceType = {&Production} 
                                                 and filear.seq       = 0)
 then
  do:
    pResponse:fault("3066", "File").
    return.
  end. 
 

{lib\callsp.i &name=spGetOpenProductionFiles &load-into=tArTran 
    &params="input pcEntityID,input pcFileID, input piReferenceID" &noResponse=true}
    
csp-icount = 0.
for each tArTran no-lock :
  if not can-find(first ttArTran where ttArTran.artranID = tArTran.artranID)
   then
    do:
      create ttArTran.
      buffer-copy tArTran to ttArTran.
      csp-icount = csp-icount + 1.
    end.
   else
    for first ttArTran field(appliedamtbyref) where ttArTran.artranID = tArTran.artranID no-lock :
      ttArTran.appliedamtbyref = ttArTran.appliedamtbyref + tArTran.appliedamtbyref.
    end.
   
end.


if csp-icount > 0
 then
  do:
    pResponse:setParameter("ProductionFiles", table ttArTran). 
  end.
  
pResponse:success("2005", string(csp-icount) {&msg-add} "ProductionFiles").
