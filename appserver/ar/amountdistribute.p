/*------------------------------------------------------------------------
@name ar/amountdistribute.p
@action distributeAmount
@description distribute applied amount of a file among 
             invoices that are linked to that file number
                      
@throws 3000;create failed
@success 2002;success 
@author Rahul Sharma
@version 1.0
@created 09.18.2019
@modification
Date             Name             Description  
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable cFileID           as character no-undo.
define variable cEntity           as character no-undo.
define variable cEntityID         as character no-undo.
define variable deDistributeAmt   as decimal   no-undo.

{lib/std-def.i}
{lib/ar-def.i}

{tt/artran.i &tableAlias="ttartran"} 

/* Getting temptable */
empty temp-table ttartran.

pRequest:getParameter("Entity",   output cEntity).
pRequest:getParameter("EntityID", output cEntityID).
pRequest:getParameter("FileID",   output cFileID).
pRequest:getParameter("artran",   input-output table ttartran).


std-de = 0.
/* calculating total applied amount for the invoices */
for each ttartran
  where ttartran.selectrecord:
  std-de = std-de + ttartran.appliedamt.
end.

deDistributeAmt = 0.
/* calculating total amount to be distributed among invoices */
for each artran no-lock
  where artran.entity      = cEntity
    and artran.entityID    = cEntityID
    and artran.fileID      = cFileID
    and (artran.tranID     = ? or artran.tranID = "")
    and artran.type        = {&Apply}:
    
  deDistributeAmt = deDistributeAmt + artran.tranamt.
end.    

deDistributeAmt = if deDistributeAmt < 0 then absolute(deDistributeAmt) else deDistributeAmt.

if std-de > deDistributeAmt
 then                           
  do: 
    pResponse:fault("3005", "Cannot distribute amount greater than the total applied amount on the file").
    return.
  end. 
  
std-lo = false.
TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:
   
  for each ttartran: 
                                                              
    /* Updating applied amount of artran record 
       of type Invoice under same file number */  
    for first artran exclusive-lock
      where artran.artranID = ttartran.artranID : 
      
      artran.appliedamt = ttartran.appliedamt.                                    
    end. /* for first artran...*/    
                                                         
    validate artran.    
    release artran.       
  end. /* for each ttartran */       
  std-lo = true.  
end. /* do transaction */

if not std-lo
 then
  do: 
    pResponse:fault("3000", "Update").
    return.
  end.
 
pResponse:success("2000", "Distribution of amount among invoices").
