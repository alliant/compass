/*------------------------------------------------------------------------
@name islockedByUser.p
@action islockedByUser
@description Locks a entity with the specified ID
@param entityID;char;entity ID to lock
@returns Success;2000;entity lock was successfull
@author Anjly
@version 1.0 
@created 10/09/2019
@notes
----------------------------------------------------------------------*/
define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable pcEntity    as character no-undo.
define variable pcEntityID  as character no-undo.

{lib/std-def.i}
{lib/ar-def.i}

pRequest:getParameter("Entity", output pcEntity).
pRequest:getParameter("EntityID", output pcEntityID).

{lib/islocked.i &obj=pcEntity &id=pcEntityID &exclude-fault=true}

if std-lo
 then
     pRequest:setParameter("LockedByUser", std-ch). 
 

pResponse:success("2000", pcEntity + " is lock").