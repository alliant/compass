/*------------------------------------------------------------------------
@file 
@action arTransactionDelete
@description Delete a record in the Transaction table

@param ID;int;The row identifier

@throws 3000;The record was not found

@success 2008;The delete was successful

@author John Oliver
@version 1.0
@created XX/XX/20XX
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}

/* parameter variables */
def var pID as int no-undo.

/* the parameters */
pRequest:getParameter("ID", OUTPUT pID).

std-lo = false.
for each artrx exclusive-lock
   where artrx.artrxID = pID:

  delete artrx.
  release artrx.
  
  std-lo = true.
end.

if not std-lo
 then
  do: pResponse:fault("3000", "Delete").
      return.
  end.

pResponse:success("2008", "Transaction").
