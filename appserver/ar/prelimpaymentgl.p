/*------------------------------------------------------------------------
@name prelimpaymentgl.p
@action paymentGlPrelim 

@returns glpayment
@author Anjly
@version 1.0
@created 07/16/2020
Modification:
Date          Name      Description
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable pcPaymenttList   as character   no-undo.
define variable iCount           as integer     no-undo.

{lib/std-def.i}
{lib/ar-def.i}

{tt/ledgerreport.i &tableAlias="glpayment"}
{tt/ledgerreport.i &tableAlias="tempglpayment"}

pRequest:getParameter("PaymentList",   output pcPaymenttList).

if pResponse:isFault()
 then return.
 
std-in = 0.
do iCount = 1 to num-entries(pcPaymenttList, ","): 
  run ar\getgl.p(input {&PostPayment},
                        input integer(entry(iCount,pcPaymenttList,",")),
						input 0,
                        input ?,
                        input 0,
                        output table tempglpayment).
  for each tempglpayment:
    create glpayment.
    buffer-copy tempglpayment to glpayment.
    std-in = std-in + 1.
  end.  
  empty temp-table tempglpayment.
end.

if std-in > 0 
 then
  pResponse:setParameter("glpayment", table glpayment). 
  
pResponse:success("2005", string(std-in) {&msg-add} "glpayment").


