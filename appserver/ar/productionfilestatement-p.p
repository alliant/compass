/*------------------------------------------------------------------------
@name productionfilestatement-p.p
@description Get statement data of all production files

@returns Success;int;2005
@author Shefali
@version 1.0 06/05/2024
@notes
@modified 
Date          Name          Description

----------------------------------------------------------------------*/
def input parameter pRequest  AS framework.IRequestable.
def input parameter pResponse AS framework.IRespondable.

{tt/filestatement.i}
{tt/filestatement.i &tableAlias=tfilestatement}
{tt/filestatement.i &tableAlias=ttfilestatement}
{tt/filestatement.i &tableAlias=tempfilestatement}
{lib/std-def.i}
{lib/ar-def.i}
{lib/callsp-defs.i }

/* Variables for parameter */
define input parameter ipcAgentList     as character no-undo.
define input parameter ipdtStartDate    as datetime  no-undo.
define input parameter ipdtEndDate      as date      no-undo.
define output parameter table for tempfilestatement.
define output parameter oplSuccess      as logical   no-undo.
define output parameter opcMsg          as character no-undo.

define variable dtEndDate               as datetime  no-undo.

assign dtEndDate = datetime(string(date(ipdtEndDate)) + " " + "23:59:59.998").
 
/* find balances for agent */
{lib/callsp.i &name=spProductionFileStatement &load-into=tempfilestatement &params="input ipcAgentList,input ipdtStartDate,input dtEndDate" 
 &nocount=true}
 
if not csp-lSucess and csp-cErr > ''
 then
  do:
    pResponse:fault("3005", csp-cErr).
    return.    
  end.

assign oplSuccess =  true.


