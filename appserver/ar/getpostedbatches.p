/*------------------------------------------------------------------------
@name ar/getpostedbatches.p
@action postedBatchesGet
@description Return posted batches of an agent

@author Rahul Sharma 
@version 1.0
@created 01/16/2020      
@modified
01-25-2022 SA Task:89648 Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

/* stored procedure variables */
define variable pcAgentID          as character no-undo.
define variable pcSearchString     as character no-undo.
define variable plOnlyOpenBatches  as logical   no-undo.

{lib/std-def.i}
{lib/callsp-defs.i}
{tt/arbatch.i}

pRequest:getParameter("AgentID",         output pcAgentID).
pRequest:getParameter("SearchString",    output pcSearchString).
pRequest:getParameter("OnlyOpenBatches", output plOnlyOpenBatches).

if pcAgentID = ""
 then
  do:
    pResponse:fault("3005", "Agent ID cannot be blank.").
    return.     
  end.

if pcAgentID > "" and not can-find(first agent where agent.agentID = pcAgentID)
 then
  do:
    pResponse:fault("3066", "Agent").
    return.
  end.
  
if pcSearchString = ""
 then
  do:
    pResponse:fault("3005", "Search string cannot be blank.").
    return.     
  end.

if pcSearchString > "" and length(pcSearchString) < 2
 then
  do:
    pResponse:fault("3005", "Search string cannot be less than two characters.").
    return.     
  end.  
  
{lib\callsp.i &name=spGetPostedBatches &load-into=arbatch &params="input pcAgentID, input pcSearchString, input plOnlyOpenBatches"}
 
pResponse:success("2005", string(csp-icount) {&msg-add} "Arbatch").
