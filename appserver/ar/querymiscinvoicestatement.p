/*------------------------------------------------------------------------
@name querymiscinvoicestatement
@description Get data of Misc invoice

@returns Success;int;2005
@author Sachin Chaturvedi
@version 1.0 02-15-2021
@notes
@modification
Date                Name          Description
03-25-2021          SA            Edit field phase
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

{tt/miscinvstatement.i}
{lib/std-def.i}
{lib/ar-def.i}

/* Variables for parameter */
define variable ipiInvoiceID                as integer   no-undo.
define variable ipcInvIDList                as character no-undo.
define variable ipcOutputAction             as character no-undo.
define variable ipcOutputFormat             as character no-undo.
define variable ipcOutputDestinationType    as character no-undo.
define variable ipcOutputDestinationValue   as character no-undo.
define variable iplPrintEmptyPDF            as logical   no-undo.

define variable chARContactPhone       as character no-undo.
define variable chARContactName        as character no-undo.
define variable chARContactEmail       as character no-undo.
define variable chARContactAddress     as character no-undo.
define variable cDueDays               as character no-undo. 

define variable hTable               as handle    no-undo.
define variable icount               as integer   no-undo.
define variable iInvoiceID           as integer   no-undo.

pRequest:getParameter("miscInvList",              output ipcInvIDList).
pRequest:getParameter("outputAction",             output ipcOutputAction).
pRequest:getParameter("outputFormat",             output ipcOutputFormat).
pRequest:getParameter("outputDesstinationType",   output ipcOutputDestinationType).
pRequest:getParameter("outputDesstinationValue",  output ipcOutputDestinationValue).
pRequest:getParameter("PrintEmptyPDF",            output iplPrintEmptyPDF).

iInvoiceID = integer(ipcInvIDList) no-error.
if ipcOutputAction = {&view} and
   not can-find(first arMisc where arMisc.arMiscID = iInvoiceID ) 
  then
  do:
     pResponse:fault("3066", "Invoice with ID " + ipcInvIDList + "does not exist.").
     return.  
  end.

do icount = 1 to num-entries(ipcInvIDList, ",") :

  ipiInvoiceID = integer(entry(icount , ipcInvIDList, ",")) no-error.
    
  pResponse:setParameter("ipiInvoiceID" + string(icount), ipiInvoiceID).
    
  empty temp-table miscinvstatement.

  create temp-table hTable.
  run stored-procedure spGetMiscInvoiceStatement load-result-into hTable(input ipiInvoiceID).  

  hTable:name = "miscinvstatement".
  std-ch = guid + ".json".
  hTable:write-json("FILE", std-ch).
  temp-table miscinvstatement:read-json("FILE", std-ch, "EMPTY").
  os-delete value(std-ch).
  delete object hTable no-error.

  for each miscinvstatement: 
    for first agent fields(Addr1 Addr2 City State Zip Name) no-lock where agent.agentID = miscinvstatement.agentID:
      assign
          miscinvstatement.agentAddr1 = agent.Addr1
          miscinvstatement.agentAddr2 = agent.Addr2
          miscinvstatement.agentCity  = agent.City
          miscinvstatement.agentState = agent.State 
          miscinvstatement.agentZip   = agent.Zip 
          miscinvstatement.agentName  = agent.Name
          .
    end.
  end.
  
  publish "GetSystemParameter" ("ARContactPhone",   output chARContactPhone). 
  publish "GetSystemParameter" ("ARContactEmail",   output chARContactEmail). 
  publish "GetSystemParameter" ("ARContactAddress", output chARContactAddress). 
  publish "GetSystemParameter" ("ARContactName",    output chARContactName). 

  if (can-find(first miscinvstatement) or iplPrintEmptyPDF)
   then
    for each miscinvstatement break by miscinvstatement.invoiceID:
      if first-of(miscinvstatement.invoiceID)
       then
        do:
          run generateFile.
          pResponse:addFile(std-ch).
        end.
    end.
end.

pResponse:success("2005", string(std-in) {&msg-add} "miscinvstatement").

Procedure generateFile:

 if ipcOutputFormat = {&PDF}
  then
   run util\armiscinvoicestatementpdf.p (input  miscinvstatement.invoiceID,
                                         input  string(year(miscinvstatement.tranDate)) + string(month(miscinvstatement.tranDate),"99"),
                                         input  chARContactPhone,
                                         input  chARContactName,
                                         input  chARContactEmail,
                                         input  chARContactAddress,
                                         input  ipcOutputAction,
                                         input  table miscinvstatement,
                                         output std-ch).
  else if ipcOutputFormat = {&CSV}
   then
    run util\armiscinvoicestatementcsv.p (input  miscinvstatement.invoiceID,
                                      input  string(year(miscinvstatement.tranDate)) + string(month(miscinvstatement.tranDate),"99"),
                                      input  chARContactPhone,
                                      input  chARContactName,
                                      input  chARContactEmail,
                                      input  chARContactAddress,
                                      input  ipcOutputAction,
                                      input  table miscinvstatement,
                                      output std-ch).


end procedure.




