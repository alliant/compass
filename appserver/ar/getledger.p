/*------------------------------------------------------------------------
@name getledger.p
@description Get ledger info

@returns Success;int;2005
@author Anjly
@version 1.0 
@notes
@modified
01-25-2022 SA Task:89648 Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"
----------------------------------------------------------------------*/

define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

{tt/ledger.i &tableAlias="ttLedger"}
{lib/std-def.i}
{lib/ar-def.i} 
{lib/callsp-defs.i}

define variable ipiLedgerID  as integer   no-undo.
define variable ipcAccountID as character no-undo.
define variable ipdaEnd      as date      no-undo.
define variable ipdtStart    as datetime  no-undo.
define variable ipdtEnd      as datetime  no-undo.

/* local variables */
pRequest:getParameter("LedgerID",  OUTPUT ipiLedgerID).
pRequest:getParameter("AccountID", OUTPUT ipcAccountID).
pRequest:getParameter("StartDate", OUTPUT ipdtStart).
pRequest:getParameter("EndDate",   OUTPUT ipdaEnd).

if ipiLedgerID = 0 
 then
  do:
    if ipdtStart = ? or ipdaEnd = ? 
     then
      do:
        pResponse:fault("3005", "Start Date and End Date can not be blank.").
        return.
      end.
  
    if date(ipdtStart) > ipdaEnd 
     then
      do:
        pResponse:fault("3005", "Start Date can not be greater then End Date.").
        return.
      end.

    if ipdaEnd < date(ipdtStart) 
     then
      do:
        pResponse:fault("3005" , "End Date cannot be less than Start Date.").
        return.
      end.  
  
    if ipcAccountID = "" 
     then
      ipcAccountID = {&all}.

    /* Get the records till the midnight of the provided end date. */
    ipdtEnd = datetime(string(ipdaEnd) + " " + "23:59:59.998"). 
  end.
 else
  assign
      ipcAccountID = {&all}
      ipdtStart    = ?
      ipdtEnd      = ?
      .
      
{lib\callsp.i &name=spGetLedger &load-into=ttLedger &params="input ipiLedgerID,input ipcAccountID,input ipdtStart,input ipdtEnd"}
 
pResponse:success("2005", string(csp-icount) {&msg-add} "Ledger").
