/*------------------------------------------------------------------------
@name locktransaction.p
@action transactionLock
@description Locks a entity with the specified ID
@param entityID;char;entity ID to lock
@returns Success;2018;No changes applied to entity
@returns Success;2000;entity lock was successfull
@throws 3066;Invalid entityID
@throws 3081;entity is locked by someone else
@throws 3000;Lock failed
@author Rahul Sharma
@version 1.0 
@created 08/26/2019
@notes
----------------------------------------------------------------------*/
define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable pcEntity    as character no-undo.
define variable pcEntityID  as character no-undo.
define variable cOrgEntity  as character no-undo.

{lib/std-def.i}
{lib/ar-def.i}

pRequest:getParameter("Entity", output pcEntity).
pRequest:getParameter("EntityID", output pcEntityID).

if (pcEntity = {&ArAgent} or pcEntity = {&Agent}) and
   (not can-find(first agent where agent.agentID = pcEntityID))
 then
  do: 
    pResponse:fault("3066", "Agent").
    return.
  end.
if (pcEntity = {&ArPayment} or pcEntity = {&Payment}) and
   (not can-find(first artran where artran.type = {&Payment} and artran.artranID = integer(pcEntityID)))
 then
  do: 
    pResponse:fault("3066", "Payment").
    return.
  end.
if (pcEntity = {&ArCredit} or pcEntity = {&Credit}) and
   (not can-find(first artran where artran.type = {&Credit} and artran.artranID = integer(pcEntityID)))
 then
  do: 
    pResponse:fault("3066", "Credit").
    return.
  end.
  
cOrgEntity = pcEntity. 

{lib/islocked.i &obj=pcEntity &id=pcEntityID &exclude-fault=true}
if not std-lo
 then
  do:
    case pcEntity:
      when {&ArAgent}   then pcEntity = {&Agent}. 
      when {&Agent}     then pcEntity = {&ArAgent}.
      when {&ArPayment} then pcEntity = {&Payment}.
      when {&Payment}   then pcEntity = {&ArPayment}.
      when {&ArCredit}  then pcEntity = {&Credit}.
      when {&Credit}    then pcEntity = {&ArCredit}.
    end case.
    
    {lib/islocked.i &obj=pcEntity &id=pcEntityID &exclude-fault=true}
    if std-lo
     then
      do:
        if std-ch = pRequest:uid
         then
          do:
            pResponse:setParameter("LockedByUser", std-ch).
            pResponse:success("2018", pcEntity).
          end.
         else 
          pResponse:fault("3081", pcEntity {&msg-add} std-ch).
        return.
      end.
  end.
 else
  do:
    if std-ch = pRequest:uid
     then
      pResponse:fault("3005", "Record cannot be locked as " + pRequest:uid + " already holds the lock").
     else 
      pResponse:fault("3081", pcEntity {&msg-add} std-ch).
    return.
  end.
      
std-lo = false.
 
run util/newsyslock.p (cOrgEntity, pcEntityID, 0, pRequest:uid, output std-lo).

if not std-lo
 then
  do:
    pResponse:fault("3000", "Lock").  
    return.
  end.  
  
pResponse:success("2000", cOrgEntity + " lock").
