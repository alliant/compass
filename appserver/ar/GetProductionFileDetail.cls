/*------------------------------------------------------------------------
@name ar/GetProductionFileDetail.cls
@action productionFileDetailGet
@description  Gets the Production File Transaction Details.
@returns Success;2000
@author  S Chandu
@version 1.0
@created 06.04.2024
@Modified :
Date        Name          Comments   
07/12/24   S Chandu       Modified parameters and removed date range fetching of files.             
----------------------------------------------------------------------*/

class ar.GetProductionFileDetail inherits framework.ActionBase:
       
  {lib/callsp-defs.i}  
  {tt/filedetail.i &tableAlias="ttFileDetail"}  
  
  constructor GetProductionFileDetail ():
    super().
  end constructor.

  destructor public GetProductionFileDetail ( ):
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    {lib/std-def.i}
    
    /* stored procedure variables */
    define variable ipiAgentFileID as integer    no-undo.
    define variable ipcAgentID     as character  no-undo.
    define variable ipcFileNumber  as character  no-undo.
    define variable dsHandle       as handle     no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttFileDetail:handle).
    
    pRequest:getParameter("AgentID",     output ipcAgentID).
    pRequest:getParameter("FileNumber",  output ipcFileNumber).
    
    
    /* validation on agentID */
    if (ipcAgentID = ""  or ipcAgentID = ?)
     then 
      do:
        pResponse:fault("3001", "AgentID").
        return.
      end.
    
    if not can-find(first agentFile where agentFile.agentID = ipcAgentID) 
     then
      do:
        pResponse:fault("3066", "AgentID").
        return.
      end.
    else
      for first agentFile fields (agentFileID agentID fileNumber) no-lock 
                        where agentFile.agentID    = ipcAgentID
                          and agentfile.fileNumber = ipcFileNumber:
          ipiAgentFileID = agentfile.agentFileID.
        end.

    {lib\callsp.i 
          &name=spGetProductionFileDetail 
          &load-into=ttFileDetail 
          &params="input ipiAgentFileID"
          &noResponse=true &nocount=true}
    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end.
  
    if csp-lSucess and (can-find(first ttFileDetail))
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldLIst).
    
  pResponse:success("2005", string(csp-icount) {&msg-add} " Production File Transaction Details.").
             
  end method.
      
end class.





