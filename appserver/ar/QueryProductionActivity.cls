/*------------------------------------------------------------------------
@name ar/QueryProductionActivity.cls
@action productionActivityQuery
@description  Gets the Production Files with activity Details.
@returns Success;2000
@author  S Chandu
@version 1.0
@created 06.01.2024
@Modified :
Date        Name          Comments   
07/12/24   S Chandu       Added exclude zero balance file parameter.              
----------------------------------------------------------------------*/

class ar.QueryProductionActivity inherits framework.ActionBase:
       
  {lib/callsp-defs.i}  
  {tt/fileAr.i &tableAlias="ttFileAR"}  
  
  constructor QueryProductionActivity ():
    super().
  end constructor.

  destructor public QueryProductionActivity ( ):
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    {lib/std-def.i}
    
    /* stored procedure variables */
    define variable ipcAgentID      as character  no-undo.
    define variable ipcStateID      as character  no-undo.
    define variable ipdtStartDate   as datetime   no-undo.
    define variable ipdtEndDate     as datetime   no-undo.
    define variable dsHandle        as handle     no-undo.

    define variable iplExcludeZeroBalanceFile as logical   no-undo.


    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttFileAR:handle).
    
    pRequest:getParameter("AgentID",    output ipcAgentID).
    pRequest:getParameter("State",      output ipcStateID).
    pRequest:getParameter("StartDate",  output ipdtStartDate).
    pRequest:getParameter("EndDate",    output ipdtEndDate).
    pRequest:getParameter("ExcludeZeroBalanceFile",  output iplExcludeZeroBalanceFile).

    if ipcAgentID = ""
     then
     ipcAgentID = "ALL".

    {lib\callsp.i 
          &name=spGetProductionActivity 
          &load-into=ttFileAR 
          &params="input ipcAgentID, input ipdtStartDate, input ipdtEndDate, input iplExcludeZeroBalanceFile "
          &noResponse=true &nocount=true}
    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end.
  
    if csp-lSucess and (can-find(first ttFileAR))
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldLIst).
    
    pResponse:success("2005", string(csp-icount) {&msg-add} " Production Files Activity").
             
  end method.
      
end class.





