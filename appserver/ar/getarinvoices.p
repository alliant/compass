/*------------------------------------------------------------------------
@file geaptinvoices.p
@action arInvoicesGet
@description Get one or multiple record(s) in the Invoice table

@param ID;int;The row identifier

@returns The Invoice dataset

@throws 3066;The row does not exist in the Invoice

@success 2005;Invoice returned X rows

@author John Oliver
@version 1.0
@created XX/XX/20XX
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}
{tt/arinv.i &tableAlias="ttInvoice"}
{tt/artrx.i &tableAlias="ttTransaction"}

/* These preprocessors are used inside xml/xmlparse.i but not needed in this program */
&scoped-define traceFile std-ch
&scoped-define xmlFile std-ch
{lib/xmlparse.i}

/* parameter variables */
def var pID as int no-undo.

/* the parameters */
pRequest:getParameter("ID", OUTPUT pID).

/* if there is a fault thrown within the procedure, exit */
if pResponse:isFault()
 then return.

std-in = 0.
for each arinv no-lock:

  /* move to the next record if the ID is found and is not the right one */
  if pID > 0 and arinv.arinvID <> pID
   then next.

  create ttInvoice.
  buffer-copy arinv to ttInvoice.
  ttInvoice.notes = encodeXml(arinv.notes).
  
  ttInvoice.hasDocument = {lib/canfindinvoicedoc.i "Invoice-AR" arinv.arinvID}.
  if ttInvoice.hasDocument
   then
    for first sysdoc no-lock
        where sysdoc.entityType = "Invoice-AR"
          and sysdoc.entityID = string(arinv.arinvID):
          
      ttInvoice.documentFilename = sysdoc.objID.
      if sysdoc.objAttr > ""
       then ttInvoice.documentFilename = ttInvoice.documentFilename + "." + sysdoc.objAttr.
    end.
  
  /* get the transactions */
  for each artrx no-lock
     where artrx.arinvID = arinv.arinvID:

    create ttTransaction.
    buffer-copy artrx to ttTransaction.
    ttTransaction.notes = encodeXml(artrx.notes).
  end.

  std-in = std-in + 1.
end.

IF std-in > 0
 THEN
  do:
    pResponse:setParameter("Invoice", table ttInvoice). 
    pResponse:setParameter("Transaction", table ttTransaction).
  end.

pResponse:success("2005", STRING(std-in) {&msg-add} "Invoice").
