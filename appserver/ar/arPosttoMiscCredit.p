/*-------------------------------------------------------------------------
@name ar/arPosttoMiscCredit.p
@action miscCreditPost
@description Post Misc credit

@author Vignesh Rajan 
@version 1.0
@created 03/30/2022  
@modified 
    Date         Name       Description                         
-------------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable ipcAgentID      as character no-undo.
define variable ipcFilenumber   as character no-undo.
define variable ipcRevenuetype  as character no-undo.
define variable ipdeTranamt     as decimal   no-undo.
define variable ipdtTrandate    as datetime  no-undo.
define variable ipcSourcetype   as character no-undo.
define variable ipcSourceID     as character no-undo.
define variable ipcReference    as character no-undo.
define variable ipcDestType     as character no-undo.
define variable ipcDestID       as character no-undo.
define variable opSuccess       as logical   no-undo.
define variable opMsg           as character no-undo. 
define variable opcLedgerReport as character no-undo.
define variable opLedgerID      as integer   no-undo.

pRequest:getParameter("AgentID",        output ipcAgentID).
pRequest:getParameter("Filenumber",     output ipcFilenumber).
pRequest:getParameter("RevenueType",    output ipcRevenuetype).
pRequest:getParameter("Tranamt",        output ipdeTranamt).
pRequest:getParameter("Trandate",       output ipdtTrandate).
pRequest:getParameter("Sourcetype",     output ipcSourcetype).
pRequest:getParameter("SourceID",       output ipcSourceID).
pRequest:getParameter("Reference",      output ipcReference).
pRequest:getParameter("DestType",       output ipcDestType).
pRequest:getParameter("DestID",         output ipcDestID).

run ar\postmisccredit.p (input ipcAgentID,
                         input ipcFilenumber,
                         input ipcRevenuetype,
                         input ipdeTranamt,
                         input ipdtTrandate,
                         input ipcSourcetype,
                         input ipcSourceID,
                         input ipcReference,
                         input ipcDestType,
                         input ipcDestID,
                         input pRequest:uID,
                         input pRequest:clientID,
                         input pRequest:actionID,                  
                         output opSuccess,
                         output opMsg,
                         output opcLedgerReport,
                         output opLedgerID).

if not opSuccess 
 then
  do:
    pResponse:fault("3005", opMsg).
  end.
 else
  do:
    pResponse:success("2000", 'Posting of AR misc credit').
  end.


