/*------------------------------------------------------------------------
@name ar/modifyrevenue.p   
@action revenueModify
@description Modify existing Revenue code.   
@param arrevenue table
@throws 3066;Revenue Type does not exist
@throws 3004;ArRevenue is locked
@throws 3000;Modify failed
@author  Rahul Sharma
@version 1.0
@created 01/06/2020
@modified 
Date        Name     Comments
01/21/2021  Shefali  Remove Net GL Ref validation while edit revenue.
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable cRevenueType   as character no-undo.
define variable dtModifiedDate as datetime  no-undo.
define variable lFound         as logical   no-undo.

/* Standard Lib files */
{lib/std-def.i}
{lib/ar-def.i}

{tt/arrevenue.i &tableAlias="ttarrevenue"}

empty temp-table ttarrevenue.
pRequest:getParameter("ArRevenue", input-output table ttarrevenue).   

for first ttarrevenue:
  if not can-find(first arrevenue where arrevenue.revenueType = ttarrevenue.revenueType)
   then
    pResponse:fault("3066", "Revenue Type").
  
  if ttarrevenue.grossGLRef = "" 
   then
    pResponse:fault("3005", "Gross GL Reference field cannot be blank"). 
    
end.

if pResponse:isFault() 
 then 
  return.

assign 
    std-lo = false
    lFound = false
    .

TRX-BLOCK:
do transaction                               
  on error undo TRX-BLOCK, leave TRX-BLOCK:  

  for first ttarrevenue:  

    for first arrevenue exclusive-lock
      where arrevenue.revenueType = ttarrevenue.revenueType:
       
      lFound = true.
      buffer-copy ttarrevenue except ttarrevenue.revenueType ttarrevenue.notes ttarrevenue.createddate ttarrevenue.createdby to arrevenue.
      assign          
          arrevenue.modifiedBy    = pRequest:uid
          arrevenue.modifiedDate  = now         
          arrevenue.notes         = ttarrevenue.notes
          cRevenueType            = arrevenue.revenueType
          dtModifiedDate          = arrevenue.modifiedDate
	      .
                              
      validate arrevenue.
      release arrevenue.
      
      std-lo = true.   
    end.
  end.
end.  

if not lFound
 then
  pResponse:fault("3004", "ArRevenue").
 else if not std-lo 
  then
   pResponse:fault("3000", "Modify").

if pResponse:isFault() 
 then 
  return.

pResponse:setParameter("ModifiedDate",dtModifiedDate).

pResponse:success("2006", "ArRevenue").
pResponse:updateEvent("ArRevenue", cRevenueType).
