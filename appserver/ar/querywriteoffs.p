/*---------------------------------------------------------------------------
@name ar/querywriteoffs.p
@action writeoffsQuery
@description Return Posted writeoff report

@author Shefali
@version 1.0
@created 06/02/2021  
Modified
Date       Name    Description
01-25-2022 Shefali Task:89648 - Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"               

----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

/* stored procedure variables */
define variable pcEntityID             as character no-undo. 
define variable pdFromPostDate         as datetime  no-undo.
define variable pdToPostDate           as datetime  no-undo.
define variable daToDate               as date      no-undo.
define variable pcSearchString         as character no-undo.
define variable plIncludeVoid          as logical   no-undo.

{lib/std-def.i}
{lib/callsp-defs.i}
{lib/ar-def.i}
{tt/arwriteoff.i &tableAlias="ttarwriteoff"}
   
std-in = 0.

pRequest:getParameter("EntityID",            output pcEntityID).
pRequest:getParameter("FromPostDate",        output pdFromPostDate).
pRequest:getParameter("ToPostDate",          output daToDate).
pRequest:getParameter("SearchString",        output pcSearchString).
pRequest:getParameter("IncludeVoid",         output plIncludeVoid).

if pcEntityID = "" 
 then
  pcEntityID = "ALL".
  
if date(pdFromPostDate) > today 
 then
  pResponse:fault("3005" , 'Post From Date cannot be in future.').
  
if daToDate > today 
 then
  pResponse:fault("3005" , 'Post To Date cannot be in future.').

if daToDate < date(pdFromPostDate) 
 then
  pResponse:fault("3005" , 'Post To Date cannot be less than Post From Date.').

if pcEntityID = "ALL" and pcSearchString = "" then
 do:
  if pdFromPostDate = ? or daToDate = ? 
   then  
    pResponse:fault("3005", 'You must enter date range and/or search string when searching for all agents.').
    
 end.
 
if pResponse:isFault() 
 then return.

/* Get the records till the midnight of the provided end date. */
if daToDate <> ?
 then
  pdToPostDate = datetime(string(daToDate) + " " + "23:59:59.998").

{lib/callsp.i  &name=spGetWriteoffs &load-into=ttArWriteoff &setParam="'arwriteoff'" &params="input pcEntityID, input pdFromPostDate, input pdToPostDate, input pcSearchString, input plIncludeVoid"}

pResponse:success("2005", string(csp-icount) {&msg-add} "Writeoff").





