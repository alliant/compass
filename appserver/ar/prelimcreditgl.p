/*------------------------------------------------------------------------
@name prelimcreditgl.p
@action creditglprelim 

@returns glcredit
@author Anjly
@version 1.0
@created 07/16/2020
Modification:
Date          Name      Description
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable pcCreditList   as character   no-undo.
define variable iCount         as integer     no-undo.

{lib/std-def.i}
{lib/ar-def.i}

{tt/ledgerreport.i &tableAlias="glcredit"}
{tt/ledgerreport.i &tableAlias="tempglcredit"}

pRequest:getParameter("creditList",   output pcCreditList).

if pResponse:isFault()
 then return.
 
std-in = 0.
do iCount = 1 to num-entries(pcCreditList, ","): 
  run ar\getgl.p(input {&PostCredit},
                        input integer(entry(iCount,pcCreditList,",")),
						input 0,
                        input ?,
                        input 0,
                        output table tempglcredit).
  for each tempglcredit:
    create glcredit.
    buffer-copy tempglcredit to glcredit.
    std-in = std-in + 1.
  end.  
  empty temp-table tempglcredit.
end.

if std-in > 0 
 then
  pResponse:setParameter("glcredit", table glcredit). 
  
pResponse:success("2005", string(std-in) {&msg-add} "glcredit").


