/*------------------------------------------------------------------------
@file deletearinvoicedocument.p
@action arInvoiceDocumentDelete
@description Deletes an invoice document from the repository

@param ID;int;The row identifier

@author John Oliver
@version 1.0
@created 10.16.2019
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

{lib/std-def.i}
{lib/account-repository.i}

/* parameter variables */
define variable pID as integer no-undo.

/* local variables */
define variable cServerPath as character no-undo.
define variable cFileName   as character no-undo.

/* the parameters */
pRequest:getParameter("ID", OUTPUT pID).

std-lo = false.
for each arinv exclusive-lock
   where arinv.arinvID = pID:
   
  cServerPath = getReceivableServerPath(arinv.dateRequested).
  for first sysdoc no-lock
      where sysdoc.entityType = "Invoice-AR"
        and sysdoc.entityID = string(arinv.arinvID):
        
    cFilename = sysdoc.objID + "." + sysdoc.objAttr.
  end.
  
  /* delete the record from the sysdoc table */
  if {lib/canfindinvoicedoc.i "Invoice-AR" pID}
   then 
    do:
      run repository/deleterepositoryitem.p ("", "", cServerPath + cFileName, output std-lo, output std-ch).
      pRequest:setParameter("Type","Invoice-AR").
      pRequest:setParameter("ID",string(pID)).
      pRequest:setParameter("Seq",0).
      run sys/deletesysdoc.p (pRequest,pResponse).
    end.
    
  std-lo = true.
end.

if not std-lo
 then pResponse:fault("3000", "Delete").
 else pResponse:success("2008", "Invoice").
