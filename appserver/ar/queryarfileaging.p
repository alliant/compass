/*------------------------------------------------------------------------
@name queryarfileaging.p
@description Get aging data of all agents
@param AgentID;char;Specific agent ID to retrieve  (optional)

@returns Success;int;2005
@author Rahul
@version 1.0 10/15/2019
@notes
@modification
Date                Name          Description
03-25-2021          SA            Edit field phase
03-09-2021          MK            Updated for framework changes
01-25-2022          Shubham       Task:89648 - Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"
06-28-2022          SA            Task:93485 - Added region field and logic to get region description.
09-06-2022          SD            Task 97317 - Add 3 new fields in Aging Report
----------------------------------------------------------------------*/

define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

{tt/araging.i}
{tt/araging.i &tableAlias=ttaraging}
{lib/std-def.i}
{lib/ar-def.i}
{lib/callsp-defs.i}

define variable ipcFirstAgentID           as character no-undo.
define variable ipcLastAgentID            as character no-undo.
define variable ipcStateID                as character no-undo.
define variable ipdtasOfDate              as datetime  no-undo.
define variable ipcReportType             as character no-undo.
define variable ipcReportData             as character no-undo.
define variable iplAllAgents              as logical   no-undo.

/* local variables */
define variable cAgentList      as character no-undo.
define variable cAgentID        as character no-undo.
define variable cStateCode      as character no-undo. 
define variable chErrorMsg      as character no-undo.
define variable hTable          as handle    no-undo.

pRequest:getParameter("FirstAgentID",             output ipcFirstAgentID).
pRequest:getParameter("LastAgentID",              output ipcLastAgentID).
pRequest:getParameter("StateID",                  output ipcStateID).
pRequest:getParameter("AsOfDate",                 output ipdtasOfDate).
pRequest:getParameter("ReportType",               output ipcReportType).
pRequest:getParameter("ReportData",               output ipcReportData).
pRequest:getParameter("IncludeAllAgents",         output iplAllAgents).

if ipcStateID = ""
 then
  ipcStateID = {&All}.

/* Creating range */
if ipcFirstAgentID <> ""
 then
  do:     
    if length(ipcFirstAgentID) = 1
     then
      ipcFirstAgentID = "0" + ipcFirstAgentID.
    
    assign
        cAgentID   = ipcFirstAgentID
        cStateCode = substring(ipcFirstAgentID,1,2)
        .
        
    do std-in = 1 to (5 - (length(cAgentID) - 1)):
      ipcFirstAgentID = ipcFirstAgentID + "0".
    end.  
    ipcLastAgentID = if (ipcLastAgentID = "" or (ipcStateID <> {&All} and substring(ipcLastAgentID,1,2) <> cStateCode)) then cStateCode + "9999" else ipcLastAgentID.
  end.

if ipcLastAgentID <> ""
 then
  do:     
    if length(ipcLastAgentID) = 1
     then
      ipcLastAgentID = "0" + ipcLastAgentID.
    
    assign
        cAgentID   = ipcLastAgentID
        cStateCode = substring(ipcLastAgentID,1,2)
        .
        
    do std-in = 1 to (5 - (length(cAgentID) - 1)):
      ipcLastAgentID = ipcLastAgentID + "9".
    end.  
    ipcFirstAgentID = if (ipcFirstAgentID = "" or (ipcStateID <> {&All} and substring(ipcFirstAgentID,1,2) <> cStateCode)) then cStateCode + "0000" else ipcFirstAgentID.
  end.  
    
/* Creating list of agents */ 
if ipcFirstAgentID = "" and ipcLastAgentID = "" 
 then
  cAgentList = {&All}. /* When range is not provided */
 else
  for each agent fields(agentID) no-lock
    where agent.stateID = (if ipcStateID = {&All} then agent.stateID else ipcStateID)   
      and agent.agentID >= ipcFirstAgentID  
      and agent.agentID <= ipcLastAgentID:
    cAgentList = cAgentList + (if cAgentList > "" then "," else "") + agent.agentID. /* When first agentID smaller than last agent ID */  
  end.

/*
else if ipcFirstAgentID = ipcLastAgentID
 then
  cAgentList = ipcFirstAgentID. /* When first and last agentID is same */ 
else if ipcFirstAgentID = ""
 then
  for first agent fields(agentID) no-lock
    where agent.stateID = (if ipcStateID = {&All} then agent.stateID else ipcStateID)   
      and agent.agentID = ipcLastAgentID:
    cAgentList = agent.agentID. /* When first agentID is blank */  
  end.

else if ipcLastAgentID = ""
 then
  for first agent fields(agentID) no-lock
    where agent.stateID = (if ipcStateID = {&All} then agent.stateID else ipcStateID)   
      and agent.agentID = ipcFirstAgentID:
    cAgentList = agent.agentID. /* When last agentID is blank */  
  end. 
  
else if ipcFirstAgentID < ipcLastAgentID 
 then
  for each agent fields(agentID) no-lock
    where agent.stateID = (if ipcStateID = {&All} then agent.stateID else ipcStateID)   
      and agent.agentID >= ipcFirstAgentID  
      and agent.agentID <= ipcLastAgentID:
    cAgentList = cAgentList + (if cAgentList > "" then "," else "") + agent.agentID. /* When first agentID smaller than last agent ID */  
  end.
  
else 
 for each agent fields(agentID) no-lock
   where agent.stateID = (if ipcStateID = {&All} then agent.stateID else ipcStateID)   
     and agent.agentID >= ipcLastAgentID   
     and agent.agentID <= ipcFirstAgentID:
   cAgentList = cAgentList + (if cAgentList > "" then "," else "") + agent.agentID. /* When first agentID is greater than last agent ID */     
 end. */

{lib\callsp.i 
    &name=spGetFileAging 
    &load-into=arAging 
    &params="input ipcStateID,input cAgentList,input ipdtasOfDate,input ipcReportType,input ipcReportData,input iplAllAgents"
	&noCount=true}



std-in = 0.
for each araging:  
  assign 
      arAging.region       = if (arAging.region = "?" or arAging.region = ?)            then ""   else arAging.region
      arAging.stateID      = if (arAging.stateID = "?" or arAging.stateID = ?)          then ""   else arAging.stateID
      arAging.agentID      = if (arAging.agentID  = "?" or arAging.agentID  = ?)        then ""   else arAging.agentID          
      arAging.name         = if (arAging.name  = "?" or arAging.name  = ?)              then ""   else arAging.name           
      arAging.filenumber   = if (arAging.filenumber  = "?" or arAging.filenumber  = ?)  then ""   else arAging.filenumber  
      arAging.invoiced     = if (arAging.invoiced = ?)                                  then 0    else arAging.invoiced
      arAging.manager      = if (arAging.manager  = "?" or arAging.manager  = ?)        then ""   else arAging.manager           
      arAging.stat         = if (arAging.stat = "?" or arAging.stat = ?)                then ""   else arAging.stat           
      arAging.arTranID     = if  arAging.arTranID = ?                                   then 0    else arAging.arTranID
      arAging.due0_30      = if  arAging.due0_30 = ?                                    then 0    else arAging.due0_30
      arAging.due31_60     = if  arAging.due31_60 = ?                                   then 0    else arAging.due31_60
      arAging.due61_90     = if  arAging.due61_90 = ?                                   then 0    else arAging.due61_90
      arAging.due91_180    = if  arAging.due91_180 = ?                                  then 0    else arAging.due91_180
      arAging.due181_365   = if  arAging.due181_365 = ?                                 then 0    else arAging.due181_365
      arAging.due366_730   = if  arAging.due366_730 = ?                                 then 0    else arAging.due366_730
      arAging.due730_      = if  arAging.due730_ = ?                                    then 0    else arAging.due730_
      arAging.balance      = if  arAging.balance = ?                                    then 0    else arAging.balance.  
    
  if arAging.region = ""
   then
    do:
      for first state where state.stateID = arAging.stateID:
        arAging.region = state.region.
      end.
    end.
 
  create ttaraging.
  buffer-copy araging to ttaraging.
  
  /* Get manager name from sysuser */
  for first sysuser fields(name) no-lock
    where sysuser.UID = araging.manager:
    ttaraging.manager = sysuser.name.
  end.

  /* Get region desc from syscode */
  for first syscode no-lock
    where syscode.code = araging.region and syscode.codeType = "Region":
    ttaraging.region   = syscode.description.
  end.

  std-in = std-in + 1.
end.

if (can-find(first ttaraging))
 then
  do:
    if pRequest:outputFileFormat = {&PDF} /*PDF*/
     then
      do:
        if ipcReportType = {&Summary} /* Summary*/ 
         then      
          run util/aragingsummarypdf.p (input table ttaraging,
                                        input ipdtasOfDate,                                            										                                     
                                        output std-ch,
                                        output chErrorMsg).
	     else /* Detail */		  
	      run util/aragingDetailpdf.p (input table ttaraging,                                  
                                       input  ipdtasOfDate,                                     
                                       output std-ch,
                                       output chErrorMsg).
      end.																				
     else  /* CSV */
      run util/aragingcsv.p(input table ttaraging,
                            input ipdtasOfDate,
		                    input ipcReportType,                          
                            output std-ch,
                            output chErrorMsg).                             
    
    if chErrorMsg <> ""
     then 
      do:
        pResponse:fault("3005", chErrorMsg).
        return.
      end.                  
     else 
      pResponse:addfile(std-ch).
  end.

pResponse:success("2005", string(std-in) {&msg-add} "araging").  
