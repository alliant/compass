/*------------------------------------------------------------------------
@name preliminvoicevoidgl.p
@action invoicevoidGlPrelim 

@returns glinvoice
@author Anjly
@version 1.0
@created 07/16/2020
Modification:
Date          Name      Description
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable piInvoiceID   as integer   no-undo.

{lib/std-def.i}
{lib/ar-def.i}

{tt/ledgerreport.i &tableAlias="glinvoice"}

pRequest:getParameter("invoiceID",   output piInvoiceID).

if pResponse:isFault()
 then return.

run ar\getgl.p(input {&VoidMiscInvoice},
               input piInvoiceID,
			   input 0,
               input ?,
               input 0,
               output table glinvoice).

pResponse:setParameter("glinvoice", table glinvoice). 
  
pResponse:success("2000", "glinvoice void").


