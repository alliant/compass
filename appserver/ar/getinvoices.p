/*---------------------------------------------------------------------------
@name ar/getinvoices.p
@action invoicesGet
@description Returns a table of temporary AR invoices

@returns The arMisc dataset
@success 2005;char;The count of the records returned

@author Rahul Sharma
@version 1.0
@created 07/25/2019
@modified
Date          Name          Description
07/22/20      AG            Replaced arTemp by arMisc
03-24-2021    SA            Edit field phase
-----------------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable pcArType as character no-undo.
define variable piArMiscID as integer no-undo.

{lib/std-def.i}
{lib/ar-def.i}

{tt/arMisc.i &tableAlias="ttARMisc"}

pRequest:getParameter("ArType",        output pcArType).
pRequest:getParameter("ArMiscID",      output piArMiscID).


if pcArType = "" then pcArType = {&all}.

std-in = 0.
if piArMiscID <> 0
 then
  for each arMisc no-lock where arMisc.arMiscID = piArMiscID :
        
    create ttARMisc.
    buffer-copy arMisc except arMisc.transDate to ttARMisc .
    assign ttARMisc.creditInvoiceDt = arMisc.transDate. /* Credited On / Invoiced On */

    for first agent fields(name) no-lock where agent.agentID = arMisc.entityID:
      ttARMisc.agentname = agent.name.
    end.
    
    for first artran fields(voidDate tranDate) no-lock where arTran.entity    = arMisc.entity and
                                   arTran.entityID  = arMisc.entityID and
                                                             arTran.tranID    = string(arMisc.arMiscID) and 
                                   arTran.transtype = "" and
                                                             arTran.type      = pcArType and
                                   arTran.seq       = 0:
      assign
        ttARMisc.voidDate  = arTran.voidDate
        ttARMisc.transDate = arTran.tranDate.
    end.
  
    /* include file to get username, postByName and voidByName based on uid */
    {lib/getusername.i &var=ttARMisc.username   &user=arMisc.createdby}
	  {lib/getusername.i &var=ttARMisc.postByName &user=arMisc.postedBy}
	  {lib/getusername.i &var=ttARMisc.voidByName &user=arMisc.voidedBY}
  
    std-in = std-in + 1.
  end. /* for each arMisc */  
 else 
  for each arMisc no-lock where arMisc.type = (if pcArType = {&all} then arMisc.type else pcArType ) and not arMisc.posted  :
        
    create ttARMisc.
    buffer-copy arMisc except arMisc.transDate to ttARMisc.
    assign ttARMisc.creditInvoiceDt = arMisc.transDate. /* Credited On / Invoiced On */

    /* include file to get username, postByName and voidByName based on uid */
    {lib/getusername.i &var=ttARMisc.username   &user=arMisc.createdby}
	  {lib/getusername.i &var=ttARMisc.postByName &user=arMisc.postedBy}
	  {lib/getusername.i &var=ttARMisc.voidByName &user=arMisc.voidedBY}
	  
    for first agent fields(name) no-lock where agent.agentID = arMisc.entityID:
      ttARMisc.agentname = agent.name.
    end.
    for first artran fields(voidDate tranDate) no-lock where arTran.entity    = arMisc.entity and
                                   arTran.entityID  = arMisc.entityID and
                                                             arTran.tranID    = string(arMisc.arMiscID) and 
                                   arTran.transtype = "" and
                                                             arTran.type      = pcArType and
                                   arTran.seq       = 0:
      assign
        ttARMisc.voidDate  = arTran.voidDate
        ttARMisc.transDate = arTran.tranDate.
    end.
  
    std-in = std-in + 1.
  end. /* for each arMisc */

if std-in > 0 
 then
  pResponse:setParameter("arMisc", table ttARMisc).

pResponse:success("2005", string(std-in) {&msg-add} "arMisc").

