/*------------------------------------------------------------------------
@name ar/modifywriteoffnotes.p   
@action 
@description Update existing arwriteoff Notes

@param arwriteoffID;int;
@param Notes;char;

@throws 3004;Record not found
@throws 3000;Update failed
@throws 3005;Update not applicable
@success 2006;The update was successful
@author Shefali
@version 1.0
@created 06/07/2021
@Modification:
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.  

{lib/std-def.i}

/* local variables */
define variable lFound as logical no-undo.

/* parameter variables */
define variable piarWriteoffID as integer   no-undo.
define variable pcNotes        as character no-undo.
      
/* the parameters */
pRequest:getParameter("arwriteoffID", output piarWriteoffID). 
pRequest:getParameter("notes", output pcNotes). 

message  "piarWriteoffID :" piarWriteoffID.

/* validation */
/* make sure that the atranID is valid */
if piarWriteoffID = ? or piarWriteoffID = 0
 then
  do:
    pResponse:fault("3005", "arwriteoffID cannot be blank").
    return.
  end.
  
if not can-find(first arwriteoff where arwriteoff.arWriteoffID = piarWriteoffID)
 then 
  do:
    pResponse:fault("3066", "arwriteoffID does not exist").
    return.
  end.
 
assign
  lFound = false
  std-lo = false
  .  

/* all parameters are valid */
TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:
  
  for first arwriteoff exclusive-lock 
    where arwriteoff.arWriteoffID = piarWriteoffID:

    assign
        lFound       = true
        arwriteoff.notes = pcNotes
        .
	  
    validate arwriteoff.
    release arwriteoff.

    assign std-lo  = true.    
  end.
end.

if not lFound 
 then 
  pResponse:fault("3004", "Write-off").
 else if not std-lo 
  then
     pResponse:fault("3000", "Update").
      
if pResponse:isFault() 
 then return. 
 
pResponse:success("2006", "Write-off"). 

