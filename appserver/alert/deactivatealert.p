&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*---------------------------------------------------------------------
@name deactivatealert.p
@action N/A
@description Deactivates an alert if the agent, source, and code match

@author John Oliver
@version 1.0
@created 2017/11/27
---------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

define input parameter pAgentID as character no-undo.
define input parameter pSource as character no-undo.
define input parameter pProcessCode as character no-undo.
define input parameter pUserID as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* set the other alerts for the agent/code to not active */
for each alert exclusive-lock
   where alert.agentID = pAgentID
     and alert.source = pSource
     and alert.processCode = pProcessCode:
  
  assign
    alert.stat = "C"
    alert.active = false
    alert.closedBy = pUserID
    alert.dateClosed = now
    .
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


