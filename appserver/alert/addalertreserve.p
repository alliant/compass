&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*---------------------------------------------------------------------
@name addalertreserve.p
@action N/A
@description Add or previews the alerts when the reserve is greater than
             the threshold of net revenue

@param AgentID;character;The agent ID
@param UserID;character;The user ID
@param Preview;logical;True if the user wants to preview the alert first
@param Alert;complex;The temp table for the alert if previewing

@author John Oliver
@version 1.0
@created 2017/09/29
@Modification
08/29/19    Gurvindar    Added notifications for Y and R alerts
01-25-2022  Shubham      Task:89648 - Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"
03-17-2022  Sachin C     Task:92318 - Check if temp-table is prepared before rererencing its attributes.
---------------------------------------------------------------------*/
{tt/alert.i &tableAlias="ttAlert"}

/* ***************************  Definitions  ************************** */
define input parameter pResponse  as service.IResponse.
define input parameter pAgentID as character no-undo.
define input parameter pReserveDate as datetime no-undo.
define input parameter pUserID as character no-undo.
define input parameter pPreview as logical no-undo.
define output parameter table for ttAlert.

{lib/std-def.i}
{lib/callsp-defs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
{lib\callsp.i 
 &name=spAlertReserve 
 &noload=true
 &params="input pAgentID, input pReserveDate, input pUserID, input pPreview"
 &noCount=true}
 
if not csp-hProcedureTable:prepared
 then 
  return. 

{lib\callsp-copytt.i  &load-into=ttAlert  &noResponse=true}

if not valid-object(pResponse) 
then
 return.

/* Creating Notification */
for each ttAlert where ttalert.severity > 0 no-lock:

  pResponse:notify(input "",                       /* UID */
                   input "A",                      /* entityType */
                   input ttalert.agentID,         /* entityId */
                   input 0,                       /* refsysNotficationID */
                   input if ttalert.severity = 1 then "Y" else "R",  /* notificationStat  */
                   input ttalert.source,                             /* sourcetype */
                   input string(ttalert.alertID),  /* sourceID */
                   input ttalert.note).    /* notification message */  

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


