&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file add-mail-parameter.i
@description Adds a mail parameter to the iData temp table for use in
             sendhtmlemail.p
             
@param f;character;The field to in the HTML email
@param v;character;The value to replace for the field 

@author John Oliver
@created 02.05.2021
@notes Must include the {service/iData.i} include in the definition section
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
 
&IF DEFINED(f) = 0 &THEN
&SCOPED-DEFINE f 
&ENDIF

&IF DEFINED(v) = 0 &THEN
&SCOPED-DEFINE v 
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

CREATE iData.
ASSIGN
  iData.objSeq        = 0
  iData.objName       = "parameter"
  iData.objProperty   = {&f}
  iData.propertyValue = {&v}
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


