/*------------------------------------------------------------------------
File        : tt/glproductioninvoice.i
Purpose     :

Syntax      :

Description :

Author(s)   : Shefali
Created     : 05-10-2024
Notes       :

Modification:
Date          Name      Description

----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias glproductioninvoice
&endif
   
define temp-table {&tableAlias}
  field agentid       as character
  field artranID      as integer
  field type          as character
  field reference     as character
  field tranID        as character
  field postingdate   as datetime
  field argldef       as character
  field arglref       as character
  field debit         as decimal
  field credit        as decimal
  field description   as character
  field voided        as character
  field notes         as character
  .

