/*
 batchAction.i
 BATCH-ACTION data structure
 D.Sinclair 5.30.2012
 */

&IF defined(tableAlias) = 0 &THEN
  &scoped-define tableAlias batchAction
&ENDIF

&IF defined(nodeType) = 0 &THEN
  &scoped-define nodeType xml-node-type "attribute"
&ENDIF

def temp-table {&tableAlias} no-undo
  field batchID as int {&nodeType} /* das: Change of datatype */
  field seq as int {&nodeType}
  field postDate as datetime {&nodeType}
  field postUserID as char {&nodeType}
  field postInitials as char {&nodeType}
  field comments as char {&nodeType}
  field secure as logical {&nodeType}
  .
