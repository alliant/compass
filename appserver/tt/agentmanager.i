&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/agentmanager.i
   AGENTMANAGER datastructure
   @Modified
   Date        Name           Description
   12/15/20    Shefali        Task-86695-Agent Manager Type Implementation.
 
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias agentmanager
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

def temp-table {&tableAlias}
  field managerID             as integer   {&nodeType}
  field agentID               as character {&nodeType}
  field uid                   as character {&nodeType}
  field effDate               as datetime  {&nodeType} column-label "Effective"  format "99/99/9999"
  field isPrimary             as logical   {&nodeType} column-label "Primary"
  field expDate               as datetime  {&nodeType} column-label "Expiration"  format "99/99/9999"
  field commissionCode        as character {&nodeType}
  field commissionCodeDesc    as character {&nodeType}
  field commissionRate        as decimal   {&nodeType}
  field commissionRateEncrypt as character {&nodeType}
  field stat                  as character {&nodeType}
  field createdBy             as character {&nodeType}
  field dateCreated           as datetime  {&nodeType}
  field type                  as character {&nodeType} column-label "Type"       format "x(50)"
  
  /* used for the client */
  field uidDesc               as character {&nodeType} column-label "Name"       format "x(100)"
  field statDesc              as character {&nodeType} column-label "Status"     format "x(50)"
  field createdByDesc         as character {&nodeType} column-label "Created By" format "x(100)"
  field typeDesc              as character {&nodeType} format "x(50)"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


