&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : stateform.i
    Purpose     : linking an insuring FORM to a STATE datastructure
    Author(s)   : D.Sinclair
    Created     : 1.24.2012
    Notes       :
    Modification:
    Date         Name        Description
    07-31-2019   Vikas Jain  Added field revenueType  
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias stateform
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

def temp-table {&tableAlias}
 field stateID as char {&nodeType}
 field formID as char {&nodeType}        /* Identifier used during processing */

 field statCode as char {&nodeType}
 field active as log {&nodeType}

 field effDate as datetime {&nodeType}
 field fileDate as datetime {&nodeType}  /* db is 'fileDate' */
 field minGross as dec {&nodeType}       /* added to match db */
 field maxGross as dec {&nodeType}       /* added to match db */
 field formType as char {&nodeType}      /* (P)olicy, (E)ndorsement, (C)PL, Commi(T)ment, (S)earch, Sche(D)ule, M)isc */
 field rateCheck as char {&nodeType}     /* F)ixed:  rateMin and rateMax are absolute */
                                         /* L)iability:  rateMin/rateMax are calculated */
                                         /* N)one: don't check rate */
 field rateMin as decimal {&nodeType}
 field rateMax as decimal {&nodeType}
 field description as char {&nodeType}
 field formCode as char {&nodeType}
 field insuredType as char {&nodeType}   /* (O)wner or (L)ender for policies */
 field revenueType as char {&nodeType}  /* (O)wner, (L)ender, (B)oth for endorsements */
                                         /* (N)one for CPL, Schedule, Search, and Misc */

 field orgName as char {&nodeType}
 field orgRev as char {&nodeType}
 field orgRelDate as datetime {&nodeType}
 field orgEffDate as datetime {&nodeType}
 

 index stateform is primary unique
  stateID
  formID
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


