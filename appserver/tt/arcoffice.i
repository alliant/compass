&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/office.i
   Office datastructure
   Author(s) B.Johnson
   Created 10.1.2015
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias arcoffice
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

def temp-table {&tableAlias}
  field OfficeId as int {&nodeType}
  field OfficeName as char {&nodeType}
  field Address1 as char {&nodeType}
  field Address2 as char {&nodeType}
  field City as char {&nodeType}
  field State as char {&nodeType}
  field County as char {&nodeType}
  field Zipcode as char {&nodeType}
  field Phone as char {&nodeType}
  field Fax as char {&nodeType}
  field Website as char {&nodeType}
  field Email as char {&nodeType}
  field ParentOfficeId as int {&nodeType}
  field AgentId as char {&nodeType}
  field Stat as char {&nodeType}
  field IssuesPolicies as log {&nodeType}
  field UseParentAddressOnPJ as log {&nodeType}
  field UserParentAddressOnCPL as log {&nodeType}
  field ContactName as char {&nodeType}
  field ContactTitle as char {&nodeType}
  field ContactPhone as char {&nodeType}
  field ContactEmail as char {&nodeType}
  field UnremittedPolicyLimit as dec {&nodeType}
  field THLogin as char {&nodeType}
  field THPassword as char {&nodeType}
  field ANServiceKey as int {&nodeType}
  field ImportAgentID as char {&nodeType}
  field PolicyCountWarningLimit as int {&nodeType}
  field PolicyCountWarningText as char {&nodeType}
  field PolicyLiabilityLimit as dec {&nodeType}
  field Branches as int {&nodeType}
  field AltAddress1 as char {&nodeType}
  field AltAddress2 as char {&nodeType}
  field AltCity as char {&nodeType}
  field AltState as char {&nodeType}
  field AltZipcode as char {&nodeType}
  field AltCounty as char {&nodeType}
  field PJAddress1 as char {&nodeType}
  field PJAddress2 as char {&nodeType}
  field PJCity as char {&nodeType}
  field PJState as char {&nodeType}
  field PJZipcode as char {&nodeType}
  field PJCounty as char {&nodeType}
  field CPLAddress1 as char {&nodeType}
  field CPLAddress2 as char {&nodeType}
  field CPLCity as char {&nodeType}
  field CPLState as char {&nodeType}
  field CPLZipcode as char {&nodeType}
  field CPLCounty as char {&nodeType}
  
  index AgentId is primary 
    AgentId
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


