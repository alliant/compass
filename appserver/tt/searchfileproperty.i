/* tt/searchfileproperty.i
   Datastructure for an search in file property
   @created 03.09.2023
   @author Sachin Chaturvedi
   Modifications:
   Date          Name           Description
   09/10/2024    SRK            Added New fields
   10/23/2024    SRK            Added seq field
*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias searchfileproperty
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ENDIF

&IF DEFINED(serializeName) = 0 &THEN
&SCOPED-DEFINE serializeName serialize-name "searchfileproperty"
&ELSE
&SCOPED-DEFINE serializeName serialize-name "{&serializeName}"
&ENDIF

def temp-table {&tableAlias} NO-UNDO {&serializeName}

field agentfileID         as integer   {&nodeType}
field seq                 as integer   {&nodeType}
field agentID             as character {&nodeType}
field fileNumber          as character {&nodeType}
field agentName           as character {&nodeType}
field propertyAddr        as character {&nodeType} 
field parcelID            as character {&nodeType}
field book                as character {&nodeType}
field pageNumber          as character {&nodeType}
field condominum          as character {&nodeType}
field subdivision         as character {&nodeType}
field buyerDescription    as character {&nodeType}
field sellerDescription   as character {&nodeType} 
field section             as character {&nodeType} 
field township            as character {&nodeType} 
field townshipDirection   as character {&nodeType} 
field range               as character {&nodeType} 
field rangeDirection      as character {&nodeType} 
field lot                 as character {&nodeType} 
field block               as character {&nodeType} 
field levels              as character {&nodeType} 
field percentMatch        as decimal   serialize-hidden {&nodeType} 
field tempPercent         as decimal   serialize-hidden {&nodeType}
field ipercentMatch       as integer   serialize-name "matchPercentage" {&nodeType}

index idxAFID agentfileID
.

