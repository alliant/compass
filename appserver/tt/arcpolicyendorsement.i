&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 policy.i
 POLICY data structure
 D.Sinclair 5.30.2012
 Modified:
         Date        Name          Comments 
         08-01-2022  S Chandu      added xmlNodename
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias arcpolicyendorsement
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

&if defined(serializename) = 0 &then
&scoped-define serializename serialize-name "arcpolicyendorsement"
&else
&scoped-define serializename serialize-name "{&serializename}"
&endif


define temp-table {&tableAlias} no-undo {&serializename}
  field StateId      as character {&nodeType} serialize-hidden
  field PolicyId     as integer   {&nodeType}
  field FormId       as character {&nodeType} column-label "Form"        format "x(1000)"
  field Description  as character {&nodeType} column-label "Description" format "x(200)"
  field GrossPremium as decimal   {&nodeType} column-label "Gross"       format ">>>,>>>,>>9.99"
  field NetPremium   as decimal   {&nodeType} column-label "Net"         format ">>>,>>>,>>9.99"
  field StatCode     as character {&nodeType} column-label "STAT"        format "x(10)"
  field Charge       as decimal   {&nodeType} column-label "Charge"      format ">>>,>>9.99"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */


/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* ************************  Frame Definitions  *********************** */


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: 
   Allow: Basic,Browse,DB-Fields
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 16.71
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


