/* tt/queryfiles.i
   Datastructure for a queryfile
   @created 12.31.2024
   @author Sachin
   Modifications:
   Date          Name           Description
   
 */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias queryfiles
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "queryfiles"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo  {&serializeName}
  field agent                  as character    {&nodeType} /* AgentID + Name */
  field agentFileId            as integer      {&nodeType}
  field agentID                as character    {&nodeType}
  field fileID                 as character    {&nodeType}
  field fileNumber             as character    {&nodeType}
  field closingDate            as datetime     {&nodeType}
  field createdDate            as datetime     {&nodeType}
  field notes                  as character    {&nodeType}
  field stateID                as character    {&nodeType}
  field stateDesc              as character    {&nodeType}
  field county                 as character    {&nodeType}
  field openTasks              as integer      {&nodeType}
  field lastCompletedTask      as character    {&nodeType}
  field completedBy            as character    {&nodeType}
  field completedDate          as datetime     {&nodeType}
  .
