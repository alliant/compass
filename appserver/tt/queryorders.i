/* tt/queryorders.i
   Datastructure for an queryorders
   @created 11.22.2024
   @author S Chandu
   Modifications:
   Date          Name           Description
   
 */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias queryorders
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "queryorders"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo  {&serializeName}
  field Agent                  as character    {&nodeType} 
  field AgentFileId            as integer      {&nodeType}
  field fileNumber             as character    {&nodeType}
  field closingDate            as datetime     {&nodeType}
  field county                 as character    {&nodeType}
  field assignedTo             as character    {&nodeType} serialize-name "AssignedTo"
  field assignedToName         as character    {&nodeType} serialize-name "AssignedToName"
  field assignedDate           as datetime-tz  {&nodeType}
  field priorFulfillments      as integer      {&nodeType}
  field createdDate            as datetime     {&nodeType}
  field createdHours           as integer      {&nodeType}
  field elapsedHours           as integer      {&nodeType}
  field notes                  as character    {&nodeType}
  field stateID                as character    {&nodeType}
  field stateDesc              as character    {&nodeType}
  field oldestOpenTask         as character    {&nodeType}
  .
