&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/clm01.i
@desc CLaiM datastructure for aggregation reporting
@author D.Sinclair
@date 12.28.2016
@modification:
Date           Name         Description
07-28-22       SA           Task #: 95509 :Implement clob framework changes 
*/


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias clm01
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ENDIF

&IF DEFINED(nodeName) = 0 &THEN
&SCOPED-DEFINE nodeName XML-NODE-NAME "clm01"
&ELSE
&SCOPED-DEFINE nodeName XML-NODE-NAME "{&nodeName}"
&ENDIF


def temp-table {&tableAlias} no-undo
 field stateID                as char  {&nodeType}
 field agentID                as char  {&nodeType}
 field agentName              as char  {&nodeType} /* Typically set on client to export more complete data */
 field claimID                as int   {&nodeType}
 field claimIDDesc            as char  {&nodeType}
 field stat                   as char  {&nodeType}
 field fileNumber             as char  {&nodeType} /* Agent file number */
 field altaRiskCode           as char  {&nodeType}
 field altaRiskDesc           as char  {&nodeType} /* Typically set on client to export more complete data */
 field altaResponsibilityCode as char  {&nodeType}
 field altaResponsibilityDesc as char  {&nodeType} /* Typically set on client to export more complete data */
 field causeCode              as char  {&nodeType}
 field coCause                as char  {&nodeType} /* Typically set on client to export more complete data */
 field descriptionCode        as char  {&nodeType}
 field coDescription          as char  {&nodeType} /* Typically set on client to export more complete data */
 field dateOpened             as date  {&nodeType}
 field yearOpened             as int   {&nodeType} /* Typically set on client to export more complete data */
 field assignedTo             as char  {&nodeType} 
 
 field summary      as char {&nodeType}

 field laeOpen      as dec  {&nodeType}
 field laeApproved  as dec  {&nodeType}
 field laeLTD       as dec  {&nodeType} 
 field laeReserve   as dec  {&nodeType}
 field laeAdjOpen   as dec  {&nodeType}
 field laeAdjLTD    as dec  {&nodeType}

 field lossOpen     as dec  {&nodeType}
 field lossApproved as dec  {&nodeType}
 field lossLTD      as dec  {&nodeType}
 field lossReserve  as dec  {&nodeType}
 field lossAdjOpen  as dec  {&nodeType}
 field lossAdjLTD   as dec  {&nodeType} 

 field ownerPolicyID            as char  {&nodeType}
 field ownerPolicyEffDate       as date  {&nodeType}
 field ownerPolicyInsuredType   as char  {&nodeType}
 field ownerPolicyInsuredName   as char  {&nodeType}
 field ownerPolicyOrigLiability as dec   {&nodeType}
 field ownerPolicyCurrLiability as dec   {&nodeType}
 field ownerPolicyFormID        as char  {&nodeType}
 field ownerPolicyFormDesc      as char  {&nodeType}
 field ownerPolicyFormType      as char  {&nodeType}

 field lenderPolicyID            as char {&nodeType}
 field lenderPolicyEffDate       as date {&nodeType}
 field lenderPolicyInsuredType   as char {&nodeType}
 field lenderPolicyInsuredName   as char {&nodeType}
 field lenderPolicyOrigLiability as dec  {&nodeType}
 field lenderPolicyCurrLiability as dec  {&nodeType}
 field lenderPolicyFormID        as char {&nodeType}
 field lenderPolicyFormDesc      as char {&nodeType}
 field lenderPolicyFormType      as char {&nodeType}

 field coverageID            as char  {&nodeType}
 field coverageEffDate       as date  {&nodeType}
 field coverageCoverageType  as char  {&nodeType}
 field coverageInsuredType   as char  {&nodeType}
 field coverageInsuredName   as char  {&nodeType}
 field coverageOrigLiability as dec   {&nodeType}
 field coverageCurrLiability as dec   {&nodeType}
 field coverageFormID        as char  {&nodeType}
 field coverageFormDesc      as char  {&nodeType}
 field coverageFormType      as char  {&nodeType}
 
 field insuredName   as char {&nodeType}
 field insuredAddr   as char {&nodeType}
 field description   as char {&nodeType}
 field lastNoteDate  as date {&nodeType}
 field lastNoteText  as clob {&nodeType}
 field noteCnt       as int  {&nodeType}
 field stage         as char {&nodeType}
 field type          as char {&nodeType}
 field action        as char {&nodeType}
 field difficulty    as int  {&nodeType}
 field urgency       as int  {&nodeType}
 field litigation    as log  {&nodeType}
 field significant   as log  {&nodeType}
 field agentError    as char {&nodeType}
 field searcherError as char {&nodeType}
 field attorneyError as char {&nodeType}
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


