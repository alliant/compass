/*------------------------------------------------------------------------
    File        : lockedfile.i
    Purpose     : To get locked files

    Syntax      :

    Description :

    Author(s)   : Shefali
    Created     : 11/25/2023
    Notes       :
    Modification:
    Date          Name       Description 
 
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias lockedfile
&endif
define temp-table {&tableAlias}
  field agentFileID      as character
  field agentID          as character
  field agentName        as character
  field agent            as character
  field fileNumber       as character
  field lockedBy         as character
  field lockedByName     as character
  field lockDate         as datetime
  .
