&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/datacall.i
    Purpose     : Data structure for data call reports (based on 'periodmix.i')
    Author(s)   : B.Johnson
    Created     : 5.10.2016

   The table holds summary data based on a set of two keys:
    StateID or AgentID 
    and 
    STATCode or FormID

   Each record in the liability table is for a range of liability values.

   Type     ID       codeType Code      Meaning
   ----     -------- -------- ----      -------
    S       StateID   S       STAT      Values for the state/STATcode combo
    S       StateID   S       <blank>   Subtotal for all STATcodes in the state
    S       StateID   F       FormID    Values for the state/formID combo
    S       StateID   F       <blank>   Subtotal for all FormIDs in the state
    A       AgentID   S       STAT      Values for the agent/STATcode combo
    A       AgentID   F       FormID    Values for the agent/FormID combo
    <blank> ALL       <blank> <blank>   Totals for all records in all states

   Conceptually, we also should have subtotals for all STATcodes and FormIDs
   for an agent.  We may need to implement this later.

   ---

   The seq and *Calc fields are used in export to CSV for use in Excel.
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias periodmix
&ENDIF

def temp-table {&tableAlias}
 field type as char /* S=StateID, A=AgentID */
 field ID as char
 field codeType as char /* S=STATcode, F=FormID */
 field code as char
 field rateCode as char
 field residential as log
 field sPeriodID as int
 field ePeriodID as int
 field minLiab as dec
 field maxLiab as dec
 field description as char
 field numForms as int
 field numPolicies as int

 field grossPremium as dec
 field endorsementPremium as dec
 field otherPremium as dec
 field totalPremium as dec

 field netPremium as dec
 field retainedPremium as dec

 field amount as dec
 field grossRate as dec
 field netRate as dec

 field lowGross as dec
 field highGross as dec
 field lowNet as dec
 field highNet as dec
 field lowRetained as dec
 field highRetained as dec
 field lowLiability as dec
 field highLiability as dec
 index pidx type id codeType code rateCode residential minLiab maxLiab
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


