/*------------------------------------------------------------------------
File        : tt/ledgerreport.i
Purpose     :

Syntax      :

Description :

Author(s)   : Anjly
Created     : 08-06-20
Notes       :

Modification:
Date          Name      Description
03/04/21      Shefali   Add reference field to show in the post invoice GL report
07/02/21      Shefali   Add fileDate field to show in the write-off file date in GL report
----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias ledgerReport
&endif
   
define temp-table {&tableAlias}
  /*ledger*/
  field ledgerID          as integer
  field ledgerCreateBy    as character
  field ledgerCreateDate  as datetime
  field transDate         as datetime
  
  field actionID          as character
  field agentID           as character
  field agentName         as character
   
  field fileID            as character
  field fileNumber        as character
  field fileDate          as datetime
  
  field reference         as character  
  
  field glDesc            as character
  field glRef             as character
  field debit             as decimal
  field credit            as decimal
  
  /*pmtyment*/
  field checkNum          as character
  field checkAmt          as decimal
  
  /*deposit*/
  field depositDate       as datetime
  field depositRef        as character
  
  /*arMisc*/
  field source            as character
  field sourceID          as character
  field dueDate           as datetime
  field transAmt          as decimal
  
  field description       as character
  field transType          as character
  .

