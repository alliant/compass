/*
@file tt/fileInvAr.i
@description Data structure for file invoice

@author Shefali
@created 02/06/2023
@modified
Name          Date       Note
Chandu      05/21/2024   Removed cancel related fields.
------------- ---------- -----------------------------------------------

 */
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias fileinvar
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "{&tableAlias}"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
     field fileInvID     as integer   {&nodeType} 
     field agentFileID   as integer   {&nodeType}  	 
     field invoiceNumber as character {&nodeType}                             
     field invoiceDate   as date      {&nodeType}                             
     field invoiceAmount as decimal   {&nodeType} format "zzz,zz9.99"         
     field posted        as logical   {&nodeType}                             
     field postedAmount  as decimal   {&nodeType} format "zzz,zz9.99"         
     field postedDate    as datetime  {&nodeType}	 
                                                                              
	 /*client side*/                                                          
     field osAmt         as decimal   {&nodeType} format "-zzz,zz9.99"        
     field stateID       as character {&nodeType}                             
     field selectrecord  as logical   {&nodeType}                             
     field postinvAmt    as decimal   {&nodeType} format "zzz,zz9.99"         
     field agentID       as character {&nodeType}
     field agentName     as character {&nodeType}
     field fileNumber    as character {&nodeType}
     .
