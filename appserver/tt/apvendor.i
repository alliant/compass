/*------------------------------------------------------------
    tt/apVendor.i
   @created 11.21.2022
   @author KR
   @modified
   Name          Date       Note
   ------------- ---------- -----------------------------------------------
   Ube          01/24/2023  Task 102000 - Changed as per new framework  
   K.R          07/12/2023  Renamed the file
   K.R          07/28/2023  Modified to add fields
    --------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias apvendor
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ELSE
&SCOPED-DEFINE nodeType XML-NODE-TYPE "{&nodeType}"
&ENDIF

&IF DEFINED(serializeName) = 0 &THEN
&SCOPED-DEFINE serializeName serialize-name "apvendor"
&ELSE
&SCOPED-DEFINE serializeName serialize-name "{&serializeName}"
&ENDIF

&IF DEFINED(posttasks) = 0  &THEN
define temp-table {&tableAlias} no-undo {&serializeName}
  field vendorID     as character {&nodeType}
  field category     as character {&nodeType}
  field vendorname   as character {&nodeType}
  field active       as logical   {&nodeType}
  .
&ELSE
define temp-table {&tableAlias} no-undo {&serializeName}
  field vendorID      as character {&nodeType}
  field vendorname    as character {&nodeType}
  field vendoraddress as character {&nodeType}
  field city          as character {&nodeType}
  field state         as character {&nodeType}
  field zip           as character {&nodeType}
  .
&ENDIF
