/*------------------------------------------------------------------------
File        : tt/ledger.i
Purpose     :

Syntax      :

Description :

Author(s)   : Anjly
Created     : 07-27-20
Notes       :

Modification:
Date          Name      Description

----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias ledger
&endif
   
define temp-table {&tableAlias}
  field ledgerID          as character
  field seq               as character
  field agentID           as character
  field accountID         as character
  field creditAmount      as decimal
  field debitAmount       as decimal
  field transDate         as datetime 
  field createdBy         as character
  field createDate        as datetime
  field notes             as character
  field sourceID          as character
  field source            as character
  
  field agentName         as character
  field accountDesc       as character
  field createdByName     as character
  field void              as logical
  .
