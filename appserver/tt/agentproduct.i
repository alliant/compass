/* tt/agentproduct.i
   @created 07.11.2022
   @author Vignesh Rajan
   Modifications:
   Date          Name           Description
   05/09/23      S Chandu      Added Category field

 */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias agentProduct
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "agentProduct"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
  field agentID      as character {&nodeType}
  field agentName    as character {&nodeType}
  field stateID      as character {&nodeType}
  field stateName    as character {&nodeType}
  field productID    as integer   {&nodeType}
  field productDesc  as character {&nodeType}
  field uid          as character {&nodeType}
  field userName     as character {&nodeType}
  field notUid       as character {&nodeType}
  field notUserName  as character {&nodeType}
  field price        as decimal   {&nodeType}
  field dueDays      as integer   {&nodeType}
  field contactName  as character {&nodeType}
  field contactEmail as character {&nodeType}
  field contactPhone as character {&nodeType}
  .
