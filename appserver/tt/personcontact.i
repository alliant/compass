/*------------------------------------------------------------------------
File        : tt/personcontact.i
Purpose     :

Syntax      :

Description :

Author(s)   : Sachin Anthwal
Created     : 08-04-2022
Notes       :

Modification:
Date          Name           Description
04/10/2024    SR            Task 111689 - Added field extension.
----------------------------------------------------------------------*/
&IF defined(tableAlias) = 0 &THEN 
&scoped-define tableAlias personContact
&ENDIF
   
define temp-table {&tableAlias}
  field personcontactID as integer
  field personID       	as character
  field contactType    	as character
  field contactSubType 	as character
  field contactID      	as character
  field uid            	as character
  field createDate     	as datetime
  field expirationdate 	as datetime
  field active          as logical
  field extension       as character
  .
 


