/* tt/filetask.i
   @created 04.06.2023
   @author Shefali
   Modifications:
   Date          Name           Description
 17/04/2023      sagar K        added topic and Category fields.
 07/20/2023     S chandu       added posted field.
 */
 
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias filetask
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "filetask"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
    field fileTaskID     as integer      {&nodeType}
    field ID             as integer      {&nodeType} 
    field topic          as character    {&nodeType} serialize-name "Task"
	field AgentFileId    as integer      {&nodeType} 
	field Agent          as character    {&nodeType} 
	field AgentID        as character    {&nodeType}
    field FileNumber     as character    {&nodeType} serialize-name "FileNumber"
	field Address        as character    {&nodeType}
    field Order          as integer      {&nodeType}
    field productDesc    as character    {&nodeType} serialize-name "Product"
	field DueDateTime    as datetime     {&nodeType} serialize-name "DueDate"
    field FileId         as character    {&nodeType} 
	field AssignedTo     as character    {&nodeType} serialize-name "AssignedTo"
    field AssignedToName as character    {&nodeType} serialize-name "AssignedToName"
	field VendorName     as character    {&nodeType} 
	field VendorID       as character    {&nodeType}
    field AssignedDate   as datetime-tz  {&nodeType}
    field Category       as character    {&nodeType} serialize-name "Queue"	
    field TaskStatus     as character    {&nodeType} serialize-name "Status"	
	field ImportantNote  as character    {&nodeType} 
    field StartDateTime  as datetime     {&nodeType} 
	field EndDateTime    as datetime     {&nodeType}
    field cost           as decimal      {&nodeType}
    field posted         as logical      {&nodeType}
    field stateID        as character    {&nodeType}
    field glaccount      as character    {&nodeType}
    field glaccountDesc  as character    {&nodeType} 
	field apInvID        as integer      {&nodeType} 
	field apVoucherID    as integer      {&nodeType}
	field completedDate  as datetime     {&nodeType}
    
    /* Posted vender Task*/
    field invoiceNumber  as character    {&nodeType}
    field invoiceDate    as datetime-tz  {&nodeType}
    field username       as character    {&nodeType}
    field amount         as decimal      {&nodeType}
    field refID          as character    {&nodeType}
    .                                      







