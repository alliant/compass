&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12v
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/render.i
   The temp table used to send into the searchconsole.exe
   John Oliver 11.30.2018
 */


&if defined(tableAlias) = 0 &then
&scoped-define tableAlias render
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif

define temp-table {&tableAlias}
  field textValue   as character {&nodeType} column-label "Value"       format "x(1000)"
  field metaField   as character {&nodeType} column-label "Placeholder" format "x(100)"
  field controlType as character {&nodeType} column-label "Type"        format "x(50)"
  field listIndent  as integer   {&nodeType}
  field listOrder   as integer   {&nodeType} column-label "Seq"         format ">>9"
  field listStart   as integer   {&nodeType}
  field listType    as integer   {&nodeType} /* 0 = Numeric, 2 = lowercase roman, 4 = lowercase letter, 1 = uppercase roman, 3 = uppercase letter */
  
  /* reference field for code */
  field refID       as character {&nodeType}
  
  /* join to link temp-table */
  field linkRel     as character {&nodeType}
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


