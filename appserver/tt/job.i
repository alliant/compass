/* 
   tt/job.i
   Datastructure for an fileProperty
   @created 05.31.2022
   @author Sachin Anthwal
   Modifications:
   Date          Name           Description
   07/25/2023   S Chandu        Added fulfilled and render related fields.
   11/20/2024   SRK             Modified fulfillsoftware datatype
 */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias job
&endif

&if defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&endif

&IF DEFINED(serializeName) = 0 &THEN
&SCOPED-DEFINE serializeName serialize-name "job"
&ELSE
&SCOPED-DEFINE serializeName serialize-name "{&serializeName}"
&ENDIF

define temp-table {&tableAlias} no-undo {&serializeName}
  field agentFileID        as integer     {&nodeType} 
  field suffix             as integer     {&nodeType}
  field orderID            as integer     {&nodeType}
  field revision           as integer     {&nodeType}
  field productID          as integer     {&nodeType}
  field productname        as character   {&nodeType}
  field agentProduct       as character   {&nodeType}
  field statCode           as character   {&nodeType}
  field stat               as character   {&nodeType}
  field dueDate            as datetime-tz {&nodeType}  
  field quotedPrice        as decimal     {&nodeType}
  field contactName        as character   {&nodeType}
  field estimatedDate      as date        {&nodeType}
  field customerOrder      as character   {&nodeType}
  field contactEmail       as character   {&nodeType}
  field contactPhone       as character   {&nodeType}
  field effectiveDate      as datetime-tz {&nodeType} 
  field notes              as character   {&nodeType}
  field orderStatusCode    as character   {&nodeType} serialize-hidden
  field fulfillDate        as datetime-tz {&nodeType} serialize-hidden
  field fulfillsoftware    as character   {&nodeType}
  field fulfilledDate      as datetime-tz {&nodeType}
  field fulfilledBy        as character   {&nodeType}
  field renderDate         as datetime    {&nodeType}
  field renderBy           as character   {&nodeType}
  .
