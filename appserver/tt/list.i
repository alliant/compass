&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* 
@name tt/list.i
@description Temp table for the LIST builder
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias list
&ENDIF

define temp-table {&tableAlias}
  field displayName as character
  field reportName as character
  field entityName as character
  field createDate as datetime
  field modifyDate as datetime
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
