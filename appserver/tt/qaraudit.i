&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* audit.i
   D.Sinclair 10.20.2015 Added qarID
   Modification:
   Date          Name      Description
   08/02/2017    AC        New fields schedStartDate and schedFinishDate added
   10/04/2017    AC        New fields Audittype and Errtype added
   13/11/2017    RS        Added reason code for QUR audits.
   29/07/2022    SD       Task#95509: Modified to support clob related changes
   03/17/2023    K.R       Task #103305 Added fields priorAuditDate,priorAuditScore and priorAuditType
   ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias audit
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ENDIF

&IF DEFINED(nodeName) = 0 &THEN
&SCOPED-DEFINE nodeName XML-NODE-NAME "audit"
&ELSE
&SCOPED-DEFINE nodeName XML-NODE-NAME "{&nodeName}"
&ENDIF

   
def temp-table {&tableAlias} {&nodeName}
  field qarID as int      {&nodeType}
  field version as char   {&nodeType}
  field auditYear as int  {&nodeType}
  field agentID as char   {&nodeType}
  field name as char      {&nodeType}  /* comes from agent table */
  field addr as char      {&nodeType} /* comes from agent table */
  field city as char      {&nodeType} /* comes from agent table */
  field state as char     {&nodeType} /* comes from agent table */
  field zip as char       {&nodeType} /* comes from agent table */
  field auditStartDate as datetime    {&nodeType}
  field auditFinishDate as datetime   {&nodeType}
  field schedStartDate as datetime    {&nodeType}
  field schedFinishDate as datetime   {&nodeType}
  field onsiteStartDate as datetime   {&nodeType}
  field onsiteFinishDate as datetime  {&nodeType}
  field auditor as char               {&nodeType}
  field auditorUsername as char       {&nodeType}
  field score as int                  {&nodeType}
  field uid as char                   {&nodeType}
  field contactName as char           {&nodeType}
  field contactPhone as char          {&nodeType}
  field contactFax as char            {&nodeType}
  field contactEmail as char          {&nodeType}
  field contactOther as char          {&nodeType}
  field deliveredTo as char           {&nodeType}
  field comments as char              {&nodeType}
  field stateID as char               {&nodeType}
  field stat as char                  {&nodeType}
  field auditType as char             {&nodeType}
  field errType as char               {&nodeType}
  field draftReportDate as datetime   {&nodeType}
  field grade as int                  {&nodeType}
 
  /* server fields reflecting original posting */
  field submittedBy as char      {&nodeType}
  field submittedAt as datetime  {&nodeType}
  
  /* not in database */
  field auditScore as int             {&nodeType}      
  field auditDate as datetime         {&nodeType}
  field parentName as char            {&nodeType}
  field parentAddr as char            {&nodeType}
  field parentCity as char            {&nodeType}
  field parentState as char           {&nodeType}
  field parentZip as char             {&nodeType}
  field contactPosition as char       {&nodeType}
  field policyType as char            {&nodeType}  /* P)aper, E)Jacket, B)oth */
  field mainOffice as logical         {&nodeType}
  field numOffices as int             {&nodeType}
  field numEmployees as int           {&nodeType}
  field numUnderwriters as int        {&nodeType}
  field lastRevenue as decimal        {&nodeType}
  field ranking as int                {&nodeType}
  field services as char              {&nodeType}
  field escrowReviewer as char        {&nodeType}
  field escrowReviewDate as datetime  {&nodeType}
  field auditGap as character         {&nodeType}
  field startGap as character         {&nodeType}
  field ERRscore as int               {&nodeType}
  field cQarID as character           {&nodeType}
  
  /* description */
  field statDesc as character        {&nodeType}
  field auditTypeDesc as character   {&nodeType}
  field errTypeDesc as character     {&nodeType}
  field username as character        {&nodeType}
  field reasonCode as character      {&nodeType}
  
  
  /* used for the scheduling screen */
  field schedNewAudit as logical     {&nodeType}
  field schedReassign as logical     {&nodeType}
  field schedReschedule as logical   {&nodeType}
  field schedAuditType as logical    {&nodeType}
  
  field createDate as datetime        {&nodeType}
  field finalReportDate as datetime   {&nodeType}
  
  field priorAuditDate as datetime    {&nodeType}
  field priorAuditScore as int        {&nodeType}
  field priorAuditType as character   {&nodeType}
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


