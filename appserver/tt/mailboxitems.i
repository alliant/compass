/*------------------------------------------------------------------------
File        : tt/mailboxitems.i
Purpose     :

Syntax      :

Description : store email headers
                                           
Author(s)   : K.R
Created     : 09-04-2023
Notes       :
----------------------------------------------------------------------*/
&IF DEFINED(tableAlias) = 0 &THEN
&SCOPED-DEFINE tableAlias MailboxItems
&ENDIF

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "MailboxItems"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif
  
define temp-table {&tableAlias} no-undo {&serializeName}
  field mailFrom	        as  character   {&nodeType}
  field mailActualFrom      as  character   {&nodeType}
  field mailActualTo        as  character   {&nodeType}    
  field mailSubject         as  character   {&nodeType}
  field mailMessageUID      as  character   {&nodeType}
  field mailSent            as  datetime-tz {&nodeType}
  .

