&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/Qualification.i
Purpose     :

Syntax      :

Description :

Author(s)   : Sachin Chaturvedi
Created     : 02-9-2018
Notes       :

Modification:
Date          Name          Description
03/19/2018    Rahul         Added new fields
04/09/2018    Rahul         Added field "name".
04/19/2018    Rahul         Modified to change "Status_" field to "Stat"
05/23/2018    Rahul         Added field inUse.
06/02/2018    Rahul         Added new fields.
07/12/2018    Rahul         Added new field hasDoc.
08/17/2018    Rahul         Added new field inUseCompany.
12/12/2018    Rahul Sharma  Added new fields for last review information
01/04/2019    Gurvindar     Added index idxQual
11/28/2024    SRK           Added qualificationType field
----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias Qualification
&endif
   
def temp-table {&tableAlias}
  field qualificationID        as integer
  field stateID                as character
  field entity                 as character
  field entityId               as character
  field name                   as character
  field qualification          as character
  field qualificationType      as character
  field qualificationNumber    as character  
  field active                 as log
  field stat                   as character 
  field authorizedBy           as character
  field effectiveDate          as datetime
  field expirationDate         as datetime
  field notes                  as character 
  field dateCreated            as datetime
  field userCreated            as character
  field dateInactive           as datetime
  field userInactive           as character
  field coverageAmt            as decimal
  field deductibleAmt          as decimal
  field insuranceBroker        as character
  field provider               as character
  field retrodate              as datetime
  field inUse                  as logical
  field inUseAgent             as logical
  field inUseRole              as logical
  field inUseAttorney          as logical  
  field inUseCompany           as logical 
  field hasDoc                 as logical
  field aggregateAmt           as decimal
  
  field reviewDate             as datetime  /* used for client only */
  field reviewBy               as character /* used for client only */
  field nextReviewDueDate      as datetime  /* used for client only */  
  field numvaliddays           as integer   /* used for client only */
  field qualmet                as logical   /* used for client only */
  field qualreqid              as integer   /* used for client only */
  index idxQual qualification
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


