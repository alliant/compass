&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/policyaudit.i
Purpose     : used to fetch/store policy audit details records

Syntax      :

Description : Used specifically for AR app 

Author(s)   : Shefali
Created     : 04-20-2021
Notes       : 

Modification:
Date         Name           Description 
----------------------------------------------------------------------*/
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias policyaudit
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

DEF TEMP-TABLE {&tableAlias}
  field policyID              as character {&nodeType}
  field fileNumber            as character {&nodeType}
  field agentID               as character {&nodeType}
  field agentname             as character {&nodetype}
  field amount                as decimal   {&nodeType}
  field postDate              as datetime  {&nodeType}
  field action                as character {&nodeType}
  field reference             as character {&nodeType}
  field createdby             as character {&nodeType}
  field note                  as character {&nodeType}

  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 16.67
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


