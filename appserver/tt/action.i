&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : action.i
    Purpose     : Action datastructure
    Author(s)   : Rahul
    Created     : 7.11.2017
    Modification:
    Date          Name      Description
    07/29/2022    Shefali   Task #: 95509 :Implement clob framework changes            
  ----------------------------------------------------------------------*/
  
&if defined(tableAlias) = 0 &THEN
&scoped-define tableAlias action
&endif

&IF DEFINED(nodeName) = 0 &THEN
&SCOPED-DEFINE nodeName XML-NODE-NAME "action"
&ELSE
&SCOPED-DEFINE nodeName XML-NODE-NAME "{&nodeName}"
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ELSE
&SCOPED-DEFINE nodeType XML-NODE-TYPE "{&nodeType}"
&ENDIF

def temp-table {&tableAlias} no-undo {&nodeName}
  field actionID           as integer column-label "Action" format ">>>>>9" {&nodeType} 
  field source             as character column-label "Source" format "x(50)" {&nodeType}
  field sourceID           as character column-label "Source ID" format "x(50)" {&nodeType}
  field targetProcess      as character serialize-hidden {&nodeType}
  field findingID          as integer column-label "Finding" format ">>>>>9" {&nodeType}
  field actionType         as character serialize-hidden {&nodeType}
  field actionTypeDesc     as character column-label "Type" format "x(20)" {&nodeType}
  field comments           as character column-label "Action" format "x(200)" {&nodeType}
  field ownerID            as character serialize-hidden {&nodeType}
  field ownerDesc          as character column-label "Owner" format "x(200)" {&nodeType}
  field ownername          as character serialize-hidden {&nodeType}
  field uid                as character serialize-hidden {&nodeType}
  field stat               as character serialize-hidden {&nodeType}
  field statDesc           as character column-label "Status" format "x(20)" {&nodeType}
  field dueDate            as datetime column-label "Due Date" format "99/99/9999" {&nodeType}
  field startDate          as datetime column-label "Start Date" format "99/99/9999" {&nodeType}
  field completeStatus     as character serialize-hidden {&nodeType}
  field completeStatusDesc as character column-label "Complete!Status" format "x(20)" {&nodeType}
  field method             as character serialize-hidden {&nodeType}
  field createdDate        as datetime column-label "Created" format "99/99/9999" {&nodeType}
  field openedDate         as datetime column-label "Opened" format "99/99/9999" {&nodeType}
  field completedDate      as datetime column-label "Completed" format "99/99/9999" {&nodeType}
  field cancellationDate   as datetime column-label "Cancellation" format "99/99/9999" {&nodeType}
  field notificationDate   as datetime column-label "Notification" format "99/99/9999" {&nodeType}
  field originalDueDate    as datetime column-label "Orig. Due" format "99/99/9999" {&nodeType}
  field extendedDate       as datetime column-label "Extended" format "99/99/9999" {&nodeType}
  field followupDate       as datetime column-label "Follow-Up" format "99/99/9999" {&nodeType}
  field referenceActionID  as character serialize-hidden {&nodeType}
  field iconMessages       as character column-label "Alert Message" format "x(500)" {&nodeType}
  field iconSeverity       as integer serialize-hidden {&nodeType}
  field lastNote           as character column-label "Last Note" format "x(500)" {&nodeType}
  
  /* for the client only */
  field severity           as integer serialize-hidden {&nodeType}
  field msg                as character column-label "Alert Message" format "x(500)" {&nodeType}
  field hasDoc             as logical column-label "Has!Doc" {&nodeType} 
  field section            as character serialize-hidden {&nodeType}
  field sectionID          as character serialize-hidden {&nodeType}
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


