&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* 
@name tt/checkrequest.i
@description Temp table for the LIST builder
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias checkrequest
&ENDIF

define temp-table {&tableAlias}
  field claimID as integer
  field claimSummary as character
  field altaRisk as character
  field altaResponsibility as character
  field claimCategory as character
  /* agent info */
  field agentName as character
  field agentID as character
  field agentFileNumber as character
  /* policy info */
  field policyID as integer
  field policyAmount as decimal
  field policyDate as datetime
  field insuredName as character
  /* the amount of the invoice */
  field amount as decimal
  /* account info */
  field account as character
  field accountName as character
  /* vendor info */
  field vendorID as character
  field vendorName as character
  field vendorAddress as character
  /* contact info */
  field contactName as character
  field contactEmail as character
  /* invoice info */
  field invoiceDate as datetime
  field invoiceNotes as character
  field invoiceNumber as character
  field invoiceDue as datetime
  /* company info (needed for receivables)  */
  field companyTitle as character
  field companyAddress as character
  field companySuite as character
  field companyCity as character
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
