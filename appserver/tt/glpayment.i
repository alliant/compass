/*------------------------------------------------------------------------
File        : tt/glPayment.i
Purpose     :

Syntax      :

Description :

Author(s)   : Rahul
Created     : 01-29-2020
Notes       :

Modification:
Date          Name      Description

----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias glPayment
&endif
   
define temp-table {&tableAlias}
  field agentid       as character
  field artranID      as integer
  field chknum        as character
  field pmtid         as character
  field postingdate   as datetime
  field argldef       as character
  field arglref       as character
  field debit         as decimal
  field credit        as decimal
  field depositRef    as character
  field description   as character
  field voided        as character
  field notes         as character
  .
