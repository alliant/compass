&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 policy.i
 POLICY data structure
 D.Sinclair 5.30.2012
 @Modified :
 Date        Name          Comments 
 08-01-2022  S Chandu      added xmlNodename
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias arcpolicyresult
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

&if defined(serializename) = 0 &then
&scoped-define serializename serialize-name "arcpolicyresult"
&else
&scoped-define serializename serialize-name "{&serializename}"
&endif

define temp-table {&tableAlias} no-undo {&serializename}
  field PolicyId as integer   {&nodeType}
  field TypeId   as integer   {&nodeType}
  field ErrMsg   as character {&nodeType} serialize-name "Error"
  field PartyId  as character {&nodeType}
  field Insured  as character {&nodeType}

  index iPolicyID is primary unique PolicyID
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Typeas Include
   Allowas 
   Framesas 0
   Add Fields toas Neither
   Other Settingsas INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 16.67
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


