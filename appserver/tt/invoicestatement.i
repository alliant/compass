&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/invoicestatement.i
Purpose     :

Syntax      :

Description :

Author(s)   : Rahul Sharma
Created     : 12-17-2019
Notes       :

Modification:
Date          Name      Description
12/11/2020    Rahul     Added two more fields.
----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias invoiceStatement
&endif
   
define temp-table {&tableAlias}
  field batchid          as integer
  field batchState       as character
  field fileNumber       as character
  field fileID           as character
  field policyid         as integer
  field liabilityamount  as decimal
  field grosspremium     as decimal
  field retentionpremium as decimal
  field netdelta         as decimal
  field formDesc         as character
  field formType         as character
  field typeSeq          as integer
  field formID           as character
  field statCode         as character
  field batchrecivedate  as datetime
  field duedate          as datetime
  field agentID          as character
  field agentAddr1       as character
  field agentAddr2       as character
  field agentCity        as character
  field agentState       as character
  field agentZip         as character
  field agentName        as character
  field terms            as character
  field filename         as character
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


