&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : tag.i
    Purpose     : To get tags associated to an entity

    Syntax      :

    Description :

    Author(s)   : Sachin Chaturvedi
    Created     : 11/19/2019
    Notes       :
    Modification:
    Date          Name       Description 
    06/26/2023    SB         Task# 104008 Modify to remove private field and add createddate field. 
    11/27/2023    SRK        Modified to add isSystem field.
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias tag
&ENDIF
define temp-table {&tableAlias}
  field tagID            as integer     serialize-hidden
  field entity           as character
  field entityID         as character
  field UID              as character   serialize-hidden
  field name             as character
  field entityName       as character  /* client side field */
  field username         as character serialize-hidden  /* client side field */
  field createdDate      as datetime  serialize-hidden
  field active           as logical   serialize-hidden
  field isSystem         as logical   serialize-hidden
  field percentageMatch  as integer   serialize-hidden
  
  index entityID entityID
  index name name
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 62.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


