/*------------------------------------------------------------------------
@file officenote.i
@description Datastructure for officenote table
@author Vignesh Rajan
@version 1.0
@created 10/30/2023
Modification:

----------------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias officeNote
&endif

&if defined(nodetype) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "officeNote"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
  field officeID as integer
  field seq      as integer
  field noteDate as datetime
  field uid      as character
  field category as character
  field subject  as character
  field noteType as character
  field notes    as character
  .


