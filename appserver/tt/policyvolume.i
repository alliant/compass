&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 policyvolume.i
 POLICY VOLUME data structure
 D.Sinclair 5.30.2012
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias policyvolume
&ENDIF
 
&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF
 
DEF TEMP-TABLE {&tableAlias}
  FIELD agentID AS CHARACTER {&nodeType}
  FIELD stateID AS CHARACTER {&nodeType}
  FIELD agentName as character {&nodeType}
  FIELD manager as character {&nodeType}
  FIELD stat as character {&nodeType}
  FIELD officeID as integer {&nodeType}
  FIELD officeName AS CHARACTER {&nodeType}
  FIELD policyLimit AS integer {&nodeType}
  FIELD unremitted AS integer {&nodeType}
  FIELD month3 as integer {&nodeType}
  FIELD month2 as integer {&nodeType}
  FIELD month1 as integer {&nodeType}
  FIELD policiesAvailable AS integer {&nodeType}
  FIELD daysSinceLastIssue as integer {&nodeType}
  FIELD threeMonthAverage AS decimal {&nodeType}
  FIELD availableAvgRatio AS decimal {&nodeType}
  FIELD unremittedAvgRatio AS decimal {&nodeType}
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 16.67
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


