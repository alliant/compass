&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/fulfillment.i
Purpose     :

Syntax      :

Description :

Author(s)   : Gurvindar 
Created     : 06-09-2019
Notes       :

Modification:
Date          Name           Description
10-22-2019    Shubham      Added new fields in fulfillment.
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias fulfillment
&ENDIF

define temp-table {&tableAlias}
  field fulfillmentID   as integer
  field requirementID   as integer
  field entity          as character
  field entityID        as character
  field qualificationID as integer
  field activatedBy     as character
  field activatedDate   as datetime
  field stateReqQualID  as integer

/* for client side use only*/

  field qualEntityID          as character  
  field qualification         as character
  field name                  as character
  field stat                  as character    
  field fulfilledBy           as character
  field effectiveDate         as datetime
  field expirationDate        as datetime
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


