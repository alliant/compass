/*------------------------------------------------------------
   tt/vendortopic.i
   @created 11.21.2022
   @author S chandu
   Modifications:
   Name          Date       Note
   ------------- ---------- -----------------------------------------------
   Ube          01/24/2023  Task 102000 - Changed as per new framework
   Sagar K      10/23/2023  Modified change vendorproduct to vendortopic
   --------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias vendortopic
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "vendortopic"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
  field vendorID     as character {&nodeType}
  field topic        as character {&nodeType}
  field price        as decimal   {&nodeType}
  field vendorName   as character {&nodeType}
  .
