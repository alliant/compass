&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
  tt/unreportpolicy.i
  Unremitted policies temp table
  Author: John Oliver
  Date: 2023.05.12

*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias unreportpolicy
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF


define temp-table {&tableAlias} no-undo
  field officeID       as character {&nodeType}
  field officeName     as character {&nodeType}
  field agentID        as character {&nodeType}
  field agentName      as character {&nodeType}
  field agentStat      as character {&nodeType}
  field agentStatDesc  as character {&nodeType}
  field regionID       as character {&nodeType}
  field regionDesc     as character {&nodeType}
  field managerID      as character {&nodeType}
  field managerName    as character {&nodeType}
  field stateID        as character {&nodeType}
  field stateDesc      as character {&nodeType}
  field bucketLT30     as integer   {&nodeType}
  field bucket31to60   as integer   {&nodeType}
  field bucket61to90   as integer   {&nodeType}
  field bucket91to180  as integer   {&nodeType}
  field bucket181to270 as integer   {&nodeType}
  field bucket271to365 as integer   {&nodeType}
  field bucket366to730 as integer   {&nodeType}
  field bucketGT731    as integer   {&nodeType}
  field total          as integer   {&nodeType}
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 1
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


