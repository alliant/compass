/*------------------------------------------------------------------------
    File        : templatefield.i
    Purpose     : templatefield datastructure
    Author(s)   : Shweta Dhar
    Created     : 7.08.2022
    Modification:
    Date          Name      Description
              
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias ttTemplatefield
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ELSE
&SCOPED-DEFINE nodeType XML-NODE-TYPE "{&nodeType}"
&ENDIF

&IF DEFINED(serializeName) = 0 &THEN
&SCOPED-DEFINE serializeName serialize-name "templatefield"
&ELSE
&SCOPED-DEFINE serializeName serialize-name "{&serializeName}"
&ENDIF

def temp-table {&tableAlias} NO-UNDO {&serializeName}
field templateID     as integer   {&nodeType}
field fieldname      as character {&nodeType}
field fieldlabel     as character {&nodeType}
field datatype       as character {&nodeType}.
