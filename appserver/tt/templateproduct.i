/* tt/templateproduct.i
   @created 02.08.2024
   @author S Chandu
   @Modifications:
   Date          Name           Description.
 */
 
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias templateproduct
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "templateproduct"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
  field productID         as integer   {&nodeType}   
  field templateID        as integer   {&nodeType}                            
  .





