&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* 
@name tt/reportclaimsinvoices.i
@description Temp table for the REPORT CLAIMS INVOICES builder
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias reportclaiminvoices
&ENDIF

define temp-table {&tableAlias}
  field stat as character
  field claimID as integer
  field apinvID as integer
  field assignedTo as character
  field vendorID as character
  field vendorName as character
  field refCategory as character
  field invoiceNumber as character
  field invoiceDate as datetime
  field age as integer
  field amount as decimal
  field trxDate as datetime
  field acct as character
  field reserveBalance as decimal
  field insufficientReserve as logical
  .
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
