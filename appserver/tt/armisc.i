&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------------
File        : tt/armisc.i
Purpose     : used to fetch/store AR invoices

Syntax      :

Description : Used specifically for AR app 

Author(s)   : Rahul Sharma
Created     : 07-16-2019
Notes       : 

Modification:
Date       Name           Description
09/05/19   Rahul Sharma   Modified temp-table to add new field reference, fileID                             
------------------------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias arMisc 
&endif
   
def temp-table {&tableAlias}
  field arMiscID        as integer 
  field revenuetype     as character /* (P)remium,(C)laim,(S)earch,(M)iscellaneous */
  field transID         as character
  field sourcetype      as character
  field sourceID        as character
  field type            as character /* Transaction Type: Invoice, Debit, Credit */
  field entity          as character
  field entityID        as character
  field filenumber      as character
  field fileID          as character /* Normalized file no */
  field transamt        as decimal
  field transdate       as datetime /*  Posting effective Date arTran.transDate */
  field creditInvoiceDt as datetime /* Credited On or Invoiced On arMisc.transDate */  
  field duedate         as datetime
  field createdate      as datetime
  field createdby       as character
  field notes           as character
  field reference       as character /* credit memo no */
  field transtype       as character
  field ledgerID        as integer
  field voidLedgerID    as integer
  field posted          as logical
  field postedBy        as character
  field postDate        as datetime
  field void            as logical
  field voidedBy        as character
  field voidDate        as datetime
  field remainingAmt    as decimal
  field archived        as logical
  field selectrecord    as logical   /* Client side */
  field agentname       as character /* Client side */
  field username        as character /* Client side */
  field postByName      as character /* Client side */
  field voidByName      as character /* Client side */
  field itemSeq         as character /* Client side */
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


