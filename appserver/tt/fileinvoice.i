/* 
@file tt/fileInvoice.i
@description Data structure for file invoice to get unnormalized data

@author Shefali
@created 02/06/2023
@modified
Name          Date       Note
Chandu      05/21/2024   Removed cancel related fields and and ProductID field.
------------- ---------- -----------------------------------------------

 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias fileinvoice
&ENDIF

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "{&tableAlias}"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
     field fileInvID           as integer     {&nodeType}
     field agentFileID         as integer     {&nodeType} 
     field fileNumber          as character   {&nodeType}  
     field agentID             as character   {&nodeType}  
     field agentName           as character   {&nodeType}   
     field agentAddress        as character   {&nodeType}  
     field invoiceNumber       as character   {&nodeType}  
     field invoiceAmt          as decimal     {&nodeType}  
     field invoiceDate         as date        {&nodeType}
     field posted              as logical     {&nodeType}
     field postedAmount        as decimal     {&nodeType}
     field postedDate          as datetime    {&nodeType}
     field outstandingAmount   as decimal     {&nodeType}
     
     
     /* invoicedetail */
     field suffix              as integer     {&nodeType}
     field seq                 as integer     {&nodeType}
     field description         as character   {&nodeType}
     field amount              as decimal     {&nodeType}
     field itemAmount          as decimal     {&nodeType}
     .
