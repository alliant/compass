&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/comlog.i
Purpose     :

Syntax      :

Description :

Author(s)   : Sachin Chautrvedi
Created     : 05-17-2018
Notes       :

Modification:
Date          Name            Description
05/30/2018    Rahul           Added new field.
06/19/2018    Naresh Chopra   Added new fields.(username,action)
08/18/2018    Naresh Chopra   Added new field (stateId)
----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias comlog
&ENDIF
   
def temp-table {&tableAlias}
  field logID      as integer
  field refType    as character  /*agent,person*/
  field refID      as character  /* agentID, personID,attorneyID*/
  field seq        as integer
  field logdate    as datetime
  field uid        as character  /* logged In user*/
  field username   as character
  field action     as character
  field notes      as character
  field comstat    as character
  field objRef     as character
  field objID      as character
  field stateID    as character
  
  /* for the client only */
  field entityName as character
  field role       as character
  field sequence   as character
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


