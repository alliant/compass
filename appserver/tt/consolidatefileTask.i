/* tt/consolidatefileTask.i
   @created 07.17.2023
   @author Shefali
   Modifications:
   Date          Name           Description
 */
 
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias consolidatefileTask
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "consolidatefileTask"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
  field stateID             as character    {&nodeType}
  field vendor              as character    {&nodeType}
  field vendorName          as character    {&nodeType}
  field topic               as character    {&nodeType}
  field glaccount           as character    {&nodeType}
  field glaccountDesc       as character    {&nodeType}
 
  field filetaskcount       as integer      {&nodeType} 
  field costSum             as decimal      {&nodeType} 
  field posted              as logical      {&nodeType}
  field apInvId             as integer      {&nodeType}
  .  







