&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
  tt/mailparameter.i
  MAIL PARAMETERs for sending a HTML email
  Author: John Oliver
  Date: 08.01.2017
*/


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias mailparameter
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

define temp-table {&tableAlias}
  field name              as character {&nodeType}
  field uid               as character {&nodeType}
  field pass              as character {&nodeType}
  field username          as character {&nodeType}
  field agentID           as character {&nodeType}
  field batchID           as integer   {&nodeType}
  field batchCount        as character {&nodeType}
  field agentName         as character {&nodeType}
  field shareFileFolder   as character {&nodeType}
  field content           as character {&nodeType}
  field rowsTotal         as integer   {&nodeType}
  field rowsImported      as integer   {&nodeType}
  field rowsErred         as integer   {&nodeType}
  field timeTaken         as decimal   {&nodeType}
  field auditType         as character {&nodeType}
  field auditReschedule   as character {&nodeType}
  field auditReassign     as character {&nodeType}
  field auditNew          as character {&nodeType}
  field report            as character {&nodeType}
  field agentManager      as character {&nodeType}
  field agentManagerEmail as character {&nodeType}
  field qarID             as integer   {&nodeType}
  field contactPhone      as character {&nodeType}
  field contactEmail      as character {&nodeType}
  field contactName       as character {&nodeType}
  field yearsActive       as integer   {&nodeType}
  field agentAddress      as character {&nodeType}
  field agentCity         as character {&nodeType}
  field agentState        as character {&nodeType}
  field agentZip          as character {&nodeType}
  field agentFullAddress  as character {&nodeType}
  field actionMonth       as character {&nodeType}
  field actionYear        as integer   {&nodeType}
  field periodID          as integer   {&nodeType}
  field mailDate          as datetime  {&nodeType}
  field serverURL         as character {&nodeType}
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 1
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

