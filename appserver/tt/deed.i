/* 
   tt/deed.i
   Datastructure for an deed
   @created 
   @author 
   Modifications:
   Date          Name           Description
   
 */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias deed
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "deed"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
&if defined(getAgentFile) &then
  field deedID                   as integer   {&nodeType}
  field agentFileID              as integer   {&nodeType}
  field seq                      as integer   {&nodeType}
  field grantee                  as character {&nodeType}
  field grantor                  as character {&nodeType}
  field typeofdeed               as character {&nodeType}
  field deeddate                 as datetime  {&nodeType}
  field recordeddate             as datetime  {&nodeType}  
  field book                     as character {&nodeType} 
  field pagenumber               as character {&nodeType} 
  field documentnumber           as character {&nodeType}
  field instrumentnumber         as character {&nodeType}
  field certificateoftitlenumber as character {&nodeType}
  field typeoftitle              as character {&nodeType}
  
&elseif defined(updateFile) &then
  field deedID                   as integer   {&nodeType}
  field agentFileID              as integer   {&nodeType}
  field seq                      as integer   {&nodeType}
  field grantee                  as character {&nodeType}
  field grantor                  as character {&nodeType}
  field typeofdeed               as character {&nodeType}
  field deeddate                 as datetime  {&nodeType}
  field recordeddate             as datetime  {&nodeType}  
  field book                     as character {&nodeType} 
  field pagenumber               as character {&nodeType} 
  field documentnumber           as character {&nodeType}
  field instrumentnumber         as character {&nodeType}
  field certificateoftitlenumber as character {&nodeType}
  field typeoftitle              as character {&nodeType}
  
&else
  field deedID                   as integer   {&nodeType}
  field agentFileID              as integer   {&nodeType}
  field seq                      as integer   {&nodeType}
  field grantee                  as character {&nodeType}
  field grantor                  as character {&nodeType}
  field typeofdeed               as character {&nodeType}
  field deeddate                 as datetime  {&nodeType}
  field recordeddate             as datetime  {&nodeType}  
  field book                     as character {&nodeType} 
  field pagenumber               as character {&nodeType} 
  field documentnumber           as character {&nodeType}
  field instrumentnumber         as character {&nodeType}
  field certificateoftitlenumber as character {&nodeType}
  field typeoftitle              as character {&nodeType}
&endif
.
