&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/reqqual.i
Purpose     :

Syntax      :

Description :

Author(s)   : Naresh
Created     : 04-25-2018
Notes       :

Modification:
Date          Name         Description
08/27/2018   Rahul Sharma  Added new field role
12/04/2018   Rahul Sharma  Modified filename ttreqqual to reqqual
08/28/2020   VJ            Added new field role related and stateid
03/08/2023   S Chandu      Added new filed reviewDate
04/09/2024   Sachin        Task#111982 Show 'Fulfilled By' in person and organization fulfillment reports
06/27/2024   Sachin        Task# 113997 Recommitting the removed changes related to Task #111982 
                           (Show 'Fulfilled By' in person and organization fulfillment reports)
11/28/2024   Shefali       Task#117467 Modified to add new fields(service, agentmanager) for Agent Fulfillment Report in AMD.
----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias reqqual
&endif
   
def temp-table {&tableAlias}
  field entityId          as character
  field entityName        as character
  field role              as character
  field roleName          as character
  field roleID            as character
  field stateID           as character
  field requirementId     as integer
  field requirementDesc   as character
  field requirementType   as character
  field qualificationId   as integer
  field qualificationDesc as character
  field stat              as character
  field effDate           as datetime
  field expDate           as datetime
  field reviewDate        as datetime
  field fulfilledBy       as character
  field agentmanager      as character
  field service           as character
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


