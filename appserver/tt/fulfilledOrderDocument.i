/*------------------------------------------------------------------------
    File        : fulfilledOrderDocument.i
    Purpose     : AZURE document datastructure
    Author(s)   : Shefali
    Created     : 09.22.2023
    Modified    :
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias fulfilledOrderDocument
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&ENDIF

&if defined(nodeName) = 0 &then
&scoped-define XMLNodeName xml-node-name "fulfilledOrderDocument"
&else
&scoped-define XMLNodeName xml-node-name "{&nodeName}"
&endif


&if defined(serializename) = 0 &then
&scoped-define serializename serialize-name "fulfilledOrderDocument"
&else
&scoped-define serializename serialize-name "{&serializename}"
&endif

define temp-table {&tableAlias} no-undo {&XMLNodeName} {&serializename}
  field refType       as character {&nodeType}
  field refId         as integer   {&nodeType}
  field refGroup      as character {&nodeType}
  field fileId        as character {&nodeType}                              format "x(50)"
  field originalName  as character {&nodeType} column-label "Name"          format "x(256)"
  field createdDate   as character {&nodeType} column-label "Date Saved"    format "x(50)"
  field createdById   as character {&nodeType} serialize-name "userId"
  field userName      as character {&nodeType} column-label "Saved By"      format "x(50)"
  field entity        as character {&nodeType} 
  field entityId      as character {&nodeType}  
  field category      as character {&nodeType} 
  field extension     as character {&nodeType} column-label "Type"          format "x(12)"          
  field container     as character {&nodeType}                              format "x(200)"
  field fileSize      as decimal   {&nodeType} serialize-name "Size"          
  field url           as character {&nodeType}                              format "x(200)"
  
  field fileSizeDesc  as character {&nodeType} column-label "Size"          format "x(15)"
  field fullName      as character {&nodeType} serialize-name "fileName"    format "x(1000)"
  .
