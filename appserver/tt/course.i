/* tt/course.i
   Datastructure for an course
   @created 11.22.2024
   @author S Chandu
   Modifications:
   Date          Name           Description
   
 */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias course
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "course"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo  {&serializeName}
  field courseID          as  integer    {&nodeType}
  field stateID           as  character  {&nodeType}
  field type              as  character  {&nodeType}
  field name              as  character  {&nodeType}
  field description       as  character  {&nodeType}
  field canSchedule       as  logical    {&nodeType}
  field isApproved        as  logical    {&nodeType}
  field approvedDate      as  date       {&nodeType}
  field approvedBy        as  character  {&nodeType}
  field creditHours       as  decimal    {&nodeType}
  field paymentUrl        as  character  {&nodeType}
  field instructor        as  character  {&nodeType}
  field notes             as  character  {&nodeType}
  field createdDate       as  datetime   {&nodeType}
  field createdBy         as  character  {&nodeType}
  field lastModifiedDate  as  datetime   {&nodeType}
  field lastModifiedBy    as  character  {&nodeType}
  field courseType        as  character  {&nodeType}
  field approvedCourseID  as  character  {&nodeType}
  field stateDesc         as  character  {&nodeType}
  .

