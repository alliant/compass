/* tt/fulfillfile.i
   @created 04.06.2023
   @author S Chandu
   Modifications:
   Date          Name           Description
   08/01/2024    K.R            Modified to remove fulfillmethod field
 */
 
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias fulfill
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "fulfill"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
	field mailto         as character  serialize-name "to"	    {&nodeType} 
	field subject        as character  	  		 {&nodeType}
    field body           as clob        		 {&nodeType} 
	field cc             as logical      		 {&nodeType} 
    .                                      
