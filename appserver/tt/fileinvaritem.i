/*
@file tt/fileInvArItem.i
@description Data structure for file invoice items

@author Shefali
@created 02/06/2023
@modified
Name          Date       Note
------------- ---------- -----------------------------------------------

 */
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias fileinvaritem
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "{&tableAlias}"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
     field fileInvID    as integer   {&nodeType}
     field suffix       as integer   {&nodeType}
     field seq          as integer   {&nodeType}
     field amount       as decimal   {&nodeType} format "zzz,zz9.99"
     field description  as character {&nodeType}
     field productID    as character {&nodeType}
     field arinvid      as integer   {&nodeType}
     field postedamount as integer   {&nodeType}
     
     field invNumber    as character {&nodeType}
     field invAmtFinal  as decimal   {&nodeType} format "zzz,zz9.99"
     .