/* tt/titlesoftware.i
   Datastructure for an titlesoftware
   @created 07.11.2022
   @author Sachin Chaturvedi
   Modifications:
   Date          Name           Description
   11/22/2024    SRK           Modifidied to add new fields
   
 */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias titlesoftware
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "titlesoftware"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo  {&serializeName}
  field softwareID        as  integer    {&nodeType}
  field name              as  character  {&nodeType}
  field vendorID          as  character  {&nodeType}
  field addr              as  character  {&nodeType}
  field city              as  character  {&nodeType}
  field state             as  character  {&nodeType}
  field zip               as  character  {&nodeType}
  field url               as  character  {&nodeType}
  field contactname       as  character  {&nodeType}
  field contactEmail      as  character  {&nodeType}
  field contactPhone      as  character  {&nodeType}
  field progExec          as  character  {&nodeType}
  field CreatedDate       as  datetime   {&nodeType}
  field CreatedBy         as  character  {&nodeType}
  field lastModifiedDate  as  datetime   {&nodeType}
  field lastModifiedBy    as  character  {&nodeType}
  field active            as  logical    {&nodeType}
  field FulfillName       as  character  {&nodeType}
  field description       as  character  {&nodeType}
  field outprogexec       as  character  {&nodeType}
  field EDICode           as  character  {&nodeType}
  field EDIPayloadFormat  as  character  {&nodeType}
  .

