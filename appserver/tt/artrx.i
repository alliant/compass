&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/artrx.i
   Generic datastructure for A/R receipts (partial or complete)
   D.Sinclair 4.23.2015
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias artrx
&ENDIF

def temp-table {&tableAlias}
  field artrxID as int

  field transType as char     /* A)pproved or V)oided */
  field transAmount as deci   /* Actual amount received */
  field transDate as datetime /* Date received/deposited */

  field arinvID as int

  field refType as char
  field refID as char
  field refSeq as int
  field refCategory as char

  field uid as character
  field username as char
  field notes as char
 
 field hasDocument as logical
 field documentFilename as character
  .

/* Indexes:  artrxID + transType (unique; max 2 records - see below)
             arinvID (non-unique, foreign key)
             refType, refID, refSeq, refCategory

   If a received payment is "voided", a new artrx is created with the
   same artrxID and a type = "V".  The transAmount is negative.  A payment
   can be voided once and for the complete amount.

   Claims   refType = "C"
            refID = string(claim.claimID)
            refSeq = 0
            refCategory = ""
 */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


