&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/qualstatus.i
Purpose     :

Syntax      :

Description : Made for Qualification status report

Author(s)   : Sachin Chaturvedi
Created     : 05-8-2018
Notes       :

Modification:
Date          Name          Description
07/24/2018   Rahul Sharma   Added new field inUse
----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias qualstatus
&endif
   
def temp-table {&tableAlias}
  field qualificationID        as integer
  field stateID                as character
  field entity                 as character
  field entityId               as character
  field name                   as character
  field qualification          as character
  field qualificationNumber    as character  
  field active                 as logical
  field inUse                  as logical
  field stat                   as character 
  field authorizedBy           as character
  field effectiveDate          as datetime
  field expirationDate         as datetime
  field notes                  as character 
  field dateCreated            as datetime
  field userCreated            as character
  field dateInactive           as datetime
  field userInactive           as character
  field reviewID               as integer
  field lastReviewDate         as datetime 
  field lastReviewBy           as character
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


