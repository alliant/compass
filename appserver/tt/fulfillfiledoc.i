/* tt/fulfillfiledoc.i
   @created 04.06.2023
   @author S chandu
   Modifications:
   K.R  12/09/2024  Modified to add new field to store the actual path of the downloaded document

 */
 
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias docs
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "doc"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
    field filename         as character   {&nodeType} 
	field id               as character   {&nodeType}
    field filepath         as character   {&nodeType} serialize-hidden
    .                                      
