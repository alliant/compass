&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/artran.i
Purpose     : used to fetch/store AR Transaction records

Syntax      :

Description : Used specifically for AR app 

Author(s)   : Rahul Sharma
Created     : 07-16-2019
Notes       : 

Modification:
Date         Name           Description
07/18/19     Rahul Sharma   Modified to add new fields
09/03/19     Rahul Sharma   Modified to add fields related to check 
06/02/21     SA             Modified to add fields total refund
----------------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias artran
&endif
   
def temp-table {&tableAlias}
  field artranID         as integer
  field revenuetype      as character
  field tranID           as character
  field seq              as integer
  field reference        as character /* check num + credit memo no*/
  field sourcetype       as character
  field sourceID         as character
  field type             as character
  field entity           as character /* Agent */
  field entityID         as character /* AgentID */
  field filenumber       as character
  field fileID           as character /* Normalized file no */    
  field tranamt          as decimal   /* invoice amt + check amt + payment applied amt */
  field remainingamt     as decimal   /* invoice remaining amt + check remaining amt */
  field appliedamt       as decimal   /* allow to distribute the total applied amount to invoices */
  field trandate         as datetime  /* invoice date + check date + apply payment date */
  field duedate          as datetime
  field void             as logical
  field voiddate         as datetime
  field voidby           as character
  field notes            as character 
  field createddate      as datetime
  field createdby        as character
  field fullypaid        as logical
  field transtype        as character /* (F)ile or (G)eneral */
  field postdate         as datetime
  field postby           as character
    
  field totalPymtAmt     as decimal   /* Initial Payment Amt */
  field accumbalance     as decimal   /* Rolling Balance */
  field appliedamtbyref  as decimal   /* Amount applied on check/credit */
  field selectrecord     as logical   /* Client side apply Invoice + file */
  field username         as character /* Client side */
  field entityname       as character /* Client side */
  field arnotes          as character /* Client side */
  field isRefapplied     as logical
  
  field batchList          as character
  field batchFileAmtList   as character
  field netProcessed       as decimal   /* Client side:Batch file Amount */
  field latestBatch        as character /* Client side:Last batch */ 
  field ledgerID           as integer
  field voidLedgerID       as integer
  field postbyName         as character
  field voidbyName         as character
  field stat               as character
  field totalrefundamt     as decimal    /* Client side */
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


