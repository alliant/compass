/*------------------------------------------------------------------------
    File        : county.i
    Purpose     : COUNTY datastructure
    Author(s)   : D.Sinclair
    Created     : 1.24.2012 
	Modifications:
    Date          Name           Description
    04.04.2023     SP            Modified to rearrange fields  
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
  &scoped-define tableAlias county
&ENDIF

&IF defined(nodeType) = 0 &THEN
  &scoped-define nodeType xml-node-type "attribute"
&ENDIF

def temp-table {&tableAlias} no-undo

&if defined(getStateCounties) &then
  field stateID     as character {&nodeType} 
  field countyID    as character {&nodeType} label "County ID"   format "x(20)"
  field description as character {&nodeType} label "Common Name" format "x(100)"
  field regionID    as character {&nodeType} 
  field month       as integer   {&nodeType}
  field year        as integer   {&nodeType} 
&else 
  field stateID     as character {&nodeType} serialize-hidden
  field countyID    as character {&nodeType} label "County ID"   format "x(20)"
  field description as character {&nodeType} label "Common Name" format "x(100)"
  field regionID    as character {&nodeType} serialize-hidden
  field month       as integer   {&nodeType} serialize-hidden
  field year        as integer   {&nodeType} serialize-hidden
  
  /* client only */
  field stateName   as character {&nodeType} label "State"       format "x(30)"
  field regionDesc  as character {&nodeType} label "Region"      format "x(100)"
  field monthYear   as character {&nodeType} label "Digitized"   format "x(50)"
  field needRefresh as logical   {&nodeType} serialize-hidden
&endif

  index county is primary unique
    stateID
    countyID
  .
