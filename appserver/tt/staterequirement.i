&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/staterequirement.i
Purpose     :

Syntax      :

Description :

Author(s)   : Rahul Sharma
Created     : 03-14-2018
Notes       :

Modification:
Date          Name       Description
03/20/2018    Rahul      Modified to add "AuthorisedBy" field.
05/28/2018    Rahul      Added new fields.
07/10/2018    Rahul      Added new fields fulfilledBy.
08/01/2018    Gurvindar  Added new fields expire and removed renewal fields.
08/07/2018    Gurvindar  Added new field expireYear.
01/04/2019    Gurvindar  Added index idxappliesTo
10/01/2019    Rahul      Added new field reqFor.
12/06/2021    Shefali    Task# 86699 Added new field fulfillable
11/28/2024    Shefali    Task#117467 Change to condition based to get active requirements 
                         for Agent Fulfillment Report in AMD.
----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias stateRequirement
&ENDIF
   
&if defined(getactiverequirements)  &then
define temp-table {&tableAlias}
  field requirementID                as integer
  field stateID                      as character
  field description                  as character
  field stateDesc                    as character
  .
&else
define temp-table {&tableAlias}
  field requirementID                as integer
  field stateID                      as character
  field description                  as character
  field appliesTo                    as character
  field role                         as character
  field effectiveDate                as datetime
  field expirationDate               as datetime
  field authorizedBy                 as character /* (C)ompany/(T)hirdParty*/
  field qualification                as character
  field fulfilledBy                  as character
  field contactName                  as character
  field contactAddress               as character
  field contactcity                  as character
  field contactstate                 as character
  field contactzip                   as character
  field url                          as character
  field method                       as character
  field notificationApproval         as logical
  field notificationApprovalDays     as integer
  field notificationCancellation     as logical
  field notificationCancellationDays as integer
  field expire                       as character
  field expireMonth                  as integer
  field expireDay                    as integer
  field expireYear                   as integer
  field expireDays                   as integer
  field notes                        as character
  field refId                        as character
  field name                         as character
  field address                      as character
  field reqFor                       as character /*requirement for organization(O) or Person(P)*/
  field fulfillable                  as logical
  index idxappliesTo appliesTo refId description

&endif
.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


