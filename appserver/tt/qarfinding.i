&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : qarfinding.i
    Purpose     : hold FINDING dataset
    Author(s)   : D.Sinclair
    Created     : 3.22.2012
    Notes       :
    Modification:
    Date          Name      Description
    01/04/2017    AC        New field added to handle escrow accounts  
    08/09/2017    AG        Changes related to sectionID
    29/07/2022    SD       Task#95509: Modified to support clob related changes
  ----------------------------------------------------------------------*/
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias finding
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ENDIF

&IF DEFINED(nodeName) = 0 &THEN
&SCOPED-DEFINE nodeName XML-NODE-NAME "finding"
&ELSE
&SCOPED-DEFINE nodeName XML-NODE-NAME "{&nodeName}"
&ENDIF


def temp-table {&tableAlias} no-undo {&nodeName}
 field qarID as int               {&nodeType}
 field sectionID as int           {&nodeType}
 field questionSeq as int         {&nodeType}
 field questionID as char         {&nodeType}
 field description as char        {&nodeType}
 field priority as int            {&nodeType}
 field comments as clob           {&nodeType}
 field files as char              {&nodeType}
 field accounts as char           {&nodeType}
 field reference as char          {&nodeType}
 field uid as char                {&nodeType}
 field version as char            {&nodeType}
 field year as int                {&nodeType}
 field agentID as char            {&nodeType}
 field agentName as char          {&nodeType}
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 14.95
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


