&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/araging.i
Purpose     :

Syntax      :

Description :

Author(s)   : Rahul Sharma
Created     : 10-15-2019
Notes       :

Modification:
Date          Name      Description
09/27/2021    AG        Task 86863: Added tranDate to show Batch posting date
06/28/2022    SA        Task 93485: Added region field
09/06/2022    SD        Task 97317: Add 3 new fields in Aging Report
----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias arAging
&endif
   
define temp-table {&tableAlias}
  field region        as character
  field stateID       as character
  field agentID       as character
  field orgID         as character
  field name          as character
  field managerID     as integer
  field manager       as character
  field fileNumber    as character
  field tranDate      as datetime
  field receiptDate   as datetime
  field postedDate    as datetime
  field invoiced      as decimal
  field artranID      as integer  
  field stat          as character
  field type          as character  
  field tranID        as character   
  field due0_30       as decimal
  field due31_60      as decimal
  field due61_90      as decimal
  field due91_180     as decimal
  field due181_365    as decimal
  field due366_730    as decimal
  field due730_       as decimal
  field balance       as decimal 
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


