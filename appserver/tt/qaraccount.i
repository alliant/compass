&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* account.i
   D.Sinclair 10.20.2015 Added qarID
   
   @modified
   Date          Name   Comments
   29/07/2022     SD    Task#95509: Modified to support clob related changes
   ----------------------------------------------------------------------*/
   
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias account
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ENDIF

&IF DEFINED(nodeName) = 0 &THEN
&SCOPED-DEFINE nodeName XML-NODE-NAME "account"
&ELSE
&SCOPED-DEFINE nodeName XML-NODE-NAME "{&nodeName}"
&ENDIF
   
def temp-table {&tableAlias} {&nodeName}
  field qarID as int                 {&nodeType}
  field bankID as int                {&nodeType}
  field bankName as char             {&nodeType}
  field acctTitle as char            {&nodeType}
  field acctNumber as char           {&nodeType}
  field reconDate as datetime        {&nodeType}
  field reconCurrent as logical      {&nodeType}
  field reconAdjBankBal as decimal   {&nodeType}
  field reconCheckbookBal as decimal {&nodeType}
  field reconTrialBal as decimal     {&nodeType}
  field stmtBal as decimal           {&nodeType}
  field dipBal as decimal            {&nodeType}
  field outChecksBal as decimal      {&nodeType}
  field netAdj as decimal            {&nodeType}
  field netNegAdj as decimal         {&nodeType}
  field calcBal as decimal           {&nodeType}
  field actualCheckbookBal as decimal   {&nodeType}
  field trialBal as decimal          {&nodeType}
  field reconciled as logical        {&nodeType}
  field comments as char             {&nodeType}
  index pi-acct bankID.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


