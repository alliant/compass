/*------------------------------------------------------------------------
File        : tt/productionfile.i
Purpose     :

Syntax      :

Description :

Author(s)   : s Chandu
Created     : 04-29-2024
Notes       :

Modification:
Date          Name       Description
----------------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias productionfile
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif


&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "productionfile"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif
   
define temp-table {&tableAlias} no-undo {&serializeName}
  field agentFileID    as integer   {&nodeType}
  field cancelReason   as character {&nodeType}
  field cancelDate     as datetime  {&nodeType}
  .
