&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/statereqqual.i
Purpose     :

Syntax      :

Description :

Author(s)   : Sachin Chaturvedi
Created     : 03-14-2018
Notes       :

Modification:
Date          Name      Description
03/28/18      Rahul     Added new field description
04/02/18      Rahul     Added new fields
06/19/18      Rahul     Added new field
10/01/18      Rahul     Added new field reqFor
12/06/2021    Shefali   Task# 86699 Added new field notes and reqdesc
----------------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias StateReqQual
&endif
   
define temp-table {&tableAlias} 
  field stateReqQualID       as integer
  field requirementID        as integer
  field description          as character
  field reqappliesTo         as character
  field refId                as character
  field role                 as character
  field qualification        as character
  field appliesTo            as character
  field active               as logical
  field effectiveDate        as datetime
  field expirationDate       as datetime  
  field authorizedBy         as character
  field tempAppliesTo        as character
  field reqFor               as character /*requirement for organization(O) or Person(P)*/
  field reqDesc              as character
  field notes                as character
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


