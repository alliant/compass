&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
  tt/distribution.i
  distribution list table model
  Author: John Oliver
  Date: 06.28.2024

*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias distribution
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF


define temp-table {&tableAlias} no-undo
  /* distributions */
  field distName        as character {&nodeType}
  field description     as character {&nodeType}
  
  /* contacts */
  field personContactID as integer   {&nodeType}
  
  /* from other tables */
  field contactName     as character {&nodeType}
  field agentStateID    as character {&nodeType}
  field agentStat       as character {&nodeType}
  field agentID         as character {&nodeType}
  field agentName       as character {&nodeType}
  field email           as character {&nodeType}
  field doNotCall       as logical   {&nodeType}
  field personID        as character {&nodeType}
  field address         as character {&nodeType}
  
  /* descriptions */
  field agentStateDesc  as character {&nodeType}
  field agentStatDesc   as character {&nodeType}
  .
  

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 1
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


