&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* 
@name tt/listentity.i
@description Temp table for the LIST builder to store how to RUN the criteria
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias listentity
&ENDIF

def temp-table {&tableAlias}
  field displayName as character
  field entityName as character
  field execAction as character
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
