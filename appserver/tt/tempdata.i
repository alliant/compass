&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : tempdata.i
    Purpose     : generic datastructure
    Author(s)   : D.Sinclair
    Created     : 10.21.2014
    Notes       :
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias tempdata
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF


def temp-table {&tableAlias}
 field seq as int {&nodeType}
 field ch1 as char {&nodeType}
 field ch2 as char {&nodeType}
 field ch3 as char {&nodeType}
 field ch4 as char {&nodeType}
 field ch5 as char {&nodeType}
 field ch6 as char {&nodeType}
 field ch7 as char {&nodeType}
 field ch8 as char {&nodeType}
 field ch9 as char {&nodeType}
 field ch10 as char {&nodeType}

 field in1 as int {&nodeType}
 field in2 as int {&nodeType}
 field in3 as int {&nodeType}
 field in4 as int {&nodeType}
 field in5 as int {&nodeType}

 field de1 as decimal {&nodeType}
 field de2 as decimal {&nodeType}
 field de3 as decimal {&nodeType}
 field de4 as decimal {&nodeType}
 field de5 as decimal {&nodeType}

 field dt1 as datetime {&nodeType}
 field dt2 as datetime {&nodeType}
 field dt3 as datetime {&nodeType}
 field dt4 as datetime {&nodeType}
 field dt5 as datetime {&nodeType}

 field lo1 as logical {&nodeType}
 field lo2 as logical {&nodeType}
 field lo3 as logical {&nodeType}
 field lo4 as logical {&nodeType}
 field lo5 as logical {&nodeType}
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


