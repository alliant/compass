/* tt/prodFile.i
   Datastructure for a Production File
   @created 06.04.2024
   @author Archana 
   Modifications:
   Date          Name           Description
   
 */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias prodFile
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "prodFile"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
  field fileARID	    as  integer    {&nodeType}
  field agentfileID	    as  integer    {&nodeType}
  field agentID      	as  character  {&nodeType}
  field agentName       as  character  {&nodeType}
  field fileID          as  character  {&nodeType}
  field fileNumber      as  character  {&nodeType}
  field invoiceDate     as  datetime   {&nodeType}
  field invoicedAmount  as  decimal    {&nodeType}
  field cancelledAmount	as  decimal    {&nodeType}
  field appliedAmount   as  decimal    {&nodeType}
  .
