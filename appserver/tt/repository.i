&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : doc.i
    Purpose     : ARC repository datastructure
    Author(s)   : D.Sinclair
    Created     : 4.4.2013
        Modified    :
                 Date        Name            Comments 
                 08-01-2022  S Chandu        added xmlNodename
                 04-04-2024  Vignesh Rajan   Added 'noSerializeHidden'
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias repository
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

&if defined(nodeName) = 0 &then
&scoped-define XMLNodeName xml-node-name "repository"
&else
&scoped-define XMLNodeName xml-node-name "{&nodeName}"
&endif


&if defined(serializename) = 0 &then
&scoped-define serializename serialize-name "repository"
&else
&scoped-define serializename serialize-name "{&serializename}"
&endif


define temp-table {&tableAlias} no-undo {&XMLNodeName} {&serializename}
&if defined(noSerializeHidden) &then
  field identifier    as character {&nodeType} 
  field category      as character {&nodeType} 
  field entity        as character {&nodeType} 
  field entityID      as character {&nodeType} 
  field name          as character {&nodeType}             
  field displayName   as character {&nodeType} 
  field folderPath    as character {&nodeType} 
  field fileType      as character {&nodeType} 
  field fileSize      as decimal   {&nodeType} 
  field createdDate   as character {&nodeType} 
  field createdBy     as character {&nodeType} 
  field modifiedDate  as character {&nodeType} 
  field fileCount     as integer   {&nodeType} 
  field isPrivate     as logical   {&nodeType} 
  field content       as character {&nodeType} 
  field link          as character {&nodeType} 
  
  /* descriptions */
  field createdByDesc as character {&nodeType} 
  field fileSizeDesc  as character {&nodeType} 
  field fileTypeDesc  as character {&nodeType} 
&else
  field identifier    as character {&nodeType} serialize-hidden
  field category      as character {&nodeType} serialize-hidden
  field entity        as character {&nodeType} serialize-hidden
  field entityId      as character {&nodeType} serialize-hidden
  field name          as character {&nodeType} serialize-hidden            
  field displayName   as character {&nodeType} column-label "Name"          format "x(256)"
  field folderpath    as character {&nodeType} column-label "Folder Path"   format "x(200)"
  field filetype      as character {&nodeType} serialize-hidden             format "x(12)"
  field fileSize      as decimal   {&nodeType} serialize-hidden             
  field createdDate   as character {&nodeType} column-label "Date Saved"    format "x(50)"
  field createdBy     as character {&nodeType} serialize-hidden
  field modifiedDate  as character {&nodeType} column-label "Date Modified" format "x(100)"
  field fileCount     as integer   {&nodeType} column-label "File Count"    format "ZZZZ9"
  field isPrivate     as logical   {&nodeType} column-label "Private?"
  field content       as character {&nodeType} serialize-hidden
  field link          as character {&nodeType} column-label "Link"          format "x(200)"
  
  /* descriptions */
  field createdByDesc as character {&nodeType} column-label "Created By"    format "x(50)"
  field fileSizeDesc  as character {&nodeType} column-label "Size"          format "x(15)"
  field fileTypeDesc  as character {&nodeType} column-label "Type"          format "x(12)"
&endif  
.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


