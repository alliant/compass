&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : sysprop.i
    Purpose     : General purpose system properties datastructure
    Author(s)   : B.Johnson
    Created     : 3.16.2016
    Modification:
    Date         Name          Description    
    10/11/2018   Rahul         Add new field RowId
    10/16/2018   Rahul         Modified to remove sysrowId field    
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias sysprop
&ENDIF

def temp-table {&tableAlias}
 field appCode as char
 field objAction as char
 field objID as char
 field objProperty as char
 field objValue as char
 field objName as char
 field objDesc as char
 field objRef as char
 field lastModified as datetime
 field modifiedBy as char
 field comments as char
 .

/*
 Sample Usage:
 
 Table		Field			SQL DataType	Sample Data
 ---------------------------------------------------------------
 sysprop	appCode			varchar(20)		CLM
			objAction		varchar(100)	ApproveClaimPayable
			objID			varchar(100)	bjohnson
			objProperty 	varchar(100)    ApprovalLimit
			objValue    	varchar(100)    10000
			objName			varchar(200)	Bryan Johnson
			objDesc			varchar(max)	
			objRef			varchar(200)	
			lastModified	datetime		3/16/2016
			modifiedBy		varchar(20)		bjohnson
			comments		varchar(max)
 */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


