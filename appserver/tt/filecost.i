/*------------------------------------------------------------
   tt/filecost.i
   @created 07.05.23
   @author K.R
   Modifications:
   Name          Date       Note
   ------------- ---------- -----------------------------------------------
   
   --------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias fileCost
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "fileCost"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
  field fileTaskID     as integer {&nodeType}
  field cost           as decimal {&nodeType}

  .
