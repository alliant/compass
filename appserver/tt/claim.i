&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/claim.i
   CLAIM datastructure
   D.Sinclair 2.15.2015
   @Modified
   08/07/19 - RS - Modified to add new fields.
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias claim
&ENDIF

def temp-table {&tableAlias}
 field claimID as int
 field description as char
 
 field stat as char             	/* O)pen, C)losed */
 field type as char             	/* N)otice, C)laim, M)atter */
 field stage as char            	/* E)valuation, A)ctive, D)ormant, R)ecovery, L)itigation, C)omplete */
 field action as char           	/* A)ccepted, D)enied, R)esolved, I)ndemnified, S)ettled */

 field difficulty as int        	/* 1 = High, 2 = Med, 3 = Low */
 field urgency as int           	/* 1 = High, 2 = Med, 3 = Low */

 field effDate as datetime			  /* Effective date of the earliest policy/cpl/commitment associated with claim or user entered */
 field refYear as int
 field yearFirstReport as int

 field periodID as int
 field stateID as char

 field assignedTo as char         /* The internal person assigned to handle this file */

 field agentID as char
 field agentName as char
 field agentStat as char
 field fileNumber as char
 field insuredName as char
 field borrowerName as char
 field hasReinsurance as logical
 field hasDeductible as logical
 field deductibleAmount as deci
 field amountWaived as dec
 field seekingRecovery as char      	  /* Yes, No, or Blank */
 field recoveryNotes as char
 field eoPolicy as char
 field eoCarrier as char
 field eoEffDate as datetime
 field eoRetroDate as datetime
 field eoCoverageAmount as dec
 field agentError as char           	  /* Y)es, N)o, U)nknown, M)ixed */
 field searcherError as char        	  /* Y)es, N)o, U)nknown, M)ixed */
 field altaRisk as char
 field altaResponsibility as char

 field priorPolicyUsed as char			    /* Y)es, N)o, U)nknown */
 field priorPolicyID as char
 field underwritingCompliance as char   /* Y)es, N)o, U)nknown, N(A) */
 field sellerBreach as char             /* Y)es, N)o, N(A) */
 field priorPolicyCarrier as char
 field priorPolicyAmount as deci
 field priorPolicyEffDate as datetime

 field summary as char

 field dateClmReceived as datetime
 field dateTransferred as datetime
 field dateCreated as datetime
 field userCreated as char
 field dateAssigned as datetime
 field userDateAssigned as datetime
 field userUserAssigned as character
 field userAssigned as char
 field dateAcknowledged as datetime
 field userAcknowledged as char
 field dateClosed as datetime
 field userClosed as char
 field dateRecReceived as datetime		  /* Date reconsideration received */
 field dateReOpened as datetime
 field userReOpened as char
 field dateResponded as datetime
 field userResponded as char
 field dateReClosed as datetime
 field userReClosed as char
 field lastActivity as datetime

 /* The following fields are not in the db, but populated/calculated as needed. */
 
 field litigation as log  /* True if the 'Litigation' claim attribute (claimattr table) is set */
 field significant as log /* True if the 'Significant' claim attribute (claimattr table) is set */
 field statDesc as char
 field typeDesc as char
 field stageDesc as char
 field actionDesc as char
 field difficultyDesc as char
 field urgencyDesc as char
 field assignedToName as char
 field displayPropAddr as char
 field displayAgentName as char
 
 field suppClaimID as int             	/* Supplemental claim ID (if present) */

 field liabilityAmount as dec
 field formEffDate as datetime			/* Effective date of the primary form (policy/cpl/commitment) */

 /* These capture the "current balance" of the reserves.  These should
    always equal the sum of claimsreserveadj for the following refCategory:
        (L)oss = lossReserve
        LA(E) = laeReserve
  */
 field lossReserve as deci         
 field laeReserve as deci          
 field incidentalsReserve as deci  

 /* These capture the "totals" of the 4 buckets of cost.  These should
    always equal the sum of apvoucher for the following refCategory:
        (L)oss = lossPaid
        LA(E) = laePaid
        (I)ncidental = incidentalsPaid
        (U)nallocated = unallocatedPaid
  */
 field lossPaid as deci         
 field laePaid as deci          
 field incidentalsPaid as deci  
 field unallocatedPaid as deci  
 field attorneyError as character
 field searcher as character
 field userInitResponded as character
 field dateInitResponded as datetime
 

 index claimID is primary unique claimID
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


