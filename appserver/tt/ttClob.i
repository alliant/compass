/* tt/ttClob.i
   Created Vignesh Rajan 7.28.2022
 */
 
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias ttClob
&endif

&if defined(nodeName) = 0 &then
&scoped-define nodeName xml-node-name "ttClob"
&else
&scoped-define nodeName xml-node-name "{&nodeName}"
&endif

define temp-table {&tableAlias} no-undo {&nodeName}
    field fileName as char
    field lcValue  as clob.
