&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/apclm.i
   Temporary data structure for Claims A/P transactions
   This table combines the fields of 'apinv' and 'aptrx'
   This table is not in the database
   B.Johnson 5.14.2015
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias apclm
&ENDIF

def temp-table {&tableAlias}
 field seq as int       /* Temporary sequence for sorting */
 field aptrxID as int

 field transType as char    /* A)pproved, V)oided */
 field transAmount as deci  /* Amount approved and recorded in accounting */
 field transDate as datetime

 field apinvID as int       /* The parent apinv */
 field stat as char  		/* O)pen, P)aid, D)enied */
 
 field refType as char
 field refID as char
 field refSeq as int
 field refCategory as char

 field approval as character
 field approvalDate as datetime
 field notes as char

 field vendorID as char
 field vendorName as char
 field invoiceNumber as char
 field PONumber as char
 field invoiceDate as datetime
 field dateReceived as datetime
 field amount as deci
 field dueDate as datetime
 field report as logical
 field reduceLiability as logical
 
 field hasDocument as logical
 field documentFilename as character
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


