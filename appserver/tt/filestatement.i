/*------------------------------------------------------------------------
File        : tt/filestatement.i
Purpose     :

Syntax      :

Description :

Author(s)   : Shefali
Created     : 06-04-2024
Notes       :

Modification:
Date          Name      Description

----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias filestatement
&endif
   
define temp-table {&tableAlias}
  field agentFileID      as character
  field fileNumber       as character
  field arAmount         as decimal
  field periodCount      as integer
  field tranDate         as datetime
  field totalInvAmt      as decimal
  field invoiceDate      as date
  field agentID          as character
  field fileID           as character
  field sourceID         as character
  field sourceType       as character
  field type             as character
  field paymentAmt       as decimal
  field receviedDate     as datetime
  field checkNumber      as character
  field checkAmt         as decimal
  field refundAmt        as decimal
  field remainingAmt     as decimal
  field category         as character
  field propertyAddress  as character
  field notes            as character
  .
