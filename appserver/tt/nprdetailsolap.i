/*------------------------------------------------------------------------
File        : tt/nprdetailsolap.i
Purpose     :

Syntax      :

Description : temp-table for nprdetailsolap
                                           
Author(s)   : sagar K
Created     : 18-04-2023
Notes       :
----------------------------------------------------------------------*/
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias nprdetailsolap
&endif
&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "nprdetailsolap"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif
  
define temp-table {&tableAlias} no-undo {&serializeName}
    field AgentID               as character    {&nodeType} serialize-hidden 
    field name                  as character    {&nodeType} serialize-hidden 
	field Stat                  as character    {&nodeType} serialize-hidden
	field StateID               as character    {&nodeType} serialize-name  "State"
	field Manager               as character    {&nodeType}
    field Netpremium0monthprev  as decimal      {&nodeType} serialize-name "Netpremium"
	field Netpremium1monthprev  as decimal      {&nodeType}
	field Netpremium2monthprev  as decimal      {&nodeType}
	field NetPlan               as decimal      {&nodeType} serialize-hidden 
    field NetpremiumdiffNetPlan as decimal      {&nodeType} serialize-hidden
	field NetpremiumYTD         as decimal      {&nodeType} serialize-name "YTD Actual"
    field NetPlanYTD            as decimal      {&nodeType} serialize-name "YTD Plan"
    field NetpremiumDiffplanYTD as decimal      {&nodeType} serialize-name "YTD Actual to Plan"	
    field NetPremium12MonthsLag as decimal      {&nodeType} 
	field isFavorite            as character    {&nodeType}
    
    field stateName     as character {&nodeType} column-label "State"           format "x(50)"
    field managerDesc   as character {&nodeType} column-label "Primary Manager" format "x(30)"
    field statDesc      as character {&nodeType} column-label "Status"          format "x(20)"
    
    
    .
  
