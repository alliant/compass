/*------------------------------------------------------------------------
@file tt/partyclause.i
Datastructure for a partyclause
@created 07.11.2022
@author Sachin Chaturvedi
Modifications:
Date          Name           Description
   
---------------------------------------------------------------------- */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias partyclause
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "partyclause"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo  {&serializeName}
  field objID        as  character  {&nodeType}
  field objValue     as  character  {&nodeType}
  
  .

