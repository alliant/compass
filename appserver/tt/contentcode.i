/*------------------------------------------------------------------------
    File        : contentcode.i
    Purpose     : contentcode datastructure
    Author(s)   : Shefali
    Created     : 7.19.2022
    Modification:
    Date          Name      Description
              
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias contentCode
&ENDIF


&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ENDIF

&IF DEFINED(serializeName) = 0 &THEN
&SCOPED-DEFINE serializeName serialize-name "contentcode"
&ELSE
&SCOPED-DEFINE serializeName serialize-name "{&serializeName}"
&ENDIF

def temp-table {&tableAlias} NO-UNDO {&serializeName}
field contentCodeID   as integer   {&nodeType}
field contentCode     as character {&nodeType}
field stateID         as character {&nodeType}
field type            as character {&nodeType}
field content         as character {&nodeType}
field typeDescription as character {&nodeType} /* non DB field */
field description     as character {&nodeType}
field prefix          as logical   {&nodeType}
.

