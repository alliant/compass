&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* 
@name tt/listfilter.i
@description Temp table for the LIST builder to store the FILTER
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias listfilter
&ENDIF

define temp-table {&tableAlias}
  field tableName as character
  field columnName as character
  field columnOperator as character
  field columnValue as character
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

