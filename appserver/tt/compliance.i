&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/compliance.i
Purpose     :

Syntax      :

Description :

Author(s)   : Rahul Sharma
Created     : 06-11-2018
Notes       :

Modification:
Date          Name      Description
06/21/2018    Rahul     Added new field currentstat
07/25/2018    Rahul     Added new field currentdate 
08/16/2018    Rahul     Added new field stateID
09/24/2018    Rahul     Added new field role
08/28/2020    VJ        Added new fiel related to role
----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias compliance
&ENDIF
   
def temp-table {&tableAlias}
  field stateID        as character
  field refType        as character  /* agent, person, attorney, Company */
  field refID          as character  /* agentID, personID, attorneyID, 0 */
  field name           as character
  field role           as character
  field logdate        as datetime
  field currentdate    as datetime
  field comstat        as character
  field currentstat    as character
  field roleName       as character
  field roleID         as character
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


