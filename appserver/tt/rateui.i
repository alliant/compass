&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
define temp-table developerComments
 field regionComments            as character
 field propertyTypeComments      as character
 field loanRateTyperComments     as character
 field ownerRateTypeComments     as character
 field scndLoanRateTypeComments  as character
 field endorsementComments       as character.

define temp-table region
 field regionCode      as character
 field description     as character.

define temp-table proposedRateConfig
 field showProposedRateFlag as logical.

define temp-table propertyType
 field propertyTypecode    as character
 field description         as character
 field regionList          as character
 .

define temp-table loanRateType
 field propertyTypeCode        as character
 field loanRateTypeCode        as character
 field description             as character
 field simo                    as logical
 field priorPolicyAmountFlag   as logical
 field priorPolicyDateFlag     as logical
 field regionList              as character
 .

define temp-table ownerRateType
 field propertyTypecode      as character
 field ownerRateTypeCode     as character
 field description           as character
 field simo                  as logical
 field priorPolicyAmountFlag as logical
 field priorPolicyDateFlag   as logical
 field regionList            as character
 .

define temp-table scndLoanRateType
 field propertyTypecode       as character
 field scndLoanRateTypeCode   as character
 field description            as character
 field simo                   as logical
 field priorPolicyAmountFlag  as logical
 field priorPolicyDateFlag    as logical
 field regionList             as character
 .

define temp-table endorsement
 field endorsementCode          as character
 field description              as character
 field endorsementType          as character
 field proposedRate             as decimal
 field propertyTypeList         as character
 field rateTypeList             as character
 field regionList               as character 
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


