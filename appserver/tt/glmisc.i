/*------------------------------------------------------------------------
File        : tt/glmisc.i
Purpose     :

Syntax      :

Description :

Author(s)   : Sachin Chaturvedi
Created     : 01-14-2021
Notes       :

Modification:
Date          Name      Description

----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias glmisc
&endif
   
define temp-table {&tableAlias}
  field agentid       as character
  field type          as character
  field reference     as character
  field tranID        as character
  field postingdate   as datetime
  field argldef       as character
  field arglref       as character
  field debit         as decimal
  field credit        as decimal
  field description   as character
  field voided        as character
  field notes         as character
  .

