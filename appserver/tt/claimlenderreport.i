&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/claimlenderreport.i
   datastructure for a CLAIM Wells Fargo STATUS REPORT report
   Created John Oliver
   Date 03.01.2017
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias claimlenderreport
&ENDIF

/* Should recoupments be against the Loss or LAE in accounting terms?
 */

def temp-table {&tableAlias}
  field claimID as integer label "File ID"
  field serviceName as character label "Servicer Name"
  field loanNumber as character label "Loan Number"
  field statusToLender as character label "Status To Insured Lender"
  field acceptedAction as character label "Accepted Action"
  field denialReason as character label "Denial Reason"
  field dateCreated as datetime label "File Open Date"
  field description as character label "Claim Issue"
  field assignedTo as character label "Claim Administrator"
  field titleCounsel as character label "Title Counsel"
  field address as character label "Property Address"
  field city as character label "City of Property"
  field state as character label "State of Property"
  field zipcode as character label "Zip Code of Property"
  field summary as character label "Summary Status"
  field repurchase as character label "Repurchase"
  field reconsideration as character label "Reconsideration"
  field litigation as character label "Active Litigation"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


