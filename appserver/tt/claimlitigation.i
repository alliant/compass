&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/claimlitigation.i
Purpose     : datastructure for CLAIMs management
Author(s)   : D.Sinclair
Created     : 4.23.2015
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias claimlitigation
&ENDIF

def temp-table {&tableAlias}
 field claimID as int
 field litigationID as int
 field stat as char             /* O)pen, C)losed */

 field caseNumber as char
 field dateFiled as datetime
 field courtName as char
 field addr1 as char
 field addr2 as char
 field city as char
 field stateID as char
 field zipcode as char
 field judge as char
 field dateServed as datetime
 field isParty as logical
 field plaintiff as char
 field defendant as char
 field notes as char
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


