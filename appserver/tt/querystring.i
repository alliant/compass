/*------------------------------------------------------------------------
File        : tt/topicexpense.i
Purpose     :

Syntax      :

Description : Store querystring parameter key and value and its type (S:system and A:action)
                                           
Author(s)   : K.R
Created     : 08-25-2023
Notes       :
----------------------------------------------------------------------*/
&IF DEFINED(tableAlias) = 0 &THEN
&SCOPED-DEFINE tableAlias querystring
&ENDIF

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "querystring"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif
  
define temp-table {&tableAlias} no-undo {&serializeName}
  field paramName	       as  character {&nodeType}    
  field paramValue         as  character {&nodeType}
  field paramType          as  character {&nodeType}
  .

