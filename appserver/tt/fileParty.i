/* tt/fileParty.i
   Datastructure for an fileParty
   @created 05.31.2022
   @author Vignesh Rajan
   Modifications:
   Date          Name           Description
   02/13/2024    SR            Added policyID field in updateFile scenario. 
 */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias fileparty
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

&IF DEFINED(serializeName) = 0 &THEN
&SCOPED-DEFINE serializeName serialize-name "fileParty"
&ELSE
&SCOPED-DEFINE serializeName serialize-name "{&serializeName}"
&ENDIF

define temp-table {&tableAlias} no-undo {&serializeName}
&if defined(getAgentFile) &then
  field agentFileID      as integer   {&nodeType}
  field entityType       as character {&nodeType}
  field entityTypeCode   as character {&nodeType}
  field seq              as integer   {&nodeType}
  field name             as character {&nodeType}
  field firstName        as character {&nodeType}
  field middleName       as character {&nodeType}
  field lastName         as character {&nodeType}
  field joinBy           as character {&nodeType}
  field clause           as character {&nodeType}
  field spouse           as logical   {&nodeType}
  field policyID         as integer   {&nodeType}
  field liabilityAmount  as decimal   {&nodeType}
  field addr1            as character {&nodeType}
  field addr2            as character {&nodeType}
  field addr3            as character {&nodeType}
  field addr4            as character {&nodeType}
  field city             as character {&nodeType} serialize-name 'addrCity'
  field state            as character {&nodeType} serialize-name 'addrState'
  field zip              as character {&nodeType} serialize-name 'addrZip'
  field reference        as character {&nodeType}
  field partyType        as character {&nodeType} serialize-hidden /* Used for content replacement */
&elseif defined(updateFile) &then
  field agentFileID      as integer   {&nodeType}
  field entityType       as character {&nodeType}
  field seq              as integer   {&nodeType}
  field name             as character {&nodeType}
  field firstName        as character {&nodeType}
  field middleName       as character {&nodeType}
  field lastName         as character {&nodeType}
  field joinBy           as character {&nodeType}
  field clause           as character {&nodeType}
  field spouse           as logical   {&nodeType}
  field liabilityAmount  as decimal   {&nodeType}
  field addr1            as character {&nodeType}
  field addr2            as character {&nodeType}
  field addr3            as character {&nodeType}
  field addr4            as character {&nodeType}
  field city             as character {&nodeType} serialize-name 'addrCity'  
  field state            as character {&nodeType} serialize-name 'addrState' 
  field zip              as character {&nodeType} serialize-name 'addrZip'   
  field reference        as character {&nodeType}
  field policyID         as integer   {&nodeType}
&else
  field agentFileID      as integer   {&nodeType}
  field entityType       as character {&nodeType}
  field partyType        as character {&nodeType}
  field seq              as integer   {&nodeType}
  field name             as character {&nodeType}
  field firstName        as character {&nodeType}
  field middleName       as character {&nodeType}
  field lastName         as character {&nodeType}
  field joinBy           as character {&nodeType}
  field clause           as character {&nodeType}
  field spouse           as logical   {&nodeType}
  field policyID         as integer   {&nodeType}
  field liabilityAmount  as decimal   {&nodeType}
  field addr1            as character {&nodeType}
  field addr2            as character {&nodeType}
  field addr3            as character {&nodeType}
  field addr4            as character {&nodeType}
  field city             as character {&nodeType}
  field state            as character {&nodeType}
  field zip              as character {&nodeType}
  field reference        as character {&nodeType}
  field CreatedDate      as DATETIME  {&nodeType}
  field CreatedBy        as character {&nodeType}
  field lastModifiedDate as DATETIME  {&nodeType}
  field lastModifiedBy   as character {&nodeType}  
&endif
.
 

