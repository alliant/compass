/* tt/queryTasks.i
   @created 12.24.2024
   @author SRK
   Modifications:
   Date          Name           Description
 */
 
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias queryTasks
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "queryTasks"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
    field fileTaskID     as integer      {&nodeType}
    field ID             as integer      {&nodeType} 
    field topic          as character    {&nodeType} serialize-name "Task"
	field AgentFileId    as integer      {&nodeType} 
	field Agent          as character    {&nodeType} 
    field FileNumber     as character    {&nodeType} serialize-name "FileNumber"
	field AssignedTo     as character    {&nodeType} serialize-name "AssignedTo"
    field AssignedToName as character    {&nodeType} serialize-name "AssignedToName"
    field AssignedDate   as datetime     {&nodeType}
    field Category       as character    {&nodeType} serialize-name "Queue"	
    field TaskStatus     as character    {&nodeType} serialize-name "Status"	
	field ImportantNote  as character    {&nodeType} 
    field stateID        as character    {&nodeType}
    field closingDate    as datetime     {&nodeType}
    field ElapsedHrs     as integer      {&nodeType}
    field ElapsedTime    as character    {&nodeType}
    field county         as character    {&nodeType}
    field stateDesc      as character    {&nodeType}
    
    .                                      







