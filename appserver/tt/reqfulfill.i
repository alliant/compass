&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/reqfulfill.i
Purpose     :

Syntax      :

Description : temp-table for requirement fulfillment
                join of tables 
                  staterequirement
                  qualification
                  person
                based on linking temptables
                  qualreq
                  statereqQual
                                           
Author(s)   : Rahul Sharma
Created     : 07-04-2018
Notes       :

Modification:
Date          Name           Description
07-16-2018  Rahul Sharma   Added field entityNPN & qualificationNumber  
07-27-2018  Rahul Sharma   Added field tempqual & tempappliesTo
08-02-2018  Rahul Sharma   Added field expire, expireMonth, expireDay & expiryDays                                               
08-07-2018  Rahul Sharma   Added field expireYear
01-10-2019  Rahul Sharma   Modified to change the field name
09-10-2019  Gurvindar      Added field orgRoleId 
----------------------------------------------------------------------*/


&if defined(tablealias) = 0 &then
&scoped-define tableAlias reqfulfill
&endif
   
define temp-table {&tableAlias}
/*-------Entity Info-----------------*/
  field entity              as character
  field entityID            as character
  field entityNPN           as character
  field StateId             as character
  field entityname          as character /* entity that fulfill the requirement*/
  
/*--------IDs of temptables----------*/
  field requirementID       as integer
  field qualificationID     as integer
  field statereqqualID      as integer
  field personRoleID        as integer
  field qualreqID           as integer
  field qualEntityId        as character
  field orgRoleId           as integer  /*for client use only*/
  
/*------------staterequirement-------*/   
  field url                 as character
  field expire              as character
  field expireMonth         as integer
  field expireDay           as integer
  field expireYear          as integer
  field expireDays          as integer
  field irefID              as character
  field authorizedBy        as character
  field reqdesc             as character  
  field requirementQual     as character
  field appliesTo           as character  
  
/*------------qualification---------*/       
  field qualificationNumber as character
  field stat                as character
  field notes               as character
  field reqMet              as logical
  field effectiveDate       as datetime
  field expirationDate      as datetime 
  field activ               as logical   
  field qualification       as character     
          
  field role                as character
  field comStat             as character
  field description         as character.
.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


