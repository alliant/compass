
/* tt/renderfiledocument.i
   Datastructure for an render file
   @created 09.05.2024
   @author 
   Modifications:
   Date          Name           Description
   09/05/2024    S Chandu      Modifed  datatype character 'content' to clob fields.
   10/29/2024    SRK           Modified to add stateName
   
 */
 
 
define temp-table ttAgentFile no-undo serialize-name 'order'
  field agentFileID                  as integer
  field agentID                      as character
  field agentName                    as character
  field fileNumber                   as character
  field orderId                      as integer
  field productDescription           as character
  field orderDueDate                 as character   
  field orderStatus                  as character
  field commitmentNumber             as integer
  field typeOfEstate                 as character
  field estateType                   as character
  field buyerDescription             as character serialize-name 'buyersDescription'
  field buyerDescriptionUnformatted  as character serialize-name 'buyersDescriptionUnformatted'
  field buyersCoverage               as decimal
  field buyersPolicyID               as integer
  field sellerDescription            as character serialize-name 'sellersDescription'
  field sellerDescriptionUnformatted as character serialize-name 'sellersDescriptionUnformatted'
  field legalDescription             as clob
  field legalDescriptionUnformatted  as clob
  field effectiveDate                as date
  field effectiveTime                as character
  field officeID                     as integer serialize-hidden
  field templateID                   as integer serialize-hidden
  field orderedBy                    as character
  field orderedEmail                 as character
  field orderedPhone                 as character
  field orderedDate                  as date
  field searchOrdered                as logical
  field policyOrdered                as logical
  .  

define temp-table ttUserRender no-undo serialize-name 'searcherContact'
  field orderId as integer serialize-hidden
  field name    as character 
  field email   as character
  .                       

define temp-table ttoffice no-undo serialize-name 'issuingAgent' /* K.R issuingAgent missing*/
  field officeID as integer   serialize-hidden
  field agentID  as character serialize-hidden 
  field name     as character
  field address  as character
  field contact  as character
  field email    as character
  .

define temp-table ttBuyer no-undo serialize-name 'buyers'
  field agentFileID as integer   serialize-hidden
  field name        as character
  field address     as character
  field joinBy      as character serialize-name 'join'
  field type        as character
  field policyID    as integer
  .

define temp-table ttSeller no-undo serialize-name 'sellers'
  field agentFileID as integer   serialize-hidden
  field name        as character
  field address     as character
  field joinBy      as character serialize-name 'join'
  field type        as character
  .

define temp-table ttLender no-undo serialize-name 'lenders' /* K.R Lenders header missing*/
  field agentFileID as integer   serialize-hidden
  field name        as character
  field address     as character
  field clause      as character
  field coverage    as integer
  field policyID    as integer
  .

define temp-table ttFileProperty no-undo serialize-name 'properties'
  field agentFileID          as integer   serialize-hidden
  field parcelID             as character
  field address              as character
  field county               as character
  field stateName            as character
  field section              as character
  field township             as character
  field townshipDirection    as character
  field range                as character
  field rangeDirection       as character
  field condominum           as character
  field subdivision          as character
  field unitno               as character
  field instrumentNumber     as character
  field lot                  as character
  field block                as character
  field book                 as character
  field pageNumber           as character
  field levels               as character
  field platDate             as datetime
  field latitude             as decimal
  field longitude            as decimal
  field deedGrantor          as character
  field deedGrantee          as character
  field deedDate             as datetime
  field deedRecordedDate     as datetime
  field deedBook             as character
  field deedPage             as character
  field shortLegal           as clob
  field longLegal            as clob
  field longLegalUnformatted as clob
  field taxYear              as integer
  field taxAmtDue            as decimal serialize-name 'taxDue'
  field taxAmtPaid           as decimal serialize-name 'taxPaid'
  .

define temp-table ttFileContentRequirement no-undo serialize-name 'requirements' /* K.R requirments missing*/
  field agentFileID        as integer   serialize-hidden
  field content            as clob      serialize-name 'text'
  field unformattedcontent as clob      serialize-name 'unformattedText'
  field isNote             as logical
  field indent             as integer
  .

define temp-table ttFileContentException no-undo serialize-name 'exceptions'
  field agentFileID        as integer   serialize-hidden
  field content            as clob      serialize-name 'text'           
  field unformattedcontent as clob      serialize-name 'unformattedText'
  field isNote             as logical
  field indent             as integer
  .

define temp-table ttFileContentInstrument no-undo serialize-name 'instruments'
  field agentFileID        as integer   serialize-hidden
  field content            as clob      serialize-name 'text'           
  field unformattedcontent as clob      serialize-name 'unformattedText'
  field isNote             as logical
  field indent             as integer
  .

define temp-table ttTemplate no-undo serialize-name 'templatecontent'
  field templateID   as integer serialize-hidden
  field agentFileID  as integer serialize-hidden
  field name         as character
  .

define temp-table ttTemplateContentRequirement no-undo serialize-name 'requirements'
  field templateID         as integer   serialize-hidden
  field content            as clob
  field unformattedContent as clob
  field isNote             as logical
  field indent             as integer
  .

define temp-table ttTemplateContentException no-undo serialize-name 'exceptions'
  field templateID         as integer   serialize-hidden
  field content            as clob
  field unformattedContent as clob
  field isNote             as logical
  field indent             as integer
  .

define temp-table ttTemplateContentInstrument no-undo serialize-name 'instruments'
  field templateID         as integer   serialize-hidden
  field content            as clob
  field unformattedContent as clob
  field isNote             as logical
  field indent             as integer
  .

define temp-table ttPolicyException no-undo serialize-name 'PolicyException'
  field content            as clob serialize-name "text"
  field isNote             as logical
  field indent             as integer
  field seq                as integer
.

{tt/policycontent.i &tableAlias=ttPolicyContent &serializeName="PolicyContent" &includeextra=yes}
