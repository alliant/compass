/* tt/training.i
   Datastructure for an training
   @created 11.22.2024
   @author S Chandu
   Modifications:
   Date          Name           Description
   
 */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias training
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "training"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo  {&serializeName}
  field trainingID         as  integer    {&nodeType}
  field courseID           as  integer    {&nodeType}
  field trainingUrl        as  character  {&nodeType}
  field trainingQR         as  clob       {&nodeType}
  field stat               as  character  {&nodeType}
  field StatDescription    as  character  {&nodeType}
  field agentID            as  character  {&nodeType}
  field timeStart          as  datetime   {&nodeType}
  field timeEnd            as  datetime   {&nodeType}
  field timezone           as  character  {&nodeType}
  field officeID           as  integer    {&nodeType}
  field addr1              as  character  {&nodeType}
  field addr2              as  character  {&nodeType}
  field addr3              as  character  {&nodeType}
  field addr4              as  character  {&nodeType}
  field city               as  character  {&nodeType}
  field state              as  character  {&nodeType}
  field zip                as  character  {&nodeType}
  field instructorEmail    as  character  {&nodeType}
  field instructorName     as  character  {&nodeType}
  field instructorReminder as  integer    {&nodeType}
  field contactEmail       as  character  {&nodeType}
  field contactName        as  character  {&nodeType}
  field contactPhone       as  character  {&nodeType}
  field contactReminder    as  integer    {&nodeType}
  field collectPayment     as  logical    {&nodeType}
  field paymentUrl         as  character  {&nodeType}
  field notes              as  character  {&nodeType}
  field requestorEmail     as  character  {&nodeType}
  field requestorName      as  character  {&nodeType}
  .
