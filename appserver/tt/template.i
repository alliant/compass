/*------------------------------------------------------------------------
    File        : template.i
    Purpose     : template datastructure
    Author(s)   : Shweta Dhar
    Created     : 7.08.2022
    Modification:
    Date          Name      Description
    02/08/2024    SR        Removed productID field.       
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias ttTemplate
&ENDIF


&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ELSE
&SCOPED-DEFINE nodeType XML-NODE-TYPE "{&nodeType}"
&ENDIF


&IF DEFINED(serializeName) = 0 &THEN
&SCOPED-DEFINE serializeName serialize-name "template"
&ELSE
&SCOPED-DEFINE serializeName serialize-name "{&serializeName}"
&ENDIF

def temp-table {&tableAlias} no-undo {&serializeName}
field templateID     as integer   {&nodeType}
field stateID        as character {&nodeType}
field agentID        as character {&nodeType}
field documenttype   as character {&nodeType}
field name           as character {&nodeType}
field active         as logical   {&nodeType}
field fileurl        as character {&nodeType}
field filename       as character {&nodeType}
field schemaurl      as character {&nodeType}
field schemaname     as character {&nodeType}
field outputFormat   as character {&nodeType}
.

 
