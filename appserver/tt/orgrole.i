&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/orgrole.i
Purpose     :

Syntax      :

Description :

Author(s)   : Rahul Sharma
Created     : 09-04-2019
Notes       :

Modification:
Date          Name      Description
20/08/2020    VJ        Added new field active
----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias orgrole
&endif
   
def temp-table {&tableAlias}
  field orgRoleID        as integer
  field orgID            as character
  field stateID          as character
  field source           as character
  field sourceID         as character
  field effDate          as datetime
  field expDate          as datetime 
  field active           as logical 
  
  field comstat          as character    /* used for client only */
  field name             as character    /* used for client only */  
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


