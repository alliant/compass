&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@name reviewclaim.i
@description Temp table to hold the REVIEW CLAIMs
@author John Oliver
@created 10/05/2017
------------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias reviewclaim
&ENDIF

define temp-table {&tableAlias}
  field agentID as character
  field claimID as integer column-label "Claim ID" format "99999999"
  field agentError as character column-label "Agent Error" format "x(20)"
  field stat as character column-label "Status" format "x(20)"
  field description as character column-label "Description" format "x(1000)"
  field laePaid as decimal column-label "LAE Paid" format "(>>,>>>,>>>,>>Z)"
  field laeReserve as decimal
  field lossPaid as decimal column-label "Loss Paid" format "(>>,>>>,>>>,>>Z)"
  field lossReserve as decimal
  field recoveries as decimal
  field netPremium as decimal
  
  /* for the client */
  field agentErrorDesc as character column-label "Agent Error" format "x(20)"
  field statDesc as character column-label "Status" format "x(20)"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


