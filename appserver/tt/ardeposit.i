&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/ardeposit.i
Purpose     : used to fetch/store AR deposits

Syntax      :

Description : Used specifically for AR app 

Author(s)   : Anjly
Created     : 01-21-2020
Notes       : 

Modification:
Date          Name           Description
07/22/2020    AG              Added new field "posted"
----------------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias arDeposit
&endif
   

def temp-table {&tableAlias}
  field depositID    as integer   
  field bankID       as character 
  field depositType  as character
  field depositDate  as datetime
  field stat         as character
  field depositRef   as character
  field amount       as decimal
  field posted       as logical
  field ledgerID     as integer
  field voidLedgerID as integer
  field void         as logical
  field voidedBy     as character 
  field voidDate     as datetime
  field postedBy     as character 
  field postDate     as datetime
  field archived     as logical
  field transDate    as datetime
  field pmtamount    as decimal /* Client side */
  field createdBy    as character 
  field createDate   as datetime
  field username     as character /* Client side */
  field voidByname   as character /* Client side */
  field postByname   as character /* client side */  
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


