&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        :tt/attorney.i
Purpose     :

Syntax      :

Description :

Author(s)   : Sachin Chautrvedi
Created     : 07.24.2017
Notes       :

Modification:
Date          Name      Description
03/15/2018    Rahul     Added field personID
06/18/2018    Rahul     Added field comStat
04/29/2019    Rahul     Added "matchPercentage" and "Seq" field.
12/06/21      Shefali   Task# 86699 Added "orgID" and "orgRoleID" field.
----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias attorney
&endif

&if defined(nodetype) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif

def temp-table {&tableAlias}
 field attorneyID      as character {&nodeType}
 field orgID           as character {&nodeType}
 field personID        as character {&nodeType}
 field altauID         as character {&nodeType}
 field NPN             as character {&nodeType}
 field name1           as character {&nodeType}
 field firmName        as character {&nodeType} 
 field addr1           as character {&nodeType}
 field addr2           as character {&nodeType}
 field city            as character {&nodeType}
 field state           as character {&nodeType}
 field zip             as character {&nodeType}
 field phone           as character {&nodeType}
 field fax             as character {&nodeType}
 field email           as character {&nodeType}
 field contact         as character {&nodeType}
 field contractDate    as datetime  {&nodeType}
 field contractID      as character {&nodeType}
 field createDate      as datetime  {&nodeType}
 field createdBy       as character {&nodeType}
 field activeDate      as datetime  {&nodeType}
 field cancelDate      as datetime  {&nodeType}
 field closedDate      as datetime  {&nodeType}
 field prospectDate    as datetime  {&nodeType}
 field withdrawnDate   as datetime  {&nodeType}
 field submissionDate  as datetime  {&nodeType}
 field comments        as character {&nodeType}
 field website         as character {&nodeType}
 field stat            as character {&nodeType}
 field stateID         as character {&nodeType}
 field comStat         as character {&nodeType} 
 field matchPercentage as decimal   {&nodeType} /* Only client side */
 field seq             as integer   {&nodeType} /* Only client side */
 field ComSeq          as character {&nodeType} /* Only client side */
 field address         as character {&nodeType} /* Only client side */
 field personRoleID    as integer   {&nodeType} /* Only client side */
 field orgRoleID       as integer   {&nodeType} /* Only client side */
 index attorneyid is primary unique
  attorneyID
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


