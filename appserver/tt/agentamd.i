&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/agent.i
   AGENT Customer Relationship Manager datastructure
   Author(s) J.Oliver
   Created 3.3.2017
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias agentamd
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "element"
&ENDIF

define temp-table {&tableAlias}
  field agentID as character {&nodeType} label "Agent ID"
  field stateID as character {&nodeType} label "State"
  field stat as character {&nodeType}
  field name as character {&nodeType} label "Agent" format "x(200)"
  field manager as character {&nodeType}
  field secondManager as character {&nodeType}
  field hasDocument as logical column-label "Has!Doc" {&nodeType}
  field lastNoteDate as datetime {&nodeType} label "Last Note" format "99/99/99"
  field qarDate as datetime {&nodeType}
  field qarScore as integer {&nodeType}
  field errScore as integer {&nodeType}
  field monthNP_1 as decimal {&nodeType}
  field monthGP_1 as decimal {&nodeType}
  field monthRP_1 as decimal {&nodeType}
  field monthBC_1 as decimal {&nodeType}
  field monthFC_1 as decimal {&nodeType}
  field monthPC_1 as decimal {&nodeType}
  field monthRG_1 as decimal {&nodeType}
  field monthNP_2 as decimal {&nodeType}
  field monthGP_2 as decimal {&nodeType}
  field monthRP_2 as decimal {&nodeType}
  field monthBC_2 as decimal {&nodeType}
  field monthFC_2 as decimal {&nodeType}
  field monthPC_2 as decimal {&nodeType}
  field monthRG_2 as decimal {&nodeType}
  field monthNP_3 as decimal {&nodeType}
  field monthGP_3 as decimal {&nodeType}
  field monthRP_3 as decimal {&nodeType}
  field monthBC_3 as decimal {&nodeType}
  field monthFC_3 as decimal {&nodeType}
  field monthPC_3 as decimal {&nodeType}
  field monthRG_3 as decimal {&nodeType}
  field alertWarn as character {&nodeType}
  field alertCrit as character {&nodeType}
  
  /* for the client only */
  field month1Data as decimal {&nodeType} column-label "3" format "(ZZ,ZZZ,ZZZ,ZZZ)"
  field month2Data as decimal {&nodeType} column-label "2" format "(ZZ,ZZZ,ZZZ,ZZZ)"
  field month3Data as decimal {&nodeType} column-label "1" format "(ZZ,ZZZ,ZZZ,ZZZ)"
  FIELD alertWarnCnt AS INT {&nodeType} LABEL "Y" FORMAT "ZZ"
  FIELD alertCritCnt AS INT {&nodeType} LABEL "R" FORMAT "ZZ"
  FIELD alertNoneCnt AS INT {&nodeType} LABEL "G" FORMAT "ZZ"
  field alertShow as character {&nodeType}
  FIELD alertScore AS INT {&nodeType} /* for sorting */
  
  field managerDesc as character {&nodeType} column-label "Primary Manager" format "x(30)"
  field statDesc as character {&nodeType} label "Status" format "x(20)"

  index agentid is primary unique agentID
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


