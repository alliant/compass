&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------------------------
    File        : sysuser.i
    Purpose     : SYStem USER datastructure
    Author(s)   : D.Sinclair
    Created     : 1.24.2012
    Modification:
    Date            Name          Description
    10/03/2018    Rahul Sharma    Modified to add passwordSetDate and passwordExpired fields            
    10/11/2018    Rahul Sharma    Modified to add pwdExpireDays	
    03/26/2019    Rahul Sharma    Added new field "deptID"
    29/07/2022    Shweta Dhar     Task#95509: Modified to support clob related changes
    04/05/2023    Spandana        Modified to rearrange fields
	06/06/2023    SB              Modified to add new field vendorID
  ------------------------------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias sysuser
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ENDIF

&IF DEFINED(nodeName) = 0 &THEN
&SCOPED-DEFINE nodeName XML-NODE-NAME "sysuser"
&ELSE
&SCOPED-DEFINE nodeName XML-NODE-NAME "{&nodeName}"
&ENDIF

def temp-table {&tableAlias}

&if defined(getActiveUsers) &then
  field uid as char       {&nodeType}
  field name as char      {&nodeType}
  field email as char     {&nodeType}
&else 
  field uid as char       {&nodeType}
  field name as char      {&nodeType}
  field initials as char  {&nodeType}
  field email as char     {&nodeType}
  field role as char      {&nodeType}
  field password as char  {&nodeType}
  field isActive as logical {&nodeType}
  field createDate as datetime {&nodeType}
  field comments as char       {&nodeType}
  field passwordSetDate as datetime {&nodeType}
  field pwdExpireDays as integer    {&nodeType}
  field passwordExpired as logical  {&nodeType}
  field department as char              {&nodeType}
  field vendorID as char            {&nodeType}
&endif
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


