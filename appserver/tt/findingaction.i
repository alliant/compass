&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : findingaction.i
    Purpose     : Combination of action and finding
    Author(s)   : John Oliver
    Created     : 12.14.2018
    Modification:
    Date          Name      Description
    07-29-22      SR        clob related changes
  ----------------------------------------------------------------------*/
  
&if defined(tableAlias) = 0 &THEN
&scoped-define tableAlias findingaction
&endif

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

def temp-table {&tableAlias} no-undo
  /* common table elements */
  field findingactionID    as character {&nodeType} serialize-hidden
  field entity             as character {&nodeType} column-label "Entity" format "x(50)"
  field entityID           as character {&nodeType} column-label "Entity ID" format "x(50)"
  field refType            as character {&nodeType} serialize-hidden
  field refID              as character {&nodeType} serialize-hidden
  field source             as character {&nodeType} column-label "Source" format "x(50)"
  field sourceID           as character {&nodeType} column-label "Source ID" format "x(50)"
  field question           as clob      {&nodeType} column-label "Question" format "x(200)"
  field finding            as clob      {&nodeType} column-label "Finding" format "x(200)"
  field action             as character {&nodeType} column-label "Action" format "x(200)"
  field ownerID            as character {&nodeType} serialize-hidden
  field ownerDesc          as character {&nodeType} column-label "Owner" format "x(200)"
  field severity           as integer   {&nodeType} serialize-hidden
  field iconSeverity       as integer   {&nodeType} serialize-hidden
  
  /* finding table elements */
  field findingID          as integer   {&nodeType} column-label "Finding!ID" format ">>>>>9"
  field severityDesc       as character {&nodeType} column-label "Severity" format "x(20)"
  field description        as clob      {&nodeType} column-label "Description"
  field stage              as character {&nodeType} serialize-hidden
  field stageDesc          as character {&nodeType} column-label "Stage" format "x(20)"
  field findingUID         as character {&nodeType} serialize-hidden
  field findingUIDDesc     as character {&nodeType} column-label "Finding User" format "x(100)"
  field findingCreatedDate as datetime  {&nodeType} column-label "Created Date" format "99/99/9999"
  field findingClosedDate  as datetime  {&nodeType} column-label "Closed Date" format "99/99/9999"
  
  /* action table elements */
  field actionID           as integer   {&nodeType} column-label "Action!ID" format ">>>>>9"
  field targetProcess      as character {&nodeType} serialize-hidden
  field actionType         as character {&nodeType} serialize-hidden
  field actionTypeDesc     as character {&nodeType} column-label "Action!Type" format "x(20)"
  field actionUID          as character {&nodeType} serialize-hidden
  field actionUIDDesc      as character {&nodeType} column-label "Action User" format "x(100)"
  field stat               as character {&nodeType} serialize-hidden
  field statDesc           as character {&nodeType} column-label "Status" format "x(20)"
  field startDate          as datetime  {&nodeType} column-label "Start Date" format "99/99/9999"
  field completeStatus     as character {&nodeType} serialize-hidden
  field completeStatusDesc as character {&nodeType} column-label "Complete!Status" format "x(20)"
  field method             as character {&nodeType} serialize-hidden
  field actionCreatedDate  as datetime  {&nodeType} column-label "Created Date" format "99/99/9999"
  field openedDate         as datetime  {&nodeType} column-label "Opened Date" format "99/99/9999"
  field completedDate      as datetime  {&nodeType} column-label "Completed!Date" format "99/99/9999"
  field cancellationDate   as datetime  {&nodeType} column-label "Cancellation!Date" format "99/99/9999"
  field notificationDate   as datetime  {&nodeType} column-label "Notification!Date" format "99/99/9999"
  field originalDueDate    as datetime  {&nodeType} column-label "Original!Due Date" format "99/99/9999"
  field dueDate            as datetime  {&nodeType} column-label "Due Date" format "99/99/9999"
  field extendedDate       as datetime  {&nodeType} column-label "Extended!Date" format "99/99/9999"
  field followupDate       as datetime  {&nodeType} column-label "Follow-Up!Date" format "99/99/9999"
  field referenceActionID  as character {&nodeType} serialize-hidden
  field iconMessages       as character {&nodeType} column-label "Alert Message" format "x(500)"
  field lastNote           as character {&nodeType} column-label "Last Note" format "x(500)"
  field hasDoc             as logical   {&nodeType} column-label "Has!Doc"
  
  /* notification report */
  field dueIn              as integer   {&nodeType} column-label "Due In" format ">>>>>9"
  field sendNotifications  as logical   {&nodeType} column-label "Send?"
  
  /* approval report */
  field approvalStat       as character {&nodeType} serialize-hidden
  field approvalStatDesc   as character {&nodeType} column-label "Approval!Status" format "x(20)"
  field approvalDate       as datetime  {&nodeType} column-label "Approval!Date" format "99/99/9999"
  field sentDate           as datetime  {&nodeType} column-label "Sent Date" format "99/99/9999"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


