&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@name qar.i
@description Quality Assurance Review summary datastructure
@author D.Sinclair
@created 1.24.2012

@modified
Date          Name     Comments
29/07/2022     SD      Task#95509: Modified to support clob related changes
------------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias qarnote
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ENDIF

&IF DEFINED(nodeName) = 0 &THEN
&SCOPED-DEFINE nodeName XML-NODE-NAME "qarnote"
&ELSE
&SCOPED-DEFINE nodeName XML-NODE-NAME "{&nodeName}"
&ENDIF

def temp-table {&tableAlias}  {&nodeName}
 field qarID as int           {&nodeType}
 field seq as int             {&nodeType}
 field dateCreated as datetime  {&nodeType}
 field uid as char             {&nodeType}
 field secured as logical      {&nodeType}
 field takenBy as char         {&nodeType}
 field stat as char            {&nodeType}
 field comments as char        {&nodeType}
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


