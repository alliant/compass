&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file sysdest.i
@description Temp Table definition for the sysdest table

@author John Oliver
@created 01.03.2020
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias sysdest
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

define temp-table {&tableAlias} no-undo
  field destID       as integer   {&nodeType} serialize-hidden
  field entityType   as character {&nodeType} serialize-hidden
  field entityID     as character {&nodeType} column-label "ID"           format "x(50)"
  field action       as character {&nodeType} serialize-hidden            
  field destType     as character {&nodeType} serialize-hidden            
  field destName     as character {&nodeType} column-label "Destination"  format "x(50)"
  
  /* for client */
  field entityDesc   as character {&nodeType} column-label "Entity"       format "X(20)"
  field actionDesc   as character {&nodeType} column-label "Action"       format "x(100)"
  field destTypeDesc as character {&nodeType} column-label "Type"         format "x(50)"
  field entityName   as character {&nodeType} column-label "Name"         format "x(100)"
  field destNameDesc as character {&nodeType} column-label "Destination"  format "x(100)"

  /* used for spGetActionDestination */
  field emailHTML    as character {&nodeType}
  field emailSubject as character {&nodeType}
  field displayName  as character {&nodeType}
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


