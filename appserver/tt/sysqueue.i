&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file sysqueuehead.i
@description System Queue Header temp table
@modified 
09.03.2021	MK          Framework changes
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias sysqueue
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

define temp-table {&tableAlias}
  field queueID         as integer   {&nodeType} column-label "Queue ID"     format "ZZZZ9"
  field actionID        as character {&nodeType} serialize-hidden
  field actionDesc      as character {&nodeType} column-label "Action"       format "x(100)"
  field uid             as character {&nodeType} column-label "UID"          format "x(100)"
  field Stat            as character {&nodeType} serialize-hidden
  field queueStatDesc   as character {&nodeType} column-label "Queue Status" format "x(100)"
  field appCode         as character {&nodeType} column-label "App Code"     format "x(100)"
  field queueDate       as datetime  {&nodeType} column-label "Queue Date"   format "99/99/9999"
  field startDate       as datetime  {&nodeType} column-label "Start Date"   format "99/99/9999"
  field endDate         as datetime  {&nodeType} column-label "End Date"     format "99/99/9999"
  field notifyRequestor as LOGICAL   {&nodeType} column-label "Notify Requester"
  
  /* from queueitem table */
  field seq           as integer   {&nodeType} column-label "Seq"          format "ZZZZ9"
  field itemStat      as character {&nodeType} serialize-hidden
  field itemStatDesc  as character {&nodeType} column-label "Item Status"  format "x(100)"
  field itemFault     as character {&nodeType} column-label "Item Fault"   format "x(1000)"
  field destinations  as character {&nodeType} column-label "Destinations" format "x(1000)"
  field parameters    as character {&nodeType} column-label "Parameters"   format "x(1000)"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


