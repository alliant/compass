&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* 
@name tt/reportagentstatement.i
@description Temp table for the REPORT AGENT STATEMENT
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias reportagentstatement
&ENDIF

define temp-table {&tableAlias}
  field claimID as integer
  field policyID as integer
  field fileNumber as character
  field category as character
  field stat as character
  field dateClosed as datetime
  field stateID as character
  field laePaid as decimal
  field lossPaid as decimal
  field totalPaid as decimal
  field deductible as decimal
  field unadjusted as decimal
  field billable as decimal
  field billedAmount as decimal
  field billedDate as datetime
  field invoiceNumber as character
  field paidDate as datetime
  field age as integer
  field paidAmount as decimal
  field deMiniums as decimal
  field waivedAmount as decimal
  .
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
