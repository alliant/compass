&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/contact.i
   data-structure for a CONTACT
   @author D.Sinclair
   @created 7.18.2014
 */



&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias contact
&ENDIF

def temp-table {&tableAlias}
 field contactID as int
 field stat as char
 field fname as char
 field lname as char
 field fullname as char
 field jobtitle as char
 field email as char
 field phone as char

 field company as char
 field addr1 as char
 field addr2 as char
 field city as char
 field state as char
 field zip as char
 field country as char

 field website as char
 field notes as char
 field agentID as char /* first is optionally for the address */
 field useAgentAddress as logical
 field categories as char
 field uid as char
 field modifiedDate as datetime
 field modifiedBy as char
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


