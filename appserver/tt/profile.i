&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/profile.i
Purpose     :

Syntax      :

Description : Used specifically for WEB CRM app 

Author(s)   : Rahul 
Created     : 04-15-2019
Notes       :

Modification:
Date          Name           Description
----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias profile
&ENDIF
   
def temp-table {&tableAlias}
  field type       as character 
  field entityId   as character    
  field favorite   as logical
  field name       as character  
  field stat       as character 
  field city       as character 
  field state      as character 
  field zip        as character  
  field email      as character  
  field rowNum     as character
  
  &if defined(agentstop) &then
    field netPermium as decimal  
    field cpl        as decimal
  &endif
 
  &if defined(favorite) &then
    field sysfavoriteID as integer  
  &endif
  
  &if defined(agentsperformance) &then
    field  nprPlanned      as decimal
    field  nprActual       as decimal
    field  nprPercent      as decimal
    field  nprPlannedYTD   as decimal
    field  nprPercentYTD   as decimal
  &endif
  
  &if defined(personAgent) &then
    field  agentNameList   as character
  &endif
  
  &if defined(noOfPeople) &then
    field  noOfPeople      as integer
  &endif  
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


