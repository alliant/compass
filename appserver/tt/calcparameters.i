&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/calcParameters.i
Purpose     : 

Syntax      :

Description :  

Author(s)   : Rahul Sharma
Created     : 04-28-2020
Notes       : 
----------------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias calcParameters
&endif
   
def temp-table {&tableAlias}
fields scenarioID                 as integer
fields scenarioName               as character
fields version                    as character
fields region                     as character
fields propertyType               as character
fields simultaneous               as character
fields coverageAmount             as character
fields ratetype                   as character
fields reissueCoverageAmount      as character
fields PriorEffectiveDate         as character
fields ownerEndors                as character
fields loanCoverageAmount         as character
fields loanRateType               as character
fields loanReissueCoverageAmount  as character
fields loanPriorEffectiveDate     as character
fields loanEndors                 as character
fields lenderCPL                  as character
fields buyerCPL                   as character
fields sellerCPL                  as character
fields ratetypecode               as character
fields loanRateTypecode           as character
fields secondloanRateType         as character
fields secondloanCoverageAmount   as character
fields secondLoanEndors           as character
.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


