&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/apinv.i
   Generic datastructure for A/P invoices
   D.Sinclair 4.23.2015
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias apinv
&ENDIF

def temp-table {&tableAlias}

 field apinvID as int

 field refType as char 
 field refID as char 
 field refSeq as int
 field refCategory as char 

 field vendorID as char
 field vendorName as character
 field vendorAddress as character
 field invoiceNumber as char
 field PONumber as char 
 field invoiceDate as datetime
 field dateReceived as datetime
 field amount as deci
 field dueDate as datetime
 field deptID as char

 field notes as char
 field stat as char  /* O)pen, P)osted, D)enied, V)oided */
 field uid as character
 field contention as logical
 field reduceLiability as logical

 /* The following fields are not in the DB */
 field username as char
 field contactName as char	/* Contact info from the vendor */
 field contactTitle as char
 field contactEmail as char
 field contactPhone as char
 field contactAddress as char
 field hasDocument as logical /* flag to tell if the invoice has a document in sharefile */
 field documentFilename as character
 
 field isFileCreated as logical
 field isFileDeleted as logical
 field isFileEdited as logical
 .

/* Indexes:  apinvID (unique)
             vendorID (non-unique, foreign key)
             refType, refID, refSeq, refCategory

  The ref fields point to the originating/related transaction.

   Claims  refType = "C"
           refID = string(claimID) or "" for unknown
           refSeq = 0
           refCategory = L)oss, LA(E), I)ncidental, U)nallocated, blank = unknown 

   Claims Rules:
    1. Approving an apinv results in the creation of an aptrx for <= amount.  In other
       words, we can approve a partial payment. ???
    2. If refCategory = "U", the apinv cannot be approved if there is not at least 
       one contact associated with the file that has the role of "Administrator".
    3. If refCategory = "L" or "E", the apinv cannot be approved if the associated
       reserve balance on the file is less than the amount being approved.
    4. The apinv cannot be approved if the refID does not point to a valid claim.
    5. The apinv cannot be approved if the refCategory is blank or invalid.
    6. The sum of aptrx.transAmount must be <= apinv.amount.
    7. Apinv.amount cannot be negative. ???
 */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


