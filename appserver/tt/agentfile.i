/* tt/agentfile.i
   Datastructure for an AGENT FILE
   @created 2.19.2015
   @author D.Sinclair
   Modifications:
   Date          Name           Description
   12/16/2019    Rahul Sharma   Modified to add fields related to agentfile
   1.12.2020     D.Sinclair     Added 5 description fields to avoid need for reference tables to lookup
   2.03.2022     SC             Added Origin, FileType, FirstCPLIssueDt and FirstPolicyIssueDt
   1.27.2023     SA             Task 99033 modified to rearrange fields 
   07.4.2023     Sagar K        Added fields related to agentfile
   01/24/2024    K.R            Removed suffix field
   06/03/2024    Sachin         Task 112908- Changes in Agentfile related to Notes, Source and CPLAddr1, etc.
   08/26/2024    Vignesh R      Modified to add estateType field.
   08/29/2024    S chandu       Added unpostedAmount and unpostedFileinvID fields.
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias agentfile
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ENDIF

&IF DEFINED(serializeName) = 0 &THEN
&SCOPED-DEFINE serializeName serialize-name "agentfile"
&ELSE
&SCOPED-DEFINE serializeName serialize-name "{&serializeName}"
&ENDIF


def temp-table {&tableAlias} NO-UNDO {&serializeName}

field agentFileID         as integer   {&nodeType}
field addr1               as character {&nodeType}
field addr2               as character {&nodeType}
field addr3               as character {&nodeType}
field addr4               as character {&nodeType}
field buyerDescription    as character {&nodeType}  
field closingDate         as date      {&nodeType}
field invoiceAmt          as decimal   {&nodeType}
field liabilityAmt        as decimal   {&nodeType}
field notes               as character {&nodeType}
field officeID            as integer   {&nodeType}
field osAmt               as decimal   {&nodeType}
field paidAmt             as decimal   {&nodeType}
field propertyDescription as clob      {&nodeType}
field sellerDescription   as character {&nodeType}
field softwareID          as integer   {&nodeType}
field transactionPrice    as decimal   {&nodeType}
field source              as character {&nodeType}
field cplAddr1            as character {&nodeType}
field cplCity             as character {&nodeType}
field cplState            as character {&nodeType}
field cplZip              as character {&nodeType}

&if defined(getAgentFile) &then
  field agentID             as character {&nodeType}
  field agentName           as character {&nodeType}  
  field fileNumber          as character {&nodeType}
  field stateID             as character {&nodeType} serialize-name "state"
  field stateName           as character {&nodeType}
  field officeName          as character {&nodeType}  
  field city                as character {&nodeType} serialize-name "addrCity"
  field zip                 as character {&nodeType} serialize-name "addrZip"
  field state               as character {&nodeType} serialize-name "addrState"
  field addrStateName       as character {&nodeType}
  field transactionType     as character {&nodeType} serialize-name "propertyType"
  field fileType            as character {&nodeType} serialize-name "category"
  field estateType          as character {&nodeType}
  field stage               as character {&nodeType} serialize-name "fileStage"
  field canEdit             as logical   {&nodeType}
  field parID               as integer   {&nodeType} 
  field orderedby           as character {&nodeType}
  field orderedemail        as character {&nodeType}
  field orderedphone        as character {&nodeType}
  field ordereddate         as datetime  {&nodeType}
  field searchordered       as logical   {&nodeType}
  field policyordered       as logical   {&nodeType}
  field ishold              as logical   {&nodeType}
  field holddescription     as character {&nodeType}
  field holdby              as character {&nodeType}
  field holddate            as datetime  {&nodeType}
  field unpostedamount      as decimal   {&nodeType}
  field unpostedFileInvID   as integer   {&nodeType}
&elseif defined(updateFile) &then
  field city                as character {&nodeType} serialize-name "addrCity"  
  field zip                 as character {&nodeType} serialize-name "addrZip"  
  field state               as character {&nodeType} serialize-name "addrState"
  field transactionType     as character {&nodeType} serialize-name "propertyType"
  field fileType            as character {&nodeType} serialize-name "category"
  field estateType          as character {&nodeType}    
  field officeName          as character {&nodeType}
&else
  field fileID              as character {&nodeType} 
  field stat                as character {&nodeType}
  field stage               as character {&nodeType}
  field city                as character {&nodeType}
  field countyID            as character {&nodeType}
  field state               as character {&nodeType}
  field zip                 as character {&nodeType}
  field reportingDate       as datetime  {&nodeType}
  field transactionType     as character {&nodeType}
  field insuredType         as character {&nodeType}
  field arNotes             as character {&nodeType} /* Used when applying payment/credit */
  field agentName           as character {&nodeType}
  field origin              as character {&nodeType}
  field firstCPLIssueDt     as datetime  {&nodeType}
  field firstPolicyIssueDt  as datetime  {&nodeType}
  field fileType            as character {&nodeType}
  field estateType          as character {&nodeType}
  field assignedTo          as character {&nodeType}  /* remove later */
  field agentID             as character {&nodeType}
  field fileNumber          as character {&nodeType}
  field liabilityAmount     as decimal   {&nodeType}
  field grossPremium        as decimal   {&nodeType}
  field retentionPremium    as decimal   {&nodeType}
  field netPremium          as decimal   {&nodeType}
  field invoiceDate         as datetime  {&nodeType}
  field effDate             as datetime  {&nodeType}
  field numPolicies         as integer   {&nodeType}
  field descStat            as character {&nodeType}
  field descCountyID        as character {&nodeType}
  field descTransactionType as character {&nodeType}
  field descInsuredType     as character {&nodeType}
  field descStage           as character {&nodeType}
  field stateID             as character {&nodeType}
  
&endif
.
