&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/ratestate.i
Purpose     :

Syntax      :

Description :

Author(s)   : Anjly Chanana
Created     :
Notes       :

Modification:
Date          Name      Description
----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias ratestate
&endif

define temp-table {&tableAlias}
  field cardSetID            as integer
  field version              as integer
  field active               as logical
  field stateID              as character
  field effectiveDate        as datetime 
  field createdBy            as character
  field dateApproved         as datetime
  field approvedBy           as character
  field dateFiled            as datetime
  field filingMethod         as character
  field dateStateApproved    as datetime
  field description          as character
  field comments             as character
  field lastModifiedDate     as datetime
  field lastModifiedBy       as character
  field execBusinessLogic    as character
  field execDataModel        as character
  field execUserInterface    as character
  field configUserInterface  as character
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 62.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


