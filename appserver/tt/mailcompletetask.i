/*------------------------------------------------------------------------
File        : tt/mailcompletetask.i
Purpose     :

Syntax      :

Description : store complete task mail
                                           
Author(s)   : S Chandu
Created     : 11-23-2023
Notes       :
----------------------------------------------------------------------*/
&IF DEFINED(tableAlias) = 0 &THEN
&SCOPED-DEFINE tableAlias MailCompleteTask
&ENDIF

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "MailCompleteTask"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif
  
define temp-table {&tableAlias} no-undo {&serializeName} 
  field mailTo              as  character   {&nodeType}
  field mailSubject         as  character   {&nodeType}
  field mailCc              as  logical     {&nodeType}
  field mailBody            as  clob        {&nodeType}
  .