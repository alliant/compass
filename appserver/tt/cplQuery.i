&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 cpl.i
 Closing Protection Letter data structure
 D.Sinclair 5.30.2012
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias cplreport
&ENDIF
 
&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF
 
DEF TEMP-TABLE {&tableAlias}  
  FIELD stat                    AS CHARACTER {&nodeType}
  FIELD cplID                   AS CHARACTER {&nodeType}
  FIELD issueDate               AS DATETIME  {&nodeType} 
  FIELD fileNumber              AS CHARACTER {&nodeType}
  FIELD formID                  AS CHARACTER {&nodeType}
  FIELD noOfCPLReported         AS INTEGER   {&nodeType}
  FIELD revReported             AS DECIMAL   {&nodeType}
  FIELD noOfCPLIssued           AS INTEGER   {&nodeType}
  FIELD revExpected             AS DECIMAL   {&nodeType}  
  FIELD rateReportedPerCPL      AS DECIMAL   {&nodeType}  
  FIELD rateExpectedPerCPL      AS DECIMAL   {&nodeType}
  FIELD grossPremium            AS DECIMAL   {&nodeType}
  FIELD netPremium              AS DECIMAL   {&nodeType}
  FIELD retentionPremium        AS DECIMAL   {&nodeType}
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 16.67
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


