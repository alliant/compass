&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/unappliedtran.i
Purpose     : used to fetch/store unapplied payments/credits

Syntax      :

Description : Used specifically for AR app 

Author(s)   : MK
Created     : 03-02-2021
Notes       : 

Modification:
Date          Name    Description
06-02-21      SA      Modified to add fields total refund and sourceID 
----------------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias unappliedtran
&endif
   
def temp-table {&tableAlias}
  field unappliedtranID     as integer
  field revenuetype         as character /* revenue type of payment */
  field entity              as character
  field entityID            as character
  field tranID              as character
  field arTranID            as integer
  field reference           as character
  field referencedate       as datetime
  field receiptdate         as datetime
  field referenceamt        as decimal
  field remainingamt        as decimal
  field void                as logical
  field voiddate            as datetime
  field voidby              as character
  field description         as character
  field createddate         as datetime
  field createdby           as character
  field filenumber          as character
  field fileID              as character
  field posted              as logical   /* true if the payment is posted */
  field transType           as character /* 'F' if payment is file based else '' */
  field postdate            as datetime
  field postby              as character
  field archived            as logical
  field transDate           as datetime
  field type                as character /* Payment or credit type transaction */
  
  field selectrecord        as logical    /* Client side */
  field entityname          as character  /* Client side */
  field appliedamt          as decimal    /* Client side */
  field refundamt           as decimal    /* Client side */
  field amtToRefund         as decimal    /* Client side */
  field username            as character  /* Client side */
  field voidbyusername      as character  /* Client side */
  field postbyname          as character  /* Client side */
  field fileExist           as character  /* Client side */
  field notes               as character  /* Client side */
  field stat                as character  /* Client side */
  field totalrefundamt      as decimal    /* Client side */
  field sourceID            as character  /* Client side */
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


