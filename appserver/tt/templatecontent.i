/*------------------------------------------------------------------------
    File        : templatecontent.i
    Purpose     : templatecontent datastructure
    Author(s)   : Shweta Dhar
    Created     : 7.08.2022
    Modification:
    Date          Name      Description
              
  ----------------------------------------------------------------------*/
  
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias ttTemplatecontent
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ELSE
&SCOPED-DEFINE nodeType XML-NODE-TYPE "{&nodeType}"
&ENDIF

&IF DEFINED(serializeName) = 0 &THEN
&SCOPED-DEFINE serializeName serialize-name "templatecontent"
&ELSE
&SCOPED-DEFINE serializeName serialize-name "{&serializeName}"
&ENDIF


def temp-table {&tableAlias} no-undo {&serializeName}
field templateID     as integer   {&nodeType}
field type           as character {&nodeType}
field seq            as integer   {&nodeType}
field content        as character {&nodeType}
field indent         as integer   {&nodeType}
field isNote         as logical   {&nodeType}.
