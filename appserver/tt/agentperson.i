&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/agentPerson.i
Purpose     :

Syntax      :

Description :

Author(s)   : Rahul Sharma
Created     : 03-05-2018
Notes       :

Modification:
Date          Name           Description
05/14/2018    Naresh Chopra  Remove active field of agentPerson.
08/22/2018    Naresh Chopra  Added new field "Notes".
12/07/2018    Rahul Sharma   Modified to add new field personName
01/04/2019    Gurvindar      Added index idxName
----------------------------------------------------------------------*/
&IF defined(tableAlias) = 0 &THEN 
&scoped-define tableAlias agentPerson
&ENDIF
   
define temp-table {&tableAlias}
  field agentPersonID      as integer
  field agentID            as character
  field PersonID           as character
  field effectivedate      as datetime
  field expirationDate     as datetime
  field jobTitle           as character
  field isCtrlPerson       as logical
  field notes              as character
  
  field personName         as character /* used for client only */
  field agentName          as character /* used for client only */
  field comStat            as character /* used for client only */
  field stateID            as character /* used for client only */
  
  index idxName personName
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


