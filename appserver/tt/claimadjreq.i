&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/claimadjreq.i
   REQuest to adjust a cost limit
   D.Sinclair 4.23.2015
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias claimadjreq
&ENDIF

def temp-table {&tableAlias}
 field claimID as int
 field refCategory as char        /* L)oss, LA(E), I)ncidental */
 field requestedBy as char        /* Requester */
 field requestedAmount as deci    /* Amount to adjust the reserve balance */
 field uid as char                /* Approver */
 field dateRequested as datetime
 field notes as char
 
 /* not in the database */
 field username as char
 field requestedUsername as char
 field isNew as logical
 field isView as logical
 .

/* Index:  claimID, refCategory

   Only one claimadjreq for a refCategory can exist for a claimID at one time.

   If request is approved, this record is deleted and a claimadjtrx is created.
   If the request is denied, this record is deleted.
 */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


