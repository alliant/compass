/* tt/fileProperty.i
   Datastructure for an fileProperty
   @created 05.31.2022
   @author Vignesh Rajan
   Modifications:
   Date          Name           Description
   08/09/2024    K.R            Modififed to remove deed related fields   
   
*/

&IF DEFINED(tableAlias) = 0 &THEN
&SCOPED-DEFINE tableAlias fileProperty
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ENDIF

&IF DEFINED(serializeName) = 0 &THEN
&SCOPED-DEFINE serializeName SERIALIZE-NAME "fileProperty"
&ELSE
&SCOPED-DEFINE serializeName SERIALIZE-NAME "{&serializeName}"
&ENDIF

DEFINE TEMP-TABLE {&tableAlias} NO-UNDO  {&serializeName}
&IF DEFINED(getAgentFile) &THEN
  FIELD agentFileID       AS INTEGER   {&nodeType}
  FIELD seq               AS INTEGER   {&nodeType}
  FIELD propertyType      AS CHARACTER {&nodeType}
  FIELD parcelID          AS CHARACTER {&nodeType}
  FIELD addr1             AS CHARACTER {&nodeType}
  FIELD addr2             AS CHARACTER {&nodeType}
  FIELD addr3             AS CHARACTER {&nodeType}
  FIELD addr4             AS CHARACTER {&nodeType}
  FIELD city              AS CHARACTER {&nodeType} SERIALIZE-NAME 'addrCity'
  FIELD stateID           AS CHARACTER {&nodeType} SERIALIZE-NAME 'addrState'
  FIELD stateName         AS CHARACTER {&nodeType} SERIALIZE-NAME 'addrStateName'
  FIELD zip               AS CHARACTER {&nodeType} SERIALIZE-NAME 'addrZip'
  FIELD countyID          AS CHARACTER {&nodeType}
  FIELD countyDesc        AS CHARACTER {&nodeType}  
  FIELD section           AS CHARACTER {&nodeType}
  FIELD township          AS CHARACTER {&nodeType}
  FIELD townshipDirection AS CHARACTER {&nodeType}
  FIELD range             AS CHARACTER {&nodeType}
  FIELD rangeDirection    AS CHARACTER {&nodeType}
  FIELD portion           AS CHARACTER {&nodeType}
  FIELD condominum        AS CHARACTER {&nodeType} SERIALIZE-NAME 'condoName'
  FIELD subdivision       AS CHARACTER {&nodeType} SERIALIZE-NAME 'subdivisionName'
  FIELD unitno            AS CHARACTER {&nodeType}
  FIELD instrumentNumber  AS CHARACTER {&nodeType}
  FIELD lot               AS CHARACTER {&nodeType}
  FIELD block             AS CHARACTER {&nodeType}
  FIELD book              AS CHARACTER {&nodeType}
  FIELD pageNumber        AS CHARACTER {&nodeType}
  FIELD levels            AS CHARACTER {&nodeType}
  FIELD platDate          AS DATETIME  {&nodeType}
  FIELD shortLegal        AS CLOB      {&nodeType}
  FIELD longLegal         AS CLOB      {&nodeType}
  FIELD latitude          AS DECIMAL   {&nodeType}
  FIELD longitude         AS DECIMAL   {&nodeType}
  FIELD taxYear           AS INTEGER   {&nodeType}
  FIELD taxAmtDue         AS DECIMAL   {&nodeType}
  FIELD taxAmtPaid        AS DECIMAL   {&nodeType}
  FIELD notes             AS CLOB      {&nodeType}
&ELSEIF  DEFINED(updateFile) &THEN
  FIELD agentFileID       AS INTEGER   {&nodeType}
  FIELD seq               AS INTEGER   {&nodeType}
  FIELD propertyType      AS CHARACTER {&nodeType}
  FIELD parcelID          AS CHARACTER {&nodeType}
  FIELD addr1             AS CHARACTER {&nodeType}
  FIELD addr2             AS CHARACTER {&nodeType}
  FIELD addr3             AS CHARACTER {&nodeType}
  FIELD addr4             AS CHARACTER {&nodeType}
  FIELD city              AS CHARACTER {&nodeType} SERIALIZE-NAME 'addrCity' 
  FIELD stateID           AS CHARACTER {&nodeType} SERIALIZE-NAME 'addrState'
  FIELD zip               AS CHARACTER {&nodeType} SERIALIZE-NAME 'addrZip'  
  FIELD countyID          AS CHARACTER {&nodeType}
  FIELD section           AS CHARACTER {&nodeType}
  FIELD township          AS CHARACTER {&nodeType}
  FIELD townshipDirection AS CHARACTER {&nodeType}
  FIELD range             AS CHARACTER {&nodeType}
  FIELD rangeDirection    AS CHARACTER {&nodeType}
  FIELD portion           AS CHARACTER {&nodeType}
  FIELD instrumentNumber  AS CHARACTER {&nodeType}
  FIELD lot               AS CHARACTER {&nodeType}
  FIELD block             AS CHARACTER {&nodeType}
  FIELD book              AS CHARACTER {&nodeType}
  FIELD pageNumber        AS CHARACTER {&nodeType}
  FIELD levels            AS CHARACTER {&nodeType}
  FIELD platDate          AS DATETIME  {&nodeType}
  FIELD shortLegal        AS CLOB      {&nodeType}
  FIELD longLegal         AS CLOB      {&nodeType}
  FIELD latitude          AS DECIMAL   {&nodeType}
  FIELD longitude         AS DECIMAL   {&nodeType}
  FIELD taxYear           AS INTEGER   {&nodeType}
  FIELD taxAmtDue         AS DECIMAL   {&nodeType}
  FIELD taxAmtPaid        AS DECIMAL   {&nodeType}
  FIELD notes             AS CLOB      {&nodeType}
  FIELD Condominum        AS CHARACTER {&nodeType} SERIALIZE-NAME 'condoName'
  FIELD subdivision       AS CHARACTER {&nodeType} SERIALIZE-NAME 'subdivisionName'
  FIELD unitno            AS CHARACTER {&nodeType}
&ELSE
  FIELD agentFileID       AS INTEGER   {&nodeType}
  FIELD seq               AS INTEGER   {&nodeType}
  FIELD propertyType      AS CHARACTER {&nodeType}
  FIELD parcelID          AS CHARACTER {&nodeType}
  FIELD addr1             AS CHARACTER {&nodeType}
  FIELD addr2             AS CHARACTER {&nodeType}
  FIELD addr3             AS CHARACTER {&nodeType}
  FIELD addr4             AS CHARACTER {&nodeType}
  FIELD city              AS CHARACTER {&nodeType}
  FIELD stateID           AS CHARACTER {&nodeType}
  FIELD zip               AS CHARACTER {&nodeType}
  FIELD countyID          AS CHARACTER {&nodeType}
  FIELD section           AS CHARACTER {&nodeType}
  FIELD township          AS CHARACTER {&nodeType}
  FIELD townshipDirection AS CHARACTER {&nodeType}
  FIELD range             AS CHARACTER {&nodeType}
  FIELD rangeDirection    AS CHARACTER {&nodeType}
  FIELD portion           AS CHARACTER {&nodeType}
  FIELD instrumentNumber  AS CHARACTER {&nodeType}
  FIELD lot               AS CHARACTER {&nodeType}
  FIELD block             AS CHARACTER {&nodeType}
  FIELD book              AS CHARACTER {&nodeType}
  FIELD pageNumber        AS CHARACTER {&nodeType}
  FIELD levels            AS CHARACTER {&nodeType}
  FIELD platDate          AS DATETIME  {&nodeType}
  FIELD shortLegal        AS CLOB      {&nodeType}
  FIELD longLegal         AS CLOB      {&nodeType}
  FIELD latitude          AS DECIMAL   {&nodeType}
  FIELD longitude         AS DECIMAL   {&nodeType}
  FIELD taxYear           AS INTEGER   {&nodeType}
  FIELD taxAmtDue         AS DECIMAL   {&nodeType}
  FIELD taxAmtPaid        AS DECIMAL   {&nodeType}
  FIELD notes             AS CLOB      {&nodeType}
  FIELD CreatedDate       AS DATETIME  {&nodeType}
  FIELD CreatedBy         AS CHARACTER {&nodeType}
&ENDIF
.
