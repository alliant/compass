&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/aragentstatement.i
Purpose     :

Syntax      :

Description :

Author(s)   : Rahul Sharma
Created     : 11-18-2019
Notes       :

Modification:
Date          Name      Description
07-26-21       MK	Added "sourceType" field.
08-16-21       MK	Added "sourceID" field.
----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias aragentstatement
&endif
   
define temp-table {&tableAlias}
  field stateID           as character
  field agentID           as character
  field fileNumber        as character
  field fileID            as character
  field invoiceID         as character
  field policyDate        as datetime
  field policyNumber      as character
  field tranCode          as character
  field TranID            as character
  field adj               as logical
  field liability         as decimal
  field policyPremium     as decimal
  field remittanceAmt     as decimal
  field paymentAmt        as decimal
  field refundAmt         as decimal
  field receviedDate      as datetime
  field checkNumber       as character
  field checkAmt          as decimal
  field difference        as decimal
  field paymentType       as character
  field type              as character
  field sourceID          as character
  field sourceType        as character
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


