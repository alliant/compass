&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 batch.i
 BATCH data structure
 D.Sinclair 5.30.2012
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias batchforperiod
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF
 
define temp-table {&tableAlias}
  field batchID                  as integer   {&nodeType} column-label "Batch ID"             format "ZZZZZZZZ"
  field periodID                 as integer   {&nodeType} column-label "Period ID"            format "ZZZZZZ"
  field agentID                  as character {&nodeType} column-label "Agent ID"             format "x(20)"
  field stateID                  as character {&nodeType} column-label "State ID"             format "x(50)"
  field stateDesc                as character {&nodeType} column-label "State"                format "x(50)"
  field stat                     as character {&nodeType} column-label "Stat"                 format "x(6)"
  field statDesc                 as character {&nodeType} column-label "Status"               format "x(50)"
                                                                                              
  field grossPremiumReported     as decimal   {&nodeType} column-label "Reported Gross"       format "$(>>,>>>,>>9.99)"
  field grossPremiumDelta        as decimal   {&nodeType} column-label "Gross Delta"          format "$(>>,>>>,>>9.99)"
                                                                                              
  field netPremiumReported       as decimal   {&nodeType} column-label "Reported Net"         format "$(>>,>>>,>>9.99)"
  field netPremiumDelta          as decimal   {&nodeType} column-label "Net Delta"            format "$(>>,>>>,>>9.99)"
                                                                                              
  field retainedPremiumDelta     as decimal   {&nodeType} column-label "Retained!Delta"       format "$(>>,>>>,>>9.99)"
                                                                                              
  field agentActiveDate          as datetime  {&nodeType} column-label "Active Date"          format "99/99/9999"
  field agentStat                as character {&nodeType} serialize-hidden
  field agentStatDesc            as character {&nodeType} column-label "Current!Agent Status" format "x(30)"
  field agentName                as character {&nodeType} column-label "Agent Name"           format "x(200)"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 16.67
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


