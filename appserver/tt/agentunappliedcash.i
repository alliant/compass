&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/agentunappliedCash.i
Purpose     :

Syntax      :

Description :

Author(s)   : Rahul Sharma
Created     : 01-08-2020
Notes       :

Modification:
Date          Name      Description
12/08/2022    Shefali   Task #100854 Add status on unapplied transactions report

----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias agentunappliedCash
&endif
   
define temp-table {&tableAlias}
  field stateID       as character
  field agentID       as character
  field stat          as character
  field name          as character
  field manager       as character
  field fileNumber    as character
  field tranID        as character
  field artranID      as integer
  field checknum      as character
  field checkAmt      as decimal
  field remainingAmt  as decimal
  field appliedAmt    as decimal
  field refundedAmt   as decimal  
  field checkDate     as datetime  
  field receiptDate   as datetime
  field postDate      as datetime
  field depositRef    as character  
  field arCashglRef   as character
  field arglref       as character
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


