&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/arwriteoff.i
Purpose     : used to fetch/store AR Transaction and    writeoff records

Syntax      :

Description : Used specifically for AR app 

Author(s)   : Shefali
Created     : 05-05-2021
Notes       : 

Modification:
Date         Name           Description

----------------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias arwriteoff
&endif
   
def temp-table {&tableAlias}
  field arwriteoffID     as integer
  field artranID         as integer
  field entity           as character /* Agent */
  field entityID         as character /* AgentID */
  field agentname        as character /* Agent Name */  
  field tranAmount       as decimal   /* total writeoff amt for specific entityID */
  field tranDate         as datetime  /* postdate */
  field createdBy        as character
  field createdByName    as character
  field createdDate      as datetime
  field postBy           as character
  field postByName       as character
  field postDate         as datetime
  field void             as logical
  field voidDate         as datetime
  field voidBy           as character
  field voidByName       as character
  field ledgerID         as integer
  field voidLedgerID     as integer
  field type             as character 
  field source           as character
  field notes            as character
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


