/* tt/participant.i
   Datastructure for an participant
   @created 11.22.2024
   @author S Chandu
   Modifications:
   Date          Name           Description
   
 */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias  participant
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "participant"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo  {&serializeName}
  field participantID      as  integer    {&nodeType}
  field trainingID         as  integer    {&nodeType}
  field email              as  character  {&nodeType}
  field name               as  character  {&nodeType}
  field createdDate        as  datetime   {&nodeType}
  field needCE             as  logical    {&nodeType}
  field license            as  character  {&nodeType}
  field company            as  character  {&nodeType}
  field didAttend          as  logical    {&nodeType}
  field hasPaid            as  logical    {&nodeType}
  field paymentID          as  character  {&nodeType}
  field notes              as  character  {&nodeType}
  .
