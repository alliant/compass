/*------------------------------------------------------------------------
@file tt/partyjoinby.i
Datastructure for a partyjoinby
@created 09.01.2024
@author SRK
Modifications:
Date          Name           Description
   
---------------------------------------------------------------------- */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias partyjoinby
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "partyjoinby"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo  {&serializeName}
  field objValue     as  character  {&nodeType} serialize-name "Value"
  
  .

