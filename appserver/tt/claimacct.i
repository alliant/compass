&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/claimacct.i
   datastructure for a CLAIM ACCounTing
   Created D.Sinclair 2.25.2015
   
   01.26.2017 John Oliver
   Added ability to capture approved date
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias claimacct
&ENDIF

/* Should recoupments be against the Loss or LAE in accounting terms?
 */

def temp-table {&tableAlias}
  field claimID          as integer
  field acctType         as character /* (L)oss, LA(E), I)ncidentals, U)nallocated */
  field transType        as character /* reserve (A)dj, (P)ayable, (R)eceipt */
  field transDescription as character /* Readable version of transType */
  field seq              as integer
  field stat             as character /* (O)pen, (A)pproved, or (C)ompleted */
  field refType          as character /* (A)gent, (V)endor, (I)nsured */
  field refID            as character /* agentID, vendorID */
  field refDescription   as character /* name of refID */
  field requestedBy      as character
  field dateRequested    as datetime 
  field requestedAmount  as decimal
  field uid              as character /* Approver */
  field approvedDate     as datetime
  field transDate        as datetime  /* Actual date recorded in accounting */
  field transAmount      as decimal   /* Actual amount affecting accounting */
  field waivedAmount     as decimal   /* Waived amount for receivables */
  field balance          as decimal   /* A rolling balance for the UI */
  field coverageID       as character 
  
  /* descriptions */
  field statDesc         as character
  field requestedDesc    as character
  field uidDesc          as character
  .

/* Rules:

    1. policyID must be set and the same as a claimcoverage 
        record for a (L)oss payment/receipt
    2. Only one pending reserve adjustment for each type can be 
        (P)ending at a time
    3. A "denied" reserve adjustment is deleted (i.e., not marked 
        with a different status
    4. The transDate is the date the transaction is recorded in accounting 
*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


