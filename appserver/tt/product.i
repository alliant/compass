/* tt/product.i
   @created 07.12.2022
   @author S Chandu
   Modifications:
   Date          Name           Description
   02/08/2024    SR             Added templateID field.
   05/01/24      Sachin         Removed product.revenuetype field
 */
 
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias product
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "product"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
  field productID         as integer   {&nodeType} column-label "ID"   
  field templateID        as integer   {&nodeType}  
  field stateID           as character {&nodeType} column-label "State ID"
  field stateName         as character {&nodeType} column-label "State"                
  field description       as character {&nodeType} column-label "Product"             
  field price             as decimal   {&nodeType} 
  field effectivedate     as date      {&nodeType} column-label "Effective Date"       
  field expiryDate        as date      {&nodeType} column-label "Expiry Date"          
  field active            as logical   {&nodeType} column-label "Active"  
  field createddate       as datetime  {&nodeType}
  field createdby         as character {&nodeType}
  field lastModifiedDate  as datetime  {&nodeType}
  field lastModifiedBy    as character {&nodeType}             
  .





