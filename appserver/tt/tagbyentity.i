/*------------------------------------------------------------------------
    File        : tagbyentity.i
    Purpose     : To get tags report to an entity
    Syntax      :
    Description :
    Author(s)   : Shefali
    Created     : 06/15/2023
    Notes       :
    Modification:
    Date          Name       Description 
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias tagbyentity
&ENDIF
define temp-table {&tableAlias}
  field name             as character
  field tagscount        as integer
  .