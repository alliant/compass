/*------------------------------------------------------------
   tt/filepost.i
   @created 07/19/2023
   @author S chandu
   Modifications:
   Name          Date       Note
   ------------- ---------- -----------------------------------------------
   
   --------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias filePost
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "filePost"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
  field postedCount     as  integer {&nodeType}
  field notclosedIDs    as character  {&nodeType}
  field notFoundIDs     as character  {&nodeType}
  field errorIDs        as character  {&nodeType}
  .
