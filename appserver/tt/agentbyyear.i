&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/agent.i
   Audit For AGENT datastructure
   Author(s) Archana Gupta
   Created 7.31.2017
   Modified 08/03/2017 yOKE sAM c Fixed syntax error
   Modified AG added audittype and errtype field
            07/15/2021 SA Task 83510 add new fields grade and score
   */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias agent
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

def temp-table {&tableAlias}
 field agentID as char {&nodeType}
 field qarID as int {&nodeType}
 field audittype as char {&nodeType}
 field errType as char {&nodeType}
 field qarStatus as char {&nodeType}
 field schedStartDate as datetime {&nodeType}
 field schedFinishDate as datetime {&nodeType}
 field auditStartDate as datetime {&nodeType}
 field auditFinishDate as datetime {&nodeType}
 field whetherAudited as logical {&nodeType}
 field draftReportDate as datetime {&nodeType}
 field corporationID as char {&nodeType}                                
 field stateID as char {&nodeType}                              
 field stat as char {&nodeType}             /* P)rospect, A)ctive, C)losed, X)Cancelled */
 field name as char {&nodeType}
 field addr1 as char {&nodeType}
 field addr2 as char {&nodeType}
 field addr3 as char {&nodeType}
 field addr4 as char {&nodeType}
 field city as char {&nodeType}
 field county as char {&nodeType}                               
 field state as char {&nodeType}
 field zip as char {&nodeType}
 field contact as char {&nodeType}                              
 field phone as char {&nodeType}
 field fax as char {&nodeType}
 field email as char {&nodeType}
 field website as char {&nodeType}
 field stateLicense as char {&nodeType}                 
 field stateLicenseEff as datetime {&nodeType}              
 field stateLicenseExp as datetime {&nodeType}      
 field eoRequired as log {&nodeType}
 field eoCompany as char {&nodeType}
 field eoPolicy as char {&nodeType}
 field eoCoverage as decimal format "zzz,zzz,zz9" {&nodeType}
 field eoAggregate as decimal format "zzz,zzz,zz9" {&nodeType}
 field eoStartDate as datetime {&nodeType}                  
 field eoEndDate as datetime {&nodeType}                    
 field isAiga as logical {&nodeType}                    
 field isAffiliated as logical {&nodeType}              
 field createDate as datetime {&nodeType}                   
 field agentDate as datetime {&nodeType}                    
 field cancelDate as datetime {&nodeType}
 field closedDate as datetime {&nodeType}
 field orgType as char {&nodeType}                              
 field policyLimit as integer format "zz,zz9" {&nodeType}               
 field liabilityLimit as decimal format "zzz,zzz,zz9" {&nodeType}    
 field lastRemitDate as datetime {&nodeType}                
 field lastRemitAmt as decimal format "-zzz,zzz,zz9.99" {&nodeType}              
 field lastAuditDate as datetime {&nodeType}                
 field lastAuditScore as integer {&nodeType}    
 field nextAuditDue as datetime {&nodeType}                 
 field lastCertDate as datetime {&nodeType}                 
 field nextCertDue as datetime {&nodeType}                  
 field comments as character {&nodeType}                
 field contractID as char {&nodeType}
 field contractDate as datetime {&nodeType}
 field maxCoverage as decimal format "zzz,zzz,zz9" {&nodeType}
 field remitType as char {&nodeType}        /* P)ercentage of Gross and based on L)iability */
 field remitValue as decimal {&nodeType}    /* If type = (P) then net percentage of gross */
                                            /* If type = (L) then net rate per 1000 of liability */
 field remitAlert as decimal {&nodeType}
 field prospectDate as datetime {&nodeType}
 field activeDate as datetime {&nodeType}
 field reviewDate as datetime {&nodeType}
 field swVendor as char {&nodeType}
 field swVersion as char {&nodeType}
 field batchCount as int format "zzz,zz9" {&nodeType}
 field batchTotal as decimal format "-zzz,zzz,zz9.99" {&nodeType}
 field manager as char {&nodeType}
 field hasDocument as logical {&nodeType}
 
 field mName as char {&nodeType}
 field mAddr1 as char {&nodeType}
 field mAddr2 as char {&nodeType}
 field mCity as char {&nodeType}
 field mState as char {&nodeType}
 field mZip as char {&nodeType}
 
 field altaUID as char {&nodeType}
 field score as integer {&nodeType} 
 field grade as integer {&nodeType} 

 index agentid is primary unique
  agentID
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


