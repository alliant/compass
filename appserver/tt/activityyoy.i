&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/activityyoy.i
   ACTIVITY for the Year Over Year report
   Author(s) John Oliver
   Created 8/30/2018
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias activityyoy
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

define temp-table {&tableAlias}
  field agentID                          as character {&nodeType} column-label "Agent"                          format "x(20)"
  field stateID                          as character {&nodeType} column-label "State"                          format "x(30)"
  field stat                             as character {&nodeType}
  field corporationID                    as character {&nodeType} column-label "Company"                        format "x(200)"
  field manager                          as character {&nodeType}
  field region                           as character {&nodeType}
  field type                             as character {&nodeType}
  field prevMonth1                       as decimal   {&nodeType}
  field prevMonth2                       as decimal   {&nodeType}
  field prevMonth3                       as decimal   {&nodeType}
  field prevMonth4                       as decimal   {&nodeType}
  field prevMonth5                       as decimal   {&nodeType}
  field prevMonth6                       as decimal   {&nodeType}
  field prevMonth7                       as decimal   {&nodeType}
  field prevMonth8                       as decimal   {&nodeType}
  field prevMonth9                       as decimal   {&nodeType}
  field prevMonth10                      as decimal   {&nodeType}
  field prevMonth11                      as decimal   {&nodeType}
  field prevMonth12                      as decimal   {&nodeType}
  field currMonth1                       as decimal   {&nodeType}
  field currMonth2                       as decimal   {&nodeType}
  field currMonth3                       as decimal   {&nodeType}
  field currMonth4                       as decimal   {&nodeType}
  field currMonth5                       as decimal   {&nodeType}
  field currMonth6                       as decimal   {&nodeType}
  field currMonth7                       as decimal   {&nodeType}
  field currMonth8                       as decimal   {&nodeType}
  field currMonth9                       as decimal   {&nodeType}
  field currMonth10                      as decimal   {&nodeType}
  field currMonth11                      as decimal   {&nodeType}
  field currMonth12                      as decimal   {&nodeType}
  /* used for client only */
  field plannedYearTotalCurr             as decimal   {&nodeType} column-label "&2 Year Plan"                   format "(>>>,>>>,>>9)"
  field plannedThruMonthCurr             as decimal   {&nodeType} column-label "Plan thru!&3-&2"                format "(>>>,>>>,>>9)"
  field actualThruMonthPrev              as decimal   {&nodeType} column-label "Actual thru!&3-&1"              format "(>>>,>>>,>>9)"
  field actualThruMonthCurr              as decimal   {&nodeType} column-label "Actual thru!&3-&2"              format "(>>>,>>>,>>9)"
  field actualDifference                 as decimal   {&nodeType} column-label "&1 to &2!Difference"            format "(>>>,>>>,>>9)"
  field actualPercentage                 as decimal   {&nodeType} column-label "&1 to &2!Change"                format "(>>>,>>>,>>9)%"
  field actualToPlanDiff                 as decimal   {&nodeType} column-label "&2 Actual to!Plan Difference"   format "(>>>,>>>,>>9)"
  field actualToPlanPercent              as decimal   {&nodeType} column-label "&2 Actual!to Plan"              format "(>>>,>>>,>>9)%"
  field actualThruMonthCurrToPlannedYear as decimal   {&nodeType} column-label "Actual thru &3-&2!to Year Plan" format "(>>>,>>>,>>9)%"
  /* used for the browse only */
  field name                             as character {&nodeType} column-label "Agent Name"                     format "x(200)"
  field managerDesc                      as character {&nodeType} column-label "Manager"                        format "x(100)"
  field statDesc                         as character {&nodeType} column-label "Status"                         format "x(100)"
  field regionDesc                       as character {&nodeType} column-label "Region"                         format "x(50)"
  field stateName                        as character {&nodeType} column-label "State"                          format "x(50)"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

