&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/filefield.i
   Datastructure for an filefield
   @created 05.31.2022
   @author Sachin Anthwal
   Modifications:
   Date          Name           Description
   
 */

&IF DEFINED(tableAlias) = 0 &THEN
&SCOPED-DEFINE tableAlias filefield
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ENDIF

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "filefield"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

DEFINE TEMP-TABLE {&tableAlias} no-undo {&serializeName}
  FIELD agentFileID      AS  INTEGER    {&nodeType}
  FIELD templateID       AS  INTEGER    {&nodeType}
  FIELD templateName     AS  CHARACTER  {&nodeType}
  FIELD fieldName        AS  CHARACTER  {&nodeType} SERIALIZE-NAME "name"
  FIELD fieldValue       AS  CHARACTER  {&nodeType} SERIALIZE-NAME "value"
  FIELD dataType         AS  CHARACTER  {&nodeType} SERIALIZE-NAME "type"
  /*
  FIELD createdDate      AS  DATETIME   {&nodeType}
  FIELD createdBy        AS  CHARACTER  {&nodeType}
  FIELD lastModifiedDate AS  DATETIME   {&nodeType}
  FIELD lastModifiedBy   AS  CHARACTER  {&nodeType}
  */
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


