/* tt/agentfile.i
   Datastructure for an search in agent file
   @created 03.09.2023
   @author Sachin Anthwal
   Modifications:
   Date        Name      Description
   08/30/2024  AG        Modified to return the stateID
*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias searchagentfile
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ENDIF

&IF DEFINED(serializeName) = 0 &THEN
&SCOPED-DEFINE serializeName serialize-name "searchagentfile"
&ELSE
&SCOPED-DEFINE serializeName serialize-name "{&serializeName}"
&ENDIF

def temp-table {&tableAlias} NO-UNDO {&serializeName}

field agentFileID         as integer   {&nodeType}
field agentName           as character {&nodeType}
field stateID             as character {&nodeType} 
field fileID              as character {&nodeType} 
field fileNumber          as character {&nodeType}
field propertyAddress     as character {&nodeType} 
field parcelID            as character {&nodeType}
field book                as character {&nodeType}
field pageNumber          as character {&nodeType}
field Condominum          as character {&nodeType}
field subdivision         as character {&nodeType}  
field buyerDescription    as character {&nodeType}  
field sellerDescription   as character {&nodeType}
field percentMatch        as decimal   serialize-hidden {&nodeType} 
field ipercentMatch       as integer   serialize-name "matchPercentage" {&nodeType}

field productInfo         as character {&nodeType}
.


