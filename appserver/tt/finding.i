&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : finding.i
    Purpose     : Finding datastructure
    Author(s)   : Rahul
    Created     : 7.11.2017
    Modification:
    Date          Name      Description
    07/29/2022    Shefali   Task #: 95509 :Implement clob framework changes            
  ----------------------------------------------------------------------*/
  
&if defined(tableAlias) = 0 &THEN
&scoped-define tableAlias finding
&endif

&IF DEFINED(nodeName) = 0 &THEN
&SCOPED-DEFINE nodeName XML-NODE-NAME "finding"
&ELSE
&SCOPED-DEFINE nodeName XML-NODE-NAME "{&nodeName}"
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ELSE
&SCOPED-DEFINE nodeType XML-NODE-TYPE "{&nodeType}"
&ENDIF


define temp-table {&tableAlias} no-undo {&nodeName}
  field findingID      as integer   {&nodeType}
  field entity         as character {&nodeType}
  field entityID       as character {&nodeType}
  field entityName     as character {&nodeType} 
  field source         as character {&nodeType}
  field sourceID       as character {&nodeType}
  field description    as clob      {&nodeType}
  field sourceQuestion as character {&nodeType} 
  field severity       as integer   {&nodeType}
  field severityDesc   as character {&nodeType}
  field comments       as character {&nodeType}
  field stat           as character {&nodeType}
  field uid            as character {&nodeType}
  field createdDate    as datetime  {&nodeType}
  field closedDate     as datetime  {&nodeType}
  field refType        as character {&nodeType}
  field refID          as character {&nodeType}
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


