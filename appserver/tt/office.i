&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/arcoffice.i
   Office datastructure
   Author(s) B.Johnson
   Created 10.1.2015
 */

&if defined(tableAlias) = 0 &THEN
&scoped-define tableAlias office
&ENDIF

&if defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "office"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

def temp-table {&tableAlias} no-undo {&serializeName}
  field officeId   as integer   {&nodeType} column-label "Office ID"   format ">>>>9"
  field officeName as character {&nodeType} column-label "Office Name" format "x(200)"
  field agentId    as character {&nodeType} column-label "Agent ID"    format "x(20)"
  field agentName  as character {&nodeType} column-label "Agent Name"  format "x(200)"
  field stateID    as character {&nodeType} 
  field stateDesc  as character {&nodeType} column-label "Oper. State" format "x(50)"
  field stat       as character {&nodeType} column-label "Status"      format "x(20)"
  field addr1      as character {&nodeType} serialize-hidden
  field addr2      as character {&nodeType} serialize-hidden
  field fullAddr   as character {&nodeType} column-label "Address"     format "x(500)"
  field city       as character {&nodeType} column-label "City"        format "x(500)"
  field state      as character {&nodeType} column-label "State"       format "x(500)"
  /*field county     as character {&nodeType} column-label "County"      format "x(100)"*/
  field zipcode    as character {&nodeType} column-label "Zip Code"    format "x(500)"
  field phone      as character {&nodeType} column-label "Phone"       format "x(500)"
  field fax        as character {&nodeType} column-label "Fax"         format "x(500)"
  field website    as character {&nodeType} column-label "Website"     format "x(500)"
  field email      as character {&nodeType} column-label "Email"       format "x(500)"
  field notes      as character {&nodeType} column-label "Notes"       format "x(4000)"
  
  /* used for tempoffice */
  field name       as character {&nodeType} serialize-hidden
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


