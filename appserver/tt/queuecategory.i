/*------------------------------------------------------------------------
    File        : queuecategory.i
    Purpose     : queuecategory datastructure
    Author(s)   : Shefali
    Created     : 05.02.2023
    Modification:
    Date          Name      Description
    01/12/2024    SB        Modified to add isSecure field in definition          
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias queuecategory
&ENDIF


&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ENDIF

&IF DEFINED(serializeName) = 0 &THEN
&SCOPED-DEFINE serializeName serialize-name "queuecategory"
&ELSE
&SCOPED-DEFINE serializeName serialize-name "{&serializeName}"
&ENDIF

def temp-table {&tableAlias} no-undo {&serializeName}
field category          as character {&nodeType}
field description       as character {&nodeType}
field active            as logical   {&nodeType}
field isSecure          as logical   {&nodeType}
.

