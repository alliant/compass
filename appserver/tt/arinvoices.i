&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 arinvoices.i
 arinvoices data structure
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias arinvoices
&ENDIF
 
define temp-table {&tableAlias}
  field artranID                 as integer    format "->>>>>>>>9"
  field batchID                  as integer    column-label "Batch ID"         format " >>>>>>>>>9  "
  field periodID                 as integer    column-label "Period ID"        format "999999"
  field invoiceID                as character
  field reference                as character  
  field type                     as character
  field yearID                   as integer    column-label "Year ID"          format "9999"
  field agentID                  as character  column-label "Agent ID"         format "x(20)"
  field stateID                  as character  column-label "State ID"         
  field createDate               as datetime   column-label "Create!Date"      format "99/99/9999"
  field receivedDate             as datetime   column-label "Received!Date"    format "99/99/9999"
  field liabilityDelta           as decimal    column-label "Liability!Delta"  format "$->>,>>>,>>9.99"  
  field grossPremiumDelta        as decimal    column-label "Gross!Delta"      format "$->>,>>>,>>9.99"
  field netPremiumDelta          as decimal    column-label "Net!Delta"        format "$->>,>>>,>>9.99"
  field retainedPremiumDelta     as decimal    column-label "Retained!Delta"   format "$->>,>>>,>>9.99"
  field fileCount                as integer    column-label "File!Count"       format ">,>>9"
  field policyCount              as integer    column-label "Policy!Count"     format ">,>>9"
  field endorsementCount         as integer    column-label "Endorse!Count"    format ">,>>9"
  field cplCount                 as integer    column-label "CPL!Count"        format ">,>>9"
  field agentName                as character  column-label "Agent Name"       format "x(200)"
  field agentStat                as character  serialize-hidden
  field agentManager             as character  serialize-hidden
  field invoiceDate              as datetime   column-label "Invoice!Date"     format "99/99/9999"
  field tranDate                 as datetime   column-label "Transaction!Date"
  field posted                   as logical    
  field selectRecord             as logical  /* client side field */ 
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 16.67
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


