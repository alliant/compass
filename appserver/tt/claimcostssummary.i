&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/claimcostssummary.i
   datastructure for a CLAIM COSTS SUMMARY report
   Created John Oliver
   Date 06.24.2020
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias claimcostssummary
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "element"
&ENDIF

/* Should recoupments be against the Loss or LAE in accounting terms?
 */

define temp-table {&tableAlias}
  field agentID      as character {&nodeType} column-label "Agent ID"                format "x(10)"
  field period       as integer   {&nodeType} serialize-hidden                       
  field startDate    as datetime  {&nodeType} column-label "Start Date"              format "99/99/99"
  field endDate      as datetime  {&nodeType} column-label "End Date"                format "99/99/99"
  field netPremium   as decimal   {&nodeType} column-label "Net Premium"             format ">>>,>>>,>>9.99"
  field claimCosts   as decimal   {&nodeType} column-label "Claim Costs"             format ">>>,>>>,>>9.99"
  field costRatio    as decimal   {&nodeType} column-label "Cost Ratio"              format "9.99 %"
  field claimCostsAE as decimal   {&nodeType} column-label "Claim Costs!Agent Error" format ">>>,>>>,>>9.99"
  field costRatioAE  as decimal   {&nodeType} column-label "Cost Ratio!Agent Error"  format "9.99 %"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


