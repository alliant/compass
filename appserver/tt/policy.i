&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 policy.i
 POLICY data structure
 @author D.Sinclair
 @created 5.30.2012
 @version 1.12.2020 D.Sinclair Added 4 description fields + agent's name to avoid requiring reference tables on client
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias policy
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

DEF TEMP-TABLE {&tableAlias}
  FIELD policyID              AS INTEGER   {&nodeType}
  FIELD stat                  AS CHARACTER {&nodeType} /* (I)ssued, (P)rocessed, (V)oided */
  FIELD agentID               AS CHARACTER {&nodeType}
  FIELD companyID             AS CHARACTER {&nodeType}
  FIELD stateID               AS CHARACTER {&nodeType}
  FIELD fileNumber            AS CHARACTER {&nodeType}
  FIELD issueDate             as datetime      {&nodeType}
  FIELD voidDate              as datetime      {&nodeType}
  FIELD invoiceDate           as datetime      {&nodeType} /* Date of last invoicing */
  FIELD periodID              AS INTEGER   {&nodeType} /* Period used for revenue/SPR recognition (after set, it doesn't change) */
  FIELD reprocessed           AS LOGICAL   {&nodeType} /* Default = false; True means it was reprocessed/invoiced */
  
  /* Suggested values when policy issued by the Agent */
  FIELD issuedFormID          AS CHARACTER {&nodeType}
  FIELD issuedLiabilityAmount AS DECIMAL   {&nodeType}
  FIELD issuedGrossPremium    AS DECIMAL   {&nodeType}
  FIELD issuedResidential     AS LOGICAL   {&nodeType}
  FIELD issuedEffDate         as datetime      {&nodeType}
  
  /* Current values (from processing) */
  FIELD formID                AS CHARACTER {&nodeType}
  FIELD statcode              AS CHARACTER {&nodeType}
  FIELD effDate               as datetime      {&nodeType}
  FIELD liabilityAmount       AS DECIMAL   {&nodeType}
  FIELD grossPremium          AS DECIMAL   {&nodeType}
  FIELD netPremium            AS DECIMAL   {&nodeType}
  FIELD retentionPremium      AS DECIMAL   {&nodeType}
  FIELD residential           AS LOGICAL   {&nodeType}
  FIELD countyID              AS CHARACTER {&nodeType}

  FIELD reportDate            as datetime      {&nodeType}
  FIELD paidDate              as datetime      {&nodeType}
  FIELD addr1                 AS CHARACTER {&nodeType}
  FIELD addr2                 AS CHARACTER {&nodeType}
  FIELD addr3                 AS CHARACTER {&nodeType}
  FIELD addr4                 AS CHARACTER {&nodeType}
  FIELD city                  AS CHARACTER {&nodeType}
  FIELD county                AS CHARACTER {&nodeType}
  FIELD state                 AS CHARACTER {&nodeType}
  FIELD zip                   AS CHARACTER {&nodeType}

  FIELD trxID                 AS CHARACTER {&nodeType}
  FIELD trxDate               as datetime      {&nodeType}

  field agentname             as character {&nodetype}
  field descStat              as character {&nodeType}
  field descFormID            as character {&nodeType}
  field descStatcode          as character {&nodeType}
  field descCountyID          as character {&nodeType}
 
  INDEX policyID is primary unique
    policyID
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 16.67
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


