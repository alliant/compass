&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@name findingreport.i
@description finding report datastructure
@author Anjly Chanana
@created 07/06/2017

@modified
Date         Name   Comments
29/07/2022    SD    Task#95509: Modified to support clob related changes
------------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias ttqarfinding
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ENDIF

&IF DEFINED(nodeName) = 0 &THEN
&SCOPED-DEFINE nodeName XML-NODE-NAME "ttqarfinding"
&ELSE
&SCOPED-DEFINE nodeName XML-NODE-NAME "{&nodeName}"
&ENDIF

def temp-table {&tableAlias} no-undo {&nodeName}
 field qarID          as int  {&nodeType}
 field agentID        as char {&nodeType}
 field name           as char   {&nodeType}
 field state          as char  {&nodeType}
 field auditStartDate as datetime  {&nodeType}
 field auditor        as char  {&nodeType}
 field questionID     as char  {&nodeType}
 field priority       as int  {&nodeType}
 field sectionID      as int   {&nodeType}
 field description    as char  {&nodeType}
 field comments       as clob   {&nodeType}
 field files          as char  {&nodeType}
 field accounts       as char  {&nodeType}
 field reference      as char   {&nodeType}
 field auditType      as char   {&nodeType}
 field errType        as char   {&nodeType}
 field score          as int  {&nodeType}
 field grade          as int  {&nodeType}
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 6.33
         WIDTH              = 54.6.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


