/* tt/filedetail.i
   Datastructure for an filedetail
   @created 05.01.2024
   @author S Chandu 
   Modifications:
   Date          Name           Description
  07/12/2024    S Chandu       Added appliedBy and accumbalance fields.
  11/26/2204    S Chandu       Modified to add new field name 'referenceID' in
                               place of 'arTranID'.  
  12/10/2024    S Chandu       Added 'artranID' fields.
 */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias filedetail
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "{&tableAlias}"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} {&serializeName}
  field agentfileID	    as  integer    {&nodeType}
  field fileInvID	    as  integer    {&nodeType}
  field trxdetail   	as  character  {&nodeType}
  field description   	as  character  {&nodeType}
  field type            as  character  {&nodeType}
  field amount	        as  decimal    {&nodeType}
  field artranID        as  integer    {&nodeType}
  field referenceID	    as  integer    {&nodeType}
  field tranDate        as  date       {&nodeType}
  field checkNumber     as  character  {&nodeType}
  field appliedBy       as  character  {&nodeType}
  field appliedDate     as  date       {&nodeType}
  .
