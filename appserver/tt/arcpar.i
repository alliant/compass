&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/par.i
   PAR datastructure
   Author(s) John Oliver
   Created 2021.12.09
   @Modified :
   Date        Name          Comments 
   08-01-2022  S Chandu      added xmlNodename
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias arcpar
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

&if defined(serializename) = 0 &then
&scoped-define serializename serialize-name "arcpar"
&else
&scoped-define serializename serialize-name "{&serializename}"
&endif


define temp-table {&tableAlias} no-undo {&serializename} 
  field parID                    as integer   {&nodeType}
  field officeID                 as integer   {&nodeType}
  field officeName               as character {&nodeType}
  field uid                      as character {&nodeType}
  field stateID                  as character {&nodeType}
  field stateName                as character {&nodeType}
  field agentID                  as character {&nodeType}
  field agentName                as character {&nodeType}
  field fileNumber               as character {&nodeType}
  field ownersPolicy             as logical   {&nodeType}
  field ownersPolicyAmount       as decimal   {&nodeType}
  field loanPolicy               as logical   {&nodeType}
  field loanPolicyAmount         as decimal   {&nodeType}
  field otherPolicy              as logical   {&nodeType}
  field otherPolicyInfo          as character {&nodeType}
  field propertyCounties         as character {&nodeType}
  field propertyState            as character {&nodeType}
  field propertyType             as character {&nodeType}
  field propertyCondition        as character {&nodeType}
  field propertyAccess           as character {&nodeType}
  field accessBasis              as character {&nodeType}
  field easementExamined         as character {&nodeType}
  field propertyTitle            as character {&nodeType}
  field recentConstruction       as character {&nodeType}
  field improvementsComplete     as character {&nodeType}
  field improvementsCompleteDate as datetime  {&nodeType}
  field constructionContemplated as character {&nodeType}
  field risks                    as character {&nodeType}
  field risksOther               as character {&nodeType}
  field endorsements             as character {&nodeType}
  
  /* for the repository files */
  field entity                   as character {&nodeType}
  field entityID                 as character {&nodeType}
  field fileIDList               as character {&nodeType}
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


