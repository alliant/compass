/* tt/trainingfeedback.i
   Datastructure for trainingfeedback
   @created 01.08.2025
   @author SB
   Modifications:
   Date          Name           Description
   
 */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias  trainingfeedback
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "trainingfeedback"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo  {&serializeName}
  field feedbackID         as  integer    {&nodeType}
  field participantID      as  integer    {&nodeType}
  field trainingID         as  integer    {&nodeType}
  field score              as  integer    {&nodeType}
  field feedback           as  character  {&nodeType}
  field createdDate        as  datetime   {&nodeType}
  .


