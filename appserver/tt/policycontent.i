/* tt/policycontent.i
   Datastructure for an policy content
   @created 04.18.2024
   @author K.R
   Modifications:
   Date          Name           Description
   09/05/2024    S Chandu      Modifed datatype character 'longlegal' to clob fields.
      
 */

&if defined(tableAlias) = 0 &then
&SCOPED-DEFINE tableAlias policycontent
&ENDIF

&if defined(nodeType) = 0 &then
&SCOPED-DEFINE nodeType xml-node-type "attribute"
&ENDIF

&IF DEFINED(serializeName) = 0 &THEN
&SCOPED-DEFINE serializeName serialize-name "policycontent"
&ELSE
&SCOPED-DEFINE serializeName serialize-name "{&serializeName}"
&ENDIF

define temp-table {&tableAlias} no-undo {&serializeName}
&if defined(includeextra) &then
  field key                    as integer          {&nodeType} serialize-hidden
  field ownerNames             as character        {&nodeType}
  field titleVested            as character        {&nodeType}
  field longLegal              as clob             {&nodeType}
  field seq                    as integer          {&nodeType}
  field policyType             as character        {&nodeType}
  field insuredName            as character        {&nodeType}  
  field mortgageAndAssignments as character        {&nodeType}
&else
  field key                    as integer          {&nodeType} serialize-hidden
  field ownerNames             as character        {&nodeType}
  field titleVested            as character        {&nodeType}
  field longLegal              as clob             {&nodeType}
  field seq                    as integer          {&nodeType}
  field policyType             as character        {&nodeType}
  field insuredName            as character        {&nodeType}   
&endif
  .
