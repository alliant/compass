&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12v
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/arinv.i
   Generic datastructure for A/R expected receipts
   D.Sinclair 4.23.2015
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias arinv
&ENDIF

def temp-table {&tableAlias}
  field arinvID as int

  field stat as char          /* O)pen, C)ompleted, V)oided */

  field refType as char
  field refID as char
  field refSeq as int
  field refCategory as char

  field name as char
  field addr1 as char
  field addr2 as char
  field city as char
  field stateID as char
  field zipcode as char
  field contactName as char
  field contactPhone as char
  field contactEmail as char

  field invoiceNumber as char
  field dateRequested as datetime
  field dueDate as datetime
  field requestedAmount as deci
  field waivedAmount as deci
  field uid as character
  field username as char
  field notes as char
  field hasDocument as logical /* flag to tell if the invoice has a document in sharefile */
  field documentFilename as char
 
  field isFileCreated as logical
  field isFileDeleted as logical
  field isFileEdited as logical
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


