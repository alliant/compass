/*------------------------------------------------------------------------
File        : tt/holdfilenote.i
Purpose     :

Syntax      :

Description :

Author(s)   : Shefali
Created     : 06-03-2022
Notes       :

Modification:
Date          Name       Description
----------------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias holdfilenote
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif


&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "holdfilenote"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif
   
define temp-table {&tableAlias} no-undo {&serializeName}
  field note    as character {&nodeType}
  .
