/* tt/filecontent.i
   Datastructure for an filecontent
   @created 05.31.2022
   @author Sachin Anthwal
   Modifications:
   Date          Name           Description
   09/05/2024    S Chandu      Modifed datatype character 'content' to clob fields.
   
 */

&if defined(tableAlias) = 0 &then
&SCOPED-DEFINE tableAlias filecontent
&ENDIF

&if defined(nodeType) = 0 &then
&SCOPED-DEFINE nodeType xml-node-type "attribute"
&ENDIF

&IF DEFINED(serializeName) = 0 &THEN
&SCOPED-DEFINE serializeName serialize-name "filecontent"
&ELSE
&SCOPED-DEFINE serializeName serialize-name "{&serializeName}"
&ENDIF

define temp-table {&tableAlias} no-undo {&serializeName}
&if defined(getAgentFile) &then
  field agentFileID		 as  integer    {&nodeType} 
  field seq	        	 as  integer    {&nodeType}
  field content	    	 as  clob       {&nodeType}
  field indent	    	 as  integer    {&nodeType}
  field isNote	    	 as  logical    {&nodeType}
  field appliesTo		 as  character  {&nodeType}  
&elseif defined(updateFile) &then
  field agentFileID		 as  integer    {&nodeType} 
  field seq	        	 as  integer    {&nodeType}
  field content	    	 as  clob       {&nodeType}
  field indent	    	 as  integer    {&nodeType}
  field isNote	    	 as  logical    {&nodeType}
  field appliesTo		 as  character  {&nodeType}  
&else
  field agentFileID		 as  integer    {&nodeType} 
  field seq	        	 as  integer    {&nodeType}
  field content	    	 as  clob       {&nodeType}
  field indent	    	 as  integer    {&nodeType}
  field isNote	    	 as  logical    {&nodeType}
  field appliesTo		 as  character  {&nodeType}
  field type	    	 as  character  {&nodeType}
  field CreatedDate	     as  datetime   {&nodeType}
  field CreatedBy	     as  character  {&nodeType}
  field lastModifiedDate as  datetime   {&nodeType}
  field lastModifiedBy	 as  character  {&nodeType}
&endif
  .
