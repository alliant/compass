&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 reinsurance.i
 Temp-table definition for Reinsurance Report (wops07-r.w)
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias reinsurance
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF
 
def temp-table {&tableAlias}
 field agentID       as character {&nodeType}
 field name          as character {&nodeType}
 field stateID       as character {&nodeType}
 field fileNumber    as character {&nodeType}
 field formID        as character {&nodeType}
 field formCode      as character {&nodeType}
 field policyID      as integer   {&nodeType}
 field effDate       as date      {&nodeType}
 field fullLiability as decimal   {&nodeType}
 field amountCeded   as decimal   {&nodeType}
 field fullPremium   as decimal   {&nodeType}
 field netPremium    as decimal   {&nodeType}
 index pu1 is primary unique agentID fileNumber policyID
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


