&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* auditfile.i
   D.Sinclair 10.20.2015 Added qarID
   Yoke Sam C. 06/26/2017 Change "state" to "stateID";
                          change "county" to "countyID";
                          change type of "ownerPolicy"  from char to int;
                          change type of "lender1Policy" from char to int;
                          change type of "lender2Policy" from char to int;
                          added statutoryGapLimit.
                          
@modified
Date         Name   Comments
29/07/2022    SD    Task#95509: Modified to support clob related changes
   ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias agentFile
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ENDIF

&IF DEFINED(nodeName) = 0 &THEN
&SCOPED-DEFINE nodeName XML-NODE-NAME "agentFile"
&ELSE
&SCOPED-DEFINE nodeName XML-NODE-NAME "{&nodeName}"
&ENDIF
   
def temp-table {&tableAlias} {&nodeName}
  field qarID as int                       {&nodeType}
  field fileID as int                      {&nodeType}
  field answered as int                    {&nodeType}
  field fileNumber as char format "x(30)"  {&nodeType}
  field stateID as char                    {&nodeType}
  field countyID as char                   {&nodeType}
  field ownerLiability as decimal          {&nodeType}
  field ownerPolicy as int                 {&nodeType}
  field lender1Liability as decimal        {&nodeType}
  field lender1Policy as int               {&nodeType}
  field lender2Liability as decimal        {&nodeType}
  field lender2Policy as int               {&nodeType}
  field premiumStatement as decimal        {&nodeType}
  field rate as decimal                    {&nodeType}
  field delta as decimal                   {&nodeType}
  field originalDate as datetime           {&nodeType}
  field preliminaryDate as datetime        {&nodeType}
  field updateDate as datetime             {&nodeType}
  field finalDate as datetime              {&nodeType}
  field closingDate as datetime            {&nodeType}
  field fundingDate as datetime            {&nodeType}
  field recordingDate as datetime          {&nodeType}
  field statutoryGapLimit as int           {&nodeType}
  
  field ownerDeliveryDate as datetime       {&nodeType}
  field ownerDeliveryGapBusiness as int     {&nodeType}
  field ownerDeliveryGapCalendar as int     {&nodeType}
  field lender1DeliveryDate as datetime     {&nodeType}
  field lender1DeliveryGapBusiness as int   {&nodeType}
  field lender1DeliveryGapCalendar as int   {&nodeType}
  field lender2DeliveryDate as datetime     {&nodeType}
  field lender2DeliveryGapBusiness as int   {&nodeType}
  field lender2DeliveryGapCalendar as int   {&nodeType}

  field bringToDateGapCalendar as int    {&nodeType}
  field bringToDateGapBusiness as int    {&nodeType}
  
  field searchGapCalendar as int   {&nodeType}
  field searchGapBusiness as int   {&nodeType}
  field fundingGapCalendar as int  {&nodeType}
  field fundingGapBusiness as int  {&nodeType}

  field recordingGapCalendar as int   {&nodeType}
  field recordingGapBusiness as int   {&nodeType}

  index pi-file fileID ascending .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


