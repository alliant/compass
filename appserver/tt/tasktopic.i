/*------------------------------------------------------------------------
File        : tt/tasktopic.i
Purpose     :

Syntax      :

Description : temp-table for tasktopic
                                           
Author(s)   : Spandana
Created     : 04-13-2023
Notes       :
Modification:
Date          Name      Description
01/12/2024    SB        Modified to add isSecure field in definition
----------------------------------------------------------------------*/
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias tasktopic
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "tasktopic"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif
  
define temp-table {&tableAlias} no-undo {&serializeName}
  field topic           as character {&nodeType}    
  field description     as character {&nodeType}
  field active          as logical   {&nodeType}
  field isSecure        as logical   {&nodeType}
  .

