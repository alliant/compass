&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/cmpfulfillments.i
Purpose     :

Syntax      :

Description :

Author(s)   : Rahul Sharma
Created     : 05-11-2018
Notes       :

Modification:
Date          Name           Description
05/28/2018    Rahul Sharma   Added new fields.
06/21/2018    Rahul Sharma   Modified to update fields name.
06/22/2018    Rahul Sharma   Temp-table name changed from 
                             agentpersonrole to cmpfulfillments
06/28/2018    Rahul Sharma   Added new field qualReqID
07/04/2018    Rahul Sharma   Added new field comStat                                                     
07/09/2018    Rahul Sharma   Added new fields for Agent's NPN and Person's NPN
07/25/2018    Rahul Sharma   Added reviewDate,reviewBy,numValidDays & nextreviewDueDate         
11/13/2019    Rahul Sharma   Modified personid to fulfilledByID and personRoleID to roleID
08/28/2020    VJ             Added new field roleName            
----------------------------------------------------------------------*/


&if defined(tablealias) = 0 &then
&scoped-define tableAlias cmpfulfillments
&endif
   
define temp-table {&tableAlias}
  field entity              as character
  field entityID            as character
  field entityNPN           as character
  field entityName          as character
  field fulfillByID         as character
  field personNPN           as character
  field personName          as character
  field StateId             as character
  field role                as character
  field requirementID       as integer
  field stateReqQualID      as integer
  field qualReqID           as integer
  field qualificationID     as integer
  field qualificationNumber as character
  field roleID              as character
  field qualification       as character
  field stat                as character
  field effectiveDate       as datetime 
  field expirationDate      as datetime
  field authorizedBy        as character 
  field notes               as character
  field comStat             as character
  field reviewDate          as datetime
  field reviewBy            as character
  field nextreviewDueDate   as datetime
  field numValidDays        as integer
  field roleName            as character
  field lSelect             as logical.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


