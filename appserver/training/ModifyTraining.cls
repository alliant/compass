/*------------------------------------------------------------------------
@file ModifyTraining.cls
@action trainingModify
@description  Modify the existing training record for trainingID.
@param  trainingID;int
@throws 3001;Invalid trainingID
@throws 3067;TrainingID does not exist
@throws 3005;Data Parse failed
@throws 3000;Traning modify failed 
@author S Chandu
@version 1.0
@created 12/09/2024
@Modification:
Date        Name          Comments 
----------------------------------------------------------------------*/

class training.ModifyTraining inherits framework.ActionBase:
    
  {tt/training.i &tableAlias="ttTraining" }

  constructor ModifyTraining ():
    super().
  end constructor.

  destructor public ModifyTraining ():
  end destructor.  

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable dsHandle      as handle    no-undo.
    define variable std-ch        as character no-undo.
    define variable piTrainingID  as integer   no-undo.
    define variable lSuccess      as logical   no-undo.
    
    &scoped-define msg-add + "^" +
           
    pRequest:getParameter("TrainingID", output piTrainingID).

    /* validate traningID */
    if piTrainingID = ? or piTrainingID = 0
     then
      do:
        pResponse:fault("3001", "TrainingID").
        return. 
      end.

    /* validate TrainingID */
    if not can-find(first training where training.trainingID = piTrainingID)
     then
      do:  
        pResponse:fault("3067", "Training" {&msg-add} "ID" {&msg-add} string(pitrainingID)).
        return.
      end.

      
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttTraining:handle).

    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > "" 
     then
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.
    
    lSuccess = false.
    trxBlock:
    for first training exclusive-lock
      where training.trainingID = piTrainingID transaction
      on error undo trxBlock, leave trxBlock:
    
      /* updating training record*/
      for first ttTraining no-lock:
    
         /* as of now we are not updating fields trainingURL,trainingQR,coourseID,agentid */ 
        assign
            training.timeStart          = ttTraining.timeStart
            training.timeEnd            = ttTraining.timeEnd
            training.timezone           = ttTraining.timezone
            training.officeID           = ttTraining.officeID
            training.addr1              = ttTraining.addr1
            training.addr2              = ttTraining.addr2
            training.addr3              = ttTraining.addr3
            training.addr4              = ttTraining.addr4
            training.city               = ttTraining.city
            training.state              = ttTraining.state
            training.zip                = ttTraining.zip
            training.instructorEmail    = ttTraining.instructorEmail
            training.instructorName     = ttTraining.instructorname
            training.instructorReminder = ttTraining.instructorReminder
            training.contactEmail       = ttTraining.contactEmail
            training.contactName        = ttTraining.contactName
            training.contactPhone       = ttTraining.contactPhone
            training.contactReminder    = ttTraining.contactReminder
            training.collectPayment     = ttTraining.collectPayment
            training.requestorEmail     = ttTraining.requestorEmail
            training.requestorName      = ttTraining.requestorName
            training.notes              = ttTraining.notes
            training.lastModifiedDate   = now
            training.lastModifiedBy     = pRequest:uid
            .          
            
      end. /* for first ttTraining */
       
      lSuccess = true.

      empty temp-table ttTraining.
      buffer-copy training to ttTraining.
      assign
          ttTraining.statDescription = getTrainingStatusDescription(ttTraining.stat)
          .

      validate training.
      release training.

    end. /* for first training*/
  
    if not lSuccess
     then 
      do:
        presponse:fault("3000", "Training update").
        return.
      end. /* if not lSuccess */
    
    if lSuccess
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
    
    pResponse:success("2000", "Training update").

  end method.
      
  method private char getTrainingStatusDescription ( cTrainingStatus as char):

    if cTrainingStatus = 'P' 
     then
      return "Planned".
    else if cTrainingStatus = 'X' 
     then
      return "Cancelled".
    else if cTrainingStatus = 'S' 
     then
      return "Scheduled".
    else if cTrainingStatus = 'C' 
     then
      return "Completed".
    else
     return cTrainingStatus.

  end method.

end class.


