/*------------------------------------------------------------------------
@file RequestTraining.cls
@action trainingRequest
@description Creates a new Training record.
@throws 3005;Data Parsing Failed 
@throws 3067;AgentID does not exist
@throws 3067;CourseID does not exist
@throws 3005;Invalid Email Address
@throws 3005;Invlaid phone number
@returns success;2002;Request Training has been created.
@author S Chandu
@version 1.0
@created 11/25/2024
Modification:

----------------------------------------------------------------------*/

class training.RequestTraining inherits framework.ActionBase:
    
  {tt/training.i &tableAlias="ttTraining" }

  constructor RequestTraining ():
    super().
  end constructor.

  destructor public RequestTraining ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable dsHandle      as handle    no-undo.
    define variable opiTrainingID as integer   no-undo.
    define variable oplSuccess    as logical   no-undo.
    define variable opcMsg        as character no-undo.
    define variable std-ch        as character no-undo.
    define variable lerror        as logical   no-undo.
    define variable cerrmsg       as character no-undo.
    define variable cvalidNumber  as character no-undo.

    &scoped-define msg-add + "^" +
      
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttTraining:handle).

    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > "" 
     then
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.
   

    for first ttTraining:

        /* validate agentID */
      if not can-find(first agent where agent.agentID = ttTraining.agentID) 
      then 
       do:
         pResponse:fault('3067', 'agent' {&msg-add} 'ID' {&msg-add} string(ttTraining.agentID)).
         return.
       end.

       /* validate courseID */
      if not can-find(first course where course.courseID = ttTraining.courseID) 
      then 
       do:
         pResponse:fault("3067", "course" {&msg-add} "ID" {&msg-add} string(ttTraining.courseID)).
         return.
       end.

       /* validate email */
      if (ttTraining.contactEmail <> "" and ttTraining.contactEmail <> ? )  and  not validEmailAddr(ttTraining.contactEmail)
       then
        do:
	      pResponse:fault("3005","Invalid ConatctEmail Address.").
	      return.
	    end.

      /* validate phone number */
      if (ttTraining.contactPhone <> "" and ttTraining.contactPhone <> ? )
       then
        do:
          run util/validatecontactnumber-p.p(input ttTraining.contactPhone,output cvalidNumber,output lerror,output cerrmsg).
          if lerror
           then
            do:
        	  pResponse:fault("3005", cerrmsg).
        	  return.
        	end.
        end.

    end.

    run training/createtraining-p.p(input table ttTraining,
                                    input pRequest:uid,
                                    output opiTrainingID,
                                    output oplSuccess,
                                    output opcMsg).
    
    if not oplSuccess
     then
      do:
         pResponse:fault("3005", string(opcMsg)).
      end.

    empty temp-table ttTraining.
    for first training no-lock where training.trainingID = opiTrainingID:
      create ttTraining.
      buffer-copy training to ttTraining.
      assign
          ttTraining.statDescription = getTrainingStatusDescription(ttTraining.stat)
          .

    end.

    pResponse:success("2002", "Request Training").

    if opiTrainingID > 0 and opiTrainingID <> ?
     then 
      do:
         setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
         pResponse:CreateEvent("Training", string(opiTrainingID)).
      end.
             
  end method.
     
  method private char getTrainingStatusDescription ( cTrainingStatus as char):

    if cTrainingStatus = 'P' 
     then
      return "Planned".
    else if cTrainingStatus = 'X' 
     then
      return "Cancelled".
    else if cTrainingStatus = 'S' 
     then
      return "Scheduled".
    else if cTrainingStatus = 'C' 
     then
      return "Completed".
    else
     return cTrainingStatus.

  end method.

  method private logical  validEmailAddr(input pEmailAddr as character):

    def var tBadEmailChars as char init "/?<>\:*|~"~~~'~`!#$%^&~{~}[];," no-undo.
    def var i as int no-undo.
    def var tEmailOK as log no-undo init yes.

    pEmailAddr = trim(pEmailAddr).

    do i = 1 to length(pEmailAddr):
      if index(tBadEmailChars, substring(pEmailAddr,i,1)) > 0 then
       do:
         tEmailOK = no.
         leave.
       end.
    end.
  
    if tEmailOK then
    if num-entries(pEmailAddr, "@") <> 2 then
     tEmailOK = no.

    if tEmailOK then
    if num-entries(pEmailAddr, ".") < 2 then
     tEmailOK = no.

    if tEmailOK then
    if num-entries(pEmailAddr, " ") > 1 then
     tEmailOK = no.

    if tEmailOK then
    do i = 1 to num-entries(pEmailAddr, "@"):
      if entry(i, pEmailAddr, "@") = "" or entry(i, pEmailAddr, "@") = "?" then
       do:
         tEmailOK = no.
         leave.
       end.
    end.
  
    if tEmailOK then
    do i = 1 to num-entries(pEmailAddr, "."):
     if entry(i, pEmailAddr, ".") = "" or entry(i, pEmailAddr, ".") = "?" then
      do:
       tEmailOK = no.
       leave.
      end.
    end.
  
    RETURN tEmailOK.

   end method.

end class.


