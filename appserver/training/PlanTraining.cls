/*------------------------------------------------------------------------
@file PlanTraining.cls
@action trainingPlan
@description  Plans the existing training record for trainingID.
@param  trainingID;int
@throws 3001;Invalid trainingID
@throws 3067;TrainingID does not exist
@throws 3005;Completed Training cannot be Planned
@throws 3005;
@throws 3000;Traning planned failed 
@author S Chandu
@version 1.0
@created 12/09/2024
@Modification:
Date        Name          Comments 
----------------------------------------------------------------------*/

class training.PlanTraining inherits framework.ActionBase:
    
  {tt/training.i &tableAlias="ttTraining" }

  constructor PlanTraining ():
    super().
  end constructor.

  destructor public PlanTraining ():
  end destructor.  

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable dsHandle      as handle    no-undo.
    define variable std-ch        as character no-undo.
    define variable piTrainingID  as integer   no-undo.
    define variable lSuccess      as logical   no-undo.
    
    &scoped-define msg-add + "^" +
           
    pRequest:getParameter("TrainingID", output piTrainingID).

    /* validate traningID */
    if piTrainingID = ? or piTrainingID = 0
     then
      do:
        pResponse:fault("3001", "TrainingID").
        return. 
      end.

    /* validate TrainingID */
    if not can-find(first training where training.trainingID = piTrainingID)
     then
      do:  
        pResponse:fault("3067", "Training" {&msg-add} "ID" {&msg-add} string(pitrainingID)).
        return.
      end.

    /* completed tranining cannot be planned */
    if can-find(first training where training.trainingID = piTrainingID and training.stat = 'C')
     then
      do:  
        pResponse:fault("3005", "Completed Training cannot be Planned"). 
        return.
      end.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttTraining:handle).
    
    lSuccess = false.
    trxBlock:
    for first training exclusive-lock
      where training.trainingID = piTrainingID transaction
      on error undo trxBlock, leave trxBlock:
    
      assign
          training.stat             = "P" /* (P)lanned */
          training.lastModifiedDate = now
          training.lastModifiedBy   = pRequest:uid
          .          
             
      lSuccess = true.

      buffer-copy training to ttTraining.
      assign
          ttTraining.statDescription = getTrainingStatusDescription(ttTraining.stat)
          .

      validate training.
      release training.

    end. /* for first training*/
  
    if not lSuccess
     then 
      do:
        presponse:fault("3000", "Training plan").
        return.
      end. /* if not lSuccess */
    
    if lSuccess
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
    
    pResponse:success("2000", "Training plan").

  end method.
      
  method private char getTrainingStatusDescription ( cTrainingStatus as char):

    if cTrainingStatus = 'P' 
     then
      return "Planned".
    else if cTrainingStatus = 'X' 
     then
      return "Cancelled".
    else if cTrainingStatus = 'S' 
     then
      return "Scheduled".
    else if cTrainingStatus = 'C' 
     then
      return "Completed".
    else
     return cTrainingStatus.

  end method.

end class.


