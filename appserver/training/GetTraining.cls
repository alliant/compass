/*------------------------------------------------------------------------
@file GetTraining.cls
@action trainingGet
@description  Get the training Details.
@returns Success;3005;Training Get was successful
@author S Chandu
@version 1.0
@created 11/22/2024
Modification:
course
----------------------------------------------------------------------*/

class training.GetTraining inherits framework.ActionBase:
    
  {tt/training.i &tableAlias="ttTraining"}  
  {tt/participant.i &tableAlias="ttParticipant"}

  constructor GetTraining ():  
    super().
  end constructor.

  destructor public GetTraining ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable piTrainingID  as integer  no-undo.
    define variable dsHandle      as handle   no-undo.

    {lib/callsp-defs.i}
    {lib/std-def.i}

    pRequest:getParameter("TrainingID", output piTrainingID).

    /* validate traningID */
    if piTrainingID = ? or piTrainingID = 0
     then
      do:
        pResponse:fault("3001", "TrainingID").
        return. 
      end.

    /* validate TrainingID */
    if not can-find(first training where training.trainingID = piTrainingID)
     then
      do:  
        pResponse:fault("3067", "training" {&msg-add} "ID" {&msg-add} string(pitrainingID)).
        return.
      end.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttTraining:handle,
                         buffer ttParticipant:handle).
    dsHandle:add-relation(dsHandle:get-buffer-handle(1), dsHandle:get-buffer-handle(2), "trainingID,trainingID",?,yes,?,?).

    {lib\callsp.i &name=spGetTraining &load-into=ttTraining &params="input piTrainingID" &noResponse=true}
    
    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end. 

    /* participant table fetching records */
    for each participant where participant.trainingID = piTrainingID:

      create ttParticipant.
      buffer-copy participant to ttParticipant.
    end.
    for first ttTraining:
      assign
          ttTraining.statDescription = getTrainingStatusDescription(ttTraining.stat).
    end.
    
    if can-find(first ttTraining)
     then 
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).  
    
    pResponse:success("3005", "TrainingGet was successful"). 

  end method.
      
  method private char getTrainingStatusDescription ( cTrainingStatus as char):

   if cTrainingStatus = 'P' 
    then
     return "Planned".
   else if cTrainingStatus = 'X' 
    then
     return "Cancelled".
   else if cTrainingStatus = 'S' 
    then
     return "Scheduled".
   else if cTrainingStatus = 'C' 
    then
     return "Completed".
   else
    return cTrainingStatus.

  end method.

end class.




