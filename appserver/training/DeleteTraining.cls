/*------------------------------------------------------------------------
@name DeleteTraining.cls
@action trainingDelete
@description  Deletes training record form training table.
@param TraningID;int
@throws 3067;Training ID does not exist
@throws 3005;The Training cannot be deleted as it is in use
@throws 3000;delete training failed
@returns Success;2000
@author S Chandu
@version 1.0
@created 12.09.2024
@Modified :
Date        Name          Comments  

----------------------------------------------------------------------*/

class training.DeleteTraining inherits framework.ActionBase:
      
  constructor DeleteTraining ():
    super().
  end constructor.

  destructor public DeleteTraining ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
            
    /*parameter variables*/
    define variable piTrainingID  as integer   no-undo.
    define variable std-lo        as logical   no-undo.
    &scoped-define msg-add + "^" +
                
    pRequest:getParameter("TrainingID", output piTrainingID).

    /* validation when no parameter is supplied */
    if (piTrainingID = 0  or piTrainingID = ?)
     then 
      do:
        pResponse:fault("3001", "TrainingID").
        return.
      end.

     /* validate TrainingID */
    if not can-find(first training where training.trainingID = piTrainingID)
     then
      do:  
        pResponse:fault("3067", "Training" {&msg-add} "ID" {&msg-add} string(pitrainingID)).
        return.
      end.

    if can-find(first participant where participant.trainingID = piTrainingID)
     then
      do: 
        pResponse:fault("3005", "The Training cannot be deleted as it is in use").
        return.
      end.          
      
    std-lo = false.
    
    trxBlock:
    for first training exclusive-lock
      where training.trainingID = piTrainingID transaction
      on error undo trxBlock, leave trxBlock:
      
      delete training.
      release training.
      
      std-lo = true.
    end.
    
    if not std-lo
     then
      do: 
        pResponse:fault("3000", "Delete Training").
        return.
      end.
    
    pResponse:success("2000", "Delete Training").
    
  end method.
      
end class.


