/*------------------------------------------------------------------------
@file SearchTraining.Cls
@action trainingSearch
@description search records in training table according to the filter

@returns 

@success 2005;

@author K.R
@version 1.0
@created 12/19/2024
Modification:

----------------------------------------------------------------------*/

class training.SearchTraining inherits framework.ActionBase:
    
  {tt/training.i     &tableAlias="ttTraining"}


  constructor SearchTraining ():
    super().
  end constructor.

  destructor public SearchTraining ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable piCourseID        as integer    no-undo.
    define variable pcActive          as character  no-undo.
    define variable pcInstructorEmail as character  no-undo.
    define variable pcAgentID         as character  no-undo.

    define variable dsHandle       as handle     no-undo.

    {lib/callsp-defs.i}
    {lib/std-def.i}

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer tttraining:handle).

    pRequest:getParameter("CourseID",        output piCourseID).
    pRequest:getParameter("Active",          output pcActive).
    pRequest:getParameter("InstructorEmail", output pcInstructorEmail).
    pRequest:getParameter("AgentID",         output pcAgentID).

    if piCourseID = ? 
     then
      piCourseID = 0.

    if pcActive = ? 
     then
      pcActive = ''.

    if pcInstructorEmail = ? 
     then
      pcInstructorEmail = ''.

    if pcAgentID = ? 
     then
      pcAgentID = ''.

    {lib\callsp.i &name=spSearchTraining &load-into=ttTraining &params="input piCourseID, input pcActive, input pcInstructorEmail, input pcAgentID" &noResponse=true}
    
    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end.

    for each ttTraining fields (trainingID) no-lock:
      std-in = std-in + 1.
    end.

    if can-find(first ttTraining)
     then SetContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2005", string(std-in) {&msg-add} "Training").

  end method.
      
end class.


