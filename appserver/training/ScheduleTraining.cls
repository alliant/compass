/*------------------------------------------------------------------------
@file ScheduleTraining.cls
@action trainingSchedule
@description  Schedules the existing training record for trainingID.
@param  trainingID;int
@throws 3001;Invalid trainingID
@throws 3067;TrainingID does not exist
@throws 3005;
@throws 3005;
@throws 3000;Traning scheduled failed 
@author S Chandu
@version 1.0
@created 12/12/2024
@Modification:
Date        Name          Comments 
----------------------------------------------------------------------*/

class training.ScheduleTraining inherits framework.ActionBase:
    
  {tt/training.i &tableAlias="ttTraining" }

  constructor ScheduleTraining ():
    super().
  end constructor.

  destructor public ScheduleTraining ():
  end destructor.  

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable dsHandle      as handle    no-undo.
    define variable std-ch        as character no-undo.
    define variable piTrainingID  as integer   no-undo.
    define variable pcInstructor  as character no-undo.
    define variable pcCourseName  as character no-undo.
    define variable lSuccess      as logical   no-undo.
    
    &scoped-define msg-add + "^" +
           
    pRequest:getParameter("TrainingID", output piTrainingID).
    pRequest:getParameter("Instructor", output pcInstructor).
    pRequest:getParameter("CourseName", output pcCourseName).

    /* validate traningID */
    if piTrainingID = ? or piTrainingID = 0
     then
      do:
        pResponse:fault("3001", "TrainingID").
        return. 
      end.

    /* validate TrainingID */
    if not can-find(first training where training.trainingID = piTrainingID)
     then
      do:  
        pResponse:fault("3067", "Training" {&msg-add} "ID" {&msg-add} string(pitrainingID)).
        return.
      end.

   /*  /* validate Course Instructor */
    if not can-find(first course where course.instructor = pcInstructor)
     then
      do:
        pResponse:fault("3005", "Course Instructor does not exist").
        return.        
      end.

      /* validate Course Name */
    if not can-find(first course where course.name = pcCourseName)
     then
      do:
        pResponse:fault("3005", "Course Name does not exist").
        return.        
      end. */

     /* only a planned can be schedule */
    if not can-find(first training where training.trainingID = piTrainingID and training.stat = 'P')
     then
      do:  
        pResponse:fault("3005", "Only the Planned Training can be Scheduled"). 
        return.
      end.
      
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttTraining:handle).
    
    lSuccess = false.
    trxBlock:
    for first training exclusive-lock
      where training.trainingID = piTrainingID transaction
      on error undo trxBlock, leave trxBlock:
    
      assign
          training.stat             = "S" /* (S)chedule */
          training.lastModifiedDate = now
          training.lastModifiedBy   = pRequest:uid
          .          
             
      lSuccess = true.

      buffer-copy training to ttTraining.
      assign
          ttTraining.statDescription = getTrainingStatusDescription(ttTraining.stat)
          .
      validate training.
      release training.

    end. /* for first training*/
  
    if not lSuccess
     then 
      do:
        presponse:fault("3000", "Training Schedule").
        return.
      end. /* if not lSuccess */
    
    if lSuccess
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
    
    pResponse:success("2000", "Training Schedule").

  end method.
      
  method private char getTrainingStatusDescription ( cTrainingStatus as char):

    if cTrainingStatus = 'P' 
     then
      return "Planned".
    else if cTrainingStatus = 'X' 
     then
      return "Cancelled".
    else if cTrainingStatus = 'S' 
     then
      return "Scheduled".
    else if cTrainingStatus = 'C' 
     then
      return "Completed".
    else
     return cTrainingStatus.

  end method.

end class.


