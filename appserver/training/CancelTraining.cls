/*------------------------------------------------------------------------
@file CancelTraining.cls
@action trainingCancel
@description  Cancels the existing training record for trainingID.
@param  trainingID;int
@throws 3001;Invalid trainingID
@throws 3067;TrainingID does not exist
@throws 3005;Completed Training cannot be Cancelled
@throws 3005;
@throws 3000;Traning Cancel failed 
@author S Chandu
@version 1.0
@created 12/13/2024
@Modification:
Date        Name          Comments 
----------------------------------------------------------------------*/

class training.CancelTraining inherits framework.ActionBase:
    
  {tt/training.i &tableAlias="ttTraining" }

  constructor CancelTraining ():
    super().
  end constructor.

  destructor public CancelTraining ():
  end destructor.  

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable dsHandle      as handle    no-undo.
    define variable std-ch        as character no-undo.
    define variable piTrainingID  as integer   no-undo.
    define variable pcReason      as character no-undo.
    define variable lSuccess      as logical   no-undo.
    
    &scoped-define msg-add + "^" +
           
    pRequest:getParameter("TrainingID", output piTrainingID).
    pRequest:getParameter("Reason", output pcReason).

    /* validate traningID */
    if piTrainingID = ? or piTrainingID = 0
     then
      do:
        pResponse:fault("3001", "TrainingID").
        return. 
      end.

    /* validate TrainingID */
    if not can-find(first training where training.trainingID = piTrainingID)
     then
      do:  
        pResponse:fault("3067", "Training" {&msg-add} "ID" {&msg-add} string(pitrainingID)).
        return.
      end.

    /* Completed cannot be  cancelled */
    if can-find(first training where training.trainingID = piTrainingID and training.stat = 'C')
     then
      do:  
        pResponse:fault("3005", "Completed Training cannot be Cancelled").
        return.
      end.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttTraining:handle).
    
    lSuccess = false.
    trxBlock:
    for first training exclusive-lock
      where training.trainingID = piTrainingID transaction
      on error undo trxBlock, leave trxBlock:
    
      assign
          training.stat             = "X" /* (X)Cancelled */
          training.lastModifiedDate = now
          training.lastModifiedBy   = pRequest:uid
          training.notes            = pcReason
          .          
             

      lSuccess = true.

      buffer-copy training to ttTraining.
      assign
          ttTraining.statDescription = getTrainingStatusDescription(ttTraining.stat)
          .

      validate training.
      release training.

    end. /* for first training*/
  
    if not lSuccess
     then 
      do:
        presponse:fault("3000", "Training Cancel").
        return.
      end. /* if not lSuccess */
    
    if lSuccess
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
    
    pResponse:success("2000", "Training Cancel").

  end method.
      
  method private char getTrainingStatusDescription ( cTrainingStatus as char):

    if cTrainingStatus = 'P' 
     then
      return "Planned".
    else if cTrainingStatus = 'X' 
     then
      return "Cancelled".
    else if cTrainingStatus = 'S' 
     then
      return "Scheduled".
    else if cTrainingStatus = 'C' 
     then
      return "Completed".
    else
     return cTrainingStatus.

  end method.

end class.


