/* ************************  Function Prototypes ********************** */
FUNCTION doEmailAlerts RETURNS LOGICAL PRIVATE
  (  )  FORWARD.


/* ************************  Function Implementations ***************** */
FUNCTION doEmailAlerts RETURNS LOGICAL PRIVATE
  (  ) :
    def var tFaultCode    as char no-undo.
    def var tFaultMessage as char no-undo.
     
    if not iResponse:isFault() and index("SB", iEmailsEnabled) > 0 /* Success or Both */
     then run util/simplemail.p ("", iEmailSuccess, suppliedActionID + " succeeded for " + suppliedUid, "").
     else
    if iResponse:isFault() and index("FB", iEmailsEnabled) > 0 /* Failure or Both */
     then
      do: iResponse:getFault(output tFaultCode, output tFaultMessage).
          run util/simplemail.p ("", iEmailFailure, 
                                 suppliedActionID + " failed for " + suppliedUid, 
                                 tFaultMessage + "(" + tFaultCode + ")").
      end.

  RETURN true.
END FUNCTION.
