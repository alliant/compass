/* framework/ActionBase
 */
CLASS framework.ActionBase IMPLEMENTS framework.IActable, framework.IQueueable, framework.IAsyncable ABSTRACT:

    def temp-table ttContentField
     field fieldName as char
     field fieldValue as char
     .

    define temp-table ContentField
      field data as char.

    {framework/sysmailparameter.i &tableAlias=mailparameter}

    /* System Log */
    def var logRefType as char no-undo.
    def var logRefNum as char no-undo.
    def var logMessage as char no-undo.

    /* Storage (Azure) keys */
    def var storageEntity as char no-undo.
    def var storageID as char no-undo.
    def var storageCategory as char no-undo.
    
    def var hContentDataset as handle no-undo.
    def protected var tContentFile as char no-undo.

    

    CONSTRUCTOR PUBLIC ActionBase ():
     create widget-pool.
     create dataset hContentDataset.
     hContentDataset:serialize-name = 'data'.

    END CONSTRUCTOR.

    DESTRUCTOR PUBLIC ActionBase ():
    END DESTRUCTOR.


    /*--- Content to return to caller ---*/
    METHOD PRIVATE FINAL void createContentField(input pName as char, input pValue as char):
     def buffer ttContentField FOR ttContentField.
    
     if pName = "" or pName = ?
      then return.
     
     FIND ttContentField
       WHERE ttContentField.fieldName = pName no-error.
     IF NOT AVAILABLE ttContentField 
      THEN 
       DO: create ttContentField.
           ttContentField.fieldName = pName.
       END.
     ttContentField.fieldValue = pValue.
    END METHOD.
  
    METHOD PUBLIC FINAL void setContentField(INPUT pName AS CHARACTER, INPUT pValue AS CHARACTER ):
     createContentField(pName, pValue).
    END METHOD.

    METHOD PUBLIC FINAL void setContentField(INPUT pName AS CHARACTER, INPUT pValue AS DATE ):
     createContentField(pName, string(pValue)).
    END METHOD.

    METHOD PUBLIC FINAL void setContentField(INPUT pName AS CHARACTER, INPUT pValue AS DATETIME ):
     createContentField(pName, string(pValue)).
    END METHOD.

    METHOD PUBLIC FINAL void setContentField(INPUT pName AS CHARACTER, INPUT pValue AS DECIMAL ):
     createContentField(pName, string(pValue)).
    END METHOD.
 
    METHOD PUBLIC FINAL void setContentField(INPUT pName AS CHARACTER, INPUT pValue AS INTEGER ):
     createContentField(pName, string(pValue)).
    END METHOD.

    METHOD PUBLIC FINAL void setContentField(INPUT pName AS CHARACTER, INPUT pValue AS LOGICAL ):
     createContentField(pName, if pValue then "Yes" else "No").
    END METHOD.


    METHOD PUBLIC FINAL void setContentTable(INPUT pName AS CHARACTER, INPUT TABLE-HANDLE pTable, input pFieldList as char ):

     define variable iNumBufferField as integer   no-undo.
     define variable iCount          as integer   no-undo.
     define variable hBuffer         as handle    no-undo.
     define variable hBufferField    as handle    no-undo.
     define variable cSubFieldList   as character no-undo.
    
     if NOT VALID-HANDLE(pTable) 
       OR (VALID-HANDLE(pTable) and not pTable:HAS-RECORDS)
      then return.

     assign
         pName = IF pName = ? OR pName = "" THEN pTable:NAME ELSE pName
         hBuffer = pTable:default-buffer-handle
         hBuffer:serialize-name = pName.
         .
     hBuffer = SerializeFields(input pName, input hBuffer, input pFieldList).

     this-object:hContentDataset:add-buffer(hBuffer).

     delete object pTable no-error.
    END METHOD.


    METHOD PUBLIC FINAL void setContentDataset(input dataset-handle pDataset, input pFieldList as char):
     define variable iBuffer as integer no-undo.
     define variable hBuffer as handle extent no-undo.
     define variable cSubFieldList as char no-undo.
     define variable iCount as integer no-undo.
     define variable iNumBufferField as integer no-undo.
     DEFINE VARIABLE hBufferField AS HANDLE      NO-UNDO.

     if not valid-handle(pDataset) 
     then
       return.

     extent(hbuffer) = pDataset:num-buffers.

     do iBuffer = 1 to pDataset:num-buffers:
        hBuffer[iBuffer] = pDataset:get-buffer-handle(iBuffer).       
        hBuffer[iBuffer] = SerializeFields(input hBuffer[iBuffer]:serialize-name, input hBuffer[iBuffer], input pFieldList).
     end.
     
     this-object:hContentDataset = pDataset.

    END METHOD.
    /*--- END of Content to return to caller ---*/


    method private handle SerializeFields(input pName as char, input pBufferHandle as handle, input pFieldList as char):
      define variable iCount as integer no-undo.
      define variable cSubFieldList as character no-undo.
      define variable iNumBufferField as integer no-undo.
      define variable hBufferField as handle no-undo.
      
      if num-entries(pFieldList, ",") > 0 
        then
        do iCount = 1 TO num-entries(pFieldList, ","):
            assign
                cSubFieldList = cSubFieldList + "," +
                                if entry(1,entry(iCount,pFieldList,","),".") eq pName
                                 then 
                                  entry(2,entry(iCount,pFieldList,","),".") 
                                 else ""
                cSubFieldList = trim(cSubFieldList,",")
            .
        end.
       if num-entries(cSubFieldList, ",") > 0
       then
        do:
         do iNumBufferField = 1 to pBufferHandle:num-fields:
            hBufferField = pBufferHandle:buffer-field(iNumBufferField).
            if lookup(hBufferField:name, cSubFieldList,",") = 0
              then
               do:               
                 pBufferHandle:BUFFER-FIELD(hBufferField:name):xml-node-type = "HIDDEN".
                 pBufferHandle:BUFFER-FIELD(hBufferField:name):serialize-hidden = true.
               end.
         end.
       end.

       return pBufferHandle.
    end method.

    /*--- Send a real-time email to system adminstrator ---*/
    METHOD PUBLIC void alert(input pSubject as char, input pMessage as char):
     run util/sysmail.p (pSubject, pMessage).
    END METHOD.


    /*--- Attributes of the created sysLog record ---*/    
    METHOD PUBLIC void setLogReference(INPUT pRefType AS CHAR, INPUT pRefNum AS CHAR,INPUT pMessage AS CHAR):
     if pRefType = ? then pRefType = "".
     if pRefNum = ? then pRefNum = "".
     if pMessage = ? then pMessage = "".
     assign
       logRefType = pRefType
       logRefNum = pRefNum
       logMessage = pMessage
       .
    END METHOD.
    
    METHOD PUBLIC void getLogReference(output pRefType as char, output pRefNum as char, output pMsg as char):
     assign
       pRefType = logRefType
       pRefNum = logRefNum
       pMsg = logMessage
       .
    END METHOD.
    /*--- END of logging attributes ---*/    


    /*--- Attributes of the uploaded content to storage (Azure) ---*/    
    METHOD PUBLIC void setStorage(INPUT pType AS CHAR, INPUT pID AS CHAR, INPUT pCategory AS CHAR):
     if pType = ? then pType = "".
     if pID = ? then pID = "".
     if pCategory = ? then pCategory = "".
     assign
       storageEntity = pType
       storageID = pID
       storageCategory = pCategory
       .
    END METHOD.
    /*--- END of storage attributes ---*/


    /*--- Email template field replacement values ---*/
    METHOD PUBLIC FINAL logical setEmailParameter (input pEmailObjectName as char, input pEmailObjectValue as char):
     def buffer mailparameter for mailparameter.
     IF pEmailObjectName = "" OR pEmailObjectValue = "" 
      THEN RETURN false.

     find first mailparameter 
       where mailparameter.fieldName = pEmailObjectName no-error.
     if not available mailparameter
      then 
       do: create mailparameter.
           mailparameter.fieldName = pEmailObjectName.
       end.
     mailparameter.fieldValue = pEmailObjectValue.
     return true.
    END METHOD.

    METHOD public final void getEmailParameters(output table mailparameter):
    END METHOD.
    /*--- END of email template fields ---*/


    METHOD PUBLIC VOID act(pRequest as framework.IRequestable, pResponse as framework.IRespondable):
      alert("Act method of " + THIS-OBJECT:GetClass():TypeName + " Not Implemented","").
      pResponse:fault("2000", "There is an incomplete implementation of action. Please contact Administrator.").
      RETURN.
    END METHOD.
	
	METHOD PUBLIC LOGICAL distribute(pRequest as framework.IRequestable):
      RETURN true.
    END METHOD.

   METHOD PUBLIC char render(input pFormat as char, input pTemplate as char):
      def buffer ttContentField for ttContentField.
      def variable hTableContentField as handle. 
      def variable hBufContentField as handle.

      /* KR:modify this to render nothing if there nothing to render . */

      if can-find(first ttContentField)
       then
        do:
             
             create temp-table hTableContentField.
             for each ttContentField no-lock:
                 hTableContentField:add-new-field(ttContentField.fieldname,"character").
             end.
             hTableContentField:temp-table-prepare("parameters").
             hTableContentField:serialize-name = "parameter".

             hBufContentField = hTableContentField:default-buffer-handle.
             hBufContentField:buffer-create().

             for each ttContentField no-lock:
                 hBufContentField:buffer-field(ttContentField.fieldname):buffer-value = ttContentField.fieldvalue.
             end.
            hContentDataset:add-buffer(hBufContentField) .
        end.
      if hContentDataset:num-buffers > 0
       then
        do:
          /* ds: ignore format for default implementation and write to a generic file in JSON */
          PUBLISH "GetSystemParameter" FROM (SESSION:first-procedure) ("TempFolder", output tContentFile).
          tContentFile = tContentFile + "\" + guid + ".json".
          hContentDataset:serialize-name = 'data'.
          hContentDataset:write-json("FILE", tContentFile).
          PUBLISH "AddTempFile" FROM (SESSION:first-procedure) ("ContentFile", tContentFile).
          
        end.
      delete object hBufContentField no-error.
      delete object hTableContentField no-error.
      RETURN tContentFile.
    END METHOD.

    
    /*--- DISTRIBUTION mechanisms to email, printer, directory, or storage (Azure) ---*/
    METHOD public logical emailContent (input pAddr as char, 
                                        input pSubject as char,
                                        input pTemplate as char, 
                                        output pMsg as char ) :
        def var tFrom as char no-undo.
        def var tOk as logical no-undo.
        def var tDefaultTemplate as char no-undo.
        FILE-INFO:filename = tContentFile.
        if FILE-INFO:full-pathname = ?
         then 
          do: pMsg = "No rendered content to send.".
              run util/sysmail.p ("Emailing file failed",
                                  pMsg).
              return false.
          end.
        if pAddr = ? or pAddr = "" or index(pAddr, "@") = 0
         then 
          do: pMsg = "Invalid email address.".
              run util/sysmail.p ("Emailing file failed",
                                  pMsg).
              return false.
          end.
            
        /* Emails may have a custom HTML body template and Subject line */
        pTemplate = search(pTemplate).
        file-info:file-name = pTemplate.
        if file-info:full-pathname = ?
         then 
          do:
            publish "GetSystemParameter" from (SESSION:first-procedure) ("emailDefaultTemplate", output tDefaultTemplate).
            pTemplate = search(tDefaultTemplate).
          end.
         
        if pSubject = "" or pSubject = ?
         then pSubject = "File from Compass".

        publish "GetSystemParameter" from (SESSION:first-procedure) ("emailDefaultFrom", output tFrom).

        /* Name/value pairs to replace fields in the optional HTML template ands Subject line */
        run framework/sendsyshtmlemail.p (input  pTemplate,
                                         input  tFrom,
                                         input  pAddr,
                                         input  pSubject,
                                         input  tContentFile,
                                         input  table mailparameter,
                                         output tOk).
        if not tOk
         then 
          do: pMsg = "Email failed to " + pAddr + " with file " + tContentFile.
              run util/sysmail.p ("Emailing file failed",
                                  pMsg).
          end.
          
        RETURN tOk.
    END METHOD.

    
    METHOD public logical printContent (input pPrinter as char, output pMsg as char):
        DEFINE VARIABLE std-ch AS CHARACTER NO-UNDO.
        FILE-INFO:filename = tContentFile.
        if FILE-INFO:full-pathname = ?
         then 
          do: pMsg = "No rendered content to send.".
              run util/sysmail.p ("Printing file failed",
                                  pMsg).
              return false.
          end.
        if pPrinter = ? or pPrinter = ""
         then 
          do: pMsg = "Invalid Printer.".
              run util/sysmail.p ("Printing file failed",
                                  pMsg).
              return false.
          end.
           
        &SCOPED-DEFINE SumatraPDF "C:\Progress\OpenEdge\bin\SumatraPDF.exe"
        std-ch = {&SumatraPDF} + ' -silent -print-to "' + pPrinter + '" ' + tContentFile.
        os-command silent value(std-ch). 
        if os-error > 0
         then 
          do: pMsg = "Printing of File " + file-info:full-pathname + " failed to " + pPrinter + " with error " + getOsError(os-error).
              run util/sysmail.p ("Printing file failed",
                                  pMsg).
          end.
          
        RETURN (os-error = 0). 
    END METHOD.


    METHOD public logical saveContent (input pDest as char, output pMsg as char):
        FILE-INFO:filename = tContentFile.
        if FILE-INFO:full-pathname = ?
         then 
          do: pMsg = "No rendered content to send.".
              run util/sysmail.p ("Saving file to directory failed",
                                  pMsg).
              return false.
          end.
        if pDest = ? or pDest = ""
         then 
          do: pMsg = "Invalid Destination folder.".
              run util/sysmail.p ("Saving file to directory failed",
                                  pMsg).
              return false.
          end.
        
        pDest = right-trim(pDest, "\") + "\".
        os-copy value(tContentFile) value(pDest).
        if os-error> 0
         then 
          do: pMsg = "Saving file " + file-info:full-pathname + " to " + pDest + " failed with error " + getOsError(os-error).
              run util/sysmail.p ("Saving file failed",
                                  pMsg).
          end.    
        RETURN (os-error = 0).
    END METHOD.


    METHOD public logical storeContent(input pUID as char,
                                       OUTPUT pMsg as char ) :
        def var tOk as logical no-undo.
        
        FILE-INFO:filename = tContentFile.
        if FILE-INFO:full-pathname = ?
         then 
          do: pMsg = "No rendered content to send.".
              run util/sysmail.p ("Saving file to repository failed",
                                  pMsg).
              return false.
          end.
        
        if storageEntity = ? or storageEntity = "" 
           or storageID = ? or storageID = ""
         then
          do: pMsg = "Unable to save file when respository keys are not defined.".
              run util/sysmail.p ("Saving file to repository failed",
                                  pMsg).
              return false.
          end.
        if storageCategory = ?
         then storageCategory = "".
          
        run util\UploadFile.p (pUID, true, storageEntity, storageID, storageCategory, tContentFile, output tOk, output pMsg).
        if not tOk
         then 
          do: pMsg = "Saving file " + tContentFile + " to repository failed with error " + pMsg.
              run util/sysmail.p ("Saving file to repository failed",
                                  pMsg).
          end.
          
        return tOk.
    END METHOD.

    METHOD PUBLIC VOID queue(pRequest as framework.IRequestable, pResponse as framework.IRespondable):
      alert("Act method of " + THIS-OBJECT:GetClass():TypeName + " Not Queueable","").
      pResponse:fault("1003", "Not a Queueable Action. Please contact Administrator.").
      RETURN.
    END METHOD.

    METHOD PUBLIC VOID async(pRequest as framework.IRequestable, pResponse as framework.IRespondable):
      alert("Act method of " + THIS-OBJECT:GetClass():TypeName + " Not Asyncable","").
      pResponse:fault("1003", "Not a Asyncable Action. Please contact Administrator.").
      RETURN.
    END METHOD.

    
    METHOD private FINAL char getOsError (input pCode as int ) :
        case pCode:
          when 1  then return "Not owner".
          when 2  then return "No such file or directory".
          when 3  then return "Interrupted system call".
          when 4  then return "I/O error".
          when 5  then return "Bad file number".
          when 6  then return "No more processes".
          when 7  then return "Not enough core memory".
          when 8  then return "Permission denied".
          when 9  then return "Bad address".
          when 10 then return "File exists".
          when 11 then return "No such device".
          when 12 then return "Not a directory".
          when 13 then return "Is a directory".
          when 14 then return "File table overflow".
          when 15 then return "Too many open files".
          when 16 then return "File too large".
          when 17 then return "No space left on device".
          when 18 then return "Directory not empty".
          otherwise return "Unknown".
        end case.      
    END METHOD.
    /*--- END of distribution ---*/
          
END CLASS.
