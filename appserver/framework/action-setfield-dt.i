/* framework/action-setfield-dt.i
   define a uniquely named function to set a Content Field
   {1} must be a unique name for the field to be passed back to the caller  
 */

METHOD public void set{1} (pValue as datetime):
 setContentField("{1}", pValue).
END METHOD.
