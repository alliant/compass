/* framework/IAsyncable
 */

INTERFACE framework.IAsyncable: 

    METHOD public void async (input pRequest as framework.IRequestable,
                              input pResponse as framework.IRespondable).
                            
    
END INTERFACE.
