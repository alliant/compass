/* tt/sysmailparameter.i
   MAIL PARAMETERs for sending a HTML email
   Author: John Oliver
   Date: 08.01.2017
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias mailparameter
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

define temp-table {&tableAlias}
  field fieldName  as char {&nodeType}
  field fieldValue as char {&nodeType}
  .
