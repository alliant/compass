/* framework/do-def.i
   Shared common variables for a starting entry point program
   Note: The entry point program must be an external procedure as class structures are not supported
   D.Sinclair 11.28.2022
   &request:  class name that implements IRequestable
   &response: class that implements IRespondable
 */

&IF defined(response) = 0
 &THEN
&SCOP response framework.ResponseBase
&ENDIF

def var iRequest as {&request} no-undo.
def var iResponse as {&response} no-undo.
def var iAction as framework.IActable no-undo.
def var iQueueAction as framework.IQueueable no-undo.
def var iAsyncAction as framework.IAsyncable no-undo.
def var iContentFile as char no-undo.


&IF defined(exclude-function-forwards) = 0
 &THEN
FUNCTION init returns logical private FORWARD.
FUNCTION act returns logical private FORWARD.
FUNCTION render returns logical private FORWARD.
FUNCTION distribute returns logical private FORWARD.
FUNCTION queue returns logical private FORWARD.
&ENDIF


/* Passed in values on the request I# */ 
DEF VAR suppliedFormat           AS CHAR NO-UNDO.
DEF VAR suppliedUid              AS CHAR NO-UNDO. 
DEF VAR suppliedPassword         AS CHAR NO-UNDO.
DEF VAR suppliedActionID         AS CHAR NO-UNDO.
DEF VAR suppliedClientID         AS CHAR NO-UNDO.  
DEF VAR suppliedSessionID        AS CHAR NO-UNDO.
DEF VAR suppliedIPAddress        AS CHAR NO-UNDO.
DEF VAR suppliedQueryString      AS CHAR NO-UNDO.
DEF VAR suppliedFieldList        AS CHAR NO-UNDO.
DEF VAR suppliedActionType       AS CHAR NO-UNDO.  
DEF VAR suppliedNotifyReq        AS CHAR NO-UNDO.  
DEF VAR suppliedBooleanDest      AS CHAR NO-UNDO.  
DEF VAR suppliedEmailAddrList    AS CHAR NO-UNDO.  
DEF VAR suppliedPrinterAddr      AS CHAR NO-UNDO.  
def var suppliedSaveDir          as char no-undo.  
DEF VAR suppliedStorageRoot      AS CHAR NO-UNDO.
DEF VAR suppliedOutputFormat     AS CHAR NO-UNDO.
DEF VAR suppliedDistType         AS CHAR NO-UNDO.
DEF VAR suppliedDataFile         AS CHAR NO-UNDO.


/* Determined from the sysAction */                           
DEFINE VAR iValidAction AS LOGICAL NO-UNDO INIT FALSE.
DEFINE VAR iIsActive AS LOGICAL NO-UNDO INIT FALSE.
DEFINE VAR iIsAnonymous AS LOGICAL NO-UNDO INIT FALSE.
DEFINE VAR iIsSecure AS LOGICAL NO-UNDO INIT TRUE.
DEFINE VAR iIsLog AS LOGICAL NO-UNDO INIT TRUE.
DEFINE VAR iIsAudit AS LOGICAL NO-UNDO INIT FALSE.
DEFINE VAR iUserids AS CHAR NO-UNDO INIT "!*".
DEFINE VAR iRoles AS CHAR NO-UNDO INIT "!*".
DEFINE VAR iIPAddresses AS CHAR NO-UNDO INIT "!*".
DEFINE VAR iActionHandler AS CHAR NO-UNDO.
DEFINE VAR iIsEventsEnabled AS LOG NO-UNDO.
DEFINE VAR iEmailsEnabled as char no-undo.
DEFINE VAR iEmailSuccess AS CHAR NO-UNDO.
DEFINE VAR iEmailFailure AS CHAR NO-UNDO.
DEFINE VAR iQueueHandler AS CHAR NO-UNDO.
DEFINE VAR iQueueable AS LOGICAL NO-UNDO INIT FALSE.
DEFINE VAR iDisplayName AS CHAR NO-UNDO.
DEFINE VAR iEmailHtml AS CHAR NO-UNDO.
DEFINE VAR iEmailSubject AS CHAR NO-UNDO.
DEFINE VAR iActionDesc   AS CHAR NO-UNDO.
                                       
/* Determined from the sysUser */                         
DEFINE VAR iUserName AS CHAR NO-UNDO.
DEFINE VAR iUserPass AS CHAR NO-UNDO.
DEFINE VAR iUserInitials AS CHAR NO-UNDO.
DEFINE VAR iUserRole AS CHAR NO-UNDO.
DEFINE VAR iUserActive AS LOGICAL NO-UNDO.
DEFINE VAR iUserEmail AS CHAR NO-UNDO.
DEFINE VAR iUserPasswordSetDate AS DATETIME NO-UNDO.
DEFINE VAR iUserPasswordExpired AS LOGICAL NO-UNDO.
DEFINE VAR iUserPasswordExpiredDate AS DATETIME NO-UNDO.
DEFINE VAR iUserPasswordWarning AS INT NO-UNDO.
DEFINE VAR iUserPasswordWarningDays AS CHAR NO-UNDO.


DEF var doDebug  AS LOGICAL NO-UNDO.
DEF var canDebug AS LOGICAL NO-UNDO.
def var canExec as logical init false no-undo.
def var iFaultCode as char no-undo init "".
def var iFaultMessage as char no-undo init "".
def var iSuccessCode as char no-undo init "".
def var iSuccessMessage as char no-undo init "".

{lib/std-def.i}
