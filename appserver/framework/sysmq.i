/* tt/sysmq.i
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias mqData
&ENDIF

DEFINE TEMP-TABLE {&tableAlias}
  field seq AS INT
  field businessObject as char
  field keyID as char
  field action as char
  field description AS CHARACTER
  index iSeq IS PRIMARY UNIQUE seq.
