/* framework/QueueRequest
 */
using Progress.Json.ObjectModel.JsonObject.
CLASS framework.RequestQueue INHERITS framework.RequestBase: 

	DEFINE PRIVATE VARIABLE lcRequestPayload AS longchar no-undo.
    DEFINE PRIVATE VARIABLE cQueryString AS LONGCHAR NO-UNDO.
    DEFINE PRIVATE VARIABLE cMimeType AS CHARACTER NO-UNDO.
    CONSTRUCTOR PUBLIC RequestQueue (
		INPUT pUid AS char,
		INPUT pName AS char,
		INPUT pRole AS char,
		INPUT pEmail AS char,
		INPUT pActionID AS char,
		INPUT pQueryString AS char,
		input pRequestPayload as longchar,
        input pFieldList as char,
        input pMimeType as char
		):
	SUPER (
		INPUT pUid,
		INPUT pName,
		INPUT pRole,
		INPUT pEmail,
		INPUT pActionID,
        input pFieldList
		).

     parseQueryString(pQueryString).
	 lcRequestPayload = pRequestPayload.
     cQueryString = pQueryString. /* Increase the scope of the variable*/
     cMimeType = pMimeType.
    END CONSTRUCTOR.

    METHOD PUBLIC OVERRIDE VOID getContent(INPUT-OUTPUT DATASET-HANDLE pDataSet, OUTPUT pErrMsg AS CHAR):
        DEFINE VARIABLE lSuccess AS LOGICAL   NO-UNDO.
        DEFINE VARIABLE iCount   AS INTEGER   NO-UNDO.
        
        IF NOT VALID-HANDLE(pDataSet) 
         THEN 
          DO:	
            pErrMsg = 'Invalid DataSet Handle'.
            RETURN .
          END.

        IF THIS-OBJECT:lcRequestPayload = "" OR this-object:lcRequestPayload = ? OR THIS-OBJECT:lcRequestPayload = "?"  
         THEN
          DO:
            pErrMsg = 'Invalid Request Payload'.
            RETURN .
          END.

        IF THIS-OBJECT:cMimeType = "" OR this-object:cMimeType = ? OR THIS-OBJECT:cMimeType = "?"  
         THEN
          DO:
            pErrMsg = 'Invalid Request Payload Type'.
            RETURN .
          END.

		IF THIS-OBJECT:cMimeType = "text/json" 
         THEN
          DO: 
            lSuccess = pDataset:read-json("LONGCHAR", THIS-OBJECT:lcRequestPayload, "EMPTY") no-error.
          END.
        ELSE IF THIS-OBJECT:cMimeType = "text/xml"
         THEN
          DO: 
            lSuccess = pDataset:read-xml("LONGCHAR", THIS-OBJECT:lcRequestPayload, "EMPTY", ?, ?) no-error.
          END.

        IF NOT lSuccess OR ERROR-STATUS:ERROR
         THEN 
          DO:	
            DO iCount = 1 TO ERROR-STATUS:NUM-MESSAGES:
              pErrMsg = pErrMsg + ERROR-STATUS:GET-MESSAGE(iCount) + ' '. 
            END.
          END.
        RETURN.  
    END METHOD.

    METHOD PUBLIC LONGCHAR getPayload():
    /* Yet to be implemented*/
      RETURN "".
    END.

    METHOD PUBLIC LONGCHAR getQueryString():
      RETURN getRequestQueryString(cQueryString).
    END.

    METHOD PUBLIC char getPayloadType():
      return "".
    END.

    DESTRUCTOR PUBLIC RequestQueue ():
    END DESTRUCTOR.

END CLASS.
