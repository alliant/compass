/* do-distribute.i 
   Default implementation of distribution methods
   DSinclair 12.14.2022
   &success [optional] logical field/variable to hold if any distribution fails
   Modified:
   Name          Date          Note
   SB            08/23/2023    Calling distribute method in all the cases for distribution option
 */

/* &success = logical variable/field to hold if any distribution failed */
&IF defined(success) <> 0
 &THEN
&SCOP SetVar if not std-lo then {&success} = false.
&ENDIF

if not valid-object(iAction)
 then return false.

std-lo = iAction:distribute(input iRequest).  /* might be implemented by action */
{&SetVar}

if suppliedEmailAddrList <> ""
 then
  do: std-lo = iAction:emailContent(suppliedEmailAddrList,iEmailSubject, iEmailHtml, output std-ch).
      {&SetVar}
  end.
  
if suppliedPrinterAddr <> "" 
 then
  do: std-lo = iAction:printContent(suppliedPrinterAddr, output std-ch).
      {&SetVar}
  end. 

if suppliedStorageRoot <> "" 
 then
  do: iAction:storeContent(suppliedUID, output std-ch).
      {&SetVar}
  end. 
  
if suppliedSaveDir <> ""
 then
  do: std-lo = iAction:saveContent(suppliedSaveDir, output std-ch).
      {&SetVar}
  end.

