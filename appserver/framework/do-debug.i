/* ************************  Function Prototypes ********************** */
FUNCTION debug RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

                                         

/* ************************  Function Implementations ***************** */
FUNCTION debug RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
    define variable lcReqPayload as longchar no-undo.
    define variable cReqQuery   as char      no-undo.
    define variable cMimeType   as char      no-undo.

    IF not doDebug 
     THEN return false. 

     if log-manager:logfile-name > ""
      then
       do:
         LOG-MANAGER:WRITE-MESSAGE ("Client: " + suppliedClientid).
         LOG-MANAGER:WRITE-MESSAGE ("Session: " + suppliedSessionid).
         LOG-MANAGER:WRITE-MESSAGE ("Action: " + suppliedActionid).
         LOG-MANAGER:WRITE-MESSAGE ("Valid Action: " + STRING(iValidAction)).
         LOG-MANAGER:WRITE-MESSAGE ("Action Active: " + STRING(iIsActive)).
         LOG-MANAGER:WRITE-MESSAGE ("Anonymous Access: " + STRING(iIsAnonymous)).
         LOG-MANAGER:WRITE-MESSAGE ("Secure: " + STRING(iIsSecure)).
         LOG-MANAGER:WRITE-MESSAGE ("Log: " + STRING(iIsLog)).
         LOG-MANAGER:WRITE-MESSAGE ("Audit: " + STRING(iIsAudit)).
         LOG-MANAGER:WRITE-MESSAGE ("Action Handler: " + iActionHandler).
         LOG-MANAGER:WRITE-MESSAGE ("Queue Handler: " +  iQueueHandler).
         LOG-MANAGER:WRITE-MESSAGE ("Uid: " + suppliedUid).
         LOG-MANAGER:WRITE-MESSAGE ("Active User: " + STRING(iUserActive)).
         LOG-MANAGER:WRITE-MESSAGE ("Name: " + STRING(iUserName)).
         LOG-MANAGER:WRITE-MESSAGE ("Initials: " + STRING(iUserInitials)).
         LOG-MANAGER:WRITE-MESSAGE ("Valid Userids: " + iUserids).
         LOG-MANAGER:WRITE-MESSAGE ("Valid IPs: " + iIpAddresses).
         LOG-MANAGER:WRITE-MESSAGE ("Role: " + iUserRole).
         LOG-MANAGER:WRITE-MESSAGE ("Valid Roles: " + iRoles).
         LOG-MANAGER:WRITE-MESSAGE("Input Payload:").

         if valid-object(iRequest) then
           assign
              lcreqPayload = iRequest:getPayload()
              cReqQuery    = iRequest:getQueryString()
             .
         copy-lob lcReqPayload to file log-manager:logfile-name append.
         output to value(log-manager:logfile-name) append.
         put unformatted skip(1).
         output close.
         LOG-MANAGER:WRITE-MESSAGE("Query:" + cReqQuery).
         if iContentFile <> "" 
           and iContentFile <> ? 
           and num-entries(iContentFile, ".") = 2
           and file-info:full-pathname <> ?
         then
          do:
              cMimeType = entry(2, iContentFile, ".").
              if cMimeType = 'xml' or cMimeType = 'json' 
               then
                do:
                    LOG-MANAGER:WRITE-MESSAGE("Response:").
                    copy-lob from file iContentFile to file log-manager:logfile-name append.
                end.
          end.
       end.
      else
       do:
         message "Client: " + suppliedClientid view-as alert-box.
         message "Session: " + suppliedSessionid view-as alert-box.
         message "Action: " + suppliedActionid view-as alert-box.
         message "Valid Action: " + STRING(iValidAction) view-as alert-box.
         message "Action Active: " + STRING(iIsActive) view-as alert-box.
         message "Anonymous Access: " + STRING(iIsAnonymous) view-as alert-box.
         message "Secure: " + STRING(iIsSecure) view-as alert-box.
         message "Log: " + STRING(iIsLog) view-as alert-box.
         message "Audit: " + STRING(iIsAudit) view-as alert-box.
         message "Action Handler: " + iActionHandler view-as alert-box.
         message "Queue Handler: " + iQueueHandler view-as alert-box.
         message "Uid: " + suppliedUid view-as alert-box.
         message "Active User: " + STRING(iUserActive) view-as alert-box.
         message "Name: " + iUserName view-as alert-box.
         message "Initials: " + iUserInitials view-as alert-box.
         message "Valid Userids: " + iUserids view-as alert-box.
         message "Valid IPs: " + iIpAddresses view-as alert-box.
         message "Role: " + iUserRole view-as alert-box.
         message "Valid Roles: " + iRoles view-as alert-box.
         cReqQuery   = if valid-object(iRequest) then iRequest:getQueryString() else ''.
         message "Query: " + cReqQuery view-as alert-box.
       end.


  /* ds: get and output data from IRequest and IResponse? */ 

  RETURN true.

END FUNCTION.
