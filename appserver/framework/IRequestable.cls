/* framework/IRequestable

   Modification:
   
   K.R    02/20/2024 Modified to add getPayloadJson and getPayloadXML method definations
   S.B    06/25/2024 Task # 113923 - Modified to get system parameters (I8,I9,I10)
 */
using Progress.Json.ObjectModel.JsonObject.
INTERFACE framework.IRequestable: 

    DEFINE PUBLIC PROPERTY Uid AS CHAR Get.
    DEFINE PUBLIC PROPERTY name AS CHAR Get.
    DEFINE PUBLIC PROPERTY role AS CHAR Get.
    DEFINE PUBLIC PROPERTY email AS CHAR Get.
    DEFINE PUBLIC PROPERTY actionid AS CHAR Get.
    define public property FieldList as char get.

    METHOD PUBLIC void getParameter (INPUT pName AS CHAR, output pValue as char).
    METHOD PUBLIC void getParameter (INPUT pName AS CHAR, output pValue as date).
    METHOD PUBLIC void getParameter (INPUT pName AS CHAR, output pValue as datetime).
    METHOD PUBLIC void getParameter (INPUT pName AS CHAR, output pValue as datetime-tz).
    METHOD PUBLIC void getParameter (INPUT pName AS CHAR, output pValue as deci).
    METHOD PUBLIC void getParameter (INPUT pName AS CHAR, output pValue as int).
    METHOD PUBLIC void getParameter (INPUT pName AS CHAR, output pValue as logi).

    METHOD PUBLIC logi getNotifyRequestor().
    METHOD PUBLIC CHAR getDestination    ().
    METHOD PUBLIC CHAR getDistributionEmailList().
    METHOD PUBLIC CHAR getPrinter        ().
    METHOD PUBLIC CHAR getStorageRootKey ().
    METHOD PUBLIC CHAR getOutputFormat   ().
    
    METHOD PUBLIC void getContent(INPUT-OUTPUT DATASET-HANDLE pDataSet, OUTPUT pErrMsg AS CHAR).
    METHOD PUBLIC void getPayloadJSON(INPUT-OUTPUT oJsonObect  as JsonObject, OUTPUT pErrMsg AS CHAR).
    METHOD PUBLIC void getPayloadXML(INPUT-OUTPUT  hX-Document as HANDLE, OUTPUT pErrMsg AS CHAR).
    
    METHOD public void debug ().
END INTERFACE.
