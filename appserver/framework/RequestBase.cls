/* framework/RequestBase

    Modification
    
    K.R  02/20/2024 Modified to add default implementation of getPayloadJson and getPayloadXML    
    S.B  06/25/2024 Task # 113923 - Modified to get system parameters (I8,I9,I10)  
    K.R  07/23/2024 Modified to add method to get query string without sensitive system parameters  
 */
using Progress.Json.ObjectModel.JsonObject.
CLASS framework.RequestBase IMPLEMENTS framework.IRequestable ABSTRACT:

    DEFINE PUBLIC PROPERTY Uid AS CHAR Get. PRIVATE Set.
    DEFINE PUBLIC PROPERTY name AS CHAR Get. PRIVATE Set.
    DEFINE PUBLIC PROPERTY role AS CHAR Get. PRIVATE Set.
    DEFINE PUBLIC PROPERTY email AS CHAR Get. PRIVATE Set.
    DEFINE PUBLIC PROPERTY actionid AS CHAR Get. PRIVATE Set.
    define public property FieldList as char get. private set.

    define variable cNotifyRequestor   as logical   no-undo.
    define variable cBoolenDestination as character no-undo.
    define variable cEmailAddrList     as character no-undo.
    define variable cPrinterAddress    as character no-undo.
    define variable cStorageRootkey    as character no-undo.
    define variable cOutputFormat      as character no-undo.

    {framework/sysdata.i &Mode="PROTECTED" &tableAlias=iData}          /* iData is used in the code */
    
    
    
    CONSTRUCTOR PUBLIC RequestBase (
		INPUT pUid AS char,
		INPUT pName AS char,
		INPUT pRole AS char,
		INPUT pEmail AS char,
		INPUT pActionID AS char,
        input pFieldList as char
		):
    
      ASSIGN
          THIS-OBJECT:Uid = pUid
          THIS-OBJECT:name = pName
          THIS-OBJECT:email = pEmail
          THIS-OBJECT:role = pRole
          THIS-OBJECT:actionid = pActionID
          this-object:FieldList = pFieldList
          .
    END CONSTRUCTOR.

    DESTRUCTOR PUBLIC RequestBase ( ):
    END DESTRUCTOR.


    METHOD PUBLIC VOID getContent(INPUT-OUTPUT DATASET-HANDLE pDataSet, OUTPUT pErrMsg AS CHAR):
		pErrMsg = "Method Not Implemented".
		return.
    END METHOD.

    METHOD PUBLIC VOID getPayloadJSON(INPUT-OUTPUT oJsonObject as JsonObject, OUTPUT pErrMsg AS CHAR):
		pErrMsg = "Method Not Implemented".
		return.
    END METHOD.
    
    METHOD PUBLIC VOID getPayloadXML(INPUT-OUTPUT  hX-Document as HANDLE, OUTPUT pErrMsg AS CHAR):
    pErrMsg = "Method Not Implemented".
    return.
    END METHOD.

    METHOD PUBLIC void getParameter(INPUT pName AS CHARACTER, output pValue as char ):
        DEFINE BUFFER iData FOR iData.
        
        pValue = ?.
        FIND iData NO-LOCK
          WHERE iData.objName = "parameter"
            AND iData.objProperty = pName NO-ERROR.
        IF AVAILABLE iData 
         THEN pValue = iData.propertyValue.

        error-status:error = false.
        return.
    END METHOD.

    METHOD PUBLIC void getParameter(INPUT pName AS CHARACTER, output pValue as date ):
        DEFINE BUFFER iData FOR iData.
        def var std-da as date.
        
        pValue = ?.
        FIND iData NO-LOCK
          WHERE iData.objName = "parameter"
            AND iData.objProperty = pName NO-ERROR.
        IF AVAILABLE iData 
         THEN 
          do: std-da = DATE(iData.propertyValue) NO-ERROR.
              if not error-status:error
               then pValue = std-da.
          end.

        error-status:error = false.
        return.
    END METHOD.

    METHOD PUBLIC void getParameter(INPUT pName AS CHARACTER, output pValue as datetime ):
        DEFINE BUFFER iData FOR iData.
        def var std-dt as datetime.
        
        pValue = ?.
        FIND iData NO-LOCK
          WHERE iData.objName = "parameter"
            AND iData.objProperty = pName NO-ERROR.
        IF AVAILABLE iData 
         THEN 
          do: std-dt = DATETIME(iData.propertyValue) NO-ERROR.
              if not error-status:error
               then pValue = std-dt.
          end.

        error-status:error = false.
        return.
    END METHOD.

    METHOD PUBLIC void getParameter(INPUT pName AS CHARACTER, output pValue as datetime-tz):
        DEFINE BUFFER iData FOR iData.
        def var std-dz as datetime-tz.
        
        pValue = ?.
        FIND iData NO-LOCK
          WHERE iData.objName = "parameter"
            AND iData.objProperty = pName NO-ERROR.
        IF AVAILABLE iData 
         THEN 
          do: std-dz = DATETIME-TZ(iData.propertyValue) NO-ERROR.
              if not error-status:error
               then pValue = std-dz.
          end.

        error-status:error = false.
        return.
    END METHOD.

    METHOD PUBLIC void getParameter(INPUT pName AS CHARACTER, output pValue as decimal ):
        DEFINE BUFFER iData FOR iData.
        def var std-de as decimal.
        
        pValue = ?.
        FIND iData NO-LOCK
          WHERE iData.objName = "parameter"
            AND iData.objProperty = pName NO-ERROR.
        IF AVAILABLE iData 
         THEN 
          do: std-de = DECIMAL(iData.propertyValue) NO-ERROR.
              if not error-status:error
               then pValue = std-de.
          end.

        error-status:error = false.
        return.
    END METHOD.

    METHOD PUBLIC void getParameter(INPUT pName AS CHARACTER, output pValue as integer ):
        DEFINE BUFFER iData FOR iData.
        def var std-in as int.
        
        pValue = ?.
        FIND iData NO-LOCK
          WHERE iData.objName = "parameter"
            AND iData.objProperty = pName NO-ERROR.

        IF AVAILABLE iData 
         THEN 
          do: std-in = INT(iData.propertyValue) NO-ERROR.
              if not error-status:error
               then pValue = std-in.
          end.

       error-status:error = false.
       return.
    END METHOD.

    METHOD PUBLIC void getParameter(INPUT pName AS CHARACTER, output pValue as logi ):
        DEFINE BUFFER iData FOR iData.
        
        pValue = ?.
        FIND iData NO-LOCK
          WHERE iData.objName = "parameter"
            AND iData.objProperty = pName NO-ERROR.
        IF AVAILABLE iData 
         THEN pValue = (if lookup(iData.propertyValue,"yes,true,1,Y,T") > 0 then true else false).

        error-status:error = false.
        return.
    END METHOD.

    METHOD PUBLIC logical getNotifyRequestor():
        return cNotifyRequestor.
    END METHOD.

    METHOD PUBLIC CHAR GetDestination():
        return cBoolenDestination.
    END METHOD.

    METHOD PUBLIC CHAR getDistributionEmailList():
        return cEmailAddrList.
    END METHOD.

    METHOD PUBLIC CHAR getPrinter():
        return cPrinterAddress.
    END METHOD.

    METHOD PUBLIC CHAR getStorageRootKey():
        return cStorageRootkey.
    END METHOD.

    METHOD PUBLIC CHAR getOutputFormat():
        return cOutputFormat.
    END METHOD.

    METHOD PUBLIC VOID debug():
        DEFINE BUFFER iData FOR iData.
        FOR EACH iData NO-LOCK:
          if log-manager:logfile-name > ""
           then LOG-MANAGER:WRITE-MESSAGE ("Object: " + iData.objName + " | Property: " + iData.objProperty + " | Value: " + iData.propertyValue,THIS-OBJECT:GetClass():TypeName).
           else message "Object: " + iData.objName + " | Property: " + iData.objProperty + " | Value: " + iData.propertyValue view-as alert-box.
        END.
    END METHOD.
    
     

    METHOD PRIVATE character decodeValue(input cValue as char):
        /**************************************************************************** 
        Description: Decodes unsafe characters in a URL as per RFC 1738 section 2.2. 
        <URL:http://ds.internic.net/rfc/rfc1738.txt>, 2.2 
        Input Parameters: Encoded character string to decode, 
        In addition, all characters specified in the variable cUnsafe plus ASCII values 0 <= x <= 31 and 127 <= x <= 255 are considered unsafe. 
        Returns: Decoded string (unkown value is returned as blank) 
        ****************************************************************************/ 
        DEFINE VARIABLE cHex        AS CHARACTER   NO-UNDO INITIAL "0123456789ABCDEF":U. 
        DEFINE VARIABLE iCounter    AS INTEGER     NO-UNDO. 
        DEFINE VARIABLE cChar       AS INTEGER     NO-UNDO. 
        define variable cTriplet    as character   no-undo.
        
        /* Unsafe characters that must be encoded in URL's.  See RFC 1738 Sect 2.2. */
        DEFINE VARIABLE cUnsafe   AS CHARACTER NO-UNDO INITIAL " <>~"#%~{}|~\^~~[]`":U.
        
        /* Reserved characters that normally are not encoded in URL's */
        DEFINE VARIABLE cReserved AS CHARACTER NO-UNDO INITIAL "~;/?:@=&":U.
        
        /* Don't bother with blank or unknown */ 
        IF LENGTH(cValue) EQ 0 OR 
           cValue         EQ ? THEN 
          RETURN "". 
        
        /* Loop through entire input string */ 
        iCounter = 0. 
        DO WHILE TRUE: 
          ASSIGN iCounter = iCounter + 1 
                 /* ASCII value of character using single byte codepage */ 
                 cChar = ASC(SUBSTRING(cValue, iCounter, 1, "RAW":U), 
                             "1252":U, 
                             "1252":U). 
        
          if chr(cChar) = "%" then
          do:
            assign 
                cTriplet = substring(cValue, iCounter, 3, "RAW":U)
                cChar    = ((index(cHex,substring(cTriplet,2,1)) - 1) * 16) + /* high hex digit */
                           (index(cHex,substring(cTriplet,3,1)) - 1).         /* low hex digit */
                           
            substring(cValue, iCounter, 3, "RAW":U) = chr(cChar).    
          end.
          
          IF iCounter = LENGTH(cValue,"RAW":U) THEN LEAVE. 
        END. 
        
        return cValue.
    END METHOD.


    METHOD protected void parseQueryString(pQueryString as char):
        DEF VAR i AS INT NO-UNDO.
        DEF VAR iSeq# AS INT NO-UNDO INIT 0.
        DEF VAR iName AS CHAR NO-UNDO.
        DEF VAR iValue AS CHAR NO-UNDO.

        EMPTY temp-table iData.

        if pQueryString = ? and pQueryString < ""
         then return.
        
        DO i = 1 TO NUM-ENTRIES(pQueryString,"&"):
            ASSIGN
                iName  = ENTRY(1,ENTRY(i,pQueryString,"&"),"=")
                iValue = ENTRY(2,ENTRY(i,pQueryString,"&"),"=") NO-ERROR.
            IF ERROR-STATUS:ERROR 
             THEN RETURN. /* 'Invalid Query String Format: ' + ERROR-STATUS:GET-MESSAGE(1). */
                
            IF LOOKUP(iName, "I0,I1,I2,I3,I4,I5,I6,I7,I8,I9,I10") > 0 
             THEN NEXT. /* Skip because it's a 'system' parameter */

            iSeq# = iSeq# + 1.        
            CREATE iData.
            ASSIGN
             iData.objSeq = iSeq#
             iData.objName = "parameter"
             iData.objPropSeq = 1
             iData.objProperty = iName
             iData.propertyValue = decodeValue(iValue)
             iData.searchName = iData.objName + "." + iData.objProperty
             .
        END.
    END METHOD.

    METHOD protected void parseSystemQueryString(pQueryString as char):
        DEF VAR i AS INT NO-UNDO.
        DEF VAR iName AS CHAR NO-UNDO.
        DEF VAR iValue AS CHAR NO-UNDO.

        if pQueryString = ? and pQueryString < ""
         then return.
        
        DO i = 1 TO NUM-ENTRIES(pQueryString,"&"):
            ASSIGN
                iName  = ENTRY(1,ENTRY(i,pQueryString,"&"),"=")
                iValue = ENTRY(2,ENTRY(i,pQueryString,"&"),"=") NO-ERROR.
            IF ERROR-STATUS:ERROR 
             THEN RETURN. /* 'Invalid Query String Format: ' + ERROR-STATUS:GET-MESSAGE(1). */

           /* System Parameters */
           case upper(iName):
            when "I8a" then 
              assign cNotifyRequestor   =  if decodeValue(iValue) = "Yes" then yes else no.
            when "I9" then 
              assign cBoolenDestination =  decodeValue(iValue).
            when "I9e" then
              assign cEmailAddrList     =  decodeValue(iValue).
            when "I9p" then
              assign cPrinterAddress    =  decodeValue(iValue).
            when "I9s" then
              assign cStorageRootkey    =  decodeValue(iValue).
            when "I10" then 
              assign cOutputFormat      =  decodeValue(iValue).

           end case.
        END.
    END METHOD.

    METHOD PROTECTED LONGCHAR getRequestQueryString(input pQueryString AS LONGCHAR):
        DEFINE VARIABLE iCounter     AS INTEGER   NO-UNDO.
        DEFINE VARIABLE cName        AS CHARACTER NO-UNDO.
        DEFINE VARIABLE cValue       AS CHARACTER NO-UNDO.
        DEFINE VARIABLE cQueryString AS LONGCHAR  NO-UNDO.

        if pQueryString = ? and pQueryString < ""
         then return ''.
        
        DO iCounter = 1 TO NUM-ENTRIES(pQueryString,"&"):
          ASSIGN
            cName  = ENTRY(1,ENTRY(iCounter,pQueryString,"&"),"=")
            cValue = ENTRY(2,ENTRY(iCounter,pQueryString,"&"),"=") NO-ERROR.
          IF ERROR-STATUS:ERROR 
           THEN return ''. /* 'Invalid Query String Format */
                
          IF LOOKUP(cName, "I0,I1,I2,I3") > 0 
           THEN NEXT. /* Skip because it's a 'system' parameter */
      
          cQueryString = cQueryString + entry(iCounter,pQueryString, "&") + "&".
        END.
        RETURN TRIM(cQueryString, "&").
    END.

END CLASS.
