/* framework/IActable
 */

INTERFACE framework.IQueueable: 

    METHOD public void queue (input pRequest as framework.IRequestable,
                              input pResponse as framework.IRespondable).
                            
    
END INTERFACE.
