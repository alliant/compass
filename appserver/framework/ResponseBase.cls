/* framework/ResponseBase
 */

CLASS framework.ResponseBase IMPLEMENTS framework.IRespondable: 
  
  {framework/sysnotification.i &tableAlias=ttSysNotification}
  {framework/sysmq.i &tableAlias=ttMqData}                    
  
  /* Fault and Success Code */
  def var faultCode as char no-undo.
  def var faultMessage as char no-undo.
  def var successCode as char no-undo.
  def var successMessage as char no-undo.
  def var isFault as logical no-undo init false.

  CONSTRUCTOR PUBLIC ResponseBase():
  END CONSTRUCTOR.

  DESTRUCTOR PUBLIC ResponseBase():
  END DESTRUCTOR.


    /*--- Fault ---*/
    METHOD PUBLIC void fault(INPUT pCode AS CHARACTER, INPUT pMessage AS CHARACTER ):
     if pCode = ? or pCode = ""
      then assign
           pCode = "3000" /* Default generic code */
           pMessage = "Action"
           .
            
     /* Only set the code once */
     IF not isFault 
      THEN faultCode = pCode.

     run util/msgsubs.p (pCode, pMessage, output pMessage).

     /* Keep appending messages as faults occur */       
     faultMessage = faultMessage + pMessage + "  ".
     isFault = TRUE.
   END METHOD.

   METHOD PUBLIC LOGICAL isFault():
    RETURN isFault.
   END METHOD.

   METHOD PUBLIC void getFault(OUTPUT pCode AS CHAR, OUTPUT pMessage AS CHAR):
    IF not isFault
     THEN return.
    ASSIGN
      pCode = faultCode
      pMessage = faultMessage
      .
   END METHOD.
   /*--- END of Fault ---*/

  
   /*--- Success ---*/
   METHOD PUBLIC void success(INPUT pCode AS CHARACTER, INPUT pMessage AS CHARACTER ):
    if isFault
     then return.
     
    if pCode = "" or pCode = ?
     then assign
            pCode = "2000"
            pMessage = "Action was successful."
            .
    
    run util/msgsubs.p (pCode, pMessage, output pMessage).
         
    ASSIGN
      successCode = pCode
      successMessage = pMessage
      .
   END METHOD.

   METHOD PUBLIC void getSuccess(OUTPUT pCode AS CHAR, OUTPUT pMessage AS CHAR):
    if isFault
     then return.
     
    ASSIGN
      pCode = successCode
      pMessage = successMessage
      .
   END METHOD.
   /*--- END of Success ---*/


   /*--- Notifications (records used to inform users of various actions) ---*/
   METHOD public void notifyReAgent(input pEntityID as character, 
                                    input pSourceType as character,
                                    input pSourceID as character,
                                    input pNotification as character):
    def buffer ttSysNotification for ttSysNotification.
    create ttSysNotification.
    assign
          ttSysNotification.UID                  = ""                                     
          ttSysNotification.entityType           = "A"
          ttSysNotification.entityID             = pEntityID
          ttSysNotification.refSysNotificationID = 0
          ttSysNotification.notificationStat     = "N"
          ttSysNotification.sourceType           = pSourceType
          ttSysNotification.sourceID             = pSourceID
          ttSysNotification.notification         = pNotification
          ttSysNotification.createDate           = now.
   END METHOD.

   METHOD public void notifyRePerson(input pEntityID as character, 
                                     input pSourceType as character,
                                     input pSourceID as character,
                                     input pNotification as character):
    def buffer ttSysNotification for ttSysNotification.
    create ttSysNotification.
    assign
          ttSysNotification.UID                  = ""
          ttSysNotification.entityType           = "E"
          ttSysNotification.entityID             = pEntityID
          ttSysNotification.refSysNotificationID = 0
          ttSysNotification.notificationStat     = "N"
          ttSysNotification.sourceType           = pSourceType
          ttSysNotification.sourceID             = pSourceID
          ttSysNotification.notification         = pNotification
          ttSysNotification.createDate           = now.
   END METHOD.

   METHOD public void notifyToEmployee(input pUID as character, 
                                       input pSourceType as character,
                                       input pSourceID as character,
                                       input pNotification as character):
    def buffer ttSysNotification for ttSysNotification.
    create ttSysNotification.
    assign
          ttSysNotification.UID                  = pUID                                     
          ttSysNotification.entityType           = ""
          ttSysNotification.entityID             = ""
          ttSysNotification.refSysNotificationID = 0
          ttSysNotification.notificationStat     = "N"
          ttSysNotification.sourceType           = pSourceType
          ttSysNotification.sourceID             = pSourceID
          ttSysNotification.notification         = pNotification
          ttSysNotification.createDate           = now.
   END METHOD.

  
   METHOD public logical hasNotifications():
    def buffer ttSysNotification for ttSysNotification.
    return can-find(first ttSysNotification).
   END METHOD.
  
   METHOD public void getNotifications(output table ttSysNotification):
   END METHOD.
   /*--- END of Notifications ---*/


   /*--- Events (Message Queue [ApacheMQ] messages) ---*/  
   METHOD private void otherEvent(input pName as char, input pKey as char, input pAction as char, input pMsg as char):
    def buffer ttMqData for ttMqData.
    def var tSeq as int no-undo.
    
    /* It makes no sense to notify of an event that we cannot identify */
    if pName = ""
      or pName = ?
      or pKey = ?
      or pAction = ?
     then return.
    if pMsg = ?
     then pMsg = "Unknown".
     
    tSeq = 1.
    for last ttMqData 
      by ttMqData.seq:
     tSeq = ttMqData.seq + 1.
    end.
    
    create ttMqData.
    ttMqData.seq = tSeq.
    ttMqData.businessObject = pName.
    ttMqData.keyID = pKey.
    ttMqData.action = pAction.
    ttMqData.description = pMsg.
   END METHOD.
  

   METHOD public void createEvent(input pName as char, input pKey as char):
    otherEvent(pName, pKey, "Created", pName + " created with key " + pKey).
   END METHOD.
  
   METHOD public void updateEvent(input pName as char, input pKey as char):
    otherEvent(pName, pKey, "Updated", pName + " modified with key " + pKey).
   END METHOD.
  
   METHOD public void deleteEvent(input pName as char, input pKey as char):
    otherEvent(pName, pKey, "Deleted", pName + " deleted with key " + pKey).
   END METHOD.
  
   METHOD public logical hasEvents():
    def buffer ttMqData for ttMqData.
    return can-find(first ttMqData).
   END METHOD.

   METHOD public void getEvents(output table ttMqData):
   END METHOD.
   /*--- END of Events ---*/
END CLASS.
