DO-MAIN:
do on error undo DO-MAIN, leave DO-MAIN
   on stop undo DO-MAIN, leave DO-MAIN
   on quit undo DO-MAIN, leave DO-MAIN:

  canExec = init().
  if canExec
   then 
    do: if suppliedActionType = "Q" 
         then std-lo = queue().
        else if suppliedActionType = "A" 
         then std-lo = async().
        else
         do: std-lo = act().
             if std-lo
              then render().
         end.
       
    end.
    
  log().
  std-lo = distribute(). 
  if std-lo /* Success */
   then
    do:
        doUserNotifications().
        doMqEvents().
        doEmailAlerts().
    end.
end. /* DO-MAIN */


&IF defined(exclude-debug) = 0
 &THEN
debug().
&ENDIF

&IF defined(exclude-delete) = 0
 &THEN
if valid-object(iResponse) 
 then delete object iResponse no-error.

if valid-object(iRequest)
 then delete object iRequest no-error.

if valid-object(iAction)
 then delete object iAction no-error.
 
publish "DeleteAllTempFiles".
&ENDIF
