/* framework/RequestRun

   Modification:
   
   K.R 02/20/2024 Modified to override default implementation of getPayloadJson and getPayloadXml
 */
using Progress.Json.ObjectModel.JsonObject.
USING Progress.Json.ObjectModel.ObjectModelParser. 

CLASS framework.RequestRun INHERITS framework.RequestBase: 

	DEFINE PRIVATE PROPERTY DataFile AS char PRIVATE Get. PRIVATE Set.
    DEFINE PRIVATE VARIABLE cQueryString AS LONGCHAR NO-UNDO.

    CONSTRUCTOR PUBLIC RequestRun (
		INPUT pUid AS char,
		INPUT pName AS char,
		INPUT pRole AS char,
		INPUT pEmail AS char,
		INPUT pActionID AS char,
		INPUT pQueryString AS char,
		input pDataFile as char,
        input pFieldList as char
		):
	SUPER (
		INPUT pUid,
		INPUT pName,
		INPUT pRole,
		INPUT pEmail,
		INPUT pActionID,
        input pFieldList
		).

     parseQueryString(pQueryString).
	 DataFile = pDataFile.
     cQueryString = pQueryString. /* Increase the scope of the variable*/
    END CONSTRUCTOR.

    METHOD PUBLIC OVERRIDE VOID getContent(INPUT-OUTPUT DATASET-HANDLE pDataSet, OUTPUT pErrMsg AS CHAR):
        DEFINE VARIABLE lSuccess AS LOGICAL   NO-UNDO.
        DEFINE VARIABLE iCount   AS INTEGER   NO-UNDO.
        
        IF NOT VALID-HANDLE(pDataSet) 
         THEN 
          DO:	
            pErrMsg = 'Invalid DataSet Handle'.
            return.
          END.
		file-info:file-name = this-object:DataFile. /*changing . to :*/
		if file-info:full-pathname <> ?
		 then
		  do:
		   if index(file-info:full-pathname, ".xml") > 0
			then lSuccess = pDataset:read-xml("FILE", FILE-INFO:FULL-PATHNAME, "EMPTY", ?, ?) no-error.
			else
		   if index(file-info:full-pathname, ".json") > 0
			then lSuccess = pDataset:read-json("FILE", FILE-INFO:FULL-PATHNAME, "EMPTY") no-error.
		  end.

        IF NOT lSuccess OR ERROR-STATUS:ERROR
         THEN 
          DO:	
            DO iCount = 1 TO ERROR-STATUS:NUM-MESSAGES:
              pErrMsg = pErrMsg + ERROR-STATUS:GET-MESSAGE(iCount) + ' '. 
            END.
          END.
        RETURN.  //this is the void method
    END METHOD.

    METHOD public override void getPayloadJSON(INPUT-OUTPUT oJsonObject as JsonObject, OUTPUT pErrMsg AS CHAR):
        DEFINE VARIABLE lSuccess AS LOGICAL   NO-UNDO.
        DEFINE VARIABLE iCount   AS INTEGER   NO-UNDO.
        define variable oParser            as ObjectModelParser no-undo.

        oParser = new ObjectModelParser ().
        file-info:file-name = this-object:DataFile.

        if file-info:full-pathname = ? 
         then
          do:
            pErrMsg = "Invalid Data File".
            return.
          end.

         oJsonObject = cast(oParser:ParseFile(file-info:full-pathname), JsonObject).
      
        CATCH oError AS progress.Lang.error:
            DO iCount = 1 to oError:NumMessages:
              pErrMsg = pErrMsg + oError:GetMessage(iCount).
            END.
        END CATCH.  
    END METHOD.

    METHOD public override void getPayloadXML(INPUT-OUTPUT hX-Document AS HANDLE, OUTPUT pErrMsg AS CHAR):
        DEFINE VARIABLE lSuccess AS LOGICAL           NO-UNDO.
        DEFINE VARIABLE iCount   AS INTEGER           NO-UNDO.
        DEFINE VARIABLE oParser  AS ObjectModelParser NO-UNDO.
        
        file-info:file-name = this-object:DataFile.
        if file-info:full-pathname = ?
         then
          do:
            pErrMsg = "Invalid Data File".
            return.
          end.

        create x-document hx-document.
        hx-Document:load("FILE", file-info:full-pathname,false) no-error.

        CATCH oError AS progress.Lang.error:
            DO iCount = 1 to oError:NumMessages:
              pErrMsg = pErrMsg + oError:GetMessage(iCount).
            END.
        END CATCH.
    END METHOD.

    METHOD PUBLIC LONGCHAR getPayload():
    /* Yet to be implemented*/
      RETURN "".
    END.

    METHOD PUBLIC LONGCHAR getQueryString():
      RETURN getRequestQueryString(cQueryString).
    END.

    METHOD PUBLIC char getPayloadType():
      file-info:file-name = this-object:DataFile.

	  if file-info:full-pathname = ?
       then return "".

      if index(file-info:full-pathname, ".xml") > 0
        then return "text/xml".
      else if index(file-info:full-pathname, ".json") > 0
       then return "text/json".
    END.

    DESTRUCTOR PUBLIC RequestRun ():
    END DESTRUCTOR.

END CLASS.
