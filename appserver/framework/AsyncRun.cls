/* framework/AsyncRun
 */
 
CLASS framework.AsyncRun:

    {tt/QueryString.i &tableAlias="ttQueryString"}

    DEFINE PRIVATE PROPERTY pcActionID AS CHARACTER NO-UNDO
    PRIVATE GET.
    PRIVATE SET.

    DEFINE PRIVATE PROPERTY pcUID AS CHARACTER NO-UNDO
    PRIVATE GET.
    PRIVATE set.

    DEFINE PRIVATE PROPERTY piRetries AS INTEGER NO-UNDO
    PRIVATE GET.
    PRIVATE SET.

    DEFINE PRIVATE PROPERTY pcPayLoadPath AS CHARACTER NO-UNDO
    PRIVATE GET.
    PRIVATE SET.

    DEFINE PRIVATE VARIABLE cSystemParameter AS CHARACTER NO-UNDO.
    DEFINE PRIVATE VARIABLE cActionParameter AS CHARACTER NO-UNDO.

    CONSTRUCTOR PUBLIC AsyncRun (INPUT pcActionID     AS CHARACTER,
                                 INPUT pcUID         AS CHARACTER,
                                 INPUT piRetries     AS INTEGER,
                                 INPUT pcPayLoadPath AS CHARACTER):

      ASSIGN
        THIS-OBJECT:pcActionID    = pcActionID
        THIS-OBJECT:pcUID         = pcUID
        THIS-OBJECT:piRetries     = piRetries
        THIS-OBJECT:pcPayLoadPath = pcPayLoadPath
      .

    END CONSTRUCTOR.

    METHOD PUBLIC VOID SetActionType(INPUT pcParamValue AS CHARACTER):
      SetSystemParameter("I8", pcParamValue). /* Q = Queue action '' = run action*/
    END METHOD.

    METHOD PUBLIC VOID SetNotifyReq(INPUT pcParamValue AS CHARACTER):
      SetSystemParameter("I8a", pcParamValue). /*Notify the action completetion to requestor*/
    END METHOD.

    METHOD PUBLIC VOID SetDistType(INPUT pcParamValue AS CHARACTER):
      SetSystemParameter("I8b", pcParamValue). /* '(B)oth or '' (B)oth means return the response back to client along with distribution */
    END METHOD.

    METHOD PUBLIC VOID SetBooleanDest(INPUT pcParamValue AS CHARACTER):
      SetSystemParameter("I9", pcParamValue). /*C=configured and S=Supplied*/
    END METHOD.

    METHOD PUBLIC VOID SetEmailAddrList(INPUT pcParamValue AS CHARACTER):
      SetSystemParameter("I9e", pcParamValue). /* Email Address*/
    END METHOD.

    METHOD PUBLIC VOID SetPrinterAddr(INPUT pcParamValue AS CHARACTER):
      SetSystemParameter("I9p", pcParamValue). /* Printer address*/
    END METHOD.

    METHOD PUBLIC VOID SetStorageRoot(INPUT pcParamValue AS CHARACTER):
      SetSystemParameter("I9s", pcParamValue). /* Storage (non-blank = true) */
    END METHOD.

    METHOD PUBLIC VOID SetSaveDir(INPUT pcParamValue AS CHARACTER):
      SetSystemParameter("I9f", pcParamValue). /* Save-to directory */
    END METHOD.
  

    METHOD PUBLIC VOID SetActionParameters(INPUT pcParamName  AS CHARACTER,
                                           INPUT pcParamValue AS CHARACTER):

      CREATE ttQueryString.
      ASSIGN
        ttQueryString.paramName  = pcParamName
        ttQueryString.paramValue = pcParamValue
        ttQueryString.paramType  = 'A'
      .
    END METHOD.

    METHOD PRIVATE VOID SetSystemParameter(INPUT pcParamName  AS CHARACTER,
                                           INPUT pcParamValue AS CHARACTER):
      CREATE ttQueryString.
      ASSIGN
        ttQueryString.paramName  = pcParamName
        ttQueryString.paramValue = pcParamValue
        ttQueryString.paramType  = "S"
        .
    END METHOD.

    METHOD PUBLIC LOGICAL RunAsync ():
      DEFINE VARIABLE std-ch AS CHARACTER NO-UNDO.
      DEFINE VARIABLE std-lo AS LOGICAL   NO-UNDO.

      PUBLISH "GetSystemParameter" FROM (SESSION:FIRST-PROCEDURE) ("DoServiceWrapper", OUTPUT std-ch).
      CreateQueryString().
      std-ch = std-ch + substitute(' &1 &2 "&3" "&4" "&5" "&6"', pcActionID, pcUID, cActionParameter, cSystemParameter, pcPayLoadPath, "Async").

      RUN util/runcommandasync.p(std-ch, piRetries, output std-lo).

      RETURN std-lo.
    END METHOD.

    METHOD PRIVATE VOID CreateQueryString ():
      FOR EACH ttQueryString NO-LOCK:
        IF ttQueryString.paramType = 'S' 
         THEN cSystemParameter = cSystemParameter + SUBSTITUTE("&1=&2?", ttQueryString.ParamName,util.CommonUtils:EncodeUrl(ttQueryString.ParamValue)).
        ELSE IF ttQueryString.paramType = 'A'
         THEN cActionParameter = cActionParameter + SUBSTITUTE("&1=&2?", ttQueryString.ParamName,util.CommonUtils:EncodeUrl(ttQueryString.ParamValue)).
      END.

      ASSIGN
        cSystemParameter = TRIM(cSystemParameter, "?")
        cActionParameter = trim(cActionParameter, "?")
      .
    END METHOD.

    DESTRUCTOR PUBLIC AsyncRun ():
    END DESTRUCTOR.

END CLASS.
