&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* framework/do-file.i
   Functionality to support file delivery (email, print, save)
   D.Sinclair 11.30.2022
 */
 
{framework/sysmailparameter.i &tableAlias=mailparameter}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD fileEmail Include 
FUNCTION fileEmail RETURNS LOGICAL PRIVATE
  ( INPUT pEmail as char,
    INPUT pFile as char,
    OUTPUT pMsg as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD filePrint Include 
FUNCTION filePrint RETURNS LOGICAL PRIVATE
  ( INPUT pPrinter as char,
    INPUT pFile as char, 
    OUTPUT pMsg as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD fileSaveToDirectory Include 
FUNCTION fileSaveToDirectory RETURNS LOGICAL PRIVATE
  ( INPUT pDestination as char,
    INPUT pFile as char, 
    OUTPUT pMsg as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD fileSaveToRepository Include 
FUNCTION fileSaveToRepository RETURNS LOGICAL PRIVATE
  ( INPUT pStorageEntity as char,
    input pStorageEntityID as char, 
    input pCategory as char, 
    INPUT pFile as char, 
    OUTPUT pMsg as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getOsError Include 
FUNCTION getOsError RETURNS CHARACTER PRIVATE
  ( input pCode as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION fileEmail Include 
FUNCTION fileEmail RETURNS LOGICAL PRIVATE
  ( INPUT pEmail as char,
    INPUT pFile as char,
    OUTPUT pMsg as char ) :

    define variable cHTML    as character no-undo initial "".
    define variable cSubject as character no-undo.

    /* Emails may have a custom HTML body template and Subject line */
    cHTML = iEmailHtml.
    if cHTML = "" or cHTML = ?
     then cHTML = "html\queueFinish.html". /* ds: This needs to be a default generic template */

    cHTML = search(cHTML).
    file-info:file-name = cHTML.
    if file-info:full-pathname = ?
     then cHTML = "".
      
    cSubject = iEmailSubject.
    if cSubject = "" or cSubject = ?
     then cSubject = "File from Compass".

    /* ds: Make sure this is the correct parameter name */
    publish "GetSystemParameter" ("NoReplyEmail", output std-ch).
    
    /* Name/value pairs to replace fields in the optional HTML template ands Subject line */
    iAction:getEmailParameters(output table mailparameter).
    run framework/sendhtmlmail.p (input  cHTML,
                                  input  std-ch,
                                  input  pEmail,
                                  input  cSubject,
                                  input  pFile,
                                  input  table mailparameter,
                                  output std-lo).
    if not std-lo
     then pMsg = "Email failed to " + pEmail + " with file " + pFile.
    RETURN std-lo.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION filePrint Include 
FUNCTION filePrint RETURNS LOGICAL PRIVATE
  ( INPUT pPrinter as char,
    INPUT pFile as char, 
    OUTPUT pMsg as char ) :

  &SCOPED-DEFINE SumatraPDF "C:\Progress\OpenEdge\bin\SumatraPDF.exe"

  os-command silent value({&SumatraPDF} + ' -silent -print-to "' + pPrinter + '" ' + pFile). 
  
  if os-error > 0
   then pMsg = "Printing of File " + pFile + " failed to " + pPrinter + " with error " + getOsError(os-error).

  RETURN (os-error = 0). 
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION fileSaveToDirectory Include 
FUNCTION fileSaveToDirectory RETURNS LOGICAL PRIVATE
  ( INPUT pDestination as char,
    INPUT pFile as char, 
    OUTPUT pMsg as char ) :

     pDestination = right-trim(pDestination, "\") + "\".
     os-copy value(pFile) value(pDestination).
     if os-error> 0
      then 
       do: pMsg = "Saving file " + file-info:full-pathname + " to " + pDestination + " failed with error " + getOsError(os-error).
           run util/sysmail.p (suppliedActionID + " saving file failed",
                               pMsg).
       end.    
           
  RETURN (os-error = 0).
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION fileSaveToRepository Include 
FUNCTION fileSaveToRepository RETURNS LOGICAL PRIVATE
  ( INPUT pStorageEntity as char,
    input pStorageEntityID as char, 
    input pCategory as char, 
    INPUT pFile as char, 
    OUTPUT pMsg as char ) :

  if pStorageEntity = ? or pStorageEntity = "" 
     or pStorageEntityID = ? or pStorageEntityId = ""
   then
    do: pMsg = "Unable to save file when respository keys are not defined.".
        run util/sysmail.p (suppliedActionID + " saving file to repository failed",
                            pMsg).
        return false.
    end.
   
  run util\UploadFile.p (suppliedUid, true, pStorageEntity, pStorageEntityId, pCategory, pFile, output std-lo, output pMsg).
  if not std-lo
   then pMsg = "Saving file " + pFile + " to repository failed with error " + pMsg.

  RETURN std-lo.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getOsError Include 
FUNCTION getOsError RETURNS CHARACTER PRIVATE
  ( input pCode as int ) :
  case pCode:
    when 1  then return "Not owner".
    when 2  then return "No such file or directory".
    when 3  then return "Interrupted system call".
    when 4  then return "I/O error".
    when 5  then return "Bad file number".
    when 6  then return "No more processes".
    when 7  then return "Not enough core memory".
    when 8  then return "Permission denied".
    when 9  then return "Bad address".
    when 10 then return "File exists".
    when 11 then return "No such device".
    when 12 then return "Not a directory".
    when 13 then return "Is a directory".
    when 14 then return "File table overflow".
    when 15 then return "Too many open files".
    when 16 then return "File too large".
    when 17 then return "No space left on device".
    when 18 then return "Directory not empty".
    otherwise return "Unknown".
  end case.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

