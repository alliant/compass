/* framework/IActable
 */

INTERFACE framework.IActable: 

    METHOD public void act (input pRequest as framework.IRequestable,
                            input pResponse as framework.IRespondable).
                            
	METHOD public logical distribute (input pRequest as framework.IRequestable).
    METHOD public char render (input pFormat as char, 
                               input pTemplate as char).
    
    METHOD public logical emailContent (input pAddr as char, input pSubject as char, input pBody as char, output pMsg as char).
    METHOD public logical printContent (input pDest as char, output pMsg as char).
    METHOD public logical saveContent (input pDir as char, output pMsg as char).
    METHOD public logical storeContent (input pUID as char, output pMsg as char).
    
    METHOD public void getLogReference(output pType as char, output pNum as char, output pMsg as char).
    
END INTERFACE.
