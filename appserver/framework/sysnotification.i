/* framework/sysnotification.i
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias sysnotification
&ENDIF
   
def temp-table {&tableAlias}
  field sysNotificationID     as integer 
  field UID                   as character    
  field entityType            as character
  field entityID              as character  
  field stat                  as character  /* (S)upress , (N)ew , (D)ismiss */
  field notificationStat      as character  /* (R)ED,(G)REEN,(Y)ellow ds: this is a confusing field name; please rethink */
  field sourceType            as character   
  field sourceID              as character
  field dismissedDate         as datetime
  field notification          as character
  field createDate            as dateTime

  field rowNum                as character
  field refSysNotificationID  as integer 
  field entityName            as character /* Client side only */
  .
