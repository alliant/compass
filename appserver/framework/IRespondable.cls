/* framework/IRespondable
 */

INTERFACE framework.IRespondable:
    /* Business logical error and success structures */
    METHOD PUBLIC void fault(INPUT pCode AS CHAR, INPUT pMessage AS CHAR).
    METHOD PUBLIC void success(INPUT pCode AS CHAR, INPUT pMessage AS CHAR).
    METHOD PUBLIC logical isFault().

    /* Notifications to 0:N users when the action is successful
        SourceType: Claim, Audit, Remittance etc. 
        SourceID: key reference of source type
        Description: readable purpose for the notification
     */
    method public void notifyReAgent(input pAgentID as char, input pSourceType as char, input pSourceID as char, input pDesc as char).
    method public void notifyRePerson(input pPersonID as char, input pSourceType as char, input pSourceID as char, input pDesc as char).
    method public void notifyToEmployee(input pUID as char, input pSourceType as char, input pSourceID as char, input pDesc as char).

    /* MQ messages to 0:N subscribers when the action is successful
        pName: Entity that was impacted and named for listening MQ clients
        pKey:  Key reference of pName entity
    */
    method public void createEvent(input pName as char, input pKey as char).  
    method public void updateEvent(input pName as char, input pKey as char).
    method public void deleteEvent(input pName as char, input pKey as char).
END INTERFACE.
