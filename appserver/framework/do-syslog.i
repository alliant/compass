/* ************************  Function Prototypes ********************** */
FUNCTION log RETURNS LOGICAL PRIVATE
  (  )  FORWARD.



/* ************************  Function Implementations ***************** */
FUNCTION log RETURNS LOGICAL PRIVATE
  (  ) :
     def var tRefType as char init "" NO-UNDO.
     def var tRefNum as char init "" NO-UNDO.
     def var tSysMessage as char init "None" NO-UNDO.
           
     if iIsLog = false
       or not CONNECTED("COMPASS") 
      then return false.

     if valid-object(iAction)
      then iAction:getLogReference(OUTPUT tRefType, OUTPUT tRefNum, OUTPUT tSysMessage).
     
     if valid-object(iResponse) and iResponse:isFault()
      then iResponse:getFault(output iFaultCode, output iFaultMessage).

     run framework/addsyslog.p
            (suppliedActionID,
             suppliedUid,
             iUserRole,
             suppliedIpAddress,
             if suppliedActionType = "Q" then iQueueHandler else iActionHandler,
             suppliedClientID,
             iFaultCode <> "",
             iFaultCode,
             iFaultMessage,
             tRefType,
             tRefNum,
             tSysMessage,
             if valid-object(iRequest) then iRequest:getQueryString() else '',
             if valid-object(iRequest) then iRequest:getPayload() else '',
             if valid-object(iRequest) then iRequest:getPayloadType () else ''
             ).
     RETURN TRUE.
END FUNCTION.
