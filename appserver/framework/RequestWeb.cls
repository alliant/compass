/* framework/RequestWeb
   
   Modification:
   
   K.R 02/20/2024 Modified to override default implementation of getPayloadJson and getPayloadXml
   S.B 06/25/2024 Task # 113923 - Modified to get system parameters (I8,I9,I10)
 */
using Progress.Json.ObjectModel.JsonObject.
USING Progress.Json.ObjectModel.ObjectModelParser.

CLASS framework.RequestWeb INHERITS framework.RequestBase:
    DEFINE PRIVATE VARIABLE cQueryString AS LONGCHAR NO-UNDO. 

    CONSTRUCTOR PUBLIC RequestWeb (
		INPUT pUid AS char,
		INPUT pName AS char,
		INPUT pRole AS char,
		INPUT pEmail AS char,
		INPUT pActionID AS char,
		INPUT pQueryString AS char,
        input pFieldList as char
		):
	SUPER (
		INPUT pUid,
		INPUT pName,
		INPUT pRole,
		INPUT pEmail,
		INPUT pActionID,
        input pFieldList
		).
     
     parseQueryString(pQueryString).
     parseSystemQueryString(pQueryString).
     cQueryString = pQueryString. /* To increase the scope of the variable*/
    END CONSTRUCTOR.

    METHOD PUBLIC OVERRIDE VOID getContent(INPUT-OUTPUT DATASET-HANDLE pDataSet, OUTPUT pErrMsg AS CHAR):
        DEFINE VARIABLE lSuccess AS LOGICAL   NO-UNDO.
        DEFINE VARIABLE iCount   AS INTEGER   NO-UNDO.
        IF NOT VALID-HANDLE(pDataSet) 
         THEN 
          DO:	
            pErrMsg = 'Invalid DataSet Handle'.
            return.
          END.
		if WEB-CONTEXT:IS-XML
		 then lSuccess = pDataSet:read-xml("HANDLE", WEB-CONTEXT, "EMPTY", ?, ?) NO-ERROR.
        else
		if WEB-CONTEXT:IS-JSON
		 then lSuccess = pDataSet:read-json("HANDLE", WEB-CONTEXT, "EMPTY") NO-ERROR.
      
        IF NOT lSuccess OR ERROR-STATUS:ERROR
         THEN 
          DO:	
            DO iCount = 1 TO ERROR-STATUS:NUM-MESSAGES:
              pErrMsg = pErrMsg + ERROR-STATUS:GET-MESSAGE(iCount) + ' '. 
            END.
          END.
        RETURN.  
    END METHOD.

    METHOD public override void getPayloadJSON(INPUT-OUTPUT oJsonObject as JsonObject, OUTPUT pErrMsg AS CHAR):
        DEFINE VARIABLE lSuccess AS LOGICAL           NO-UNDO.
        DEFINE VARIABLE iCount   AS INTEGER           NO-UNDO.
        DEFINE VARIABLE oParser  AS ObjectModelParser NO-UNDO.
        
        IF NOT VALID-OBJECT (oJsonObject) 
         THEN
          DO:
            pErrMsg = "Invalid Json object passed to method".
            RETURN.
          END.
        oParser = new ObjectModelParser ().
		IF WEB-CONTEXT:IS-JSON
		 THEN oJsonObject = CAST(oParser:Parse(WEB-CONTEXT), JsonObject).    

        CATCH oError as progress.Lang.error:
            do iCount = 1 to oError:NumMessages:
              pErrMsg = pErrMsg + oError:getmessage(iCount).
            end.
        END catch.
        FINALLY:
            if valid-object (oParser) 
             then delete object oParser.
        END.
  
    END METHOD.

    METHOD public override void getPayloadXML(INPUT-OUTPUT hX-Document AS HANDLE, OUTPUT pErrMsg AS CHAR):
        DEFINE VARIABLE lSuccess AS LOGICAL   NO-UNDO.
        DEFINE VARIABLE iCount   AS INTEGER   NO-UNDO.
        
        if not valid-handle (hx-Document) 
         then
          do:
            pErrMsg = "Invalid X-Document handle passed to method".
            return.
          end.

		if WEB-CONTEXT:IS-XML
		 then hx-Document = WEB-CONTEXT:X-DOCUMENT.
       
        CATCH oError AS progress.Lang.error:
            DO iCount = 1 TO oError:NumMessages:
              pErrMsg = pErrMsg + oError:GetMessage(iCount).
            END.
        END CATCH.
         
    END METHOD.

    METHOD PUBLIC LONGCHAR getPayload():
      DEFINE VARIABLE temp-lc AS LONGCHAR NO-UNDO.
      define variable oJsonResponse as JsonObject no-undo.
      define variable hx-Document   as handle     no-undo.
      define variable cErrMsg       as character  no-undo.

      if web-context:is-json
       then
        do:
          oJsonResponse = new JsonObject ().
          getPayloadJSON(input-output oJsonResponse, output cErrMsg).
          if cErrMsg = '' 
           then oJsonResponse:write(temp-lc).
        end.
       else if web-context:is-xml then
        do:
          create x-document hx-Document.
          getPayloadXML(input-output hx-Document, output cErrMsg).
          if cErrMsg = ''
           then hx-Document:save("longchar", temp-lc).
        end.

      if valid-object (oJsonResponse) 
       then delete object oJsonResponse.
      if valid-handle (hx-Document) 
       then delete object hx-Document.
      RETURN temp-lc.
    end.

    METHOD PUBLIC LONGCHAR getQueryString():
      RETURN getRequestQueryString(cQueryString).
    END.

    METHOD PUBLIC char getPayloadType():
      if not valid-handle(web-context) 
        then return "".

      if web-context:is-json 
       then return "text/json".
      if web-context:is-xml
       then return "text/xml".
    END.

    DESTRUCTOR PUBLIC RequestWeb ():
    END DESTRUCTOR.

END CLASS.
