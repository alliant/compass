
/*------------------------------------------------------------------------
@name  GetAllEmails
@action emailsAllGet
@description Returns the list of emails
@returns Success;2000;
@author K.R
@version 1.0
@created 09.01.2023
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/


class mail.GetAllEmails inherits framework.ActionBase:
  
  {tt/mailboxitems.i &tableAlias="ttMailboxItems"}
  constructor public GetAllEmails ():
   super().
  end constructor.

  destructor public GetAllEmails ( ):
  end destructor.

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):


    define variable pcUserName    as character          no-undo.
    define variable plSuccess     as logical            no-undo.
    define variable pcErrMessage  as character          no-undo.
    define variable dsHandle      as handle             no-undo.
    define variable oAsyncRun     as framework.AsyncRun no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = "data".
    dsHandle:set-buffers(buffer ttMailboxItems:handle).

    pRequest:getParameter(input "UserName", output pcUserName).

    for first agentfile where agentfile.agentfileID = integer(pcUserName) no-lock:
      if not agentfile.hasmailbox 
       then
        do:
          oAsyncRun = new framework.AsyncRun ("emailBoxCreate", pRequest:UID, 1, "").
          oAsyncRun:setActionParameters("UserName", pcUserName).
          oAsyncRun:runAsync().

          if valid-object (oAsyncRun) 
            then delete object oAsyncRun no-error.

          return.
        end.
    end.
    run mail/getallemails-p.p(input  pcUserName, 
                            output table ttMailboxItems,
                            output plSuccess, 
                            output pcErrMessage).

    if not plSuccess
    then
     do:
       pResponse:fault("3005", "Reading Mailbox Failed : " + pcErrMessage).
       return.
     end.
    if temp-table ttMailboxItems:has-records 
     then SetContentDataset(input dataset-handle dshandle, input pRequest:FieldList). 
    pResponse:success("2000","Read Mailbox").
     
  end method.

end class.




                                                              
