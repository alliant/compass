
/*------------------------------------------------------------------------
@name  GetEmail
@action emailGet
@description Read a particular mail of a mailbox
@returns Success;2000;
@author K.R
@version 1.0
@created 08.28.2023
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/


class mail.GetEmail inherits framework.ActionBase:


  constructor public GetEmail ():
   super().
  end constructor.

  destructor public GetEmail ( ):
  end destructor.

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    
    define variable pcUserName        as character no-undo.
    define variable pcMessageUID       as character no-undo.
    define variable plSuccess          as logical   no-undo.
    define variable pcErrMessage       as character no-undo.
    define variable pcMailJsonFilePath as character no-undo.

    pRequest:getParameter(input "UserName", output pcUserName).
    pRequest:getParameter(input "MessageUID", output pcMessageUID).
    
    run mail/getemail-p.p(input pcUserName, input pcMessageUID, output pcMailJsonFilePath, output plSuccess, output pcErrMessage).

    if not plSuccess 
     then
      do:
        pResponse:fault("3005" ,"Fetching Email Failed :" + pcErrMessage).
        return.
      end.

    file-info:file-name = pcMailJsonFilePath.

    if file-info:full-pathname <> ? and file-info:file-size > 0
     then tContentFile = file-info:full-pathname.

    pResponse:success("2000","Reading Email").
     
  end method.

end class.




