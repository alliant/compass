
/*------------------------------------------------------------------------
@name  UploadEmail.cls
@action emailsUpload
@description upload emails to mail server
@returns Success;2000;
@author K.R
@version 1.0
@created 03.20.2024
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/
USING Progress.Json.ObjectModel.JsonObject FROM PROPATH.

class mail.UploadEmail inherits framework.ActionBase:

  constructor public UploadEmail ():
   super().
  end constructor.

  destructor public UploadEmail ( ):
  end destructor.

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):


    define variable oEmail               as JsonObject no-undo.
    define variable cMsg                 as character  no-undo.
    define variable pcUserName           as character  no-undo.
    define variable cEMailType           as character  no-undo.
    define variable cEncodedEmailContent as longchar   no-undo.
    define variable cDecodedEmailContent as memptr     no-undo.
    define variable cEmailFilePath       as character  no-undo.
    define variable lSuccess             as logical    no-undo.
    define variable cErrMessage          as character  no-undo.
    define variable oEmailFileParser     as util.EmailFileParser no-undo.
    define variable cEmailErrFilePath    as character  no-undo.
    define variable cDomainName          as character  no-undo.
    define variable cUserEmailID         as character  no-undo.

    oEmail = new JsonObject ().
    pRequest:getPayloadJson(input-output oEmail, output cMsg).

    if cMsg > '' 
     then
      do:
        pResponse:fault('3005', "Data Parsing failed. Reason: " + cMsg).
        return.
      end.

    pRequest:getParameter(input "UserName", output pcUserName).
    if pcUserName = ? or pcUserName = ""
     then
      do:
        pResponse:fault('3005', "User name cannot be blank").
        return.
      end.

    publish "GetSystemParameter" from (session:first-procedure) ("JamesMailDomain", output cDomainName).
    if cDomainName = ? or cDomainName = '' 
     then
      do:
        SendErrorMail("Configuration Error",
                     "JamesMailDomain entry is missing or has invalid value in Compass ini file",
                     ""
                    ).


        return.

      end.
    cUserEmailID = pcUserName + cDomainName.

    assign
      cEncodedEmailContent = oEmail:getlongchar("MailFileContent")
      cEmailType           = oEmail:getcharacter("MailFileType")
      cDecodedEmailContent = base64-decode(cEncodedEmailContent)
      .

    cEmailFilePath = "".
    publish "GetTempDir" from (session:first-procedure) (output cEmailFilePath).
    if cEmailFilePath = ""
     then publish "GetSystemParameter" from (session:first-procedure) ("dataRoot", OUTPUT cEmailFilePath).
    cEmailFilePath = cEmailFilePath + guid + cEMailType.

    copy-lob cDecodedEmailContent to file cEmailFilePath.
    set-size(cDecodedEmailContent) = 0.
    publish "AddTempFile" from (session:first-procedure) ("UploadEMailFile", cEmailFilePath).

    oEmailFileParser = new util.EmailFileParser (input cEmailType, input cEmailFilePath).
    oEmailFileParser:parse(output cMsg, output cEmailErrFilePath, output lSuccess).


    if not lSuccess 
     then
      do:
        
        file-info:file-name = cEmailErrFilePath.
        if file-info:full-pathname <> ? and file-info:file-size > 0 
         then
          do:
            SendErrorMail("Error uploading email to :" + cUserEmailId,
                           cMsg,
                           cEmailErrFilePath).
            pResponse:fault("3005", "Uploading Email Failed").
          end.

        else
          pResponse:fault("3005", "Uploading Email Failed " + cMsg).
        return.
      end.

    run mail/sendemail-p.p("",
                           cUserEmailID,
                           oEmailFileParser:EmailSender,
                           oEmailFileParser:EmailRecipients,
                           oEmailFileParser:EmailSubject,
                           oEmailFileParser:EmailSentDate,
                           oEmailFileParser:EmailBody,
                           oEmailFileParser:EmailAttachmentList,
                           output lSuccess,
                           output cMsg).

    pResponse:success("2000", "Uploading Email").

    if valid-object (oEmail) 
     then delete object oEmail.
    if valid-object(oEmailFileParser) 
     then delete object oEmailFileParser.
  end method.

  method private void SendErrorMail  (
                                     input pcSubject as character,
                                     input pcMailBody   as character,
                                     input pcAttachment as character
                                     ):

    define variable cAddr as character no-undo.
    define variable cFrom as character no-undo.
    
    publish "GetSystemParameter" from (session:first-procedure) ("AlertsTo", output cAddr).

    publish "GetSystemParameter" from (session:first-procedure) ("AlertsFrom", output cFrom).
    if cFrom = "" 
     then publish "GetSystemParameter" from (session:first-procedure) ("emailDefaultFrom", output cFrom).

    if pcAttachment <> "" 
     then run util/attachmail.p (cFrom, 
                                 cAddr, 
                                 "", 
                                 pcSubject, 
                                  pcMailBody, 
                                 pcAttachment).
    else
      run util/sysmail.p(pcSubject,  pcMailBody). 

    
  end.

end class.




