
/*------------------------------------------------------------------------
@name  CreateEmailBox
@action emailBoxCreate
@description create mailbox
@returns Success;2000;
@author K.R
@version 1.0
@created 08.28.2023
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/


class mail.CreateEmailBox inherits framework.ActionBase:

  constructor public CreateEmailBox ():
   super().
  end constructor.

  destructor public CreateEmailBox ( ):
  end destructor.

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):


    define variable pcUserName    as character no-undo.
    define variable pcPassword    as character no-undo.
    define variable lSuccess      as logical   no-undo.
    define variable cErrMessage   as character no-undo.

    pRequest:getParameter(input "UserName", output pcUserName).
    pRequest:getParameter(input "Password", output pcPassword).
    
    run mail/createemailbox-p.p(pcUserName, pcPassword, output lSuccess, output cErrMessage).

    if not lSuccess 
     then
      do:
        pResponse:fault("3005", "Creating MailBox Failed :" + cErrMessage).
        return.
      end.

    TRX-BLOCK:
    for first agentfile fields(hasmailbox) exclusive-lock
      where agentfile.agentfileID = integer(pcUserName) transaction
       on error undo TRX-BLOCK, leave TRX-BLOCK:
        agentfile.hasmailbox = true.
    end.
    pResponse:success("2000","Creating MailBox").
     
  end method.

end class.




