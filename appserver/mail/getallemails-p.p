/*------------------------------------------------------------------------
@name mail/getallemails-p.p
@description wrapper procedure for getallemails
@author K.R
@created 09.04.2023
Modification:
Date          Name             Description 
----------------------------------------------------------------------*/
{tt/mailboxitems.i &tableAlias="ttMailboxItems"}

define input  parameter pcUserName as character no-undo.
define output parameter table for ttMailboxItems.
define output parameter plSuccess     as logical   no-undo initial true.
define output parameter pcErrorMsg    as character no-undo.

define variable cGetAllEmailsProgram as character no-undo.
define variable cProgramName        as character no-undo.

error-status:error = false.
if pcUserName = ?  or pcUserName = ''
 then
  do:
    assign
      plSuccess = false
      pcErrorMsg = "User name cannot be blank"
    .
    return.
  end.

publish "GetSystemParameter" ("GetAllEmailsProgram", output cProgramName).

cGetAllEmailsProgram = search(cProgramName).
if search(cProgramName + ".p") = ? and search(cProgramName + ".r") = ? 
 then
  do:
    assign
      plSuccess = false
      pcErrorMsg = "Configuration Error"
    .
    run util/sysmail.p("Configuration Error",
                       "GetAllEmailsProgram entry is missing or has invalid value in Compass ini file").
    return.
  end.

cProgramName = cProgramName + ".p".
run value(cProgramName) (pcUserName,
                         output table ttMailboxItems,
                         output plSuccess, 
                         output pcErrorMsg) no-error.

if error-status:error = true 
 then
  do:
    assign
        plSuccess = false
        pcErrorMsg = "Something went wrong while fetching email list. Please contact system administrator"
    .
    run util/sysmail.p("Procedure Error",
                       "Error running GetAllEmailsProgram:" + error-status:get-message(1) + " " + error-status:get-message(2)).
  end.






