
/*------------------------------------------------------------------------
@name  GetEmailFile
@action emailFileGet
@description Downloads an attachment of a email
@returns Success;2000;
@author K.R
@version 1.0
@created 10.06.2023
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/


class mail.GetEmailFile inherits framework.ActionBase:


  constructor public GetEmailFile ():
   super().
  end constructor.

  destructor public GetEmailFile ( ):
  end destructor.

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    
    define variable pcUserName           as character no-undo.
    define variable pcMessageID          as character no-undo.
    define variable pcAttachmentName     as character no-undo.
    define variable plSuccess            as logical   no-undo.
    define variable pcErrMessage         as character no-undo.
    define variable pcAttachmentFilePath as character no-undo.

    pRequest:getParameter(input "UserName", output pcUserName).
    pRequest:getParameter(input "MessageUID", output pcMessageID).
    pRequest:getParameter(input "AttachmentName", output pcAttachmentName).
    
    run mail/getemailfile-p.p(input pcUserName, 
                              input pcMessageID, 
                              input pcAttachmentName, 
                              output pcAttachmentFilePath, 
                              output plSuccess, 
                              output pcErrMessage).

    if not plSuccess 
     then
      do:
        pResponse:fault("3005" ,"Getting attachment failed :" + pcErrMessage).
        return.
      end.

    file-info:file-name = pcAttachmentFilePath.

    if file-info:full-pathname <> ? and file-info:file-size > 0
     then tContentFile = file-info:full-pathname.

    pResponse:success("2000","GEtting Email Attachment").
     
  end method.

end class.




