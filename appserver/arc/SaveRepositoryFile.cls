/*------------------------------------------------------------------------
@name SaveRepositoryFile.cls
@action repositoryFileSave
@description  Save the arc repository file.

@throws 3000;Save the ARC Repository File failed
@returns Success;2000;
@author Shefali
@version 1.0
@created 19.09.2024

@Modified :
Date        Name          Comments 

------------------------------------------------------------------------*/

class arc.SaveRepositoryFile inherits framework.ActionBase:
 
  constructor public SaveRepositoryFile ():
    super().
  end constructor.

  destructor public SaveRepositoryFile ():
  end destructor.

  {tt/repository.i &tableAlias="document"}
  {tt/repository.i &tableAlias="CompassDocument" &noSerializeHidden}

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    
    define variable dsHandleInput     as handle    no-undo.
    define variable dsHandleOutput    as handle    no-undo.

    define variable lFileSaved        as logical   no-undo.
    define variable pMsg              as character no-undo.

    /* Include Files */
    {lib/std-def.i}

    /* dataset creation */
    create dataset dsHandleInput.
    dsHandleInput:serialize-name = 'data'.
    dsHandleInput:set-buffers(buffer document:handle). 

    /* dataset creation */
    create dataset dsHandleOutput.
    dsHandleOutput:serialize-name = 'data'.
    dsHandleOutput:set-buffers(buffer CompassDocument:handle).

    empty temp-table document.

    /*dataset get*/
    pRequest:getContent(input-output dataset-handle dsHandleInput, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.

    if not can-find(first document) 
     then
      do:
        pResponse:fault('3005', 'Document record not provided').
        return.
      end.

    run arc\saverepositoryfile-p.p (input dataset-handle dsHandleInput, 
                                    output dataset-handle dsHandleOutput,
                                    output lFileSaved, 
                                    output pMsg).

    if not lFileSaved
     then
      do:
        pResponse:fault("3005", "Save ARC repository file failed." + pMsg).
        return.
      end.

    if can-find(first CompassDocument) then
      setcontentdataset(input dataset-handle dsHandleOutput, input pRequest:FieldList).

    pResponse:success("2000", "Save Repository File").

  end method.

end class.
