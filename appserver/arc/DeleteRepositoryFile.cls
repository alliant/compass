/*------------------------------------------------------------------------
@name DeleteRepositoryFile.cls
@action repositoryFileDelete
@description  Delete the arc repository file.

@throws 3000;Delete ARC Repository File failed
@returns Success;2000;
@author Shefali
@version 1.0
@created 19.09.2024

@Modified :
Date        Name          Comments 

------------------------------------------------------------------------*/

class arc.DeleteRepositoryFile inherits framework.ActionBase:
 
  constructor public DeleteRepositoryFile ():
    super().
  end constructor.

  destructor public DeleteRepositoryFile ():
  end destructor.

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    define variable lFileDeleted     as logical   no-undo.
    define variable pMsg             as character no-undo.

    define variable cID              as character no-undo.

    pRequest:getparameter("ID", output cID).

    run arc\deleterepositoryfile-p.p (input cID,
                                      output lFileDeleted, 
                                      output pMsg).

    if not lFileDeleted
     then
      do:
        pResponse:fault("3005", "Delete ARC repository file failed." + pMsg).
        return.
      end.

    pResponse:success("2000", "Delete Repository File").

  end method.

end class.
