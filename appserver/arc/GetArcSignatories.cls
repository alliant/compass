/*------------------------------------------------------------------------
@name  GetArcSignatories.cls
@action arcSignatoriesGet
@description  Get the ARC signatories for an office.
@returns Success;2005
@author K.R
@version 1.0
@created 15.03.2023
@Modified :
Name          Date            Comments
Sagar k      3-27-20223       changed as per new framework
----------------------------------------------------------------------*/

class arc.GetArcSignatories inherits framework.ActionBase:

  constructor public GetArcSignatories ():
   super().
  end constructor.

  destructor public GetArcSignatories ( ):
  end destructor.
    

  {lib/callsp-defs.i}
  define variable pResponseFile as character no-undo.

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
    
    define variable pError        as logical   no-undo.
    define variable pErrorMsg     as character no-undo.
    define variable pOfficeID     as character no-undo.
    define variable pUserID       as character no-undo.
    define variable dsHandle      as handle    no-undo.
    define variable pCount        as integer   no-undo.

    pRequest:getParameter("OfficeID", output pOfficeID).
    pRequest:getParameter("UserID", output pUserID).
    
    run arc/getarcsignatories-p.p(input  pOfficeID,
                         input  pUserID,
                         output pError,
                         output pErrorMsg,
                         output pCount,
                         output dataset-handle dsHandle).

    if pError 
     then
      do:
       pResponse:fault ("3005",  "GetArcSignatories Failed: " + pErrorMsg).
       return.
      end.
    
 
    if pCount > 0 
     then
      setcontentdataset(input dataset-handle dsHandle,input pRequest:FieldList).

    pResponse:success("2000", string(pCount) + "Signatory").
         
  end method.

end class.



