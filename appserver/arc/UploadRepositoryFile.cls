/*------------------------------------------------------------------------
@name UploadRepositoryFile.cls
@action repositoryFileUpload
@description  Upload file to arc repository.

@param pPrivate;logical
@param Entity;character
@param EntityID;character
@param Category;character
@param File;character

@throws 3000;Upload File to ARC Repository
@returns Success;2000;
@author Shefali
@Modified :
Date        Name          Comments 
10/09/2024  SRK           Modified to get encoded file 
------------------------------------------------------------------------*/

class arc.UploadRepositoryFile inherits framework.ActionBase:
 
  {tt/ttClob.i}
  constructor public UploadRepositoryFile ():
    super().
  end constructor.

  destructor public UploadRepositoryFile ():
  end destructor.
 
  define variable pFile     as character no-undo.
  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    /* Variables */
    define variable pUser     as character no-undo.
    define variable pPrivate  as logical   no-undo.
    define variable pEntity   as character no-undo.
    define variable pEntityID as character no-undo.
    define variable pCategory as character no-undo.
    

    define variable lFileStored  as logical   no-undo.
    define variable pMsg         as character no-undo.
    define variable dsHandle     as handle    no-undo.
    define variable dsHandle1    as handle    no-undo.
    define variable mFile        as memptr    no-undo.
    define variable lccontent    as longchar  no-undo.
    define variable cFileType    as character no-undo. 

    /* Include Files */
    {lib/std-def.i}
    

    /*Parameters get*/
    pRequest:getParameter('UserID',    output pUser).
    pRequest:getParameter('isPrivate', output pPrivate).
    pRequest:getParameter('entity',    output pEntity).
    pRequest:getParameter('entityID',  output pEntityID).
    pRequest:getParameter('category',  output pCategory).
    pRequest:getParameter('filename',  output pFile).

    create dataset dsHandle1.
    dsHandle1:serialize-name = 'data'.
    dsHandle1:set-buffers(buffer ttClob:handle).

    pRequest:getContent(input-output dataset-handle dsHandle1, output std-ch).
    if std-ch > ''
     then
      do:
       pResponse:fault('3005', "Json Parsing failed. Reason: " + std-ch).
       return.
     end.

    for first ttclob no-lock :
      cFileType = entry(num-entries(ttclob.filename,"."), ttclob.filename, "." ) .
      COPY-LOB FROM ttclob.lcValue TO lccontent.
      mFile = base64-decode(lccontent).
      GetFileName().
      COPY-LOB mFile to file pFile.
    end.


    run arc\uploadrepositoryfile-p.p (pUser, pPrivate, pEntity, pEntityID, pCategory, pFile, output dataset-handle dsHandle, output lFileStored, output pMsg).

    if not lFileStored
     then
      do:
        pResponse:fault("3005", "Upload File to ARC Repository failed:" + pMsg).
        return.
      end.

    setcontentdataset(input dataset-handle dsHandle, input pRequest:FieldList).
    pResponse:success("2000", "Repository File Upload").
  end method.

  method private void GetFileName ():

    define variable cTempFolder as character no-undo.
    publish "GetSystemParameter" from (session:first-procedure) ("TempFolder", output cTempFolder).

    if cTempFolder = ? or cTempFolder = ""
     then
      cTempFolder = os-getenv("TEMP").
  
    if cTempFolder = ? or cTempFolder = ""
     then
      cTempFolder = os-getenv("TMP").
  
    if cTempFolder = ? or cTempFolder = ""
     then
      cTempFolder = "d:\temp\".

    pFile = substring(pFile, r-index(pFile, "\") + 1).

    pFile = cTempFolder + "\" + pFile.

  end method.

end class.
