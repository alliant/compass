&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file cplbatchreport.p
@description Sends the CPLs to the ARC for marking as the batch

@param BatchID;integer;The batch number to send the policies

@author John Oliver
@created 12.04.2019
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

/* Parmaeters ---                                                       */
define variable pBatchID as integer no-undo.

/* Temp Tables ---                                                      */

/* Local Variables ---                                                  */
{lib/std-def.i}

/* Functions ---                                                        */
{lib/add-delimiter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

pRequest:getParameter("BatchID", output pBatchID).

/* validation */
if not can-find(first batch where batchID = pBatchID)
 then pResponse:fault("3066", "Batch " + string(pBatchID)).
 
if pResponse:isFault()
 then return.
       
run arc/cplbatchreport.p (pBatchID, output std-lo, output std-ch).

if not std-lo
 then pResponse:fault("3005", std-ch).
 else pResponse:success("2019", std-ch).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

