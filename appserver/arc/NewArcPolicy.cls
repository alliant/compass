/*------------------------------------------------------------------------
@name  NewArcPolicy.cls
@action arcPolicyNew
@description Creates a new Policy in the ARC
@returns Success;2005
@author Sachin Chaturvedi
@version 1.0
@created 24.04.2023
@Modified :
Date        Name          Comments   
02/27/2024  SR            Added validation on job type policy.
03/13/2024  SR            Modified to create new order if order is fulfilled.
03/20/2024  SR            Modified logic to create new order if order is fulfilled 
                          and code alignment.
04/16/2024  K.R          Modified to create owner and lender type filecontent
04/28/2024  K.R          Modified to also store other field data of filecontent
11/20/2024  SRK          Modified to assign fulfilsoftware value
----------------------------------------------------------------------*/


class arc.NewArcPolicy inherits framework.ActionBase:

  /* Temp Tables */
  {tt/arcpolicy.i            &tableAlias="ttPolicy" &serializename="Policy"}
  {tt/arcpolicyendorsement.i &tableAlias="PolicyEndorsement" }
  {tt/arcpolicyresult.i      &tableAlias="PolicyResult"}
  {lib/removeNullValues.i}

  &scoped-define OWNER-POLICY 'O'
  &scoped-define LENDER-POLICY 'L'

  constructor public NewArcPolicy ():
   super().
  end constructor.

  destructor public NewArcPolicy ( ):
  end destructor.

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
   
    {lib/std-def.i}
    {lib/nextkey-def.i}
    define variable lSuccess        as logical    no-undo. 
    define variable lReturnRecord   as logical    no-undo.
    define variable dsHandle        as handle     no-undo.
    define variable dsHandleOutput  as handle     no-undo.
    define variable pError          as logical    no-undo. 
    define variable pErrormsg       as character  no-undo.
    define variable piAgentFileID   as integer    no-undo.

    define buffer   bufJob         for job.
    define buffer   bfJob          for job.
    define variable iJobOrderID        as integer   no-undo.
    define variable iNewsuffix         as integer   no-undo initial 1.
    define variable lOwnerPolicyExist  as logical no-undo.
    define variable lLenderPolicyExist as logical no-undo.


    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttPolicy:handle,
                         buffer PolicyEndorsement:handle ). 

    create dataset dsHandleOutput.
    dsHandleOutput:serialize-name = 'data'.
    dsHandleOutput:set-buffers(buffer PolicyResult:handle).
    /* get Dataset */
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.
      
      /* get and validate variables */
    pRequest:getParameter("AgentFileID", output piAgentFileID). 
    if not can-find(first agentFile where agentFile.agentFileID = piAgentFileID) 
     then
      do: 
        pResponse:fault("3066", "Agent File").
        return.
      end.

    if not can-find(first job where job.agentfileID = piAgentFileID and job.jobtype = "Policy")
     then
      do:
        pResponse:fault("3005", "Atleast one open order of type Policy must exist.").
        return.
      end.

    if can-find(first ttPolicy where ttPolicy.policyType = {&OWNER-POLICY}) 
     then lOwnerPolicyExist = true.
    if can-find(first ttPolicy where ttPolicy.policyType = {&LENDER-POLICY})
     then lLenderPolicyExist = true.

    for last job no-lock where job.agentfileID = piAgentFileID and job.jobtype = "Policy" by job.suffix:
      if job.stat ='X' 
       then
        do:
          pResponse:fault("3005", "Atleast one open order of type Policy must exist.").
          return.
        end.

      if job.stat = 'F'
       then
        do:
          for last bufJob no-lock where bufJob.agentfileid = piAgentFileID by bufJob.suffix:
            assign iNewsuffix = bufJob.suffix + 1.
          end.
         
          TRX-BLOCK:
          do transaction 
             on error undo TRX-BLOCK, leave TRX-BLOCK:
              
             create bfJob.
             {lib/nextkey.i &type='orderID' &var=iJobOrderID &err="undo TRX-BLOCK, leave TRX-BLOCK."}
             assign
                 bfJob.agentFileID     = piAgentFileID
                 bfJob.orderID         = iJobOrderID
                 bfJob.suffix          = iNewsuffix
                 bfJob.customerOrder   = job.customerOrder
                 bfJob.productID       = job.productID
                 bfJob.stat            = 'N'
                 bfJob.dueDate         = job.dueDate
                 bfJob.estimatedDate   = job.estimatedDate
                 bfJob.quotedPrice     = job.quotedPrice
                 bfJob.contactName     = job.contactName
                 bfJob.contactEmail    = job.contactEmail
                 bfJob.contactPhone    = job.contactPhone
                 bfJob.effectiveDate   = job.effectiveDate
                 bfJob.notes           = job.notes
                 bfJob.createdDate     = now
                 bfJob.createdBy       = pRequest:uid
                 bfJob.agentProduct    = job.agentProduct
                 bfJob.jobtype         = 'Policy'
                 bfJob.fulFillSoftware = Job.fulFillSoftware
                 .
             
               /* Set Default Values */
             assign
                 bfJob.cancelReason     = ""
                 bfJob.cancelBy         = ""
                 bfJob.renderDate       = ?
                 bfJob.renderBy         = ""
                 bfJob.cancelPrice      = 0.00
                 bfJob.fulFilledBy      = ""
                 bfJob.lastModifiedBy   = ""
                 bfJob.startdate        = ?
                 bfJob.canceldate       = ?
                 bfJob.fulfilleddate    = ? 
                 bfJob.lastmodifieddate = ?
                 .

             validate bfJob.
             release bfJob.
          end. /* do end transaction */
        end.  
    end. /* for last job */

    lSuccess = false.

    run arc/newarcpolicy-p.p(input piAgentFileID,
                             input  table ttPolicy,
                             input  table PolicyEndorsement,
                             output table PolicyResult,
                             output pError,
                             output pErrorMsg).

    if pError
     then
      do:
        pResponse:fault("3005", "NewArcPolicy Failed: " + pErrorMsg).
        return.
      end.

    if lOwnerPolicyExist 
     then CreateOwnerFileContent (piAgentFileID, pRequest:UID).
    if lLenderPolicyExist
     then CreateLenderFileContent (piAgentFileID, pRequest:UID).

    setContentDataset(input dataset-handle dsHandleOutput, input pRequest:FieldLIst).

    pResponse:success("2000", "ArcPolicy").        
  end method.


  method private void CreateOwnerFileContent (input piAgentFileID as integer,
                                              input ipcUID        as character):
    
    define variable cBuyerDesc as character no-undo.
    define variable cException  as character no-undo.
    define variable cLongLegal  as longchar  no-undo.

    define buffer bfFileContent    for filecontent.

    TRX-BLK:
    do transaction
        on error undo TRX-BLK, leave TRX-BLK:

      /* Setting OO = Ownersnames*/
      if not can-find (first filecontent where filecontent.agentfileID = piAgentFileID
                                         and filecontent.type = 'OO')
       then
        do:
          create filecontent.
          assign
             filecontent.agentfileID      = piAgentFileID
             filecontent.type             = 'OO'
             filecontent.seq              = 0
             filecontent.createdDate      = now
             filecontent.createdBy        = ipcUID
             filecontent.lastModifiedBy   = ''
             filecontent.lastModifiedDate = ?
           .
        end.
      else
       do:
         for first filecontent exclusive-lock
                   where filecontent.agentfileID = piAgentFileID
                     and filecontent.type        = 'OO':
           assign
              filecontent.lastModifiedDate = now
              filecontent.lastModifiedBy   = ipcUID
              .
         end.
       end.
       
      for each fileparty no-lock
          where filepart.agentfileID = piAgentFileID
            and fileparty.partyType = "B":
        if fileparty.firstName > "" 
         then
          cBuyerDesc = if cBuyerDesc > "" then cBuyerDesc + " " + trim(fileparty.firstName) else trim(fileparty.firstName).

        if fileparty.middleName > "" 
         then
          cBuyerDesc = if cBuyerDesc > "" then cBuyerDesc + " " + trim(fileparty.middleName) else trim(fileparty.middleName).

        if fileparty.lastName > "" 
         then
          cBuyerDesc = if cBuyerDesc > "" then cBuyerDesc + " " + trim(fileparty.lastName) else trim(fileparty.lastName).

        if fileparty.joinBy > "" 
         then
          cBuyerDesc = if cBuyerDesc > "" then cBuyerDesc + " " + trim(fileparty.joinBy) else trim(fileparty.joinBy).

        if fileparty.clause > "" 
         then
          cBuyerDesc = if cBuyerDesc > "" then cBuyerDesc + " " + trim(fileparty.clause) else trim(fileparty.clause).        
      end.

      filecontent.content = cBuyerDesc.

      /* Set Default Values*/
      assign
         filecontent.indent      = 0
         filecontent.isNote      = false
         filecontent.appliesTo   = ''
         filecontent.isCleared   = false
         filecontent.clearedBy   = ""
         filecontent.clearedDate = ?
         .

      validate filecontent.
      release filecontent.

      /* Setting OT- Title vested in*/
      if not can-find(first filecontent where filecontent.agentfileID = piAgentFileID
                                          and filecontent.type        = 'OT') 
       then
        do:
          create filecontent.
          assign   
              filecontent.agentfileID      = job.agentfileID
              filecontent.type             = 'OT'
              filecontent.seq              = 0
              fileContent.createdDate      = now  
              fileContent.createdBy        = ipcUID
              fileContent.lastModifiedBy   = ""
              filecontent.lastModifiedDate = ?
              .
        end.
      else
       do:
         for first filecontent exclusive-lock
             where filecontent.agentfileID = piAgentFileID
               and filecontent.type        = 'OT':
           assign
              filecontent.lastModifiedDate = now
              filecontent.lastModifiedBy   = ipcUID
           .
         end.
       end.

      /*Getting vestedin data*/
      for first agentfile field(buyerdescription) no-lock
          where agentfile.agentfileID = piAgentFileID:
        filecontent.content = agentfile.buyerdescription.
      end.

      /* Setting default values*/
      assign
         filecontent.indent      = 0
         filecontent.isNot       = false
         filecontent.appliesTo   = ""
         filecontent.isCleared   = false
         filecontent.clearedDate = ?
         .

      validate filecontent.
      release filecontent.

      /* Setting longlegal*/
      if not can-find(first filecontent where filecontent.agentfileID = piAgentFileID
                                          and filecontent.type        = 'OL') 
       then
        do:
          create filecontent.
          assign
             filecontent.agentfileID      = piAgentFileID
             filecontent.type             = 'OL'
             filecontent.seq              = 0
             filecontent.createdDate      = now
             filecontent.createdBy        = ipcUID
             filecontent.lastModifiedBy   = ''
             filecontent.lastModifiedDate = ?
             .
        end.
      else
       do:
         for first filecontent exclusive-lock
             where filecontent.agentfileID = piAgentFileID
               and filecontent.type        = 'OL':
           assign
              filecontent.lastModifiedDate = now
              filecontent.lastModifiedBy   = ipcUID
              .
         end.
       end.

      /* Getting longleal*/
      if can-find(first filecontent where filecontent.agentfileID = piAgentFileID
                                      and filecontent.type        = 'CL')
       then
        for first bfFileContent fields(content) where bfFileContent.agentfileID = piAgentFileID
                                                  and bfFileContent.type        = 'CL' no-lock:
          cLongLegal = bfFileContent.content.
        end.
      else
        for each fileproperty field(longlegal) no-lock
            where fileproperty.agentfileID = piAgentFileID:

          if cLongLegal <> '' and cLongLegal <> '?' 
           then
            cLongLegal = cLongLegal + chr(10) + string(fileproperty.longlegal).
          else
            cLongLegal = fileproperty.longlegal.
        end.

      filecontent.content = cLongLegal.
      /* Set default value*/
      assign
         filecontent.indent      = 0
         filecontent.isNote      = false
         filecontent.appliesTo   = ''
         filecontent.isCleared   = false
         filecontent.clearedBy   = ''
         filecontent.clearedDate = ?
        .

      validate filecontent.
      release filecontent.
      release bfFileContent.

      /*Setting Exception - OE*/

      for each filecontent where filecontent.agentfileID = piAgentFileID
                             and filecontent.type        = 'OE' exclusive-lock:
        delete filecontent.
        release filecontent.
      end.
      
      if can-find(first filecontent where filecontent.agentfileID = piAgentFileID
                                      and filecontent.type        = 'CE')
       then
        do:
          for each filecontent no-lock where filecontent.agentfileID = piAgentFileID
                                        and filecontent.type        = 'CE' by filecontent.seq:

            create bfFileContent.
            assign
               bfFileContent.agentfileID      = piAgentFileID
               bfFileContent.type             = 'OE'
               bfFileContent.seq              = filecontent.seq
               bfFileContent.createdDate      = now
               bfFileContent.createdBy        = ipcUID
               bfFileContent.lastModifiedBy   = ''
               bfFileContent.lastModifiedDate = now
               bfFileContent.content          = filecontent.content
               bfFileContent.indent           = filecontent.indent
               bfFileContent.isNote           = filecontent.isnote
               bfFileContent.appliesTo        = filecontent.appliesTo
               bfFileContent.isCleared        = false
               bfFileContent.clearedBy        = ''
               bfFileContent.clearedDate      = ?
             .
            validate bfFileContent.
            release bfFileContent.
          end.
        end.
      else
       do:
          for each filecontent no-lock where filecontent.agentfileID = piAgentFileID
                                        and filecontent.type        = 'E' by filecontent.seq:

            create bfFileContent.
            assign
               bfFileContent.agentfileID      = piAgentFileID
               bfFileContent.type             = 'OE'
               bfFileContent.seq              = filecontent.seq
               bfFileContent.createdDate      = now
               bfFileContent.createdBy        = ipcUID
               bfFileContent.lastModifiedBy   = ''
               bfFileContent.lastModifiedDate = now
               bfFileContent.content          = filecontent.content
               bfFileContent.indent           = filecontent.indent
               bfFileContent.isNote           = filecontent.isNote
               bfFileContent.appliesTo        = filecontent.appliesTo
               bfFileContent.isCleared        = false
               bfFileContent.clearedBy        = ''
               bfFileContent.clearedDate      = ?
             .
            validate bfFileContent.
            release bfFileContent.
          end.
       end.

      /*Setting Instuments - OI*/

      for each filecontent where filecontent.agentfileID = piAgentFileID
                             and filecontent.type        = 'OI' exclusive-lock:
        delete filecontent.
        release filecontent.
      end.
      
      if can-find(first filecontent where filecontent.agentfileID = piAgentFileID
                                      and filecontent.type        = 'CI')
       then
        do:
          for each filecontent no-lock where filecontent.agentfileID = piAgentFileID
                                        and filecontent.type        = 'CI' by filecontent.seq:

            create bfFileContent.
            assign
               bfFileContent.agentfileID      = piAgentFileID
               bfFileContent.type             = 'OI'
               bfFileContent.seq              = filecontent.seq
               bfFileContent.createdDate      = now
               bfFileContent.createdBy        = ipcUID
               bfFileContent.lastModifiedBy   = ''
               bfFileContent.lastModifiedDate = now
               bfFileContent.content          = filecontent.content
               bfFileContent.indent           = filecontent.indent
               bfFileContent.isNote           = filecontent.isNote
               bfFileContent.appliesTo        = filecontent.appliesTo
               bfFileContent.isCleared        = false
               bfFileContent.clearedBy        = ''
               bfFileContent.clearedDate      = ?
             .
            validate bfFileContent.
            release bfFileContent.
          end.
        end.
      else
       do:
          for each filecontent no-lock where filecontent.agentfileID = piAgentFileID
                                        and filecontent.type        = 'I' by filecontent.seq:

            create bfFileContent.
            assign
               bfFileContent.agentfileID      = piAgentFileID
               bfFileContent.type             = 'OI'
               bfFileContent.seq              = filecontent.seq
               bfFileContent.createdDate      = now
               bfFileContent.createdBy        = ipcUID
               bfFileContent.lastModifiedBy   = ''
               bfFileContent.lastModifiedDate = now
               bfFileContent.content          = filecontent.content
               bfFileContent.indent           = filecontent.indent
               bfFileContent.isNote           = filecontent.isNote
               bfFileContent.appliesTo        = filecontent.appliesTo
               bfFileContent.isCleared        = false
               bfFileContent.clearedBy        = ''
               bfFileContent.clearedDate      = ?
             .
            validate bfFileContent.
            release bfFileContent.
          end.
       end.

      /*Setting Requirement - OR*/

      for each filecontent where filecontent.agentfileID = piAgentFileID
                             and filecontent.type        = 'OR' exclusive-lock:
        delete filecontent.
        release filecontent.
      end.
      
      if can-find(first filecontent where filecontent.agentfileID = piAgentFileID
                                      and filecontent.type        = 'CR')
       then
        do:
          for each filecontent no-lock where filecontent.agentfileID = piAgentFileID
                                        and filecontent.type        = 'CR' by filecontent.seq:

            create bfFileContent.
            assign
               bfFileContent.agentfileID      = piAgentFileID
               bfFileContent.type             = 'OR'
               bfFileContent.seq              = filecontent.seq
               bfFileContent.createdDate      = now
               bfFileContent.createdBy        = ipcUID
               bfFileContent.lastModifiedBy   = ''
               bfFileContent.lastModifiedDate = now
               bfFileContent.content          = filecontent.content
               bfFileContent.indent           = filecontent.indent
               bfFileContent.isNote           = filecontent.isNote
               bfFileContent.appliesTo        = filecontent.appliesTo
               bfFileContent.isCleared        = false
               bfFileContent.clearedBy        = ''
               bfFileContent.clearedDate      = ?
             .
            validate bfFileContent.
            release bfFileContent.
          end.
        end.
      else
       do:
          for each filecontent no-lock where filecontent.agentfileID = piAgentFileID
                                        and filecontent.type        = 'R' by filecontent.seq:

            create bfFileContent.
            assign
               bfFileContent.agentfileID      = piAgentFileID
               bfFileContent.type             = 'OR'
               bfFileContent.seq              = filecontent.seq
               bfFileContent.createdDate      = now
               bfFileContent.createdBy        = ipcUID
               bfFileContent.lastModifiedBy   = ''
               bfFileContent.lastModifiedDate = now
               bfFileContent.content          = filecontent.content
               bfFileContent.indent           = filecontent.indent
               bfFileContent.isNote           = filecontent.isNote
               bfFileContent.appliesTo        = filecontent.appliesTo
               bfFileContent.isCleared        = false
               bfFileContent.clearedBy        = ''
               bfFileContent.clearedDate      = ?
             .
            validate bfFileContent.
            release bfFileContent.
          end.
       end.
    end. /* transaction end*/
  end method.

  method private void CreateLenderFileContent(input piAgentFileID as integer,
                                              input ipcUID        as character):
    define variable cBuyerDesc as character no-undo.
    define variable cException  as character no-undo.
    define variable cLongLegal  as longchar  no-undo.

    define buffer bfFileContent for filecontent.
    
    TRX-BLK:
    do transaction
        on error undo TRX-BLK, leave TRX-BLK:

      /* Setting LO = Ownersnames*/
      if not can-find (first filecontent where filecontent.agentfileID = piAgentFileID
                                         and filecontent.type = 'LO')
       then
        do:
          create filecontent.
          assign
             filecontent.agentfileID      = piAgentFileID
             filecontent.type             = 'LO'
             filecontent.seq              = 0
             filecontent.createdDate      = now
             filecontent.createdBy        = ipcUID
             filecontent.lastModifiedBy   = ''
             filecontent.lastModifiedDate = ?
           .
        end.
      else
       do:
         for first filecontent exclusive-lock
                   where filecontent.agentfileID = piAgentFileID
                     and filecontent.type        = 'LO':
           assign
              filecontent.lastModifiedDate = now
              filecontent.lastModifiedBy   = ipcUID
              .
         end.
       end.
       
      for each fileparty no-lock
          where filepart.agentfileID = piAgentFileID
            and fileparty.partyType = "B":
        if fileparty.firstName > "" 
         then
          cBuyerDesc = if cBuyerDesc > "" then cBuyerDesc + " " + trim(fileparty.firstName) else trim(fileparty.firstName).

        if fileparty.middleName > "" 
         then
          cBuyerDesc = if cBuyerDesc > "" then cBuyerDesc + " " + trim(fileparty.middleName) else trim(fileparty.middleName).

        if fileparty.lastName > "" 
         then
          cBuyerDesc = if cBuyerDesc > "" then cBuyerDesc + " " + trim(fileparty.lastName) else trim(fileparty.lastName).

        if fileparty.joinBy > "" 
         then
          cBuyerDesc = if cBuyerDesc > "" then cBuyerDesc + " " + trim(fileparty.joinBy) else trim(fileparty.joinBy).

        if fileparty.clause > "" 
         then
          cBuyerDesc = if cBuyerDesc > "" then cBuyerDesc + " " + trim(fileparty.clause) else trim(fileparty.clause).        
      end.

      filecontent.content = cBuyerDesc.

      /* Set Default Values*/
      assign
         filecontent.indent      = 0
         filecontent.isNote      = false
         filecontent.appliesTo   = ''
         filecontent.isCleared   = false
         filecontent.clearedBy   = ""
         filecontent.clearedDate = ?
         .

      validate filecontent.
      release filecontent.

      /* Setting LT- Title vested in*/
      if not can-find(first filecontent where filecontent.agentfileID = piAgentFileID
                                          and filecontent.type        = 'LT') 
       then
        do:
          create filecontent.
          assign   
              filecontent.agentfileID      = job.agentfileID
              filecontent.type             = 'LT'
              filecontent.seq              = 0
              fileContent.createdDate      = now  
              fileContent.createdBy        = ipcUID
              fileContent.lastModifiedBy   = ""
              filecontent.lastModifiedDate = ?
              .
        end.
      else
       do:
         for first filecontent exclusive-lock
             where filecontent.agentfileID = piAgentFileID
               and filecontent.type        = 'LT':
           assign
              filecontent.lastModifiedDate = now
              filecontent.lastModifiedBy   = ipcUID
           .
         end.
       end.

      /*Getting vestedin data*/
      for first agentfile field(buyerdescription) no-lock
          where agentfile.agentfileID = piAgentFileID:
        filecontent.content = agentfile.buyerdescription.
      end.

      /* Setting default values*/
      assign
         filecontent.indent      = 0
         filecontent.isNot       = false
         filecontent.appliesTo   = ""
         filecontent.isCleared   = false
         filecontent.clearedDate = ?
         .

      validate filecontent.
      release filecontent.

      /* Setting longlegal*/
      if not can-find(first filecontent where filecontent.agentfileID = piAgentFileID
                                          and filecontent.type        = 'LL') 
       then
        do:
          create filecontent.
          assign
             filecontent.agentfileID      = piAgentFileID
             filecontent.type             = 'LL'
             filecontent.seq              = 0
             filecontent.createdDate      = now
             filecontent.createdBy        = ipcUID
             filecontent.lastModifiedBy   = ''
             filecontent.lastModifiedDate = ?
             .
        end.
      else
       do:
         for first filecontent exclusive-lock
             where filecontent.agentfileID = piAgentFileID
               and filecontent.type        = 'LL':
           assign
              filecontent.lastModifiedDate = now
              filecontent.lastModifiedBy   = ipcUID
              .
         end.
       end.

      /* Getting longleal*/
      if can-find(first filecontent where filecontent.agentfileID = piAgentFileID
                                      and filecontent.type        = 'CL')
       then
        for first bfFileContent fields(content) where bfFileContent.agentfileID = piAgentFileID
                                                  and bfFileContent.type        = 'CL' no-lock:
          cLongLegal = bfFileContent.content.
        end.
      else
        for each fileproperty field(longlegal) no-lock
            where fileproperty.agentfileID = piAgentFileID:

          if cLongLegal <> '' and cLongLegal <> '?' 
           then
            cLongLegal = cLongLegal + chr(10) + string(fileproperty.longlegal).
          else
            cLongLegal = fileproperty.longlegal.
        end.

      filecontent.content = cLongLegal.

      /* Set default value*/
      assign
         filecontent.indent      = 0
         filecontent.isNote      = false
         filecontent.appliesTo   = ''
         filecontent.isCleared   = false
         filecontent.clearedBy   = ''
         filecontent.clearedDate = ?
        .

      validate filecontent.
      release filecontent.
      release bfFileContent.

    /*Setting Exception - LE*/

      for each filecontent where filecontent.agentfileID = piAgentFileID
                             and filecontent.type        = 'LE' exclusive-lock:
        delete filecontent.
        release filecontent.
      end.
      
      if can-find(first filecontent where filecontent.agentfileID = piAgentFileID
                                      and filecontent.type        = 'CE')
       then
        do:
          for each filecontent no-lock where filecontent.agentfileID = piAgentFileID
                                        and filecontent.type        = 'CE' by filecontent.seq:

            create bfFileContent.
            assign
               bfFileContent.agentfileID      = piAgentFileID
               bfFileContent.type             = 'LE'
               bfFileContent.seq              = filecontent.seq
               bfFileContent.createdDate      = now
               bfFileContent.createdBy        = ipcUID
               bfFileContent.lastModifiedBy   = ''
               bfFileContent.lastModifiedDate = now
               bfFileContent.content          = filecontent.content
               bfFileContent.indent           = filecontent.indent
               bfFileContent.isNote           = filecontent.isNote
               bfFileContent.appliesTo        = filecontent.appliesTo
               bfFileContent.isCleared        = false
               bfFileContent.clearedBy        = ''
               bfFileContent.clearedDate      = ?
             .
            validate bfFileContent.
            release bfFileContent.
          end.
        end.
      else
       do:
          for each filecontent no-lock where filecontent.agentfileID = piAgentFileID
                                        and filecontent.type        = 'E' by filecontent.seq:

            create bfFileContent.
            assign
               bfFileContent.agentfileID      = piAgentFileID
               bfFileContent.type             = 'LE'
               bfFileContent.seq              = filecontent.seq
               bfFileContent.createdDate      = now
               bfFileContent.createdBy        = ipcUID
               bfFileContent.lastModifiedBy   = ''
               bfFileContent.lastModifiedDate = now
               bfFileContent.content          = filecontent.content
               bfFileContent.indent           = filecontent.indent
               bfFileContent.isNote           = filecontent.isNote
               bfFileContent.appliesTo        = filecontent.appliesTo
               bfFileContent.isCleared        = false
               bfFileContent.clearedBy        = ''
               bfFileContent.clearedDate      = ?
             .
            validate bfFileContent.
            release bfFileContent.
          end.
       end.

    /*Setting Exception - LI*/

      for each filecontent where filecontent.agentfileID = piAgentFileID
                             and filecontent.type        = 'LI' exclusive-lock:
        delete filecontent.
        release filecontent.
      end.
      
      if can-find(first filecontent where filecontent.agentfileID = piAgentFileID
                                      and filecontent.type        = 'CI')
       then
        do:
          for each filecontent no-lock where filecontent.agentfileID = piAgentFileID
                                        and filecontent.type        = 'CI' by filecontent.seq:

            create bfFileContent.
            assign
               bfFileContent.agentfileID      = piAgentFileID
               bfFileContent.type             = 'LI'
               bfFileContent.seq              = filecontent.seq
               bfFileContent.createdDate      = now
               bfFileContent.createdBy        = ipcUID
               bfFileContent.lastModifiedBy   = ''
               bfFileContent.lastModifiedDate = now
               bfFileContent.content          = filecontent.content
               bfFileContent.indent           = filecontent.indent
               bfFileContent.isNote           = filecontent.isNote
               bfFileContent.appliesTo        = filecontent.appliesTo
               bfFileContent.isCleared        = false
               bfFileContent.clearedBy        = ''
               bfFileContent.clearedDate      = ?
             .
            validate bfFileContent.
            release bfFileContent.
          end.
        end.
      else
       do:
          for each filecontent no-lock where filecontent.agentfileID = piAgentFileID
                                        and filecontent.type        = 'I' by filecontent.seq:

            create bfFileContent.
            assign
               bfFileContent.agentfileID      = piAgentFileID
               bfFileContent.type             = 'LI'
               bfFileContent.seq              = filecontent.seq
               bfFileContent.createdDate      = now
               bfFileContent.createdBy        = ipcUID
               bfFileContent.lastModifiedBy   = ''
               bfFileContent.lastModifiedDate = now
               bfFileContent.content          = filecontent.content
               bfFileContent.indent           = filecontent.indent
               bfFileContent.isNote           = filecontent.isNote
               bfFileContent.appliesTo        = filecontent.appliesTo
               bfFileContent.isCleared        = false
               bfFileContent.clearedBy        = ''
               bfFileContent.clearedDate      = ?
             .
            validate bfFileContent.
            release bfFileContent.
          end.
       end.

    /*Setting Exception - LR*/

      for each filecontent where filecontent.agentfileID = piAgentFileID
                             and filecontent.type        = 'LR' exclusive-lock:
        delete filecontent.
        release filecontent.
      end.
      
      if can-find(first filecontent where filecontent.agentfileID = piAgentFileID
                                      and filecontent.type        = 'CR')
       then
        do:
          for each filecontent no-lock where filecontent.agentfileID = piAgentFileID
                                        and filecontent.type        = 'CR' by filecontent.seq:

            create bfFileContent.
            assign
               bfFileContent.agentfileID      = piAgentFileID
               bfFileContent.type             = 'LR'
               bfFileContent.seq              = filecontent.seq
               bfFileContent.createdDate      = now
               bfFileContent.createdBy        = ipcUID
               bfFileContent.lastModifiedBy   = ''
               bfFileContent.lastModifiedDate = now
               bfFileContent.content          = filecontent.content
               bfFileContent.indent           = filecontent.indent
               bfFileContent.isNote           = filecontent.isNote
               bfFileContent.appliesTo        = filecontent.appliesTo
               bfFileContent.isCleared        = false
               bfFileContent.clearedBy        = ''
               bfFileContent.clearedDate      = ?
             .
            validate bfFileContent.
            release bfFileContent.
          end.
        end.
      else
       do:
          for each filecontent no-lock where filecontent.agentfileID = piAgentFileID
                                        and filecontent.type        = 'R' by filecontent.seq:

            create bfFileContent.
            assign
               bfFileContent.agentfileID      = piAgentFileID
               bfFileContent.type             = 'LR'
               bfFileContent.seq              = filecontent.seq
               bfFileContent.createdDate      = now
               bfFileContent.createdBy        = ipcUID
               bfFileContent.lastModifiedBy   = ''
               bfFileContent.lastModifiedDate = now
               bfFileContent.content          = filecontent.content
               bfFileContent.indent           = filecontent.indent
               bfFileContent.isNote           = filecontent.isNote
               bfFileContent.appliesTo        = filecontent.appliesTo
               bfFileContent.isCleared        = false
               bfFileContent.clearedBy        = ''
               bfFileContent.clearedDate      = ?
             .
            validate bfFileContent.
            release bfFileContent.
          end.
       end.
    end. /* transaction end*/  
  end method.
end class.



