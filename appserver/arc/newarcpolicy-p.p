
/*------------------------------------------------------------------------
@file newarcpolicy-p.p
@action ARcPolicyNew
@description Get the agent journal information from ARC.

@param arcpolicy;complex;The Policy data

@author Sachin Chaturvedi
@created 2023.04.24

@Modified :
Name          Date            Comments
S chandu     10/21/2024      Modified to send date in ISO format.
------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
  /* Local variables */
define variable tTempParent     as character no-undo initial "".
define variable tFileName       as character no-undo.
define variable tEndorseCount   as integer   no-undo.
define variable tEndorseTotal   as integer   no-undo.
define variable tPolicyTotal    as integer   no-undo.
define variable tPolicyCount    as integer   no-undo.
define variable tClosingDate    as character no-undo.
define variable tEffectiveDate  as character no-undo.
{lib/std-def.i}

/* Used for getting the ARC policy */
define variable tDataFile       as character no-undo.
define variable pSuccess        as logical   no-undo.
define variable pMsg            as character no-undo.
define variable tURL            as character no-undo.
define variable tTraceFile      as character no-undo.
define variable tResponseFile   as character no-undo.

/* Functions and Procedures */
{lib/add-delimiter.i}
{lib/run-http.i}
{lib/build-json.i &doNull=true &noRoot=true}
{lib/arc-bearer-token.i}
  
  /* Temp Tables */
  {tt/arcpolicy.i            &tableAlias="ttPolicy"}
  {tt/arcpolicyendorsement.i &tableAlias="ttPolicyEndorsement"}
  {tt/arcpolicyresult.i      &tableAlias="ttReturnPolicy"}

    /* Temp table for the JSON input file to the ARC call */
define temp-table policyARC
  field OfficeId        as integer   
  field UserName        as character 
  field GfNumber        as character 
  field policyProperty  as character serialize-hidden          serialize-name "Property"
  field EffectiveDate   as datetime  
  field ClosingDate     as datetime  
  field Signatory       as integer   
  field PropertyType    as character 
  field policyInsert    as character serialize-hidden extent 1 serialize-name "Policies"
  field Residential     as logical
  field TransactionType as character
  .
  
define temp-table policyProperty
  field Address1       as character
  field Address2       as character
  field City           as character
  field State          as character
  field Zipcode        as character
  field County         as character
  .

define temp-table policyInsert
  field PolicyId        as integer
  field TypeId          as integer
  field InsuredName     as character
  field StatCode        as character
  field LiabilityAmount as decimal
  field endorsement     as character serialize-hidden extent 1 serialize-name "Endorsements" label "PolicyId"
  field GrossPremium    as decimal
  field NetPremium      as decimal
  .
  
define temp-table endorsement
  field PolicyId        as integer
  field Description     as character
  field StatCode        as character
  field Charge          as decimal
  field NetPremium      as decimal
  field GrossPremium    as decimal
  .
  
  define input  parameter pAgentFileID    as integer   no-undo.
  define input  parameter table for ttPolicy.
  define input  parameter table for ttPolicyEndorsement.
  define output parameter table for ttReturnPolicy.
  define output parameter  pError         as logical   no-undo.
  define output parameter  pErrorMsg      as character no-undo.

  /* get and validate variables */
  if not can-find(first agentFile where agentFile.agentFileID = pAgentFileID) 
   then
      do: 
        assign pError = true
               pErrorMsg = "Agent File does not exist".
        return.
      end.

  /* validate policy and endorsement */
  for each ttPolicy no-lock:
    /* must have an office ID */
    if ttPolicy.OfficeId = "" or ttPolicy.OfficeId = "0" or ttPolicy.OfficeId = "None"
     then
      do: 
        assign pError = true
               pErrorMsg = "Office is invalid.".
        return.
      end.
   
    /* must have a non-blank file number */
    if ttPolicy.GfNumber = ""
     then
      do: 
        assign pError = true
               pErrorMsg = "A blank file number is invalid.".
        return.
      end.
   
    /* must have a user */
    if ttPolicy.Username = ""
     then
      do: 
        assign pError = true
               pErrorMsg = "Username is invalid.".
        return.
      end.
   
    /* must have a policy template Id */
    if ttPolicy.PolicyTemplateId = 0
     then
      do: 
        assign pError = true
               pErrorMsg = "One of the Policy Types is invalid.".
        return.
      end.
  end.
 
  tPolicyTotal = 0.
  tPolicyCount = 0.
  for each ttPolicy no-lock:
    tPolicyTotal = tPolicyTotal + 1.
  end.

  tDataFile = "build-json.json".
  output to value(tDataFile).
  put unformatted "~{" skip.
  for each ttPolicy no-lock
     where ttPolicy.PolicyId < 100:
   
    tClosingDate   = if ttPolicy.ClosingDate <> ? then string(iso-date(ttPolicy.ClosingDate)) else ''.
    tEffectiveDate = if ttPolicy.EffectiveDate <> ? then string(iso-date(ttPolicy.EffectiveDate)) else ''.
    tPolicyCount = tPolicyCount + 1.
    if ttPolicy.PolicyId = 1
     then
      do:
        put unformatted "  ~"OfficeId~": " + string(ttPolicy.OfficeID) + "," skip.
        put unformatted "  ~"UserName~": ~"" + ttPolicy.Username + "~"," skip.
        put unformatted "  ~"GFNumber~": ~"" + ttPolicy.GfNumber + "~"," skip.
        put unformatted "  ~"Property~":" skip.
        put unformatted "  ~{" skip.
        put unformatted "    ~"Address1~": " + MakeNull(ttPolicy.Address1, true) + "," skip.
        put unformatted "    ~"Address2~": " + MakeNull(ttPolicy.Address2, true) + "," skip.
        put unformatted "    ~"City~": " + MakeNull(ttPolicy.City, true) + "," skip.
        put unformatted "    ~"State~": " + MakeNull(ttPolicy.State, true) + "," skip.
        put unformatted "    ~"Zipcode~": " + MakeNull(ttPolicy.Zipcode, true) skip.
        put unformatted "  ~}," skip.
        put unformatted "  ~"EffectiveDate~": ~"" + substring(tEffectiveDate,1,19) + "~"," skip.
        put unformatted "  ~"ClosingDate~": ~"" + substring(tClosingDate,1,19) + "~"," skip.
        put unformatted "  ~"Signatory~": " + MakeNull(ttPolicy.Signatory, false) + "," skip.
        put unformatted "  ~"PropertyType~": " + MakeNull(ttPolicy.PropertyType, true) + "," skip.
        put unformatted "  ~"Residential~": " + (if ttPolicy.Residential then "true" else "false") + "," skip.
        put unformatted "  ~"TransactionType~": " + MakeNull(ttPolicy.TransactionType, true) + "," skip.
        put unformatted "  ~"Policies~": [" skip.
      end.
    put unformatted "    ~{" skip.
    put unformatted "      ~"TypeId~": " + MakeNull(string(ttPolicy.PolicyTemplateID), false) + "," skip.
    put unformatted "      ~"InsuredName~": " + MakeNull(ttPolicy.InsuredName, true) + "," skip.
    put unformatted "      ~"StatCode~": " + MakeNull(ttPolicy.StatCode, true) + "," skip.
    put unformatted "      ~"LiabilityAmount~": " + MakeNull(string(ttPolicy.LiabilityAmount), false) + "," skip.
    put unformatted "      ~"GrossPremium~": " + MakeNull(string(ttPolicy.GrossPremiumAmount), false) + "," skip.
    put unformatted "      ~"NetPremium~": " + MakeNull(string(ttPolicy.NetPremium), false) + "," skip.
    if not can-find(first ttPolicyEndorsement where PolicyId = ttPolicy.PolicyId)
     then put unformatted "      ~"Endorsements~": []" skip.
     else
      do:
        tEndorseTotal = 0.
        for each ttPolicyEndorsement no-lock
           where ttPolicyEndorsement.PolicyId = ttPolicy.PolicyId:
         
          tEndorseTotal = tEndorseTotal + 1.
        end.
        put unformatted "      ~"Endorsements~": [" skip.
        tEndorseCount = 0.
        for each ttPolicyEndorsement no-lock
           where ttPolicyEndorsement.PolicyId = ttPolicy.PolicyId:
         
          tEndorseCount = tEndorseCount + 1.
          put unformatted "        ~{" skip.
          put unformatted "          ~"Description~": " + MakeNull(ttPolicyEndorsement.Description, true) + "," skip.
          put unformatted "          ~"Charge~": " + MakeNull(string(ttPolicyEndorsement.Charge), false) + "," skip.
          put unformatted "          ~"NetPremium~": " + MakeNull(string(ttPolicyEndorsement.NetPremium), false) + "," skip.
          put unformatted "          ~"GrossPremium~": " + MakeNull(string(ttPolicyEndorsement.GrossPremium), false) skip.
          put unformatted "        ~}" + (if tEndorseCount = tEndorseTotal then "" else ",") skip.
        end.
        put unformatted "      ]" skip.
      end.
    put unformatted "    ~}" + (if tPolicyCount = tPolicyTotal then "" else ",") skip.
  end.
  put unformatted "  ~]" skip.
  put unformatted "~}" skip.
  output close.

  publish "AddTempFile" ("ARCActivity", tDataFile).

  /* send to ARC */
  tPolicyCount = 1.
  publish "GetSystemParameter" ("ARCSendPolicy", output tURL).
  run HTTPPost ("ARCPolicyNew", tURL, CreateArcHeader(""), "application/json", search(tDataFile), output pSuccess, output pMsg, output tResponseFile).

  if not pSuccess
   then
    do:
      assign pError = true
             pErrorMsg = "Failed to connect to the ARC. " + pMsg.
      return.
    end.

  std-lo = temp-table ttReturnPolicy:read-json("FILE",tResponseFile) no-error.

  if error-status:error
   then
    do:
    assign pError = true
           pErrorMsg = "Error parsing Data" + error-status:get-message(1).
    return.
  end.

  for each ttReturnPolicy:        
    assign
        ttReturnPolicy.PolicyId = if (ttReturnPolicy.PolicyId = ?) then -1 * tPolicyCount else integer(ttReturnPolicy.PolicyId)
        .

    for first ttPolicy no-lock
      where ttPolicy.PolicyId = tPolicyCount:
          
        
      if ttPolicy.PolicyTemplateID = ttReturnPolicy.TypeId
       then
        assign
          ttReturnPolicy.PartyId = ttPolicy.PartyId
          ttReturnPolicy.Insured = ttPolicy.InsuredName
          .
    end.

    tPolicyCount = tPolicyCount + 1.
  end.

  for each fileparty exclusive-lock
     where fileparty.agentFileID = pAgentFileID:
       
    for first ttReturnPolicy no-lock
        where ttReturnPolicy.PartyId = fileparty.partyType + string(fileparty.seq):
          
      fileparty.policyID = ttReturnPolicy.policyID.
    end.
  end.



