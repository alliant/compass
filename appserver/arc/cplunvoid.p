&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file cplunvoid.p
@description Unvoids the CPLs sent from the ARC into Compass

@param arccpl;complex;The ARC CPL

@author John Oliver
@created 12.04.2019
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

/* Parmaeters ---                                                       */
define variable pSuccess as logical no-undo initial false.

/* Temp Tables ---                                                      */
{tt/arccpl.i}

/* Local Variables ---                                                  */
{lib/std-def.i}
define variable tRefType as character no-undo.
define variable tRefNum  as character no-undo.
define variable tCount   as integer   no-undo initial 0.

/* Functions ---                                                        */
{lib/add-delimiter.i}

&scoped-define CPLStatus "I"
&scoped-define CPLStatusValidation "V"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

empty temp-table arccpl.
pRequest:getParameter("Root", input-output table arccpl).

if not can-find(first arccpl)
 then 
  do:
    std-ch = "".
    pRequest:getParameter("JSON", output std-ch).
    pResponse:fault("3005", "No cpl data retrieved. The JSON is:" + chr(13) + chr(13) + std-ch).
  end.

if pResponse:isFault()
 then return.

TRX-BLOCK:
do transaction on error UNDO TRX-BLOCK, LEAVE TRX-BLOCK:
  for each arccpl no-lock:
    /* add the message variables */
    assign
      tRefType = arccpl.source
      tRefNum  = addDelimiter(tRefNum, ",") + arccpl.cplCode
      .
      
    /* check if the CPL exists */
    if not can-find(first cpl where cplID = arccpl.cplCode)
     then
      do:
        pResponse:addFault("3066", "CPL").
        leave TRX-BLOCK.
      end.
      
    /* check if the CPL is voided */
    if not can-find(first cpl where cplID = arccpl.cplCode and stat = {&CPLStatusValidation})
     then
      for first sysprop no-lock
          where sysprop.appCode = "ARM"
            and sysprop.objAction = "CPL"
            and sysprop.objProperty = "Status"
            and sysprop.objID = {&CPLStatusValidation}:
      
        pResponse:addFault("3012", "CPL" {&msg-add} "Status" {&msg-add} sysprop.objValue).
        leave TRX-BLOCK.
      end.
    
    /* revise the CPL */
    for first cpl exclusive-lock
        where cpl.cplID = arccpl.cplCode:
      
      assign
        cpl.stat     = {&CPLStatus}
        cpl.voidDate = ?
        .
    end.
    if not can-find(first cpl where cplID = arccpl.cplCode and stat = {&CPLStatus})
     then
      do:
        pResponse:addFault("3016", "CPL" {&msg-add} "cplID" {&msg-add} arccpl.cplCode {&msg-add} "Status = " + {&CPLStatus}).
        leave TRX-BLOCK.
      end.
     else tCount = tCount + 1.
  end.
end.

for first sysprop no-lock
    where sysprop.appCode = "ARM"
      and sysprop.objAction = "CPL"
      and sysprop.objProperty = "Status"
      and sysprop.objID = {&CPLStatus}:
      
  pResponse:logMessage(tRefType, tRefNum, "There have been " + string(tCount) + " CPL(s) " + sysprop.objValue).
end.
pResponse:success("2000", "ARC CPL Unvoid").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


