/*---------------------------------------------------------------------
@name modifyrepositoryfile-p.p
@description Modify the repository File

@author Shefali
@version 1.0
@created 17.09.2024
@notes 
---------------------------------------------------------------------*/

{tt/repository.i &tableAlias="document"}
{tt/repository.i &tableAlias="CompassDocument" &noSerializeHidden}

define input parameter  dataset-handle dsHandleInput.
define output parameter dataset-handle dsHandleOutput.
define output parameter oplSuccess       as logical    no-undo.
define output parameter opcMsg           as character no-undo.

define variable dsHandleInput1           as handle no-undo.

{lib/arc-repository-procedures.i}

/* dataset creation */
create dataset dsHandleInput1.
dsHandleInput1:serialize-name = 'documents'.
dsHandleInput1:set-buffers(buffer document:handle).

oplSuccess = dsHandleInput1:copy-dataset(dsHandleInput).

if not oplSuccess 
 then
  do:
    opcMsg = "Dataset Parsing Failed".
    return.
  end.

/* dataset creation */
create dataset dsHandleOutput.
dsHandleOutput:serialize-name = 'ArrayOfCompassDocument'.
dsHandleOutput:set-buffers(buffer CompassDocument:handle).
    
empty temp-table CompassDocument.

/* call procedure */
run ModifyFile (input  table document,
                output table CompassDocument,
                output oplSuccess,
                output opcMsg).

if not oplSuccess
 then
  opcMsg = if opcMsg = '' then 'Failed to connect to the ARC.' else opcMsg.
	            

