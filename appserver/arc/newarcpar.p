&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file newarcpar.p
@action arcParNew
@description Creates a new PAR in the ARC

@param par;complex;The PAR data

@author John Oliver
@created 2022.01.17

Name          Date       Note
------------- ---------- -----------------------------------------------
S Chandu      08-01-2022  changed to use dataset handles
S Chandu      02-27-2024  Added validation on job type Search.
------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

/* Parameters */
define variable pAgentFileID    as integer   no-undo.

/* Local variables */
define variable tTempParent     as character no-undo initial "".
define variable tFileName       as character no-undo.
{lib/std-def.i}

/* Used for getting the ARC code */
define variable tParID          as integer   no-undo.
define variable tValue          as character no-undo.
define variable tCurrentElement as character no-undo.
define variable tDataFile       as character no-undo.
define variable tFileID         as character no-undo.
define variable pSuccess        as logical   no-undo.
define variable pMsg            as character no-undo.

/* Temp Tables */
{tt/arcpar.i &tableAlias="ttPar" &serializename=Par}
{tt/repository.i &tableAlias="document"}

/* Functions and Procedures */
{lib/arc-repository-procedures.i &exclude-startElement=true}
{lib/add-delimiter.i}
{lib/run-http.i}
{lib/arc-bearer-token.i}

define variable dsHandle as handle no-undo.
create dataset  dsHandle.
dsHandle:xml-node-name = 'data'.
dsHandle:set-buffers(buffer ttPar:handle).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

empty temp-table ttPAR.
std-lo = pRequest:getParameter(input-output dataset-handle dsHandle, output std-ch).
if not std-lo 
 then
  do:
    pResponse:fault('3005', "XML Parsing failed. Reason: " + std-ch).
    return.
  end.
  
/* get and validate variables */
pRequest:getParameter("AgentFileID", output pAgentFileID). 
if not can-find(first agentFile where agentFile.agentFileID = pAgentFileID) 
 then pResponse:fault("3066", "Agent File").

if pResponse:isFault()
 then return.

if not can-find(first job where job.agentfileID = pAgentFileID and job.jobtype = "Search")
 then
  do:
    pResponse:fault("3005", "Atleast an order of type Search must exist.").
    return.
  end.

tTempParent = guid(generate-uuid).

/* validate par */
for first ttPar no-lock:
  /* upload files to the ARC ticketing system */
  if ttPar.fileIDList > ""
   then
    do std-in = 1 to num-entries(ttPar.fileIDList):
      /* the file ID */
      tFileID = entry(std-in, ttPar.fileIDList).
      
      /* get the filename */
      run GetFiles (input ttPar.entity, input ttPar.entityID, input false, output table document, output std-lo, output std-ch).
      if not std-lo
       then pResponse:fault("3005", std-ch).
       else
        for each document exclusive-lock:
          if document.identifier = entry(std-in, ttPar.fileIDList)
           then tFileName = document.name.
        end.
        
      if pResponse:isFault()
       then return.
    
      /* download the file */
      tDataFile = tFileID + "PAR".
      run DownloadFile (input tFileID, input-output tDataFile, output std-lo, output std-ch).
      if not std-lo
       then pResponse:fault("3005", std-ch).
        
      if pResponse:isFault()
       then return.
       
      /* upload the file under the ticketing system */
      run UploadPAR (input pRequest:uid, input pRequest:password, input tTempParent, input tDataFile, input tFileName, output std-lo, output std-ch).
      if not std-lo
       then pResponse:fault("3005", std-ch).
        
      if pResponse:isFault()
       then return.
    end.
end.

if pResponse:isFault()
 then return.

/* create the data file */
tDataFile = guid(generate-uuid).
output to value(tDataFile).
put unformatted "~{" skip.
for first ttPar no-lock:
  put unformatted "  ~"OfficeId~": " + string(ttPar.officeID) + "," skip.
  put unformatted "  ~"UserName~": ~"" + ttPar.uid + "~"," skip.
  put unformatted "  ~"GFNumber~": ~"" + ttPar.fileNumber + "~"," skip.
  put unformatted "  ~"OwnersPolicy~": " + (if ttPar.ownersPolicy then "true" else "false") + "," skip.
  put unformatted "  ~"OwnersPolicyAmount~": " + string(ttPar.ownersPolicyAmount) + "," skip.
  put unformatted "  ~"LoanPolicy~": " + (if ttPar.loanPolicy then "true" else "false") + "," skip.
  put unformatted "  ~"LoanPolicyAmount~": " + string(ttPar.loanPolicyAmount) + "," skip.
  put unformatted "  ~"OtherPolicy~": " + (if ttPar.otherPolicy then "true" else "false") + "," skip.
  if ttPar.otherPolicyInfo > ""
   then put unformatted "  ~"OtherPolicyInfo~": ~"" + ttPar.otherPolicyInfo + "~"," skip.
  if ttPar.propertyCounties > ""
   then put unformatted "  ~"PropertyCounties~": ~"" + ttPar.propertyCounties + "~"," skip.
  if ttPar.propertyState > ""
   then put unformatted "  ~"PropertyState~": ~"" + ttPar.propertyState + "~"," skip.
  if ttPar.propertyType > ""
   then put unformatted "  ~"PropertyType~": ~"" + ttPar.propertyType + "~"," skip.
  if ttPar.propertyCondition > ""
   then put unformatted "  ~"Condition~": ~"" + ttPar.propertyCondition + "~"," skip.
  if ttPar.propertyAccess > ""
   then put unformatted "  ~"Access~": ~"" + ttPar.propertyAccess + "~"," skip.
  if ttPar.accessBasis > ""
   then put unformatted "  ~"AccessBasis~": ~"" + ttPar.accessBasis + "~"," skip.
  if ttPar.easementExamined > ""
   then put unformatted "  ~"EasementExamined~": ~"" + ttPar.easementExamined + "~"," skip.
  put unformatted "  ~"PropertyTitle~": ~"" + ttPar.propertyTitle + "~"," skip.
  if ttPar.recentConstruction > ""
   then put unformatted "  ~"RecentConstruction~": ~"" + ttPar.recentConstruction + "~"," skip.
  if ttPar.improvementsComplete > ""
   then put unformatted "  ~"ImprovementsComplete~": ~"" + ttPar.improvementsComplete + "~"," skip.
  if ttPar.improvementsCompleteDate <> ?
   then put unformatted "  ~"ImprovementsCompleteDate~": ~"" + iso-date(ttPar.improvementsCompleteDate) + "~"," skip.
  if ttPar.constructionContemplated > ""
   then put unformatted "  ~"ConstructionContemplated~": ~"" + ttPar.constructionContemplated + "~"," skip.
  put unformatted "  ~"Risks~": ~"" + ttPar.risks + "~"," skip.
  if ttPar.risksOther > ""
   then put unformatted "  ~"RisksOther~": ~"" + ttPar.risksOther + "~"," skip.
  if ttPar.endorsements > ""
   then put unformatted "  ~"Endorsements~": ~"" + ttPar.endorsements + "~"," skip.
  put unformatted "  ~"FileParentId~": ~"" + tTempParent + "~"" skip.
end.
put unformatted "~}" skip.
output close.
publish "AddTempFile" ("ARCActivity", tDataFile).

/* send to ARC */
publish "GetSystemParameter" ("ARCSendPar", output tURL).
run HTTPPost ("ARCParNew", tURL, CreateArcHeader("Accept:application/xml"), "application/json", search(tDataFile), output pSuccess, output pMsg, output tResponseFile).

if not pSuccess
 then pResponse:fault("3005", "Failed to connect to the ARC").

if not parseXML(output std-ch)
 then pResponse:fault("3005", "Failed to parse XML.").
    
if tParID = 0
 then pResponse:fault("3005", "Failed to create a PAR.").
 else
  do:
    for first agentfile exclusive-lock
        where agentfile.agentFileID = pAgentFileID:
        
      agentfile.parID = tParID.
    end.
    pResponse:success("2002", "PAR").
  end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-Characters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Characters Procedure 
PROCEDURE Characters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEFINE INPUT PARAMETER ppText AS MEMPTR NO-UNDO.
  DEFINE INPUT PARAMETER piNumChars AS INTEGER NO-UNDO.

  assign tValue = tValue + GET-STRING(ppText,1,piNumChars) no-error.
  if error-status:error 
   then tValue = ?.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-EndElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE EndElement Procedure 
PROCEDURE EndElement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter namespaceURI as character.
  define input parameter localName    as character.
  define input parameter qName        as character.
  
  case qName:
   when "Id" then tParID = integer(tValue) no-error.
  end case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/get-temp-table.i &t=arc-output-document &setParam="'CompassDocument'"}
  tCurrentElement = qName.
  tValue = "".
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

