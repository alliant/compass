
/*------------------------------------------------------------------------
@file getarcsignatories-p.p
@action arcSignatoriesGet
@description Get the ARC signatories for an office

@author John Oliver
@created 2022.03.24
@modified
Name          Date       
Sagar k      3-23-20223 
Spandana     04-19-2023

------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Temp Tables */
{tt/arcpolicytype.i &nodeName=PolicyType}

/* Parameters */
define input  parameter  pOfficeID   as character no-undo.
define output parameter  pError      as logical   no-undo.
define output parameter  pErrorMsg   as character no-undo.
define output parameter  pCount      as integer   no-undo.
define output parameter  dataset-handle dsHandle.

create dataset  dsHandle.
dsHandle:xml-node-name = 'data'.
dsHandle:set-buffers(buffer arcpolicytype:handle).

/* Local variables */
{lib/std-def.i}
define variable tUrl            as character no-undo.
define variable tResponseFile   as character no-undo.

/* Functions and Procedures */
{lib/run-http.i}
{lib/arc-bearer-token.i}

/* ***************************  Main Block  *************************** */

publish "GetSystemParameter" ("ARCGetPolicyTypes", output tUrl).
tUrl = substitute(tUrl, string(pOfficeID)).
 
run HTTPGet ("GetARCPolicyType", tUrl, CreateArcHeader(""), output std-lo, output std-ch, output tResponseFile).

if not std-lo  
 then
  do:
    assign pError = true
           pErrorMsg = "Failed to connect to the ARC".
    return.
  end.

std-lo = temp-table arcpolicytype:read-json("FILE",tResponseFile) no-error.

/* count how many policies */
pCount = 0.

for each arcpolicytype exclusive-lock:

  arcpolicytype.officeid = pOfficeID.
  if arcpolicytype.policyType = "C"
    then delete arcpolicytype.
  else pCount = pCount + 1.
   
end.
