/*------------------------------------------------------------------------
@name  GetARcPolicyType.cls
@action ARcParGet
@description  Get the agent journal information from ARC.
@returns Success;2005
@author K.R
@version 1.0
@created 15.03.2023
@Modified :
Name          Date            Comments
Sagar k      3-23-20223       changed as per new framework
----------------------------------------------------------------------*/

class arc.GetArcPolicyType inherits framework.ActionBase:

  constructor public GetArcPolicyType ():
   super().
  end constructor.

  destructor public GetArcPolicyType ( ):
  end destructor.
    

  {lib/callsp-defs.i}
  define variable pResponseFile as character no-undo.

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
    
    define variable lError        as logical   no-undo.
    define variable cErrorMsg     as character no-undo.
    define variable cOfficeID     as character no-undo.
    define variable iCount        as integer   no-undo.
    define variable dsHandle      as handle    no-undo.

    pRequest:getParameter("OfficeID", output cOfficeID).
    
    run arc/getarcpolicytypes-p.p(input  cOfficeID,
                         output lError,
                         output cErrorMsg,
                         output iCount,
                         output dataset-handle dsHandle).
    if lError 
     then
      do:
       pResponse:fault ("3005",  "GetARCPolicyType Failed: " + cErrorMsg).
       return.
      end.
 
   if iCount > 0
    then
     setcontentdataset(input dataset-handle dsHandle,input pRequest:FieldList).

   pResponse:success("2000", string(iCount) + "Policy Type").
         
  end method.

end class.



