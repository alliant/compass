&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file policybatchreport.p
@description Sends the policies to the ARC for marking as paid

@param BatchID;integer;The batch number to send the policies

@author John Oliver
@created 12.04.2019
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

define input  parameter pBatchID as integer   no-undo.
define output parameter pSuccess as logical   no-undo initial false.
define output parameter pMsg     as character no-undo.

/* Parmaeters ---                                                       */
define variable pNumBad as character no-undo initial "".

/* Temp Tables ---                                                      */

/* Local Variables ---                                                  */
{lib/std-def.i}
define variable tURL          as character no-undo.
define variable tPolicy       as character no-undo.
define variable tResponseFile as character no-undo.
define variable tARCFile      as character no-undo.

/* Functions ---                                                        */
{lib/add-delimiter.i}
{lib/run-http.i}
{lib/jsonparse.i &jsonfile=tResponseFile &root=Result &has-namespace=true}
{lib/arc-bearer-token.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* get the policies */
std-in = 0.
for each batchform no-lock
   where batchform.batchID = pBatchID
     and batchform.formType = "P":
   
  if lookup(string(batchform.policyID), tPolicy) = 0
   then tPolicy = addDelimiter(tPolicy, ",") + string(batchform.policyID).
   
  std-in = std-in + 1.
end.

/* create a POST file */
tARCFile = replace(guid, "-", "").
output to value(tARCFile).
put unformatted "~{" skip.
put unformatted "  ~"batchID~":" + string(pBatchID) + "," skip.
put unformatted "  ~"policyIds~":" + "[" + tPolicy + "]" skip.
put unformatted "~}" skip.
output close.
publish "AddTempFile" (tARCFile, search(tARCFile)).

/* call the ARC */
publish "GetSystemParameter" ("ARCPolicyBatchReport", output tURL).
run HTTPPost ("PolicyBatchReport", tURL, CreateArcHeader("Accept:application/json"), "application/json", search(tARCFile), output std-lo, output std-ch, output tResponseFile).

if parseJson(output std-ch) and pNumBad > ""
 then 
  do:
    pMsg = "The following policies were not marked as paid: " + pNumBad.
    return.
  end.

pSuccess = true.
pMsg = string(num-entries(tPolicy) - num-entries(pNumBad)) + " Policies reported to ARC.".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-EndElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE EndElement Procedure 
PROCEDURE EndElement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pNamespace   as character no-undo.
  define input parameter pLocalName   as character no-undo.
  define input parameter pElementName as character no-undo.

  case pLocalName:
    when "NotUpdatedIds" then pNumBad = addDelimiter(pNumBad, ", ") + curElementValue.
  end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

