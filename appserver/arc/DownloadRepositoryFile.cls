/*------------------------------------------------------------------------
@name  DownloadRepositoryFile.cls
@action repositoryFileDownload
@description  Download the file from ARC Repository.
@returns Success;2000
@author Vignesh Rajan
@version 1.0
@created 04.01.2024
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/

class arc.DownloadRepositoryFile inherits framework.ActionBase:

  constructor public DownloadRepositoryFile ():
   super().
  end constructor. 

  define variable pResponse     as framework.IRespondable no-undo.
  define variable pRequest      as framework.IRequestable no-undo.

  define variable cFileName     as character no-undo.

  method public override void act (input pActRequest as framework.IRequestable,
                                   input pActResponse as framework.IRespondable):

    {lib/std-def.i}

    define variable ipcFileID    as character no-undo.
    define variable iopcFileName as character no-undo.

    define variable lSuccess as logical no-undo.
    define variable cMsg     as character no-undo.

    assign 
        pRequest  = pActRequest
        pResponse = pActResponse
        .
       
    pRequest:getParameter("FileID", output ipcFileID).
    pRequest:getParameter("FileName", output iopcFileName).

    if ipcFileID = "" or ipcFileID = ?
     then
      do:
        pResponse:fault('3005', 'File ID cannot be blank').
        return.
     end.

    if iopcFileName = "" or iopcFileName = ?
     then
      do:
        pResponse:fault('3005', 'File Name cannot be blank').
        return.
      end.
    

    run arc/downloadrepositoryfile-p.p(input ipcFileID,
                                       input-output iopcFileName,
                                       output lSuccess,
                                       output cMsg).
    if not lSuccess
     then
      do:
        pResponse:fault('3005', "DownloadRepositoryFile Failed: " + cMsg).
        return.
      end.

    cFileName = iopcFileName.

  end method.

  method public override char render(input pFormat as char, input pTemplate as char):
    
    if pResponse:isfault() then return ''.
    pResponse:success("2000", "DownloadRepositoryFile").
    return cFileName.

  end method.
  

  destructor public DownloadRepositoryFile ( ):
  end destructor.  

end class.



