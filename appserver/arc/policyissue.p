&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file policyissue.p
@description Issues the policies sent from the ARC into Compass

@param arcpolicy;complex;The ARC Policy

@author John Oliver
@created 12.04.2019
@modification
Date       Name          Description
02/03/20   Rahul Sharma  Modified to set agentfile status as "Open"
01/07/21   Shubham       Added validation not to store null value.
11/15/24   SRK           Modified to call liabilityamountalert-p

*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

/* Temp Tables ---                                                      */
{tt/arcpolicy.i}
{tt/agentfile.i &tableAlias="ttAgentFile"}

/* Local Variables ---                                                  */
{lib/std-def.i}
define variable tRefType as character no-undo.
define variable tRefNum  as character no-undo.
define variable cFileID  as character no-undo.
define variable tCount   as integer   no-undo initial 0.
def var tUpdError        as logical   no-undo.
def var tUpdMsg          as character no-undo.
define variable ipolicyID  as integer no-undo. 

/* Functions ---                                                        */
{lib/normalizefileid.i}
{lib/add-delimiter.i}

&scoped-define PolicyStatus "I"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

empty temp-table arcpolicy.
empty temp-table ttAgentFile.

pRequest:getParameter("Root", input-output table arcpolicy).

if not can-find(first arcpolicy)
 then 
  do:
    std-ch = "".
    pRequest:getParameter("JSON", output std-ch).
    pResponse:fault("3005", "No policy data retrieved. The JSON is:" + chr(13) + chr(13) + std-ch).
    return.
  end.


TRX-BLOCK:
for each arcpolicy no-lock 
  on error UNDO TRX-BLOCK, next TRX-BLOCK
  TRANSACTION:

  assign
    tRefType = arcpolicy.Source
    tRefNum  = addDelimiter(tRefNum, ",") + string(arcpolicy.PolicyId)
    .
  /* if a policy already exists, call it good */
  if can-find(first policy where PolicyId = arcpolicy.PolicyId)
   then
    do:
      pResponse:addFault("3011", "Policy" {&msg-add} "PolicyId" {&msg-add} string(arcpolicy.PolicyId)).
      next TRX-BLOCK.
    end.
    
  cFileID = normalizeFileID(arcpolicy.GfNumber).
 
  if cFileID = ? or cFileID = "?" 
   then
    cFileID = "".
    
  /* create the policy */
  create policy.
  policy.PolicyId = arcpolicy.PolicyId.
  ipolicyID = arcpolicy.PolicyId. 
  validate policy.
  
  /* add the additional elements */
  assign
    policy.agentID      = arcpolicy.agentID
    policy.stat         = "I"   /* I)ssued, P)rocessed, V)oided */
    policy.companyID    = arcpolicy.OfficeId
    policy.fileNumber   = arcpolicy.GfNumber
    policy.fileID       = cFileID
    policy.issueDate    = arcpolicy.IssueDate
    policy.voidDate     = ?
    policy.invoiceDate  = ?
    policy.periodID     = 0
    policy.reprocessed  = false
    policy.officeID     = integer(arcpolicy.OfficeID)
    
    /* Suggested values when policy issued by the Agent */
    policy.issuedFormID          = string(arcpolicy.PolicyTemplateId)
    policy.issuedLiabilityAmount = arcpolicy.LiabilityAmount
    policy.issuedGrossPremium    = arcpolicy.GrossPremiumAmount
    policy.issuedResidential     = arcpolicy.Residential
    policy.issuedEffDate         = arcpolicy.EffectiveDate
    
    /* Current values (from processing) */
    policy.formID           = arcpolicy.FormId
    policy.statcode         = ""
    policy.effDate          = ?
    policy.liabilityAmount  = 0
    policy.grossPremium     = 0
    policy.netPremium       = 0
    policy.retention        = 0
    policy.residential      = if arcpolicy.Residential <> ? then arcpolicy.Residential else true
    policy.countyID         = ""
    
    policy.trxID            = substring(string(year(today), "9999"), 3) + string(month(today), "99") + caps(encode("policyIssue" + string(now)))
    policy.trxDate          = now
    .
  
  create ttAgentFile.
  assign
    ttAgentFile.agentID = policy.agentID
    ttAgentFile.fileNumber = policy.fileNumber
    ttAgentFile.fileID = policy.fileID
    .
  
  std-ch = "".
  for first agent no-lock
    where agent.agentID = arcpolicy.agentId
      and agent.agentID > "":
        
      buffer-copy agent except agent.agentID agent.addr1 agent.addr2 agent.addr3 agent.addr4 agent.city agent.state agent.zip 
                  to ttAgentFile.
      
      assign
        ttAgentFile.agentID    = agent.agentID
        ttAgentFile.fileNumber = policy.fileNumber
        ttAgentFile.fileID     = policy.fileID
        ttAgentFile.stage      = pRequest:actionid
        ttagentfile.stat       = "O"
        ttAgentFile.notes      = pRequest:actionid + "," + "ARC," + "Policy Issued for Agent ID: " + agent.agentID + ", File ID: " + policy.fileID + ", Policy ID: " + string(policy.PolicyId)
        std-ch                 = agent.stateID
        .
  end.

  policy.stateID = std-ch.
  release policy.

  tCount = tCount + 1.
end. /* TRX-BLOCK */

/* update the agentfile table */
if tCount > 0 
 then run util/updateagentfile.p (pRequest:actionID,
                                  pRequest:clientID,
                                  pRequest:uid,
                                  table ttAgentFile,
                                  output tUpdError,
                                  output tUpdMsg).

pResponse:logMessage(tRefType, tRefNum, string(tCount) + " Policy(s) created as Issued ").

if pResponse:isFault()
 then 
  DO: std-ch = "".
      pRequest:getParameter("JSON", output std-ch).
      run util/sysmail.p ("Policy Issue had fault(s)", 
                          tRefType + " " + tRefNum + " " 
                          + string(tCount) + " Policy(s) created with status Issued; JSON is:" 
                          + chr(13) + chr(13) + std-ch
                          ).
  END.

run alert/liabilityamountalert-p.p(input string(ipolicyID),input 'P').    
/* das: ARC calling routine does not handle errors */ 
pResponse:success("2000", "ARC Policy Issue").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


