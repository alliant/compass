/*------------------------------------------------------------------------
@name  GetArcPolicy.cls
@action arcPolicyGet
@description  Get the ARC Policy
@returns Success;2005
@author Spandana
@version 1.0
@created 27.03.2023
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/

class arc.GetARcPolicy inherits framework.ActionBase:

  constructor public GetARcPolicy ():
   super().
  end constructor.

  destructor public GetARcPolicy ( ):
  end destructor.
    
  {lib/callsp-defs.i}

  define variable pResponseFile as character no-undo.

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
    
    define variable lError        as logical   no-undo.
    define variable lErrorMsg     as character no-undo.
    define variable cPolicyIDs    as character no-undo.
    define variable iCount        as integer   no-undo.
    define variable dsHandle      as handle    no-undo.

    /* ***************************  Main Block  *************************** */

    pRequest:getParameter("PolicyIDs", output cPolicyIDs).
  
    run arc/getarcpolicy-p.p(input  cPolicyIDs,
                             output lError,
                             output lErrorMsg,
                             output iCount,
                             output dataset-handle dsHandle).

    if lError 
     then
      do:
       pResponse:fault ("3005",  "GetARCPolicy Failed: " + lErrorMsg).
       return.
      end.
    
    if iCount > 0 
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2000", string(iCount) + " Policy").
         
  end method.

end class.
