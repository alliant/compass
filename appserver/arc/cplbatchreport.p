&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file policybatchreport.p
@description Sends the policies to the ARC for marking as paid

@param BatchID;integer;The batch number to send the policies

@author John Oliver
@created 12.04.2019
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

define input  parameter pBatchID as integer   no-undo.
define output parameter pSuccess as logical   no-undo initial false.
define output parameter pMsg     as character no-undo.

/* Parmaeters ---                                                       */
define variable pCPLList    as character no-undo.
define variable pNumBad     as character no-undo initial "".
define variable cErrMsg     as character no-undo.

/* Temp Tables ---                                                      */

/* Local Variables ---                                                  */
{lib/std-def.i}
define variable tURL          as character no-undo.
define variable tResponseFile as character no-undo.
define variable tARCFile      as character no-undo.

/* Functions ---                                                        */
{lib/add-delimiter.i}
{lib/run-http.i}
{lib/jsonparse.i &jsonfile=tResponseFile &root=Result &has-namespace=true}
{lib/arc-bearer-token.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* get the CPLs */
for each cpl no-lock
   where cpl.batchID = pBatchID
     and cpl.stat <> "V":
  
  pCPLList = addDelimiter(pCPLList, "~",~"") + cpl.cplID.
end.
  
/* create a POST file */
tARCFile = replace(guid, "-", "").
output to value(tARCFile).
put unformatted "~{" skip.
put unformatted "  ~"BatchID~":" + string(pBatchID) + "," skip.
put unformatted "  ~"CPLCodes~":" + "[~"" + pCPLList + "~"]" skip.
put unformatted "~}" skip.
output close.
publish "AddTempFile" (tARCFile, search(tARCFile)).

/* call the ARC */
publish "GetSystemParameter" ("ARCCPLBatchReport", output tURL).
run HTTPPost ("CPLBatchReport", tURL, CreateArcHeader("Accept:application/json"), "application/json", search(tARCFile), output std-lo, output std-ch, output tResponseFile).

pNumBad = "".
if parseJson(output std-ch) and pNumBad > ""
 then 
  do:
    pMsg = "The following CPLs were not marked as paid: " + pNumBad.  
    return.
  end.

pSuccess = true.
pMsg = string(num-entries(pCPLList) - num-entries(pNumBad)) + " CPL(s) reported to ARC.".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-EndElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE EndElement Procedure 
PROCEDURE EndElement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pNamespace   as character no-undo.
  define input parameter pLocalName   as character no-undo.
  define input parameter pElementName as character no-undo.

  case pLocalName:
    when "NotUpdatedIds" then pNumBad = addDelimiter(pNumBad, ", ") + curElementValue.
  end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

