/*------------------------------------------------------------------------
@name  GetArcPar.cls
@action ARcParGet
@description  Get the agent journal information from ARC.
@returns Success;2005
@author K.R
@version 1.0
@created 15.03.2023
@Modified :
Name          Date            Comments
Sagar k      3-29-20223       changed as per new framework
----------------------------------------------------------------------*/

class arc.GetArcPar inherits framework.ActionBase:

  constructor public GetArcPar ():
   super().
  end constructor.

  destructor public GetArcPar ( ):
  end destructor.
    

  {lib/callsp-defs.i}
  define variable pResponseFile as character no-undo.

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
    
    define variable pError        as logical   no-undo.
    define variable pErrorMsg     as character no-undo.
    define variable pParID        as integer   no-undo.
    define variable pEntity       as character no-undo.
    define variable pEntityID     as character no-undo.
    define variable iCount        as integer   no-undo.
    define variable dsHandle      as handle    no-undo.

    pRequest:getParameter("ParID", output pParID).
    pRequest:getParameter("Entity", output pEntity).
    pRequest:getParameter("EntityID", output pEntityID).
    
    run arc/getarcpar-p.p(input pParID,
                         input  pEntity,
                         input  pEntityID,
                         output pError,
                         output pErrorMsg,
                         output iCount,
                         output dataset-handle dsHandle).

    if pError 
     then
      do:
       pResponse:fault ("3005",  "GetARCPar Failed: " + pErrorMsg).
       return.
      end.
 
    if iCount > 0 
     then
      setcontentdataset(input dataset-handle dsHandle,input pRequest:FieldList).

    pResponse:success("2000", string(iCount) + " Par").
         
  end method.

end class.



