/*------------------------------------------------------------------------
@name  GetAgentJournals.cls
@action agentJournalsGet
@description  Get the agent journal information from ARC.
@returns Success;2005
@author K.R
@version 1.0
@created 15.03.2023
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/
USING Progress.Json.ObjectModel.ObjectModelParser.
USING progress.Json.ObjectModel.*.

class arc.GetAgentJournals inherits framework.ActionBase:

  constructor public GetAgentJournals ():
   super().
  end constructor.

  destructor public GetAgentJournals ( ):
  end destructor.
    

  {lib/callsp-defs.i}
  define variable pResponseFile as character no-undo.

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
    
    define variable pAgentID      as character no-undo.
    define variable pError        as logical   no-undo.
    define variable pErrorMsg     as character no-undo.
    
    pRequest:getParameter("AgentID" ,output pAgentID ).

    run arc/getagentjournals-p.p(input pAgentID,
                                 output pError,
                                 output pErrorMsg,
                                 output pResponseFile).

    if pError 
     then pResponse:fault("3005","GetAgentJournal failed:" + PErrorMsg).
    
    parseResponseFile(pRequest, pResponse).
    if pResponse:isFault() 
     then
      return.

    pResponse:success("2000", "GetAgentJournal").
     
  end method.

  method public override char render (input pFormat as char, input pTemplate as char):
    define variable cFileName as character.

    cFileName = pResponseFile + ".json".
    os-rename value(pResponseFile) value(cFileName).
    tContentFile = cFileName.
    return tContentFile.
  end.

  method private void parseResponseFile(input pRequest as framework.IRequestable, 
                                        input pResponse as framework.IRespondable):

    define variable oParser              as ObjectModelParser no-undo.
    define variable oAgentJournalsParent as JsonObject        no-undo.
    define variable oAgentJournalsChild  as JsonArray         no-undo.
    define variable oResponse            as JsonObject        no-undo.
    
    oResponse = new JsonObject().
    oAgentJournalsParent = new JsonObject().
    oAgentJournalsChild= new JsonArray().

    /* Parse the JSON file into a JSON Array */
    oParser = NEW ObjectModelParser().
    oAgentJournalsChild = CAST(oParser:ParseFile(pResponseFile), JsonArray).

    /* Group the Arrays into a Json Object */
    oAgentJournalsParent:add('agentJournals', oAgentJournalsChild).
    oResponse:add('data',oAgentJournalsParent). 
    oResponse:WriteFile(pResponseFile,yes).

    catch eError as progress.Lang.error:
      pResponse:fault("3005","Parsing failed: " + eError:GetMessage(1)).
    end catch.
    
    finally:
      if valid-object(oParser) 
       then
        delete object oParser.

      if valid-object(oAgentJournalsParent) 
       then 
        delete object oAgentJournalsParent.

      if valid-object(oAgentJournalsChild) 
       then
        delete object oAgentJournalsChild.
    
      if valid-object(oResponse)
       then 
        delete object oResponse.
    end finally.

  end.

end class.



