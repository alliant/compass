
/*------------------------------------------------------------------------
@file getarcsignatories-p.p
@action arcSignatoriesGet
@description Get the ARC signatories for an office

@author John Oliver
@created 2022.03.24
@modified
Name          Date          Comments
Sagar k      3-27-20223     changed as per new framework
Spandana     04-19-2023     changed as Json format

------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Temp Tables */
{tt/arcsignatory.i &serializeName=Signatory }

/* Parameters */
define input  parameter  pOfficeID   as character no-undo.
define input  parameter  pUserID     as character no-undo.
define output parameter  pError      as logical   no-undo.
define output parameter  pErrorMsg   as character no-undo.
define output parameter  pCount      as integer   no-undo.
define output parameter  dataset-handle dsHandle.

create dataset  dsHandle.
dsHandle:xml-node-name = 'data'.
dsHandle:set-buffers(buffer arcsignatory:handle).

/* Local variables */
{lib/std-def.i}
define variable tUrl            as character no-undo.
define variable tResponseFile   as character no-undo.

/* Functions and Procedures */
{lib/run-http.i}
{lib/arc-bearer-token.i}

/* ***************************  Main Block  *************************** */

publish "GetSystemParameter" ("ARCGetSignatories", output tUrl).
tUrl = substitute(tUrl, pOfficeID, pUserID).
 
run HTTPGet ("GetARCSignatories", tUrl, CreateArcHeader(""), output std-lo, output std-ch, output tResponseFile).

if not std-lo  
 then
  do:
    assign pError = true
           pErrorMsg = "Failed to get the signatories.".
    return.
  end.

std-lo = temp-table arcsignatory:read-json("FILE",tResponseFile) no-error. 

/* count how many records*/
pCount = 0.
for each arcsignatory no-lock:
  arcsignatory.officeID = pOfficeID.
  pCount = pCount + 1.
end.
