/*------------------------------------------------------------------------
@name  GetArcUsersByAgent.cls
@action arcUsersByAgentGet
@description  Get the ARC users and offices based on the agentID.
@returns Success;2005
@author S.P
@version 1.0
@created 27.03.2023
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/


class arc.GetArcUsersByAgent inherits framework.ActionBase:

  constructor public GetArcUsersByAgent ():
   super().
  end constructor.

  destructor public GetArcUsersByAgent ( ):
  end destructor.
    
  {lib/callsp-defs.i}

  define variable pResponseFile as character no-undo.

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
    
    define variable lError     as logical   no-undo.
    define variable lErrorMsg  as character no-undo.
    define variable cAgentID   as character no-undo.
    define variable iCount     as integer   no-undo.
    define variable dsHandle   as handle    no-undo.

    /* ***************************  Main Block  *************************** */

    pRequest:getParameter("AgentID", output cAgentID).
  
    run arc/getarcusersbyagent-p.p(input  cAgentID,
                                   output lError,
                                   output lErrorMsg,
                                   output iCount,
                                   output dataset-handle dsHandle).

    if lError 
     then
      do:
       pResponse:fault ("3005",  "GetArcUsersByAgent Failed: " + lErrorMsg).
       return.
      end.
    
    if iCount > 0 
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2000", string(iCount) + "Agents").
         
  end method.

end class.



