/*------------------------------------------------------------------------
@name  GetRepositoryFilesList.cls
@action repositoryFilesListGet
@description  Get the files list from ARC Repository.
@returns Success;2005
@author Vignesh Rajan
@version 1.0
@created 28.03.2024
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/

class arc.GetRepositoryFilesList inherits framework.ActionBase:

  constructor public GetRepositoryFilesList ():
   super().
  end constructor.

  destructor public GetRepositoryFilesList ( ):
  end destructor.
  

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}

    define variable ipcEntity     as character no-undo.
    define variable ipcEntityID   as character no-undo.
    define variable iplOnlyPublic as logical   no-undo.

    define variable dsHandle      as handle    no-undo.
    define variable iFilesCount   as integer   no-undo.
    define variable lSuccess      as logical   no-undo.
    define variable cMsg          as character no-undo.

    pRequest:getParameter("Entity",     output ipcEntity).
    pRequest:getParameter("EntityID",   output ipcEntityID).
    pRequest:getParameter("OnlyPublic", output iplOnlyPublic).

    if ipcEntity = "" or ipcEntity = ?
     then
      do:
        pResponse:fault("3005", "Entity cannot be blank").
        return.
      end.

    if ipcEntityID = "" or ipcEntityID = ?
     then
      do:
        pResponse:fault("3005", "EntityID cannot be blank").
        return.
      end.


    run arc/getrepositoryfileslist-p.p(input ipcEntity,
                                       input ipcEntityID,
                                       input iplOnlyPublic,
                                       output dataset-handle dsHandle,
                                       output iFilesCount,
                                       output lSuccess,
                                       output cMsg).
    
    if not lSuccess
     then
      do:
        pResponse:fault('3005', "GetRepositoryFilesList Failed: " + cMsg).
        return.
      end.

    if iFilesCount > 0
     then
      setcontentdataset(input dataset-handle dsHandle, input pRequest:FieldList).
   

    pResponse:success("2005", string(iFilesCount) {&msg-add} "Repository File").
 
  end method.

end class.



