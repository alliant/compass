/*------------------------------------------------------------------------
@name ReprocessedFormsGet
@description Provides either a complete list of all reprocessed batch forms for a PeriodI
@param PeriodID;int;period ID to select
@returns BatchForms;complex;BatchForm dataset
@returns Success;int;2005
@throws 3066;Invalid PeriodID
@author D.Sinclair
@version 1.0 2.27/2015
@notes
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{tt/batchform.i &tableAlias="ttBatchForm"}

def var tPeriodID as int no-undo.
{lib/std-def.i}

pRequest:getParameter("PeriodID", OUTPUT tPeriodID).
if not CAN-FIND(FIRST period
                  WHERE period.periodID = tPeriodID)
 then
  DO: pResponse:fault("3066", "Period").
      RETURN.
  END.

std-in = 0.
for each batch fields (periodID batchID) no-lock
  where batch.periodID = tPeriodID,
    each batchform no-lock
  where batchform.batchID = batch.batchID
    and batchform.reprocess = true:
 create ttBatchForm.
 buffer-copy batchForm to ttBatchForm.
 std-in = std-in + 1.
end.

IF std-in > 0 
 THEN pResponse:setParameter("BatchForm", table ttBatchForm).
 
pResponse:success("2005", string(std-in) {&msg-add} "Form")
