&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*
@action GetPeriodStateStatCodeMix
@description Return the set of aggregate data of State/LiabRanges for a period range
@author D.Sinclair
@date 12.23.2014
@notes
@parameter sPeriodID;int;Starting periodID to review
@parameter ePeriodID;int;Ending periodID to review
@parameter state;ch;StateID to review (blank = all)
@parameter agent;ch;AgentID to review (blank = all)
@throws 3066;Invalid State or Period
@throws 3023;Invalid month (too low)
@throws 3024;Invalid month (too high)
@throws 3039;Invalid liability range (count > 32 or non-decimal values)
@throws 3042;Invalid year (too low)
@returns Success;int;2005;Count of batchforms read
@returns Data;complex based on ops18 definitions
*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{tt/periodmix.i &tableAlias="data"}

def temp-table ttpolicy
  field policyid        as int
  field liabilityAmount as dec
  index pu1 is primary unique policyid.
  
{lib/std-def.i}

def var sPeriodID as int no-undo.
def var ePeriodID as int no-undo.
def var pLiabRange as char no-undo.

def buffer x-data for data.
def buffer x-batchform for batchform.

def var tLiabilityAmount as deci.
def var tRangeRowid as rowid.
def var tCnt as int no-undo.


def var pStateID as char no-undo.
def var pAgentID as char no-undo.
def var pID as char no-undo.

def var tReportStateID as char no-undo.
def var tReportStatCode as char no-undo.
define variable dStartTime as datetime no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-createAllSet) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createAllSet Procedure 
FUNCTION createAllSet RETURNS LOGICAL PRIVATE
  ( pType as char,         /* A=AgentID, S=StateID */
    pCodeType as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createDataSet) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createDataSet Procedure 
FUNCTION createDataSet RETURNS ROWID PRIVATE
  ( input pType as char,
    input pID as char,
    input pCodeType as char,
    input pCode as char,
    input pLiabAmt as dec )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createTotalSet) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createTotalSet Procedure 
FUNCTION createTotalSet RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createZeroSet) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createZeroSet Procedure 
FUNCTION createZeroSet RETURNS LOGICAL PRIVATE
  ( input pType as char,        /* S=State, A=Agent, Blank=All */
    input pID as char,          /* StateID or AgentID */
    input pCodeType as char,    /* S=StatCode, F=FormID, Blank=All */
    input pCode as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 70.2.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 

dStartTime = now.

pRequest:getParameter("sPeriodID", output sPeriodID).
if not can-find(first period 
                  where period.periodID = sPeriodID)
 then
  do: pResponse:fault("3066", "Starting period").
      return.
  end.

pRequest:getParameter("ePeriodID", output ePeriodID).
if not can-find(first period 
                  where period.periodID = ePeriodID)
 then
  do: pResponse:fault("3066", "Ending period").
      return.
  end.

pRequest:getParameter("ranges", output pLiabRange).
if pLiabRange > ""
 then
  do:
      std-lo = false.
      do std-in = 1 to num-entries(pLiabRange):
       if std-in > 32 
        then
         do: pResponse:fault("3039", "Liability Range Count").
             return.
         end.

       std-de = decimal(entry(std-in, pLiabRange)) no-error.
       if error-status:error or std-de <= 0 
        then std-lo = true. /* error */
      end.
      if std-lo 
       then
        do: pResponse:fault("3039", "Liability Range Value").
            return.
        end.
  end.
 
pRequest:getParameter("state", output pStateID).
pStateID = caps(pStateID).

if pStateID <> "ALL" 
and not can-find(first state 
                  where state.stateID = pStateID)
 then
  do: pResponse:fault("3066", "State").
      return.
  end.

pRequest:getParameter("agent", output pAgentID).

/* This is so I can deploy to live before the new client is built */
if pAgentID = "" or pAgentID = ? then
assign pAgentID = "ALL".

if pAgentID <> "ALL"
and not can-find(first agent 
                  where agent.agentID = pAgentID)
 then
  do: pResponse:fault("3066", "Agent").
      return.
  end.

pID = if pAgentID <> "ALL" then pAgentID else pStateID.

/* Build index table of policies and 'net' liability amounts for bucket assigment
*/
tLiabilityAmount = 0.
for each period 
    where period.periodID >= sPeriodID
    and period.periodID <= ePeriodID
    no-lock,
  each batch 
    where batch.periodID = period.periodID
    and (if pStateID = "ALL" then true else batch.stateID = pStateID)
    and (if pAgentID = "ALL" then true else batch.agentID = pAgentID)
    and batch.stat = "C"
    no-lock,
  each batchform 
    where batchform.batchID = batch.batchID
    and batchform.formType = "P"
    no-lock
    break by batchform.policyid:
    
  tLiabilityAmount = tLiabilityAmount + batchform.liabilityDelta.
  
  if last-of(batchform.policyid) then
  do:
    create ttpolicy.
    assign
      ttpolicy.policyid        = batchform.policyid
      ttpolicy.liabilityAmount = tLiabilityAmount.
    
    tLiabilityAmount = 0.
  end.
end.

/* Build main data set to return to client
*/
tCnt = 0.
for each period 
    where period.periodID >= sPeriodID
    and period.periodID <= ePeriodID
    no-lock,
  each batch 
    where batch.periodID = period.periodID
    and (if pStateID = "ALL" then true else batch.stateID = pStateID)
    and (if pAgentID = "ALL" then true else batch.agentID = pAgentID)
    and batch.stat = "C"
    no-lock,
  each batchform 
    where batchform.batchID = batch.batchID
    no-lock:
    
    if batchform.statcode = "" 
     then tReportStatCode = batchform.formType.
     else tReportStatCode = batchform.statcode.

    tLiabilityAmount = 0.
    find ttpolicy where ttpolicy.policyid = batchform.policyid no-lock no-error.
    if avail ttpolicy then
    tLiabilityAmount = ttpolicy.liabilityAmount.
      
    if tLiabilityAmount = 0 
     then
      do: /* das -- find Policy and use policy.liabilityAmount ??
          find first policy 
            where policy.policyID = batchform.policyID no-error.
          tLiabilityAmount = policy.liabilityAmount.
           */

          /* These are "errors" so we capture them in a zero record */
          createZeroSet("S", pID, "S", tReportStatCode).
      end.

    /* mix-set.i START */
    find first data
      where data.type = "S"
        and data.ID = pID
        and data.codeType = "S"
        and data.code = tReportStatCode
        and data.minLiab <= tLiabilityAmount
        and data.maxLiab >= tLiabilityAmount no-error.
    if not available data 
     then
      do: tRangeRowid = createDataSet("S", pID, "S", tReportStatCode, tLiabilityAmount).
          find data
            where rowid(data) = tRangeRowid no-error.
            
          /* If no bucket found then skip it (this should not occur) */
          if not avail data then next.
      end.

    case batchform.formType:
     when "P" then assign
                     data.grossPremium = data.grossPremium + batchform.grossDelta
                     data.numPolicies = data.numPolicies + 1
                     /*data.amount = data.amount + tLiabilityAmount /* Not delta */ */
                     data.amount = data.amount + batchform.liabilityDelta /* now using delta */
                     .
     when "E" then data.endorsementPremium = data.endorsementPremium + batchform.grossDelta.
     otherwise data.otherPremium = data.otherPremium + batchform.grossDelta.
    end.

    /* The form count should only include Endorsements and 'other' */
    if batchform.formType <> "P" then
    assign
      data.numForms = data.numForms + 1.
      
    assign
      data.totalPremium = data.totalPremium + batchform.grossDelta
      data.netPremium = data.netPremium + batchform.netDelta
      data.retainedPremium = data.retainedPremium + batchform.retentionDelta
      .

    assign
      data.lowGross = min(data.lowGross, batchform.grossDelta)
      data.highGross = max(data.highGross, batchform.grossDelta)
      data.lowNet = min(data.lowNet, batchform.netDelta)
      data.highNet = max(data.highNet, batchform.netDelta)
      data.lowRetained = min(data.lowRetained, batchform.retentionDelta)
      data.highRetained = max(data.highRetained, batchform.retentionDelta)
      data.lowLiability = min(data.lowLiability, tLiabilityAmount)
      data.highLiability = max(data.highLiability, tLiabilityAmount)
      .
    /* mix-set.i END */

   tCnt = tCnt + 1.

end. /* for each period, batch, batchform */

/* createAllSet("S", "S"). */

createTotalSet().

message "retrieved " + trim(string(tCnt,">>,>>9")) + " rows in " + string(interval(now,dStartTime,"milliseconds") / 1000) + " seconds" view-as alert-box.

if tCnt > 0
 then pResponse:setParameter("Data", table data).
pResponse:success("2005", string(tCnt) {&msg-add} "Batch Form").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-createAllSet) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createAllSet Procedure 
FUNCTION createAllSet RETURNS LOGICAL PRIVATE
  ( pType as char,         /* A=AgentID, S=StateID */
    pCodeType as char ) :  /* S=StatCode, F=FormID */
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def buffer x-data for data.
def buffer data for data.

if tCnt = 0
 then return false.


  createDataSet("", "", "", "", 1).
  

  for each x-data
    where x-data.type = ""
      and x-data.ID = "":
   for each data
     where data.type = pType
       and data.codeType = pCodeType
       and data.code = "" /* Use the summary records */
       and data.minLiab = x-data.minLiab
       and data.maxLiab = x-data.maxLiab:
    assign
     x-data.grossPremium = x-data.grossPremium + data.grossPremium
     x-data.endorsementPremium = x-data.endorsementPremium + data.endorsementPremium
     x-data.otherPremium = x-data.otherPremiu + data.otherPremium
     x-data.totalPremium = x-data.totalPremium + data.totalPremium
     x-data.numForms = x-data.numForms + data.numForms
     x-data.numPolicies = x-data.numPolicies + data.numPolicies
     x-data.netPremium = x-data.netPremium + data.netPremium
     x-data.retainedPremium = x-data.retainedPremium + data.retainedPremium
     x-data.amount = x-data.amount + data.amount
     .
   end.
  end.
 
  for each data:
   if data.amount <> 0
    then assign
          data.grossRate = data.totalPremium / (data.amount / 1000)
          data.netRate = data.netPremium / (data.amount / 1000).
   if data.lowGross = 999999999 
    then data.lowGross = ?.
   if data.lowNet = 999999999 
    then data.lowNet = ?.
   if data.lowRetained = 999999999 
    then data.lowRetained = ?.
   if data.lowLiability = 999999999 
    then data.lowLiability = ?.
   if data.highGross = -999999999 
    then data.highGross = ?.
   if data.highNet = -999999999 
    then data.highNet = ?.
   if data.highRetained = -999999999 
    then data.highRetained = ?.
   if data.highLiability = -999999999 
    then data.highLiability = ?.
  end.

 
  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createDataSet) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createDataSet Procedure 
FUNCTION createDataSet RETURNS ROWID PRIVATE
  ( input pType as char,
    input pID as char,
    input pCodeType as char,
    input pCode as char,
    input pLiabAmt as dec ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  def buffer local-data for data.
  def var tNextMin as decimal no-undo.

 TRY-BLOCK:
 do on error undo, retry:
  if retry 
   then leave TRY-BLOCK.

  /* Create single bucket if no ranges have been defined
  */
  if pLiabRange = "" 
   then
    do: create local-data.
        assign
          local-data.type          = pType
          local-data.ID            = pID
          local-data.sPeriodID     = sPeriodID
          local-data.ePeriodID     = ePeriodID
          local-data.codeType      = pCodeType
          local-data.code          = pCode
          local-data.minLiab       = -999999999.99
          local-data.maxLiab       = 999999999.99
          local-data.description   = "ALL"
          local-data.lowGross      = 999999999
          local-data.lowNet        = 999999999
          local-data.lowRetained   = 999999999
          local-data.lowLiability  = 999999999
          local-data.highGross     = -999999999
          local-data.highNet       = -999999999
          local-data.highRetained  = -999999999
          local-data.highLiability = -999999999
          .

        return rowid(local-data).
    end.

  /* Create less than zero bucket
  */
  create local-data.
  assign
    local-data.type          = pType
    local-data.ID            = pID
    local-data.sPeriodID     = sPeriodID
    local-data.ePeriodID     = ePeriodID
    local-data.codeType      = pCodeType
    local-data.code          = pCode
    local-data.minLiab       = -999999999.99
    local-data.maxLiab       = 0
    local-data.description   = "<= $0"
    local-data.lowGross      = 999999999
    local-data.lowNet        = 999999999
    local-data.lowRetained   = 999999999
    local-data.lowLiability  = 999999999
    local-data.highGross     = -999999999
    local-data.highNet       = -999999999
    local-data.highRetained  = -999999999
    local-data.highLiability = -999999999
    .
  
  /* Create user-defined buckets
  */
  do std-in = 1 to num-entries(pLiabRange):
   if std-in = 1 
    then
     do: create local-data.
         assign
           local-data.type      = pType
           local-data.ID        = pID
           local-data.sPeriodID = sPeriodID
           local-data.ePeriodID = ePeriodID
           local-data.codeType  = pCodeType
           local-data.code      = pCode
           local-data.minLiab   = 0.01
           local-data.maxLiab   = decimal(entry(std-in, pLiabRange))
           .
     end.
    else
     do: create local-data.
         assign
           local-data.type      = pType
           local-data.ID        = pID
           local-data.sPeriodID = sPeriodID
           local-data.ePeriodID = ePeriodID
           local-data.codeType  = pCodeType
           local-data.code      = pCode
           local-data.minLiab   = tNextMin
           local-data.maxLiab   = decimal(entry(std-in, pLiabRange))
           .
     end.
     
   assign
    tNextMin                 = local-data.maxLiab + 0.01
    local-data.description   = trim(string(local-data.minLiab, "$ >>>,>>>,>>9.99")) + " to " + 
                               trim(string(local-data.maxLiab, ">>>,>>>,>>9.99"))
    local-data.lowGross      = 999999999
    local-data.lowNet        = 999999999
    local-data.lowRetained   = 999999999
    local-data.lowLiability  = 999999999
    local-data.highGross     = -999999999
    local-data.highNet       = -999999999
    local-data.highRetained  = -999999999
    local-data.highLiability = -999999999
    .
  end.
  
  /* Create high limit bucket
  */
  create local-data.
  assign
    local-data.type          = pType
    local-data.ID            = pID
    local-data.sPeriodID     = sPeriodID
    local-data.ePeriodID     = ePeriodID
    local-data.codeType      = pCodeType
    local-data.code          = pCode
    local-data.minLiab       = tNextMin
    local-data.maxLiab       = 999999999.99
    local-data.description   = trim(string(local-data.minLiab, "$ >>>,>>>,>>9.99")) + "+"
    local-data.lowGross      = 999999999
    local-data.lowNet        = 999999999
    local-data.lowRetained   = 999999999
    local-data.lowLiability  = 999999999
    local-data.highGross     = -999999999
    local-data.highNet       = -999999999
    local-data.highRetained  = -999999999
    local-data.highLiability = -999999999
    .

  find first local-data
    where local-data.type = pType
      and local-data.ID = pID
      and local-data.codeType = pCodeType
      and local-data.code = pCode
      and local-data.minLiab <= pLiabAmt
      and local-data.maxLiab >= pLiabAmt
      no-lock no-error.

  if avail local-data then
  RETURN rowid(local-data).   /* Function return value. */
  else
  do:
    output to D:\webspeed\logs\busmixerrors.log append.
    put unformatted pType + "," + pID + "," + pCodeType + "," + pCode + "," + string(pLiabAmt) skip.
    output close.
    return ?.
  end.
 end.
 RETURN ?.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createTotalSet) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createTotalSet Procedure 
FUNCTION createTotalSet RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  def buffer xdata for data.
  def buffer tdata for data.

  for each xdata 
    where xdata.code <> "TOTAL"
    no-lock
    break by xdata.maxLiab:
    
    find first tdata
      where tdata.type = xdata.type
      and tdata.ID = xdata.ID
      and tdata.codeType = xdata.codeType
      and tdata.code = "TOTAL"
      and tdata.minLiab = xdata.minLiab
      and tdata.maxLiab = xdata.maxLiab
      no-error.
      
    if not avail tdata then
    do:
      create tdata.
      assign
        tdata.type = xdata.type
        tdata.ID = xdata.ID
        tdata.codeType = xdata.codeType
        tdata.code = "TOTAL"
        tdata.minLiab = xdata.minLiab
        tdata.maxLiab = xdata.maxLiab
        tdata.description = xdata.description
        tdata.sPeriodID = xdata.sPeriodID
        tdata.ePeriodID = xdata.ePeriodID
        tdata.lowGross = 999999999
        tdata.lowNet = 999999999
        tdata.lowRetained = 999999999
        tdata.lowLiability = 999999999.
    end.
    
    assign
      tdata.numForms = tdata.numForms + xdata.numForms
      tdata.numPolicies = tdata.numPolicies + xdata.numPolicies
      tdata.grossPremium = tdata.grossPremium + xdata.grossPremium
      tdata.endorsementPremium = tdata.endorsementPremium + xdata.endorsementPremium
      tdata.otherPremium = tdata.otherPremium + xdata.otherPremium
      tdata.totalPremium = tdata.totalPremium + xdata.totalPremium
      tdata.netPremium = tdata.netPremium + xdata.netPremium
      tdata.retainedPremium = tdata.retainedPremium + xdata.retainedPremium
      tdata.amount = tdata.amount + xdata.amount
      tdata.highGross = max(tdata.highGross, xdata.highGross)
      tdata.highNet = max(tdata.highNet, xdata.highNet)
      tdata.highRetained = max(tdata.highRetained, xdata.highRetained)
      tdata.highLiability = max(tdata.highLiability, xdata.highLiability)
      .
      
      if xdata.lowGross > 0 then
      tdata.lowGross = min(tdata.lowGross, xdata.lowGross).
      
      if xdata.lowNet > 0 then
      tdata.lowNet = min(tdata.lowNet, xdata.lowNet).
      
      if xdata.lowRetained > 0 then
      tdata.lowRetained = min(tdata.lowRetained, xdata.lowRetained).
      
      if xdata.lowLiability > 0 then
      tdata.lowLiability = min(tdata.lowLiability, xdata.lowLiability).
  end.
  
  for each tdata where tdata.code = "TOTAL":
   if tdata.amount <> 0
    then assign
          tdata.grossRate = tdata.totalPremium / (tdata.amount / 1000)
          tdata.netRate = tdata.netPremium / (tdata.amount / 1000).
   if tdata.lowGross = 999999999 
    then tdata.lowGross = ?.
   if tdata.lowNet = 999999999 
    then tdata.lowNet = ?.
   if tdata.lowRetained = 999999999 
    then tdata.lowRetained = ?.
   if tdata.lowLiability = 999999999 
    then tdata.lowLiability = ?.
   if tdata.highGross = -999999999 
    then tdata.highGross = ?.
   if tdata.highNet = -999999999 
    then tdata.highNet = ?.
   if tdata.highRetained = -999999999 
    then tdata.highRetained = ?.
   if tdata.highLiability = -999999999 
    then tdata.highLiability = ?.
  end.

  RETURN TRUE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createZeroSet) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createZeroSet Procedure 
FUNCTION createZeroSet RETURNS LOGICAL PRIVATE
  ( input pType as char,        /* S=State, A=Agent, Blank=All */
    input pID as char,          /* StateID or AgentID */
    input pCodeType as char,    /* S=StatCode, F=FormID, Blank=All */
    input pCode as char ) :     /* StatCode or FormID */
/*------------------------------------------------------------------------------
  Purpose:  
    Notes: Each of the following special zero buckets may have been created before,
           because the STATcode and FormID are not inter-dependent. 
------------------------------------------------------------------------------*/
  def buffer local-data for data.

  if not can-find(local-data
                    where local-data.type = pType
                      and local-data.ID = pID
                      and local-data.codeType = pCodeType
                      and local-data.code = pCode
                      and local-data.minLiab = 0
                      and local-data.maxLiab = 0)
   then
    do:
      create local-data.
      assign
        local-data.type = pType
        local-data.ID = pID
        local-data.codeType = pCodeType
        local-data.code = pCode
    
        local-data.sPeriodID = sPeriodID
        local-data.ePeriodID = ePeriodID
    
        local-data.minLiab = 0.0
        local-data.maxLiab = 0.0
        local-data.description = "Zero (no policy)"
        .
    end.

/*
  if not can-find(local-data
                    where local-data.type = pType
                      and local-data.ID = pID
                      and local-data.codeType = pCodeType
                      and local-data.code = "" /* All */
                      and local-data.minLiab = 0
                      and local-data.maxLiab = 0)
   then
    do:
      create local-data.
      assign
        local-data.type = pType
        local-data.ID = pID
        local-data.codeType = pCodeType
        local-data.code = "" /* All */
    
        local-data.startMonth = pFromMonth
        local-data.endMonth = pToMonth
        local-data.rangeYear = pYear
    
        local-data.minLiab = 0.0
        local-data.maxLiab = 0.0
        local-data.description = "Zero (no policy)"
        .
    end.


  if not can-find(local-data
                    where local-data.type = "" /* All */
                      and local-data.ID = ""
                      and local-data.codeType = "" /* All */
                      and local-data.code = ""
                      and local-data.minLiab = 0
                      and local-data.maxLiab = 0)
   then
    do:
      create local-data.
      assign
        local-data.type = ""
        local-data.ID = ""
        local-data.codeType = ""
        local-data.code = ""
    
        local-data.startMonth = pFromMonth
        local-data.endMonth = pToMonth
        local-data.rangeYear = pYear
    
        local-data.minLiab = 0.0
        local-data.maxLiab = 0.0
        local-data.description = "Zero (no policy)"
        .
    end.
 */

  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

