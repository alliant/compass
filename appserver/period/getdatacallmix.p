&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*
@action GetDataCallMix
@description Return the set of aggregate data of State/LiabRanges for a period range
@author B.Johnson
@date 11.25.2015
@notes
@parameter sPeriodID;int;Starting periodID to review
@parameter ePeriodID;int;Ending periodID to review
@parameter state;ch;StateID to review (blank = all)
@parameter agent;ch;AgentID to review (blank = all)
@throws 3066;Invalid State or Period
@throws 3023;Invalid month (too low)
@throws 3024;Invalid month (too high)
@throws 3039;Invalid liability range (count > 32 or non-decimal values)
@throws 3042;Invalid year (too low)
@returns Success;int;2005;Count of batchforms read
@returns Data;complex based on ops18 definitions
@modification
Date          Name          Description
10/08/2020    VJ            Moved agent filter logic into sql
01-25-2022    Shubham       Task:89648 - Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"
*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{tt/periodmix.i &tableAlias="data"}

def temp-table ttpolicy
  field policyid        as int
  field liabilityAmount as dec
  index pu1 is primary unique policyid.
  
{lib/std-def.i}
{lib/callsp-defs.i}

def var sPeriodID as int no-undo.
def var ePeriodID as int no-undo.
def var pLiabRange as char no-undo.

def buffer xdata for data.
def buffer xbatchform for batchform.

def var tLiabilityAmount as deci.
def var tRangeRowid as rowid.
def var tCnt as int no-undo.


def var pStateID as char no-undo.
def var pAgentID as char no-undo.
def var pID as char no-undo.
def var pUID as character no-undo.

def var tReportStateID as char no-undo.
def var tReportStatCode as char no-undo.

/* local variables */

/* stored procedure variables */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 70.2.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 

pRequest:getParameter("sPeriodID", output sPeriodID).
if not can-find(first period 
                  where period.periodID = sPeriodID)
 then
  do: pResponse:fault("3066", "Starting period").
      return.
  end.

pRequest:getParameter("ePeriodID", output ePeriodID).
if not can-find(first period 
                  where period.periodID = ePeriodID)
 then
  do: pResponse:fault("3066", "Ending period").
      return.
  end.

pRequest:getParameter("ranges", output pLiabRange).
if pLiabRange > ""
 then
  do:
      std-lo = false.
      do std-in = 1 to num-entries(pLiabRange):
       if std-in > 32 
        then
         do: pResponse:fault("3039", "Liability Range Count").
             return.
         end.

       std-de = decimal(entry(std-in, pLiabRange)) no-error.
       if error-status:error or std-de <= 0 
        then std-lo = true. /* error */
      end.
      if std-lo 
       then
        do: pResponse:fault("3039", "Liability Range Value").
            return.
        end.
  end.
 
pRequest:getParameter("state", output pStateID).
pStateID = caps(pStateID).

if pStateID <> "ALL" 
and not can-find(first state 
                  where state.stateID = pStateID)
 then
  do: pResponse:fault("3066", "State").
      return.
  end.

pRequest:getParameter("agent", output pAgentID).

/* This is so I can deploy to live before the new client is built */
if pAgentID = "" or pAgentID = ? then
assign pAgentID = "ALL".

if pAgentID <> "ALL"
and not can-find(first agent 
                  where agent.agentID = pAgentID)
 then
  do: pResponse:fault("3066", "Agent").
      return.
  end.
  
if pResponse:isFault()
 then return.

pUID  = pRequest:uid.
{lib\callsp.i 
    &name=spReportDataCall 
    &load-into=data 
    &params="input sPeriodID, input ePeriodID, input pStateID, input pAgentID, input pLiabRange, input pUID"
	&setparam="'Data'"}
    
pResponse:success("2005", string(csp-icount) {&msg-add} "Batch Form").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

