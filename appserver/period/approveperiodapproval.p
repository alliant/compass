&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file sendactionapproval.p
@action actionApprovalSend
@description Sends the actions for approval

@param action;complex;The action

@author John Oliver
@created 01.15.2019
@notes
------------------------------------------------------------------------*/

define input param pRequest as service.IRequest.
define input param pResponse as service.iResponse.

/* parameters */
define variable pPeriodID as integer   no-undo.
define variable pEntity   as character no-undo.
define variable pApprove  as logical   no-undo.
define variable pComment  as character no-undo.
define variable pReturn   as character no-undo.

/* variables */
{lib/std-def.i}

/* functions */
{lib/encrypt.i}

/* temp table */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.38
         WIDTH              = 65.4.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

pRequest:getParameter("PeriodID", output pPeriodID).
pRequest:getParameter("Entity", output pEntity).
if not can-find(first periodapproval where periodID = pPeriodID and entity = pEntity)
 then pResponse:fault("3066", "Period " + string(pPeriodID) + " for Entity " + pEntity).
 
if pResponse:isFault()
 then return.
 
for first periodapproval no-lock
    where periodapproval.periodID = pPeriodID
      and periodapproval.entity = pEntity:
      
  if periodapproval.stat <> "S"
   then pResponse:fault("3005", "Actions have already been approved/denied").
end.
 
if pResponse:isFault()
 then return.
 
pRequest:getParameter("Approve", output pApprove).
pRequest:getParameter("Comment", output pComment).
 
TRX-BLOCK:
for first periodapproval exclusive-lock
    where periodapproval.periodID = pPeriodID
      and periodapproval.entity = pEntity TRANSACTION
       on error undo TRX-BLOCK, leave TRX-BLOCK:
       
  if periodapproval.uid <> pRequest:uid
   then pComment = (if pApprove then "Approved" else "Denied") + " by " + pRequest:uid + (if pComment > "" then ": " else "") + pComment.
       
  assign
    periodapproval.stat          = (if pApprove then "A" else "D")
    periodapproval.completedDate = now
    periodapproval.comment       = pComment
    pReturn                      = (if pApprove then "Actions were approved" else "Actions were denied")
    .
end.

pResponse:success("2019", pReturn).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


