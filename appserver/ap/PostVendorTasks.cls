/*------------------------------------------------------------------------
@name  PostVendorTasks
@action vendorTasksPost
@description  Posting Taksks
@author K.R
@Modified :
Date        Name          Comments   
11/15/23   S Chandu      changed action name "voucherRender" to "fileVoucherRender".  
02/26/24   K.R           Modified to remove code which sets completeddate in filetask table
06/25/24   S Chandu      Task: 113230 Modified to send end date to rendering action.
07/02/24   S Chandu      Modified to add validations completed through and task completetion dates.
07/29/2024 S B           Modified to change arctaskpost as Sync process instead of queued process
08/02/2024 K.R           Modififed to send payload and mimetype while queueing the action
08/05/2024 S Chandu      Modified validation logic on invoice date cannot be prior to CTD.
11/20/2024 K.R           Modified to add notify requestor
------------------------------------------------------------------------*/

class ap.PostVendorTasks inherits framework.ActionBase:

  {lib/nextkey-def.i}
  {lib/callsp-defs.i}

  /* temp-tables */
  {tt/filetask.i &tableAlias=ttfiletask}
  {tt/filetask.i &tableAlias=ttarcfiletask}
  {tt/consolidatefileTask.i}
  {tt/proerr.i    &tableAlias="ttTaskPostErr"}
  {tt/apvendor.i &tableAlias="gpvendordetails" &posttasks}

  define variable iErrorSeq as integer no-undo.
    
  constructor PostVendorTasks ():
    super().
  end constructor.

  destructor public PostVendorTasks ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     
    {lib/std-def.i}

    define variable iApInvID          as integer   no-undo.
    define variable iApTrxID          as integer   no-undo.
    define variable iPeriod           as integer   no-undo.
    define variable pInvoiceDate      as date      no-undo.
    define variable pEndDate          as date      no-undo.
    define variable dsHandle          as handle    no-undo.
    define variable dsHandle2         as handle    no-undo.
    define variable lError            as logical   no-undo.
    define variable cErrMessage       as character no-undo.
    define variable lSuccess          as logical   no-undo.
    define variable lValidated        as logical   no-undo initial true.
    define variable cArcTaskIdList    as longchar  no-undo.
    define variable cApInvIdList      as longchar  no-undo.
    define variable iCurrentInvID     as integer   no-undo.
    define variable cUsername         as character no-undo.
    define variable cParameterList    as longchar  no-undo.
    define variable cEntityType       as character no-undo initial "V".
    define variable cDestinationList  as longchar  no-undo.
    define variable cDestination      as character no-undo.
    define variable cVendorErrList    as character no-undo.
    define variable iCOunter          as integer   no-undo.
    define variable iCounter2         as integer   no-undo.
    define variable iApVoucherID      as integer   no-undo.
    define variable cRefIdList        as character no-undo.

    define variable pcStatus          as character no-undo.
    define variable pcStatusMessage   as character no-undo.
    define variable pcFailedIdList    as character no-undo.         

    empty temp-table ttfiletask.
    empty temp-table ttarcfiletask.
    empty temp-table consolidatefileTask.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttarcfiletask:handle). 

    create dataset dsHandle2.
    dsHandle2:serialize-name = 'data'.
    dsHandle2:set-buffers(buffer ttfiletask:handle,
                          buffer consolidatefileTask:handle
                          ).

    /*dataset get*/
    pRequest:getContent(input-output dataset-handle dsHandle2, output std-ch).
    if std-ch > ''
     then 
      do:
        lValidated = false.
        createErrorRecord ('Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.
    pRequest:getParameter("invoiceDate", output pInvoiceDate).                   /* only one date completedthroughdate*/
    pRequest:getParameter("endDate", output pEndDate).  /* if date = ? or '' then today*/

    /* When filetaskposting is locked by any user */
    {lib/islocked.i &obj='FileTaskAPPosting' &id='' &exclude-fault=true}
    if std-lo and (std-ch <> pRequest:Uid)
     then
      do:
        {lib/getusername.i &var=cUsername &user=std-ch}  
        /* if file is locked by another user,cannot unlock it, return fault */
        lValidated = false.
        createErrorRecord ("AP Posting is locked by " + cUsername).
      end.

    /* Validating pInvoiceDate for future date*/
    if pInvoiceDate > today 
     then
      do:
        lValidated = false.
        createErrorRecord ("Invoice Date cannot be in the future").
      end.

    /* Validate active acounting period */
    find first Period no-lock where Period.periodmonth = month(pInvoiceDate) and period.periodyear = year(pInvoiceDate) no-error.
    if not available Period or (Period.activeAccounting = false or Period.activeAccounting = ?)
     then
      do:
        lValidated = false.
        createErrorRecord ("Acounting period not active for Invoice Date").
      end.

    /* validate Completed Through date  */
    if pEndDate >= date(today) 
     then
      do:
        lValidated = false.
        createErrorRecord ("Completed Through date must be less than today").
      end.

    /* validate invoice date cannot be prior to completed date */
    if pInvoiceDate < pEndDate
     then
      do:
        lValidated = false.
        createErrorRecord ("Invoice Date cannot be prior to the completed through date"). 
      end.

    /* Validate filetask should be completed and not posted */

    run file/getclosedtasks-p.p(input "",            /* UserName */
                                input "",            /* AssignedTo */
                                input "Closed",     /* Status = complete */
                                input "",            /* Queue */
                                input "",           /* Task */
                                input '',           /* EndDateRangeStart */
                                input pEnddate,     /* EndDateRangeEnd */
                                input false,        /* posted flag*/
                                output dataset-handle dsHandle,
                                output lError,
                                output cErrMessage,
                                output std-in).
    if lError 
     then
      do:
        lValidated = false.
        createErrorRecord ("Posting Failed: " + cErrMessage).
      end.

    for each ttfiletask:

      for first filetask fields (filetaskID posted taskID) 
          where filetask.filetaskID = ttfiletask.fileTaskID:

      /* validate from arc */
      if not can-find(first ttarcfiletask where ttarcfiletask.ID = filetask.taskID)
       then
        do:
          lValidated = false.
          createErrorRecord ("Task with ID " + string(ttfiletask.filetaskID) + " is not completed in ARC").
        end.
         /* validate the posted flag*/
        if filetask.posted = true 
         then
          do:
            lValidated = false.
            createErrorRecord ("Task is already posted for " + string(filetask.filetaskID)).
         end. 

         /* Validate amount on file task cost is 0 for file ID - filetaskID*/
         if filetask.cost = 0 
          then
           do:
             lValidated = false.
             createErrorRecord ("Posting amount cannot be 0 for Task: " + string(filetask.filetaskID)).
           end.
         if ttfiletask.cost <> filetask.cost 
          then
           do:
             lValidated = false.
             createErrorRecord ("File cost doesnot match with the value in database, please refresh the records").
           end.

          /* validate filetask completed date */
         if pInvoiceDate < date(filetask.completeddate)
          then
           do:
             lValidated = false.
             createErrorRecord ("Invoice Date cannot be prior to the Task Completion Date for Task: " + string(filetask.filetaskID) ).
           end.
     
      end.

      /* validate topicexpense GL account Client send account same or not */
      if not can-find(first topicexpense where topicexpense.stateID = ttfiletask.stateID and topicexpense.topic = ttfiletask.topic and topicexpense.GLAccount = ttfiletask.glaccount) 
       then
        do:
          lValidated = false.
          createErrorRecord (substitute("Invalid GL Account code: &1 for State: &2 ,Task: &3", ttfiletask.glaccount,ttfiletask.stateID,ttfiletask.topic)).   
        end.

    end.

    for each consolidatefileTask :

      if not can-find(first PM00200 where PM00200.VENDORID =  consolidatefileTask.vendor)
       then
        do:
          lValidated = false.
          createErrorRecord(substitute("Vendor ID &1 doesn't exist in GP", consolidatefileTask.vendor)).
        end.
      if not can-find(first GL00105 where GL00105.ACTNUMST = consolidatefileTask.glaccount)
       then
        do:
          lValidated = false.
          createErrorRecord(substitute("GL Account code &1 doesn't exist in GP", consolidatefileTask.glaccount)).
        end.

      /* consolidated table record amount should not be zero */
      if consolidatefileTask.costsum = 0
       then
        do:
          lValidated = false.
          createErrorRecord (substitute("Posting amount cannot be 0 for state &1, Vendor &2 and Topic &3",
                                            consolidatefileTask.stateID, consolidatefileTask.vendor, consolidatefileTask.topic)).
        end.

      for each ttfiletask where ttfiletask.stateID   = consolidatefiletask.stateID and
                                ttfiletask.vendorID  = consolidatefiletask.vendor  and
                                ttfiletask.topic     = consolidatefiletask.topic   and
                                ttfiletask.glaccount = consolidatefiletask.glaccount:
        accumulate ttfiletask.cost (total).
      end.
      if (accum total ttfiletask.cost) <> consolidatefileTask.costsum 
       then
        do:
           lValidated = false. 
           createErrorRecord ( substitute("Posting amount does not match the total costs of tasks for state &1, vendor &2 and Topic &3", 
                                              consolidatefileTask.stateID, consolidatefileTask.vendor, consolidatefileTask.topic)). 
        end.

       /* Consolidated table, validate glaccount should exist in topic expense table */

      if consolidatefileTask.glaccount = '' or consolidatefileTask.glaccount = ?
       then
        do:
          find first topicexpense where topicexpense.stateid = consolidatefileTask.stateID and topicexpense.topic = consolidatefileTask.topic no-lock no-error.
          if not available topicexpense or (topicexpense.glaccount = '' or topicexpense.glaccount = ?)
           then
            do:
              lValidated = false. 
              createErrorRecord (substitute("GL Account for state &1, Topic &2 is not configured", consolidatefileTask.stateID,consolidatefileTask.topic)). 
            end.
            else
              consolidatefileTask.glaccount = topicexpense.glaccount.
        end.           
    end.

    if not lValidated 
     then
      do:
        setContentTable("FileTaskPostErr",input table ttTaskPostErr, input pRequest:FieldList).
        pResponse:success("3005","Validations failed, check the error file generated for more details").
        return.
      end.

    {lib\callsp.i &name=spGetVendorDetails &load-into=gpvendordetails &noResponse=true}

    lsuccess = false.
    TRX-BLOCK:
    for each consolidatefileTask transaction
        on error undo TRX-BLOCK, next TRX-BLOCK:
       /*  undo and next transaction*/

      {lib/nextkey.i &type='apInvoice' &var=iApInvID &err="undo TRX-BLOCK, next TRX-BLOCK."}
       if can-find(first apinv where apinv.apinvID = iApInvID) 
        then
         do:
           run util/sysmail.p(
                              input "Sequence Failed",
                              input "Not able to fetch unique ID for apInv table"
                             ).
           leave TRX-BLOCK.
         end.

      {lib/nextkey.i &type='apTransaction' &var=iApTrxID &err="undo TRX-BLOCK, next TRX-BLOCK."}
       if can-find(first apTrx where apTrx.aptrxID = iApTrxID) 
        then
         do:
           run util/sysmail.p(
                              input "Sequence Failed",
                              input "Not able to fetch unique ID for apTrx table"
                             ).
           leave TRX-BLOCK.
         end.

      /* creating apinv record */
      create apinv.
      assign
        apinv.apinvID         = iApInvID
        iCurrentInvID         = iApInvID
        apinv.refType         = "T"
        apinv.refID           = '' 
        apinv.refSeq          = 0
        apinv.refCategory     = ''  
        apinv.invoiceNumber   = string(iApInvID)
        apinv.invoiceDate     = pInvoiceDate
        apinv.dateReceived    = today
        apinv.amount          = consolidatefileTask.costsum
        apinv.dueDate         = add-interval(pInvoiceDate, 30, "days")
        apinv.deptID          = ''
        apinv.notes           = "AP posted by " + pRequest:uid + " on " + string(today) 
        apinv.stat            = "C"
        apinv.PONumber        = ''
        apinv.uid             = pRequest:uid
        apinv.contention      = false
        apinv.reduceLiability = false
        .

      for first gpvendordetails where gpvendordetails.vendorid = consolidatefileTask.vendor no-lock:
        assign
          apinv.vendorid      = gpvendordetails.vendorid
          apinv.vendorname    = gpvendordetails.vendorname
          apinv.vendoraddress = if gpvendordetails.vendoraddress = ? then '' else gpvendordetails.vendoraddress
          .
      end.

      /* creating apinvd record */
      create apinvd.  
      assign
        apinvd.apinvID  = apinv.apinvID
        apinvd.seq      = 1
        apinvd.acct     = consolidatefileTask.glaccount
        apinvd.acctName = consolidatefileTask.glaccountdesc
        apinvd.amount   = consolidatefileTask.costsum
        .

      /* Creating ApTrx record */

      create aptrx.
      assign
        aptrx.aptrxID      = iApTrxID
        aptrx.transType    = "C"  /* completed */
        aptrx.transAmount  = consolidatefileTask.costsum
        aptrx.transDate    = datetime(date(pInvoiceDate),mtime(datetime("01-01-2000 23:59:00")))
        aptrx.apinvID      = apinv.apinvID
        aptrx.refType      = apinv.refType
        aptrx.refID        = apinv.refID
        aptrx.refSeq       = apinv.refSeq
        aptrx.refCategory  = apinv.refCategory
        aptrx.uid          = pRequest:uid
        aptrx.notes        = "AP posted by " + pRequest:uid + " on " + string(today)
        aptrx.report       = true
        aptrx.completeDate = now
        .

      /* updating filetask with apinvID */   
      for each ttfiletask no-lock where ttfiletask.stateID   = consolidatefiletask.stateID and
                                        ttfiletask.vendorID  = consolidatefiletask.vendor  and
                                        ttfiletask.topic     = consolidatefiletask.topic   and
                                        ttfiletask.glaccount = consolidatefiletask.glaccount:

        for first filetask exclusive-lock where filetask.filetaskID = ttfiletask.filetaskID:
          assign
            filetask.apinvID = apinv.apinvID
            filetask.posted = true.
          for first ttarcfiletask where ttarcfiletask.id = filetask.taskid no-lock:

            assign
                filetask.assigneddate  = datetime-tz(ttarcfiletask.startdatetime,0)
                filetask.assignedto    = ttarcfiletask.AssignedTo
                .
          end.
        end.
      end.

      validate apinv.
      release apinv.
      validate aptrx.
      release aptrx.
      validate apinvd.
      release apinvd.
      assign
        lSuccess = true
        consolidatefiletask.posted = true
        consolidatefiletask.apInvId = iCurrentInvID
        cApInvIdList = cApInvIdList + string(iCurrentInvID) + ","
        iCurrentInvID = 0
        .
    end.

    if lSuccess 
     then
      do:
        for each consolidatefiletask where consolidatefiletask.posted = true: 
            for each ttfiletask where ttfiletask.stateID   = consolidatefiletask.stateID and
                                      ttfiletask.vendorID  = consolidatefiletask.vendor  and
                                      ttfiletask.topic     = consolidatefiletask.topic   and
                                      ttfiletask.glaccount = consolidatefiletask.glaccount:


               for first ttarcfiletask where ttarcfiletask.filetaskID = ttfiletask.filetaskID:
                 cArcTaskIdList = cArcTaskIdList + string(ttarcfiletask.ID) + ",". 
               end.
            end.
        end.

        /*All the errors are handled in postarctask-p.p program*/
        run ap/postarctask-p.p(input trim(cArcTaskIdList, ","), output pcStatus, output pcStatusMessage, output pcFailedIdList).

        /* Error handling is done inside procedure itself */
        run util/queueaction-p.p("taskVouchersImport",pRequest:uid, pRequest:GetNotifyRequestor (), "ApInvIdList=" + trim(cApInvIdList, ","), "", "APM", "", "").

        assign
          cApInvIdList     = ""
          cParameterList   = ""
          cDestinationList = ""
          .
        DEST-BLK:
        for each consolidatefiletask where consolidatefiletask.posted = true break by consolidatefiletask.vendor:
          if first-of(consolidatefiletask.vendor)
           then
            do:
              assign
                cDestination = GetSysDestintion("V", consolidatefiletask.vendor, "fileVoucherRender")
                cVendorErrList = if cDestination = "" then cVendorErrList + consolidatefiletask.vendor + "," else ""                 
                cApInvIdList = ""
                .
              if cDestination = ""
               then next DEST-BLK.
            end.


          cApInvIdList = cApInvIdList + string(consolidatefiletask.apinvid) + ",".

          if last-of(consolidatefiletask.vendor)
           then 
            assign
              cDestinationList = cDestinationList + cDestination + "|"
              cParameterList = cParameterList + trim(cApInvIdList, ",") + "|".

        end.

        assign
          cDestinationList = trim(cDestinationList,"|")
          cParameterList   = trim(cParameterList, "|")
          .

        /*
        transaction block
        loop through for each entry in the cParameterList
        get-nextket for apVoucher
        
        loop through all the apinvids in the nth entry of cParameterList
        find the apinv record in exclusive lock and set the refid = apvouherID
        
        */

        if num-entries(cVendorErrList) > 0 
         then run util/sysmail.p(
                   input "Render voucher failed",
                   input "Render Voucher destination is not set up for vendors:" + cVendorErrList
                  ).

        assign
          cApInvIdList       = ""
          error-status:error = false
          .

        TRX-BLOCK2:
        do iCounter = 1 to num-entries(cParameterList, "|") transaction on error undo TRX-BLOCK2, next TRX-BLOCK2:

          cApInvIdList = entry(iCounter, cParameterList,"|").

          {lib/nextkey.i &type='apVoucher' &var=iApVoucherID &err="undo TRX-BLOCK2, next TRX-BLOCK2."}
          if can-find(first apinv where apinv.refType = 'T' and apinv.refID = string(iApVoucherID))
           then
            do:
              run util/sysmail.p(
                   input "Sequence Failed",
                   input "Not able to fetch unique ID for Voucher"
                  ).
              leave TRX-BLOCK2.
            end.

          do iCounter2 = 1 to num-entries(cApInvIdList):

            iApinvID = integer(entry(iCounter2,cApInvIdList)).

            for first apinv exclusive-lock where apinv.apinvID = iApinvID: 
              apinv.refID = string(iApVoucherID).
            end.
          end.
          cRefIdList = cRefIdList + "RefID=" + string(iApVoucherID) + "|".
        end.
        if error-status:error
         then run util/sysmail.p(
                   input "Render voucher failed",
                   input "Rendering Voucher failed for " + cParameterList
                   ).


        /* Error handling is done inside procedure itself*/
        run util/queueaction-p.p("fileVoucherRender",pRequest:uid, pRequest:GetNotifyRequestor (), trim(cRefIdList, "|") + "^EndDate=" + string(pEndDate) , cDestinationList, "APM", "", "").
        pResponse:success("2010", "File Vouchers").
      end.
  end method.

  method private void createErrorRecord (input pcErrorMsg as character):
    create ttTaskPostErr.
    assign 
      iErrorSeq                 = iErrorSeq + 1
      ttTaskPostErr.err         = string(iErrorSeq)
      ttTaskPostErr.description = pcErrorMsg
      .
  end method.

  method private character GetSysDestintion (input pcEntityType as character,
                                     input pcEntityID   as character,
                                     input pcAction     as character):

    define variable cAllDestination as character no-undo.

    if not can-find(first sysdest where sysdest.entityType = pcEntityType
                                    and sysdest.entityID   = pcEntityID        
                                    and sysdest.action     = pcAction) 
     then
       for each sysdest no-lock
         where sysdest.entityType = pcEntityType
           and sysdest.entityID   = ""
           and sysdest.action     = pcAction:
         cAllDestination = cAllDestination + sysdest.destType + "=" + sysdest.destName + "^".
       end.
     else
       for each sysdest no-lock
         where sysdest.entityType = pcEntityType
           and sysdest.entityID    = pcEntityID
           and sysdest.action     = pcAction:

         cAllDestination = cAllDestination + sysdest.destType + "=" + sysdest.destName + "^".

       end.
    return trim(cAllDestination,"^").
  end method.
      
end class.



