/*------------------------------------------------------------------------
@file getapinvoicesbystatus.p
@action apInvoicesGetByStatus
@description Get one or multiple record(s) in the Invoice table by status

@param ID;int;The row identifier

@returns The table dataset

@throws 3066;The row does not exist in the table

@success 2005;Invoice returned X rows

@author John Oliver
@version 1.0
@created XX/XX/20XX

Modified
Date           Name            Description
01-25-2022     Shubham         Task:89648 - Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}
{tt/apminvoice.i}

{lib/callsp-defs.i}

/* parameter variables */
define variable pStatus as character no-undo.
define variable pVendorName as character no-undo.
define variable pStartDate as datetime no-undo.
define variable pEndDate as datetime no-undo.
define variable pType as character no-undo.

/* the parameters */
pRequest:getParameter("Status", OUTPUT pStatus).
pRequest:getParameter("Vendor", OUTPUT pVendorName).
pRequest:getParameter("StartDate", OUTPUT pStartDate).
pRequest:getParameter("EndDate", OUTPUT pEndDate).
pRequest:getParameter("Type", OUTPUT pType).

/* if there is a fault thrown within the procedure, exit */
if pResponse:isFault()
 then return.
 
if pType = ""
 then pType = "ALL".

{lib\callsp.i 
    &name=spGetInvoicesByStatus 
    &load-into=apminvoice 
    &params="input pStatus, input pVendorName, input pStartDate, input pEndDate, input pType"  
	&setparam="'Invoice'"}

pResponse:success("2005", STRING(csp-icount) {&msg-add} "Invoice").
