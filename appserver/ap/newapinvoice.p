/*------------------------------------------------------------------------
@file  newapinvoice.p
@action apInvoiceNew
@description Create a new record in the Invoice table

@param RefType;char
@param RefCategory;char
@param VendorID;char
@param InvoiceNumber;char
@param InvoiceDate;datetime
@param Amount;decimal
@param DueDate;datetime
@param DeptID;char
@param Notes;char
@param DateReceived;datetime
@param PONumber;char

@throws 3000;The create failed
@throws 3007;Column(s) are not found in the table
@throws 3054;Amount cannot be negative
@throws 3066;The table does not exist
@throws 3067;Row not found in table with criteria provided

@success 2002;The record was added

@author John Oliver
@version 1.0
@created XX/XX/20XX
@notes
Date          Name           Description
03/03/2022     SD            Task 89909 - Modified nextkey.i
----------------------------------------------------------------------*/

define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

{lib/std-def.i}
{lib/nextkey-def.i}
{lib/get-reserve-type.i}

/* local variables */
define variable iSeq as int no-undo.

/* parameter variables */
define variable pRefType as character no-undo.
define variable pRefID as character no-undo.
define variable pRefCategory as character no-undo.
define variable pVendorID as character no-undo.
define variable pVendorName as character no-undo.
define variable pVendorAddress as character no-undo.
define variable pInvoiceNumber as character no-undo.
define variable pInvoiceDate as datetime no-undo.
define variable pAmount as decimal no-undo.
define variable pDueDate as datetime no-undo.
define variable pDeptID as character no-undo.
define variable pNotes as character no-undo.
define variable pPONumber as character no-undo.
define variable pDateReceived as datetime no-undo.
define variable pContention as logical no-undo.

/* the parameters */
pRequest:getParameter("refType", output pRefType).
pRequest:getParameter("refID", output pRefID).
pRequest:getParameter("refCategory", output pRefCategory).
pRequest:getParameter("vendorID", output pVendorID).
pRequest:getParameter("vendorName", output pVendorName).
pRequest:getParameter("vendorAddress", output pVendorAddress).
pRequest:getParameter("invoiceNumber", output pInvoiceNumber).
pRequest:getParameter("invoiceDate", output pInvoiceDate).
pRequest:getParameter("amount", output pAmount).
pRequest:getParameter("dueDate", output pDueDate).
pRequest:getParameter("deptID", output pDeptID).
pRequest:getParameter("notes", output pNotes).
pRequest:getParameter("poNumber", output pPONumber).
pRequest:getParameter("dateReceived", output pDateReceived).
pRequest:getParameter("contention", output pContention).
 
/* validate that the vendor is valid */
if pVendorID > "" and not can-find (first PM00200 where PM00200.VENDORID = pVendorID)
 then pResponse:fault("3001", "Vendor ID").
 
/* if claim and Loss, make sure the claim type is not a Matter */
if pRefType = "C" and pRefCategory = "L" and can-find(first claim where claimID = integer(pRefID) and type <> "C")
 then pResponse:fault("3055", "Claim" {&msg-add} "Notice or Matter" {&msg-add} getReserveDesc(pRefCategory)).
 
/* if there is a fault thrown within the procedure, exit */
if pResponse:isFault()
 then return.
 
/* all parameters are valid */
std-lo = false.
TRX-BLOCK:
do TRANSACTION
  on error undo TRX-BLOCK, leave TRX-BLOCK:

  {lib/nextkey.i &type='apInvoice' &var=iSeq &err="undo TRX-BLOCK, leave TRX-BLOCK."}
 
  if can-find(first apinv where apinv.apinvID = iSeq)
   then
    do: pResponse:fault("3011", "Invoice" {&msg-add} "ID" {&msg-add} string(iSeq)).
        leave TRX-BLOCK.
    end.

  create apinv.
  assign
    apinv.apinvID = iSeq
    apinv.refType = ""
    apinv.refID = ""
    apinv.refSeq = 0
    apinv.refCategory = upper(pRefCategory)
    apinv.vendorID = pVendorID
    apinv.vendorName = pVendorName
    apinv.vendorAddress = pVendorAddress
    apinv.invoiceNumber = pInvoiceNumber
    apinv.invoiceDate = pInvoiceDate
    apinv.dateReceived = pDateReceived
    apinv.amount = pAmount
    apinv.dueDate = pDueDate
    apinv.deptID = pDeptID
    apinv.notes = pNotes
    apinv.stat = "O"
    apinv.PONumber = pPONumber
    apinv.uid = pRequest:uid
    apinv.contention = pContention
    apinv.reduceLiability = false
    .
  validate apinv.
  if pRefType > ""
   then 
    do:
      pRequest:setParameter("ID", iSeq).
      run ap/allocateapinvoice.p (pRequest, pResponse).
      
      if pResponse:isFault()
       then
        do: delete apinv.
            leave TRX-BLOCK.
        end.
    end.
  
  std-lo = true.
end.

if pResponse:isFault()
 then
  return.
  
if not std-lo
 then
  do: pResponse:fault("3000", "Create").
      return.
  end.

pResponse:setParameter("apinvID", iSeq).
pResponse:success("2002", "Invoice").
