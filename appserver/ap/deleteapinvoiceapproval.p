/*------------------------------------------------------------------------
@file deleteapinvoiceapproval.p
@action apInvoiceActionDelete
@description Deletes the invoice approval

@param ID;int;The row identifier
@param Seq;int;The sequence of the row

@throws 3000;The record was not found

@success 2008;The delete was successful

@author John Oliver
@version 1.0
@created XX/XX/20XX
@modified 02/2018 - YS - Activated log message
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}

/* parameter variables */
def var pID as int no-undo.
def var pSeq as int no-undo.

def var pRefType as char no-undo.
def var pRefNum as char no-undo.
def var pRefMessage as char no-undo.

/* the parameters */
pRequest:getParameter("ID", OUTPUT pID).
pRequest:getParameter("seq", OUTPUT pSeq).

find first apinv no-lock where apinv.apinvID = pID no-error.

assign
pRefType = (if avail apinv then apinv.refType else "Error:" + string(pID))
pRefNum = (if avail apinv then apinv.refID else "")
pRefMessage = (if avail apinv then "category: " + apinv.refCategory + " / " +
                                   "vendorid: " + apinv.vendorID + " / " +
                                   "vendor name: " + apinv.vendorName 
                                                       
                               else "")
.

pResponse:logMessage(pRefType, pRefNum, pRefMessage).

std-lo = false.
for each apinva exclusive-lock
   where apinva.apinvID = pID:
   
  if pSeq > 0 and apinva.seq <> pSeq
   then next.

  delete apinva.
  release apinva.
  
  std-lo = true.
end.

if not std-lo
 then
  do: pResponse:fault("3000", "Delete").
      return.
  end.

pResponse:success("2008", "Invoice Approval").
