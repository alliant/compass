/*------------------------------------------------------------------------
@file  resetapinvoiceapproval.p
@approval apinvaNew
@description Create a new record in the Invoice Approval table

@param ID;int;The row identifier
@param Role;char
@param Uid;char
@param Amount;decimal
@param Notes;char

@throws 3000;The create failed
@throws 3061;The amounts are greater than the invoice amount
@throws 3066;Record does not exist
@throws 3067;Record does not exist with ID

@success 2006;The update was successful

@author John Oliver
@version 1.0
@created XX/XX/20XX
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}
{tt/apinva.i &tableAlias="ttInvoice"}

/* the parameters */
def var pID as int no-undo.

/* local variables */
def var dTotal as decimal no-undo.
def var dSum as decimal no-undo.
DEF VAR tFound AS LOGICAL NO-UNDO.
DEF VAR tFieldChanged AS LOGICAL NO-UNDO.

/* the parameters */
pRequest:getParameter("ID", output pID).

/* validation */
/* make sure that the invoice exists */
if not can-find(first apinva where apinva.apinvID = pID)
 then pResponse:fault("3066", "Invoice").

/* if there is a fault thrown within the procedure, exit */
if pResponse:isFault()
 then return.
 
/* all parameters are valid */
ASSIGN
  tFound = false
  tFieldChanged = false
  .  
  
TRX-BLOCK:
for each apinva exclusive-lock
   where apinva.apinvID = pID TRANSACTION
      on error undo TRX-BLOCK, leave TRX-BLOCK:
    
  tFound = true.
  
  create ttInvoice.
  buffer-copy apinva to ttInvoice.

  assign
    apinva.stat = "P"
    apinva.dateActed = ?
    apinva.amount = 0.0
    apinva.notes = ""
    .
    
  validate apinva.
  buffer-compare apinva to ttInvoice case-sensitive save result in std-ch.
  if std-ch <> ""
   then tFieldChanged = true.
   
  release apinva.
end.

IF NOT tFound 
 THEN pResponse:fault("3004", "Invoice Approval").
 
if pResponse:isFault()
 then return.

IF NOT tFieldChanged 
 THEN pResponse:success("2018", "Invoice Approval"). /* Update succeeded, but no changes detected */
 ELSE 
  for first apinv exclusive-lock
      where apinv.apinvID = pID:
      
    apinv.stat = "O".
    pResponse:success("2006", "Invoice Approval").
  end.
