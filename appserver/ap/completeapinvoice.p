/*----------------------------------------------------------------------
@file completeapinvoice.p
@action apInvoiceComplete
@description Create a new record in the Transaction table

@param ID;int;The invoice ID
@param Amount;decimal
@param TransDate;char
@param Notes;char

@throws 3000;The create failed
@throws 3001;The date is invalid
@throws 3005;Not all approvers have approved the invoice
@throws 3006;The algorithm to get the next key failed
@throws 3011;The id already exists
@throws 3043;The id must be less than 999999
@throws 3066;The invoice is invalid

@returns The id of the transaction

@success 2002;The record was added

@author John Oliver
@version 1.0
@created 01/09/2017
@changelog
------------------------------------------------------------------------
Date        Author  Change
------------------------------------------------------------------------
2017-02-22  JO      Added fault to ensure posted month is the same as
                    the approval month      
2022-03-04  SR      Task 89909 - Modified nextkey.i
----------------------------------------------------------------------*/

define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

{lib/std-def.i}
{lib/nextkey-def.i}
{lib/account-functions.i}

/* parameter variables */
define variable pID as int no-undo.
define variable pAmount as decimal no-undo.
define variable pTransDate as datetime no-undo.
define variable pNotes as character no-undo.
define variable pReport as logical no-undo.

/* local variables */
define variable iSeq as int no-undo initial 0.
define variable cInvoiceNumber as character no-undo.
define variable cVendorID as character no-undo.

/* the parameters */
pRequest:getParameter("ID", output pID).
pRequest:getParameter("Amount", output pAmount).
pRequest:getParameter("TransDate", output pTransDate).
pRequest:getParameter("Notes", output pNotes).
pRequest:getParameter("Report", output pReport).

/* validation */ 
/* make sure invoice actually exists */
if not can-find(first apinv where apinvID = pID)
 then pResponse:fault("3066", "Invoice").
 
/* make sure the invoice is not complete, denied, and has a valid vendor id */
for first apinv no-lock
    where apinv.apinvID = pID
      and not pResponse:isFault():
   
  case apinv.stat:
   when "C" then pResponse:fault("3020", "Invoice" {&msg-add} "already completed").
   when "V" then pResponse:fault("3020", "Invoice" {&msg-add} "already voided").
  end case.
  
  cVendorID = apinv.vendorID.
  if cVendorID = ""
   then pResponse:fault("3051", "Vendor Identifier" {&msg-add} "Blank").
   else
    do:
      if not can-find(first PM00200 where PM00200.VENDORID = apinv.vendorID)
       then pResponse:fault("3007", "Vendor Number " + apinv.vendorID {&msg-add} "GP").
    end.
     
  if apinv.invoiceDate = ?
   then pResponse:fault("3001", "Invoice Date").
   
  cInvoiceNumber = apinv.invoiceNumber.
  if cInvoiceNumber = ""
   then pResponse:fault("3051", "Invoice Number" {&msg-add} "Blank").
end.

/* make sure that the invoice number doesn't repeat for any invoice */
if not pResponse:isFault() and can-find(first apinv where stat = "C" and vendorID = cVendorID and invoiceNumber = cInvoiceNumber and not apinvID = pID)
 then pResponse:fault("3056", "Invoice Number" {&msg-add} cInvoiceNumber {&msg-add} "Vendor ID " + cVendorID).

/* make sure the invoice is fully approved */
if not pResponse:isFault() and not HasAPInvoiceApprovedAll(pID)
 then pResponse:fault("3005", "Not all approvers have approved the invoice"). 
 
/* must be in an open period */
std-lo = false.
for first period no-lock
    where period.startDate <= pTransDate
      and period.endDate > pTransDate
      and period.activeAccounting = true:
      
  std-lo = true.
end.
if not pResponse:isFault() and not std-lo
 then pResponse:fault("3021","Period" {&msg-add} "Open").

/* make sure the date is valid */
date(pTransDate) no-error.
if error-status:error
 then pResponse:fault("3001", "Date").

/* if there is a fault thrown within the procedure, exit */
if pResponse:isFault()
 then return.
 
if pAmount = ?
 then pAmount = 0.
 
/* all parameters are valid */
std-lo = false.
TRX-BLOCK:
for first apinv exclusive-lock
    where apinv.apinvID = pID TRANSACTION
    on error undo TRX-BLOCK, leave TRX-BLOCK:

  {lib/nextkey.i &type='apTransaction' &var=iSeq &err="undo TRX-BLOCK, leave TRX-BLOCK."}
 
  if iSeq = 0
   then 
    do: pResponse:fault("3006", "Algorithm" {&msg-add} "Transaction ID").
        leave TRX-BLOCK. /* agentID key remains updated... */ 
    end.
  
  if can-find(first aptrx where aptrx.aptrxID = iSeq)
   then
    do: pResponse:fault("3011", "Transaction" {&msg-add} "ID" {&msg-add} string(iSeq)).
        leave TRX-BLOCK. /* agentID key remains updated... */ 
    end.
    
  create aptrx.
  assign
    aptrx.aptrxID = iSeq
    aptrx.transType = "C"
    aptrx.transAmount = pAmount
    aptrx.transDate = datetime(date(pTransDate),mtime(datetime("01-01-2000 23:59:00")))
    aptrx.apinvID = pID
    aptrx.refType = apinv.refType
    aptrx.refID = apinv.refID
    aptrx.refSeq = apinv.refSeq
    aptrx.refCategory = apinv.refCategory
    aptrx.uid = pRequest:uid
    aptrx.notes = pNotes
    aptrx.report = pReport
    aptrx.completeDate = now
    .
    
  validate aptrx.
  release aptrx.
  
  for each claim exclusive-lock
     where claim.claimID = integer(apinv.refID):
     
    assign 
      claim.lastActivity = now
      .
  end.
  
  apinv.stat = "C".
      
  std-lo = true.
end.

if pResponse:isFault()
 then
  return.

if not std-lo
 then pResponse:fault("3000", "Create").
 else
  do:
    pResponse:setParameter("TrxID", iSeq).
    run ap/importapvoucher.p (pRequest, pResponse).
  end.
  
/* if there is a fault thrown within the procedure, exit */
if pResponse:isFault()
 then return.

pResponse:success("2002", "Transaction").
