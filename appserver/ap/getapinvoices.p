/*------------------------------------------------------------------------
@file appserverTemplateGet
@action apinvGet
@description Get one or multiple record(s) in the Invoice table

@param ID;int;The row identifier

@returns The table dataset

@throws 3066;The row does not exist in the table

@success 2005;Invoice returned X rows

@author John Oliver
@version 1.0
@created XX/XX/20XX
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}
{tt/apinv.i &tableAlias="ttInvoice"}
{tt/apinva.i &tableAlias="ttApproval"}
{tt/apinvd.i &tableAlias="ttDistribution"}
{tt/aptrx.i &tableAlias="ttTransaction"}

/* These preprocessors are used inside xml/xmlparse.i but not needed in this program */
&scoped-define traceFile std-ch
&scoped-define xmlFile std-ch
{lib/xmlparse.i}

/* parameter variables */
def var pID as int no-undo.

/* the parameters */
pRequest:getParameter("ID", OUTPUT pID).

/* if there is a fault thrown within the procedure, exit */
if pResponse:isFault()
 then return.

std-in = 0.
for each apinv no-lock
   where apinv.apinvID = pID:
  
  create ttInvoice.
  buffer-copy apinv to ttInvoice.
  ttInvoice.notes = encodeXml(apinv.notes).
  
  /* get the username */
  {lib/getusername.i &var=ttInvoice.username &user=apinv.uid}
  
  ttInvoice.hasDocument = {lib/canfindinvoicedoc.i "Invoice-AP" apinv.apinvID}.
  if ttInvoice.hasDocument
   then
    for first sysdoc no-lock
        where sysdoc.entityType = "Invoice-AP"
          and sysdoc.entityID = string(apinv.apinvID):
          
      ttInvoice.documentFilename = sysdoc.objID.
      if sysdoc.objAttr > ""
       then ttInvoice.documentFilename = ttInvoice.documentFilename + "." + sysdoc.objAttr.
    end.
  
  /* get the approvals */
  for each apinva no-lock
     where apinva.apinvID = apinv.apinvID:

    create ttApproval.
    buffer-copy apinva to ttApproval.
    ttApproval.notes = encodeXml(apinva.notes).
    
    /* get the username */
    {lib/getusername.i &var=ttApproval.username &user=apinva.uid}
  end.
  
  /* get the distributions */
  for each apinvd no-lock
     where apinvd.apinvID = apinv.apinvID:

    create ttDistribution.
    buffer-copy apinvd to ttDistribution.
  end.
  
  /* get the transactions */
  for each aptrx no-lock
     where aptrx.apinvID = apinv.apinvID:

    create ttTransaction.
    buffer-copy aptrx to ttTransaction.
    ttTransaction.notes = encodeXml(aptrx.notes).
    
    /* get the username */
    {lib/getusername.i &var=ttTransaction.username &user=aptrx.uid}
  end.
  
  std-in = std-in + 1.
end.
   
IF std-in > 0
 THEN
  do:
    pResponse:setParameter("Invoice", table ttInvoice). 
    pResponse:setParameter("Approval", table ttApproval).
    pResponse:setParameter("Distribution", table ttDistribution).
    pResponse:setParameter("Transaction", table ttTransaction).
  end.

pResponse:success("2005", STRING(std-in) {&msg-add} "Invoice").
