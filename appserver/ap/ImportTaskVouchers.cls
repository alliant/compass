/*------------------------------------------------------------------------
@name ImporTaskVouchers.cls
@description Queued program to import task vouchers to GP
@param ApinvID;int;The Invoice ID 

@returns Error:json text with list apinvID and log path

@author K.R
----------------------------------------------------------------------*/
using Progress.Json.ObjectModel.JsonObject.
using progress.Json.ObjectModel.JsonArray.
class ap.ImportTaskVouchers inherits framework.ActionBase:

  {tt/proerr.i    &tableAlias="ttTaskImportErr"}
  constructor public ImportTaskVouchers ():
    super().
  end constructor.

  destructor public ImportTaskVouchers ( ):
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):


    define variable cInvoiceIDList as character  no-undo.
    define variable oErrJson       as JsonObject no-undo.
    define variable cErrText       as character  no-undo.
    define variable oErrArray      as JsonArray  no-undo.
    define variable oMainErrJson   as JsonObject no-undo.

    empty temp-table ttTaskImportErr.
    assign
      oErrArray    = new JsonArray()
      oMainErrJson = new JsonObject()
      .
    pRequest:getParameter("ApInvIdList",output cInvoiceIDList).
    run ap/importtaskvouchers-p.p(input cInvoiceIDList, output table ttTaskImportErr).
    if temp-table ttTaskImportErr:has-records
     then
      do:
        for each ttTaskImportErr:
          oErrJson = new JsonObject().
          oErrJson:add("ApinvID",ttTaskImportErr.err).
          oErrJson:add("LogFilePath", ttTaskImportErr.description).
          oErrArray:add(oErrJson).
        end.
        oMainErrJson:add("ErrorInfo",oErrArray).
        oMainErrJson:write(output cErrText).
        cErrText = 'Error importing apinvIDs ' + cErrText.
        pResponse:fault("3005",cErrText).
      end.
    if valid-object(oErrJson)
     then delete object oErrJson.
    if valid-object(oErrArray) 
     then delete object oErrArray.
    if valid-object(oMainErrJson)
     then delete object oMainErrJson.

  end method.
end class.
