&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*
@action GetPayables
@description Return the set of aggregate data about payables
@author D.Sinclair
@date 1.1.2017

@parameter module;ch;ModuleID associated with the vendor(s) (blank = all)
@parameter vendor;ch;StateID (blank = all)
@parameter assignedTo;ch;AssignedTo (blank = all)
@parameter cutoff;date;Invoice Date
@parameter stat;ch;Status (blank = all)

@throws 3066;Invalid Vendor
@throws 3066;Invalid Date

@returns Success;int;2005;Count of payables returned
@returns Data;complex based on clm01 definitions

Modified
Date           Name            Description
01-25-2022     Shubham         Task:89648 - Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"
*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}
{tt/clm04.i &tableAlias="data"}
{tt/apminvoice.i}
{lib/callsp-defs.i}

def var tCnt as int init 0 no-undo.

def var pModule as char no-undo.
def var pVendorID as char no-undo.
def var pAssignedTo as char no-undo.
def var pCutoff as datetime no-undo.
def var pStat as char no-undo.

define variable tApprovedDate as datetime no-undo.
define variable tApprovedBy as character no-undo.
define variable tApprovedAmount as decimal no-undo.
define variable tVendorName as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 70.2.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


pRequest:getParameter("module", output pModule).
if pModule = ? 
 then pModule = "".

pRequest:getParameter("vendor", output pVendorID).
if pVendorID = "" or pVendorID = ? 
 then pVendorID = "ALL".

pRequest:getParameter("stat", output pStat).
if pStat = ? or pStat = ""
 then pStat = "OAC".

pRequest:getParameter("assignedTo", output pAssignedTo).
if pAssignedTo = ? 
 then pAssignedTo = "".

pRequest:getParameter("cutoff", output pCutoff).
if pCutoff = ?
 then pCutoff = today.

{lib\callsp.i 
    &name=spGetInvoicesByStatus 
    &load-into=apminvoice 
    &params="input pStat, input '', input ?, input ?, input 'C'"
	&noCount=true}


std-in = 0.
for each apminvoice exclusive-lock:

  std-lo = false.
  if pVendorID <> "ALL" and trim(apminvoice.vendorID) <> pVendorID
   then std-lo = true.
   
  if pAssignedTo > "" and apminvoice.uid <> pAssignedTo
   then std-lo = true.
   
  if pCutoff <> ? and apminvoice.invoiceDate >= pCutoff
   then std-lo = true.
   
  /* John Oliver 05.10.2021 - Commenting out vendor filtering per Bryan Johnson's request. 
                              Refer to Claim Module Requests - 4.9.2021 Row 13 */
  /*if pModule > "" and not can-find(first sysprop where appCode = pModule and objAction = "Vendor" and objID = apminvoice.vendorID)
   then std-lo = true.*/
   
  if apminvoice.invoiceType = "R"
   then std-lo = true.
   
  if not std-lo
   then
    do:
      create data.
      assign
        data.apinvID = apminvoice.apinvID
        data.vendorID = trim(apminvoice.vendorID)
        data.vendorName = apminvoice.vendorName
        data.stat = apminvoice.stat
        data.invoiceNumber = apminvoice.invoiceNumber
        data.invoiceDate = apminvoice.invoiceDate
        data.invoiceAmount = apminvoice.amount
        data.approvedAmount = apminvoice.amount
        data.completedAmount = apminvoice.amount
        data.approvedDate = apminvoice.appDate
        data.uid = apminvoice.uid
        data.username = apminvoice.approval
        data.createDate = apminvoice.recDate
        data.completedDate = apminvoice.transDate
        data.refID = apminvoice.refID
        data.refCategory = apminvoice.refCategory
        std-in = std-in + 1
        .
    end.
end.

if std-in > 0 and temp-table data:has-records
 then pResponse:setParameter("Data", table data).
 
pResponse:success("2005", string(std-in) {&msg-add} "Payable").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


