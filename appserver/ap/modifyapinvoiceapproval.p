/*------------------------------------------------------------------------
@file  modifyapinvoiceapproval.p
@approval apinvaNew
@description Create a new record in the Invoice Approval table

@param ID;int;The row identifier
@param Role;char
@param Uid;char
@param Amount;decimal
@param Notes;char

@throws 3000;The create failed
@throws 3061;The amounts are greater than the invoice amount
@throws 3066;Record does not exist
@throws 3067;Record does not exist with ID

@success 2006;The update was successful

@author John Oliver
@version 1.0
@created XX/XX/20XX
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}
{tt/apinva.i &tableAlias="ttInvoice"}

/* the parameters */
def var pID as int no-undo.
def var pSeq as int no-undo.
def var pRole as char no-undo.
def var pUid as char no-undo.
def var pDateAction as datetime no-undo.
def var pAmount as decimal no-undo.
def var pStat as char no-undo.
def var pNotes as char no-undo.

/* local variables */
def var dTotal as decimal no-undo.
def var dSum as decimal no-undo.
DEF VAR tFound AS LOGICAL NO-UNDO.
DEF VAR tFieldChanged AS LOGICAL NO-UNDO.

/* the parameters */
pRequest:getParameter("ID", output pID).
pRequest:getParameter("seq", output pSeq).
pRequest:getParameter("role", output pRole).
pRequest:getParameter("uid", output pUid).
pRequest:getParameter("amount", output pAmount).
pRequest:getParameter("notes", output pNotes).

/* validation */
/* make sure that the invoice exists */
if not can-find(first apinv where apinv.apinvID = pID)
 then pResponse:fault("3066", "Invoice").
 
/* make sure an acted invoice record exists */
if not can-find(first apinva where apinva.apinvID = pID and apinva.seq = pSeq)
 then pResponse:fault("3067", "Invoice Approval" {&msg-add} "Sequence" {&msg-add} string(pSeq)).
 
/* make sure that the sum of all the act amounts are not greater than the invoice amount */
find first apinv where apinv.apinvID = pID.
if available apinv and pAmount > 0
 then 
  do:
    dTotal = apinv.amount.
    for each apinva where apinva.apinvID = pID and apinva.role <> pRole:
      dSum = dSum + apinva.amount.
    end.
    if dSum + pAmount > dTotal
     then pResponse:fault("3061","The previous acted amounts" {&msg-add} "the current acted amount" {&msg-add} "the invoice amount").
  end.
  
/* validate that the user is valid */
if not can-find(first sysuser where sysuser.uid = pUid)
 then pResponse:fault("3066", "User " + pUid).

/* if there is a fault thrown within the procedure, exit */
if pResponse:isFault()
 then return.

/* get the approver's role */
for first sysuser no-lock
    where sysuser.uid = pUid:
    
  pRole = sysuser.role.
end.
 
/* all parameters are valid */
ASSIGN
  tFound = false
  tFieldChanged = false
  .  
  
TRX-BLOCK:
for first apinva exclusive-lock
    where apinva.apinvID = pID
      and apinva.seq = pSeq TRANSACTION
       on error undo TRX-BLOCK, leave TRX-BLOCK:
    
  tFound = true.
  
  create ttInvoice.
  buffer-copy apinva to ttInvoice.

  assign
    apinva.role = pRole
    apinva.uid = pUid
		apinva.amount = pAmount
		apinva.notes = pNotes
    .
    
  validate apinva.
  buffer-compare apinva to ttInvoice case-sensitive save result in std-ch.
  if std-ch <> ""
   then tFieldChanged = true.
   
  release apinva.
      
  std-lo = true.
end.

IF NOT tFound 
 THEN pResponse:fault("3004", "Invoice Approval").
 
if pResponse:isFault()
 then return.

IF NOT tFieldChanged 
THEN pResponse:success("2018", "Invoice Approval"). /* Update succeeded, but no changes detected */
ELSE pResponse:success("2006", "Invoice Approval").

