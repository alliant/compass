/*------------------------------------------------------------------------
@file  newapinvoicedistribution.p
@action apInvoidDistributionNew
@description Create a new record in the Invoice Distribution table

@param ID;int;The row identifier
@param Acct;char
@param Amount;decimal

@throws 3000;The create failed
@throws 3011;A record was already found matching the criteria
@throws 3061;The amounts are greater than the invoice amount
@throws 3066;Record does not exist

@success 2002;The record was added

@author John Oliver
@version 1.0
@created XX/XX/20XX
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}

/* the parameters */
def var pID as int no-undo.
def var pAcct as char no-undo.
def var pAmount as decimal no-undo.

/* local variables */
def var iSeq as integer no-undo.
def var dTotal as decimal no-undo.
def var dSum as decimal no-undo.
def var cAcctName as char no-undo.

/* the parameters */
pRequest:getParameter("ID", output pID).
pRequest:getParameter("acct", output pAcct).
pRequest:getParameter("amount", output pAmount).

/* validation */
/* make sure that the invoice exists */
find first apinv where apinv.apinvID = pID no-error.
if not available apinv
 then pResponse:fault("3066", "Invoice").
 else dTotal = apinv.amount.
 
if pAcct = ? or pAcct = ""
 then pResponse:fault("3066", "Account Number").
 
/* make sure there is an amount */
if pAmount = ?
 then pResponse:fault("3066", "Amount").
 
/* if there is a fault thrown within the procedure, exit */
if pResponse:isFault()
 then return.
 
/* validate that the account is valid in GP */
find first GL00105 where GL00105.ACTNUMST = pAcct no-error.
if not available GL00105
 then
  do:
    pResponse:fault("3007", "Account " + pAcct {&msg-add} "GP").
    next.
  end.

/* get the account name from GP */
find first GL00100 where GL00100.ACTINDX = GL00105.ACTINDX no-error.
if not available GL00100
 then cAcctName = "Unknown".
 else cAcctName = trim(GL00100.ACTDESCR).
   
for each apinvd
   where apinvd.apinvID = pID:
  dSum = dSum + apinvd.amount.
end.

/* the distribution amount must equal the invoice amount */
if dSum + pAmount <> dTotal
 then pResponse:fault("3041", "Distribution Amount Total" {&msg-add} "Invoice Amount").
 
/* if there is a fault thrown within the procedure, exit */
if pResponse:isFault()
 then return.
 
/* all parameters are valid */
std-lo = false.
TRX-BLOCK:
do TRANSACTION
  on error undo TRX-BLOCK, leave TRX-BLOCK:
  
  iSeq = 1.
  for first apinvd
      where apinvd.apinvID = pID
         by apinvd.seq desc:
    iSeq = apinvd.seq + 1.
  end.
  
  create apinvd.  
  assign
    apinvd.apinvID = pID
    apinvd.seq = iSeq
    apinvd.acct = pAcct
    apinvd.acctName = cAcctName
    apinvd.amount = pAmount
    .
  validate apinvd.
  release apinvd.
  
  std-lo = true.
end.

if not std-lo
 then
  do: pResponse:fault("3000", "Create").
      return.
  end.

pResponse:success("2002", "Invoice Distribution").
