/*------------------------------------------------------------------------
@name com/setattorneyorganization.p
@action 
@description Create new organization , organization role from attoney dialog

@throws 3005;No XML file attached

@author Shefali
@version 1.0
@created 10/22/2021
Modification:

Date          Name       Note
03/17/22      Shefali    Task# 86699 Modified to set attorney organization.
11/07/2022    SA         Task# 96812 - Modified for organization stat
------------- ---------- ---------------------------------------------------

---------------------------------------------------------------------------*/
/* Temp-table Definitions */
{tt/organization.i &tableAlias="torganization"}
{tt/orgrole.i &tableAlias="torgrole"}
{tt/attorney.i &tableAlias="ttAttorney"}

/* Standard lib files */
{lib/std-def.i}
{lib/nextkey-def.i}
{lib/com-def.i}

define input  parameter pRequest      as service.IRequest.
define input  parameter pResponse     as service.IResponse.
define input  parameter ipcAction     as character  no-undo.
define input  parameter cAttorneyID   as character  no-undo.
define input  parameter table for ttAttorney.
define output parameter opcorgID      as character  no-undo.

define variable corgID        as character no-undo.
define variable iorgKeyID     as integer   no-undo.   
define variable iorgroleID    as integer   no-undo.
define variable cQuery        as character no-undo.
define variable cMsgLog       as character no-undo.
define variable lIsActive     as logical   no-undo.      
define variable lError        as logical   no-undo.
define variable lSuccess      as logical   no-undo.
define variable lActive       as logical   no-undo.

std-lo = false.

TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:   
  for first ttAttorney:
    assign
        ttAttorney.attorneyID = cAttorneyID .
        lActive = if ttAttorney.stat eq {&ActiveStat} then true else false 
         .
    if ipcAction = "New" 
     then
      do:
        /* -99 is for New organization */
        if ttAttorney.orgID = "-99"  
         then
          do:
            run createorganization in this-procedure.
            if lError 
             then 
              leave TRX-BLOCK.
            if std-lo 
             then
              leave TRX-BLOCK.
          end. /* if ttAttorney.orgID = "-99" */
        else
         corgID = ttattorney.orgID .
        if not can-find(first orgrole where orgrole.source   = {&AttorneyFirm}
                                        and orgrole.sourceID = ttattorney.attorneyID) 
         then
        DO:
            run createorgrole in this-procedure.
        END.
      end. /* if ipcAction = "New" */   
    else if ipcAction = "Modify" 
     then
      do:
        if ttAttorney.orgID = "" or ttAttorney.orgID = ? 
         then
          run deleteorgrole in this-procedure.
        else if ttAttorney.orgID = "-99" 
         then
          do:
            run createorganization in this-procedure.
            if lError 
              then 
               leave TRX-BLOCK.
            if not can-find(first orgrole where orgrole.source   = {&AttorneyFirm}
                                            and orgrole.sourceID = ttattorney.attorneyID) 
            then
             run createorgrole in this-procedure.      
          end. /* else if ttAttorney.orgID = -99 */
        else
         do:
           run deleteorgrole in this-procedure.
           corgID = ttAttorney.orgID.
           run createorgrole in this-procedure.
         end. /* else */
      end. /* else if ipcAction = "Modify" */
    else
     run deleteorgrole in this-procedure.
  end. /* for first ttAttorney */
end. /* do transaction */ 

procedure createorganization:

 find first organization no-lock 
      where organization.addr1 = ttAttorney.addr1 
        and organization.addr2 = ttAttorney.addr2 
        and organization.city  = ttAttorney.city
        and organization.state = ttAttorney.state no-error .
 if not available organization
  then
   do:
                                                         
     {lib/nextkey.i &type='organization' &var=iorgKeyID}
 
     corgID = string(iorgKeyID).
     if corgID = "0" or corgID = ?  or corgID = ""
      then 
       do:
         pResponse:fault("3006", "Algorithm" {&msg-add} "organization ID").
         lError = true.
         return.
       end. /* if corgID = 0 or corgID = ? or corgID = "" */
 
     if can-find(first organization where organization.orgID = corgID)
      then
       do:
         pResponse:fault("3011", "organization" {&msg-add} "ID" {&msg-add} corgID).
         lError = true.
         return.
       end. /* if can-find(FIRST organization */
 
     create torganization.
     assign 
           torganization.orgID   = corgID
           torganization.name    = ttAttorney.firmName
           torganization.addr1   = ttAttorney.addr1
           torganization.addr2   = ttAttorney.addr2
           torganization.city    = ttAttorney.city
           torganization.state   = ttAttorney.state
           torganization.zip     = ttAttorney.zip
           torganization.phone   = ttAttorney.phone         
           torganization.mobile  = ttAttorney.contact
           torganization.email   = ttAttorney.email         
           .
        
     create organization.
     buffer-copy torganization to organization.

     opcorgID = organization.orgID.
 
     validate organization.
     release organization.

     /* Build keyword index to use for searching */
     cQuery = "for each organization where organization.orgID = '" + corgID + "'".
     run sys/buildsysindex.p (input pRequest,
                              input pResponse,
                              input {&organization},     /* ObjType */
                              input "",                  /* ObjAttribute */
                              input corgID,              /* ObjID */
                              input {&organization},     /* SourceTable */
                              input cQuery,              /* SourceQuery */
                              input "").                 /* ExcludeFieldList */
     std-lo = true.
   end.
 else
   assign 
      opcorgID = organization.orgID
      corgID   = organization.orgID.
end procedure.  /* procedure createorganization : */

procedure createorgrole:

 empty temp-table torgrole.
               
 create torgrole.  
 assign 
       torgrole.orgID     = corgID
       torgrole.stateID   = ttAttorney.stateID
       torgrole.source    = {&AttorneyFirm}
       torgrole.sourceID  = ttattorney.attorneyID
       torgrole.active    = lActive
       .
 for first torgrole:
   {lib/nextkey.i &type='orgrole' &var=iorgroleID}                          
 
   create orgrole.                                                                
   buffer-copy torgrole except torgrole.orgroleID to orgrole. 
   
   orgrole.orgroleID  = iorgroleID. 
   
   validate orgrole.
   
   for first staterequirement no-lock
     where staterequirement.stateId   = orgrole.stateid
       and staterequirement.appliesto = orgrole.source
       and staterequirement.reqFor    = {&organizationcode}:
     if can-find(first statereqqual where statereqqual.requirementId = staterequirement.requirementId
                                      and statereqqual.active) 
      then
       do:
          {lib/comlogmsg.i &codeVal = '107' , &Msg = cMsgLog , &role = orgrole.source , &roleID = orgrole.sourceID}
          run com/newcompliancelog.p(input  {&organizationcode},
                                     input  orgrole.orgID,
                                     input  cMsgLog,
                                     input  stateRequirement.stateId,
                                     input  pRequest:UID,
                                     input  pRequest:actionid,
                                     input  orgrole.source,
                                     input  orgrole.sourceID,
                                     input  "",
                                     output lSuccess ).
          if not lSuccess 
           then
            do:
               pResponse:fault("3000", "Comlog").
               return.
            end. /* if not plSuccess */
       end. /* if can-find(first statereqqual  */        
   end. /* for first staterequirement no-lock */
   
   /* Activate the organization */
   for first organization exclusive-lock
     where organization.orgID = orgrole.orgID:  

     if organization.stat = {&InActiveStat} or organization.stat = ''
      then
       do:
         organization.stat = {&ActiveStat}.
         {lib/comlogmsg.i &codeVal = '105' , &Msg = cMsgLog}
         run com/newcompliancelog.p(input  {&organizationcode},
                                    input  organization.orgID,
                                    input  cMsgLog,
                                    input  orgrole.stateid,
                                    input  pRequest:UID,
                                    input  pRequest:actionid,
                                    input  "",
                                    input  "",
                                    input  "",
                                    output lSuccess ).
         if not lSuccess 
          then
           do:
              pResponse:fault("3000", "Comlog").
              return.
           end. /* if not plSuccess  */
       end. /* if organization.stat = {&InActiveStat}  */
   end. /* for first organization exclusive-lock */
   release orgrole.
 end. /* for first torgrole: */

end procedure. /* procedure createorgrole : */

procedure deleteorgrole :

for first orgrole exclusive-lock
  where orgrole.source   = {&AttorneyFirm}
    and orgrole.sourceID = ttAttorney.attorneyID:

  corgID = orgrole.orgID.
  
  for each staterequirement no-lock
    where staterequirement.stateId   = orgrole.stateID
      and staterequirement.reqFor    = {&organizationCode}
      and staterequirement.appliesTo = orgrole.source:
    
    if can-find(first statereqqual where statereqqual.requirementId = staterequirement.requirementId
                                     and statereqqual.active) 
     then
      do:
        {lib/comlogmsg.i &codeVal = '108' , &Msg = cMsgLog , &role = orgrole.source , &roleID = orgrole.sourceID }
        run com/newcompliancelog.p(input  {&organizationCode},   /*(P)*/
                                   input  orgrole.orgID,  
                                   input  cMsgLog,  
                                   input  orgrole.stateId,
                                   input  pRequest:UID,  
                                   input  pRequest:actionid,
                                   input  orgrole.source,        /*Lawfirm,Underwriter,Agent*/
                                   input  orgrole.sourceID,      /*sourceID or AgentID*/
                                   input  "",
                                   output lSuccess).
        
        for each fulfillment exclusive-lock 
          where fulfillment.requirementId = staterequirement.requirementId
            and fulfillment.entity        = orgrole.source   /* orgrole.source = Any role of orgrole like Attorney,closing agent,control organization...etc  */
            and fulfillment.entityID      = orgrole.sourceID: 
       
          delete fulfillment.
          release fulfillment.
          
        end. /* for each fulfillment exclusive-lock */
      end. /* if can-find(first statereqqual */
  end. /* for each staterequirement no-lock */ 

  delete orgrole.
  release orgrole.  
  if can-find(first affiliation where affiliation.partnerA    eq corgID ) or       
     can-find(first orgrole  where orgrole.orgID     eq corgID ) 
   then
    lIsActive = yes.
  
  if not lIsActive 
   then
    do:                                                                                      
      for first organization exclusive-lock
        where organization.orgID = corgID: 
        if organization.stat = {&ActiveStat} 
         then
          do:
            organization.stat = {&InActiveStat}.
            {lib/comlogmsg.i &codeVal = '106' , &Msg = cMsgLog ,&stat = 'inactive'}
            run com/newcompliancelog.p(input  {&organizationCode},
                                       input  organization.orgID ,
                                       input  cMsgLog,
                                       input  organization.state,
                                       input  pRequest:Uid,
                                       input  pRequest:actionid,
                                       input  "",
                                       input  "",
                                       input  "",
                                       output lSuccess ).
            if not lSuccess 
             then
              do:
                pResponse:fault("3000", "Comlog").
                return.
              end. /* if not lSuccess  */ 
          end. /* if organization.stat  */
        
      end. /* for first organization exclusive-lock */                                                                                 
    end. /* if not lIsActive then */ 
  std-lo = true.
end. /* for first orgrole exclusive-lock */

end procedure.  /* procedure deleteorgrole : */


