/*------------------------------------------------------------------------
@file getIDVerificationRate.cls
@action 
@description Get 

@returns 

@success 2005;SysProp returned X rows

@author SRK
@version 1.0
@created 11/26/2024
Modification:

----------------------------------------------------------------------*/

class com.getIDVerificationRate inherits framework.ActionBase:
    
  {tt/idverificationrate.i}
  
  

  constructor getIDVerificationRate ():
    super().
  end constructor.

  destructor public getIDVerificationRate ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable cagentid as character no-undo.
    define variable dsHandle as handle no-undo.

    {lib/std-def.i}
    {lib/callsp-defs.i}


    pRequest:getParameter("Agentid", output cagentid).


    if cagentid = ? or cagentid = "ALL"
     then
      cagentid = ''.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer idverificationrate:handle).

    {lib\callsp.i &name=spGetIDVerificationRate &load-into=idverificationrate &params="input cagentid" &noResponse=true}
    
    
    if csp-icount > 0
     then 
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList). 
    
    pResponse:success("2005", STRING(csp-icount) {&msg-add} "ID Verification Rate").
             
  end method.
      
end class.


