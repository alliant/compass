/*------------------------------------------------------------------------
@name com/newattorney.p
@action attorneyNew
@description Create new attorney

@throws 3005;No XML file attached

@author Sachin Chaturvedi
@version 1.0
@created 05/01/2018
Modification:
05/15/18    Sachin         Modified to fix stateID check.
05/18/18    Rahul          Modified t o fix testing bug
06/08/18    Rahul          Modified to add comlog logic.
06/18/18    Rahul          Modified to add validation for duplicate record.
06/20/18    Rahul          Modified to activate person status
06/21/18    Rahul          Modified to get compliance status 
07/10/18    Rahul          Modified comlog validation 
08/16/18    Naresh Chopra  Modified to add new parameter stateID in newcomlog.
09/09/18    Naresh Chopra  Modified to add new function getStat.
09/19/18    Gurvindar      Modified to add decodefromXML.
10/17/18    Naresh Chopra  Modfied to add new input Parameter "ExcludeFieldList" in buildsysindex.p call.
10/23/18    Gurvindar      Modified to remove add functionality
01/29/19    Anjly          Added comStatus and attorneyID output parameter
                           and removed attorney table as output param.
02/19/19    Anubha         Replaced XML parsing logic with get parameter
                           method to retrieve temp-table from XML. 
12/10/19    Shubham        Added the code to create New Person and Person Role.
09/16/20    AG             Set status change dates and create Info.
03/04/2022  SD             Task 89909 - Modified nextkey.i
03/17/2022  Shefali        Task# 86699 Modified to add new attorney firm.
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable hParser          as handle    no-undo.
define variable cNewAttorneyID   as character no-undo.
define variable cQuery           as character no-undo.
define variable plSuccess        as logical   no-undo.
define variable lisActive        as logical   no-undo.
define variable cnotes           as character no-undo.
define variable cMsgLog          as character no-undo.
define variable cComStatus       as character no-undo.
define variable opcOrgID         as character no-undo.
define variable opcPersonID      as character no-undo.
define variable dtCreateDate     as datetime  no-undo.


define buffer bfattorney for attorney.

{tt/attorney.i &tableAlias="ttAttorney"}

{tt/file.i}
{lib/std-def.i}
{lib/nextkey-def.i}
{lib/com-def.i}

empty temp-table ttattorney.
pRequest:getParameter("attorney", input-output table ttattorney).

std-lo = false.

for first ttattorney:
  if ttattorney.orgid <> "" and ttattorney.orgid <> ? 
   then
    do:
      if can-find(first attorney where attorney.orgid   = ttattorney.orgid
                                   and attorney.stateid = ttattorney.stateid) 
       then
        do:
          pResponse:fault("3005", "Organization is already attorney for this state.").
          return.
        end.  /* if can-find(first attorney */
    end. /* if ttattorney.personid ne "" */
  else 
   if ttattorney.personid <> "" and ttattorney.personid <> ? 
    then
     do:
       if can-find(first attorney where attorney.personid = ttattorney.personid
                                    and attorney.stateid  = ttattorney.stateid) 
        then
         do:
           pResponse:fault("3005", "Person is already attorney for this state.").
           return.
         end.  /* if can-find(first attorney */
     end. /* if ttattorney.personid ne "" */
end. /* for first ttattorney: */

TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:   

  for first ttattorney:

    {lib/nextkey.i &type='attorney' &var=std-in &err="undo TRX-BLOCK , leave TRX-BLOCK."}
      
    cNewAttorneyID = "".
    for first state no-lock
      where state.stateID = ttattorney.stateID :

      cNewAttorneyID = string(state.seq,"99") + trim(string(std-in, ">>9999")) no-error.
      
      if error-status:error or cNewAttorneyID = ?
       then 
        cNewAttorneyID = "".
    end. /* for first state no-lock */ 
       
    if cNewAttorneyID = ""
     then 
      do:
        pResponse:fault("3006", "Algorithm" {&msg-add} "Attorney ID").
        leave TRX-BLOCK. 
      end. /*  if cNewAttorneyID = "" */
    
    if can-find(first attorney where attorney.attorneyID = cNewAttorneyID)
     then
      do:
        pResponse:fault("3011", "Attorney" {&msg-add} "ID" {&msg-add} cNewAttorneyID).
        leave TRX-BLOCK. 
      end. /* if can-find(first attorney */
      
    if can-find(first ttattorney where ttattorney.orgID ne "" and ttattorney.orgID ne ?) 
     then
      do:
        if ttattorney.orgid <> "" and ttattorney.orgid <> ? 
         then
          do:
            if ttattorney.orgid = "-99" 
             then
              run setAttorneyOrganization in this-procedure.    
          end.
      end.
     else 
      do:
        if ttattorney.personid <> "" and ttattorney.personid <> ? 
         then
          do:
            if ttattorney.personid = "-99" 
             then
              run setAttorneyPerson in this-procedure.    
          end.
       end.
      
       
    create attorney.
    buffer-copy ttattorney 
      except ttattorney.attorneyID ttattorney.createdBy ttattorney.createDate 
      to attorney.    

    find first organization WHERE organization.orgID = ttattorney.orgID no-lock no-error.
    if avail organization 
     then
      assign attorney.addr1   = organization.addr1
             attorney.addr2   = organization.addr2
             attorney.city    = organization.city
             attorney.state   = organization.state
             attorney.zip     = organization.zip
             attorney.phone   = organization.phone
             attorney.website = organization.website
             attorney.state   = organization.state
             .
    assign
      attorney.attorneyID = cNewAttorneyID
      attorney.createdBy  = pRequest:UID
      attorney.createDate = now
      dtCreateDate        = now
      .

    case attorney.stat:
      when {&ActiveStat}    then attorney.activeDate    = now.
      when {&ProspectStat}  then attorney.prospectDate  = now.
      when {&ClosedStat}    then attorney.closedDate    = now.
      when {&CancelledStat} then attorney.cancelDate    = now.
      when {&WithdrawnStat} then attorney.withdrawnDate = now.
    end case.

    if ttattorney.orgid <> "" and ttattorney.orgid <> ? 
     then
      do:
        run setAttorneyOrganization in this-procedure.
        opcOrgID = ttAttorney.orgid.
      end.
    else if ttattorney.personid <> "" and ttattorney.personid <> ? 
     then
      do:
        run setAttorneyPerson in this-procedure.
        opcPersonID = ttAttorney.personID.
      end.

    validate attorney.     
    release attorney.
    
    run arc/sendattorney.p (cNewAttorneyID, output std-lo, output std-ch).
    if not std-lo
     then pResponse:addFault("3005", std-ch).

    if can-find(first ttattorney where ttattorney.orgID ne "" and ttattorney.orgID ne ?) 
     then
      do:
       run com/checkcompliance.p(input  {&OrganizationCode},       /*(O)*/
                                 input  ttattorney.stateID,
                                 input  {&AttorneyFirm},          /* (AttorneyFirm) */
                                 input  cNewAttorneyID,
                                 output cComStatus).
      end.
     else
     do:
       run com/checkcompliance.p(input  {&PersonCode},       /*(P)*/
                                 input  ttattorney.stateID,
                                 input  {&Attorney},         /* (Attorney) */
                                 input  cNewAttorneyID,
                                 output cComStatus). 
     end.
    /* Build keyword index to use for searching */
    cQuery = "for each attorney where attorney.attorneyID = '" + cNewAttorneyID + "'".
    run sys/buildsysindex.p (input pRequest,
                             input pResponse,
                             input {&Attorney},           /* ObjType      */
                             input "",                    /* ObjAttribute */
                             input cNewAttorneyID,        /* ObjID        */
                             input {&Attorney},           /* SourceTable  */
                             input cQuery,                /* SourceQuery  */
                             input "").                   /* ExcludeFieldList */
    std-lo = true.
  end. /* for first ttattorney: */ 
end. /* do transaction */

if pResponse:isFault()
 then
  return.

if not std-lo
 then
  do: 
    pResponse:fault("3000", "Create").
    return.
  end. /* if not std-lo */
pResponse:success("2002", "Attorney").
pResponse:setParameter("attorneyID", cNewAttorneyID).
pResponse:setParameter("ComStatus", cComStatus).
pResponse:setParameter("createDate", dtCreateDate).

if opcOrgID <> "" and opcOrgID <> ?
 then
  do:
    pResponse:setParameter("orgID", opcOrgID).
  end.
 else 
  if opcPersonID <> "" and opcPersonID <> ?
 then
  do:
    pResponse:setParameter("personID", opcPersonID).
  end.

PROCEDURE setAttorneyOrganization:
  RUN com\setattorneyorganization.p(input pRequest,
                                    input pResponse,
                                    input "New",
                                    input cNewAttorneyID,
                                    input table ttAttorney,
                                    output opcOrgID).
                          
  if ttAttorney.orgID = "-99" 
   then
    ttAttorney.orgID = opcOrgID.
  if pResponse:isFault()
   then 
    return.
END.  /* PROCEDURE setAttorneyOrganization: */

PROCEDURE setAttorneyPerson:
  RUN com\setattorneyperson.p(input pRequest,
                              input pResponse,
                              input "New",
                              input cNewAttorneyID,
                              input table ttAttorney,
                              output opcPersonID).
                          
  if ttAttorney.personID = "-99" 
   then
    ttAttorney.personID = opcPersonID.
  if pResponse:isFault()
   then 
    return.
END.  /* PROCEDURE setAttorneyPerson: */
