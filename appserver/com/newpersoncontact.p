/*------------------------------------------------------------------------
@name com/newpersoncontact.p   
@action personContactNew
@description new person contact.   
@param personContact table
@throws 3066;personContact does not exist
@throws 3004;personContact is locked
@throws 3000;Update failed
@author  Sachin Anthwal
@version 1.0
@created 08/12/2022
@modified
name        date           comments
SRK         03/27/24       Added validations
S chandu    04/10/24       Task 111689 - Modified to add extension for phone type.              
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable lFound             as logical   no-undo.
define variable lCreate            as logical   no-undo.
define variable lerror             as logical   no-undo.
define variable cerrmsg            as character no-undo.
define variable cvalidNumber       as character no-undo.
define variable iPersonContactID   as integer   no-undo.
define variable cEmailDomain       as character no-undo.
define variable iposition          as integer   no-undo.
define variable cEmailExtension    as character no-undo.

/* Standard Lib files */
{lib/std-def.i}
{lib/validEmailAddr.i}
{lib/nextkey-def.i}

{tt/personContact.i &tableAlias="ttpersonContact"}

empty temp-table ttpersonContact.
pRequest:getParameter("personContact", input-output table ttpersonContact).   

if not can-find(first ttpersonContact where ttpersonContact.personID <> ? or ttpersonContact.personID <> '')
 then
  do:
    pResponse:fault("3066", "PersonContact").
    return.
  end.

for first ttpersonContact :

  if ttpersonContact.contactType = "P"
   then
    do:
      if ttpersoncontact.contactSubType = "Mobile" and can-find( first personcontact where personcontact.contactID = ttpersonContact.contactID)
       then
        do:
          pResponse:fault("3005","Duplicate Person Contact.").
          return.
        end.
       else
        if ttpersoncontact.contactSubType <> "Mobile" and can-find( first personcontact where personcontact.contactID = ttpersonContact.contactID and personcontact.contactSubType = "Mobile")
         then
          do:
            pResponse:fault("3005","Duplicate Person Contact.").
            return.
          end.
    end.

  if ttpersonContact.contactType = "E"
   then
    do:
      if ttpersonContact.contactSubType = "Work" and can-find( first person where person.personID = ttpersonContact.personID and person.internal) then
       do:
         publish "GetSystemParameter" ("emailDomain", output cEmailDomain).
         iposition = index(ttpersonContact.contactID, "@",1).
         cEmailExtension = substring(ttpersonContact.contactID,iposition,length(ttpersonContact.contactID)).
         if cEmailExtension <> cEmailDomain
          then 
           do:
             pResponse:fault("3005","Invalid Email Domain.").
             return.
           end.
       end.

      if can-find( first personcontact where personcontact.contactID = ttpersonContact.contactID )
       then
        do:
          pResponse:fault("3005","Duplicate Person Contact.").
          return.
        end. 
    end.

  if ttpersonContact.contactID = "" or ttpersonContact.contactID = ?
   then
    do:
      pResponse:fault("3005","Person Contact cannot be blank.").
      return.
    end.

  if ttpersonContact.contactType = "E" and (ttpersonContact.contactID <> "" or ttpersonContact.contactID <> ?)
     and not validEmailAddr(ttpersonContact.contactID)
   then
    do:
      pResponse:fault("3005","Invalid Email Address.").
      return.
    end.

  if ttpersonContact.contactType = "P" and (ttpersonContact.contactID <> "" or ttpersonContact.contactID <> ?)
   then
    do:
      run util/validatecontactnumber-p.p(input ttpersonContact.contactID,output cvalidNumber,output lerror,output cerrmsg).
      if lerror
       then
        do:
          pResponse:fault("3005", cerrmsg).
          return.
        end.     
    end.
end.

assign 
    std-lo  = false
    .

TRX-BLOCK: 
do transaction                               
  on error undo TRX-BLOCK, leave TRX-BLOCK:  

  for first ttpersonContact: 

    if ttpersonContact.expirationDate = ? or ttpersonContact.expirationDate > datetime(today)
     then
      do:
        if can-find( first personcontact where personcontact.personID = ttpersonContact.personID and personcontact.personContactID <> ttpersonContact.personContactID 
                  and personcontact.contactType = ttpersonContact.contactType  and personContact.contactSubType = ttpersonContact.contactSubType and
                  (personcontact.expirationDate = ? or personcontact.expirationDate > datetime(today)))
         then
          for each personcontact where personcontact.personID = ttpersonContact.personID and personcontact.personContactID <> ttpersonContact.personContactID 
                           and personcontact.contactType = ttpersonContact.contactType and personContact.contactSubType = ttpersonContact.contactSubType and
                         (personcontact.expirationDate = ? or personcontact.expirationDate > datetime(today)) exclusive-lock :

            personcontact.expirationDate = today.

            validate personcontact.
            release personcontact.
          end.
      end.
   
    {lib/nextkey.i &type='personContact' &var=iPersonContactID &err="undo TRX-BLOCK, leave TRX-BLOCK."}

    create personContact.
    assign   
        personContact.personcontactID  = iPersonContactID
        personContact.personID         = ttpersoncontact.personID
        personContact.contactType      = ttpersonContact.contactType 
        personContact.contactSubType   = ttpersonContact.contactSubType 
        personContact.contactID        = if ttpersonContact.contactType  = "P" 
                                            then (if ttpersonContact.extension <> '' and ttpersonContact.contactSubType <> 'Mobile' then cvalidNumber + ','  + ttpersonContact.extension else cvalidNumber) 
                                           else ttpersonContact.contactID
        personContact.uid              = pRequest:uid
        personContact.createDate       = today.

    
    validate personContact.
    release personContact. 
    std-lo = true.

  end.
end.  

if not std-lo
 then
  do:
    pResponse:fault("3000", "Create").
    return.
  end.

pResponse:success("2002", "Person Contact").
pResponse:setParameter("PersonContactID", iPersonContactID).
