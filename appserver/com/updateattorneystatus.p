&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file updateattorneystatus.p
@description It's a wrapper to call changeattorneystatus.p

@author Shefali
@created 11.23.2021
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

{lib/std-def.i}

/* parameters */
define variable cAttorneyID   as character no-undo.
define variable cStatus       as character no-undo.
define variable dtStatusDate  as datetime  no-undo.

/* functions */
{lib/replace-null.i}
{lib/add-delimiter.i}
{lib/run-http.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

pRequest:getParameter("AttorneyID", output cAttorneyID).
pRequest:getParameter("Status",     output cStatus).

/* validate the attorney id is valid */
if not can-find(first attorney where attorney.attorneyID = cAttorneyID) 
 then
  pResponse:fault("3067", "attorney" {&msg-add} "ID" {&msg-add} string(cAttorneyID)).

if pResponse:isFault() 
 then
  return.
 
run com/changeattorneystatus.p (cAttorneyID, cStatus,pRequest:UID,pRequest:actionid,output dtStatusDate,output std-lo, output std-ch).
if not std-lo
 then
  do:
    pResponse:fault("3005", std-ch).
    return.
  end.

pResponse:success("2000", "Attorney status change").
pResponse:setParameter("StatusDate", dtStatusDate).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


