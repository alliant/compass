/*----------------------------------------------------------------------
@name com/queryypendingreviews.p
@action pendingReviewsQuery
@description Returns a dataset of the qualfication and its review status.
             Required for Pending Reviews Report

@param pcstateID;char;The pcstateID 
@param pcEntity;char;The pcEntity
@param pcEntityID;int;The pcEntityID
@param plIsLinked;logical;The plIsLinked

@returns The qualification dataset
@success 2005;char;The count of the records returned

@author Rahul
@version 1.0
@created 07/12/2018
Modification:
Date        Name           Description
07-16-2018  Gurvindar      Modified to add ALL option for entityID 
07-18-2018  Rahul          Modified to get only linked qualifications
07-31-2018  Rahul Sharma   Modified to get linked or all qualifications based on param
11-15-2018  Rahul Sharma   Modified to change file name from getqualreviews.p to querypendingreviews.p
12-28-2018  Rahul Sharma   Modified to remove qualreview temp-table
01-03-2018  Rahul Sharma   Modified to change logical parameter for Unused qualifications
23/04/2019  Gurvindar      Added logic to return Username instead of uid 
18/08/2020  VJ             Added logic to return all state pending reviews
28/08/2020  VJ             Added logic to get active pending reviews
05/20/2022  Shefali        Task# 93684 To get only Entity specific pending reviews
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable pcEntityID       as character no-undo.
define variable pcstateID        as character no-undo.
define variable pcEntity         as character no-undo.
define variable plIncludeUnused  as logical   no-undo.
define variable plActive         as logical   no-undo.

pRequest:getParameter("stateID",       output pcstateID).
pRequest:getParameter("Entity",        output pcEntity).
pRequest:getParameter("EntityID",      output pcEntityID).
pRequest:getParameter("includeUnused", output plIncludeUnused).
pRequest:getParameter("includeActive", output plActive).

{lib/std-def.i}
{lib/com-def.i}
{tt/qualification.i &tableAlias=ttqualification}

std-in = 0.
/* Get qualification and its associated review record as per searching criteria*/
for each qualification no-lock
  where qualification.stateId   = (if pcstateID  = {&ALL} then  qualification.stateId else pcstateID)
    and qualification.entity    = pcEntity
    and qualification.entityId  = (if pcentityid = {&ALL} then  qualification.entityid else pcentityid)
    and qualification.active :
  
  if not plIncludeUnused and not can-find(first fulfillment where fulfillment.qualificationId = qualification.qualificationId)
   then 
    next.
  
   if qualification.entity = {&OrganizationCode} and not can-find( first organization where organization.orgID = (if pcentityid = {&ALL} then qualification.entityid else pcentityid))
   then
    next.

  if qualification.entity = {&PersonCode} and not can-find( first person where person.personID = (if pcentityid = {&ALL} then  qualification.entityid else pcentityid))
   then
    next.
  
  /* if orgrole is inactive then do not create record */
  if plActive and qualification.entity = {&OrganizationCode} and (not can-find(first orgrole where orgrole.orgID = qualification.entityID and orgrole.active )) 
   then
    next.
  
  /* if personrole is inactive then do not create  record */
  if plActive and qualification.entity = {&PersonCode} and  (not can-find(first personrole where personrole.personID = qualification.entityID and personrole.active )) 
   then
    next.  
    
  /* Get qualifications which are linked to requirement */
  create ttqualification.
  buffer-copy Qualification to ttqualification.
  
  assign 
        std-in = std-in + 1.
  
  for last review no-lock
    where review.stateId         = qualification.stateid                       
      and review.qualificationId = qualification.qualificationId:

    assign 
          ttqualification.reviewDate        = review.reviewDate
          ttqualification.numvaliddays      = review.numvaliddays
          ttqualification.nextReviewDueDate = review.nextreviewDueDate
          .
    
    for first sysuser no-lock 
      where sysuser.uid = review.reviewBy:
      ttqualification.reviewBy  = sysuser.name.
    end. /*  for first sysuser no-lock  */

  end. /* for last review no-lock */
  
  /* Get entity name of qualification*/
  if qualification.entity = {&OrganizationCode} 
   then
    do:
      for first organization no-lock
        where organization.orgID = qualification.entityID:
        ttqualification.name =  organization.name.        
      end. /* for first organization no-lock */
    end. /* if qualification.entity = {&OrganizationCode}  */
  else if qualification.entity = {&PersonCode} 
   then
    do:
      for first person no-lock
        where person.personID = qualification.entityID:
        ttqualification.name =  person.dispname.
      end. /* for first person no-lock */
    end. /* else if qualification.entity = {&PersonCode}  */    
end. /* for each qualification...*/

if std-in > 0 
 then 
  pResponse:setParameter("qualification", table ttqualification).

pResponse:success("2005", string(std-in) {&msg-add} "qualification").

