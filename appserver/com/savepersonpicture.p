/*------------------------------------------------------------------------
@name com/savepersonpicture.p
@description Save Person Picture in DB

@author 
@version 
@created 
Modification:
Date          Name         Description
28/07/2022     VR          Modified to support clob field changes
----------------------------------------------------------------------*/
define input  parameter cPersonID  as character no-undo.
define input  parameter cPicture   as longchar  no-undo.
define output parameter lSuccess   as logical   no-undo.
define output parameter cErrorMsg  as character no-undo.

define variable icnt as integer no-undo.

TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:
  for first person exclusive-lock 
    where person.personID = cPersonID :
    assign 
          person.picture = cPicture 
          no-error
          . 

    if error-status:error then
    do:
      do iCnt = 1 to error-status:num-messages:
        cErrorMsg = cErrorMsg + "::" + error-status:get-message(iCnt).
      end.
      cErrorMsg = cErrorMsg + "Size: " + string(length(cPicture, "RAW")) + string(length(cPicture, "CHARACTER")).
      leave TRX-BLOCK.
    end.
  end.
  
  lSuccess = true. 
end.
