/*------------------------------------------------------------------------
@name deletecompliancenote.p
@action complianceNoteDelete
@description Deletes a comnote record from the database
@param cEntity;char;
@param cEntityId;char;
@param cReqId;char;
@param iSeq;integer;

@throws 3000;The delete failed
@throws 3004;The error message
@success 2008;The comnote was deleted

@author :Rahul Sharma
@version 1.0
@created 11/14/2018
Date          Name       Description

----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

{lib/std-def.i}

define variable cEntity   as character no-undo.
define variable cEntityId as character no-undo.
define variable cReqId    as integer   no-undo.
define variable iSeq      as integer   no-undo.

pRequest:getParameter("Entity", cEntity).
pRequest:getParameter("EntityId", cEntityId).
pRequest:getParameter("cReqId", cReqId).
pRequest:getParameter("Seq", iSeq).

assign
  std-lo = false.

comnote-BLOCK:
for first comnote exclusive-lock
  where comnote.entity    = cEntity 
    and comnote.entityId  = cEntityId
    and comnote.reqId     = cReqId
    and comnote.seq       = iSeq transaction
  on error undo comnote-block, leave comnote-block:
   
  delete comnote.
  release comnote.

  std-lo = true.
end.

if not std-lo then 
do:
  pResponse:fault("3000", "Delete").
  return.
end.

pResponse:success("2008", "Compliance Note").


