/*------------------------------------------------------------------------
@name com/getagentpersons.p
@action agentpersonsget
@description Return the tables for personagent i.e.
@param piPersonAgentID;char;The RoleID of a personagent
       
@returns The role dataset
@success 2005;char;The count of the records returned

@author Sachin Anthwal
@version 1.0
@created 08/08/2022
Modification:
Date          Name             Description 
04/02/2024    S Chandu         Task #:111689 Modified to get details from personcontact-p.p
                               and if notes is empty fetching from person table.
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable pcAgentID      as character no-undo.
define variable cContactEmail  as character no-undo.
define variable cContactPhone  as character no-undo.
define variable cContactMobile as character no-undo.

{lib/std-def.i}
{lib/com-def.i}

{tt/personagent.i &tableAlias="ttpersonagent"}


pRequest:getParameter("agentID",output pcAgentID).  

if pResponse:isFault()
 then 
  return.

std-in = 0.

for each personagent no-lock 
  where personagent.agentID = pcAgentID :
  
  create ttpersonagent.
  assign
      ttpersonagent.personAgentID   =  personagent.personAgentID  
      ttpersonagent.agentID         =  personagent.agentID        
      ttpersonagent.PersonID        =  personagent.PersonID       
      ttpersonagent.effectivedate   =  personagent.effectivedate  
      ttpersonagent.expirationDate  =  personagent.expirationDate 
      ttpersonagent.jobTitle        =  personagent.jobTitle                
      .
  if can-find(first affiliation where ((affiliation.partnerAType = 'O' and affiliation.partnerBType = 'P') and affiliation.partnerB = personagent.personid)) or
     can-find(first personrole where personrole.personid = personagent.personid and personrole.active)
      then
        ttpersonagent.linkedToCompliance = true. 

  for first affiliation no-lock where affiliation.partnerB = personagent.personID :
  ttpersonagent.isCtrlPerson = affiliation.isCtrlPerson.
  end.

  for first person no-lock
    where person.personID = personagent.personID:
      ttpersonagent.personName = person.dispname.
      ttpersonagent.doNotCall = person.doNotCall.
      ttpersonagent.notes      = if (personagent.notes = "" or personagent.notes = ? ) then person.notes else personagent.notes .
  end.

  assign
      cContactEmail  = ''
      cContactMobile = ''
      cContactPhone  = ''
      .

  run com/getpersoncontact-p.p(input personagent.PersonID , output cContactEmail,output cContactMobile,output cContactPhone).
  assign
      ttpersonagent.email = cContactEmail
      ttpersonagent.phone = cContactMobile
      .

  std-in = std-in + 1.
end. /* for each personagent */
   
if std-in > 0 
 then
  pResponse:setParameter("personagent", table ttpersonagent).

  
pResponse:success("2005", string(std-in) {&msg-add} "personagents").
