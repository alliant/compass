/*------------------------------------------------------------------------
@file GetIDVerificationSW.cls
@action 
@description Get 

@returns 

@success 2005;SysProp returned X rows

@author SRK
@version 1.0
@created 11/26/2024
Modification:

----------------------------------------------------------------------*/

class com.GetIDVerificationSW inherits framework.ActionBase:
    
  {tt/idverificationsw.i}

  constructor GetIDVerificationSW ():
    super().
  end constructor.

  destructor public GetIDVerificationSW ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable dsHandle as handle no-undo.

    {lib/std-def.i}

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer idverificationsw:handle).

    std-in = 0.
    for each syscode no-lock where syscode.codeType  = "IdentityVerificationService" :
    
      create idverificationsw.
      buffer-copy syscode to idverificationsw.
    
      std-in = std-in + 1.
    end.
    
    
    if std-in > 0
     then 
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).  
    
    pResponse:success("2005", STRING(std-in) {&msg-add} "ID VerificationSW").
             
  end method.
      
end class.


