/*------------------------------------------------------------------------
@name modifyorganizationrole.p
@action organizationroleModify
@description Modify organization role record

@throws 3005;No XML file attached

@author Gurvindar
@version 1.0
@create  09/12/2019
Modification:
Date          Name           Description
-------------------------------------------------------------------------

-------------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

/* Temp-table Definitions */
{tt/orgrole.i &tableAlias="ttorgrole"}
{tt/file.i}

/* Standard lib files */
{lib/std-def.i}
{lib/com-def.i}
    
empty temp-table ttorgrole.
pRequest:getParameter("orgrole", input-output table ttorgrole).
        
std-lo = false.

for first ttorgrole:
  /* If non-zero orgroleID is received, then check for the existence of orgrole record */
  if (ttorgrole.orgroleID eq 0) or not can-find(first orgrole where orgrole.orgroleId = ttorgrole.orgroleId)  
   then
    do:
      pResponse:fault("3083", "orgrole").
      return.
    end.
end.

TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:

  for first ttorgrole:
    /* Modifying existing orgrole record */
    for first orgrole exclusive-lock 
      where orgrole.orgroleID = ttorgrole.orgroleID :
      buffer-copy ttorgrole except ttorgrole.orgroleID to orgrole.
    end.   
    release orgrole.
  end.
  std-lo = true.
end.

if not std-lo
 then
  do: 
    pResponse:fault("3000", "Update").
    return.
  end.  
pResponse:success("2012", "orgrole").
