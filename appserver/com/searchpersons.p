/*------------------------------------------------------------------------
@name com/searchpersons.p
@action personsSearch
@description Returns a dataset of the person based on the criteria

@param cstate;char;The stateID of person
@param csearchstring;char;The searchString of person

@returns The person data
@success 2005;char;The count of the records returned

@author Shubham
@version 1.0
@created
Date          Name      Description
07/16/2020    Shubham   Added new field for client side use.
11/07/2022    S Chandu  Modified code to get Data according to Role parameter.
02/24/2022    SB        Task# 102649 Parameterize the Person Screen.
06/27/2023    SB        Task# 104008 Modified to search person by tags
03/28/2024    S Chandu  Task #:111689 Remove the reference of Person.mobile, Person.phone and Person.email
                        and gets details from personcontact table.
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable cSearchString  as character no-undo.
define variable cNameType      as character no-undo.
define variable cNameBegins    as character no-undo.
define variable cstate         as character no-undo.
define variable cRoleParam     as character no-undo.

define variable iCount         as integer   no-undo.
define variable cObjKey        as character no-undo.
define variable cRoleList      as character no-undo.
define variable cRoles         as character no-undo.
define variable iRindex        as integer   no-undo.
define variable cNewSearchString  as character no-undo.
define variable cTagsList         as character no-undo.
define variable cStringsList      as character no-undo.
define variable cContactEmail     as character no-undo.
define variable cContactPhone     as character no-undo.
define variable cContactMobile    as character no-undo.

{lib/std-def.i}
{lib/com-def.i}
{tt/person.i &tableAlias="tagperson"}
{tt/person.i &tableAlias="tperson"}

define temp-table ttperson like tperson
  field tempPercent as decimal.

pRequest:getParameter("searchString", output cSearchString).
pRequest:getParameter("nameType", output cNameType).
pRequest:getParameter("nameBegins", output cNameBegins).

cSearchString = trim(cSearchString).

pRequest:getParameter("stateID", output cstate).
if cstate = "" 
 then
  cstate = "ALL".

pRequest:getParameter("role",   output cRoleParam).
if cRoleParam = ? or cRoleParam = ""
 then
  cRoleParam = "ALL".

if pResponse:isFault()
 then 
  return.

/* keywords have space and comma as delimiter */
cSearchString = replace(cSearchString," ",',').

/* remove any blank strings */
do std-in = 1 to num-entries(cSearchString):
  if entry(std-in,cSearchString) eq "" 
   then
    next.
   else
    cNewSearchString = cNewSearchString + "," + entry(std-in,cSearchString).     
end.
cNewSearchString = trim(cNewSearchString,",").

/* separate normal and hash strings */
do std-in = 1 to num-entries(cNewSearchString):
  if entry(std-in,cNewSearchString) begins "#" 
   then
    cTagsList = cTagsList + "," + left-trim(entry(std-in,cNewSearchString),"#").
   else
    cStringsList = cStringsList + "," + entry(std-in,cNewSearchString).     
end.

assign
    cTagsList    = trim(cTagsList,",")
    cStringsList = trim(cStringsList,",")
    .
 
if cTagsList ne "" 
 then
  do:
    /* get persos as per first search tag */
    for each tag no-lock where tag.entity = "P" and
                               tag.name = entry(1,cTagsList,","):
    
      /* Get all the person as per stateId in ttperson */
      for first person no-lock 
        where person.personID = tag.entityID and 
              person.state    = (if cstate = {&ALL} then Person.state else cState): 
    
        if not can-find(first tagperson where tagperson.personID = person.personID) then
         do:
           create tagperson.
           buffer-copy person to tagperson.
           assign tagperson.tagBy = tag.uid.
         end.
      end.
    end.
   
   /* delete those persons for which rest of the search tags are not met(AND operation) */
   if num-entries(cTagsList,",") > 1 then
    for each tagperson: /* person */
      count-loop:
      do icount = 2 to num-entries(cTagsList,","):
        std-lo = false.
        for first tag no-lock where tag.entity   = "P"                             and
                                    tag.entityID = tagperson.personID               and 
                                    tag.name     = entry(icount,cTagsList,","):

          std-lo = true.
          next count-loop.               
        end.
        
        if std-lo = false
         then
          delete tagperson.
      end. /* count-loop */
    end.
  end.

if cNameType <> "" and cNameBegins <> ""
 then
  do:
    for each person no-lock: 
      assign iRindex = r-index(person.dispName, " ").

      if (person.state = if cState = "ALL" then person.state else cState) and 
       ((cNameType = "FirstName" and ((person.firstName <> "" and person.firstName begins cNameBegins) or person.dispName begins cNameBegins)) or
        (cNameType = "LastName" and  ((person.lastName  <> "" and person.lastName  begins cNameBegins) or (iRindex <> 0 and substring(person.dispName, iRindex + 1) begins cNameBegins))))
        then
         do:
           create ttperson.
           buffer-copy person to ttperson.
         end.
      
    end. /* for first person no-lock */
  end.
else if cStringsList = {&ActiveStat} or cStringsList = {&InActiveStat}
 then
  do:
    for each person no-lock
      where person.active = if cStringsList = {&ActiveStat} then yes else no:
      if ((person.state    = if cState = "ALL" then person.state else cState)
       or can-find(first personrole where personrole.personID   = person.personID 
                                      and personrole.stateID    = cState))
       then
        do:
	      if not can-find(first ttperson where ttperson.personID = person.personID) then
	       do:
             create ttperson.
             buffer-copy person to ttperson.
             ttperson.matchPercent = 100.
	       end.
        end. /* if (person.state = if cState = "ALL" then person.state else cState) */
    end. /* for first person no-lock */
  end. /* if cSearchString = {&ActiveStat} or cSearchString = {&InActiveStat} */
else
 do:
   do icount = 1 to num-entries(cStringsList,","): 
   cObjKey = entry(icount,cStringsList,",").
     for each sysindex no-lock
       where sysindex.objType      = {&Person}
         and sysindex.objAttribute = ""
         and sysindex.objKey       begins cObjKey :    
                      
       std-de = (length(entry(icount,cStringsList,",")) / length(sysindex.objKey)) * (100 / num-entries(cStringsList," ")).
     
       if can-find(first ttperson where ttperson.personID = sysindex.objID) 
        then
         do:
           for first ttperson 
             where ttperson.personID = sysindex.objID:                           
             if std-de > ttperson.tempPercent  
              then
               ttperson.tempPercent = std-de.
           end. /* for first ttperson */
         end. /* if can-find(first ttperson where ttperson.personID = integer(sysindex.objID))  */
       else
        do:
          for first person no-lock
            where person.personID = sysindex.objID:
            if ((person.state = if cState = "ALL" then person.state else cState)
             or can-find(first personrole where personrole.personID = sysindex.objID 
                                            and personrole.stateID  = cState))
             then
              do:
	            if not can-find(first ttperson where ttperson.personID = person.personID) then
		         do:
                   create ttperson.
                   buffer-copy person to ttperson.
                   ttperson.tempPercent = std-de.
		         end.
              end. /* for first person no-lock */
          end. /* for first person no-lock */
        end. /* else */
     end. /* for each sysindex no-lock */
     
     for each ttperson:
       assign
             ttperson.matchPercent = ttperson.matchPercent + ttperson.tempPercent
             ttperson.tempPercent  = 0
             .

     end. /* for each ttperson: */

   end. /* do icount = 1 to num-entries(cSearchString," "): */

  if cNewSearchString = "" then
   do:
     for each person no-lock
       where person.state = (if cState = "ALL" then person.state else cState):

       create ttperson.
       buffer-copy person to ttperson.
     end. /* for first person no-lock */
   end.
 end. /* else */

 /* Find records common between two tables. */                                              
 if ((not temp-table ttperson:has-records) 
      or 
     (not temp-table tagperson:has-records))      

  then
   do:
      /* If both lists have entries and any of the temp-table(tagperson or ttperson) is empty then
      empty both tables as then there would not be any record common between two tables. */
      if (cTagsList ne "") and 
         (cStringsList ne "")
       then
        do:
          empty temp-table tagperson.
          empty temp-table ttperson.
        end.
       else if num-entries(cTagsList)    eq 0       or 
               num-entries(cStringsList) eq 0 
        then
         do:
           if (not temp-table ttperson:has-records)     and 
               temp-table tagperson:has-records
            then
             for each tagperson:
               /* As ttperson table would be used to contain paged records, 
                  hence store the records of 'ttperson' into 'tagperson' */
               create ttperson.
               buffer-copy tagperson to ttperson.
             end.
         end. 
   end.

 else if temp-table tagperson:has-records     and 
         temp-table ttperson:has-records 
  then
   /* find records common between the two tables */ 
   for each ttperson:
     if not can-find(first tagperson where tagperson.personID = ttperson.personID) 
      then
       delete ttperson.
   end.


 
 std-in = 0.
/* Add Seq field for sorting as per search relevancy and list of person roles. */
for each ttperson by ttperson.matchPercent desc by ttperson.dispname by ttperson.personID:

  if can-find(first personagent where personagent.personid  = ttperson.personid and                                
                                    ((personagent.effectivedate <= now or personagent.effectivedate = ?) and
                                     (personagent.expirationdate > now or personagent.expirationdate = ?))) 
    then
     assign ttperson.linkedToAgent = true.

  if can-find(first affiliation where affiliation.partnerB = ttperson.personid and affiliation.partnerBType = "P") or
     can-find(first personrole  where personrole.personid  = ttperson.personid and personrole.active) 
    then
     assign ttperson.linkedToCompliance = true.

  assign
      cContactEmail  = ''
      cContactPhone  = ''
      cContactMobile = ''
      .

  run com/getpersoncontact-p.p(input ttperson.PersonID , output cContactEmail,output cContactMobile,output cContactPhone).
  assign
      ttperson.contactMobile = cContactMobile
      ttperson.contactPhone  = cContactPhone
      ttperson.contactemail  = cContactEmail
      .

  assign cRoleList = "".

  /* a role list is genrated  for person */ 
  for each personrole no-lock where personrole.personId = ttperson.personID :   
    if cRoleList = ""
     then 
      cRoleList = personrole.role.  
    else
     do:
       if not can-do(cRoleList,personrole.role)
        then
         cRoleList = cRoleList + "," + personrole.role.
     end. /* end else cRoleList ne "" */
  end. /* for each personRole */
  
  cRoles = "".

  /* a roles list is generated according to comcode*/
  for each comcode no-lock where comcode.code = {&PersonRole} :
    if can-do(cRoleList,comcode.description)
      then
       do:
         if cRoles = ""
          then
           cRoles = comcode.description.
         else
           cRoles = cRoles + "," + comcode.description.   
       end. /* if can-find(cRoleList,comcode.description) */
  end. /* for each comcode */

  ttperson.roles = cRoles .

  if cRoleParam ne "ALL" and not can-do(ttperson.roles,cRoleParam)  
   then
    delete ttperson.
   else
    do:
      assign 
          std-in       = std-in + 1
          ttperson.seq = std-in.  
    end.
end. /* for each ttperson by ttperson.matchPercent desc by ttperson.name by ttperson.personID: */

if std-in > 0 
 then
  pResponse:setParameter("person", table ttperson).

pResponse:success("2005", string(std-in) {&msg-add} "Person"). 



