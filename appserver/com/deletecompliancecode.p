/*------------------------------------------------------------------------
@name deletecompliancecode.p
@action complianceCodeDelete
@description Deletes a comcode record from the database
@param cComCodeID;char;The codeId of the comcode (required)
@throws 3000;The delete failed
@throws 3004;The error message
@success 2008;The comcode was deleted

@author :Rahul Sharma
@version 1.0
@created 08/01/2018
Date          Name       Description
10/29/2018    Gurvindar  Modified file name 'deletecomcode' to 'deletecompliancecode'
04/15/2019    Gurvindar  Updated error log message 
03/16/2020    Shubham    Added code for check orgrole to delete.
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

{lib/std-def.i}

define variable iComCodeID as integer no-undo.

pRequest:getParameter("ComCodeID", iComCodeID).

std-lo = false.

if not can-find(first comcode where comcode.comCodeID = iComCodeID) 
 then
  do:
    pResponse:fault("3083", "ComCode").
    return.
  end. /* if not can-find(first comcode where comcode.comCodeID = iComCodeID)  */

comcode-BLOCK:
for first comCode exclusive-lock
  where comCode.comCodeID = icomCodeID transaction
  on error undo comCode-block, leave comCode-block:

  if can-find (first staterequirement where staterequirement.description   = comCode.description
                                         or staterequirement.role          = comCode.description) or
     can-find (first statereqqual     where statereqqual.qualification     = comCode.description) or
     can-find (first qualification    where qualification.qualification    = comCode.description) or
     can-find (first personrole       where personrole.role                = comCode.description) or
     can-find (first orgrole          where orgrole.source                 = comCode.description) 
   then
    do:
      pResponse:fault("3005", "This Compliance code cannot be deleted as it is in use.").
      return.
    end. /* if can-find (first staterequirement */
 
  delete comCode.
  release comCode.
                                                                                                    
  std-lo = true.
end. /* for first comCode exclusive-lock */

if not std-lo 
 then 
  do:
    pResponse:fault("3000", "Delete").
    return.
  end. /* if not std-lo */

pResponse:success("2008", "System Code").


