/*------------------------------------------------------------------------
@name com/deletepersonpicture.p
@action personPictureDelete
@description Delete Person Picture in DB

@throws 3005;Person picture delete failed

@author Archana 
@version 1.0
@created 06/19/2020
Modification:
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.  
define input parameter pResponse as service.IResponse. 

define variable cPersonID  as character no-undo.

{lib/std-def.i}

pRequest:getParameter("PersonID", output cPersonID).

TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:
  for first person exclusive-lock 
    where person.personID = cPersonID :
    assign 
          person.picture = "". 
  end.
  
  std-lo = true. 
end.
if not std-lo then
do:
  pResponse:fault("3005", "Person picture delete failed").
  return.
end.

pResponse:success("2000", "Person picture delete").
