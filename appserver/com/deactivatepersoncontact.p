/*------------------------------------------------------------------------
@name com/deactivatepersoncontact.p.p
@action personcontactdeactivate
@description modify expdate 
@param ipcpersoncontactID;char;
       ipcExpDate;char;
       ipcNotes;char;
       
@returns 
@success 2005;char;

@author sachin anthwal
@version 1.0
@created 08/30/2022
Modification:
Date          Name         Description
04/04/2024    SRK          Task #111689 Added Validation.
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable ipcpersoncontactID as character no-undo.
define variable ipcExpDate         as date      no-undo.
define variable ipcNotes           as character no-undo.
define variable cEmailDomain       as character no-undo.
define variable iposition          as integer   no-undo.
define variable cEmailExtension    as character no-undo.

{lib/std-def.i}
{lib/com-def.i}
define buffer bfpersoncontact for personcontact.

pRequest:getParameter("personcontactID"  ,output ipcpersoncontactID).
pRequest:getParameter("ExpDate"     ,output ipcExpDate   ).

/* validation when no parameter is supplied */
if (ipcpersoncontactID = ''  or ipcpersoncontactID= ?)
 then 
  pResponse:fault("3001", "personcontactID").
   

/* validate personcontactID */
if not can-find(first personcontact where personcontact.personcontactID = integer(ipcpersoncontactID) ) 
 then 
  do:
    pResponse:fault("3066", "personcontact").
    return.
  end.
  
if ipcExpDate = ? or ipcExpDate > today
 then
  for first personContact where personContact.personcontactID = integer(ipcpersoncontactID) no-lock:
   if personContact.contactType = "E" and personContact.contactSubType = "Work" and can-find(first person where person.personID = personContact.personID and person.internal) 
    then
     do:
       publish "GetSystemParameter" ("emailDomain", output cEmailDomain).
       iposition = index(personContact.contactID, "@",1).
       cEmailExtension = substring(personContact.contactID,iposition,length(personContact.contactID)).
       if cEmailExtension <> cEmailDomain
        then 
         do:
           pResponse:fault("3005","Invalid Email Domain.").
           return.
         end.
     end.
  end.
 
std-lo = false.

TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK: 

  if ipcExpDate = ? or ipcExpDate > today
   then
    for first personcontact no-lock 
      where personcontact.personContactID = integer(ipcpersoncontactID):

      if can-find( first bfpersoncontact where bfpersoncontact.personID = personcontact.personID and bfpersoncontact.personContactID <> personcontact.personContactID 
               and bfpersoncontact.contactType = personcontact.contactType  and bfpersoncontact.contactSubType = personcontact.contactSubType and
                (bfpersoncontact.expirationDate = ? or bfpersoncontact.expirationDate > datetime(today)))
       then
        for each bfpersoncontact where bfpersoncontact.personID = personcontact.personID and bfpersoncontact.personContactID <> personcontact.personContactID 
                   and bfpersoncontact.contactType = personcontact.contactType and bfpersoncontact.contactSubType = personcontact.contactSubType and
                   (bfpersoncontact.expirationDate = ? or bfpersoncontact.expirationDate > datetime(today)) exclusive-lock :
           bfpersoncontact.expirationDate = datetime(today).

           validate bfpersoncontact.
           release  bfpersoncontact.

        end.
    end.

  for first personcontact exclusive-lock 
    where personcontact.personcontactID = integer(ipcpersoncontactID):

    personcontact.expirationDate = date(ipcExpDate).

    validate personcontact.
    release personcontact.

    std-lo = true.
  end.

end. /*do transaction*/

if not std-lo 
 then
  do:
     pResponse:fault("3000", "deactivate").
     return.
  end.

pResponse:success("2012", "PersonContact").
   
