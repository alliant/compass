&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file getdistibutions.p
@description Used to get the distributions

@return Distribution;complex;The distribution temp-table

@author John Oliver
@created 2024.07.02
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

{lib/std-def.i}
{tt/distribution.i &tableAlias="ttDistribution"}

define variable pName as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

pRequest:getParameter("Name", output pName).

if not can-find(first distribution where name = pName)
 then pResponse:fault("3066", "Distribution " + pName).
 
if pResponse:isFault()
 then return.
 
pRequest:getParameter("Distribution", input-output table ttDistribution).

for first ttDistribution no-lock:
  for first distribution exclusive-lock
      where distribution.name = pName:
      
    distribution.description = ttDistribution.description.
  end.
end.
  
/* delete all contacts for distribution */
for each distributioncontact exclusive-lock
   where distributioncontact.name = pName:
   
  delete distributioncontact.
end.

/* create the distribution contacts if there are contacts */
if can-find(first ttDistribution where personContactID > 0)
 then run com/adddistributioncontacts.p (pRequest, pResponse).

pResponse:success("2006", "Distribution").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

