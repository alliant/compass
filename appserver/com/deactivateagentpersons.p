/*------------------------------------------------------------------------
@name com/deactivateagentpersons.p
@action agentpersonsdeactivate
@description modify notes and expdate 
@param ipcPersonAgentID;char;
       ipcExpDate;char;
       ipcNotes;char;
       
@returns 
@success 2005;char;

@author sachin anthwal
@version 1.0
@created 08/23/2022
Modification:
Date          Name               Description
10/06/2023    SK                 Modified Change person status
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable ipcPersonAgentID   as character no-undo.
define variable ipcExpDate         as date      no-undo.
define variable lIsActive          as logical   no-undo.
define variable lPersonActive      as logical   no-undo initial  true.
define variable cMsgLog            as character no-undo.
define variable lsuccess           as logical   no-undo.

define buffer bpersonagent for personagent. 

{lib/std-def.i}
{lib/com-def.i}

pRequest:getParameter("PersonAgentID"  ,output ipcPersonAgentID).
pRequest:getParameter("ExpDate"     ,output ipcExpDate   ).


/* validation when no parameter is supplied */
if (ipcPersonAgentID = ''  or ipcPersonAgentID = ?)
 then 
  do:
    pResponse:fault("3001", "PersonAgentID").
    return.
  end.
  
   

/* validate personagentID */
if not can-find(first personagent where personagent.personagentID = integer(ipcPersonAgentID) ) 
 then 
  do:
    pResponse:fault("3066", "Personagent").
    return.
  end.
  
if ipcExpDate = ?
 then
  for first personagent where personagent.personagentID = integer(ipcPersonAgentID) no-lock:
    if can-find (first person where person.personID = personagent.personID and person.internal) 
     then
      do:
        pResponse:fault("3005" , "Our Employee cannot be actively linked with agent.").
        return.
      end.
  end.

std-lo = false.

TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK: 

  for first personagent exclusive-lock 
    where personagent.PersonAgentID = integer(ipcPersonAgentID):

    personagent.expirationDate = date(ipcExpDate). /* update expiration date */

    /* Set to false for organization check */
    lIsActive = false.
    validate personagent.
    release personagent.

    std-lo = true.

  end.
end.

for first bpersonagent no-lock 
  where bpersonagent.PersonAgentID = integer(ipcPersonAgentID):
   
   if can-find(first affiliation where affiliation.partnerA  eq bpersonagent.personID or affiliation.partnerB  eq bpersonagent.personID) or
      can-find(first personrole where personrole.personID    eq bpersonagent.personID )    or   
      can-find(first personagent where (personagent.personID  eq bpersonagent.personID) and (personagent.expirationDate = ? ))
    then
     lIsActive = yes. 

     if not lIsActive 
      then
       do:
         for first person exclusive-lock 
           where person.personID = bpersonagent.personID: 
            if person.active or person.active = ?
             then
              do:
                person.active = false.
                lPersonActive = false.
                {lib/comlogmsg.i &codeVal = '106' , &Msg = cMsgLog , &stat = 'inactive'}
                run com/newcompliancelog.p(input  {&PersonCode},
                                           input  person.personid,
                                           input  cMsgLog,
                                           input  "",
                                           input  pRequest:Uid,
                                           input  pRequest:actionid,
                                           input  "",
                                           input  "",
                                           input  "",
                                           output lSuccess ).

                if not lSuccess 
                 then
                  do:
                    pResponse:fault("3000", "Comlog").
                    return.
                  end. /* if not lSuccess then  */        
              end. /* if person.active then */                                                        
         end. /* for first person exclusive-lock  */
       end. /* if not lIsActive then */ 
      else
       do:
         for first person exclusive-lock 
           where person.personID = bpersonagent.personID: 
            if not person.active or person.active = ?
             then
              do:
                person.active = true.
                lPersonActive = true.
                {lib/comlogmsg.i &codeVal = '106' , &Msg = cMsgLog , &stat = 'inactive'}
                run com/newcompliancelog.p(input  {&PersonCode},
                                           input  person.personid,
                                           input  cMsgLog,
                                           input  "",
                                           input  pRequest:Uid,
                                           input  pRequest:actionid,
                                           input  "",
                                           input  "",
                                           input  "",
                                           output lSuccess ).
                if not lSuccess 
                 then
                  do:
                    pResponse:fault("3000", "Comlog").
                    return.
                  end. /* if not lSuccess then  */        
              end. /* if person.active then */                                                        
         end. /* for first person exclusive-lock  */
       end. /* if not lIsActive then */ 
 end.

if not std-lo 
 then
  do:
     pResponse:fault("3000", "deactivate").
     return.
  end.

pResponse:setParameter("PersonActive", lPersonActive).

pResponse:success("2012", "PersonAgent").

