/*------------------------------------------------------------------------
@name com/getorganizations.p
@action organizationsGet
@description Returns a temp-table of the Organization records
@returns The organization temp-table
@success 2005;char;The count of the records returned

@author Rahul
@version 1.0
@created 09/13/2019
----------------------------------------------------------------------*/

def input parameter pRequest  as service.IRequest.
def input parameter pResponse as service.IResponse.

def variable pcStatus as character no-undo.

{lib/std-def.i}
{lib/com-def.i}
{tt/organization.i &tableAlias="ttOrganization"}

pRequest:getParameter("status", output pcStatus).

std-in = 0.

if pcStatus = {&both} 
 then
  pcStatus = {&all}. 
if pcStatus = {&ActiveStat} 
 then
  pcStatus = {&ActiveStat}. 
if pcStatus = {&InActiveStat} 
 then
  pcStatus = {&InActiveStat}.
  
for each organization no-lock
  where organization.stat = (if pcStatus =  {&all} then organization.stat else  pcStatus ):   
  create ttOrganization.
  buffer-copy organization to ttOrganization.
  std-in = std-in + 1.
end.

IF std-in > 0
 THEN pResponse:setParameter("organization", table ttOrganization).

pResponse:success("2005", STRING(std-in) {&msg-add} "organization").

