/*------------------------------------------------------------------------
@name com/getrequirementquals.p
@action  requirementQualsGet
@description Returns a table of the statereqqual based on the criteria
@param pcStateID;char;The StateID of staterequirement

@returns The statereqqual dataset
@success 2005;char;The count of the records returned

@author Rahul
@version 1.0
@created 05/28/2018
Modification:
Date          Name           Description
06/19/2018    Rahul Sharma   Only agent specific state requirements are being populated
06/22/2018    Rahul Sharma   Requirements specific to an entity (Agent,Person,Role) are being populated 
07/09/2018    Sachin         Modified to use com-def.i 
11/20/2018    Rahul          Modified to add new parameter pcauthorizedby and change file name
                             from getfirstpartystatereqqual to getrequirementquals.p
10/01/2019    Rahul          Assigned  value to new field reqFor in ttstatereqqual
08/19/2020    VJ             Modified to add logic when staterequirement is made for specific agent
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable pcStateID        as character no-undo.
define variable pcEntity         as character no-undo.
define variable pcauthorizedby   as character no-undo.
define variable iReqID           as integer   no-undo.

{lib/std-def.i}
{lib/com-def.i}
{tt/statereqqual.i  &tableAlias="ttstatereqqual"}

pRequest:getParameter("StateID", output pcStateID).
pRequest:getParameter("Entity",  output pcEntity).
pRequest:getParameter("authorizedby",  output pcauthorizedby).
pRequest:getParameter("RequirementID",  output iReqID).

if pcStateID = "" then
   pcStateID = {&ALL}.


if pResponse:isFault()
 then return.

std-in = 0.

for each staterequirement no-lock
  where staterequirement.stateid        = (if pcStateID = {&ALL} then staterequirement.stateid       else pcStateID)
    and staterequirement.appliesTo      = (if pcEntity  = {&ALL} then staterequirement.appliesTo     else pcEntity)
    and staterequirement.requirementID  = (if iReqID    = 0     then staterequirement.requirementID else iReqID)
    and staterequirement.authorizedby   = pcauthorizedby:

  if iReqID = 0 and staterequirement.refId <> {&ALL}  and staterequirement.refId <> ""  /* this condition lies in case of agent when staterequirement is made for specific agent */
   then
    next.

    for each statereqqual no-lock
      where statereqqual.requirementId = staterequirement.requirementid
        and statereqqual.active        = yes:  
      create ttstatereqqual.
      buffer-copy statereqqual to ttstatereqqual.
      assign ttstatereqqual.description  = staterequirement.description
             ttstatereqqual.reqappliesTo = staterequirement.appliesTo
             ttstatereqqual.role         = staterequirement.role
             ttstatereqqual.reqFor       = staterequirement.reqFor
             .
      std-in = std-in + 1.
    end. 
 end. /* for each staterequirement..*/

if std-in > 0 then
  pResponse:setParameter("statereqqual", table ttstatereqqual).

pResponse:success("2005", string(std-in) {&msg-add} "statereqqual").
