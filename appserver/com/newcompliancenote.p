/*------------------------------------------------------------------------
@name com/newcompliancenote.p   
@action complianceNoteNew
@description Create new comnote

@throws 3005;No XML file attached

@author Rahul Sharma
@version 1.0
@created 08/28/2018
Modification:
Date          Name        Description
09/19/2018    Gurvindar   Modified to add decodefromXML
10/25/2018    Gurvindar   Modified to remove edit comNotes functionality 
                          and change file name from 'comnoteNew' to 'complianceNoteNew'
02/19/2019    Anubha      Replaced XML parsing logic with get parameter
                          method to retrieve temp-table from XML.                           
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable hParser       as handle     no-undo.
define variable iNextSeq      as integer    no-undo.
define variable cNoteDate     as datetime   no-undo.
define variable cNoteType     as character  no-undo.

{tt/comnote.i &tableAlias="ttcomnote"}
{tt/file.i}
{lib/std-def.i}
{lib/com-def.i}  

empty temp-table ttcomnote.
pRequest:getParameter("comnote", input-output table ttcomnote).
        
std-lo = false.

TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:

  iNextSeq = 1.
  
  for first ttcomnote:
    /* New note */
    if ttcomnote.seq eq 0 then
    do:
      for last comnote no-lock
        where comnote.stateID  = ttcomnote.stateID
          and comnote.entity   = ttcomnote.entity
          and comnote.entityID = ttcomnote.entityID
          and comnote.reqID    = ttcomnote.reqID:
        assign iNextSeq = comnote.seq + 1.
      end.

      create comnote.
      buffer-copy ttcomnote except ttcomnote.seq ttcomnote.UID ttcomnote.notetype to comnote.
      assign comnote.seq      = iNextSeq
             comnote.UID      = pRequest:UID
             comnote.notetype = 'U':U
             comnote.noteDate = now
             cNoteDate        = comnote.noteDate
             cNoteType        = comnote.notetype
	     . 

    end. /* if ttcomnote.seq eq 0 then */   
 
    validate comnote.
    release comnote.
    assign std-lo = true.  
             
  end. /* for first ttcomnote*/
end. /* do transaction */ 

if not std-lo
 then
   do: pResponse:fault("3000", "Create").
       return.
   end. 
pResponse:setParameter("comnoteSeq", iNextSeq).
pResponse:setParameter("NoteDate", cNoteDate ).
pResponse:setParameter("NoteType", cNoteType ).

pResponse:success("2012", "comnote").
