/*------------------------------------------------------------------------
@name com/querycompanyfulfillment.p
@action companyFulfillmentQuery
@description Returns a dataset of all the first party qualifications
             depending on search criteria. 

@param pcStateID;char;StateID
@param pcYear;char;Year
@param piRequirementID;int;RequirementID

@returns 
@success 2005;char;The count of the records returned

@author Rahul Sharma
@version 1.0
@created 05/29/2018
Modification:
Date          Name          Description
08/13/2018   Rahul Sharma   Modified to return data for the Company
10/31/2018   Rahul Sharma   Modified file name from getcmpfulfillreport.p 
                            to querycompanyfulfillment.p
01/07/2018   Rahul Sharma   Modified to add qualification.entity check in qualification query                           
08/18/2020   VJ             Modified logic to get all state firstparty qualification
09/01/2020   VJ             Modified logic to get active role dataset
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable pcStateID        as character no-undo.
define variable pcEntity         as character no-undo.
define variable pcYear           as character no-undo.
define variable piRequirementID  as integer   no-undo. 

pRequest:getParameter("StateID",        output  pcStateID).
pRequest:getParameter("Entity",         output  pcEntity).
pRequest:getParameter("Year",           output  pcYear).
pRequest:getParameter("RequirementID",  output  piRequirementID).

{lib/com-def.i}
{lib/std-def.i}
{tt/reqfulfill.i}

std-in = 0.

for first statereqqual no-lock
  where statereqqual.requirementID = piRequirementID
    and statereqqual.active:

  for each qualification no-lock 
    where  qualification.stateID            = (if pcStateID = {&ALL} then qualification.stateID else pcStateID) 
      and  qualification.qualification      = statereqqual.qualification
      and  qualification.entity             = (if pcEntity = "" or pcEntity = {&ALL} then qualification.entity else pcEntity)                                                
      and  qualification.authorizedBy       = {&CompanyCode}
      and year(qualification.effectiveDate) = (if pcYear = {&ALL} then year(qualification.effectiveDate) else integer(pcYear)):

    /* if orgrole is inactive then do not create record */  
    if qualification.entity = {&OrganizationCode} and (not can-find(first orgrole where orgrole.orgID = qualification.entityID and orgrole.active))
      then
       next.
    /* if personrole is inactive then do not create record */
    if qualification.entity = {&PersonCode} and (not can-find(first personrole where personrole.personID = qualification.entityID and personrole.active))
     then
      next.
    
    create reqfulfill.      
    assign reqfulfill.entityID            = qualification.entityID
           reqfulfill.StateId             = qualification.stateID
           reqfulfill.entity              = qualification.entity
           reqfulfill.requirementID       = statereqqual.requirementID
           reqfulfill.qualificationID     = qualification.qualificationID
           reqfulfill.qualificationNumber = qualification.qualificationNumber
           reqfulfill.qualification       = qualification.qualification
           reqfulfill.activ               = qualification.active
           reqfulfill.stat                = qualification.stat
           reqfulfill.notes               = qualification.notes        
           reqfulfill.effectiveDate       = qualification.effectiveDate
           reqfulfill.expirationDate      = qualification.expirationDate
           std-in                         = std-in + 1.

   if qualification.entity = {&OrganizationCode} 
    then
     for first organization no-lock
       where organization.orgID = qualification.entityID:
       reqfulfill.entityName = organization.name.
     end. /* for first organization no-lock */
   else if qualification.entity = {&PersonCode} 
    then
     for first person no-lock
       where person.personID = qualification.entityID:
       assign 
           reqfulfill.entityNPN  = person.NPN
           reqfulfill.entityName = person.dispname
           .
     end. /* for first person no-lock */
  end. /* for each qualification...*/
end. /* for first statereqqual...*/
   
if std-in > 0 
 then 
  pResponse:setParameter("reqfulfill", table reqfulfill).

pResponse:success("2005", string(std-in) {&msg-add} "reqfulfill").
