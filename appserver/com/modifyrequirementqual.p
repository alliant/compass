/*------------------------------------------------------------------------
@name modifyrequirementqual.p
@action requirementQualModify
@description Modify statereqqual record

@throws 3005;No XML file attached

@author Rahul Sharma
@version 1.0
@created 10/23/18
Modification:
Date          Name           Description 
10/23/18      Rahul          File renamed from modifystatereqqual.p to 
                             modifyrequirementqual.p
12/04/2018    Gurvindar      Modified to return success message
02/19/2019    Anubha         Replaced XML parsing logic with get parameter
                             method to retrieve temp-table from XML. 
04/09/2019    Gurvindar      Updated the error log message 
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable hParser           as handle     no-undo.
define variable iActionID         as integer    no-undo.
define variable iseq              as integer    no-undo.

{tt/statereqqual.i &tableAlias="ttstatereqqual"}

{tt/file.i}
{lib/std-def.i}

empty temp-table ttstatereqqual.
pRequest:getParameter("statereqqual", input-output table ttstatereqqual).
        
std-lo = false.

for first ttstatereqqual:
  if not can-find(first statereqqual where statereqqual.statereqqualId = ttstatereqqual.statereqqualId) or ttstatereqqual.statereqqualID = 0 then
  do:
    pResponse:fault("3005", "StateReqQual").
    return.
  end.

  if can-find(first statereqqual where statereqqual.requirementID    eq ttstatereqqual.requirementID
                                   and statereqqual.qualification    eq ttstatereqqual.qualification
                                   and statereqqual.appliesTo        eq ttstatereqqual.appliesTo
                                   and statereqqual.statereqqualID   ne ttstatereqqual.statereqqualId) then
  do:
    pResponse:fault("3005", "This State Requirement Qualification record already exists.").
    return.
  end.
end.


TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:

  for first ttstatereqqual:

    for first statereqqual exclusive-lock
      where statereqqual.statereqqualID = ttstatereqqual.statereqqualID:

      buffer-copy ttstatereqqual except ttstatereqqual.statereqqualID to statereqqual.
      validate statereqqual.
    end.

    release statereqqual.
    std-lo = true.
  end.
end.
if not std-lo
 then
  do:
    pResponse:fault("3000", "Modify").
    return.
  end.

pResponse:success("2012", "StateReqQual").
