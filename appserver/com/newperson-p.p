/*------------------------------------------------------------------------
@name com/newperson-p.p
@action 
@description Create new person
@author Archana Gupta
@version 1.0
@created 05/14/2024
Modification:
Date          Name          Description
----------------------------------------------------------------------*/
    
/* Temp-table Definitions */
{tt/person.i &tableAlias="ttPerson"}

define input  parameter table for ttperson.
define input  parameter cUID      as character no-undo.
define output parameter iPersonID as integer   no-undo.
define output parameter lError    as logical   no-undo.
define output parameter cErrMsg   as character no-undo.

define variable cMobileNumber           as character no-undo.
define variable cPhoneNumber            as character no-undo.
define variable cEmailDomain            as character no-undo.
define variable iposition               as integer   no-undo.
define variable cEmailExtension         as character no-undo.
define variable cQuery                  as character no-undo.
define variable cAgentName              as character no-undo.
define variable cobjKeyList             as character no-undo.
define variable lsuccess                as logical   no-undo. 
define variable icount                  as integer   no-undo.
define variable cFirstName              as character no-undo.
define variable cMiddleName             as character no-undo.
define variable cLastName               as character no-undo.

define variable pRequest  as service.IRequest.
define variable pResponse as service.IResponse.

/* Standard lib files */
{lib/com-def.i}
{lib/std-def.i}
{lib/validEmailAddr.i}
{lib/nextkey-def.i}


/* Execute validation for person creation */
for first ttPerson no-lock: 
 /* check ttperson is new or not */
 if ttperson.personid = '0' or ttperson.personid = ? or ttperson.personid = ''  or ttperson.personid = "-1"
  then 
   do:
     ttperson.personid = ''.

     if (ttperson.dispname = "" or ttperson.dispname = ? )
      then
       do:
         lError  = true.
         cErrMsg = "Name is required.".
         return.
       end.

     if ((ttPerson.contactEmail = "" or ttPerson.contactEmail = ?) and (ttPerson.contactMobile = "" or ttPerson.contactMobile = ?)) 
      then
       do:
         lError  = true.
         cErrMsg = "Email or Mobile is required.".
         return.
       end.

     if (ttPerson.contactMobile <> "" and ttPerson.contactMobile <> ?) and (ttPerson.contactPhone = ttPerson.contactMobile) 
      then
       do:
         lError  = true.
         cErrMsg = "Both Phone and Mobile cannot be same.".
         return.
       end.

     if (ttPerson.contactEmail <> "" and ttPerson.contactEmail <> ?)
        and not validEmailAddr(ttPerson.contactEmail)
      then
       do:
         lError  = true.
         cErrMsg = "Invalid Email Address.".
         return.
       end.
          
     if (ttPerson.contactPhone <> "" and ttPerson.contactPhone <> ?)
      then
       do:
         run util/validatecontactnumber-p.p(input ttPerson.contactPhone,output cPhoneNumber,output lError,output cerrmsg).
         if lError then return.
       end.

     if (ttPerson.contactMobile <> "" and ttPerson.contactMobile <> ?)
      then
       do:
         run util/validatecontactnumber-p.p(input ttPerson.contactMobile,output cMobileNumber,output lError,output cerrmsg).
         if lError then return.
       end.

     if (cPhoneNumber <> "" and cPhoneNumber <> ?) and can-find(first personcontact where personcontact.contactID = cPhoneNumber and personcontact.contactSubType = "Mobile") 
      then
       do:
         lError  = true.
         cErrMsg = "Duplicate Phone.".
         return.
       end. 

     if (cMobileNumber <> "" and cMobileNumber <> ?) and can-find(first personcontact where personcontact.contactID = cMobileNumber)
      then
       do:
         lError  = true.
         cErrMsg = "Duplicate Mobile.".
         return.
       end.

     if (ttPerson.contactEmail <> "" and ttPerson.contactEmail <> ?) and can-find(first personcontact where personcontact.contactID = ttPerson.contactEmail)
      then
       do:
         lError  = true.
         cErrMsg = "Duplicate Email.".
         return.
       end. 


     if ttperson.internal and ttPerson.contactEmail ne "" then
     do:
       publish "GetSystemParameter" ("emailDomain", output cEmailDomain).
   
       iposition = index(ttPerson.contactEmail, "@",1).
   
       cEmailExtension = substring(ttPerson.contactEmail,iposition,length(ttPerson.contactEmail)).
   
       if cEmailExtension <> cEmailDomain
        then 
         do:
           lError  = true.
           cErrMsg = "Invalid Email Domain.".
           return.
         end.
     end.
  end.
end.

TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:  
  for first ttPerson: 
    
    run createperson(output lError ).

    if lError
     then
      undo TRX-BLOCK, leave TRX-BLOCK.

    /*Creating contacts for new person created*/
    if ttperson.contactmobile <> '' and ttperson.contactmobile <> ?
     then
      run createContact ('P','Mobile',cMobileNumber,output lError).

    if lError
     then
      undo TRX-BLOCK, leave TRX-BLOCK.

    if ttperson.contactphone <> '' and ttperson.contactphone <> ?
     then
      do: 
        if ttperson.phExtension <> '' and ttperson.phExtension <> ?
         then
          cPhoneNumber = cPhoneNumber + ',' + ttperson.phExtension.

         run createContact ('P','Work',cPhoneNumber,output lError).
      end.
    if lError
     then
      undo TRX-BLOCK, leave TRX-BLOCK.

    if ttperson.contactemail <> '' and ttperson.contactemail <> ?
     then
      run createContact ('E','Work',ttperson.contactemail,output lError).

    if lError
     then
      undo TRX-BLOCK, leave TRX-BLOCK. 
  end.  /* for first ttPerson */
  std-lo = true.
end.
  
if not std-lo 
 then
  do:
    lError = true. 
    cErrMsg = "New Person creation failed.".
    return.
  end.

for first ttPerson:
  /* Build keyword index to use for searching */
  cQuery = "for each person where person.personid = '" + string(iPersonID) + "'".
  run sys/buildsysindex.p (input pRequest,
                           input pResponse,
                           input {&Person},           /* ObjType */
                           input "",                  /* ObjAttribute */
                           input string(iPersonID),   /* ObjID */
                           input {&Person},           /* SourceTable */
                           input cQuery,              /* SourceQuery */
                           input "").                 /* ExcludeFieldList */

  cobjKeyList = trim((if (ttperson.firstname  ne "" and ttperson.firstname  ne ?) then (ttperson.firstname)        else "") + 
                     (if (ttperson.middlename ne "" and ttperson.middlename ne ?) then ("|" + ttperson.middlename) else "") + 
                     (if (ttperson.lastname   ne "" and ttperson.lastname   ne ?) then ("|" + ttperson.lastname)   else "") +
                     (if (ttperson.dispname   ne "" and ttperson.dispname   ne ?) then ("|" + ttperson.dispname)   else "")
                    ).

  /* create sysindex for person name */
  if cobjKeyList ne ''  
   then
    /* Build keyword index to use for searching */
     run sys/createsysindex.p (input "PersonName",      /* ObjType */
                               input "",                /* ObjAttribute*/
                               input string(iPersonID), /* ObjID */
                               input cobjKeyList,       /* ObjKeyList*/
                               output lsuccess,
                               output cerrmsg).
    /* END of if pobjKeyList ne '' */ 

  if not lsuccess /* if sysindex fails put message in appserver file */
   then
    message cerrmsg.
end.

procedure createContact :
  define input parameter ipcContactType     as character no-undo.
  define input parameter ipcConstactSubType as character no-undo.
  define input parameter ipcContactID       as character no-undo.
  define output parameter oplError          as logical   no-undo.

  define variable iPersonContactID as integer no-undo.

  oplError = false.

  {lib/nextkey.i &type='personContact' &var=iPersonContactID  &noresponse=true &err= "undo , leave." }

  if iPersonContactID = 0 or iPersonContactID = ?
   then
    do:
      oplError = true.
      return.
    end.

  create personContact.
  assign   
      personContact.personcontactID  = iPersonContactID
      personContact.personID         = string(iPersonID)
      personContact.contactType      = ipcContactType
      personcontact.contactSubType   = ipcConstactSubType
      personContact.contactID        = ipcContactID
      personContact.uid              = cUID
      personContact.createDate       = today
      .

  validate personContact.
  release personContact. 

  lerror = false.
end procedure.


procedure createperson :

 define output parameter lerror    as logical   no-undo.

 define variable iEntries          as integer     no-undo.

 {lib/nextkey.i &type='person' &var=iPersonID &noresponse=true  &err= "undo , leave." }

 if iPersonID = 0 or iPersonID = ?
  then
   do:
     lerror = true.
     return.
   end.

 for first ttperson no-lock:
   iEntries = num-entries(ttperson.dispname," "). 
   do icount = 1 to iEntries:
    if icount = 1
     then
      cFirstName = ENTRY(icount,ttperson.dispname," ").
     else if icount = 2 and iEntries >= 3
      then
       cMiddleName = ENTRY(icount,ttperson.dispname," ").
      else
       cLastName = cLastName + " " +  ENTRY(icount,ttperson.dispname," ").
   end.

   create Person.
   assign
       person.personid      = string(ipersonId)
       person.NPN           = ttperson.NPN
       person.altaUID       = ttperson.altaUID
       person.address1      = ttperson.address1
       person.address2      = ttperson.address2
       person.city          = ttperson.city
       person.state         = ttperson.state
       person.zip           = ttperson.zip
       person.active        = false
       person.notes         = ttperson.notes
       person.firstName     = cFirstName
       person.middleName    = cMiddleName
       person.lastName      = cLastName
       person.dispName      = ttperson.dispName
       person.internal      = ttperson.internal
       person.doNotcall     = ttperson.doNotcall
       .

   validate person.
   release person.

   lerror = false.

 end.
end procedure.

