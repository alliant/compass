/*------------------------------------------------------------------------
@name com/modifyattorney.p
@action attorneyModify
@description Update attorney record

@throws 3005;No XML file attached

@author Rahul
@version 1.0
@created 10/23/2018
Modification:
01/29/19    Anjly      Added comStatus output parameter
                       and removed attorney table as output param.
02/19/19    Anubha     Replaced XML parsing logic with get parameter
                       method to retrieve temp-table from XML.
04/09/2019  Gurvindar  Updated the error log message  
12/17/2019  Shubham    Modified the code for new Organization and Person Requirement.
03/17/2022  Shefali    Task# 86699 Modified to modify attorney.  
04/26/2022  Shefali    Task #93684 Modified to correct the validation.  
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable hParser          as handle    no-undo.
define variable cQuery           as character no-undo.
define variable plSuccess        as logical   no-undo.
define variable lisActive        as logical   no-undo.
define variable cnotes           as character no-undo.
define variable cMsgLog          as character no-undo.
define variable opcPersonID      as character no-undo.
define variable opcOrgID         as character no-undo.

define buffer bfattorney for attorney.

{tt/attorney.i &tableAlias="ttAttorney"}

{tt/file.i}
{lib/std-def.i}
{lib/com-def.i}

empty temp-table ttattorney.
pRequest:getParameter("attorney", input-output table ttattorney).
    
std-lo = false.

for first ttattorney:
  if not can-find(first attorney where attorney.attorneyId = ttattorney.attorneyId) or ttattorney.attorneyID = "" then
  do:
    pResponse:fault("3083", "Attorney").
    return.
  end.
  if ttattorney.orgid <> "" and ttattorney.orgid <> ? 
   then
    do:
      if can-find(first attorney where attorney.attorneyId ne ttattorney.attorneyId 
                                   and attorney.orgid   = ttattorney.orgid
                                   and attorney.stateid = ttattorney.stateid) 
       then
        do:
          pResponse:fault("3005", "Organization is already attorney for this state.").
          return.
        end.  /* if can-find(first attorney */
    end. /* if ttattorney.orgid ne "" */
  else 
   if ttattorney.personid <> "" and ttattorney.personid <> ? 
    then
     do:
       if can-find(first attorney where attorney.attorneyId ne ttattorney.attorneyId 
                                   and  attorney.personid = ttattorney.personid
                                    and attorney.stateid  = ttattorney.stateid) 
        then
         do:
           pResponse:fault("3005", "Person is already attorney for this state.").
           return.
         end.  /* if can-find(first attorney */
     end. /* if ttattorney.personid ne "" */
end.

TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:    
  for first ttattorney:
    for first attorney exclusive-lock
      where attorney.attorneyID = ttattorney.attorneyID:
          
      if attorney.orgID <> ttattorney.orgID 
       then
         run setAttorneyOrganization in this-procedure.
      else if attorney.personID <> ttattorney.personID 
       then
        run setAttorneyPerson in this-procedure.

      buffer-copy ttattorney to attorney.    
      
      validate attorney.
      
      run arc/sendattorney.p (attorney.attorneyID, output std-lo, output std-ch).
      if not std-lo
       then pResponse:addFault("3005", std-ch).
      
      
    end. /* for first attorney exclusive-lock */   

    release attorney.

    /* Build keyword index to use for searching */
    cQuery = "for each attorney where attorney.attorneyID = '" + ttattorney.attorneyID + "'".
    run sys/buildsysindex.p (input pRequest,
                             input pResponse,
                             input {&Attorney},            /* ObjType      */
                             input "",                     /* ObjAttribute */
                             input ttattorney.attorneyID , /* ObjID        */
                             input {&Attorney},            /* SourceTable  */
                             input cQuery,                 /* SourceQuery */
                             input "").                    /* ExcludeFieldList */
    std-lo = true.
  end.  
end.
if not std-lo
 then
  do: 
    pResponse:fault("3000", "Modify").
    return.
  end.

pResponse:success("2012", "Attorney").

if opcPersonID <> "" or opcPersonID <> ?
 then
  do:
    pResponse:setParameter("personID", opcPersonID).
  end. 

if opcOrgID <> "" or opcOrgID <> ?
 then
  do:
    pResponse:setParameter("orgID", opcOrgID).
  end.  
 
PROCEDURE setAttorneyOrganization:
  RUN com\setattorneyOrganization.p(input pRequest,
                                    input pResponse,
                                    input "Modify",
                                    input ttattorney.attorneyID,
                                    input table ttAttorney,
                                    output opcOrgID).
 
  if ttAttorney.OrgID = "-99" 
   then
    do:
      attorney.OrgID   = opcOrgID.
      ttAttorney.OrgID = opcOrgID.
    end.
  if pResponse:isFault()
   then 
    return.
END.  /* PROCEDURE setAttorneyOrganization: */
 
PROCEDURE setAttorneyPerson:
  RUN com\setattorneyperson.p(input pRequest,
                              input pResponse,
                              input "Modify",
                              input ttattorney.attorneyID,
                              input table ttAttorney,
                              output opcPersonID).
 
  if ttAttorney.personID = "-99" 
   then
    do:
      attorney.personID   = opcPersonID.
      ttAttorney.personID = opcPersonID.
    end.
  if pResponse:isFault()
   then 
    return.
END.  /* PROCEDURE setAttorneyPerson: */
