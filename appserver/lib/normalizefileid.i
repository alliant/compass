/*------------------------------------------------------------------------
    File        : normalizeFileID.i
    Purpose     : Standardize the FileID value to remove non-alphanumeric characters                  

    Syntax      :

    Description : &method: non-blank to define as method (default to define as function)

    Author(s)   : Vikas Jain
    Created     : 09/03/2019
    Notes       :
  ----------------------------------------------------------------------*/

&if defined(method) = 0
&then
function normalizeFileID returns character
  ( pOriginalFileNumber as character ):
&else
method public character normalizeFileID
  ( pOriginalFileNumber as character):
&endif
  
  define variable tValidFileNumberChars as character no-undo init "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".
  define variable tIndex                as integer   no-undo.
  define variable tNormalizedFileID     as character no-undo init "".
  
  pOriginalFileNumber = trim(pOriginalFileNumber).
  
  do tIndex = 1 to length(pOriginalFileNumber):
    if index(tValidFileNumberChars, substring(pOriginalFileNumber,tIndex,1)) > 0 
    then tNormalizedFileID = tNormalizedFileID + substring(pOriginalFileNumber,tIndex,1).
  end.
 
  return upper(tNormalizedFileID).
 
&if defined(method) = 0
&then
end function.
&else
end method.
&endif
