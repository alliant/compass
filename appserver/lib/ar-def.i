/*------------------------------------------------------------------------
File        : lib/ar-def.i
Purpose     :

Syntax      :

Description :

Author(s)   : Vikas
Created     : 08/13/2019
Notes       :

Modification:                             
MK	03/10/2021	Added "RefundBatch" as "RB". 
SA  06/02/2021  Added VcR,VPR and PR related to refund 
SB  07/02/2021  Added W,PW,VW related to write-off"                        
----------------------------------------------------------------------*/
&if defined(ar-def-defined) = 0 &then

&global-define UnrepCPLInRepFiles                    "1"
&global-define CPLForUnrepFiles                      "2"
&global-define CPLCountDiscrepancyForRepFiles        "3"
&global-define CPLValueDiscrepancyForRepFiles        "4"

/*-------------Transaction Types--------------*/
&global-define Payment           "P"  
&global-define Invoice           "I"
&global-define Credit            "C"
&global-define Debit             "D"
&global-define File              "F"
&global-define production        "P"
&global-define Apply             "A"
&global-define Reprocess         "R"
&global-define Deposit           "D"
&global-define ArBatch           "B"
&global-define Refund            "RF"  
&global-define RefundBatch       "RB"
&global-define Writeoff          "W" 
&global-define productionInvoice "I"  

/*------------Entity-------------------*/
&global-define agent          "A"
&global-define organization   "O"
&global-define InvBatch       "B"
&global-define AgentFile      "AF"
&global-define person         "P"

/*-----------Misc Source Types-------------*/
&global-define Appointment        "A"
&global-define CPL                "CPL"
&global-define Training           "T"
&global-define Search             "S"
&global-define Sponsorship        "N"
&global-define Misc               "M"
&global-define HistoryTransaction "HT"

/*-----------Trans Types-------------*/
&global-define None           " "

/*-------Add/Modify functionality-----------*/
&global-define Save           "Save"
&global-define Create         "Create"
&global-define Add            "N"
&global-define Modify         "M"
&global-define Copy           "C"
&global-define View           "V"
&global-define ModifyPosted   "MP"

/*-----------Combo box-------------------*/
&global-define all            "ALL" 
&global-define SelectCombo    "----Select----" 
&global-define NatureOfAffiliation    "NatureOfAffiliation" 

/*-------------General--------------*/
&global-define NotApplicable  "N/A"

/*------------duedate--------------------*/
&global-define Invdue        "Invdue"
&global-define Invdueindays  "Invdueindays"
&global-define Invdueonday   "Invdueonday"

/*-------------user message--------------*/
&global-define DuplicateCheck              "DuplicateCheck"
&global-define RemoveDistribution          "RemoveDistribution"
&global-define ResultNotMatch              "Results may not match current parameters."
&global-define ResultNotMatchSearchString  "Results may not match current search string."

/*------------Agent File status----------*/
&global-define Open        "O"
&global-define Closed      "C"

/*------------Deposit status----------*/
&global-define Posted      "P"

/*------------Mandatory Field symbol----------*/
&global-define Mandatory        "*"

/*------------Printer setting----------*/
&global-define printappcode        "ALL"
&global-define printobjAction      "Print"
&global-define printProperty       "Device"

/*------------Output Destination Type----------*/
&global-define printer        "P"
&global-define email          "E"
&global-define agentdefault   "A"

/*------------Output Format----------*/
&global-define CSV        "C"
&global-define PDF        "P"

/*-----------Output Action----------*/
&global-define Queue       "Q"
&global-define Summary     "S"
&global-define Detail      "D"

/*---------- Status ----------*/
&global-define Active     "Active"
&global-define InActive   "Inactive"

/*--------GL actions-----------*/
&global-define PostPayment           "PP"
&global-define VoidPayment           "VP"
&global-define PostDeposit           "PD"
&global-define VoidDeposit           "VD"
&global-define VoidRefund            "VR"
&global-define PostProductionInvoice "I"
&global-define VoidproductionInvoice "VPI"
&global-define PostCredit            "PC"
&global-define VoidCredit            "VC"
&global-define PostBatch             "PB"
&global-define PostBatchDetail       "PBD"
&global-define PostMiscInvoice       "PMI"
&global-define VoidMiscInvoice       "VMI"
&global-define PostFile              "PF"
&global-define PostMisc              "PM"
&global-define RefundPayment         "RP"
&global-define RefundCredit          "RC"
&global-define VoidPaymentRefund     "VPR"
&global-define VoidCreditRefund      "VCR"
&global-define PostRefund            "PR"
&global-define PostWriteOff          "PW" 
&global-define VoidWriteOff          "VW" 

/*-----------Locking Entities -------------*/
&global-define ArAgent          "Agent"
&global-define ArCredit         "ArCredit"
&global-define ArPayment        "ArPayment"

/*-------------Post Locking------------- */
&global-define ArPosting       "arPosting"
&global-define LockIterations  "lockiterations"
&global-define PauseTime       "pauseTime"

&global-define ar-def-defined true

&endif

 
