/* lib/canfindagentdoc.i
   9.19.2014 D.Sinclair
   @param First unnamed parameter should be the AgentID
 */
can-find(first sysdoc
           where sysdoc.entityType = {1}
             and sysdoc.entityID = {2})
