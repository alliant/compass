&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/comlogmsg.i
Purpose     :

Syntax      :

Description :

Author(s)   : Sachin Chautrvedi
Created     : 06-22-2018
Notes       : &qual      = qualificatiobn name
              &qualID    = qualificatiobn ID
              &req       = requirement name
              &reqId     = requirement ID
              &stat      = status
              &role      = Role of person
              &ageniId   = AgentID
              &personID  = PersonID
              &role      = Role of person or organization
              &roleID    = RoleID of person or organization
              &orgID     = OrganizationID

Modification:
Date          Name            Description
07/10/18      Rahul           Add new preprocessor cName
08/16/18      Rahul           Add log message for company.
09/18/19      Gurvindar       Add log messages for organization.
10/25/19      Gurvindar       Add log messages 316.
12/17/19      Gurvindar       Add log messages for person and organization.
12/23/19      Shubham         Add log messages for person and organization.
/----------------------------------------------------------------------*/
&if  defined(stat) = 0 &THEN
&scoped-define stat ""
&endif
&if defined(req) = 0 &THEN
&scoped-define req ""
&endif
&if defined(reqID) = 0 &THEN
&scoped-define reqID ""
&endif
&if defined(qual) = 0 &THEN
&scoped-define qual ""
&endif
&if defined(qualID) = 0 &THEN
&scoped-define qualID ""
&endif

&if defined(agentID) = 0 &THEN
&scoped-define agentID ""
&endif
&if defined(personID) = 0 &THEN
&scoped-define personID ""
&endif

&if defined(cName) = 0 &THEN
&scoped-define cName ""
&endif

&if defined(role) = 0 &THEN
&scoped-define role ""
&endif

&if defined(roleID) = 0 &THEN
&scoped-define roleID ""
&endif

&if defined(entity) = 0 &THEN
&scoped-define entity ""
&endif

&if defined(orgID) = 0 &THEN
&scoped-define orgID ""                                                                            
&endif


case {&codeVal}:  
  when "100"  then {&Msg} = "State Requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'" + " is active".
  when "101"  then {&Msg} = "State Requirement " +  "'" + {&req} + " " + "(" + {&reqID} + ")"  + " / qualification " + {&qual} + "'"  + " is inactive".
  when "102"  then {&Msg} = "Compliance for " + {&entity}  + " Role '" + {&role} + "' and RoleID '" + {&roleID} + "' is now tracked".
  when "103"  then {&Msg} = "Compliance for " + {&entity}  + " Role '" + {&role} + "' and RoleID '" + {&roleID} + "' is no longer tracked".
  when "104"  then {&Msg} = {&cName} + " " + "(" + {&agentID} + ")" +  " is fully qualified due to no requirements".
  when "105"  then {&Msg} = "Compliance is now tracked due to status change to Active".
  when "106"  then {&Msg} = "Compliance is no longer tracked due to a status change to " + "'" + {&stat} + "'".
  when "107"  then {&Msg} = "Assigned to role "   + "'" + {&role} + " " + "(" + {&roleID} + ")" + "'" .
  when "108"  then {&Msg} = "Unassigned from role " + "'" + {&role} + " " + "(" + {&roleID} + ")" + "'" .
  
  when "200"  then {&Msg} = "Unaffiliated "  + {&cName} + " " + "(" + {&agentID} + ")".
  when "201"  then {&Msg} = "Linked agent qualification " + "'" + {&qual} + " " + "(" + "'" + {&qualID} + ")" + "'" + " to the requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'".

  when "202"  then {&Msg} = "Unlinked agent qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " from the requirement " + "'" + {&req} + " " + "(" + "'" + {&reqID} + ")" + "'".
  when "203"  then {&Msg} = "Agent qualification " + "'" + {&qual} + " " + "(" + {&qualId} + ")" + "'" +  " is inactive".
  when "204"  then {&Msg} = "Agent qualification " + "'" + {&qual} + " " + "(" + "'" + {&qualId} + ")" + "'" +  " status change to " + "'" + {&Stat} + "'".

  when "300"  then {&Msg} = "Assigned to role " + "'" + {&role} + "'".
  when "301"  then {&Msg} = "Unassigned from role" + "'" + {&role} + "'" .
  when "302"  then {&Msg} = "Unaffiliated " + "'" + {&cName} + " " + "(" + {&personID} + ")" + "'".
  when "303"  then {&Msg} = "Linked person qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'"  + " to the requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'".
  when "304"  then {&Msg} = "Linked person qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'"  + " to agent requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'".
  when "305"  then {&Msg} = "Linked person qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " to attorney requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'" .
  when "306"  then {&Msg} = "Linked person qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'"  + " to role requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'".
  when "307"  then {&Msg} = "Unlinked person qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " from the requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'".
  when "308"  then {&Msg} = "Unlinked person qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " from agent requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'".
  when "309"  then {&Msg} = "Unlinked person qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " from attorney requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'" .
  when "310"  then {&Msg} = "Unlinked person qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'"  + " from role requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'" .
  when "311"  then {&Msg} = "Person qualification " + "'" + {&qual} + " " + "(" + {&qualId} + ") " + "'" +  " is inactive".                                                                        
  when "312"  then {&Msg} = "Person qualification " + "'" + {&qual} + " " + "(" + {&qualId} + ") " + "'" +  " status change to " + "'" + {&Stat} + "'".
  when "313"  then {&Msg} = "Unlinked person qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " from company requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'".
  when "314"  then {&Msg} = "Linked person qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'"  + " to company requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'".
 
  when "315"  then {&Msg} = "Linked person qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " to the requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'" + " of organization's role " + "'" + {&role} + " " + "(" + {&roleID} + ")" + "'".
  when "316"  then {&Msg} = "Linked person qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " to the requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'" + " of person's role "       + "'" + {&role} + " " + "(" + {&roleID} + ")" + "'".
  when "317"  then {&Msg} = "Unlinked person qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " from the requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'" + " of organization's role " + "'" + {&role} + " " + "(" + {&roleID} + ")" + "'".
  when "318"  then {&Msg} = "Unlinked person qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " from the requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'" + " of person's role "       + "'" + {&role} + " " + "(" + {&roleID} + ")" + "'".
  when "319"  then {&Msg} = "Linked person qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'"   + " to the requirement "       + "'" + {&req}  + " " + "(" + {&reqID} + ")"  + "'".
  when "320"  then {&Msg} = "Unlinked person qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " from the requirement "     + "'" + {&req}  + " " + "(" + {&reqID} + ")"  + "'".
  when "321"  then {&Msg} = "Person qualification "          + "'" + {&qual} + " " + "(" + {&qualId} + ")" + "'" +  " is inactive".                                                                        
  when "322"  then {&Msg} = "Affiliated to organization   " + "'"  + {&orgID} + "'".
  when "323"  then {&Msg} = "Unaffiliated from organization " + "'"  + {&orgID} + "'".
  when "324"  then {&Msg} = "Person qualification " + "'" + {&qual} + " " + "(" + "'" + {&qualId} + ")" + "'" +  " status change to " + "'" + {&Stat} + "'".
  
  when "401"  then {&Msg} = "Linked attorney qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'"  + " to requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'".
  when "402"  then {&Msg} = "Unlinked attorney qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " from requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'".
  when "403"  then {&Msg} = "Attorney qualification " + "'" + {&qual} + " " + "(" + {&qualId} + ") " + "'" +  " is inactive".
  when "404"  then {&Msg} = "Attorney qualification " + "'" + {&qual} + " " + "(" + {&qualId} + ") "  + "'" +  " status change to " + "'" + {&Stat} + "'". 

  when "501"  then {&Msg} = "Linked company qualification " + "'" + {&qual} + " " + "(" + "'" + {&qualID} + ")" + "'" + " to requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'".
  when "502"  then {&Msg} = "Unlinked company qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " from requirement " + "'" + {&req} + " " + "(" + "'" + {&reqID} + ")" + "'".
  when "503"  then {&Msg} = "Company qualification " + "'" + {&qual} + " " + "(" + {&qualId} + ")" + "'" +  " is inactive".
  when "504"  then {&Msg} = "Company qualification " + "'" + {&qual} + " " + "(" + "'" + {&qualId} + ")" + "'" +  " status change to " + "'" + {&Stat} + "'".
  when "505"  then {&Msg} = {&cName} + " is fully qualified due to no requirements".
 
  when "601"  then {&Msg} = "Linked organization qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " to the requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'" + " of organization's role " + "'" + {&role} + " " + "(" + {&roleID} + ")" + "'".
  when "602"  then {&Msg} = "Linked organization qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " to the requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'" + " of person's role "       + "'" + {&role} + " " + "(" + {&roleID} + ")" + "'".
  when "603"  then {&Msg} = "Unlinked organization qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " from the requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'" + " of organization's role " + "'" + {&role} + " " + "(" + {&roleID} + ")" + "'".
  when "604"  then {&Msg} = "Unlinked organization qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " from the requirement " + "'" + {&req} + " " + "(" + {&reqID} + ")" + "'" + " of person's role "       + "'" + {&role} + " " + "(" + {&roleID} + ")" + "'".
  when "605"  then {&Msg} = "Linked organization qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " to the requirement " + "'" + {&req} + " "  + "(" + {&reqID} + ")" + "'".
  when "606"  then {&Msg} = "Unlinked organization qualification " + "'" + {&qual} + " " + "(" + {&qualID} + ")" + "'" + " from the requirement " + "'" + {&req} + " "  + "(" + {&reqID} + ")" + "'".
  when "607"  then {&Msg} = "Organization qualification "          + "'" + {&qual} + " " + "(" + {&qualId} + ")" + "'" + " is inactive".
  when "608"  then {&Msg} = "Person ID " + "'" + {&personID} + "'" + " is affiliated ".
  when "609"  then {&Msg} = "Person ID " + "'" + {&personID} + "'" + " is unaffiliated ".
  when "610"  then {&Msg} = "Organization qualification " + "'" + {&qual} + " " + "(" + "'" + {&qualId} + ")" + "'" +  " status change to " + "'" + {&Stat} + "'".
  
end case.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 2.29
         WIDTH              = 58.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


