/* lib/canfindstatedoc.i
   9.19.2014 D.Sinclair
   @param First unnamed parameter should be the StateID
 */
can-find(first sysdoc
           where sysdoc.entityType = "State"
             and sysdoc.entityID = {1}
             and sysdoc.entitySeq = 0)
