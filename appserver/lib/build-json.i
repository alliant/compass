&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file build-json.i
@description Builds a JSON file from a temp table
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

&if defined(JSONFile) = 0 &then
&scoped-define JSONFile "build-json.json"
&endif

&if defined(doNull) = 0 &then
&scoped-define doNull false
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD EpochDate Include 
FUNCTION EpochDate RETURNS CHARACTER
  ( input pDate as datetime )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD MakeNull Include 
FUNCTION MakeNull RETURNS CHARACTER
  ( input pValue  as character,
    input pQuoted as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE createJSON Include 
PROCEDURE createJSON :
/*------------------------------------------------------------------------------
  Purpose:     Loop through a temp table to build a JSON file
  Parameters:  <none>
  Rules:       If a field is marked as SERIALIZE-HIDDEN, then the
               SERIALIZED-NAME is the name of a child table and put in the spot
               the field is in the parent JSON
               For a field marked as XML-NODE-TYPE = HIDDEN then the field will
               not be displayed in the resulting file
               A child tables linking field must be an integer and the label
               for the parent field must be the name of the field in the child
               table
  Notes:       A temp table field that links to a child table cannot be last in
               the parent temp table
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pTable as character no-undo.
  define input parameter pLabel as character no-undo.
  define input parameter pLevel as integer   no-undo.
  define input parameter pArray as logical   no-undo.
  define input parameter pWhere as character no-undo.

  /* variables */
  define variable hQuery      as handle  no-undo.
  define variable hBuffer     as handle  no-undo.
  define variable hField      as handle  no-undo.
  define variable fieldCount  as integer no-undo.
  define variable rowCount    as integer no-undo initial 0.
  
  /* for clobs */
  define variable tLongchar as longchar  no-undo.
  define variable tString   as character no-undo.
  define variable tIndex    as integer   no-undo initial 1.
  &scoped-define ChunkSize 30000
  
  /* create a query to build the JSON */
  create buffer hBuffer for table pTable.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + pTable + " no-lock " + pWhere).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hQuery:get-next().
  end.
  
  if valid-handle(hBuffer:table-handle) and not hBuffer:table-handle:has-records
   then return.
  
  /* output to the file */
  output to value({&JSONFile}) append.
  if pLabel > ""
   then put unformatted "  " + fill(" ", (pLevel - 1) * 2) + "~"" + pLabel + "~": ".
  
  /* if is an array or has multiple rows */
  if pArray or hQuery:num-results > 1
   then put unformatted "[".
  
  /* loop through records */
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    rowCount = rowCount + 1.
    put unformatted "~{" skip.
    do fieldCount = 1 to hBuffer:num-fields:
      hField = hBuffer:buffer-field(fieldCount).
      if not hField:serialize-hidden and hField:xml-node-type <> "HIDDEN"
       then
        do:
          put unformatted "  " + fill(" ", pLevel * 2) + "~"" + hField:serialize-name "~": ".
          case hField:data-type:
           when "CLOB"      then
            do:
              tIndex = 1.
              put unformatted "~"".
              copy-lob hField:buffer-value() to tLongchar.
              do while tIndex < length(tLongchar):
                tString   = substring(tLongchar, tIndex, {&ChunkSize}).
                put unformatted tString.
                tIndex = tIndex + {&ChunkSize}.
              end.
              put unformatted "~"".
            end.
           when "DATETIME"  then put unformatted EpochDate(hField:buffer-value()).
           when "CHARACTER" then put unformatted MakeNull(hField:buffer-value(), true).
           when "LOGICAL"   then
            do:
              if hField:buffer-value()
               then put unformatted "true".
               else put unformatted "false".
            end.
           otherwise put unformatted MakeNull(string(hField:buffer-value()), false).
          end.
          if fieldCount = hBuffer:num-fields
           then put unformatted "" skip.
           else put unformatted "," skip.
        end.
      else if hField:serialize-hidden
       then
        do:
          output close.
          std-ch = "".
          if hField:label <> hField:name
           then std-ch = "where " + hField:label + " = " + string(hBuffer:buffer-field(hField:label):buffer-value()).
          run createJSON in this-procedure (hField:name, hField:serialize-name, pLevel + 1, (hField:extent = 1), std-ch).
          output to value({&JSONFile}) append.
        end.
    end.
    if rowCount = hQuery:num-results and (pLevel = 1 or hQuery:num-results > 1 or pArray)
     then put unformatted "  " + fill(" ", (pLevel - 1) * 2) + "~}".
     else put unformatted "  " + fill(" ", (pLevel - 1) * 2) + "~},".
    hQuery:get-next().
  end.
  
  /* close the JSON */
  /* if is an array with rows */
  if pArray
   then put unformatted "]" + (if pLevel > 1 then "," else "").
   
  put unformatted "" skip.
  
  /* close the query */
  hQuery:query-close().
  delete object hQuery.
  
  /* close the output */
  output close.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE createRootJSON Include 
PROCEDURE createRootJSON :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pRootTable as character no-undo.
  define input  parameter pRootLabel as character no-undo.
  define output parameter pOutput    as character no-undo.
  
  os-delete value({&JSONFile}).
  if pRootLabel > ""
   then
    do:
      output to value({&JSONFile}) append.
      put unformatted "~{" skip.
      output close.
    end.
  
  run createJSON in this-procedure (pRootTable, pRootLabel, 1, false, "").
  
  if pRootLabel > ""
   then
    do:
      output to value({&JSONFile}) append.
      put unformatted "~}" skip.
      output close.
    end.
  
  pOutput = search({&JSONFile}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION EpochDate Include 
FUNCTION EpochDate RETURNS CHARACTER
  ( input pDate as datetime ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tBaseDate as datetime no-undo.
  define variable tEpoch    as int64    no-undo.

  if pDate = ?
   then return "null".

  tBaseDate = datetime(1,1,1970,0,0,0,0).
  tEpoch    = interval(pDate, tBaseDate, "seconds").

  return "~"\/Date(" + string(tEpoch) + ")\/~"".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION MakeNull Include 
FUNCTION MakeNull RETURNS CHARACTER
  ( input pValue  as character,
    input pQuoted as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable pReturn as character no-undo.
  define variable tQuote  as character no-undo.
  tQuote = (if pQuoted then "~"" else "").

  if pValue = "" or pValue = ?
   then pReturn = (if {&doNull} then "null" else tQuote + tQuote).
   else pReturn = tQuote + pValue + tQuote.
   
  RETURN pReturn.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

