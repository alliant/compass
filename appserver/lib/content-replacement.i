/*------------------------------------------------------------------------
@file content-replacement.i
@description Replace the database driven fields with their respective tables
             
@notes Rules:
       - User placeholders need to be a single word (text surrounded by blank spaces)
       - User placeholders can't contain a period
       
@Modified :
Name      Date          Comments
SRK       11/11/2024    Added Deed table        
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* replacement characters */
&scoped-define ContentReplacementStartChar         "~~~{"
&scoped-define ContentReplacementEndChar           "~~~}"
&scoped-define ContentReplacementDatabaseDelimiter "."
&scoped-define ContentReplacementUserDelimiter     "|"

/* used for the names of the user replace combo boxes and button */
&scoped-define ContentReplacementUserDisplayList   "Character,Currency,Date,Number"
&scoped-define ContentReplacementUserDatatypeList  "string" + {&ContentReplacementUserDelimiter} + "decimal" + {&ContentReplacementUserDelimiter} + "date" + {&ContentReplacementUserDelimiter} + "integer"
&scoped-define ContentReplacementUserFormatList    "x(1000)" + {&ContentReplacementUserDelimiter} + ">>>,>>>,>>>,>>>,>>9.99" + {&ContentReplacementUserDelimiter} + "99/99/99" + {&ContentReplacementUserDelimiter} + "9(100)"

/* displayed table */
&global-define DatabaseReplacementEntityFile     "file"
&global-define DatabaseReplacementEntitySearch   "search"
&global-define DatabaseReplacementEntityPolicy   "policy"
&scoped-define DatabaseReplacementEntityProperty "property"
&scoped-define DatabaseReplacementEntityLender   "lender"
&scoped-define DatabaseReplacementEntityBuyer    "buyer"
&scoped-define DatabaseReplacementEntitySeller   "seller"
&scoped-define DatabaseReplacementEntitydeed     "deed"

/* the tables */
&if defined(agentfile) = 0 &then
&scoped-define agentfile agentfile
&endif

&if defined(search_) = 0 &then
&scoped-define search_ search_
&endif

&if defined(policy) = 0 &then
&scoped-define policy policy
&endif

&if defined(fileproperty) = 0 &then
&scoped-define fileproperty fileproperty
&endif

&if defined(buyer) = 0 &then
&scoped-define buyer buyer
&endif

&if defined(seller) = 0 &then
&scoped-define seller seller
&endif

&if defined(lender) = 0 &then
&scoped-define lender lender
&endif

/* used for the party types */
&if defined(LendersType) = 0 &then
&scoped-define LendersType "L"
&endif

&if defined(BuyersType) = 0 &then
&scoped-define BuyersType  "B"
&endif

&if defined(SellersType) = 0 &then
&scoped-define SellersType "S"
&endif

&if defined(SearchType) = 0 &then
&scoped-define SearchType "Search"
&endif

&if defined(PolicyType) = 0 &then
&scoped-define PolicyType "Policy"
&endif

&if defined(deed) = 0 &then
&scoped-define deed deed
&endif

/* fields for handling exceptions for replacement logic */
/* fileparty*/
&scoped-define FilePartyFieldName        "name"
&scoped-define FilePartyFieldFirstName   "firstname"
&scoped-define FilePartyFieldMiddleName  "middlename"
&scoped-define FilePartyFieldLastName    "lastname"
/*property field */
&scoped-define PropertyFieldLongLegal   "longLegal"
 
method private longchar ReplaceDatabasePlaceholder (input table-handle htemp_agentfile,
                                                    input table-handle htemp_search ,
                                                    input table-handle htemp_policy ,
												    input table-handle htemp_fileProperty ,
                                                    input table-handle htemp_Buyer ,
                                                    input table-handle htemp_Seller ,
                                                    input table-handle htemp_Lender ,                                            
                                                    input table-handle htemp_deed , 
                                                    input pText as longchar):
    
    /* get the entity, field, and sequence of the field */
  define variable tEntity   as character no-undo.
  define variable tField    as character no-undo.
  define variable tSequence as integer   no-undo.
  
  /* return text */
  define variable tPeriod   as logical   no-undo initial false.
  define variable tReturn   as character no-undo.

  /* handles */
  define variable hQuery    as handle    no-undo.
  define variable hBuffer   as handle    no-undo.
  define variable hField    as handle    no-undo.
  define variable iField    as integer   no-undo.

    /* field handles for party table's name, first name, middle name and lastname field*/
  define variable hname       as handle no-undo.
  define variable hFirstName  as handle no-undo.
  define variable hMiddleName as handle no-undo.
  define variable hLastName   as handle no-undo.
  
  /* variables */
  define variable iCount    as integer   no-undo.
  define variable tWord     as character no-undo.
  define variable iStartPos as integer   no-undo.
  define variable tStart    as character no-undo.
  define variable iEndPos   as integer   no-undo.
  define variable tEnd      as character no-undo.
  define variable tOriginal as character no-undo.
  define variable tExit     as logical   no-undo initial false.

  /* loop through all the words */
  do iCount = 1 to num-entries(pText, " "):
    /* get the word */
    assign
      tOriginal = entry(iCount, pText, " ")
      tWord     = tOriginal
      .

    
    if index(tWord, {&ContentReplacementStartChar}) > 0 and index(tWord, {&ContentReplacementEndChar}) > 0 and IsDatabasePlaceholder(tWord)
     then
      do while index(tWord, {&ContentReplacementStartChar}) > 0 and index(tWord, {&ContentReplacementEndChar}) > 0 and IsDatabasePlaceholder(tWord) and not tExit:
        /* get the entity, sequence, and field names */
        assign
          tOriginal = tWord
          iStartPos = index(tWord, {&ContentReplacementStartChar})
          iEndPos   = index(tWord, {&ContentReplacementEndChar})
          tStart    = if iStartPos = 1 then "" else substring(tOriginal, 1, iStartPos - 1)
          tEnd      = if iEndPos = length(tOriginal) then "" else substring(tOriginal, iEndPos + 1)
          tWord     = substring(tOriginal, iStartPos + 1, iEndPos - iStartPos - 1)
          tEntity   = entry(1, tWord, {&ContentReplacementDatabaseDelimiter})
          .
		
        if num-entries(tWord, {&ContentReplacementDatabaseDelimiter}) = 3
         then 
          assign
            tSequence = integer(entry(2, tWord, {&ContentReplacementDatabaseDelimiter}))
            tField    = entry(3, tWord, {&ContentReplacementDatabaseDelimiter})
            .
         else
          assign
            tSequence = 0
            tField    = entry(2, tWord, {&ContentReplacementDatabaseDelimiter})
            .

        if (tEntity <> {&DatabaseReplacementEntityFile}     and 
            tEntity <> {&DatabaseReplacementEntitySearch}   and 
            tEntity <> {&DatabaseReplacementEntityPolicy}   and 
            tEntity <> {&DatabaseReplacementEntityProperty} and
            tEntity <> {&DatabaseReplacementEntityLender}   and
            tEntity <> {&DatabaseReplacementEntityBuyer}    and
            tEntity <> {&DatabaseReplacementEntitySeller}   and   
            tEntity <> {&DatabaseReplacementEntitydeed})    or
            (tEntity = {&DatabaseReplacementEntityProperty} and tField = {&PropertyFieldLongLegal})
         then 
          do:
            tWord = tOriginal.
            leave.
          end.
   
        /* get the data from the correct table */
        create buffer hBuffer for table entry(1, GetDatabasePlaceholderQuery(tEntity), " ").
        create query hQuery.
        hQuery:set-buffers(hBuffer).
       
        hQuery:query-prepare("for each " + GetDatabasePlaceholderQuery(tEntity)).
        hQuery:query-open().
        hQuery:get-first().
       
        repeat while not hQuery:query-off-end:
          if tSequence > 0 and hBuffer:buffer-field("seq"):buffer-value() <> tSequence
           then
            do: 
              hQuery:get-next().
              next.
            end.

          if tEntity = {&DatabaseReplacementEntityLender} or
             tEntity = {&DatabaseReplacementEntityBuyer}  or
             tEntity = {&DatabaseReplacementEntitySeller} and tField = {&FilePartyFieldName}
           then
            do:
              assign
                hname       = hBuffer:buffer-field({&FilePartyFieldName})
                hFirstName  = hBuffer:buffer-field({&FilePartyFieldFirstName})
                hMiddleName = hBuffer:buffer-field({&FilePartyFieldMiddleName})
                hLastName   = hBuffer:buffer-field({&FilePartyFieldLastName}).
              if valid-handle(hname)       and 
                 valid-handle(hFirstName)  and 
                 valid-handle(hMiddleName) and 
                 valid-handle(hLastName)
               then
                do:
                  if string(hname:buffer-value()) = ""
                   then tWord = string(hFirstName:buffer-value()) + 
                                (if string(hMiddleName:buffer-value()) = "" then " " 
                                 else " " + string(hMiddleName:buffer-value()) + " ") + string(hLastName:buffer-value()).
                  else tWord = string(hname:buffer-value()).
                  hQuery:get-next().
                  next.
                end.
            end.
            

          hField = hBuffer:buffer-field(tField).
          if valid-handle(hField)
           then
            do:
              tWord = if string(hField:buffer-value()) eq ? then '' else string(hField:buffer-value()).
              tWord = replace(tWord, '.', '^').
            end.
           else tExit = true.
          
          hQuery:get-next().
  
        end.
        hQuery:query-close().
        
        tWord = tStart + tWord + tEnd.
        
        delete object hQuery    no-error.
        delete object hBuffer   no-error.
        delete object hField    no-error.
      end. /* end of database word */
      
      tReturn = addDelimiter(tReturn, " ") + tWord.
  end.
  
  return tReturn.   /* Function return value. */
end method.
  
method private logical IsDatabasePlaceholder (pField as char):
  /* start and end positions of the replacement characters */
  define variable iStartPos as integer no-undo.
  define variable iEndPos   as integer no-undo.
  
  assign
    iStartPos = index(pField, {&ContentReplacementStartChar})
    iEndPos   = index(pField, {&ContentReplacementEndChar})
    .
    
  if index(pField, {&ContentReplacementDatabaseDelimiter}) > iStartPos and index(pField, {&ContentReplacementDatabaseDelimiter}) < iEndPos
   then return true.
  return false.   /* Function return value. */
end method.

method private character GetDatabasePlaceholderQuery (pEntity as char):
  case pEntity:
    when {&DatabaseReplacementEntityFile}     then return "{&agentfile}".
    when {&DatabaseReplacementEntitySearch}   then return "{&search_}".
    when {&DatabaseReplacementEntityPolicy}   then return "{&policy}".
    when {&DatabaseReplacementEntityProperty} then return "{&fileproperty}".
    when {&DatabaseReplacementEntityLender}   then return "{&lender}".
    when {&DatabaseReplacementEntityBuyer}    then return "{&buyer}".
    when {&DatabaseReplacementEntitySeller}   then return "{&seller}".
    when {&DatabaseReplacementEntitydeed}     then return "{&deed}".
  end case.
  
  return "".   /* Function return value. */
end method.

method private character addDelimiter 
 ( input pString as character,
   input pDelimiter as character ) :
  
  def var cDelimitedString as char no-undo.
  cDelimitedString = pString + (if pString <> "" then pDelimiter else "").
  
  return cDelimitedString.
  
end method.

