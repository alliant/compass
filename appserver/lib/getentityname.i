&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/getentityname.i
   Gets the entity name for given the entity
   rahul sharma
   09/11/2017
   Notes:    entity  -  entity on finding (agent,department,attorney,company)/person
             entityId - The entityId to get the entityname from
             entityName - field which store name of entity
   @Modification: 
   Date          Name               Description
   01/13/2020    Sachin Chaturvedi  Added 'person' entity
   */

if {&entity} = "Agent" then
do:
  find first agent no-lock where agent.agentID = {&entityId} no-error.
  if available agent then
    {&entityName} = agent.name.
end.

else if {&entity} = "Attorney" then
do:
  find first attorney no-lock where Attorney.attorneyID = {&entityId} no-error.
  if available Attorney then
    {&entityName} = Attorney.name1.  
end.

else if {&entity} = "Department" then
do:
  find first dept no-lock where dept.deptID = {&entityId} no-error.
  if available dept then
    {&entityName} = dept.name.   
end.  

else if {&entity} = "Company" then
  {&entityName} = "".

else if {&entity} = "Person" then
do:
  find first person no-lock where person.personID = {&entityId} no-error.
  if available person
   then
    {&entityName} = if (person.dispName ne "" and person.dispName ne "?")
                     then
                      person.dispName
                     else
                      trim((if (person.firstname  ne "" and person.firstname  ne ?) then (person.firstname)        else "") +
                           (if (person.middlename ne "" and person.middlename ne ?) then (" " + person.middlename) else "") +
                           (if (person.lastname   ne "" and person.lastname   ne ?) then (" " + person.lastname)   else "")
                          ).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


