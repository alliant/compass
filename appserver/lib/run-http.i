&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*---------------------------------------------------------------------
@name run-http.i
@description Procedures for getting the sharefile data

@author John Oliver
@version 1.0
@created 10/20/2016
@notes 
---------------------------------------------------------------------*/

/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

do:
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE HTTPDelete Include 
PROCEDURE HTTPDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pName as character no-undo.
  define input parameter pURL as character no-undo.
  define input parameter pHeader as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  define output parameter pMsg as character no-undo.
  define output parameter pReponseFile as character no-undo.

  pReponseFile = "".
  run util/httpdelete.p (input pName,
                         input pURL, 
                         input pHeader,
                         output pSuccess,
                         output pMsg,
                         output pReponseFile).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE HTTPGet Include 
PROCEDURE HTTPGet :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pName as character no-undo.
  define input parameter pURL as character no-undo.
  define input parameter pHeader as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  define output parameter pMsg as character no-undo.
  define output parameter pReponseFile as character no-undo.

  pReponseFile = "".
  run util/httpget.p (input pName,
                      input pURL, 
                      input pHeader,
                      output pSuccess,
                      output pMsg,
                      output pReponseFile).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE HTTPPatch Include 
PROCEDURE HTTPPatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pName as character no-undo.
  define input parameter pURL as character no-undo.
  define input parameter pHeader as character no-undo.
  define input parameter pContentType as character no-undo.
  define input parameter pPostFile as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  define output parameter pMsg as character no-undo.
  define output parameter pReponseFile as character no-undo.

  pReponseFile = "".
  run util/httppatch.p (input pName,
                        input pURL, 
                        input pHeader,
                        input pContentType,
                        input pPostFile,
                        output pSuccess,
                        output pMsg,
                        output pReponseFile).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE HTTPPost Include 
PROCEDURE HTTPPost :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pName as character no-undo.
  define input parameter pURL as character no-undo.
  define input parameter pHeader as character no-undo.
  define input parameter pContentType as character no-undo.
  define input parameter pPostFile as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  define output parameter pMsg as character no-undo.
  define output parameter pReponseFile as character no-undo.

  pReponseFile = "".
  run util/httppost.p (input pName,
                       input pURL, 
                       input pHeader,
                       input pContentType,
                       input pPostFile,
                       output pSuccess,
                       output pMsg,
                       output pReponseFile).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE HTTPPut Include 
PROCEDURE HTTPPut :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pName        as character no-undo.
  define input  parameter pURL         as character no-undo.
  define input  parameter pHeader      as character no-undo.
  define input  parameter pContentType as character no-undo.
  define input  parameter pPutFile     as character no-undo.
  define output parameter pSuccess     as logical   no-undo initial false.
  define output parameter pMsg         as character no-undo.
  define output parameter pReponseFile as character no-undo.

  pReponseFile = "".
  run util/httpput.p (input pName,
                      input pURL, 
                      input pHeader,
                      input pContentType,
                      input pPutFile,
                      output pSuccess,
                      output pMsg,
                      output pReponseFile).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

