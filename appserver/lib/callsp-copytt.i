/*------------------------------------------------------------------------
    File        : lib/callsp-copytt.i   
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : Sachin Anthwal
    Created     : 12/17/21
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* ***************************  Main Block  *************************** */
&if defined(load-handle) = 0 &then
&scoped-define load-handle csp-hProcedureTable
&endif

&if defined(setParam) = 0 &then
&scoped-define setParam "{&load-into}"
&endif

&if defined(load-into) <> 0 &then
 {&load-handle}:name = "{&load-into}".
 csp-cFile = guid + ".json".
 {&load-handle}:write-json("FILE", csp-cFile).
 temp-table {&load-into}:read-json("FILE", csp-cFile, "EMPTY").
 os-delete value(csp-cFile).
 delete object {&load-handle} no-error.
&endif

&if defined(noCount) = 0 and defined(load-into) <> 0 &then
 csp-icount = 0.
 for each {&load-into} no-lock:
   csp-icount = csp-icount + 1.
 end.
 
  &if defined(noResponse) = 0 &then
  if csp-icount > 0
   then 
    pResponse:setParameter({&setParam}, table {&load-into}).
  &endif
&endif
