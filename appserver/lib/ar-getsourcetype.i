/*------------------------------------------------------------------------
    File        : ar-getsourcetype.i
    Purpose     : Returns source type of AR records

    Syntax      :

    Description :

    Author(s)   : Rahul Sharma
    Created     : 04/15/2020
    Notes       :
    @Modified
	03/10/2021	MK	"Added 'RB' sourceType for refund".
    07/02/2021	SB	"Added 'W' sourceType for write-off"
  ----------------------------------------------------------------------*/
&IF DEFINED(SourceType) = 0
&THEN

FUNCTION getSourceType RETURNS CHARACTER
  ( cSourceType as character )  FORWARD.

FUNCTION getSourceType RETURNS CHARACTER
  ( cSourceType as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cSourceType = 'B' 
   then
    return 'Batch'. 
  else if cSourceType = 'S' 
   then
    return 'Search'.
  else if cSourceType = 'T' 
   then
    return 'Training'.  
  else if cSourceType = 'A' 
   then
    return 'Appointment'.  
      else if cSourceType = 'R' 
   then
    return 'Recovery'.  
  else if cSourceType = 'N' 
   then
    return 'Sponsorship'. 
  else if cSourceType = 'D' 
   then
    return 'Deposit'.
  else if cSourceType = 'C' 
   then
    return 'Credit'.  
  else if cSourceType = 'P' 
   then
    return 'Payment'.
  else if cSourceType = 'M' 
   then
    return 'Miscellaneous'.  
 else if cSourceType = 'RB' 
   then
    return 'Refund'.
  else if cSourceType = 'W' 
   then
    return 'Write-off'.    
  else
    return cSourceType.   /* Function return value. */

END FUNCTION.
&GLOBAL-DEFINE SourceType true
&ENDIF
