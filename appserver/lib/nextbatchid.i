/* lib/nextbatchid.i
   calculate the NEXT BATCH ID
   R.Kenny / D.Sinclair
   5.7.2014
 */
  
  FUNCTION nextBatchID RETURNS int PRIVATE 
      (input piMonth as int, 
       input piYear as int):
    def buffer batch for batch.
    def buffer period for period.

    def var iNextBatch as int no-undo.
    def var tPeriodID AS INT NO-UNDO.

    tPeriodID = 0.
    for FIRST period no-lock
      where period.periodMonth = piMonth
        and period.periodYear = piYear:
     if not period.active
      THEN return 0.
     tPeriodID = period.periodID. 
    end.
    IF tPeriodID = 0 
     THEN RETURN 0.

    iNextBatch = 0.
    for each batch FIELDS (periodID batchID) no-lock
      where batch.periodID = tPeriodID
       by batch.batchID descending:
     iNextBatch = batch.batchID + 1.
     leave.
    end.
    if iNextBatch = 0
     then iNextBatch = ((tPeriodID - 200000) * 10000) + 1.
        /* Strip century from year portion of periodID and add seq number */     
    return iNextBatch.
  END FUNCTION.  /* getNextBatchID */
