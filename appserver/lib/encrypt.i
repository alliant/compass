&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/encrypt.i
   D.Sinclair
   6.14.2012
 */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD decrypt Include 
FUNCTION decrypt RETURNS CHARACTER PRIVATE
  ( pEncrypted as CHAR ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD encrypt Include 
FUNCTION encrypt RETURNS CHARACTER PRIVATE
   ( pUnencrypted as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD ceiling Procedure
FUNCTION ceiling RETURNS integer PRIVATE
  ( input pNum as decimal ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION decrypt Include 
FUNCTION decrypt RETURNS CHARACTER PRIVATE
  ( pEncrypted as CHAR ) :
/*------------------------------------------------------------------------------
  Purpose: Converts a base64 string to a contiguous string
    Notes: The algorithm is to convert the base64 string back into a memory
           pointer. The first memory position is the length of the string so then
           we loop through that number starting at memory pointer 2 and convert
           that integer into an ascii character.
------------------------------------------------------------------------------*/
  define variable i as integer no-undo.
  define variable tAsc as integer no-undo.
  define variable tOffset as integer no-undo.
  define variable tStart as integer no-undo.
  define variable tLength as integer no-undo.
  define variable tMemPtr as memptr no-undo.
  define variable pResult as character no-undo.
 
  /* decrypt the string */
  tMemPtr = base64-decode(pEncrypted) no-error.
  if error-status:error or get-byte(tMemPtr, 1) < 97
   then return "".
   
  assign
    tLength = 0
    /* the first character is start position */
    tStart = (get-byte(tMemPtr, 1) - 96) + index(string(tMemPtr), "=")
    /* the second character is the offset */
    tOffset = get-byte(tMemPtr, 2) - 96
    .
  /* exit if not right encryption as the first byte is a lowercase letter */
  /* get the length of the string */
  do i = 3 to index(string(tMemPtr), "=") - 1:
    tLength = tLength + get-byte(tMemPtr, i) - 96.
  end.
  /* get the word */
  do i = tStart to tLength + tStart - 1:
    tAsc = get-byte(tMemPtr, i).
    /* if the unicode character is an uppercase character */
    if tAsc >= 65 and tAsc <= 90
     then
      do:
        tAsc = tAsc - tOffset.
        if tAsc < 65
         then tAsc = tAsc + 26.
      end.
    /* if the unicode character is a lowercase character */
    if tAsc >= 97 and tAsc <= 122
     then
      do:
        tAsc = tAsc - tOffset.
        if tAsc < 97
         then tAsc = tAsc + 26.
      end.
    pResult = pResult + chr(tAsc).
  end.
    
 RETURN pResult.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION encrypt Include 
FUNCTION encrypt RETURNS CHARACTER PRIVATE
   ( pUnencrypted as char ) :
/*------------------------------------------------------------------------------
  Purpose: Convert a contiguous string to base64 string
    Notes: The algorithm is to create an encoded string that is easily
           recreated in any language if it has a base64 decode function. The
           first character is the starting position of the word, the second
           character is the offset, and the third character to the = sign is
           length (add up all of the numbers).
------------------------------------------------------------------------------*/
  define variable i as integer no-undo.
  define variable tAsc as integer no-undo.
  define variable tOffset as integer no-undo.
  define variable tStart as integer no-undo.
  define variable tLength as integer no-undo.
  define variable tMemPtr as memptr no-undo.
  define variable pResult as character no-undo.
 
  assign
    /* get the length of all characters before the = sign */
    tLength = ceiling(length(pUnencrypted) / 26) + 3
    /* create the memory pointer */
    set-size(tMemPtr) = tLength + maximum(length(pUnencrypted), 128)
    /* the first character is start position */
    tStart = random(1, 26)
    put-byte(tMemPtr, 1) = asc(chr(tStart + 96))
    /* the second character is the offset */
    tOffset = random(1, 26)
    put-byte(tMemPtr, 2) = asc(chr(tOffset + 96))
    .
  /* we need to determine how long the word is */
  do i = 1 to ceiling(length(pUnencrypted) / 26):
    put-byte(tMemPtr, i + 2) = asc(chr(length(substring(pUnencrypted, 1 + ((i - 1) * 26), 26)) + 96)).
  end.
  put-byte(tMemPtr, tLength) = asc("=").
  /* pad with values until the start of the word */
  do i = tLength + 1 to tStart + tLength - 1:
    put-byte(tMemPtr, i) = (if random(1, 2) = 1 then random(97, 122) else random(65, 90)).
  end.
  tLength = tStart + tLength - 1.
  /* encrypt the word */
  do i = 1 to length(pUnencrypted):
    tAsc = asc(substring(pUnencrypted, i, 1)).
    /* if the unicode character is an uppercase character */
    if tAsc >= 65 and tAsc <= 90
     then
      do:
        tAsc = tAsc + tOffset.
        if tAsc > 90
         then tAsc = tAsc - 26.
      end.
    /* if the unicode character is a lowercase character */
    if tAsc >= 97 and tAsc <= 122
     then
      do:
        tAsc = tAsc + tOffset.
        if tAsc > 122
         then tAsc = tAsc - 26.
      end.
    put-byte(tMemPtr, tLength + i) = tAsc.
  end.
  /* pad with values until the end of the string */
  tLength = tLength + length(pUnencrypted).
  do i = tLength + 1 to get-size(tMemPtr):
    put-byte(tMemPtr, i) = (if random(1, 2) = 1 then random(97,122) else random(65, 90)).
  end.
  /* encode the string */
  assign
    pResult = base64-encode(tMemptr)
    set-size(tMemPtr) = 0
    .

 RETURN pResult.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION ceiling Include 
FUNCTION ceiling RETURNS integer PRIVATE
  ( input pNum as decimal ) :
/*------------------------------------------------------------------------------
  Purpose: Gets the ceiling of the number provided
    Notes: 
------------------------------------------------------------------------------*/
  if truncate(pNum, 0) = pNum then
   return integer(truncate(pNum, 0)).
  else
    return integer(truncate(pNum, 0)) + 1.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
