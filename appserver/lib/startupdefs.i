/*------------------------------------------------------------------------
    File        : startupdefs.i
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : MK
    Created     :
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{lib/std-def.i} 

/* Webspeed related variables and functions */
def {1} SHARED var startup-utilities-hdl as handle no-undo.

function getRequestBodyType returns character()
  in startup-utilities-hdl.
  
function getRequestBodyXML returns HANDLE()
  in startup-utilities-hdl.
  
function getRequestBodyJSON returns character()
  in startup-utilities-hdl.

function getStartup returns character()
  in startup-utilities-hdl.

&IF "{1}" = "NEW" &THEN
 startup-utilities-hdl = THIS-PROCEDURE:HANDLE.
&ENDIF




