&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@name lib/rpt-table.i
@description standard RePorT TABLE functions

@author John Oliver
@created 5.31.2017
@notes
----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD columnHeader Include 
FUNCTION columnHeader RETURNS LOGICAL
  ( input pColumn as character,
    input pWidth as integer  ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createTable Procedure
FUNCTION createTable RETURNS logical 
 (  ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD defineTable Include 
FUNCTION defineTable RETURNS logical 
 ( input pTableName as character,
   input hTable as handle )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION columnHeader Include 
FUNCTION columnHeader RETURNS LOGICAL
  ( input pColumn as character,
    input pWidth as integer  ) :
/*------------------------------------------------------------------------------
@description Create a column header
------------------------------------------------------------------------------*/
  assign
    iColumn = iColumn + 1
    iWidthTotal = iWidthTotal + pWidth
    .
  RUN pdf_set_tool_parameter({&rpt},cTableName,"ColumnHeader",iColumn,pColumn).
  RUN pdf_set_tool_parameter({&rpt},cTableName,"ColumnWidth",iColumn,string(pWidth)).
  run pdf_set_tool_parameter({&rpt},cTableName,"MaxX",iColumn,string(pWidth)).
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createTable Include 
FUNCTION createTable RETURNS logical 
 (  ) :
/*------------------------------------------------------------------------------
@description Creates the table in the PDF
------------------------------------------------------------------------------*/
  define variable oldMargin as integer no-undo.
  oldMargin = pdf_LeftMargin({&rpt}).
  RUN pdf_set_LeftMargin({&rpt},40).
  RUN pdf_tool_create ({&rpt},cTableName).
  run pdf_tool_destroy ({&rpt},cTableName).
  RUN pdf_set_LeftMargin({&rpt},oldMargin).
  cTableName = "".
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION defineTable Include 
FUNCTION defineTable RETURNS logical 
 ( input pTableName as character,
   input hTable as handle ) :
/*------------------------------------------------------------------------------
@description Defines the table structure
------------------------------------------------------------------------------*/
  if valid-handle(hTable) and hTable:type = "TEMP-TABLE"
   then
    do:
      run pdf_tool_add ({&rpt},pTableName,"TABLE",hTable).
      RUN pdf_set_tool_parameter({&rpt},pTableName,"Outline",0,"1").
      RUN pdf_set_tool_parameter({&rpt},pTableName,"HeaderFont",0,"Helvetica-Bold").
      RUN pdf_set_tool_parameter({&rpt},pTableName,"HeaderFontSize",0,"11").
      RUN pdf_set_tool_parameter({&rpt},pTableName,"HeaderBGColor",0,"198,206,221").
      RUN pdf_set_tool_parameter({&rpt},pTableName,"HeaderTextColor",0,"102,102,102").
      RUN pdf_set_tool_parameter({&rpt},pTableName,"DetailTextColor",0,"102,102,102").
      RUN pdf_set_tool_parameter({&rpt},pTableName,"DetailFont",0,"Helvetica").
      RUN pdf_set_tool_parameter({&rpt},pTableName,"DetailFontSize",0,"11").
      RUN pdf_set_tool_parameter({&rpt},pTableName,"ColumnPadding",0,"6").
      RUN pdf_set_tool_parameter({&rpt},pTableName,"StartY",0, 700 - ((iLine - 1) * 12)).
      assign
        iColumn = 0
        iWidthTotal = 0
        cTableName = pTableName
        .
    end.
   else return false.
   
 return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

