/* lib/deletebatchform.i
   function to DELETE a single BATCHFORM record
   This procedure should be called from within an enclosing TRANSACTION.
   Created 5.7.2014 D.Sinclair
   2.25.2015 D.Sinclair   Use retention fields for consistency
                          Restructured batch aggregate field calculations
                          Use secondary buffer to avoid confusion
   Modified: 
   Date          Name         Comments
   09/24/2019    Vikas Jain   Changed code to use the fileID instead of 
                              fielNumber to compare the batchforms
   02/12/2021    MK           Modified for change in commitment processing
 */
                                

  FUNCTION deleteBatchForm RETURNS LOGICAL PRIVATE
    (input pBatchID as int,
     input pSeq as int,
     output pcErr as char):
   
    def var plSuccess as LOGICAL INIT FALSE no-undo.

    def buffer batch for batch.
    def buffer batchForm for batchForm.
    def buffer xBatchForm for batchForm.

    def var iPolicyID as int no-undo.
    def var lValidPolicy as log no-undo.
    def var tUpdated as logical.
    def var lNoFile as logical init true no-undo.
    def var cFormType as char no-undo.
    def var cFileID   as char no-undo.
    
    tUpdated = false.
    pcErr = "Delete failed".

    UPDATE-BLOCK:
    for FIRST batch exclusive-lock
      where batch.batchID = pBatchID,

      FIRST batchForm exclusive-lock
      where batchForm.batchID = pBatchID
        and batchForm.seq = pSeq
        ON ERROR UNDO UPDATE-BLOCK, LEAVE UPDATE-BLOCK:

     assign   
      iPolicyID = batchForm.policyID
      cFormType = batchform.formType
      cFileID   = batchForm.fileID
      .
      assign
        batch.liabilityAmount = batch.liabilityAmount - batchForm.liabilityAmount
        batch.grossPremiumProcessed = batch.grossPremiumProcessed - batchForm.grossPremium
        batch.netPremiumProcessed = batch.netPremiumProcessed - batchForm.netPremium
        batch.retainedPremiumProcessed = batch.retainedPremiumProcessed - batchForm.retentionPremium

        batch.liabilityDelta = batch.liabilityDelta - batchForm.liabilityDelta
        batch.grossPremiumDelta = batch.grossPremiumDelta - batchForm.grossDelta
        batch.netPremiumDelta = batch.netPremiumDelta - batchForm.netDelta
        batch.retainedPremiumDelta = batch.retainedPremiumDelta - batchForm.retentionDelta
        .

      if batchform.formType = "P" 
       then batch.policyCount = batch.policyCount - 1.
       else
      if batchform.formType = "E" 
       then batch.endorsementCount = batch.endorsementCount - 1.
       else
      if batchform.formType = "C" 
       then batch.cplCount = batch.cplCount - (if batchForm.formCount < 1 then 1 else batchForm.formCount).

      for first xBatchForm fields (batchID fileID seq) no-lock
        where xBatchForm.batchID = pBatchID
          and xBatchForm.fileID = batchForm.fileID
          and xBatchForm.seq <> pSeq:
       lNoFile = false.
      end.
      if lNoFile 
       then batch.fileCount = batch.fileCount - 1.

      pcErr = "Batch validate failed".
      validate batch.

      pcErr = "Batchform delete failed".
      delete batchForm.

      tUpdated = true.
    end.

    if not tUpdated 
     then return false.

    lValidPolicy = TRUE.
     
    if lookup(cFormType, "P,E") <> 0
     then
      do:    
        for FIRST xBatchForm fields (batchID policyID validForm) no-lock
          where xBatchForm.batchID = pBatchID
          and xBatchForm.policyID = iPolicyID
          and xBatchForm.validForm = "E":
          lValidPolicy = FALSE.
        end.

        pcErr = "Batchform setting valid policy failed".
        for each xBatchForm exclusive-lock
          where xBatchForm.batchID = pBatchID
          and xBatchForm.policyID = iPolicyID
          and xBatchform.validPolicy <> lValidPolicy:
          xBatchForm.validPolicy = lValidPolicy.
        end.
      end.    
    
    plSuccess = TRUE.
    pcErr = "".

    RETURN plSuccess.
  END FUNCTION.
