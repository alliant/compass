/* xml/tt-xml.i
 * manage an XML document from Temp-Table data
 *
 * createXml() INTERFACE:
 *  RootName	The name for the root node to be created
 *  Tables	The tables that are to be queried
 *  Query	The query string to retrieve from the tables
 *
 * appendXml() INTERFACE:
 *  BaseNode	The node below which the query will be appended
 *  Tables	The tables that are to be queried
 *  Query	The query string to retrieve from the tables
 */

&IF defined(tt-xml) = 0 
 &THEN
&global-define tt-xml true

FUNCTION appendXml returns logical 
  (input pBaseNode as handle,
   input pTables AS CHAR,
   input pQuery  AS CHAR):
  
 def var hQuery AS HANDLE NO-UNDO. 
 def var hTable AS HANDLE EXTENT 100 NO-UNDO. 
 def var hField AS HANDLE NO-UNDO. 
 def var t-value as char.

 def var i           AS INT NO-UNDO. /* general counter */
 def var iLine       AS INT NO-UNDO. /* current line of query */
 def var iLevel      AS INT NO-UNDO. /* which level of query */
 def var iField      AS INT NO-UNDO. /* field counter */
 def var iExtent     AS INT NO-UNDO. /* extent counter */
 def var iNumBuffers AS INT NO-UNDO. /* number of buffers in query */
 
 def var roTable AS ROWID EXTENT 100 NO-UNDO. /* rowid of each buffer */

 def var xDoc as handle.
 def var xRoot as handle.
 def var xParent AS HANDLE.
 def var xNode AS HANDLE EXTENT 100.
 
 if not valid-handle(pBaseNode)
   or (valid-handle(pBaseNode)
       and pBaseNode:type <> "X-NODEREF")
  then return false.

 create X-DOCUMENT xDoc.
 create X-NODEREF xRoot.
 create X-NODEREF xParent.

 xRoot = pBaseNode.
 xDoc = xRoot:owner-document.

 CREATE QUERY hQuery.
 
 DO i = 1 TO NUM-ENTRIES(pTables):
  CREATE BUFFER hTable[i] FOR TABLE ENTRY(i,pTables). 
  hQuery:ADD-BUFFER(hTable[i]).  
 END.
  
 hQuery:QUERY-PREPARE(pQuery). 
  
 assign 
  iNumBuffers = hQuery:NUM-BUFFERS
  roTable     = ?
  .
 
 hQuery:QUERY-OPEN().
 hQuery:GET-FIRST().
 
 iLevel = 1.
 
 repeat while not hQuery:QUERY-OFF-END iLine = 1 to iLine + 1: 
  
  do i = iLevel to iNumbuffers: 
         /* ilevel represents the last changed table */
                                         
   roTable[i] = hTable[i]:ROWID. /* store new rowid of table */

   IF i = 1 
    THEN xParent = xRoot. 
    ELSE xParent = xNode[i - 1].

   if not valid-handle(xNode[i]) 
    then create x-noderef xNode[i]. 
   xDoc:CREATE-NODE(xNode[i],hTable[i]:NAME, "ELEMENT").
   xParent:APPEND-CHILD(xNode[i]).

   do iField = 1 to hTable[i]:NUM-FIELDS: 
    hField = hTable[i]:BUFFER-FIELD(iField).
           
    if hField:EXTENT > 1 
     then
      do iExtent = 1 TO hField:EXTENT:
        if hField:data-type begins "DATE"
         then t-value = encodeForXml(trim(iso-date(hField:buffer-value(iExtent)))).
         else t-value = encodeForXml(trim(string(hField:buffer-value(iExtent)))).
        if t-value = ?
         then t-value = "".
        &if defined(CSharp) <> 0 &then
        if hField:data-type = "LOGICAL"
         then t-value = encodeForXml(if hField:buffer-value(iExtent) then "true" else "false").
        &endif
        xNode[i]:SET-ATTRIBUTE(hField:NAME + "_" + STRING(iExtent), t-value).
      end.
     else 
      do: if hField:data-type begins "DATE"
           then t-value = encodeForXml(trim(iso-date(hField:buffer-value))).
           else t-value = encodeForXml(trim(string(hField:buffer-value))).
          if t-value = ?
           then t-value = "".
          &if defined(CSharp) <> 0 &then
          if hField:data-type = "LOGICAL"
           then t-value = encodeForXml(if hField:buffer-value then "true" else "false").
          &endif
          xNode[i]:SET-ATTRIBUTE(hField:NAME, t-value).
      end.
   end.
  end.
  
  hQuery:GET-NEXT.

  do i = iNumbuffers to 1 by -1: 
/* go backwards through the tables in the query. 
   If the rowid is the same as the new result row, then the table
   has not changed. eg. for each sales-rep, each cust of sales-rep - 
   sales-rep would change only when all cust records of the sales-rep
   have been processed.
 */

   if roTable[i] = hTable[i]:ROWID 
    then leave.
   
   iLevel = i.
  end.
  
 end. 
 hQuery:QUERY-CLOSE.
 DELETE OBJECT hQuery.
 return true.
end FUNCTION.


FUNCTION createXml returns handle
  (pRootName as char,
   pTables as char,
   pQuery as char):
 def var xDoc AS HANDLE.
 def var xRoot AS HANDLE.
 create X-DOCUMENT xDoc.
 create x-noderef xRoot.

 if pRootName = "" or pRootName = ?
  then pRootName = "Content".
 xDoc:CREATE-NODE(xRoot, pRootName, "ELEMENT").
 xDoc:APPEND-CHILD(xRoot).

/*
 xRoot:SET-ATTRIBUTE("Tables", pTables).
 xRoot:SET-ATTRIBUTE("Query", pQuery).
 */

 appendXml(xRoot, pTables, pQuery).
 return xDoc.
end. /* createXml() */

&ENDIF
