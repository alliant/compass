&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : normalizeFileNumber.i
    Purpose     : Strips erroneous characters from File Numbers entered
                  by users or imported into the batchform table

    Syntax      :

    Description :

    Author(s)   : Bryan Johnson
    Created     : 06/26/2019
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD normalizeFileNumber Include 
FUNCTION normalizeFileNumber RETURNS CHARACTER
  ( pOriginalFileNumber as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION normalizeFileNumber Include 
FUNCTION normalizeFileNumber RETURNS CHARACTER
  ( pOriginalFileNumber as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  def var tValidFileNumberChars as char no-undo init "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_/()".
  def var tIndex                as int  no-undo.
  def var tNormalizedFileNumber as char no-undo init "".
  
  pOriginalFileNumber = trim(pOriginalFileNumber).
  
  do tIndex = 1 to length(pOriginalFileNumber):
    if index(tValidFileNumberChars, substring(pOriginalFileNumber,tIndex,1)) > 0 
    then tNormalizedFileNumber = tNormalizedFileNumber + substring(pOriginalFileNumber,tIndex,1).
  end.

  RETURN tNormalizedFileNumber.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

