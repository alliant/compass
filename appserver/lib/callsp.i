/*------------------------------------------------------------------------
    File        : lib/callsp.i
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : Sachin Anthwal
    Created     : 12/17/21
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */


/* ***************************  Main Block  *************************** */

&if defined(load-handle) = 0 &then
&scoped-define load-handle csp-hProcedureTable
&endif

&if (defined(load-into) <> 0) or (defined(noLoad) <> 0)
&then
if not valid-handle({&load-handle}) then
  create temp-table {&load-handle}.
&endif 

do on stop undo, leave:
  csp-lSucess = false.
  &if defined(params) = 0 &then
    &if (defined(load-into) <> 0) or (defined(noLoad) <> 0) &then
    run stored-procedure {&name} load-result-into {&load-handle} no-error.
    &else
    run stored-procedure {&name} no-error.
    &endif
  &else
    &if (defined(load-into) <> 0) or (defined(noLoad) <> 0) &then
    run stored-procedure {&name} load-result-into {&load-handle} no-error ({&params}).
    &else
    run stored-procedure {&name} no-error ({&params}).
    &endif
  &endif
  csp-lSucess = true.
end.

if not csp-lSucess
 then
  do:
    &if defined(noResponse) = 0 &then
    pResponse:fault("3005", "Stored Procedure {&name} failed. Please contact System Administrator.").
    &else
    csp-cErr = "Stored Procedure {&name} failed. Please contact System Administrator.".
    &endif
    return.
  end.

&if (defined(load-into) <> 0) &then
 &if defined(noCount) <> 0 &then
  {lib\callsp-copytt.i &load-handle={&load-handle} &load-into={&load-into}  &noCount={&noCount} &noResponse={&noResponse}}
  &elseif defined(noResponse) <> 0 &then
  {lib\callsp-copytt.i &load-handle={&load-handle} &load-into={&load-into} &noResponse={&noResponse}}
  &elseif defined(setParam) <> 0 &then
  {lib\callsp-copytt.i &load-handle={&load-handle} &load-into={&load-into}  &setParam={&setParam}}
  &else
  {lib\callsp-copytt.i &load-handle={&load-handle} &load-into={&load-into}}
 &endif
&endif


