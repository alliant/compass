/*------------------------------------------------------------------------
@name IsClaimPolicyInUse
@description Checks if the given policy is in use under another open claim.
@param &PolicyID;char;ID of the policy to verify
@param &ClaimID;int;Claim ID to which the policy is to be assigned
@throws 3005;Error returned that the policy is in use
@author Bryan Johnson
@version 1.0 3/17/2017
@notes
----------------------------------------------------------------------*/
 
{lib/isnumeric.i}

def buffer xxpolicy for policy.
def buffer xxclaimcoverage for claimcoverage.
def buffer xxclaim for claim.
 
/* Verify that the policy is not currently in use under another open claim */
if isNumeric({&PolicyID}) then
do:
   if can-find(first xxpolicy where xxpolicy.policyid = int({&PolicyID})) then
   for each xxclaimcoverage where xxclaimcoverage.coverageID = {&PolicyID}
                            and xxclaimcoverage.claimID <> {&ClaimID}
                            no-lock,
       each xxclaim where xxclaim.claimID = xxclaimcoverage.claimID 
                    and xxclaim.stat = "O" 
                    no-lock:

      pResponse:fault("3005", "Policy " + {&PolicyID} + " is already assigned to open claim " + string(xxclaim.claimID)). /* Error */
      return.            
   end.
end.
