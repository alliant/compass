/* lib/islocked.i
   check to see if a business object IS LOCKED
   D.Sinclair
   5.9.2014
   Params:
     obj    Name of business object (char)
     id     Key field value (char)
     seq    Key field value (int)           - optional (default = 0)
     lock   Output true = locked (logical)  - optional (default = std-lo)
     uid    Output locked by (char)         - optional (default = std-ch)
     
     response       Response object name    - optional (default = pResponse)
     fault          MessageID               - optional (default = 3081)
     exclude-fault  defined to not call fault() on response
     exclude-return defined to not check isFault() on response and return
 */
    
&IF DEFINED(lock) = 0 &THEN
&SCOPED-DEFINE lock std-lo
&ENDIF
&IF DEFINED(uid) = 0 &THEN
&SCOPED-DEFINE uid std-ch
&ENDIF
&IF DEFINED(lockdate) = 0 &THEN
&SCOPED-DEFINE lockdate std-da
&ENDIF
&IF DEFINED(seq) = 0 &THEN
&SCOPED-DEFINE seq 0
&ENDIF

run util/issyslock.p ({&obj}, {&id}, {&seq}, output {&lock}, output {&uid}, output {&lockdate}).

&IF DEFINED(response) = 0 &THEN
&SCOPED-DEFINE response pResponse
&ENDIF
&IF DEFINED(fault) = 0 &THEN
&SCOPED-DEFINE fault 3081
&ENDIF
&IF DEFINED(exclude-fault) = 0 &THEN
if {&lock}
 then 
  DO: {&response}:fault("{&fault}", {&obj} {&msg-add} {&uid}).
      &IF DEFINED(exclude-return) = 0 &THEN
      return.
      &ENDIF
  END.
&ENDIF
