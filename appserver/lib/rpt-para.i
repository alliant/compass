&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : lib/rpt-para.i
    Purpose     : standard RePorT PARAgraph function (paragraph)
    Author(s)   : D.Sinclair
    Created     : 9.12.2011
    Notes       :
    Modified    :
    09/13/17 AG - Fixed infinite loop problem
  ----------------------------------------------------------------------*/
function paragraph returns logical private
  ( tText as char,
    tLength as int,
    tFirstIndent as int,
    tIndent as int,
    pNewLineAtEnd as logical) :
 
 def var tFirst as logical init true.

 /* das: Put &br; back in as a break placeholder */
 tText = replace(tText, chr(10), "&br;").
 tText = replace(tText, "&#10;", "&br;").

 tIndent = max(1, tIndent).
 if tFirstIndent <= 0 
  then tFirstIndent = tIndent.

 do while tText > "": 
  std-in = index(tText, "&br;").
  if std-in > 0
    and std-in <= tLength 
   then /* use shortened text, remove it, remove break, continue */
    do: std-ch = substring(tText, 1, std-in - 1).
        tText = if length(tText) > std-in + 3
                 then substring(tText, std-in + 4)
                 else "".
    end.
   else
  if length(tText) > tLength
   then 
    do: std-in = r-index(tText, " ", tLength).
        if std-in = 0 then
          std-in = tLength.
        std-ch = substring(tText, 1, std-in).
        tText = if length(tText) > std-in
                 then substring(tText, std-in + 1)
                 else "".
    end.
   else 
    do: std-ch = tText.
        tText = "".
    end.
  textAt(std-ch, if tFirst then tFirstIndent else tIndent).
  if (tText > "") or (tText = "" and pNewLineAtEnd)
   then newLine(1).
  tFirst = false.
 end.

 RETURN true.   /* Function return value. */
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


