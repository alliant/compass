/* lib/canfindbatchdoc.i
   9.19.2014 D.Sinclair
   @param First unnamed parameter should be the BatchID
 */
can-find(first sysdoc
           where sysdoc.entityType = "Batch"
             and sysdoc.entityID = string({1})
             and sysdoc.entitySeq = 0)
