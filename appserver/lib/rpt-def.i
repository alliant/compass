&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : lib/rpt-def.i
    Purpose     : standard RePorT DEFinitions
    Author(s)   : D.Sinclair
    Created     : 9.12.2011
    Parameters  : &lines:   Number of lines
                            12pt = 56
                            8pt = 85

                  &ID:      Temporary File key (used for temp file management)
                  
                  &rptOrientation 
                            "Portrait" or "Landscape"
  ----------------------------------------------------------------------*/

{lib/std-def.i}
{pdf/pdf_inc.i "this-procedure"}

define variable activeFilename as character.
define variable activeFont as character.
define variable activeSize as decimal.
define variable rptOrientation as character.
define variable iPage as integer.
define variable iLine as integer.
define variable iColumn as integer.
define variable cTableName as character.
define variable iWidthTotal as integer.

/* PDF Stream name */
&global-define rpt "S"

&IF defined(ID) <> 0 &THEN
&global-define tempID {&ID}
&ENDIF

&IF defined(rptOrientation) = 0 &THEN
&global-define rptOrientation "Portrait"
&ENDIF

rptOrientation = {&rptOrientation}.

function setFont returns logical private
  (pFontName as char,
   pFontSize as decimal) forward.
{lib/rpt-setfont.i {&*}}

function textColor returns logical private
 (pRedValue as decimal,
  pGreenValue as decimal,
  pBlueValue as decimal) forward.
{lib/rpt-textcolor.i {&*}}

function newPage returns logical private () forward.
{lib/rpt-newpage.i {&*}}

function newLine returns logical private
  (pLineCount as int) forward.
{lib/rpt-newline.i {&*}}

function textAt returns logical private
 (pText as char,
  pColumn as int) forward.
{lib/rpt-textat.i {&*}}

function textTo returns logical private
 (pText as char,
  pColumn as int) forward.
{lib/rpt-textto.i {&*}}

function paragraph returns logical private
 (pText as char,
  pLengthOfLine as int,
  pFirstLineIndentChars as int,
  pRemainingLineIndentChars as int,
  pNewLineAtEnd as logical) forward.
{lib/rpt-para.i {&*}}

function blockTitle returns logical private
 (pTitle as char) forward.
{lib/rpt-blocktitle.i {&*}}

function wordWrap returns integer private
 ( input pString as character,
   input pWrapWidth as integer,
   input pWrapIndent as integer ) forward.
{lib/rpt-wordwrap.i {&*}}

function createLine returns logical private
 ( input posY as integer ) forward.
{lib/rpt-createline.i {&*}}

{lib/rpt-table.i {&*}}

/* run startReport. (in lib/rpt-open.i)
   
   Sets up image for header.
   Establishes reportHeader routine to be called on new page
   Established reportFooter routine to be called on each page
   */
{lib/rpt-start.i {&*}}

/* run endReport. (in lib/rpt-cloz.i) */
{lib/rpt-end.i {&*}}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


