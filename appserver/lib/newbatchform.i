/* lib/newbatchform.i
   function to create a NEW BATCHFORM record
   This function is intended to be called from within an enclosing transaction block.
   Created 5.7.2014 D.Sinclair
   2.25.2015 D.Sinclair Use variables everywhere rather than relying on the values in the record buffers
  Modified: 
  Date          Name         Comments
  08/09/2019    Vikas Jain   added two new fields formCount, revenueType
  09/03/2019    Vikas Jain   added new field, fileID
  11/01/2019    Rahul Sharma modified code to process agentfile
  01/27/2020    Vikas Jain   Modified for CPL processing
  02/08/2021    MK           Modified for Committment processing
 */
 
 /* AR Module Standard Library File */
 {lib/ar-def.i}
 
 /* Temp-table for creating agentfile record */
 /* {tt/agentfile.i &tableAlias="ttagentfile"} */
  
  FUNCTION newBatchForm RETURNS LOGICAL PRIVATE
    (input pcBatchID as int,
     input piSeq as int,
     input pcFormType as char,
     input pcFileNumber as char,
     input piPolicyID as int,
     input pcRateCode as char,
     input pcFormID as char,
     input pcStatCode as char,
     input pdtmEffDate as datetime,
     input pdLiabilityAmount as dec,
     input pcCountyID as char,
     input plResidential as log,
     input pdGrossPremium as dec,
     input pdNetPremium as dec,
     input pcZipCode as char,
     input pcValidForm as char,
     input pcValidMsg as char,
     input piFormCount as int,
     input pcRevenueType as char,
     output pcErr as char,
     output plRedo as log):

    def var pSuccess AS LOGICAL INIT FALSE NO-UNDO.

    def buffer batch for batch.
    def buffer policy for policy.
    def buffer endorsement for endorsement.

    def buffer origBatchForm for batchForm.
    def buffer newBatchForm for batchForm.
    def buffer policyBatchForm for batchForm.

    def var dRetentionPremium as dec no-undo.
    def var dLiabilityDelta as dec no-undo.
    def var dGrossDelta as dec no-undo.
    def var dNetDelta as dec no-undo.
    def var dRetentionDelta as dec no-undo.

    def var lValidPolicy as log no-undo init TRUE.
    def var lPolicyResidential as log init no.
    def var lPolicyFormFound as log init no.
    def var lNewFile as logi no-undo init true.

    def var cFileID as char no-undo.
    def var iAgentFileFormID as integer no-undo.

    define variable lUpdError               as logical   no-undo.
    define variable cUpdMsg                 as character no-undo.

    pcFileNumber = normalizeFileNumber(pcFileNumber).
    cFileID      = normalizeFileID(pcFileNumber).
    
    if lookup(pcFormType,"P,E") <> 0 then
    do:
        lValidPolicy = FALSE.
        for FIRST policy fields (policyID stat) no-lock
          where policy.policyID = piPolicyID:
         plRedo = (policy.stat = "P").
         lValidPolicy = TRUE.
        end.
        /* We're going to allow the creation of a batchform record even if
           the policy is not valid.  The batch however will not be allowed 
           to move to reviewing/complete until the error is corrected or
           a policy has been manually created.
        */
        /* if not lValidPolicy                                                */
        /*  then                                                              */
        /*   do: pcErr = "Policy " + string(piPolicyID) + " does not exist.". */
        /*       return false.                                                */
        /*   end.                                                             */
    end.

    /* Force liability, gross and net premium to 2 decimal places (BEJ 10.13.2015) */
    assign
      pdLiabilityAmount = round(pdLiabilityAmount,2)
      pdGrossPremium    = round(pdGrossPremium,2)
      pdNetPremium      = truncate(pdNetPremium,2).

    pSuccess = false.

    TRX-BLOCK:
    for FIRST batch exclusive-lock
      where batch.batchID = pcBatchID
       on error undo TRX-BLOCK, leave TRX-BLOCK:
      case pcFormType:
        when "P" /* Policy */
         then
          do: /* Initially set the deltas to the values entered during processing */
              assign
                dLiabilityDelta = pdLiabilityAmount
                dGrossDelta = pdGrossPremium
                dNetDelta = pdNetPremium
                dRetentionDelta = (pdGrossPremium - pdNetPremium)
                .
              
              /* Adjust deltas against "current" values, if available */
              for first policy fields (policyID liabilityAmount grossPremium netPremium retention) no-lock
                where policy.policyID = piPolicyID:
                assign
                  dLiabilityDelta = pdLiabilityAmount - policy.liabilityAmount
                  dGrossDelta = pdGrossPremium - policy.grossPremium
                  dNetDelta = pdNetPremium - policy.netPremium
                  dRetentionDelta = dRetentionDelta - policy.retention
                  .
              end.
          end.

        when "E"  /* Endorsement */
         then
          do: /* Initially set the deltas to the values entered during processing */
              assign
                pdLiabilityAmount = 0 /* Not set for an endorsement */
                pcCountyID = ""       /* Not set for an endorsement */
                dLiabilityDelta = 0   /* Not set for an endorsement */

                dGrossDelta = pdGrossPremium
                dNetDelta = pdNetPremium
                dRetentionDelta = (pdGrossPremium - pdNetPremium)
                .

              /* Adjust deltas against "current" values, if available */
              for first endorsement fields (policyID formID effDate grossPremium netPremium retentionPremium) no-lock
                where endorsement.policyID = piPolicyID
                  and endorsement.formID = pcFormID 
                  and endorsement.effDate = pdtmEffDate:
                assign
                  dGrossDelta = pdGrossPremium - endorsement.grossPremium
                  dNetDelta = pdNetPremium - endorsement.netPremium
                  dRetentionDelta = dRetentionDelta - endorsement.retentionPremium
                  .
              end. 
          end.

        /* Calculate delta using previous amounts reported in agentFileForm table */ 
        when "C" or  /* CPL */
        when "T"    /* Commitment */
         then
          do:  
            assign
              dLiabilityDelta = pdLiabilityAmount
              dGrossDelta     = pdGrossPremium
              dNetDelta       = pdNetPremium
              dRetentionDelta = (pdGrossPremium - pdNetPremium)
              .
              
            for first agentfileform 
              where agentfileform.agentID  = batch.agentID and
                    agentfileform.fileID   = cFileID       and
                    agentfileform.formID   = pcFormID      and
                    agentfileform.formType = pcFormType:
                      
                assign
                  dLiabilityDelta = pdLiabilityAmount - agentfileform.liabilityAmoun
                  dGrossDelta     = pdGrossPremium    - agentfileform.grossPremium
                  dNetDelta       = pdNetPremium      - agentfileform.netPremium
                  dRetentionDelta = dRetentionDelta   - agentfileform.retentionPremium 
                  .            
              end.                      
          end.
        /* For future use... */
        when "S" then . /* Search */
        when "D" then . /* Schedule */
        when "M" then . /* Misc */
      end case.


      /* Set related forms to "E"rrors if this one is the basis for the error */
      if lookup(pcFormType,"P,E") <> 0 
       then
        do:
          if pcValidForm = "E"
           then
            do:
                lValidPolicy = FALSE.
          
                for each origBatchForm exclusive-lock
                  where origBatchForm.batchID = pcBatchID
                    and origBatchForm.policyID = piPolicyID
                    and origBatchForm.validPolicy = TRUE
                  on error undo TRX-BLOCK, leave TRX-BLOCK:
                  origBatchForm.validPolicy = FALSE.
                  validate origBatchForm.
                end.
            end.  /* if pcValidForm = "E" */

            /* If this one is not an "E"rror, see if there is a related form in the
               batch that is an error and set this one if necessary */
          else lValidPolicy = not can-find(first origBatchForm
                                           where origBatchForm.batchID = pcBatchID
                                             and origBatchForm.policyID = piPolicyID
                                             and origBatchForm.validPolicy = FALSE).
      
          for first policyBatchForm fields (batchID policyID formType residential) no-lock
            where policyBatchForm.batchID = pcBatchID
              and policyBatchForm.policyID = piPolicyID
              and policyBatchForm.formType = "P":
            assign
              lPolicyFormFound   = yes
              lPolicyResidential = policyBatchForm.residential.
          end.
        end.
            
      dRetentionPremium = (pdGrossPremium - pdNetPremium).

      create newBatchForm.
      assign
        newBatchForm.batchID = pcBatchID
        newBatchForm.seq = piSeq

        newBatchForm.formType = substring(trim(pcFormType),1,1)
        newBatchForm.fileNumber = pcFileNumber
        newBatchForm.fileID = cFileID 
        newBatchForm.formID = pcFormID
        newBatchForm.formCount = if piFormCount <  1 then 1 else piFormCount        

        newBatchForm.countyID = pcCountyID
        newBatchForm.residential = if lPolicyFormFound = yes 
                                    then lPolicyResidential 
                                    else plResidential

        newBatchForm.muniID = ""

        newBatchForm.statCode = pcStatCode

        newBatchForm.liabilityAmount = pdLiabilityAmount
        newBatchForm.grossPremium = pdGrossPremium
        newBatchForm.netPremium = pdNetPremium
        newBatchForm.retentionPremium = dRetentionPremium
        newBatchForm.zipcode = (if pcFormType = "P"
                                and pcZipCode <> ? and pcZipCode <> "0" 
                                then pcZipCode else "")

        newBatchForm.liabilityDelta = dLiabilityDelta
        newBatchForm.grossDelta = dGrossDelta
        newBatchForm.netDelta = dNetDelta
        newBatchForm.retentionDelta = dRetentionDelta
        newBatchForm.revenueType = pcRevenueType
        newBatchForm.reprocess = plRedo

        newBatchForm.validForm = substring(trim(pcValidForm),1,1)
        newBatchForm.validMsg = pcValidMsg
        newBatchForm.validPolicy = lValidPolicy
        newBatchForm.rateCode = substring(trim(pcRateCode),1,1)
        
        newBatchForm.createdDate = now
        .

      if lookup(pcFormType,"P,E") <> 0
       then
        assign
          newBatchForm.policyID = piPolicyID
          newBatchForm.effDate = pdtmEffDate
          .
        
      validate newBatchForm.

      /* Add new values into aggregations on batch record */
      assign
        batch.liabilityAmount = batch.liabilityAmount + pdLiabilityAmount
        batch.grossPremiumProcessed = batch.grossPremiumProcessed + pdGrossPremium
        batch.netPremiumProcessed = batch.netPremiumProcessed + pdNetPremium

        batch.retainedPremiumProcessed = batch.retainedPremiumProcessed + dRetentionPremium

        batch.liabilityDelta = batch.liabilityDelta + dLiabilityDelta
        batch.grossPremiumDelta = batch.grossPremiumDelta + dGrossDelta
        batch.netPremiumDelta = batch.netPremiumDelta + dNetDelta
        batch.retainedPremiumDelta = batch.retainedPremiumDelta + dRetentionDelta /* das */
        .

      if pcFormType = "P" 
       then batch.policyCount = batch.policyCount + 1.
       else
      if pcFormType = "E" 
       then batch.endorsementCount = batch.endorsementCount + 1.
       else
      if pcFormType = "C"
       then batch.cplCount = batch.cplCount + (if piFormCount <  1 then 1 else piFormCount).

      for first origBatchForm fields (batchID fileID seq) no-lock
        where origBatchForm.batchID = pcBatchID
          and origBatchForm.fileID = cFileID
          and origBatchForm.seq <> piSeq:
       lNewFile = false.
      end.
      if lNewFile
       then batch.fileCount = batch.fileCount + 1.
       
/* Moved code of create agentfile to invoicebatch.p discussed on 30-12-19 */
/*      /* Processing agentfile record and creating sysnotes */                                                            */
/*      if pcFileNumber <> ""                                                                                              */
/*       then                                                                                                              */
/*        do:                                                                                                              */
/*          empty temp-table ttagentfile.                                                                                  */
/*          create ttagentfile.                                                                                            */
/*          assign                                                                                                         */
/*              ttagentfile.agentID         = batch.agentID                                                                */
/*              ttagentfile.fileNumber      = pcFileNumber                                                                 */
/*              ttagentfile.stat            = {&Open}                                                                      */
/*              ttagentfile.stage           = pRequest:actionid                                                            */
/*              ttagentfile.transactionType = if newBatchForm.residential then "Residential" else "Commercial"             */
/*              ttagentfile.reportingDate   = if (batch.receivedDate = ?) then batch.createDate else batch.receivedDate    */
/*              ttagentfile.insuredType     = newBatchForm.insuredType                                                     */
/*              ttagentfile.notes           = pRequest:actionid + "," + pRequest:clientid + "," +                          */
/*                                           "Created batchform with form ID " + newBatchForm.formID + " for policy ID " + */
/*                                           string(piPolicyID) + " and batch " + string(pcBatchID)                        */
/*              .                                                                                                          */
/*                                                                                                                         */
/*          run util/updateagentfile.p (input pRequest:actionID,                                                           */
/*                                      input pRequest:clientID,                                                           */
/*                                      input pRequest:uid,                                                                */
/*                                      input table ttagentfile,                                                           */
/*                                      output lUpdError,                                                                  */
/*                                      output cUpdMsg).                                                                   */
/*                                                                                                                         */
/*        end. /* if pcFileNumber <> "" then */                                                                            */
      
      validate batch.

      pSuccess = true.
    end. 

    return pSuccess.
  END FUNCTION.
