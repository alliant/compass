/*------------------------------------------------------------------------
    File        : lib/queueAction.i
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : MK
    Created     : 06/25/21
    Notes       :
    @Modified    :
    Date        Name    Comments
    01/21/2022  SC      Validate sysdests before adding them to queue. 
                        Commented code is placeholder for future validation for Undefined Entities.
  ----------------------------------------------------------------------*/
/******************************************************** Definitions *******************************************************/
&if defined(q-entityTyp) = 0 &then
&scoped-define q-entityTyp ""
&endif

&if defined(q-entityIdList) = 0 &then
&scoped-define q-entityIdList ""
&endif

&if defined(q-def-defined) = 0 &then

 define variable q-success                as logical   no-undo.
 define variable q-msg                    as character no-undo.

 define variable q-printerAddr            as character no-undo.
 define variable q-parameters             as character no-undo.
 define variable q-destinations           as character no-undo.
 define variable q-command                as character no-undo.
 define variable q-faultEntityID          as character no-undo.

 define variable q-hAddAction             as handle    no-undo.
 define variable iSeq                     as integer   no-undo initial 0.
 define variable q-lNoSysDest             as logical   no-undo initial false.
 define variable q-lStat                  as logical   no-undo initial false.
 
 define variable q-DestCount       as integer   no-undo.
 define variable q-allDestns       as character no-undo.
 define variable q-DestKeyVal      as character no-undo.
 define variable q-DestKey         as character no-undo.   
 define variable q-DestVal         as character no-undo.
 define variable q-DestEmail       as character no-undo.
 define variable q-DestPrinter     as character no-undo.
 define variable q-DestFileStored  as character no-undo.
 define variable q-DestStorage     as character no-undo.
 define variable q-EntityID        as character no-undo.
 define variable q-Count           as integer no-undo.
 define variable q-actionDesc      as character no-undo.
 define variable q-entityDesc      as character no-undo.
 /* define variable q-Entity          as character no-undo. */
 define variable q-fltEmailEntities       as character no-undo.
 define variable q-fltEmails       as character no-undo.
 define variable q-fltPrinterEntities     as character no-undo.
 define variable q-fltPrinters     as character no-undo.
 define variable q-fltFolderEntities  as character no-undo.
 define variable q-fltFolders      as character no-undo.
 define variable q-fltStorageEntities     as character no-undo.
 /* define variable q-fltUndefinedEntities     as character no-undo. */
 define variable q-invalidDestEntities     as character no-undo.
 define variable q-URLStorage      as character no-undo.
 define variable q-ToEmail         as character no-undo.
 define variable q-AlertMsg        as character no-undo.
 define variable q-lInvalidDest     as logical   no-undo initial false.
 
 /* Functions and Procedures ---                                         */
 {lib/add-delimiter.i}
 {lib/addActionToQueue.i}
 {lib/validEmailAddr.i}

 &global-define q-def-defined true

&endif   

{lib/std-def.i}
{lib/ar-def.i}

function setActionAttributes returns logical forward.

/* destination validations */
if pRequest:printerAddr <> ""
 then 
  for each sysprop fields(objValue) no-lock 
    where sysprop.appCode     = {&printappcode}     
      and sysprop.objAction   = {&printobjAction}   
      and sysprop.objProperty = {&printProperty}    
      and sysprop.objID       = pRequest:printerAddr:    
    q-PrinterAddr = sysprop.objValue.
  end.

/* validation */
if pRequest:destination = "S" and pRequest:emailAddrList = "" and q-printerAddr = ""
 then
  pResponse:fault("3001", "Destination ").     
  
if pRequest:printerAddr <> "" and q-printerAddr = ""
 then
  pResponse:fault("3001", "Printer ").

if pRequest:emailAddrList <> "" and not(validEmailAddr(pRequest:emailAddrList))
 then 
  pResponse:fault("3001", "Email ID " ).
  
assign
    q-fltEmailEntities   = ""
    q-fltPrinterEntities = ""
    q-fltFolderEntities  = ""
    q-fltStorageEntities = ""
    /* q-fltUndefinedEntities = "" */
    q-fltEmails          = ""
    q-fltPrinters        = ""
    q-fltFolders         = ""
    q-alldestns          = ""
    q-DestKeyVal         = ""
    q-DestKey            =  ""
    q-DestVal            =  ""
    q-DestEmail          = ""
    q-DestPrinter        = ""
    q-DestFileStored     = ""
    q-DestStorage        = ""
    q-faultEntityID      = ""
    q-invalidDestEntities = ""
    q-lNoSysDest          = false
    q-lInvalidDest        = false
    .
  
/* Checking system destination records */
if pRequest:destination = "C"
 then
  do q-Count = 1 to num-entries({&q-entityIdList}):
       
    q-EntityID = entry(q-Count, ({&q-entityIdList})).

    if lookup(q-EntityID,q-faultEntityID) > 0
     then next.

    assign
        q-DestEmail      = ""
        q-DestPrinter    = ""
        q-DestFileStored = ""
        q-DestStorage    = ""
        .

    if not can-find(first sysdest where sysdest.entityType  = {&q-entityTyp}
                                    and sysdest.entityID    = q-EntityID /* agent ID */
                                    and sysdest.action      = pRequest:actionId) and
       not can-find(first sysdest where sysdest.entityType  = {&q-entityTyp}
                                    and sysdest.entityID    = "" /* agent ID */
                                    and sysdest.action      = pRequest:actionId)
     then
      do:
        assign
            q-lNoSysDest    = true
            q-faultEntityID = q-faultEntityID + (if q-faultEntityID > "" then "," else "") + q-EntityID. /* Entities whose destination not set */
            .
        next.
      end.
	/* Validating all configured destinations */
    {lib/get-sysdest.i &entityType ={&q-entityTyp} &entityID=q-EntityID &action=pRequest:actionId &OutFormat=pRequest:outputFileFormat  &outdest=q-alldestns}

	q-alldestns = trim(q-alldestns,"^").

    do q-DestCount = 1 to num-entries(q-alldestns,"^"):
      assign
          q-DestKeyVal = entry(q-DestCount, q-alldestns, "^")
          q-DestKey    =  entry(1, q-DestKeyVal, "=")
          q-DestVal    =  entry(2, q-DestKeyVal, "=")
          .

      case q-DestKey:
        when 'E' 
         then
          q-DestEmail = q-DestVal.
        when 'P' 
         then
          q-DestPrinter = q-DestVal.
        when 'F' 
         then
          q-DestFileStored = q-DestVal.
        when 'S' 
         then
          q-DestStorage = q-DestVal.  
      end case.
    end.
    
    if q-DestEmail <> "" and not(validEmailAddr(q-DestEmail))
     then 
      assign
          q-fltEmailEntities = q-fltEmailEntities + (if q-fltEmailEntities > "" then "," else "") + q-EntityID
          q-fltEmails        = q-fltEmails + (if q-fltEmails > "" then "," else "") + q-DestEmail
          .
      
    if q-DestPrinter <> "" and not can-find(first sysprop  
        where sysprop.appCode     = {&printappcode}     
          and sysprop.objAction   = {&printobjAction}   
          and sysprop.objProperty = {&printProperty}    
          and sysprop.objValue    = q-DestPrinter)     
     then
      assign
          q-fltPrinterEntities = q-fltPrinterEntities + (if q-fltPrinterEntities > "" then "," else "") + q-EntityID
          q-fltPrinters = q-fltPrinters + (if q-fltPrinters > "" then "," else "") + q-DestPrinter
          .
      
    if q-DestFileStored <> ""
     then
      do:
        file-info:file-name = q-DestFileStored.
        if file-info:full-pathname = ?
         then
          assign
              q-fltFolderEntities = q-fltFolderEntities + (if q-fltFolderEntities > "" then "," else "") + q-EntityID
              q-fltFolders = q-fltFolders + (if q-fltFolders > "" then "," else "") + q-DestFileStored
              .
      end.
      
    if q-DestStorage <> ""  
     then
      do:
        publish "GetSystemParameter" ("ARCRepositoryUpload", OUTPUT q-URLStorage).
        /* publish "GetSystemParameter" (".....", OUTPUT q-entity). */
        
        if q-URLStorage = "" or q-URLStorage = ?
         then
          q-fltStorageEntities = q-fltStorageEntities + (if q-fltStorageEntities > "" then "," else "") + q-EntityID. 
          
        /* if q-entity = "" or q-entity = ?       /* lookup function*/
         then
          do:
            q-fltUndefinedEntities = q-fltUndefinedEntities + (if q-fltUndefinedEntities > "" then "," else "") + q-EntityID.
          end.  */
      end.
      
      if lookup(q-EntityID,q-fltEmailEntities)   > 0   
      or lookup(q-EntityID,q-fltPrinterEntities) > 0
      or lookup(q-EntityID,q-fltFolderEntities)  > 0 
      or lookup(q-EntityID,q-fltStorageEntities) > 0
      /* or lookup(q-EntityID,q-fltUndefinedEntities)   > 0  */
       then
        assign
            q-lInvalidDest          = true
            q-invalidDestEntities = q-invalidDestEntities + (if q-invalidDestEntities > "" then "," else "") + q-EntityID
            .             
   end.
/* Sending failure email for the agent ID's whose system destination is not set */
if q-lNoSysDest or q-lInvalidDest
 then
  do:
    publish "GetSystemParameter" ("AlertsTo", output q-ToEmail).
    
    if not can-do(q-ToEmail,pRequest:uid)
     then
      q-ToEmail  = q-ToEmail + ";" + pRequest:uid.

    case {&q-entityTyp}:
      when 'A'  
       then
        q-entityDesc = 'Auditor'.
      when 'C'  
       then
        q-entityDesc = 'Company'.
      when 'G'  
       then
        q-entityDesc = 'Agent'.  
    end case.
    
    assign
        q-ToEmail  = trim(q-ToEmail,",;")
        q-AlertMsg = (if q-faultEntityID eq "" 
                               then "" 
                               else 
                                ("Default as well as specific system destination is not set for the action " + pRequest:actionId + " and " + q-entityDesc + " ID(s) '" + q-faultEntityID + "'. <br>"))
                             + (if q-fltEmailEntities eq "" 
                                then "" 
                                else 
                                 ("Email address(es) '" + q-fltEmails + "' are invalid for " + q-entityDesc + 
                                    (if {&q-entityTyp} = 'C' then "" else  (" ID(s) '" + q-fltEmailEntities + "' respectively")) 
                                   + ". <br>")) 
                             + (if q-fltPrinterEntities eq "" 
                                then "" 
                                else
                                ("Printer location(s) '" + q-fltPrinters + "' are invalid for " + q-entityDesc + " ID(s) '" + q-fltPrinterEntities + "' respectively. <br>"))
                             + (if q-fltFolderEntities eq "" 
                                then "" 
                                else
                                ("File storage path(s) '" + q-fltFolders + "' are invalid for " + q-entityDesc + " ID(s) '" + q-fltFolderEntities + "' respectively. <br>"))
                             + (if q-fltStorageEntities eq "" 
                                then "" 
                                else
                                ("ARC repository URL(s) are invalid for " + q-entityDesc +
                                  (if {&q-entityTyp} = 'C' then "" else (" ID(s) '" + q-fltStorageEntities + "'")) 
                                  + ". <br>"))                             
                             /* + (if q-fltUndefinedEntities eq ""                 /* lookup */
                                then "" 
                                else
                                ("Entity is not defined for ARC repository for action "                + pRequest:actionId + " and entity(s) " + q-fltStorageEntities + ".")) */
                             + "<br>Unable to send output for action '" + (if (pRequest:displayName eq "") or (pRequest:displayName eq ?) then "" else pRequest:displayName) + "' (" + pRequest:actionId + ")."    
                       
                      .
    run util/simplemail.p (input "", 
                           input q-ToEmail,
                           input "Unable to generate " + (if (pRequest:displayName eq "") or (pRequest:displayName eq ?) then "output" else pRequest:displayName),
                           input q-AlertMsg).  
  
        
    if num-entries({&q-entityIdList}) = num-entries(q-faultEntityID) +  num-entries(q-invalidDestEntities)
     then
      pResponse:fault("3005", "Action " + pRequest:actionId + " failed due to no system destination available for the entities, please check email for further details.").           
  end.      
       
if pResponse:isFault()
 then 
  return.
 
TRX-BLK:
do transaction
  on error undo TRX-BLK, leave TRX-BLK: 
  
   run addactiontoqueue.p persistent set q-hAddAction (input  pRequest:uid,
                                                       input  pRequest:actionId, /* input action to .i*/
                                                       input  pRequest:clientID,
                                                       input  logical(pRequest:notifyRequestor),    /* Notify Requestor */
                                                       output q-success,
                                                       output q-msg) no-error.

   if error-status:error
    then 
     do:
       pResponse:fault("3005", "Unable to add action to the system queue. Please contact the system administrator.").
       leave TRX-BLK.
     end.  
     
   if not q-success 
    then
     do:
       pResponse:fault("3005", q-msg).
       leave TRX-BLK. 
     end.
     
   q-lStat = setActionAttributes() no-error.
   
   if error-status:error or not q-lStat
    then 
     do:
       pResponse:fault("3005", "Unable to add action to the system queue. Please contact the system administrator.").
       leave TRX-BLK.
     end.
end.

if pResponse:isFault()
 then
  do:
    if valid-handle(q-hAddAction) 
     then
      do:
        delete procedure q-hAddAction.
        q-hAddAction = ?.
      end.
    return.
  end.
    
if valid-handle(q-hAddAction) 
 then
  do:
    run QueueAction in q-hAddAction (output q-success,
                                     output q-msg).
                                   
    
    delete procedure q-hAddAction no-error.
    q-hAddAction = ?.
    
    if not q-success
     then 
      do:
        pResponse:fault("3005", q-msg).
        return.
      end.
  end.
 else
  do:
   pResponse:fault("3005", "Unable to add action to the system queue. Please contact the system administrator.").
   return.
  end.

/* default response messages */  
&if defined(excludeSuccess) = 0 &then
 run util/msgsubs.p ("2013", string(iSeq) {&msg-add} "Queue Item", output q-msg).
 pResponse:setParameter("Message", q-msg).
 pResponse:success("2013", string(iSeq) {&msg-add} "Queue Item").
&endif

&if defined(q-func-defined) = 0 &then 
function hasInvalidDest returns logical
  ( input charEntityID as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if (lookup(charEntityID,q-faultEntityID) > 0) or (lookup(charEntityID,q-invalidDestEntities) > 0) 
   then
    return true.
  return false.
end function.
&global-define q-func-defined true
&endif  

