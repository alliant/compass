/*------------------------------------------------------------------------
    File        : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : Vignesh Rajan
    Created     : 02/14/23
    Notes       :
    Modified    :
    Date          Name     Description
    
  ----------------------------------------------------------------------*/
  
  
method public void removeNullValues(input pTable as character):

  define variable hQuery    as handle   no-undo.
  define variable hBuffer   as handle   no-undo.
  define variable iFieldCnt as integer  no-undo.
  define variable lcValue   as longchar no-undo.

  create buffer hBuffer for table pTable.
  create query hQuery.
  hQuery:set-buffers(hBuffer). 
  hQuery:query-prepare("for each " + pTable + " no-lock").
  hQuery:query-open().
  hQuery:get-first().

  repeat while not hQuery:query-off-end:
    do iFieldCnt = 1 to hBuffer:num-fields:

      if hBuffer:buffer-field(iFieldCnt):data-type = "DATE" 
       then
        next.

      if hBuffer:buffer-field(iFieldCnt):data-type = "CLOB" or
         hBuffer:buffer-field(iFieldCnt):buffer-value = ? or
         hBuffer:buffer-field(iFieldCnt):buffer-value = "?" 
       then
        do:
          case hBuffer:buffer-field(iFieldCnt):data-type:
            when "CLOB" 
             then 
              do:
                copy-lob hBuffer:buffer-field(iFieldCnt):buffer-value to lcValue.
                if lcValue = ? or lcValue = "?" then  lcValue = "".
                copy-lob lcValue to hBuffer:buffer-field(iFieldCnt):buffer-value.
              end.
            when "CHARACTER" then hBuffer:buffer-field(iFieldCnt):buffer-value = "".
            when "INTEGER" then hBuffer:buffer-field(iFieldCnt):buffer-value = 0.
            when "DECIMAL" then hBuffer:buffer-field(iFieldCnt):buffer-value = 0.00.
            when "LOGICAL" then hBuffer:buffer-field(iFieldCnt):buffer-value = false.
          end case.
        end.
    end.
    hQuery:get-next().
  end.

  if valid-handle(hQuery) then delete object hQuery.
  if valid-handle(hBuffer) then delete object hBuffer.

end method.
