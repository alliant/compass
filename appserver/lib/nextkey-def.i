&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/nextkey-def.i
   
  ----------------------------------------------------------------------*/
/* to hold the nextkey.i stored-procedure handle */


define variable nk-prochandle as integer   no-undo.
define variable nk-success    as logical   no-undo.
define variable nk-err        as character no-undo.
define variable nk-message    as character no-undo.
