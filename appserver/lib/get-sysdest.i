&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/get-sysdest.i
   Return list of system destination values of an entity
   Rahul Sharma
   02.07.2020
   Notes:    entityType   - Agent(G)/Company(C)
             entityID     - Entiy ID of an entity
             action       - Calling program action
   */

&if defined(entityType) = 0 &then
&scoped-define entityType "G"
&endif

&if defined(entityID) = 0 &then
&scoped-define entityID ""
&endif

&if defined(action) = 0 &then
&scoped-define action pRequest:actionid
&endif

&if defined(OutFormat) = 0 &then
&scoped-define outFormat "P"
&endif

{&outdest} = "".

if not can-find(first sysdest where sysdest.entityType = {&entityType} 
                                and sysdest.entityID   = {&entityID} 
                                and sysdest.action     = {&action})
 then
  for each sysdest no-lock
    where sysdest.entityType = {&entityType} 
      and sysdest.entityID   = "" 
      and sysdest.action     = {&action}:
           
    if {&outFormat} <> {&PDF} and sysdest.destType = {&printer}
     then next.
         
    {&outdest} = addDelimiter({&outdest}, {&msg-dlm}) + sysdest.destType + "=" + sysdest.destName.
  end.  
 else 
  for each sysdest no-lock
    where sysdest.entityType = {&entityType}
      and sysdest.entityID   = {&entityID}
      and sysdest.action     = {&action}:
           
    if {&outFormat} <> {&PDF} and sysdest.destType = {&printer}
     then next.
         
    {&outdest} = addDelimiter({&outdest}, {&msg-dlm}) + sysdest.destType + "=" + sysdest.destName.
  end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


