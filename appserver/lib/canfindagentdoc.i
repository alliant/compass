/* lib/canfindagentdoc.i
   9.19.2014 D.Sinclair
   @param First unnamed parameter should be the AgentID
 */
can-find(first sysdoc
           where sysdoc.entityType = "Agent"
             and sysdoc.entityID = {1}
             and sysdoc.entitySeq = 0)
