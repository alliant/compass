&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : lib/rpt-blocktitle.i
    Purpose     : standard RePorT BLOCK TITLE function (blockTitle)
    Author(s)   : D.Sinclair
    Created     : 9.12.2011
    Notes       :
    Modification:
    Date          Name      Description
    01/05/2017    AC        Fixed section headings  
   ----------------------------------------------------------------------*/
function blockTitle returns logical private
 ( pTitle as char ) :
 def var currentFont as char no-undo.
 def var currentSize as decimal no-undo.

 if iLine >= 52
  then newPage().
 newLine(1).
 
 assign
   currentFont = activeFont
   currentSize = activeSize
   .
 run pdf_set_font ({&rpt}, "Helvetica-Bold", 12.0).
/*  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "176196222").  */
/*  run pdf_rgb ({&rpt}, "pdf_stroke_color", "176196222"). */
 
 run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
 run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").

 run pdf_rect ({&rpt}, 50, 700 - ((iLine - 1) * 12), 511, 11, 5.0).
 run pdf_text_color ({&rpt}, 1.0, 1.0, 1.0).
/*  newLine(1).  */
 textAt(pTitle, 18).
 newLine(2).
 setFont(currentFont, currentSize). /* reset */
 return true.
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


