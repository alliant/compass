&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*---------------------------------------------------------------------
@name account-functions.i
@description Functions for the payable or receivable invoices

@param 

@author John Oliver
@version 1.0
@created 04/10/2016
@notes The description of each function is defined above the function
       definition
---------------------------------------------------------------------*/

/*----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

&scoped-define APTable apinv
&scoped-define ARTable arinv
&scoped-define approveTable apinva

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD HasAPInvoiceApproved Include 
FUNCTION HasAPInvoiceApproved RETURNS LOGICAL
  ( input pInvoiceID as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD HasAPInvoiceApprovedAll Include 
FUNCTION HasAPInvoiceApprovedAll RETURNS LOGICAL
  ( input pInvoiceID as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD HasAPInvoiceApprover Include 
FUNCTION HasAPInvoiceApprover RETURNS LOGICAL
  ( input pInvoiceID as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD IsAPInvoiceComplete Include 
FUNCTION IsAPInvoiceComplete RETURNS LOGICAL
  ( input pInvoiceID as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD IsAPInvoiceDenied Include 
FUNCTION IsAPInvoiceDenied RETURNS LOGICAL
  ( input pInvoiceID as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD IsAPInvoiceVoided Include 
FUNCTION IsAPInvoiceVoided RETURNS LOGICAL
  ( input pInvoiceID as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION HasAPInvoiceApproved Include 
FUNCTION HasAPInvoiceApproved RETURNS LOGICAL
  ( input pInvoiceID as integer ) :
/*------------------------------------------------------------------------------
@description Determines if at least one approver has approved or denied the payable invoice
@param Invoice;integer;The invoice ID
@return True if the invoice has at least one approver has approved or denied
------------------------------------------------------------------------------*/
  return can-find(first {&approveTable} no-lock 
                  where {&approveTable}.apinvID = pInvoiceID
                   and ({&approveTable}.stat = "A"
                    or  {&approveTable}.stat = "D")).
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION HasAPInvoiceApprovedAll Include 
FUNCTION HasAPInvoiceApprovedAll RETURNS LOGICAL
  ( input pInvoiceID as integer ) :
/*------------------------------------------------------------------------------
@description Determines if the all the approvers have approved the invoice
@param Invoice;integer;The invoice ID
@return True if the invoice has been fully approved
------------------------------------------------------------------------------*/
  define variable iApprovedCount as integer no-undo initial 0.
  define variable iTotalCount as integer no-undo initial 0.
  
  define variable hQuery as handle no-undo.
  define variable hBuffer as handle no-undo.
  
  create buffer hBuffer for table "{&approveTable}".
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each {&approveTable} where apinvID = " + string(pInvoiceID)).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    /* count the number of rows */
    iTotalCount = iTotalCount + 1.
    /* count the rows that have been approved */
    if hBuffer:buffer-field("stat"):string-value() = "A"
     then iApprovedCount = iApprovedCount + 1.
    hQuery:get-next().
  end.
  /* return true if the total row count matches the approved row count */
  if iTotalCount > 0 and iTotalCount = iApprovedCount
   then return true.
   
  return false.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION HasAPInvoiceApprover Include 
FUNCTION HasAPInvoiceApprover RETURNS LOGICAL
  ( input pInvoiceID as integer ) :
/*------------------------------------------------------------------------------
@description Determines if at least one approver assigned to the payable invoice
@param Invoice;integer;The invoice ID
@return True if the invoice has at least one approver
------------------------------------------------------------------------------*/
  return can-find(first {&approveTable} no-lock 
                  where {&approveTable}.apinvID = pInvoiceID).
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION IsAPInvoiceComplete Include 
FUNCTION IsAPInvoiceComplete RETURNS LOGICAL
  ( input pInvoiceID as integer ) :
/*------------------------------------------------------------------------------
@description Determines if the payable invoice is complete
@param Invoice;integer;The invoice ID
@return True if the invoice is complete
------------------------------------------------------------------------------*/
  return can-find(first {&APTable} no-lock
                  where {&APTable}.apinvID = pInvoiceID
                    and {&APTable}.stat = "C").

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION IsAPInvoiceDenied Include 
FUNCTION IsAPInvoiceDenied RETURNS LOGICAL
  ( input pInvoiceID as integer ) :
/*------------------------------------------------------------------------------
@description Determines if the payable invoice is denied
@param Invoice;integer;The invoice ID
@return True if the invoice is denied
------------------------------------------------------------------------------*/
  return can-find(first {&APTable} no-lock
                  where {&APTable}.apinvID = pInvoiceID
                    and {&APTable}.stat = "D").

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION IsAPInvoiceVoided Include 
FUNCTION IsAPInvoiceVoided RETURNS LOGICAL
  ( input pInvoiceID as integer ) :
/*------------------------------------------------------------------------------
@description Determines if the payable invoice is voided
@param Invoice;integer;The invoice ID
@return True if the invoice is voided
------------------------------------------------------------------------------*/
  return can-find(first {&APTable} no-lock
                  where {&APTable}.apinvID = pInvoiceID
                    and {&APTable}.stat = "V").

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

