&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : lib/nacheck.i
    Purpose     : server side vaidation for na 
    Author(s)   : Anjly Chanana
    Created     : 06-20-2018

  ----------------------------------------------------------------------*/

if ({&rateType} eq {&NAUI} and {&loanRateType} eq {&NAUI} and {&ownerEndors} ne "~{&ownerEndors~}" and {&ownerEndors} ne "" and {&loanEndors} ne "~{&loanEndors~}" and {&loanEndors} ne "")  then
  do:
    {&errorMsg} =  "Stand Alone endorsements can't be taken simultaneously.".
    {&errorStatus} = true.
    return.
  end.
if ({&rateType} ne {&none} and {&loanRateType} eq {&NAUI} and {&loanEndors} ne "~{&loanEndors~}" and {&loanEndors} ne "")  or 
   ({&rateType} eq {&NAUI} and {&loanRateType} ne {&none} and {&ownerEndors} ne "~{&ownerEndors~}" and {&ownerEndors} ne "" )  then
  do:
    {&errorMsg} =  "Stand Alone endorsements can't be taken simultaneously.".
    {&errorStatus} = true.
    return.
  end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 14.86
         WIDTH              = 44.2.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


