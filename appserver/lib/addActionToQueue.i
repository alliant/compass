/*------------------------------------------------------------------------
    File        : addActionToQueue.i
    Purpose     : Create Caret separator list of parameters

    Syntax      :

    Description :

    Author(s)   : Shefali Bhatt
    Created     : 03/26/2021
    Notes       :
  ----------------------------------------------------------------------*/
  
def var actionParameters as char no-undo.
&scoped-define actionParameterDelim "^"
&scoped-define actionParameterNVdelim "="

function resetParameters returns logical:
 actionParameters = "".
 return true.
end function.

function addactionParameter returns logical
(input cName as char,input cValue as char):
 actionParameters = actionParameters + (if actionParameters <> "" then {&actionParameterDelim} else "") + 
                    cName + {&actionParameterNVdelim} + cValue. 
 return true.
end function.

function getactionParameters returns char :
 return actionParameters.
end function.
