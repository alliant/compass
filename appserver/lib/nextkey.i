&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/nextkey.i
   return the NEXT primary KEY value for a pseudo-sequence
   D.Sinclair
   10.1.2012
   Notes:    type       - The sequence name
             var        - The field getting the "next" value (available for use)
             err        - Action to take if sequence unavailable
             tableAlias - Unique buffer for syskey [optional]
   It is assumed we are already inside a transaction block.

   4/10/2020 D.Sinclair - Added the option of tableAlias to handle multiple uses in the same transaction scope
   6/01/2021 J.Oliver   - Getting key from stored procedure that uses sequences
   2/03/2022 Shweta     - Call the stored procedure spGetNextSequence to get Next unique key from the 
                          System Sequence table and handle the erros while generating the sequence. 
*/

nk-success = false.
do on stop undo, leave:
 
  run stored-procedure spGetNextSequence nk-procHandle = proc-handle no-error (input param keyType = {&type}, output param key = -1).
  close stored-procedure spGetNextSequence where proc-handle = nk-procHandle.
  {&var} = spGetNextSequence.key.
  
  nk-success = true.
end.
  
if not nk-success or {&var} = -1 or error-status:error 
 then
  do:
    if error-status:error 
     then
      nk-message = "Unable to get next sequence {&type}.".
     else 
      nk-message = "Sequence {&type} is not defined.". 
    
    &if defined(noResponse) = 0 &then 
      pResponse:fault("3005", nk-message + " Please contact system administrator.").
    &else
      nk-err = nk-message + " Please contact system administrator.".
    &endif
   
    run util/sysmail.p(input "Error in Sequence",   /* Subject */
                       input if error-status:error then (nk-message + "<br><br>" + error-status:get-message(1)) else nk-message).     /* Body */ 
                                   
    &if defined(err) <> 0 &then
      if {&var} = -1 then
          {&err}
    &endif      
  end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


