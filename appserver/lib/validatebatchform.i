/* lib/validatebatchform.i
   function to VALIDATE the key data FIELDS for a single BATCHFORM record
   Uses variables only -- i.e., it doesn't set any fields in the database
   D.Sinclair
   5.7.2014
   Modified
   02/08/2021  MK  Committment processing

    Errors:
      E01 - Blank File
      E02 - Invalid Policy
      E03 - Future effective date
      E04 - Blank effective date
      E05 - Invalid Form Type
      E06 - Invalid FormID                                    /* das: new */
      E07 - Invalid CountyID                                  /* das: new */
      E08 - Policy assigned to a different agent
      E09 - Voided Policy
      E10 - Invalid Agent
      E11 - Invalid FormID/Form Type combination              /* das: new */
      E12 - Invalid STAT code                                 /* das: new */
      E13 - Policy exists in more than on file
      E14 - Policy number same as file number                 /* Added 8/9/2016 */
      E15 - Policy number is blank                            /* Added 8/31/16 */
      E16 - Effective date later than policy effective date   /* Added 8/31/16 */
      E17 - Revenue Type cannot be blank                      /* Added 01/06/19 */

    Warnings:
      W01 - Low Gross premium
      W02 - High Gross premium
      W03 - Inactive FormID                                   /* das: new */
      W04 - Low (zero or negative) liability
      W05 - Not default form for STAT code
      W06 - File number previously used
      W07 - High (over $1M) liability
      W08 - Previously processed
      W09 - Old effective date
      W10 - Inactive STAT code
      W11 - Policy assigned to different agent
      W12 - Gross receipt is less than zero
      W13 - Policy processed under another file 
      W15 - Net premium exceeded the agent remittance alert amount /* Added 12/7/17 */
 */
 
  {lib/normalizefilenumber.i}
  {lib/normalizefileid.i}    

  FUNCTION validateBatchForm RETURNS CHAR private
    (input piBatchID as int,
     input pcAgentID as char,
     input pcFormType as char,
     input pcFileNumber as char,
     input piPolicyID as int,
     input pcFormID as char,
     input pcRateCode as char,
     input pcStatCode as char,
     input pdtmEffDate as datetime,
     input pdLiabilityAmount as dec,
     input pcCountyID as char,
     input plResidential as log,
     input pdGrossPremium as dec,
     input pdNetPremium as dec,
     output pcValidMsg as char):

    def buffer validateBatchForm for batchForm.
    def buffer validateBatch     for batch.

    define var pcValid as char init "E" no-undo.

    def var iSev as int no-undo.
    def var lHasCounties as log no-undo.
    def var lHasSTAT as log no-undo.

    def var tFoundAgent as log init false.
    def var tFoundSTAT as log init false.
    def var tFoundStateForm as log init false.
    def var tFoundPolicy as log init false.

    def var dLiabilityDelta as dec no-undo.
    def var dGrossDelta as dec no-undo.
    def var dNetDelta as dec no-undo.
    def var dRetentionDelta as dec no-undo.

    def var tTotalNet as dec no-undo.
    def var tYearID as int no-undo.
    
    def var cFileID as char no-undo.
    
    def buffer agent for agent.
    def buffer policy for policy.
    def buffer stateForm for stateForm.
    def buffer statCode for statCode.
    def buffer county for county.

    pcFileNumber = normalizeFileNumber(pcFileNumber).
    cFileID      = normalizeFileID(pcFileNumber).
    
    /* Validate supplied data quality */
    if cFileID = ? or cFileID = ""
     then
      do:
          iSev = max(2, iSev).
          pcValidMsg = pcValidMsg + ",E01". /* MUST NOT be blank file number */
      end.

    if not can-find(first syscode
                      where syscode.codetype = "form"
                        and syscode.code = pcFormType)
                     /* das: LOOKUP(pcFormType, "P,E,C,T,S,D,M") = 0 */
     then
      do:
          iSev = max(2, iSev).
          pcValidMsg = pcValidMsg + ",E05". /* MUST have a valid form type */
      end.

    if pcFormType = "P" 
     then
      do:
        if date(pdtmEffDate) > TODAY
         then
          do:
              iSev = max(2, iSev).
              pcValidMsg = pcValidMsg + ",E03". /* MUST NOT have a future policy effective date */
          end.
    
        if date(pdtmEffDate) < (TODAY - 270)
         then
          do:
              iSev = max(1, iSev).
              pcValidMsg = pcValidMsg + ",W09". /* SHOULD NOT have an old policy effective date */
          end.
    
        if pdtmEffDate = ?
         then
          do:
              iSev = max(2, iSev).
              pcValidMsg = pcValidMsg + ",E04". /* MUST NOT have blank policy effective date */
          end.
    
        /* check the liability */
        LIAB-CHECK:
        for each sysprop no-lock
           where sysprop.appCode = "OPS"
             and sysprop.objAction = "Processing"
             and sysprop.objProperty = "Code"
              by sysprop.objID descending:
        
          if pdLiabilityAmount >= decimal(sysprop.objValue)
           then
            do:
                iSev = max(1, iSev).
                pcValidMsg = pcValidMsg + "," + sysprop.objID. /* SHOULD NOT have a very very high policy liability */
                leave LIAB-CHECK.
            end.
        end.
    
        if pdLiabilityAmount <= 0
         then
          do:
              iSev = max(1, iSev).
              pcValidMsg = pcValidMsg + ",W04". /* SHOULD NOT have a low policy liability */
          end.
      end.

/*     if pcFormType = "T" then                                                                             */
/*     do:                                                                                                  */
/*       if date(pdtmEffDate) > TODAY                                                                       */
/*        then                                                                                              */
/*         do:                                                                                              */
/*             iSev = max(2, iSev).                                                                         */
/*             pcValidMsg = pcValidMsg + ",E03". /* MUST NOT have a future policy effective date */         */
/*         end.                                                                                             */
/*                                                                                                          */
/*       if date(pdtmEffDate) < (TODAY - 270)                                                               */
/*        then                                                                                              */
/*         do:                                                                                              */
/*             iSev = max(1, iSev).                                                                         */
/*             pcValidMsg = pcValidMsg + ",W09". /* SHOULD NOT have an old policy effective date */         */
/*         end.                                                                                             */
/*                                                                                                          */
/*       if pdtmEffDate = ?                                                                                 */
/*        then                                                                                              */
/*         do:                                                                                              */
/*             iSev = max(2, iSev).                                                                         */
/*             pcValidMsg = pcValidMsg + ",E04". /* MUST NOT have blank policy effective date */            */
/*         end.                                                                                             */
/*                                                                                                          */
/*       if piPolicyID <> ? and piPolicyID >= 0 and pdtmEffDate <> ? then                                   */
/*       do:                                                                                                */
/*         for first policy where policy.policyID = piPolicyID no-lock:                                     */
/*           if policy.effDate <> ? and date(pdtmEffDate) > date(policy.effDate) then                       */
/*           do:                                                                                            */
/*             iSev = max(2, iSev).                                                                         */
/*             pcValidMsg = pcValidMsg + ",E16". /* Effective date cannot be later than policy eff. date */ */
/*           end.                                                                                           */
/*           else                                                                                           */
/*           if policy.issuedEffDate <> ? and date(pdtmEffDate) > date(policy.issuedEffDate) then           */
/*           do:                                                                                            */
/*             iSev = max(2, iSev).                                                                         */
/*             pcValidMsg = pcValidMsg + ",E16". /* Effective date cannot be later than policy eff. date */ */
/*           end.                                                                                           */
/*         end.                                                                                             */
/*       end.                                                                                               */
/*     end.                                                                                                 */
 
    /* Validate RevenueType */
    for first validateBatch no-lock 
        where validateBatch.batchID = piBatchID:
        
      for first stateForm no-lock 
          where stateForm.stateID = validateBatch.stateID
            and stateForm.formID  = pcFormID:
                                    
        if stateform.revenuetype = "" or stateform.revenuetype = ? 
         then
          do: 
             iSev = max(2, iSev).
             pcValidMsg = pcValidMsg + ",E17". /* MUST NOT have blank revenuetype  */
          end.
      end.
    end. 
   
    /* Validate FileNumber */
    if can-find(FIRST policy
                  where policy.agentID = pcAgentID
                    and policy.fileID = cFileID
                    and policy.stat = "P")
     then
      do:
          iSev = max(1, iSev).
          pcValidMsg = pcValidMsg + ",W06". /* SHOULD NOT be a reused file number by the agent */
      end.


    /* Validate Agent attributes */
    tFoundAgent = false.
    for FIRST agent fields (agentID stateID) no-lock
      where agent.agentID = pcAgentID:
      tFoundAgent = true.

      /* Validate state/form attributes */
      tFoundStateForm = false.
      for FIRST stateForm no-lock
        where stateForm.stateID = agent.stateID
          and stateForm.formID = pcFormID:
       tFoundStateForm = true.

       if stateform.active = false 
        then
         do: 
              iSev = max(1, iSev).
              pcValidMsg = pcValidMsg + ",W03". /* SHOULD be an active form */
         end.

       if stateform.formType <> pcFormType
        then
          do:
              iSev = max(2, iSev).
              pcValidMsg = pcValidMsg + ",E11". /* MUST be a form of the correct type */
          end.
       if stateForm.ratecheck = "F"
        then
         do:
              if pdGrossPremium < stateForm.rateMin
                then
                  do:
                    iSev = max(1, iSev).
                    pcValidMsg = pcValidMsg + ",W01". /* SHOULD be a reasonable gross premium */
                  end.

              if pdGrossPremium > stateForm.rateMax and stateForm.rateMax <> 0
                then
                  do:
                    iSev = max(1, iSev).
                    pcValidMsg = pcValidMsg + ",W02". /* SHOULD NOT be an ureasonably high gross premium */
                  end.
         end.

       if stateForm.rateCheck = "L"
        then
         do:
              if pdGrossPremium < ((pdLiabilityAmount / 1000) * stateForm.rateMin)
                then
                  do:
                    iSev = max(1, iSev).
                    pcValidMsg = pcValidMsg + ",W01". /* SHOULD be a reasonable gross premium */
                  end.

              if pdGrossPremium > ((pdLiabilityAmount / 1000) * stateForm.rateMax)
                then
                  do:
                    iSev = max(1, iSev).
                    pcValidMsg = pcValidMsg + ",W02". /* SHOULD NOT be an ureasonably high gross premium */
                  end.
         end.
      end.
      if not tFoundStateForm 
       then
        do:
            iSev = max(2, iSev).
            pcValidMsg = pcValidMsg + ",E06". /* MUST be valid form */
        end.


      /* Validate county attributes */
      lHasCounties = CAN-FIND(FIRST county
                                WHERE county.stateID = agent.stateID).
      if lHasCounties and pcFormType = "P"
       then
        do: if not can-find(first county
                            where county.stateID = agent.stateID
                            and (county.countyID = pcCountyID or county.description = pcCountyID))
             then
              do: 
                  /* iSev = max(2, iSev). */
                  /* pcValidMsg = pcValidMsg + ",E07". /* MUST be a valid county for a policy */ */
                  iSev = max(1, iSev).
                  pcValidMsg = pcValidMsg + ",W14". /* SHOULD be a valid county for a policy */
              end.
        end.


      /* Validate STAT code attributes */
      lHasSTAT = CAN-FIND(FIRST statcode
                            WHERE statcode.stateID = agent.stateID).
      if lHasSTAT 
       then
        do: tFoundSTAT = false.
            for first statCode no-lock
              where statCode.stateID = agent.stateID
                and statCode.statCode = pcStatCode:
             tFoundSTAT = true.
             if statCode.formID <> "" and statCode.formID <> ? and statCode.formID <> pcFormID 
              then
               do:
                   iSev = max(1, iSev).
                   pcValidMsg = pcValidMsg + ",W05". /* SHOULD be the default formID for the StatCode */
               end.
             if statCode.active = FALSE
              then
               do:
                   iSev = max(1, iSev).
                   pcValidMsg = pcValidMsg + ",W10". /* SHOULD be an active StatCode */
               end.
            end.
            if not tFoundSTAT 
             then
              do:
                  iSev = max(2, iSev).
                  pcValidMsg = pcValidMsg + ",E12". /* MUST be valid STAT code */
              end.
        end.
    end.
    if not tFoundAgent
     then
      do:
          iSev = max(2, iSev).
          pcValidMsg = pcValidMsg + ",E10". /* MUST be a valid agent */
      end.

    /* Validate policy attributes */
    tFoundPolicy = false.
    if index("PE",pcFormType) > 0 
     then     
      for first policy no-lock
        where policy.policyID = piPolicyID:
        tFoundPolicy = true.

        if policy.stat = "V" and pdNetPremium > 0
         then
          do:
              iSev = max(2, iSev).
              pcValidMsg = pcValidMsg + ",E09". /* MUST NOT be voided policy */
          end.
         else
        if policy.stat = "P"
         then
          do:
              iSev = max(1, iSev).
              pcValidMsg = pcValidMsg + ",W08". /* SHOULD NOT be a processed policy */
          end.

        if policy.agentID <> pcAgentID
         then
          do:
            if pcFormType = "E" then      /* Endorsement */
              do:
                iSev = max(1, iSev).
                pcValidMsg = pcValidMsg + ",W11". /* Warn that policy is assigned to a different agent */
              end.
            else
              do:
                iSev = max(2, iSev).
                pcValidMsg = pcValidMsg + ",E08". /* MUST have correct agent assigned to the policy */
              end.
          end.
      end.
    
    if pcFormType = "C" or pcFormType = "T" then
      tFoundPolicy = true.
      
    if not tFoundPolicy
     then
      do:
          iSev = max(2, iSev).
          pcValidMsg = pcValidMsg + ",E02". /* MUST be a valid policy */
      end.
      
    /* Validate that the policy number does not exist in more than one file number */
    if index("PE",pcFormType) > 0
     then
      for each validateBatchForm no-lock
        where validateBatchForm.batchID = piBatchID
        and validateBatchForm.policyID = piPolicyID:
        
        if validateBatchForm.fileID <> cFileID then
        do:
          iSev = max(2, iSev).
          pcValidMsg = pcValidMsg + ",E13". /* Policy cannot exist in more than one file */
          leave.
        end.
      end.

    if cFileID = string(piPolicyID) then
    do:
      iSev = max(2, iSev).
      pcValidMsg = pcValidMsg + ",E14". /* Policy number same as file number */
    end.

    if index("PE",pcFormType) > 0 then
    if piPolicyID <= 0 or piPolicyID = ? then
    do:
      iSev = max(2, iSev).
      pcValidMsg = pcValidMsg + ",E15". /* Policy number is blank */
    end.
    
    /* Warn the user if the policy was previoulsy processed under another file
       number.  Here, we're checking other batches instead of just validating
       againt 'policy.fileNumber' because the agent could have entered the 
       incorrect file number in the ARC when the policy was pulled, so it
       would initially be wrong in Compass, which would cause the W13 warning
       to appear unncessarily. */
    if index("PE",pcFormType) > 0 then
    for each validateBatchForm no-lock
      where validateBatchForm.policyID = piPolicyID
      and validateBatchForm.batchID <> piBatchID:

      if validateBatchForm.fileID <> cFileID then
      do:
        iSev = max(1, iSev).
        pcValidMsg = pcValidMsg + ",W13". /* Warn that policy was already processed under another file number */
        leave.
      end.
    end.

    /* GP has a requirement that the markdown (in our case, the retention) cannot
       make the Gross Receipt less than zero.  So, for our purposes, the absolute
       value of the the Gross Delta less the absolute value of the Retention Delta
       cannot be less than zero.  Batch forms that do not meet this criteria are 
       considered exceptions and must be accounted for manually in GP.
       
       A 'W12' warning will be generated if this condition is true.
    */
    case pcFormType:
      when "P" /* Policy */
       then
        do:
            assign
              dLiabilityDelta = pdLiabilityAmount
              dGrossDelta = pdGrossPremium
              dNetDelta = pdNetPremium
              dRetentionDelta = (pdGrossPremium - pdNetPremium)
              .
            
            for first policy no-lock
              where policy.policyID = piPolicyID:
              /* Deltas are calculated against "processed" values and "current" values */
              assign
                dLiabilityDelta = pdLiabilityAmount - policy.liabilityAmount
                dGrossDelta = pdGrossPremium - policy.grossPremium
                dNetDelta = pdNetPremium - policy.netPremium
                dRetentionDelta = (pdGrossPremium - pdNetPremium) - policy.retention
                .
            end.
        end.

      when "E"  /* Endorsement */
       then
        do:
            assign
              pdLiabilityAmount = 0 /* Not set for an endorsement */
              dLiabilityDelta = 0

              dGrossDelta = pdGrossPremium
              dNetDelta = pdNetPremium
              dRetentionDelta = (pdGrossPremium - pdNetPremium)
              .

            for first endorsement no-lock
              where endorsement.policyID = piPolicyID
                and endorsement.formID = pcFormID 
                and endorsement.effDate = pdtmEffDate:

              assign
                dGrossDelta = pdGrossPremium - endorsement.grossPremium
                dNetDelta = pdNetPremium - endorsement.netPremium
                dRetentionDelta = (pdGrossPremium - pdNetPremium) - endorsement.retentionPremium
                .
            end. 
        end.
      
      when "C" or   /* CPL */
       when "T"    /* Commitment */
       then
        do:
            assign
              dLiabilityDelta = pdLiabilityAmount
              dGrossDelta     = pdGrossPremium
              dNetDelta       = pdNetPremium
              dRetentionDelta = (pdGrossPremium - pdNetPremium)
              .
            
            for first agentfileform no-lock
              where agentfileform.agentID  = pcAgentID and
                    agentfileform.fileID   = cFileID   and
                    agentfileform.formID   = pcFormID  and
                    agentfileform.formType = pcFormType:
              /* Deltas are calculated against "processed" values and "current" values */
              assign
                dLiabilityDelta = pdLiabilityAmount - agentfileform.liabilityAmount
                dGrossDelta     = pdGrossPremium    - agentfileform.grossPremium
                dNetDelta       = pdNetPremium      - agentfileform.netPremium
                dRetentionDelta = (pdGrossPremium   - pdNetPremium) - agentfileform.retention
                .
            end.
        end.
      /* For future use... */
      when "S" then . /* Search */
      when "D" then . /* Schedule */
      when "M" then . /* Misc */
    end case.

    if (abs(dGrossDelta) - abs(dRetentionDelta) < 0) then
    do:
      iSev = max(1, iSev).
      pcValidMsg = pcValidMsg + ",W12". /* Warn that gross receipt is less than zero */
    end.
    
    /* Remittance alert for agent */

    for first agent no-lock
        where agent.agentID = pcAgentID
        and agent.remitAlert > 0:

    
      for first batch no-lock
        where batch.batchID = piBatchID:
        tYearID = batch.yearID.
      end.

      tTotalNet = 0.
      for each batch fields (yearID agentID netPremiumDelta) no-lock
          where batch.yearID = tYearID
          and batch.agentID = pcAgentID:
          tTotalNet = tTotalNet + batch.netPremiumDelta.
      end.

       tTotalNet = tTotalNet + pdNetPremium.


       if tTotalNet >= agent.remitAlert 
       then
         do:
          iSev = max(1, iSev).
          pcValidMsg = pcValidMsg + ",W15".
         end.

    end.


    /* Set the return severity to the "highest" level issue */
    case iSev:
      when 0 then pcValid = "G". /* G)ood:    no issues */
      when 1 then pcValid = "W". /* W)arning: issues that will not prevent the batch from completing */
      when 2 then pcValid = "E". /* E)rror:   one or more issues that must be corrected */
    end case.

    pcValidMsg = trim(pcValidMsg, ",").
    RETURN pcValid.
  END FUNCTION.
