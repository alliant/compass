&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : lib/sys-def.i
Purpose     :

Syntax      :

Description :

Author(s)   : Anubha Jain
Created     : 15-02-2019
Notes       :

Modification:                              
----------------------------------------------------------------------*/
/*----------Definition of Actions -------------------*/
&global-define Agent            "A"
&global-define Person           "P"
&global-define Attorney         "T"
&global-define Employee         "E" 
&global-define New              "N"
&global-define Modify           "M"
&global-define Copy             "C"
&global-define active           "A"
&global-define Inactive         "I"
&global-define Both             "B"
&global-define CalledAction     "C"
&global-define ErrorAction      "E"
&global-define SelectRole       "--Select Role--"

&global-define All              "ALL"
&global-define sysUser          "U"
&global-define NewValue         "New"
&global-define Supress          "S"
&global-define SupressValue     "Supress"
&global-define Dismiss          "D"
&global-define DismissValue     "Dismiss"
&global-define Red              "R"
&global-define RedValue         "Red"
&global-define Yellow           "Y"
&global-define YellowValue      "Yellow"
&global-define Green            "G"
&global-define GreenValue       "Green"

/*----------window status bar messages----------------*/
&global-define ResultNotMatch                "Results may not match current parameters."
&global-define ResultNotMatchSearchString    "Results may not match current search string."

/*----------Statistics Type----------------*/

&global-define Action        "Action"
&global-define ActionType    "ActionType"
&global-define Users         "User"
&global-define UserType      "UserType"

/*----------Window Titles----------------*/
&global-define ActiveActionReport          "Active Actions Report"
&global-define InactiveActionReport        "Inactive Actions Report"
&global-define ActiveUsersReport           "Active Users Report"
&global-define InactiveUsersReport         "Inactive Users Report"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


