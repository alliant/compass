&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : lib/rpt-wordwrap.i
    Purpose     : Function to WORD WRAP a sentence
    Author(s)   : John Oliver
    Created     : 11/1/2016
    Notes       :
  ----------------------------------------------------------------------*/

&IF defined(lines) = 0 &THEN
&scoped-define lines 56
&ENDIF

function wordWrap returns integer private
  ( input pString as character,
    input pWrapWidth as integer,
    input pWrapIndent as integer ) :
    
  if iLine >= 54
   then newPage().
    
  define variable iEntry as integer no-undo.
  define variable iLength as integer no-undo initial 0.
  define variable iNewLine as integer no-undo initial 0.
  define variable cLine as character no-undo.
  
  do iEntry = 1 to num-entries(pString," "):
    iLength = iLength + length(entry(iEntry,pString," ")) + 1.
    if iLength < pWrapWidth
     then cLine = cLine + entry(iEntry,pString," ") + " ".
     else
      do:
        textAt (cLine, pWrapIndent).
        newLine(1).
        assign
          cLine = entry(iEntry,pString," ") + " "
          iLength = length(cLine) - 1
          iNewLine = iNewLine + 1
          .
      end.
  end.
  if cLine > ""
   then
    do:
      textAt (cLine, pWrapIndent).
      newLine(1).
      iNewLine = iNewLine + 1.
    end.
  RETURN iNewLine.   /* Function return value. */
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


