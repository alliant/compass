/* lib/canfindinvoicedoc.i
   9.19.2014 D.Sinclair
   @param First unnamed parameter should be the type of invoice
   @param Second unnamed parameter should be the invoice ID
 */
can-find(first sysdoc
           where sysdoc.entityType = "{1}"
             and sysdoc.entityID = string({2})
             and sysdoc.entitySeq = 0)
