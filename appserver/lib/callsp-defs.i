/*------------------------------------------------------------------------
    File        : lib/callsp-defs.i
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : Sachin Anthwal
    Created     : 12/17/21
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
define variable csp-cFile           as character no-undo. 
define variable csp-hProcedureTable as handle    no-undo.
define variable csp-lSucess         as logical   no-undo.
define variable csp-cErr            as character no-undo. 
define variable csp-icount          as integer   no-undo. 

