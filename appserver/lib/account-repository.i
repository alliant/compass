&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file account-repository.i
@description Functions that assist in getting the files for use in the
             AP module easier
------------------------------------------------------------------------*/
  
{lib/validFilename.i}

/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getPayableFilename Include 
FUNCTION getPayableFilename RETURNS CHARACTER
  ( INPUT pInvoiceID     AS INT,
    INPUT pInvoiceNumber AS CHAR,
    INPUT pRefType       AS CHAR,
    INPUT pRefID         AS CHAR  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getPayableServerPath Include 
FUNCTION getPayableServerPath RETURNS CHARACTER
  ( INPUT pVendorID    AS CHAR,
    INPUT pInvoiceDate AS DATETIME )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getReceivableFileName Include 
FUNCTION getReceivableFileName RETURNS CHARACTER
  ( INPUT pInvoiceID AS INT,
    INPUT pName      AS CHAR,
    INPUT pRefType   AS CHAR,
    INPUT pRefID     AS CHAR  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getReceivableServerPath Include 
FUNCTION getReceivableServerPath RETURNS CHARACTER
  ( INPUT pInvoiceDate AS DATETIME )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getPayableFilename Include 
FUNCTION getPayableFilename RETURNS CHARACTER
  ( INPUT pInvoiceID     AS INT,
    INPUT pInvoiceNumber AS CHAR,
    INPUT pRefType       AS CHAR,
    INPUT pRefID         AS CHAR  ) :
/*------------------------------------------------------------------------------
@description Builds and returns the ShareFile filename
------------------------------------------------------------------------------*/
  define variable cFilename as character no-undo.
  define variable cReplace as character no-undo.
  define variable cBadChars as character no-undo.
  define variable i as integer no-undo.
  define variable pos as integer no-undo.
  
  /* no invoice number */
  if pInvoiceNumber = ""
   then pInvoiceNumber = "NoInvoiceNumber".
  
  cReplace = replace(pInvoiceNumber," ","_") + "_" + string(pInvoiceID).
  cFilename = cReplace.
  if not validFilename(cReplace, output cBadChars)
   then
    do i = 1 to length(cReplace):
      pos = index(cBadChars, substring(cReplace,i,1)).
      if pos > 0
       then cFilename = replace(cFilename,substring(cBadChars,pos,1),"").
    end.
    
  case pRefType:
   when "C" then cFilename = cFilename + "_Claim" + pRefID.
  end case.
  /* 
  cFilename = replace(guid, "-", "").
  */
  
  RETURN cFilename.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getPayableServerPath Include 
FUNCTION getPayableServerPath RETURNS CHARACTER
  ( INPUT pVendorID    AS CHAR,
    INPUT pInvoiceDate AS DATETIME ) :
/*------------------------------------------------------------------------------
@description Builds and returns the ShareFile server path
------------------------------------------------------------------------------*/
  define variable cServerPath as character no-undo.
  define variable cRootFolder as character no-undo.
  
  publish "GetRepositoryRoot" (output cRootFolder).
  /* no vendor id */
  cServerPath = cRootFolder + "/AP/Unassigned/".
  /* vendor id but no invoice date */
  if pVendorID > ""
   then
    if pInvoiceDate = ?
     then cServerPath = cRootFolder + "/AP/" + pVendorID + "/".
     else cServerPath = cRootFolder + "/AP/" + pVendorID + "/" + string(year(pInvoiceDate)) + "/".

  RETURN cServerPath.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getReceivableFileName Include 
FUNCTION getReceivableFileName RETURNS CHARACTER
  ( INPUT pInvoiceID AS INT,
    INPUT pName      AS CHAR,
    INPUT pRefType   AS CHAR,
    INPUT pRefID     AS CHAR  ) :
/*------------------------------------------------------------------------------
@description Builds and returns the ShareFile filename
------------------------------------------------------------------------------*/
  define variable cFilename as character no-undo.
  define variable cReplace as character no-undo.
  define variable cBadChars as character no-undo.
  define variable i as integer no-undo.
  define variable pos as integer no-undo.
  
  /* no invoice number */
  if pName = ""
   then pName = "Unknown".
  
  cReplace = string(pInvoiceID) + "_" + replace(pName," ","_").
  cFilename = cReplace.
  if not validFilename(cReplace, output cBadChars)
   then
    do i = 1 to length(cReplace):
      pos = index(cBadChars, substring(cReplace,i,1)).
      if pos > 0
       then cFilename = replace(cFilename,substring(cBadChars,pos,1),"").
    end.
    
  case pRefType:
   when "C" then cFilename = cFilename + "_Claim" + pRefID.
  end case.
  /* 
  cFilename = replace(guid, "-", "").
  */
  
  RETURN cFilename.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getReceivableServerPath Include 
FUNCTION getReceivableServerPath RETURNS CHARACTER
  ( INPUT pInvoiceDate AS DATETIME ) :
/*------------------------------------------------------------------------------
@description Builds and returns the ShareFile server path
------------------------------------------------------------------------------*/
  define variable cServerPath as character no-undo.
  define variable cRootFolder as character no-undo.
  
  publish "GetRepositoryRoot" (output cRootFolder).
  cServerPath = cRootFolder + "/AR/" + string(year(pInvoiceDate)) + "/".

  RETURN cServerPath.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
