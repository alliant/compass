&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{lib/std-def.i}
{lib/add-delimiter.i}

&IF defined(codeType) = 0 &THEN
&scoped-define codeType ""
&ENDIF

&IF defined(err) = 0 &THEN
&scoped-define err std-lo
&ENDIF

&IF defined(out) = 0 &THEN
&scoped-define out std-ch
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

{&err} = false.
{&out} = "".
for each syscode no-lock
   where syscode.codeType = {&codeType}:
  
  /* create the error message */
  std-in = index(syscode.description, syscode.code).
  if std-in = 0
   then {&out} = addDelimiter({&out}, ", ") + "(" + syscode.code + ")" + syscode.description.
   else {&out} = addDelimiter({&out}, ", ") + substring(syscode.description, 1, std-in - 1) + "(" + syscode.code + ")" + substring(syscode.description, length(syscode.code) + 1).
  
  /* check if valid */
  if syscode.code = {&code}
   then {&err} = true.
end.

std-in = r-index({&out}, ",").
{&out} = substring({&out}, 1, std-in) + " or" + substring({&out}, std-in + 1).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


