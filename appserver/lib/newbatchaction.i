/*
 lib/newbatchaction.i
 function to create a NEW BATCH ACTION (ie task) record
 D.Sinclair
 5.9.2014
 */
 

FUNCTION newBatchAction returns logical
 (pBatchID as int,
  pUserID as char,
  pComments as char,
  pSecure as logical):
  
 def var tCreated as logical init false.
 def var iNewSeq as int no-undo.
 def var tFound as logical init false no-undo.
 def var tAgentID as char no-undo.

 def buffer task for task.
 def buffer batch for batch.

 /* Only create records for existing batch records */
 for first batch fields (batchID agentID) no-lock
   where batch.batchID = pBatchID:
  tAgentID = batch.agentID.
  tFound = true.
 end.

 if not tFound 
  then return false.

 tCreated = false.

 TRX-BLK:
 do TRANSACTION
   on error undo, leave:
  {lib/nextkey.i &type='task' &var=iNewSeq &err="leave TRX-BLK."} 

  create task.
  task.taskID = iNewSeq.
  validate task.

  assign
    task.entityType = "B"
    task.entityID = string(pBatchID)  
    task.subject = pComments
    task.createDate = now
    task.completeDate = now
    task.uid = pUserID
    task.owner = pUserID
    task.userids = "*"
    task.stat = "C"
    task.agentID = tAgentID
    task.secure = pSecure
    .
  release task.
  tCreated = true.
 end.

 return tCreated.  
end.  
