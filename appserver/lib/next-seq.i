&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------------
@description Gets the next sequence
@returns The table's buffer
@notes Call the function: std-in = nextSeq(temp-table table:default-buffer-handle).
------------------------------------------------------------------------------*/
&IF DEFINED(EXCLUDE-nextSeq) = 0 &THEN
FUNCTION nextSeq RETURNS INTEGER
  ( INPUT hBuffer AS HANDLE  )  FORWARD.
&ENDIF

&IF DEFINED(nextSeq) = 0
&THEN
FUNCTION nextSeq RETURNS INTEGER
  ( INPUT hBuffer AS HANDLE  ) :
  def var iCount as int no-undo init 0.
  def var hQuery as handle no-undo.
  
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + hBuffer:table + " by seq desc").
  hQuery:query-open().
  hQuery:get-first().
  if not hQuery:query-off-end
   then iCount = hBuffer:buffer-field("seq"):buffer-value().
  hQuery:query-close().
  delete object hQuery.
  
  return iCount + 1.

END FUNCTION.
&GLOBAL-DEFINE nextSeq true
&ENDIF


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


