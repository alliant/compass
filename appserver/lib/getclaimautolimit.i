/*---------------------------------------------------------------------
@name getclaimautoapprove.i
@description Get the user's auto approval limit to automatically
             approve an invoice

@param {1} is the claim ID (int)
@param {2} is the reference (char)
@param {3) is the user id (char)
@param {4} is the return variable (decimal)

@author John Oliver
@version 1.0
@created 08/6/16
@notes The formula is:
       Limit = AutoApproveLimit - CompletedPayableInvoices
---------------------------------------------------------------------*/
{lib/get-reserve-type.i}

define variable dAutoLimit as decimal.
define variable dAutoAmount as decimal.
define variable dAutoMinAmount as decimal no-undo.
define variable dAutoApinvID as integer no-undo initial 0.
define variable cAutoType as character no-undo.
assign
  dAutoLimit = 0.0
  dAutoAmount = 0.0
  dAutoMinAmount = lf-max
  cAutoType = getReserveDesc({2})
  {4} = 0.0
  .
FOR FIRST sysprop NO-LOCK
    WHERE sysprop.appCode = "CLM"
      AND sysprop.objAction = "ClaimAdjustmentRequest"
      AND sysprop.objProperty = cAutoType + "AutoLimit"
      AND sysprop.objID = {3}:
    
  dAutoLimit = DECIMAL(sysprop.objValue) NO-ERROR.
  FOR EACH aptrx NO-LOCK
     WHERE aptrx.refID = STRING({1})
       AND aptrx.refCategory = {2}:
       
    dAutoAmount = dAutoAmount - aptrx.transAmount.
  END.
END.

/* get the minimum approved amount of non-completed invoices  */
for each apinv no-lock
   where apinv.refID = string({1})
     and apinv.refCategory = {2}:
      
  if apinv.stat = "O" or apinv.stat = "A"
   then
    for each apinva no-lock
       where apinva.apinvID = apinv.apinvID
         and apinva.stat = "A":
      
      /* we want the minimum */
      if apinva.amount < dAutoMinAmount
       then dAutoMinAmount = apinva.amount.
    end.
end.
dAutoAmount = dAutoAmount - dAutoMinAmount.
if dAutoLimit = -1
 then {4} = lf-max.
 else {4} = dAutoLimit - ABSOLUTE(dAutoAmount).