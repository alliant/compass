&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file get-os-error.i
@description Function for getting the OS-ERROR descriptions

@param os-error;int;The error code
@param out;character;The error description

@author John Oliver
@created 12.26.2019
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

&scoped-define No1 "Not owner"
&scoped-define No2 "No such file or directory"
&scoped-define No3 "Interrupted system call"
&scoped-define No4 "I/O error"
&scoped-define No5 "Bad file number"
&scoped-define No6 "No more processes"
&scoped-define No7 "Not enough core memory"
&scoped-define No8 "Permission denied"
&scoped-define No9 "Bad address"
&scoped-define No10 "File exists"
&scoped-define No11 "No such device"
&scoped-define No12 "Not a directory"
&scoped-define No13 "Is a directory"
&scoped-define No14 "File table overflow"
&scoped-define No15 "Too many open files"
&scoped-define No16 "File too large"
&scoped-define No17 "No space left on device"
&scoped-define No18 "Directory not empty"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getError Include 
FUNCTION getError RETURNS CHARACTER
  ( input pCode as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getError Include 
FUNCTION getError RETURNS CHARACTER
  ( input pCode as integer ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable pDesc as character.
  case pCode:
    when 1  then pDesc = {&No1}.
    when 2  then pDesc = {&No2}.
    when 3  then pDesc = {&No3}.
    when 4  then pDesc = {&No4}.
    when 5  then pDesc = {&No5}.
    when 6  then pDesc = {&No6}.
    when 7  then pDesc = {&No7}.
    when 8  then pDesc = {&No8}.
    when 9  then pDesc = {&No9}.
    when 10 then pDesc = {&No10}.
    when 11 then pDesc = {&No11}.
    when 12 then pDesc = {&No12}.
    when 13 then pDesc = {&No13}.
    when 14 then pDesc = {&No14}.
    when 15 then pDesc = {&No15}.
    when 16 then pDesc = {&No16}.
    when 17 then pDesc = {&No17}.
    when 18 then pDesc = {&No18}.
  end case.
  RETURN pDesc.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

