&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@description Include to validate the activity
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

define variable cActivityError as character no-undo.
{lib/std-def.i}
{lib/add-delimiter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doFault Include 
FUNCTION doFault RETURNS CHARACTER
  ( input pCode as character,
    input pArgs as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&analyze-resume


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

cActivityError = "".

/* is this a modified activity */
&IF defined(activityID) <> 0 &THEN
if not can-find(first agentactivity where activityID = {&activityID})
 then cActivityError = addDelimiter(cActivityError, " ") + doFault("3066", "Agent Activity").
&ENDIF

/* is the state valid */
&IF defined(activityState) <> 0 &THEN
if not can-find(first state where stateID = {&activityState})
 then cActivityError = addDelimiter(cActivityError, " ") + doFault("3066", "State " + {&activityState}).
&ENDIF

/* is the year valid */
&IF defined(activityYear) <> 0 &THEN
date(1,1,{&activityYear}) no-error.
if error-status:error
 then cActivityError = addDelimiter(cActivityError, " ") + doFault("3001", "Year " + string({&activityYear})).

/* have to be admin to add previous year */
if not {lib/isadmin.i} and {&activityYear} < year(today)
 then cActivityError = addDelimiter(cActivityError, " ") + doFault("1005", "Creating an Activity for a previous year").
&endif

/* is the category valid */
&IF defined(activityCategory) <> 0 &THEN
{lib/get-sysprop.i &appCode="'AMD'" &objAction="'Activity'" &objProperty="'Category'" &objID={&activityCategory}}
if std-ch = ""
 then cActivityError = addDelimiter(cActivityError, " ") + doFault("3066", "Category " + {&activityCategory}).
&ENDIF

/* valid type */
&IF defined(activityType) <> 0 &THEN
{lib/get-sysprop.i &appCode="'AMD'" &objAction="'Activity'" &objProperty="'Type'" &objID={&activityType}}
if std-ch = ""
 then cActivityError = addDelimiter(cActivityError, " ") + doFault("3066", "Type " + {&activityType}).
 else
  do:
    &if defined(activityID) <> 0 &then
    /* have to be admin to change type from A to something else */
    {lib/get-sysprop.i &appCode="'AMD'" &objAction="'Activity'" &objProperty="'Type'" &objID="'A'" &out=cType}
    if not {lib/isadmin.i} and pType <> "A" and can-find(first agentactivity where activityID = pActivityID and type = "A")
     then cActivityError = addDelimiter(cActivityError, " ") + doFault("1005", "Changing Type from " + cType + " to " + std-ch).
    &endif
  end.
&ENDIF

/* is the status valid */
&IF defined(activityStatus) <> 0 &THEN
{lib/get-sysprop.i &appCode="'AMD'" &objAction="'Activity'" &objProperty="'Status'" &objID={&activityStatus}}
if std-ch = ""
 then cActivityError = addDelimiter(cActivityError, " ") + doFault("3066", "Status " + {&activityStatus}).
 else
  do:
    &if defined(activityID) <> 0 &then
    /* can't modify closed activity */
    {lib/get-sysprop.i &appCode="'AMD'" &objAction="'Activity'" &objProperty="'Status'" &objID="'C'"}
    if can-find(first agentactivity where activityID = pActivityID and stat = "C")
     then cActivityError = addDelimiter(cActivityError, " ") + doFault("3005", "Cannot modify a " + std-ch + " Activity.").
    &endif
  end.
&endif

/* is the name valid */
&IF defined(activityName) <> 0 and defined(activityAgent) <> 0 &then
if {&activityName} = "" and {&activityAgent} = ""
 then cActivityError = addDelimiter(cActivityError, " ") + doFault("3005", "There is no target name or agent selected").
&endif

/* only validate name if agent isn't present */
&if defined(activityName) <> 0 and defined(activityAgent) <> 0 and defined(activityYear) <> 0 and defined(activityCategory) <> 0 and defined(activityType) <> 0 &then
if {&activityName} > "" and {&activityAgent} = ""
 then
  &if defined(activityID) <> 0 &then
  if cActivityError = "" and can-find(first agentactivity where activityID <> {&activityID} and year = {&activityYear} and type = {&activityType} and category = {&activityCategory} and name = {&activityName})
   then cActivityError = addDelimiter(cActivityError, " ") + doFault("3016", "Agent Activity" {&msg-add} "Name" {&msg-add} {&activityName} {&msg-add} "Category=" + {&activityCategory} + ", Type=" + {&activityType} + ", and Year=" + string({&activityYear})).
  &else
  if cActivityError = "" and can-find(first agentactivity where year = {&activityYear} and type = {&activityType} and category = {&activityCategory} and name = {&activityName})
   then cActivityError = addDelimiter(cActivityError, " ") + doFault("3016", "Agent Activity" {&msg-add} "Name" {&msg-add} {&activityName} {&msg-add} "Category=" + {&activityCategory} + ", Type=" + {&activityType} + ", and Year=" + string({&activityYear})).
  &endif
&endif

/* validate agent */
&if defined(activityAgent) <> 0 and defined(activityName) <> 0 and defined(activityYear) <> 0 and defined(activityCategory) <> 0 and defined(activityType) <> 0 &then
if {&activityAgent} > ""
 then
  if not can-find(first agent where agentID = {&activityAgent})
   then cActivityError = addDelimiter(cActivityError, " ") + doFault("3066", "Agent " + {&activityAgent}).
   else
    do:
      &if defined(activityID) <> 0 &then
      if cActivityError = "" and can-find(first agentactivity where activityID <> {&activityID} and year = {&activityYear} and type = {&activityType} and category = {&activityCategory} and agentID = {&activityAgent})
       then cActivityError = addDelimiter(cActivityError, " ") + doFault("3016", "Agent Activity" {&msg-add} "Agent" {&msg-add} {&activityAgent} {&msg-add} "Category=" + {&activityCategory} + ", Type=" + {&activityType} + ", and Year=" + string({&activityYear})).
      &else
      if cActivityError = "" and can-find(first agentactivity where year = {&activityYear} and type = {&activityType} and category = {&activityCategory} and agentID = {&activityAgent})
       then cActivityError = addDelimiter(cActivityError, " ") + doFault("3016", "Agent Activity" {&msg-add} "Agent" {&msg-add} {&activityAgent} {&msg-add} "Category=" + {&activityCategory} + ", Type=" + {&activityType} + ", and Year=" + string({&activityYear})).
      &endif
    end.
&endif

&if defined(appserver) <> 0 &then
if cActivityError > ""
 then pResponse:fault("3005", cActivityError).
&elseif defined(out) <> 0 &then
{&out} = cActivityError.
&else
std-ch = cActivityError.
&endif


/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doFault Include 
FUNCTION doFault RETURNS CHARACTER
  ( input pCode as character,
    input pArgs as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable pResult as character no-undo.
  
  run util/msgsubs.p (pCode, pArgs, output pResult).

  RETURN pResult.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&analyze-resume

