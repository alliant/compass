&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : lib/rpt-cloz.i
    Purpose     : standard RePorT CLOsing orchestration
    Author(s)   : D.Sinclair
    Created     : 9.12.2011
    Notes       : tempID:           Constant or Field that defines temp-file
                  exclude-pdfview:  Set to prevent auto-opening
    Modification:
    Date          Name      Description
    10/09/2018    AC        Modified for testing
  ----------------------------------------------------------------------*/
run endReport in this-procedure.

&if defined(no-deleteproc) = 0 &then
if valid-handle(h_PDFinc) then
  delete procedure h_PDFinc no-error.
&endif

session:set-wait-state("").

&if defined(no-save) = 0 &then
publish "AddTempFile" ({&tempID}, search(activeFilename)).
&endif

&IF defined(exclude-pdfview) = 0 &THEN
run util/openfile.p (activeFilename).
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


