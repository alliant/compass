/*------------------------------------------------------------------------
@file GetCourse.Cls
@action courseGet
@description 

@returns 

@success 2005;

@author Sachin Chaturvedi
@version 1.0
@created 12/09/2024
Modification:

----------------------------------------------------------------------*/

class course.GetCourse inherits framework.ActionBase:
    
  {tt/course.i      &tableAlias="ttCourse"}
  {tt/training.i    &tableAlias="ttTraining"}
  {tt/participant.i &tableAlias="ttParticipant"}

  constructor GetCourse ():
    super().
  end constructor.

  destructor public GetCourse ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable piCourseID  as integer  no-undo.
    define variable dsHandle    as handle   no-undo.

    {lib/callsp-defs.i}
    {lib/std-def.i}

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttCourse:handle,
                         buffer ttTraining:handle,
                         buffer ttParticipant:handle).

    dsHandle:add-relation(dsHandle:get-buffer-handle(1), dsHandle:get-buffer-handle(2), "courseID,courseID",?,yes,?,?).
    dsHandle:add-relation(dsHandle:get-buffer-handle(2), dsHandle:get-buffer-handle(3), "trainingID,trainingID",?,yes,?,?).


    pRequest:getParameter("CourseID", output piCourseID).

    /* validate courseID */
    if piCourseID = ? or piCourseID = 0
     then
      do:
        pResponse:fault("3001", "CourseID").
        return. 
      end.

    /* validate courseID */
    if not can-find(first course where course.courseID = piCourseID)
     then
      do:  
        pResponse:fault("3067", "course" {&msg-add} "ID" {&msg-add} string(piCourseID)).
        return.
      end.

    std-in = 0.
    for first course no-lock where course.courseID = piCourseID:
      create ttCourse.
      buffer-copy course to ttCourse.
      for first state fields (description) no-lock where state.stateID = ttCourse.stateID:
        ttCourse.stateDesc = state.description.
      end.
      
      /* fetching training table records */
      for each training no-lock where training.courseID = piCourseID:
        create ttTraining.
        buffer-copy training to ttTraining.

        /* fetching participant table records */
        for each participant no-lock where participant.trainingID = training.trainingID:
          create ttParticipant.
          buffer-copy participant to ttParticipant.
        end.
      end.
      std-in = std-in + 1.
    end.

    if can-find(first ttCourse)
     then 
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).  
    
    pResponse:success("2005", string(std-in) {&msg-add} "Course").
             
  end method.
      
end class.


