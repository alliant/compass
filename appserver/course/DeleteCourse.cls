/*------------------------------------------------------------------------
@name DeleteCourse.cls
@action courseDelete
@description  Deletes course record form course table.
@param TraningID;int
@throws 3067;Course ID does not exist
@throws 3005;The course cannot be deleted as it is in use
@throws 3000;delete course failed
@returns Success;2000
@author Sachin
@version 1.0
@created 12.10.2024
@Modified :
Date        Name          Comments  

----------------------------------------------------------------------*/

class course.DeleteCourse inherits framework.ActionBase:
      
  constructor DeleteCourse ():
    super().
  end constructor.

  destructor public DeleteCourse ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
            
    /*parameter variables*/
    define variable piCourseID  as integer   no-undo.
    define variable std-lo        as logical   no-undo.
    &scoped-define msg-add + "^" +
                
    pRequest:getParameter("courseID", output piCourseID).

    /* validation when no parameter is supplied */
    if (piCourseID = 0  or piCourseID = ?)
     then 
      do:
        pResponse:fault("3001", "CourseID").
        return.
      end.

     /* validate courseID */
    if not can-find(first course where course.courseID = piCourseID)
     then
      do:  
        pResponse:fault("3067", "Course" {&msg-add} "ID" {&msg-add} string(piCourseID)).
        return.
      end.

    if can-find(first training where training.courseID = piCourseID)
     then
      do: 
        pResponse:fault("3005", "The Course cannot be deleted as it is in use").
        return.
      end.        
      
    std-lo = false.
    
    trxBlock:
    for first course exclusive-lock
      where course.courseID = piCourseID transaction
      on error undo trxBlock, leave trxBlock:
      
      delete course.
      release course.
      
      std-lo = true.
    end.
    
    if not std-lo
     then
      do: 
        pResponse:fault("3000", "Delete Course").
        return.
      end.
    
    pResponse:success("2000", "Delete Course").
    
  end method.
      
end class.


