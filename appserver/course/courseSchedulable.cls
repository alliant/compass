/*------------------------------------------------------------------------
@name courseSchedulable.cls
@action schedulableCourse
@description  Schedulable course record form course table.
@param CourseID;int
@throws 3067;Course ID does not exist
@throws 3000;course Schedulable failed
@returns Success;2000
@author SRK
@version 1.0
@created 12.18.2024
@Modified :
Date        Name          Comments  

----------------------------------------------------------------------*/

class course.courseSchedulable inherits framework.ActionBase:
      
  constructor courseSchedulable ():
    super().
  end constructor.

  destructor public courseSchedulable ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
            
    /*parameter variables*/
    define variable piCourseID  as integer   no-undo.
    define variable plActive    as logical   no-undo.
    define variable std-lo      as logical   no-undo.

    &scoped-define msg-add + "^" +
                
    pRequest:getParameter("courseID", output piCourseID).
    pRequest:getParameter("Active", output plActive).

    /* validation when no parameter is supplied */
    if (piCourseID = 0  or piCourseID = ?)
     then 
      do:
        pResponse:fault("3001", "CourseID").
        return.
      end.

     /* validate courseID */
    if not can-find(first course where course.courseID = piCourseID)
     then
      do:  
        pResponse:fault("3067", "Course" {&msg-add} "ID" {&msg-add} string(piCourseID)).
        return.
      end.       
      
    std-lo = false.
    
    trxBlock:
    for first course exclusive-lock
      where course.courseID = piCourseID transaction
      on error undo trxBlock, leave trxBlock:
      
      course.canSchedule  = plActive.
      if plActive
       then
        assign
           course.expiredDate = date('')
           course.expiredBy   = ''.
       else
        assign
           course.expiredDate = now
           course.expiredBy   = pRequest:uid.

      validate course.
      release course.
      
      std-lo = true.
    end.
    
    if not std-lo
     then
      do: 
        pResponse:fault("3000", "Course Schedule").
        return.
      end.
    
    pResponse:success("2000", "Course Schedule").
    
  end method.
      
end class.


