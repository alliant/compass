/*------------------------------------------------------------------------
@file SearchCourses.Cls
@action coursesSearch
@description 

@returns 

@success 2005;

@author Sachin Chaturvedi
@version 1.0
@created 12/19/2024
Modification:

----------------------------------------------------------------------*/

class course.SearchCourses inherits framework.ActionBase:
    
  {tt/course.i      &tableAlias="ttCourse"}
  define temp-table ttsearch
    field courseID as character
    field matched  as decimal.

  constructor SearchCourses ():
    super().
  end constructor.

  destructor public SearchCourses ():
  end destructor.   

  /* global variables */
  define variable plActive as logical    no-undo.
  define variable iNum     as integer    no-undo.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable pcStateID      as character  no-undo.
    define variable cCourseName    as character  no-undo.
    define variable cWord          as character  no-undo.
    define variable iCount         as integer    no-undo.
    define variable dsHandle       as handle     no-undo.

    {lib/callsp-defs.i}
    {lib/std-def.i}

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttCourse:handle).

    pRequest:getParameter("StateID",    output pcStateID).
    pRequest:getParameter("CourseName", output cCourseName).
    pRequest:getParameter("Active",     output plActive).

    if pcStateID = "" or pcStateID = ?
     then
      pcStateID = "ALL".

    /* validate courseID */
    if pcStateID <> "ALL" and (not can-find(first state where state.stateID = pcStateID))
     then
      do:  
        pResponse:fault("3067", "State" {&msg-add} "ID" {&msg-add} string(pcStateID)).
        return.
      end.

    assign
        cCourseName = trim(cCourseName)
        iNum          = 0
        .

    /* If no search string passed, then return all course records of the state */
    if cCourseName = ? or cCourseName = "" 
     then
      do:
        for each course no-lock 
         where course.stateid = (if pcStateID = "ALL" then course.stateid else pcStateID)
           and course.canschedule = plActive :
          createttCourse(string(course.courseID)).
        end.
      end.
     else
      do:
        do iCount = 1 to num-entries(cCourseName," "):
          cWord = entry(iCount,cCourseName," ").
          for each sysindex where sysindex.objtype = "CourseName" and
                                  sysindex.objkey  = cWord no-lock :
            if not can-find(first ttsearch where ttsearch.courseID = sysindex.objID)
             then
              do:
                create ttsearch.
                assign 
                    ttsearch.courseID = sysindex.objID
                    ttsearch.matched  = 1.
              end.
             else
		      for first ttsearch where ttsearch.courseID = objID no-lock :
                ttsearch.matched = ttsearch.matched + 1.
              end.
          end.
        end.

        for each ttsearch no-lock: 
          ttsearch.matched = (ttsearch.matched / num-entries(cCourseName,' ')) .
        end.

        for each ttsearch no-lock where ttsearch.matched = 1 :
          createttCourse(ttsearch.courseID).
        end.

      end.


    if can-find(first ttCourse)
     then 
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).  
    
    pResponse:success("2005", string(iNum) {&msg-add} "Course").
             
  end method.

  method private void createttCourse (input pcCourseID as character):
    if not can-find(first ttCourse where ttCourse.courseID = integer(pcCourseID) and ttcourse.canschedule = plActive)
     then
      for first course where course.courseID = integer(pcCourseID)
                         and course.canschedule = plActive no-lock:

        create ttCourse.
        buffer-copy course to ttCourse.

        for first state  fields (description) where state.stateid = course.stateID no-lock:
          ttCourse.stateDesc = state.description. 
        end.

        iNum = iNum + 1.
      end.
     
  end method.
      
end class.


