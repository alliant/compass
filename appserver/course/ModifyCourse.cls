/*------------------------------------------------------------------------
@file ModifyCourse.cls
@action courseModify
@description  Modify the existing course record with given courseID.
@param  courseID;int
@throws 3001;Invalid courseID
@throws 3067;CourseID does not exist
@throws 3000;Course modify failed 
@author Sachin Chaturvedi
@version 1.0
@created 12/09/2024
@Modification:
Date        Name          Comments 
1/08/2025    SRK           modified to create sysindex 
----------------------------------------------------------------------*/

class course.ModifyCourse inherits framework.ActionBase:
    
  {tt/course.i &tableAlias="ttCourse" }

  constructor ModifyCourse ():
    super().
  end constructor.

  destructor public ModifyCourse ():
  end destructor.  

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable dsHandle      as handle    no-undo.
    define variable std-ch        as character no-undo.
    define variable piCourseID    as integer   no-undo.
    define variable lSuccess      as logical   no-undo.


    define variable cName      as character no-undo.
    define variable lresult    as logical   no-undo.
    define variable cmsg       as character no-undo.
    
    &scoped-define msg-add + "^" +
           
    pRequest:getParameter("courseID", output piCourseID).

    /* validate courseID */
    if piCourseID = ? or piCourseID = 0
     then
      do:
        pResponse:fault("3001", "courseID").
        return. 
      end.

    /* validate courseID */
    if not can-find(first course where course.courseID = piCourseID)
     then
      do:  
        pResponse:fault("3067", "Course" {&msg-add} "ID" {&msg-add} string(piCourseID)).
        return.
      end.
      
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttCourse:handle).

    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > "" 
     then
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.
    
    lSuccess = false.
    trxBlock:
    for first course exclusive-lock
      where course.courseID = piCourseID transaction
      on error undo trxBlock, leave trxBlock:
    
      /* updating course record*/
      for first ttCourse no-lock: 

        assign
            course.stateID          = ttCourse.stateID
            course.type             = ttCourse.type
            course.name             = ttCourse.name
            course.description      = ttCourse.description
            course.isApproved       = ttCourse.isApproved
            course.approvedDate     = ttCourse.approvedDate
            course.approvedBy       = ttCourse.approvedBy
            course.creditHours      = ttCourse.creditHours
            course.paymentUrl       = ttCourse.paymentUrl
            course.instructor       = ttCourse.instructor
            course.notes            = ttCourse.notes
            course.courseType       = ttCourse.courseType
            course.approvedCourseID = ttCourse.approvedCourseID
            course.lastModifiedDate = now
            course.lastModifiedBy   = pRequest:uid
            cName                   = ttCourse.name
            .    
      end. /* for first ttCourse */
      
      empty temp-table ttCourse.
      buffer-copy course to ttCourse.

      for first state no-lock where state.stateid = course.stateID:
        ttCourse.stateDesc = state.description.
      end.

      validate course.
      release course.

      lSuccess = true.
    
    end. /* for first course*/
  
    if not lSuccess
     then 
      do:
        presponse:fault("3000", "Course update").
        return.
      end. /* if not lSuccess */

    /* create sysindex for course name */
   if cName ne ''  
    then
    /* Build keyword index to use for searching */
     run sys/createsysindex.p (input "CourseName",      /* ObjType */
                               input "",                /* ObjAttribute*/
                               input string(piCourseID), /* ObjID */
                               input cName,       /* ObjKeyList*/
                               output lsuccess,
                               output cmsg).
    
    if lSuccess
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
    
    pResponse:success("2000", "Course update").

  end method.
      
end class.


