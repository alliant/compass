/*------------------------------------------------------------------------
@name course/createcourse-p.p
@description Creates new course record

@param 
@param 
			
@author Sachin

@Modification
Date         Name          Description
1/08/2025    SRK           modified to create sysindex 
----------------------------------------------------------------------*/
{tt/course.i &tableAlias="ttCourse" }

define input  parameter table for ttCourse. 
define input  parameter pcUID         as character no-undo.
define output parameter opiCourseID   as integer   no-undo.
define output parameter oplSuccess    as logical   no-undo.
define output parameter opcMsg        as character no-undo.

define variable iCourseID  as integer   no-undo.
define variable cName      as character no-undo.
define variable lsuccess   as logical   no-undo.

{lib/nextkey-def.i}

trxBlock:
for first ttCourse 
    transaction on error undo trxBlock, leave trxBlock:  


     /* get new CourseID sequence */
    {lib/nextkey.i &type='courseID' &var=iCourseID &noResponse=true}.
    
    if iCourseID = -1 or iCourseID = 0 or iCourseID = ? 
     then
      do:
        assign
          oplSuccess = false
          opcMsg     = "Course Sequence failed"
        .
        return.
      end.

    /* create Course */

    create course.
    assign
        course.courseID          = iCourseID
        course.stateID           = ttCourse.stateID
        course.type              = ttCourse.type
        course.name              = ttCourse.name
        course.description       = ttCourse.description
        course.canSchedule       = ttCourse.canSchedule
        course.isApproved        = ttCourse.isApproved
        course.approvedDate      = ttCourse.approvedDate
        course.approvedBy        = ttCourse.approvedBy
        course.creditHours       = ttCourse.creditHours
        course.paymentUrl        = ttCourse.paymentUrl
        course.instructor        = ttCourse.instructor
        course.notes             = ttCourse.notes
        course.courseType        = ttCourse.courseType
        course.approvedCourseID  = ttCourse.approvedCourseID
        course.createdDate       = now
        course.createdBy         = pcUID
        cName                    = ttCourse.name
        .
            
    /* Set Default Values */
    assign
        course.lastmodifiedby   = ""
        course.lastmodifieddate = ?
        .

    validate course.
    release  course.

end.

/* create sysindex for course name */
if cName ne ''  
 then
  /* Build keyword index to use for searching */
  run sys/createsysindex.p (input "CourseName",      /* ObjType */
                            input "",                /* ObjAttribute*/
                            input string(iCourseID), /* ObjID */
                            input cName,       /* ObjKeyList*/
                            output lsuccess,
                            output opcMsg).
opiCourseID = iCourseID.

oplSuccess = true.
