/*------------------------------------------------------------------------
@file NewCourse.cls
@action courseNew
@description 

@returns 

@success 2005;

@author Sachin
@version 1.0
@created 12/05/2024
Modification:

----------------------------------------------------------------------*/

class course.NewCourse inherits framework.ActionBase:
    
  {tt/course.i &tableAlias="ttCourse" }

  constructor NewCourse ():
    super().
  end constructor.

  destructor public NewCourse ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable dsHandle      as handle    no-undo.
    define variable opiCourseID   as integer   no-undo.
    define variable oplSuccess    as logical   no-undo.
    define variable opcMsg        as character no-undo.
    define variable std-ch        as character no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttCourse:handle).

    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > "" 
     then
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.

  
    run course/createcourse-p.p(input table ttCourse,
                                input pRequest:uid,
                                output opiCourseID,
                                output oplSuccess,
                                output opcMsg).
    
 
    if not oplSuccess
     then
      pResponse:fault("3005", string(opcMsg)).

    empty temp-table ttCourse.
    for first course no-lock where course.courseID = opiCourseID:
      create ttCourse.
      buffer-copy course to ttCourse.

      for first state fields (description) where state.stateid = course.stateID no-lock:
        ttCourse.stateDesc = state.description.
      end.

    end.
    
    pResponse:success("2002", "New Course").

    if opiCourseID > 0 and opiCourseID <> ?
     then 
      do:
         setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
      end.      

  end method.
      
end class.


