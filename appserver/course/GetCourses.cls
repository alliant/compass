/*------------------------------------------------------------------------
@file GetCourses.Cls
@action coursesGet
@description 

@returns 

@success 2005;

@author SRK
@version 1.0
@created 12/18/2024
Modification:

----------------------------------------------------------------------*/

class course.GetCourses inherits framework.ActionBase:
    
  {tt/course.i      &tableAlias="ttCourse"}

  constructor GetCourses ():
    super().
  end constructor.

  destructor public GetCourses ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable dsHandle    as handle   no-undo.

    {lib/callsp-defs.i}
    {lib/std-def.i}

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttCourse:handle).


    {lib\callsp.i &name=spGetcourses &load-into=ttCourse &noResponse=true} 
    
    if csp-icount > 0
     then 
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList). 
    
    pResponse:success("2005", STRING(csp-icount) {&msg-add} "Courses").

             
  end method.
      
end class.


