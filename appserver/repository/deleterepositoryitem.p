&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*---------------------------------------------------------------------
@name deleterepositoryitem.p
@description Deletes the item in the folder

@param UID;character;The username for logging in
@param Pass;character;The password for logging in
@param Path;character;The path of the item to delete
@return Logical status of outcome
@return Error message

@author John Oliver
@version 1.0
@created 10.10.2019
@notes If the uid and password is not passed in, the admin credentials
       will be used. As the credentials are not transmitted to the client,
       this can be server-side only
---------------------------------------------------------------------*/

define input  parameter pUID      as character no-undo.
define input  parameter pPass     as character no-undo.
define input  parameter pPath     as character no-undo.
define output parameter pSuccess as logical   no-undo.
define output parameter pMsg     as character no-undo.

{lib/repository-procedures.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.95
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* login */
if pUID > "" and pPass > ""
 then pSuccess = setUserToken(pUID, pPass, output pMsg).
 else pSuccess = setAdminToken(output pMsg).

/* if login error, return */
if not pSuccess
 then return.

/* call procedure */
run DeleteRepositoryItem (pPath, output pSuccess, output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */


