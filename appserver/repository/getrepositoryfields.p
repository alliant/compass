/*------------------------------------------------------------------------
@file appserverTemplateGet
@action apinvGet
@description Get one or multiple record(s) in the Invoice table

@param ID;int;The row identifier

@returns The table dataset

@throws 3066;The row does not exist in the table

@success 2005;Invoice returned X rows

@author John Oliver
@version 1.0
@created XX/XX/20XX
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}
{tt/ini.i}

empty temp-table ini.
publish "GetSystemTable" (output table ini).

if not can-find(first ini)
 then pResponse:fault("3066","Server INI file").

/* if there is a fault thrown within the procedure, exit */
if pResponse:isFault()
 then return.

std-in = 0.
for each ini exclusive-lock:
  if ini.id begins "Repository" and not ini.id begins "RepositoryAdmin"
   then std-in = std-in + 1.
   else delete ini.
end.
   
IF std-in > 0
 then pResponse:setParameter("Ini", table ini). 

pResponse:success("2005", STRING(std-in) {&msg-add} "Repository Field").
