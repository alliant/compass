&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*---------------------------------------------------------------------
@name copyitem.p
@action repositoryItemCopy
@description Copies a repositoryitem

@param FileID;char;The file id of the file to delete

@author John Oliver
@version 1.0
@created 10.03.2019
@notes 
---------------------------------------------------------------------*/

define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

{lib/repository-procedures.i}

/* local variables */
define variable tAdminID as character no-undo.
define variable tAdminPW as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.95
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* get the user token */
for first sysuser no-lock
    where sysuser.uid = pRequest:uid:
    
  {&repo-token} = "".
  run GetRepositoryToken (sysuser.uid, sysuser.password, output std-lo, output std-ch).

  if not std-lo
   then pResponse:fault("3005", std-ch).
   else pResponse:setParameter("UserToken", {&repo-token}).
end.

if not pResponse:isFault()
 then pResponse:success("2000", "Repository Token").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */


