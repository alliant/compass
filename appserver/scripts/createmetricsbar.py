import os, sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm
from datetime import date

filename = sys.argv[1]
n_groups = sys.argv[2]
n_val_1  = sys.argv[3]
n_val_2  = sys.argv[4]
n_val_3  = sys.argv[5]

n_val_1 = list(n_val_1.split(",")) 
n_val_2 = list(n_val_2.split(",")) 
n_val_3 = list(n_val_3.split(",")) 

for i in range(0, len(n_val_1)): 
 n_val_1[i] = int(n_val_1[i]) 
 
for i in range(0, len(n_val_2)): 
 n_val_2[i] = int(n_val_2[i]) 
 
for i in range(0, len(n_val_3)): 
 n_val_3[i] = int(n_val_3[i]) 

current_date = date.today() 

fontsize2use = 20

fig = plt.figure(figsize=(20,10))
plt.xticks(fontsize=fontsize2use)  
plt.yticks(fontsize=fontsize2use)    

fontprop = fm.FontProperties(size=fontsize2use)
ax = fig.add_subplot(111)
ax.set_xlabel('Months', size = 20)
ax.set_ylabel('Premium', size = 20)

# create plot
index = np.arange(int(n_groups))
bar_width = 0.25
opacity = 0.8

rects1 = plt.bar(index, n_val_1, bar_width,
alpha=opacity,
color='lightcyan',
label=str(current_date.year - 2) ,  edgecolor='navy')

rects2 = plt.bar(index + bar_width, n_val_2, bar_width,
alpha=opacity,
color='cyan',
label=str(current_date.year - 1) ,  edgecolor='navy')

rects3 = plt.bar(index + bar_width + bar_width, n_val_3, bar_width,
alpha=opacity,
color='teal',
label=str(current_date.year) ,  edgecolor='navy')


plt.xticks(index + (bar_width / 2 ) + (bar_width / 2 ), ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'))

plt.legend(loc=0, prop=fontprop)

plt.tight_layout()
plt.savefig(filename)
