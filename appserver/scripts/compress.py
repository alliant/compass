import os, sys
inImage = sys.argv[1]
outImage = sys.argv[2]
cXSize = sys.argv[3]
cYSize = sys.argv[4]
cCompSize = sys.argv[5]
from PIL import Image
xSize = int(cXSize)
ySize = int(cYSize)
compSize = int(cCompSize)

def compressImage(reImg, qual, xxSize, yySize):    
    imgToResize = Image.open(reImg)
    imgToResize = imgToResize.resize((xxSize,yySize),Image.BICUBIC)
    imgToResize.save(outImage,optimize=True,quality=qual) 
    updSize = os.stat(outImage).st_size
    return updSize

imgToResize = Image.open(inImage)
imgToResize = imgToResize.resize((xSize,ySize),Image.NEAREST)
imgToResize.save(outImage,optimize=True,quality=100)

imgSize = os.stat(outImage).st_size  

quality = 99
while imgSize > compSize or quality == 0:
    if quality < 2 :
        break
    compressImage(outImage, quality,xSize,ySize) 
    imgSize = os.stat(outImage).st_size
    quality -= 2
    
quality = 99
while imgSize > compSize or quality == 0:
    if quality == 0 :
        break
    compressImage(outImage, quality,xSize,ySize) 
    imgSize = os.stat(outImage).st_size
    if quality > 1:
        quality -= 1
    if quality == 1:
        xSize -= 10
        ySize -= 10                 
