#Log file to be read
$readlogfile=$args[0]

#Final output file to be send on mail
$outfile=$args[1]

#File for storing errors
$errorfile=$args[2]

#File for storing last read date
$lastdatefile=$args[3]

#using last file read date
$inputdate=$args[4]

#exclude lines which contains words in this list
$excludelist=$args[5]

$log = Get-Content $readlogfile | Where {$_.trim()}
   
$lowerLimit = 0
if (($inputdate -ne $null) -and ($inputdate -ne "") -and (!(([DateTime]::TryParseexact($inputdate, "ddd MM/dd/yyyy - HH:mm:ss.ff",[system.Globalization.DateTimeFormatInfo]::InvariantInfo,[system.Globalization.DateTimeStyles]::None, [ref]$lowerLimit)) -or
[DateTime]::TryParseexact($inputdate, "ddd MM/dd/yyyy -  H:mm:ss.ff",[system.Globalization.DateTimeFormatInfo]::InvariantInfo,[system.Globalization.DateTimeStyles]::None, [ref]$lowerLimit))))
{
  Write "Supplied last read date is invalid: $inputdate" | Add-Content -Path $errorfile -Force
  if (Test-Path $lastdatefile -PathType leaf)
  {
    Remove-Item -Path $lastdatefile -Force
  } 
  break
}

$count = 0
$log | foreach {
  $dateAsText = $_.Split("C")[0]

  $dateLength = $dateAsText.Length
  if ($dateLength -gt 1) {
  $dateAsText = $dateAsText.Substring(0,($dateLength - 2))}

  $weekDays  = "Sun,Mon,Tue,Wed,Thu,Fri,Sat"
  $startText = $_.split(" ")[0]

  $date = 0
  $Matchfound = 0

  if(($startText -ne "") -and ($weekDays.IndexOf($startText) -gt -1) -and ($_.Indexof("touch") -lt 0))
  {
    $beforeStartText = $_
    if (!(([DateTime]::TryParseexact($dateAsText, "ddd MM/dd/yyyy - HH:mm:ss.ff",[system.Globalization.DateTimeFormatInfo]::InvariantInfo,[system.Globalization.DateTimeStyles]::None, [ref]$date) -or 
     [DateTime]::TryParseexact($dateAsText, "ddd MM/dd/yyyy -  H:mm:ss.ff",[system.Globalization.DateTimeFormatInfo]::InvariantInfo,[system.Globalization.DateTimeStyles]::None, [ref]$date))))
    {
      Write "Line cannot be reported. since, date is invalid at line: $_" | Add-Content -Path $errorfile -Force
    } 
  }
  elseif (($_.indexof(" (") -gt 0) -or ($_.indexof("[") -gt 0) -or ($_.indexof("act.p") -gt 0) -or (($_.indexof("   ") -gt 0) -and ($startText -ne "&1") -and ($_.indexof("   |") -lt 0)))
  {  
    if (($inputdate -ne $null) -and ($inputdate -ne ""))
    {
      $dateAsText = $beforeStartText.Split("C")[0]
      $dateLength = $dateAsText.Length
      if ($dateLength -gt 1) {
      $dateAsText = $dateAsText.Substring(0,($dateLength - 2))}

      if ((([DateTime]::TryParseexact($dateAsText, "ddd MM/dd/yyyy - HH:mm:ss.ff",[system.Globalization.DateTimeFormatInfo]::InvariantInfo,[system.Globalization.DateTimeStyles]::None, [ref]$date) -or 
      [DateTime]::TryParseexact($dateAsText, "ddd MM/dd/yyyy -  H:mm:ss.ff",[system.Globalization.DateTimeFormatInfo]::InvariantInfo,[system.Globalization.DateTimeStyles]::None, [ref]$date))))
      {
        if ($lowerLimit -lt $date) 
        { 
          $MatchFound = $beforeStartText  
          if (($Matchfound -ne $null) -and ($Matchfound -ne "")) 
          { 
            $Matchfound | Add-Content -Path $outfile -Force  #output the current item because it satisfy condition
          }
          if (($excludelist -ne $null) -and ($excludelist -ne ""))
          {	 
            $Matchfound = $_ | Select-String -Pattern $excludelist -NotMatch
          }
          if (($Matchfound -ne $null) -and ($Matchfound -ne "")) 
          { 
            $count = $count + 1
            $Matchfound | Add-Content -Path $outfile -Force  #output the current item because it satisfy condition
          } 
        }
      }
    } 
    else
    { 
      $MatchFound = $beforeStartText        
      if (($Matchfound -ne $null) -and ($Matchfound -ne "")) 
      { 
        $Matchfound | Add-Content -Path $outfile -Force  #output the current item because it satisfy condition
      } 
      if (($excludelist -ne $null) -and ($excludelist -ne ""))
      {   
        $Matchfound = $_ | Select-String -Pattern $excludelist -NotMatch 
      }	 
      if (($Matchfound -ne $null) -and ($Matchfound -ne "")) 
      { 
        $count = $count + 1
        $Matchfound | Add-Content -Path $outfile -Force  #output the current item because it satisfy condition
      }
    }
  }	         
}

"$dateAsText,$count" | Out-File -FilePath $lastdatefile


