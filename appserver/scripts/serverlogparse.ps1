#Log file to be read
$readlogfile=$args[0]

#Final output file to be send on mail
$outfile=$args[1]

#File for storing errors
$errorfile=$args[2]

#File for storing last read date
$lastdatefile=$args[3]

#using last file read date
$inputdate=$args[4]

#exclude lines which contains words in this list
$excludelist=$args[5]

$log = Get-Content $readlogfile | Where {$_.trim()}
   
$lowerLimit = 0
if (($inputdate -ne $null) -and ($inputdate -ne "") -and (!([DateTime]::TryParseexact($inputdate, "yy/MM/dd@HH:mm:ss.fff",[system.Globalization.DateTimeFormatInfo]::InvariantInfo,[system.Globalization.DateTimeStyles]::None, [ref]$lowerLimit))))
{
  Write "Supplied last read date is invalid: $inputdate" | Add-Content -Path $errorfile -Force
  if (Test-Path $lastdatefile -PathType leaf)
  {
    Remove-Item -Path $lastdatefile -Force
  } 
  break
}

$count = 0
$log | foreach {
  $dateAsText = $_.trim("[").Split("]")[0].Split("-")[0].Split("+")[0]
  
  $date = 0
  $Matchfound = 0
  
  if (!([DateTime]::TryParseexact($dateAsText, "yy/MM/dd@HH:mm:ss.fff",[system.Globalization.DateTimeFormatInfo]::InvariantInfo,[system.Globalization.DateTimeStyles]::None, [ref]$date)))
  {
    Write "Line cannot be reported. since, date is invalid at line: $_" | Add-Content -Path $errorfile -Force 
  }  
  elseif (($inputdate -ne $null) -and ($inputdate -ne ""))
  {  
    if ($lowerLimit -lt $date) 
    { 
      if (($excludelist -ne $null) -and ($excludelist -ne ""))
      {	  
        $Matchfound = $_ | Select-String -Pattern $excludelist -NotMatch | Select-String -Pattern "Procedure:", "Microsoft" 
      }
      else
      {
        $Matchfound = $_ | Select-String -Pattern "Procedure:", "Microsoft" 
      }	  
      if (($Matchfound -ne $null) -and ($Matchfound -ne "")) 
      { 
        $count = $count + 1
        $Matchfound | Add-Content -Path $outfile -Force  #output the current item because it satisfy condition
      } 
    }
  }
  else
  {
    if (($excludelist -ne $null) -and ($excludelist -ne ""))
    {	  
      $Matchfound = $_ | Select-String -Pattern $excludelist -NotMatch | Select-String -Pattern "Procedure:", "Microsoft" 
    }
    else
    {
      $Matchfound = $_ | Select-String -Pattern "Procedure:", "Microsoft" 
    }	 
    if (($Matchfound -ne $null) -and ($Matchfound -ne "")) 
    { 
      $count = $count + 1
      $Matchfound | Add-Content -Path $outfile -Force  #output the current item because it satisfy condition
    } 
  }  
}

"$dateAsText,$count" | Out-File -FilePath $lastdatefile