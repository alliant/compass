from extract_msg import message
from email.header import decode_header
from dateutil import parser
import imghdr
import json
import base64
import sys,os
import email
from email.utils import parsedate_to_datetime
import re

email_file_type = ''
email_file_path = ''
communication_json_path = ''
out_email_attachment_dir= ''
html_body = ''
text_body = ''
body = ''
email_from = ''
email_to = []
email_cc = []
email_subject = ''
email_sent_date = None
email_attachments = []
reciepients = []
json_data = {}
exec_files = ['exe', 'bat', 'com', 'cmd', 'inf', 'ipa', 'osx', 'pif', 'wsh', 'run']
address_pattern = r'<(.*?@.*?)>'
    
def replace_cid_with_image (html_body, inline_image):

    for cid, image in inline_image.items ():
        image_format = imghdr.what(None, h=image)
       
        image_base64 = base64.b64encode(image).decode('utf-8')
        data_uri = f"data:image/{image_format};base64,{image_base64}"
        html_body = html_body.replace(f"cid:{cid}", data_uri)
    
    return html_body

def parse():
    if not validate ():
        return
    if email_file_type == ".msg":
        parse_msg (email_file_path)
    elif email_file_type == ".eml":
        with open(email_file_path, 'rb') as email_file:
            parse_eml (email_file)
    json_string = json.dumps(json_data)
    with open(communication_json_path, "w") as json_file:
        json_file.write(json_string)    


def  subject_decode (encoded_subject) -> str:
    decode_subject = []
    for subject, encode in decode_header(encoded_subject):
        if encode:
            decode_subject.append(subject.decode(encode))
        else:
            decode_subject.append(subject)
    return ' '.join(decode_subject)


def parse_msg (msg_file):
    html_body = ''
    text_body = ''
    body = ''
    email_from = ''
    email_to = []
    email_cc = []
    email_subject = ''
    email_sent_date = None
    email_attachments = []  
    reciepients = []
    exec_files_exists = False

    msg_message = message.Message(msg_file)
    email_from = msg_message.sender
    email_to = msg_message.to.split(';')
    if msg_message.cc:
        email_cc = msg_message.cc.split(';') if msg_message.cc.count(';') > 0 else []
    email_subject = msg_message.subject
    email_sent_date = msg_message.date
    text_body = msg_message.body
    html_body = msg_message.getSaveHtmlBody ().decode('utf-8')

    if msg_message.inReplyTo:
        json_data['Error'] = 'Cannot upload conversation email file'
        return
    for att in msg_message.attachments:
        attachment_data = att.data
        attachment_file_path = os.path.join(out_email_attachment_dir, att.getFilename ())
        filte_type = os.path.splitext(attachment_file_path)[1][1:]
        if filte_type in exec_files:
            exec_files_exists = True
            break
        with open(attachment_file_path, "wb") as attachment:
            attachment.write(attachment_data)
        if att.hidden:
            cid = att.cid
            if cid:
                email_attachments.append(f"{attachment_file_path}|{cid}")
        else:
            email_attachments.append(attachment_file_path)
        
    if exec_files_exists:
        json_data['Error'] = 'Email contains executable attachment(s)'
        return
    body = html_body if html_body != '' else text_body

    for temp_email_to in email_to:
        pattern_match = re.search(address_pattern, temp_email_to)
        if pattern_match:
            reciepients.append(pattern_match.group(1))
        else:
            reciepients.append(temp_email_to)
    for temp_email_cc in email_cc:
        pattern_match = re.search(address_pattern,temp_email_cc)
        if pattern_match:
            reciepients.append(pattern_match.group(1))
        else:
            reciepients.append(temp_email_cc)

    json_data["Sender"] = email_from
    json_data["Recipients"] = reciepients
    json_data["SentDate"] = parsedate_to_datetime(email_sent_date).isoformat()
    json_data["Subject"] = email_subject
    json_data["Attachments"] = email_attachments

    encoded_bytes = base64.b64encode(body.encode('utf-8'))
    encoded_body = encoded_bytes.decode('utf-8')
    json_data["Body"] = encoded_body    


def parse_eml (eml_file):
    exec_files_exists = False

    message = email.message_from_binary_file(eml_file)
    ## extracting headers
    email_from = message.get("From")
    email_to = message.get_all("To", [])
    email_cc = message.get_all("Cc", [])

    email_sent_date = parser.parse(message.get("date")).isoformat()

    email_subject = subject_decode(message.get("Subject", ""))
    for temp_email_to in email_to:
        pattern_match = re.search(address_pattern, temp_email_to)
        if pattern_match:
            reciepients.append(pattern_match.group(1))
        else:
            reciepients.append(temp_email_to)
    for temp_email_cc in email_cc:
        pattern_match = re.search(address_pattern, temp_email_cc)
        if pattern_match:
            reciepients.append(pattern_match.group(1))
        else:
            reciepients.append(temp_email_cc)
    if message.is_multipart():
        for message_part in message.walk():
            content_type = message_part.get_content_type()
            content_disposition = str(message_part.get("Content-Disposition"))
            if message_part.get_content_maintype() == "multipart":
                continue
            filename = message_part.get_filename ()
            if filename != None :
                attachment_data = message_part.get_payload(decode=True)
                attachment_file_path = os.path.join(out_email_attachment_dir, filename)
                filte_type = os.path.splitext(attachment_file_path)[1][1:]
                if filte_type in exec_files:
                    exec_files_exists = True
                    break
                with open(attachment_file_path, "wb") as attachment:
                    attachment.write(attachment_data)
                
                if not "attachment" in content_disposition:
                    cid = message_part.get("Content-ID")
                    if cid:
                        cid = cid.strip('<>')
                        email_attachments.append(f"{attachment_file_path}|{cid}")
                else:
                    email_attachments.append(attachment_file_path)
            if content_type == "text/plain":
                text_body = message_part.get_payload(decode=True).decode(message_part.get_content_charset(),'ignore')
            if content_type == 'text/html':
                html_body = message_part.get_payload(decode=True).decode(message_part.get_content_charset(),'ignore')

    if exec_files_exists:
        json_data['Error'] = 'Email contains executable attachment(s)'
        return
    body = html_body if html_body != '' else text_body

    json_data["Sender"] = email_from
    json_data["Recipients"] = reciepients
    json_data["SentDate"] = email_sent_date
    json_data["Subject"] = email_subject
    json_data["Attachments"] = email_attachments
    encoded_bytes = base64.b64encode(body.encode('utf-8'))
    encoded_body = encoded_bytes.decode('utf-8')
    json_data["Body"] = encoded_body
    

def validate () -> bool:
    if email_file_path == '':
        sys.stderr.write("Email file path cannot be blank")
        return False
    
    if email_file_type == '':
        sys.stderr.write("Email file type cannot be blank") 
        return False
    
    if not os.path.exists(email_file_path):
        sys.stderr.write("Email file does not exits in file system")
        return False

    if email_file_type != ".msg" and email_file_type != ".eml":
        sys.stderr.write("Invalid email file type")
        return False

    if communication_json_path == '':
        sys.stderr.write("Script communication path cannot be blank")
        return False

    if out_email_attachment_dir =='':
        sys.stderr.write("Email attachment output file cannot be blank")     
        return False   
    
    return True


if __name__ == "__main__":
    email_file_type = sys.argv[1]
    email_file_path = sys.argv[2]
    communication_json_path = sys.argv[3]
    out_email_attachment_dir = sys.argv[4]
    parse()