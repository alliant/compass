import os, sys
import openpyxl
from datetime import datetime
from openpyxl.styles import Font, Alignment, PatternFill, Border, Side
from openpyxl.utils import get_column_letter
import json

#arguments from cmd line
file_path         = sys.argv[1]
agentid           = sys.argv[2]
agentname         = sys.argv[3]
agentaddr         = sys.argv[4]
agentmanager      = sys.argv[5]
agentmanagername  = sys.argv[6]
startdate         = sys.argv[7]
enddate           = sys.argv[8]
output_file       = sys.argv[9] #Replace with desired output file path

#reading json data
with open(file_path,'r') as file:
    data = json.load(file)

ttfilears = data.get('filear',[])

def format_address(address: str) -> str:
    """
    Formats a given address string such that each line does not exceed 30 characters.
    Words are separated by commas, and a newline character separates each line.
 
    :param address: The input address string
    :return: The formatted address string
    """
    formatted_address = ""
    current_line = ""
    current_pos = 0
 
    while current_pos < len(address):
        # Find the next comma or end of the string
        next_comma_pos = address.find(",", current_pos)
 
        if next_comma_pos == -1:
            # No more commas, take the rest of the string
            word = address[current_pos:].strip()
        else:
            # Extract the word until the next comma
            word = address[current_pos:next_comma_pos].strip()
 
        # Check if adding the word exceeds 30 characters
        if len(current_line) + len(word) + (1 if current_line else 0) <= 30:
            if current_line:
                current_line += "," + word
            else:
                current_line = word
        else:
            # Add the current line to formatted address
            formatted_address += current_line + "\n"
            current_line = word
 
        # Move the position past the comma or end of the string
        if next_comma_pos == -1:
            break
        current_pos = next_comma_pos + 1
 
    # Add the last line if there's any remaining text
    if current_line:
        formatted_address += current_line
 
    return formatted_address

#data formatting
address = agentaddr.split(";")
formatted_address = "\n".join(address)

# Parse the date string into a datetime object
enddate_obj = datetime.strptime(enddate, '%m/%d/%y')
 
# Get the month name and year
month_name = enddate_obj.strftime('%B')  # Full month name
year = enddate_obj.strftime('%Y')  # Full year (4 digits)

# Font Styling
bold_font = Font(bold=True)  
italic_font = Font(italic=True)
blue_font = Font(color="0000FF")  # Blue text for "City" column

# Cell Alignment (Center alignment for headers and values)
centered_alignment = Alignment(horizontal="center", vertical="center")
left_alignment = Alignment(horizontal="left", vertical="top",wrap_text=True)
leftbottom_alignment = Alignment(horizontal="left", vertical="bottom",wrap_text=True)

# Load the existing Excel template
template_file = "C:/Python/wrk/template_activity_report.xlsx"
workbook = openpyxl.load_workbook(template_file)

# Select the active sheet (or any specific sheet by name)
sheet = workbook.active  # You can also use workbook['SheetName'] if you know the sheet name

# Merge A5 and B5 first part in header
existing_value = sheet['A5'].value
new_value  = existing_value + " " + agentid
sheet['A5'] = new_value

sheet.merge_cells('A6:B7')
merged_cell = sheet['A6']
merged_cell.alignment = left_alignment
merged_cell.value = agentname + '\n' + formatted_address

# Merge E6 and G6 last in header
sheet.merge_cells('E6:G6')
merged_cell = sheet['E6']
merged_cell.alignment = left_alignment
merged_cell.value = "Agency Manager " + '\n' + agentmanagername + '\n' + agentmanager

# Set number format for specific columns
columns_to_format = ['D', 'E', 'F', 'G']
number_format = '0.00;(0.00);""'

#Add data dynamically (for example, filling in a table or writing multiple rows)
#Creating data rows
iRowNumber = 12
 
deTotalInvoiced = 0.0
deTotalCancelled = 0.0
deTotalPaid = 0.0
deTotalBalance = 0.0
 
for ttfilear in ttfilears:
    deBalance = 0.0
    sheet.cell(row=iRowNumber, column=1).value = ttfilear['fileID']

    # trandate format MM/DD/YYYY
    tran_date = ttfilear['tranDate']
    if tran_date:
        if isinstance(tran_date, str):
            tran_date = datetime.strptime(tran_date,'%Y-%m-%dT%H:%M:%S.%f').date()
        elif isinstance(tran_date, datetime):
              tran_date = tran_date.date()
        sheet.cell(row=iRowNumber, column=2).value = tran_date
        sheet.cell(row=iRowNumber, column=2).number_format = 'MM/DD/YYYY'
    else:
        sheet.cell(row=iRowNumber, column=2).value = ""
    
    # format the property address
    property_info = format_address(ttfilear['propertyInfo'])
    cell = sheet.cell(row=iRowNumber, column=3)
    cell.value = property_info
    cell.alignment = left_alignment 
    
    sheet.cell(row=iRowNumber, column=4).value = ttfilear['invoicedAmount']
    sheet.cell(row=iRowNumber, column=5).value = ttfilear['cancelledAmount']
    sheet.cell(row=iRowNumber, column=6).value = ttfilear['paidAmount']
 
    deBalance = (
        ttfilear['invoicedAmount'] + ttfilear['cancelledAmount'] + ttfilear['paidAmount']
    )
    sheet.cell(row=iRowNumber, column=7).value = deBalance
 
    # Add current row amounts to total
    deTotalInvoiced += ttfilear['invoicedAmount']
    deTotalCancelled += ttfilear['cancelledAmount']
    deTotalPaid += ttfilear['paidAmount']
    deTotalBalance += deBalance
 
    # Apply formatting to each data row
    for colNumber in range(1, 3):
        cell = sheet.cell(row=iRowNumber, column=colNumber)
        cell.alignment = Alignment(horizontal="left")
        cell.font = Font(size=12)

    for col in range(4,8):
        cell = sheet.cell(row=iRowNumber , column=col)
        cell.number_format = number_format

     
    iRowNumber += 1

iRowNumber +=1
# Write "Total:" in column C and apply bold font
sheet.cell(row=iRowNumber, column=3, value="Total:")
sheet.cell(row=iRowNumber, column=3).font = Font(bold=True)
sheet.cell(row=iRowNumber, column=3).alignment = Alignment(horizontal='right')
 
# Write totals in columns D, E, F, and G and apply bold font
sheet.cell(row=iRowNumber, column=4, value=deTotalInvoiced)
sheet.cell(row=iRowNumber, column=4).font = Font(bold=True)
sheet.cell(row=iRowNumber, column=4).number_format = number_format
 
sheet.cell(row=iRowNumber, column=5, value=deTotalCancelled)
sheet.cell(row=iRowNumber, column=5).font = Font(bold=True)
sheet.cell(row=iRowNumber, column=5).number_format = number_format

sheet.cell(row=iRowNumber, column=6, value=deTotalPaid)
sheet.cell(row=iRowNumber, column=6).font = Font(bold=True)
sheet.cell(row=iRowNumber, column=6).number_format = number_format

sheet.cell(row=iRowNumber, column=7, value=deTotalBalance)
sheet.cell(row=iRowNumber, column=7).font = Font(bold=True)
sheet.cell(row=iRowNumber, column=7).number_format = number_format

# creating middle part of header
# Merge C5 and D5 midpart in header
sheet['C5'].value = ""
existing_value = sheet['C5'].value
new_value  = existing_value + month_name + " " + year
sheet['C5'] = new_value

# Merge C6 and C7
sheet.merge_cells('C6:D7')
merged_cell = sheet['C6']
merged_cell.alignment = left_alignment
merged_cell.value = (
    f"Statement Date:                        {enddate}\n"
    f"Statement Period: {startdate} - {enddate}\n"
    f"Balance Due:                                 {deTotalBalance:.2f}"
)
# Save the modified workbook as a new file
# Replace with desired output file path
workbook.save(output_file)
