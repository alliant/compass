from email import encoders
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
import logging
import smtplib
import sys
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from enum import IntEnum
import string
import os
import base64
from bs4 import BeautifulSoup as bs
import re

##store system parameters
smtp_server_address = ''
from_address = ''
to_address = ''
cc_address = ''
body = ''
subject = ''
attachments = ''
local_files = ''
importance = ''

class ImportanceLevel(IntEnum):
   High = 1
   Normal = 2
   Low = 3


def  convert_base64_images ():
   
  if not body:
    return '',[]
  # parse all html content and get aLL
  soup = bs(body, 'html.parser')
  # if soup is None that means the mail would be of tye text not html
  if not soup:
    return body,[]
  img_tags = soup.find_all('img')
  inline_attachments = []
  if not img_tags:
    return body,[]
  
  # iterare to all img tags
  for index, img in enumerate(img_tags):
    img_src = img.get('src')

    if img_src and img_src.startswith ('data:image'):
      img_base64_data = re.search(r'data:(image/\w+);base64,(.*)', img_src)
      if img_base64_data:
        mime_type =img_base64_data.group(1) # gives the image type
        img_base64_str = img_base64_data.group(2) # actual base64 image data string
          
        # Generating cid for the image
        cid = f'image{index}@alliantnational.com'
        img['src'] = f'cid:{cid}'
          
        # decode the base64 image
        img_data = base64.b64decode(img_base64_str)
        img_mime = MIMEImage(img_data, mime_type.split('/')[1])
        img_mime.add_header('Content-ID', f'<{cid}>')
        inline_attachments.append(img_mime)
  return str(soup), inline_attachments
  

def send_mail () -> bool:
  try:
    temp_to_address = to_address.split(';')
    email_to = ','.join(temp_to_address)
    temp_cc_address = cc_address.split(';')
    email_cc = ','.join(temp_cc_address)
    email_subject = subject
    email_body, html_inline_attachments = convert_base64_images ()
    email_from = ''
    if len(from_address.split(';')) > 1:
      email_from = from_address.split(';')[1] + '<' + from_address.split(';')[0] + '>'
    else:
      email_from = from_address.split(';')[0]
    importance_level = None
    email_recipient = email_to.split(",") + email_cc.split(",")

    if importance.isnumeric() and (int(importance) in iter(ImportanceLevel)):
      importance_level = int(importance)

    message = MIMEMultipart()

    message["From"] = email_from
    msg_to = ''
    for person in email_to.split(','):
      msg_to += "{0}, ".format(person)
    message["To"] = msg_to
    message["Cc"] = email_cc
    message['Date'] = formatdate(localtime=True)
    message["Subject"] = email_subject
    if importance_level != None:
      message['Importance'] = ImportanceLevel(importance_level).name

    message.attach(MIMEText(email_body, "html"))
    email_attachments = []
    attachments_props = []

    if attachments != '':
      attachments_props = attachments.strip().split(',')
    if local_files != '':
      email_attachments = local_files.strip().split(',')
    for email_attachment in range(len(email_attachments)):
        props = attachments_props[email_attachment]
        props_list = props.split(':')
        filename = props_list[0]
        char_set = ''
        mime_type = ''
        file_type = ''
        disposition = ''

        for prop in props_list:
            prop_key = prop.split('=')[0]
            if prop_key.lower() == 'type':
              mime_type = prop.split('=')[1]
            elif prop_key.lower() == 'charset':
              char_set = prop.split('=')[1]
            elif prop_key.lower() == 'filetype':
              file_type = prop.split('=')[1]
            elif prop_key.lower() == 'disposition':
              disposition = prop.split('=')[1]

        if disposition == '':
          with open(email_attachments[email_attachment],"rb") as file:
              part = MIMEApplication(
                file.read(),
                Name=basename(email_attachments[email_attachment])
              )
              part.add_header('Content-Disposition','attachment; filename="%s"' % basename(email_attachments[email_attachment]))
              encoders.encode_base64(part)
              message.attach(part)
        elif filename.startswith('alliantlogo'):
            with open(email_attachments[email_attachment], "rb") as inline_img:
              image_alliant = MIMEImage(inline_img.read(), name=filename)
              image_alliant.add_header("Content-ID","<image@alliantnational.com>")
              image_alliant.add_header('Content-Disposition','inline; filename="%s"' % basename(email_attachments[email_attachment]))
              message.attach(image_alliant)
        elif filename.startswith('cid__') or filename.startswith('CID__'):
            with open(email_attachments[email_attachment], "rb") as inline_img:
              image_inline = MIMEImage(inline_img.read(), name=filename)
              filename_without_extn = os.path.splitext(os.path.basename(filename))[0]
              image_inline.add_header("Content-ID", filename_without_extn)
              image_inline.add_header('Content-Disposition','inline; filename="%s"' % basename(email_attachments[email_attachment]))
              message.attach(image_inline)
        else:
            with open(email_attachments[email_attachment], "rb") as inline_img:
              image = MIMEImage(inline_img.read(), name=filename)
              image.add_header("Content-ID",filename.split('.')[0] + "@alliantnational.com>")
              image.add_header('Content-Disposition','inline; filename="%s"' % basename(email_attachments[email_attachment]))
              message.attach(image)
    for html_inline_attachment in html_inline_attachments:
      message.attach(html_inline_attachment)

    log_message = '''
EmailFrom   : {emailFrom}
EmailTo     : {emailTo}
EmailCc     : {emailCc}
Attachments : {attachments}
LocalFiles  : {localFiles}
Subject     : {subject}
Body        : {body}
Importance  : {importance}
Recipients  : {recipents}
'''.format(emailFrom=email_from, emailTo=message["To"], emailCc=email_cc, attachments=attachments,localFiles=local_files,
                             subject=email_subject,body=email_body, importance=importance_level, recipents=email_recipient)
    logging.info(log_message)
    logging.info("Connecting to SMTP server")
    smtp_server = smtplib.SMTP(smtp_server_address)
    logging.info("Connected to SMTP server")
    smtp_server.sendmail(email_from, email_recipient, message.as_string())
    logging.info("Email Queued to send")
    smtp_server.close()
    logging.info("SMTP connection closed")
  except Exception as err:
     logging.error("Something went wrong " + str(err))
     with open(sys.argv[11].strip(),"w") as error_stream:
      error_stream.write(str(err))

## program entry point
if __name__ == "__main__":   
  smtp_server_address = sys.argv[1].strip()
  to_address = sys.argv[2].strip()
  from_address = sys.argv[3].strip()
  cc_address = sys.argv[4].strip()
  subject = sys.argv[6]
  attachments = sys.argv[7].strip()
  local_files = sys.argv[8].strip()
  importance =  sys.argv[9].strip()
  log_file = sys.argv[10].strip()

  with open(sys.argv[5].strip(),"r") as body_stream:
    body = body_stream.read()

  ##removing any inprintable characters
  subject = ''.join(filter(lambda x: x in string.printable, subject))

  logging.basicConfig(filename=log_file, filemode='a' ,format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level='DEBUG')
  logging.info("-"*15 + "New Mail" + "-"*15)

  logging.info("Started sending email")

  send_mail()




