/*------------------------------------------------------------------------
@name DeleteParticipant.cls
@action trainingDelete
@description  Deletes Participant record form Participant table.
@param TraningID;int
@throws 3067;Participant ID does not exist
@throws 3005;The Training cannot be deleted as it is in use
@throws 3000;delete training failed
@returns Success;2000
@author S Chandu
@version 1.0
@created 12.09.2024
@Modified :
Date        Name          Comments  

----------------------------------------------------------------------*/

class participant.DeleteParticipant inherits framework.ActionBase:
      
  constructor DeleteParticipant ():
    super().
  end constructor.

  destructor public DeleteParticipant ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
            
    /*parameter variables*/
    define variable piParticipantID  as integer     no-undo.
    define variable cstatdesc        as character   no-undo.
    define variable std-lo           as logical     no-undo.
    &scoped-define msg-add + "^" +
                
    pRequest:getParameter("ParticipantID", output piParticipantID).

    /* validation when no parameter is supplied */
    if (piParticipantID = 0  or piParticipantID = ?)
     then 
      do:
        pResponse:fault("3001", "ParticipantID").
        return.
      end.

    if not can-find(first participant where participant.participantID = piParticipantID)
     then
      do: 
        pResponse:fault("3066", "Participant").
        return.
      end. 


    for first participant where participant.participantID = piParticipantID no-lock :
      if can-find(first training where training.trainingID = participant.trainingID and training.stat = 'C')
       then
        do:
          pResponse:fault("3005", "Participant cannot be Added to a completed training.").
          return.
        end.

      if can-find(first training where training.trainingID = participant.trainingID  and training.stat = 'X')
       then
        do:
          pResponse:fault("3005", "Participant cannot be Added to a cancelled training.").
          return.
        end.
    end.
      
    std-lo = false.
    
    trxBlock:
    for first participant exclusive-lock
      where participant.participantID = piParticipantID  transaction
      on error undo trxBlock, leave trxBlock:
      
      delete participant.
      release participant.
      
      std-lo = true.
    end.
    
    if not std-lo
     then
      do: 
        pResponse:fault("3000", "Delete participant").
        return.
      end.
    
    pResponse:success("2000", "Delete participant").
    
  end method.
      
end class.


