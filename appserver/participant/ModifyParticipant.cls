/*------------------------------------------------------------------------
@file ModifyParticipant.cls
@action participantModify
@description Modify the existing participant record for participantID.
@param participantID;int
@throws 3005;Data Parse failed
@throws 3001;Invalid participantID
@throws 3067;ParticipantID does not exist
@throws 3000;Participant modify failed
@author Shefali
@version 1.0
@created 12/20/2024
Modification:
Date        Name          Comments
01/08/2024  SRK           Modified to add sysindex for name
----------------------------------------------------------------------*/

class participant.ModifyParticipant inherits framework.ActionBase:
    
  {tt/participant.i &tableAlias="ttparticipant" }

  constructor ModifyParticipant ():
    super().
  end constructor.

  destructor public ModifyParticipant ():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable dsHandle         as handle    no-undo.
    define variable ipiParticipantID as integer   no-undo.
    define variable lSuccess         as logical   no-undo.
    define variable std-ch           as character no-undo.
    define variable cName            as character no-undo.
    define variable opcMsg           as character no-undo.
	  
	&scoped-define msg-add + "^" +

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttparticipant:handle).

    pRequest:getParameter("ParticipantID", output ipiParticipantID).

    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > "" 
     then
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.

    /* validate participantID */
    if ipiParticipantID = ? or ipiParticipantID = 0
     then
      do:
        pResponse:fault("3001", "ParticipantID").
        return. 
      end.

    if not can-find(first participant where participant.participantID = ipiParticipantID )
     then
      do:
        pResponse:fault("3067", "Participant" {&msg-add} "ID" {&msg-add} string(ipiParticipantID)).
        return. 
      end.

    lSuccess = false.
    trxBlock:
    for first participant exclusive-lock
      where participant.participantID = ipiparticipantID transaction
      on error undo trxBlock, leave trxBlock:
    
      /* updating participant record*/
      for first ttParticipant no-lock:

        assign
            participant.email            = ttParticipant.email
            participant.name             = ttParticipant.name
            participant.needCE           = ttParticipant.needCE
            participant.license          = ttParticipant.license
            participant.company          = ttParticipant.company
            participant.hasPaid          = ttParticipant.hasPaid
            participant.paymentID        = ttParticipant.paymentID
            participant.notes            = ttParticipant.notes
            participant.lastModifiedDate = now
            participant.lastModifiedBy   = pRequest:uid
            cName                        = ttParticipant.name
            .  
      end. /* for first ttParticipant */
      
      empty temp-table ttParticipant.
      buffer-copy participant to ttParticipant.

      validate participant.
      release participant.

      lSuccess = true.
    
    end. /* for first participant*/
    
 
    if not lSuccess
     then 
      do:
        presponse:fault("3000", "Participant modify").
        return.
      end. /* if not lSuccess */

    /* create sysindex for person name */
    if cName ne ''  
     then
      /* Build keyword index to use for searching */
      run sys/createsysindex.p (input "participantName",      /* ObjType */
                                input "",                /* ObjAttribute*/
                                input string(ipiParticipantID), /* ObjID */
                                input cName,       /* ObjKeyList*/
                                output lsuccess,
                                output opcMsg).
    
    if can-find(first ttParticipant)
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).      

  end method.
      
end class.


