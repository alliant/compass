/*------------------------------------------------------------------------
@file SubmitFeedback.cls
@action FeedbackSubmit
@description Creates a new trainingfeedback record.
@throws 3005;Data Parsing Failed 
@throws 3067;TrainingID does not exist
@throws 3067;ParticipantID does not exist
@throws 3005;Score must be 0 - 10
@throws 3005;Feedback is already submitted for supplied training ID and Participant ID.
@throws 3000;Training Feedback submit failed.
@returns success;3005;Training feedback submitted successfully.
@author SB
@version 1.0
@created 01/08/2025
Modification:

----------------------------------------------------------------------*/

class participant.SubmitFeedback inherits framework.ActionBase:
    
  {tt/trainingfeedback.i &tableAlias="ttTrainingFeedback"}

  constructor SubmitFeedback ():
    super().
  end constructor.

  destructor public SubmitFeedback ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable dsHandle      as handle    no-undo.
    define variable iFeedbackID   as integer   no-undo.
    define variable lSuccess      as logical   no-undo.

    {lib/std-def.i}
    {lib/nextkey-def.i}
      
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttTrainingFeedback:handle).

    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > "" 
     then
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.

    for first ttTrainingFeedback:

       /* validate trainingID */
      if not can-find(first training where training.trainingID = ttTrainingFeedback.trainingID) 
      then 
       do:
         pResponse:fault('3067', 'Training' {&msg-add} 'ID' {&msg-add} string(ttTrainingFeedback.trainingID)).
         return.
       end.

       /* validate participantID */
      if not can-find(first participant where participant.participantID = ttTrainingFeedback.participantID) 
      then 
       do:
         pResponse:fault("3067", "Participant" {&msg-add} "ID" {&msg-add} string(ttTrainingFeedback.participantID)).
         return.
       end.

       /* validate score */
      if (ttTrainingFeedback.score < 1 or ttTrainingFeedback.score > 10)
       then
        do:
	      pResponse:fault("3005","Score must be 1 - 10").
	      return.
	    end.

      /*If feedback is already submitted by the participant for training*/
      if can-find (first trainingfeedback where trainingfeedback.trainingID    = ttTrainingFeedback.trainingID and 
                                                trainingfeedback.participantID = ttTrainingFeedback.participantID) 
       then
        do:
          pResponse:fault("3005","Feedback is already submitted for supplied training ID and Participant ID." ).
	      return.
        end.

    end.
  
    lSuccess = false.

    TRX-BLOCK:
    for first tttrainingfeedback 
        transaction on error undo TRX-BLOCK, leave TRX-BLOCK:  
    
        /* get new feedbackID sequence */
        {lib/nextkey.i &type='feedbackID' &var=ifeedbackID &err="undo TRX-BLOCK, leave TRX-BLOCK."}.
    
        /* create trainingfeedback */
        create trainingfeedback.
        assign
            trainingfeedback.feedbackID     = ifeedbackID
            trainingfeedback.trainingID     = tttrainingfeedback.trainingID
            trainingfeedback.participantID  = tttrainingfeedback.participantID
            trainingfeedback.feedback       = tttrainingfeedback.feedback
            trainingfeedback.score          = tttrainingfeedback.score
            trainingfeedback.createdDate    = now
            trainingfeedback.createdBy      = pRequest:uid
            .
    
        validate trainingfeedback.
        release  trainingfeedback.
    
        lSuccess = true.
    end.
    
    if pResponse:isFault()
     then
      return.        
    
    if not lSuccess
     then
      do:
        pResponse:fault("3000", "Training Feedback submit").
        return.
      end.
    
    empty temp-table ttTrainingFeedback.
    for first trainingfeedback no-lock where trainingfeedback.feedbackID = ifeedbackID:
      create ttTrainingFeedback.
      buffer-copy trainingfeedback to ttTrainingFeedback.
    end.

    pResponse:success("3005", "Training feedback submitted successfully.").

    if can-find (first ttTrainingFeedback)
     then 
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).     

  end method.

end class.


