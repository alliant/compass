/*------------------------------------------------------------------------
@file SearchParticipant.Cls
@action participantSearch
@description search records in participant table according to the filter

@returns 

@success 2005;

@author S Chandu
@version 1.0
@created 01/06/2025
Modification:

----------------------------------------------------------------------*/

class participant.SearchParticipant inherits framework.ActionBase:
    
  {tt/participant.i &tableAlias="ttparticipant" }

  constructor SearchParticipant ():
    super().
  end constructor.

  destructor public SearchParticipant ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable pcParticipantEmail as character  no-undo.
    define variable pcLicense          as character  no-undo.
    define variable piCourseID         as integer    no-undo.
    define variable pcAgentID          as character  no-undo.
    define variable piTrainingID       as integer    no-undo.
    define variable pcName             as character  no-undo.                                 
    define variable dsHandle           as handle     no-undo.
    define variable std-ch             as character  no-undo.
    define variable std-in             as integer    no-undo.

    &scoped-define msg-add + "^" +
    {lib/callsp-defs.i}
    
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttparticipant:handle).

    pRequest:getParameter("ParticipantEmail", output pcParticipantEmail).
    pRequest:getParameter("Name",             output pcName).
    pRequest:getParameter("AgentID",          output pcAgentID).
    pRequest:getParameter("License",          output pcLicense).
    pRequest:getParameter("TrainingID",       output piTrainingID).
    pRequest:getParameter("CourseID",         output piCourseID).

    if piCourseID = ? 
     then
      piCourseID = 0.

    if piTrainingID = ? 
     then
      piTrainingID = 0.

    if pcParticipantEmail = ? 
     then
      pcParticipantEmail = ''.

    if pcAgentID = ? 
     then
      pcAgentID = ''.

    if pcName = ?
     then
      pcName = ''.

    if pcLicense = ?
     then
      pcLicense = ''.

    {lib\callsp.i &name=spSearchParticipant &load-into=ttparticipant &params="input pcParticipantEmail,input pcName, input pcAgentID, input pcLicense, input piTrainingID, input piCourseID" &noResponse=true}
    
    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end.

    for each ttparticipant fields (trainingID) no-lock:
      std-in = std-in + 1.
    end.

    if can-find(first ttparticipant)
     then SetContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2005", string(std-in) {&msg-add} "Participant").

  end method.
      
end class.


