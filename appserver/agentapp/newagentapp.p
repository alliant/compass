&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name newagentapp.p
@action agentAppNew
@description Creates a new agent application record in the agentapplication table

@throws 3001: The agent is invalid
@throws 3067: The agent is not found in the agent table
@throws 3001: User name is invalid
@throws 3001: Agent password is invalid 
@throws 3005: Failed to create agent ID (xxxxxx) with status

@success 2002;The record was created

@author Yoke Sam C.
@version 1.0
@created 05/22/2017
@Modified Yoke Sam C. 08/09/2017 - Read sysprop table for status desc.

Name          Date       Note
------------- ---------- -----------------------------------------------
John Oliver   05/21/2019 Modified to use POST rather than GET
----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

/* local variable */
{lib/std-def.i}

/* temp tables */
{tt/agentapp.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

empty temp-table agentapp.
pRequest:getParameter("AgentApp", input-output table agentapp).

VALIDATION-BLOCK:
for each agentapp exclusive-lock:
  /* validate the type */
  {lib/validate-sysprop.i &appCode="'AMD'" &objAction="'Application'" &objProperty="'Type'" &objID=agentapp.type}
  if not std-lo
   then pResponse:fault("3027", "Type" {&msg-add} std-ch).

  /* validate the subtype */
  {lib/get-sysprop.i &appCode="'AMD'" &objAction="'Application'" &objProperty="'Type'" &objID=agentapp.type &out=agentapp.typeDesc}
  {lib/validate-syscode.i &codeType="agentapp.typeDesc + 'Type'" &code=agentapp.subtype}
  if not std-lo
   then pResponse:fault("3027", agentapp.typeDesc + " Subtype" {&msg-add} std-ch).

  /* validate the agent */
  if agentapp.agentID = ? or agentapp.agentID = ""
   then pResponse:fault("3001", "Agent").
  case agentapp.type:
   when "G" then
    if not can-find(first agent where agent.agentID = agentapp.agentID)
     then pResponse:fault("3066", "Agent " + agentapp.agentID).
   when "A" then
    if not can-find(first attorney where attorney.attorney = agentapp.agentID)
     then pResponse:fault("3066", "Attorney " + agentapp.agentID).
  end case.
   
  if pResponse:isFault()
   then leave VALIDATION-BLOCK.

  /* validate that the agent doesn't already have a valid application */
  for each agentapplication no-lock
     where agentapplication.agentID = agentapp.agentID:
    
    if pResponse:isFault()
     then next.
    
    {lib/get-sysprop.i &appCode="'AMD'" &objAction="'Application'" &objProperty="'Status'" &objID=agentapplication.stat}
    case agentapplication.stat:
     when "N" or
     when "R" or
     when "A" then pResponse:fault("3005", agentapp.typeDesc + " " + agentapp.agentID + " already has an Open Application.").
    end case.
  end.
   
  if pResponse:isFault()
   then leave VALIDATION-BLOCK.
end.

if pResponse:isFault()
 then return.

/* all parameters are valid so start to insert the new row */
std-lo = false.
TRX-BLOCK:
for each agentapp no-lock
      on error undo TRX-BLOCK, leave TRX-BLOCK:

  std-in = 1.
  for first agentapplication no-lock
      where agentapplication.agentID = agentapp.agentID
         by agentapplication.applicationID desc:
    
    std-in = std-in + agentapplication.applicationID.
  end.

  std-ch = "".
  case agentapp.type:
   when "G" then
    for first agent no-lock
        where agent.agentID = agentapp.agentID:
      
      std-ch = agent.stateID.
    end.
   when "A" then
    for first attorney no-lock
        where attorney.attorneyID = agentapp.agentID:
      
      std-ch = attorney.stateID.
    end.
  end case.
  
  create agentapplication.
  assign
    agentapplication.applicationID = std-in
    agentapplication.agentID = agentapp.agentID
    agentapplication.dateCreated = now
    agentapplication.username = agentapp.username
    agentapplication.password = agentapp.password
    agentapplication.stateID = std-ch
    agentapplication.reasonCode = ""
    agentapplication.type = agentapp.type
    agentapplication.subtype = agentapp.subtype
    agentapplication.stat = "N"
    .
  
  validate agentapplication.
  release agentapplication.
  
  std-lo = true.
end.

if not std-lo
 then pResponse:fault("3000", "Create Agent application").
 else pResponse:success("2002", "Agent Application").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


