&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*-----------------------------------------------------------------------
  @file cancelagentapp.p
  @description Set the agent application status to "Cancelled"
  @param agentID;varchar;The agentID
  
  @returns success;int;2006
  
  @throws 3000; Update failed
  @throws 3066; Agent ID (xxxxxx) does not exist
  @throws 2018; No changes applied to agent ID (XXXXXX)
  @throws 3004; agent id is locked
  @throws 3005; failed to set the "cancelled" status
  
  @author Yoke Sam C.
  @created 05.26.2017
  @Version 1.0
  @Modified Yoke Sam C. 08/09/2017 - Read sysprop table for status desc.
----------------------------------------------------------------------*/

def input param pRequest as service.IRequest.
def input param pResponse as service.IResponse.

{lib/std-def.i}
/* local variables flags */
def var tFound as logical no-undo.
def var tUpdated as logical no-undo.
def var tFieldChanged as logical no-undo.

/* variables for getparameter */
def var pAgentID as char no-undo.
def var pReasoncode as char no-undo.

def var tstatDesc as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.48
         WIDTH              = 67.2.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


pRequest:getParameter("AgentID",  output pAgentID).
if not can-find(first agentapplication where agentapplication.agentID = pAgentID)
     then pResponse:fault("3066", "Agent ID (" + pAgentID + ")").

for first agentapplication where agentapplication.agentID = pAgentID no-lock
                           by agentapplication.applicationID desc:
    if agentapplication.stat = ?  then agentapplication.stat = "".
    if lookup(agentapplication.stat, "C,D") > 0 then
    do:
        find first sysprop no-lock where sysprop.appCode = "AMD"
                                and sysprop.objAction = "Application"
                                and sysprop.objProperty = "Status"
                                and caps(objID) = caps(agentapplication.stat) no-error.

        if avail sysprop  
            then tstatDesc = sysprop.objValue.

/*         case agentapplication.stat:      */
/*            when "D" then                 */
/*                 tstatDesc = "Denied".    */
/*            when "C" then                 */
/*                 tstatDesc = "Completed". */
/*         end case.                        */
        pResponse:fault("3005", "Failed to set the status (cancelled) with the current status of ~"" + tstatDesc + "~"").
    
    end.

end.


/* if can-find(first agentapplication where agentapplication.agentID = pAgentID                                              */
/*                                      and lookup(agentapplication.stat, "C,D") > 0)                                        */
/*   then pResponse:fault("3005", "Failed to set the status (started) with the current status of " + agentapplication.stat). */

if pResponse:isFault() 
    then return.

pRequest:getParameter("ReasonCode", output pReasonCode).

assign
    tFound = false
    tFieldChanged = false
    tUpdated = false
    .

TRX-BLOCK:
do  transaction
on error undo TRX-BLOCK, leave TRX-BLOCK:

    for first agentapplication  exclusive-lock
        where agentapplication.agentID = pAgentID by agentapplication.applicationID desc:

        tFound = true.
        
        if agentapplication.stat = "X" 
            then
            do:
                tUpdated = true.
                leave TRX-BLOCK.
            end.

        agentapplication.stat = "X".
        agentapplication.datestopped = now.
        agentapplication.reasoncode = pReasonCode.

        validate agentapplication.

        tFieldChanged = true.
        tUpdated = true.

    end.



end. /* TRX-BLOCK */


if not tFound
     then pResponse:fault("3004", "Agent ID (" + pAgentID + ")").
     else
if not tUpdated
     then pResponse:fault("3000", "Update").

if pResponse:isFault()
     then return.

if not tFieldChanged
     then
      do:
         pResponse:success("2018", "Agent ID (" + pAgentID + ")").
         return.
      end.

pResponse:Success("2006", "Status (cancelled) for " + pAgentID).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


