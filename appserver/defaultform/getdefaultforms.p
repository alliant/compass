/*------------------------------------------------------------------------
@name DefaultFormsGet
@description Gets default endorsements for the specified ID and type
@param AgentID;char;AgentID (optional)
@param StateID;char;StateID (optional)
@param InsuredType;char;O)wner or L)ender
@returns DefaultForms;complex;Default endorsement dataset
@returns Success;int;2005
@throws 3051;Invalid ID
@throws 3027;Invalid Type
@author Russ Kenny
@version 1.0 01/04/2014
@notes
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}

def var pcStateID as char no-undo.
def var pcAgentID as char no-undo.
def var pcInsuredType as char no-undo.

def var cRemitType as char no-undo.
def var dRemitValue as dec no-undo.
def var tStateID as char no-undo.

{tt/defaultform.i &tableAlias="ttDefaultForm"}


pRequest:getParameter("StateID", output pcStateID).
pRequest:getParameter("AgentID", output pcAgentID).
/* if pcStateID = "" AND pcAgentID = ""                */
/*  then pResponse:fault("3051", "ID" {&msg-add} "").  */

pRequest:getParameter("InsuredType", output pcInsuredType).
/* if index("OL", pcInsuredType) = 0                                                */
/*  then pResponse:fault("3027", "Insured Type" {&msg-add} "(O)wner or (L)ender").  */

if pResponse:isFault()
 then return.


if pcAgentID <> "" then
for first agent FIELDS (agentID remitType remitValue stateID) no-lock
  where agent.agentID = pcAgentID:
 assign
   cRemitType = agent.remitType
   dRemitValue = agent.remitValue
   .
 IF pcStateID = "" OR pcStateID <> agent.stateID
  THEN pcStateID = agent.stateID.
end.

if cRemitType = ""
 then assign
        cRemitType = "P"
        dRemitValue = .15
        .

std-in = 0.

/* All default forms if no params passed */
if pcAgentID = "" and pcStateID = "" then
for each defaultForm no-lock:

  create ttDefaultForm.
  assign
    ttDefaultForm.ID = defaultForm.ID
    ttDefaultForm.insuredType = defaultForm.insuredType
    ttDefaultForm.formID = defaultForm.formID
    ttDefaultForm.seq = defaultForm.seq
    ttDefaultForm.include = TRUE
    .

  tStateID = "".
  for first agent FIELDS (agentID stateID) no-lock
      where agent.agentID = defaultForm.ID:
      tStateID = agent.stateID.
  end.
  if tStateID = "" then
  tStateID = defaultForm.ID.
  
  for first stateForm no-lock
    where stateForm.stateID = tStateID
      and stateForm.formID = defaultForm.formID:
        
        ttDefaultForm.description = stateForm.description + " (" + stateform.insuredType + ")".
        
        if cRemitType = "P"
        then assign
           ttDefaultForm.grossPremium = stateForm.rateMin
           ttDefaultForm.netPremium = round(stateForm.rateMin * dRemitValue, 2)
           .
  end. 

  for first statcode no-lock
    where statcode.stateID = tStateID
      and statcode.formID = defaultForm.formID:
        ttDefaultForm.statCode = statcode.statCode.
  end.

  std-in = std-in + 1.
end. 

/* Default forms for an agent */
if std-in = 0 and pcAgentID <> "" then
for each defaultForm no-lock
  where defaultForm.ID = pcAgentID
    and (if pcInsuredType <> "" 
        then defaultform.insuredType begins pcInsuredType
        else true):

  create ttDefaultForm.
  assign
    ttDefaultForm.ID = defaultForm.ID
    ttDefaultForm.insuredType = defaultForm.insuredType
    ttDefaultForm.formID = defaultForm.formID
    ttDefaultForm.seq = defaultForm.seq
    ttDefaultForm.include = TRUE
    .

  tStateID = "".
  for first agent FIELDS (agentID stateID) no-lock
      where agent.agentID = defaultForm.ID:
      tStateID = agent.stateID.
  end.
  if tStateID = "" then
  tStateID = defaultForm.ID.
  
  for first stateForm no-lock
    where stateForm.stateID = tStateID
      and stateForm.formID = defaultForm.formID:
        
        ttDefaultForm.description = stateForm.description + " (" + stateform.insuredType + ")".
        
        if cRemitType = "P"
        then assign
           ttDefaultForm.grossPremium = stateForm.rateMin
           ttDefaultForm.netPremium = round(stateForm.rateMin * dRemitValue, 2)
           .
  end. 

  for first statcode no-lock
    where statcode.stateID = tStateID
      and statcode.formID = defaultForm.formID:
        ttDefaultForm.statCode = statcode.statCode.
  end.

  std-in = std-in + 1.
  if std-in = 10
   then leave.
end. 

/* Default forms for a state */
if std-in = 0 and pcStateID <> "" then
for each defaultForm no-lock
  where defaultForm.ID = pcStateID
    and (if pcInsuredType <> "" 
        then defaultform.insuredType begins pcInsuredType
        else true):

  create ttDefaultForm.
  assign
    ttDefaultForm.ID = defaultForm.ID
    ttDefaultForm.insuredType = defaultForm.insuredType
    ttDefaultForm.formID = defaultForm.formID
    ttDefaultForm.seq = defaultForm.seq
    ttDefaultForm.include = TRUE
    .

  tStateID = "".
  for first agent FIELDS (agentID stateID) no-lock
      where agent.agentID = defaultForm.ID:
      tStateID = agent.stateID.
  end.
  if tStateID = "" then
  tStateID = defaultForm.ID.
  
  for first stateForm no-lock
    where stateForm.stateID = tStateID
      and stateForm.formID = defaultForm.formID:
        
        ttDefaultForm.description = stateForm.description + " (" + stateform.insuredType + ")".
        
        if cRemitType = "P"
        then assign
           ttDefaultForm.grossPremium = stateForm.rateMin
           ttDefaultForm.netPremium = round(stateForm.rateMin * dRemitValue, 2)
           .
  end. 

  for first statcode no-lock
    where statcode.stateID = tStateID
      and statcode.formID = defaultForm.formID:
        ttDefaultForm.statCode = statcode.statCode.
  end.

  std-in = std-in + 1.
  if std-in = 10
   then leave.
end. 


if std-in > 0
 then pResponse:setParameter("defaultEndorsement", table ttDefaultForm).
pResponse:success("2005", string(std-in) {&msg-add} "Form").
