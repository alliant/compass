/*-----------------------------------------------------------------------
@name deletesysfavorite.p
@action systemFavoriteDelete
@description Deletes favorite entity record from the database
@param sysFavoriteid;character;The sysFavoriteid of the user(required)
       
@throws 3000;The delete failed
@throws 3066;The ID not found
@throws 3004;System Note  is locked

@success 2008;The favorite was deleted

@author :
@version 1.0
@created 04/26/2019
@modification
Date          Name          Description
06-06-2019    Rahul Sharma  Modified to add entity & entityID check
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable ipSysFavoriteID  as integer     no-undo.
define variable pcEntity         as character   no-undo.
define variable pcEntityID       as character   no-undo.
define variable tFound           as logical     no-undo.

{lib/std-def.i}

pRequest:getParameter("SysFavoriteID", output ipSysFavoriteID).
pRequest:getParameter("Entity",        output pcEntity).
pRequest:getParameter("EntityID",      output pcEntityID).

if ipSysFavoriteID <> 0 and not can-find(first sysfavorite where sysfavorite.sysfavoriteID = ipSysFavoriteID) 
 then 
  pResponse:fault("3066", "SystemFavorite"). 
    
if pcEntity = "A" and not can-find(first agent where agent.agentID = pcEntityID) 
 then
  pResponse:fault("3066", "Agent").

if pcEntity = "P" and not can-find(first person where person.personID = pcEntityID) 
 then
  pResponse:fault("3066", "Person").

if pcEntity = "E" and not can-find(first sysuser where sysuser.uid = pcEntityID) 
 then
  pResponse:fault("3066" , "SystemUser").
  
if pResponse:isFault() 
 then 
  return.

assign
   tFound = false
   std-lo = false
   .

TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:
  
  if ipSysFavoriteID <> 0 
   then
    for first sysfavorite exclusive-lock
      where   sysfavorite.sysfavoriteID = ipSysFavoriteID: 
      tFound = true. 
      delete sysfavorite.
      release sysfavorite.
      std-lo = true.
    end.
   else
    for first sysfavorite exclusive-lock
      where sysfavorite.UID      = pRequest:Uid
        and sysFavorite.entity   = pcEntity 
        and sysFavorite.entityID = pcEntityID: 
      tFound = true. 
      delete sysfavorite.
      release sysfavorite.
      std-lo = true.
    end.
end.

if not tFound 
 then
  pResponse:fault("3004", "SystemFavorite").
 else if not std-lo
  then
   pResponse:fault("3000", "Delete").
  
if pResponse:isFault() 
 then
  return.
  
pResponse:success("2008", "System Favorite").
pResponse:deleteEvent("SystemFavorite", string(ipSysFavoriteID)). 
  
