/*------------------------------------------------------------------------
@name crm/deleteusernote.p
@action userNoteDelete
@description Deletes a system note using the specified parameters
@param sysNoteID;int;sysnoteID of the SystemNote to delete
@returns 2008;System Note has been removed
@throws 3005;System Note cannot be blank
@throws 3066;System Note does not exist
@throws 3004;System Note  is locked
@throws 3000;Delete failed
@author Gurvindar
@version 1.0 
@notes
@created 05/02/2019
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable iSysNoteID  as integer no-undo.
define variable tFound      as logical no-undo.

{lib/std-def.i}

pRequest:getParameter("sysNoteID", iSysNoteID).
  
if not can-find(first sysNote where sysNote.sysNoteID = iSysNoteID) 
 then
  do:
    pResponse:fault("3066", "sysNote"). 
    return.
  end.
  
assign
   tFound = false
   std-lo = false
   .

SysNote-BLOCK:
do transaction
  on error undo SysNote-BLOCK, leave SysNote-BLOCK:

  for first sysNote exclusive-lock
    where sysNote.sysNoteID = iSysNoteID: 
    tFound = true.
    delete sysNote.
    release sysNote.
    std-lo = true.
  end.
end.
 
if not tFound 
 then
  pResponse:fault("3004", "Note").
 else if not std-lo
  then
   pResponse:fault("3000", "Delete").
  
if pResponse:isFault() 
 then
  return.

 pResponse:success("2008", "System Note").
 pResponse:deleteEvent("SystemNote", string(iSysNoteID)).
