/*------------------------------------------------------------------------
@name crm/getagentsnpractualtoplan.p
@action agentsNprActualToPlanGet
@description Returns agents with highest/Lowest net prenium

@returns 
@success 2005;char;The count of the records returned

@author Rahul
@version 1.0
@created 03.11.19
@modification
Date          Name          Description
09/18/2020    VJ            Moved agent filter logic into sql
01/25/2022    Shefali       Task:89648 Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"
----------------------------------------------------------------------*/ 

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

/* Stored procedure variables */

define variable piPageNum       as integer   no-undo.
define variable piNoOfRows      as integer   no-undo.
define variable piCount         as integer   no-undo.
define variable pcNprSortOrder  as character no-undo.
define variable pcAgentList     as character no-undo  initial "ALL".
define variable PcStateList     as character no-undo  initial "ALL".
define variable pUID            as character no-undo.
define variable iCount          as integer   no-undo.

{lib/std-def.i}
{lib/callsp-defs.i }
{tt/profile.i &agentstop=true &favorite=true &noOfPeople=true}

pRequest:getParameter("PageNum",      output piPageNum).
pRequest:getParameter("NoOfRows",     output piNoOfRows).
pRequest:getParameter("NprSortOrder", output pcNprSortOrder).

pUID  = pRequest:uid.

{lib/callsp.i  &name=spGetAgentsNprActualToPlan &load-into=profile &params="input piPageNum, input piNoOfRows, input pcNprSortOrder,  input pcAgentList, input PcStateList, input pUID"}

pResponse:success("2005", string(piNoOfRows) {&msg-add} (if pcNprSortOrder = "T" then "Top Agents" else "Under Plan Agents")).
