/* -----------------------------------------------------------------------------
@name crm/getnotification.p
@action notificationsGet
@description fetches the Notifications
@param pcStat;char;Stat identifier
@param piPageNum;int;pagenumber 
@param piNoOfRows;int;no. of rows
@returns Success;int;2005
@author Gurvindar
@created 05.24.2019
@Modification
Date           Name              Description
08/09/2019     Gurvindar         Returning name for specific entity
----------------------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable pcStat          as character  no-undo.
define variable pcEntityType    as character  no-undo.
define variable piPageNum       as integer    no-undo.
define variable piNoOfRows      as integer    no-undo.

{lib/std-def.i}
{tt/sysnotification.i &tableAlias="ttsysnotification"}

pRequest:getParameter("Stat",     output pcStat).
pRequest:getParameter("Entity",   output pcEntityType).
pRequest:getParameter("PageNum",  output piPageNum).
pRequest:getParameter("NoOfRows", output piNoOfRows).

if pcStat = ? or pcStat = ""
 then 
  do:
    pResponse:fault("3005","Status cannot be blank." ).
    return. 
  end.

std-in = 0. 

for each sysnotification no-lock
  where sysnotification.UID = pRequest:UID and  
        sysnotification.entityType = (if pcEntityType = "ALL" then sysnotification.entityType else pcEntityType):
        
    
  if lookup(sysnotification.stat,pcStat) > 0
   then
    do:
      std-in = std-in + 1.
      if std-in gt ((piPageNum - 1) * piNoOfRows) and
         std-in le ((piPageNum - 1) * piNoOfRows) + piNoOfRows 
       then
        do:
          create ttsysnotification.
          buffer-copy sysnotification to ttsysnotification.
          
          if sysnotification.entityType = "A" 
           then
            do:
              for first agent where agent.agentID = sysnotification.entityID no-lock:
                ttsysnotification.entityName = agent.name.
              end.
            end.
          else if sysnotification.entityType = "P"  
           then
            do:
              for first person where person.personID = sysnotification.entityID no-lock:
                ttsysnotification.entityName = person.dispname.
              end.
            end.
          else
           do:
             for first sysuser where sysuser.uid = sysnotification.entityID no-lock:
               ttsysnotification.entityName = sysuser.name.
             end.
          end.  
          
       end.       
    end.
end.

if std-in > 0
 then
 do:
   create ttsysnotification.
   assign
       ttsysnotification.entityType = "Total Notifications"
       ttsysnotification.rowNum     = string(std-in)
       .
   pResponse:setParameter("SysNotification", table ttsysnotification).

 end.  
pResponse:success("2005", string(std-in) {&msg-add} "System Notification").

