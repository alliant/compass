/*-----------------------------------------------------------------------
@name deleteuserpicture.p
@action userPictureDelete
@description Deletes Picture from sysUser
@param userID;character;The UID of the user(required)
       
@throws 3000;The delete failed
@throws 3066;The ID not found

@success 2008;Users Picture was deleted

@author : Rahul
@version 1.0
@created 05/07/2019
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable cUID   as character no-undo.
define variable lFound as logical   no-undo.

{lib/std-def.i}

pRequest:getParameter("UID", output cUID).
  
if not can-find(sysUser where sysUser.UID = cUID) 
 then 
  do:
    pResponse:fault("3066", "SystemUser"). 
    return.  
  end.  

assign
    std-lo = false
    lFound = false
    .

TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:

  for first sysUser exclusive-lock
    where sysUser.UID = cUID: 
    
    assign
        lFound          = true 
        sysUser.picture =  ""
        std-lo          = true
        .
  end. 
end.

if not lFound 
 then
  do:
    pResponse:fault("3004", "User").
    return.
  end.

else if not std-lo 
 then
  do:
    pResponse:fault("3000", "Delete").
    return.
  end.
    
pResponse:success("2008", "User Picture").
pResponse:updateEvent("SystemUser", cUID). 
  
