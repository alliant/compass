/*------------------------------------------------------------------------
@name crm/untagentity.p
@action EntityUntag
@description Removes an entity from tag
@param tagID;int;tagID of the tag record to delete
@throws 3066;tag does not exist
@throws 3004;tag is locked
@throws 3000;untag failed
@author Sachin Chaturvedi
@version 1.0 
@notes
@created 11/28/2019
@modified 
Date         Name            Comments
06/26/2023   SB              Task# 104008 Modify to create taglogs.
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable lSuccess      as logical   no-undo.
define variable cPersonIDList as character no-undo.

{tt/tag.i &tableAlias=tempTag}
{tt/tag.i &tableAlias=removedTag}
{lib/std-def.i}

/* Getting temptable */
empty temp-table tempTag.

pRequest:getParameter("tag", input-output table tempTag).


for each temptag:
  if not can-find(first tag where tag.name     = trim(tempTag.name,"#") and
                                  tag.entity   = temptag.entity and
                                  tag.entityID = temptag.entityID) 
   then
    do:
      create removedTag.
      buffer-copy tempTag to removedTag.
      assign removedTag.name = trim(tempTag.name,"#").

      delete tempTag.
      lSuccess = true.
    end.

  if can-find(first tag where tag.entity   = tempTag.entity and
                              tag.entityID = tempTag.entityID and
                              tag.name     = trim(tempTag.name,"#") and
                              tag.uid      <> pRequest:uid)
   then
    do:
      if cPersonIDList = "" then
        assign cPersonIDList = temptag.entityID.
       else
        assign cPersonIDList = cPersonIDList + "," + temptag.entityID.

      delete tempTag.
      lSuccess = true.
    end.
end.

tag-BLOCK:
do transaction
  on error undo tag-BLOCK, leave tag-BLOCK:

  for each temptag:
    for first tag exclusive-lock
      where tag.name     = trim(tempTag.name,"#") and
            tag.entity   = temptag.entity and
            tag.entityID = temptag.entityID: 

      delete tag.
      release tag.
      lSuccess = true.

    end.
  end.
end.

for each temptag:
  run crm/createtaglog-p.p(input temptag.entity,
                           input temptag.entityID,
                           input pRequest:actionID,
                           input trim(tempTag.name,"#"),
                           input pRequest:UID).

  create removedtag.
  buffer-copy tempTag to removedTag.
  assign removedTag.name = trim(tempTag.name,"#").
end.
 
if not lSuccess
 then
  pResponse:fault("3000", "Untag").
  
if pResponse:isFault() 
 then
  return.

if cPersonIDList <> "" then
 do:
   pResponse:setParameter("tag", table removedtag).
   pResponse:success("3005", "You cannot un-tag people tagged by other users. Cannot untag people with IDs: " + cPersonIDList).
 end.
else
 do:
   pResponse:setParameter("tag", table removedtag).
   pResponse:success("2000", "Untag").
 end.

