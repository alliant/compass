/*------------------------------------------------------------------------
@name getevents.p
@action eventsGet
@description Get events for an entity
@author Sachin Chaturvedi
@version 1.0
@created 02/20/2020
@notes
Modification:
Date          Name                Description
05/07/2020  Sachin Chaturvedi    Added fields userIDs,month,day and year
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable pcEntity         as character no-undo.
define variable pcEntityID       as character no-undo.

{tt/event.i &tableAlias="tempEvent"}
{lib/std-def.i}

/* Getting parameters */
pRequest:getParameter("Entity",            output pcEntity).
pRequest:getParameter("EntityID",          output pcEntityID).
    
std-in = 0.

if pcEntity = "" or
   pcEntity = ?  then
   pcEntity = "ALL".
   
if pcEntityID = "" or
   pcEntityID = ?  then
   pcEntityID = "ALL". 
  
for each event where event.entity   = (if pcEntity   = "ALL" then event.entity   else pcEntity)   and
                     event.entityID = (if pcEntityID = "ALL" then event.entityID else pcEntityID) and
                     can-do(event.userIDs,pRequest:UID)
                     :
  std-in = std-in + 1.

  create tempevent.
  buffer-copy event to tempevent.
  
  find first person no-lock where person.personID = event.personID no-error.
  if available person 
   then
    tempevent.personName = if (person.dispName ne "" and person.dispName ne "?") 
                            then 
                             person.dispName 
                            else 
                             trim((if (person.firstname  ne "" and person.firstname  ne ?) then (person.firstname)        else "") + 
                                  (if (person.middlename ne "" and person.middlename ne ?) then (" " + person.middlename) else "") + 
                                  (if (person.lastname   ne "" and person.lastname   ne ?) then (" " + person.lastname)   else "")
                                 )
                                 . 
end.
    
if std-in > 0 
 then
  pResponse:setParameter("event", table tempevent).
  
pResponse:success("2005", string(std-in) {&msg-add} "event").




