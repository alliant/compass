/*---------------------------------------------------------------------------
@name crm/getsysNotes.p
@action systemNotesGet
@description Returns a table of the sysNote based on the criteria

@param pcUID;char;UserID
@param pcEntity;char;
@param pcEntityID;char;

@returns The sysNote dataset
@success 2005;char;The count of the records returned

@author Rahul Sharma
@version 1.0
@created 04/24/2019
@modification
Date          Name          Description 
05/20/2019    Gurvindar     Added agentfilter check
05/21/2019    Rahul Sharma  Modified code to add paging
06/07/2019    Rahul Sharma  Modified to get notes for all user/logged-in user
10/08/2020    VJ            Moved agent filter logic into sql
01/25/2022    Shefali       Task:89648 Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"
-----------------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable pcUID       as character no-undo.
define variable pcEntity    as character no-undo.
define variable pcEntityID  as character no-undo.
define variable pcAgentList as character no-undo.
define variable piPageNum   as integer   no-undo.
define variable piNoOfRows  as integer   no-undo.

define variable pUID        as character no-undo.

{lib/std-def.i}
{lib/callsp-defs.i }

{tt/sysNote.i}
{tt/agentfilter.i}

pRequest:getParameter("Entity",   output pcEntity).
pRequest:getParameter("EntityID", output pcEntityID).
pRequest:getParameter("UID",      output pcUID).
pRequest:getParameter("PageNum",  output piPageNum).
pRequest:getParameter("NoOfRows", output piNoOfRows).

if pcEntity = "" or pcEntity = ? 
 then 
  pResponse:fault("3001", "Entity").
  
if pcEntityID = "" or pcEntityID = ?
 then 
  pResponse:fault("3001", "EntityID").  
  
if pResponse:isFault()
 then 
  return. 

if pcEntity = "A" /* (A)gent */
 then
  do:
    pUID = pRequest:uid.
    /*  Fetch agent list related to logged-in sysuser  */
     {lib/callsp.i  &name=spAgentFilter &load-into=agentfilter &params="input pcEntityID, input pUID" &noCount=true}
      
    if not can-find(first agentfilter where agentfilter.agentID = "ALL") and 
       not can-find(first agentfilter where agentfilter.agentID = pcEntityID)
     then
      do:
        pResponse:fault("3005", "User is not authorised to view the notes information of an agent").
        return.
      end.
  end.

if pcUID = "" or
   pcUID = ?  then
   pcUID = "ALL".

{lib/callsp.i  &name=spGetSysNotes &load-into=sysNote &params="input piPageNum, input piNoOfRows, input pcEntity, input pcEntityID, input pcUID"}
  
pResponse:success("2005", string(csp-icount) {&msg-add} "sysNote").
