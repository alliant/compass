import os
import sys
import imaplib
import email
import ctypes
import logging

log_file = sys.argv[7]

logging.basicConfig(filename=log_file,
                    filemode='w',
                    level=logging.DEBUG,
                    )
try:

    user = sys.argv[1]
    password = sys.argv[2]
    emailfrom= sys.argv[3]
    imap_url = sys.argv[4]
    imap_port = int(sys.argv[5])
    print_file = sys.argv[6]

    #print(user)
    #print(password)
    #Where you want your attachments to be saved (ensure this directory exists)
    attachment_dir = 'your_attachment_dir'
    # sets up the auth
    def auth(user,password,imap_url):
        con = imaplib.IMAP4_SSL(imap_url,imap_port)
        #print(con)
        con.login(user,password)
        return con
    # extracts the body from the email

    # allows you to download attachments
    def get_attachments(msg):
        for part in msg.walk():
            if part.get_content_maintype()=='multipart':
                continue
            if part.get('Content-Disposition') is None:
                continue
            fileName = part.get_filename()

            if bool(fileName):
                filePath = os.path.join(attachment_dir, fileName)
                with open(filePath,'wb') as f:
                    f.write(part.get_payload(decode=True))
    #search for a particular email
    def search(key,value,con):
        result, data  = con.search(None,key,'"{}"'.format(value))
        return data
    #extracts emails from byte array
    def get_emails(result_bytes):
        msgs = []
        for num in result_bytes[0].split():
            typ, data = con.fetch(num, '(RFC822)')
            msgs.append(data)
        return msgs

    def get_body(msg):
        if msg.is_multipart():
            for payload in msg.get_payload():
                    # if payload.is_multipart(): ...
                    #print(msg['date'])
                    #input("a")
                    body = (
                        payload.get_payload()
                        .split(msg['from'])[0]
                        .split('\r\n\r\n2015')[0]
                    )
                    return body
            else:
                body = (
                    msg.get_payload()
                    .split(msg['from'])[0]
                    .split('\r\n\r\n2015')[0]
                )
        return body

    def get_subject(msg):
        return msg['Subject']

    def get_emailfrom(msg):
        return msg['from']

    def get_emailto(msg):
        return msg['to']

    def get_emaildate(msg):
        return msg['date']


    con = auth(user,password,imap_url)

    con.select('INBOX')
    data=get_emails(search('from',emailfrom,con))

    for data in data:
        msg=email.message_from_bytes(data[0][1])
        emailBody=get_body(msg)
        subject=get_subject(msg)
        emailfrom=get_emailfrom(msg)
        emailto=get_emailto(msg)
        emaildate=get_emaildate(msg)

        #print(subject,emailfrom,emailto,emaildate)
        f=open(print_file,"a")
        f.write("date       : " + emaildate + "\n\n")
        f.write("email from : " + emailfrom + "\n\n")
        f.write("email to   : " + emailto   + "\n\n")
        f.write("email subject: " + subject   + "\n\n\n")
        f.write(emailBody + "\n\n\n\n")
        
    f.close()




    #result, data = con.fetch(b'10','(RFC822)')
    #raw = email.message_from_bytes(data[0][1])
    #get_attachments(raw)
    con.logout()


except Exception as e:
    logging.error(str(e))
