/*---------------------------------------------------------------------------
@name crm/getanniversaries.p
@action anniversariesGet
@description Returns people associated to entities with a marked event

@param pcUID;char;UserID
@param piPageNum;char;
@param piNoOfRows;char;

@returns The event dataset
@success 2005;char;The count of the records returned

@author Sachin Chaturvedi
@version 1.0
@created 02/24/2020
@modification
Date          Name          Description 
-----------------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable pcUID       as character no-undo.
define variable piPageNum   as integer   no-undo.
define variable piNoOfRows  as integer   no-undo.

{lib/std-def.i}

{tt/event.i &tableAlias="ttevent"}

pRequest:getParameter("UID",      output pcUID).
pRequest:getParameter("PageNum",  output piPageNum).
pRequest:getParameter("NoOfRows", output piNoOfRows).

if pcUID = "" or
   pcUID = ?  then
   pcUID = "ALL".
   
std-in = 0.

for each event no-lock where event.UID = pRequest:UID                    and
                             (absolute(date(event.date) - today) le 15)  and 
                             event.reminder
                             :           
  std-in = std-in + 1.

  if std-in gt ((piPageNum - 1) * piNoOfRows) and
     std-in le ((piPageNum - 1) * piNoOfRows) + piNoOfRows 
   then
    do:
      create ttevent.
      buffer-copy event to ttevent.
      
      find first person no-lock where person.personID = ttevent.personID no-error.
      if available person 
       then
        ttevent.personName = if (person.dispName ne "" and person.dispName ne "?") 
                                then 
                                 person.dispName 
                                else 
                                 trim((if (person.firstname  ne "" and person.firstname  ne ?) then (person.firstname)        else "") + 
                                      (if (person.middlename ne "" and person.middlename ne ?) then (" " + person.middlename) else "") + 
                                      (if (person.lastname   ne "" and person.lastname   ne ?) then (" " + person.lastname)   else "")
                                     )
                                     .
                                     
      if ttevent.entity = "A" 
       then
        do:
          find first agent no-lock where agent.agentID = ttevent.entityID no-error.
          if available agent 
           then
            assign
                ttevent.agentName = agent.name
                ttevent.agentAddr = trim(((if (agent.city  ne "" and agent.city  ne ?) then (agent.city)        else "") + 
                                          (if (agent.state ne "" and agent.state ne ?) then (", " + agent.state) else "") + 
                                          (if (agent.ZIP   ne "" and agent.ZIP   ne ?) then (", " + agent.ZIP)   else "")
                                         ),", "
                                        )
                .
        end.
    end.    
end.

if std-in > 0 
 then
  do:
    create ttevent.
    assign
        ttevent.entity = "TotalAnniversaries"    
        ttevent.entityID = string(std-in)
        .
    pResponse:setParameter("event", table ttevent).
  end.
  
pResponse:success("2005", string(std-in) {&msg-add} "Anniversary").

