/*-------------------------------------------------------------------------------
@name crm/searchstrings.p
@action stringsSearch
@description Returns profile temp-table of the Agent/Person/User based on the criteria

@param pcStateID;char;The stateID of person
@param pcsearchstring;char;The searchString of person
@param pcProfileType;char;Type of profile Agent/Person/All

@returns The entity profile data

@author Rahul
@version 1.0
@created
Date         Name                Description
01/08/2020   Sachin Chaturvedi   Modified to apply AND operation on search strings.
01/25/2025   Vignesh Rajan       Modified to remove the custom delimiter
03/28/2024   S Chandu            Task #:111689 Remove the reference of Person.email.
---------------------------------------------------------------------------------*/

/*----------Temp-Table Definition------------------ */
{tt/person.i &tableAlias="tempPerson"}
{tt/profile.i &favorite=true &personAgent=true &noOfPeople=true}

define input  parameter pRequest  as service.IRequest.
define input  parameter pcSearchString  as character no-undo.
define output parameter table for profile.

/*----------Variable Definition------------------ */
define variable iCount             as integer   no-undo.
define variable pcStateID          as character no-undo.
define variable pcProfileType      as character no-undo.

{lib/std-def.i}

/* Get Parameter values in variable */
pRequest:getParameter("stateID", output pcStateID).

if pcStateID = "" 
 then
  pcStateID = "ALL".

pRequest:getParameter("Type", output pcProfileType ).

if lookup("A",pcProfileType)   > 0 or /* Agent */
   lookup("ALL",pcProfileType) > 0    /* For 'ALL' get all agent, person and sysuser record */
 then  
  do:      
    for each sysIndex where sysIndex.objType begins "Agent"
                      and   sysIndex.objKey  begins entry(1,pcSearchString) no-lock:    
        
      for first agent where agent.agentID = sysIndex.objID 
                       and agent.stateid = (if pcStateID = "ALL" then agent.stateid else pcStateID) no-lock:
      
        if not can-find(first profile where profile.type     = "A" /* (A)gent */ 
                                        and profile.entityID = agent.agentID) 
         then
          do:
            create profile.
            assign 
                profile.type     = "A"  /* (A)gent */   
                profile.entityId = agent.agentId
                profile.name     = agent.name                      
                profile.stat     = agent.stat
                profile.email    = agent.email
                profile.city     = agent.city  
                profile.state    = agent.state 
                profile.zip      = agent.zip
                . 

            for each personAgent where personAgent.agentID = agent.agentID no-lock:
              profile.noOfPeople = profile.noOfPeople + 1.
            end.

            for first sysfavorite no-lock
              where sysfavorite.uid      = pRequest:Uid
                and sysfavorite.entity   = "A" /* (A)gent */
                and sysfavorite.entityID = agent.agentId:
              assign
                  profile.sysfavoriteID = sysfavorite.sysfavoriteID 
                  profile.favorite      = true
                  .
            end. /* for first sysfavorite...*/
          end. /* if not can-find(first profile */
      end. /* for each agent */
    end. /* for each sysIndex */
    
    /* delete those agent profiles for which all search keywords are not met(AND operation) */
    if num-entries(pcSearchString) > 1 
     then
      do:
        profile-loop:
        for each profile where profile.type = "A":
     
          string-loop:
          do icount = 2 to num-entries(pcSearchString):
            std-lo = false.
            if can-find (first sysIndex where sysIndex.objType begins "Agent"
                               and   sysIndex.objKey  begins entry(icount,pcSearchString)
                               and   sysIndex.objID = profile.entityID)
             then
              do: 
                 std-lo = true.
                 next string-loop.
              end.

            if std-lo = false
             then
              do:
                delete profile.
                next profile-loop.
              end.
          end. /* string-loop */
       
        end. /* for each profile */ 
      end.
  end. /* if pcProfileType = "A" */
  

if lookup("P",pcProfileType)   > 0  or /* Person */                                      
   lookup("ALL",pcProfileType) > 0     /* For 'ALL' get all agent, person and sysuser record */
 then
  do:
    /* Get all the person as per stateId in tempPerson */
    for each person no-lock 
      where person.state = (if pcStateID = "ALL" then Person.state else pcStateID):
      create tempPerson.
      buffer-copy person to tempPerson.
    end. 

    if pcStateID ne "ALL" 
     then
      do:
        for each agent no-lock
          where agent.stateId = pcStateID:

          for first orgrole no-lock where orgrole.sourceID = agent.agentID:
            for each affiliation no-lock
              where affiliation.partnerA = orgrole.orgID or affiliation.partnerB = orgrole.orgID :
              if not can-find(first tempPerson where tempPerson.personId eq affiliation.partnerB)
               then
                do:
                  for first person no-lock
                    where person.personId = affiliation.partnerB:
                
                    create tempPerson.
                    buffer-copy Person to tempPerson.            	    
                  end. /* for first person no-lock */
                end. /* if not can-find(first tempPerson */
            end. /* for each affiliation no-lock */
          end.  /* for first orgrole */
        end. /* for each agent no-lock */ 
      end. /* if pcStateID ne "ALL" */

    for each sysIndex where sysIndex.objType begins "Person"
                        and sysIndex.objKey  begins entry(1,pcSearchString)
                        no-lock:

      for first tempPerson where tempPerson.personId = sysIndex.objID: 
        if not can-find(first profile where profile.type     = "P" /* (P)erson */
                                        and profile.entityID = sysIndex.objID) 
         then
          do:
                create profile.
                assign
                    profile.type     = "P" /* (P)erson */   
                    profile.entityId = tempPerson.personId
                    profile.name     = tempPerson.dispName
                    profile.stat     = if tempPerson.active then "A" else "I"
                    profile.city     = tempPerson.city  
                    profile.state    = tempPerson.state 
                    profile.zip      = tempPerson.zip
                    .   
                for each personAgent where personAgent.personID = tempPerson.personID no-lock:
                  for each agent field(name) where agent.agentID = personAgent.agentID no-lock:
                    if profile.agentNameList = "" 
                     then
                      profile.agentNameList = agent.name.
                     else
                      profile.agentNameList = profile.agentNameList + "," + agent.name.
                  end.
                end.
                  
                for first sysfavorite no-lock
                  where sysfavorite.uid      = pRequest:Uid
                      and sysfavorite.entity   = "P" /* (P)erson */
                      and sysfavorite.entityID = tempPerson.personId:
                    assign
                        profile.sysfavoriteID = sysfavorite.sysfavoriteID 
                        profile.favorite      = true
                        .
                  end. /* for first sysfavorite...*/

            end. /* if not can-find(first profile */
        end. /* for first tempPerson */
      end. /* for each sysIndex */   

      /* delete those person profiles for which all search keywords are not met */
      if num-entries(pcSearchString) > 1 
       then
        do:
          profile-loop: 
          for each profile where profile.type = "P":
     
            string-loop:
            do icount = 2 to num-entries(pcSearchString):
              std-lo = false.
              if can-find (first sysIndex where sysIndex.objType begins "Person"
                                   and sysIndex.objKey  begins entry(icount,pcSearchString) 
                                   and sysIndex.objID = profile.entityID)
               then
                do:
                  std-lo = true.
                  next string-loop.
                end.                                

              if std-lo = false
               then
                do:
                  delete profile.
                  next profile-loop.
                end.
            end. /* string-loop */
            
          end. /* for each profile */ 
        end.
  end. /* if pcProfileType = "P" */

if lookup("E",pcProfileType) >   0 or /* SysUser */
   lookup("ALL",pcProfileType) > 0    /* For 'ALL' get all agent, person and sysuser record */
 then  
  do:      

    for each sysIndex where sysIndex.objType begins "SysUser"
                      and   sysIndex.objKey  begins entry(1,pcSearchString)
                      no-lock:    
    
      for each sysUser where sysUser.UID  = sysIndex.objID no-lock:

        if not can-find(first profile where profile.type     = "E" /* (E)mployee or Sysuser */
                                        and profile.entityID = sysUser.UID) 
         then
          do:
                create profile.
                assign 
                    profile.type     = "E" /* (E)mployee or Sysuser */    
                    profile.entityId = sysUser.UID
                    profile.name     = sysUser.name 
                    profile.stat     = if sysUser.isActive then "A" else "I"
                    profile.email    = sysuser.email
                    profile.city     = sysUser.city  
                    profile.state    = sysUser.state
                    profile.zip      = sysUser.zip
                    .   
                
                for first sysfavorite no-lock
                  where sysfavorite.uid      = pRequest:Uid
                    and sysfavorite.entity   = "E" /* (E)mployee or Sysuser */
                    and sysfavorite.entityID = sysUser.UID:
                  assign
                      profile.sysfavoriteID = sysfavorite.sysfavoriteID 
                      profile.favorite      = true
                      .
                end. /* for first sysfavorite...*/
          end. /* if not can-find(first profile */
      end. /* for each sysUser */
    end. /* for each sysIndex */
    
     /* delete those sysuser profiles for which all search keywords are not met */
    if num-entries(pcSearchString) > 1 
     then
      do:
        profile-loop:
        for each profile where profile.type = "E":
     
          string-loop:
          do icount = 2 to num-entries(pcSearchString):
            std-lo = false.
            if can-find (first sysIndex where sysIndex.objType begins "SysUser"
                               and   sysIndex.objKey  begins entry(icount,pcSearchString) 
                               and   sysIndex.objID = profile.entityID)
             then
              do:
                std-lo = true.
                next string-loop.
              end.
               
            if std-lo = false
             then
              do:
                delete profile.
                next profile-loop.
              end.
          end. /* string-loop */
          
        end. /* for each profile */ 
      end.
    
  end. /* if pcProfileType = "E" */ 




