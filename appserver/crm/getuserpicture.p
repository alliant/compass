/*------------------------------------------------------------------------
@name crm/getuserpicture.p
@action userPictureGet
@description Get Picture of user

@throws 3005;No XML file attached
@throws 3000;Failed
@throws 2000;Success

@author Rahul
@version 1.0
@created 05/07/2019
Modification:
Date      Name               Description
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.  
define input parameter pResponse as service.IResponse. 

define variable cPicture   as character no-undo.

{lib/std-def.i}

std-lo = false.
  
for first sysUser where sysUser.UID = pRequest:UID no-lock:
  assign cPicture  = sysUser.picture
         std-lo    = true.
end.

if not std-lo 
 then
  do:
    pResponse:fault("3000", "Get User Picture").
    return.
  end.

pResponse:setParameter("Picture", cPicture).
pResponse:success("2000", "Get User Picture").

              
