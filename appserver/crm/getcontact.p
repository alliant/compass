/*------------------------------------------------------------------------
@name crm/getcontact.p
@action ContactGet
@description Get selected entity contact info  from server
@param pcEntity;char;Entity
@param pcEntityID;char;EntityID

@returns The profile dataset
@success 3005;char;Contact found

@author Rahul Sharma
@version 1.0
@created 05/07/2019
Modification:
Date          Name           Description
06/03/2109    Gurvindar      Modified logic to return email ID 
08/16/2022    SA            Task #96812 Modified to add functionality for personagent.
11/22/2022    VR             Modified to assign the values to doNotCall and internal fields in person table.
03/28/2024    S Chandu       Task #:111689 Remove the reference of Person.mobile, Person.phone and Person.email
                             and gets details from personcontact table.
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable pcEntity    as character no-undo.
define variable pcEntityID  as character no-undo.
define variable pcAgentList as character no-undo.
define variable cEmail      as character no-undo.
define variable cMobile     as character no-undo.
define variable cPhone      as character no-undo.

{lib/std-def.i}
{tt/entitycontact.i}
{tt/personcontact.i &tableAlias="ttpersonContact"}

pRequest:getParameter("Entity",   output pcEntity).
pRequest:getParameter("EntityID", output pcEntityID).

if pcEntity = ""
 then
  do:
    pResponse:fault("3005", "Entity Code cannot be blank").
    return.
  end.

if pcEntityID = ""
 then
  do:
    pResponse:fault("3005", "Entity ID cannot be blank").
    return.
  end.

std-lo = false.

if pcEntity = "A"
 then
  do:
    if not can-find(first agent where agent.agentID = pcEntityID)  
     then
      do:
        pResponse:fault("3083", "Agent").
        return.
      end.
    
    /*   Removed the security in contact information
    /* Fetch agent list related to logged-in sysuser */       
    run agent/agentfilter.p (pRequest:uid, output pcAgentList).

    if pcAgentList                    ne "ALL" and
       lookup(pcEntityID,pcAgentList) ne 0 
     then
      do:
        pResponse:fault("3005", "User is not authorised to view the contact information of an agent").
        return.
      end.*/

    for first agent except (agent.logo) no-lock 
      where agent.agentID = pcEntityID:
      
      create entitycontact.
      assign
          entitycontact.entity    = "A" /* (A)gent */
          entitycontact.entityID  = agent.agentID
          entitycontact.email     = agent.email
          entitycontact.phone1    = agent.contact
          entitycontact.phone2    = agent.phone
          entitycontact.addr1     = agent.addr1
          entitycontact.addr2     = agent.addr2
          entitycontact.addr3     = agent.addr3
          entitycontact.addr4     = agent.addr4
          entitycontact.city      = agent.city
          entitycontact.state     = agent.state
          entitycontact.zip       = agent.zip
          .
      std-lo = true.
    end.
  end.

else if pcEntity = "P"
 then
  do:
    if not can-find(first person where person.personID = pcEntityID)  
     then
      do:
        pResponse:fault("3083", "Person").
        return.
      end.

    for first person no-lock 
      where person.personID = pcEntityID:
      
      assign
          cEmail  = ''
          cPhone  = ''
          cMobile = ''
          .

      run com/getpersoncontact-p.p(input person.PersonID , output cEmail,output cMobile,output cPhone).
      create entitycontact.
      assign
          entitycontact.entity    = "P" /* (P)erson */
          entitycontact.entityID  = person.personID
          entitycontact.phone1    = cMobile
          entitycontact.phone2    = cPhone
          entitycontact.email     = cEmail
          entitycontact.addr1     = person.address1
          entitycontact.addr2     = person.address2
          entitycontact.city      = person.city
          entitycontact.state     = person.state
          entitycontact.zip       = person.zip
          entityContact.internal  = person.internal
          entitycontact.doNotCall = person.doNotCall.

      for each personcontact no-lock 
        where personcontact.personID = person.personID:
        
        create ttpersoncontact.
        assign
            ttpersoncontact.personcontactID  = personcontact.personcontactID
            ttpersoncontact.personID         = personcontact.personID
            ttpersoncontact.contactType      = personcontact.contactType
            ttpersoncontact.contactSubType   = personcontact.contactSubType
            ttpersoncontact.contactID        = personcontact.contactID
            ttpersoncontact.uid              = personcontact.uid
            ttpersoncontact.createDate       = personcontact.createDate
            ttpersoncontact.expirationdate   = personcontact.expirationdate
            .

      end.

      std-lo = true.
    end.
  end.  

if std-lo
 then
  do:
    pResponse:setParameter("entitycontact", table entitycontact).

    if pcEntity = "P"
     then
      pResponse:setParameter("personcontact", table ttpersonContact).

  end.
  
pResponse:success("3005", if pcEntity = "A" then "Agent" else "Person" + " contact found"). 
