/*------------------------------------------------------------------------
@name crm/shareusercard.p
@action userCardShare
@description Share contact info of the user,agent,person        
@param To;char;The to address 
@param entity;char;Entity i.e Agent,User,Person
@param entityID;char;agentID,uid,personID 
@throws 3001;The parameter is invalid
@throws 3066;The parameter is invalid
@success 2000;The mail has been sent

@author Gurvindar
@version 1.0
@created 24/04/2019
@modified
Date          Name        Description
06/03/2019    Gurvindar   Modified logic
06/24/2019    Gurvindar   Added logic to show image on vcard
07/29/2022    S chandu    clob related changes
03/28/2024    S Chandu    Task #:111689 Remove the reference of Person.mobile, Person.phone and Person.email
                          and gets details from personcontact table.
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

{lib/std-def.i}

define variable tTo            as character no-undo.
define variable ipEntity       as character no-undo.
define variable ipEntityID     as character no-undo.
define variable cVCard         as longchar  no-undo.
define variable chName         as character no-undo.
define variable cTempFilePath  as character no-undo.
define variable cTempFileDir   as character no-undo.
define variable lcLogo         as longchar  no-undo.
define variable cContactEmail  as character no-undo.
define variable cContactPhone  as character no-undo.
define variable cContactMobile as character no-undo.

pRequest:getParameter("To",       OUTPUT tTo).
pRequest:getParameter("entity",   OUTPUT ipEntity).
pRequest:getParameter("entityID", OUTPUT ipEntityID).

/* Get the directory and the filename prefix */
publish "GetSystemParameter" ("dataRoot", output cTempFileDir).

if cTempFileDir = "" 
 then
  do:
    pResponse:fault("3005", "Root directory not found").
    return.
  end.

/* Checking whether UserID exists or not */
if ipEntity = "E" /* (E)mployee */
 then
  do:
   if not can-find (first sysuser where sysuser.uid = ipEntityID)
    then
     do:
      pResponse:fault("3066", "UserID").
      return.
     end.

   /* Checking whether shareable field is true or not for sharing the VCARD of the contact/ID.*/
   if can-find (first sysuser where sysuser.uid       = ipEntityID 
                                and sysuser.shareable = false)
    then 
     do:
      pResponse:fault("3005", "You cannot share the VCard.").
      return.
     end.
  end.

/* checking whether IDs exists or not for person/agent . */
if ipEntity = "A" /* (A)gent */
   and not can-find (first agent where agent.agentID = ipEntityID)
    then 
     do:
       pResponse:fault("3066", "Agent").
       return.
     end.

if ipEntity = "P"  /* (P)erson */
   and not can-find (first person where person.personID = ipEntityID)
    then 
     do:
       pResponse:fault("3066", "Person").
       return.
     end.

if tTo = ? or tTo = ""
 then pResponse:fault("3005", "EmailID cannot be blank.").

if pResponse:isFault()
 then return.
 
case ipEntity :
 when "A"  /* (A)gent */
  then
   do: 
     for first agent where agent.agentID = ipEntityID no-lock:

       copy-lob agent.logo to lcLogo.
       chName =  if agent.name = "" then agent.agentID else agent.name.
       cVCard = "BEGIN:VCARD" + chr(10) +                                                                                                                                 
                "VERSION:4.0" + chr(10) +                                                                                                                                 
                "N:;" + agent.name + ";;;" + chr(10) +                                                                                                                   
                "FN:" + agent.name  + chr(10) +   
                "PHOTO;TYPE=JPEG;ENCODING=BASE64:" + lcLogo + chr(10) +                                                                                                                                                                                                                                          
                "TEL;TYPE=WORK,voice;VALUE=uri:" + agent.phone  + chr(10) +                                                                                
                "ADR;TYPE=WORK;PREF=1;LABEL= "":;;" + agent.addr1 + " " + agent.addr2 + " " + agent.addr3 + " " + agent.addr4 + ";"  + agent.city + ";"+  agent.state + ";" + agent.zip + ";" + agent.county + ";" +  chr(10) +   
                "URL;WORK:" + agent.website + chr(10) +       
                "EMAIL:" + if index(agent.email,"@") = 0 then "" else agent.email + chr(10) +                                                                                                                                                      
                "END:VCARD" .
     end.
   end.
 when "P"  /* (P)erson */
  then
   do:
     for first person where person.personID = ipEntityID no-lock:
        
       assign
           cContactEmail  = ''
           cContactMobile = ''
           cContactPhone  = ''
           .
       run com/getpersoncontact-p.p(input person.PersonID , output cContactEmail,output cContactMobile,output cContactPhone).

       chName =  if person.dispname = "" then person.personID else person.dispName.
       cVCard = "BEGIN:VCARD" + chr(10) +                                                                                                                                 
                "VERSION:4.0" + chr(10) +                                                                                                                                 
                "N:" + person.lastname + ";" + person.middlename + ";" + person.firstname + ";" + person.title_ + ";" + chr(10) +                                                                                                                                  
                "FN:" + person.dispname + chr(10) +                                                                                                                                                                                                                                           
                "TEL;TYPE=WORK,voice;VALUE=uri:" + cContactPhone + chr(10) +                                                                                    
                "TEL;TYPE=CELL,voice;VALUE=uri:" + cContactMobile + chr(10) +                                                                                    
                "ADR;TYPE=WORK;PREF=1;LABEL= "":;;" +  person.address1 + " " + person.address2 + ";" + person.city + ";"+  person.state + ";" + person.zip + ";" + person.county + ";" +  chr(10) +   
                "EMAIL:" + if index(cContactEmail,"@") = 0 then "" else cContactEmail + chr(10) +                                                                                                                                                      
                "END:VCARD" . 

     end.
   end.
 when "E" /* (E)mployee */
  then
  do:
    for first sysuser where sysuser.uid = ipEntityID and sysuser.shareable = true no-lock:
      chName = if sysuser.name = "" then sysuser.uid else sysuser.name .
      cVCard = "BEGIN:VCARD" + chr(10) +
                "VERSION:4.0" + chr(10) +
                "N:;" + sysuser.name + ";;;" + chr(10) +
                "FN:" + sysuser.name + chr(10) +
                "TITLE:" + sysuser.jobtitle + chr(10) +
                "PHOTO;TYPE=JPEG;ENCODING=BASE64:" + sysuser.picture + chr(10) +   
                "TEL;TYPE=" + sysuser.phonetype  + "," + "voice;VALUE=uri:" + sysuser.phone + chr(10) +
                "TEL;TYPE=" + sysuser.phone2type + "," + "voice;VALUE=uri:" + sysuser.Phone2 + chr(10) +
                "ADR;TYPE=WORK;PREF=1;LABEL= "":;;" + sysuser.address1 + " " + sysuser.address2 + ";" + sysuser.city + ";" + sysuser.state + ";" + sysuser.zip + ";" + chr(10) +
                "EMAIL:" + if index(sysuser.email,"@") = 0 then "" else sysuser.email + chr(10) +
                "END:VCARD" .

    end.
  end.
 end case.

/* temppath to generate vcard */
assign
   cTempFileDir        = cTempFileDir + "VCard\"  
   file-info:file-name = cTempFileDir
   .
/* Check whether the Vcard folder is already exist or not */
if file-info:full-pathname eq ? 
 then
  do:
    os-create-dir value(cTempFileDir).
    if os-error ne 0 
     then
      do:
        pResponse:fault("3005", "Failed to create folder 'VCard'.").
        return.
      end.
  end.


if index(chName,",") > 0
 then
  chName = replace(chName,",","").

if index(chName,".") > 0 
 then
  chName = replace(chName,".","").

/* Creating vCard on using this path */
cTempFilePath = cTempFileDir + chName + ".vcf".

copy-lob cVCard to file cTempFilePath.

/* Sending email */
run util/attachmail.p (pRequest:Uid, /* from loggedIN user*/
                       tTo,          /* To */
                       "",          /* CC */
                       "Contact information for" + "-" + chName , /* Subject */
                       "", /* Body */
                       cTempFilePath).  /* Attachment */

pResponse:success("3005", "The email has been sent successfully").

os-delete value (cTempFilePath).
