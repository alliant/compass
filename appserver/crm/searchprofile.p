/*-------------------------------------------------------------------------------
@name crm/searchprofile.p
@action profileSearch
@description Returns a dataset of the Agent/Person based on the criteria

@param pcStateID;char;The stateID of person
@param pcsearchstring;char;The searchString of person
@param pcProfileType;char;Type of profile Agent/Person/All
@param piPageNum;int;
@param piNoOfRows;int;


@returns The person data
@success 2005;char;The count of the records returned

@author Rahul
@version 1.0
@created
Date         Name          Description
05/21/19     Rahul Sharma  Modified logic based on the UI
05/31/19     Rahul Sharma  Modified to return sysfavoriteID in profile temp-table  
09/24/19     Rahul Sharma  Modified to trim name field for extra spaces
03/28/2024   S Chandu      Task #:111689 Removed the reference of Person.email.
---------------------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

/*----------Variable Defination------------------ */
define variable pcSearchString     as character no-undo.
define variable pcStateID          as character no-undo.
define variable pcProfileType      as character no-undo.
define variable piPageNum          as integer   no-undo.
define variable piNoOfRows         as integer   no-undo.
                                   
define variable iCount             as integer   no-undo.
define variable iTotalMatch        as integer   no-undo.
define variable iNumResultAgent    as integer   no-undo.
define variable iNumResultPerson   as integer   no-undo.
define variable iNumResultsysUser  as integer   no-undo.

{lib/std-def.i}

/*----------Temp-Table Defination------------------ */
{tt/person.i &tableAlias="tempPerson"}
{tt/profile.i &favorite=true}

pRequest:getParameter("searchString", output pcSearchString).

if pcSearchString = "" or pcSearchString = ?
 then
  do:
    pResponse:fault("3005", "Search string cannot be blank. ").
    return.
  end.

pcSearchString = trim(pcSearchString).

/* Get Parameter values in variable */
pRequest:getParameter("stateID", output pcStateID).

if pcStateID = "" 
 then
  pcStateID = "ALL".

pRequest:getParameter("Type",         output pcProfileType).
pRequest:getParameter("PageNum",      output piPageNum).
pRequest:getParameter("NoOfRows",     output piNoOfRows).

assign 
   /* std-in            = 0 */
    iNumResultAgent   = 0
    iNumResultPerson  = 0
    iNumResultsysUser = 0
    .

if lookup("A",pcProfileType)   > 0 or /* Agent */
   lookup("ALL",pcProfileType) > 0    /* For 'ALL' get all agent, person and sysuser record */
 then  
  do icount = 1 to num-entries(pcSearchString," "):      

    for each sysIndex where sysIndex.objType begins "Agent"
                      and   sysIndex.objKey  begins entry(icount,pcSearchString," ") no-lock:    
        
      for each agent where agent.agentID = sysIndex.objID 
                       and agent.stateid = (if pcStateID = "ALL" then agent.stateid else pcStateID) no-lock:
      
        if not can-find(first profile where profile.type     = "A" /* (A)gent */ 
                                        and profile.entityID = agent.agentID) 
         then
          do:
            iNumResultAgent = iNumResultAgent + 1.
      
            if iNumResultAgent gt ((piPageNum - 1) * piNoOfRows) and
               iNumResultAgent le ((piPageNum - 1) * piNoOfRows) + piNoOfRows 
             then
              do:
                create profile.
                assign 
                    profile.type     = "A"  /* (A)gent */   
                    profile.entityId = agent.agentId
                    profile.name     = trim(agent.name)
                    profile.stat     = agent.stat
                    profile.email    = agent.email
                    profile.city     = agent.city  
                    profile.state    = agent.state 
                    profile.zip      = agent.zip
                    .   

                for first sysfavorite no-lock
                  where sysfavorite.uid      = pRequest:Uid
                    and sysfavorite.entity   = "A" /* (A)gent */
                    and sysfavorite.entityID = agent.agentId:
                  assign
                      profile.sysfavoriteID = sysfavorite.sysfavoriteID 
                      profile.favorite      = true
                      .
                end. /* for first sysfavorite...*/
                     
              end. /* if iNumResult gt ((piPageNum - 1) * piNoOfRows) */                      
          end. /* if not can-find(first profile */
      end. /* for each agent */
    end. /* for each sysIndex */
  end. /* if pcProfileType = "A" */ 

if lookup("P",pcProfileType)   > 0  or /* Person */                                      
   lookup("ALL",pcProfileType) > 0     /* For 'ALL' get all agent, person and sysuser record */
 then
  do:
    /* Get all the person as per stateId in tempPerson */
    for each person no-lock 
      where person.state = (if pcStateID = "ALL" then Person.state else pcStateID):
      create tempPerson.
      buffer-copy person to tempPerson.
    end. 

    if pcStateID ne "ALL" 
     then
      do:
        for each agent no-lock
          where agent.stateId = pcStateID:
          for first orgrole no-lock where orgrole.sourceID = agent.agentID:
            for each affiliation no-lock
              where affiliation.partnerA = orgrole.orgID or affiliation.partnerB = orgrole.orgID :
              if not can-find(first tempPerson where tempPerson.personId eq affiliation.partnerB)
               then
                do:
                  for first person no-lock
                    where person.personId = affiliation.partnerB:
                
                    create tempPerson.
                    buffer-copy Person to tempPerson.            	    
                  end. /* for first person no-lock */
                end. /* if not can-find(first tempPerson */
            end. /* for each affiliation no-lock */
          end.  /* for first orgrole */
        end. /* for each agent no-lock */ 
      end. /* if pcStateID ne "ALL" */

    do icount = 1 to num-entries(pcSearchString , " "):
      for each sysIndex where sysIndex.objType begins "Person"
                          and sysIndex.objKey  begins entry(icount,pcSearchString," ")
                          no-lock:

        for first tempPerson where tempPerson.personId = sysIndex.objID: 
          if not can-find(first profile where profile.type     = "P" /* (P)erson */
                                          and profile.entityID = sysIndex.objID) 
           then
            do:
              iNumResultPerson = iNumResultPerson + 1.
              if iNumResultPerson gt ((piPageNum - 1) * piNoOfRows) and
                 iNumResultPerson le ((piPageNum - 1) * piNoOfRows) + piNoOfRows 
               then
                do:
                  create profile.
                  assign
                      profile.type     = "P" /* (P)erson */   
                      profile.entityId = tempPerson.personId
                      profile.name     = trim(tempPerson.dispName)
                      profile.stat     = if tempPerson.active then "A" else "I"
                      profile.city     = tempPerson.city  
                      profile.state    = tempPerson.state 
                      profile.zip      = tempPerson.zip
                      .   
                  
                  for first sysfavorite no-lock
                    where sysfavorite.uid      = pRequest:Uid
                      and sysfavorite.entity   = "P" /* (P)erson */
                      and sysfavorite.entityID = tempPerson.personId:
                    assign
                        profile.sysfavoriteID = sysfavorite.sysfavoriteID 
                        profile.favorite      = true
                        .
                  end. /* for first sysfavorite...*/

                end. /* if iNumResult gt (piPageNum * piNoOfRows) and */          
            end. /* if not can-find(first profile */
        end. /* for first tempPerson */
      end. /* for each sysIndex */   
    end. /* do icount = 1 to num-entries(pcSearchString , " ") */
  end. /* if pcProfileType = "P" */

if lookup("E",pcProfileType) >   0 or /* SysUser */
   lookup("ALL",pcProfileType) > 0    /* For 'ALL' get all agent, person and sysuser record */
 then  
  do icount = 1 to num-entries(pcSearchString," "):      

    for each sysIndex where sysIndex.objType begins "SysUser"
                      and   sysIndex.objKey  begins entry(icount,pcSearchString," ")
                      no-lock:    
    
      for each sysUser where sysUser.UID  = sysIndex.objID no-lock:

        if not can-find(first profile where profile.type     = "E" /* (E)mployee or Sysuser */
                                        and profile.entityID = sysUser.UID) 
         then
          do:
            iNumResultsysUser = iNumResultsysUser + 1.

            if iNumResultsysUser gt ((piPageNum - 1) * piNoOfRows) and
               iNumResultsysUser le ((piPageNum - 1) * piNoOfRows) + piNoOfRows 
             then
              do:
                create profile.
                assign 
                    profile.type     = "E" /* (E)mployee or Sysuser */    
                    profile.entityId = sysUser.UID
                    profile.name     = trim(sysUser.name) 
                    profile.stat     = if sysUser.isActive then "A" else "I"
                    profile.email    = sysuser.email
                    profile.city     = sysUser.city  
                    profile.state    = sysUser.state
                    profile.zip      = sysUser.zip
                    .   
                
                for first sysfavorite no-lock
                  where sysfavorite.uid      = pRequest:Uid
                    and sysfavorite.entity   = "E" /* (E)mployee or Sysuser */
                    and sysfavorite.entityID = sysUser.UID:
                  assign
                      profile.sysfavoriteID = sysfavorite.sysfavoriteID 
                      profile.favorite      = true
                      .
                end. /* for first sysfavorite...*/

              end. /* if iNumResult gt ((piPageNum - 1) * piNoOfRows) */                      
          end. /* if not can-find(first profile */
      end. /* for each sysUser */
    end. /* for each sysIndex */
  end. /* if pcProfileType = "E" */ 

iTotalMatch = iNumResultAgent + iNumResultPerson + iNumResultsysUser.

if iTotalMatch > 0
 then
  do:
    create profile.
    assign
        profile.type   = "TotalMatch"    
        profile.rowNum = string(iTotalMatch)
        .
    pResponse:setParameter("profile", table profile).
  end.
    
pResponse:success("2005", string(iTotalMatch) {&msg-add} "SearchProfile").
