/*------------------------------------------------------------------------
@name crm/modifycontact.p
@action contactModify
@description Modify contact information of an entity
@param
@returns 2006;Contact Information has been changed
@throws 3066;Entity does not exist
@throws 3004;Entity is locked
@throws 3000;ModifyContact failed
@author Rahul Sharma
@version 1.0
@created 07/08/2019
@notes
@modified
Date          Name          Decritpion
06/03/2019    Gurvindar     Modified logic 
08/16/2022    SA            Task #96812 Modified to add functionality for personagent.
11/22/2022    VR            Modified to assign the values to doNotCall and internal fields in person table
04/01/2024    S Chandu      Task #:111689 Remove the reference of Person.mobile, Person.phone and Person.email
                             and  added validations for duplicate contacts,updates the personcontact table.
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable cEntity          as character no-undo.
define variable lFound           as logical   no-undo.
define variable cEntityID        as character no-undo.
define variable iPersonContactID as integer   no-undo.
define variable cerrmsg          as character no-undo.
define variable cvalidNumber     as character no-undo.
define variable lerror           as logical   no-undo.

define variable iEmailPersonContactID  as integer no-undo.
define variable iMobilePersonContactID as integer no-undo.

{lib/std-def.i}
{lib/nextkey-def.i}
{lib/validEmailAddr.i}

{tt/entitycontact.i} 

/* Getting temptable */
empty temp-table entitycontact.
pRequest:getParameter("EntityContact", input-output table entitycontact).

for first entitycontact:

  if entitycontact.entity = "A" and not can-find(first agent where agent.agentID = entitycontact.entityID) 
   then
    pResponse:fault("3066", "Agent").

  if entitycontact.entity = "P" and not can-find(first person where person.personID = entitycontact.entityID) 
   then
    pResponse:fault("3066", "Person").
             
  /* validation on Person Entity */
  if entitycontact.entity = "P"
   then
    do:
      if (entitycontact.email = "" or entitycontact.email = ?) and (entitycontact.phone1 = "" or entitycontact.phone1 = ?)
       then
        do:
          pResponse:fault("3005","Both Email and Mobile cannot be blank.").
          return.
        end.
        
      /* validation for duplication of email */
      if (entitycontact.email <> "" or entitycontact.email <> ?)
       then
        do:
          /* checks for valid Email Address */
          if not validEmailAddr(entitycontact.email)
           then
            do:
              pResponse:fault("3005","Invalid Email Address.").
              return.
            end.

          /* To find the Email type record personcontactID */
          find first personcontact where personcontact.personid       = entitycontact.entityID
                                     and personcontact.contactType    = 'E'
                                     and personcontact.ContactSubType = 'Work' no-lock no-error.
          if not available personcontact
           then
            find first personcontact where personcontact.personid    = entitycontact.entityID 
                                       and personcontact.contactType = 'E' no-lock no-error.


          if available personcontact
           then
            iEmailPersonContactID = personcontact.personcontactID.
 
          /* if exists check for duplicate record */
          if iEmailPersonContactID ne 0 and iEmailPersonContactID ne ?
           then           
            if can-find(personcontact where personcontact.contactID = entitycontact.email and personcontact.personcontactID <> iEmailPersonContactID)
             then
              do:
                pResponse:fault("3005", "Duplicate Email").
                return.
              end. 
          else
           if can-find(personcontact where personcontact.contactID = entitycontact.email)
            then
             do:
               pResponse:fault("3005", "Duplicate Email").
               return.
             end.

        end.  /* if email <> '' */

      /* Validation for duplication of Mobile */
      if (entitycontact.phone1 <> "" or entitycontact.phone1 <> ? )
       then
        do:
           /* checks for valid number */
           run util/validatecontactnumber-p.p(input entitycontact.phone1,output cvalidNumber,output lerror,output cerrmsg).
           if lerror
            then
             do:       
               pResponse:fault("3005", cerrmsg).
               return.
            end.

           /* To find the Mobile type record personcontactID */
          find first personcontact where personcontact.personid       = entitycontact.entityID
                                     and personcontact.contactType    = 'P'
                                     and personcontact.ContactSubType = 'Mobile' no-lock no-error.


          if available personcontact
           then
            iMobilePersonContactID = personcontact.personcontactID.
 
           /* if exists check for duplicate record */
          if iMobilePersonContactID ne 0 and iMobilePersonContactID ne ?
           then             
            if can-find(personcontact where personcontact.contactID = entitycontact.phone1 and personcontact.personcontactID <> iMobilePersonContactID)
             then
              do:
                pResponse:fault("3005", "Duplicate Mobile").
                return.
              end. 
          else   /* checks for duplicate in personcontact */
           if can-find(personcontact where personcontact.contactID = entitycontact.phone1)
            then
             do:
               pResponse:fault("3005", "Duplicate Mobile").
               return.
             end.

       end.  /* if Mobile <> '' */
    end. /* do: if end */
end. /* for first entity contact */

assign 
    std-lo = false
    lFound = false
    .

TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:

  for first entitycontact: 
    
    if entitycontact.entity = "A" 
     then
      do:
        cEntity = "Agent".
        for first agent exclusive-lock 
          where agent.agentID = entitycontact.entityID:

          assign 
              cEntityID = agent.agentID
              lFound    = true
              .
         
          assign        
              agent.email   = entitycontact.email
              agent.contact = entitycontact.phone1
              agent.phone   = entitycontact.phone2
              agent.addr1   = entitycontact.addr1
              agent.addr2   = entitycontact.addr2
              agent.addr3   = entitycontact.addr3
              agent.addr4   = entitycontact.addr4
              agent.city    = entitycontact.city
              agent.state   = entitycontact.state 
              agent.zip     = entitycontact.zip 
              . 
         
          validate agent.
          release agent.
          
          /* Build keyword index to use for searching */
          std-ch = "for each agent where agent.agentID = '" + cEntityID + "'".
          
          run sys/buildsysindex.p (input pRequest,
                                   input pResponse,
                                   input "Agent",       /* ObjType */
                                   input "",             /* ObjAttribute */
                                   input cEntityID,      /* ObjID */
                                   input "Agent",       /* SourceTable */
                                   input std-ch,         /* SourceQuery */
                                   input "logo").        /* ExcludeFieldList */
         
          std-lo = true. 
        end. /* for first agent...*/
      end. /* if entitycontact.entity = "A" */
    else if entitycontact.entity = "P" 
     then
      do:
        cEntity = "Person".
        for first person exclusive-lock 
          where person.personID = entitycontact.entityID:
          
          assign 
              cEntityID = person.personID
              lFound    = true.
          
          assign  
              person.address1  = entitycontact.addr1
              person.address2  = entitycontact.addr2
              person.city      = entitycontact.city
              person.state     = entitycontact.state 
              person.zip       = entitycontact.zip 
              person.doNotCall = entityContact.doNotCall
              person.internal  = entityContact.internal
              . 
         
          validate person.
          release person.
                    
          /* Email */ 
          /* create person contact for email if not exist */
          if iEmailPersonContactID = 0 or iEmailPersonContactID = ?               
           then
            do:
               {lib/nextkey.i &type='personContact' &var=iPersonContactID &err="undo TRX-BLOCK, leave TRX-BLOCK."}
       
               create personContact.
               assign   
                   personContact.personcontactID  = iPersonContactID
                   personContact.personID         = entitycontact.entityID
                   personContact.contactType      = 'E' 
                   personContact.contactID        = entitycontact.email 
                   personContact.uid              = pRequest:uid
                   personContact.createDate       = today
                   personcontact.contactsubtype   = 'Work'
                   .
               
               validate personContact.
               release personContact.
            end. /* if not can-find email */
            
          /* modify person contact for email if not exist */ 
          else
           do:
             /* if entitycontact.email have value then update */
             if entitycontact.email ne ''
              then 
               do:
                 for first personcontact exclusive-lock
                   where personcontact.personcontactID = iEmailPersonContactID  and
                         personcontact.personID        = entitycontact.entityID and
                         personcontact.contactType     = 'E' and                 
                         personContact.expirationDate  = ? : 
            
                   personcontact.contactID = entitycontact.email.
       
                   validate personcontact.
                   release personcontact.
                 end. /* for first personcontact */
               end. /* do if entitycontact.email ne '' */
             
             /* if entitycontact.email is blank and previosly it have value then deactivate */
             else
              do:
                if can-find(first personcontact where personcontact.personcontactID = iEmailPersonContactID  and
                                                      personcontact.personID        = entitycontact.entityID and 
                                                      personcontact.contactType     = 'E' and 
                                                      personcontact.contactID       ne '' and 
                                                      personcontact.expirationDate  = ?  ) 
                 then
                  do:
                    for first personcontact exclusive-lock
                      where personcontact.personcontactID = iEmailPersonContactID and
                            personcontact.personID        = entitycontact.entityID and 
                            personcontact.contactType     = 'E' and 
                            personContact.expirationDate  = ?: 
                    
                      personcontact.expirationDate = today.
                    
                      validate personcontact.
                      release personcontact.
                    end. /* for first personcontact */
                  end. /* if not can-find( */
                
              end. /* do: else deactivate */
           end. /* do: else modify email */
           
          /* Mobile */
          /* create person contact for phone1 if not exist */
        if iMobilePersonContactID = 0 and iMobilePersonContactID = ?        
         then
          do:
             {lib/nextkey.i &type='personContact' &var=iPersonContactID &err="undo TRX-BLOCK, leave TRX-BLOCK."}
        
             create personContact.
             assign   
                 personContact.personcontactID  = iPersonContactID
                 personContact.personID         = entitycontact.entityID
                 personContact.contactType      = 'P' 
                 personContact.contactID        = entitycontact.phone1 
                 personContact.uid              = pRequest:uid
                 personContact.createDate       = today
                 personcontact.contactsubtype   = 'Mobile'
                 .
             
             validate personContact.
             release personContact.
              
          end. /* if not can-find */
            
          /* modify person contact for phone if not exist */
          else
           do:
             /* if entitycontact.phone have value then update */
             if entitycontact.phone1 ne ''
              then 
               do:
                 for first personcontact exclusive-lock
                   where personcontact.personcontactID = iMobilePersonContactID and
                         personcontact.personID        = entitycontact.entityID and 
                         personcontact.contactType     = 'P' and 
                         personcontact.expirationDate  = ? : 
            
                   personcontact.contactID = entitycontact.phone1.
       
                   validate personcontact.
                   release personcontact.
                 end. /* for first personcontact */
               end. /* do if entitycontact.phone1 ne '' */
             
             /* if entitycontact.phone is blank and previosly it have value then deactivate */
             else
              do:
                if can-find(first personcontact where personcontact.personcontactID = iMobilePersonContactID and
                                                      personcontact.personID        = entitycontact.entityID and 
                                                      personcontact.contactType     = 'P' and 
                                                      personcontact.contactID       ne '' and 
                                                      personcontact.expirationDate  = ?  ) 
                 then
                  do:
                    for first personcontact exclusive-lock
                      where personcontact.personcontactID = iMobilePersonContactID and
                            personcontact.personID        = entitycontact.entityID and 
                            personcontact.contactType     = 'P' and 
                            personcontact.expirationDate  = ? : 
                    
                      personcontact.expirationDate = today.
                    
                      validate personcontact.
                      release personcontact.
                    end. /* for first personcontact */
                  end. /* if not can-find( */
                
              end. /* do: else deactivate */
           end. /* do: else modify phone */
          
          /* Build keyword index to use for searching */
          std-ch = "for each person where person.personID = '" + cEntityID + "'".
          run sys/buildsysindex.p (input pRequest,
                                   input pResponse,
                                   input "Person",       /* ObjType */
                                   input "",              /* ObjAttribute */
                                   input cEntityID,       /* ObjID */
                                   input "Person",       /* SourceTable */
                                   input std-ch,          /* SourceQuery */
                                   input "").             /* ExcludeFieldList */
         
          std-lo = true. 
        end. /* for first person...*/
      end. /* else if entitycontact.entity = "P" */
  end. /* for first entitycontact...*/
end.

if not lFound
 then
  pResponse:fault("3004", cEntity).
 else if not std-lo 
  then
   pResponse:fault("3000", "ModifyContact").

if pResponse:isFault() 
 then 
  return.
  
pResponse:success("2006", "Contact Information").
pResponse:updateEvent("cEntity", cEntityID).
