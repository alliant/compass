/*----------------------------------------------------------------------
@name GetStateCounties.cls
@action StateCountiesGet
@description Provides either a complete list of all counties or a specific county in a state
@param stateIDList;
@returns  Counties;complex;County dataset
@author Spandana
@created 04.04.2023
Modified
Date       Name    Description              

----------------------------------------------------------------------*/

class county.GetStateCounties inherits framework.ActionBase:

  {lib/callsp-defs.i }
  {tt/county.i &getStateCounties=true}

  constructor public GetStateCounties ():
    super().
  end constructor.

  destructor public GetStateCounties ():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}
    define variable cStateIDList as character no-undo.
    define variable dsHandle     as handle    no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer county:handle).

    pRequest:getParameter("StateIDList", output cStateIDList).

    if cStateIDList = ? 
     then
      cStateIDList = ''.

    {lib\callsp.i 
        &name=spGetStateCounties 
        &load-into=county 
        &params="input cStateIDList"
        &noResponse=true}
 
    if can-find(first county) 
     then
       setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2005", string(csp-icount) {&msg-add} "County").
     
  end method.
  
end class.
