/*------------------------------------------------------------------------
@name CountyModify
@description Modifies a county using the specified state, county, and description
@param StateID;char;State of the county to add
@param CountyID;char;County code of the county to add
@param Description;char;Description of the county to add
@returns Success;int;2006
@throws 3066;Invalid State/County
@throws 3004;Record locked
@author Russ Kenny
@version 1.0 12/16/2013
@notes
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}

def var pcState as char no-undo.
def var pcCounty as char no-undo.
def var pcDescription as char no-undo.

pRequest:getParameter("StateID", output pcState).
pRequest:getParameter("CountyID", output pcCounty).
IF NOT CAN-FIND(FIRST county
                  WHERE county.stateID = pcState
                    AND county.countyID = pcCounty)
 THEN 
  do: pResponse:fault("3066", "County").
      RETURN.
  END.

pRequest:getParameter("Description", pcDescription).

if pcDescription = ? 
 then pcDescription = "".
  

std-lo = false.
TRX-BLK:
for first county exclusive-lock
  where county.stateID = pcState
    and county.countyID = pcCounty TRANSACTION
   on error undo TRX-BLK, leave TRX-BLK:

 county.description = pcDescription.
 validate county.
 
 std-lo = true.
end.

if not std-lo
 then
  do: pResponse:fault("3004", "County").
      return.
  end.
  
pResponse:success("2006", "County").
pResponse:updateEvent("County", pcState {&id-add} pcCounty).
