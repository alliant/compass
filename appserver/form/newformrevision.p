&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*---------------------------------------------------------------------
@name newformrevision.p
@action formRevisionNew
@description Create a form revision within the iform table

@param FormID;integer;The original form ID to revise

@author John Oliver
@version 1.0
@created 2017/06/19
@notes 
---------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

{tt/form.i}
{lib/std-def.i}

/* parameters */
define variable pFormID as integer no-undo.

/* local variables */
define variable iSeq as integer no-undo initial 0.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

pRequest:getParameter("formID", output pFormID).
if not can-find(first iform where formID = pFormID)
 then pResponse:fault("3066", "Form " + string(pFormID)).

if pResponse:isFault()
 then return.

/* all parameters are valid so start to insert the new row */
std-lo = false.
TRX-BLOCK:
do TRANSACTION
  on error undo TRX-BLOCK, leave TRX-BLOCK:
  
  iSeq = 1.
  for first iform no-lock
      where iform.formID = pFormID
         by iform.revisionID desc:
         
    iSeq = iSeq + iform.revisionID.
  end.
  
  for first iform exclusive-lock
      where iform.formID = pFormID:
      
    create formtable.
    buffer-copy iform to formtable.
  end.
  
  for first formtable no-lock:
  
    create iform.
    assign
      std-lo = true
      iform.formID = formtable.formID
      iform.revisionID = iSeq
      iform.stat = "N"
      iform.formType = formtable.formType
      iform.insuredType = formtable.insuredType
      iform.orgName = formtable.orgName
      iform.orgVersion = formtable.orgVersion
      iform.orgEffDate = formtable.orgEffDate
      iform.orgPubDate = formtable.orgPubDate
      iform.orgReference = formtable.orgReference
      iform.repositoryID = formtable.repositoryID
      iform.description = formtable.description
      iform.formTitle = formtable.formTitle
      iform.formHeader = formtable.formHeader
      iform.formFooter = formtable.formFooter
      iform.anticNumber = formtable.anticNumber
      iform.dateCreated = now
      iform.createdBy = pRequest:uid
      iform.parentID = formtable.parentID
      .
  end.
end.

if not std-lo
 then
  do: pResponse:fault("3000", "Revise").
      return.
  end.

pResponse:setParameter("revisionID", iSeq).
pResponse:success("2000", "Revision").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


