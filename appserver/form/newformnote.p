&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*---------------------------------------------------------------------
@name newformnote.p
@action formNoteNew
@description Create a form note within the iformnote table

@param FormID;integer;The form ID
@param Stat;char;The status of the note
@param Subject;char;The subject of the note
@param Type;char;The type off the note
@param Notes;char;The note
  
@author John Oliver
@version 1.0
@created 2017/07/13
@notes 
---------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

{lib/std-def.i}

/* parameters */
define variable pFormID as integer no-undo.
define variable pStat as character no-undo.
define variable pSubject as character no-undo.
define variable pType as character no-undo.
define variable pNotes as character no-undo.

/* local variables */
define variable iSeq as integer no-undo initial 0.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

pRequest:getParameter("formID", output pFormID).
if not can-find(first iform where formID = pFormID)
 then pResponse:fault("3066", "Form " + string(pFormID)).

pRequest:getParameter("stat", output pStat).
if not can-find(first sysprop where appCode = "FMA" and objAction = "FormNote" and objProperty = "Status" and objID = pStat)
 then pResponse:fault("3066", "Note Status " + pStat).
 
pRequest:getParameter("subject", output pSubject).
pRequest:getParameter("type", output pType).
if not can-find(first sysprop where appCode = "FMA" and objAction = "FormNote" and objProperty = "Type" and objID = pType)
 then pResponse:fault("3066", "Note Type " + pType).
 
pRequest:getParameter("notes", output pNotes).

/* if erred, exit */
if pResponse:isFault()
 then return.
 

/* all parameters are valid so start to insert the new row */
TRX-BLOCK:
do TRANSACTION
  on error undo TRX-BLOCK, leave TRX-BLOCK:
  
  iSeq = 1.
  for first iformnote no-lock
      where iformnote.formID = pFormID
         by iformnote.seq desc:
         
    iSeq = iSeq + iformnote.seq.
  end.
  
  create iformnote.
  assign
    
    iformnote.formID = pFormID
    iformnote.seq = iSeq
    iformnote.stat = pStat
    iformnote.type = pType
    iformnote.subject = pSubject
    iformnote.createDate = now
    iformnote.notes = pNotes
    iformnote.uid = pRequest:uid
    .
  validate iformnote.
  release iformnote.
end.

if not can-find(first iformnote where formID = pFormID and seq = iSeq)
 then
  do: pResponse:fault("3000", "Create Note").
      return.
  end.

pResponse:setParameter("noteID", iSeq).
pResponse:success("2000", "Create Note").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


