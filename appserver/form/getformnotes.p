&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*---------------------------------------------------------------------
@name getformnotes.p
@action formNotesGet
@description Gets the form notes from the iformnote table

@return FormNote;complex;The iformnote table rows

@author John Oliver
@version 1.0
@created 2017/06/19
@notes 
---------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

{lib/std-def.i}
{tt/formnote.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

std-in = 0.
for each iformnote no-lock:

  create formnote.
  buffer-copy iformnote to formnote.
  std-in = std-in + 1.
end.


IF std-in > 0 
 then pResponse:setParameter("FormNote", TABLE formnote).

pResponse:success("2005", STRING(std-in) {&msg-add} "Form Note").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


