&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*---------------------------------------------------------------------
@name deleteform.p
@action formDelete
@description Delete a form within the iform table

@param FormID;integer;The form to delete

@author John Oliver
@version 1.0
@created 2017/06/19
@notes 
---------------------------------------------------------------------*/

/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

{tt/form.i}
{lib/std-def.i}

/* parameters */
define variable pFormID as integer no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

pRequest:getParameter("formID", output pFormID).

/* all parameters are valid so start to insert the new row */
std-in = 0.
for each iform exclusive-lock
   where iform.formID = pFormID:
  
  std-in = std-in + 1.
  delete iform.
end.

if can-find(first iform where formID = pFormID)
 then
  do: pResponse:fault("3000", "Delete").
      return.
  end.

pResponse:success("2009", STRING(std-in) {&msg-add} "Form").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


