import smtplib
import sys
from dateutil.parser import parse
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.utils import COMMASPACE, formatdate, format_datetime
import string
from bs4 import BeautifulSoup as bs
import re
import base64

##store system parameters
smtp_server_address = ''
from_address = ''
to_address = ''
cc_address = ''
body = ''
subject = ''
attachments = ''
actual_from = ''
actual_to = ''
sent_date = ''

def  convert_base64_images ():
   
  if not body:
    return'',[]
  # parse all html content and get aLL
  soup = bs(body, 'html.parser')

  # if soup is None that means the mail would be of tye text not html
  if not soup:
    return body,[]
   
  img_tags = soup.find_all('img')
  if not img_tags:
    return body,[]
  
  inline_attachments = []

  # iterare to all img tags
  for index, img in enumerate(img_tags):
    img_src = img.get('src')
    if img_src and img_src.startswith ('data:image'):
      img_base64_data = re.search(r'data:(image/\w+);base64,(.*)', img_src)
      if img_base64_data:
        mime_type =img_base64_data.group(1) # gives the image type
        img_base64_str = img_base64_data.group(2) # actual base64 image data string
          
        # Generating cid for the image
        cid = f'image{index}@alliantnational.com'
        img['src'] = f'cid:{cid}'
          
        # decode the base64 image
        img_data = base64.b64decode(img_base64_str)
        img_mime = MIMEImage(img_data, mime_type.split('/')[1])
        img_mime.add_header('Content-ID', f'<{cid}>')
        inline_attachments.append(img_mime)
  return str(soup), inline_attachments
   
def send_mail () -> bool:
  try:
    temp_to_address = to_address.split(';')
    email_to = ','.join(temp_to_address)
    temp_cc_address = cc_address.split(';')
    email_cc = ','.join(temp_cc_address)
    email_subject = subject
    email_body, html_inline_attachments = convert_base64_images ()
    email_from = ''
    if len(from_address.split(';')) > 1:
      email_from = from_address.split(';')[1] + '<' + from_address.split(';')[0] + '>'
    else:
      email_from = from_address.split(';')[0]
    importance_level = None
    email_recipient = email_to.split(",") + email_cc.split(",")

    message = MIMEMultipart()

    message["From"] = email_from
    msg_to = ''
    for person in email_to.split(','):
      msg_to += "{0}, ".format(person)
    message["To"] = msg_to
    message["Cc"] = email_cc
    message["Subject"] = email_subject

    message["X-Actual-From"] = actual_from
    message["X-Actual-To"] = actual_to

    if sent_date == "?":
          message['Date'] = formatdate(localtime=True)
    else:
        dt = parse(sent_date)
        message['Date'] = format_datetime(dt)
        
    message.attach(MIMEText(email_body, "html"))
    email_attachments = []

    if attachments != '':
      email_attachments = attachments.strip().split(',')

    for email_counter in range(len(email_attachments)):
        if (email_attachments[email_counter].count('|') + 1) > 1:
          attachment_path = email_attachments[email_counter].split('|')[0]
          cid = email_attachments[email_counter].split('|')[1]
          with open(attachment_path, "rb") as inline_attachment:
            inline_part = MIMEImage(inline_attachment.read(), name=basename(attachment_path))
            inline_part.add_header("Content-ID",f"<{cid}>")
            inline_part.add_header('Content-Disposition','inline; filename="%s"' % basename(attachment_path))
            message.attach(inline_part)
        else:
          with open(email_attachments[email_counter], "rb") as attachment_file:
            attach = MIMEApplication(attachment_file.read(), _subtype="auto")
            attach.add_header('Content-Disposition','attachment; filename="%s"' % basename(email_attachments[email_counter]))
            message.attach(attach)
    for html_inline_attachment in html_inline_attachments:
      message.attach(html_inline_attachment)

    smtp_server = smtplib.SMTP(smtp_server_address, smtp_port_number)
    smtp_server.sendmail(email_from, email_recipient, message.as_string())
    smtp_server.close()

  except Exception as err:
    sys.stderr.write(str(err))

## program entry point
if __name__ == "__main__":  

  smtp_server_address = sys.argv[1]
  smtp_port_number = sys.argv[2]
  to_address = sys.argv[3]
  from_address = sys.argv[4]
  cc_address = sys.argv[5].strip()
  subject = sys.argv[6]
  attachments = sys.argv[7].strip()
  actual_from = sys.argv[8].strip()
  actual_to =  sys.argv[9].strip()
  sent_date = sys.argv[11]

  with open(sys.argv[10].strip(),"r") as body_stream:
    body = body_stream.read()

  ##removing any inprintable characters
  subject = ''.join(filter(lambda x: x in string.printable, subject))

  send_mail()




