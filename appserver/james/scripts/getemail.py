import sys
import imaplib
import email
import json
import base64
from bs4 import BeautifulSoup

#Global variable to store IMAP instance
imap_server = None

def _get_email(Message_uid, output_file_name):
    try:
        status, msgCount = imap_server.select(mailbox="INBOX", readonly=True)
        if status != 'OK':
            sys.stderr.write('Something went wrong. Please check Apache James log and IMAP server log files in log folder in apache James installation directory')
            return
        
        # Geting all the mails and parsing only the headers 
        htmlbody = ''
        plainbody = ''
        body = ''
        isMailExits = False
        dataset = {}
        attachments = []
        email_section_list = []
        mail = {}
        sections = {}
        status,message = imap_server.uid('FETCH',Message_uid,'(RFC822)')

        if status != 'OK':
            sys.stderr.write('Something went wrong. Please check Apache James log and IMAP server log files in log folder in apache James installation directory')
            return
        for response in message:
            if isinstance(response, tuple):
                isMailExits = True
                binary_email = email.message_from_bytes(response[1])
                
                #attachdoc = '' 
                # if the email message is multipart
                if binary_email.is_multipart():
                    # iterate over email parts
                    for part in binary_email.walk():
                        # extract content type of email
                        content_type = part.get_content_type()
                        content_disposition = str(part.get("Content-Disposition"))

                        if content_type == "text/html" and "attachment" not in content_disposition:
                            htmlbody = htmlbody + part.get_payload(decode=True).decode()

                        if content_type == "text/plain" and "attachment" not in content_disposition:
                            plainbody = plainbody + part.get_payload(decode=True).decode()

                        if content_type == "image/png" or content_type == "image/jpeg" and "attachment" not in content_disposition:
                            content_id = "cid:" + str(part.get("Content-Id")).strip("<").strip(">") 
                            embeded_image = part.get_payload()
                            htmlbody = " ".join(line.strip() for line in htmlbody.splitlines())
                            htmlbody =  htmlbody.replace(content_id,"data:image/png;base64,"+embeded_image)

                        if "attachment" in content_disposition:
                            # Get Attachment Info
                            attachment_info = {}
                            attachment_info["filename"] = part.get_filename()
                            attachment_info["filesize"] = len(part.get_payload(decode=True))
                            attachments.append(attachment_info)
                else:
                    htmlbody = binary_email.get_payload(decode=True).decode()
        json_string = ''
        if isMailExits:
            if htmlbody:
                soup = BeautifulSoup (htmlbody, 'html.parser')
                head_tag = soup.head
                if soup.html:
                    if not head_tag:
                        head_tag = soup.new_tag('head')
                        soup.html.insert(0, head_tag)
                    
                    meta_tag = soup.new_tag('meta')
                    meta_tag.attrs['http-equiv'] = 'X-UA-Compatible'
                    meta_tag.attrs['content'] = 'IE=11'  
                    htmlbody = soup.prettify()
                          
            body = htmlbody if htmlbody != '' else plainbody
            sections["messageID"] = message_uid
            encoded_bytes = base64.b64encode(body.encode('utf-8'))
            encoded_body = encoded_bytes.decode('utf-8')
            sections["body"] = encoded_body
            sections["attachment"] = attachments
            mail["Mail"] = sections
            json_string = json.dumps(mail)
        with open(output_file_name, "w", encoding="utf-8") as outfile:
            outfile.write(json_string)  


    except Exception as err:
        sys.stderr.write(str(err))

                

if __name__ == "__main__":
    imap_host_addr = sys.argv[1]
    imap_port_number = sys.argv[2]
    user_email_address = sys.argv[3]
    admin_email_address = sys.argv[4] 
    admin_password = sys.argv[5]
    message_uid = sys.argv[6]
    output_file_name = sys.argv[7]
    
    imap_server = imaplib.IMAP4(imap_host_addr, imap_port_number) 
    # Try to connect IMAP server and login to the mailbox
    try:
       authObj = lambda resp: "{0}\x00{1}\x00{2}".format(user_email_address,admin_email_address, admin_password).encode()
       imap_server.authenticate("PLAIN", authObj)
    except Exception as err:
        sys.stderr.write(str(err))
        exit()
    else:
        """If there is no error while connecting to IMAP server and login to mailbox then move furthure 
        to connect to the INBOX and fetch the email"""
        _get_email(message_uid, output_file_name)
        imap_server.logout()