import sys
import imaplib
import email
from email.header import decode_header

#Global variable to store IMAP instance
imap_server = None

def _download_attachment(Message_uid, output_file_name, attachment_name):
    try:
        attachment_payload = bytes()
        status, msgCount = imap_server.select(mailbox="INBOX", readonly=True)

        if status != 'OK':
            sys.stderr.write('Something went wrong. Please check Apache James log and IMAP server log files in log folder in apache James installation directory')
            return

        status,message = imap_server.uid('FETCH',Message_uid,'(RFC822)')

        if status != 'OK':
            sys.stderr.write('Something went wrong. Please check Apache James log and IMAP server log files in log folder in apache James installation directory')
            return

        for response in message:
            if isinstance(response, tuple):
                binary_email = email.message_from_bytes(response[1])
                
                # if the email message is multipart
                for part in binary_email.walk():
                    if part.get_content_maintype() == 'multipart':
                        continue
                    if part.get('Content-Disposition') is None:
                        continue
                    if part.get_content_disposition () != 'attachment':
                        continue
                    filename = part.get_filename()
                    if filename:
                        filename = decode_header(filename)[0][0]

                    if filename == attachment_name:
                        attachment_payload = part.get_payload(decode=True)

        with open(output_file_name, "wb") as attachment_stream:
            attachment_stream.write(attachment_payload)

    except Exception as err:
        sys.stderr.write(str(err))

                

if __name__ == "__main__":
    imap_host_addr = sys.argv[1]
    imap_port_number = sys.argv[2]
    user_email_address = sys.argv[3]
    admin_email_address = sys.argv[4] 
    admin_password = sys.argv[5]
    message_uid = sys.argv[6]
    output_file_name = sys.argv[7]
    attachment_name = sys.argv[8]
    
    imap_server = imaplib.IMAP4(imap_host_addr, imap_port_number) 
    # Try to connect IMAP server and login to the mailbox
    try:
       authObj = lambda resp: "{0}\x00{1}\x00{2}".format(user_email_address,admin_email_address, admin_password).encode()
       imap_server.authenticate("PLAIN", authObj)
    except Exception as err:
        sys.stderr.write(str(err))
        exit()
    else:
        """If there is no error while connecting to IMAP server and login to mailbox then move furthure 
        to connect to the INBOX and fetch the email"""
        _download_attachment(message_uid, output_file_name, attachment_name)
        imap_server.logout()