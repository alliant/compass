import os
import sys

pathtoscript = sys.argv[4]
def CreateMailBox ():
    try:
        os.chdir(pathtoscript)
        createMailBoxCmd = f'james-cli.bat -h {sys.argv[1]} adduser {sys.argv[2]} {sys.argv[3]}'
        os.system(createMailBoxCmd)
    except Exception as err:
        sys.stderr.write(str(err))

if __name__ == "__main__":
    CreateMailBox()

        