import email
import sys
from email.parser import HeaderParser
import imaplib
import json
from dateutil import parser

#Global variable to store IMAP instance
imap_server = None

def _get_email_list (output_file_name):
    headersArray = []
    try:
        status, msg = imap_server.select(mailbox="INBOX", readonly=True)

        if status != 'OK':
            sys.stderr.write('Something went wrong. Please check Apache James log and IMAP server log files in log folder in apache James installation directory')
            return

        # Loop through all the emails in the inbox
        for num in range(1, int(msg[0]) + 1):
            typ,message = imap_server.fetch(str(num), '(RFC822)')
            header = {}
            header["mailMessageUID"] = num
            for response in message:
                if isinstance(response, tuple):
                    emailMsg = email.message_from_bytes(response[1])
                    hearderParser = HeaderParser()
                    header["mailSent"] = parser.parse(emailMsg['date']).isoformat()
                    headers = hearderParser.parsestr(emailMsg.as_string())
                    header["mailActualFrom"] = headers.get_all(name="X-Actual-From")[0] if headers.get_all(name="X-Actual-From") != None else ""
                    header["mailActualTo"] = headers.get_all(name="X-Actual-To")[0] if headers.get_all(name="X-Actual-To") != None else ""
                    header["mailFrom"] = headers.get_all(name="From")[0]
                    header["mailSubject"] = headers.get_all(name="Subject")[0]
            headersArray.append(header)

        #Serializing headers Json
        jsonString = json.dumps(headersArray)
        with open(output_file_name, "w") as outfile:
            outfile.write(jsonString)

    except Exception as err:
        sys.stderr.write(str(err))

    

if __name__ == "__main__":
    imap_host_addr = sys.argv[1]
    imap_port_number = sys.argv[2]
    user_email_address = sys.argv[3]
    admin_email_address = sys.argv[4] 
    admin_password = sys.argv[5]
    output_file_name = sys.argv[6]
    
    imap_server = imaplib.IMAP4(imap_host_addr, imap_port_number) 
    # Try to connect IMAP server and login to the mailbox
    try:
       authObj = lambda resp: "{0}\x00{1}\x00{2}".format(user_email_address,admin_email_address, admin_password).encode()
       imap_server.authenticate("PLAIN", authObj)
    except Exception as err:
        sys.stderr.write(str(err))
        exit()
    else:
        """If there is no error while connecting to IMAP server and login to mailbox then move furthure 
        to connect to the INBOX and fetch the email list"""
        _get_email_list (output_file_name)
        imap_server.logout ()