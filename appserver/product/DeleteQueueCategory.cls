/*------------------------------------------------------------------------
@name  product/DeleteQueueCategory.cls
@action queueCategoryDelete
@description  Deletes a queuecategory record 
@param Category;character  
@throws 3001; Category is invalid  
@throws 3066; Queue does not exist
@throws 3000; Queue deletion failed
@throws 3005;Deleted in compass but failed to update in ARC         
@returns Success;2000; Queue deletion was successful.
@author Shefali
@Modified :
Date        Name          Comments   
01/12/2023  SB            Modifed to add the validation for isSecure field 
03/19/2024  SB            Modified to set isDeleted true and make it inactive 
                          instead of deleting queue from DB. 
   
----------------------------------------------------------------------*/

class product.DeleteQueueCategory inherits framework.ActionBase:

 constructor public DeleteQueueCategory ():
  super().
 end constructor.

 destructor public DeleteQueueCategory ( ):
 end destructor.
    

 {tt/queuecategory.i &tableAlias=ttQueuecategory}

 method public override void act (input pRequest as framework.IRequestable,
                                  input pResponse as framework.IRespondable):
   define variable plReturnRecord  as logical   no-undo.
   define variable pcCategory      as character no-undo.
   define variable lSuccess        as logical   no-undo.

   define variable lError          as logical   no-undo.
   define variable cErrMessage     as character no-undo.


   {lib/std-def.i}
    
   pRequest:getParameter("Category", output pcCategory).

    
   /* validation when no parameter is supplied */
   if (pcCategory = ''  or pcCategory = ?)
    then 
     do:
       pResponse:fault("3001", "Category").
       return.
     end.
  
   /* validate category */
   if not can-find(first queueCategory where queueCategory.category = pcCategory and queueCategory.isDeleted = no)
    then
     do:
       pResponse:fault("3066", "Queue").
       return.
     end.
   
   if can-find(first filetask where filetask.category = pcCategory)
    then
     do:
       pResponse:fault("3005", "Queue Category cannot be deleted as it is already in use").
       return.
     end.

   /* there must be isSecure no to delete the record */
   if can-find(first queueCategory where queueCategory.category = pcCategory and queueCategory.isSecure = true)
    then
     do:
       pResponse:fault("3005", "This is a secured queue category, cannot be deleted from TPS application.").
       return.
     end.
 
   lSuccess = false.

   TRX-BLOCK:
   for first queueCategory exclusive-lock
    where queueCategory.category  = pcCategory
      and queueCategory.isDeleted = no
     on error undo TRX-BLOCK, leave TRX-BLOCK:

    assign 
        queueCategory.isDeleted = yes
        queueCategory.active    = no
        .

    validate queueCategory.
    release queueCategory.
    
    lSuccess = true.

   end. /* for first queueCategory */

   if not lSuccess
    then 
     do:
       presponse:fault("3000", "Queue deletion").
       return.
     end. /* if not lSuccess */

   run product/updatecategorylist-p.p(output lError,
                                      output cErrMessage).

     
   if lError
    then 
     do:
       pResponse:fault("3000", "Deleted in Compass but failed to delete in ARC. " + cErrMessage).
       return.
     end.
 
   pResponse:success("2000", "Queue deletion").
 end method.
  
end class.

