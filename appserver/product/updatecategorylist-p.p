/*------------------------------------------------------------------------
@name  updatecategorylist-p.p
@description  Update category list in the ARC

@author   Shefali
@Modified : 04/26/2023
Date        Name          Comments  
03/19/2024  SB            Modified to handle isDeleted field.   
----------------------------------------------------------------------*/
define output parameter pError        as logical   no-undo.
define output parameter pErrMessage   as character no-undo.

/* Used for getting the ARC code */
define variable tDataFile       as character no-undo.
define variable pSuccess        as logical   no-undo.
define variable pMsg            as character no-undo.

define variable cCategoryList   as character no-undo.

/* Functions and Procedures */
{lib/arc-repository-procedures.i}
{lib/run-http.i}
{lib/arc-bearer-token.i}

pSuccess = false.
 
/* create the data file */
tDataFile = guid(generate-uuid).

for each queuecategory no-lock where queuecategory.active = true and queuecategory.isDeleted = false:
  if cCategoryList = "" 
   then 
     assign cCategoryList = "~"" + queuecategory.category + "~"".
  else 
     assign cCategoryList = cCategoryList + "," + "~"" + queuecategory.category + "~"" .
end.

output to value(tDataFile).
put unformatted "~{" skip.
put unformatted "  ~"Categories~": " + "[" + string(cCategoryList) + "]" skip.
put unformatted "~}" skip.
output close.

/* send to ARC */
publish "GetSystemParameter" ("ARCUpdateQueue", output tURL).

run HTTPPost ("queueCategory", tURL , CreateArcHeader(""), "application/json", search(tDataFile), output pSuccess, output pMsg, output tResponseFile).

if not pSuccess
 then do:
   assign pError = true 
          pErrMessage = if pMsg = "" then "Failed to connect to ARC" else pMsg.
   return.
 end.


