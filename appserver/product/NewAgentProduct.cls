/*------------------------------------------------------------------------
@name NewAgentProduct.cls
@action agentProductNew
@description  Create the new agent product record.
@param returnRecord;logical
@param agentproduct;dataset
@throws 3005;Data Parsing Failed
@throws 3066;Agent ID does not exist
@throws 3000;Agent Product creation failed
@returns Success;2002;AgentProduct has been created
@author Vignesh Rajan
@Modified :
Date        Name          Comments  
05/09/2023  Spandana     Added category field in AgentProduct table 
10/17/2023  Sagar K      Added Contact name,Email,Phone in Agentproduct table
03/18/2024  Sachin       Added validation that Product must be active.
08/19/2024  SRK          Modifed to return statename
----------------------------------------------------------------------*/

class product.NewAgentProduct inherits framework.ActionBase:
    
  {tt/agentProduct.i &tableAlias="ttAgentProduct"}
  {lib/removeNullValues.i}
          
  constructor NewAgentProduct ():
    super().    
  end constructor.

  destructor public NewAgentProduct ():        
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
          
    {lib/std-def.i} 
         
    /* Variables */
    define variable plReturnRecord as logical   no-undo.
    define variable iAgentID       as character no-undo.
    define variable iProductID     as integer   no-undo.
    define variable iCount         as integer   no-undo.
    define variable lsuccess       as logical   no-undo.
    
    define variable dsHandle  as handle no-undo.
    
    /* Dataset creation */
    create dataset dsHandle.  
    dsHandle:serialize-name = 'data'.      
    dsHandle:set-buffers(buffer ttAgentProduct:handle).   
    
    pRequest:getParameter('returnRecord', output plReturnRecord).

    if plReturnRecord = ?
     then
      plReturnRecord = no.
    
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch). 
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.       
      
    if not can-find(first ttAgentProduct) 
     then
      do:
        pResponse:fault('3005', 'Agent Product record not provided').
        return.
      end.

	  
    for first ttAgentProduct:

      if can-find(first product where product.productID = ttAgentProduct.productID and product.active = false )
       then
        do:
          pResponse:fault('3005', 'Product must be active').
          return.
        end.
    
      if ttAgentProduct.agentID = "" or 
         ttAgentProduct.agentID = ?  or
         ttAgentProduct.agentID = "?"
       then 
        do:
          pResponse:fault("3005", "Agent ID cannot be blank/Invalid value").
          return.
        end.
    
      if not can-find(first agent where agent.agentID = ttAgentProduct.agentID)
       then 
        do:
          pResponse:fault("3066", "AgentID " + ttAgentProduct.agentID).
          return.
        end.
    
      /* validate productID */
      if not can-find(first product where product.productID = ttAgentProduct.productID ) 
       then 
        do:
          pResponse:fault("3066", "Product").
          return.
        end.
    
      for first agent where agent.agentID = ttAgentProduct.agentID:
        for first product where product.productID = ttAgentProduct.productID:
         if agent.stateID <> product.stateID then
          do:
            pResponse:fault("3005", "Product is not of agent state").
            return.
          end.
        end.
      end.  
    
      if can-find(first agentProduct where agentProduct.agentID = ttAgentProduct.agentID and agentProduct.productid = ttAgentProduct.productid)
       then
        do:
          pResponse:fault("3005", "Agent Product already exist").
          return.
        end.
    
    end.       
    
    removeNullValues("ttAgentProduct").

    trxBlock:
    for first ttAgentProduct 
        transaction on error undo trxBlock, leave trxBlock:         
    
      create agentProduct.
      assign 
        iAgentID                    = ttAgentProduct.agentID
        iProductID                  = ttAgentProduct.productID
        agentProduct.agentID        = ttAgentProduct.agentID
        agentProduct.productID      = ttAgentProduct.productID
        agentProduct.price          = ttAgentProduct.price
        agentProduct.dueDays        = ttAgentProduct.dueDays
        agentProduct.contactName    = ttAgentProduct.contactName
        agentProduct.contactEmail   = ttAgentProduct.contactEmail
        agentProduct.contactPhone   = ttAgentProduct.contactPhone
        agentProduct.createdDate    = now
        agentProduct.createdBy      = pRequest:UID
        agentProduct.lastModifiedBy = "".

      validate agentProduct.
      release agentProduct.
      lsuccess = true.
    end.
    
    if pResponse:isFault()
     then
      return.
    
    if not lsuccess
     then
      do:
        pResponse:fault("3000", "Agent Product creation"). /* Agent Product creation failed */
        return.
      end.
    
    if plReturnRecord 
     then 
      do:
        empty temp-table ttAgentProduct.
        for first agentProduct where agentProduct.agentID   = iAgentID and
                                     agentProduct.productID = iProductID no-lock:
          create ttAgentProduct.
          assign
            ttAgentProduct.agentID      = agentProduct.agentID          
            ttAgentProduct.productID    = agentProduct.productID
            ttAgentProduct.price        = agentProduct.price
            ttAgentProduct.dueDays      = agentProduct.dueDays
            ttagentProduct.contactName  = AgentProduct.contactName
            ttagentProduct.contactEmail = AgentProduct.contactEmail
            ttagentProduct.contactPhone = AgentProduct.contactPhone
            .
   
    
          /*Get agent name and state name*/
          for first agent field (name stateID) no-lock where agent.agentID = ttAgentProduct.agentID:
            assign
                ttAgentProduct.agentName = agent.name
                ttAgentProduct.stateID   = agent.stateID.
            for first state field (description) no-lock where state.stateID = agent.stateID:
              ttAgentProduct.stateName  = state.description.
            end.
          end.
    
          /*Get product description*/
          for first product field (description) no-lock where product.productID = ttAgentProduct.productID:
            ttAgentProduct.productDesc = product.description.
          end.
        end.
    
        setContentDataSet(input dataset-handle dsHandle, input pRequest:FieldList).
      end.
      
    pResponse:success("2002", "Agent Product"). /* Agent Product has been created */        
                           
  end method.
  
end class.


