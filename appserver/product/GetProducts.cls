/*------------------------------------------------------------------------
@name  GetProducts.cls
@action productsGet
@description  Get the product Details.
@returns Success;2005;Product(s) were returned
@author S Chandu
@version 1.0
@created 07.14.2022
@Modified :
Date        Name          Comments   
4-4-2023    Sagar K       changed single stateId to multiple StateId
----------------------------------------------------------------------*/


class product.GetProducts inherits framework.ActionBase:
    
  {lib/callsp-defs.i}
  {tt/product.i &tableAlias="ttProduct"}
      
  constructor GetProducts ():
    super().
  end constructor.

  destructor public GetProducts ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
          
    {lib/std-def.i}
               
    define variable pcStateIDlist    as character no-undo.
    define variable plActiveOnly as logical   no-undo.
    define variable piProductID  as integer   no-undo. 
    
    define variable dsHandle     as handle    no-undo.
    
    create dataset dsHandle.  
    dsHandle:serialize-name = 'data'.      
    dsHandle:set-buffers(buffer ttProduct:handle). 
    
    pRequest:getParameter("StateIDlist",output pcStateIDlist).
    pRequest:getParameter("ActiveOnly", output plActiveOnly).
    pRequest:getParameter("ProductID",  output piProductID).
    
    if pcStateIDlist = "ALL" or pcStateIDlist = ? 
     then 
      pcStateIDlist = "".

    if piProductID = ? 
     then
      piProductID = 0.

    if plActiveOnly = ? 
     then
      plActiveOnly = no.
                              
    {lib\callsp.i &name=spGetProduct &load-into=ttProduct &params="input pcStateIDlist, input plActiveOnly, input piProductID" &noResponse=true}
    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end.    
    
    for each ttProduct:
      for first state no-lock where state.stateID = ttProduct.stateID:
        ttProduct.stateName = state.description.
      end.
    end.
    
    if can-find(first ttProduct)
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
      
    pResponse:success("2000", "Product").
       
  end method.
    
end class.


