/*------------------------------------------------------------------------
@name NewProduct.cls
@action productNew
@description  Creates a new product record.
@param ReturnOrder;logical  
@throws 3005;Data Parsing Failed 
@throws 3005;State of the Revenue Type does not match with the state of the product
@throws 3005;An active product with the same name already exists in state 
@throws 3000;creating new product failed
@throws 3066;state does not exist
@returns success;2002;product has been created.
@author S Chandu
@version 1.0
@created 07.14.2022
@Modified :
Date        Name          Comments  
08/17/22    Shefali       Modified to add validations for revenueType
05/01/24    Sachin        Removed product.revenuetype field
09/02/24    S Chandu      Modified to valid stateID field.
----------------------------------------------------------------------*/

class product.NewProduct inherits framework.ActionBase:
    
  {tt/product.i &tableAlias="ttProduct"}
  {lib/removeNullValues.i}
          
  constructor NewProduct ():
    super().    
  end constructor.

  destructor public NewProduct ( ):        
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
          
    {lib/std-def.i} 
    {lib/nextkey-def.i}
    
    define variable iProductID     as integer    no-undo.
    define variable lSuccess       as logical    no-undo. 
    define variable lReturnRecord  as logical    no-undo.
    
    define variable dsHandle as handle no-undo.

    create dataset  dsHandle. 
    dsHandle:serialize-name = 'data'.       
    dsHandle:set-buffers(buffer ttProduct:handle).
         
    pRequest:getParameter("returnRecord", output lReturnRecord).

    if lReturnRecord = ?
     then
      lReturnRecord = no.
    
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end. 
    
    for first ttProduct no-lock:
      if (ttProduct.effectiveDate <> ? and ttProduct.expiryDate <> ?) and 
         (ttProduct.effectiveDate > ttProduct.expiryDate)
       then
        do:
          pResponse:fault("3005", "Expiration Date cannot be prior to the Effective Date.").
          return.
        end.
                   
      /* validate state */
      if not can-find(first state where stateID = ttProduct.stateID)
       then
        do:
          pResponse:fault("3066", "State " + ttProduct.stateID).
          return.
        end.
           
    end.    
            
    lSuccess = false.

    removeNullValues("ttProduct").
    
    trxBlock:
    do transaction
      on error undo trxBlock, leave trxBlock:
    
      for first ttProduct: 
         
         /* productname is valid or not */
        if ttProduct.description = "" or
           ttProduct.description = ?
         then
          do:  
            pResponse:fault("3005", "Product cannot be blank").
            return.
          end. 
          
        if can-find(first product where product.stateID      = ttProduct.stateID     and
                                        product.description  = ttProduct.description and
                                        (product.active and product.active = ttProduct.active)) 
         then
          do:
            pResponse:fault("3005","An active product with the same name already exists in state " + ttProduct.stateID). 
            return.
          end.
          
        {lib/nextkey.i &type='productid' &var=iProductID &err="undo trxBlock, leave trxBlock."}
          
        create product.
        assign
          product.productID        = iProductID  
          product.stateID          = ttProduct.stateID
          product.description      = ttProduct.description
          product.price            = ttProduct.price
          product.effectiveDate    = ttProduct.effectiveDate
          product.expiryDate       = ttProduct.expiryDate
          product.active           = ttProduct.active
          product.CreatedDate      = now  
          product.CreatedBy        = pRequest:uid
          product.lastModifiedBy   = ""
          .      
    
        validate product. 
        release  product.  
      end.  
      lSuccess = true.
    end.
    
    if pResponse:isFault()
     then
      return.        
    
    if not lSuccess
     then
      do:
        pResponse:fault("3000", "Create").
        return.
      end.  
      
    if lReturnRecord
     then
      do:
        for first product no-lock
         where Product.productID = iProductID:
          empty temp-table ttProduct.
          create ttProduct.
          assign
            ttProduct.productID      = product.productID
            ttProduct.stateID        = product.stateID
            ttProduct.description    = product.description
            ttProduct.price          = product.price
            ttProduct.effectiveDate  = product.effectiveDate
            ttProduct.expiryDate     = product.expiryDate
            ttProduct.active         = product.active
            .
            
          for first state no-lock where state.stateID = product.stateID:
            ttProduct.stateName = state.description.  
          end.
          
        end.
    
        setContentDataSet(input dataset-handle dsHandle, input pRequest:FieldList).
      end.
    
    pResponse:success("2002","Product").
                           
  end method.

end class.


