/*------------------------------------------------------------------------
@name ModifyProduct.cls
@action productModify
@description  Modify the existing product record for productID.
@param productID;int
@throws 3001;Invalid productID
@throws 3066;Product does not exist
@throws 3005;State of the Revenue Type does not match with the state of the product
@throws 3005;An active product with the same name already exists in state
@throws 3000;Product modify failed 
@returns Success;2000
@author Shefali
@Modified :
Date        Name          Comments   
08/17/22    Shefali       Modified to add validations for revenueType 
05/01/24    Sachin        Removed product.revenuetype field 
----------------------------------------------------------------------*/

class product.ModifyProduct inherits framework.ActionBase:
    
  {tt/product.i &tableAlias="ttProduct"}
  {lib/removeNullValues.i}
      
  constructor ModifyProduct ():
    super().
  end constructor.

  destructor public ModifyProduct ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
          
    {lib/std-def.i}

    /* variable */
    define variable lSuccess        as logical   no-undo.
    define variable piProductID     as integer   no-undo.
    define variable plReturnRecord  as logical   no-undo init false.  
    
    /* dataset handle */
    define variable dsHandle        as handle no-undo.
    
    /* buffer */
    define buffer bProduct for product.
 
    /* parameter get */
    pRequest:getParameter("productID",    output piProductID).
    pRequest:getParameter("returnRecord", output plReturnRecord).

    /* dataset creation */
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttProduct:handle). 

    if plReturnRecord = ?
     then
      plReturnRecord = no.

    /*dataset get*/
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.
    
    /* validation when no parameter is supplied */
    if (piProductID = 0  or piProductID = ?)
     then 
      do:
        pResponse:fault("3001", "productID").
        return.
      end.
    
    /* validate productID */
    if not can-find(first product where product.productID = piProductID ) 
     then 
      do:
        pResponse:fault("3066", "Product").
        return.
      end.
    
    for first ttProduct no-lock:
      if (ttProduct.effectiveDate <> ? and ttProduct.expiryDate <> ?) and 
         (ttProduct.effectiveDate > ttProduct.expiryDate)
       then
        do:
          pResponse:fault("3005", "Expiration Date cannot be prior to the Effective Date.").
          return.
        end.
    end.

    removeNullValues("ttProduct").
    
    trxBlock:
    for first product exclusive-lock
      where product.productID = piProductID transaction
      on error undo trxBlock, leave trxBlock:
    
      /* updating product record*/
      for first ttProduct no-lock
        where ttProduct.productID = product.productID: 
    
    
        if can-find(first bProduct where bProduct.stateID      = ttProduct.stateID     and
                                         bProduct.description  = ttProduct.description and
                                         (bProduct.active and bProduct.active = ttProduct.active) and
                                         rowID(bProduct) <> rowID(product)) 
         then
          do:
            pResponse:fault("3005","An active product with the same name already exists in state " + ttProduct.stateID). 
            return.
          end.

        assign
          product.stateID          = ttProduct.stateID
          product.description      = ttProduct.description
          product.price            = ttProduct.price
          product.effectiveDate    = ttProduct.effectiveDate
          product.expiryDate       = ttProduct.expiryDate
          product.active           = ttProduct.active
          product.lastModifiedDate = now
          product.lastModifiedBy   = pRequest:uid
          .                           
      end. /* for first ttProduct */
      
      validate product.
      release product.
       
      lSuccess = true.
    
    end. /* for first product*/
    
    if plReturnRecord 
     then
      do:
        for first product no-lock
          where product.productID = piProductID:
          empty temp-table ttProduct.
          create ttProduct.
          assign
            ttProduct.productID     = product.productID
            ttProduct.stateID       = product.stateID
            ttProduct.description   = product.description
            ttProduct.price         = product.price
            ttProduct.effectiveDate = product.effectiveDate
            ttProduct.expiryDate    = product.expiryDate
            ttProduct.active        = product.active
            .
            
          for first state no-lock where state.stateID = product.stateID:
            ttProduct.stateName = state.description.  
          end.
          
        end.
      end.
    
    if not lSuccess
     then 
      do:
        presponse:fault("3000", "Product update").
        return.
      end. /* if not lSuccess */
    
    if plReturnRecord
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
    
    pResponse:success("2000", "Product update").
            
  end method.
        
end class.


