/*------------------------------------------------------------------------
@name  GetVendors.cls
@action apVendorsGet
@description  Get vendors.
@returns Success;2005;Vendors(s) were returned
@author Kasun Dhananjaya
@version 1.0
@created 12.01.2023
@Modified :
Date             Name          Comments   
07/12/2023        K.R           Renamed the file and changed stored procedure
----------------------------------------------------------------------*/

class product.GetVendors inherits framework.ActionBase:

  constructor public GetVendors ():
   super().
  end constructor.

  destructor public GetVendors ():

  end destructor.

  {lib/callsp-defs.i}
  {tt/apVendor.i &tableAlias="ttapvendor"}

  method public override void act (input pRequest as framework.IRequestable,
                                  input pResponse as framework.IRespondable):

    define variable dsHandle as handle    no-undo.
    define variable pcStatus  as character no-undo.

    {lib/std-def.i}

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttapvendor:handle).

    pRequest:getParameter("Status", output pcStatus).

    for first sysuser fields (uid vendorid) where sysuser.uid = pRequest:UID no-lock:
        if sysuser.vendorid = "" or sysuser.vendorid = ? then
         do:
           {lib\callsp.i &name=spGetVendors &load-into=ttapvendor &params="input '', input pcStatus" &noResponse=true}

           if not csp-lSucess and csp-cErr > ''
            then
             do:
               pResponse:fault("3005", csp-cErr).
               return.
            end.

         end.
        else
         for first vendor where vendor.vendorid = sysuser.vendorid no-lock:
           create ttapvendor.
           assign
             ttapvendor.vendorid   = vendor.vendorid
             ttapvendor.category   = vendor.category
             ttapvendor.vendorname = vendor.vendorname
             ttapvendor.active     = vendor.active
             .
         end.
    end.
      
    if can-find(first ttapvendor) 
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2005", string(csp-icount) {&msg-add} "Vendors").
  end method.
end class.
