/*------------------------------------------------------------------------
@name DeleteAgentProduct.cls
@action agentProductDelete
@description  Delete the agent product record.
@param returnRecord;logical
@param agentproduct;dataset
@throws 3005;XML Parsing Failed
@throws 3066;AgentID does not exist
@throws 3000;Delete Agent Product failed
@returns Success;2000;
@author Vignesh Rajan
@Modified :
Date        Name          Comments  
----------------------------------------------------------------------*/

class product.DeleteAgentProduct inherits framework.ActionBase:
    
  {tt/agentProduct.i &tableAlias="ttAgentProduct"}
      
  constructor DeleteAgentProduct ():
    super().
  end constructor.

  destructor public DeleteAgentProduct ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
          
    {lib/std-def.i}
               
    define variable pcAgentID   as character no-undo.
    define variable piProductID as integer   no-undo.

    pRequest:getParameter("AgentID",   output pcAgentID).
    pRequest:getParameter("ProductID", output piProductID).
    
    if pcAgentID = ""  or pcAgentID = "?" or pcAgentID = ? 
     then
      do:
        pResponse:fault("3005", "AgentID cannot be blank/Invalid value").
        return.
      end.

    if piProductId = 0 or piProductId = ? 
     then 
      do:
        pResponse:fault("3005", "ProductID cannot be Zero/Invalid value").
        return.
      end.

    if not can-find(first agentProduct where agentProduct.productID = piProductID and agentProduct.agentID   = pcAgentID) 
     then
      do:
        pResponse:fault('3066', "AgentID " + pcAgentID + " ProductID " + string(piProductID)).
        return.
      end.
      
      
    trxBlock:
    for first agentProduct exclusive-lock
      where agentProduct.productID = piProductID
        and agentProduct.agentID   = pcAgentID
        transaction on error undo trxBlock, leave trxBlock:

        delete agentProduct.

        release agentProduct.

        std-lo = true.
    end.
    
    if not std-lo
     then
      do:
        pResponse:fault("3000", "Delete Agent Product").
        return.
      end.
    
    pResponse:success("2000", "Delete Agent Product").
    
  end method.
    
end class.


