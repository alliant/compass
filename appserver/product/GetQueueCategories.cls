/*------------------------------------------------------------------------
@name  product/GetQueueCategories.cls
@action queueCategoriesGet
@description  Get the Queue Category Details.
@returns Success;2005;QueueCategory(s) were returned

@author Shefali
@Modified :
Date        Name          Comments    
   
----------------------------------------------------------------------*/ 

class product.GetQueueCategories inherits framework.ActionBase:

 constructor public GetQueueCategories ():
  super().
 end constructor.

 destructor public GetQueueCategories ( ):
 end destructor.
    

 {tt/queuecategory.i &tableAlias=ttQueueCategory}
 {lib/callsp-defs.i}

 method public override void act (input pRequest as framework.IRequestable,
                                  input pResponse as framework.IRespondable):
    define variable pcQueueCategory as character no-undo.
    define variable pcActive        as character no-undo.
    define variable dsHandle        as handle.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttQueueCategory:handle). 

    {lib/std-def.i}
    
    pRequest:getParameter("Category", output pcQueueCategory).
    pRequest:getParameter("Active", output pcActive).

    if pcQueueCategory = "ALL" or pcQueueCategory = ?
     then
      pcQueueCategory = "".
    
    {lib\callsp.i 
        &name=spGetQueueCategories 
        &load-into=ttQueueCategory 
        &params="input pcQueueCategory, input pcActive"
        &noresponse=true}
        
    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end.  

    if can-find(first ttQueueCategory)
     then
      setContentDataset(input dataset-handle dsHandle, pRequest:FieldLIst).
    
    pResponse:success("2005", string(csp-icount) {&msg-add} "QueueCategory").
 end method.
  
end class.

