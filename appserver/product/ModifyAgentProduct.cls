/*------------------------------------------------------------------------
@name ModifyAgentProduct.cls
@action agentProductModify
@description  Modify the agent product record.
@param returnRecord;logical
@param agentproduct;dataset
@throws 3005;Data Parsing Failed
@throws 3066;AgentID does not exist
@throws 3000;Modify Agent Product failed
@returns Success;2000;
@author Vignesh Rajan
@Modified :
Date        Name          Comments   
05/09/23    S Chandu      Modified to use category field
10/17/2023  Sagar K       Added Contact name,Email,Phone in Agentproduct table
08/19/2024  SRK           Modifed to return statename
----------------------------------------------------------------------*/

class product.ModifyAgentProduct inherits framework.ActionBase:
  
  {tt/agentProduct.i &tableAlias="ttAgentProduct"}
  {lib/removeNullValues.i}

  constructor ModifyAgentProduct ():
    super().
  end constructor.

  destructor public ModifyAgentProduct ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
        
    {lib/std-def.i}
    
    /* Variables */
    define variable pcAgentID      as character no-undo.
    define variable piProductId    as integer   no-undo.
    define variable plReturnRecord as logical   no-undo.
    define variable iCount         as integer   no-undo.
    
    define variable dsHandle as handle no-undo.        
               
    /* dataset creation */
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttAgentProduct:handle). 
    
    pRequest:getParameter('returnRecord', output plReturnRecord).
    pRequest:getParameter('agentID',      output pcAgentID).
    pRequest:getParameter('productID',    output piProductId).

    if plReturnRecord = ?
     then
      plReturnRecord = no.
    
    /* get Dataset */
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end. 
    
    if pcAgentID = "" or 
       pcAgentID = ?  or
       pcAgentID = "?" 
     then 
      do:
        pResponse:fault("3005", "AgentID cannot be blank/Invalid value").
        return.
      end.
    
    if piProductId = 0 or 
       piProductId = ?
     then 
      do:
        pResponse:fault("3005", "ProductID cannot be Zero/Invalid value").
        return.
      end.
    
    if not can-find(first agentProduct where agentProduct.agentID   = pcAgentID 
                                         and agentProduct.productID = piProductID) 
     then
      do:
        pResponse:fault('3005', 'Agent Product record not provided').
        return.
      end.
    
    /* validate agentID */
    if not can-find(first agent where agent.agentID = pcAgentID)
     then 
      do:
        pResponse:fault("3066", "AgentID " + pcAgentID).
        return.
      end.
    
    /* validate productID */
    if not can-find(first product where product.productID = piProductId ) 
     then 
      do:
        pResponse:fault("3066", "Product").
        return.
      end.
    
    for first agent where agent.agentID = pcAgentID:
      for first product where product.productID = piProductId:
       if agent.stateID <> product.stateID then
        do:
          pResponse:fault("3005", "Product is not of agent state").
          return.
        end.
      end.
    end.
    
    if not can-find(first agentProduct where agentProduct.productID = piProductID
                                         and agentProduct.agentID   = pcAgentID) 
     then
      do:
        pResponse:fault('3066', "Agent ID " + pcAgentID + " Product ID " + string(piProductID)).
        return.
      end.
    
    removeNullValues("ttAgentProduct").

    trxBlock:
    for first agentProduct exclusive-lock
      where agentProduct.productID = piProductID
        and agentProduct.agentID   = pcAgentID
      transaction on error undo trxBlock, leave trxBlock:
    
      for first ttAgentProduct 
          where ttAgentProduct.agentID   = agentProduct.agentID
            and ttAgentProduct.productID = agentProduct.productID:
    
        assign
          agentProduct.price            = ttAgentProduct.price
          agentProduct.dueDays          = ttAgentProduct.dueDays
          agentProduct.lastModifiedDate = now
          agentProduct.lastModifiedBy   = pRequest:UID 
          agentProduct.contactName      = ttAgentProduct.contactName
          agentProduct.contactEmail     = ttAgentProduct.contactEmail
          agentProduct.contactPhone     = ttAgentProduct.contactPhone
          .
      end.
    
      validate agentProduct.
      release agentProduct.
      
      std-lo = true.
    
    end.
    
    
    if not std-lo 
     then
      do:
        pResponse:fault("3000", "Modify Agent Product"). /* Modify Agent Product failed */
        return.
      end.
    
    if plReturnRecord 
     then 
      do:
        empty temp-table ttAgentProduct.
    
        for first agentProduct 
          where agentProduct.agentID   = pcAgentID 
            and agentProduct.productID = piProductID no-lock:

          create ttAgentProduct.
          assign
            ttAgentProduct.agentID      = agentProduct.agentID          
            ttAgentProduct.productID    = agentProduct.productID
            ttAgentProduct.price        = agentProduct.price
            ttAgentProduct.dueDays      = agentProduct.dueDays
            ttagentProduct.contactName  = AgentProduct.contactName
            ttagentProduct.contactEmail = AgentProduct.contactEmail
            ttagentProduct.contactPhone = AgentProduct.contactPhone
            .
    
          /*Get agent name and state name*/
          for first agent field (name stateID) no-lock where agent.agentID = ttAgentProduct.agentID:
            assign
                ttAgentProduct.agentName = agent.name
                ttAgentProduct.stateID   = agent.stateID.
            for first state field (description) no-lock where state.stateID = agent.stateID:
              ttAgentProduct.stateName  = state.description.
            end.
          end.
    
          /*Get product description*/
          for first product field (description) no-lock where product.productID = ttAgentProduct.productID:
            ttAgentProduct.productDesc = product.description.
          end.

        end.
    
        setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
      end.  
    
    pResponse:success("2000", "Modify Agent Product"). /* Modify Agent Product was successful. */
            
  end method.
    
end class.


