/*------------------------------------------------------------------------
@name  product/NewQueueCategory.cls
@action queueCategoryNew
@description  Creates new queuecategory record in queuecategory table 
@param ReturnOrder;logical  
@throws 3005;Data Parsing Failed
@throws 3005;Queue category cannot be blank
@throws 3005;Queue Category with same name already exists. 
@throws 3005;Created in compass but failed to create in ARC 
@throws 3000;Queue Category creation failed            
@returns Success;2000; queueCategory Record creation was successful.
@author Shefali
@Modified :
Date        Name          Comments   
01/12/2024  SB            Modified to add validation for isSecure field. 
03/19/2024  SB            Modified to handle isDeleted field. 
    
----------------------------------------------------------------------*/

class product.NewQueueCategory inherits framework.ActionBase:

  constructor public NewQueueCategory ():
    super().
  end constructor.

  destructor public NewQueueCategory ( ):
  end destructor.
    

  {tt/queuecategory.i &tableAlias="ttQueueCategory"}
  {lib/removeNullValues.i}

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    define variable pcCategory     as character no-undo.
    define variable plReturnRecord as logical   no-undo.
    define variable lSuccess       as logical   no-undo.

    define variable lError         as logical   no-undo.
    define variable cErrMessage    as character no-undo.
    
    /*dataset handle */
    define variable dsHandle as handle.
    
    /* parameter get */
    pRequest:getParameter("ReturnRecord",plReturnRecord).

    if plReturnRecord = ?
     then
      plReturnRecord = false.
    
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttQueueCategory:handle). 

    {lib/std-def.i}

    /* get Dataset */
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.

    for first ttQueueCategory no-lock:

      if ttQueueCategory.category =  "" or 
         ttQueueCategory.category =  ?  
       then 
        do:
          pResponse:fault("3005", "Queue category cannot be blank").
          return.
        end.

      /* validate category so that same category should not be created */
      if can-find(first queuecategory where (queuecategory.category = ttQueueCategory.category
         or replace(queuecategory.category," ","") = replace(ttQueueCategory.category," ",""))
          and queueCategory.isDeleted = no)
       then
        do:
          pResponse:fault("3005", "Queue Category with same name already exists.").
          return.
        end.

      if ttQueueCategory.isSecure = yes 
       then
        do:  
          pResponse:fault("3005","Secured queue category cannot be created from TPS application. Please contact System Administrator."). 
          return.
        end.
    end.
      
    lSuccess = false.

    removeNullValues("ttQueueCategory").
    
    TRX-BLOCK:
    for first ttQueueCategory no-lock transaction
     on error undo TRX-BLOCK, leave TRX-BLOCK:

      if not can-find(first queuecategory where queuecategory.category = ttQueueCategory.category)
       then
        do:
          create queueCategory.
          assign 
              queueCategory.category       = ttQueueCategory.category
              queueCategory.description    = ttQueueCategory.description
              queueCategory.active         = ttQueueCategory.active
              queueCategory.isSecure       = no
              queueCategory.createdDate    = now
              queueCategory.createdBy      = pRequest:uid
              queueCategory.isDeleted      = no
              pcCategory                   = ttQueueCategory.category 
             .

          validate queueCategory.
          release  queueCategory.
        end.
      else 
       for first queuecategory where queuecategory.category  = ttQueueCategory.category
                                 and queueCategory.isDeleted = yes exclusive-lock:
         assign 
             queueCategory.isDeleted    = no
             queueCategory.active       = ttQueueCategory.active
             queueCategory.description  = ttQueueCategory.description
             pcCategory                 = ttQueueCategory.category
             .
	     
         validate queueCategory.
         release queueCategory.
       end.
      lSuccess = true.
    end.

    if not lSuccess then 
     do:
       presponse:fault("3000", "Queue Category creation").
       return.
     end.

    run product/updatecategorylist-p.p(output lError,
                                       output cErrMessage).

     
    if lError
     then 
      do:
        pResponse:fault("3005", "Created in Compass but failed to create in ARC. " + cErrMessage).
        return.
      end.

    if plReturnRecord 
     then
      do:   
        for first queueCategory no-lock
            where queueCategory.category = pcCategory:

          empty temp-table ttQueueCategory.
          create ttQueueCategory.
          assign
              ttQueueCategory.category    = queueCategory.category
              ttQueueCategory.description = queueCategory.description
              ttQueueCategory.active      = queueCategory.active
              ttQueueCategory.isSecure    = queueCategory.isSecure
              .
        end.
        setContentDataset(input dataset-handle dsHandle, pRequest:FieldLIst).
      end.

    pResponse:success("2000", "Queue Record creation").
 
  end method.
 
end class.

