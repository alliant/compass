/*------------------------------------------------------------------------
@name  GetAgentProducts.cls
@action agentProductsGet
@param agentproduct;dataset
@param pcAgentID,piProductID
@description  Get the Agent Product Details.
@returns Success;2000;Agent Product
@author Vignesh Rajan
@Modified :
Date        Name          Comments   
12-02-2022  SR           Added new parameter productid to stored procedure.
05-09-2023  Spandana     changed to get few fields data from stored procedure
----------------------------------------------------------------------*/

class product.GetAgentProducts inherits framework.ActionBase:
    
  {lib/callsp-defs.i}
  {tt/agentProduct.i &tableAlias="ttAgentProduct"}
      
  constructor GetAgentProducts ():
    super().
  end constructor.

  destructor public GetAgentProducts ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
          
    {lib/std-def.i}
               
    define variable pcAgentID   as character no-undo.
    define variable piProductID as integer   no-undo.
    define variable iCount      as integer   no-undo.
    
    define variable dsHandle    as handle    no-undo.
    
    create dataset dsHandle.  
    dsHandle:serialize-name = 'data'.      
    dsHandle:set-buffers(buffer ttAgentProduct:handle). 
    
    pRequest:getParameter("AgentID",   output pcAgentID).
    pRequest:getParameter("ProductID", output piProductID).       
    
    if pcAgentID = "ALL" or pcAgentID = ? 
     then 
      pcAgentID = "".

    if piProductID = ? 
     then
      piProductID = 0.
                              
    {lib\callsp.i &name=spGetAgentProduct &load-into=ttAgentProduct &params="input pcAgentID, input piProductID" &noCount=true &noResponse=true}

    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end. 
                      
    for each ttAgentProduct:

      do iCount = 1 to num-entries(ttAgentProduct.notUid,","):
        for first sysUser field (name) no-lock where sysUser.uid = entry(iCount,ttAgentProduct.notUid):
          if ttAgentProduct.notUserName = "" 
           then
            ttAgentProduct.notUserName  = sysUser.name.
           else
            ttAgentProduct.notUserName  = ttAgentProduct.notUserName + "," + sysUser.name.
        end.
      end.
     
    end.
    
    if can-find(first ttAgentProduct)
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
      
    pResponse:success("2000", "Agent Product").
    
  end method.
    
end class.


