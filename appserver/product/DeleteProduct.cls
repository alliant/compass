/*------------------------------------------------------------------------
@name DeleteProduct.cls
@action productDelete
@description  Deletes product record form product table.
@param productID;int
@throws 3066;Product does not exist
@throws 3005;The Product cannot be deleted as it is in use
@throws 3000;delete product failed
@returns Success;2000
@author S Chandu
@version 1.0
@created 07.18.2022
@Modified :
Date        Name          Comments  
08/17/22    Shefali       Modified to add validations if product is use then
                          don't let it to delete.
----------------------------------------------------------------------*/

class product.DeleteProduct inherits framework.ActionBase:
    
  {tt/product.i &tableAlias="ttProduct"}
      
  constructor DeleteProduct ():
    super().
  end constructor.

  destructor public DeleteProduct ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
          
    {lib/std-def.i}
    
    /*parameter variables*/
    define variable piProductID as integer no-undo.
                
    pRequest:getParameter("productID", output piProductID).

    /* validation when no parameter is supplied */
    if (piProductID = 0  or piProductID = ?)
     then 
      do:
        pResponse:fault("3001", "ProductID").
        return.
      end.

    /* validate productID */
    if not can-find(first product where productID = piProductID)
     then
      do: 
        pResponse:fault("3066", "Product record").
        return.
      end.

    if can-find(first job where productID = piProductID)
     then
      do: 
        pResponse:fault("3005", "The Product cannot be deleted as it is in use").
        return.
      end.          
      
    std-lo = false.
    
    trxBlock:
    for first product exclusive-lock
      where product.productID = piProductID transaction
      on error undo trxBlock, leave trxBlock:
      
      delete product.
      release product.
      
      std-lo = true.
    end.
    
    if not std-lo
     then
      do: 
        pResponse:fault("3000", "Delete Product").
        return.
      end.
    
    pResponse:success("2000", "Delete Product").
    
  end method.
      
end class.


