/*------------------------------------------------------------------------
@name  product/ModifyQueueCategory.cls
@action queueCategoryModify
@description  Modify the existing queuecategory record for category.
@param category;character
@throws 3001;Invalid category
@throws 3066;Queue Category does not exist
@throws 3005;Updated in compass but failed to update in ARC
@throws 3000;Queue update failed 
@returns Success;2000;Queue updation was successful.
@author Shefali
@Modified :
Date        Name          Comments   
01/12/2024  SB            Modified to add validation for isSecure field.  
03/19/2024  SB            Modified to handle isDeleted field.  
   
----------------------------------------------------------------------*/

class product.ModifyQueueCategory inherits framework.ActionBase:

  constructor public ModifyQueueCategory ():
    super().
  end constructor.

  destructor public ModifyQueueCategory ( ):
  end destructor.
    

  {tt/queuecategory.i &tableAlias=ttQueueCategory}
  {lib/removeNullvalues.i}

  method public override void act (input pRequest as framework.IRequestable,
                                  input pResponse as framework.IRespondable):

    define variable pcQueueCategory as character no-undo.
    define variable lSuccess        as logical   no-undo.
    define variable plReturnRecord  as logical   init false no-undo.

    define variable lError          as logical   no-undo.
    define variable cErrMessage     as character no-undo.
    
    define variable dsHandle        as handle.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttQueueCategory:handle). 

    {lib/std-def.i}
    
    pRequest:getParameter("Category", output pcQueueCategory).
    pRequest:getParameter("ReturnRecord", output plReturnRecord).

    if plReturnRecord = ?
     then
      plReturnRecord = false.

    /* get Dataset */
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.

    /* validation when no parameter is supplied */
    if (pcQueueCategory = ''  or pcQueueCategory= ?)
     then 
      do:
        pResponse:fault("3001", "Category").
        return.
      end.

    /* validate queueCategory */
    if not can-find(first queueCategory where queueCategory.category = pcQueueCategory )
     then
      do:
        pResponse:fault("3066", "Queue category").
        return.
      end.

    if can-find(first queueCategory where queueCategory.category = pcQueueCategory and queueCategory.isDeleted = yes) 
     then
      do:
        pResponse:fault("3005", "This queue is already marked as deleted. Cannot be modified.").
        return.
      end.

    removeNullValues("ttQueueCategory").

    TRX-BLOCK:
    do transaction
      on error undo TRX-BLOCK, leave TRX-BLOCK:

      for first queueCategory where queueCategory.category = pcqueueCategory exclusive-lock: 
        for first ttQueueCategory no-lock where ttQueueCategory.category = queueCategory.category:

         if queueCategory.isSecure = yes then
          do: 
            pResponse:fault("3005","This is a secured queue category, cannot be edited from TPS application."). 
            return.
          end.
         else
          do:
            if ttQueueCategory.isSecure = yes then
             do:
               pResponse:fault("3005","Secured queue category, cannot be edited from TPS application." + chr(13) + 
                                 "Please contact System Administrator."). 
               return.
             end.
          end.

         assign
             queueCategory.description      = ttQueueCategory.description
             queueCategory.active           = ttQueueCategory.active
             queueCategory.isSecure         = ttQueueCategory.isSecure
             queueCategory.lastModifiedDate = now
             queueCategory.lastModifiedBy   = pRequest:uid
             queueCategory.isDeleted        = no
             .
        end. /* for first ttQueueCategory */

        validate queueCategory.
        release queueCategory.

        lSuccess = true.
      end. /* for first queueCategory */
    end.

    if not lSuccess
     then 
     do:
       presponse:fault("3000", "Queue update").
       return.
     end. /* if not lSuccess */

    run product/updatecategorylist-p.p(output lError,
                                       output cErrMessage).

     
    if lError 
     then 
      do:
        pResponse:fault("3005", "Updated in Compass but failed to update in ARC. " + cErrMessage).
        return.
      end.

    if plReturnRecord 
     then
      do:
        for first queueCategory no-lock
         where queueCategory.category = pcqueueCategory:

         empty temp-table ttQueueCategory.
         create ttQueueCategory.
         assign
              ttQueueCategory.category    = queueCategory.category
              ttQueueCategory.description = queueCategory.description
              ttQueueCategory.active      = queueCategory.active
              ttQueueCategory.isSecure    = queueCategory.isSecure
              .
        end.
        setContentDataset(input dataset-handle dsHandle, pRequest:FieldLIst).
      end.
    
    pResponse:success("2000", "Queue updation").
    
  end method.

  
END CLASS.

