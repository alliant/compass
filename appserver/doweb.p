&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* doweb.p
   single entry point to run a system action via the Webspeed Agent
   Modified:
   Name          Date          Note
   SB            08/25/2023    Calling distribute method in all the cases for distribution option
   K.R           09/12/2023    Modified to improve the http call performance
   K.R           02/07/2024    Modified to support multiple dots '.' in content file
   K.R           04/12/2024    Modified to support zip and rar formats
   K.R           09/25/2024    Modified to add content-dispostition header for json and xml type of response
   K.R           12/09/2024    Modified to implement async function 
 */

{lib/webdefs.i}
{lib/encrypt.i}

{framework/do-def.i &request="framework.RequestWeb"}
{framework/do-emailalert.i}
{framework/do-mqevents.i}
{framework/do-usernotifications.i}
{framework/do-syslog.i}
{framework/do-debug.i}

def stream infile.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-act) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD act Procedure 
FUNCTION act RETURNS LOGICAL PRIVATE
  ( )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-async) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD async Procedure 
FUNCTION async RETURNS logical PRIVATE
  ( )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-distExist) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD distExist Procedure 
FUNCTION distExist RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-distribute) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD distribute Procedure 
FUNCTION distribute RETURNS LOGICAL PRIVATE
  ( )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-init) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD init Procedure 
FUNCTION init RETURNS LOGICAL PRIVATE
  (  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-queue) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD queue Procedure 
FUNCTION queue RETURNS LOGICAL
  ( )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-render) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD render Procedure 
FUNCTION render RETURNS LOGICAL PRIVATE
  (  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-setMimeType) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setMimeType Procedure 
FUNCTION setMimeType RETURNS LOGICAL PRIVATE
  ( input filetype as char)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-writeToWeb) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD writeToWeb Procedure 
FUNCTION writeToWeb RETURNS LOGICAL PRIVATE
  ( input contentFile as char)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 23.71
         WIDTH              = 89.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


{framework/do-main.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-act) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION act Procedure 
FUNCTION act RETURNS LOGICAL PRIVATE
  ( ) :

      def var tHandlerError as logical no-undo init true.
      def var lExecuted as logical no-undo init false.
      def var cLogFaultMsg as char no-undo init ''. 
      
      etime(TRUE).
      RUN-BLOCK:
      DO on ERROR undo RUN-BLOCK, leave RUN-BLOCK 
         on STOP undo RUN-BLOCK, leave RUN-BLOCK
         on QUIT undo RUN-BLOCK, leave RUN-BLOCK:
       
          iAction = dynamic-new iActionHandler () no-error.
          
          tHandlerError = error-status:error.
          if not tHandlerError
           then 
            do: iAction:act(iRequest, iResponse).
                lExecuted = true.
            end.
      END.

      /* If there is invalid value in actionHandler (progexec) or queueHandler (queueExec),
         Progress raises the compiler error condition. After adding all the compilar error 
         in response object, clear the error status so that webutill.p does not add another 
         error message in the response XML. 
       */         
      if (tHandlerError OR not lExecuted OR compiler:error) 
       then 
        do: 
            assign
               iFaultMessage = "Unable to execute action. Please contact the system administrator.".
               cLogFaultMsg  = suppliedActionID + ":"     .

            if tHandlerError
             then DO std-in = 1 TO ERROR-STATUS:NUM-MESSAGES:
                    assign
                      iFaultCode   = "1003"
                      cLogFaultMsg = cLogFaultMsg + ERROR-STATUS:GET-MESSAGE(std-in) + " "
                      .
                  END.

            if compiler:error
             then DO std-in = 1 TO COMPILER:NUM-MESSAGES:
                    assign
                      iFaultCode   = "1004"
                      cLogFaultMsg = cLogFaultMsg + COMPILER:GET-MESSAGE(std-in) + " ".
                      .
                  END.

            ASSIGN
              error-status:error = false
              compiler:error     = false
              . 
        end.
    RETURN iFaultCode = "".
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-async) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION async Procedure 
FUNCTION async RETURNS logical PRIVATE
  ( ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
      def var tHandlerError as logical no-undo init true.
      def var lExecuted as logical no-undo init false.
      def var cLogFaultMsg as char no-undo init ''. 
      
      etime(TRUE).
      RUN-BLOCK:
      DO on ERROR undo RUN-BLOCK, leave RUN-BLOCK 
         on STOP undo RUN-BLOCK, leave RUN-BLOCK
         on QUIT undo RUN-BLOCK, leave RUN-BLOCK:
       
          iAsyncAction = dynamic-new iActionHandler () no-error.
          
          tHandlerError = error-status:error.
          if not tHandlerError
           then 
            do: iAsyncAction:async(iRequest, iResponse).
                lExecuted = true.
            end.
      END.

      /* If there is invalid value in actionHandler (progexec) or queueHandler (queueExec),
         Progress raises the compiler error condition. After adding all the compilar error 
         in response object, clear the error status so that webutill.p does not add another 
         error message in the response XML. 
       */         
      if (tHandlerError OR not lExecuted OR compiler:error) 
       then 
        do: 
            assign
               iFaultMessage = "Unable to execute action. Please contact the system administrator.".
               cLogFaultMsg  = suppliedActionID + ":"     .

            if tHandlerError
             then DO std-in = 1 TO ERROR-STATUS:NUM-MESSAGES:
                    assign
                      iFaultCode   = "1003"
                      cLogFaultMsg = cLogFaultMsg + ERROR-STATUS:GET-MESSAGE(std-in) + " "
                      .
                  END.

            if compiler:error
             then DO std-in = 1 TO COMPILER:NUM-MESSAGES:
                    assign
                      iFaultCode   = "1004"
                      cLogFaultMsg = cLogFaultMsg + COMPILER:GET-MESSAGE(std-in) + " ".
                      .
                  END.

            message cLogFaultMsg.
            ASSIGN
              error-status:error = false
              compiler:error     = false
              . 
        end.
    RETURN iFaultCode = "".


END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-distExist) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION distExist Procedure 
FUNCTION distExist RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  if suppliedEmailAddrList <> '' or suppliedPrinterAddr <> '' or
     suppliedStorageRoot   <> '' or suppliedSaveDir     <> '' or
     suppliedBooleanDest   = 'C'

  then RETURN TRUE. 

  RETURN FALSE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-distribute) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION distribute Procedure 
FUNCTION distribute RETURNS LOGICAL PRIVATE
  ( ) :

    def var tSuccess as logical init true no-undo.    
    def var tSupportedFormats as char init 'json,xml,txt,png,jpeg,bmp,ico,htm,html,pdf,csv,xlsx,docx,zip,rar' no-undo.

    output to 'WEB'.
    run outputhttpheader in web-utilities-hdl("Cache-Control":U, "no-store, no-cache, must-revalidate").
    run outputhttpheader in web-utilities-hdl("Expires":U, "Sat, 01 Jan 2000 01:00:00 GMT").
    run outputhttpheader in web-utilities-hdl("Last-Modified":U, DYNAMIC-FUNCTION("format-datetime" in web-utilities-hdl,"HTTP",today,time,"LOCAL")).
    run outputhttpheader in web-utilities-hdl("Pragma":U, "no-cache").
    
    run outputhttpheader in web-utilities-hdl("Access-Control-Expose-Headers","X-Compass, X-CompassMsg, X-CompassCode").

    if valid-object(iResponse) and iResponse:isFault()
     then iResponse:getFault(output iFaultCode, output iFaultMessage).
   
    if iFaultCode <> ""
     then 
      do: run outputhttpheader in web-utilities-hdl("X-Compass":U, "F").
          run outputhttpheader in web-utilities-hdl("X-CompassMsg":U, iFaultMessage).
          run outputhttpheader in web-utilities-hdl("X-CompassCode":U, iFaultCode).
          run outputhttpheader in web-utilities-hdl("", "").
          output close.
          return false. /* Indicates a fault was returned */
      end.
    
    file-info:file-name = iContentFile.
    if iContentFile = "" 
      or iContentFile = ? 
      or num-entries(iContentFile, ".") < 2
      or file-info:full-pathname = ?
     then 
      do: /* No content */
        iResponse:getSuccess(output iSuccessCode, output iSuccessMessage).
        if iSuccessCode <> ""
         then 
          do: run outputhttpheader in web-utilities-hdl("X-Compass":U, "S").
              run outputhttpheader in web-utilities-hdl("X-CompassMsg":U, iSuccessMessage).
              run outputhttpheader in web-utilities-hdl("X-CompassCode":U, iSuccessCode).
          end.
          run outputhttpheader in web-utilities-hdl("", "").
          output close.
          return true.
      end.

    /*Validate unsupported format*/
    /* Content file name can have multiple '.' in it name so getting the last entry to get the extention of the file*/

    std-in = num-entries(iContentFile, ".").
    std-ch = entry(std-in, iContentFile, ".").
    if not (lookup(std-ch, tSupportedFormats) > 0) 
     then
      do:
         run outputhttpheader in web-utilities-hdl("X-Compass":U, "F").
         run outputhttpheader in web-utilities-hdl("X-CompassMsg":U, "Unsupported output format.").
         run outputhttpheader in web-utilities-hdl("X-CompassCode":U, "1003").
         run outputhttpheader in web-utilities-hdl("", "").
         output close.
         return false.
      end.

    iResponse:getSuccess(output iSuccessCode, output iSuccessMessage).
   
    if iSuccessCode <> ""
     then 
      do: run outputhttpheader in web-utilities-hdl("X-Compass":U, "S").
          run outputhttpheader in web-utilities-hdl("X-CompassMsg":U, iSuccessMessage).
          run outputhttpheader in web-utilities-hdl("X-CompassCode":U, iSuccessCode).
      end.

   /* distribution logic */ 

    if not distExist() /* If no distribution esists then we have to output the response only to web*/
     then
      do:
        std-in = num-entries(iContentFile, ".").
        std-ch = entry(std-in, iContentFile, ".").
        setMimeType(std-ch).
        writeToWeb(iContentFile).
        output close.
      end.
    else /* if distribution exits then checking the response should also be output to web or not along with distribution*/
     do:
        if(suppliedDistType = '')  /* suppliedDistType(I8b) is blank means response only needs to be distribut and not send to client (not output to web)*/
         then
          do:
            run outputhttpheader in web-utilities-hdl("", "").
            output close.
          end.
         else /* suppliedDistType(I8b) = 'B' means the response should be distrubut and send back to client (output to web)*/
          do:
            std-in = num-entries(iContentFile, ".").
            std-ch = entry(std-in, iContentFile, ".").
            setmimetype(std-ch).
            writetoweb(iContentFile).
            output close.
          end.
     end.
     {framework/do-distribute.i &success=tSuccess}
    //copy-lob from file iContentFile to std-lc.
    /*ds: check that export works for both text-based and binary outputs */
    //export std-lc.
    //put skip.
    return tSuccess.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-init) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION init Procedure 
FUNCTION init RETURNS LOGICAL PRIVATE
  (  ) :

    canExec = false.

    /* Parameters can be either in the cookie or in the Query string;
       Query String first to override cookie(s) */
    ASSIGN
        suppliedFormat         = get-value("I0")
        suppliedUid            = get-value("I1")
        suppliedPassword       = decrypt(decodeUrl(get-value("I2")))
        suppliedActionid       = get-value("I3")
        suppliedClientid       = get-value("I4")
        suppliedSessionid      = get-value("I5")
        doDebug                = LOGICAL(get-value("I6"))
        suppliedFieldList      = get-value("I7")
        suppliedActionType     = get-value("I8")    /* 'Q' or ''       */
        suppliedNotifyReq      = get-value("I8a")
        SuppliedDistType       = get-value("I8b")   /* '(B)oth or '' (B)oth means return the response back to client along with distribution */
        suppliedBooleanDest    = get-value("I9")    
        suppliedEmailAddrList  = get-value("I9e")   /* Email address   */
        suppliedPrinterAddr    = get-value("I9p")   /* printer address */
        suppliedStorageRoot    = get-value("I9s")   /* Storage (non-blank = true) */
        suppliedSaveDir        = get-value("I9f")   /* Save-to directory */
        suppliedOutputFormat   = get-value("I10")   /* Output Format   */
        NO-ERROR
        .

    IF suppliedFormat = "" OR suppliedFormat = ?
     THEN suppliedFormat = get-cookie("I0").
    IF suppliedFormat = "" OR suppliedFormat = ?
     THEN suppliedFormat = "JSON".
     
    IF suppliedUid = "" OR suppliedUid = ?
     THEN suppliedUid = get-cookie("I1").
    IF suppliedPassword = "" OR suppliedPassword = ?
     THEN suppliedPassword = decrypt(decodeUrl(get-cookie("I2"))).
    IF suppliedActionid = "" OR suppliedActionid = ?
     THEN suppliedActionid = get-cookie("I3").
    
    IF suppliedClientid = "" OR suppliedClientid = ?
     THEN suppliedClientid = get-cookie("I4").
    
    if suppliedClientid = "" or suppliedClientID = ?
     then suppliedClientid = get-cgi("USER_AGENT"). /* aka, ApplicationCode */
    
    IF suppliedSessionid = "" OR suppliedSessionid = ?
     THEN suppliedSessionid = get-cookie("I5").
     
     IF suppliedFieldList = "" OR suppliedFieldList = ?
     THEN suppliedFieldList = get-cookie("I7").
    
    
    suppliedIPAddress = get-cgi("REMOTE_ADDR").      /* aka, IP address */
    suppliedQueryString = get-cgi("QUERY_STRING").

    PUBLISH "GetSystemParameter" ("canDebug", OUTPUT std-ch).
    canDebug = LOGICAL(std-ch) no-error.
    IF canDebug = ? 
     THEN canDebug = FALSE.
    IF doDebug = ? OR NOT canDebug
     THEN doDebug = FALSE.

    IF CONNECTED("COMPASS") 
     THEN
      DO: RUN util/getactioninfo.p 
            (INPUT suppliedActionID,
             OUTPUT iValidAction,
             OUTPUT iIsActive,
             OUTPUT iIsAnonymous,
             OUTPUT iIsSecure,
             OUTPUT iIsLog,
             OUTPUT iIsAudit,
             OUTPUT iUserids,
             OUTPUT iRoles,
             OUTPUT iIPAddresses,
             OUTPUT iActionHandler,
             OUTPUT iIsEventsEnabled,
             OUTPUT iEmailsEnabled,
             OUTPUT iEmailSuccess,
             OUTPUT iEmailFailure,
             OUTPUT iQueueHandler,
             OUTPUT iQueueable,
             OUTPUT iDisplayName,
             OUTPUT iEmailHtml,
             OUTPUT iEmailSubject,
             OUTPUT iActionDesc
             ).
          
          RUN util/getuserinfo.p
            (INPUT suppliedUid,
             OUTPUT iUserName,
             OUTPUT iUserInitials,
             OUTPUT iUserRole,
             OUTPUT iUserPass,
             OUTPUT iUserActive,
             OUTPUT iUserEmail,
             OUTPUT iUserPasswordSetDate
             ).
         
          RUN util/getpwexpire.p
            (INPUT suppliedUid,
             INPUT iUserPasswordSetDate,
             OUTPUT iUserPasswordExpiredDate,
             OUTPUT iUserPasswordExpired,
             OUTPUT iUserPasswordWarning,
             OUTPUT iUserPasswordWarningDays
             ).

             
         IF NOT iValidAction
          THEN ASSIGN
                 ifaultCode = "1000"
                 ifaultMessage = suppliedActionID + ":InvalidAction"
                 .
         ELSE IF NOT iisactive 
          THEN ASSIGN
                 ifaultCode = "1002"
                 ifaultMessage = suppliedActionID + ":InactiveAction"
                 .
         ELSE IF suppliedActionType = "Q" and (NOT iQueueable OR iQueueable = ?) 
          THEN ASSIGN
                 ifaultCode = "1008"
                 ifaultMessage = suppliedActionID + ":NotQueueable"
                 .
         ELSE IF iUserName = "" AND NOT iisAnonymous 
          THEN ASSIGN
                 ifaultCode = "3001"
                 ifaultMessage = suppliedUid + ":InvalidUser"
                 .
         ELSE IF suppliedUid = "" AND NOT iisAnonymous 
          THEN ASSIGN
                 ifaultCode = "1004"
                 ifaultMessage = "AnonymousDenied"
                 .
         ELSE IF iisSecure
          THEN
           DO: IF NOT CAN-DO(iIpAddresses,suppliedIpAddress)
                THEN ASSIGN
                       ifaultCode = "1006"
                       ifaultMessage = suppliedIpAddress + ":" + "AddressDenied"
                       .
               ELSE IF iUserPass <> suppliedPassword
                THEN ASSIGN
                       ifaultCode = "1004"
                       ifaultMessage = "InvalidCredentials"
                       .
               ELSE IF NOT iUserActive
                THEN ASSIGN
                        ifaultCode = "1004"
                        ifaultMessage = suppliedUID + ":" + "InactiveUser"
                        .
               ELSE IF iUserPasswordExpired
                THEN
                 DO: IF NOT suppliedActionID = "systemIdentity" AND NOT suppliedActionID = "systemUserPassword"
                      THEN ASSIGN
                             ifaultCode = "1007"
                             ifaultMessage = "PasswordExpired"
                             .
                      ELSE canExec = TRUE.
                 END.
               ELSE 
               do: /* Validate user's role can perform the action */
                    std-lo = (lookup("Administrator", iUserRole) <> 0).
                    if not std-lo 
                     then
                      do std-in = 1 to num-entries(iUserRole):
                        std-lo = can-do(iRoles, entry(std-in,iUserRole)).
                        if std-lo 
                         then leave.
                      end.
                   
                    IF NOT(std-lo or CAN-DO(iUserids,suppliedUid))
                    THEN ASSIGN
                           ifaultCode = "1005"
                           ifaultMessage = "PermissionDenied"
                           .
                    ELSE canExec = TRUE.
               end.
           END.
         ELSE canExec = TRUE.
      END.   
     ELSE ASSIGN
            iFaultCode = "1003"
            iFaultMessage = "No Compass database connection"
            .
     
  if canExec
   then
    do:       
        iRequest = new framework.RequestWeb(suppliedUID,
                                            iUserName,
                                            iUserRole,
                                            iUserEmail,
                                            suppliedActionID,
                                            suppliedQueryString,
                                            suppliedFieldList
                                            ).
        iResponse = new framework.ResponseBase().
    end.
         
  RETURN canExec.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-queue) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION queue Procedure 
FUNCTION queue RETURNS LOGICAL
  ( ) :

      def var tHandlerError as logical no-undo init true.
      def var lExecuted as logical no-undo init false.
      def var cLogFaultMsg as char no-undo init ''. 
      
      etime(TRUE).
      RUN-BLOCK:
      DO on ERROR undo RUN-BLOCK, leave RUN-BLOCK 
         on STOP undo RUN-BLOCK, leave RUN-BLOCK
         on QUIT undo RUN-BLOCK, leave RUN-BLOCK:
       
          iQueueAction = dynamic-new iActionHandler () no-error.
          
          tHandlerError = error-status:error.
          if not tHandlerError
           then 
            do: iQueueAction:queue(iRequest, iResponse).
                lExecuted = true.
            end.
      END.

      /* If there is invalid value in actionHandler (progexec) or queueHandler (queueExec),
         Progress raises the compiler error condition. After adding all the compilar error 
         in response object, clear the error status so that webutill.p does not add another 
         error message in the response XML. 
       */         
      if (tHandlerError OR not lExecuted OR compiler:error) 
       then 
        do: 
            assign
               iFaultMessage = "Unable to execute action. Please contact the system administrator.".
               cLogFaultMsg  = suppliedActionID + ":"     .

            if tHandlerError
             then DO std-in = 1 TO ERROR-STATUS:NUM-MESSAGES:
                    assign
                      iFaultCode   = "1003"
                      cLogFaultMsg = cLogFaultMsg + ERROR-STATUS:GET-MESSAGE(std-in) + " "
                      .
                  END.

            if compiler:error
             then DO std-in = 1 TO COMPILER:NUM-MESSAGES:
                    assign
                      iFaultCode   = "1004"
                      cLogFaultMsg = cLogFaultMsg + COMPILER:GET-MESSAGE(std-in) + " ".
                      .
                  END.

            message cLogFaultMsg.
            ASSIGN
              error-status:error = false
              compiler:error     = false
              . 
        end.
    RETURN iFaultCode = "".
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-render) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION render Procedure 
FUNCTION render RETURNS LOGICAL PRIVATE
  (  ) :
  if not valid-object(iAction)
   then return false.
.
  case suppliedOutputFormat:
   when "JSON" 
    or when "XML" 
    or when "PDF" 
    or when "CSV"
    or when "TXT" 
    or when "HTM" then .
   when "HTML" then suppliedOutputFormat = "HTM".
   otherwise suppliedOutputFormat = "JSON".
  end case.
    
  iContentFile = iAction:render(input suppliedOutputFormat, "").
  publish "AddTempFile" ("ContentFile", iContentFile).
  
  RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-setMimeType) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setMimeType Procedure 
FUNCTION setMimeType RETURNS LOGICAL PRIVATE
  ( input filetype as char) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
    def var tMimeType as char no-undo.

    case filetype:
     when "json" 
       then 
        do:
          run outputhttpheader IN web-utilities-hdl("Content-Disposition":U,
                                                   "attachment; filename=" + substring(file-info:file-name,r-index(file-info:file-name,"\") + 1)).
          tMimeType = "text/json".
        end.
     when "xml"  
       then
        do:
         run outputhttpheader IN web-utilities-hdl("Content-Disposition":U,
                                                   "attachment; filename=" + substring(file-info:file-name,r-index(file-info:file-name,"\") + 1)).
          tMimeType = "text/xml".
        end.
     when "txt"  
       then 
       do: 
         run outputhttpheader IN web-utilities-hdl("Content-Disposition":U,
                                                   "attachment; filename=" + substring(file-info:file-name,r-index(file-info:file-name,"\") + 1)).
         tMimeType = "text/plain".
       end.
     when "png" or when "jpeg" or when "bmp"
       then
       do: 
         run outputhttpheader IN web-utilities-hdl("Content-Disposition":U,
                                                   "attachment; filename=" + substring(file-info:file-name,r-index(file-info:file-name,"\") + 1)).
         tMimeType = "image/" + filetype.
       end.
     when "ico"  
       then
       do:
         run outputhttpheader IN web-utilities-hdl("Content-Disposition":U,
                                                   "attachment; filename=" + substring(file-info:file-name,r-index(file-info:file-name,"\") + 1)).
         tMimeType = "image/x-icon".
       end.
     when "htm" or when "html"  
       then
       do: 
         run outputhttpheader IN web-utilities-hdl("Content-Disposition":U,
                                                   "attachment; filename=" + substring(file-info:file-name,r-index(file-info:file-name,"\") + 1)).
         tMimeType = "text/html".

       end.
     when "pdf"  
       then
       do:
         run outputhttpheader IN web-utilities-hdl("Content-Disposition":U,
                                                   "attachment; filename=" + substring(file-info:file-name,r-index(file-info:file-name,"\") + 1)).
         tMimeType = "application/pdf".
       end. 
     when "csv"  
      then
      do: 
        run outputhttpheader IN web-utilities-hdl("Content-Disposition":U,
                                                  "attachment; filename=" + substring(file-info:file-name,r-index(file-info:file-name,"\") + 1)).
        tMimeType = "text/csv".
     end.
     when "xlsx" 
      then 
       do:
         run outputhttpheader IN web-utilities-hdl("Content-Disposition":U,
                                                   "attachment; filename=" + substring(file-info:file-name,r-index(file-info:file-name,"\") + 1)).
         tMimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet".
       end.
     when "docx" 
      then
       do:
         run outputhttpheader IN web-utilities-hdl("Content-Disposition":U,
                                                   "attachment; filename=" + substring(file-info:file-name,r-index(file-info:file-name,"\") + 1)).
         tMimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document".
       end.
     when "zip" 
      then
       do:
         run outputhttpheader IN web-utilities-hdl("Content-Disposition":U,
                                                   "attachment; filename=" + substring(file-info:file-name,r-index(file-info:file-name,"\") + 1)).
         tMimeType = "application/zip".

       end.
     when "rar" 
      then
       do:
         run outputhttpheader IN web-utilities-hdl("Content-Disposition":U,
                                                   "attachment; filename=" + substring(file-info:file-name,r-index(file-info:file-name,"\") + 1)).
         tMimeType = "application/x-rar-compressed".

       end.
     otherwise
      do: /* Unknown content type */
          output close.
          return false.
      end.
    end case.

    run outputhttpheader IN web-utilities-hdl("Content-Type":U, tMimeType).
    run outputhttpheader in web-utilities-hdl("", "").
  RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-writeToWeb) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION writeToWeb Procedure 
FUNCTION writeToWeb RETURNS LOGICAL PRIVATE
  ( input contentFile as char) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
    def var tData as raw no-undo.
    input stream infile from value(ContentFile) binary  no-convert  .
    length(tdata) = 30000.
    repeat:
       import stream infile unformatted tdata.
       put control tdata.
    end.
    length(tdata) = 0.
    input stream infile close.

  RETURN TRUE.   

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

