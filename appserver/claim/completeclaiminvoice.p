/*------------------------------------------------------------------------
@file  completeclaiminvoice.p
@action claimInvoiceComplete
@description Complete a claim invoice

@param ID;int;The row identifier
@param Amount;decimal
@param RefCategory;char

@throws 3000;The create failed
@throws 3061;The amounts are greater than the invoice amount
@throws 3066;Record does not exist
@throws 3067;Record does not exist with ID

@success 2006;The update was successful

@author John Oliver
@version 1.0
@created XX/XX/20XX
----------------------------------------------------------------------*/

define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

{lib/std-def.i}

/* parameter variables */
DEFINE VARIABLE pID as int no-undo.
DEFINE VARIABLE pAmount as decimal no-undo.

/* the parameters */
pRequest:getParameter("ID", output pID).
pRequest:getParameter("Amount", output pAmount).

/* validation */
ASSIGN
  std-lo = FALSE
  std-de = 0
  .
FOR FIRST apinv NO-LOCK
    WHERE apinv.apinvID = pID:
    
  std-lo = true.
  {lib/getclaimreserve.i integer(apinv.refID) apinv.refCategory std-de}
  IF std-de < pAmount
   THEN pResponse:fault("3043","The approved amount ($" + STRING(pAmount) + ")" {&msg-add} "the reserve amount ($" + string(std-de) + ")").
end.
 
/* if the invoice exists, then std-lo will be true */
if not std-lo
 then pResponse:fault("3066","Invoice").

if pResponse:isFault()
 then return.
 
/* add the invoice if no fault */
run ap/completeapinvoice.p (pRequest,pResponse).

if not pResponse:isFault()
 then 
  for first apinv no-lock
      where apinv.apinvID = pID
        and apinv.refCategory = "L"
        and apinv.refType = "C":
        
    run claim/calculateliability.p (integer(apinv.refID), string(apinv.refSeq), output std-de).
  end.
