&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file setclaimassignee.p
@description Set a file Assigned To
@param ClaimID;int;The ID of the claim (must match claim)
@param uid;ch;The UserID of the new assignee
@param note;ch;Notes associated with the reassignment

@returns Success;int;2002

@throws 3000;Update failed
@throws 3004;Claim not found
@throws 3005;Update not applicable

@author D.Sinclair
@created 1.1.2017
@notes
@Modified    :
    Date        Name             Comments
    11/26/2021  Vignesh Rajan    Modified to get the html file path value from 
                                 pRequest:emailHtml instead of .ini file  
----------------------------------------------------------------------*/

DEF INPUT PARAM pRequest AS service.IRequest.
DEF INPUT PARAM pResponse AS service.IResponse.

{lib/std-def.i}

/* local variables */
DEF VAR tFound AS LOGICAL NO-UNDO.
DEF VAR tUpdated AS LOGICAL NO-UNDO.
DEF VAR tFieldChanged AS LOGICAL NO-UNDO.

/* used for the email */
define variable startComment as logical no-undo initial false.
define variable cUserEmail as character no-undo.
define variable cUserName as character no-undo.
define variable cEmailBody as character no-undo.
define variable htmlBody as character no-undo.
define variable htmlLine as character no-undo.
define variable imgString as character no-undo.


/* variables for getParameter */
DEF VAR piClaimID AS INT NO-UNDO.
def var pUid as char no-undo.
def var pNote as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


pRequest:getParameter("ClaimID", OUTPUT piClaimID).
if not can-find(first claim where claim.claimID = piClaimID)
 then pResponse:fault("3066", "Claim").
 
pRequest:getParameter("Uid", OUTPUT pUid).
if not can-find(first sysuser where sysuser.uid = pUid)
 then pResponse:fault("3066", "User").
if not can-find(first sysprop where sysprop.appCode = "CLM"
                                and sysprop.objAction = "ClaimUser"
                                and sysprop.objID = pUid)
 then pResponse:fault("3066", "Claim User").

/* check for valid permission */
if not ({lib/isclaimrole.i &claimID=piClaimID})
 then pResponse:fault("1005","Setting the Claim Assignee").

if pResponse:isFault()
 then return.

pRequest:getParameter("note", OUTPUT pNote).
if pNote = ?
 then pNote = "".

ASSIGN
  tFound = false
  tFieldChanged = false
  tUpdated = false
  .  
 
TRX-BLOCK:
do transaction
on error undo TRX-BLOCK, leave TRX-BLOCK:

  UPD-BLOCK:
  for first claim fields (claimID stat assignedTo) exclusive-lock
    where claim.claimID = piClaimID:
      
    tFound = true.

    if claim.stat = "C" 
     then
      do: 
          leave TRX-BLOCK.
      end.

    claim.assignedTo = pUid.
    
    if claim.dateAssigned = ? 
     then assign
            claim.dateAssigned = now
            claim.userAssigned = pRequest:uid
            .
    /* Always set the Date/User Reassigned the file (overwriting if multiple times) */
     else assign
            claim.dateTransferred = now
            .
    
    validate claim.

    std-ch = "File assigned to " + pUid.
    {claim/addsystemnote.i &subject=std-ch &note=pNote}
    
    tFieldChanged = true.
    tUpdated = true.
  end.
  
end. /* TRX-BLOCK */

IF NOT tFound 
 THEN pResponse:fault("3004", "Claim"). /* Unable to lock */
 ELSE
if not tUpdated
 then pResponse:fault("3000", "Update"). 
      
IF pResponse:isFault() 
 THEN return.

IF NOT tFieldChanged 
 THEN 
  do: pResponse:success("2018", "Claim"). /* Update succeeded, but no changes detected */
      return.
  end.
  
/* the claim was assigned to someone successfully so send them an email */
publish "GetSystemParameter" ("AlliantLogo", output imgString).
htmlBody = search(pRequest:emailHtml).
for first sysuser no-lock
    where sysuser.uid = pUid:
  
  assign
    cUserEmail = sysuser.email
    cUserName = sysuser.name
    .
end.
if htmlBody <> ?
 then
  do:
    cEmailBody = "".
    
    input from value(htmlBody).
    repeat:
      import unformatted htmlLine.
      /* we want to ignore html comments */
      if index(htmlLine, "<!--") > 0
       then startComment = true.
      
      if index(htmlLine, "-->") > 0
       then 
        do:
          startComment = false.
          next.
        end.
       
      if not startComment
       then 
        do:
          /* replace the {} words with the actual values */
          htmlLine = replace(htmlLine, "~{name~}", entry(1, cUserName, " ")).
          htmlLine = replace(htmlLine, "~{claim~}", string(piClaimID)).
          htmlLine = replace(htmlLine, "~{requester~}", pRequest:name).
          
          /* the newline character is for readability when viewing the HTML source */
          cEmailBody = cEmailBody + htmlLine + "~n".
        end.
    end.
    input close.
  end.
 else cEmailBody = "You have been assigned to file " + string(piClaimID) + " by " + pRequest:name.
run util/attachmail.p ("",
                       cUserEmail,
                       "",
                       "Claim File Assignment",
                       cEmailBody,
                       imgString).

pResponse:success("2006", "Claim").
pResponse:updateEvent("Claim", string(piClaimID)).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


