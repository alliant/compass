/*---------------------------------------------------------------------
@name validateclaimpayable.i
@description Validates a claim payable by comparing the user's reserve
             limit to the amount

@param {1};int;The invoice ID
@param {2};logical;True if the invoice contains an admin approval

@author John Oliver
@version 1.0
@created 09/02/2016
@notes 
---------------------------------------------------------------------*/

/* for a reference category of U, an administrator must approve */
{2} = false.
for each apinva no-lock
   where apinva.apinvID = {1}:
   
  if index("clmadmin",lower(apinva.role)) > 0
   then {2} = true.
end.
