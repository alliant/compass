&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file modifyclaim.p
@description Modifies a claim
@param ClaimID;int;The ID of the claim (must match claim)
@parameter Description;char;Claim description
@parameter Stage;char;E)valuation, A)ctive, D)ormant, R)ecovery, L)itigation, or C)omplete
@parameter Action;char;A)ccepted, D)enied, R)esolved, I)ndemnified, or S)ettled
@parameter Difficulty;int;1 = High, 2 = Med, 3 = Low
@parameter Urgency;int;1 = High, 2 = Med, 3 = Low
@parameter EffDate;datetime;
@parameter RefYear;int;YYYY
@parameter YearFirstReport;int;YYYY
@parameter PeriodID;int;YYYYMM
@parameter StateID;char;Two digit abbreviation of state
@parameter AgentID;char
@parameter AgentName;char
@parameter AgentStat;char
@parameter FileNumber;char
@parameter InsuredName;char
@parameter BorrowerName;char
@parameter HasReinsurance;logical
@parameter HasDeductible;logical
@parameter DeductibleAmount;deci
@parameter AmountWaived;deci
@parameter SeekingRecovery;char;Yes, No, or Blank
@parameter EoPolicy;char
@parameter EoCarrier;char
@parameter EoEffDate;datetime
@parameter EoRetroDate;datetime
@parameter EoCoverageAmount;deci
@parameter AgentError;char;Y)es, N)o, U)nknown, or M)ixed
@parameter SearcherError;char;Y)es, N)o, U)nknown, or M)ixed
@parameter AltaRisk;char
@parameter AltaResponsibility;char
@parameter PriorPolicyUsed;char;Y)es, N)o, or U)nknown
@parameter PriorPolicyID;char
@parameter UnderwritingCompliance;char
@parameter SellerBreach;char
@parameter PriorPolicyCarrier;char
@parameter PriorPolicyAmount;deci
@parameter PriorPolicyEffDate;datetime
@parameter Summary;char
@parameter DateClmReceived;datetime
@parameter DateAcknowledged;datetime
@parameter UserAcknowledged;char
@parameter DateRecReceived;datetime
@parameter DateResponded;datetime
@parameter UserResponded;char
@parameter LastActivity;datetime

@returns Success;int;2002 (Object created)
@returns ClaimID;int;Unique identifier for claims

@throws 3000;Update failed
@throws 3001;Field invalid
@throws 3004;Claim not found
@throws 3005;Update not applicable
@throws 3027;Not one of
@throws 3067;Does not exist

@author John Oliver
@created XX.XX.2015
@notes 1.2.2017 D.Sinclair Removed fields that cannot be directly modified by the user
                           These are handled via other actions:

                            @parameter Stat;char;O)pen or C)losed
                            @parameter Type;char;N)otice, C)laim, M)atter
                            @parameter DateCreated;datetime
                            @parameter UserCreated;char
                            @parameter DateTransferred;datetime
                            @parameter AssignedTo;char;The userid of the person assigned to the claim
                            @parameter DateAssigned;datetime
                            @parameter UserAssigned;char
                            @parameter DateClosed;datetime
                            @parameter UserClosed;char
                            @parameter DateReOpened;datetime
                            @parameter UserReOpened;char
                            @parameter DateReClosed;datetime
                            @parameter UserReClosed;char

@notes 03.07.2017 YS    Add the system note to the modified alta risk and alta responsibility.
@modified 02/2018 - YS - Activated log message
          10/17/18 - NC - Modified to add new input Parameter "ExcludeFieldList" in buildsysindex.p call.
          08/07/19 - RS - Modified to add new fields.      
          08/28/19 - RS - Modified to add validation for state and agent.  
----------------------------------------------------------------------*/

define input param pRequest AS service.IRequest.
define input param pResponse AS service.IResponse.

/* temp tables */
{tt/claim.i &tableAlias="ttClaim"}
define temp-table ttPrevClaim no-undo like claim.

{lib/std-def.i}
{lib/validstate.i}

/* local variables */
define variable tFound        as logical   no-undo.
define variable tUpdated      as logical   no-undo.
define variable tFieldChanged as logical   no-undo.
define variable piClaimID     as integer   no-undo.
define variable pRefType      as character no-undo.
define variable pRefNum       as character no-undo.
define variable pRefMessage   as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 2.38
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

empty temp-table ttClaim.
pRequest:getParameter("Claim", input-output table ttClaim).

for first ttClaim exclusive-lock:
  /* validate that the period ID is good */.
  if not can-find(first period where period.periodID = ttClaim.periodID)
   then pResponse:fault("3067", "Period" {&msg-add} "ID" {&msg-add} string(ttClaim.periodID)).
 
  /* Validate state and agent when claim type not equals to "Notice" */
  if can-find(first claim where claim.claimID eq ttClaim.claimID and claim.type ne "N")
   then 
    do:
      /* validate the state is valid */ 
      if not validState(ttClaim.stateID)
       then pResponse:fault("3001", "State").

      /* make sure we have a valid agent for the claim */ 
      if not can-find(first agent where agent.agentID = ttClaim.agentID)
       then pResponse:fault("3067", "Agent" {&msg-add} "ID" {&msg-add} ttClaim.agentID).
    end.
    
  /* Validate state and agent when claim type equals to "Notice" and
   (state not equals to "Unknown" or 
   agent not equals to "Unknown") */
  if can-find(first claim where claim.claimID eq ttClaim.claimID and claim.type eq "N")
   then 
    do:
      /* validate the state is valid */  
      if ttClaim.stateID ne "Unknown"
       then
        do:
          if not validState(ttClaim.stateID)
           then pResponse:fault("3001", "State").
        end.
      
      /* make sure we have a valid agent for the claim */
      if ttClaim.agentID ne "Unknown"
       then
        do:
          if not can-find(first agent where agent.agentID = ttClaim.agentID)
           then pResponse:fault("3067", "Agent" {&msg-add} "ID" {&msg-add} ttClaim.agentID).
        end.
    end.
    
  /* valid agent error is one of Y, N, U, or M */
  {lib/validate-sysprop.i &appCode="'CLM'" &objAction="'ClaimDescription'" &objProperty="'AgentError'" &objID=ttClaim.agentError}
  if not std-lo
   then pResponse:fault("3027", "Agent Error" {&msg-add} std-ch).

  /* valid searcher error is one of Y, N, U, or M */
  {lib/validate-sysprop.i &appCode="'CLM'" &objAction="'ClaimDescription'" &objProperty="'AgentError'" &objID=ttClaim.searcherError}
  if not std-lo
   then pResponse:fault("3027", "Agent Error" {&msg-add} std-ch).
   
  /* validate there is a claim alta risk */
  if ttClaim.altaRisk = ? then ttClaim.altaRisk = "".
  if ttClaim.altaRisk <> "" and not can-find(syscode where syscode.codeType = "ClaimAltaRisk" and syscode.code = ttClaim.altaRisk)
   then pResponse:fault("3067", "ALTA Risk" {&msg-add} "Code" {&msg-add} ttClaim.altaRisk).
   
  /* validate there is a claim alta responsibility */
  if ttClaim.altaResponsibility = ? then ttClaim.altaResponsibility = "".
  if ttClaim.altaResponsibility <> "" and not can-find(syscode where syscode.codeType = "ClaimAltaResp" and syscode.code = ttClaim.altaResponsibility)
   then pResponse:fault("3067", "ALTA Responsibility" {&msg-add} "Code" {&msg-add} ttClaim.altaResponsibility).
   
  /* validate the date responded */
  if ttClaim.dateResponded <> ? and date(ttClaim.dateResponded) > today 
   then pResponse:fault("3005", "Response Date cannot be in the future").
   
  /* validate the date init responded */
  if ttClaim.dateInitResponded <> ? and date(ttClaim.dateInitResponded) > today 
   then pResponse:fault("3005", "General Activity Response Date cannot be in the future").
   
  /* validate the date claim received */
  if ttClaim.dateClmReceived <> ? and date(ttClaim.dateClmReceived) > today 
   then pResponse:fault("3005", "Receipt Date cannot be in the future").
   
  /* validate the date acknowledged */
  if ttClaim.dateAcknowledged <> ? and date(ttClaim.dateAcknowledged) > today 
   then pResponse:fault("3005", "Acknowledgement Date cannot be in the future").
   
  /* validate the date rec received */
  if ttClaim.dateRecReceived <> ? and date(ttClaim.dateRecReceived) > today 
   then pResponse:fault("3005", "Receipt Date cannot be in the future").
   
  if not ({lib/isclaimrole.i &claimID=ttClaim.claimID})
   then pResponse:fault("1005","Modifying a Claim").
  
  /* must be in opened status */
  if can-find(first claim where claim.claimID = ttClaim.claimID and claim.stat = "C")
   then pResponse:fault("3005","Status must be Open to modify the file").

  /* if there is a fault thrown within the parameters, exit */
  if pResponse:isFault()
   then next.
   
  for first claim no-lock
      where claim.claimID = ttClaim.claimID
        and claim.stat <> "C":
    
    assign 
      pRefType = claim.stat
      pRefNum = string(claim.claimID)
      pRefMessage = "Stage: " + claim.stage + " / " +
                    "Action: " + claim.action + " / " +
                    "Difficulty: " + string(claim.difficulty) + " / " +
                    "Urgency: " + string(claim.urgency) + " / " +
                    "Agent ID: " + claim.agentID + " / " +
                    "File Number: " + claim.fileNumber + " / " +
                    "Alta risk: " + claim.altaRisk + " / " +
                    "Alta responsibility: " + claim.altaResponsibility + " / " +
                    "Date claim received: " + string(claim.dateClmReceived,"99-99-9999") + " / " +
                    "Summary: " + claim.summary.

    pResponse:logMessage(pRefType, pRefNum, pRefMessage).
  end.
end.

/* if there is a fault thrown within the parameters, exit */
if pResponse:isFault()
 then return.
 
assign
  tFound = false
  tUpdated = false
  .  
 
/* all parameters are valid */
TRX-BLOCK:
for first ttClaim no-lock transaction
       on error undo TRX-BLOCK, leave TRX-BLOCK:
       
  for first claim exclusive-lock
      where claim.claimID = ttClaim.claimID:
      
    tFound = true.

    /* If agentID is updated, update the reference fields */
    if claim.agentID <> ttClaim.agentID 
     then
      do: 
        for first agent no-lock
            where agent.agentID = ttClaim.agentID:
         
          assign
            claim.agentID   = agent.agentID
            claim.agentName = agent.name
            claim.agentStat = agent.stat
            .
        end.
      end.

    /* Create a system note if any 'Activity' fields have changed.
    */
    std-ch = "".
    
    if date(claim.dateClmReceived) <> date(ttClaim.dateClmReceived) then
    std-ch = std-ch + (if std-ch <> "" then chr(10) else "") +
             "Date Received changed from " +
             (if claim.dateClmReceived <> ? then string(date(claim.dateClmReceived)) else "<blank>") + " to " +
             (if ttClaim.dateClmReceived <> ? then string(date(ttClaim.dateClmReceived)) else "<blank>").
             
    if date(claim.dateAcknowledged) <> date(ttClaim.dateAcknowledged) then
    std-ch = std-ch + (if std-ch <> "" then chr(10) else "") +
             "Date Acknowledged changed from " +
             (if claim.dateAcknowledged <> ? then string(date(claim.dateAcknowledged)) else "<blank>") + " to " +
             (if ttClaim.dateAcknowledged <> ? then string(date(ttClaim.dateAcknowledged)) else "<blank>").
    
    if claim.userAcknowledged <> ttClaim.userAcknowledged then
    std-ch = std-ch + (if std-ch <> "" then chr(10) else "") +
             "User Acknowledged changed from '" +
             claim.userAcknowledged + "' to '" +
             ttClaim.userAcknowledged + "'".

    if date(claim.dateRecReceived) <> date(ttClaim.dateRecReceived) then
    std-ch = std-ch + (if std-ch <> "" then chr(10) else "") +
             "Date Reconsideration Received changed from " +
             (if claim.dateRecReceived <> ? then string(date(claim.dateRecReceived)) else "<blank>") + " to " +
             (if ttClaim.dateRecReceived <> ? then string(date(ttClaim.dateRecReceived)) else "<blank>").

    if date(claim.dateResponded) <> date(ttClaim.dateResponded) then
    std-ch = std-ch + (if std-ch <> "" then chr(10) else "") +
             "Date Reconsideration Responded changed from " +
             (if claim.dateResponded <> ? then string(date(claim.dateResponded)) else "<blank>") + " to " +
             (if ttClaim.dateResponded <> ? then string(date(ttClaim.dateResponded)) else "<blank>").

    if claim.userResponded <> ttClaim.userResponded then
    std-ch = std-ch + (if std-ch <> "" then chr(10) else "") +
             "User Reconsideration Responded changed from '" +
             claim.userResponded + "' to '" +
             ttClaim.userResponded + "'".

    
    if claim.altaRisk <> ttClaim.altaRisk then
    std-ch = std-ch + (if std-ch <> "" then chr(10) else "") +
             "Alta Risk changed from " +
              (if claim.altaRisk <> "" then "'" +  claim.altaRisk  + "'" else "<blank>") + " to " +
              (if ttClaim.altaRisk > "" then "'"  +  ttClaim.altaRisk + "'" else "<blank>") + ".".

    if claim.altaResponsibility <>  ttClaim.altaResponsibility then
     std-ch = std-ch + (if std-ch <> "" then chr(10) else "") +
              "Alta Responsibility changed from " +
              (if claim.altaResponsibility > "" then "'" + claim.altaResponsibility + "'" else "<blank>") + " to " + 
              (if ttClaim.altaResponsibility > "" then  "'"  + ttClaim.altaResponsibility + "'" else "<blank>")+ "." .
         
    if std-ch <> "" 
     then
      do:
        {claim/addsystemnote.i &subject="'Activity Modified'" &note=std-ch}  
      end.

    /* Record the new values to the claim record 
    */
    create ttPrevClaim.
    buffer-copy claim to ttPrevClaim.
    
    assign
      piClaimID                    = ttClaim.claimID
      claim.description            = ttClaim.Description
      claim.stage                  = caps(ttClaim.stage)
      claim.action                 = caps(ttClaim.action)
      claim.difficulty             = ttClaim.Difficulty
      claim.urgency                = ttClaim.Urgency
      claim.effDate                = ttClaim.EffDate
      claim.refYear                = ttClaim.RefYear
      claim.periodID               = ttClaim.PeriodID
      claim.stateID                = ttClaim.StateID
      claim.agentID                = ttClaim.AgentID
      claim.agentName              = ttClaim.AgentName
      claim.agentStat              = ttClaim.AgentStat
      claim.fileNumber             = ttClaim.FileNumber
      claim.insuredName            = ttClaim.InsuredName
      claim.borrowerName           = ttClaim.BorrowerName
      claim.hasReinsurance         = ttClaim.HasReinsurance
      claim.hasDeductible          = ttClaim.HasDeductible
      claim.deductibleAmount       = ttClaim.DeductibleAmount
      claim.amountWaived           = ttClaim.AmountWaived
      claim.seekingRecovery        = caps(ttClaim.SeekingRecovery)
      claim.recoveryNotes          = ttClaim.RecoveryNotes
      claim.eoPolicy               = ttClaim.EoPolicy
      claim.eoCarrier              = ttClaim.EoCarrier
      claim.eoEffDate              = ttClaim.EoEffDate
      claim.eoRetroDate            = ttClaim.EoRetroDate
      claim.eoCoverageAmount       = ttClaim.EoCoverageAmount
      claim.agentError             = caps(ttClaim.AgentError)
      claim.attorneyError          = caps(ttClaim.AttorneyError)
      claim.searcherError          = caps(ttClaim.SearcherError)
      claim.searcher               = caps(ttClaim.Searcher)
      claim.altaRisk               = ttClaim.AltaRisk
      claim.altaResponsibility     = ttClaim.AltaResponsibility
      claim.priorPolicyUsed        = ttClaim.PriorPolicyUsed
      claim.priorPolicyID          = ttClaim.PriorPolicyID
      claim.underwritingCompliance = ttClaim.UnderwritingCompliance
      claim.sellerBreach           = ttClaim.SellerBreach
      claim.priorPolicyCarrier     = ttClaim.PriorPolicyCarrier
      claim.priorPolicyAmount      = ttClaim.PriorPolicyAmount
      claim.priorPolicyEffDate     = ttClaim.PriorPolicyEffDate
      claim.summary                = ttClaim.Summary
      claim.dateClmReceived        = ttClaim.DateClmReceived
      claim.userDateAssigned       = ttClaim.userDateAssigned
      claim.userUserAssigned       = ttClaim.userUserAssigned
      claim.dateAcknowledged       = ttClaim.DateAcknowledged
      claim.userAcknowledged       = ttClaim.UserAcknowledged
      claim.dateRecReceived        = ttClaim.DateRecReceived
      claim.dateResponded          = ttClaim.DateResponded
      claim.userResponded          = ttClaim.UserResponded
      claim.dateInitResponded      = ttClaim.DateInitResponded
      claim.userInitResponded      = ttClaim.UserInitResponded
      .
      
    validate claim.
    
    buffer-compare claim to ttPrevClaim case-sensitive save result in std-ch.
    if std-ch <> "" 
      then tFieldChanged = true.
    
    /* Build keyword index to use for searching
    */
    std-ch = "for each claim where claim.claimid = " + string(piClaimID).
    run sys/buildsysindex.p (input pRequest,
                             input pResponse,
                             input "Claim",             /* ObjType */
                             input "",                  /* ObjAttribute */
                             input string(piClaimID),   /* ObjID */
                             input "claim",             /* SourceTable */
                             input std-ch,              /* SourceQuery */
                             input "").                 /* ExcludeFieldList */

    /* Add an index key so users can locate a claim with the last 4 digits of the claim ID */
    if not can-find(sysindex where sysindex.objType = "Claim"
                               and sysindex.objAttribute = ""
                               and sysindex.objKey = substring(string(piClaimID),5)
                               and sysindex.objID = string(piClaimID)) 
     then 
      do:
        create sysindex.
        assign 
          sysindex.objType = "Claim"
          sysindex.objAttribute = ""
          sysindex.objKey = substring(string(piClaimID),5)
          sysindex.objID = string(piClaimID)
          .
      end.

    tUpdated = true.
  end.
end. /* TRX-BLOCK */

IF NOT tFound 
 THEN pResponse:fault("3004", "Claim").
 ELSE
if not tUpdated
 then pResponse:fault("3000", "Update"). 
      
IF pResponse:isFault() 
 THEN return.

IF NOT tFieldChanged 
 THEN 
  do: pResponse:success("2018", "Claim"). /* Update succeeded, but no changes detected */
      return.
  end.

pResponse:success("2006", "Claim").
pResponse:updateEvent("Claim", string(piClaimID)).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


