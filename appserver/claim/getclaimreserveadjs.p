&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*
@action GetClaimReserveAdjs
@description Return the set of aggregate data about reserve adjustments
@author D.Sinclair
@date 1.4.2017

@parameter claim;in;ClaimID (0 = all)
@parameter approver;ch;uid (blank = all)
@parameter stat;ch;Status (O)pen, (A)pproved, or blank = all

@throws 3066;Invalid claimID

@returns Success;int;2005;Count of reserve adjustments returned
@returns Data;complex based on clm06 definitions
*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}
{tt/clm06.i &tableAlias="data"}

def var tCnt as int init 0 no-undo.

def var pClaimID as int no-undo.
def var pAssignedTo as char no-undo.
def var pStat as char no-undo.

define variable tApprovedDate as datetime no-undo.
define variable tApprovedBy as character no-undo.
define variable tApprovedAmount as decimal no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 70.2.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


pRequest:getParameter("claim", output pClaimID).
if pClaimID = ? 
 then pClaimID = 0.

pRequest:getParameter("stat", output pStat).
if pStat = ? or pStat = ""
 then pStat = "OA".

pRequest:getParameter("approver", output pAssignedTo).
if pAssignedTo = ? 
 then pAssignedTo = "".


tCnt = 0.
if pClaimID <> 0 
 then
  do:
      if index(pStat, "O") > 0 
       then
        for each claimadjreq no-lock
          where claimadjreq.claimID = pClaimID:
         if pAssignedTo > ""
            and claimadjreq.uid <> pAssignedTo
          then next.

         create data.
         assign
           data.claimID = pClaimID
           data.refCategory = claimadjreq.refCategory
           data.dateRequested = claimadjreq.dateRequested
           data.requestedAmount = claimadjreq.requestedAmount
           data.requestedBy = claimadjreq.requestedBy
           data.uid = claimadjreq.uid
           data.notes = claimadjreq.notes
           data.stat = "O"
           tCnt = tCnt + 1
           .
        end.

      if index(pStat, "A") > 0 
       then
        for each claimadjtrx no-lock
          where claimadjtrx.claimID = pClaimID:
         if pAssignedTo > ""
            and claimadjtrx.uid <> pAssignedTo
          then next.

         create data.
         assign
           data.claimID = pClaimID
           data.refCategory = claimadjtrx.refCategory
           data.dateRequested = claimadjtrx.dateRequested
           data.requestedAmount = claimadjtrx.requestedAmount
           data.requestedBy = claimadjtrx.requestedBy
           data.uid = claimadjtrx.uid
           data.notes = claimadjtrx.notes
           data.transDate = claimadjtrx.transDate
           data.transAmount = claimadjtrx.transAmount
           data.completeDate = claimadjtrx.completeDate
           data.stat = "A"
           tCnt = tCnt + 1
           .
        end.
  end.
 else
  do:
      if index(pStat, "O") > 0 
       then
        for each claimadjreq no-lock
           where (if pAssignedTo > "" then claimadjreq.uid = pAssignedTo else true):
           
          for first claim no-lock
              where claim.claimID = claimadjreq.claimID:
              
            create data.
            assign
              data.claimID = claimadjreq.claimid
              data.stateID = claim.stateID
              data.refCategory = claimadjreq.refCategory
              data.dateRequested = claimadjreq.dateRequested
              data.requestedAmount = claimadjreq.requestedAmount
              data.requestedBy = claimadjreq.requestedBy
              data.uid = claimadjreq.uid
              data.notes = claimadjreq.notes
              data.stat = "O"
              tCnt = tCnt + 1
              .
          end.
        end.

      if index(pStat, "A") > 0 
       then
        for each claimadjtrx no-lock
           where (if pAssignedTo > "" then claimadjtrx.uid = pAssignedTo else true):
           
          for first claim no-lock
              where claim.claimID = claimadjtrx.claimID:
              
            create data.
            assign
              data.claimID = claimadjtrx.claimID
              data.stateID = claim.stateID
              data.refCategory = claimadjtrx.refCategory
              data.dateRequested = claimadjtrx.dateRequested
              data.requestedAmount = claimadjtrx.requestedAmount
              data.requestedBy = claimadjtrx.requestedBy
              data.uid = claimadjtrx.uid
              data.notes = claimadjtrx.notes
              data.transDate = claimadjtrx.transDate
              data.transAmount = claimadjtrx.transAmount
              data.completeDate = claimadjtrx.completeDate
              data.stat = "A"
              tCnt = tCnt + 1
              .
          end.
        end.
  end.

if tCnt > 0 and temp-table data:has-records
 then pResponse:setParameter("Data", table data).
pResponse:success("2005", string(tCnt) {&msg-add} "Reserve Adjustment").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


