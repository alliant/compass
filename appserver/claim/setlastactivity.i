&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : setlastactivity.i
    Purpose     : To set the last activity date on the claim record

    Syntax      : {setlastactivity.i pClaimID}

    Description :

    Author(s)   : B. Johnson
    Created     : 11/26/16
    Notes       : Include this code inside a transaction block anywhere
                  the setting of the claim.lastActivity date is desired.
  ----------------------------------------------------------------------*/


/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

  /* For now, we are not going to set the last activity date
     on every change of any claim-related data.  We are only 
     going to set the date when a note record is added.
     
     
  for each claim where claim.claimID = {&1} exclusive-lock:
    assign claim.lastActivity = now.
  end.
  release claim.
  ***/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


