&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file deleteclaimsysindex.p
@action claimSysIndexDelete
@description Deletes sysindex records for a claim

@param ClaimID;int;Claim identifier

@throws 3000;Delete failed
@throws 3067;ClaimID does not exist

@returns Nothing
@success 2005

@author Bryan Johnson
@created 01.28.2016
@notes
----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEF INPUT PARAMETER pRequest AS service.IRequest.
DEF INPUT PARAMETER pResponse AS service.IResponse.

DEF VAR iClaimID AS INT NO-UNDO.

{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* try to retrieve the claim ID from the request */
pRequest:getParameter("ClaimID", OUTPUT iClaimID).
if not can-find(first claim where claim.claimID = iClaimID)
 then 
  do:
    pResponse:fault("3067", "Claim" {&msg-add} "ID" {&msg-add} string(iClaimID)).
    return.
  end.
 
std-lo = false.
FOR EACH sysindex EXCLUSIVE-LOCK
  WHERE sysindex.objType begins "Claim"
  and sysindex.objID = string(iClaimID):
  
  DELETE sysindex.
  RELEASE sysindex.

  std-lo = true.
END.

if not std-lo
 then 
  do: pResponse:fault("3000", "Delete").
      return.
  end.

pResponse:success("2008", "Claim SysIndex").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


