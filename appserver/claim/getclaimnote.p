&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file getclaimnote.p
@action claimNoteGet
@description Returns a claim note dataset for the criteria

@param ClaimID;int;Claim identifier
@param Seq;int;The note sequence number

@throws 3066;ClaimID does not exist

@returns Claim;dataset;The claim note dataset
@success 2005;The number of claim note records returned

@author John Oliver
@created XX.XX.2015
@notes
@modification:
Date         Name  Description
07-28-22     SA    Task #: 95509 :Implement clob framework changes 
----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEF INPUT PARAMETER pRequest AS service.IRequest.
DEF INPUT PARAMETER pResponse AS service.IResponse.

{tt/claimnote.i &tableAlias="ttClaimNote"}

DEF VAR iClaimNbr AS INT NO-UNDO.
DEF VAR iSeq AS INT NO-UNDO.
DEF VAR dsHandle AS HANDLE NO-UNDO.
DEF VAR lcVar AS LONGCHAR      NO-UNDO.

{lib/std-def.i}

/* These preprocessors are used inside xml/xmlparse.i but not needed in this program */
&SCOP traceFile std-ch
&SCOP xmlFile std-ch
{lib/xmlparse.i}

create dataset dsHandle.
dsHandle:xml-node-name = 'data'.
dsHandle:set-buffers(buffer ttClaimNote:handle).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* try to retrieve the claim ID from the request */
pRequest:getParameter("ClaimID", OUTPUT iClaimNbr).
pRequest:getParameter("Seq", OUTPUT iSeq).

/* check for valid permission */
if not ({lib/isclaimrole.i &claimID=iClaimNbr})
 then
  do:
    pResponse:fault("1005","Getting a Claim Note").
    return.
  end.

if iSeq = ? then iSeq = 0.

std-in = 0.
/* perform the table search only if there is a valid claimID */
IF iClaimNbr > 0 THEN DO:
  FOR EACH claimnote NO-LOCK
     WHERE claimnote.claimID = iClaimNbr
       AND claimnote.category <> "11":
    /* check for sequence */
    IF iSeq > 0 AND iSeq <> claimnote.seq THEN
      /* if the sequence is not found, go to the next record */
      NEXT.
  
    CREATE ttClaimNote.
    /* Have to encode the 'notes' field in order to preserve line feeds */
    
    assign 
        ttClaimNote.claimID  = claimNote.claimID
        ttClaimNote.seq      = claimNote.seq
        ttClaimNote.noteDate = claimNote.noteDate 
        ttClaimNote.uid      = claimNote.uid  
        ttClaimNote.category = claimNote.category
        ttClaimNote.subject  = claimNote.subject
        ttClaimNote.noteType = claimNote.noteType
        ttClaimNote.notes    = claimnote.notes
        .

    std-in = std-in + 1.
  END.
  
  IF std-in > 0 THEN
      pResponse:setParameter(input dataset-handle dsHandle).

  pResponse:success("2005", STRING(std-in) {&msg-add} "ClaimNote").
END.
ELSE
    /* if there is no claimID, throw an error */
    pResponse:fault("3066", "ClaimID").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


