/*------------------------------------------------------------------------
@name lockclaim.p
@action claimLock
@description Locks a claim record
@param ClaimID;int;The id of the claim (required)

@throws 3004;The claim was not locked
@throws 3067;The claim is not found in the claim table
@throws 3081;The claim is locked by another user

@success 2000;The claim has been locked
@success 2018;The claim is locked by user

@author John Oliver
@version 1.0
@created 11/20/2015
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

def var piClaimID as int no-undo.

{lib/std-def.i}

pRequest:getParameter("ClaimID", OUTPUT piClaimID).
/* validate the claim id is valid */
if not can-find(first claim where claim.claimID = piClaimID)
 then pResponse:fault("3067", "Claim" {&msg-add} "ID" {&msg-add} string(piClaimID)).
 
/* validate that the claim is not locked already locked */
run util/issyslock.p ("Claim", string(piClaimID), 0, OUTPUT std-lo, OUTPUT std-ch, OUTPUT std-dt).
if std-lo
 then
  do: if std-ch = pRequest:uid
       then pResponse:success("2018", "Claim").
       else pResponse:fault("3081", "Claim" {&msg-add} std-ch).
      return.
  end.
  
if pResponse:isFault()
 then return.

std-lo = false.
run util/newsyslock.p ("Claim", string(piClaimID), 0, pRequest:Uid, output std-lo).

if not std-lo
 then
  do: pResponse:fault("3004", "Claim").
      return.
  end.

pResponse:success("2000", "Lock").
