&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*---------------------------------------------------------------------
@name addsystemnote.i
@description Add a System Note to a Claim

@param claimID;int;The claim id
@param username;char;The user id of current user (usually pRequest:uid on the server)
@param subject;char;The note subject
@param note;char;The note

@author John Oliver
@version 1.0
@created 12/12/2016
@notes 1.2.2017 D.Sinclair Generalized subject/note parameter
                           Added fields list
                           Allow include to be included more than once
                           Defaulted all parameters
---------------------------------------------------------------------*/

&IF defined(AddNoteSeq) = 0
 &THEN
define variable iClaimNoteSeq as integer no-undo.
&global-define AddNoteSeq true
&ENDIF

&IF defined(claimID) = 0 
 &THEN
&scoped-define claimID claim.claimID
&ENDIF

&IF defined(username) = 0 
 &THEN
&scoped-define username pRequest:uid
&ENDIF

&IF defined(subject) = 0 
 &THEN
&scoped-define subject ''
&ENDIF

&IF defined(note) = 0 
 &THEN
&scoped-define note ''
&ENDIF

iClaimNoteSeq = 1.
FOR FIRST claimnote fields (claimID seq) NO-LOCK 
    WHERE claimnote.claimID = {&claimID}
       BY claimnote.seq DESCENDING:
  iClaimNoteSeq = claimnote.seq + 1.
END.

create claimnote.
assign
  claimnote.claimID = {&claimID}
  claimnote.seq = iClaimNoteSeq
  claimnote.noteDate = now
  claimnote.uid = {&username}
  claimnote.category = "0" /* 0 = System */
  claimnote.subject = {&subject}
  claimnote.noteType = "A"          /* (A)ction note */
  claimnote.notes = {&note}
  .

validate claimnote.
release claimnote.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


