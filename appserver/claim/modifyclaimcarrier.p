&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file modifyclaimcarrier.p
@action claimBondCarrier
@description Modify a claim carrier's name

@param ClaimID;int;The identifier of the claim (must match claim)
@param CarrierID;int;The identifier of the claim carrier
@param CarrierName;char;The carrier's name

@success 2005;The carrier's name was modified

@throws 3000;Update failed
@throws 3004;Claim bond not found
@throws 3005;Update not applicable
@throws 3067;Claim ID was not found

@author John Oliver
@created XX.XX.2015
@notes
------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEF INPUT PARAM pRequest AS service.IRequest.
DEF INPUT PARAM pResponse AS service.IResponse.

{lib/std-def.i}

/* local variables */
DEF VAR tFound AS LOGICAL NO-UNDO.
DEF VAR tUpdated AS LOGICAL NO-UNDO.
DEF VAR tFieldChanged AS LOGICAL NO-UNDO.

DEF TEMP-TABLE ttPrevValues LIKE claimcarrier.

/* variables for getParameter */
DEF VAR piClaimID AS INT NO-UNDO.
def var piCarrierID as int no-undo.
DEF VAR pcCarrierName AS CHAR NO-UNDO.

/* local variables */
def var iSeq as int no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* Start getting the parameters */
pRequest:getParameter("ClaimID", OUTPUT piClaimID).
if not can-find(first claim where claim.claimID = piClaimID)
 then pResponse:fault("3067", "Claim" {&msg-add} "ID" {&msg-add} string(piClaimID)).

pRequest:getParameter("CarrierID", OUTPUT piCarrierID).
pRequest:getParameter("CarrierName", OUTPUT pcCarrierName).

/* if there is a fault thrown within the procedure, exit */
ASSIGN
  tFound = false
  tUpdated = false
  .  
 
/* all parameters are valid */
TRX-BLOCK:
for first claimcarrier exclusive-lock
  where claimcarrier.claimID = piClaimID
    and claimcarrier.carrierID = piCarrierID TRANSACTION
    on error undo TRX-BLOCK, leave TRX-BLOCK:
    
  tFound = true.
  
  create ttPrevValues.
  buffer-copy claimcarrier to ttPrevValues.
  
  assign
    claimcarrier.carrierName = pcCarrierName
    .
    
  validate claimcarrier.
  
  buffer-compare claimcarrier to ttPrevValues case-sensitive save result in std-ch.
  if std-ch <> ""
   then tFieldChanged = true.
  
  release claimcarrier.
  
  {claim/setlastactivity.i piClaimID}
  
  std-lo = true.
  tUpdated = true.
end.

IF NOT tFound 
 THEN pResponse:fault("3004", "Claim Carrier").
 ELSE
if not tUpdated
 then pResponse:fault("3000", "Update"). 

IF NOT tFieldChanged 
 THEN pResponse:success("2018", "Claim Carrier"). /* Update succeeded, but no changes detected */
 ELSE pResponse:success("2006", "Claim Carrier").
 
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME