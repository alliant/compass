&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file getclaimbond.p
@purpose Returns a claim note dataset for the criteria

@parameters ClaimID;int;Claim identifier
@parameters Seq;int;The note sequence number

@throws 3066;ClaimID does not exist

@returns Claim;dataset;The claim note dataset
@returns 2005;int;Success number in the syscode table

@author John Oliver
@created XX.XX.2015
@notes
------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEF INPUT PARAMETER pRequest AS service.IRequest.
DEF INPUT PARAMETER pResponse AS service.IResponse.

{tt/claimbond.i &tableAlias="ttClaimBond"}

DEF VAR iClaimID AS INT NO-UNDO.
DEF VAR iBondID AS INT NO-UNDO.

{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* try to retrieve the claim ID from the request */
pRequest:getParameter("ClaimID", OUTPUT iClaimID).
pRequest:getParameter("BondID", OUTPUT iBondID).

std-in = 0.
/* perform the table search only if there is a valid claimID */
IF iClaimID > 0 THEN DO:
  FOR EACH claimbond NO-LOCK
    WHERE claimbond.claimID = iClaimID:
    /* check for sequence */
    IF iBondID > 0 AND iBondID <> claimbond.bondID THEN
      /* if the sequence is not found, go to the next record */
      NEXT.
  
    CREATE ttClaimBond.
    BUFFER-COPY claimbond TO ttClaimBond.
  
      std-in = std-in + 1.
  END.
  
  IF std-in > 0 THEN
      pResponse:setParameter("ClaimBond", TABLE ttClaimBond).
  
  pResponse:success("2005", STRING(std-in) {&msg-add} "ClaimBond").
END.
ELSE
    /* if there is no claimID, throw an error */
    pResponse:fault("3066", "ClaimID").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


