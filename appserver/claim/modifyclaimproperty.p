&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file modifyclaimproperty.p
@purpose Modify a claim property

@parameter ClaimID;int;Claim number
@parameter Seq;int;The seq for the property
@parameter Addr1;char;The addr1 for the property
@parameter Addr2;char;The addr2 for the property
@parameter City;char;The city for the property
@parameter CountyID;char;The countyid for the property
@parameter StateID;char;The stateid for the property
@parameter Zipcode;char;The zipcode for the property
@parameter Residential;logical;If the property is residential
@parameter Subdivision;char;The subdivision for the property
@parameter LegalDescription;char;The legal description of the property
@parameter PropertyType;char;The type of property

@success 2006;The Claim Property was updated

@throws 3000;Update failed
@throws 3001;Field invalid
@throws 3004;Claim not found
@throws 3005;Update not applicable
@throws 3067;Field not found in foriegn key table

@author John Oliver
@created XX.XX.2015
@notes
Date        Name           Description
10/17/18    Naresh Chopra  Modfied to add new input Parameter "ExcludeFieldList" in buildsysindex.p call.
----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEF INPUT PARAM pRequest AS service.IRequest.
DEF INPUT PARAM pResponse AS service.IResponse.

{lib/std-def.i}

DEF VAR tFound AS LOGICAL NO-UNDO.
DEF VAR tUpdated AS LOGICAL NO-UNDO.
DEF VAR tFieldChanged AS LOGICAL NO-UNDO.
def var cQuery as char no-undo.

DEF TEMP-TABLE ttPrevValues LIKE claimproperty.

/* variables for getParameter */
DEF VAR piClaimID AS INT NO-UNDO.
DEF VAR piSeq AS INT NO-UNDO.
DEF VAR plIsPrimary AS LOGICAL NO-UNDO.
DEF VAR pcAddr1 AS CHAR NO-UNDO.
DEF VAR pcAddr2 AS CHAR NO-UNDO.
DEF VAR pcCity AS CHAR NO-UNDO.
DEF VAR pcCountyID AS CHAR NO-UNDO.
DEF VAR pcStateID AS CHAR NO-UNDO.
DEF VAR pcZipcode AS CHAR NO-UNDO.
DEF VAR plResidential AS LOGICAL NO-UNDO.
DEF VAR pcSubdivision AS CHAR NO-UNDO.
DEF VAR pcLegalDescription AS CHAR NO-UNDO.
DEF VAR pcPropertyType AS CHAR NO-UNDO.

DEF VAR iSeq AS INT NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* Start getting the parameters */
pRequest:getParameter("ClaimID", OUTPUT piClaimID).
if not can-find(first claim where claim.claimID = piClaimID)
 then pResponse:fault("3067", "Claim" {&msg-add} "ID" {&msg-add} string(piClaimID)).
 
pRequest:getParameter("Seq", OUTPUT piSeq).
if piSeq = 0
 then pResponse:fault("3001", "Sequence").
 
pRequest:getParameter("isPrimary", OUTPUT plIsPrimary).
pRequest:getParameter("Addr1", OUTPUT pcAddr1).
pRequest:getParameter("Addr2", OUTPUT pcAddr2).
pRequest:getParameter("City", OUTPUT pcCity).
pRequest:getParameter("CountyID", OUTPUT pcCountyID).
pRequest:getParameter("StateID", OUTPUT pcStateID).
pRequest:getParameter("Zipcode", OUTPUT pcZipcode).
pRequest:getParameter("Residential", OUTPUT plResidential).
pRequest:getParameter("Subdivision", OUTPUT pcSubdivision).
pRequest:getParameter("LegalDescription", OUTPUT pcLegalDescription).
pRequest:getParameter("PropertyType", OUTPUT pcPropertyType).

/* check for valid permission */
if not ({lib/isclaimrole.i &claimID=piClaimID})
 then pResponse:fault("1005","Modifying a Claim Property").

/* if there is a fault thrown within the procedure, exit */
if pResponse:isFault()
 then return.
 
ASSIGN
  tFound = false
  tUpdated = false
  .  
 
/* all parameters are valid */
TRX-BLOCK:
for first claimproperty exclusive-lock
  where claimproperty.claimID = piClaimID
    and claimproperty.seq = piSeq  TRANSACTION
    on error undo TRX-BLOCK, leave TRX-BLOCK:
    
  tFound = true.
  
  create ttPrevValues.
  buffer-copy claimproperty to ttPrevValues.
  
  assign
    claimproperty.isPrimary = plIsPrimary
    claimproperty.addr1 = pcAddr1
    claimproperty.addr2 = pcAddr2
    claimproperty.city = pcCity
    claimproperty.countyID = pcCountyID
    claimproperty.stateID = caps(pcStateID)
    claimproperty.zipcode = pcZipcode
    claimproperty.residential = plResidential
    claimproperty.subdivision = pcSubdivision
    claimproperty.legalDescription = pcLegalDescription
    claimproperty.propertyType = pcPropertyType
    .
    
  validate claimproperty.

  buffer-compare claimproperty to ttPrevValues case-sensitive save result in std-ch.
  if std-ch <> ""
   then tFieldChanged = true.

  release claimproperty.

  {claim/setlastactivity.i piClaimID}
  
  /* Build keyword index to use for searching
  */
  cQuery = "for each claimproperty where claimproperty.claimid = " + string(piClaimID) 
         + " and claimproperty.seq = " + string(piSeq).
  run sys/buildsysindex.p (input pRequest,
                           input pResponse,
                           input "ClaimProperty",     /* ObjType */
                           input string(piSeq),       /* ObjAttribute */
                           input string(piClaimID),   /* ObjID */
                           input "claimproperty",     /* SourceTable */
                           input cQuery,              /* SourceQuery */
                           input "").                 /* ExcludeFieldList */
  
  std-lo = true.
  tUpdated = true.
end.

IF NOT tFound 
 THEN pResponse:fault("3004", "Claim Property").
 ELSE
IF NOT tUpdated
 then pResponse:fault("3000", "Update").
 
IF pResponse:isFault() 
 THEN return.

IF NOT tFieldChanged 
 THEN pResponse:success("2018", "Claim Property"). /* Update succeeded, but no changes detected */
 ELSE pResponse:success("2006", "Claim Property").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
