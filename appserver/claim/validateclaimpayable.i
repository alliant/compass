/*---------------------------------------------------------------------
@name validateclaimpayable.i
@description Validates a claim payable by comparing the user's reserve
             limit to the amount

@param {1};int;The claim ID
@param {2};char;The category type of the invoice
@param {3};char;The user
@param {4};decimal;The returned amount the user is allowed to approve


@author John Oliver
@version 1.0
@created 09/02/2016
@notes 
---------------------------------------------------------------------*/

{lib/getclaimreservelimit.i {1} {2} {3} {4}}
