/*----------------------------------------------------------------------
@file getclaimlenderreport.p
@action claimWellsFargoStatusReport
@description Get the Wells Fargo Status Report

@param ID;int;The claim ID
@param module;ch;the appCode that must be assigned to the Vendor (Optional)

@author John Oliver
@created XX.XX.2015
@notes
1.1.2017 D.Sinclair - Added concept of Module to restrict the vendor list
Modified
Date       Name    Description
01-25-2022 Shefali Task:89648 - Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"               

----------------------------------------------------------------------*/

define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

/* stored procedure */
define variable hTable as handle no-undo.

/* parameters */
define variable pPeriod as integer no-undo.

/* local variables */
define variable iMonth as integer no-undo initial 0.
define variable iYear as integer no-undo initial 0.

{lib/std-def.i}
{lib/callsp-defs.i} 
{tt/claimlenderreport.i}

pRequest:getParameter("Period", output pPeriod).

/* validation */
for first period no-lock
    where period.periodID = pPeriod:
  
  assign
    iMonth = period.periodMonth
    iYear = period.periodYear
    .
end.
if iMonth = 0 or iYear = 0
 then pResponse:fault("3066","Period").
 
/* if fault, return */
if pResponse:isFault()
 then return.
{lib/callsp.i  &name=spReportClaimWellsFargoStatus &load-into=claimlenderreport &params="input iMonth, input iYear" &setparam="'Report'"}
 
pResponse:success("2005", string(csp-icount) {&msg-add} "Claim").
