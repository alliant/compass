&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file getclaimbond.p
@action claimBondDelete
@purpose Deletes a claim bond

@parameters ClaimID;int;Claim identifier
@parameters BondID;int;The bond identifier

@throws 3067;Claim ID does not exist

@success 2008;The claim bond was deleted

@author John Oliver
@created XX.XX.2015
@notes
@modified 02/2018 - YS - Activated log message
------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEF INPUT PARAMETER pRequest AS service.IRequest.
DEF INPUT PARAMETER pResponse AS service.IResponse.

{tt/claimbond.i &tableAlias="ttClaimBond"}

DEF VAR iClaimID AS INT NO-UNDO.
DEF VAR iBondID AS INT NO-UNDO.

DEF VAR cSearchKey AS CHAR INIT "" NO-UNDO.
DEF VAR cValue AS CHAR INIT "" NO-UNDO.

def var pRefType as char no-undo.
def var pRefNum as char no-undo.
def var pRefMessage as char no-undo.

{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 2.43
         WIDTH              = 73.6.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* try to retrieve the claim ID from the request */
pRequest:getParameter("ClaimID", OUTPUT iClaimID).
if not can-find(first claim where claim.claimID = iClaimID)
 then 
  do:
    pResponse:fault("3067", "Claim" {&msg-add} "ID" {&msg-add} string(iClaimID)).
    return.
  end.
 
pRequest:getParameter("BondID", OUTPUT iBondID).

/* check for valid permission */
if not ({lib/isclaimrole.i &claimID=iClaimID})
 then
  do:
    pResponse:fault("1005","Deleting a Claim Bond").
    return.
  end.

for each claimbond no-lock where claimbond.claimID = iclaimID and claimbond.bondID = iBondID:
   assign
    pRefType = claimbond.type 
    pRefNum = string(claimbond.claimID)
    pRefMessage =  "Entity: " + claimbond.entity + " / " +
                   "Amount: " + string(claimbond.amount)                                     
    .

    pResponse:logMessage(pRefType, pRefNum, pRefMessage).

end.
  
std-lo = false.
TRX-BLOCK:
do TRANSACTION
  on error undo TRX-BLOCK, leave TRX-BLOCK:

  FOR EACH claimbond EXCLUSIVE-LOCK
    WHERE claimbond.claimID = iClaimID:
    
    if iBondID > 0 and claimbond.bondID <> iBondID
     then next.
  
    DELETE claimbond.
    RELEASE claimbond.
  END.

  {claim/setlastactivity.i iClaimID}
  
  std-lo = true.
end.

if not std-lo
 then
  do: pResponse:fault("3000", "Delete").
      return.
  end.

pResponse:success("2008", "Claim").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


