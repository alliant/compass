&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file closeclaim.p
@action claimClose
@description Closes a claim

@param ClaimID;int;The ID of the claim (must match claim)

@returns Success;int;2002 (Object created)
@returns ClaimID;int;Unique identifier for claims

@throws 3000;Update failed
@throws 3001;Field invalid
@throws 3004;Claim not found
@throws 3005;Update not applicable
@throws 3027;Not one of
@throws 3067;Does not exist

@author John Oliver
@created XX.XX.2015
@notes
@modification
date         Name           Description
08/09/2019   Gurvindar      Added Crm notification logic
----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEF INPUT PARAM pRequest AS service.IRequest.
DEF INPUT PARAM pResponse AS service.IResponse.

{lib/std-def.i}
{lib/validstate.i}
{lib/get-reserve-type.i}

/* local variables */
define variable tFound AS LOGICAL NO-UNDO.
define variable iSeq as int no-undo.

define variable cSearchKey AS character no-undo initial "".
define variable cValue AS character no-undo initial "".
define variable cRefCategory as character no-undo initial "L,E".

/* variables for getParameter */
define variable piClaimID AS INT NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* Start getting the parameters */
pRequest:getParameter("ClaimID", OUTPUT piClaimID).
 
/* validation */
IF NOT CAN-FIND(FIRST claim WHERE claimID = piClaimID)
 THEN pResponse:fault("3066", "Claim").
 
/* can't close a claim with a reserve balance */
DO std-in = 1 TO NUM-ENTRIES(cRefCategory):
  std-ch = ENTRY(std-in,cRefCategory).
  {lib/getclaimreserve.i piClaimID std-ch std-de}
  IF NOT std-de = 0
   THEN
    DO:
      pResponse:fault("3021", "The reserve category " + std-ch {&msg-add} "0").
      std-in = NUM-ENTRIES(cRefCategory).
    END.
END.
 
/* can't close a claim with open payables */
for each apinv no-lock
   where apinv.refID = string(piClaimID):
  
  if apinv.stat = "O" or apinv.stat = "A"
   then pResponse:fault("3015", "Open Payable" {&msg-add} "Claim " + string(piClaimID)).
end.

/* if there is a fault thrown within the procedure, exit */
if pResponse:isFault()
 then return.
 
/* validation after determining claim is valid */
/* if there was an agent error, it must have a note attached */

 
/* if there is a fault thrown within the procedure, exit */
if pResponse:isFault()
 then return.
 
/* all parameters are valid */
TRX-BLOCK:
FOR FIRST claim EXCLUSIVE-LOCK
    WHERE claim.claimID = piClaimID TRANSACTION
    ON ERROR UNDO TRX-BLOCK, LEAVE TRX-BLOCK:
    
  tFound = TRUE.
     
  ASSIGN
    claim.stat = "C"
    .
  VALIDATE claim.
  
  iSeq = 1.
  FOR FIRST claimnote NO-LOCK
      WHERE claimnote.claimID = piClaimID
         BY claimnote.seq DESCENDING:
    iSeq = iSeq + claimnote.seq.
  END.
  
  CREATE claimnote.
  ASSIGN
    claimnote.claimID = piClaimID
    claimnote.seq = iSeq
    claimnote.noteDate = now
    claimnote.category = "0" /* 0 = System */
    claimnote.noteType = "A"          /* (A)ction note */
    claimnote.uid = pRequest:Uid
    claimnote.subject = "Claim Closed"
    claimnote.notes = "Closing the claim"
    .
  VALIDATE claimnote.
  
  /* Remove existing index entries for this claim */
  RUN claim/deleteclaimsysindex.p (pRequest,pResponse).
  
  RELEASE claimnote.
  RELEASE claim. 

  /* creating notification */                 
  pResponse:notify(input "",                /* uid specfically created for this uid */
                   input "E",               /* (E)mployeee entityType */
                   input pRequest:Uid,         /* entityId */
                   input 0,                 /* refsysNotficationID */
                   input "G",               /* notificationStat   */
                   input "Claim",           /* sourcetype */
                   input string(piClaimID),  /* sourceID */
                   input "Claim Closed with ID: " + string(piClaimID)). /* notification message  */      
       
END.
 
IF NOT tFound 
 THEN pResponse:fault("3001", "Closing the Claim").
      
IF pResponse:isFault() 
 THEN return.

pResponse:success("2006", "Claim").
pResponse:updateEvent("Claim", string(piClaimID)).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


