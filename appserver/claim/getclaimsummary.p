&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*
@action GetClaimSummary
@description Return the set of aggregate data about claims
@author D.Sinclair
@date 12.28.2016

@parameter stateID;ch;StateID (blank = all)
@parameter agentID;ch;AgentID (blank = all)
@parameter assignedTo;ch;AssignedTo (blank = all)
@parameter cutoff;int;Age with no note (0 = all)
@parameter stat;ch;Status (blank = all)

@throws 3066;Invalid State or Agent

@returns Success;int;2005;Count of claims returned
@returns Data;complex based on clm01 definitions
Modified
Date       Name    Description
01-25-2022 Shubham Task:89648 - Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"               
07-28-2022 SA      Task:95509 - Implement clob framework changes 
*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}
{lib/callsp-defs.i}
{lib/add-delimiter.i}
{tt/clm01.i &tableAlias="data"}

def temp-table xclaim
 field claimID as int.

def var tCnt as int init 0 no-undo.
def var pStateID as char no-undo.
def var pAgentID as char no-undo.
def var pAssignedTo as char no-undo.
def var pCutoff as int no-undo.
def var pStat as char no-undo.

def var tNoteCnt as int.
def var tNoteDate as date.
def var tNoteText as longchar.

def var tCause as char.
def var tDesc as char.

def var tOwnerPolicyID as char.
def var tOwnerPolicyEffDate as date.
def var tOwnerPolicyInsuredType as char.
def var tOwnerPolicyInsuredName as char.
def var tOwnerPolicyOrigLiability as dec.
def var tOwnerPolicyCurrLiability as dec.
def var tOwnerPolicyFormID as char.
def var tOwnerPolicyFormDesc as char.
def var tOwnerPolicyFormType as char.

def var tLenderPolicyID as char.
def var tLenderPolicyEffDate as date.
def var tLenderPolicyInsuredType as char.
def var tLenderPolicyInsuredName as char.
def var tLenderPolicyOrigLiability as dec.
def var tLenderPolicyCurrLiability as dec.
def var tLenderPolicyFormID as char.
def var tLenderPolicyFormDesc as char.
def var tLenderPolicyFormType as char.

def var tCoverageID as char.
def var tCoverageEffDate as date.
def var tCoverageCoverageType as char.
def var tCoverageInsuredType as char.
def var tCoverageInsuredName as char.
def var tCoverageOrigLiability as dec.
def var tCoverageCurrLiability as dec.
def var tCoverageFormID as char.
def var tCoverageFormDesc as char.
def var tCoverageFormType as char.

def var tInsuredName as char.
def var tInsuredAddr as char.

def temp-table xclaimacct
 field claimID as int
 field refCategory as char
 field asOfDate as datetime
 field pendingInvoiceAmount as dec
 field approvedInvoiceAmount as dec
 field completedInvoiceAmount as dec
 field pendingReserveAmount as dec
 field approvedReserveAmount as dec
 field reserveBalance as dec
 .

def var hTable as handle.

def var dsHandle as handle no-undo.
def var lcVar as longchar no-undo.

create dataset dsHandle.
dsHandle:xml-node-name = 'data'.
dsHandle:set-buffers(buffer data:handle).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 70.2.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


pRequest:getParameter("state", output pStateID).
if pStateID = "" or pStateID = ? 
 then pStateID = "ALL".
pStateID = caps(pStateID).

if pStateID <> "ALL" 
and not can-find(first state 
                  where state.stateID = pStateID)
 then
  do: pResponse:fault("3066", "State").
      return.
  end.

pRequest:getParameter("agent", output pAgentID).
if pAgentID = "" or pAgentID = ? 
 then pAgentID = "ALL".

if pAgentID <> "ALL"
and not can-find(first agent 
                  where agent.agentID = pAgentID)
 then
  do: pResponse:fault("3066", "Agent").
      return.
  end.

pRequest:getParameter("stat", output pStat).
if pStat = ? or pStat = ""
 then pStat = "OC".

pRequest:getParameter("assignedTo", output pAssignedTo).
if pAssignedTo = ? 
 then pAssignedTo = "".

pRequest:getParameter("cutoff", output pCutoff).
if pCutoff < 0 or pCutoff = ?
 then pCutoff = 0.

{lib/callsp.i  &name=spReportClaimBalances &load-into=xclaimacct &params="input 0, input ?" &noCount=true &noresponse=true}




for each claim no-lock
  where (claim.agentID = pAgentID or pAgentID = "ALL")
    and (claim.stateID = pStateID or pStateID = "ALL")
    and (claim.assignedTo = pAssignedTo or pAssignedTo = "" or pAssignedTo = "ALL")
    and index(pStat, claim.stat) > 0:

 tNoteCnt = 0.
 tNoteDate = ?.
 tNoteText = "".
 for each claimnote no-lock
   where claimnote.claimID = claim.claimID
     by claimnote.noteDate:

    assign
      tNoteCnt = tNoteCnt + 1
      tNoteDate = claimnote.noteDate.

    COPY-LOB FROM Claimnote.notes TO lcVar.

    if lcVar <> "" and lcVar <> ? then
      copy-lob from lcVar to tNoteText.
 end.

 if pCutoff > 0 
   and tNoteDate <> ?
   and tNoteDate > (today - pCutoff)
  then next.

 tCause = "".
 for each claimcode no-lock
   where claimcode.claimID = claim.claimID
     and claimcode.codeType = "ClaimCause"
   /*and claimcode.seq = 1*/
   by claimcode.seq:
   
   for first syscode no-lock
       where syscode.codeType = claimcode.codeType
         and syscode.code = claimcode.code:
     
     tCause = addDelimiter(tCause, ",") +  syscode.description + " (" + trim(syscode.code) + ")".
   end.
 end.

 tDesc = "".
 for each claimcode no-lock
   where claimcode.claimID = claim.claimID
     and claimcode.codeType = "ClaimDescription"
   /*and claimcode.seq = 1*/
   by claimcode.seq:
   
   for first syscode no-lock
       where syscode.codeType = claimcode.codeType
         and syscode.code = claimcode.code:
     
     tDesc = addDelimiter(tDesc, ",") +  syscode.description + " (" + trim(syscode.code) + ")".
   end.
 end.

 assign
   tOwnerPolicyID = ""
   tOwnerPolicyEffDate = ?
   tOwnerPolicyInsuredType = ""
   tOwnerPolicyInsuredName = ""
   tOwnerPolicyOrigLiability = 0
   tOwnerPolicyCurrLiability = 0
   tOwnerPolicyFormID = ""
   tOwnerPolicyFormDesc = ""
   tOwnerPolicyFormType = ""

   tLenderPolicyID = ""
   tLenderPolicyEffDate = ?
   tLenderPolicyInsuredType = ""
   tLenderPolicyInsuredName = ""
   tLenderPolicyOrigLiability = 0
   tLenderPolicyCurrLiability = 0
   tLenderPolicyFormID = ""
   tLenderPolicyFormDesc = ""
   tLenderPolicyFormType = ""

   tCoverageID = ""
   tCoverageEffDate = ? 
   tCoverageCoverageType = ""
   tCoverageInsuredType = ""
   tCoverageInsuredName = ""
   tCoverageOrigLiability = 0
   tCoverageCurrLiability = 0   
   tCoverageFormID = ""
   tCoverageFormDesc = ""
   tCoverageFormType = ""
   
   tInsuredName = ""
   tInsuredAddr = ""
   .
   
 for each claimcoverage no-lock
   where claimcoverage.claimID = claim.claimID:
     /*and claimcoverage.isPrimary = true:*/

   case claimcoverage.coverageType:
    when "O" then
    do:
       assign
         tOwnerPolicyID = claimcoverage.coverageID
         tOwnerPolicyEffDate = claimcoverage.effDate
         tOwnerPolicyInsuredType = claimcoverage.insuredType
         tOwnerPolicyInsuredName = claimcoverage.insuredName
         tOwnerPolicyOrigLiability = claimcoverage.origLiabilityAmount
         tOwnerPolicyCurrLiability = claimcoverage.currentLiabilityAmount
         .
    end.
    when "L" then
    do:
       assign
         tLenderPolicyID = claimcoverage.coverageID
         tLenderPolicyEffDate = claimcoverage.effDate
         tLenderPolicyInsuredType = claimcoverage.insuredType
         tLenderPolicyInsuredName = claimcoverage.insuredName
         tLenderPolicyOrigLiability = claimcoverage.origLiabilityAmount
         tLenderPolicyCurrLiability = claimcoverage.currentLiabilityAmount
         .
    end.
    otherwise
    do:
       assign
         tCoverageID = claimcoverage.coverageID
         tCoverageEffDate = claimcoverage.effDate 
         tCoverageCoverageType = claimcoverage.coverageType
         tCoverageInsuredType = claimcoverage.insuredType
         tCoverageInsuredName = claimcoverage.insuredName
         tCoverageOrigLiability = claimcoverage.origLiabilityAmount
         tCoverageCurrLiability = claimcoverage.currentLiabilityAmount   
         .
    end.
   end case.
     
   std-in = integer(claimcoverage.coverageID) no-error.
   if not error-status:error 
    then
   for first policy no-lock
     where policy.policyID = std-in:
    
      case claimcoverage.coverageType:
        when "O" then
        do:
           if tOwnerPolicyEffDate = ? 
            then tOwnerPolicyEffDate = policy.effDate.
           
           if tOwnerPolicyOrigLiability = 0 or tOwnerPolicyOrigLiability = ?
            then tOwnerPolicyOrigLiability = policy.liabilityAmount.
        end.
        when "L" then
        do:
           if tLenderPolicyEffDate = ? 
            then tLenderPolicyEffDate = policy.effDate.
           
           if tLenderPolicyOrigLiability = 0 or tLenderPolicyOrigLiability = ?
            then tLenderPolicyOrigLiability = policy.liabilityAmount.
        end.
        otherwise
        do:
           if tCoverageEffDate = ? 
            then tCoverageEffDate = policy.effDate.
           
           if tCoverageOrigLiability = 0 or tCoverageOrigLiability = ?
            then tCoverageOrigLiability = policy.liabilityAmount.
        end.
      end case.

      for first stateform no-lock
        where stateform.stateID = policy.stateID
          and stateform.formID = policy.formID:

        case claimcoverage.coverageType:
          when "O" then
          do:
             assign
               tOwnerPolicyFormID = stateform.formID
               tOwnerPolicyFormDesc = stateform.description
               tOwnerPolicyFormType = stateform.formtype
               .
          end.
          when "L" then
          do:
             assign
               tLenderPolicyFormID = stateform.formID
               tLenderPolicyFormDesc = stateform.description
               tLenderPolicyFormType = stateform.formtype
               .
          end.
          otherwise
          do:
             assign
               tCoverageFormID = stateform.formID
               tCoverageFormDesc = stateform.description
               tCoverageFormType = stateform.formtype
               .
          end.
        end case.
      end.
   end.
 end.

 create data.
 assign
   data.stateID = claim.stateID
   data.agentID = claim.agentID
   data.agentName = ""                  /* Set client side as needed */
   data.claimID = claim.claimID
   data.claimIDDesc = ""
   data.stat = claim.stat
   data.fileNumber = claim.fileNumber
   data.altaRiskCode = claim.altaRisk
   data.altaRiskDesc = ""               /* Set client side as needed */
   data.altaResponsibilityCode = claim.altaResp
   data.altaResponsibilityDesc = ""     /* Set client side as needed */
   data.causeCode = tCause
   data.coCause = ""                    /* Set client side as needed */
   data.descriptionCode = tDesc
   data.coDescription = ""              /* Set client side as needed */
   data.dateOpened = claim.dateCreated
   data.yearOpened = year(data.dateOpened)
   data.assignedTo = claim.assignedTo
   data.summary = claim.summary
   
   data.ownerPolicyID = tOwnerPolicyID
   data.ownerPolicyEffDate = tOwnerPolicyEffDate
   data.ownerPolicyInsuredType = tOwnerPolicyInsuredType
   data.ownerPolicyInsuredName = tOwnerPolicyInsuredName
   data.ownerPolicyOrigLiability = tOwnerPolicyOrigLiability
   data.ownerPolicyCurrLiability = tOwnerPolicyCurrLiability
   data.ownerPolicyFormID = tOwnerPolicyFormID
   data.ownerPolicyFormDesc = tOwnerPolicyFormDesc
   data.ownerPolicyFormType = tOwnerPolicyFormType

   data.lenderPolicyID = tLenderPolicyID
   data.lenderPolicyEffDate = tLenderPolicyEffDate
   data.lenderPolicyInsuredType = tLenderPolicyInsuredType
   data.lenderPolicyInsuredName = tLenderPolicyInsuredName
   data.lenderPolicyOrigLiability = tLenderPolicyOrigLiability
   data.lenderPolicyCurrLiability = tLenderPolicyCurrLiability
   data.lenderPolicyFormID = tLenderPolicyFormID
   data.lenderPolicyFormDesc = tLenderPolicyFormDesc
   data.lenderPolicyFormType = tLenderPolicyFormType

   data.coverageID = tCoverageID
   data.coverageEffDate = tCoverageEffDate
   data.coverageCoverageType = tCoverageCoverageType
   data.coverageInsuredType = tCoverageInsuredType
   data.coverageInsuredName = tCoverageInsuredName
   data.coverageOrigLiability = tCoverageOrigLiability
   data.coverageCurrLiability = tCoverageCurrLiability
   data.coverageFormID = tCoverageFormID
   data.coverageFormDesc = tCoverageFormDesc
   data.coverageFormType = tCoverageFormType
   
   data.insuredName = claim.insuredName
   data.insuredAddr = ""
   data.description = claim.description
   data.lastNoteDate = tNoteDate
   data.noteCnt = tNoteCnt   

   data.stage = claim.stage
   data.type = claim.type
   data.action = claim.action
   data.difficulty = claim.difficulty
   data.urgency = claim.urgency
   data.litigation = can-find(first claimattr 
                              where claimattr.claimID = claim.claimID
                              and claimattr.attrCode = "CA9"
                              and claimattr.attrName = "Litigation"
                              and lookup(claimattr.attrValue,"Yes,Y,True") <> 0)
   data.significant = can-find(first claimattr 
                               where claimattr.claimID = claim.claimID
                               and claimattr.attrCode = "CA8"
                               and claimattr.attrName = "Significant"
                               and lookup(claimattr.attrValue,"Yes,Y,True") <> 0)
   data.agentError = claim.agentError
   data.searcherError = claim.searcherError
   data.attorneyError = claim.attorneyError
   .

/*   COPY-LOB FROM tNoteText TO data.lastNoteText. */
  data.lastNoteText = tNoteText.

 for first xclaimacct
   where xclaimacct.claimID = claim.claimID
     and xclaimacct.refCategory = "E":
  data.laeOpen = xclaimacct.pendingInvoiceAmount.
  data.laeApproved = xclaimacct.approvedInvoiceAmount.
  data.laeLTD = xclaimacct.completedInvoiceAmount.
  data.laeReserve = xclaimacct.reserveBalance.
  data.laeAdjOpen = xclaimacct.pendingReserveAmount.
  data.laeAdjLTD = xclaimacct.approvedReserveAmount.
 end.

 for first xclaimacct
   where xclaimacct.claimID = claim.claimID
     and xclaimacct.refCategory = "L":
  data.lossOpen = xclaimacct.pendingInvoiceAmount.
  data.lossApproved = xclaimacct.approvedInvoiceAmount.
  data.lossLTD = xclaimacct.completedInvoiceAmount.
  data.lossReserve = xclaimacct.reserveBalance.
  data.lossAdjOpen = xclaimacct.pendingReserveAmount.
  data.lossAdjLTD = xclaimacct.approvedReserveAmount.
 end.

 tCnt = tCnt + 1.
end.

if tCnt > 0 and temp-table data:has-records
 then 
   pResponse:setParameter(input dataset-handle dsHandle).

pResponse:success("2005", string(tCnt) {&msg-add} "Claim").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


