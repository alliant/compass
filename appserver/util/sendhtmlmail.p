&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@name sendhtmlmail.p
@description Sends an email from an HTML email

@param HtmlFile;char;The HTML file
@param From;char;The person to send the email from
@param To;char;The person to send the email to
@param Subject;char;The subject line of the email
@param Attachment;char;The attachments for for the email
@param iData;complex;Temp table of the mail parameters
@param Success;logical;True if sending the email was sent successfully

@author John Oliver
@version 1.0
@created 09/15/2020
@modified
05/05/2022  SC  Task:80191 Modified to fix HTML file failure email
----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{service/iData.i}

/* parameters */
define input  parameter pHtmlFile   as character no-undo.
define input  parameter pFrom       as character no-undo.
define input  parameter pTo         as character no-undo.
define input  parameter pSubject    as character no-undo.
define input  parameter pAttachment as character no-undo.
define input  parameter table       for iData.
define output parameter pSuccess    as logical   no-undo initial false.

/* variables */
define variable htmlLine     as character no-undo.
define variable cBody        as character no-undo initial "".
define variable startComment as logical   no-undo initial false.
define variable cAttachList  as character no-undo.
define variable cPlaceholder as character no-undo.

{lib/add-delimiter.i}
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

file-info:file-name = pHtmlFile.
pHtmlFile = file-info:full-pathname.

if search(pHtmlFile) <> ?
 then
  do:
    input from value(pHtmlFile).
    repeat:
      import unformatted htmlLine.
      
      /* we want to ignore html comments */
      if index(htmlLine, "<!--") > 0
       then startComment = true.
      
      

      if not startComment
       then
        do:
          for each iData no-lock:
            assign
              cPlaceholder = "~{" + iData.objProperty + "~}"
              htmlLine     = replace(htmlLine, cPlaceholder, iData.propertyValue)
              .
          end.
          /* HTML file failure email */
          cBody = cBody + htmlLine + "~n".
          if htmlLine matches "*~{*~}*"
           then
            do:
              assign
                std-in = index(htmlLine, "~{")
                std-ch = substring(htmlLine, std-in, index(htmlLine, "~}") - (std-in - 1))
                .
              run util/sysmail.p ("HTML file failure", "The HTML email " + pHtmlFile + " has a parameter " + std-ch + " and sent to " + pTo + ".").
            end.
        end.

      if index(htmlLine, "-->") > 0
       then startComment = false.
    end.
    input close.
    /* add the attachments */
    if pAttachment <> ? and pAttachment <> ""
     then
      do std-in = 1 to num-entries(pAttachment):
        if search(entry(std-in, pAttachment)) <> ?
         then cAttachList = addDelimiter(cAttachList, ",") + entry(std-in, pAttachment).
      end.
    publish "GetSystemParameter" ("AlliantLogo", output std-ch).
    cAttachList = addDelimiter(cAttachList, ",") + std-ch.

    /* Temporarily commenting out these lines for task#107623*/
    /*publish "GetSystemParameter" ("ApproveButton", output std-ch).
    cAttachList = addDelimiter(cAttachList, ",") + std-ch.
    publish "GetSystemParameter" ("DenyButton", output std-ch).
    cAttachList = addDelimiter(cAttachList, ",") + std-ch.*/

    run util/attachmail.p (pFrom, pTo, "", pSubject, cBody, cAttachList).
    pSuccess = true.
  end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


