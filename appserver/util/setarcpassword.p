&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* sys/setarcpassword.p
   @param iUser;UserID to reset password
   @param iPw;New Password
   @author D.Sinclair
   @notes This procedure relies on the following system parameters:
            ARCPasswordService,ARCAdminID,ARCAdminPw
            
   09.11.2015 jpo : Modified to split the iUser variable if possible and
                    try both to send the email then the user to the ARC as
                    the user may have either usernames.
 */

def input parameter iUser as char.
def input parameter iPw as char.
def output parameter pSuccess as logical init true.
def output parameter pMsg as char.

def var tAdminID as char no-undo.
def var tAdminPW as char no-undo.
def var tHeaders as char no-undo.
def var tInputFile as char no-undo.
def var tOutputFile as char no-undo.

def var tURL as char.
def var tReplaceURL as char.
def var tName as char no-undo.
def var tResult as longchar.
def var tUser as char no-undo.
def var tEmail as char no-undo.

{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 

/* split the iUser variable if delimited by |@| */
/* Note: a user can still have an email adress or user id with |@| */
/*       in the name through it is very unlikely */
if index(iUser, {&msg-dlm}) > 0
 then
  do:
    assign
      tUser = entry(1, iUser, {&msg-dlm})
      tEmail = entry(2, iUser, {&msg-dlm})
      .
  end.
else
  tUser = iUser.

publish "GetSystemParameter" ("ARCPasswordService", output tURL).
  if tURL = "" 
   then return.

  publish "GetSystemParameter" ("ARCAdminID", output tAdminID).
  publish "GetSystemParameter" ("ARCAdminPW", output tAdminPW).

  tName = replace(guid, "-", "") + "Arc".

  /* Create the JSON file based on the ARC API */
  tInputFile = tName + "DAT".
  output to value(tInputFile) page-size 0.
  put unformatted '~{Auth:~{Username:"' + tAdminID + '"'
        + ',Password:"' + tAdminPW + '"~}' 
        + ',NewPassword:"' + iPw + '"~}'.
  output close.
  tInputFile = search(tInputFile).
  
  assign
    pSuccess = false
    pMsg = ""
    .
    
  /* first test with the email address if available */
  if tEmail <> "" or tEmail <> ?
   then
    do:
      tReplaceURL = replace(tURL, "&1", tEmail).  
      run util/httppost.p (tName,
                           tReplaceURL, 
                           "", /* Headers */ 
                           "application/json", 
                           tInputFile, 
                           output pSuccess,
                           output pMsg,
                           output tOutputFile).
  
      if pSuccess
       then
        do:            
            copy-lob file tOutputFile to tResult.
            pSuccess = (index(replace(tResult, " ", ""), '"Code":"2000"') > 0).
        end.
    end.
  else
    pSuccess = false.
    
  /* then test with the username */
  if not pSuccess
   then
    do:
      tReplaceURL = replace(tURL, "&1", tUser).  
      run util/httppost.p (tName,
                           tReplaceURL, 
                           "", /* Headers */ 
                           "application/json", 
                           tInputFile, 
                           output pSuccess,
                           output pMsg,
                           output tOutputFile).
  
      if pSuccess
       then
        do:            
            copy-lob file tOutputFile to tResult.
            pSuccess = (index(replace(tResult, " ", ""), '"Code":"2000"') > 0).
        end.
    end.

  publish "GetSystemParameter" ("DebugModeEnabled", output std-lo). /* Server */
  publish "GetAppDebug" (output std-lo). /* Client */
  if not std-lo 
   then os-delete value(tOutputFile).
  
  if not pSuccess 
   then pMsg = "ARC password update failed".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


