/*------------------------------------------------------------------------
    File        : util/getpwdaystoexpire.p
    Purpose     : Sets the passwordExpired flag for the user to true or false

    Syntax      : OpenEdge Progress 10.2B
    
    Parameter   : pUid;char;The uid of the user
    Parameter   : pExpired;logical;True if the password is expired

    Author(s)   : John Oliver (joliver)
    Created     : 09.16.2015
    Notes       :
  ----------------------------------------------------------------------*/
def input parameter pUid as char no-undo.
def input parameter pExpired as logical no-undo.
def var tMsg as char no-undo.
def var tEmail as char no-undo.
def var tNewPass as char no-undo.

def var tAlfresco as logical init false no-undo.
def var tSharefile as logical init false no-undo.
def var tARC as logical init false no-undo.
{lib/std-def.i}

FOR FIRST sysuser EXCLUSIVE-LOCK
  WHERE sysuser.uid = pUid:
 ASSIGN
   sysuser.passwordExpired = pExpired
   tEmail = sysuser.email
   .
 
 if pExpired
  then 
    do:
      publish "GetSystemParameter" ("ExpiredPassword", output tNewPass).
        
      run util/setsharefilepassword.p (tEmail, sysuser.password, tNewPass, output tSharefile, output std-ch).

      run util/setarcpassword.p (pUid {&msg-add} tEmail, tNewPass, output tARC, output std-ch).
    end.
 RELEASE sysuser.
END.

