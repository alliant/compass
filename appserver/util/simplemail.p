/* simplemail.p
   D.Sinclair
   4.9.2012
@Modified
Date        Name    Comments
10/29/2024  K.R     Modififed to convert body to be a longchar to support large body   
   */ 
 
 def input parameter pFrom as char.
 def input parameter pTo as char.
 def input parameter pSubject as char.
 def input parameter pBody as longchar.
 def var tSent as logical.
 def var tMsg as char.
 def var tServer as char.
 def var tDomain as char no-undo.

 publish "GetSystemParameter" ("SmtpServer", output tServer).
 if tServer = "" 
  then return.
 
 publish "GetSystemParameter" ("emailDomain", output tDomain).
 if tDomain = "" 
  then return.

 if (pFrom > "" and index(pFrom, tDomain) = 0)
   or pFrom = ""
  then publish "GetSystemParameter" ("emailDefaultFrom", output pFrom).
 if pFrom = "" 
  then return.

 run util/smtpmail.p
   (tServer,
    pTo,
    pFrom,
    "",
    "",
    "",
    pSubject,
    pBody,
    "type=text/html:charset=us-ascii:filetype=ascii",
    "",
    1,
    false,
    "",
    "",
    "",
    output tSent,
    output tMsg).
