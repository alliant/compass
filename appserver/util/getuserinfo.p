&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* getuserinfo.p
   return the INFOrmation for a USER
   CKM 11.02.2013
   JPO 08.25.2013 Modified to get the user's email address
   JPO 09.XX.2013 Modified to get the passwordSetDate and
                  passwordExpired fields. If the fields
                  are NULL, then the password needs to be 
                  changed.
 */
 
DEFINE INPUT PARAMETER iUid AS CHAR NO-UNDO.
DEFINE OUTPUT PARAMETER iUserName AS CHAR init "" NO-UNDO.
DEFINE OUTPUT PARAMETER iUserInitials AS CHAR init "" NO-UNDO.
DEFINE OUTPUT PARAMETER iUserRole AS CHAR init "" NO-UNDO.
DEFINE OUTPUT PARAMETER iUserPass AS CHAR init "" NO-UNDO.
DEFINE OUTPUT PARAMETER iUserActive AS LOGICAL init false NO-UNDO.
DEFINE OUTPUT PARAMETER iUserEmail AS CHAR INIT "" NO-UNDO.
DEFINE OUTPUT PARAMETER pPasswordSetDate AS DATETIME NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 

FOR FIRST sysuser NO-LOCK
  WHERE sysuser.uid = iUid:
 ASSIGN
  iUserName = sysuser.name
  iUserInitials = sysuser.initials
  iUserRole = sysuser.role
  iUserPass = sysuser.password
  iUserActive = sysuser.isactive
  iUserEmail = sysuser.email
  pPasswordSetDate = sysuser.passwordSetDate
  .
END.
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


