/*------------------------------------------------------------------------
@name  arpaymentpdf.p
@description Creates the payment PDF

@author Rahul Sharma
@version 1.0
@created 07/21/2020
@modified 12/17/2020 Layout Changes. 
----------------------------------------------------------------------*/
{tt/ledgerReport.i &tableAlias="glPayment"}

define input  parameter ipcReportAction     as character  no-undo.
define input  parameter table               for glPayment. 
define output parameter opcFilename         as character no-undo.

{lib/rpt-def.i &ID="'arPayment'" &rptOrientation="'Landscape'" &logo=images\alliantblue.jpg &blockTitleColor=169169169}
{lib/std-def.i}
{lib/ar-def.i}

/* Local Variables */
define variable xVal          as integer  initial 477  no-undo.
define variable iCount        as integer               no-undo.
define variable dePmtTotal    as decimal               no-undo.
define variable cPostedBy     as character             no-undo.
define variable lShowHeader   as logical               no-undo.
define variable dtPostDate    as date                  no-undo.

/* Function Prototype */
FUNCTION setxPos RETURNS INTEGER
  ( input ixPos as integer,
    input lAdd as logical )  FORWARD.

FUNCTION calculateAmount RETURNS DECIMAL
  ( input ipcAgentID  as character,
    input ipcAcctType as character )  FORWARD.    
    
assign
  activeFont = "Helvetica"
  activeSize = 12.0
  .

/* Get commonly used variables */  
iCount = 0.
for each glPayment:
  if iCount < 1
   then
    assign
        cPostedBy  = glPayment.ledgerCreateBy 
        dtPostDate = glPayment.ledgerCreateDate
        .
          
  assign
      dePmtTotal = dePmtTotal + glPayment.debit
      iCount     = iCount + 1
      .
end.

run getFilename in this-procedure no-error.
if error-status:error
 then return.

activeFilename = opcFilename.   
  
{lib/rpt-open.i &tempID='arPayment' }  
  
run setMainPmtHead    in this-procedure.
run setPaymentDetails in this-procedure.
run setAccountSummary in this-procedure.
  
if ipcReportAction = {&agentdefault}
 then
  do:
     {lib/rpt-cloz.i &tempID='arPayment' &no-save=true &exclude-pdfview=true}
  end.
 else if ipcReportAction = {&view} 
  then
   do:
     {lib/rpt-cloz.i &tempID='arPayment' &no-save=true }
   end.


PROCEDURE getFilename :
  if ipcReportAction = {&agentdefault} 
   then
    std-ch = session:temp-directory.
   else if ipcReportAction = {&view} 
    then
     publish "getReportDir" (output std-ch).
    
  opcFilename = std-ch  + (if dtPostDate <> ? then "PostedPayment_" else "PrelimPayment_") + replace(string(today), "/", "") + "_" + replace(string(time,"HH:MM:SS"),":","") + ".pdf".

  if ipcReportAction = {&view} 
   then
    publish "SetCurrentValue" ("filename", opcFilename).
END PROCEDURE.


PROCEDURE pmtDetailHeader :
  define input parameter iPos as integer no-undo.
  
  /* GL Header */
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
  run pdf_rect({&rpt}, 10, iPos + 17, 772, 24, 0.5).  
  
  run pdf_set_font({&rpt}, "Helvetica-bold", 11).
  run pdf_text_color({&rpt}, 1, 1, 1).
  run pdf_text_xy({&rpt}, "Agent ID", 20, iPos + 26).    
  run pdf_text_xy({&rpt}, "Name", 80, iPos + 26).
  run pdf_text_xy({&rpt}, "Account Number", 330, iPos + 26).
  run pdf_text_xy({&rpt}, "Description", 460, iPos + 26).
  run pdf_text_xy({&rpt}, "Debit", 633, iPos + 26).
  run pdf_text_xy({&rpt}, "Credit", 736, iPos + 26).
  
  /* Payment Header */                      
  run pdf_set_font({&rpt}, "Helvetica-boldoblique", 11).
  run pdf_text_color({&rpt}, 0, 0, 0).
  
  if dtPostDate <> ? 
   then
    do:
      run pdf_text_xy({&rpt}, "Check Number", 140, iPos - 3).
      run pdf_text_xy({&rpt}, "Posting ID", 230, iPos - 3).
      run pdf_text_xy({&rpt}, "Posting Date", 295, iPos - 3).      
      run pdf_text_xy({&rpt}, "Description", 375, iPos - 3). 
      run pdf_text_xy({&rpt}, "File", 510, iPos - 3).
    end.
   else
    do:
      run pdf_text_xy({&rpt}, "Check Number", 140, iPos - 3).
      run pdf_text_xy({&rpt}, "Description", 240, iPos - 3). 
      run pdf_text_xy({&rpt}, "File", 455, iPos - 3).
    end. 
    
  run pdf_text_xy({&rpt}, "Check Amount", 570, iPos - 3).
  /* linestrock */
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "0,0,0").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "0,0,0").
  run pdf_rect({&rpt}, 130, iPos - 11, 530, 0.10, 0.5).
   
  run pdf_text_color({&rpt}, 0, 0, 0).  
END PROCEDURE.

 
PROCEDURE reportFooter :
  run pdf_text_color ({&rpt}, 0.0, 0.0, 0.0).
  run pdf_set_font ({&rpt}, "Courier", 7.0).
  
  run pdf_text_xy ({&rpt}, "Printed:", 20, 14).
  run pdf_text_xy ({&rpt}, string(now , "99/99/9999 HH:MM:SS" ), 55, 14).
 
  /* Line added for "Posted By" */
  if cPostedBy > ""
   then
    run pdf_text_xy ({&rpt}, "Posted: " + cPostedBy + " " + string(dtPostDate), 390, 14). 
 
  run pdf_text_xy ({&rpt}, string(iPage, "zz9"), 
                    if rptOrientation = "Landscape" then 765 else 580, 
                    14).
                     
  setFont(activeFont, activeSize).
END PROCEDURE.

 
PROCEDURE reportHeader :
  def var tTitle as char no-undo.
  def var tXpos as int no-undo.
  tXpos = 400 - (length(tTitle) / 2 * 6). /* there are 6 points in one character and 400 points is the middle of the header */
  {lib/rpt-hdr.i &title=tTitle &title-x=tXpos &title-y=740}
END PROCEDURE.

 
PROCEDURE setAccountSummary : 
  define variable deDebit  as decimal no-undo.
  define variable deCredit as decimal no-undo.
  
  setxPos(30,no).
  
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").   
  run pdf_rect({&rpt}, 10, setxPos(0,no), 772, 24, 0.5). 
  
  run pdf_set_font({&rpt}, "Helvetica-bold", 11).    
  run pdf_text_color({&rpt}, 1, 1, 1).
  run pdf_text_xy({&rpt}, "Summary", 20, setxPos(0,no) + 9).
  
  /* Summary Sub Header */
  run pdf_set_font({&rpt}, "Helvetica-boldoblique", 11).
  run pdf_text_color({&rpt}, 0, 0, 0).
  run pdf_text_xy({&rpt}, "Account Number", 330, setxPos(20,no)).
  run pdf_text_xy({&rpt}, "Description", 460, xVal).
  run pdf_text_xy({&rpt}, "Debit", 633, xVal).
  run pdf_text_xy({&rpt}, "Credit", 736, xVal).
  /* linestrock */
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "0,0,0").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "0,0,0").
  run pdf_rect({&rpt}, 320, xVal - 9 , 461, 0.10, 0.5).
  setxPos(12,no).
    
  setFont("Helvetica", 9.0).        
  for each glPayment break by glPayment.glRef:
    
    accumulate glPayment.debit (total by glPayment.glRef).
    accumulate glPayment.credit (total by glPayment.glRef).
    
    if last-of(glPayment.glRef)
     then
      do:
        setxPos(15,no).
        run pdf_text_xy({&rpt}, glPayment.glRef, 330, setxPos(0,no)). 
        run pdf_text_xy({&rpt}, glPayment.glDesc, 460, setxPos(0,no)). 
        
        assign
            deDebit  = accum total by glPayment.glRef glPayment.debit
            deCredit = accum total by glPayment.glRef glPayment.credit
            .
        
        if deDebit > 0
         then
          run pdf_text_align({&rpt}, string(deDebit, ">>>,>>>,>>>,>>9.99"), "RIGHT", 660, setxPos(0,no)).
          
        if deCredit > 0
         then
          run pdf_text_align({&rpt}, string(deCredit, ">>>,>>>,>>>,>>9.99"), "RIGHT", 768, setxPos(0,no)).
      end.       
  end.
END PROCEDURE.


PROCEDURE setMainPmtHead :
  run pdf_skipn ({&rpt},1).
  
  if dtPostDate <> ?
   then
    do:
      /* Payment label on top right */
      setFont("Helvetica-Bold", 14.0).
      run pdf_text_xy({&rpt}, "POSTED PAYMENT JOURNAL", 550, 560). 
      run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
      run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
      
      /*--------------Payment Total Amount--------------*/
      setFont("Helvetica-Bold", 11.0).
      run pdf_text_xy ({&rpt}, "Total Amount:", 550, /*510*/ 525). 
      run pdf_text_align({&rpt}, string(dePmtTotal,">>>,>>>,>>>,>>9.99"),"RIGHT", 692, 525).
    end.
   else
    do:
      /* Payment label on top right */
      setFont("Helvetica-Bold", 14.0).
      run pdf_text_xy({&rpt}, "PRELIMINARY PAYMENT JOURNAL", 530, 560). 
      run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
      run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
      
      /*--------------Payment Total Amount--------------*/
      setFont("Helvetica-Bold", 11.0).
      run pdf_text_xy ({&rpt}, "Total Amount:", 530, 525). 
      run pdf_text_align({&rpt}, string(dePmtTotal,">>>,>>>,>>>,>>9.99"),"RIGHT", 672, 525).
    end.
END PROCEDURE.


PROCEDURE setPaymentDetails :
  
  run pmtDetailHeader in this-procedure (input 475).
  
  setxPos(18,no).
  for each glPayment break by glPayment.agentID by glPayment.actionid by glPayment.glRef:
    lShowHeader = true.
    run pdf_text_color({&rpt}, 0, 0, 0).
       
    if first-of(glPayment.agentID)
     then
      do: 
        iCount = 0.
        setxPos(10,no).
        setFont("Helvetica", 9.0).
        run pdf_text_xy({&rpt}, glPayment.agentID, 20, setxPos(0,no)).
        
        if length(glPayment.agentName) gt 46 
         then
          do:
            run pdf_text_xy ({&rpt}, substring(glPayment.agentName,1,46), 80, setxPos(0,no)). 
            run pdf_text_xy ({&rpt}, substring(glPayment.agentName,47), 80, setxPos(0,no) - 10).
          end.
         else
          run pdf_text_xy ({&rpt}, glPayment.agentName, 80, setxPos(0,no)).          
      end.
      
    if first-of(glPayment.glRef) and (iCount < 2)   
     then
      do: 
        if iCount > 0
         then setxPos(15,no).
                
        setFont("Helvetica", 9.0).
        run pdf_text_xy({&rpt}, glPayment.glRef, 330, setxPos(0,no)).
        run pdf_text_xy({&rpt}, glPayment.glDesc, 460, setxPos(0,no)).
     
        if glPayment.debit > 0 
         then 
          run pdf_text_align({&rpt}, string(calculateAmount(input glPayment.agentID, input if glPayment.debit > 0 then "D" else "C"), ">>>,>>>,>>>,>>9.99"),"RIGHT", 660, setxPos(0,no)). /* Debit amount */
         else 
          run pdf_text_align({&rpt}, string(calculateAmount(input glPayment.agentID, input if glPayment.debit > 0 then "D" else "C"), ">>>,>>>,>>>,>>9.99"),"RIGHT", 768, setxPos(0,no)). /* Credit amount */
                
        iCount = iCount + 1.
        
        if iCount > 1
         then setxPos(10,no).
      end.
        
    if last-of(glPayment.actionid)
     then
      do:
        setxPos(15,no).
        setFont("Helvetica", 9.0).
        
        run pdf_text_xy({&rpt}, glPayment.checkNum, 140, setxPos(0,no)).
        
        if glPayment.ledgerID > 0
         then
          do:       
            run pdf_text_align({&rpt}, glPayment.ledgerID,"RIGHT", 282, setxPos(0,no)).
            run pdf_text_align({&rpt}, date(glPayment.transDate),"RIGHT", 362, setxPos(0,no)). 
            if length(glPayment.description) gt 25 
             then
              run pdf_text_xy ({&rpt}, substring(glPayment.description,1,25), 375, setxPos(0,no)).
             else
              run pdf_text_xy({&rpt}, glPayment.description, 375, setxPos(0,no)).
            run pdf_text_xy({&rpt}, glPayment.filenumber, 510, setxPos(0,no)).
          end.
         else
          do:
            if length(glPayment.description) gt 38 
             then
              run pdf_text_xy ({&rpt}, substring(glPayment.description,1,38), 240, setxPos(0,no)). 
             else 
              run pdf_text_xy({&rpt}, glPayment.description, 240, setxPos(0,no)).
            run pdf_text_xy({&rpt}, glPayment.filenumber, 455, setxPos(0,no)).
          end.       
        
        run pdf_text_align({&rpt}, string(glPayment.checkAmt, ">>>,>>>,>>>,>>9.99"), "RIGHT", 647, setxPos(0,no)).
      end.
    
    if last-of(glPayment.agentID)
     then
      do:          
        setxPos(10,no).
        run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
        run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
        run pdf_line ({&rpt},20,setxPos(0,no), 772,setxPos(0,no),2).        
        setxPos(6,no).
      end.    
  end.
  lShowHeader = false.    
END PROCEDURE.

/* ************************  Function Implementations ***************** */

FUNCTION calculateAmount RETURNS DECIMAL
  ( input ipcAgentID as character,
    input ipcAcctType as character ) :
 
  define variable deAmount as decimal no-undo.
  
  define buffer glPayment for glPayment.
  
  for each glPayment where glPayment.agentID = ipcAgentID:   
    deAmount  = deAmount + if ipcAcctType = "D" then glPayment.debit else glPayment.credit.
  end.   
    
  return deAmount.  /* Function return value. */

END FUNCTION.
   
   
FUNCTION setxPos RETURNS INTEGER
  ( input ixPos as integer,
    input lAdd as logical ) :
 
  if xVal le (ixPos + 38)
   then
    do:
      run pdf_new_page2({&rpt}, "Landscape").
      if lShowHeader
       then
        do:
          run pmtDetailHeader in this-procedure(input 490).
          xVal = 475.
        end.
       else
        xVal = 520.
    end.
    
  if lAdd
   then
    xval = xval +  ixPos.
   else
    xval = xval -  ixPos.   
    
  return xval.   /* Function return value. */

END FUNCTION.
