/*------------------------------------------------------------------------
File        : util/CommonUtils.cls
Purpose     : A utility class having common utility method

Author(s)   : K.R
Created     : 09/01/2023
Notes       :

Modification:  
   K.R       09/12/2023      Added new method to validate email address  
   K.R       02/22/2024      Modified valid email address regex   
   K.R       07/18/2024      Modified to add ,ethod to conver unix EPOCH time to progress date-time and date -time tz                  
----------------------------------------------------------------------*/
USING System.Text.RegularExpressions.*. 
CLASS util.CommonUtils :


  CONSTRUCTOR CommonUtils ():
  END CONSTRUCTOR.

  METHOD PUBLIC STATIC CHARACTER EncodeURL (INPUT cValue AS CHARACTER):
      /**************************************************************************** 
    Description: Encodes unsafe characters in a URL as per RFC 1738 section 2.2. 
    <URL:http://ds.internic.net/rfc/rfc1738.txt>, 2.2 
    Input Parameters: Character string to encode, Encoding option where "query", 
    "cookie", "default" or any specified string of characters are valid. 
    In addition, all characters specified in the variable cUnsafe plus ASCII values 0 <= x <= 31 and 127 <= x <= 255 are considered unsafe. 
    Returns: Encoded string (unkown value is returned as blank) 
    Variables: cUnsafe, cReserved 
    ****************************************************************************/ 
    DEFINE VARIABLE cHex        AS CHARACTER   NO-UNDO INITIAL "0123456789ABCDEF":U. 
    DEFINE VARIABLE cEncodeList AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE cEnctype    AS CHARACTER   NO-UNDO INITIAL "query".
    DEFINE VARIABLE iCounter    AS INTEGER     NO-UNDO. 
    DEFINE VARIABLE cChar       AS INTEGER     NO-UNDO. 
    
    /* Unsafe characters that must be encoded in URL's.  See RFC 1738 Sect 2.2. */
    DEFINE VARIABLE cUnsafe   AS CHARACTER NO-UNDO INITIAL " <>~"#%~{}|~\^~~[]`":U.
    
    /* Reserved characters that normally are not encoded in URL's */
    DEFINE VARIABLE cReserved AS CHARACTER NO-UNDO INITIAL "~;/?:@=&":U.
    
    /* Don't bother with blank or unknown */ 
    IF LENGTH(cValue) EQ 0 OR 
       cValue         EQ ? THEN 
      RETURN "". 
    
    /* What kind of encoding should be used? */ 
    CASE cEnctype: 
      WHEN "query":U THEN /* QUERY_STRING name=value parts */ 
        cEncodeList = cUnsafe + cReserved + "+":U. 
      WHEN "cookie":U THEN /* Persistent Cookies */ 
        cEncodeList = cUnsafe + " ,~;":U. 
      WHEN "default":U OR WHEN "" THEN /* Standard URL encoding */ 
        cEncodeList = cUnsafe. 
      OTHERWISE 
        cEncodeList = cUnsafe + cEnctype. /* user specified ... */ 
    END CASE. 
    
    /* Loop through entire input string */ 
    iCounter = 0. 
    DO WHILE TRUE: 
      ASSIGN iCounter = iCounter + 1 
             /* ASCII value of character using single byte codepage */ 
             cChar = ASC(SUBSTRING(cValue, iCounter, 1, "RAW":U), 
                         "1252":U, 
                         "1252":U). 
    
      IF cChar LE 31  OR 
         cChar GE 127 OR 
         INDEX(cEncodeList, CHR(cChar)) GT 0 THEN DO: 
        /* Replace character with %hh hexidecimal triplet */ 
        SUBSTRING(cValue, iCounter, 1, "RAW":U) = "%":U + 
          SUBSTRING(cHex, INTEGER(TRUNCATE(cChar / 16, 0)) + 1, 1, "RAW":U) + /* high */ 
          SUBSTRING(cHex, cChar MODULO 16 + 1, 1, "RAW":U). /* low digit */ 
    
        iCounter = iCounter + 2. /* skip over hex triplet just inserted */ 
      END. 
      IF iCounter EQ LENGTH(cValue,"RAW":U) THEN LEAVE. 
    END. 
    
    RETURN cValue.
  END METHOD.

  METHOD PUBLIC STATIC LOGICAL isValidEmailAddress (INPUT pcEmailAddress AS CHARACTER):
    
    DEFINE VARIABLE oRegex AS Regex NO-UNDO.
    
    /* Regular expression for validating email address*/
    oRegex = NEW Regex ("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$").

    IF oRegex:IsMatch(pcEmailAddress)
     THEN
       RETURN TRUE.
    ELSE
      RETURN FALSE.
  END METHOD.
          
  method public static datetime ConvertEpochToDateTime (input iEpochTimeValue as int64):

    define variable dtEpochBaseDate     as datetime no-undo initial 1/1/1970.
    define variable dtConvertedDateTime as datetime no-undo.

    if iEpochTimeValue = 0 
     then return ?.
    
    dtConvertedDateTime = add-interval(dtEpochBaseDate, iEpochTimeValue, "SECONDS").

    return dtConvertedDateTime.
  end method.

  method public static datetime-tz ConvertEpochToDateTimeTZ (input iEpochTimeValue as int64):

    define variable dtEpochBaseDate     as datetime-tz no-undo initial 1/1/1970.
    define variable dtConvertedDateTime as datetime-tz no-undo.

    if iEpochTimeValue = 0 
     then return ?.
    
    dtConvertedDateTime = add-interval(dtEpochBaseDate, iEpochTimeValue, "MILLISECONDS").

    return dtConvertedDateTime.
  end method.

  DESTRUCTOR CommonUtils ():
  END DESTRUCTOR.
END CLASS.
