/*------------------------------------------------------------------------
@name qualificationstatusfix.p
@action fixQualificationStatus
@description Modify qualification status for existing qualification
@throws 3000; Modify failed
@throws 3005; Success

@author : Rahul Sharma
@version 1.0
@created 
Date          Name           Description

----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

{lib/std-def.i}
{tt/qualification.i &tableAlias="tQualification"}
{tt/review.i        &tableAlias="tReview"}

define variable cMsg                  as character             no-undo.
define variable cLogMsg               as character             no-undo.
define variable lUpdateQualification  as logical initial true  no-undo.
define variable lUpdateReview         as logical initial true  no-undo.
define variable lFolder               as logical initial true  no-undo.
define variable cdir_name             as character             no-undo.
define variable iCountQual            as integer               no-undo.
define variable iCountReview          as integer               no-undo.

define stream strQual.

/* Get the directory and the filename prefix */
publish "GetSystemParameter" ("dataRoot", output cdir_name).

if cdir_name = "" then
do:
  cMsg = "Root Directory not found".
  lFolder = false.
end.

if lFolder then
do:
  assign cdir_name           = cdir_name + "qualification\"
         file-info:file-name = cdir_name.

  /* Check whether the qualification folder is already exist or not */
  if file-info:full-pathname eq ? then
  do:
    os-create-dir value(cdir_name).
    if os-error ne 0 then
    do:
      cMsg = "Failed to create folder 'qualification'".
      lFolder = false.
    end.
  end.
end.

if not (can-find(first qualification where qualification.stat  = "Good Standing")  or
        can-find(first qualification where qualification.stat  = "Expired")        or
        can-find(first qualification where qualification.stat  = "Cancelled")      or
        can-find(first qualification where qualification.stat  = "Probation")      or
        can-find(first qualification where qualification.stat  = "Suspended")      or
        can-find(first qualification where qualification.stat  = "Waived")         or
        can-find(first qualification where qualification.stat  = "Needs Review"))       
 then
  do:
    cMsg = cMsg + chr(10) + "Qualification records are already updated". 
    lUpdateQualification = false.
  end.

if not (can-find(first review where review.stat  = "Good Standing")  or
        can-find(first review where review.stat  = "Expired")        or
        can-find(first review where review.stat  = "Cancelled")      or
        can-find(first review where review.stat  = "Probation")      or
        can-find(first review where review.stat  = "Suspended")      or
        can-find(first review where review.stat  = "Waived")         or
        can-find(first review where review.stat  = "Needs Review"))       
 then
  do:
    cMsg = cMsg + chr(10) + "Review records are already updated". 
    lUpdatereview = false.
  end.

if lFolder 
 then
  do:  
    if lUpdateQualification  
     then
      do:
        /* Create qualification CSV file */
        cdir_name  = cdir_name + ( "qualification" + replace(string(today), "/", "") + "_" + string(time) + ".csv").
        
        output stream strQual to value(cdir_name).
        for each qualification no-lock:
      
          if qualification.stat ne "Good Standing" and
             qualification.stat ne "Expired"       and
             qualification.stat ne "Cancelled"     and
             qualification.stat ne "Probation"     and
             qualification.stat ne "Suspended"     and
             qualification.stat ne "Waived"        and
             qualification.stat ne "Needs Review" 
           then
            next.
      
          export stream strQual qualification.
      
          buffer qualification:handle:find-current (exclusive-lock,no-wait).
      
          case qualification.stat:
            when "Good Standing" then qualification.stat = "G".
            when "Expired"       then qualification.stat = "E".
            when "Cancelled"     then qualification.stat = "X".
            when "Probation"     then qualification.stat = "P".
            when "Suspended"     then qualification.stat = "S".
            when "Waived"        then qualification.stat = "W".
            when "Needs Review"  then qualification.stat = "N".
        
          end case.
          
          create tQualification.
          buffer-copy qualification to tQualification.
      
          buffer qualification:handle:find-current (no-lock).
      
          iCountQual = iCountQual + 1.
        end.
        output stream strQual close.
      end.

    if lUpdateReview  
     then
      do:
        /* Create review CSV file */
        cdir_name  = cdir_name + ( "review" + replace(string(today), "/", "") + "_" + string(time) + ".csv").

        output stream strQual to value(cdir_name).
        for each review no-lock:

          if review.stat ne "Good Standing" and
             review.stat ne "Expired"       and
             review.stat ne "Cancelled"     and
             review.stat ne "Probation"     and
             review.stat ne "Suspended"     and
             review.stat ne "Waived"        and
             review.stat ne "Needs Review" 
           then
            next.

          export stream strQual review.

          buffer review:handle:find-current (exclusive-lock,no-wait).

          case review.stat:
            when "Good Standing" then review.stat = "G".
            when "Expired"       then review.stat = "E".
            when "Cancelled"     then review.stat = "X".
            when "Probation"     then review.stat = "P".
            when "Suspended"     then review.stat = "S".
            when "Waived"        then review.stat = "W".
            when "Needs Review"  then review.stat = "N".

          end case.

          create treview.
          buffer-copy review to treview.

          buffer review:handle:find-current (no-lock).

          iCountReview = iCountReview + 1.
        end.
        output stream strQual close.
      end.

  end.

if not lFolder or
  (not lUpdateQualification and
   not lUpdateReview)
 then
  pResponse:fault("3005", cMsg).
 else
  do:
    if iCountQual > 0 or iCountReview > 0
     then 
      do:
        pResponse:setParameter("review",        table tReview).
        pResponse:setParameter("Qualification", table tQualification).
      end.
    pResponse:success("3005", string(iCountQual) + " Qualifications and " + string(iCountReview) + " Reviews were updated.").
  end.

cLogMsg = string(iCountQual) + " Qualifications and " + string(iCountReview) + " Reviews were updated.".

pResponse:logMessage("qualification", "", cLogMsg).
