/*------------------------------------------------------------------------
@name  util/CreateTaskForCCFiles.cls
@action CreateTaskForCCFiles
@description Create order review task for the files 
imported from Closers choice. 

----------------------------------------------------------------------*/

class util.CreateTaskForCCFiles inherits framework.ActionBase:
  {lib/nextkey-def.i} 

  define variable dsHandle         as handle      no-undo.
  define variable pcTopic          as character   no-undo.
  define variable pcQueue          as character   no-undo.
  define variable piAgentFileID    as integer     no-undo.
  define variable pdcost           as decimal     no-undo.
  define variable pcAssignedTo     as character   no-undo.
  define variable cErrMessage      as character   no-undo.
  define variable lSuccess         as logical     no-undo.
  define variable lcreatetask      as logical.
  define variable iTaskID          as integer     no-undo.
  define variable iFileTaskID      as integer     no-undo.
  define variable lReturnVal       as logical     no-undo. 
  define variable cErrorMsg        as character   no-undo.
  define variable pdtDueDate       as datetime-tz no-undo.
  define variable icount           as integer     no-undo.

   /* converting local time into utc datetime-tz  */ 
  define variable cTodayDateTime as datetime-tz no-undo. 
  define variable utcMinutes     as integer     no-undo.
  define variable utcMinutesdiff as integer     no-undo.

  define buffer bagentfile for agentfile.

  constructor public CreateTaskForCCFiles():
    super().
  end constructor.

  destructor public CreateTaskForCCFiles():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}

    MainBlock:
    for each bagentfile no-lock
      where  bagentfile.source = 'P'
        and (bagentfile.origin <> ?
         or  bagentfile.origin > '')
        //and agentfileid = 2023556
        :

      if can-find(first filetask where filetask.agentfileid = bagentfile.agentfileid)
        then next MainBlock.

      piAgentFileID = bagentfile.agentfileid.
      pdtDueDate = today  + 5.
      
      pcTopic = "Order Review".
      pcQueue = "Production".
      pcAssignedTo = ''.
      pdcost = 0.

      assign
          cTodayDateTime  = now
          utcMinutes      = timezone(cTodayDateTime)
          utcMinutesDiff  = int(- utcminutes) /* converted to utc required format */
          .
 
      /* client's due date is converting into UTC time */
      pdtDueDate = datetime-tz(pdtDueDate,utcMinutesDiff).
      pdtDueDate = datetime-tz(pdtDueDate,0).
 
      /* now on server time changed to UTc time */
      cTodayDateTime = datetime-tz(cTodayDateTime,utcMinutesDiff).
      cTodayDateTime = datetime-tz(cTodayDateTime,0).
 
     
      /* File Task creation in compass */
      run file/newfiletask-p.p(input piAgentFileID,
                               input pcTopic,
                               input pcQueue, 
                               input pRequest:UID,
                               input now,
                               input pcAssignedTo,
                               input pdcost,
                               output iFileTaskID,
                               output lReturnVal,
                               output cErrorMsg
                                 ). 
      if not lReturnVal
        then next MainBlock.
     
      /*MESSAGE "Order Review Task created in Compass for agent file: " + string(piAgentFileID)
        VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.*/

      /* ARC Task creation */
      run file/createarctask-p.p(input pcQueue,
                                 input pcTopic,
                                 input bagentfile.agentFileID,
                                 input bagentfile.fileNumber,
                                 input bagentfile.fileID,
                                 input pcAssignedTo,
                                 input pdtDueDate,
                                 output iTaskID,
                                 output lReturnVal,
                                 output cErrorMsg
                                ).
      if lReturnVal 
       then next MainBlock.
     
      FILETASK_UPD:
      do transaction 
       on error undo FILETASK_UPD, leave FILETASK_UPD:
        for first filetask exclusive-lock where filetaskid = iFileTaskID:
          filetask.taskid = iTaskID.
        end.
      end.

      /*MESSAGE "Order Review Task created in ARC for agent: " + bagentfile.agentid  + " and fileID " + bagentfile.fileid
        VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.
      */
      icount = icount + 1.

    end.
    MESSAGE icount " Order Review Task created in Compass and ARC"
        VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.
  end method.
end class.
