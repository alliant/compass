/*------------------------------------------------------------------------
File        : util/EmailFileParser.cls
Purpose     : Parsing an eml or msg email file to get it's content

Author(s)   : K.R
Created     : 03/21/2024
Notes       :

Modification:                      
----------------------------------------------------------------------*/
using Progress.Json.ObjectModel.JsonObject.
using Progress.Json.ObjectModel.JsonArray.
USING Progress.Json.ObjectModel.ObjectModelParser.

class util.EmailFileParser :
  
  define private variable cEmailType          as character no-undo.
  define private variable cEmailFilePath      as character no-undo.
  define private variable cScriptAbsolutePath as character no-undo.
  define private variable cOutputDir          as character no-undo.
  define private variable cJsonOutputPath     as character no-undo.
  define private variable cCmdErrorPath       as character no-undo.

  define public property EmailSender          as character  no-undo
  public get.
  private set.

  define public property EmailRecipients     as character   no-undo
  public get.
  private set.

  define public property EmailSubject        as character   no-undo
  public get.
  private set.

  define public property EmailBody           as longchar    no-undo
  public get.
  private set.

  define public property EmailSentDate       as datetime-tz no-undo
  public get.
  private set.

  define public property EmailAttachmentList as character   no-undo
  public get.
  private set.

  &scoped-define SCRIPT_NAME "scripts/emailfileparser.py"

  constructor EmailFileParser ( input pcEmailType as character,
                                input pcEmailFilePath as character
                                ):
    assign
      this-object:cEmailType     = pcEmailType
      this-object:cEmailFilePath = pcEmailFilePath
    .
  end constructor. 
  
  method public void Parse (output pcErrMsg      as character,
                            output pcErrFilePath as character,
                            output plSuccess     as logical                           
                           ):

    plSuccess = true.

    if cEmailType = '' or cEmailType = ? 
     then
      do:
        assign
          pcErrMsg = 'Invalid email file type'
          plSuccess = false
        .

        return.
      end.
    if cEmailFilePath = '' or cEmailFilePath = ?
     then
      do:
        assign
          pcErrMsg = 'Email file path cannot be blank'
          plSuccess = false
         .
        return.
      end.

     if search(cEmailFilePath) = ? 
      then
       do:
         assign
           pcErrMsg = 'Email file not found'
           plSuccess = false.
       end.

     cScriptAbsolutePath = ScriptFullPath ({&SCRIPT_NAME}).

     if cScriptAbsolutePath = '' or cScriptAbsolutePath = ? 
      then
       do:
         assign
           pcErrMsg = 'Script file not found'
           plSuccess = false
         .
         return.
       end.

     if GetOutputDir () = ''
      then
       do:
         assign
           pcErrMsg = 'Error creating temporary email folder'.
           plSuccess = false.
       end.

     os-command silent value (GetOsCommand ()). 

     file-info:file-name = cCmdErrorPath.
     if file-info:full-pathname <> ? and file-info:file-size > 0 
      then
       do:
         assign
           pcErrMsg      = 'Error parsing email file'
           plSuccess     = false
           pcErrFilePath = cCmdErrorPath
         .
         return.
       end.

     file-info:file-name = cJsonOutputPath.
     if file-info:full-pathname = ? 
      then
       do:
         assign
           pcErrMsg = 'Json communication file not found'
           plSuccess = false
         .
         return.
       end.
     pcErrMsg = ParseJson ().
     if pcErrMsg > '' 
      then
       plSuccess = false.

  end.

  /* This method parses the json data created by python script to get all mail info*/
  method private character ParseJson ():

    define variable oScriptOutputJson as JsonObject        no-undo.
    define variable oParser           as ObjectModelParser no-undo.
    define variable oJsonArray        as JsonArray         no-undo.
    define variable std-ch            as character         no-undo.
    define variable iCounter          as integer           no-undo.
    define variable ptrBody           as memptr            no-undo.
    define variable cDateFormat       as character         no-undo.

    oScriptOutputJson = new JsonObject ().
    oParser = new ObjectModelParser ().

    oScriptOutputJson = cast(oParser:ParseFile(cJsonOutputPath), JsonObject).
    
    /* Checking for any issue with email file which is getting parsed*/
    if oScriptOutputJson:has("Error") 
     then
      do:
        return oScriptOutputJson:getcharacter("Error").
      end.

    /* Getting From*/
    this-object:EmailSender = oScriptOutputJson:getcharacter("Sender").
    
    /* Getting Recipients*/
    oJsonArray = oScriptOutputJson:getjsonarray("Recipients").
    do iCounter = 1 to oJsonArray:length:
      std-ch = oJsonArray:getcharacter(iCounter).
      this-object:EmailRecipients = this-object:EmailRecipients + std-ch + ";".
    end.
    this-object:EmailRecipients = trim(this-object:EmailRecipients, ";").
    

    /* Getting Attachments*/
    oJsonArray = oScriptOutputJson:getjsonarray("Attachments").
    do iCounter = 1 to oJsonArray:length:
      std-ch = oJsonArray:getcharacter(iCounter).
      this-object:EmailAttachmentList = this-object:EmailAttachmentList + std-ch + ",".
    end.
    this-object:EmailAttachmentList = trim(this-object:EmailAttachmentList, ",").

    /* Getting Subject*/
    this-object:EmailSubject = oScriptOutputJson:getcharacter("Subject").

    cDateFormat = session:date-format.
    session:date-format = 'ymd'.
    /* Getting Sent date*/
    this-object:EmailSentDate = datetime-tz(oScriptOutputJson:getcharacter("SentDate")).
    session:date-format = cDateFormat.

    /* Getting mail body*/
    ptrBody = oScriptOutputJson:getMemptr("Body").

    copy-lob ptrBody to this-object:EmailBody.

    set-size(ptrBody) = 0.
    if valid-object (oScriptOutputJson) 
     then delete object oScriptOutputJson.

  end.


  /* This method creates the os command*/
  method private character GetOsCommand ():
    define variable cCommand        as character no-undo.

    cJsonOutputPath = GetOutputDir () + "/JsonOutput.json".
    cCmdErrorPath   = GetOutputDir () + "/ErrOutput.txt".

    cCommand = substitute('python &1 "&2" "&3" "&4" "&5"2>"&6" ', 
                          cScriptAbsolutePath, 
                          cEmailType, 
                          cEmailFilePath,
                          cJsonOutputPath,
                          cOutputDir,
                          cCmdErrorPath
                          ).
    return cCommand.
  end.

  /*This method gets the output dir where all email related files will be created*/
  method private character GetOutputDir ():
    define variable iStat as integer no-undo.

    if cOutputDir <> ''
     then return cOutputDir.
    publish "GetTempDir" from (session:first-procedure) (output cOutputDir).

    if cOutputDir = ""
     then publish "GetSystemParameter" from (session:first-procedure) ("dataRoot", OUTPUT cOutputDir).

    cOutputDir = cOutputdir + "Email_" + guid(generate-uuid).

    os-create-dir value(cOutputDir) no-error.
    iStat = os-error.    
    if iStat <> 0 
     then return ''.
    return cOutputDir.
  end.
  
  /*This method gets the absolute path of the script*/
  method private character ScriptFullPath(input cRelativeScriptPath as character):
    file-info:file-name = cRelativeScriptPath.

    if file-info:full-pathname <> ? or file-info:full-pathname <> '' 
     then return file-info:full-pathname.
  end method.

  destructor EmailFileParser ():
    os-delete value(GetOutputDir()) recursive.
  end destructor.
END CLASS.
