/*------------------------------------------------------------------------
@name  renderproductionstatement-p.p
@description Creates production statement pdf per user

@author Shefali
@version 1.0
@created 06/05/2024
@modified 

SB        10/22/2024     Modified to show invoice total instead of all invoice Amts for a file and 
                         property Information instead of invoice description 

SB        11/21/2024     Modified to add handle legacy payments.
----------------------------------------------------------------------*/

{tt/filestatement.i}
define buffer bfFileStatement for fileStatement.

define input  parameter ipdStartDate        as date       no-undo.
define input  parameter ipdEndDate          as date       no-undo.
define input  parameter ipcARContactPhone   as character  no-undo.
define input  parameter ipcARContactEmail   as character  no-undo.
define input  parameter ipcARContactAddress as character  no-undo.
define input  parameter ipcARContactName    as character  no-undo.
define input  parameter ipcAgentID          as character  no-undo.
define input  parameter ipcName             as character  no-undo.
define input  parameter ipcAgentAddr        as character  no-undo.
define input  parameter ipcAgentManager     as character  no-undo.
define input  parameter ipcManagerName      as character  no-undo.
define input  parameter table for filestatement.
define output parameter pFilename           as character  no-undo.
define output parameter opcMsg              as character  no-undo.


define variable xVal                 as integer   initial 270 no-undo.
define variable cHeaderType          as character initial "F" no-undo.
define variable dInvAmt              as decimal               no-undo.
define variable dpmtAmt              as decimal               no-undo. 
define variable lFileExist           as logical               no-undo.
define variable dBalFor              as decimal               no-undo.
define variable iFromPos             as integer               no-undo.
define variable iStartXPos           as integer               no-undo.
define variable iEndXPos             as integer               no-undo.
define variable dCurrAdjAmt          as decimal               no-undo.
define variable dFileInvoiceAmt      as decimal               no-undo.
define variable dTotalInvoiceAmt     as decimal               no-undo.
define variable dFilePaymentApplied  as decimal               no-undo.
define variable dTotalPaymentApplied as decimal               no-undo.
define variable dFileBalance         as decimal               no-undo.
define variable dTotalBalance        as decimal               no-undo.

{lib/rpt-def.i &ID="'filestatement'" &rptOrientation="'Landscape'" &logo=images\alliantblue.jpg &blockTitleColor=169169169}
{lib/std-def.i}
{lib/ar-def.i}
{lib/xmlencode.i}

FUNCTION getmonthName RETURNS CHARACTER
( INPUT ip-MonthNumber AS INTEGER )  FORWARD.

FUNCTION setxPos RETURNS integer
( input ixPos as integer,
  input lAdd as logical)  FORWARD.

assign
  activeFont = "Helvetica"
  activeSize = 12.0
  .
  
run getFilename in this-procedure no-error.
if error-status:error 
 then return.

activeFilename = pFilename.

{lib/rpt-open.i &tempID='filestatement'}

run reportHead              in this-procedure.
run setFileData             in this-procedure.
run setUnmatchedPaymentData in this-procedure.

{lib/rpt-cloz.i &tempID='filestatement' &exclude-pdfview=true}.


PROCEDURE getFilename PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 

  /* GetSystemParameter provides the temporary directory location  */
  publish "GetSystemParameter" ("TempFolder", output std-ch).
  
  if std-ch = ? or std-ch = ""
   then
    std-ch = os-getenv("TEMP").
  
  if std-ch = ? or std-ch = ""
   then
    std-ch = os-getenv("TMP").
  
  if std-ch = ? or std-ch = ""
   then
    std-ch = "d:\temp\".
  
  assign 
      std-ch           = std-ch + "filestatement\"
      file-info:file-name = std-ch.
  
  /* Check whether the filestatement folder is already exist or not */
  if file-info:full-pathname eq ? 
   then
    do:
      os-create-dir value(std-ch).
      if os-error ne 0 
       then
        do:
          opcMsg = "Failed to create folder 'filestatement'".
          return.
        end.       
    end.
      
  pFilename = std-ch  + ipcAgentID + "-ProductionFileStatement-" + String(Year(ipdEndDate), "9999") + "-" + String(MONTH(ipdEndDate), "99") + "-" + String(DAY(ipdEndDate), "99") + ".pdf".
  
END PROCEDURE.

PROCEDURE reportFooter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run pdf_text_color ({&rpt}, 0.0, 0.0, 0.0).
  run pdf_set_font ({&rpt}, "Courier", 7.0).
  
  run pdf_text_xy ({&rpt}, "Printed:", 10, 14).
  run pdf_text_xy ({&rpt}, string(now , "99/99/9999 HH:MM:SS" ), 50, 14).  
   
  run pdf_text_xy ({&rpt}, string(iPage, "zz9"), 
                    if rptOrientation = "Landscape" then 765 else 580, 
                    14).
                     
  setFont(activeFont, activeSize).

END PROCEDURE.

PROCEDURE reportHead :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable inameCount as integer    no-undo.
  
  assign 
      dInvAmt       = 0
      dpmtAmt       = 0
      dCurrAdjAmt   = 0
      .  
  
  for each filestatement where (filestatement.agentFileID ne "" and 
                               filestatement.agentFileID ne ?) break by fileNumber by invoiceDate:
     
    if first-of(filestatement.invoiceDate) and
      (filestatement.invoiceDate >= ipdStartDate and filestatement.invoiceDate <= ipdEndDate) then 

      dInvAmt = dInvAmt + totalInvAmt.

    if filestatement.category = "Header" 
     then
      do:
        if filestatement.type = "A" 
         then assign dpmtAmt = dpmtAmt + filestatement.paymentAmt.

        if filestatement.type = "W" 
         then assign dCurrAdjAmt = dCurrAdjAmt + filestatement.paymentAmt.
      end.

     if filestatement.notes = "Legacy Payment" and 
        (filestatement.receviedDate >= datetime(ipdStartDate) and filestatement.receviedDate <= datetime(ipdEndDate))  
       then assign dpmtAmt = dpmtAmt + filestatement.paymentAmt.
  end.

  for first filestatement where filestatement.type = "B":
    assign dBalFor = filestatement.paymentAmt.
  end.

  run pdf_skipn ({&rpt},1).
  
  /* Statement label on top right */
  setFont("Helvetica-Bold", 14.0).
  run pdf_text_xy({&rpt}, "PRODUCTION STATEMENT", 550, 560). 
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180"). 
  
  setFont("Helvetica-Bold", 11.0).
  run pdf_text_xy ({&rpt}, "Agent " + ipcAgentID, 20, 520).
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
  run pdf_line ({&rpt},200,515,20,515,2).
    
  setFont("Helvetica", 9.0).
  if length(ipcName) > 51
   then
    inameCount = 10.
    
  run pdf_text_xy ({&rpt}, substring(ipcName,1,50), 20, 500). 
  run pdf_text_xy ({&rpt}, substring(ipcName,51), 20, 500 - inameCount). 
  
  ipcAgentAddr = trim(ipcAgentAddr, ";").
      
  if num-entries(ipcAgentAddr,";") ge 1 and  entry(1,ipcAgentAddr,";") ne ""
   then
    run pdf_text_xy ({&rpt}, entry(1,ipcAgentAddr,";"), 20, 490 - inameCount).     

  if num-entries(ipcAgentAddr,";") ge 2  and entry(2,ipcAgentAddr,";") ne ""
   then    
    run pdf_text_xy ({&rpt}, entry(2,ipcAgentAddr,";"), 20, 480 - inameCount). 
    
  if num-entries(ipcAgentAddr,";") ge 3  and entry(3,ipcAgentAddr,";") ne ""
   then    
      run pdf_text_xy ({&rpt}, entry(3,ipcAgentAddr,";"), 20, 470 - inameCount). 
      
  setFont("Helvetica-Bold", 11.0).

  run pdf_text_xy ({&rpt}, getmonthName(month(ipdEndDate)) + " " + string(year(ipdEndDate)), 280, 520).
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
  run pdf_line ({&rpt},500,515,280,515,2).
  
  setFont("Helvetica", 9.0).  

  run pdf_text_xy ({&rpt},  "Statement Date:",       280, 500). 
  if ipdEndDate gt today
   then
    do:
      run pdf_text_color({&rpt}, 255, 0, 0).
      run pdf_text_align({&rpt}, string(ipdEndDate),"Right", 500, 500).   
      run pdf_text_color({&rpt}, 0, 0, 0).
    end.
  else
   run pdf_text_align({&rpt}, string(ipdEndDate),"Right", 500, 500).

  run pdf_text_xy ({&rpt},  "Statement Period:",     280, 485). 
  run pdf_text_align({&rpt}, string(ipdStartDate) + " - " + string(ipdEndDate),"Right", 500, 485).   
  run pdf_text_xy ({&rpt},  "Balance Forward:", 280, 470). 
  if dBalFor < 0 
   then
    do:
      run pdf_text_color({&rpt}, 255, 0, 0).
      run pdf_text_align({&rpt}, string((dBalFor),"(>>>,>>>,>>9.99)"),"Right", 500, 470).   
      run pdf_text_color({&rpt}, 0, 0, 0).
    end.
  else
   run pdf_text_align({&rpt}, string(dBalFor,">>>,>>>,>>9.99"),"Right", 500, 470).     
  run pdf_text_xy ({&rpt}, "Processed:", 280, 455).
  if dInvAmt < 0
   then
    do:
      run pdf_text_color({&rpt}, 255, 0, 0).
      run pdf_text_align({&rpt}, string((dInvAmt),"(>>>,>>>,>>9.99)"),"Right", 500, 455).   
      run pdf_text_color({&rpt}, 0, 0, 0).
    end.
  else
   run pdf_text_align({&rpt}, string(dInvAmt,">>>,>>>,>>9.99"),"Right", 500, 455).     
  
  run pdf_text_xy ({&rpt}, "Payments:" ,280, 440). 
 
  if (dpmtAmt) < 0 
   then
    do:
      run pdf_text_color({&rpt}, 255, 0, 0).
      run pdf_text_align({&rpt}, string((-1 * dpmtAmt),"(>>>,>>>,>>9.99)"),"Right", 500, 440).    
      run pdf_text_color({&rpt}, 0, 0, 0).
    end.
  else
   run pdf_text_align({&rpt}, string(dpmtAmt,">>>,>>>,>>9.99"),"Right", 500, 440). 
  
  run pdf_text_xy ({&rpt}, "Adjustment:" ,280, 425). 
  if (dCurrAdjAmt) < 0 
   then
    do:
      run pdf_text_color({&rpt}, 255, 0, 0).
      run pdf_text_align({&rpt}, string((-1 * dCurrAdjAmt),"(>>>,>>>,>>9.99)"),"Right", 500, 425).    
      run pdf_text_color({&rpt}, 0, 0, 0).
    end.
  else
   run pdf_text_align({&rpt}, string(dCurrAdjAmt,">>>,>>>,>>9.99"),"Right", 500, 425).
  
  setFont("Helvetica-Bold", 9.0).
  if (dBalFor + dInvAmt  + dpmtAmt  + dCurrAdjAmt) < 0
   then
    do:
      run pdf_text_xy ({&rpt}, "Credit Balance:", 280, 410).
      run pdf_text_color({&rpt}, 255, 0, 0).
      run pdf_text_align({&rpt}, string(dBalFor + dInvAmt  + dpmtAmt  + dCurrAdjAmt,"(>>>,>>>,>>9.99)"),"Right", 506, 410).  
      run pdf_text_color({&rpt}, 0, 0, 0).
    end.
  else
  do:
    run pdf_text_xy ({&rpt}, "Balance Due:", 280, 410). 
    run pdf_text_align({&rpt}, string((dBalFor + dInvAmt  + dpmtAmt  + dCurrAdjAmt),">>>,>>>,>>9.99"),"Right", 500, 410). 
  end. 
  
  setFont("Helvetica-Bold", 11.0).  
  run pdf_text_xy ({&rpt}, "Customer Service", 550, 520). 
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
  run pdf_line ({&rpt},550,515,780,515,2).
  
  setFont("Helvetica-Bold", 9.0).
  run pdf_text_xy ({&rpt}, 'Agency Manager', 550, 500).
  
  setFont("Helvetica", 9.0). 
  run pdf_text_xy ({&rpt}, ipcManagerName, 550, 490).
  run pdf_text_xy ({&rpt}, ipcAgentManager, 550, 480).
  
  setFont("Helvetica-Bold", 9.0).
  run pdf_text_xy ({&rpt}, "Accounts Receivable", 550, 465).
  
  setFont("Helvetica", 9.0).
  run pdf_text_xy ({&rpt}, ipcARContactName, 550, 455).  
  run pdf_text_xy ({&rpt}, ipcARContactEmail, 550, 445).  
  run pdf_text_xy ({&rpt}, ipcARContactPhone, 550, 435).
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").  
  run pdf_line ({&rpt},550,429,780,429,0.8).
  
  setFont("Helvetica-Bold", 9.0).  
  run pdf_text_xy ({&rpt}, "Remit to", 550, 420).
  
  setFont("Helvetica", 9.0).
  if num-entries(ipcARContactAddress,";") ge 1
   then
    run pdf_text_xy ({&rpt}, entry(1,ipcARContactAddress,";"), 550, 410).
    
  if num-entries(ipcARContactAddress,";") ge 2
   then    
    run pdf_text_xy ({&rpt}, entry(2,ipcARContactAddress,";"), 550, 400).

  if num-entries(ipcARContactAddress,";") ge 3
   then    
    run pdf_text_xy ({&rpt}, entry(3,ipcARContactAddress,";"), 550, 390).
  
  setFont("Helvetica", 9.0).

  std-ch = "*Please reference SEARCH and File Number(s) on all payments.".
  run pdf_text_xy ({&rpt}, std-ch, 10, 370).
 
END PROCEDURE.
 
PROCEDURE reportHeader :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var tTitle as char no-undo.
  def var tXpos as int no-undo.
  tXpos = 400 - (length(tTitle) / 2 * 6). /* there are 6 points in one character and 400 points is the middle of the header */
 {lib/rpt-hdr.i &title=tTitle &title-x=tXpos &title-y=740}
    
END PROCEDURE.

PROCEDURE setFileData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cFileNumber  as character no-undo.
  define variable cAgentFileID as character no-undo.

  if not can-find(first filestatement where filestatement.agentFileID <> "" and 
                                            filestatement.agentFileID ne ?)
   then return.
  
  run transactionHeader in this-procedure (input 330).

  assign
      dTotalInvoiceAmt     = 0
      dTotalPaymentApplied = 0
      dTotalBalance        = 0
      lFileExist           = true.
  
  setxPos(50,yes).
  for each filestatement where filestatement.agentFileID ne "" and 
                               filestatement.agentFileID ne ? and 
                               (category ne "Header" ) and 
                               sourceType ne "PropertyInfo" break by fileNumber by invoiceDate:

    run pdf_set_font({&rpt}, "Helvetica", 8).

    if first-of(filestatement.fileNumber) then
     do: 
       dFileInvoiceAmt = 0.
       cFileNumber = filestatement.fileNumber.
       cAgentFileID = filestatement.agentFileID.
       run pdf_text_xy({&rpt}, fileNumber, 15, setxPos(15,no)).

       std-ch = (if date(filestatement.tranDate) = ? then "" else string(date(filestatement.tranDate))).
       run pdf_text_xy({&rpt}, std-ch, 130, setxPos(0,no)).
     end.
  
    if filestatement.type eq ? and filestatement.sourceID eq ?
     then
        dFileInvoiceAmt = dFileInvoiceAmt + totalInvAmt.

    if last-of(fileNumber) and last-of(filestatement.invoiceDate) then
     do:
       run setPropertyInfo in this-procedure (input cAgentFileID).
       run setPaymentData in this-procedure (input cFileNumber).
     end.

    if last(fileNumber)  then
     do:
       setxPos(10,no).
       run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
       run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
       run pdf_line ({&rpt},10,setxPos(0,no) + 10,780,setxPos(0,no) + 10,2).
    end.
  end.
  
  run pdf_set_font({&rpt}, "Helvetica-bold", 8).
  run pdf_text_align({&rpt}, "Grand Totals:","Right", 230, setxPos(0,no)).  
  run pdf_rgb ({&rpt},     "pdf_stroke_fill", "070130180").
  run pdf_rgb ({&rpt},     "pdf_stroke_color", "070130180").
  run pdf_line ({&rpt},10,setxPos(0,no) - 5,780,setxPos(0,no) - 5,2).

  std-ch = if dTotalInvoiceAmt = 0 then "" else string(dTotalInvoiceAmt,">>>,>>>,>>>.99").
  
  run pdf_text_align({&rpt}, std-ch,"Right", 425, setxPos(0,no)).
 
  assign std-ch = if dTotalPaymentApplied = 0 then "" else if dTotalPaymentApplied < 0 then string(dTotalPaymentApplied,"(>>>,>>>,>>>.99)") else 
                     string(dTotalPaymentApplied,">>>,>>>,>>>.99").

  run pdf_text_align({&rpt}, std-ch,"Right", 506, setxPos(0,no)).

  if dTotalBalance < 0 
   then
    do:
      run pdf_text_color({&rpt}, 255, 0, 0).
      run pdf_text_align({&rpt}, string((-1 * dTotalBalance),"(>>>,>>>,>>9.99)"),"Right", 759, setxPos(0,no)).    
      run pdf_text_color({&rpt}, 0, 0, 0).
    end.
  else
   do:
     std-ch = if dTotalBalance = 0 then "0.00" else string(dTotalBalance,">>>,>>>,>>9.99").
     run pdf_text_align({&rpt}, std-ch,"Right", 759, setxPos(0,no)).
   end.

END PROCEDURE.

procedure setPaymentData:
  define input parameter ipcFileNumber as character no-undo.

  for each filestatement where filestatement.fileNumber = ipcFileNumber and
                              (category ne "Header" ) and
                               filestatement.type <> "W" break by FileNumber by checkNumber by receviedDate: 

   if first-of(fileNumber) 
     then 
      assign 
          dFilePaymentApplied = 0
          dFileBalance        = 0. 

   /*Legacy payments*/
   if filestatement.notes = "Legacy Payment" 
    then
     do:
       std-ch = (if Notes = ? then "" else notes).
        /*Applied Payment*/
       run pdf_text_xy({&rpt}, std-ch, 190, setxPos(10,no)).
	   
       assign std-ch = if paymentAmt < 0 then string(paymentAmt,"(>>>,>>>,>>>.99)") else 
                      string(paymentAmt,">>>,>>>,>>>.99").
       
       dFilePaymentApplied = dFilePaymentApplied + paymentAmt.
       run pdf_text_align({&rpt}, std-ch,"Right", 506, setxPos(0,no)).
	   
       std-ch = (if date(filestatement.receviedDate) = ? then "" else string(date(filestatement.receviedDate))).
	   
       run pdf_text_xy({&rpt}, std-ch, 530, setxPos(0,no)).
       run pdf_text_xy({&rpt}, checkNumber, 590, setxPos(0,no)).
	   
       run pdf_text_align({&rpt}, "","Right", 698, setxPos(0,no)).
     end.

   if filestatement.type = "A" and first-of(checkNumber) and filestatement.notes ne "Legacy Payment" 
    then
     do:
       /*Applied Payment*/
       assign std-ch = if paymentAmt < 0 then string(paymentAmt,"(>>>,>>>,>>>.99)") else 
                      string(paymentAmt,">>>,>>>,>>>.99").
       
       dFilePaymentApplied = dFilePaymentApplied + paymentAmt.
       run pdf_text_align({&rpt}, std-ch,"Right", 506, setxPos(10,no)).
	 
       std-ch = (if date(filestatement.receviedDate) = ? then "" else string(date(filestatement.receviedDate))).
	 
       run pdf_text_xy({&rpt}, std-ch, 530, setxPos(0,no)).
       run pdf_text_xy({&rpt}, checkNumber, 590, setxPos(0,no)).
       assign std-ch = if checkAmt < 0 then string(checkAmt,"(>>>,>>>,>>>.99)") else 
                      string(checkAmt,">>>,>>>,>>>.99").
	 
       run pdf_text_align({&rpt}, std-ch,"Right", 698, setxPos(0,no)).   
     end.

   if last(fileNumber) then
    for first bffileStatement where bffileStatement.fileNumber = filestatement.fileNumber and
                                    bffileStatement.type = "W":
       
      dFilePaymentApplied = dFilePaymentApplied + bffileStatement.paymentAmt.

      run pdf_set_font({&rpt}, "Helvetica-bold", 8).
      run pdf_text_xy({&rpt}, "Adjustment", 190, setxPos(10,no)).

      if bffileStatement.paymentAmt < 0 
       then
        run pdf_text_align({&rpt}, string(bffileStatement.paymentAmt,"(>>>,>>>,>>>.99)"),"Right", 506, setxPos(0,no)).    
      else
       run pdf_text_align({&rpt}, string(bffileStatement.paymentAmt,">>>,>>>,>>>.99"),"Right", 500, setxPos(0,no)).
    end.
  end.
  
  run pdf_set_font({&rpt}, "Helvetica-bold", 8).
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
  run pdf_line ({&rpt},190,setxPos(20,no) + 8,780,setxPos(0,no) + 8,0.5). 
  run pdf_text_align({&rpt}, "File Total:","Right", 230, setxPos(0,no)).
  
  std-ch = if dFileInvoiceAmt = 0 then "" else string(dFileinvoiceAmt,">>>,>>>,>>>.99").

  run pdf_text_align({&rpt}, std-ch ,"Right", 425, setxPos(0,no)).
  dTotalInvoiceAmt = dTotalInvoiceAmt + dFileInvoiceAmt.
  
  assign std-ch = if dFilePaymentApplied = 0 then "" else if dFilePaymentApplied < 0 then string(dFilePaymentApplied,"(>>>,>>>,>>>.99)") else 
                  string(dFilePaymentApplied,">>>,>>>,>>>.99").

  run pdf_text_align({&rpt}, std-ch,"Right",506, setxPos(0,no)).
  dTotalPaymentApplied = dTotalPaymentApplied + dFilePaymentApplied.
  
  dFileBalance = dFileBalance + (dFileInvoiceAmt + dFilePaymentApplied).

  if dFileBalance < 0 
   then
    do:
      run pdf_text_color({&rpt}, 255, 0, 0).
      run pdf_text_align({&rpt}, string((-1 * dFileBalance),"(>>>,>>>,>>9.99)"),"Right", 759, setxPos(0,no)).    
      run pdf_text_color({&rpt}, 0, 0, 0).
    end.
  else
  do:
     std-ch = if dFileBalance = 0 then "0.00" else string(dFileBalance,">>>,>>>,>>9.99").
     run pdf_text_align({&rpt}, std-ch,"Right", 759, setxPos(0,no)).
  end.

  dTotalBalance = dTotalBalance + dFileBalance.
        
  setxPos(4,no).
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
  run pdf_line ({&rpt},10,setxPos(0,no),780,setxPos(0,no),0.5).

end procedure.

procedure setPropertyInfo:
  define input parameter ipcAgentFileID as character no-undo.
  define variable cPropertyAddresses    as character no-undo.
  define variable iPropteryAddr         as integer   no-undo.

  if can-find(first fileStatement where filestatement.agentFileID = ipcAgentFileID and 
                                        filestatement.sourceType  = "PropertyInfo") 
   then
    for each fileStatement where filestatement.agentFileID = ipcAgentFileID and 
                                 filestatement.sourceType  = "PropertyInfo"
                                 break by agentFileID:
    
      if first-of(filestatement.agentFileID) then
       assign 
         cPropertyAddresses = ""
         iPropteryAddr      = 0.
    
      if length(filestatement.propertyAddress) > 45 then 
       do:
         if cPropertyAddresses = "" then
           cPropertyAddresses = substring(filestatement.propertyAddress,1, r-index(filestatement.propertyAddress," ",45)) + ";" 
                              + substring(filestatement.propertyAddress,r-index(filestatement.propertyAddress," ",45) + 1,length(filestatement.propertyAddress)).
         else 
           cPropertyAddresses = cPropertyAddresses + "^" + substring(filestatement.propertyAddress,1, r-index(filestatement.propertyAddress," ",45)) + ";" 
                              + substring(filestatement.propertyAddress,r-index(filestatement.propertyAddress," ",45) + 1,length(filestatement.propertyAddress)).
       end.
      else 
       do:
         if cPropertyAddresses = "" then
           cPropertyAddresses = filestatement.propertyAddress.
         else 
           cPropertyAddresses = cPropertyAddresses + "^" + filestatement.propertyAddress.
       end.
    
      cPropertyAddresses = trim(cPropertyAddresses,"^").
        
      if first-of(filestatement.agentFileID) then
       do: 
         iPropteryAddr = 1.
         if num-entries(cPropertyAddresses,"^") ge 1
          then
           do:
             if num-entries(entry(1,cPropertyAddresses,"^"),";") ge 1 then
               run pdf_text_xy ({&rpt}, entry(1,entry(1,cPropertyAddresses,"^"),";"), 190, setxPos(0,no)).
           end.
    
          std-ch = if dFileInvoiceAmt = 0 then "" else string(dFileInvoiceAmt,">>>,>>>,>>>.99").
          run pdf_text_align({&rpt}, std-ch ,"Right", 425, setxPos(0,no)).
    
          if num-entries(entry(1,cPropertyAddresses,"^"),";") ge 2 then
            run pdf_text_xy ({&rpt}, entry(2,entry(1,cPropertyAddresses,"^"),";"), 190, setxPos(10,no)).
    
          if num-entries(entry(1,cPropertyAddresses,"^"),";") ge 3 then
            run pdf_text_xy ({&rpt}, entry(3,entry(1,cPropertyAddresses,"^"),";"), 190, setxPos(10,no)).
       end.
      else 
       do:
         if num-entries(cPropertyAddresses,"^") ge 2
          then
           do:
             if num-entries(entry(2,cPropertyAddresses,"^"),";") ge 1 then
             run pdf_text_xy ({&rpt}, entry(1,entry(iPropteryAddr,cPropertyAddresses,"^"),";"), 190, setxPos(10,no)).
    
             if num-entries(entry(2,cPropertyAddresses,"^"),";") ge 2 then
             run pdf_text_xy ({&rpt}, entry(2,entry(iPropteryAddr,cPropertyAddresses,"^"),";"), 190, setxPos(10,no)).
    
             if num-entries(entry(2,cPropertyAddresses,"^"),";") ge 3 then
             run pdf_text_xy ({&rpt}, entry(3,entry(iPropteryAddr,cPropertyAddresses,"^"),";"), 190, setxPos(10,no)).
          end.
       end.
       iPropteryAddr = iPropteryAddr + 1.
   end.
   else
    do:
      std-ch = if dFileInvoiceAmt = 0 then "" else string(dFileInvoiceAmt,">>>,>>>,>>>.99").
      run pdf_text_align({&rpt}, std-ch ,"Right", 425, setxPos(0,no)).
    end.

end procedure.

PROCEDURE setUnmatchedPaymentData :
  if not can-find(first filestatement where filestatement.type = "P")
   then return.
   
  assign 
      cHeaderType = "P"
      dpmtAmt     = 0
      .

  if xVal le 82
   then
    do:
      run pdf_new_page2({&rpt}, "Landscape").
      run transactionHeader in this-procedure(input 500).
      xVal = 485.
    end. 
   else
    do:
      if lFileExist
       then 
        xval = xval -  52.
       else 
        xval = xval + 40.
      run transactionHeader in this-procedure (input xval).                
      setxPos(10,no).
    end.

  for each filestatement 
    where filestatement.type = "P":
    assign
     filestatement.paymentAmt = filestatement.paymentAmt * -1
     filestatement.checkAmt   = filestatement.checkAmt * -1
     dpmtAmt                  = dpmtAmt   + filestatement.paymentAmt
     .
    
    std-ch = (if date(filestatement.receviedDate) = ? then "" else string(date(filestatement.receviedDate))).    
    run pdf_text_xy({&rpt}, std-ch,                          15, setxPos(15,no)).

    run pdf_text_xy({&rpt}, checkNumber , 90, setxPos(0,no)).  
    
    std-ch = if filestatement.checkAmt = 0 then "" else if filestatement.checkAmt < 0 then string((filestatement.checkAmt),"(>>>,>>>,>>>.99)") else  string(filestatement.checkAmt,">>>,>>>,>>>.99").
    if filestatement.checkAmt < 0
     then
      run pdf_text_align({&rpt}, std-ch,"Right", 277, setxPos(0,no)). 
    else
     run pdf_text_align({&rpt}, std-ch,"Right", 272, setxPos(0,no)).      
    
    std-ch = if filestatement.refundAmt = 0 then "" else  string(absolute(filestatement.refundAmt),">>>,>>>,>>>.99").
    run pdf_text_align({&rpt}, std-ch,"Right", 383, setxPos(0,no)).
    
    std-ch = if filestatement.paymentAmt = 0 then "" else if filestatement.paymentAmt < 0 then string((filestatement.paymentAmt),"(>>>,>>>,>>>.99)") else string(filestatement.paymentAmt,">>>,>>>,>>>.99").
    if filestatement.paymentAmt < 0
     then
       run pdf_text_align({&rpt}, std-ch,"Right", 540, setxPos(0,no)).
    else
     run pdf_text_align({&rpt}, std-ch,"Right", 535, setxPos(0,no)).
     
    run pdf_text_xy({&rpt}, filestatement.fileNumber, 620, setxPos(0,no)).
     
  end.

  setxPos(15,no).
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
  run pdf_line ({&rpt},10,setxPos(0,no) + 10,780,setxPos(0,no) + 10,2).  

  RUN pdf_set_font({&rpt}, "Helvetica-bold", 8).  
  
  std-ch = if dpmtAmt = 0 then "" else if dpmtAmt < 0 then string((dpmtAmt),"(>>>,>>>,>>>.99)") else string(dpmtAmt,">>>,>>>,>>>.99").
  
  if dpmtAmt < 0
   then
    do:
     run pdf_text_color({&rpt}, 255, 0, 0).
     run pdf_text_align({&rpt}, std-ch,"Right", 540, setxPos(0,no)).
     run pdf_text_color({&rpt}, 0, 0, 0).
    end.
  else
   run pdf_text_align({&rpt}, std-ch,"Right", 535, setxPos(0,no)).
   
  run pdf_text_xy({&rpt}, "Unapplied Payments Total:",   200, setxPos(0,no)).
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").  
  run pdf_line ({&rpt},10,setxPos(0,no) - 5,780,setxPos(0,no) - 5,2).

END PROCEDURE.

PROCEDURE transactionHeader :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter iPos as integer no-undo.
  
  if cHeaderType = "F" 
   then
    do:
      run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
      run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
      run pdf_rect({&rpt}, 10, iPos + 17,   770, 15, 0.5).        
      run pdf_set_font({&rpt}, "Helvetica-bold", 11).   
     
      run pdf_text_color({&rpt}, 1, 1, 1).
      run pdf_text_xy({&rpt}, "Files", 20, iPos + 20).    
        
      run pdf_rect2({&rpt}, 10, ipos - 13,   770, 30, 0.5).
    
      run pdf_text_color({&rpt}, 0, 0, 0).
      run pdf_set_font({&rpt}, "Helvetica", 8). 
  
      run pdf_text_xy({&rpt}, "File Number",       15, ipos).
      run pdf_text_xy({&rpt}, "Date",              130, ipos).

      run pdf_text_xy({&rpt}, "Property Information",190, ipos).      
      run pdf_text_xy({&rpt}, "Invoice Amount",    370, ipos).
      run pdf_text_xy({&rpt}, "Payment",           470, ipos + 4).
      run pdf_text_xy({&rpt}, "Applied",           470, ipos - 6).
      run pdf_text_xy({&rpt}, "Received",          530, ipos + 4).
      run pdf_text_xy({&rpt}, "Date",              530, ipos - 6).
      run pdf_text_xy({&rpt}, "Check",             590, ipos + 4).
      run pdf_text_xy({&rpt}, "Number",            590, ipos - 6).
      run pdf_text_xy({&rpt}, "Check",             670, ipos + 4).
      run pdf_text_xy({&rpt}, "Amount",            670, ipos - 6).      
      run pdf_text_xy({&rpt}, "Balance",           730, ipos).    
    end.
   else
    do:
      run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
      run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
      run pdf_rect({&rpt}, 10, iPos + 17,   770, 15, 0.5).        
      run pdf_set_font({&rpt}, "Helvetica-bold", 11).   
    
      run pdf_text_color({&rpt}, 1, 1, 1).
      run pdf_text_xy({&rpt}, "Unapplied Payments", 20, iPos + 20).    
        
      run pdf_rect2({&rpt},10, ipos - 13,   770, 30, 0.5).
    
      run pdf_text_color({&rpt}, 0, 0, 0).
      run pdf_set_font({&rpt}, "Helvetica", 8).       
         
      run pdf_text_xy({&rpt}, "Received Date",      15,  ipos).
      run pdf_text_xy({&rpt}, "Check/Reference",    90,  ipos).      
      run pdf_text_xy({&rpt}, "Check Amount",       220, ipos).
      run pdf_text_xy({&rpt}, "Refunded",           350, ipos).
      run pdf_text_xy({&rpt}, "Unapplied Amount",   470, ipos).    
      run pdf_text_xy({&rpt}, "File Number",        620, ipos).  
    end.

END PROCEDURE.

FUNCTION getmonthName RETURNS CHARACTER
  ( INPUT ip-MonthNumber AS INTEGER ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable lv-Months as character no-undo initial "January,Febuary,March,April,May,June,July,August,September,October,November,December".
  
  if ip-MonthNumber lt 1 or ip-MonthNumber gt 12
   then
    return "".
    
  return entry(ip-MonthNumber,lv-Months).

END FUNCTION.

FUNCTION setxPos RETURNS integer
  ( input ixPos as integer,
    input lAdd as logical) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if xVal le ixPos + 30
   then
    do:
      run pdf_new_page2({&rpt}, "Landscape").
      run transactionHeader in this-procedure(input 500).
      xVal = 485.
    end.
    
  if lAdd
   then
    xval = xval +  ixPos.
   else
    xval = xval -  ixPos.   
    
  return xval.   /* Function return value. */

END FUNCTION.


