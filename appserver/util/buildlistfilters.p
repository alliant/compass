&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file buildlistfilters.p
@description Build the listfilter temp-table based on the parameters

@param Filters;char;The filters for the tables

@author John Oliver
@created 09.22.2016
@notes
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{lib/std-def.i}
{lib/add-delimiter.i}
{tt/listfield.i}
{tt/listfilter.i}

define input parameter pFilterList as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* get the filter(s) */
if pFilterList > ""
 then 
  do std-in = 1 to num-entries(pFilterList):
    create listfilter.
    assign
      std-ch = entry(std-in,pFilterList)
      listfilter.tableName = entry(1,std-ch,{&msg-dlm})
      listfilter.columnName = entry(2,std-ch,{&msg-dlm})
      listfilter.columnOperator = entry(3,std-ch,{&msg-dlm})
      listfilter.columnValue = entry(4,std-ch,{&msg-dlm})
      .
    release listfilter.
  end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-GetOpenEdgeFilters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetOpenEdgeFilters Procedure 
PROCEDURE GetOpenEdgeFilters :
/*------------------------------------------------------------------------------
@description Get the filters for use in a query for the table
@return Filters;char;The filter list to be used in a query
------------------------------------------------------------------------------*/
  define output parameter pFilters as character no-undo.

  /* only filter on the claim table first */
  pFilters = "".
  for each listfilter no-lock:
          
    pFilters = addDelimiter(pFilters," AND ").
    /* if the operator is a LIKE, then we need to format differently */
    if listfilter.columnOperator = "LIKE"
     then pFilters = pFilters + "INDEX(" + 
                     listfilter.tableName + "." + 
                     listfilter.columnName + "," + 
                     listfilter.columnValue + ") > 0"
                     .
     else pFilters = pFilters + 
                     listfilter.tableName + "." + 
                     listfilter.columnName + " " + 
                     listfilter.columnOperator + " " + 
                     listfilter.columnValue
                     .
  end.
   
  RETURN pFilters.   /* Function return value. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetSQLServerFilters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSQLServerFilters Procedure 
PROCEDURE GetSQLServerFilters :
/*------------------------------------------------------------------------------
@description Get the filters for use in a query for the table
@param Fields;complex;The fields for the quick list
@return Filters;char;The filter list to be used in a query
------------------------------------------------------------------------------*/
  define input parameter table for listfield.
  define output parameter pFilters as character no-undo.

  /* only filter on the claim table first */
  pFilters = "".
  for each listfilter no-lock:
    for first listfield no-lock
        where listfield.tableName = listfilter.tableName
          and listfield.columnName = listfilter.columnName:
          
      if listfield.dataType = "datetime"
       then pFilters = addDelimiter(pFilters," AND ") + 
                       "CONVERT(DATE, " +
                       listfilter.tableName + "." + 
                       listfilter.columnName + ") " + 
                       listfilter.columnOperator + " "
                       .
       else pFilters = addDelimiter(pFilters," AND ") + 
                       listfilter.tableName + "." + 
                       listfilter.columnName + " " + 
                       listfilter.columnOperator + " "
                       .
      if listfilter.columnOperator = "LIKE"
       then pFilters = pFilters + "'%" + trim(listfilter.columnValue,"'") + "%'".
       else
        do:
          if listfield.dataType = "datetime"
           then pFilters = pFilters + "'" + string(year(datetime(listfilter.columnValue))) + "-" 
                                          + string(month(datetime(listfilter.columnValue))) + "-"
                                          + string(day(datetime(listfilter.columnValue))) + "'".
           else pFilters = pFilters + listfilter.columnValue.
        end.
    end.
  end.
   
  RETURN pFilters.   /* Function return value. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

