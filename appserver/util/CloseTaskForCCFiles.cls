/*------------------------------------------------------------------------
@name  util/CloseTaskForCCFiles.cls
@action CloseTaskForCCFiles
@description Close order review task for the files 
imported from Closers choice. 

----------------------------------------------------------------------*/

class util.CloseTaskForCCFiles inherits framework.ActionBase:
  {lib/nextkey-def.i} 

  define variable cErrMessage      as character   no-undo.
  define variable cErrorMsg        as character   no-undo.
  define variable icount           as integer     no-undo.
  define variable ierrcode         as integer     no-undo.
  define stream s1.

  constructor public CloseTaskForCCFiles():
    super().
  end constructor.

  destructor public CloseTaskForCCFiles():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}

    /*output stream s1 to "d:\temp\closeTaskforCCFiles.csv".
    export stream s1 delimiter "," "AgentFIleID" "FiletaskID" "Task" "taskID" "Assignedto" "Note".*/

    MainBlock:
    for each filetask where completeddate = ? and posted = false 
        no-lock
        by filetaskid desc:
        
      /*if (filetask.assignedto ne ? and filetask.assignedto ne "") and filetask.assignedto ne pRequest:UID then 
      do:
          export stream s1 delimiter ","  filetask.AgentFIleID filetask.fileTaskID filetask.topic filetask.taskID filetask.assignedto pRequest:UID + " is not the assigned user".
      end.*/
      
      run file/assignuser-p.p(input  filetask.TaskID,
                              input  prequest:UID,
                              input  now,
                              output iErrCode,
                              output cErrMessage).

      /*if iErrCode > 0 then
        export stream s1 delimiter "," filetask.AgentFIleID filetask.fileTaskID filetask.topic  filetask.taskID filetask.assignedto "Unable to assign the task to " + pRequest:UID.*/
      
 
      run util/CloseTaskForCCFiles-p.p(input  filetask.TaskID,
                                       input  filetask.AgentFileID,
                                       input  filetask.filetaskID).
      /*if iErrCode > 0 then 
        export stream s1 delimiter "," filetask.AgentFIleID filetask.fileTaskID filetask.topic filetask.taskID filetask.assignedto "Task cannot be completed: " + cErrMessage.
      else
      do: */
        icount = icount + 1.
        /*export stream s1 delimiter "," filetask.AgentFIleID filetask.fileTaskID filetask.topic filetask.taskID filetask.assignedto "Task Completed in ARC and Copmpass".
      end.*/ 
      /*if icount = 5 then leave.*/
    end.
    /*output stream s1 close.*/

    MESSAGE icount " Task Closed in Compass and ARC" skip
        VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.
    
  end method.
end class.
