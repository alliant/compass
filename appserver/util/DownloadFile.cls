/*------------------------------------------------------------------------
@file DownloadFile.cls
@action downloadFile
@description Download a file given a path

@param Path;char;The path to the file
@returns Dataset;complex;The dataset containing the base64 string

@author John Oliver
@created 2024.04.17
  ----------------------------------------------------------------------*/
  
class util.DownloadFile inherits framework.ActionBase:

  define temp-table iFile no-undo
    field path   as character
    field name   as character
    field base64 as blob
    .
  
  constructor DownloadFile ():
    super().
  end constructor.

  destructor public DownloadFile ():
  end destructor.  
  
  method public override void act (input pRequest  as framework.IRequestable, input pResponse as framework.IRespondable):
    {lib/std-def.i}
    
    define variable dsHandle as handle    no-undo.
    define variable pFile    as character no-undo.
    define variable mFile    as memptr    no-undo.
    define variable tFile    as longchar  no-undo.

    /* build dataset */
    create dataset dsHandle.  
    dsHandle:serialize-name = 'data'.      
    dsHandle:set-buffers(buffer iFile:handle).
    
    /* get path to file */
    pRequest:getParameter("File", output pFile).
    
    /* make sure the file exists */
    file-info:file-name = pFile.
    if file-info:full-pathname = ?
     then pResponse:fault("3066", "File " + pFile).
     
    if not pResponse:isFault()
     then
      do:
        copy-lob from file pFile to mFile.
        create iFile.
        assign
          iFile.path = substring(pFile, 1, r-index(pFile, "\"))
          iFile.name = substring(pFile, r-index(pFile, "\") + 1)
          tFile      = base64-encode(mFile)
          .
        copy-lob tFile to iFile.base64.
      end.
      
    if can-find(first iFile)
     then
      do:
        setContentDataset(input dataset-handle dsHandle, "path,name,base64").
        pResponse:success("2000","Download File").
      end.
                                   
  end method.
  
end class.

