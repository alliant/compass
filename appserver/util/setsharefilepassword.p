&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* sys/setsharefilepassword.p
   @param iUser;UserID to reset password
   @param iPw;New Password

   @author D.Sinclair
   @notes This procedure relies on the following system parameters:
            SharefileLoginService,SharefileAdminID,SharefileAdminPw,
            SharefileUserService,SharefilePasswordService


Sharefile Services:
https://alliantnational.sharefile.com/rest/getAuthID.aspx?op=login&fmt=xml&username=&1&password=&2

https://alliantnational.sharefile.com/rest/users.aspx?op=get&fmt=xml&id=&1&authid=&2

https://alliantnational.sharefile.com/rest/users.aspx?op=resetp&fmt=xml&id=&1&newp=&2&authid=&3
 */


def input parameter iUser as char.
def input parameter iOldPw as char.
def input parameter iPw as char.
def output parameter pSuccess as logical init true.
def output parameter pMsg as char.

def var tAdminID as char no-undo.
def var tAdminPW as char no-undo.
def var tHeaders as char no-undo.
def var tInputFile as char no-undo.
def var tOutputFile as char no-undo.

{lib/std-def.i}

def var curElementName as char no-undo.
def var curElementValue as char no-undo.
def var tResponseFile as char.
def var tTraceFile as char.
def var tName as char.
def var tDebug as logical init true.

tName = replace(guid,"=", "") + "Sf".
tTraceFile = tName + "TRC".

{lib/xmlparse.i &xmlFile=tResponseFile &traceFile=tTraceFile &exclude-startElement=true}

def var tSfAuthID as char no-undo.
def var tSfUserID as char no-undo.
def var tURL as char.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


publish "GetSystemParameter" ("DebugModeEnabled", output tDebug). /* Server */
  publish "GetAppDebug" (output tDebug). /* Client */

  publish "GetSystemParameter" ("SharefileLoginService", output tURL).
  if tURL = "" 
   then return.

  publish "GetSystemParameter" ("SharefileAdminID", output tAdminID).
  publish "GetSystemParameter" ("SharefileAdminPW", output tAdminPW).

  tURL = replace(tURL, "&1", tAdminID).
  tURL = replace(tURL, "&2", tAdminPW).

  assign
    std-lo = false
    std-ch = ""
    tSfAuthID = ""
    parseStatus = false /* error */
    .
  run util/httpget.p (tName + "Login",
                      tURL, 
                      "",
                      output std-lo,
                      output std-ch,
                      output tOutputFile).

  if std-lo 
   then
    do:
        /* das: Parse for the AuthID for subsequent calls */
        tResponseFile = tOutputFile.
        parseXml(output std-ch). /* parseStatus should be set to true if it's not an error (it's backwards) */
    end.

  if not tDebug 
   then os-delete value(tOutputFile).

  if tSfAuthID = "" or not parseStatus
   then 
    do: assign
          pSuccess = false
          pMsg = "Sharefile authorization failed".
        return.
    end.



  tURL = "".
  publish "GetSystemParameter" ("SharefileUserService", output tURL).
  if tURL = "" 
   then return.

  tURL = replace(tURL, "&1", iUser).
  tURL = replace(tURL, "&2", tSfAuthID).

  assign
    std-lo = false
    std-ch = ""
    tSfUserID = ""
    parseStatus = false /* error */
    .
  run util/httpget.p (tName + "User",
                      tURL, 
                      "",
                      output std-lo,
                      output std-ch,
                      output tOutputFile).

  if std-lo 
   then
    do:
        /* das: Parse for ID of the user */
        tResponseFile = tOutputFile.
        parseXml(output std-ch).
    end.

  if not tDebug 
   then os-delete value(tOutputFile).

  if tSfUserID = "" or not parseStatus
   then 
    do: assign
          pSuccess = false
          pMsg = "Sharefile userid not available".
        return.
    end.



  tURL = "".
  publish "GetSystemParameter" ("SharefilePasswordService", output tURL).

  if tURL = "" 
   then return.
  tURL = replace(tURL, "&1", tSfUserID).
  tURL = replace(tURL, "&2", iOldPw).
  tURL = replace(tURL, "&3", iPw).
  tURL = replace(tURL, "&4", tSfAuthID).

  assign
    std-lo = false
    std-ch = ""
    parseStatus = false /* error */
    .
  run util/httpget.p (tName + "Pw",
                      tURL, 
                      "",
                      output std-lo,
                      output std-ch,
                      output tOutputFile).

  if std-lo 
   then
    do: 
        /* das: Parse for the success/failure of the password change */
        tResponseFile = tOutputFile.
        parseXml(output std-ch).
        pSuccess = parseStatus.
    end.
   else pSuccess = false.

  if not tDebug 
   then os-delete value(tOutputFile).

  if not pSuccess 
   then pMsg = "Sharefile password reset failed".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-Characters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Characters Procedure 
PROCEDURE Characters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pText as memptr no-undo.
 def input parameter pCharCount as int no-undo.

 curElementValue = curElementValue + get-string(pText, 1, pCharCount).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-EndElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE EndElement Procedure 
PROCEDURE EndElement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pNamespace as char no-undo.
 def input parameter pLocalName as char no-undo.
 def input parameter pElementName as char no-undo.

 case pElementName:
  when "error" 
   then
    do: if curElementValue = "true"
         then assign
                parseStatus = false
                .
         else parseStatus = true.
    end.
  when "errorMessage" 
   then assign
          parseMsg = curElementValue
          parseMsg = curElementValue + parseMsg
          .
  when "errorCode" 
   then parseMsg = parseMsg + "(" + curElementValue + ")".


  /* Result of Login service call */
  when "authid" 
   then tSfAuthID = curElementValue.

  /* Result of Login or Users service calls */
  when "id" 
   then tSfUserID = curElementValue.

 end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-StartElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StartElement Procedure 
PROCEDURE StartElement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE INPUT PARAMETER namespaceURI AS CHARACTER.
  DEFINE INPUT PARAMETER localName AS CHARACTER.
  DEFINE INPUT PARAMETER qName AS CHARACTER.
  DEFINE INPUT PARAMETER attributes AS HANDLE.
 
  curElementName = qName.
  curElementValue = "".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

