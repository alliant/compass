/*------------------------------------------------------------------------
@name  arcreditpdf.p
@description Creates the credit PDF

@author Anjly
@version 1.0
@created 08/04/2020
@modified
Date          Name       Description
12/17/2020               Layout Changes
02/02/2021    Shefali    Modified to changes the layout. 
11/06/2023    Shefali   Task #:107834 Updated to new logo in pdf. 
----------------------------------------------------------------------*/
{tt/ledgerreport.i &tableAlias="glcredit"}

define input  parameter ipcReportAction     as character  no-undo.
define input  parameter table               for glcredit. 
define output parameter opcFilename         as character no-undo.

{lib/rpt-def.i &ID="'arCredit'"  &logo=images\alliantblue.jpg &blockTitleColor=169169169}
{lib/std-def.i}
{lib/ar-def.i}
{lib/ar-getsourcetype.i} /* Include funcion: getSourceType */

/* Local Variables */
define variable xVal          as integer  initial 410  no-undo.
define variable iLedgerID     as character             no-undo.
define variable cPostedBy     as character             no-undo.
define variable dtPostDate    as character             no-undo.
define variable deTotal       as decimal               no-undo.
define variable iNextYPos     as integer               no-undo.
define variable lPostReport   as logical               no-undo.
define variable ilinesAdd     as integer               no-undo.
define buffer bfGlCredit for glCredit.

/* Function Prototype */
function nextLinePos returns integer
  ( input deLineBreak as decimal )  forward.

assign
  activeFont = "Helvetica"
  activeSize = 12.0
  .

for first glCredit :
  if glCredit.ledgerID ne 0 
   then
   assign
    lPostReport = true
    cPostedBy = glCredit.ledgercreateby
    dtPostDate = string(glCredit.ledgercreatedate).
 end.
/*------------------------------Report---------------------------------------------*/
run getFilename in this-procedure no-error.
if error-status:error
 then return.

activeFilename = opcFilename.   
  
{lib/rpt-open.i &tempID='arCredit' }  
iNextYPos = 690.

for each bfGlCredit break by bfGlCredit.actionID:

  if first-of (bfGlCredit.actionID) 
   then
    do:
       if bfGlCredit.ledgerID ne 0
        then
         do:
            /*CreditHeader*/
            run pageHeader(iNextYPos).
            iNextYPos = nextLinePos(4.5).
         end.
       /*CreditHeader*/
       run CreditHeader(iNextYPos).
       iNextYPos = nextLinePos(1.3).
       
       for first glCredit where glCredit.actionId = bfGlCredit.actionID :
         run Creditdata(iNextYPos , output ilinesAdd).
         iNextYPos = nextLinePos(ilinesAdd + 2).
       end.
       iNextYPos = nextLinePos(1).
       /*gldetailHeader*/
       run gldetailHeader(iNextYPos).
       iNextYPos = nextLinePos(1). 

       for each glCredit where glCredit.actionId = bfGlCredit.actionID and glCredit.credit = 0 by glCredit.debit desc: 
         run gldetaildata(iNextYPos).
         iNextYPos = nextLinePos(1).
       end.
       for each glCredit where glCredit.actionId = bfGlCredit.actionID and glCredit.debit = 0 by glCredit.credit desc: 
         run gldetaildata(iNextYPos).
         iNextYPos = nextLinePos(1).
       end.
    end.
    if last-of(bfGlCredit.actionID)  and not last(bfGlCredit.actionID) 
     then
      do:
         /*newpage*/
         run pdf_new_page({&rpt}).
         iNextYPos = 700.
      end.
end.
  
if ipcReportAction = {&agentdefault}
 then
  do:
     {lib/rpt-cloz.i &tempID='arCredit' &no-save=true &exclude-pdfview=true}
  end.
 else if ipcReportAction = {&view} 
  then
   do:
     {lib/rpt-cloz.i &tempID='arCredit' &no-save=true }
   end.

/*-------------------------------------------------------------------------------------*/ 
 
 
/* -------------------------------- Internal Procedures  -----------------------------------*/

procedure getFilename:
  if ipcReportAction = {&agentdefault} 
   then
    std-ch = session:temp-directory.
   else if ipcReportAction = {&view} 
    then
     publish "getReportDir" (output std-ch).  
  opcFilename = std-ch  + (if lPostReport then "PostedCredit_" else "PrelimCredit_")  + "_" + replace(string(today), "/", "") + "_" + replace(string(time,"HH:MM:SS"),":","") + ".pdf".
  if ipcReportAction = {&view} 
   then
    publish "SetCurrentValue" ("filename", opcFilename).
end procedure.

procedure reportFooter :
  run pdf_text_color ({&rpt}, 0.0, 0.0, 0.0).
  run pdf_set_font ({&rpt}, "Courier", 7.0).
  run pdf_text_xy ({&rpt}, "Printed: ", 20, 14).
  run pdf_text_xy ({&rpt}, string(now , "99/99/9999 HH:MM:SS" ), 55, 14). 
  /* Line added for "Posted By" */
  if cPostedBy > ""
   then
    run pdf_text_xy ({&rpt}, "Posted: " + cPostedBy + " " + string(dtPostDate), 250, 14). 
  run pdf_text_xy ({&rpt}, string(iPage, "zz9"),  580, 14).
  setFont(activeFont, activeSize).
end procedure.

procedure reportHeader :
  def var tTitle as char no-undo.
  def var tXpos as int no-undo.
  if lPostReport 
   then
    tTitle =  "POST CREDIT JOURNAL".
   else
    tTitle =  "PRELIMINARY CREDIT JOURNAL".
  tXpos = 400 - (length(tTitle) / 2 * 6). /* there are 6 points in one character and 400 points is the middle of the header */
  {lib/rpt-hdr.i &title=tTitle &title-x=tXpos &title-y=740} 
end procedure.

procedure pageHeader :
  define input parameter yPos as integer.
  setFont("Helvetica", 9.0).
  run pdf_text_xy({&rpt}, "Posting ID:", 400, yPos).
  run pdf_text_xy({&rpt}, bfGlCredit.ledgerID, 490, yPos).
  yPos = yPos - 15.
  run pdf_text_xy({&rpt}, "Posting Date:", 400, yPos).
  run pdf_text_align({&rpt}, string(date(bfGlCredit.transDate) ,"99/99/99") ,"left", 490 ,yPos).
  run pdf_text_color({&rpt}, 0, 0, 0). 
end procedure.

procedure CreditHeader :
  define input parameter yPos as integer.
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
  run pdf_rect({&rpt}, 10, yPos , 590, 20, 0.5).    
  run pdf_set_font({&rpt}, "Helvetica-bold", 11).
  run pdf_text_color({&rpt}, 1, 1, 1).
  run pdf_text_align({&rpt}, "Agent ID",     "Left",    20, yPos + 5).
  run pdf_text_align({&rpt}, "Name",         "Left",    80, yPos + 5).
  run pdf_text_align({&rpt}, "Source",       "Left",    330, yPos + 5).
  run pdf_text_align({&rpt}, "Source ID",    "Left",    430, yPos + 5).
  run pdf_text_align({&rpt}, "Amount",       "RIGHT",   580, yPos + 5).
  run pdf_text_color({&rpt}, 0, 0, 0).  
end procedure.

procedure CreditData :
  define input parameter yPos as integer.
  define output parameter iLinesAdded as integer.
  run pdf_text_color({&rpt}, 0, 0, 0).   
  setFont("Helvetica", 9.0).
  run pdf_text_xy({&rpt},    glCredit.agentID , 20, yPos).
  if length(glCredit.agentName) gt 41 
   then
    do:
       run pdf_text_xy ({&rpt}, substring(glCredit.agentName,1,41), 80, yPos). 
       run pdf_text_xy ({&rpt}, substring(glCredit.agentName,42), 80, yPos - 10).
       iLinesAdded = iLinesAdded + 1.
    end.
    else
     run pdf_text_xy ({&rpt}, glCredit.agentName, 80, yPos).
     
  run pdf_text_xy({&rpt},    getSourcetype(glCredit.source) ,                            330, yPos).
  run pdf_text_xy({&rpt},    glCredit.sourceID,                                          430, yPos).
  run pdf_text_align({&rpt}, string(glCredit.transAmt, ">>>,>>>,>>>,>>9.99"),   "RIGHT", 580 ,yPos).
end procedure.

procedure gldetailHeader :
  define input parameter yPos as integer.
  run pdf_set_font({&rpt}, "Helvetica-boldoblique", 11).
  run pdf_text_color({&rpt}, 0, 0, 0). 
  run pdf_text_align({&rpt}, "Account",          "Left",    160, yPos + 5).
  run pdf_text_align({&rpt}, "Description",      "Left",    220, yPos + 5).
  run pdf_text_align({&rpt}, "Debit",            "right",    375, yPos + 5).
  run pdf_text_align({&rpt}, "Credit",           "right",    465, yPos + 5).
  /* linestrock */
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "0,0,0").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "0,0,0").
  run pdf_rect({&rpt}, 150, yPos , 325, 0.10, 0.5).
  run pdf_text_color({&rpt}, 0, 0, 0).   
end procedure.

procedure gldetailData :
  define input parameter yPos as integer.
  run pdf_text_color({&rpt}, 0, 0, 0).
  setFont("Helvetica", 9.0). 
  run pdf_text_xy({&rpt},    glCredit.glRef, 160, yPos).
  run pdf_text_xy({&rpt},    glCredit.glDesc, 220, yPos).
  run pdf_text_align({&rpt}, (if glCredit.debit = 0 then "" else string(glCredit.debit, ">>>,>>>,>>>,>>9.99")),  "RIGHT", 375, yPos).
  run pdf_text_align({&rpt}, (if glCredit.credit = 0 then "" else string(glCredit.credit, ">>>,>>>,>>>,>>9.99")), "RIGHT", 465, yPos).
end procedure.

function nextLinePos returns integer
  ( input deLineBreak as decimal ) :
  define variable iPos as integer.
   iPos = iNextYPos  - (deLineBreak * 10).
   if iNextYPos < 40 
    then
     do:
        /*newpage*/
        run pdf_new_page({&rpt}).
        iPos = 700.
     end.
 
  return iPos.   /* Function return value. */

end function.
