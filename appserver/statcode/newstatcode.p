/*------------------------------------------------------------------------
@name StatCodeNew
@description Creates a new STAT code using the specified inputs
@param StateID;char;
@param StatCode;char;
@param FormID;char;  (optional)
@param Description;char;  (optional)
@param Regulation;char;  (optional)
@param Active;logical;  (optional)
@returns Success;log;Success indicator
@throws 3066;Invalid StateID/FormID
@throws 3000;
@author Russ Kenny
@version 1.0 12/17/2013
@notes
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

def var pcStateID as char no-undo.
def var pcStatCode as char no-undo.
def var pcFormID as char no-undo.
def var pcDescription as char no-undo.
def var pcRegulation as char no-undo.
def var plActive as log no-undo.

def var tCreated as logical init false no-undo.
def var tHasForm as logical no-undo.

{lib/std-def.i}


pRequest:getParameter("StateID", output pcStateID).
IF NOT CAN-FIND(state
                  WHERE state.stateID = pcStateID)
 then pResponse:fault("3066", "State").

pRequest:getParameter("StatCode", output pcStatCode).
IF pcStatCode = ""
 then pResponse:fault("3051", "STAT Code" {&msg-add} "").

IF CAN-FIND(first statCode
              where statCode.stateID = pcStateID
                and statCode.statCode = pcStatCode)
 THEN pResponse:fault("3010", "STAT Code" {&msg-add} "Code" {&msg-add} pcStatCode).


pRequest:getParameter("FormID", output pcFormID).

if pcFormID > ""
 then
  do:
      tHasForm = false.

      STATEFORM-BLOCK:
      for first stateform 
        where stateForm.stateID = pcStateID
          and stateForm.formID = pcFormID:
       tHasForm = true.
      end.
      
      if not tHasForm 
       then pResponse:fault("3066", "Form").
  end.

if pResponse:isFault()
 then return.


tCreated = false.

TRX-BLOCK:
do TRANSACTION
 ON ERROR UNDO TRX-BLOCK, LEAVE TRX-BLOCK:

  create statCode.
  assign
    statCode.stateID = pcStateID
    statCode.statCode = pcStatCode
    .
    
  validate statCode.
  
  statCode.formID = pcFormID.

  if pRequest:getParameter("Description", pcDescription)
   then statCode.description = pcDescription.
  if pRequest:getParameter("Regulation", pcRegulation)
   then statCode.regulation = pcRegulation.
  if pRequest:getParameter("Active", plActive)
   then statCode.active = plActive.
   else statCode.active = true.

  RELEASE statCode.

  tCreated = true.
end.

if not tCreated
 then 
  do: pResponse:fault("3000", "Create").
      RETURN.
  END.

pResponse:success("2002", "STAT Code").
pResponse:createEvent("STATCode", pcStateID {&id-add} pcStatCode).
