/*------------------------------------------------------------------------
@name StatCodesGet
@description Returns either a complete list of all STAT codes, STAT codes for a state, or a specific STAT code
@param StateID;char;State identifier (optional)
@param StatCode;char;STAT code identifier (optional)
@returns StatCodes;complex;STAT code dataset
@returns Success;int;2005
@author Russ Kenny
@version 1.0 12/17/2013
@notes
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}
def var tStatCode as char no-undo.

{tt/statcode.i &tableAlias="ttStatCode"}

pRequest:getParameter("StateID", OUTPUT std-ch).
pRequest:getParameter("StatCode", OUTPUT tStatCode).

std-in = 0.

if std-ch > "" and tStatCode > "" 
 then
    for first statCode no-lock
      where statcode.stateID = std-ch
        and statcode.statcode = tStatCode:
      create ttStatCode.
      buffer-copy statCode to ttStatCode.
      
      for first stateform no-lock
      where stateform.stateID = statcode.stateID
        and stateform.formID = statcode.formID:
        assign ttStatCode.formType = stateform.formType.
            /* formType is not stored in statcode, 
               but needed for default form selection 
               when adding/updating a statcode record */
      end.
      
      std-in = 1.
    end.

 else
if std-ch = "" 
 then
    for each statCode no-lock:
      create ttStatCode.
      buffer-copy statCode to ttStatCode.
      
      for first stateform no-lock
      where stateform.stateID = statcode.stateID
        and stateform.formID = statcode.formID:
        assign ttStatCode.formType = stateform.formType.
      end.
      
      std-in = std-in + 1.
    end.

 else
    for each statCode no-lock
      WHERE statCode.stateID = std-ch:
      create ttStatCode.
      buffer-copy statCode to ttStatCode.
      
      for first stateform no-lock
      where stateform.stateID = statcode.stateID
        and stateform.formID = statcode.formID:
        assign ttStatCode.formType = stateform.formType.
      end.
      
      std-in = std-in + 1.
    end.

IF std-in > 0
 THEN pResponse:setParameter("StatCode", table ttStatCode).
pResponse:success("2005", STRING(std-in) {&msg-add} "STAT Code").
