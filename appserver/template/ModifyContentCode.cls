/*------------------------------------------------------------------------
@name  template/ModifyContentCode.cls
@action contentCodeModify
@description  Modify the existing contentcode record.
@param contentCode;integer
@param dataset for payload
@param returnRecord;logical;Whether modified record to be returned or not
@throws 3005; Data Parsing failed
@throws 3066;Content Code does not exist
@throws 3005;StateID cannot be blank
@throws 1000;StateID is invalid
@throws 3005;Type cannot be blank
@throws 3005;ContentCode cannot be blank
@throws 3015; At least one Contentcode record exists for given stateID, type and contentcode combination.
@throws 3000;Content Code modify failed
@returns ContentCode record based on requirement. 
@returns Success;2000
@author Shefali
@Modified :
Date        Name      Comments   
09/19/24    AG        Modifed to set description and prefix fields.    
----------------------------------------------------------------------*/

class template.ModifyContentCode inherits framework.ActionBase:

  constructor public ModifyContentCode ():
    super().
  end constructor.

  destructor public ModifyContentCode ( ):
  end destructor.
    

  {tt/contentcode.i &serializename=Contentcode &tableAlias=ttContentCode }
  {lib/removeNullValues.i}

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    define variable lSuccess        as logical   no-undo.
    define variable piContentCodeID as integer   no-undo.
    define variable piStateID       as character no-undo.
    define variable plReturnRecord  as logical   init false no-undo.
    
    /*dataset handle */
    define variable dsHandle as handle.
    define buffer bContentCode for contentCode.
    
    /* parameter get */
    pRequest:getParameter("contentCodeID", output piContentCodeID).
    pRequest:getParameter("returnRecord", output plReturnRecord).

    if piContentCodeID = ?
     then
      piContentCodeID = 0.

    if plReturnRecord = ?
     then
      plReturnRecord = false.
    
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttContentCode:handle). 

    {lib/std-def.i}

    /* get Dataset */
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.

    /* validate contentCodeID */
    if not can-find(first contentcode where contentCode.contentCodeID  = piContentCodeID)
     then
      do:  
        pResponse:fault("3066", "Content Code").
        return.
      end.
    
    removeNullValues("ttContentCode").

    TRX-BLOCK:
    for first contentCode exclusive-lock
      where contentCode.contentCodeID = piContentCodeID TRANSACTION
        on error undo TRX-BLOCK, leave TRX-BLOCK:

      /* updating contentCode record*/
      for first ttContentCode no-lock
        where ttContentCode.contentCodeID = contentCode.contentCodeID:

        if ttContentCode.stateID = "" or
           ttContentCode.stateID = ?
         then
          do:  
            pResponse:fault("3005", "StateID cannot be blank").
            return.
          end.

        if not can-find(first state where state.stateID = ttContentCode.stateID)
         then
          do:  
            pResponse:fault("1000", "StateID").
            return.
          end.

        if ttContentCode.typeDescription = "" or
           ttContentCode.typeDescription = ?
         then
          do:  
            pResponse:fault("3005", "Type cannot be blank").
            return.
          end.

        if ttContentCode.contentCode = "" or
           ttContentCode.contentCode = ?
         then
          do:  
            pResponse:fault("3005", "ContentCode cannot be blank").
            return.
          end.

        /* validate contentCodeID */
        if can-find(first bContentCode where bContentCode.stateID     = ttContentCode.stateID and 
                                             bContentCode.type        = (if ttContentCode.typeDescription = "Requirement" then "R" 
                                                                         else if ttContentCode.typeDescription = "Exception" then "E"
                                                                         else if ttContentCode.typeDescription = "Instrument" then "I"
                                                                         else if ttContentCode.typeDescription = "Legal" then "L"
                                                                         else "") and
                                             bContentCode.contentCode = ttContentCode.contentCode and
                                             rowID(bContentcode)      <> rowID(contentcode))
         then
          do:  
            pResponse:fault("3015", "Contentcode record" {&msg-add} "given stateID, type and contentcode combination").
            return.
          end.

        assign
            contentCode.contentCode      = ttContentCode.contentCode
            contentCode.stateID          = ttContentCode.stateID
            contentCode.type             = if ttContentCode.typeDescription = "Requirement" then "R" 
                                           else if ttContentCode.typeDescription = "Exception" then "E"
                                           else if ttContentCode.typeDescription = "Instrument" then "I"
                                           else if ttContentCode.typeDescription = "Legal" then "L"
                                           else ""
           contentCode.content           = ttContentCode.content
           contentCode.description       = ttContentCode.description
           contentCode.prefix            = if ttContentCode.content begins '[#]'   or
                                              ttContentCode.content begins '[##]'  or
                                              ttContentCode.content begins '[###]' or 
                                              ttContentCode.content begins '[n]' then true else false
           contentCode.lastModifiedDate  = now
           contentCode.lastModifiedBy    = pRequest:uid
           .                           
      end. /* for first ttContentCode */
  
      validate contentCode.
      release contentCode.
   
      lSuccess = true.

    end. /* for first contentCode */

    if plReturnRecord 
     then
      do:
        for first contentCode no-lock
         where contentCode.contentCodeID = piContentCodeID:
          empty temp-table ttContentCode.
          create ttContentCode.
          assign 
              ttContentCode.contentCodeID   = contentCode.contentCodeID
              ttContentCode.contentCode     = contentCode.contentCode
              ttContentCode.stateID         = contentCode.stateID
              ttContentCode.type            = contentCode.type
              ttContentCode.content         = contentCode.content
              ttContentCode.typeDescription = if contentCode.type = "R" then "Requirement" 
                                              else if contentCode.type = "E" then "Exception"
                                              else if contentCode.type = "I" then "Instrument"
                                              else if contentCode.type = "L" then "Legal"
                                              else ""
              ttContentCode.description     = contentCode.description
              ttContentCode.prefix          = contentCode.prefix.
        end.
      end.

    if not lSuccess
     then 
      do:
        presponse:fault("3000", "Content Code update").
        return.
      end. /* if not lSuccess */

    if plReturnRecord
     then
      setContentDataset(input dataset-handle dsHandle, pRequest:FieldLIst).

    pResponse:success("2000", "Content Code update").
 
  end method.
 
end class.

