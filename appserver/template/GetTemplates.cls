/*------------------------------------------------------------------------
@name  gettemplates.p
@action templatesGet
@description  Get the Template Details.
@returns Success;2000;TemplateGet was successful

@author Shweta Dhar
@Modified :
Date        Name          Comments   
4/4/2023    sagar K       changed single stateID to Multiple stateID
02/09/2024  S Chandu      Added producttemplate table.
02/15/2024  S Chandu      changed "producttemplate" table to "templateproduct".
----------------------------------------------------------------------*/

class template.GetTemplates inherits framework.ActionBase:

   /* temp table */
  {tt/template.i &tableAlias=ttTemplate }
  {tt/templatefield.i &tableAlias=ttTemplateField }
  {tt/templatecontent.i &tableAlias=ttTemplateContent }
  {tt/templateproduct.i &tableAlias=ttTemplateProduct }

  constructor public GetTemplates ():
   super().
  end constructor.

  destructor public GetTemplates ():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}

    define variable pcTemplateID as integer   no-undo.
    define variable pcStateIDList    as character no-undo.
    define variable pcProductID  as integer   no-undo.
    define variable pcAgentID    as character no-undo.
    define variable plActiveOnly as logical   no-undo.
    define variable dsHandle     as handle    no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttTemplate:handle,
                         buffer ttTemplateField:handle,
                         buffer ttTemplateContent:handle,
                         buffer ttTemplateProduct:handle).


    pRequest:getParameter("TemplateID", output pcTemplateID).
    pRequest:getParameter("StateIDList",    output pcStateIDList).
    pRequest:getParameter("ProductID",  output pcProductID).
    pRequest:getParameter("AgentID",    output pcAgentID).
    pRequest:getParameter("ActiveOnly", output plActiveOnly).

    for each template no-lock 
      where template.templateID = (if pcTemplateID = 0 or pcTemplateID = ? then template.templateID else pcTemplateID) /* added nullcheck because of requestBAseClass.  */
       /* and template.productID  = (if pcproductID = 0  or pcProductID = ?  then template.productID  else pcproductID)   */
        and template.agentID    = (if pcAgentID = ""   or pcAgentID = ?    then template.agentID    else pcAgentID)
        and template.active     = (if not plActiveOnly or plActiveOnly = ? then template.active     else plActiveOnly):

      create tttemplate.
      assign tttemplate.templateID   = template.templateID  
             tttemplate.stateID      = template.stateID           
             tttemplate.agentID      = template.agentID
             tttemplate.documenttype = template.documenttype
             tttemplate.name         = template.name
             tttemplate.active       = template.active
             tttemplate.fileurl      = template.fileurl
             tttemplate.filename     = template.filename
             tttemplate.schemaurl    = template.schemaurl
             tttemplate.schemaname   = template.schemaname
             tttemplate.outputFormat = template.outputFormat.
           
      for each templateField no-lock where templateField.templateID = template.templateID :  
        create tttemplateField.
        assign tttemplateField.templateID = templateField.templateID 
               tttemplateField.FieldName  = templateField.fieldname 
               tttemplateField.fieldlabel = templateField.fieldlabel 
               tttemplateField.datatype   = templateField.datatype.
      end. 
    
      for each templateContent no-lock where templateContent.templateID = template.templateID :
        create tttemplateContent.
        assign tttemplateContent.templateID = templateContent.templateID 
               tttemplateContent.type       = templateContent.type
               tttemplateContent.seq        = templateContent.seq
               tttemplateContent.content    = templateContent.content
               tttemplateContent.indent     = templateContent.indent
               tttemplateContent.isNote     = templateContent.isNote.
      end.    

      for each templateproduct no-lock where templateproduct.templateID = template.templateID:
        create ttTemplateProduct.
        assign
            ttTemplateProduct.productID  = templateproduct.productID
            ttTemplateProduct.templateID = templateproduct.templateID
            .
      end.
    end. 
 
    if can-find(first tttemplate)
     then
	  setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("3005", "TemplateGet was successful").
     
 end method.

end class.


