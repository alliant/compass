/*------------------------------------------------------------------------
@name  template/GetContentCodes.cls
@action contentCodesGet
@description  Get the Contentcode Details.
@throws 3066; StateID does not exist
@returns Success;2000;contentCodesGet was successful

@author Sachin Chaturvedi
@Modified :
Date        Name          Comments   
   
----------------------------------------------------------------------*/ 

class template.GetContentCodes inherits framework.ActionBase:

  constructor public GetContentCodes ():
   super().
  end constructor.

  destructor public GetContentCodes ():
  end destructor.
    

  {tt/contentcode.i &serializename=ttContentcode &tableAlias=ttContentcode}
  {lib/callsp-defs.i}

  method public override void act (input pRequest as framework.IRequestable,
                                  input pResponse as framework.IRespondable):
    define variable pcContentCodeID as character no-undo.
    define variable pcStateID       as character no-undo.
    define variable pcType          as character no-undo.
    define variable pcContentCode   as character no-undo.
    define variable dsHandle        as handle    no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttContentcode:handle). 

    {lib/std-def.i}
    
    pRequest:getParameter("ContentCodeID", output pcContentCodeID).
    pRequest:getParameter("StateID",       output pcStateID).
    pRequest:getParameter("Type",          output pcType).
    pRequest:getParameter("ContentCode",   output pcContentCode).

    if pcContentCodeID = "ALL" or pcContentCodeID = ?
     then
      pcContentCodeID = "".

    if pcStateID = "ALL" or pcStateID = ?
     then
      pcStateID = "".

    if pcType= "ALL" or pcType = ?
     then
      pcType = "".

    if pcContentCode = "ALL" or pcContentCode = ?
     then
      pcContentCode = "".
    
    {lib\callsp.i 
        &name=spGetContentCodes 
        &load-into=ttContentcode 
        &params="input integer(pcContentCodeID), input pcStateID, input pcType, input pcContentCode"
        &noresponse=true}
    
    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end. 
        
    if can-find(first ttContentcode)
     then
      setContentDataset(input dataset-handle dsHandle, pRequest:FieldLIst).
    
    pResponse:success("2005", string(csp-icount) {&msg-add} "Content Code").
  end method.
 
end class.

