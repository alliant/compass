/*------------------------------------------------------------------------
@name  template/NewContentCode.cls
@action contentCodeNew
@description Creates a ContentCode record
@param dataset for payload
@param returnRecord;logical;Whether newly created record to be returned or not
@returns ContentCode record based on requirement.
@throws 3005; Data Parsing failed
@throws 3000; Create failed
@throws 3015; At least one Contentcode record exists for given stateID, type and contentcode.
@success 2002;success 
@author sachin chaturvedi
@version 1.0
@created 12.30.2022
@modified
Date        Name  Description 
09/19/24    SRK   Modifed to set description and prefix fields.   
----------------------------------------------------------------------*/

class template.NewContentCode inherits framework.ActionBase:

  constructor public NewContentCode ():
    super().
  end constructor.

  destructor public NewContentCode ( ):
  end destructor.
    

  {tt/contentcode.i &serializename=Contentcode &tableAlias=ttContentCode }
  {lib/removeNullValues.i}
 
  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    define variable lSuccess       as logical    no-undo. 
    define variable lReturnRecord  as logical    no-undo. 
    define variable cStateID       as character  no-undo.
    define variable cType          as character  no-undo.
    define variable cContentCode   as character  no-undo. 
    define variable iContentCodeID as integer    no-undo.

    {lib/nextkey-def.i}
    
    /*dataset handle */
    define variable dsHandle as handle.
    
    /* parameter get */
    pRequest:getParameter("returnRecord", output lReturnRecord).
    if lReturnRecord = ?
     then
      lReturnRecord = false.
    
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttContentCode:handle). 

    {lib/std-def.i}

    /* get Dataset */
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.
      
    lSuccess = false.

    removeNullValues("ttContentCode").
    
    TRX-BLOCK:
    do transaction
      on error undo TRX-BLOCK, leave TRX-BLOCK:

      for first ttContentCode:

        if ttContentCode.stateID = "" or
           ttContentCode.stateID = ?
         then
          do:  
            pResponse:fault("3005", "StateID cannot be blank").
            return.
          end.

        if not can-find(first state where state.stateID = ttContentCode.stateID)
         then
          do:  
            pResponse:fault("1000", "StateID").
            return.
          end.

        if ttContentCode.typeDescription = "" or
           ttContentCode.typeDescription = ?
         then
          do:  
            pResponse:fault("3005", "Type cannot be blank").
            return.
          end.

        if ttContentCode.contentCode = "" or
           ttContentCode.contentCode = ?
         then
          do:  
            pResponse:fault("3005", "ContentCode cannot be blank").
            return.
          end.

        if can-find(first contentcode where contentcode.stateID      = ttContentCode.stateID and
                                            contentcode.type         = (if      ttContentCode.typeDescription = "Requirement" then "R" 
                                                                        else if ttContentCode.typeDescription = "Exception"   then "E"
                                                                        else if ttContentCode.typeDescription = "Instrument"  then "I"
                                                                        else if ttContentCode.typeDescription = "Legal"       then "L"
                                                                        else "")             and
                                            contentcode.contentcode  = ttContentCode.contentcode
                 ) 
         then
          do:
            pResponse:fault("3015", "Contentcode record" {&msg-add} "given stateID, type and contentcode combination"). 
            return.
          end.
      
        {lib/nextkey.i &type='contentCodeID' &var=iContentCodeID &err="undo TRX-BLOCK, leave TRX-BLOCK."}

        create contentcode.
        assign
            contentcode.contentCodeID     = iContentCodeID
            contentcode.stateID           = ttContentCode.stateID
            contentcode.contentCode       = ttContentCode.contentCode
            contentcode.content           = ttContentCode.content
            contentcode.CreatedDate       = now
            contentcode.CreatedBy         = pRequest:uid
            contentCode.lastModifiedBy    = ""
            contentCode.type              = if ttContentCode.typeDescription = "Requirement" 
                                             then 
                                              "R"
                                             else if ttContentCode.typeDescription = "Exception" 
                                              then 
                                               "E"
                                             else if ttContentCode.typeDescription = "Instrument" 
                                              then 
                                               "I"
                                             else if ttContentCode.typeDescription = "Legal" 
                                              then 
                                               "L"
                                             else ""
            contentCode.description       = ttContentCode.description
            contentCode.prefix            = if ttContentCode.content begins '[#]'   or
                                               ttContentCode.content begins '[##]'  or
                                               ttContentCode.content begins '[###]' or 
                                               ttContentCode.content begins '[n]' then true else false
            cStateID                      = contentcode.stateID 
            cType                         = contentcode.type
            cContentCode                  = contentcode.contentCode
            .

        validate contentcode. 
        release  contentcode.  
      end.  
      lSuccess = true.
    end.

    if not lSuccess
     then
      do:
        pResponse:fault("3000", "Create").
        return.
      end.  
  
    if lReturnRecord
     then
      do:
        for first contentcode no-lock
          where contentcode.stateID        = cStateID      and 
                contentcode.type           = cType         and 
                contentcode.contentCode    = cContentCode:
          empty temp-table ttContentCode.
          create ttContentCode.
          assign
              ttContentCode.contentCodeID    = contentcode.contentCodeID
              ttContentCode.contentCode      = contentcode.contentCode
              ttContentCode.stateID          = contentcode.stateID 
              ttContentCode.type             = contentcode.type
              ttContentCode.content          = contentcode.content
              ttContentCode.typeDescription  = if contentcode.type = "R" 
                                                then 
                                                 "Requirement"
                                                else if contentcode.type = "E" 
                                                 then 
                                                  "Exception"
                                                else if contentcode.type = "I" 
                                                 then 
                                                  "Instrument"
                                                else if contentcode.type = "L" 
                                                 then 
                                                  "Legal"
                                                else ""
              ttcontentCode.description       = ContentCode.description
              ttcontentCode.prefix            = contentCode.prefix
                                                   
                                                .

        end.

        setContentDataset(input dataset-handle dsHandle, pRequest:FieldLIst).
      end.

    pResponse:success("2002"," Contentcode").
 
  end method.
 
end class.

