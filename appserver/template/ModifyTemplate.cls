/*------------------------------------------------------------------------
@name ModifyTemplate.cls
@action templateModify
@description  Update the template.
@param templateID;int
@throws 3001;Invalid templateID
@throws 3066;Template does not exist
@throws 3005; Data Parsing failed
@throws 3000;Template modify failed 
@returns Success;2000
@author Sachin Anthwal
@Modified :
Date        Name          Comments   
12/07/22    SA            Task:94592 removed agentID validation 
02/07/24    SR            Added validations and creating producttemplate table
                          changes.
02/15/2024  S Chandu      changed "producttemplate" table to "templateproduct".
10/21/2024  SRK           Modified to pass TemplateID as output parameter.
----------------------------------------------------------------------*/

class template.ModifyTemplate inherits framework.ActionBase:

   /* temp table */
  {tt/template.i        &tableAlias="ttTemplate"        &serializeName=template}   
  {tt/templatefield.i   &tableAlias="ttTemplatefield"   &serializeName=templatefield}   
  {tt/templatecontent.i &tableAlias="ttTemplatecontent" &serializeName=templatecontent}
  {tt/templateproduct.i &tableAlias="ttTemplateProduct" &serializeName=templateproduct}
  {lib/removeNullValues.i}


  constructor public ModifyTemplate ():
    super().
  end constructor.

  destructor public ModifyTemplate ( ):
  end destructor.

  {framework/action-setfield-in.i  TemplateID}

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    /* library file */
    {lib/std-def.i}
    {lib/nextkey-def.i}

    define variable lSuccess      as logical   no-undo.
    define variable piTemplateID  as integer   no-undo.
    define variable iTemplateID   as integer   no-undo.
    define variable lCreate       as logical   no-undo.
    define variable dsHandle      as handle    no-undo.

    
    /* parameter get */
    pRequest:getParameter("templateID", output piTemplateID).

    if piTemplateID = ?
     then
      piTemplateID = 0.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttTemplate:handle,                  
                         buffer ttTemplatefield:handle,
                         buffer ttTemplatecontent:handle,
                         buffer ttTemplateProduct:handle). 


    /* get Dataset */
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.

    if not can-find(first ttTemplate)
     then
      do:
        pResponse:fault('3005', 'Input data not provided').
        return.
      end.

     /* validation for template stateID and product StateID */
     for each ttTemplateProduct:
       for first ttTemplate:
         for first product where product.productID = ttTemplateProduct.productID no-lock:        
          if Product.StateID <> ttTemplate.stateID
           then
            do:
              pResponse:fault("3005", "Template state does not match with product's state.").
              return.
           end.
         end.
       end.
     end. 

    lSuccess = false.
    lCreate  = false.

    removeNullValues("ttTemplate").
    removeNullValues("ttTemplatefield").
    removeNullValues("ttTemplatecontent").
    removeNullValues("ttTemplateProduct").

    TRX-BLOCK:
    do transaction
      on error undo TRX-BLOCK, leave TRX-BLOCK:
    
      /* Create template if template not exist in system*/
      if not can-find(first template where (template.templateID = piTemplateID)) 
       then
        do:
          lCreate = true.
           
          for first ttTemplate no-lock:
             
            /* validate stateID */
            if ttTemplate.stateID <> "ALL" and not can-find(first state where stateID = ttTemplate.stateID)
             then 
              do:
                pResponse:fault("3066", "Tamplate state " {&msg-add} "ID" {&msg-add} ttTemplate.stateID).
                return.
              end.
           
             for each ttTemplateProduct:
               /* validate productID */
               if not can-find(first product where product.productID = ttTemplateProduct.productID)
                then
                 do:  
                   pResponse:fault("3067", "product" {&msg-add} "ID" {&msg-add} string(ttTemplateProduct.productID)).
                   return.
                 end.
             end.
 

            /* create template*/
            {lib/nextkey.i &type='templateid' &var=iTemplateID &err="undo TRX-BLOCK, leave TRX-BLOCK."}
             
            create template.
            assign
                template.templateID        = iTemplateID
                template.stateID           = ttTemplate.stateID        
                template.agentID           = ttTemplate.agentID      
                template.documenttype      = ttTemplate.documenttype 
                template.name              = ttTemplate.name         
                template.active            = ttTemplate.active       
                template.fileurl           = ttTemplate.fileurl      
                template.filename          = ttTemplate.filename     
                template.schemaurl         = ttTemplate.schemaurl    
                template.schemaname        = ttTemplate.schemaname
                template.outputFormat      = ttTemplate.outputFormat
                template.CreatedDate       = now  
                template.CreatedBy         = pRequest:uid
                template.lastModifiedBy    = ""
                .                           
               
            /* create templatefield */
            for each ttTemplatefield no-lock :
              create templatefield.
              assign
                  templatefield.templateID       = template.templateID
                  templatefield.fieldname        = ttTemplatefield.fieldname   
                  templatefield.fieldlabel       = ttTemplatefield.fieldlabel       
                  templatefield.datatype         = ttTemplatefield.datatype    
                  templatefield.createdDate      = now
                  templatefield.createdBy        = pRequest:uid
                  .
                
              validate templatefield.
              release templatefield.
             
            end. /* for each ttTemplatefield */
             
             
            /* create templatecontent */
            for each ttTemplatecontent no-lock :
              
              create templatecontent.
              assign
                  templatecontent.templateID      = template.templateID
                  templatecontent.type            = ttTemplatecontent.type      
                  templatecontent.seq             = ttTemplatecontent.seq       
                  templatecontent.content         = ttTemplatecontent.content   
                  templatecontent.indent          = ttTemplatecontent.indent    
                  templatecontent.isNote          = ttTemplatecontent.isNote  
                  templatecontent.createdDate     = now
                  templatecontent.createdBy       = pRequest:uid
                  .
              
              validate templatecontent.
              release templatecontent.
            
            end. /* for each ttTemplatecontent */
            
           validate template.
           release template.
            
           lSuccess = true.
          end. /* for first ttTemplate */

          for each ttTemplateProduct:

            if can-find(first templateproduct where templateproduct.productID = ttTemplateProduct.productid and templateproduct.templateID = iTemplateID )
             then
              next.
            else
             do:      
               create templateproduct.
               assign
                   templateproduct.productid   = ttTemplateProduct.productid
                   templateproduct.templateid  = iTemplateID
                   templateproduct.createddate = now
                   templateproduct.createdby   = pRequest:uid
                   .
               validate templateproduct.
               release templateproduct.
             end.
           end.

        end. /* if can-find(first template */ 
      
       /* Modify template if template exist in system*/
       else
        do:
           
          lCreate = false.
          for first template exclusive-lock
            where template.templateID = piTemplateID:
            
            /* updating template*/
            for first ttTemplate no-lock
              where ttTemplate.templateID = template.templateID:  
              assign
                  template.stateID           = ttTemplate.stateID              
                  template.agentID           = ttTemplate.agentID      
                  template.documenttype      = ttTemplate.documenttype 
                  template.name              = ttTemplate.name         
                  template.active            = ttTemplate.active       
                  template.fileurl           = ttTemplate.fileurl      
                  template.filename          = ttTemplate.filename     
                  template.schemaurl         = ttTemplate.schemaurl    
                  template.schemaname        = ttTemplate.schemaname
                  template.outputFormat      = ttTemplate.outputFormat
                  template.lastModifiedDate  = now
                  template.lastModifiedBy    = pRequest:uid
                  .                           
              
            end. /* for first ttTemplate */
            
            /* templatefield */
            /*delete templatefield*/
            for each templatefield exclusive-lock 
              where templatefield.templateID = template.templateID:
              delete templatefield.
              release templatefield.
            end. /* for each templatefield */
            
            /* create templatefield */
            for each ttTemplatefield no-lock
              where ttTemplatefield.templateID = template.templateID :
              
              create templatefield.
              assign
                  templatefield.templateID       = ttTemplatefield.templateID
                  templatefield.fieldname        = ttTemplatefield.fieldname   
                  templatefield.fieldlabel       = ttTemplatefield.fieldlabel       
                  templatefield.datatype         = ttTemplatefield.datatype    
                  templatefield.createdDate      = now
                  templatefield.createdBy        = pRequest:uid
                  .
               
              validate templatefield.
              release templatefield.
          
            end. /* for each ttTemplatefield */
            
            /* templatecontent */
            /*delete templatecontent */
            for each templatecontent exclusive-lock 
              where templatecontent.templateID = template.templateID:
              delete templatecontent.
              release templatecontent.
            end. /* for each templatecontent */
            
            
            /* create templatecontent */
            for each ttTemplatecontent no-lock
              where ttTemplatecontent.templateID = template.templateID:
              
              create templatecontent.
              assign
                  templatecontent.templateID      = ttTemplatecontent.templateID
                  templatecontent.type            = ttTemplatecontent.type      
                  templatecontent.seq             = ttTemplatecontent.seq       
                  templatecontent.content         = ttTemplatecontent.content   
                  templatecontent.indent          = ttTemplatecontent.indent    
                  templatecontent.isNote          = ttTemplatecontent.isNote  
                  templatecontent.createdDate     = now
                  templatecontent.createdBy       = pRequest:uid
                  .
              
              validate templatecontent.
              release templatecontent.
          
            end. /* for each ttTemplatecontent */

            /* delete templateproduct */
            for each templateproduct exclusive-lock
              where templateproduct.templateID = template.templateID:
              delete templateproduct.
              release templateproduct.
            end.
           
            for each ttTemplateProduct:
              if can-find(first templateproduct where templateproduct.productID = ttTemplateProduct.productid and templateproduct.templateID = template.templateID )
               then
                next.
              else
               do:    
                 create templateproduct.
                 assign
                     templateproduct.productid   = ttTemplateProduct.productid
                     templateproduct.templateid  = template.templateID
                     templateproduct.createddate = now
                     templateproduct.createdby   = pRequest:uid
                     .
                 validate templateproduct.
                 release templateproduct.
               end.
            end.
            
            validate template.
            release template.
          
            lSuccess = true.
          
          end. /* for first template */   
        end. /* if can-find(first template */ 
      
    end. /* do transaction */                            


    if pResponse:isFault()
     then
      return.
     
    if not lSuccess
     then 
      do:
        presponse:fault("3000", "Template " + (if lCreate then "create" else "update")).
        return.
      end. /* if not lSuccess */

    if lcreate 
     then
      setTemplateID(iTemplateID).
     else
      setTemplateID(piTemplateID).
   
    pResponse:success("2000", "Template " + (if lCreate then "create" else "update")).
 
  end method.
 
end class.

