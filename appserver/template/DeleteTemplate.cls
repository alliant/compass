/*------------------------------------------------------------------------
@name template/DeleteTemplate.cls
@action templateDelete
@description  Deletes template record form template table.
@param productID;int
@throws 3066;template does not exist
@throws 3000;delete template failed
@returns Success;2000
@author S Chandu
@version 1.0
@created 07.18.2022
@Modified :
Date        Name          Comments  
02/15/24   S Chandu      deleting child tables of template.
----------------------------------------------------------------------*/

class template.DeleteTemplate inherits framework.ActionBase:

  constructor public DeleteTemplate ():
    super().
  end constructor.

  destructor public DeleteTemplate ():
  end destructor.
    
  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}

    /*parameter variables*/
    define variable piTemplateID as integer no-undo.
    
    pRequest:getParameter("templateID", output piTemplateID).

    if piTemplateID = ?
     then
      piTemplateID = 0.

    /* validation when no parameter is supplied */
    if (piTemplateID = 0  or piTemplateID = ?)
     then 
      do:
        pResponse:fault("3001", "TemplateID").
        return.
      end.

    /* validate productID */
    if not can-find(first template where templateID = piTemplateID)
     then
      do: 
        pResponse:fault("3066", "Template record").
        return.
      end.

    if can-find(first templateproduct where templateproduct.templateID = piTemplateID )
     then
      do:
        pResponse:fault("3005", "Cannot delete the template, product's are associated with template.").
        return.
     end.

    std-lo = false.
    TRX-BLOCK:
    for first template exclusive-lock
      where template.templateID = piTemplateID transaction
       on error undo TRX-BLOCK, leave TRX-BLOCK:
      
      /* delete the template content */
      for each templatecontent exclusive-lock
        where templatecontent.templateID = template.templateID
            on error undo TRX-BLOCK, leave TRX-BLOCK:
        
        delete templatecontent.
        release templatecontent.
      end.
      
      /* delete template field */
      for each templatefield exclusive-lock
        where templatefield.templateID = template.templateID
            on error undo TRX-BLOCK, leave TRX-BLOCK:
        
        delete templatefield.
        release templatefield.
      end.

     /* delete templateproduct */
      for each templateproduct exclusive-lock
        where templateproduct.templateID = template.templateID
            on error undo TRX-BLOCK, leave TRX-BLOCK:
        
        delete templateproduct.
        release templateproduct.
      end.

      delete template.
      release template.
      
      std-lo = true.
    end.
    
    if not std-lo
     then
      do: 
        pResponse:fault("3000", "Delete Template").
        return.
      end.
    
    pResponse:success("2000", "Delete Template").
 end method.
  
end class.

