/*------------------------------------------------------------------------
@name template/DeleteContentCode.cls
@action contentCodeDelete
@description  Deletes the contentcode record
@param pcStateID;char
@param pcType;char
@param pcContentCode;char
@throws 3066;contentcode does not exist
@throws 3000;delete contentcode failed
@returns Success;2000
@author Sachin Chaturvedi
@version 1.0
@created 12.29.2022
@Modified :
Date        Name          Comments  

----------------------------------------------------------------------*/

class template.DeleteContentCode inherits framework.ActionBase:

  constructor public DeleteContentCode ():
   super().
  end constructor.

  destructor public DeleteContentCode ( ):
  end destructor.
    

  method public override void act (input pRequest as framework.IRequestable,
                                  input pResponse as framework.IRespondable):
    define variable piContentCodeID as integer no-undo.


    {lib/std-def.i}
    
    pRequest:getParameter("contentCodeID", output piContentCodeID).
    if piContentCodeID = ?
     then
      piContentCodeID = 0.

    
    /* validation when no parameter is supplied */
    if (piContentCodeID = 0)
     then 
      do:
        pResponse:fault("3001", "ContentCodeID").
        return.
      end.

    /* validate contentcode */
    if not can-find(first contentcode where contentcode.contentCodeID = piContentCodeID)
     then
      do: 
        pResponse:fault("3066", "ContentCode record").
        return.
      end.


    std-lo = false.
    TRX-BLOCK:
    for first contentcode exclusive-lock
     where contentcode.contentCodeID = piContentCodeID transaction
      on error undo TRX-BLOCK, leave TRX-BLOCK:
  
      delete contentcode.
      release contentcode.
  
      std-lo = true.
    end.

    if not std-lo
     then
      do: 
        pResponse:fault("3000", "Delete contentcode").
        return.
      end.

    pResponse:success("2000", "Delete contentcode").
  end method.
  
end class.

