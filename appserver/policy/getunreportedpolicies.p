&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file getunreportedpolicies.p
@description gets the data for the Unreported Policies report

@param StateID;character;The state abbreviation
@param AgentID;character;The agent ID
@return UnreportPolicy;complex;The unreported policies temp table

@author John Oliver
@created 2023.05.15
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

/* Parameters ---                                                       */
define variable pStateID as character no-undo.
define variable pAgentID as character no-undo.

/* Variables ---                                                        */
{lib/std-def.i}
{lib/callsp-defs.i}

/* Temp Tables ---                                                      */
{tt/unreportpolicy.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* State */
pRequest:getParameter("StateID", output pStateID).
if pStateID = "ALL" or pStateID = ? 
 then pStateID = "".

if pStateID > "" and not can-find(first state where state.stateID = pStateID)
 then pResponse:fault("3066", "State").

/* Agent */
pRequest:getParameter("AgentID", output pAgentID).
if pAgentID = "ALL" or pAgentID = ? 
 then pAgentID = "".

if pAgentID > "" and not can-find(first agent where agent.agentID = pAgentID)
 then pResponse:fault("3066", "Agent").
 
if pResponse:isFault()
 then return.

{lib/callsp.i &name=spReportUnreportedPolicies &load-into=unreportpolicy &params="input pStateID, input pAgentID"}

pResponse:success("2005", string(csp-icount) {&msg-add} "Office").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


