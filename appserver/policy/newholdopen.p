/*------------------------------------------------------------------------
@name NewHoldOpen
@description Creates a new hold open policy for the passed agent ID

@param AgentID;char;Agent ID for the batch

@returns Success;int;2002
@returns PolicyID;int;Unique ID of newly added hold open policy

@throws 3066;Invalid AgentID
@throws 3000;Internal failure

@event policyHoldOpenCreate

@author Bryan Johnson
@version 1.0 12/08/2014
@notes
Date          Name           Description
03/04/2022     SD            Task 89909 - Modified nextkey.i
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

def var tNewPolicyID as int no-undo.
def var tAgentID as char no-undo.
def var tStateID as char no-undo. 

def var tCreated as logical no-undo.

{lib/std-def.i}
{lib/nextkey-def.i}

pRequest:getParameter("AgentID", OUTPUT tAgentID).
std-lo = FALSE.
FOR FIRST agent NO-LOCK
  WHERE agent.agentID = tAgentID:
 ASSIGN
   std-lo = TRUE
   tStateID = agent.stateID
   .
END.
IF NOT std-lo
 THEN 
  do: pResponse:fault("3066", "Agent").
      RETURN.
  END.
 
std-lo = false. 
for first state no-lock
  where state.stateID = tStateID:              
 std-lo = state.active. 
end.
IF NOT std-lo
 THEN 
  do: pResponse:fault("3020", "State" {&msg-add} "Inactive").
      RETURN.
  END.

IF pResponse:isFault()
 THEN RETURN.

tCreated = FALSE.

TRX-BLOCK:
do TRANSACTION 
    ON ERROR UNDO TRX-BLOCK, LEAVE TRX-BLOCK:

 {lib/nextkey.i &type='policyHoldOpen' &var=tNewPolicyID &err="undo TRX-BLOCK, leave TRX-BLOCK."}
 
  create policy.
  assign
    policy.policyID = tNewPolicyID
    .
  validate policy.

  assign 
    policy.stat         = "I"   /* I)ssued, P)rocessed, V)oided */
    policy.agentID      = tAgentID
    policy.companyID    = ""
    policy.stateID      = tStateID
    policy.fileNumber   = ""
    policy.issueDate    = today
    policy.voidDate     = ?
    policy.invoiceDate  = ?     /* Date of last invoicing */
    policy.periodID     = 0     /* Period used for revenue/SPR recognition (after set, it doesn't change) */
    policy.reprocessed  = FALSE /* Default = false; True means it was reprocessed/invoiced */
    
    /* Suggested values when policy issued by the Agent */
    policy.issuedFormID          = ""
    policy.issuedLiabilityAmount = 0
    policy.issuedGrossPremium    = 0
    policy.issuedResidential     = yes
    policy.issuedEffDate         = today
    
    /* Current values (from processing) */
    policy.formID           = ""
    policy.statcode         = ""
    policy.effDate          = ?
    policy.liabilityAmount  = 0
    policy.grossPremium     = 0
    policy.netPremium       = 0
    policy.retention        = 0
    policy.residential      = ?
    policy.countyID         = ""
    
    policy.trxID            = 'HOLDOPEN ' + string(now)
    policy.trxDate          = today
    .    

  release policy.

  tCreated = TRUE.
end.

if pResponse:isFault()
 then
  return.

IF NOT tCreated
 THEN 
  do: pResponse:fault("3000", "Create").
      tNewPolicyID = 0.  /* just a reminder */
      RETURN.
  END.

pResponse:success("2002", "PolicyHoldOpen").
pResponse:setParameter("PolicyID", tNewPolicyID).
pResponse:createEvent("PolicyHoldOpen", string(tNewPolicyID)).
