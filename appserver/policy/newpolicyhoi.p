/*------------------------------------------------------------------------
@name newpolicyhoi.p
@action policyhoinew
@description Creates a new HOI policy record in the policyhoi table

@param PolicyID;int;the policy ID
@param FileNumber; char;policy file number 
@param Liability ; char; Liability Amount
@param GrossPremium; char; Gross Premium
@param PolicyDate; date; Policy Effective Date
@param CountyID; char; County ID of the property
@param CooperatingID; char; TDI# for the agent
@param DIPStatus; char; Reason for directly issued policy

@throws 3066: Policy # does not exist / CountyID does not exists
@throws 3005: Policy # exists in HOI table / TDI # for agent cannot be blank 
              Policy eff date cannot be in the future


@success 2002;The record was created

@author Yoke Sam C.
@version 1.0
@created Dec 2017
Modified: 
Date          Name         Comments
09/24/2019    Vikas Jain   Added code for the new field fileID in Policyhoi table
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

def var pcPolicyID as int no-undo.
def var pcFileNumber as char no-undo.
def var pcLiability as decimal no-undo.
def var pcGrossPremium as decimal no-undo.
def var pcEffDate as date no-undo.
def var pcCountyID as char no-undo.
def var pccoopAgentID as char no-undo.
def var pcdipstatus as char no-undo.

def var diplist as char no-undo.
def var agenttdi as char no-undo.
def var cFileID as char no-undo.

def var stateid as char no-undo.

{lib/std-def.i}
{lib/normalizefileid.i}


pRequest:getParameter("PolicyID", output pcPolicyID).
find first policy no-lock where policy.policyID = pcPolicyID no-error.
agenttdi = "".
if avail policy then
do:
   find first agent no-lock where agent.agentID = policy.agentID no-error.
      if avail agent then agenttdi = agent.stateUID.
   stateid = policy.stateID.
end.
else pResponse:fault("3066", "Policy ID(" + string(pcPolicyID) + ")").

if (agenttdi = "" or agenttdi = ?) then
   pResponse:fault("3051", "Requesting Agent ID" {&msg-add} "blank").

if can-find(first policyhoi where policyhoi.policyID = pcPolicyID ) 
   then pResponse:fault("3011", "policy" {&msg-add} "policy ID" {&msg-add} string(pcPolicyID)).

pRequest:getParameter("FileNumber", output pcFileNumber).

if pcFileNumber = "" 
   then pResponse:fault("3051", "File Number" {&msg-add} "blank").
   
cFileID = normalizefileid(pcFileNumber).   
if cFileID = "" 
   then pResponse:fault("3051", "Normalize File Number" {&msg-add} "blank").

pRequest:getParameter("Liability", output pcLiability).
if pcLiability < 0 
   then pResponse:fault("3054", "Liability" {&msg-add} "zero").

pRequest:getParameter("GrossPremium", output pcGrossPremium).
if pcGrossPremium < 0
   then pResponse:fault("3054", "Gross Premium." {&msg-add} "Zero").

pRequest:getParameter("PolicyDate", output pcEffDate).
if pcEffDate <> ? and date(pcEffDate) > today 
   then pResponse:fault("3005", "Policy Date cannot be in the future").

pRequest:getParameter("DIPStatus", output pcdipstatus).

diplist = "".
for each sysprop where sysprop.appCode = "OPS" and objAction = "DIPStatus" no-lock:
    diplist = diplist + (if diplist > "" then "," + sysprop.objID else sysprop.objID).

end.

if not can-find(first sysprop where sysprop.appCode = "OPS" 
                                and sysprop.objAction = "DIPStatus"
                                and lookup(pcdipstatus,diplist) > 0)
   then pResponse:fault("3066", "D(irect) I(ssued) Policy status (~"" + pcdipstatus + "~")" ).


pRequest:getParameter("CountyID", output pcCountyID).
if not can-find(first county where county.stateID = stateid and county.countyID = pcCountyID) 
   then pResponse:fault("3066", "county ID (~"" + pcCountyID + "~")").

pRequest:getParameter("CooperatingID", output pccoopAgentID).
if pccoopAgentID = "" 
   then pResponse:fault("3051", "Cooperating Agent ID" {&msg-add} "blank").



if pResponse:isFault()
 then return.

std-in = 0.

/* all parameters are valid so start to insert the new row */
std-lo = false.
TRX-BLOCK:
do TRANSACTION
  on error undo TRX-BLOCK, leave TRX-BLOCK:
  
 create policyhoi.
  assign
     policyhoi.policyID = pcPolicyID
     policyhoi.DIPStatus = integer(pcdipstatus)
     policyhoi.liabilityAmount = pcLiability
     policyhoi.grossPremium = pcGrossPremium
     policyhoi.policyDate = pcEffDate
     policyhoi.fileNumber = pcFIleNumber
     policyhoi.fileID = cFileID
     policyhoi.countyID = pcCountyID
     policyhoi.requestAgentID = agenttdi
     policyhoi.coopAgentID = pccoopAgentID
    .
  
  validate policyhoi.
  release policyhoi.
  
  std-lo = true.
end.

if not std-lo
 then
  do: pResponse:fault("3000", "Create policyhoi (" + string(pcPolicyID) + ")").
      return.
  end.

pResponse:success("2002", "policyhoi (" + string(pcPolicyID) + ")").
