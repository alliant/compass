&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : txt2pdf.p
    Purpose     : Convert a TeXT file to a PDF

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEFINE input-output PARAMETER pTextFile AS CHARACTER NO-UNDO.
DEFINE var tPdf AS CHARACTER NO-UNDO.

DEFINE var tline AS CHARACTER NO-UNDO.
def var tMargin as int no-undo.

{pdf/pdf_inc.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

run lib/tempfile.p (output tPdf).
RUN pdf_new ("Spdf", tPdf).
RUN pdf_set_BottomMargin("Spdf", 50).
tMargin = pdf_LeftMargin("Spdf").
run pdf_set_LeftMargin ("Spdf", tMargin * 2).
RUN pdf_new_page("Spdf").
run pdf_set_font ("Spdf", "Courier", 11.5).
RUN pdf_text_color ("Spdf", 0.0, .0, .0).

INPUT FROM VALUE(pTextFile) NO-ECHO.
REPEAT:
 IMPORT UNFORMATTED tline.
 
 IF INDEX(tline,CHR(12)) > 0
  then 
   do: RUN pdf_new_page("Spdf").
       tline = replace(tline, chr(12), "").
   end.

 RUN pdf_text ("Spdf", tline).
 RUN pdf_skip ("Spdf").
 tline = "".
END.
INPUT CLOSE.

RUN pdf_close("Spdf").

run lib/osdelete.p (pTextFile).
pTextFile = tPdf.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


