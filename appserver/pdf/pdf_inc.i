/* pdf/pdf_inc.i
*/

&if defined(PDFINCLUDED) = 0
  &then
&global-define PDFINCLUDED yes

{pdf/pdfglobal.i "NEW SHARED"}
 
/* Call pdf_inc.p Persistenly */
RUN pdf/pdf_inc.p PERSISTENT SET h_PDFinc.

THIS-PROCEDURE:ADD-SUPER-PROCEDURE(h_PDFinc).

{pdf/pdf_func.i h_PDFinc}
&endif
