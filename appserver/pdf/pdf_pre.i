/*
pdf/pdf_pre.i
Gordon Campbell
July 6, 2005
  
  Description:  Preprocessor defintions for PDFinclude
*/

&GLOBAL-DEFINE PDFDIR 

&IF OPSYS = "UNIX" &THEN
  &GLOBAL-DEFINE zlib          /lib/libz.so.1
&ELSE
  &GLOBAL-DEFINE zlib          zlib1.dll
&ENDIF

&GLOBAL-DEFINE MD5LIB          pdf\dll\md5.exe
&GLOBAL-DEFINE pdfencryptlib   pdf\dll\procryptlib.dll
