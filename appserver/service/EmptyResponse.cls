 
 /*------------------------------------------------------------------------
@file EmptyResponse
@description Class for creating an empty response (used for CRON jobs)
    
@author John Oliver
@created 7/13/2018
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.



CLASS service.EmptyResponse INHERITS service.BaseResponse: 
  /*------------------------------------------------------------------------------
      Purpose:                                      
      Notes:                                      
  ------------------------------------------------------------------------------*/


  CONSTRUCTOR PUBLIC EmptyResponse ():
    SUPER ().
  END CONSTRUCTOR.

  DESTRUCTOR PUBLIC EmptyResponse ( ):
  END DESTRUCTOR.

END CLASS.
