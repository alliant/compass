 
/*------------------------------------------------------------------------
@file EmptyResponse
@description Class for creating an empty request (used for CRON jobs)
    
@author John Oliver
@created 7/13/2018
@modified
    Date       Name       Description
 06/08/2022     VR        Modified to add Clob changes 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.

CLASS service.EmptyRequest INHERITS service.BaseRequest: 

    CONSTRUCTOR PUBLIC EmptyRequest (INPUT pGateway AS service.IGateway):
        SUPER (pGateway).
    END CONSTRUCTOR.   
    
    METHOD PUBLIC OVERRIDE LOGICAL getParameter(INPUT-OUTPUT DATASET-HANDLE hDataSetHandle, OUTPUT pErrMsg AS CHAR):
        RETURN TRUE.
    END METHOD.

    METHOD PUBLIC OVERRIDE LOGICAL setParameter(INPUT DATASET-HANDLE hDataSetHandle):
        RETURN FALSE.
    END METHOD.
    
    METHOD PUBLIC OVERRIDE VOID debug ():
        LOG-MANAGER:WRITE-MESSAGE ("Empty Request:No Parameters").
    END METHOD.

    DESTRUCTOR PUBLIC EmptyRequest ( ):
    END DESTRUCTOR.
END CLASS.
