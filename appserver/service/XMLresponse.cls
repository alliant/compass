/* service/XMLresponse.cls
   XML-based RESPONSE class
   MohanC 10.22.2013
   @Modified    :
   Date        Name    Comments
   04/12/2021  MK      Modified to return XML of the File.
   05/11/2021  MK      Framework Changes.
   12/21/2021  SC      Removed where clause as its not required
   05/06/22    SA      80191 - modified Filename List with '^' seperator
   06/08/2022  VR      Modified to add Clob Changes
 */

USING Progress.Lang.*.

CLASS service.XMLresponse INHERITS service.BaseResponse: 
  
  {tt/file.i}  
 
  CONSTRUCTOR PUBLIC XMLresponse (input pGateway as service.IGateway):
  
    SUPER (pGateway).
    
  END CONSTRUCTOR.

  METHOD PUBLIC OVERRIDE VOID dsOutput():
    DEFINE VARIABLE dsHandle AS HANDLE    NO-UNDO.
    DEFINE VARIABLE lSuccess AS LOGICAL   NO-UNDO.
    DEFINE VARIABLE pCode    AS CHARACTER NO-UNDO.
    DEFINE VARIABLE pMessage AS CHARACTER NO-UNDO.
   
    DEFINE VARIABLE xResponseDoc  AS HANDLE NO-UNDO.
    DEFINE VARIABLE xResponseRoot AS HANDLE NO-UNDO.
    DEFINE VARIABLE xDataSet      AS HANDLE NO-UNDO.
    DEFINE VARIABLE xDataSetRoot  AS HANDLE NO-UNDO.
    DEFINE VARIABLE xDataNode     AS HANDLE NO-UNDO.
    DEFINE VARIABLE xActionStatus AS HANDLE NO-UNDO.
     
    CREATE X-DOCUMENT xResponseDoc.
    CREATE X-NODEREF xResponseRoot.
    
    xResponseDoc:CREATE-NODE(xResponseRoot,"response","ELEMENT").
    xResponseDoc:APPEND-CHILD(xResponseRoot).
    
    CREATE X-NODEREF xActionStatus.

    IF THIS-OBJECT:isFault() THEN DO:
        THIS-OBJECT:getFault(OUTPUT pCode, OUTPUT pMessage).

        xResponseDoc:CREATE-NODE(xActionStatus,"fault","ELEMENT").
        xActionStatus:SET-ATTRIBUTE('code',THIS-OBJECT:encodeForXml(pCode)).
        xActionStatus:SET-ATTRIBUTE('message',THIS-OBJECT:encodeForXml(TRIM(pMessage))).
        xResponseRoot:APPEND-CHILD(xActionStatus).

    END.
    ELSE DO:
        THIS-OBJECT:getSuccess(OUTPUT pCode, OUTPUT pMessage).

        xResponseDoc:CREATE-NODE(xActionStatus,"success","ELEMENT").
        xActionStatus:SET-ATTRIBUTE('code',THIS-OBJECT:encodeForXml(pCode)).
        xActionStatus:SET-ATTRIBUTE('message',THIS-OBJECT:encodeForXml(TRIM(pMessage))).
        xResponseRoot:APPEND-CHILD(xActionStatus).
    END.    

    dsHandle = THIS-OBJECT:dataSetxmlHandle.
    IF VALID-HANDLE(dsHandle) THEN DO:

        CREATE X-DOCUMENT xDataSet.
        CREATE X-NODEREF xDataSetRoot.
        CREATE X-NODEREF xDataNode.

        lSuccess = dsHandle:WRITE-XML('handle',xDataSet,TRUE).
        xDataSet:GET-DOCUMENT-ELEMENT(xDataSetRoot).
        
        xResponseDoc:IMPORT-NODE(xDataNode,xDataSetRoot,TRUE).
        xResponseRoot:APPEND-CHILD(xDataNode).
    END.
     
    xResponseDoc:SAVE('stream','{&web-stream}'). 
  END METHOD.

  METHOD PUBLIC OVERRIDE VOID output():
    define variable pCode as character no-undo.
    define variable pMessage as character no-undo.
    define variable iFileNum as INTEGER no-undo.
    DEFINE BUFFER iData FOR iData.
    DEFINE BUFFER iFile FOR iFile.

    PUT UNFORMATTED '<?xml version="1.0" encoding="iso-8859-1" ?>' SKIP.
    PUT UNFORMATTED '<?xml-stylesheet type="text/css" href="/css/default.css"?>' SKIP.
    PUT UNFORMATTED '<Envelope>' SKIP.
    PUT UNFORMATTED '<Body>' SKIP.
    
    if THIS-OBJECT:isFault()
     then 
      do:
        THIS-OBJECT:getFault(output pCode, output pMessage).
        PUT UNFORMATTED '<Fault code="' THIS-OBJECT:encodeForXml(pCode) '" message="' THIS-OBJECT:encodeForXml(trim(pMessage)) '" />' SKIP.
      end.
     else
      do:
        THIS-OBJECT:getSuccess(output pCode, output pMessage).
        PUT UNFORMATTED '<Success code="' THIS-OBJECT:encodeForXml(pCode) '" message="' THIS-OBJECT:encodeForXml(pMessage) '" />' SKIP.
      end.
          
    FOR EACH iData NO-LOCK
      BREAK BY iData.ObjName
            BY iData.objSeq
            BY iData.objPropSeq
            BY iData.objProperty:
      IF FIRST-OF(iData.ObjName)
       THEN PUT UNFORMATTED '<dataset name="' iData.ObjName '">' SKIP.
      IF FIRST-OF(iData.objSeq)
       THEN PUT UNFORMATTED '<' iData.ObjName ' '.
      PUT UNFORMATTED iData.objProperty '="' THIS-OBJECT:encodeForXml(iData.propertyValue) '" '.
      IF LAST-OF(iData.objSeq)
       THEN PUT UNFORMATTED '/>' SKIP.
      IF LAST-OF(iData.ObjName)
       THEN PUT UNFORMATTED '</dataset>' SKIP.
    END.
    
    IF THIS-OBJECT:hGateway:actionType = "" AND 
       THIS-OBJECT:outputFilenameList <> "" THEN
    DO:
    DO iFileNum = 1 TO NUM-ENTRIES(THIS-OBJECT:outputFilenameList,'^'):

     THIS-OBJECT:splitFileForXml(entry(iFileNum, THIS-OBJECT:outputFilenameList)).

    END.
   
    if THIS-OBJECT:hGateway:clientid <> "" then
    FOR EACH iFile NO-LOCK
      BREAK BY iFile.fileseq
            BY iFile.order:
      IF FIRST-OF(iFile.fileseq)
       THEN
        PUT UNFORMATTED '<dataset name="' "File" '">' SKIP.
      IF FIRST-OF(iFile.order)
       THEN
        PUT UNFORMATTED '<' "File " 'Filename="' iFile.filename '" BaseFileName="' iFile.basefilename '" FileType="' iFile.filetype '" order="' string(iFile.order)  '" '. 
      
      PUT UNFORMATTED "base64" '="' THIS-OBJECT:encodeForXml(iFile.base64) '" '.
      IF LAST-OF(iFile.order)
       THEN
        PUT UNFORMATTED '/>' SKIP.
      IF LAST-OF(iFile.fileseq)
       THEN
         PUT UNFORMATTED '</dataset>' SKIP.
    END.
    else
    FOR EACH iFile NO-LOCK
      BREAK BY iFile.fileseq
            BY iFile.order:
      IF FIRST-OF(iFile.fileseq)
       THEN
       do:
        PUT UNFORMATTED '<dataset name="' "File" '">' SKIP.
        PUT UNFORMATTED '<' "File " 'Filename="' iFile.filename '" BaseFileName="' iFile.basefilename '" FileType="' iFile.filetype '" order="' string(iFile.order)  '" '. 
        PUT UNFORMATTED "base64" '="'.
       end.
       
      PUT UNFORMATTED THIS-OBJECT:encodeForXml(iFile.base64).
        
      IF LAST-OF(iFile.fileseq)
       THEN
       do:
         PUT UNFORMATTED '" '.
         PUT UNFORMATTED '/>' SKIP.
         PUT UNFORMATTED '</dataset>' SKIP.
       end.
    END.
    END.
    PUT UNFORMATTED '</Body>' SKIP.
    PUT UNFORMATTED '</Envelope>' SKIP.
   
    empty temp-table iFile.
    
    
  END METHOD.
  
  METHOD PRIVATE LOGICAL splitFileForXml(INPUT pFileName AS CHAR):
  
   def var cBaseName  as character.
   def var cExtension as character.
   def var cPath      as character.
   def var cDir       as character.
   DEF VAR iSeq#      AS INT INIT 0.
  
   DEFINE BUFFER iData FOR iData.
   DEFINE BUFFER iFile FOR iFile.
  
   cBaseName = entry(num-entries(pFileName, "\"),pFileName,"\").
   cDir      = trim(trim(pFileName,cBaseName),"\").
  
   if search(pFileName) = ?
    then
    do:
     /* checking whether filepath exists in propath */
     cPath = propath. 
  
     if lookup(cDir,cPath) = 0  
      then
       Propath =  Propath + "," + cDir.
  
     if search(pFileName) = ?
      then
       return false.
  
     Propath = cPath.
  
    end.
  
   cExtension = entry(num-entries(cBaseName, "."),cBaseName,".").
  
   run util/splitfile(input pFileName, 
                      output table file).
  
   iSeq# = 0.
   FIND iFile EXCLUSIVE-LOCK
     WHERE iFile.basefilename = cBaseName NO-ERROR.
   if not available iFile then
   do:
      FOR LAST iFile NO-LOCK BY iFile.fileseq:
       iSeq# = iFile.fileseq.
      END.
  
      iSeq# = iSeq# + 1.
  
      for each file where
       file.filename = pFileName:
  
       CREATE iFile.
       ASSIGN
           iFile.filename     = pFileName  
           iFile.basefilename = cBaseName
           iFile.filetype     = cExtension
           iFile.fileseq      = iSeq#
           iFile.order        = file.order
           iFile.base64       = file.base64
           NO-ERROR
           .      
      end.
   end.
   else
   for each file where
    file.filename = pFileName:
  
    ASSIGN
        iFile.filename     = pFileName
        iFile.filetype     = cExtension
        iFile.order        = file.order
        iFile.base64       = file.base64
        NO-ERROR
        . 
   end.
  
   empty temp-table file.
  
   IF ERROR-STATUS:ERROR 
    THEN RETURN FALSE.
    ELSE RETURN TRUE.  
  
  END METHOD.

  METHOD PRIVATE CHARACTER encodeForXml(INPUT pUnencoded AS CHAR):
    pUnencoded = REPLACE(pUnencoded, "&", "&amp;").
    /* The & would have been REPLACEd in the original up above */
    pUnencoded = REPLACE(pUnencoded, "&amp;#10;", "&#10;").
    
    pUnencoded = REPLACE(pUnencoded, "'", "&apos;").
    pUnencoded = REPLACE(pUnencoded, '"', "&quot;").
    pUnencoded = REPLACE(pUnencoded, ">", "&gt;").
    pUnencoded = REPLACE(pUnencoded, "<", "&lt;").
   
    pUnencoded = REPLACE(pUnencoded, CHR(13), "  ").
    pUnencoded = REPLACE(pUnencoded, CHR(145), "&apos;").
    pUnencoded = REPLACE(pUnencoded, CHR(146), "&apos;").
    pUnencoded = REPLACE(pUnencoded, CHR(147), "&quot;").
    pUnencoded = REPLACE(pUnencoded, CHR(148), "&quot;").
  
    RETURN pUnencoded.   /* Function return value. */
  END METHOD.

  DESTRUCTOR PUBLIC XMLresponse():
  END DESTRUCTOR.

END CLASS.
