/* service/gateway
   default GATEWAY
   MohanC 10.26.2013
   @Modified    :
    Date        Name    Comments
    06/21/2019  Archana   Modified to add Notifications
    05/11/2021  MK        Framework Changes
    11/26/2021  Vignesh   Modified to get the absolute path for HTML file
    12/21/2021  SC        Send alert if destination folder does not exist
    05/06/22    SA        80191 - modified Filename List with '^' seperator
    06/08/2022  VR        Modified to add Clob changes
 */

USING Progress.Lang.*.

CLASS service.gateway INHERITS service.BaseGateway:
  
    {tt/file.i}

    CONSTRUCTOR PUBLIC gateway (INPUT pUid AS CHAR,
	                            INPUT pPassword AS CHAR,
	                            INPUT pSessionid AS CHAR,
	                            INPUT pActionid AS CHAR,
	                            INPUT pClientid AS CHAR,
	                            INPUT pUserIP AS CHAR,
                                INPUT pQueryString AS CHAR,
                                INPUT pActionType as CHAR,
                                INPUT pFieldList as CHAR,
                                INPUT pWebUtilitiesHdl AS HANDLE,
                                INPUT pStartupType AS CHAR,
                                INPUT pOutputFormat AS CHAR,
                                INPUT pDestination AS CHAR,
                                INPUT pEmailAddrList AS CHAR,
                                INPUT pPrinterAddr AS CHAR,
                                INPUT pRepository AS CHAR,
                                INPUT pOutputFileFormat AS CHAR,
                                INPUT pNotifyRequestor AS CHAR):
                                
                                
        SUPER (pUid,
	           pPassword,
	           pSessionid,
	           pActionid,
	           pClientid,
	           pUserIP,
               pQueryString,
               pActionType,
               pFieldList,
               pWebUtilitiesHdl,
               pStartupType,
               pOutputFormat,
               pDestination,
               pEmailAddrList,
               pPrinterAddr,
               pRepository,
               pOutputFileFormat,
               pNotifyRequestor).
     
	END CONSTRUCTOR.
	

	METHOD PUBLIC OVERRIDE LOGICAL execAction (INPUT iRequest AS service.IRequest, INPUT iResponse AS service.IResponse):
	    def var tHandlerError as logical init true.
        def var iErrorCount as int.
        def var lExecuted as logical no-undo.
	    
	    etime(TRUE).
	    IF THIS-OBJECT:canExec
	     THEN
	      DO: 
              tHandlerError = true.	          
              RUN-BLOCK:
                 DO ON ERROR UNDO RUN-BLOCK, LEAVE RUN-BLOCK 
                    ON STOP UNDO RUN-BLOCK, LEAVE RUN-BLOCK:
               
                  lExecuted = FALSE.
                  if THIS-OBJECT:actionType = "Q" then
                   RUN VALUE(THIS-OBJECT:queueHandler) (iRequest,iResponse) NO-ERROR.
                   
                  else
                     RUN VALUE(THIS-OBJECT:actionHandler) (iRequest,iResponse) NO-ERROR.
                                    
                  assign
                   lExecuted     = true
                   tHandlerError = error-status:error.

                 END.
                 
                 if (tHandlerError OR NOT lExecuted OR compiler:error) 
                  then 
                   do: 
                       assign
                         THIS-OBJECT:faultCode = "1001"
                         THIS-OBJECT:faultMessage = iRequest:actionid
                         .

                       iResponse:fault(THIS-OBJECT:faultCode, THIS-OBJECT:faultMessage).
                   
                       DO iErrorCount = 1 TO ERROR-STATUS:NUM-MESSAGES:
                         iResponse:fault("1003", ERROR-STATUS:GET-MESSAGE(iErrorCount)).
                       END.

                       DO iErrorCount = 1 TO COMPILER:NUM-MESSAGES:
                         iResponse:fault("1003", COMPILER:GET-MESSAGE(iErrorCount)).
                       END.

                       ASSIGN
                        error-status:error = false
                       /* If there is invalid value in actionHandler (progexec) or queueHandler (queueExec),
                          Progress raises the compilar error condition. After adding all the compilar error 
                          in response object, clear the error status so that webutill.p does not add another 
                          error message in the response XML. */
                        compiler:error     = false. 
                   end.
                 
                 IF THIS-OBJECT:isLog
                  THEN THIS-OBJECT:log(iResponse).  
	      END.
	     ELSE 
	      DO: iResponse:fault(THIS-OBJECT:faultCode, THIS-OBJECT:faultMessage).
	          THIS-OBJECT:log(iResponse).
	      END.
           RETURN iResponse:isFault().
	END METHOD.
    
    METHOD PUBLIC OVERRIDE VOID respond(INPUT iResponse AS service.IResponse):
    
     DEFINE VARIABLE iFilesNotExist AS INTEGER     NO-UNDO.
     DEFINE VARIABLE iFileCount     AS INTEGER     NO-UNDO.
     DEFINE VARIABLE cValidFiles    AS CHARACTER   NO-UNDO.
     DEFINE VARIABLE std-in         AS INTEGER     NO-UNDO.
     DEFINE VARIABLE std-lo         AS LOGICAL     NO-UNDO.
     DEFINE VARIABLE std-ch         AS CHARACTER   NO-UNDO.

     IF THIS-OBJECT:startupType = "WEB" THEN
     DO:
        output to 'web'.
        this-object:outputhttpHeader().
        iResponse:output().
        output close.

     END.
     ELSE IF THIS-OBJECT:startupType = "QUEUE" THEN
      DO:
         assign
          iFilesNotExist = 0
          cValidFiles    = "".

         /* check if file exists */
         if iResponse:outputFilenameList <> ""
          then
           do std-in = 1 to num-entries(iResponse:outputFilenameList, "^"):
             file-info:file-name = entry(std-in, iResponse:outputFilenameList, "^").
             if file-info:full-pathname = ?
              then
               do:
                 assign
                  std-ch = "Report file not found. Expected output file is : " + file-info:file-name
                  iFilesNotExist = iFilesNotExist + 1.
                 iResponse:fault("3005", std-ch).
               end.
             else
              cValidFiles = cValidFiles + file-info:file-name + ",".
           end.

         cValidFiles = trim(cValidFiles, ",").
         
         /* if there is not an error, send the file to the destination */
         if cValidFiles <> "" 
          then
           do:
            if this-object:emailAddrList <> "" then
             do:
              this-object:email(this-object:emailAddrList, cValidFiles, THIS-OBJECT:actionid, output std-lo, output std-ch).
              if not std-lo
               then
                iResponse:fault("3005", std-ch).
             end.
            if this-object:printerAddr <> "" then
             do iFileCount = 1 to num-entries(cValidFiles, ","):
              file-info:file-name = entry(iFileCount, cValidFiles, ",").
              this-object:printer(this-object:printerAddr, file-info:full-pathname, output std-lo, output std-ch).
              if not std-lo
               then
                iResponse:fault("3005", std-ch).
             end. /* file count loop */
            if this-object:repository <> "" then
             do:
              if this-object:repository = 'A' then
               do iFileCount = 1 to num-entries(cValidFiles, ","):
                 file-info:file-name = entry(iFileCount, cValidFiles, ",").
                 this-object:repository(this-object:repository, file-info:full-pathname, output std-lo, output std-ch).
                 if not std-lo
                  then
                   iResponse:fault("3005", std-ch).
               end. /* file count loop */ 
              else
               do iFileCount = 1 to num-entries(cValidFiles, ","):
                 file-info:file-name = entry(iFileCount, cValidFiles, ",").
                 this-object:fileStorage(this-object:repository, file-info:full-pathname, output std-lo, output std-ch).
                 if not std-lo
                  then
                   iResponse:fault("3005", std-ch).
               end. /* file count loop */             
             end. /* this-object:repository <> "" */
           end. /* not lError and lFileFound */
      END.
      
    END METHOD.

    METHOD PUBLIC OVERRIDE VOID dsRespond(INPUT iResponse AS service.IResponse):
        IF THIS-OBJECT:startupType = "WEB" THEN
        DO:
            OUTPUT TO 'web'.
            THIS-OBJECT:outputhttpHeader().
            iResponse:dsOutput().
            OUTPUT CLOSE.
        END.
    END METHOD.

  	METHOD PUBLIC OVERRIDE LOGICAL notification (INPUT iResponse AS service.IResponse):
    
	 DEFINE VAR iFault AS LOGICAL NO-UNDO.
     DEFINE VAR pTable AS HANDLE.
	    
     iResponse:getNotifications(OUTPUT TABLE-HANDLE pTable).
     iFault = iResponse:isFault().
     
      IF CONNECTED("COMPASS") 
       AND NOT iFault
       and iResponse:hasNotifications()
       THEN
        DO: /*TRANSACTION:*/
         RUN util/sysnotificationadd.p(INPUT TABLE-HANDLE pTable,
                                       input THIS-OBJECT:Uid,
                                       input THIS-OBJECT:actionid).
        END.
       ELSE RETURN FALSE.
      RETURN TRUE.   /* Function return value. */
    END METHOD.   

	METHOD PUBLIC OVERRIDE LOGICAL log (INPUT iResponse AS service.IResponse):
	 DEFINE VAR iFaultCode AS CHAR NO-UNDO.
	 DEFINE VAR iFaultMessage AS CHAR NO-UNDO.
	 DEFINE VAR iRefType AS CHAR NO-UNDO.
	 DEFINE VAR iRefNum AS CHAR NO-UNDO.
	 DEFINE VAR iSysMessage AS CHAR NO-UNDO.
	 DEFINE VAR iFault AS LOGICAL NO-UNDO.
     DEFINE VAR iAction AS CHAR NO-UNDO.
     
	    
     iResponse:getFault(OUTPUT iFaultCode,OUTPUT iFaultMessage).  
     iResponse:getLogMessage(OUTPUT iRefType,OUTPUT iRefNum,OUTPUT iSysMessage).
     iFault = iFaultCode > "".
     
     IF CONNECTED("COMPASS") 
      THEN
       DO: /*TRANSACTION:*/
         RUN util/syslogadd.p
            (THIS-OBJECT:actionid,
             THIS-OBJECT:Uid,
             THIS-OBJECT:role,
             THIS-OBJECT:ipAddress,
             if THIS-OBJECT:actionType = "Q" then THIS-OBJECT:queueHandler else THIS-OBJECT:actionHandler,
             THIS-OBJECT:clientid,  /* Store clientID(COM,CLM,AMD...) in sysLog.applicationID */
             iFault,
             iFaultCode,
             iFaultMessage,
             iRefType,
             iRefNum,
             iSysMessage
             ).
       END.
      ELSE RETURN FALSE.
     RETURN TRUE.   /* Function return value. */
	END METHOD.
	
    METHOD PUBLIC OVERRIDE VOID debug ():
     if log-manager:logfile-name > ""
      then
       do:
         LOG-MANAGER:WRITE-MESSAGE ("Client: " + THIS-OBJECT:clientid,THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Session: " + THIS-OBJECT:sessionid,THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Action: " + THIS-OBJECT:actionid,THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Valid Action: " + STRING(THIS-OBJECT:validAction),THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Action Active: " + STRING(THIS-OBJECT:isActive),THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Anonymous Access: " + STRING(THIS-OBJECT:isAnonymous),THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Secure: " + STRING(THIS-OBJECT:isSecure),THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Log: " + STRING(THIS-OBJECT:isLog),THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Audit: " + STRING(THIS-OBJECT:isAudit),THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Action Handler: " + THIS-OBJECT:actionHandler,THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Queue Action: " +  THIS-OBJECT:queueHandler,THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Uid: " + THIS-OBJECT:Uid,THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Password: XXX",THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Active User: " + STRING(THIS-OBJECT:isUserActive),THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Name: " + STRING(THIS-OBJECT:name),THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Initials: " + STRING(THIS-OBJECT:initials),THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Valid Userids: " + THIS-OBJECT:Userids,THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("IP Address : " + THIS-OBJECT:ipAddress,THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Valid IPs: " + THIS-OBJECT:ipAddresses,THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Role: " + THIS-OBJECT:role,THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("Valid Roles: " + THIS-OBJECT:roles,THIS-OBJECT:GetClass():TypeName).
         LOG-MANAGER:WRITE-MESSAGE ("QueryString: " + THIS-OBJECT:queryString,THIS-OBJECT:GetClass():TypeName).
       end.
      else
       do:
         message "Client: " + THIS-OBJECT:clientid view-as alert-box.
         message "Session: " + THIS-OBJECT:sessionid view-as alert-box.
         message "Action: " + THIS-OBJECT:actionid view-as alert-box.
         message "Valid Action: " + STRING(THIS-OBJECT:validAction) view-as alert-box.
         message "Action Active: " + STRING(THIS-OBJECT:isActive) view-as alert-box.
         message "Anonymous Access: " + STRING(THIS-OBJECT:isAnonymous) view-as alert-box.
         message "Secure: " + STRING(THIS-OBJECT:isSecure) view-as alert-box.
         message "Log: " + STRING(THIS-OBJECT:isLog) view-as alert-box.
         message "Audit: " + STRING(THIS-OBJECT:isAudit) view-as alert-box.
         message "Action Handler: " + THIS-OBJECT:actionHandler view-as alert-box.
         message "Uid: " + THIS-OBJECT:Uid view-as alert-box.
         message "Password: XXX" view-as alert-box.
         message "Active User: " + STRING(THIS-OBJECT:isUserActive) view-as alert-box.
         message "Name: " + STRING(THIS-OBJECT:name) view-as alert-box.
         message "Initials: " + STRING(THIS-OBJECT:initials) view-as alert-box.
         message "Valid Userids: " + THIS-OBJECT:Userids view-as alert-box.
         message "IP Address : " + THIS-OBJECT:ipAddress view-as alert-box.
         message "Valid IPs: " + THIS-OBJECT:ipAddresses view-as alert-box.
         message "Role: " + THIS-OBJECT:role view-as alert-box.
         message "Valid Roles: " + THIS-OBJECT:roles view-as alert-box.
         message "QueryString: " + THIS-OBJECT:queryString view-as alert-box.
       end.
    END METHOD.
    
    METHOD PRIVATE CHARACTER email(INPUT pEmail as char, INPUT pFile as char, INPUT pAction as char, OUTPUT pSuccess as logical, OUTPUT pMsg as char):
 
     /* variables */
     define variable cHTML    as character no-undo initial "".
     define variable cSubject as character no-undo.
     &scoped-define DefaultEmail "html\queueFinish.html"
     &scoped-define DefaultSubject "File from Alliant National"
     
    
     /* get the action */
     if not can-find(first iData where objName = "parameter" and objProperty = "action")
      then
       do:
         create iData.
         assign
           iData.objSeq      = iData.objSeq + 1
           iData.objName     = "parameter"
           iData.objProperty = "action"
           .
       end.
       
     for first iData exclusive-lock
        where iData.objName = "parameter"
          and iData.objProperty = "action":
         
        /* Action name */
        iData.propertyValue = THIS-OBJECT:displayName.
	
        if iData.propertyValue = "" or iData.propertyValue = ?
         then iData.propertyValue = pAction.
         
        /* HTML email */
        cHTML = THIS-OBJECT:emailHtml.
        if cHTML = "" or cHTML = ?
         then cHTML = {&DefaultEmail}.
        
        /* To get the absolute path */
        cHTML = search(cHTML). 
         
        /* HTML subject */
        cSubject = THIS-OBJECT:emailSubject.
        if cSubject = "" or cSubject = ?
         then cSubject = {&DefaultSubject}.

     end.

     /* send the email */
     run util/sendhtmlmail.p (input  cHTML,
                              input  "",
                              input  pEmail,
                              input  cSubject,
                              input  pFile,
                              input  table iData,
                              output pSuccess).
                              
     if not pSuccess
      then pMsg = "Report " + pFile + " not emailed to " + pEmail.   
      
    END METHOD.
    
    METHOD PRIVATE CHARACTER printer(INPUT pPrinter as char, INPUT pFile as char, OUTPUT pSuccess as logical, OUTPUT pMsg as char):
    
     /* variables */
     define variable iErrorCode as integer no-undo.
     define variable std-ch as character no-undo.
     &scoped-define SumatraPDF "C:\Progress\OpenEdge\bin\SumatraPDF.exe"
     
     assign
      pSuccess = false
      std-ch   = {&SumatraPDF} + ' -silent -print-to "' + pPrinter + '" ' + pFile.    
     
     os-command silent value(std-ch). 
     iErrorCode = os-error.
     if iErrorCode > 0
      then pMsg = "Report " + pFile + " not printed to " + pPrinter + ". The error is: " + this-object:getError(iErrorCode).
      else pSuccess = true.

    END METHOD.
    
    METHOD PRIVATE CHARACTER fileStorage(INPUT pDestination as char, INPUT pFile as char, OUTPUT pSuccess as logical, OUTPUT pMsg as char):
    
     /* parameters */
     define variable iErrorCode as integer no-undo.
     define variable std-in as integer no-undo.
     define variable std-ch as character no-undo.
     define variable cSubj as character no-undo.
     define variable cBody as character no-undo.

     pSuccess = false.

     pDestination = right-trim(pDestination, "\").
     pDestination = pDestination + "\".
     file-info:file-name = pDestination.
     if file-info:full-pathname = ?
      then
       do: 
          assign
             cSubj = (if this-object:ActionDesc <> "" and this-object:ActionDesc <> ? then this-object:ActionDesc else this-object:Actionid) + " failed." 
             cBody = "Action: " + (if this-object:ActionDesc <> "" and this-object:ActionDesc <> ? 
                                    then 
                                     (this-object:ActionDesc + " (") 
                                    else "") 
                                + this-object:Actionid
                                + (if this-object:ActionDesc <> "" and this-object:ActionDesc <> ? then ")" else "")
                                + '<BR>'
                                + "Destination folder '" + pDestination + "'" + " does not exist."
                                . 

          run util/simplemail.p (input "no-reply@alliantnational.com", 
                                 input this-object:Uid,
                                 input cSubj,
                                 input cBody).
          pMsg = "Action: " + this-object:Actionid + ". Destination folder '" + pDestination + "'" + " does not exist.".
          return "". 
       end.
     
     iErrorCode = 0.
     os-copy value(pFile) value(pDestination).
     iErrorCode = os-error.
     if iErrorCode > 0
      then pMsg = "Report " + file-info:full-pathname + " not copied to " + pDestination + ". The error is: " + this-object:getError(iErrorCode).
      else pSuccess = true.
     
    END METHOD.
    
    METHOD PRIVATE CHARACTER repository(INPUT pRepository as char, INPUT pFile as char, OUTPUT pSuccess as logical, OUTPUT pMsg as char):

     pSuccess = false.

     /* variables */
     define variable cEntity        as character no-undo.
     define variable cEntityID      as character no-undo.
     define variable cCategory      as character no-undo initial "".
     define variable cUser          as character no-undo.
     
     cUser = THIS-OBJECT:uid.

     /* Before using make sure its valid and has value . */
     if this-object:storageEntity > "" and this-object:storageEntityId > ""
      then
       do:
         run util\UploadFile.p (cUser, true, this-object:storageEntity, this-object:storageEntityId, cCategory, pFile, output pSuccess, output pMsg).
         if not pSuccess
          then
           pMsg = "Report " + pFile + " not saved to repository. The error is: " + pMsg.
       end.
     else 
      pMsg = "Report " + pFile + " not saved to repository. The Entity and EntityID was missing.".

    END METHOD.
    
    METHOD PRIVATE CHARACTER getError(INPUT pCode as int):
    
     &scoped-define No1 "Not owner"
     &scoped-define No2 "No such file or directory"
     &scoped-define No3 "Interrupted system call"
     &scoped-define No4 "I/O error"
     &scoped-define No5 "Bad file number"
     &scoped-define No6 "No more processes"
     &scoped-define No7 "Not enough core memory"
     &scoped-define No8 "Permission denied"
     &scoped-define No9 "Bad address"
     &scoped-define No10 "File exists"
     &scoped-define No11 "No such device"
     &scoped-define No12 "Not a directory"
     &scoped-define No13 "Is a directory"
     &scoped-define No14 "File table overflow"
     &scoped-define No15 "Too many open files"
     &scoped-define No16 "File too large"
     &scoped-define No17 "No space left on device"
     &scoped-define No18 "Directory not empty"
     
     define variable pDesc as character.
     
     case pCode:
       when 1  then pDesc = {&No1}.
       when 2  then pDesc = {&No2}.
       when 3  then pDesc = {&No3}.
       when 4  then pDesc = {&No4}.
       when 5  then pDesc = {&No5}.
       when 6  then pDesc = {&No6}.
       when 7  then pDesc = {&No7}.
       when 8  then pDesc = {&No8}.
       when 9  then pDesc = {&No9}.
       when 10 then pDesc = {&No10}.
       when 11 then pDesc = {&No11}.
       when 12 then pDesc = {&No12}.
       when 13 then pDesc = {&No13}.
       when 14 then pDesc = {&No14}.
       when 15 then pDesc = {&No15}.
       when 16 then pDesc = {&No16}.
       when 17 then pDesc = {&No17}.
       when 18 then pDesc = {&No18}.
     end case.
     RETURN pDesc.   /* Function return value. */
     
    END METHOD.
    
	DESTRUCTOR PUBLIC gateway():
	END DESTRUCTOR.

END CLASS.
















