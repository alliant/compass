/* service/IResponse
   RESPONSE Interface
   MohanC 10.22.2013
   11.26.2014 D.Sinclair Added ApplicationCode to sendEvents()
   @Modified    :
    Date        Name    Comments
    06/21/2019  Archana   Modified to add Notifications
    04/12/2021  MK        Added addFile Method
    05/11/2021  MK        Framework Changes
    06/08/2022  VR        Modified to add Clob Changes
    08/04/2022  SC        Task#96557 - Overloaded the method 'addfile' to prevent files 
                           being added to temp directory
 */

USING Progress.Lang.*.

INTERFACE service.IResponse:

    /* iData temp-table definition for storing parameters */
    {service/IData.i}

    DEFINE public PROPERTY outputFilenameList AS CHAR NO-UNDO
    GET.
    SET.
    METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHAR,INPUT pValue AS CHAR).
    METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHAR,INPUT pValue AS DATE).
    METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHAR,INPUT pValue AS DATETIME).
    METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHAR,INPUT pValue AS DECIMAL).
    METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHAR,INPUT pValue AS INTEGER).
    METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHAR,INPUT pValue AS LOGICAL).
    METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHAR,INPUT TABLE-HANDLE pTable).
    METHOD PUBLIC LOGICAL setParameter(INPUT DATASET-HANDLE pDataset). 

    METHOD PUBLIC LOGICAL addFile(INPUT pName AS CHARACTER).
    METHOD PUBLIC LOGICAL addFile(INPUT pName AS CHARACTER, INPUT pPermanent AS LOGICAL).
    
    METHOD PUBLIC LOGICAL fault(INPUT pCode AS CHAR, INPUT pMessage AS CHAR).
    METHOD PUBLIC LOGICAL addFault(INPUT pCode AS CHAR, INPUT pMessage AS CHAR).
    METHOD PUBLIC LOGICAL getFault(OUTPUT pCode AS CHAR, OUTPUT pMessage AS CHAR).
    METHOD PUBLIC LOGICAL isFault().
    METHOD PUBLIC LOGICAL success(INPUT pCode AS CHAR, INPUT pMessage AS CHAR).
    METHOD PUBLIC LOGICAL logMessage(INPUT pRefType AS CHAR,INPUT pRefNum AS CHAR,INPUT pMessage AS CHAR).
    METHOD PUBLIC LOGICAL getLogMessage(OUTPUT pRefType AS CHAR,OUTPUT pRefNum AS CHAR,OUTPUT pMessage AS CHAR).
    METHOD PUBLIC LOGICAL getSuccess(OUTPUT pCode AS CHAR, OUTPUT pMessage AS CHAR).

    method public logical alert(input pSubject as char, input pMessage as char).
    method public logical hasAlert(output pSubject as char, output pMessage as char).

    /* UID: required where we want to create notification for specific user only. For example request Feedback from an employee
       EntityType: (A)gent, (P)erson, (E)mployee
       NotificationStat: (n)ew, (s)upressed, (d)ismissed
       SourceType: Claim, Audit, Remittance etc. */
    method public void notify(input pUD as character,
                              input pEntityType as character,
                              input pEntityID as character, 
                              input pRefSysNotificationID as integer,
                              input pNotificationStat as character, 
                              input pSourceType as character,
                              input pSourceID as character,
                              input pNotification as character).  
    method public logical hasNotifications().                                 
    method public char getNotifications(output table-handle pTable).   

    method public void createEvent(input pName as char, input pKey as char).  
    method public void updateEvent(input pName as char, input pKey as char).
    method public void deleteEvent(input pName as char, input pKey as char).
    method public void otherEvent(input pName as char, input pKey as char, input pAction as char, input pMsg as char).
    method public logical hasEvents().                                 
    method public void sendEvents(input tStompClient as dotr.Stomp.StompClient,
                                  input tUserID as char,
                                  input tSystemAction as char,
                                  input tClientID as char,
                                  input tSessionID as char,
                                  input tIPAddress as char).
    method public char getEvents().
    
    METHOD PUBLIC VOID output().

    METHOD PUBLIC VOID dsOutput().  
  
    method public void debug().
    
    method PUBLIC LOGICAL setStorageKeys (input pStorageEntity as char, input pStorageEntityId as char).
    method public logical setEmailParameter (input pEmailObjectName as char, input pEmailObjectValue as char).
    
END INTERFACE.
