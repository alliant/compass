 
 /*------------------------------------------------------------------------
@file JSONrequest.cls
@description Class for receiving JSON requests

@author John Oliver
@created 6/21/2018

@modified 
05/11/2021  MK        Framework Changes.
06/08/2022  VR        Modified to add Clob changes
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.

CLASS service.JSONrequest INHERITS service.BaseRequest:
  /*------------------------------------------------------------------------------
      Purpose:                                      
      Notes:                                      
  ------------------------------------------------------------------------------*/
    DEFINE PUBLIC PROPERTY dataSetJsonHandle AS handle NO-UNDO
    GET.
    PRIVATE SET.

    CONSTRUCTOR PUBLIC JSONrequest (INPUT pGateway AS service.IGateway):
        define variable pJsonString as character no-undo.

        SUPER (pGateway).
        RUN util/json2tt.p (INPUT-OUTPUT table iData, output pJsonString).

        IF pJsonString = '' OR pJsonString = ? THEN RETURN.

        create iData.
        assign
           iData.objSeq = 999
           iData.objName = "parameter"
           iData.objPropSeq = 1
           iData.objProperty = "JSON"
           iData.propertyValue = pJsonString
           iData.searchName = iData.objName + "." + iData.objProperty
          .

    END CONSTRUCTOR.

    CONSTRUCTOR PUBLIC JSONrequest (INPUT pGateway AS service.IGateway,INPUT hDataSetHandle AS HANDLE):
        SUPER (pGateway).
        THIS-OBJECT:dataSetJsonHandle = hDataSetHandle.
    END CONSTRUCTOR.

	METHOD PUBLIC OVERRIDE LOGICAL getParameter(INPUT-OUTPUT DATASET-HANDLE hDataSetHandle, OUTPUT pErrMsg AS CHAR):
		DEFINE VARIABLE lSuccess AS LOGICAL   NO-UNDO.
		DEFINE VARIABLE iCount   AS INTEGER   NO-UNDO.  
        
		IF NOT VALID-HANDLE(hDataSetHandle) 
		 THEN 
		  DO:	
			pErrMsg = 'Invalid DataSet Handle'.
			RETURN FALSE.
		  END.
		IF NOT VALID-HANDLE(THIS-OBJECT:dataSetJsonHandle) 
		 THEN 
		  DO:	
			pErrMsg = 'Invalid JSON Handle'.
			RETURN FALSE.
		  END.
		lSuccess = hDataSetHandle:READ-XML('handle', THIS-OBJECT:dataSetJsonHandle,'empty',?,?,?) NO-ERROR.
		IF NOT lSuccess 
		 THEN 
		  DO:	
			DO iCount = 1 TO ERROR-STATUS:NUM-MESSAGES:
			  pErrMsg = pErrMsg + ERROR-STATUS:GET-MESSAGE(iCount) + ' '. 
			END.
			RETURN FALSE.
		  END.
		RETURN TRUE.  
	END METHOD.

    METHOD PUBLIC OVERRIDE LOGICAL setParameter(INPUT DATASET-HANDLE hDataSetHandle):
        DEFINE VARIABLE hDoc AS HANDLE NO-UNDO.
        CREATE X-DOCUMENT hDoc.
        hDataSetHandle:WRITE-JSON('handle',hDoc).
        THIS-OBJECT:dataSetJsonHandle = hDoc.
    END METHOD.

    DESTRUCTOR PUBLIC JSONrequest ( ):
    END DESTRUCTOR.

END CLASS.
