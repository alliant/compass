/*------------------------------------------------------------------------
    File        : service/IProps.i
    Description : Standard properties used by IGateway and IRequest
    Author(s)   : B.Johnson
    Created     : 4/25/2014
    
    Arguments   : &Mode     Access mode of property - PUBLIC, PRIVATE,
                            or PROTECTED
                  &Get      GET accessor, i.e. GET. or PRIVATE GET.
                  &Set      SET accessor, i.e. SET. or PRIVATE SET.
    
    Notes       : 10.09.2014 D.Sinclair  added Email properties
                  08.21.2015 J.Oliver    Added the user’s email address
                  09.04.2015 J.Oliver    Added fields for password timeout
                  05.11.2021 MK          Framework Changes
                  12.21.2021 SC          Added property ActionDesc
  ----------------------------------------------------------------------*/

    &IF DEFINED(Mode) = 0 &THEN
    &SCOPED-DEFINE Mode PUBLIC
    &ENDIF    

    &IF DEFINED(Get) = 0 &THEN
    &SCOPED-DEFINE Get GET.
    &ENDIF    

    /* User Properties */
    DEFINE {&Mode} PROPERTY Uid AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY password AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY name AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY initials AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY role AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY canExec AS LOGICAL NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY email AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY passwordSetDate AS DATETIME NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY passwordExpireDate AS DATETIME NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY passwordExpired AS LOGICAL NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY passwordWarnDays AS CHAR NO-UNDO
    {&Get}
    {&Set}

    /* Request Properties */
    DEFINE {&Mode} PROPERTY actionid AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY sessionid AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY clientid AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY ipAddress AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY queryString AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY isUserActive AS LOGICAL NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY fieldList AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY webUtilitiesHdl AS handle NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY startupType AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY outputFormat AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY destination AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY emailAddrList AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY printerAddr AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY repository AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY outputFileFormat AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY notifyRequestor AS CHAR NO-UNDO
    {&Get}
    {&Set}
    
    

    /* Action Properties */
    DEFINE {&Mode} PROPERTY validAction AS LOGICAL NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY isSecure AS LOGICAL NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY isActive AS LOGICAL NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY isQueueable AS LOGICAL NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY isAnonymous AS LOGICAL NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY isLog AS LOGICAL NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY isAudit AS LOGICAL NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY actionHandler AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY userids AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY roles AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY ipAddresses AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY isEventsEnabled AS LOGICAL NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY iEmailsEnabled AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY iEmailSuccess AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY iEmailFailure AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY queueHandler AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY actionType AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY displayName AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY emailHtml AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY emailSubject AS CHAR NO-UNDO
    {&Get}
    {&Set}
    DEFINE {&Mode} PROPERTY actionDesc AS CHAR NO-UNDO
    {&Get}
    {&Set}
 


    
