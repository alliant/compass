
/*------------------------------------------------------------------------
    File        : IDebuggable
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : MohanC
    Created     : Wed Oct 30 23:32:42 CDT 2013
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.

INTERFACE service.IDebuggable:  
    
    METHOD PUBLIC VOID debug ().
  
END INTERFACE.