/* service/IGateway
   GATEWAY Interface
   MohanC 10.30.2013
     @Modified    :
    Date        Name    Comments
    06/21/2019  Archana   Modified to add Notifications
    05/11/2021  MK        Framework Changes
    06/08/2022  VR        Modified to add Clob changes
 */


USING Progress.Lang.*.

INTERFACE service.IGateway:  

    /* Standard Properties */
    {service/IProps.i}

    /* System Error Properties */
    DEFINE PUBLIC PROPERTY eventid AS CHAR NO-UNDO
    GET.
    SET.
    DEFINE PUBLIC PROPERTY eventParams AS CHAR NO-UNDO
    GET.
    SET.
    
    /* Fault Codes */
    DEFINE PUBLIC PROPERTY faultCode AS CHAR NO-UNDO
    GET.
    SET.
    DEFINE PUBLIC PROPERTY faultMessage AS CHAR NO-UNDO
    GET.
    SET.
    DEFINE PUBLIC PROPERTY isFault AS LOGICAL NO-UNDO
    GET.
    SET.    
    
    METHOD PUBLIC LOGICAL execAction (INPUT iRequest AS service.IRequest,INPUT iResponse AS service.IResponse).
    METHOD PUBLIC LOGICAL log (INPUT iResponse AS service.IResponse).
    METHOD PUBLIC LOGICAL notification (INPUT iResponse AS service.IResponse).
    METHOD PUBLIC VOID respond(INPUT iResponse AS service.IResponse).
    METHOD PUBLIC VOID dsRespond(INPUT iResponse AS service.IResponse). 
    METHOD PUBLIC VOID outputhttpHeader().
    METHOD PUBLIC LOGICAL setStorageKeys (input pStorageEntity as char, input pStorageEntityId as char).
    method public void debug().
    METHOD PUBLIC LOGICAL setEmailParameter (input pEmailObjectName as char, input pEmailObjectValue as char).
    
END INTERFACE.
