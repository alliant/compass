
/*------------------------------------------------------------------------
    File        : IRequest
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : MohanC
    Created     : Tue Oct 22 16:08:23 CDT 2013
    Notes       :
    Modified    :
        Date       Name         Description
     06/08/2022     VR          Modified to add Clob changes  
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.

INTERFACE service.IRequest: 

    /* Standard Properties */
    {service/IProps.i}

    METHOD PUBLIC LOGICAL getParameter (INPUT pName AS CHAR,OUTPUT pValue AS CHAR).
    METHOD PUBLIC LOGICAL getParameter (INPUT pName AS CHAR,OUTPUT pValue AS DATE).
    METHOD PUBLIC LOGICAL getParameter (INPUT pName AS CHAR,OUTPUT pValue AS DECI).
    METHOD PUBLIC LOGICAL getParameter (INPUT pName AS CHAR,OUTPUT pValue AS INT).
    METHOD PUBLIC LOGICAL getParameter (INPUT pName AS CHAR,OUTPUT pValue AS LOGICAL).
    METHOD PUBLIC LOGICAL getParameter (INPUT pName AS CHAR,INPUT-OUTPUT TABLE-HANDLE pTable).
    METHOD PUBLIC LOGICAL getParameter (INPUT-OUTPUT DATASET-HANDLE hDataSetHandle, OUTPUT pErrMsg AS CHAR).
    
    METHOD PUBLIC LOGICAL getParameterTable (OUTPUT TABLE-HANDLE pTable).  

    METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHAR,INPUT pValue AS CHAR).
    METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHAR,INPUT pValue AS DATE).
    METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHAR,INPUT pValue AS DECI).
    METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHAR,INPUT pValue AS INT).
    METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHAR,INPUT pValue AS LOGICAL).
    METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHAR,INPUT TABLE-HANDLE pTable).
    METHOD PUBLIC LOGICAL setParameter(INPUT DATASET-HANDLE hDataSetHandle). 

    method public void debug().
  
END INTERFACE.
