/*------------------------------------------------------------------------
    File        : service/iFile.i
    Description : 
    Author(s)   : MK
    Created     : 03/31/2021
    
    Arguments   : &Mode     Temp-table access mode: PRIVATE or PROTECTED
                            for use within classes.  Leave blank if
                            used within a regular procedure.
    
    Notes       :
  ----------------------------------------------------------------------*/

  DEFINE {&Mode} TEMP-TABLE iFile
  field filename     as char
  field basefilename as char
  field filetype     as char
  field fileseq      as int
  field order        as int
  field base64       as char.
