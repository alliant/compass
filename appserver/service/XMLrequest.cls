/* service/XMLrequest
   XML-based REQUEST
   MohanC 10.22.2013
   @Modified
   05/11/2021   MK  Framework Changes.
   06/08/2022   VR  Modified to add Clob Changes
 */

USING Progress.Lang.*.

CLASS service.XMLrequest INHERITS service.BaseRequest: 
    DEFINE PUBLIC PROPERTY dataSetXmlHandle AS handle NO-UNDO
    GET.
    private set.

	CONSTRUCTOR PUBLIC XMLrequest (INPUT pGateway AS service.IGateway):
		SUPER (pGateway).
		RUN util/xml2tt.p (INPUT-OUTPUT table iData) NO-ERROR.
	END CONSTRUCTOR.

    CONSTRUCTOR PUBLIC XMLrequest (INPUT pGateway AS service.IGateway,INPUT hDataSetHandle AS HANDLE):
		SUPER (pGateway).
		THIS-OBJECT:dataSetXmlHandle = hDataSetHandle.
	END CONSTRUCTOR.

    METHOD PUBLIC OVERRIDE LOGICAL getParameter(INPUT-OUTPUT DATASET-HANDLE hDataSetHandle, OUTPUT pErrMsg AS CHAR):
        DEFINE VARIABLE lSuccess AS LOGICAL   NO-UNDO.
        DEFINE VARIABLE iCount   AS INTEGER   NO-UNDO.
        
        IF NOT VALID-HANDLE(hDataSetHandle) 
         THEN 
          DO:	
            pErrMsg = 'Invalid DataSet Handle'.
            RETURN FALSE.
          END.
        IF NOT VALID-HANDLE(THIS-OBJECT:dataSetXmlHandle) 
         THEN 
          DO:
            pErrMsg = 'Invalid XML Handle'.
            RETURN FALSE.
          END.
        lSuccess = hDataSetHandle:READ-XML('handle', THIS-OBJECT:dataSetXmlHandle,'empty',?,?,?) NO-ERROR.
        IF NOT lSuccess OR ERROR-STATUS:ERROR
         THEN 
          DO:	
            DO iCount = 1 TO ERROR-STATUS:NUM-MESSAGES:
              pErrMsg = pErrMsg + ERROR-STATUS:GET-MESSAGE(iCount) + ' '. 
            END.
            RETURN FALSE.
          END.
        RETURN TRUE.  
    END METHOD.

    METHOD PUBLIC OVERRIDE LOGICAL setParameter(INPUT DATASET-HANDLE hDataSetHandle):
        DEFINE VARIABLE hDoc AS HANDLE NO-UNDO.
        CREATE X-DOCUMENT hDoc.
        hDataSetHandle:WRITE-XML('handle',hDoc).
        THIS-OBJECT:dataSetXmlHandle = hDoc.
    END METHOD. 

    DESTRUCTOR PUBLIC XMLrequest ( ):
	END DESTRUCTOR.

END CLASS.
