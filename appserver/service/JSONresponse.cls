 
 /*------------------------------------------------------------------------
@file JSONresponse
@description Class for sending JSON responses
    
@author John Oliver
@created 6/21/2018
@Modification
Date        Name        Description
09/30/2019  Gurvindar   Added encoding to handle new line characters and spaces. 
04/12/2021  Shubham     File response related changes.
05/11/2021  MK          Framework Changes.
05/06/22    SA          80191 - modified Filename List with '^' seperator
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.



CLASS service.JSONresponse INHERITS service.BaseResponse: 
  /*------------------------------------------------------------------------------
      Purpose:                                      
      Notes:                                      
  ------------------------------------------------------------------------------*/
  
  {tt/file.i}

  CONSTRUCTOR PUBLIC JSONresponse (input pGateway as service.IGateway):
    SUPER (pGateway).
  END CONSTRUCTOR.

  METHOD PUBLIC OVERRIDE VOID output():
    define variable pCode as character no-undo.
    define variable pMessage as character no-undo.
    define variable iFinalCount as integer no-undo initial 0.
    define variable iFileCount as integer no-undo initial 0.
    define variable iCount as integer no-undo initial 0.
    define variable iFileNum as integer no-undo initial 0.
    DEFINE BUFFER iData FOR iData.
    DEFINE BUFFER iFile FOR iFile.
    
    /* count the rows */
    for each iData no-lock:
      iFinalCount = iFinalCount + 1.
    end.

    PUT UNFORMATTED '~{' SKIP.
    PUT UNFORMATTED '"Envelope": ~{' SKIP.
    PUT UNFORMATTED '"Body": ~{' SKIP.
    
    if THIS-OBJECT:isFault()
     then 
      do:
        THIS-OBJECT:getFault(output pCode, output pMessage).
        PUT UNFORMATTED '"Fault": ~{' SKIP.
        put unformatted '"code": "' + string(pCode) + '",' skip.
        put unformatted '"message": "' + string(pMessage) + '"' skip.
        put unformatted '~},' skip.
      end.
     else
      do:
        THIS-OBJECT:getSuccess(output pCode, output pMessage).
        PUT UNFORMATTED '"Success": ~{' SKIP.
        put unformatted '"code": "' + this-object:encodeForJSON(pCode) + '",' skip.
        put unformatted '"message": "' + this-object:encodeForJSON(pMessage) + '"' skip.
        put unformatted '~},' skip.
      end.
    
    PUT UNFORMATTED '"dataset": ~['.
    FOR EACH iData NO-LOCK
    BREAK BY iData.ObjName
          BY iData.objSeq
          BY iData.objPropSeq:
      
      iCount = iCount + 1.
      IF FIRST-OF(iData.ObjName)
       then
        do:
          PUT UNFORMATTED '~{' SKIP.
          PUT UNFORMATTED '"name": "' + this-object:encodeForJSON(iData.ObjName) + '",' SKIP.
          PUT UNFORMATTED '"' + this-object:encodeForJSON(iData.ObjName) + '": ~[' SKIP.
        end.
        
      IF FIRST-OF(iData.objSeq)
       THEN PUT UNFORMATTED '~{' skip.
      
      put UNFORMATTED '"' + this-object:encodeForJSON(iData.objProperty) + '": '.
      if iData.propertyValue = ?
       then put unformatted '""'.
       else put unformatted '"' + this-object:encodeForJSON(iData.propertyValue) + '"'.
       
      IF LAST-OF(iData.objSeq)
       then
        do:
          PUT UNFORMATTED skip '~}'.
          IF not LAST-OF(iData.ObjName)
           THEN PUT UNFORMATTED ',' SKIP.
           else put unformatted '' skip.
        end.
       else PUT UNFORMATTED ',' SKIP.
        
      IF LAST-OF(iData.ObjName)
       then
        do:
          PUT UNFORMATTED '~]~}'.
          if iCount < iFinalCount
           then PUT UNFORMATTED ',' SKIP.
           else put unformatted '' skip.
        end.
    END.

    IF this-object:hGateway:actionType = "" and 
       this-object:outputFilenameList <> "" THEN
    DO:

     DO iFileNum = 1 TO NUM-ENTRIES(THIS-OBJECT:outputFilenameList,'^'):

      THIS-OBJECT:splitFileForJson(entry(iFileNum, THIS-OBJECT:outputFilenameList)).

     END.

     /* count the rows */
     for each ifile no-lock
       break by iFile.fileseq:
       if first-of(iFile.fileseq) 
        then
         iFileCount = iFileCount + 1.
     end.

    /* Adding file Code */
     if THIS-OBJECT:hGateway:clientid <> "" 
      then 
       for each iFile no-lock
         break by iFile.fileseq
               by iFile.order:
         if first-of(iFile.fileseq)
          then
           do: 
             put unformatted '~{' skip.
             put unformatted '"name": "File",' skip.            
             put unformatted '"FileName": "' this-object:encodeForJSON(iFile.filename) '",' skip.
             put unformatted '"BaseFileName": "' this-object:encodeForJSON(iFile.basefilename) '",' skip.
             put unformatted '"FileType": "' this-object:encodeForJSON(iFile.filetype) '",' skip.
             put unformatted '"FileSeq": "' string(iFile.Fileseq) '",' skip.            
             put unformatted '"File": ~[' skip.
           end.

         put unformatted '~{' skip.
         put unformatted '"Order": "' this-object:encodeForJSON(string(iFile.order)) '",'.  
         put unformatted '"Base64": "' this-object:encodeForJSON(iFile.base64) '"'.
         put unformatted '~}'.

         if last-of(iFile.fileseq)
          then
           do:
             put unformatted skip.
             put unformatted '~]' skip.
             put unformatted '~}'.
             if iFileCount = iFile.fileSeq
              then
               do:
                 put unformatted  skip.
               end.
              else
               put unformatted ',' skip.
           end.
          else
           put unformatted ',' skip.                  
       end.
      else
       for each iFile no-lock
         break by iFile.fileseq
               by iFile.order:
         if first-of(iFile.fileseq)
          then
           do: 
             put unformatted '~{' skip.
             put unformatted '"name": "File",' skip.            
             put unformatted '"FileName": "' iFile.filename '",' skip.
             put unformatted '"BaseFileName": "' iFile.basefilename '",' skip.
             put unformatted '"FileType": "' iFile.filetype '",' skip.
             put unformatted '"FileSeq": "' string(iFile.Fileseq) '",' skip.            
             put unformatted '"File": ~[' skip.
             put unformatted '~{' skip.
             put unformatted '"Base64": "'. 
           end.

         put unformatted this-object:encodeForJSON(iFile.base64) .

         if last-of(iFile.fileseq)
          then
           do:
             put unformatted '"' skip. 
             put unformatted '~}' skip.
             put unformatted '~]' skip.
             put unformatted '~}'.
             if iFileCount = iFile.fileSeq
              then
               do:
                 put unformatted  skip.
               end.
              else
               put unformatted ',' skip.
           end.
          else
           put unformatted ',' skip.     
       end.
    END.

    PUT UNFORMATTED '~]~}' SKIP.
    PUT UNFORMATTED '~}' SKIP.
    PUT UNFORMATTED '~}' SKIP.
    
    empty temp-table iFile.
    
  END METHOD.
  
  METHOD PRIVATE LOGICAL splitFileForJson(INPUT pFileName AS CHAR):
  
   def var cBaseName  as character.
   def var cExtension as character.
   def var cPath      as character.
   def var cDir       as character.
   DEF VAR iSeq#      AS INT INIT 0.
  
   DEFINE BUFFER iData FOR iData.
   DEFINE BUFFER iFile FOR iFile.
  
   cBaseName = entry(num-entries(pFileName, "\"),pFileName,"\").
   cDir      = trim(trim(pFileName,cBaseName),"\").
  
   if search(pFileName) = ?
    then
    do:
     /* checking whether filepath exists in propath */
     cPath = propath. 
  
     if lookup(cDir,cPath) = 0  
      then
       Propath =  Propath + "," + cDir.
  
     if search(pFileName) = ?
      then
       return false.
  
     Propath = cPath.
  
    end.
  
   cExtension = entry(num-entries(cBaseName, "."),cBaseName,".").
  
   run util/splitfile(input pFileName, 
                      output table file).
  
   iSeq# = 0.
   FIND iFile EXCLUSIVE-LOCK
     WHERE iFile.basefilename = cBaseName NO-ERROR.
   if not available iFile then
   do:
      FOR LAST iFile NO-LOCK BY iFile.fileseq:
       iSeq# = iFile.fileseq.
      END.
  
      iSeq# = iSeq# + 1.
  
      for each file where
       file.filename = pFileName:
  
       CREATE iFile.
       ASSIGN
           iFile.filename     = pFileName  
           iFile.basefilename = cBaseName
           iFile.filetype     = cExtension
           iFile.fileseq      = iSeq#
           iFile.order        = file.order
           iFile.base64       = file.base64
           NO-ERROR
           .      
      end.
   end.
   else
   for each file where
    file.filename = pFileName:
  
    ASSIGN
        iFile.filename     = pFileName
        iFile.filetype     = cExtension
        iFile.order        = file.order
        iFile.base64       = file.base64
        NO-ERROR
        . 
   end.
  
   empty temp-table file.
  
   IF ERROR-STATUS:ERROR 
    THEN RETURN FALSE.
    ELSE RETURN TRUE.  
  
  END METHOD.
  
  METHOD PRIVATE CHARACTER encodeForJSON(INPUT pUnencoded AS CHAR):
    pUnencoded = REPLACE(pUnencoded, "\", "\\").
    pUnencoded = REPLACE(pUnencoded, '"', "'"). 
    pUnencoded = REPLACE(pUnencoded, chr(10), "\n").
    pUnencoded = REPLACE(pUnencoded, chr(13), "\n").
    pUnencoded = trim(pUnencoded).
  
    RETURN pUnencoded.   /* Function return value. */
  END METHOD.

  DESTRUCTOR PUBLIC JSONresponse ( ):
  END DESTRUCTOR.

END CLASS.
