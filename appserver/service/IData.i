/*------------------------------------------------------------------------
    File        : service/IData.i
    Description : iData temp-table definition for classes based on the 
                  IRequest and IResponse interfaces.
    Author(s)   : B.Johnson
    Created     : 5/01/2014
    
    Arguments   : &Mode     Temp-table access mode: PRIVATE or PROTECTED
                            for use within classes.  Leave blank if
                            used within a regular procedure.
    
    Notes       :
    @Modified    :
    Date        Name    Comments
    12/21/2021  SC      Changed primary index; Removed index not required
  ----------------------------------------------------------------------*/

    DEFINE {&Mode} TEMP-TABLE iData
      FIELD objSeq        AS INT
      FIELD objName       AS CHARACTER
      FIELD objPropSeq    AS INT
      FIELD objProperty   AS CHARACTER
      FIELD propertyValue AS CHARACTER
      FIELD searchName    AS CHARACTER
      INDEX iObjNameSeq IS PRIMARY UNIQUE objName objSeq objPropSeq objProperty
      INDEX iObjSeq  objSeq objName
      INDEX iObjName objName objProperty      
      .
