/* service/BaseResponse
   BASic RESPONSE implementation class
   MohanC 10.30.2013
   11.26.2014 D.Sinclair Corrected ApplicationCode in Stomp Message Header
  @Modified    :
    Date        Name    Comments
    06/21/2019  Archana   Modified to add sysNotifications
    04/12/2021  MK        Added addFile method
    05/11/2021  MK        Framework Changes
    05/06/22    SA        80191 - modified Filename List with '^' seperator
    06/08/2022  VR        Modified to add Clob changes
    08/04/2022  SC        Task#96557 - Overloaded the method 'addfile' to prevent 
                           files being added to temp directory
 */

USING Progress.Lang.*.
USING service.IResponse.
USING dotr.Stomp.StompClient.

CLASS service.BaseResponse INHERITS service.BaseDebuggable IMPLEMENTS IResponse ABSTRACT: 
  
  /* iData temp-table definition for storing parameters */
  {service/IData.i &Mode="PROTECTED"}

  /* sysnotification temp-table definition for storing notifications */
  {tt/sysnotification.i &tableAlias=ttSysNotification}
  
  /* file temp-table definition for storing file(pdf, csv, etc) data */
  {service/ifile.i &Mode="PROTECTED"}

  DEFINE PRIVATE TEMP-TABLE mqData
    field seq AS INT
    field businessObject as char
    field keyID as char
    field action as char
    field description AS CHARACTER
    index iSeq IS PRIMARY UNIQUE seq. 

  def private var alertSubject as char.
  def private var alertmessage as char. 
  
  def protected var hGateway     as service.IGateway.
  
  /* Fault Code */
  DEFINE PRIVATE PROPERTY faultCode AS CHAR NO-UNDO
  GET.
  SET.
  DEFINE PRIVATE PROPERTY faultMessage AS CHAR NO-UNDO
  GET.
  SET.
  DEFINE PRIVATE PROPERTY isFault AS LOGICAL INIT FALSE NO-UNDO
  GET.
  SET.
  DEFINE PUBLIC PROPERTY outputFilenameList AS CHAR NO-UNDO
  GET.
  SET.

  /* Success Code */
  DEFINE PRIVATE PROPERTY successCode AS CHAR NO-UNDO
  GET.
  SET.
  DEFINE PRIVATE PROPERTY successMessage AS CHAR NO-UNDO
  GET.
  SET.
  
  /* System Log */
  DEFINE PRIVATE PROPERTY refType AS CHAR NO-UNDO
  GET.
  SET.
  DEFINE PRIVATE PROPERTY refNum AS CHAR NO-UNDO
  GET.
  SET.
  DEFINE PRIVATE PROPERTY logMessage AS CHAR NO-UNDO
  GET.
  SET.

  DEFINE PUBLIC PROPERTY datasetXmlHandle AS handle NO-UNDO
  GET.
  SET.
   
  CONSTRUCTOR PUBLIC BaseResponse (input pGateway as service.IGateway):

    hGateway = pGateway.

  END CONSTRUCTOR.
  
  CONSTRUCTOR PUBLIC BaseResponse ():
    
    SUPER ().
  END CONSTRUCTOR.

  METHOD PUBLIC LOGICAL setParameter(input dataset-handle pDataset):
       THIS-OBJECT:dataSetXmlHandle = pDataSet.                            
  END METHOD.

  /* FAULT */
  METHOD PUBLIC LOGICAL fault(INPUT pCode AS CHARACTER, INPUT pMessage AS CHARACTER ):
    if pCode = ? or pCode = ""
     then 
      assign
        pCode = "3000" /* Default generic code */
        pMessage = "Action"
        .
            
    /* Only set the code once */
    IF not THIS-OBJECT:isFault 
     THEN THIS-OBJECT:faultCode = pCode.

         run util/msgsubs.p (pCode, pMessage, output pMessage).

         /* Keep appending messages as faults occur */       
    ASSIGN
      THIS-OBJECT:faultMessage = this-object:faultMessage + pMessage + "  "
      THIS-OBJECT:isFault = TRUE
      .
    RETURN TRUE.
  END METHOD.

  METHOD PUBLIC LOGICAL isFault():
    RETURN THIS-OBJECT:isFault.
  END METHOD.

  METHOD PUBLIC LOGICAL addFault(INPUT pCode AS CHARACTER, INPUT pMessage AS CHARACTER):
    /* Only set the code once */
    IF THIS-OBJECT:faultCode = "" 
     THEN THIS-OBJECT:faultCode = pCode.
     
    run util/msgsubs.p (pCode, pMessage, output pMessage).
    THIS-OBJECT:faultMessage = this-object:faultMessage + pMessage + "  ".
     
    RETURN TRUE.
  END METHOD.

  METHOD PUBLIC LOGICAL getFault(OUTPUT pCode AS CHAR, OUTPUT pMessage AS CHAR):
    IF THIS-OBJECT:faultCode > ""
     THEN ASSIGN
            pCode = THIS-OBJECT:faultCode
            pMessage = THIS-OBJECT:faultMessage
            .
     ELSE RETURN FALSE.
    RETURN TRUE.
  END METHOD.
  /* FAULT */
  
  /* SUCCESS */
  METHOD PUBLIC LOGICAL success(INPUT pCode AS CHARACTER, INPUT pMessage AS CHARACTER ):
    if this-object:isFault
     then return false.
     
    if pCode = "" or pCode = ?
     then 
      assign
        pCode = "2000"
        pMessage = "Action was successful."
        .
    
    run util/msgsubs.p (pCode, pMessage, output pMessage).
         
    ASSIGN
      THIS-OBJECT:successCode = pCode
      THIS-OBJECT:successMessage = pMessage
      .
    RETURN TRUE.
  END METHOD.

  METHOD PUBLIC LOGICAL getSuccess(OUTPUT pCode AS CHAR, OUTPUT pMessage AS CHAR):
     ASSIGN
       pCode = THIS-OBJECT:successCode
       pMessage = THIS-OBJECT:successMessage
       .
    RETURN TRUE.
  END METHOD.
  /* SUCCESS */

  /* SET METHODS */
	METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHARACTER, INPUT pValue AS CHARACTER ):
    DEFINE BUFFER iData FOR iData.
    DEF VAR iSeq# AS INT INIT 0.
    
    FIND iData EXCLUSIVE-LOCK
      WHERE iData.objName = "parameter"
        AND iData.objProperty = pName NO-ERROR.
    IF NOT AVAILABLE iData THEN 
    DO: 
        FOR LAST iData NO-LOCK BY iData.objSeq:
            iSeq# = iData.objSeq.
        END.
        iSeq# = iSeq# + 1.      
        CREATE iData.
        ASSIGN
            iData.objSeq = iSeq#
            iData.objName = "parameter"
            iData.objPropSeq = 1
            iData.objProperty = pName
            NO-ERROR
            .
    END.
    
    ASSIGN
        iData.propertyValue = pValue
        NO-ERROR
        .
    
    IF ERROR-STATUS:ERROR 
     THEN RETURN FALSE.
     ELSE RETURN TRUE.
	END METHOD.

	METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHARACTER, INPUT pValue AS DATE ):
    DEFINE BUFFER iData FOR iData.
    DEF VAR iSeq# AS INT INIT 0.
    
    FIND iData EXCLUSIVE-LOCK
      WHERE iData.objName = "parameter"
        AND iData.objProperty = pName NO-ERROR.
    IF NOT AVAILABLE iData THEN 
    DO: 
        FOR LAST iData NO-LOCK BY iData.objSeq:
            iSeq# = iData.objSeq.
        END.
        iSeq# = iSeq# + 1.      
        CREATE iData.
        ASSIGN
            iData.objSeq = iSeq#
            iData.objName = "parameter"
            iData.objPropSeq = 1
            iData.objProperty = pName
            NO-ERROR
            .
    END.
    
    ASSIGN
        iData.propertyValue = STRING(pValue)
        NO-ERROR
        .
    
    IF ERROR-STATUS:ERROR 
     THEN RETURN FALSE.
     ELSE RETURN TRUE.
	END METHOD.

	METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHARACTER, INPUT pValue AS DATETIME ):
    DEFINE BUFFER iData FOR iData.
    DEF VAR iSeq# AS INT INIT 0.
    
    FIND iData EXCLUSIVE-LOCK
      WHERE iData.objName = "parameter"
        AND iData.objProperty = pName NO-ERROR.
    IF NOT AVAILABLE iData THEN 
    DO: 
        FOR LAST iData NO-LOCK BY iData.objSeq:
            iSeq# = iData.objSeq.
        END.
        iSeq# = iSeq# + 1.      
        CREATE iData.
        ASSIGN
            iData.objSeq = iSeq#
            iData.objName = "parameter"
            iData.objPropSeq = 1
            iData.objProperty = pName
            NO-ERROR
            .
    END.
    
    ASSIGN
        iData.propertyValue = STRING(pValue)
        NO-ERROR
        .
    
    IF ERROR-STATUS:ERROR 
     THEN RETURN FALSE.
     ELSE RETURN TRUE.
	END METHOD.

	METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHARACTER, INPUT pValue AS DECIMAL ):
    DEFINE BUFFER iData FOR iData.
    DEF VAR iSeq# AS INT INIT 0.
    
    FIND iData EXCLUSIVE-LOCK
      WHERE iData.objName = "parameter"
        AND iData.objProperty = pName NO-ERROR.
    IF NOT AVAILABLE iData THEN 
    DO: 
        FOR LAST iData NO-LOCK BY iData.objSeq:
            iSeq# = iData.objSeq.
        END.
        iSeq# = iSeq# + 1.      
        CREATE iData.
        ASSIGN
            iData.objSeq = iSeq#
            iData.objName = "parameter"
            iData.objPropSeq = 1
            iData.objProperty = pName
            NO-ERROR
            .
    END.
    
    ASSIGN
        iData.propertyValue = STRING(pValue)
        NO-ERROR
        .
    
    IF ERROR-STATUS:ERROR 
     THEN RETURN FALSE.
     ELSE RETURN TRUE.
	END METHOD.

	METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHARACTER, INPUT pValue AS INTEGER ):
    DEFINE BUFFER iData FOR iData.
    DEF VAR iSeq# AS INT INIT 0.
    
    FIND iData EXCLUSIVE-LOCK
      WHERE iData.objName = "parameter"
        AND iData.objProperty = pName NO-ERROR.
    IF NOT AVAILABLE iData THEN 
    DO: 
        FOR LAST iData NO-LOCK BY iData.objSeq:
            iSeq# = iData.objSeq.
        END.
        iSeq# = iSeq# + 1.      
        CREATE iData.
        ASSIGN
            iData.objSeq = iSeq#
            iData.objName = "parameter"
            iData.objPropSeq = 1
            iData.objProperty = pName
            NO-ERROR
            .
    END.
    
    ASSIGN
        iData.propertyValue = STRING(pValue)
        NO-ERROR
        .
    
    IF ERROR-STATUS:ERROR 
     THEN RETURN FALSE.
     ELSE RETURN TRUE.
	END METHOD.

	METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHARACTER, INPUT pValue AS LOGICAL ):
    DEFINE BUFFER iData FOR iData.
    DEF VAR iSeq# AS INT INIT 0.
    
    FIND iData EXCLUSIVE-LOCK
      WHERE iData.objName = "parameter"
        AND iData.objProperty = pName NO-ERROR.
    IF NOT AVAILABLE iData THEN 
    DO: 
        FOR LAST iData NO-LOCK BY iData.objSeq:
            iSeq# = iData.objSeq.
        END.
        iSeq# = iSeq# + 1.      
        CREATE iData.
        ASSIGN
            iData.objSeq = iSeq#
            iData.objName = "parameter"
            iData.objPropSeq = 1
            iData.objProperty = pName
            NO-ERROR
            .
    END.
    
    ASSIGN
        iData.propertyValue = STRING(pValue)
        NO-ERROR
        .
    
    IF ERROR-STATUS:ERROR 
     THEN RETURN FALSE.
     ELSE RETURN TRUE.
	END METHOD.

	METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHARACTER, INPUT TABLE-HANDLE pTable ):
    DEFINE VAR hTable AS HANDLE NO-UNDO.
    DEFINE VAR hField AS HANDLE NO-UNDO.
    DEFINE VAR hQuery AS HANDLE NO-UNDO.
    DEFINE VAR pQuery AS CHAR NO-UNDO.
    DEFINE VAR iFldNum AS INT NO-UNDO.
    DEFINE VAR tRecCnt AS INT NO-UNDO.
    DEFINE VAR iCount AS INT NO-UNDO.
    DEFINE VAR cSubFieldList AS CHAR NO-UNDO INITIAL "". /* to get fields of one table */

    DEFINE BUFFER iData FOR iData.
    
    IF NOT VALID-HANDLE(pTable) OR 
       (VALID-HANDLE(pTable) AND NOT pTable:HAS-RECORDS)
     THEN RETURN FALSE.
    
    ASSIGN
      pName = IF pName = ? OR pName = "" THEN pTable:NAME ELSE pName
      hTable = pTable:DEFAULT-BUFFER-HANDLE
      pQuery = "FOR EACH " + pTable:NAME
      .

    IF num-entries(this-object:hGateway:fieldList, ",") > 0 
     THEN
      DO iCount = 1 TO num-entries(this-object:hGateway:fieldList, ","):
        assign
            cSubFieldList = cSubFieldList + "," +
                            IF ENTRY(1,ENTRY(iCount,this-object:hGateway:fieldList,","),".") EQ pName 
                             THEN 
                              ENTRY(2,ENTRY(iCount,this-object:hGateway:fieldList,","),".") 
                             ELSE ""
            cSubFieldList = TRIM(cSubFieldList,",")
            .
      END. 
   
    /* Remove any previous parameter values */
    FOR EACH iData EXCLUSIVE-LOCK WHERE iData.objName = pName:
        DELETE iData.
    END.
    
    /* Create new parameter values */
    CREATE QUERY hQuery.
    hQuery:ADD-BUFFER(hTable).
    hQuery:QUERY-PREPARE(pQuery).
    hQuery:QUERY-OPEN().
    hQuery:GET-FIRST().
   
    REPEAT WHILE NOT hQuery:QUERY-OFF-END:
      tRecCnt = tRecCnt + 1.
      DO iFldNum = 1 TO hTable:NUM-FIELDS:
        hField = hTable:BUFFER-FIELD(iFldNum).
        if (num-entries(cSubFieldList, ",") le 0 )  or
           (num-entries(cSubFieldList, ",") > 0 and lookup(hField:name, cSubFieldList,",") > 0) then
           do:
             CREATE iData.
             ASSIGN
               iData.objSeq = tRecCnt
               iData.objName = pName
               iData.objPropSeq = iFldNum
               iData.objProperty = hField:NAME
               iData.propertyValue = hField:BUFFER-VALUE
               iData.searchName = iData.objName + "." + iData.objProperty
              .  
           end.        
      END.
      hQuery:GET-NEXT.
    END.
    hQuery:QUERY-CLOSE.
    DELETE OBJECT hQuery NO-ERROR.
    DELETE OBJECT pTable NO-ERROR.
    RETURN tRecCnt > 0.
	END METHOD.
    
    METHOD PUBLIC LOGICAL addFile(INPUT pName AS CHARACTER):
     DEFINE VARIABLE cBaseName AS CHARACTER NO-UNDO.
							
	 THIS-OBJECT:outputFilenameList = trim((THIS-OBJECT:outputFilenameList + "^" + pName),'^').

     cBaseName = entry(num-entries(pName, "\"),pName,"\").   /* getting filename */
     run util/addtotempfile(input cBaseName, input pName).   /* files to be deleted after execution */
     
    END METHOD.

    method public logical addFile(input pName as character, input pPermanent as logical):
      define variable cBaseName as character no-undo.
      this-object:outputFilenameList = trim((this-object:outputFilenameList + "^" + pName),'^').
      cBaseName = entry(num-entries(pName, "\"),pName,"\").   /* getting filename */
      if not pPermanent
       then
        run util/addtotempfile(input cBaseName, input pName).   /* files to be deleted after execution */
    end method.
    
  /* SETS AND SUCCESS */

  /* LOG MESSAGE */
  METHOD PUBLIC LOGICAL getLogMessage( OUTPUT pRefType AS CHARACTER, OUTPUT pRefNum AS CHARACTER, OUTPUT pMessage AS CHARACTER ):
    IF THIS-OBJECT:logMessage <> "" AND THIS-OBJECT:logMessage <> ?
     THEN ASSIGN
            pRefType = THIS-OBJECT:refType
            pRefNum = THIS-OBJECT:refNum
            pMessage = THIS-OBJECT:logMessage
            .
     ELSE RETURN FALSE.
    RETURN TRUE.
  END METHOD.

  METHOD PUBLIC LOGICAL logMessage(INPUT pRefType AS CHAR,INPUT pRefNum AS CHAR,INPUT pMessage AS CHAR):
    IF THIS-OBJECT:logMessage <> "" AND THIS-OBJECT:logMessage <> ?
     THEN RETURN FALSE.
     ELSE ASSIGN
            THIS-OBJECT:refType = pRefType
            THIS-OBJECT:refNum = pRefNum
            THIS-OBJECT:logMessage = pMessage
            .
    RETURN TRUE.
  END METHOD.
  /* LOG MESSAGE */

  /* NOTIFICATIONS */
  method public void notify(input pUID as character,
                            input pEntityType as character,
                            input pEntityID as character, 
                            input pRefSysNotificationID as integer,
                            input pNotificationStat as character, 
                            input pSourceType as character,
                            input pSourceID as character,
                            input pNotification as character):
    create ttSysNotification.
    assign
          ttSysNotification.UID                  = pUID                                     
          ttSysNotification.entityType           = pEntityType
          ttSysNotification.entityID             = pEntityID
          ttSysNotification.refSysNotificationID = pRefSysNotificationID
          ttSysNotification.notificationStat     = pNotificationStat
          ttSysNotification.sourceType           = pSourceType
          ttSysNotification.sourceID             = pSourceID
          ttSysNotification.notification         = pNotification
          ttSysNotification.createDate           = now.
  end method.
  
  method public logical hasNotifications():
    def buffer ttSysNotification for ttSysNotification.
    
    find first ttSysNotification no-error.
    return available ttSysNotification.
  end.
  
  method public char getNotifications(output table-handle hSysNotification):
    hSysNotification = temp-table ttSysNotification:handle.
  end method.
  /* NOTIFICATIONS */

  /* EVENTS */
  method public void createEvent(input pName as char, input pKey as char):
    this-object:otherEvent(pName, pKey, "Created", pName + " created with key " + pKey).
  end method.
  
  method public void updateEvent(input pName as char, input pKey as char):
    this-object:otherEvent(pName, pKey, "Updated", pName + " modified with key " + pKey).
  end method.
  
  method public void deleteEvent(input pName as char, input pKey as char):
    this-object:otherEvent(pName, pKey, "Deleted", pName + " deleted with key " + pKey).
  end method.
  
  method public void otherEvent(input pName as char, input pKey as char, input pAction as char, input pMsg as char):
    def buffer mqData for mqData.
    def var tSeq as int no-undo.
    
    /* It makes no sense to notify of an event that we cannot identify */
    if pName = ""
      or pName = ?
      or pKey = ?
      or pAction = ?
     then return.
    if pMsg = ?
     then pMsg = "Unknown".
     
    tSeq = 1.
    for last mqData 
      by mqData.seq:
     tSeq = mqData.seq + 1.
    end.
    
    create mqData.
    mqData.seq = tSeq.
    mqData.businessObject = pName.
    mqData.keyID = pKey.
    mqData.action = pAction.
    mqData.description = pMsg.
  end method.
  
  method public logical hasEvents():
    def buffer mqData for mqData.
    
    find first mqData no-error.
    return available mqData.
  end.
  
  method public void sendEvents(input iStomp as dotr.Stomp.StompClient, 
                                input pUser as char,
                                input pSysAction as char,
                                input pClient as char,
                                input pSession as char,
                                input pAddress as char):
    def buffer mqData for mqData.
    
    for each mqData
      by mqData.seq:
    
      if pClient > "" and pClient <> ?
       then iStomp:WithHeader("ApplicationCode", pClient).
      
      if pSession > "" and pSession <> ?
       then iStomp:WithHeader("ApplicationID", pSession). /* unique session ID of the running client */
      
      if pAddress > "" and pAddress <> ?
       then iStomp:WithHeader("IpAddress", pAddress).
      
      if pUser > "" and pUser <> ?
       then iStomp:WithHeader("User", pUser).
      
      if pSysAction > "" and pSysAction <> ?
       then iStomp:WithHeader("SystemAction", pSysAction).
      
      iStomp:WithHeader("Entity", mqData.businessObject).
      iStomp:WithHeader("Key", mqData.keyID).
      iStomp:WithHeader("Action", mqData.action).
      iStomp:SendTopic(mqData.businessObject, mqData.description).
    end.
  end method.
  
  method public char getEvents():
    def buffer mqData for mqData.
    def var tList as char no-undo.
    
    for each mqData
          by mqData.seq:
      
      tList = tList + (if tList > "" then "," else "") + mqData.description.
    end.
    return tList.
  end method.
  /* EVENTS */

  /* DEBUG */
  METHOD PUBLIC OVERRIDE VOID debug():
    DEFINE BUFFER iData FOR iData.
    FOR EACH iData NO-LOCK:
      if log-manager:logfile-name > ""
       then LOG-MANAGER:WRITE-MESSAGE ("Object: " + iData.objName + " | Property: " + iData.objProperty + " | Value: " + iData.propertyValue,THIS-OBJECT:GetClass():TypeName).
       else message "Object: " + iData.objName + " | Property: " + iData.objProperty + " | Value: " + iData.propertyValue view-as alert-box.
    END.
  END METHOD.
  /* DEBUG */

  /* ALERT */
  method public logical alert(input pSubject as char, input pMessage as char):
    if pSubject = ? 
     then pSubject = "".
    if pMessage = ? 
     then pMessage = "".
    alertSubject = pSubject.
    alertMessage = pMessage.
  end method.

  method public logical hasAlert(output pSubject as char, output pMessage as char):
    pSubject = alertSubject.
    pMessage = alertMessage.
    return pSubject > "".
  end method.
  /* ALERT */

  METHOD PUBLIC VOID output (  ):
    UNDO, THROW NEW Progress.Lang.AppError("METHOD NOT IMPLEMENTED").
  END METHOD.
  
  METHOD PUBLIC FINAL LOGICAL setStorageKeys (input pStorageEntity as char, input pStorageEntityId as char):
  
   IF pStorageEntity = "" OR pStorageEntityId = "" 
    THEN
     RETURN FALSE.

   THIS-OBJECT:hGateway:setStorageKeys(pStorageEntity, pStorageEntityId).
   RETURN TRUE.
     
  END METHOD.

  METHOD PUBLIC VOID dsOutput():
    UNDO, THROW NEW Progress.Lang.AppError("METHOD NOT IMPLEMENTED").
  END METHOD.
  
  METHOD PUBLIC FINAL LOGICAL setEmailParameter (input pEmailObjectName as char, input pEmailObjectValue as char):
  
   IF pEmailObjectName = "" OR pEmailObjectValue = "" 
    THEN
     RETURN FALSE.

   THIS-OBJECT:hGateway:setEmailParameter(pEmailObjectName, pEmailObjectValue).
   RETURN TRUE.
     
  END METHOD.

  DESTRUCTOR PUBLIC BaseResponse ( ):
  END DESTRUCTOR.

END CLASS.
