 
 /*------------------------------------------------------------------------
    File        : BaseDebuggable
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : MohanC
    Created     : Sat Nov 02 11:43:58 CDT 2013
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.

CLASS service.BaseDebuggable IMPLEMENTS service.IDebuggable ABSTRACT: 
	/*------------------------------------------------------------------------------
			Purpose:  																	  
			Notes:  																	  
	------------------------------------------------------------------------------*/
		
	CONSTRUCTOR PUBLIC BaseDebuggable (  ):
		SUPER ().
	END CONSTRUCTOR.


	METHOD PUBLIC VOID debug(  ):
		LOG-MANAGER:WRITE-MESSAGE ("Debug",THIS-OBJECT:GetClass():TypeName).
		RETURN.
	END METHOD.

	DESTRUCTOR PUBLIC BaseDebuggable ( ):
	END DESTRUCTOR.

END CLASS.
