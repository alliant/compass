/* service/BaseGateway
   BASic GATEWAY that passes on to action handler
   Mohanc 10.31.2013
   DSinclair 11.24.2014  Allow role "Administrator" to perform all actions
   JOliver   09.04.2015  Check to see if password is expired. An expired password
                         is a password that has not been changed in X number of
                         days as defined in the INI file. Call a procedure to
                         set the expired flag on the database. If the password 
                         is expired, a fault of 1007 is thrown except in the 
                         cases of systemIdentity and systemUserPassword.
    @Modified    :
    Date        Name    Comments
    06/21/2019  Archana   Modified to add Notifications
    05/11/2021  MK        Framework Changes
    12/21/2021  SC        Changes for actionDesc property
    06/08/2022  VR        Modified to add new method 'dsRespond' for clob changes
 */

USING Progress.Lang.*.
USING service.IGateway.

CLASS service.BaseGateway INHERITS service.BaseDebuggable IMPLEMENTS IGateway ABSTRACT: 

    /* Standard Properties */
    {service/IProps.i &Set="PROTECTED SET."}

    /* iData temp-table definition for storing parameters */
    {service/IData.i &Mode="PROTECTED"}
  

    /* System Error Properties */
    DEFINE PUBLIC PROPERTY eventid AS CHAR NO-UNDO
    GET.
    SET.
    DEFINE PUBLIC PROPERTY eventParams AS CHAR NO-UNDO
    GET.
    SET.
    
    /* Fault Codes */
    DEFINE PUBLIC PROPERTY faultCode AS CHAR NO-UNDO
    GET.
    SET.
    DEFINE PUBLIC PROPERTY faultMessage AS CHAR NO-UNDO
    GET.
    SET.
    DEFINE PUBLIC PROPERTY isFault AS LOGICAL NO-UNDO
    GET.
    SET.
    DEFINE PROTECTED PROPERTY storageEntity AS char NO-UNDO
    GET.
    SET.
    DEFINE PROTECTED PROPERTY storageEntityId AS CHAR NO-UNDO
    GET.
    SET.

	
    CONSTRUCTOR PUBLIC BaseGateway (INPUT pUid AS CHAR,
	                                INPUT pPassword AS CHAR,
	                                INPUT pSessionid AS CHAR,
	                                INPUT pActionid AS CHAR,
	                                INPUT pClientid AS CHAR,
	                                INPUT pUserIP AS CHAR,
                                    INPUT pQueryString AS CHAR,
                                    INPUT pActionType AS CHAR,
                                    INPUT pFieldList AS CHAR,
                                    INPUT pWebUtilitiesHdl AS HANDLE,
                                    INPUT pStartupType AS CHAR,
                                    INPUT pOutputFormat AS CHAR,
                                    INPUT pDestination AS CHAR,
                                    INPUT pEmailAddrList AS CHAR,
                                    INPUT pPrinterAddr AS CHAR,
                                    INPUT pRepository AS CHAR,
                                    INPUT pOutputFileFormat AS CHAR,
                                    INPUT pNotifyRequestor AS CHAR):
        
        /* Action Vars */                           
	    DEFINE VAR iValidAction AS LOGICAL NO-UNDO INIT FALSE.
        DEFINE VAR iIsActive AS LOGICAL NO-UNDO INIT FALSE.
        DEFINE VAR iIsAnonymous AS LOGICAL NO-UNDO INIT FALSE.
        DEFINE VAR iIsSecure AS LOGICAL NO-UNDO INIT TRUE.
        DEFINE VAR iIsLog AS LOGICAL NO-UNDO INIT TRUE.
        DEFINE VAR iIsAudit AS LOGICAL NO-UNDO INIT FALSE.
        DEFINE VAR iUserids AS CHAR NO-UNDO INIT "!*".
        DEFINE VAR iRoles AS CHAR NO-UNDO INIT "!*".
        DEFINE VAR iIPAddresses AS CHAR NO-UNDO INIT "!*".
        DEFINE VAR iActionHandler AS CHAR NO-UNDO.
        DEFINE VAR iIsEventsEnabled AS LOG NO-UNDO.
        DEFINE VAR iEmailsEnabled as char no-undo.
        DEFINE VAR iEmailSuccess AS CHAR NO-UNDO.
        DEFINE VAR iEmailFailure AS CHAR NO-UNDO.
        DEFINE VAR iQueueHandler AS CHAR NO-UNDO.
        DEFINE VAR iQueueable AS LOGICAL NO-UNDO INIT FALSE.
        DEFINE VAR iDisplayName AS CHAR NO-UNDO.
        DEFINE VAR iEmailHtml AS CHAR NO-UNDO.
        DEFINE VAR iEmailSubject AS CHAR NO-UNDO.
        DEFINE VAR iActionDesc   AS CHAR NO-UNDO.
	                               
	      /* User Vars */                         
        DEFINE VAR iUserName AS CHAR NO-UNDO.
        DEFINE VAR iUserPass AS CHAR NO-UNDO.
        DEFINE VAR iUserInitials AS CHAR NO-UNDO.
        DEFINE VAR iUserRole AS CHAR NO-UNDO.
        DEFINE VAR iUserActive AS LOGICAL NO-UNDO.
        DEFINE VAR iUserEmail AS CHAR NO-UNDO.
        DEFINE VAR iUserPasswordSetDate AS DATETIME NO-UNDO.
        DEFINE VAR iUserPasswordExpired AS LOGICAL NO-UNDO.
        DEFINE VAR iUserPasswordExpiredDate AS DATETIME NO-UNDO.
        DEFINE VAR iUserPasswordWarning AS INT NO-UNDO.
        DEFINE VAR iUserPasswordWarningDays AS CHAR NO-UNDO.
        
        define var i as int no-undo.
        define var valid-role as log no-undo.

        /* flushing */
        ASSIGN
         THIS-OBJECT:storageEntity   = ""
         THIS-OBJECT:storageEntityId = "".
        
        IF CONNECTED("COMPASS") 
         THEN
          DO: RUN util/getactioninfo.p 
                (INPUT pActionid,
                 OUTPUT iValidAction,
                 OUTPUT iIsActive,
                 OUTPUT iIsAnonymous,
                 OUTPUT iIsSecure,
                 OUTPUT iIsLog,
                 OUTPUT iIsAudit,
                 OUTPUT iUserids,
                 OUTPUT iRoles,
                 OUTPUT iIPAddresses,
                 OUTPUT iActionHandler,
                 OUTPUT iIsEventsEnabled,
                 OUTPUT iEmailsEnabled,
                 OUTPUT iEmailSuccess,
                 OUTPUT iEmailFailure,
                 OUTPUT iQueueHandler,
                 OUTPUT iQueueable,
                 OUTPUT iDisplayName,
                 OUTPUT iEmailHtml,
                 OUTPUT iEmailSubject,
                 OUTPUT iActionDesc
                 ).
              
              RUN util/getuserinfo.p
                (INPUT pUid,
                 OUTPUT iUserName,
                 OUTPUT iUserInitials,
                 OUTPUT iUserRole,
                 OUTPUT iUserPass,
                 OUTPUT iUserActive,
                 OUTPUT iUserEmail,
                 OUTPUT iUserPasswordSetDate
                 ).
             
              RUN util/getpwexpire.p
                (INPUT pUid,
                 INPUT iUserPasswordSetDate,
                 OUTPUT iUserPasswordExpiredDate,
                 OUTPUT iUserPasswordExpired,
                 OUTPUT iUserPasswordWarning,
                 OUTPUT iUserPasswordWarningDays
                 ).
                 
             ASSIGN
                THIS-OBJECT:Uid = pUid
                THIS-OBJECT:sessionid = pSessionid
                THIS-OBJECT:actionid = pActionid
                THIS-OBJECT:clientid = pClientid
                THIS-OBJECT:ipAddress = pUserip
                THIS-OBJECT:queryString = pQueryString
                THIS-OBJECT:validAction = iValidAction
                THIS-OBJECT:isQueueable = iQueueable
                THIS-OBJECT:isActive = iIsActive
                THIS-OBJECT:isAnonymous = iIsAnonymous
                THIS-OBJECT:isSecure = iIsSecure
                THIS-OBJECT:isLog = iIsLog
                THIS-OBJECT:isAudit = iIsAudit
                THIS-OBJECT:userids = iUserids
                THIS-OBJECT:roles = iRoles
                THIS-OBJECT:ipAddresses = iIPAddresses
                THIS-OBJECT:actionHandler = iActionHandler
                THIS-OBJECT:name = iUserName
                THIS-OBJECT:initials = iUserInitials
                THIS-OBJECT:email = iUserEmail
                THIS-OBJECT:passwordSetDate = iUserPasswordSetDate
                THIS-OBJECT:passwordExpireDate = iUserPasswordExpiredDate
                THIS-OBJECT:passwordExpired = iUserPasswordExpired
                THIS-OBJECT:passwordWarnDays = iUserPasswordWarningDays
                THIS-OBJECT:role = iUserRole
                THIS-OBJECT:password = iUserPass
                THIS-OBJECT:isUserActive = iUserActive
                THIS-OBJECT:isEventsEnabled = iIsEventsEnabled
                THIS-OBJECT:iEmailsEnabled = iEmailsEnabled
                THIS-OBJECT:iEmailSuccess = iEmailSuccess
                THIS-OBJECT:iEmailFailure = iEmailFailure
                THIS-OBJECT:actionType = pActionType
                THIS-OBJECT:queueHandler = iQueueHandler
                THIS-OBJECT:displayName = iDisplayName
                THIS-OBJECT:emailHtml = iEmailHtml
                THIS-OBJECT:emailSubject = iEmailSubject
                THIS-OBJECT:fieldList = pFieldList
                THIS-OBJECT:webUtilitiesHdl = pWebUtilitiesHdl 
                THIS-OBJECT:startupType = pStartupType
                THIS-OBJECT:outputFormat = pOutputFormat
                THIS-OBJECT:destination  = pDestination 
                THIS-OBJECT:emailAddrList = pEmailAddrList 
                THIS-OBJECT:printerAddr = pPrinterAddr
                THIS-OBJECT:repository = pRepository
                THIS-OBJECT:outputFileFormat = pOutputFileFormat
                THIS-OBJECT:notifyRequestor = pNotifyRequestor
                THIS-OBJECT:actionDesc = iActionDesc
                .
             
             IF NOT validAction
              THEN ASSIGN
                     THIS-OBJECT:eventid = "System:InvalidAction"
                     THIS-OBJECT:eventParams = THIS-OBJECT:actionid
                     THIS-OBJECT:faultCode = "1000"
                     THIS-OBJECT:faultMessage = pActionID
                     THIS-OBJECT:canExec = FALSE
                     .
             ELSE IF NOT THIS-OBJECT:isactive 
              THEN ASSIGN
                     THIS-OBJECT:eventid = "System:InactiveAction"
                     THIS-OBJECT:eventParams = THIS-OBJECT:actionid
                     THIS-OBJECT:faultCode = "1002"
                     THIS-OBJECT:faultMessage = pActionID
                     THIS-OBJECT:canExec = FALSE
                     .
             ELSE IF THIS-OBJECT:actionType = "Q" and (NOT THIS-OBJECT:isQueueable OR THIS-OBJECT:isQueueable = ?) 
              THEN ASSIGN
                     THIS-OBJECT:eventid = "System:NotQueueable"
                     THIS-OBJECT:eventParams = THIS-OBJECT:actionid
                     THIS-OBJECT:faultCode = "1008"
                     THIS-OBJECT:faultMessage = pActionID
                     THIS-OBJECT:canExec = FALSE
                     .
             ELSE IF THIS-OBJECT:name = "" AND NOT THIS-OBJECT:isAnonymous 
              THEN ASSIGN
                     THIS-OBJECT:eventid = "System:InvalidUser"
                     THIS-OBJECT:eventParams = THIS-OBJECT:actionid
                     THIS-OBJECT:faultCode = "3001"
                     THIS-OBJECT:faultMessage = THIS-OBJECT:Uid
                     THIS-OBJECT:canExec = FALSE
                     .
             ELSE IF THIS-OBJECT:Uid = "" AND NOT THIS-OBJECT:isAnonymous 
              THEN ASSIGN
                     THIS-OBJECT:eventid = "System:AnonymousDenied"
                     THIS-OBJECT:eventParams = THIS-OBJECT:actionid
                     THIS-OBJECT:faultCode = "1004"
                     THIS-OBJECT:faultMessage = ""
                     THIS-OBJECT:canExec = FALSE
                     .
             ELSE IF THIS-OBJECT:isSecure
              THEN
               DO: IF NOT CAN-DO(THIS-OBJECT:ipAddresses,THIS-OBJECT:ipAddress)
                    THEN ASSIGN
                      THIS-OBJECT:eventid = "System:IpAddressDenied" 
                      THIS-OBJECT:eventParams = THIS-OBJECT:actionid + ":" + THIS-OBJECT:ipAddress 
                      THIS-OBJECT:faultCode = "1006"
                      THIS-OBJECT:faultMessage = pActionID
                      THIS-OBJECT:canExec = FALSE 
                      .
                   ELSE IF THIS-OBJECT:password <> pPassword
                    THEN ASSIGN
                      THIS-OBJECT:eventid = "System:InvalidCredentials"
                      THIS-OBJECT:eventParams = THIS-OBJECT:Uid
                      THIS-OBJECT:faultCode = "1004"
                      THIS-OBJECT:faultMessage = ""
                      THIS-OBJECT:canExec = FALSE
                      .
                   ELSE IF NOT THIS-OBJECT:isUserActive
                    THEN ASSIGN
                      THIS-OBJECT:eventid = "System:InactiveUserid"
                      THIS-OBJECT:eventParams = THIS-OBJECT:Uid
                      THIS-OBJECT:faultCode = "1004"
                      THIS-OBJECT:faultMessage = ""
                      THIS-OBJECT:canExec = FALSE
                      .
                   /* joliver: Added block to throw fault if the password is expired except for systemIdentity
                               and systemUserPassword which validates the user and sets the user's password 
                               respectively 
                   */
                   ELSE IF THIS-OBJECT:passwordExpired
                    THEN
                     DO: IF NOT THIS-OBJECT:actionid = "systemIdentity" AND NOT THIS-OBJECT:actionid = "systemUserPassword"
                          THEN ASSIGN
                            THIS-OBJECT:eventid = "System:PasswordExpired"
                            THIS-OBJECT:eventParams = THIS-OBJECT:Uid
                            THIS-OBJECT:faultCode = "1007"
                            THIS-OBJECT:faultMessage = ""
                            THIS-OBJECT:canExec = FALSE
                            .
                         ELSE THIS-OBJECT:canExec = TRUE.
                     END.
                   ELSE 
                   do:
                    /* A user can be assigned more than one role, so we need to check
                       security for all roles. */
                    valid-role = (lookup("Administrator", this-object:role) <> 0).
                    if not valid-role then
                    do i = 1 to num-entries(this-object:role):
                      valid-role = can-do(this-object:roles, entry(i,this-object:role)).
                      if valid-role then leave.
                    end.
                   
                    IF NOT(valid-role or CAN-DO(THIS-OBJECT:userids,THIS-OBJECT:Uid))
                    THEN ASSIGN
                      THIS-OBJECT:eventid = "Event:PermissionDenied"
                      THIS-OBJECT:eventParams = THIS-OBJECT:Uid + ":" + THIS-OBJECT:role + ":" + THIS-OBJECT:actionid
                      THIS-OBJECT:faultCode = "1005"
                      THIS-OBJECT:faultMessage = pActionID
                      THIS-OBJECT:canExec = FALSE
                      .
                    ELSE THIS-OBJECT:canExec = TRUE.
                   end.
               END.
             ELSE THIS-OBJECT:canExec = TRUE.
          END.   
         ELSE ASSIGN
                THIS-OBJECT:eventid = "System:NoDatabase"
                THIS-OBJECT:eventParams = pActionid
                THIS-OBJECT:faultCode = "1003"
                THIS-OBJECT:faultMessage = "No database connection"
                THIS-OBJECT:canExec = FALSE
                .
	END CONSTRUCTOR.
    
    METHOD PUBLIC VOID respond(INPUT iResponse AS service.IResponse):
    
    end method.

    METHOD PUBLIC VOID dsRespond(INPUT iResponse AS service.IResponse):
    
    END METHOD.

	METHOD PUBLIC LOGICAL execAction(INPUT iRequest AS service.IRequest,INPUT iResponse AS service.IResponse ):
        RETURN TRUE.
	END METHOD.

	METHOD PUBLIC LOGICAL log (INPUT iResponse AS service.IResponse):
        RETURN TRUE.   /* Function return value. */
	END METHOD.
	METHOD PUBLIC LOGICAL notification (INPUT iResponse AS service.IResponse):
        RETURN TRUE.   /* Function return value. */
	END METHOD.
    
    method public void outputhttpHeader():
      
      DEFINE VARIABLE webHdl AS HANDLE NO-UNDO.
      webHdl = THIS-OBJECT:webUtilitiesHdl.
      
      if THIS-OBJECT:outputFormat = "XML"
      then do:
        run outputhttpheader IN webHdl ("Content-Type":U, "text/xml").
        run outputhttpheader in webHdl ("Cache-Control":U, "no-store, no-cache, must-revalidate").
        run outputhttpheader in webHdl("Expires":U, "Sat, 01 Jan 2000 01:00:00 GMT").
        run outputhttpheader in webHdl("Last-Modified":U, DYNAMIC-FUNCTION("format-datetime" in webHdl,"HTTP",today,time,"LOCAL")).
        run outputhttpheader in webHdl("Pragma":U, "no-cache").
        run outputhttpheader in webHdl("", "").
      end.
      else
      do:
        run outputhttpheader IN webHdl ("Content-Type":U, "text/json").
        run outputhttpheader in webHdl ("Cache-Control":U, "no-store, no-cache, must-revalidate").
        run outputhttpheader in webHdl("Expires":U, "Sat, 01 Jan 2000 01:00:00 GMT").
        run outputhttpheader in webHdl("Last-Modified":U, DYNAMIC-FUNCTION("format-datetime" in webHdl,"HTTP",today,time,"LOCAL")).
        run outputhttpheader in webHdl("Pragma":U, "no-cache").
        run outputhttpheader in webHdl("", "").
      end.
    end method.

    METHOD PUBLIC FINAL LOGICAL setStorageKeys (input pStorageEntity as char, input pStorageEntityId as char):
     
     IF pStorageEntity = "" OR pStorageEntityId = "" 
      THEN
       RETURN FALSE.
        
     ASSIGN
      THIS-OBJECT:storageEntity   = pStorageEntity
      THIS-OBJECT:storageEntityId = pStorageEntityId.
      
     RETURN TRUE.                                                                                         

    END METHOD.
    
    METHOD PUBLIC FINAL LOGICAL setEmailParameter (input pEmailObjectName as char, input pEmailObjectValue as char):
  
     IF pEmailObjectName = "" OR pEmailObjectValue = "" 
      THEN
       RETURN FALSE.

     create iData.
     assign
      iData.objName       = "parameter"
      iData.objProperty   = pEmailObjectName
      iData.propertyValue = string(pEmailObjectValue).
      
     RETURN TRUE.

    END METHOD.

    DESTRUCTOR PUBLIC BaseGateway():
	END DESTRUCTOR.
END CLASS.
