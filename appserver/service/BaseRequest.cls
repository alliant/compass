 /*------------------------------------------------------------------------
    File        : BaseRequest
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : MohanC
    Created     : Sat Oct 26 12:52:13 CDT 2013
    Notes       : 09.04.2015 J.Oliver    Added fields for password timeout
    @Modified    :
    Date        Name    Comments
    06/21/2019  Archana   Modified to add Notifications
    09/03/2021  MK        Framework Changes
    11/26/2021  VR        Added THIS-OBJECT:emailHTML in baserequest constructor
    01/21/2022  SC        Added 'THIS-OBJECT:displayName = pGateway:displayName' 
                          to display action name in fault email
    06/08/2022  VR        Modified to add clob changes                     
    
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.


CLASS service.BaseRequest INHERITS service.BaseDebuggable IMPLEMENTS service.IRequest ABSTRACT:

    /* Standard Properties */
    {service/IProps.i &Set="PRIVATE SET."}
    
    /* iData temp-table definition for storing parameters */
    {service/IData.i &Mode="PROTECTED"}
    
    CONSTRUCTOR PUBLIC BaseRequest (INPUT pGateway AS service.IGateway):
    
      ASSIGN
          THIS-OBJECT:Uid = pGateway:Uid
          THIS-OBJECT:password = pGateway:password
          THIS-OBJECT:name = pGateway:NAME
          THIS-OBJECT:initials = pGateway:initials
          THIS-OBJECT:email = pGateway:email
          THIS-OBJECT:role = pGateway:role
          THIS-OBJECT:canExec = pGateway:canExec
          THIS-OBJECT:passwordSetDate = pGateway:passwordSetDate
          THIS-OBJECT:passwordExpireDate = pGateway:passwordExpireDate
          THIS-OBJECT:passwordExpired = pGateway:passwordExpired
          THIS-OBJECT:passwordWarnDays = pGateway:passwordWarnDays
          
          /* Request Properties */
          THIS-OBJECT:actionid = pGateway:actionid
          THIS-OBJECT:sessionid = pGateway:sessionid
          THIS-OBJECT:clientid = pGateway:clientid
          THIS-OBJECT:ipAddress = pGateway:ipAddress
          THIS-OBJECT:queryString = pGateway:queryString
          THIS-OBJECT:isUserActive = pGateway:isUserActive
          
          /* Action Properties */
          THIS-OBJECT:validAction = pGateway:validAction
          THIS-OBJECT:isSecure = pGateway:isSecure
          THIS-OBJECT:isActive = pGateway:isActive
          THIS-OBJECT:isAnonymous = pGateway:isAnonymous
          THIS-OBJECT:isLog = pGateway:isLog
          THIS-OBJECT:isAudit = pGateway:isAudit
          THIS-OBJECT:actionHandler = pGateway:actionHandler
          THIS-OBJECT:userids = pGateway:userids
          THIS-OBJECT:roles = pGateway:roles
          THIS-OBJECT:ipAddresses = pGateway:ipAddresses
          THIS-OBJECT:isEventsEnabled = pGateway:isEventsEnabled
          THIS-OBJECT:destination  = pGateway:destination 
          THIS-OBJECT:emailAddrList = pGateway:emailAddrList 
          THIS-OBJECT:printerAddr = pGateway:printerAddr
          THIS-OBJECT:repository = pGateway:repository
          THIS-OBJECT:outputFileFormat = pGateway:outputFileFormat
          THIS-OBJECT:notifyRequestor =pGateway:notifyRequestor
		  THIS-OBJECT:emailHtml = pGateway:emailHtml
          THIS-OBJECT:displayName = pGateway:displayName		  
          .
      
      RUN util/querystring2tt.p (INPUT THIS-OBJECT:queryString, 
                                 INPUT-OUTPUT TABLE iData) NO-ERROR.
       IF ERROR-STATUS:ERROR OR RETURN-VALUE > '' THEN DO:
         MESSAGE "Error on Parsing Query String: "  SKIP "UID: " + THIS-OBJECT:UID + ", " + "ACTION: " + THIS-OBJECT:ACTIONID + ", " + "Query String: " + THIS-OBJECT:QUERYSTRING + ", " + "Error Message: " + IF RETURN-VALUE > '' THEN RETURN-VALUE ELSE ERROR-STATUS:GET-MESSAGE(1).
       END.                                 
    
    END CONSTRUCTOR.

  /* GET METHODS */
  METHOD PUBLIC LOGICAL getParameter(INPUT pName AS CHARACTER, OUTPUT pValue AS CHARACTER ):
    DEFINE BUFFER iData FOR iData.
    FIND iData NO-LOCK
      WHERE iData.objName = "parameter"
        AND iData.objProperty = pName NO-ERROR.
    IF AVAILABLE iData 
     THEN pValue = decodeUrl(iData.propertyValue).
    RETURN AVAILABLE iData.
	END METHOD.

	METHOD PUBLIC LOGICAL getParameter(INPUT pName AS CHARACTER, OUTPUT pValue AS DATE ):
    DEFINE BUFFER iData FOR iData.
    FIND iData NO-LOCK
      WHERE iData.objName = "parameter"
        AND iData.objProperty = pName NO-ERROR.
    IF AVAILABLE iData 
     THEN ASSIGN pValue = DATE(decodeUrl(iData.propertyValue)) NO-ERROR.
    RETURN not ERROR-STATUS:ERROR AND AVAILABLE iData.
	END METHOD.

	METHOD PUBLIC LOGICAL getParameter(INPUT pName AS CHARACTER, OUTPUT pValue AS DECIMAL ):
    DEFINE BUFFER iData FOR iData.
    FIND iData NO-LOCK
      WHERE iData.objName = "parameter"
        AND iData.objProperty = pName NO-ERROR.
    IF AVAILABLE iData 
     THEN ASSIGN pValue = DECIMAL(decodeUrl(iData.propertyValue)) NO-ERROR.
    RETURN not ERROR-STATUS:ERROR AND AVAILABLE iData.
	END METHOD.

	METHOD PUBLIC LOGICAL getParameter(INPUT pName AS CHARACTER, OUTPUT pValue AS INTEGER ):
    DEFINE BUFFER iData FOR iData.
    FIND iData NO-LOCK
      WHERE iData.objName = "parameter"
        AND iData.objProperty = pName NO-ERROR.
    IF AVAILABLE iData 
     THEN ASSIGN pValue = INT(decodeUrl(iData.propertyValue)) NO-ERROR.
    RETURN not ERROR-STATUS:ERROR AND AVAILABLE iData.
	END METHOD.

	METHOD PUBLIC LOGICAL getParameter(INPUT pName AS CHARACTER, OUTPUT pValue AS LOGICAL ):
    DEFINE BUFFER iData FOR iData.
    FIND iData NO-LOCK
      WHERE iData.objName = "parameter"
        AND iData.objProperty = pName NO-ERROR.
    IF AVAILABLE iData 
     THEN ASSIGN pValue = (lookup(iData.propertyValue,"yes,true,1,Y,T") > 0).
    RETURN AVAILABLE iData.
	END METHOD.

	METHOD PUBLIC LOGICAL getParameter(INPUT pName AS CHARACTER, INPUT-OUTPUT TABLE-HANDLE pTable ):
    DEFINE BUFFER iData FOR iData.
    DEFINE VAR hTable AS HANDLE NO-UNDO.
    DEFINE VAR hField AS HANDLE NO-UNDO.
    DEFINE VAR tRecCount AS INT.
    
    hTable = pTable:default-buffer-handle.
    FOR EACH iData NO-LOCK
      WHERE iData.objName = pName
         BY iData.objSeq
         BY iData.objPropSeq:
         
      IF iData.objPropSeq = 1
       THEN 
        DO: hTable:BUFFER-CREATE().
            tRecCount = tRecCount + 1.
        END.
      hField = hTable:BUFFER-FIELD(iData.objProperty) NO-ERROR.
      IF VALID-HANDLE(hfield) AND hField:DATA-TYPE <> "date"
       THEN hField:BUFFER-VALUE = iData.propertyValue.
    END.
    
    DELETE OBJECT hTable NO-ERROR.
    DELETE OBJECT hField NO-ERROR.
    RETURN tRecCount > 0.
	END METHOD.
  
  METHOD PUBLIC LOGICAL getParameterTable( OUTPUT TABLE-HANDLE pTable ):
      pTable = TEMP-TABLE iData:HANDLE.
      return pTable:HAS-RECORDS.
  END METHOD.

  METHOD PUBLIC ABSTRACT LOGICAL getParameter(INPUT-OUTPUT DATASET-HANDLE pDataset, OUTPUT pErrMsg AS CHAR).       

  METHOD PUBLIC ABSTRACT LOGICAL setParameter(input DATASET-HANDLE pDataset).              

  /* GET METHODS */

  /* SET METHODS */
	METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHARACTER, INPUT pValue AS CHARACTER ):
    DEFINE BUFFER iData FOR iData.
    DEF VAR iSeq# AS INT INIT 0.
    
    FIND iData EXCLUSIVE-LOCK
      WHERE iData.objName = "parameter"
        AND iData.objProperty = pName NO-ERROR.
    IF NOT AVAILABLE iData THEN 
    DO: 
        FOR LAST iData NO-LOCK BY iData.objSeq:
            iSeq# = iData.objSeq.
        END.
        iSeq# = iSeq# + 1.      
        CREATE iData.
        ASSIGN
            iData.objSeq = iSeq#
            iData.objName = "parameter"
            iData.objPropSeq = 1
            iData.objProperty = pName
            NO-ERROR
            .
    END.
    
    ASSIGN
        iData.propertyValue = pValue
        NO-ERROR
        .
    
    IF ERROR-STATUS:ERROR 
     THEN RETURN FALSE.
     ELSE RETURN TRUE.
	END METHOD.

	METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHARACTER, INPUT pValue AS DATE ):
    DEFINE BUFFER iData FOR iData.
    DEF VAR iSeq# AS INT INIT 0.
    
    FIND iData EXCLUSIVE-LOCK
      WHERE iData.objName = "parameter"
        AND iData.objProperty = pName NO-ERROR.
    IF NOT AVAILABLE iData THEN 
    DO: 
        FOR LAST iData NO-LOCK BY iData.objSeq:
            iSeq# = iData.objSeq.
        END.
        iSeq# = iSeq# + 1.      
        CREATE iData.
        ASSIGN
            iData.objSeq = iSeq#
            iData.objName = "parameter"
            iData.objPropSeq = 1
            iData.objProperty = pName
            NO-ERROR
            .
    END.
    
    ASSIGN
        iData.propertyValue = STRING(pValue)
        NO-ERROR
        .
    
    IF ERROR-STATUS:ERROR 
     THEN RETURN FALSE.
     ELSE RETURN TRUE.
	END METHOD.

	METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHARACTER, INPUT pValue AS DECIMAL ):
    DEFINE BUFFER iData FOR iData.
    DEF VAR iSeq# AS INT INIT 0.
    
    FIND iData EXCLUSIVE-LOCK
      WHERE iData.objName = "parameter"
        AND iData.objProperty = pName NO-ERROR.
    IF NOT AVAILABLE iData THEN 
    DO: 
        FOR LAST iData NO-LOCK BY iData.objSeq:
            iSeq# = iData.objSeq.
        END.
        iSeq# = iSeq# + 1.      
        CREATE iData.
        ASSIGN
            iData.objSeq = iSeq#
            iData.objName = "parameter"
            iData.objPropSeq = 1
            iData.objProperty = pName
            NO-ERROR
            .
    END.
    
    ASSIGN
        iData.propertyValue = STRING(pValue)
        NO-ERROR
        .
    
    IF ERROR-STATUS:ERROR 
     THEN RETURN FALSE.
     ELSE RETURN TRUE.
	END METHOD.

	METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHARACTER, INPUT pValue AS INTEGER ):
    DEFINE BUFFER iData FOR iData.
    DEF VAR iSeq# AS INT INIT 0.
    
    FIND iData EXCLUSIVE-LOCK
      WHERE iData.objName = "parameter"
        AND iData.objProperty = pName NO-ERROR.
    IF NOT AVAILABLE iData 
     THEN 
      DO: 
        FOR LAST iData NO-LOCK BY iData.objSeq:
            iSeq# = iData.objSeq.
        END.
        iSeq# = iSeq# + 1.      
        CREATE iData.
        ASSIGN
            iData.objSeq = iSeq#
            iData.objName = "parameter"
            iData.objPropSeq = 1
            iData.objProperty = pName
            NO-ERROR
            .
      END.
    
    ASSIGN
        iData.propertyValue = STRING(pValue)
        NO-ERROR
        .
    
    IF ERROR-STATUS:ERROR 
     THEN RETURN FALSE.
     ELSE RETURN TRUE.
	END METHOD.

	METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHARACTER, INPUT pValue AS LOGICAL ):
    DEFINE BUFFER iData FOR iData.
    DEF VAR iSeq# AS INT INIT 0.
    
    FIND iData EXCLUSIVE-LOCK
      WHERE iData.objName = "parameter"
        AND iData.objProperty = pName NO-ERROR.
        
    IF NOT AVAILABLE iData
     THEN 
      DO: 
        FOR LAST iData NO-LOCK BY iData.objSeq:
            iSeq# = iData.objSeq.
        END.
        iSeq# = iSeq# + 1.      
        CREATE iData.
        ASSIGN
            iData.objSeq = iSeq#
            iData.objName = "parameter"
            iData.objPropSeq = 1
            iData.objProperty = pName
            NO-ERROR
            .
      END.
    
    iData.propertyValue = STRING(pValue) NO-ERROR.
    
    IF ERROR-STATUS:ERROR 
     THEN RETURN FALSE.
     ELSE RETURN TRUE.
	END METHOD.

	METHOD PUBLIC LOGICAL setParameter(INPUT pName AS CHARACTER, INPUT TABLE-HANDLE pTable ):
    DEFINE VAR hTable AS HANDLE NO-UNDO.
    DEFINE VAR hField AS HANDLE NO-UNDO.
    DEFINE VAR hQuery AS HANDLE NO-UNDO.
    DEFINE VAR pQuery AS CHAR NO-UNDO.
    DEFINE VAR iFldNum AS INT NO-UNDO.
    DEFINE VAR tRecCnt AS INT NO-UNDO.
   
    DEFINE BUFFER iData FOR iData.
    
    IF NOT VALID-HANDLE(pTable) OR 
       (VALID-HANDLE(pTable) AND NOT pTable:HAS-RECORDS)
     THEN RETURN FALSE.
    
    ASSIGN
      pName = IF pName = ? OR pName = "" THEN pTable:NAME ELSE pName
      hTable = pTable:DEFAULT-BUFFER-HANDLE
      pQuery = "FOR EACH " + hTable:NAME
      .
   
    /* Remove any previous parameter values */
    FOR EACH iData EXCLUSIVE-LOCK WHERE iData.objName = pName:
        DELETE iData.
    END.
    
    /* Create new parameter values */
    CREATE QUERY hQuery.
    hQuery:ADD-BUFFER(hTable).
    hQuery:QUERY-PREPARE(pQuery).
    hQuery:QUERY-OPEN().
    hQuery:GET-FIRST().
   
    REPEAT WHILE NOT hQuery:QUERY-OFF-END:
      tRecCnt = tRecCnt + 1.
      DO iFldNum = 1 TO hTable:NUM-FIELDS:
        hField = hTable:BUFFER-FIELD(iFldNum).
        CREATE iData.
        ASSIGN
          iData.objSeq = tRecCnt
          iData.objName = pName
          iData.objPropSeq = iFldNum
          iData.objProperty = hField:NAME
          iData.propertyValue = hField:BUFFER-VALUE
          iData.searchName = iData.objName + "." + iData.objProperty
         .
          RELEASE iData.
      END.
      hQuery:GET-NEXT.
    END.
    hQuery:QUERY-CLOSE.
    DELETE OBJECT hQuery NO-ERROR.
    DELETE OBJECT pTable NO-ERROR.
    RETURN tRecCnt > 0.
	END METHOD.
  /* SET METHODS */
    
  METHOD PUBLIC OVERRIDE VOID debug():
    DEFINE BUFFER iData FOR iData.
    FOR EACH iData NO-LOCK:
      if log-manager:logfile-name > ""
       then LOG-MANAGER:WRITE-MESSAGE ("Object: " + iData.objName + " | Property: " + iData.objProperty + " | Value: " + iData.propertyValue,THIS-OBJECT:GetClass():TypeName).
       else message "Object: " + iData.objName + " | Property: " + iData.objProperty + " | Value: " + iData.propertyValue view-as alert-box.
    END.
	END METHOD.
    
  method public character decodeUrl(input cValue as char):
    /**************************************************************************** 
    Description: Decodes unsafe characters in a URL as per RFC 1738 section 2.2. 
    <URL:http://ds.internic.net/rfc/rfc1738.txt>, 2.2 
    Input Parameters: Encoded character string to decode, 
    In addition, all characters specified in the variable cUnsafe plus ASCII values 0 <= x <= 31 and 127 <= x <= 255 are considered unsafe. 
    Returns: Decoded string (unkown value is returned as blank) 
    ****************************************************************************/ 
    DEFINE VARIABLE cHex        AS CHARACTER   NO-UNDO INITIAL "0123456789ABCDEF":U. 
    DEFINE VARIABLE iCounter    AS INTEGER     NO-UNDO. 
    DEFINE VARIABLE cChar       AS INTEGER     NO-UNDO. 
    define variable cTriplet    as character   no-undo.
    
    /* Unsafe characters that must be encoded in URL's.  See RFC 1738 Sect 2.2. */
    DEFINE VARIABLE cUnsafe   AS CHARACTER NO-UNDO INITIAL " <>~"#%~{}|~\^~~[]`":U.
    
    /* Reserved characters that normally are not encoded in URL's */
    DEFINE VARIABLE cReserved AS CHARACTER NO-UNDO INITIAL "~;/?:@=&":U.
    
    /* Don't bother with blank or unknown */ 
    IF LENGTH(cValue) EQ 0 OR 
       cValue         EQ ? THEN 
      RETURN "". 
    
    /* Loop through entire input string */ 
    iCounter = 0. 
    DO WHILE TRUE: 
      ASSIGN iCounter = iCounter + 1 
             /* ASCII value of character using single byte codepage */ 
             cChar = ASC(SUBSTRING(cValue, iCounter, 1, "RAW":U), 
                         "1252":U, 
                         "1252":U). 
    
      if chr(cChar) = "%" then
      do:
        assign 
            cTriplet = substring(cValue, iCounter, 3, "RAW":U)
            cChar    = ((index(cHex,substring(cTriplet,2,1)) - 1) * 16) + /* high hex digit */
                       (index(cHex,substring(cTriplet,3,1)) - 1).         /* low hex digit */
                       
        substring(cValue, iCounter, 3, "RAW":U) = chr(cChar).    
      end.
      
      IF iCounter = LENGTH(cValue,"RAW":U) THEN LEAVE. 
    END. 
    
    return cValue.
  end method.
	
  DESTRUCTOR PUBLIC BaseRequest ( ):
  END DESTRUCTOR.

END CLASS.
