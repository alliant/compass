/*------------------------------------------------------------------------
@name BatchUnlock
@description Unlocks a batch with the specified ID
@param BatchID;int;Batch ID to unlock
@returns Success;2018;Batch already unlocked
@returns Success;2000;Batch unlocked
@throws 3066;Invalid BatchID
@throws 3018;Batch locked by someone else (non-admin permissions)
@throws 3004;Record locked
@author Russ Kenny
@version 1.0 12/26/2013
@notes
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

def var pBatchID as int no-undo.
def var tFoundBatch as logical.

{lib/std-def.i}
{lib/nextkey-def.i}
{lib/newbatchaction.i}


pRequest:getParameter("BatchID", OUTPUT pBatchID).
IF NOT CAN-FIND(FIRST BATCH 
                  WHERE BATCH.batchID = pBatchID)
 then
  DO: pResponse:fault("3066", "Batch").
      RETURN.
  END.

{lib/islocked.i &obj='Batch' &id=string(pBatchID) &exclude-fault=TRUE}
if not std-lo
 then
  do: pResponse:success("2018", "Batch").
      return.
  end.
  
if std-ch <> pRequest:uid
 then
  do: 
      tCanDo = no.
      
      do std-in = 1 to num-entries(pRequest:role):
        if can-do("Administrator,OpsAdmin", entry(std-in,pRequest:role)) then
        do:
          tCanDo = yes.
          leave.
        end.
      end.
      
      if not tCanDo
       then
        do: pResponse:fault("3081", "Batch" {&msg-add} std-ch).
            return.
        end.
  end.
  

tFoundBatch = FALSE.
std-lo = false.

TRX-BLK:
for first batch exclusive-lock
  where batch.batchID = pBatchID TRANSACTION
   on error undo TRX-BLK, leave TRX-BLK:
 tFoundBatch = TRUE.

 run util/deletesyslock.p ("Batch", string(pBatchID), 0, output std-lo).
 if not std-lo
  then leave TRX-BLK.
  
 std-lo = newBatchAction(pBatchID, pRequest:uid, "Unlocked", true).
end.
 
if not tFoundBatch
 then pResponse:fault("3004", "Batch").
  
IF pResponse:isFault() 
 THEN RETURN.

  
pResponse:success("2000", "Unlock").
pResponse:updateEvent("Batch", string(pBatchID)).
if std-lo
 then pResponse:createEvent("BatchAction", string(pBatchID) {&id-add} string(std-in)).
 
