/*------------------------------------------------------------------------
@name BatchModify
@description Modifies the batch matching the specified ID using the specified inputs
@param BatchID;int;The ID of the batch to modify
@param DateReceived;datetime;Date and time batch was received  (optional)
@param AgentID;char;Agent ID batch belongs to  (optional)
@param Gross;dec;Batch gross premium amount  (optional)
@param Net;dec;Batch net premium amount  (optional)
@param Cash;dec;Batch cash amount  (optional)
@param Via;char;Batch received via  (optional)
@param Reference;char;Batch reference  (optional)

@throws 3000;Update failed
@throws 3003;No field changes submitted
@throws 3004;Batch locked
@throws 3027;Field not one of List
@throws 3066;Batch does not exist
@throws 3053;Field > Value
@throws 3054;Field < Value

@returns Success;int;

@author Russ Kenny
@version 1.0 01/03/2013
@notes
Modified: 
  Date          Name         Comments
  02/15/2020    AC           Added validation on Batch received Date 
  01/10/2022    SH           Added validation to check the batchform exist or not to modify agent.                    
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}
{lib/validatebatchform.i}

def var piBatchID as int no-undo.
def var pdtmDateRcvd as date no-undo.
def var pcAgentID as char no-undo.
def var pdGrossAmt as dec no-undo.
def var pdNetAmt as dec no-undo.
def var pdCashAmt as dec no-undo.
def var pcRcvdVia as char no-undo.
def var pcReference as char no-undo.
DEFINE VARIABLE tUpdated AS LOGICAL     NO-UNDO.
DEFINE VARIABLE tFound AS LOGICAL     NO-UNDO.
define variable dtPeriodFirstDate as date no-undo.
define variable dtPeriodLastDate  as date no-undo.

pRequest:getParameter("BatchID", OUTPUT piBatchID).
IF NOT CAN-FIND(FIRST BATCH 
                  WHERE BATCH.batchID = piBatchID)
 then pResponse:fault("3066", "Batch").

std-lo = pRequest:getParameter("DateReceived", OUTPUT pdtmDateRcvd).
IF std-lo AND pdtmDateRcvd > TODAY
 THEN pResponse:fault("3053", "Date Received" {&msg-add} STRING(TODAY)).
if pdtmDateRcvd = ? 
 then pResponse:fault("3005", "Received Date can not be blank.").

std-lo = pRequest:getParameter("AgentID", OUTPUT pcAgentID).
IF std-lo AND NOT CAN-FIND(FIRST agent
                             WHERE agent.agentID = pcAgentID) 
 then pResponse:fault("3066", "Agent").

for first batch no-lock where batch.batchID = piBatchID:
  if pcAgentID <> batch.agentID
   then
    if can-find(first batchform where batchform.batchID = batch.batchID)
     then
      pResponse:fault("3005", "Batch Form exists for this Batch. To change the Agent on the Batch, Batch Form(s) needs to be deleted.").
   
  if pdtmDateRcvd <> date(batch.receivedDate) 
   then
    do:
       dtPeriodFirstDate = date(batch.periodMonth,1,batch.periodYear).
 
       if batch.periodMonth <> 12
        then
         dtPeriodLastDate  = date(batch.periodMonth + 1,1, batch.periodYear) - 1.
        else
         dtPeriodLastDate  = date(1,1, batch.periodYear + 1) - 1.
   
       if dtPeriodFirstDate > pdtmDateRcvd  or 
          dtPeriodLastDate < pdtmDateRcvd
        then
         pResponse:fault("3005", "Date Received must lie in Batch Period.").   
    end.
end.  
  
std-lo = pRequest:getParameter("Gross", OUTPUT pdGrossAmt).
IF std-lo AND pdGrossAmt < 0 
 then pResponse:fault("3054", "Gross Premium" {&msg-add} "Zero").

std-lo = pRequest:getParameter("Net", OUTPUT pdNetAmt).
IF std-lo AND pdNetAmt < 0 
 then pResponse:fault("3054", "Net Premium" {&msg-add} "Zero").

std-lo = pRequest:getParameter("Cash", OUTPUT pdCashAmt).
IF std-lo AND pdCashAmt < 0
 then pResponse:fault("3054", "Cash Received" {&msg-add} "Zero").

if pResponse:isFault()
 then return.

{lib/islocked.i &obj='Batch' &id=piBatchID}

std-lo = pRequest:getParameter("Via", OUTPUT pcRcvdVia).
IF NOT std-lo 
 THEN pcRcvdVia = ?.

std-lo = pRequest:getParameter("Reference", OUTPUT pcReference).
IF NOT std-lo 
 THEN pcReference = ?.


tUpdated = FALSE.

TRX-BLOCK:
for FIRST batch exclusive-lock
  where batch.batchID = piBatchID TRANSACTION:

 if index("NP", batch.stat) = 0
  then
   do: pResponse:fault("3027", "Status" {&msg-add} "(N)ew or (P)rocessing").
       LEAVE TRX-BLOCK.
   end.

 if pdtmDateRcvd = ?
  then pdtmDateRcvd = batch.receivedDate.

 if pcAgentID = ? or pcAgentID = ''
  then pcAgentID = batch.agentID.

 
 tFound = FALSE.
 FOR FIRST agent EXCLUSIVE-LOCK
   WHERE agent.agentID = pcAgentID
     ON ERROR UNDO TRX-BLOCK, LEAVE TRX-BLOCK:

  tFound = TRUE.

  /* All previous batchForm validation is incorrect */
  if pcAgentID <> batch.agentID
   then
       for each batchForm exclusive-lock
          where batchForm.batchID = piBatchID
         ON ERROR UNDO TRX-BLOCK, LEAVE TRX-BLOCK:
        batchform.validForm = validateBatchForm
                                    (input piBatchID,
                                     input pcAgentID,
                                     input batchForm.formType,
                                     input batchForm.fileNumber,
                                     input batchForm.policyID,
                                     input batchForm.formID,
                                     input batchForm.rateCode,
                                     input batchForm.statCode,
                                     input batchForm.effDate,
                                     input batchForm.liabilityAmount,
                                     input batchForm.countyID,
                                     input batchForm.residential,
                                     input batchForm.grossPremium,
                                     input batchForm.netPremium,
                                     output batchForm.validMsg).
       end.

   assign
     batch.agentID = agent.agentID
     batch.agentName = agent.name
     batch.stateID = agent.stateID
     batch.receivedDate = pdtmDateRcvd
     batch.grossPremiumReported = pdGrossAmt
     batch.grossPremiumDiff = batch.grossPremiumProcessed - batch.grossPremiumReported
     batch.netPremiumReported = pdNetAmt
     batch.netPremiumDiff = batch.netPremiumProcessed - batch.netPremiumReported
     batch.cashReceived = pdCashAmt
     batch.rcvdVia = pcRcvdVia
     batch.reference = pcReference
     .
   validate batch.
   tUpdated = TRUE.
 END.
 IF tFound = FALSE 
  THEN pResponse:fault("3004", "Agent").
end.

if pResponse:isFault()
 then return.

IF NOT std-lo 
 THEN pResponse:fault("3004", "Batch").
    
pResponse:success("2006", "Batch").
pResponse:updateEvent("Batch", string(piBatchID)).
