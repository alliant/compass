::------------------------------------------------------------------------::
::    File        : cronwrapper.bat                                       ::
::                                                                        ::
::    Description : Wraps a OpenEdge program for compass in a batch file  ::
::                  suitable to be scheduled within task scheduler        ::
::                                                                        ::
::    Parameter   : %~1;string;The environment that the schedule is for   ::
::    Parameter   : %~2;string;The compass action to call                 ::
::    Parameter   : %~3;string;The user setting up the schedule           ::
::    Parameter   : %~4;string;The parameters to the program              ::
::                                                                        ::
::    Author(s)   : John Oliver (joliver)                                 ::
::    Created     : 07.12.2018                                            ::
::    Notes       :                                                       ::
::------------------------------------------------------------------------::
@echo off

:: arguments
set ENV=%~1
set ACTION=%~2
set USER=%~3
set QUERY=%~4

:: script variables
set DIR_CONF=D:\webspeed\conf
set DIR_SCR=D:\webspeed\scripts
set DIR_LOG=%DIR_SCR%\log

::date / Time variable
set datef=%date:~-4%-%date:~7,2%-%date:~4,2%

set hour=%time:~0,2%
if "%hour:~0,1%" == " " set hour=0%hour:~1,1%
set min=%time:~3,2%
if "%min:~0,1%" == " " set min=0%min:~1,1%
set secs=%time:~6,2%
if "%secs:~0,1%" == " " set secs=0%secs:~1,1%

set timef=%hour%-%min%-%secs%

:: the log file will fail to create if there is a / character
for %%G in (%ACTION%) do set LOGFILE=%DIR_LOG%\%ENV%_%datef%_%timef%_%%~nG.log
for %%G in (%ACTION%) do set RUNFILE=%DIR_LOG%\%ENV%_%%~nG.dat

if "%ENV%" EQU "live" (
  set PGM_INI=%DIR_CONF%\wrapperlive.ini
  set PARAM=%DIR_CONF%\wslive.ini
  set PF=%DIR_CONF%\wslive.pf
  set DIR_WRK=D:\webspeed\wrk\wslive
) else if "%ENV%" EQU "beta" (
    set PGM_INI=%DIR_CONF%\wrapperbeta.ini
    set PARAM=%DIR_CONF%\wsbeta.ini
    set PF=%DIR_CONF%\wsbeta.pf
    set DIR_WRK=D:\webspeed\wrk\wsbeta
  ) else if "%ENV%" EQU "dvlp" (
      set PGM_INI=%DIR_CONF%\wrapperdvlp.ini
      set PARAM=%DIR_CONF%\wsdvlp.ini
      set PF=%DIR_CONF%\wsdvlp.pf
      set DIR_WRK=D:\webspeed\wrk\wsdvlp
	) else (
        set PGM_INI=%DIR_CONF%\wrapperalfa.ini
        set PARAM=%DIR_CONF%\wsalfa.ini
        set PF=%DIR_CONF%\wsalfa.pf
        set DIR_WRK=D:\webspeed\wrk\wsalfa
    )
	
set "PARAM=%PARAM%,%USER%,%ACTION%,%QUERY%"

set MAINLOGFILE=%DIR_LOG%\%ENV%_doqueue.log
:: Check for the RUNFILE first 

if not exist %RUNFILE% (
  cd /D %DIR_WRK%
  echo %date% %time% > %RUNFILE%
  call :log "C:\Progress\OpenEdge\bin\_progres.exe -b -ini %PGM_INI% -pf %PF% -p doqueue.p -param %PARAM% - Started"
  C:\Progress\OpenEdge\bin\_progres.exe -b -ini %PGM_INI% -pf %PF% -p doqueue.p -param %PARAM% >> %LOGFIlE%
  call :log "C:\Progress\OpenEdge\bin\_progres.exe -b -ini %PGM_INI% -pf %PF% -p doqueue.p -param %PARAM% - Ended"
  del %RUNFILE%
) else (
  call :log "There is a touch file present"
)

type %LOGFIlE% >> %MAINLOGFILE%
call :setsize %MAINLOGFILE% 

del %LOGFIlE%

if %size% geq 10000000 (  
  move "%MAINLOGFILE%" "%DIR_LOG%\%ENV%_doqueue.bak"
)

goto :eof

:setsize
set size=%~z1
exit /b

:log
  set MSG=%1
  set MSG=%MSG:~1,-1%
  echo %DATE% - %TIME%: %MSG% >> "%LOGFILE%" 2>&1
exit /b

:trim
  set STR=%~1
  if "%STR:~-1%" EQU " " (
    set STR=%STR:~0,-1%
    call :trim %STR%
  ) else (
    set COMP=%STR%
  )
exit /b