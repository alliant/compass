/*------------------------------------------------------------------------
@name agent/prospectagent.p
@action agentProspect
@description Set agent Status Prospect 
@param ipcAgentID Agent ID

@success 2000;char;Agent Status updation
@success 3000;char;Fail

@author Naresh Chopra
@version 1.0
@created 09/25/2018
Modification:
Date          Name      Description
08/20/20      VJ        Added logic for active field of role
02/14/24      SRK       Modified to create tag
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

{lib/std-def.i}
{lib/com-def.i}

define variable cAgentID       as character no-undo.
define variable cMsgLog        as character no-undo.
define variable plSuccess      as logical   no-undo.
define variable lisActive      as logical   no-undo.
define variable cAgentStat     as character no-undo.
define variable cAgentStatDesc as character no-undo.

&scoped-define ExcludeStatus "P,C,X"

pRequest:getParameter("AgentID", output cAgentID).

/* validate the statereqqual id is valid */
if not can-find(first agent where agent.agentID = cAgentID)
 then 
  pResponse:fault("3067", "agent" {&msg-add} "ID" {&msg-add} string(cAgentID)).

if (can-find(first agent where agent.agentID = cAgentID and agent.stat = {&ActiveStat})) and
   (can-find(first agentactivity where agentactivity.agentID = cAgentID  and 
            ((agentactivity.category = "P" and agentactivity.type = "I") or 
            ( agentactivity.category = "I" and agentactivity.type = "A" )))) 
 then 
  pResponse:fault("3005", "Agent status cannot be changed back to Prospect as agent has issued either a CPL or a Policy.").

if pResponse:isFault() 
 then 
  return.
 
/* validate the status */
for first agent exclusive-lock  
    where agent.agentID = cAgentID:

  cAgentStat = agent.stat.
  for first sysprop no-lock
      where sysprop.appCode = "AMD"
        and sysprop.objAction = "Agent"
        and sysprop.objProperty = "Status"
        and sysprop.objID = cAgentStat:
      
    cAgentStatDesc = sysprop.objValue.
  end.
end.

if lookup(cAgentStat, {&ExcludeStatus}) > 0
 then 
  pResponse:fault("3005", "Agent status must not go from " + cAgentStatDesc + " to Prospect").
 
if pResponse:isFault()
 then return.
  
std-lo = false.
AGENT-BLOCK:
for first agent exclusive-lock  
  where agent.agentID = cAgentID transaction
    on error undo AGENT-BLOCK, leave AGENT-BLOCK:

  assign
      agent.stat         = {&ProspectStat}
      agent.prospectDate = now
      .
         
  if cAgentStat = {&ActiveStat} 
   then
    do:
      for first orgrole where orgrole.sourceID = cAgentID no-lock:

        buffer orgrole:handle:find-current (exclusive-lock,no-wait).
        orgrole.active = false.
        buffer orgrole:handle:find-current (no-lock). 
        
        /* Creating compliance log record for agent */
        {lib/comlogmsg.i &codeVal = '106' , &Msg = cMsgLog , &stat = 'Prospect'}
        
        run com/newcompliancelog.p( input  {&OrganizationCode},
                                    input  orgrole.orgID,  
                                    input  cMsgLog,  
                                    input  agent.stateid,  
                                    input  pRequest:UID,
                                    input  pRequest:actionid,							 
                                    input  {&Agent},
                                    input  cAgentID,
                                    input  "",
                                    output plSuccess).
        if not plSuccess 
         then
          do:
            pResponse:fault("3000", "Comlog").
            return.
          end.
      end. /*end for first orgrole */
    end. /*if cAgentStat = {&ActiveStat}*/

    /* Creating compliance logs when deactivating an agent */ 
  for each fulfillment exclusive-lock
    where fulfillment.entity   = {&Agent}
      and fulfillment.entityId = cAgentID: 
  
    for first staterequirement no-lock
      where staterequirement.requirementid = fulfillment.requirementid:
      
      for first qualification no-lock
        where qualification.qualificationId = fulfillment.qualificationId:
                 
        delete fulfillment.
        release fulfillment.
      
        if qualification.entity = {&OrganizationCode} /*unlink organization qualification to requirement*/    
         then      
          do:      
            {lib/comlogmsg.i &codeVal = '606' , &Msg = cMsgLog , &qual = qualification.qualification , &qualId = string(qualification.qualificationId) , &req = staterequirement.description , &reqId = string(staterequirement.requirementid)}

            run com/newcompliancelog.p( input  {&OrganizationCode},      
                                        input  qualification.entityId, /*organizationID*/        
                                        input  cMsgLog,        
                                        input  staterequirement.stateId,      
                                        input  pRequest:UID,         
                                        input  pRequest:actionid,      
                                        input  {&Agent}, /*role of person or organization*/        
                                        input  cAgentID, /*roleid of person or organization*/           
                                        input  "",      
                                        output plSuccess).      
              
            if not plSuccess     
             then      
              do:      
                pResponse:fault("3000", "Comlog").    
                return.      
              end. /* if not plSuccess */      
                
            {lib/comlogmsg.i &codeVal = '603' , &Msg = cMsgLog , &qual = qualification.qualification , &qualId = string(qualification.qualificationId) , &role = 'Agent' , &roleID = cAgentID , &req = staterequirement.description , &reqId = string(staterequirement.requirementid)}

            run com/newcompliancelog.p( input  {&OrganizationCode},
                                        input  qualification.entityId,  
                                        input  cMsgLog,  
                                        input  staterequirement.stateId,
                                        input  pRequest:UID,
                                        input  pRequest:actionid,
                                        input  {&Agent},
                                        input  cAgentID,
                                        input  "",
                                        output plSuccess).
                               
            if not plSuccess 
             then
              do:
                pResponse:fault("3000", "Comlog").
                return.
              end.  /* if not plSuccess */      
               
          end.
          
        else /*unlink person qualification to requirement*/    
	     do:      
           {lib/comlogmsg.i &codeVal = '320' , &Msg = cMsgLog , &qual = qualification.qualification , &qualId = string(qualification.qualificationId) , &req = staterequirement.description , &reqId = string(staterequirement.requirementid)}      
          
           run com/newcompliancelog.p( input  {&PersonCode},      
                                       input  qualification.entityId, /*personID*/       
                                       input  cMsgLog,        
                                       input  staterequirement.stateId,      
                                       input  pRequest:UID,        
                                       input  pRequest:actionid,      
                                       input  {&Agent},   /*role  organization*/           
                                       input  cAgentID, /*roleid  organization*/             
                                       input  "",      
                                       output plSuccess).      
          
           if not plSuccess     
            then      
             do:      
               pResponse:fault("3000", "Comlog").     
               return.      
             end. /* if not plSuccess */      

           {lib/comlogmsg.i &codeVal = '317' , &Msg = cMsgLog , &qual = qualification.qualification , &qualId = string(qualification.qualificationId) , &role = 'Agent' , &roleID = cAgentID , &req = staterequirement.description , &reqId = string(staterequirement.requirementid)}

           run com/newcompliancelog.p( input  {&PersonCode},
                                       input  qualification.entityId,  
                                       input  cMsgLog,  
                                       input  staterequirement.stateId,
                                       input  pRequest:UID,
                                       input  pRequest:actionid,
                                       input  {&Agent},
                                       input  cAgentID,
                                       input  "",
                                       output plSuccess).
                              
           if not plSuccess 
            then
             do:
               pResponse:fault("3000", "Comlog").
               return.
             end. 
         end. /*qualfication.entity person*/
        
        /*in case of first pary the qualification become inactive*/
        if qualification.authorizedby = {&firstparty} and qualification.active = yes
         then
          do:
            buffer qualification:handle:find-current (exclusive-lock,no-wait).
            qualification.active = no.  
            buffer qualification:handle:find-current (no-lock).
          end.
          
      end. /* for first qualification... */
    end. /* for first staterequirement... */
  end. /* for each fulfillment... */
  
  std-lo = true.
  release agent.
end.
  
if not std-lo 
 then
  do:
    pResponse:fault("3000", "Agent status change").
    return.
  end. 
 else
  run amd/tagagents-p.p ( input cAgentID).

pResponse:success("2000", "Agent status change").

