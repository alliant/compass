&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file modifyagentactivity.p
@action agentPlanModify
@description Modifies an agent activity

@param Name;char;The name of the person for the plan
@param AgentID;char;The agentID for the plan
@param PeriodYear;integer;The year of the plan
@param Category;char;The category of the plan
@param Type;char;The type of the plan
@param Month1;decimal;The first month
@param Month2;decimal;The second month
@param Month3;decimal;The third month
@param Month4;decimal;The fourth month
@param Month5;decimal;The fifth month
@param Month6;decimal;The sixth month
@param Month7;decimal;The seventh month
@param Month8;decimal;The eighth month
@param Month9;decimal;The ninth month
@param Month10;decimal;The tenth month
@param Month11;decimal;The eleventh month
@param Month12;decimal;The twelfth month

@author John Oliver (joliver)
@created 02/04/2018
@notes
------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

/* Parameters */
define variable pActivityID as integer no-undo.
define variable pName as character no-undo.
define variable pAgentID as character no-undo.
define variable pStateID as character no-undo.
define variable pYear as integer no-undo.
define variable pCategory as character no-undo.
define variable pType as character no-undo.
define variable pMonth1 as decimal no-undo.
define variable pMonth2 as decimal no-undo.
define variable pMonth3 as decimal no-undo.
define variable pMonth4 as decimal no-undo.
define variable pMonth5 as decimal no-undo.
define variable pMonth6 as decimal no-undo.
define variable pMonth7 as decimal no-undo.
define variable pMonth8 as decimal no-undo.
define variable pMonth9 as decimal no-undo.
define variable pMonth10 as decimal no-undo.
define variable pMonth11 as decimal no-undo.
define variable pMonth12 as decimal no-undo.

/* Local Variables */
define variable cType as character no-undo.
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
 
/* validation */
pRequest:getParameter("ActivityID", output pActivityID).
pRequest:getParameter("Year", output pYear).
pRequest:getParameter("Category", output pCategory).
pRequest:getParameter("Type", output pType).
pRequest:getParameter("Name", output pName).
pRequest:getParameter("AgentID", output pAgentID).
pRequest:getParameter("StateID", output pStateID).
{lib/validateactivity.i &activityID=pActivityID
                        &activityYear=pYear 
                        &activityCategory=pCategory 
                        &activityType=pType
                        &activityAgent=pAgentID
                        &activityName=pName
                        &activityState=pStateID
                        &appserver=true}

if pResponse:isFault()
 then return.

if pAgentID > ""
 then pName = "".
 
if pName > ""
 then pAgentID = "".

pRequest:getParameter("Month1", output pMonth1).
pRequest:getParameter("Month2", output pMonth2).
pRequest:getParameter("Month3", output pMonth3).
pRequest:getParameter("Month4", output pMonth4).
pRequest:getParameter("Month5", output pMonth5).
pRequest:getParameter("Month6", output pMonth6).
pRequest:getParameter("Month7", output pMonth7).
pRequest:getParameter("Month8", output pMonth8).
pRequest:getParameter("Month9", output pMonth9).
pRequest:getParameter("Month10", output pMonth10).
pRequest:getParameter("Month11", output pMonth11).
pRequest:getParameter("Month12", output pMonth12).

std-lo = false.
TRX-BLOCK:
for first agentactivity exclusive-lock
    where agentactivity.activityID = pActivityID TRANSACTION
    on error undo TRX-BLOCK, leave TRX-BLOCK:
  
  assign
    agentactivity.agentID = pAgentID
    agentactivity.name = pName
    agentactivity.stateID = pStateID
    agentactivity.year = pYear
    agentactivity.category = pCategory
    agentactivity.type = pType
    .
  /* if not admin, only can modify future months */
  if {lib/isadmin.i}
   then
    assign
      agentactivity.month1 = pMonth1
      agentactivity.month2 = pMonth2
      agentactivity.month3 = pMonth3
      agentactivity.month4 = pMonth4
      agentactivity.month5 = pMonth5
      agentactivity.month6 = pMonth6
      agentactivity.month7 = pMonth7
      agentactivity.month8 = pMonth8
      agentactivity.month9 = pMonth9
      agentactivity.month10 = pMonth10
      agentactivity.month11 = pMonth11
      agentactivity.month12 = pMonth12
      .
   else
    assign
      agentactivity.month1 = (if pYear = year(today) and 1 <= month(today) then agentactivity.month1 else pMonth1)
      agentactivity.month2 = (if pYear = year(today) and 2 <= month(today) then agentactivity.month2 else pMonth2)
      agentactivity.month3 = (if pYear = year(today) and 3 <= month(today) then agentactivity.month3 else pMonth3)
      agentactivity.month4 = (if pYear = year(today) and 4 <= month(today) then agentactivity.month4 else pMonth4)
      agentactivity.month5 = (if pYear = year(today) and 5 <= month(today) then agentactivity.month5 else pMonth5)
      agentactivity.month6 = (if pYear = year(today) and 6 <= month(today) then agentactivity.month6 else pMonth6)
      agentactivity.month7 = (if pYear = year(today) and 7 <= month(today) then agentactivity.month7 else pMonth7)
      agentactivity.month8 = (if pYear = year(today) and 8 <= month(today) then agentactivity.month8 else pMonth8)
      agentactivity.month9 = (if pYear = year(today) and 9 <= month(today) then agentactivity.month9 else pMonth9)
      agentactivity.month10 = (if pYear = year(today) and 10 <= month(today) then agentactivity.month10 else pMonth10)
      agentactivity.month11 = (if pYear = year(today) and 11 <= month(today) then agentactivity.month11 else pMonth11)
      agentactivity.month12 = (if pYear = year(today) and 12 <= month(today) then agentactivity.month12 else pMonth12)
      .
  release agentactivity.
  std-lo = true.
end.

if std-lo
 then pResponse:success("2006", "Agent Activity").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

