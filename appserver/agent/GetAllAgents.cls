/*----------------------------------------------------------------------
@name GetAllAgents.cls
@action allAgentsGet
@description Provides list of active,cancelled,closed and prospect agents.
@param 
@returns Success:2005
@author S Chandu
@created 06.13.2024
Modified
Date       Name    Description              
07/16/2024 SB      Modified to return only agents which have production files
----------------------------------------------------------------------*/

class agent.GetAllAgents inherits framework.ActionBase:

  {tt/agent.i &tableAlias="ttAgent" &getActiveAgents=true}

  constructor public GetAllAgents ():
    super().
  end constructor.

  destructor public GetAllAgents ():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}
    define variable pcStateIDList as character no-undo.
    define variable pcStatusList  as character no-undo.
    define variable pcUID         as character no-undo.
    define variable dsHandle      as handle    no-undo.
    define variable lSuccess      as logical   no-undo.
    define variable iCount        as integer   no-undo.
    define variable cMsg          as character no-undo.
    define variable lOnlyProductionFiles as logical init false no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttAgent:handle).

    pRequest:getParameter("StateIDList", output pcStateIDList).
    pRequest:getParameter("StatusList", output pcStatusList).
    pRequest:getParameter("OnlyProductionFiles", output lOnlyProductionFiles).
    pcUID = prequest:UID.

    if lOnlyProductionFiles = ? 
     then 
      lOnlyProductionFiles = no.

    if pcStateIDList = ? 
     then
      pcStateIDList = ''.

    if pcStatusList = ? 
     then
      pcStatusList = ''. 

   run agent\getallagents-p.p(input pcStateIDList,
                              input pcStatusList,
                              input pcUID,
                              input lOnlyProductionFiles,
                              output table ttAgent,
                              output iCount,
                              output lSuccess,
                              output cMsg).
 
   if not lSuccess 
    then
     do:
       pResponse:fault("3005", cMsg).
       return.
     end.

    if can-find(first ttAgent) 
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2005", string(icount) {&msg-add} "Agent").
     
  end method.
  
end class.


