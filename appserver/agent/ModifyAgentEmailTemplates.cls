/*----------------------------------------------------------------------
@name ModifyAgentEmailTemplates.cls
@action agentEmailTemplatesModify
@description Modify/create a Agent Email Template data 
@param agentNote:temptabele;
@author Vignesh Rajan
@created 04/12/2023
Modified
Date       Name    Description              

----------------------------------------------------------------------*/

class agent.ModifyAgentEmailTemplates inherits framework.ActionBase:

  {lib/callsp-defs.i}
  {tt/agentnote.i &tableAlias=ttAgentNote}
  
  constructor public ModifyAgentEmailTemplates ():
    super().
  end constructor.

  destructor public ModifyAgentEmailTemplates ():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}

    define variable cAction  as character no-undo.

    define variable dsHandle as handle    no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttAgentNote:handle).

    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.

    if not can-find(first ttAgentNote) 
     then
      do:
        pResponse:fault('3066','Agent Note').
        return.
      end.

    for each ttAgentNote:
  
      if ttAgentNote.agentID = '' or ttAgentNote.agentID = ?
       then
        do:
          pResponse:fault('3005', 'AgentID cannot be blank or null.').
          return.
        end.

      if ttAgentNote.category = "" or ttAgentNote.category = ? 
       then
        do:
          pResponse:fault('3005', 'Category cannot be blank or null').
        end.
    end.

    if pResponse:isFault() 
     then
      return.

    TRX-BLOCK: 
    for each ttAgentNote transaction on error undo TRX-BLOCK, leave TRX-BLOCK:
      if not can-find(first agentNote where agentNote.agentID = ttAgentNote.agentID and agentNote.seq = ttAgentNote.seq and agentNote.category = ttAgentNote.category) 
       then
        do:
          create agentNote.
          assign
            agentNote.agentID  = ttAgentNote.agentID
            agentNote.seq      = getNextSeq(ttAgentNote.agentID)
            agentNote.noteDate = now
            agentNote.uid      = pRequest:uid
            agentNote.category = ttAgentNote.category
            agentNote.subject  = ttAgentNote.subject
            agentNote.noteType = ttAgentNote.noteType
            agentNote.notes    = ttAgentNote.notes.

          validate agentNote.
          release agentNote.

          assign
            std-lo  = true
            cAction = "Create".

        end.
       else 
        do:
          for first agentNote where agentNote.agentID  = ttAgentNote.agentID 
                                and agentNote.seq      = ttAgentNote.seq 
                                and agentNote.category = ttAgentNote.category exclusive-lock:
            agentNote.notes = ttAgentNote.notes.

            assign
              std-lo  = true
              cAction = "Update".

          end.
        end.
    end.

    if not std-lo 
     then
      do:
        pResponse:fault("3000", "Update").
        return.
      end.


    pResponse:success("2000", "Agent Note " + cAction). 
     
  end method.

  method private integer getNextSeq(ipcAgentID as character):
    define variable iSeq as integer no-undo initial 1.

    define buffer bAgentNote for agentnote.

    for first bAgentNote no-lock
        where bAgentNote.agentID = ipcAgentID
           by bAgentNote.seq desc:
      iSeq = iSeq + bAgentNote.seq.
    end.

    return iSeq.
  end method.

  
end class.


