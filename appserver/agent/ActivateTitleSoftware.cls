/*------------------------------------------------------------------------
@name ActivateTitleSoftware.cls
@action titleSoftwareActivate
@description Activate a titlesoftware record
@param dataset containing softwareID of record
@throws 3066; Title software does not exist.
@return 2000;success 
@author Sagar
@version 1.0
@created 03.14.2024
@Modified :
Date        Name          Comments
  ----------------------------------------------------------------------*/

class agent.ActivateTitleSoftware inherits framework.ActionBase:

   /* Temp Tables */
  {tt/titlesoftware.i &tableAlias="ttTitleSoftware"}

  constructor public ActivateTitleSoftware ():
    super().
  end constructor.

  destructor public ActivateTitleSoftware ():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}

    define variable piSoftwareID as integer no-undo.
    
    pRequest:getParameter("softwareID", output piSoftwareID).

    if piSoftwareID = ?
     then
      piSoftwareID = 0.
    if not can-find(first titlesoftware where softwareID = piSoftwareID)
     then
      do:   
        pResponse:fault("3066", "TitleSoftware").
        return.
      end.
   
    std-lo = false.
    TRX-BLOCK:
    for first titlesoftware exclusive-lock
      where titlesoftware.softwareID = piSoftwareID transaction
       on error undo, leave:
      titlesoftware.active = true.
      release titlesoftware.
      std-lo = true.
    end.

    if not std-lo
     then
      do: 
        pResponse:fault("3000", "TitleSoftware Activation").
        return.
      end.
   
    pResponse:success("2000", "TitleSoftware Activation").
  end method.
  
end class.

