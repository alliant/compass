/*----------------------------------------------------------------------
@name gettitlesoftwares
@action titleSoftwaresGet
@description Provides a complete list of all title softwares
@param SoftwareID;char;Software ID (optional)
@returns Title software
@author Sachin Chaturvedi
@created 07.11.2022
Modified
Date       Name    Description              

----------------------------------------------------------------------*/

class agent.GetTitleSoftwares inherits framework.ActionBase:

  {lib/callsp-defs.i}
  {tt/titlesoftware.i &tableAlias="ttTitleSoftware"}

  constructor public GetTitleSoftwares ():
    super().
  end constructor.

  destructor public GetTitleSoftwares ():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}
    define variable iSoftwareID as integer no-undo.
    define variable dsHandle    as handle  no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttTitleSoftware:handle).

    pRequest:getParameter("SoftwareID", output iSoftwareID).

    if iSoftwareID = ?
     then
      iSoftwareID = 0.

    {lib\callsp.i 
        &name=spGetTitleSoftwares 
        &load-into=ttTitleSoftware 
        &params="input iSoftwareID"
        &noResponse=true}
    

 
    if can-find(first ttTitleSoftware) 
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2005", string(csp-icount) {&msg-add} "Title Software").
     
  end method.
  
end class.


