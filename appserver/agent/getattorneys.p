/*------------------------------------------------------------------------
@name getAttorneys.p
@action attorneysGet
@description Returns a temp-table of the attorneys
@returns The Attorney temp-table
@success 2005;char;The count of the records returned

@author SC
@version 1.0
@created 07/24/2017
@modified
Date           Name      Description   
03/17/2022    Shefali    Task#86699  Get all attorneys including attorney firms
05/20/2022    Shefali    Task#93684  Get the attorney as per attorneyID parameter.
----------------------------------------------------------------------*/

def input parameter pRequest  as service.IRequest.
def input parameter pResponse as service.IResponse.

define variable pAttorneyID   as char no-undo.
define variable pAttorneyType as char no-undo.

pRequest:getParameter("attorneyID", output pAttorneyID).
pRequest:getParameter("attorneyType", output pAttorneyType).

{lib/std-def.i}
{tt/attorney.i &tableAlias="ttAttorney"}

std-in = 0.

if pAttorneyType = "P" 
 then
    for each attorney no-lock where attorney.attorneyID = (if pAttorneyID = "" then attorney.attorneyID else pAttorneyID) and 
                                    (attorney.orgID = "" or attorney.orgID = ?):   
      create ttAttorney.
      buffer-copy attorney to ttAttorney.
      std-in = std-in + 1.
    end.
 else if pAttorneyType = "O" 
  then
    for each attorney no-lock where attorney.attorneyID = (if pAttorneyID = "" then attorney.attorneyID else pAttorneyID) and
                                    (attorney.personID = "" or attorney.personID = ?):   
      create ttAttorney.
      buffer-copy attorney to ttAttorney.
      std-in = std-in + 1.
    end.
  else
   for each attorney no-lock where attorney.attorneyID = (if pAttorneyID = "" then attorney.attorneyID else pAttorneyID):   
      create ttAttorney.
      buffer-copy attorney to ttAttorney.
      std-in = std-in + 1.
    end.
if std-in > 0
 then pResponse:setParameter("Attorney", table ttAttorney).
pResponse:success("2005", string(std-in) {&msg-add} "Attorney").


