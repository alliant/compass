&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@name newagentalertnote.p
@action agentAlertNoteNew
@description Adds a new alert note

@param AlertID;int;The alert ID
@param Category;char;The category
@param Description;char;The actual note

@author John Oliver
@version 1.0
@created 10/09/2017
----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

/* parameters */
define variable pAlertID as integer no-undo.
define variable pCategory as character no-undo.
define variable pDescription as character no-undo.

/* variables */
define variable iSeq as integer no-undo.
define variable cAgentID as character no-undo.
define variable cCode as character no-undo.

{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

pRequest:getParameter("AlertID", output pAlertID).
if not can-find(first alert where alertID = pAlertID)
 then pResponse:fault("3066", "Alert " + string(pAlertID)).

pRequest:getParameter("Category", output pCategory).
if not can-find(first sysprop where appCode = "AMD" and objAction = "AlertNote" and objProperty = "Category" and objID = pCategory)
 then pResponse:fault("3066", "Category").

if pResponse:isFault()
 then return.
 
pRequest:getParameter("Description", output pDescription).

/* all parameters are valid */
std-lo = false.
TRX-BLOCK:
do TRANSACTION
  on error undo TRX-BLOCK, leave TRX-BLOCK:
  
  /* the alert is always on the active alert */
  for first alert no-lock
      where alert.alertID = pAlertID:
    
    cAgentID = alert.agentID.
    cCode = alert.processCode.
  end.
  for first alert no-lock
      where alert.agentID = cAgentID
        and alert.processCode = cCode
        and alert.active = true:
    
    pAlertID = alert.alertID.
  end.
  /* set the note */
  iSeq = 1.
  for first alertnote no-lock
      where alertnote.alertID = pAlertID
         by alertnote.seq desc:
         
    iSeq = iSeq + alertnote.seq.
  end.
  
  create alertnote.
  assign
    alertnote.alertID = pAlertID
    alertnote.seq = iSeq
    alertnote.dateCreated = now
    alertnote.createdBy = pRequest:uid
    alertnote.category = pCategory
    alertnote.description = pDescription
    .
  std-lo = true.
end.

pResponse:success("2002", "New Alert Note").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


