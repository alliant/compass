/*----------------------------------------------------------------------
@name GetActiveAgents.cls
@action activeAgentsGet
@description Provides list of active and prospect agents.
@param stateIDList;char(optional)
@returns Success:2005
@author S Chandu
@created 04.03.2023
Modified
Date        Name      Description              
06/24/2024 S Chandu   Moved code fetching code to -p.p.
----------------------------------------------------------------------*/

class agent.GetActiveAgents inherits framework.ActionBase:

  {lib/callsp-defs.i }
  {tt/agent.i &tableAlias="ttAgent" &getActiveAgents=true}

  constructor public GetActiveAgents ():
    super().
  end constructor.

  destructor public GetActiveAgents ():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}
    define variable pcStateIDList as character no-undo.
    define variable pcStatusList  as character no-undo.
    define variable pcUID         as character no-undo.
    define variable dsHandle      as handle    no-undo.
    define variable lSuccess      as logical   no-undo.
    define variable iCount        as integer   no-undo.
    define variable cMsg          as character no-undo.
    define variable lOnlyProductionFiles as logical no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttAgent:handle).

    pRequest:getParameter("StateIDList", output pcStateIDList).
    pRequest:getParameter("StatusList", output pcStatusList).
    pRequest:getParameter("OnlyProductionFiles", output lOnlyProductionFiles).
    pcUID = prequest:UID.

    if lOnlyProductionFiles = ? 
     then 
      lOnlyProductionFiles = no.

    if pcStateIDList = ? 
     then
      pcStateIDList = ''.

    if pcStatusList = ? 
     then
      pcStatusList = ''. 

    run agent\getallagents-p.p(input pcStateIDList,
                               input pcStatusList, /* Active and prospect */
                               input pcUID,
                               input lOnlyProductionFiles,
                               output table ttAgent,
                               output iCount,
                               output lSuccess,
                               output cMsg).
 
   if not lSuccess 
    then
     do:
       pResponse:fault("3005", cMsg).
       return.
     end.
    if can-find(first ttAgent) 
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2005", string(icount) {&msg-add} "Agent").
     
  end method.
  
end class.


