/*----------------------------------------------------------------------
@name GetAgentEmailTemplates.cls
@action agentEmailTemplateGet
@description Provides a complete list of all office note
@param AgentID:char;Category:char;seq:interger (optional)
@returns Office Note
@author Vignesh Rajan
@created 04/12/2023
Modified
Date       Name    Description              
08/07/24   SRK     Modified to send default email template
----------------------------------------------------------------------*/

class agent.GetAgentEmailTemplates inherits framework.ActionBase:

  {lib/callsp-defs.i}
  {tt/agentnote.i &tableAlias=ttAgentNote}
  
  constructor public GetAgentEmailTemplates ():
    super().
  end constructor.

  destructor public GetAgentEmailTemplates ():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}
    define variable cAgentID    as character no-undo.
    define variable cCategory   as character no-undo.
    define variable iSeq        as integer   no-undo.
    define variable dsHandle    as handle    no-undo.

    define variable cPolicy       as character no-undo.
    define variable cSearch       as character no-undo.
    define variable cConfirmation as character no-undo.
    define variable cErrormsg     as character no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttAgentNote:handle).

    pRequest:getParameter("AgentID", output cAgentID).
    pRequest:getParameter("Category", output cCategory).
    pRequest:getParameter("seq", output iSeq).

    publish "GetSystemParameter" from (session:first-procedure) ("OrderConfirmationEmailBody", output cConfirmation).
    publish "GetSystemParameter" from (session:first-procedure) ("SearchFulfillmentEmailBody", output cSearch).
    publish "GetSystemParameter" from (session:first-procedure) ("PolicyFulfillmentEmailBody", output cPolicy).

    if (cConfirmation = ? or cConfirmation = '')
     then
      cErrormsg = if cErrormsg <> '' then cErrormsg + "<br> OrderConfirmationEmailBody" else "OrderConfirmationEmailBody".

    if (cSearch = ? or cSearch = '')
     then
      cErrormsg = if cErrormsg <> '' then cErrormsg + "<br> SearchFulfillmentEmailBody" else "SearchFulfillmentEmailBody".

    if (cPolicy = ? or cPolicy = '')
     then
      cErrormsg = if cErrormsg <> '' then cErrormsg + "<br> PolicyFulfillmentEmailBody" else "PolicyFulfillmentEmailBody".

    if (cErrormsg <> ? and cErrormsg <> '')
     then
      do:
        cErrormsg = "Following system configuration(s) not found. Please contact the system administrator. <br>" + cErrormsg.
        run util/sysmail.p ("System configuration(s) not set", cErrormsg).
        pResponse:fault('3005', "system configuration(s) not found. Please contact the system administrator.").
        return.
      end.
   
    AgentNote:
    for each agentNote no-lock
       where agentNote.agentID = if cAgentID > '' then cAgentID else agentNote.agentID:
      
      if iSeq > 0 and agentNote.seq <> iseq then next AgentNote.
        
      if cCategory > '' and lookup(agentNote.category, cCategory) = 0 then next AgentNote.

      create ttAgentNote.
      buffer-copy agentNote to ttAgentNote.

      std-in = std-in + 1.
    end.

    if (cConfirmation <> ? and cConfirmation <> '')
     then
      do:
        create ttAgentNote.
        assign
            ttAgentNote.category  = 'Confirmation'
            ttAgentNote.notes     = cConfirmation.
      end.

    if (cSearch <> ? and cSearch <> '')
     then
      do:
        create ttAgentNote.
        assign
            ttAgentNote.category  = 'Search'
            ttAgentNote.notes     = cSearch.
      end.

    if (cPolicy <> ? and cPolicy <> '')
     then
      do:
        create ttAgentNote.
        assign
            ttAgentNote.category  = 'Policy'
            ttAgentNote.notes     = cPolicy.
      end.

    if can-find(first ttAgentNote) 
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).


    pResponse:success("2005", string(std-in) {&msg-add} "Agent Notes").
     
  end method.
  
end class.


