/*------------------------------------------------------------------------
@name AllAgentsGet
@description When called with an agent ID, gets the details for that ID, othewise provides a complete list of all agents
@param 
@returns Agents;complex;Agent dataset
@returns Success;int;2005
@author S Chandu
@version 1.0 06/13/2024
@notes
@modification
Date          Name          Description

----------------------------------------------------------------------*/
{tt/agent.i &tableAlias="ttAgent" &getActiveAgents=true}

define input  parameter ipcStateIDList  as character no-undo.
define input  parameter ipcStatusList   as character no-undo.
define input  parameter ipcUID          as character no-undo.
define input  parameter iplOnlyProductionFiles as logical no-undo.
define output parameter table for ttAgent.
define output parameter iCount     as integer  no-undo.
define output parameter lSuccess   as logical no-undo.
define output parameter cMsg       as character no-undo.

/* parameters */
define variable pcAgentID as character no-undo.

/* local variables */
define variable cUID as character no-undo.

{lib/std-def.i}
{lib/callsp-defs.i }


{lib\callsp.i 
     &name=spGetAllAgents 
     &load-into=ttAgent 
     &params="input ipcStateIDList, input ipcStatusList, input ipcUID, input iplOnlyProductionFiles"
     &noResponse=true}
     
iCount = csp-icount.

if not can-find(first ttAgent) 
 then
  do:
    cMsg     = "Fetching the Agents failed.".
    lSuccess = false.
  end.

cMSg = ''.
lSuccess = true.

