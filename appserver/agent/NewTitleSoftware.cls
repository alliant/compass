/*------------------------------------------------------------------------
@name NewTitleSoftware.p
@action titleSoftwareNew
@description Creates a new title software record
@param dataset for payload
@param returnRecord;logical;Whether newly created record to be returned or not
@returns title software record based on requirement.
@throws 3005; Data Parsing failed
@throws 3000; Create failed
@success 2002;success 
@author sachin chaturvedi
@version 1.0
@created 07.14.2022
@modified
Date        Name  Description
03/14/23    SRK   Added Active Field 
05/29/24    SRK   Added validation for phone and email
----------------------------------------------------------------------*/

class agent.NewTitleSoftware inherits framework.ActionBase:
    
  {tt/titlesoftware.i &tableAlias="ttTitleSoftware"}
            
  constructor NewTitleSoftware ():
    super().    
  end constructor.

  destructor public NewTitleSoftware ( ):        
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
        
    {lib/std-def.i} 
    {lib/nextkey-def.i}
    
    define variable iSoftwareID    as integer    no-undo.
    define variable lSuccess       as logical    no-undo. 
    define variable lReturnRecord  as logical    no-undo.  
    define variable dsHandle       as handle     no-undo.
    define variable cPhoneNumber   as character  no-undo.
    define variable lError         as logical    no-undo.
    define variable cerrmsg        as character  no-undo.

    create dataset  dsHandle.  
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttTitleSoftware:handle).
         
    pRequest:getParameter("returnRecord", output lReturnRecord).

    if lReturnRecord = ?
     then
      lReturnRecord = false.
    
    /* Getting temptable */
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).   
    if std-ch > ''
     then
      do:
        pResponse:fault('3005', "Data Parsing failed. Reason: " + std-ch).
        return.
      end.

    lSuccess = false.
    
    TRX-BLOCK:
    do transaction
      on error undo TRX-BLOCK, leave TRX-BLOCK:
    
     for first ttTitleSoftware: 
       if ttTitleSoftware.vendorID = "" or
          ttTitleSoftware.vendorID = ?
        then
         do:  
           pResponse:fault("3005", "Vendor cannot be blank").
           return.
         end. 
    
       if ttTitleSoftware.name = "" or
          ttTitleSoftware.name = ?
        then
         do:  
           pResponse:fault("3005", "Software cannot be blank").
           return.
         end.
    
         if can-find(first titlesoftware where titlesoftware.name = ttTitlesoftware.name 
            or replace(titlesoftware.name," ","") = replace(ttTitlesoftware.name," ","")) 
          then 
           do:
             pResponse:fault("3005", "Software with same name already exists.").
             return.
           end.
    
         if ttTitleSoftware.vendorID <> "" and 
            not can-find(first vendor where vendor.vendorID  = ttTitleSoftware.vendorID)
          then
           do:  
             pResponse:fault("3067", "Vendor" {&msg-add} "ID" {&msg-add} ttTitleSoftware.vendorID).
             return.
           end.

         if (ttTitleSoftware.contactEmail <> "" and ttTitleSoftware.contactEmail <> ?)
             and not validEmailAddr(ttTitleSoftware.contactEmail)
          then
           do:
             pResponse:fault("3005", "Invalid Email Address.").
             return.
           end.
          
         if (ttTitleSoftware.contactPhone <> "" and ttTitleSoftware.contactPhone <> ?)
          then
           do:
             run util/validatecontactnumber-p.p(input ttTitleSoftware.contactPhone,output cPhoneNumber,output lError,output cerrmsg).
             if lError 
              then
               do: 
                 pResponse:fault("3005", "Invalid Contact Number.").  
                 return.
               end.
           end.
        
          
       {lib/nextkey.i &type='softwareid' &var=iSoftwareID &err="undo TRX-BLOCK, leave TRX-BLOCK."}
          
       create titleSoftware.
       assign
           titleSoftware.softwareID        = iSoftwareID
           titleSoftware.name              = ttTitleSoftware.name
           titleSoftware.vendorID          = ttTitleSoftware.vendorID
           titleSoftware.addr              = ttTitleSoftware.addr
           titleSoftware.city              = ttTitleSoftware.city
           titleSoftware.state             = ttTitleSoftware.state
           titleSoftware.zip               = ttTitleSoftware.zip
           titleSoftware.url               = ttTitleSoftware.url
           titleSoftware.contactname       = ttTitleSoftware.contactname
           titleSoftware.contactEmail      = ttTitleSoftware.contactEmail
           titleSoftware.contactPhone      = cPhoneNumber
           titleSoftware.progExec          = ttTitleSoftware.progExec
           titleSoftware.CreatedDate       = now
           titleSoftware.CreatedBy         = pRequest:uid
           titleSoftware.active            = ttTitleSoftware.active
           .     
    
       validate titleSoftware. 
       release  titleSoftware.  
     end.  
     lSuccess = true.         
    end.
    
    if pResponse:isFault()
     then
      return.        
    
    if not lSuccess
     then
      do:
        pResponse:fault("3000", "Create").
        return.
      end.  
      
    if lReturnRecord
     then
      do:
        for first titleSoftware no-lock
         where titleSoftware.softwareID    = iSoftwareID:
          empty temp-table ttTitleSoftware.
          create ttTitleSoftware.
          assign
              ttTitleSoftware.softwareID        = titleSoftware.softwareID
              ttTitleSoftware.name              = titleSoftware.name
              ttTitleSoftware.vendorID          = titleSoftware.vendorID
              ttTitleSoftware.addr              = titleSoftware.addr
              ttTitleSoftware.city              = titleSoftware.city
              ttTitleSoftware.state             = titleSoftware.state
              ttTitleSoftware.zip               = titleSoftware.zip
              ttTitleSoftware.url               = titleSoftware.url
              ttTitleSoftware.contactname       = titleSoftware.contactname
              ttTitleSoftware.contactEmail      = titleSoftware.contactEmail
              ttTitleSoftware.contactPhone      = titleSoftware.contactPhone
              ttTitleSoftware.progExec          = titleSoftware.progExec
              tttitleSoftware.active            = titleSoftware.active
              .
        end.

        setContentDataSet(input dataset-handle dsHandle, input pRequest:FieldList).
      end.
    
    pResponse:success("2002"," titleSoftware").
                               
  end method.

  method private logical  validEmailAddr(input pEmailAddr as character):

    def var tBadEmailChars as char init "/?<>\:*|~"~~~'~`!#$%^&~{~}[];," no-undo.
    def var i as int no-undo.
    def var tEmailOK as log no-undo init yes.

    pEmailAddr = trim(pEmailAddr).

    do i = 1 to length(pEmailAddr):
      if index(tBadEmailChars, substring(pEmailAddr,i,1)) > 0 then
       do:
         tEmailOK = no.
         leave.
       end.
    end.
  
    if tEmailOK then
    if num-entries(pEmailAddr, "@") <> 2 then
     tEmailOK = no.

    if tEmailOK then
    if num-entries(pEmailAddr, ".") < 2 then
     tEmailOK = no.

    if tEmailOK then
    if num-entries(pEmailAddr, " ") > 1 then
     tEmailOK = no.

    if tEmailOK then
    do i = 1 to num-entries(pEmailAddr, "@"):
      if entry(i, pEmailAddr, "@") = "" or entry(i, pEmailAddr, "@") = "?" then
       do:
         tEmailOK = no.
         leave.
       end.
    end.
  
    if tEmailOK then
    do i = 1 to num-entries(pEmailAddr, "."):
     if entry(i, pEmailAddr, ".") = "" or entry(i, pEmailAddr, ".") = "?" then
      do:
       tEmailOK = no.
       leave.
      end.
    end.
  
    RETURN tEmailOK.

   end method.

end class.


