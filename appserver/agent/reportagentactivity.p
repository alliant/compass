&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file getagentactivities.p
@action agentActivitiesGet
@description Deletes an agent activity

@param Year;integer;The year of the activity
@param Category;char;The category of the activity
@param Name;char;The name of the agent for the activity
@param AgentID;char;The agent for the activity
@param StateID;char;The state of the agent for the activity
@param Decide;integer;1 for Agent and 2 for Target
@return AgentActivity;complex;The agent activity dataset

@author John Oliver (joliver)
@created 02/04/2018
@notes
@modification
Date          Name          Description
09/22/2020    VJ            Moved agent filter logic into spGetAgents
01-25-2022    Shubham       Task:89648 - Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"
------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

/* Parameters */
define variable pYear as integer no-undo.
define variable pCategory as character no-undo.
define variable pName as character no-undo.
define variable pAgentID as character no-undo.
define variable pStateID as character no-undo.
define variable pDecide as integer no-undo.
define variable pUID     as character no-undo.

/* Local Variables */

{lib/std-def.i}

{lib/callsp-defs.i}

/* Temp Table */
{tt/activity.i &tableAlias="ttActivity"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

pRequest:getParameter("Year", output pYear).
pRequest:getParameter("Category", output pCategory).
pRequest:getParameter("AgentID", output pAgentID).
if pAgentID = ""
 then pAgentID = "ALL".
if pAgentID <> "ALL" and not can-find(first agent where agentID = pAgentID)
 then pResponse:fault("3066", "Agent " + pAgentID).
pRequest:getParameter("StateID", output pStateID).
if pStateID = ""
 then pStateID = "ALL".
if pStateID <> "ALL" and not can-find(first state where stateID = pStateID)
 then pResponse:fault("3066", "State " + pStateID).

if pResponse:isFault()
 then return.

pUID  = pRequest:uid.

{lib\callsp.i 
    &name=spReportAgentActivities 
    &load-into=ttActivity 
    &params="input pAgentID, input pStateID, input pCategory, input pYear, input pUID"
        &setparam="'AgentActivity'"}

pResponse:success("2005", string(csp-icount) {&msg-add} "Agent Activity").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


