/*------------------------------------------------------------------------
@name CanProcessPolicy
@description Checks if the given policy is valid for the agent.
@param PolicyID;int;ID of the policy to verify
@param AgentID;char;ID of the agent to verify
@throws 3066;Invalid Policy or Agent
@throws 3067;Policy does not exist
@throws 3028;Invalid policy-agent relationship
@throws 3080;Policy previously processed
@returns EffDate;date;Either policy.effDate or policy.issuedEffDate depending on status
@returns LiabilityAmount;dec;Either policy.liabilityAmount or policy.issuedLiabilityAmount depending on status
@returns GrossPremium;dec;Either policy.grossPremium or policy.issuedGrossPremium depending on status
@returns Success;int;2001
@author Russ Kenny
@version 1.0 12/18/2013
@notes
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

def var piPolicyID as int no-undo.
def var pcAgentID as char no-undo.
def var tFormID as char no-undo.
def var tStateID as char no-undo.

{lib/std-def.i}


pRequest:getParameter("PolicyID", OUTPUT piPolicyID).
pRequest:getParameter("AgentID", OUTPUT pcAgentID).

if piPolicyID = 0 or piPolicyID = ?
 then
  do: pResponse:fault("3066", "Policy").
      return.
  end.
 
if pcAgentID = "" or pcAgentID = ?
 then
  do: pResponse:fault("3066", "Agent").
      return.
  end.
  
std-lo = false.
for first policy 
  no-lock
  where policy.policyID = piPolicyID:
 
 if policy.agentID <> pcAgentID
  then pResponse:fault("3028", "Agent" {&msg-add} policy.agentID {&msg-add} "Policy"). /* Error */
  else
 if policy.stat = "P"   /* It's (P)rocessed/invoiced */ /*policy.periodID <> 0*/ 
  then 
   do: pResponse:fault("3080", "Policy " + STRING(piPolicyID)). /* Warning */
       pResponse:setParameter("EffDate", policy.effDate).
       pResponse:setParameter("LiabilityAmount", policy.liabilityAmount).
       pResponse:setParameter("GrossPremium", policy.grossPremium).
       pResponse:setParameter("FormID", policy.formID).
       pResponse:setParameter("StateID", policy.stateID).
   end.
  else 
 if policy.stat = "I"  /* It's (I)ssued and is valid for processing */
  then
   do:
       pResponse:setParameter("EffDate", policy.issuedEffDate).
       pResponse:setParameter("LiabilityAmount", policy.issuedLiabilityAmount).
       pResponse:setParameter("GrossPremium", policy.issuedGrossPremium).
   
       assign tFormID  = ""
              tStateID = "".
       if policy.stateID <> "" then
       tStateID = policy.stateID.
       else
       do:
         for first agent where agent.agentID = policy.agentID no-lock:
            tStateID = agent.stateID.
         end.
       end.
       for first stateform
           where stateform.stateID = tStateID
             and stateform.formID = policy.issuedFormID
             and stateform.formID <> ""
             and stateform.formType = "P"
             no-lock:
             
         tFormID = stateform.formID.
       end.
       pResponse:setParameter("FormID", tFormID).
       pResponse:setParameter("StateID", tStateID).
   end.
  else 
 if policy.stat = "V"  /* It's (V)oided */
  then
   do:
       pResponse:fault("3028", "Status" {&msg-add} "(V)oided" {&msg-add} "Policy " + STRING(piPolicyID)). /* Error */
       pResponse:setParameter("EffDate", "").
       pResponse:setParameter("LiabilityAmount", 0).
       pResponse:setParameter("GrossPremium", 0).
       pResponse:setParameter("FormID", "").
       pResponse:setParameter("StateID", "").
   end.
 else                  /* It has an invalid status */
   do:
       pResponse:fault("3007", "Status '" + policy.stat + "'" {&msg-add} "Policy " + STRING(piPolicyID)). /* Error */
       pResponse:setParameter("EffDate", "").
       pResponse:setParameter("LiabilityAmount", 0).
       pResponse:setParameter("GrossPremium", 0).
       pResponse:setParameter("FormID", "").
       pResponse:setParameter("StateID", "").
   end.

 if policy.stat = "P" or policy.stat = "I" then
 do:
    pResponse:setParameter("FileNumber", policy.fileNumber).
    pResponse:setParameter("StatCode", policy.statCode).
    pResponse:setParameter("NetPremium", policy.netPremium).
    pResponse:setParameter("Retention", policy.retention).
    pResponse:setParameter("CountyID", policy.countyID).
    pResponse:setParameter("Residential", policy.residential).
    pResponse:setParameter("InvoiceDate", policy.invoiceDate).
 end.
 else
 do:
    pResponse:setParameter("FileNumber", "").
    pResponse:setParameter("StatCode", "").
    pResponse:setParameter("NetPremium", 0).
    pResponse:setParameter("Retention", 0).
    pResponse:setParameter("CountyID", "").
    pResponse:setParameter("Residential", no).
    pResponse:setParameter("InvoiceDate", "?").
 end.
 
 std-lo = true.
end.  /* for each policy */

if not std-lo
 then pResponse:fault("3067", "Policy" {&msg-add} "ID" {&msg-add} string(piPolicyID)).
 else
if not pResponse:isFault()
 then pResponse:success("2001", "Agent" {&msg-add} "Policy").
 
