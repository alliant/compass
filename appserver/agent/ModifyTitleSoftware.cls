/*------------------------------------------------------------------------
@name modifytitlesoftware.p
@action titleSoftwareModify
@description  Modify the existing title software.
@param softwareD;int
@throws 3001;Invalid softwareID
@throws 3066;Title software does not exist
@throws 3000;Title software modify failed 
@returns Success;2000
@author Sachin Chaturvedi
@Created 07.15.2022
@Modified :
Date        Name          Comments   
03/14/23    SRK           Added Active Field   
05/29/24    SRK   Added validation for phone and email
----------------------------------------------------------------------*/

class agent.ModifyTitleSoftware inherits framework.ActionBase:
    
  {tt/titlesoftware.i &tableAlias=ttTitlesoftware }
        
  constructor ModifyTitleSoftware ():
    super().
  end constructor.

  destructor public ModifyTitleSoftware ( ):
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
            
    {lib/std-def.i}
        
    /* Variables */
    define variable lSuccess        as logical   no-undo.
    define variable piSoftwareID    as integer   no-undo.
    define variable plReturnRecord  as logical   no-undo.       
    define variable dsHandle        as handle    no-undo.
    define variable cPhoneNumber   as character  no-undo.
    define variable lError         as logical    no-undo.
    define variable cerrmsg        as character  no-undo.
                   
     /* dataset creation */
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttTitleSoftware:handle). 
        
    /* parameter get */
    pRequest:getParameter("softwareID", output piSoftwareID).
    pRequest:getParameter("returnRecord", output plReturnRecord).

    if piSoftwareID = ?
     then
      piSoftwareID = 0.

    if PlReturnRecord = ?
     then
      plReturnRecord = false.
        
    /* get Dataset */
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.
    
    
    /* validation when no parameter is supplied */
    if (piSoftwareID = 0  or piSoftwareID= ?)
     then 
      do:
        pResponse:fault("3001", "softwareID").
        return.
      end.

    /* validate softwareID */
    if not can-find(first titlesoftware where titlesoftware.softwareID = piSoftwareID ) 
    then 
     do:
       pResponse:fault("3066", "Title Software").
       return.
     end.
      
    TRX-BLOCK:
    for first titlesoftware exclusive-lock
      where titlesoftware.softwareID = piSoftwareID TRANSACTION
      on error undo TRX-BLOCK, leave TRX-BLOCK:

      /* updating titlesoftware record*/
      for first ttTitleSoftware no-lock
        where ttTitleSoftware.softwareID = titlesoftware.softwareID:

        if ttTitleSoftware.name = "" or
           ttTitleSoftware.name = ?
         then
          do:  
            pResponse:fault("3005", "Software cannot be blank").
            return.
          end. 
     
        if can-find(first titlesoftware where 
          (titlesoftware.name = ttTitlesoftware.name or
          replace(titlesoftware.name," ","") = replace(ttTitlesoftware.name," ","")) and
          titlesoftware.softwareID <> ttTitlesoftware.softwareID)
         then 
          do:
            pResponse:fault("3005", "Software with same name already exists.").
            return.
          end.

        if (ttTitleSoftware.contactEmail <> "" and ttTitleSoftware.contactEmail <> ?)
             and not validEmailAddr(ttTitleSoftware.contactEmail)
          then
           do:
             pResponse:fault("3005", "Invalid Email Address.").
             return.
           end.
          
        if (ttTitleSoftware.contactPhone <> "" and ttTitleSoftware.contactPhone <> ?)
         then
          do:
            run util/validatecontactnumber-p.p(input ttTitleSoftware.contactPhone,output cPhoneNumber,output lError,output cerrmsg).
            if lError 
             then
              do: 
                pResponse:fault("3005", "Invalid Contact Number.").  
                return.
              end.
          end.

       if ttTitleSoftware.vendorID <> "" and 
          not can-find(first vendor where vendor.vendorID  = ttTitleSoftware.vendorID) 
        then
         do:
           pResponse:fault("3067", "Vendor" {&msg-add} "ID" {&msg-add} ttTitleSoftware.vendorID).
           release titlesoftware.
           return.
         end. 

       assign
           titlesoftware.name             = ttTitleSoftware.name
           titlesoftware.vendorID         = ttTitleSoftware.vendorID
           titlesoftware.addr             = ttTitleSoftware.addr
           titlesoftware.city             = ttTitleSoftware.city
           titlesoftware.state            = ttTitleSoftware.state
           titlesoftware.zip              = ttTitleSoftware.zip
           titlesoftware.url              = ttTitleSoftware.url
           titlesoftware.contactname      = ttTitleSoftware.contactname
           titlesoftware.contactEmail     = ttTitleSoftware.contactEmail
           titlesoftware.contactPhone     = cPhoneNumber
           titlesoftware.progExec         = ttTitleSoftware.progExec
           titlesoftware.lastModifiedDate = now
           titlesoftware.lastModifiedBy   = pRequest:uid
           titleSoftware.active           = ttTitleSoftware.active
           .
      end. /* for first ttTitleSoftware */
  
     validate titlesoftware.
     release titlesoftware.
   
     lSuccess = true.

    end. /* for first titlesoftware */
       
    if not lSuccess
     then 
      do:
        presponse:fault("3000", "Title Software update").
        return.
      end. /* if not lSuccess */
        
    if plReturnRecord 
     then 
      do:
        
        for first titlesoftware no-lock
          where titlesoftware.softwareID = piSoftwareID:

          empty temp-table ttTitleSoftware.
          create ttTitleSoftware.
          assign
              ttTitleSoftware.softwareID        = titleSoftware.softwareID
              ttTitleSoftware.name              = titleSoftware.name
              ttTitleSoftware.vendorID          = titleSoftware.vendorID
              ttTitleSoftware.addr              = titleSoftware.addr
              ttTitleSoftware.city              = titleSoftware.city
              ttTitleSoftware.state             = titleSoftware.state
              ttTitleSoftware.zip               = titleSoftware.zip
              ttTitleSoftware.url               = titleSoftware.url
              ttTitleSoftware.contactname       = titleSoftware.contactname
              ttTitleSoftware.contactEmail      = titleSoftware.contactEmail
              ttTitleSoftware.contactPhone      = titleSoftware.contactPhone
              ttTitleSoftware.progExec          = titleSoftware.progExec
              tttitleSoftware.active            = titleSoftware.active
              .

        end.
    
       setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
     end.  
        
    pResponse:success("2000", "Title Software update"). /* Modify title software was successful. */
                
 end method.

 method private logical  validEmailAddr(input pEmailAddr as character):

    def var tBadEmailChars as char init "/?<>\:*|~"~~~'~`!#$%^&~{~}[];," no-undo.
    def var i as int no-undo.
    def var tEmailOK as log no-undo init yes.

    pEmailAddr = trim(pEmailAddr).

    do i = 1 to length(pEmailAddr):
      if index(tBadEmailChars, substring(pEmailAddr,i,1)) > 0 then
       do:
         tEmailOK = no.
         leave.
       end.
    end.
  
    if tEmailOK then
    if num-entries(pEmailAddr, "@") <> 2 then
     tEmailOK = no.

    if tEmailOK then
    if num-entries(pEmailAddr, ".") < 2 then
     tEmailOK = no.

    if tEmailOK then
    if num-entries(pEmailAddr, " ") > 1 then
     tEmailOK = no.

    if tEmailOK then
    do i = 1 to num-entries(pEmailAddr, "@"):
      if entry(i, pEmailAddr, "@") = "" or entry(i, pEmailAddr, "@") = "?" then
       do:
         tEmailOK = no.
         leave.
       end.
    end.
  
    if tEmailOK then
    do i = 1 to num-entries(pEmailAddr, "."):
     if entry(i, pEmailAddr, ".") = "" or entry(i, pEmailAddr, ".") = "?" then
      do:
       tEmailOK = no.
       leave.
      end.
    end.
  
    RETURN tEmailOK.

   end method.
        
end class.


