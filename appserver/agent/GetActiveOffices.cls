/*----------------------------------------------------------------------
@name GetActiveOffices.cls
@action ActiveOfficesGet
@description Provides either a complete list of all offices or offices in specific states
@param stateIDList;
@returns  Offices
@author Sachin Chaturvedi
@created 04.04.2023
Modified
Date       Name    Description              

----------------------------------------------------------------------*/

class agent.GetActiveOffices inherits framework.ActionBase:

  {lib/callsp-defs.i }
  {tt/office.i &tableAlias="ttoffice"}

  constructor public GetActiveOffices ():
    super().
  end constructor.

  destructor public GetActiveOffices ():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}
    define variable cStateIDList as character no-undo.
    define variable dsHandle     as handle    no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttOffice:handle).

    pRequest:getParameter("StateIDList", output cStateIDList).

    if cStateIDList = 'ALL' or cStateIDList = ? 
     then
      cStateIDList = ''.

    {lib\callsp.i 
        &name=spGetActiveOffices 
        &load-into=ttOffice 
        &params="input cStateIDList"
        &noResponse=true}
 
    if can-find(first ttOffice) 
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2005", string(csp-icount) {&msg-add} "Office").
     
  end method.
  
end class.


