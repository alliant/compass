/* -----------------------------------------------------------------------------
@name sys/getsysnotifications.p
@action SysNotificationsGet
@description fetches the Notifications
@param pdtStartDate;datetime;
@param daEndDate;datetime;
@returns Success;int;2005
@author Gurvindar
@created 05.24.2019
Date        Name       Comments
07/29/2019  Gurvindar  Testing Bug Fix.
----------------------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable pdtStartDate   as datetime   no-undo.
define variable pdtEndDate     as datetime   no-undo.
define variable daEndDate      as date       no-undo.
define variable pcUID          as character  no-undo.
define variable pcEntityType   as character  no-undo.
define variable pcEntityID     as character  no-undo.
define variable pcStat         as character  no-undo.

{lib/std-def.i}
{tt/sysnotification.i &tableAlias="ttsysnotification"}

pRequest:getParameter("StartDate",  output pdtStartDate).
pRequest:getParameter("EndDate",    output daEndDate).
pRequest:getParameter("UID",        output pcUID).
pRequest:getParameter("EntityType", output pcEntityType).
pRequest:getParameter("EntityID",   output pcEntityID).
pRequest:getParameter("Stat",       output pcStat).


if pcEntityType = "" 
 then
  pcEntityType = "ALL".
  
if pcEntityID = "" 
 then
  pcEntityID = "ALL".

if pcStat = "" 
 then
  pcStat = "ALL".
  
if pcUID = "" 
 then
  pcUID = "ALL".  

if daEndDate = ? 
 then
  do:
    pResponse:fault("3005" , "End Date cannot be blank").
    return.
  end.

if daEndDate > today 
 then
  do:
    pResponse:fault("3005" , "End Date cannot be in future").
    return.
  end.

/* Get the records till the midnight of the provided end date. */

assign pdtEndDate = datetime(string(daEndDate) + " " + "23:59:59.999").

/* set StartDate with the date of first record created in the table.
   This check is/willbe used in purge Utility for sysNotification records.  */
if pdtStartDate = ? 
 then
  for first sysnotification no-lock: 
    pdtStartDate = sysnotification.createDate.
  end.

/* Checking date range  */
if interval(pdtEndDate,pdtStartDate,"months") > 6
 then
  do:
     pResponse:fault("3005" , "Start Date is " + string(date(pdtStartDate)) + ", Input Date Range cannot be more than Six months.").
     return.
  end.

std-in = 0.
for each sysnotification no-lock
  where sysnotification.createDate >= pdtStartDate
  and   sysnotification.createDate <= pdtEndDate
  and   sysnotification.entityType = (if pcEntityType = "ALL" then sysnotification.entityType else pcEntityType)
  and   sysnotification.entityID   = (if pcEntityID   = "All" then sysnotification.entityID   else pcEntityID)
  and   sysnotification.UID        = (if pcUID        = "All" then sysnotification.UID        else pcUID)
  and   sysnotification.Stat       = (if pcStat       = "ALL" then sysnotification.Stat       else pcStat):

  create ttsysnotification.
  buffer-copy sysnotification to ttsysnotification.
  std-in = std-in + 1.

end.

if std-in > 0
 then
  pResponse:setParameter("SysNotification", table ttsysnotification).
  
 pResponse:success("2005", string(std-in) {&msg-add} "System Notifications").



