/*------------------------------------------------------------------------
@name sys/queryusersbymodule.p
@action usersByModuleQuery
@description Returns syslog dataset based on module

@returns The syslog records 
@success 2005;char;The count of the records returned

@author Rahul Sharma
@version 1.0
@created 03.28.19
Date          Name      Description
01-25-2022    Shefali   Task:89648 Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

{lib/std-def.i}
{lib/callsp-defs.i}
{tt/syslog.i &tableAlias="ttsyslog"}

define  variable  cAppModule   as character no-undo.
define  variable  dtStartDate  as date      no-undo.
define  variable  dtEndDate    as date      no-undo.

pRequest:getParameter("AppModule", output cAppModule).
pRequest:getParameter("StartDate", output dtStartDate).
pRequest:getParameter("EndDate",   output dtEndDate).

/* If no Start date is provided then assign the start date as the beginning time of today's date
  otherwise consider the provided start date. */
if dtStartDate = ? 
 then 
  assign dtStartDate = today.

/* Checking date range  */
if interval(dtEndDate,dtStartDate,"years") > 0
 then
  do:
     pResponse:fault("3005" , "Input Date Range cannot be more than a year").
     return.
  end.

{lib\callsp.i &name=spGetUsersByModule &load-into=ttsyslog &params="input cAppModule, input dtStartDate, input dtEndDate"}

pResponse:success("2005", string(csp-icount) {&msg-add} "System Logs").


