/*------------------------------------------------------------------------
@name SystemLockNew
@description Creates a new system lock using the specified inputs
@param Type;char;
@param ID;char;
@param Seq;int;Sub-key (optional)
@returns Success;log;Success indicator
@throws 10400;Bad Request
@author D.Sinclair
@version 1.0 5.1.2014
@notes
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

def var tType as char no-undo.
def var tID as char no-undo.
def var tSeq as INT no-undo.

def var tCreated as logical init false no-undo.
{lib/std-def.i}


pRequest:getParameter("Type", OUTPUT tType).
IF tType = ""
 then pResponse:fault("3051", "Type" {&msg-add} "").

pRequest:getParameter("ID", OUTPUT tID).
IF tID = ""
 then pResponse:fault("3051", "ID" {&msg-add} "").

pRequest:getParameter("Seq", OUTPUT tSeq).
IF tSeq = ? 
 THEN tSeq = 0.

IF CAN-FIND(first syslock
              where syslock.entityType = tType
                and syslock.entityID = tID
                AND syslock.entitySeq = tSeq)
 THEN pResponse:fault("3004", tID).

if pResponse:isFault()
 then return.

RUN util/newsyslock.p (tType, 
                       tID, 
                       tSeq, 
                       pRequest:uid,
                       OUTPUT std-lo).

IF NOT std-lo 
 then 
  do: pResponse:fault("3000", "Lock").
      RETURN.
  END.

pResponse:success("2000", "Lock").
pResponse:createEvent("SystemLock", tType {&id-add} tID {&id-add} STRING(tSeq)).
