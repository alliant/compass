/*------------------------------------------------------------------------
@file serverlogemail.p
@action emailServerLog
@description Send error logs on mail

@success 2005;Email sent successful

@author Rahul Sharma
@version 1.0
@created 03/27/2020
Modification:
Date         Name    Description
03/09/21     SA      Remove hardcoded reference of temp folder code.
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

{lib/std-def.i}
                 
define variable cCommandLine      as character  no-undo.
define variable cSessionParams    as character  no-undo.
define variable cLogfileParam     as character  no-undo.
define variable cLogfilepath      as character  no-undo.
define variable cLogfileName      as character  no-undo.
define variable cTempFileDir      as character  no-undo.
define variable OutputFilePath    as character  no-undo.
define variable cErrorFilePath    as character  no-undo.
define variable cLastReadDateFile as character  no-undo.
define variable cFromTimestamp    as character  no-undo.
define variable cToTimestamp      as character  no-undo.
define variable cScriptFile       as character  no-undo.
define variable cLines            as character  no-undo.
define variable cToAddress        as character  no-undo.
define variable lErrorExist       as logical    no-undo.
define variable cExcludeList      as character  no-undo.
define variable cAppEnv           as character  no-undo.

/* Declaring function */  
FUNCTION convertTimeStamp RETURNS CHARACTER
  ( cTimestamp as character )  FORWARD.

/* Fetching Email address from INI file */ 
publish "GetSystemParameter" ("AlertsTo", output cToAddress).

if cToAddress = "" 
 then 
  do:
    cToAddress = "alerts@alliantnational.com".
    
    run sendMail (input "System email not setup",                                                /* Subject */
                  input "System AlertsTo email not setup. Please contact system administrator.", /* Body */
                  input "").
                  
    pResponse:fault("3005", "System AlertsTo email not setup. Please contact system administrator.").
    return.
  end.

/* Note: Used this for testing purpose. This is used to get all the startup parameters and value of 
   Logfile parameter of a session. Uncomment this portion to get all the available startup parameters.
/* Get list of all startup parameters */
cSessionParams = session:startup-parameters.
pRequest:getParameter("Logfile", output cLogfileParam).

run sendMail (input "Session parameters",                                                                              /* Subject */
              input "Session startup parameters: " + cSessionParams + chr(10) + "Parameter Logfile: " + cLogfileParam, /* Body */
              input ""). 
*/ 
  
cScriptFile = search("scripts/serverlogparse.ps1").

/* Return error when powershell script not foound */
if cScriptFile = ? 
 then
  do:
    run sendMail (input "Script file not found",                                                    /* Subject */
                  input "Script file serverlogparse.ps1 doesn't exist, please check file location", /* Body */
                  input ""). 
    pResponse:fault("3005", "Script file serverlogparse.ps1 doesn't found, please check file location or add new location in propath").
    return.
  end.  

/* Get list of all startup parameters */
cSessionParams = session:startup-parameters.

/* Get name and full path of server log file from app framework */
do std-in = 1 to num-entries(cSessionParams):
  std-ch = entry(std-in,cSessionParams).
  if index(std-ch,"-logfile") > 0 
   then
    do:
      assign
          cLogfilepath = left-trim(std-ch,"-logfile ")
          cLogfileName = entry(num-entries(std-ch,"\"),std-ch,"\")
          .
      leave.
    end.
end.

if cLogfilepath = ""
 then
  do:
    pRequest:getParameter("Logfile", output cLogfileParam).
    if cLogfileParam = "" 
     then
      do:
        run sendMail (input "Logfile parameter not set",                                                                           /* Subject */
                      input "Logfile parameter not set when running program from scheduler. Please contact system administrator.", /* Body */
                      input "").
        pResponse:fault("3005", "Logfile parameter not set when running program from scheduler.").
        return.
      end.              
  
    /* Get name and full path of server log file */
    run getLogFileName.
  end.
  
if search(cLogfilepath) = ?
 then
  do:
    run sendMail (input "Server logfile not found",                                                                                                           /* Subject */
                  input "Server logfile doesn't found. Either file doesn't exist or it may be moved to other location. Please contact system administrator.", /* Body */
                  input ""). 
    pResponse:fault("3005", "Server logfile doesn't found.").
    return.
  end. 
                  
/* Getting list of words which are not to be parsed through powershell script.
   Note: Exclude list must be piped separated or delimiter must be "|" */
for first sysprop no-lock
    where sysprop.appCode     = 'SYS'
      and sysprop.objAction   = 'EmailServerLog'
      and sysprop.objID       = "0"
      and sysprop.objProperty = 'ExcludeList':
  
  cExcludeList = sysprop.objDesc.
end.

/* GetSystemParameter provides the temporary directory location  */
publish "GetSystemParameter" ("TempFolder", output cTempFileDir).

if cTempFileDir = ? or cTempFileDir = ""
 then
  cTempFileDir = os-getenv("TEMP").
  
if cTempFileDir = ? or cTempFileDir = ""
 then  
  cTempFileDir = os-getenv("TMP").  
  
if cTempFileDir = ? or cTempFileDir = ""  
 then  
  cTempFileDir = "d:\temp\".  
  
/* Set physical path for the files which are used */ 
assign
    cAppEnv           = left-trim(entry(1,cLogfileName,"."), "ws")
    cErrorFilePath    = cTempFileDir + "serverLogError-" + cAppEnv + ".txt"
    OutputFilePath    = cTempFileDir + "serverLogParse-" + cAppEnv + ".txt"
    cLastReadDateFile = cTempFileDir + "serverLogDate-"  + cAppEnv + ".txt"
    .

/* Delete already existing old error file */    
if search(cErrorFilePath) <> ?
 then
  os-delete value(cErrorFilePath).
       
/* Fetching timestamp information from a file when last time script runs */
run readTimeStampFile (output cFromTimestamp, output cLines).

/* Scan after last read datetime */
cCommandLine = 'powershell -executionpolicy bypass -file ' + 
               cScriptFile + ' ' + cLogfilepath + ' ' + 
               OutputFilePath + ' ' + cErrorFilePath + ' ' +
               cLastReadDateFile + ' "' + cFromTimestamp + '" "' + cExcludeList + '"'.

/* Run powershell command to read log file */
os-command silent value(cCommandLine).

/* Report error if some error occurs while execution of os-command */
file-info:file-name = cErrorFilePath.
if file-info:file-size > 0 
 then
  do:
    lErrorExist = true.
    pResponse:fault("3005", "Some errors encountered while reading logfile " + cLogfileName + ". Error file is stored at location " + cErrorFilePath).
  end.
    
file-info:file-name = OutputFilePath.

/* Send mail when log file contains errors */
if file-info:full-pathname <> ?
 then
  do:  
    if file-info:file-size > 0
     then
      do:
        /* Fetching last read timestamp information */
        run readTimeStampFile (output cToTimestamp, output cLines).
        
        /* Converting timestamp into valid progress datetime */
        assign
            cFromTimestamp = convertTimeStamp(cFromTimestamp)
            cToTimestamp   = convertTimeStamp(cToTimestamp)
            .
        
        run sendMail (input "Error(s) in server log file " + cLogfileName,                                                                                             /* Subject */
                      input cLines + " lines have error in server log file from " + cFromTimestamp + " to " + cToTimestamp + "." + chr(10) + "Please see attachment.", /* Body */
                      input OutputFilePath). 
      end.                           
    os-delete value(OutputFilePath). 
  end.
 /* When no error encountered during log file reading */ 
 else if not lErrorExist
  then
   do:
     find first sysapplog no-lock
       where sysapplog.appCode    = "SYS" 
         and sysapplog.entityType = "ServerLog" 
         and sysapplog.entityID   = "1"  
         and sysapplog.entitySeq  = 0 no-error.
     if not available sysapplog
      then
       do:
         create sysapplog.
         assign
             sysapplog.appCode    = "SYS"
             sysapplog.entityType = "ServerLog"
             sysapplog.entityID   = "1"
             sysapplog.entitySeq  = 0                       
             sysapplog.objDesc    = "Storing last date when mail sent without any errors."
             sysapplog.objAction  = "EmailLogs" /* Mandatory */
             sysapplog.objID      = ""          /* Mandatory */
             sysapplog.objRef     = ""          /* Mandatory */
             sysapplog.logDate    = datetime(today,0)
             sysapplog.uid        = pRequest:uid
             .
        
         /* Fetching last read timestamp information */
         run readTimeStampFile (output cToTimestamp, output cLines).
     
         /* Converting timestamp into valid progress datetime */
         assign
             cFromTimestamp = convertTimeStamp(cFromTimestamp)
             cToTimestamp   = convertTimeStamp(cToTimestamp)
             .
     
         run sendMail (input "No new error reported in server log file " + cLogfileName,                                   /* Subject */
                       input "There is no error in server log file from " + cFromTimestamp + " to " + cToTimestamp + ".",  /* Body */
                       input ""). 
                       
         release sysapplog. 
       end.
      /* When no error encountered during log file reading on fresh day then send email 
         atleast once, after that no such mail should be send on the same day */ 
      else if date(sysapplog.logDate) < today
       then
        do:
          /* Fetching last read timestamp information */
          run readTimeStampFile (output cToTimestamp, output cLines).
     
          /* Converting timestamp into valid progress datetime */
          assign
              cFromTimestamp = convertTimeStamp(cFromTimestamp)
              cToTimestamp   = convertTimeStamp(cToTimestamp)
              .
     
          run sendMail (input "No new error reported in server log file " + cLogfileName,                                   /* Subject */
                        input "There is no error in server log file from " + cFromTimestamp + " to " + cToTimestamp + ".",  /* Body */
                        input "").                                                                                          /* Attachment */  
          
          buffer sysapplog:handle:find-current (exclusive-lock,no-wait).                
          assign
              sysapplog.logDate = datetime(today,0)
              sysapplog.uid     = pRequest:uid
              .       
          release sysapplog. 
        end.
      else
       pResponse:success("2019", pRequest:actionid + " was successful, no error was found, and no email was sent"). 
   end. /*  else if not lErrorExist */   

/************************Function*****************************/

FUNCTION convertTimeStamp RETURNS CHARACTER
  ( cTimestamp as character ) :
/*------------------------------------------------------------------------------
  Purpose: Return timestamp into valid progress datetime format  
    Notes:  
------------------------------------------------------------------------------*/
  define variable dtTimeStamp as datetime  no-undo.
  define variable cDateFormat as character no-undo.
  
  cTimestamp = replace(cTimestamp,"@"," ").
  
  /* Storing session date format so that it can restored later */
  cDateFormat = session:date-format.
  
  /* Changing date format to 'ymd' so that input timestamp
     can be converted into valid datetime */
  session:date-format = 'ymd'.
   
  dtTimeStamp = datetime(cTimestamp) no-error.
  
  if error-status:error
   then
    do:
      /* changing to earlier date format */
      session:date-format = cDateFormat.
      return cTimestamp.
    end.
  
  /* changing to earlier date format */
  session:date-format = cDateFormat.
   
  return substring(string(dtTimeStamp),1,19). /* Function return value. */

END FUNCTION.


/***************************Procedures******************************/

Procedure readTimeStampFile:
  define output parameter opcDateTimeStamp as character no-undo.
  define output parameter opcLines         as character no-undo.
  
  if search(cLastReadDateFile) <> ?
   then
    do:
      input from value(cLastReadDateFile).
      import delimiter "," opcDateTimeStamp opcLines.
      input close.
    end.
  
End Procedure.

Procedure sendMail:
  define input parameter ipcSubject    as character no-undo.
  define input parameter ipcBody       as character no-undo.
  define input parameter ipcAttachment as character no-undo.
  
  /* Sending email */
  if ipcAttachment > ""
   then  
    run util/attachmail.p ("no-reply@alliantnational.com", /* from loggedIN user*/
                           cToAddress,                     /* To */
                           "",                             /* CC */
                           ipcSubject,                     /* Subject */
                           ipcBody,                        /* Body */
                           ipcAttachment).                 /* Attachment */
   else                        
    /* Sending email with no attachment */
    run util/simplemail.p ("no-reply@alliantnational.com", /* from loggedIN user*/
                           cToAddress,                     /* To */
                           ipcSubject,                     /* Subject */
                           ipcBody).                       /* Body */
  
  pResponse:success("2019", "Mail sent successfully to " + cToAddress). 
End Procedure.

Procedure getLogFileName:
  /* Local Variables */
  define variable cLogReadDir     as character no-undo.
  define variable cFileName       as character no-undo.
  define variable cFullPath       as character no-undo.
  define variable cFileType       as character no-undo.
  define variable cFileExtn       as character no-undo.
  define variable cAppEnvFileName as character no-undo.
  define variable iTempFileSeq    as integer   no-undo.
  define variable iFileSeq        as integer   no-undo.
  
  assign
      cLogReadDir     = right-trim(cLogfileParam, entry(num-entries(cLogfileParam,"\"), cLogfileParam, "\")) /* cLogfileParam: Logfile parameter value */
      cAppEnvFileName = entry(num-entries(cLogfileParam,"\"), cLogfileParam, "\")
      no-error.

  input from os-dir(cLogReadDir).
  repeat:
    import cFileName cFullPath cFileType. 
        
    assign
        iTempFileSeq = integer(entry(3,cFileName,"."))
        cFileExtn    = substring(cFileName,length(cFileName) - 3) 
        no-error.
    
    if cFileType = "F"                  and 
       cFileExtn = ".log"               and
       cFileName begins cAppEnvFileName and       
       iTempFileSeq > iFileSeq 
     then        
      assign
          cLogfileName = cFileName  /* cLogfileName:Global variable */
          cLogfilepath = cFullPath  /* cLogfilepath:Global variable */
          iFileSeq     = iTempFileSeq
          no-error.
          
  end. /* repeat end */
  input close. 
End Procedure.
