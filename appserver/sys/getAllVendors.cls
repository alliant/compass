/*------------------------------------------------------------------------
@name  getAllVendors.cls
@action allVendorsGet
@description  Get vendors.
@returns Success;2005;Vendors(s) were returned
@author Sagar K
@version 1.0
@created 09.04.2023
@Modified :
Date             Name          Comments   
----------------------------------------------------------------------*/

class sys.getAllVendors inherits framework.ActionBase:

  constructor public getAllVendors ():
   super().
  end constructor.

  destructor public getAllVendors ():

  end destructor.

  {lib/callsp-defs.i}
  {tt/apVendor.i &tableAlias="ttapvendor"}

  method public override void act (input pRequest as framework.IRequestable,
                                  input pResponse as framework.IRespondable):

    define variable dsHandle as handle    no-undo.
    define variable pcStatus as character no-undo.

    {lib/std-def.i}

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttapvendor:handle).

    pRequest:getParameter("Status", output pcStatus).

    {lib\callsp.i &name=spGetVendors &load-into=ttapvendor &params="input '', input pcStatus" &noResponse=true}

    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.
      end.
      
    if can-find(first ttapvendor) 
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2005", string(csp-icount) {&msg-add} "Vendors").

  end method.

end class.
