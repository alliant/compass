/*------------------------------------------------------------------------
@name TestDoRun.cls
@action sys.doRunTest 

@returns 
@author 
@version 1.0
@created 
Modification:
Date          name      Description

----------------------------------------------------------------------*/

class sys.TestDoRun inherits framework.ActionBase:

  constructor TestDoRun ():
    super().
  end constructor.

  destructor public TestDoRun ():
  end destructor. 

  {tt/agent.i &tableAlias=ttagent}

  define temp-table DoRunPayload no-undo
      field ProgramName as character
      field InputType as character
      field GetMessage as character.

  
  define variable cAgentID  as character   no-undo.
  define variable cDocTYpe  as character   no-undo.
  define variable dsHandle  as handle      no-undo.
  define variable std-ch    as character   no-undo.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    pRequest:getParameter("AgentID",  output cAgentID).
    pRequest:getParameter("Doctype",  output cDocTYpe).

    if not can-find(first agent where agent.agentID = cAgentID)
     then
      do:                           
        pResponse:fault("3066", "Agent").
        return.
      end. 

    /* dataset creation */
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer doRunPayload:handle).

    /* Getting temptable */
    empty temp-table DoRunPayload.

    /*dataset get*/
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > ''
     then
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.

    for first doRunPayload:
      message doRunPayload.getMessage.
    end.

    for first agent where agentid = cAgentID no-lock:
        create ttagent.
        buffer-copy agent to ttagent.
    end.

    pResponse:success("2000", "TestDoRun").

  end method.

method public override logical distribute(input pRequest as framework.IRequestable):
  define variable cEmailAddrList as character no-undo.
  define variable cPrinterAddr   as character no-undo.
  define variable cStorageRoot   as character no-undo.
  define variable cSaveDir       as character no-undo.
  define variable std-lo         as logical   no-undo.

  /*set the distribution values here using sysdest*/
  for each sysdest no-lock where sysdest.action = pRequest:actionID and sysdest.entityID = "":

    if sysdest.destType = "E" then
      assign cEmailAddrList = sysdest.destName.

    else if sysdest.destType = "P" then
      assign cPrinterAddr = sysdest.destName.

    else if sysdest.destType = "S" then
      assign cStorageRoot = sysdest.destName.
    else
      assign cSaveDir = sysdest.destName.

  end.

  for first sysaction no-lock where sysaction.action = pRequest:actionID:

    if cEmailAddrList <> "" then
      std-lo = super:emailContent(cEmailAddrList,sysaction.subject, sysaction.htmlEmail, output std-ch).
  end.
  return std-lo.
end.

method public override void queue(input pRequest  as framework.IRequestable,
                                  input pResponse as framework.IRespondable):
  
  define variable cAllDestination as character no-undo.

  pRequest:getParameter("AgentID",  output cAgentID).
  pRequest:getParameter("Doctype",  output cDocTYpe).

  for each sysdest no-lock where sysdest.action = pRequest:actionID:
    cAllDestination = cAllDestination + sysdest.destType + "=" + sysdest.destName + "^".    
  end.

  cAllDestination = trim(cAllDestination,"^").

  run util/queueaction-p.p("doRunTest", pRequest:uid, "AgentID=" + cAgentID + "^DocType=" + cDocTYpe, cAllDestination ,"SYS").
end.
  
method public override char render(input pFormat as char, input pTemplate as char):
    define variable cUrl          as character no-undo.
    define variable pMsg          as character no-undo.
    define variable pResponseFile as character no-undo.
    define variable pSuccess      as logical   no-undo.
    define variable cFileName     as character no-undo.

    cFileName = "d:\temp\File" + cAgentID + "_"  + replace(string(today,"99/99/9999"),"/","-") + "-" + trim(replace(string(time,"HH:MM:SS"),":","-")," ") + "." + cDocTYpe.

    output to value(cFileName).
    for first ttAgent:
      export delimiter "," ttAgent.
    end.
    
    output close.

    tContentFile = cFileName.
 
    return tContentFile.
  end.
      
end class.
