/* sys/getsysactions.p
   
@name SystemActionsGet
@description Provides either all System Actions or a specific System Action
@param ActionID;char;Action identifier (optional)
@returns Success;int;2005
@returns SystemAction;complex
@author D.Sinclair
@version 1.1 7.2.2014
Date          Name      Description
03/18/2019    Rahul     Modified to add stored procedure and parameters
01-25-2022    Shefali   Task:89648 Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"
 */

define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

/* stored procedure variables */
define variable pcActionType   as character  no-undo.
define variable pcAction       as character  no-undo.
define variable pdtStartDate   as datetime   no-undo.
define variable pdtEndDate     as datetime   no-undo.

define variable daEndDate      as date       no-undo.

{lib/std-def.i}
{lib/callsp-defs.i }
{tt/sysAction.i &tableAlias="ttSysAction"}

pRequest:getParameter("ActionType", output pcActionType). /* (A)Active, (I)Inactive , (B)Both actions */
pRequest:getParameter("Action",     output pcAction).
pRequest:getParameter("StartDate",  output pdtStartDate).
pRequest:getParameter("EndDate",    output daEndDate).

/* Get the records till the midnight of the provided end date. */
assign pdtEndDate   = datetime(string(daEndDate) + " " + "23:59:59.999").

/* Checking date range  */
if interval(pdtEndDate,pdtStartDate,"years") > 0
 then
  do:
     pResponse:fault("3005" , "Input Date Range cannot be more than a year").
     return.
  end.
{lib/callsp.i  &name=spGetSysActions &load-into=ttSysAction &params="input pcActionType, input pcAction, input pdtStartDate, input pdtEndDate"}

pResponse:success("2005", string(csp-icount) {&msg-add} "System Actions").

