/*---------------------------------------------------------------------------------
@name sys/newsyscode.p   
@action systemCodeNew
@description Creates a new system code using the specified inputs
@param  syscode table
@returns Success;log;Success indicator
@throws 3051;CodeType/Code may not be blank
@throws 3010;System Code that has [<System code type>:<System code ID>] exists
@throws 3000;Create failed
@author D.Sinclair
@version 1.0 5.1.2014
@notes
@Modification
Date           Name             Description
01/03/2018     Gurvindar        Added fields type and issecure, 
                                modified to return table syscode
02/20/2019     Vikas            Modified logic to retrieve input temp-table instead of
                                input variables from XML using get parameter
02/22/2019     Anubha           Modified variable names in createEvent for logs
03/07/2019     Anubha           Modified Action Name                                 
--------------------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable cCodeType as character no-undo.
define variable cCode     as character no-undo.

{lib/std-def.i}
{tt/syscode.i &tablealias="ttSysCode"}


/* Getting temptable */
empty temp-table ttSysCode.
pRequest:getParameter("SysCode", input-output table ttSysCode).

for first ttSysCode:

  if ttSysCode.codeType = ""
   then
    pResponse:fault("3051", "CodeType" {&msg-add} "blank").

  if ttSysCode.code = ""
   then
    pResponse:fault("3051", "Code" {&msg-add} "blank").

  if (not pResponse:isFault()) 
     and can-find(first syscode where syscode.codeType = ttSysCode.codeType
                                  and syscode.code     = ttSysCode.code)
   then
    pResponse:fault("3010", "System Code" {&msg-add} ttSysCode.codeType {&msg-add} ttSysCode.code).

end.

if pResponse:isFault()
 then 
  return.

assign std-lo = false.
TRX-BLK:
do transaction 
    on error undo TRX-BLK, leave TRX-BLK:
  for first ttSysCode:
    create syscode.
    buffer-copy ttSysCode to syscode.
    
    validate syscode.
    
    assign
      cCodeType    = syscode.codeType   
      cCode        = syscode.code
      .
    
    release syscode.
    assign std-lo = true.
  end.  
end.

if not std-lo
 then
  do:
     pResponse:fault("3000", "Create").
     return.
  end.

pResponse:success("2002", "System Code").
pResponse:createEvent("SystemCode", cCodeType {&id-add} cCode).
