/*----------------------------------------------------------------------
@name GetActiveUsers.cls
@action ActiveUsersGet
@description Get the all active users
@param ActionType;
@returns  SysUsers
@author Spandana
@created 04.05.2023
Modified
Date       Name    Description              

----------------------------------------------------------------------*/

class sys.GetActiveUsers inherits framework.ActionBase:

  {lib/callsp-defs.i }
  {tt/sysuser.i &getActiveUsers=true}

  constructor public GetActiveUsers ():
    super().
  end constructor.

  destructor public GetActiveUsers ():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}
    define variable cActionType  as character no-undo.
    define variable dsHandle     as handle    no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer sysuser:handle).

    pRequest:getParameter("ActionType", output cActionType).

    {lib\callsp.i 
        &name=spGetActiveUsers
        &load-into=sysuser 
        &params="input cActionType"
        &noResponse=true}
 
    if can-find(first sysuser)
     then
       setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2005", string(csp-icount) {&msg-add} "SysUsers").
     
  end method.
  
end class.
