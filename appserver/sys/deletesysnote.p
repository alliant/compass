/*------------------------------------------------------------------------
@name   sys/deletesysnote.p
@action systemNoteDelete
@description Deleteing an existing system Note
@param  sysNoteID;integer
@throws 3066;System Note does not exist
@throws 3005;System Note cannot be blank
@returns
@event
@author  Rahul Sharma
@created 6/21/2019 
@Modification:
----------------------------------------------------------------------*/ 

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable iSysNoteID  as integer no-undo.
define variable lFound      as logical no-undo.

{lib/std-def.i}

pRequest:getParameter("sysNoteID", iSysNoteID).

if iSysNoteID = ? 
then
 pResponse:fault("3005", "sysNoteID cannot be blank.").

if not can-find(first sysnote where sysnote.sysNoteID = iSysNoteID) 
 then
  pResponse:fault("3066", "sysNote"). 

if pResponse:isFault()
 then return.

assign
    lFound = false
    std-lo = false
    .

SysNote-BLOCK:
for first sysnote exclusive-lock
  where sysnote.sysNoteID = iSysNoteID transaction
  on error undo SysNote-BLOCK, leave SysNote-BLOCK: 

  lFound = true.
  delete sysnote.
  release sysnote.
  std-lo = true.

end.

/* Validate if sysnote record is locked by another user */
if not lFound
 then
  pResponse:fault("3004", "System Note").
 else if not std-lo 
  then
   pResponse:fault("3000", "Delete").

if pResponse:isFault() 
 then 
  return.

pResponse:success("2008", "System Note").
pResponse:deleteEvent("SystemNote", string(iSysNoteId)).
