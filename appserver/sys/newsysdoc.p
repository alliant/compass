&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* sys/newsysdoc.p
    @name SystemDocumentNew
    @description Creates a new system document using the specified inputs
    @param Type;char;Entity type (Batch,Agent,State,etc.)
    @param ID;char;
    @param Seq;int;Sub-key (optional)
    @returns Success;log;Success indicator
    @throws 10400;Bad Request
    @author D.Sinclair
    @version 1.0 5.1.2014
    @notes AC 09/27/17 - Added entity Finding
           RS 06/14/18 - Added entity Qualification
           AC 06/21/18 - Added entity rateState for state rate Manuals
                         for RCM application.
           AG 10/19/20 - Added entity QAR 
 */

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

def var tType as char no-undo.
def var tID as char no-undo.
def var tSeq as INT no-undo.
def var tObjType as char no-undo init "".
def var tObjID as char no-undo init "".
def var tObjAttr as char no-undo init "".

{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-AddActionDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD AddActionDocument Procedure 
FUNCTION AddActionDocument RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddAgentDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD AddAgentDocument Procedure 
FUNCTION AddAgentDocument RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddBatchDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD AddBatchDocument Procedure 
FUNCTION AddBatchDocument RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddFindingDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD AddFindingDocument Procedure 
FUNCTION AddFindingDocument RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddInvoiceDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD AddInvoiceDocument Procedure 
FUNCTION AddInvoiceDocument RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddQarDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD AddQarDocument Procedure 
FUNCTION AddQarDocument RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddQualificationDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD AddQualificationDocument Procedure 
FUNCTION AddQualificationDocument RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddRateStateDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD AddRateStateDocument Procedure 
FUNCTION AddRateStateDocument RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddStateDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD AddStateDocument Procedure 
FUNCTION AddStateDocument RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createSysDoc) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createSysDoc Procedure 
FUNCTION createSysDoc RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */


pRequest:getParameter("Type", OUTPUT tType).
IF tType = ""
 then pResponse:fault("3051", "Type" {&msg-add} "").

pRequest:getParameter("ID", OUTPUT tID).
IF tID = ""
 then pResponse:fault("3051", "ID" {&msg-add} "").

pRequest:getParameter("Seq", OUTPUT tSeq).
IF tSeq = ? 
 THEN tSeq = 0.

pRequest:getParameter("ObjID", OUTPUT tObjID).
IF tObjID = ? 
 THEN tObjID = "".

pRequest:getParameter("ObjAttr", OUTPUT tObjAttr).
IF tObjAttr = ? 
 THEN tObjAttr = "".

IF CAN-FIND(first sysdoc
              where sysdoc.entityType = tType
                and sysdoc.entityID = tID
                AND sysdoc.entitySeq = tSeq)
 THEN pResponse:fault("3004", tID).

if pResponse:isFault()
 then return.

std-lo = false.
case tType:
 when "Batch" then std-lo = AddBatchDocument().
 when "Agent" then std-lo = AddAgentDocument().
 when "State" then std-lo = AddStateDocument().
 when "Invoice-AP" or
 when "Invoice-AR" then std-lo = AddInvoiceDocument().
 when "Finding" then std-lo = AddFindingDocument().
 when "Qualification" then std-lo = AddQualificationDocument().
 when "rateState" then std-lo = AddRateStateDocument().
 when "Action" then std-lo = AddActionDocument().
 when "Claims" then std-lo = true.
 when "Qar" then std-lo = AddQarDocument().
 otherwise run util/sysmail.p ("sys/newsysdoc.p: Invalid Type: " + tType, "").
end case.

IF NOT std-lo 
 then 
  do: pResponse:fault("3000", "Add").
      RETURN.
  END.

pResponse:success("2000", "Add").
pResponse:createEvent("SystemDocument", tType {&id-add} tID {&id-add} STRING(tSeq)).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-AddActionDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION AddActionDocument Procedure 
FUNCTION AddActionDocument RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  def var tFound as logical.
  
  std-in = int(tID) no-error.
  if NOT CAN-FIND(FIRST action 
                    WHERE action.actionID = std-in)
   then
    do: pResponse:fault("3066", "Action").
        RETURN false.
    end.
     
  tFound = false.
  std-lo = false.
     
  TRX-BLK:
  for first action exclusive-lock
    where action.actionID = std-in TRANSACTION
      on error undo TRX-BLK, leave TRX-BLK:
   tFound = true.
   std-lo = createSysDoc().
  end.
     
  if not tFound
   then pResponse:fault("3004", "Action").
       
  if not std-lo
   then RETURN false.
     
  pResponse:updateEvent("Action", tID).

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddAgentDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION AddAgentDocument Procedure 
FUNCTION AddAgentDocument RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def var tFound as logical.

 if NOT CAN-FIND(FIRST agent 
                   WHERE agent.agentID = tID)
  then
   do: pResponse:fault("3066", "Agent").
       RETURN false.
   end.
    
 tFound = false.
 std-lo = false.
    
 TRX-BLK:
 for first agent exclusive-lock
   where agent.agentID = tID TRANSACTION
     on error undo TRX-BLK, leave TRX-BLK:
  tFound = true.
  std-lo = createSysDoc().
 end.
    
 if not tFound
  then pResponse:fault("3004", "Agent").
      
 if not std-lo
  then RETURN false.
    
 pResponse:updateEvent("Agent", tID).

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddBatchDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION AddBatchDocument Procedure 
FUNCTION AddBatchDocument RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def var tFound as logical.

 std-in = int(tID) no-error.

 if NOT CAN-FIND(FIRST BATCH 
                   WHERE BATCH.batchID = std-in)
  then
   do: pResponse:fault("3066", "Batch").
       RETURN false.
   end.
    
 tFound = false.
 std-lo = false.
    
 TRX-BLK:
 for first batch exclusive-lock
   where batch.batchID = std-in TRANSACTION
     on error undo TRX-BLK, leave TRX-BLK:
  tFound = true.
  std-lo = createSysDoc().
 end.
    
 if not tFound
  then pResponse:fault("3004", "Batch").
      
 if not std-lo
  then RETURN false.
    
 pResponse:updateEvent("Batch", tID).

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddFindingDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION AddFindingDocument Procedure 
FUNCTION AddFindingDocument RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 def var tFound as logical.

 if NOT CAN-FIND(FIRST finding 
                   WHERE finding.findingID = int(tID))
  then
   do: pResponse:fault("3066", "Finding").
       RETURN false.
   end.
    
 tFound = false.
 std-lo = false.
    
 TRX-BLK:
 for first finding exclusive-lock
   where finding.findingID = int(tID) TRANSACTION
     on error undo TRX-BLK, leave TRX-BLK:
  tFound = true.
  std-lo = createSysDoc().
 end.
    
 if not tFound
  then pResponse:fault("3004", "Finding").
      
 if not std-lo
  then RETURN false.
    
 pResponse:updateEvent("Finding", tID).

 RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddInvoiceDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION AddInvoiceDocument Procedure 
FUNCTION AddInvoiceDocument RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def var tFound as logical no-undo init false.

 std-in = int(tID) no-error.
 /* decide if we have a payable or receivable invoice */
 case entry(2,tType,"-"):
  when "AP" then
   for first apinv no-lock
       where apinv.apinvID = std-in:
       
     tFound = true.
   end.
  when "AR" then
   for first arinv no-lock
       where arinv.arinvID = std-in:
       
     tFound = true.
   end.
 end case.
    
 tObjType = "F".
 if not tFound
  then pResponse:fault("3004", "Invoice").
  else std-lo = createSysDoc().
      
 if not std-lo
  then RETURN false.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddQarDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION AddQarDocument Procedure 
FUNCTION AddQarDocument RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 def var tFound as logical.

 if NOT CAN-FIND(FIRST Qar 
                   WHERE Qar.qarID = int(tID))
  then
   do: pResponse:fault("3066", "QAR").
       RETURN false.
   end.
    
 tFound = false.
 std-lo = false.
    
 TRX-BLK:
 for first Qar exclusive-lock
   where Qar.qarID = int(tID) TRANSACTION
     on error undo TRX-BLK, leave TRX-BLK:
  tFound = true.
  std-lo = createSysDoc().
 end.
    
 if not tFound
  then pResponse:fault("3004", "QAR").
      
 if not std-lo
  then RETURN false.
    
 pResponse:updateEvent("QAR", tID).

 RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddQualificationDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION AddQualificationDocument Procedure 
FUNCTION AddQualificationDocument RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 def var tFound as logical.

 if NOT CAN-FIND(FIRST qualification 
                   WHERE qualification.qualificationID = int(tID))
  then
   do: pResponse:fault("3066", "qualification").
       RETURN false.
   end.
    
 tFound = false.
 std-lo = false.
    
 TRX-BLK:
 for first qualification exclusive-lock
   where qualification.qualificationID = int(tID) TRANSACTION
     on error undo TRX-BLK, leave TRX-BLK:
  tFound = true.
  std-lo = createSysDoc().
 end.
    
 if not tFound
  then pResponse:fault("3004", "Qualification").
      
 if not std-lo
  then RETURN false.
    
 pResponse:updateEvent("Qualification", tID).

 RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddRateStateDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION AddRateStateDocument Procedure 
FUNCTION AddRateStateDocument RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 def var tFound as logical.

 if NOT CAN-FIND(FIRST ratestate 
                   WHERE ratestate.cardsetID  = int(tID))
  then
   do: pResponse:fault("3066", "RateState").
       RETURN false.
   end.
    
 tFound = false.
 std-lo = false.
    
 TRX-BLK:
 for first ratestate exclusive-lock
   WHERE ratestate.cardsetID  = int(tID) TRANSACTION
     on error undo TRX-BLK, leave TRX-BLK:
  tFound = true.
  std-lo = createSysDoc().
 end.
    
 if not tFound
  then pResponse:fault("3004", "RateState").
      
 if not std-lo
  then RETURN false.
    
 pResponse:updateEvent("RateState", tID).

 RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddStateDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION AddStateDocument Procedure 
FUNCTION AddStateDocument RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def var tFound as logical.

 if NOT CAN-FIND(FIRST state 
                   WHERE state.stateID = tID)
  then
   do: pResponse:fault("3066", "State").
       RETURN false.
   end.
    
 tFound = false.
 std-lo = false.
    
 TRX-BLK:
 for first state exclusive-lock
   where state.stateID = tID TRANSACTION
     on error undo TRX-BLK, leave TRX-BLK:
  tFound = true.
  std-lo = createSysDoc().
 end.
    
 if not tFound
  then pResponse:fault("3004", "State").
      
 if not std-lo
  then RETURN false.
    
 pResponse:updateEvent("State", tID).

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createSysDoc) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createSysDoc Procedure 
FUNCTION createSysDoc RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def var pSuccess as logical init false.

TRX-BLOCK:
do TRANSACTION on error undo TRX-BLOCK, leave TRX-BLOCK:
  CREATE sysdoc.
  assign
    sysdoc.entityType = tType
    sysdoc.entityID = tID
    sysdoc.entitySeq = tSeq
    .
  validate sysdoc.
  
  ASSIGN 
    sysdoc.docdate = now
    sysdoc.uid = pRequest:uid
    .
  
  if tObjType > ""
   then 
    assign
      sysdoc.objType = tObjType
      sysdoc.objID = tObjID
      sysdoc.objAttr = tObjAttr
      .
  release sysdoc.
  
  pSuccess = true.  
end.

RETURN pSuccess.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

