&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* sys/deletesysdoc.p
    @name SystemDocumentDelete
    @description Deletes a system document using the specified parameters
    @param Type;char;Type of the doc to delete
    @param ID;char;ID of the doc to delete
    @param Seq;int;Sequence of the doc to delete (optional)
    @returns Success;log;Success indicator
    @throws 10400;Bad Request
    @author D.Sinclair
    @version 1.0 5.1.2014
    @notes AC 09/27/17 - Added entity Finding
           RS 06/14/18 - Added entity Qualification
           RS 06/21/18 - Added entity rateState for state rate Manuals
                         for RCM application
           AG 10/19/20 - Added entity QAR
 */

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}

DEFINE VARIABLE tType AS CHARACTER   NO-UNDO.
DEFINE VARIABLE tID AS CHARACTER   NO-UNDO.
DEFINE VARIABLE tSeq AS INTEGER     NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-deleteSysDoc) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD deleteSysDoc Procedure 
FUNCTION deleteSysDoc RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeActionDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD removeActionDocument Procedure 
FUNCTION removeActionDocument RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeAgentDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD removeAgentDocument Procedure 
FUNCTION removeAgentDocument RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeBatchDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD removeBatchDocument Procedure 
FUNCTION removeBatchDocument RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeFindingDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD removeFindingDocument Procedure 
FUNCTION removeFindingDocument RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeInvoiceDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD removeInvoiceDocument Procedure 
FUNCTION removeInvoiceDocument RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeQARDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD removeQARDocument Procedure 
FUNCTION removeQARDocument RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeQualificationDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD removeQualificationDocument Procedure 
FUNCTION removeQualificationDocument RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeRateStateDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD removeRateStateDocument Procedure 
FUNCTION removeRateStateDocument RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeStateDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD removeStateDocument Procedure 
FUNCTION removeStateDocument RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

pRequest:getParameter("Type", tType).
pRequest:getParameter("ID", tID).
pRequest:getParameter("Seq", tSeq).
IF tSeq = ? 
 THEN tSeq = 0.

case tType:
 when "Batch" then std-lo = removeBatchDocument().
 when "Agent" then std-lo = removeAgentDocument().
 when "State" then std-lo = removeStateDocument().
 when "Invoice-AP" or
 when "Invoice-AR" then std-lo = removeInvoiceDocument().
 when "Finding" then std-lo = removeFindingDocument().
 when "Qualification" then std-lo = removeQualificationDocument().
 when "rateState" then std-lo = removeRateStateDocument().
 when "Action" then std-lo = removeActionDocument().
 when "Claims" then std-lo = true.
 when "QAR" then std-lo = removeQARDocument().
 otherwise run util/sysmail.p ("sys/deletesysdoc.p: Invalid Type: " + tType, "").
end case.

IF NOT std-lo 
 then 
  do: pResponse:fault("3000", "Delete").
      RETURN.
  END.

pResponse:success("2000", "Delete").
pResponse:deleteEvent("SystemDocument", tType {&id-add} tID {&id-add} STRING(tSeq)).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-deleteSysDoc) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION deleteSysDoc Procedure 
FUNCTION deleteSysDoc RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def var pSuccess as logical init false.

TRX-BLOCK:
for first sysdoc
  where sysdoc.entityType = tType
    and sysdoc.entityID = tID
    and sysdoc.entitySeq = tSeq TRANSACTION
  on error undo TRX-BLOCK, leave TRX-BLOCK:
 delete sysdoc.
 release sysdoc.
 pSuccess = true.
end.

RETURN pSuccess.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeActionDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION removeActionDocument Procedure 
FUNCTION removeActionDocument RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  def var tFound as logical.

  std-in = int(tID) no-error.
  if NOT CAN-FIND(FIRST action 
                    WHERE action.actionID = std-in)
   then
    do: pResponse:fault("3066", "Action").
        RETURN false.
    end.
     
  tFound = false.
  std-lo = false.
     
  TRX-BLK:
  for first action exclusive-lock
    where action.actionID = std-in TRANSACTION
      on error undo TRX-BLK, leave TRX-BLK:
   tFound = true.
   std-lo = deleteSysDoc().
  end.
     
  if not tFound
   then pResponse:fault("3004", "Action").
       
  if not std-lo
   then RETURN false.
     
  pResponse:updateEvent("Action", tID).
  
  RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeAgentDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION removeAgentDocument Procedure 
FUNCTION removeAgentDocument RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def var tFound as logical.

if NOT CAN-FIND(FIRST agent 
                  WHERE agent.agentID = tID)
 then
  DO: pResponse:fault("3066", "Agent").
      RETURN false.
  END.

tFound = FALSE.
std-lo = false.

TRX-BLK:
for first agent exclusive-lock
  where agent.agentID = tID TRANSACTION
   on error undo TRX-BLK, leave TRX-BLK:
 tFound = TRUE.
 std-lo = deleteSysDoc().
end.
 
if not tFound
 then pResponse:fault("3004", "Agent").
  
IF not std-lo
 THEN RETURN false.
  
pResponse:updateEvent("Agent", tID).

return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeBatchDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION removeBatchDocument Procedure 
FUNCTION removeBatchDocument RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def var tFound as logical.

std-in = int(tID) no-error.
if NOT CAN-FIND(FIRST BATCH 
                  WHERE BATCH.batchID = std-in)
 then
  DO: pResponse:fault("3066", "Batch").
      RETURN false.
  END.

tFound = FALSE.
std-lo = false.

TRX-BLK:
for first batch exclusive-lock
  where batch.batchID = std-in TRANSACTION
   on error undo TRX-BLK, leave TRX-BLK:
 tFound = TRUE.
 std-lo = deleteSysDoc().
end.
 
if not tFound
 then pResponse:fault("3004", "Batch").
  
IF not std-lo
 THEN RETURN false.
  
pResponse:updateEvent("Batch", tID).

return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeFindingDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION removeFindingDocument Procedure 
FUNCTION removeFindingDocument RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def var tFound as logical.

if NOT CAN-FIND(FIRST finding 
                  WHERE finding.findingID = int(tID))
 then
  DO: pResponse:fault("3066", "Finding").
      RETURN false.
  END.

tFound = FALSE.
std-lo = false.

TRX-BLK:
for first finding exclusive-lock
  where finding.findingID = int(tID) TRANSACTION
   on error undo TRX-BLK, leave TRX-BLK:
 tFound = TRUE.
 std-lo = deleteSysDoc().
end.
 
if not tFound
 then pResponse:fault("3004", "finding").
  
IF not std-lo
 THEN RETURN false.
  
pResponse:updateEvent("finding", tID).

return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeInvoiceDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION removeInvoiceDocument Procedure 
FUNCTION removeInvoiceDocument RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def var tFound as logical no-undo init false.

std-in = int(tID) no-error.
/* decide if we have a payable or receivable invoice */
case entry(2,tType,"-"):
 when "AP" then
  for first apinv no-lock
      where apinv.apinvID = std-in:
      
    tFound = true.
  end.
 when "AR" then
  for first arinv no-lock
      where arinv.arinvID = std-in:
      
    tFound = true.
  end.
end case.
    
if not tFound
 then pResponse:fault("3066", "Invoice").
 else std-lo = deleteSysDoc().
     
if not std-lo
 then RETURN false.

RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeQARDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION removeQARDocument Procedure 
FUNCTION removeQARDocument RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def var tFound as logical.

if NOT CAN-FIND(FIRST Qar 
                  WHERE Qar.QarID = int(tID))
 then
  DO: pResponse:fault("3066", "Qar").
      RETURN false.
  END.

tFound = FALSE.
std-lo = false.

TRX-BLK:
for first Qar exclusive-lock
  where Qar.QarID = int(tID) TRANSACTION
   on error undo TRX-BLK, leave TRX-BLK:
 tFound = TRUE.
 std-lo = deleteSysDoc().
end.
 
if not tFound
 then pResponse:fault("3004", "Qar").
  
IF not std-lo
 THEN RETURN false.
  
pResponse:updateEvent("Qar", tID).

return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeQualificationDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION removeQualificationDocument Procedure 
FUNCTION removeQualificationDocument RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def var tFound as logical.

if NOT CAN-FIND(FIRST qualification 
                  WHERE qualification.qualificationID = int(tID))
 then
  DO: pResponse:fault("3066", "qualification").
      RETURN false.
  END.

tFound = FALSE.
std-lo = false.

TRX-BLK:
for first qualification exclusive-lock
  where qualification.qualificationID = int(tID) TRANSACTION
   on error undo TRX-BLK, leave TRX-BLK:
 tFound = TRUE.
 std-lo = deleteSysDoc().
end.
 
if not tFound
 then pResponse:fault("3004", "Qualification").
  
IF not std-lo
 THEN RETURN false.
  
pResponse:updateEvent("Qualification", tID).

return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeRateStateDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION removeRateStateDocument Procedure 
FUNCTION removeRateStateDocument RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def var tFound as logical.

if NOT CAN-FIND(FIRST ratestate 
                WHERE ratestate.cardsetID  = int(tID))
then
DO: pResponse:fault("3066", "RateState").
   RETURN false.
END.

tFound = FALSE.
std-lo = false.

TRX-BLK:
for first ratestate exclusive-lock
    WHERE ratestate.cardsetID  = int(tID) TRANSACTION
 on error undo TRX-BLK, leave TRX-BLK:
 tFound = TRUE.
 std-lo = deleteSysDoc().
 end.

 if not tFound
 then pResponse:fault("3004", "RateState").

 IF not std-lo
 THEN RETURN false.

 pResponse:updateEvent("RateState", tID).

 return true.

 END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeStateDocument) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION removeStateDocument Procedure 
FUNCTION removeStateDocument RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def var tFound as logical.

if NOT CAN-FIND(FIRST state 
                  WHERE state.stateID = tID)
 then
  DO: pResponse:fault("3066", "State").
      RETURN false.
  END.

tFound = FALSE.
std-lo = false.

TRX-BLK:
for first state exclusive-lock
  where state.stateID = tID TRANSACTION
   on error undo TRX-BLK, leave TRX-BLK:
 tFound = TRUE.
 std-lo = deleteSysDoc().
end.
 
if not tFound
 then pResponse:fault("3004", "State").
  
IF not std-lo
 THEN RETURN false.
  
pResponse:updateEvent("State", tID).

return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

