/*------------------------------------------------------------------------
@name CreateAgentFileIndexes.cls
@action AgentFileIndexesCreate
@description  Delete and re-create agent file indexes
@author Shefali
@version 1.0
@returns Success;int;2019;AgentFileIndexesCreate successfully ran asynchronously.
@Modified    :
    Date        Name                 Comments

----------------------------------------------------------------------*/

class sys.CreateAgentFileIndexes inherits framework.ActionBase:
  
  constructor CreateAgentFileIndexes ():
    super().
  end constructor.

  destructor public CreateAgentFileIndexes ():
  end destructor.

  method public override void act (input pRequest    as framework.IRequestable,
                                   input pResponse   as framework.IRespondable):


    {lib/std-def.i}
    
    define variable iAgentFileID     as integer   no-undo.
    define variable cIndexValueList  as character no-undo.
    define variable cAddressList     as character no-undo.
    define variable cStateCountyList as character no-undo.
    define variable cStateList       as character no-undo.
    define variable cMsg             as character no-undo.
    define variable lSuccess         as logical   no-undo.
    define variable cErrMessage      as character no-undo.
    
    for each agentfile where source = 'P' no-lock:

      assign
          cIndexValueList  = ''
          cAddressList     = ''
          cStateCountyList = ''
          cStateList       = ''
          iAgentFileID     = 0 .

      /* Creating index for agentfileID and fileNumber */
      cIndexValueList = cIndexValueList + '|' + string(agentFile.agentFileID) + '|' + agentFile.fileNumber.
      iAgentFileID = agentfile.agentfileid.
    
      for each fileProperty 
        where  fileproperty.agentfileid = agentfile.agentfileid
        no-lock:
    
        cIndexValueList = cIndexValueList + '|' + fileProperty.addr1 + '|' + fileProperty.addr2 + '|' + fileProperty.addr3 + '|' + fileProperty.addr4 + '|' + fileProperty.city + '|' +
                          fileProperty.zip + '|' + fileProperty.stateID + '|' + fileProperty.countyID  + '|' + string(fileProperty.parcelID) + '|' + fileProperty.book + '|' + 
                          fileProperty.pageNumber + '|' + fileProperty.Condominum + '|' + fileProperty.subdivision.
      
        cAddressList  = cAddressList + '|' + fileproperty.city + '|' + fileproperty.zip  + '|' +  fileproperty.addr1  + '|'  + fileProperty.addr2 + '|' + fileProperty.addr3 + '|' 
                                     + fileProperty.addr4 + '|' + string(fileproperty.parcelID) + '|' + fileProperty.Condominum + '|' +  fileProperty.subdivision.
      
        cStateCountyList = cStateCountyList + (if fileproperty.countyID = '' then '' else ('|' + fileproperty.StateID + '^' + fileproperty.countyID)).
      
        cStateList  = cStateList + '|' + fileproperty.StateID.
      end.
      
      for each fileParty no-lock
        where  fileParty.agentFileID = agentFile.agentFileID 
          and (fileParty.partyType   = 'B' or  fileParty.partyType   = 'S')
           by  fileParty.partyType by fileParty.seq:
      
          /* Creating index for buyer and seller name */
        cIndexValueList = cIndexValueList + '|' + fileParty.firstName + '|' + fileParty.middleName + '|' + fileParty.lastName + '|' + fileParty.name.
        
      end. 
      
      if cIndexValueList ne ''  
       then
        do:
          /* Build keyword index to use for searching */
          run sys/createsysindex.p (input 'AgentFile',           /* ObjType     */
                                    input '',                    /* ObjAttribute*/
                                    input string(iAgentFileID),  /* ObjID       */
                                    input cIndexValueList,       /* ObjKeyList  */
                                    output lSuccess,
                                    output cMsg).
          if not lSuccess
           then
            cErrMessage = cErrMessage + " " + cMsg.
        end. /* if cIndexValueList ne '' */
      
      /* Creating file property index file property and state for production files. */
      if cStateList ne ''  
       then
        do:
          /* Build keyword index to state */
          run sys/createsysindex.p (input 'FileProperty',            /* ObjType     */
                                    input 'State',                   /* ObjAttribute*/
                                    input string(iAgentFileID),      /* ObjID       */
                                    input cStateList,                /* ObjKeyList  */
                                    output lSuccess,
                                    output cMsg).
          if not lSuccess
           then
            cErrMessage = cErrMessage + " " + cMsg.
        end. /* if cStateList ne '' */
      
      /* Creating file property index file property and address for production files. */
      if cAddressList ne ''  
       then
        do:
          /* Build keyword index to use for searching */
          run sys/createfilepropertyindex.p (input 'FileProperty',         /* ObjType     */
                                             input 'Address',              /* ObjAttribute*/
                                             input string(iAgentFileID),   /* ObjID       */
                                             input cAddressList,           /* ObjKeyList  */
                                             input cStateCountyList,
                                             output lSuccess,
                                             output cMsg).
          if not lSuccess
           then
            cErrMessage = cErrMessage + " " + cMsg.
        end. /* if cAddressList ne '' */  
      
    end.

    if cErrMessage <> "" 
	 then
	  do:
        pResponse:fault("3005", "CreateAgentFileIndexes failed. " + cErrMessage).
        return.
	  end.

  end method.

  method public override void async(input pRequest  as framework.IRequestable,
                                    input pResponse as framework.IRespondable):
									
    define variable oAsyncRun as framework.AsyncRun no-undo.
    
    oAsyncRun = NEW framework.AsyncRun( pRequest:actionID, pRequest:UID, 1, "" ).
    oAsyncRun:RunAsync ().

    if valid-object(oAsyncRun) 
     then delete object oAsyncRun no-error.

    pResponse:success("2019", pRequest:actionID + " successfully ran asynchronously.").
     
  end.
      
end class.
