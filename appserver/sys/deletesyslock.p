/*------------------------------------------------------------------------
@name SystemLockDelete
@description Deletes a system lock using the specified parameters
@param Type;char;Type of the lock to delete
@param ID;char;ID of the lock to delete
@param Seq;int;Sequence of the lock to delete (optional)
@returns Success;log;Success indicator
@throws 10400;Bad Request
@author D.Sinclair
@version 1.0 5.1.2014
@notes
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}

DEFINE VARIABLE tType AS CHARACTER   NO-UNDO.
DEFINE VARIABLE tID AS CHARACTER   NO-UNDO.
DEFINE VARIABLE tSeq AS INTEGER     NO-UNDO.


pRequest:getParameter("Type", tType).
pRequest:getParameter("ID", tID).
pRequest:getParameter("Seq", tSeq).
IF tSeq = ? 
 THEN tSeq = 0.

RUN util/deletesyslock.p (tType, tID, tSeq, OUTPUT std-lo).

IF NOT std-lo 
 then 
  do: pResponse:fault("3000", "Unlock").
      RETURN.
  END.

pResponse:success("2000", "Unlock").
pResponse:deleteEvent("SystemLock", tType {&id-add} tID {&id-add} STRING(tSeq)).
