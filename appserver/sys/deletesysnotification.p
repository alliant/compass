/*------------------------------------------------------------------------
@name crm/deletesysnotification.p
@action systemNotificationDelete
@description Deletes a system Notification using the specified parameters
@param sysNotificationID;int;sysNotificationID of the SystemNotification to delete
@returns 2008;System Notification has been removed
@throws 3000;Delete failed
@throws 3066;SystemNotification does not exist
@author Anjly
@version 1.0 
@notes   Temparary API, used only for testing phase
@created 08/21/2019
----------------------------------------------------------------------*/
define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable dtCreateDate        as date no-undo.
define variable iSyaNotificationID  as integer no-undo.

{lib/std-def.i}

pRequest:getParameter("createDate", output dtCreateDate).
pRequest:getParameter("SysNotificationID", output iSyaNotificationID).

if iSyaNotificationID ne 0 and
   (not can-find(first sysNotification where sysNotification.sysNotificationID = iSyaNotificationID))   
 then pResponse:fault("3066", "SystemNotification").

if pResponse:isFault() 
 then return.
   
std-lo = false.

if iSyaNotificationID ne 0 then
 for first sysNotification exclusive-lock
   where sysNotification.sysNotificationID = iSyaNotificationID 
   on error undo, leave:
  
   delete sysNotification.
   release sysNotification.
   std-lo = true.
  
 end.
else
 for each sysNotification exclusive-lock
   where sysNotification.createdate le datetime(dtCreateDate)
   on error undo, leave:
  
   delete sysNotification.
   release sysNotification.
   std-lo = true. 
   
 end.

if not std-lo
  then
  do:
    pResponse:fault("3000", "Delete").
    return.
  end.

 pResponse:success("2008", "System Notification").
 pResponse:deleteEvent("SystemNotification", string(iSyaNotificationID)).
