/*------------------------------------------------------------------------
@file getsysactentdest.p
@description getting fields action,entity,destination  

@return table ttsysaction,list Entity,list Destination      
        
@author Sachin Chaturvedi
@created 12.27.2021
@Modification
Date       Name          Description
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

/* Local Variables ---                                                  */
define variable cEntity      as character no-undo.
define variable cDestination as character no-undo.

/* Temp Tables ---                                                      */
{tt/sysaction.i &tableAlias="ttsysaction"}

{lib/std-def.i}
  
/* sys actions */
for each sysaction no-lock 
  where isDestination: 
  create ttsysaction.
  assign      
      ttsysaction.action = sysaction.action
      ttsysaction.name   = sysaction.name
	  std-in             = std-in + 1
      .  	
end. 

/*entity & destination*/
for each sysprop no-lock 
  where appCode     = "SYS"
    and objAction   = "Destination":
  if objProperty = "Entity" 
   then
    cEntity  = cEntity + (if cEntity = "" then "" else ",") + sysprop.objValue + "," + sysprop.objID.					  
  else if objProperty = "Type" 
   then 
    cDestination = cDestination + (if cDestination = "" then "" else ",") + sysprop.objValue + "," + sysprop.objID.
end.
	 
if std-in > 0 
 then 
  pResponse:setParameter("sysAction", table ttSysAction).
  
pResponse:setParameter("sysDestEntity", cEntity).
pResponse:setParameter("sysDestDestination", cDestination).
pResponse:success("2005", (string(std-in) + " actions for sys Destination " ) ).
