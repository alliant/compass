/*------------------------------------------------------------------------
@name deletecompliancelogs.p
@action complianceLogsDelete

@description Delete a comlog records from the database
@param dcomlogdate;date;(required)
@throws 3000; Delete failed
@throws 3005; Root Directory not found.
@throws 3005; Failed to create folder 'comlog'
@throws 3005; No comlog record found

@author : Rahul
@version 1.0
@created 03-18-2019
Date          Name          Description
08/27/2018    Naresh Chopra Modified to fix the error occured while creating .csv file.
11/14/2018    Rahul         Program name changed from purgecomlog.p to 
                            deletecompliancelogs.p
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

{lib/std-def.i}

define variable dEndDate    as date        no-undo.
define variable cdir_name   as character   no-undo.
define variable cfile_name  as character   no-undo.
define variable iCount      as integer     no-undo.
define variable cKey        as character   no-undo.

define variable lSuccess    as logical initial false.

define stream strcomlog.

pRequest:getParameter("ComLogDate", dEndDate).

if dEndDate = ? 
 then
  do:
    pResponse:fault("3005", "Input Date parameter cannot be blank.").
    return.
  end.

/* Get the directory and the filename prefix */
publish "GetSystemParameter" ("dataRoot", output cdir_name).

if cdir_name = "" 
 then
  do:
    pResponse:fault("3005", "Root Directory not found.").
    return.
  end.

assign
  cdir_name           = cdir_name + "comlog\"
  file-info:file-name = cdir_name
 .

/* Check whether the comlog folder is already exist or not */
if file-info:full-pathname eq ? 
 then
  do:
    os-create-dir value(cdir_name).
    if os-error ne 0 
     then
      do:
        pResponse:fault("3005", "Failed to create folder 'comlog'.").
        return.
      end.
  end.

cfile_name  = cdir_name + ( "comlog" + replace(string(today), "/", "") + "_" + string(time) + ".csv").
output stream strcomlog to value(cfile_name). 

/* Delete the comlog records and write them into a comma delimited .csv file */
COMLOG-BLOCK:
for each comlog exclusive-lock 
  where date(comlog.logDate) le dEndDate transaction
  on error undo COMLOG-BLOCK, retry COMLOG-BLOCK:

  if retry 
   then
    do:      
      lSuccess = false.
      leave COMLOG-BLOCK.
    end.

  export stream strcomlog delimiter "," comlog.logID comlog.refType comlog.refID comlog.seq comlog.logDate 
  comlog.uid comlog.notes comlog.comStat comlog.objRef comlog.objID comlog.action comlog.stateID.

  delete comlog.

  release comlog.

  assign 
      iCount   = iCount + 1
      lSuccess = true
      .
end.

output stream strcomlog close.

if not lSuccess 
 then 
  do:
    pResponse:fault("3000", "Comlog Delete").
    return.
  end.

pResponse:success("3005", "ComplianceLog record(s) purged in file: " + cfile_name + " till date " + string(dEndDate) + " and " + string(iCount) + " complianceLog record(s) deleted.").

pResponse:otherEvent("ComplianceLogsDelete", cKey, "Deleted", "ComplianceLog record(s) purged in file: " + cfile_name + " till date " + string(dEndDate) + " and " + string(iCount) + " complianceLog record(s) deleted.").
