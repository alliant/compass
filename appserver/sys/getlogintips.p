/*------------------------------------------------------------------------
@file getlogintip.p
@action arbingerTipGet
@description Get the arbinger tips for the login screen.

@returns The table dataset

@author John Oliver
@version 1.0
@created 12/12/2017
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable cFile as character no-undo.

{lib/std-def.i}
{tt/logintip.i}

publish "GetSystemParameter" ("LoginTipFile", output cFile).

if search(cFile) = ?
 then pResponse:fault("3066", "Login Tip File").
 
if pResponse:isFault()
 then return.

file-information:file-name = cFile.
pResponse:setParameter("Date", file-information:file-mod-date).

std-in = 0.
input from value(cFile).
repeat:
  import unformatted std-ch.
  create logintip.
  assign
    std-in = std-in + 1
    logintip.tip = std-ch
    logintip.id = std-in
    std-ch = ""
    .
end.
input close.

IF std-in > 0
 then pResponse:setParameter("Login", table logintip).

pResponse:success("2005", STRING(std-in) {&msg-add} "Login Tip").
