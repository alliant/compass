/*------------------------------------------------------------------------
@name systemCodeDelete
@description Deletes a system code using the specified parameters
@param Type;char;Type of the SystemCode to delete
@param Code;char;Code of the SystemCode to delete
@returns 2008;System Code has been removed
@throws 3005;Code Type and Code cannot be blank
@throws 3066;System Code does not exist
@throws 3004;System Code is locked
@throws 3000;Delete failed
@author Russ Kenny
@version 1.0 12/17/2013
@notes
@modified 02/2018 - YS - Activated log message
       10/03/2018 - Rahul - Modified to allow multiple delete
       03/12/2019 - Anubha Jain - Added Event for logging
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.
{lib/std-def.i}

define temp-table ttEvents
  field cCode as character.

define variable tType       as character no-undo.
define variable tCode       as character no-undo.
define variable pRefType    as character no-undo.
define variable pRefNum     as character no-undo.
define variable pRefMessage as character no-undo.
define variable lLocked     as logical   no-undo.
define variable lDeleted    as logical   no-undo.

pRequest:getParameter("Type", tType).
pRequest:getParameter("Code", tCode).

if tCode = "" or tType = "" 
 then
  pResponse:fault("3005", "Code Type and Code cannot be blank.").

if tCode <> "" and tCode <> "ALL" and tType <> "" 
 and not can-find(first syscode where syscode.codeType = tType
                                  and syscode.code     = tCode)
 then
  pResponse:fault("3066", "System Code").

if tCode = "ALL" and tType <> "" 
  and not can-find(first syscode where syscode.codeType = tType)
 then 
  pResponse:fault("3066", "System Code").

if pResponse:isFault() 
 then 
  return.

assign
  lLocked = false
  lDeleted = false
  .

syscode-BLOCK:
for each syscode exclusive-lock
  where syscode.codeType = tType
    and syscode.code     = (if tCode = "ALL" then syscode.code else tCode) transaction
  on error undo syscode-BLOCK, leave syscode-BLOCK:
 
  assign 
    lLocked      = true
    pRefType     = syscode.codeType
    pRefNum      = syscode.code
    pRefMessage  = "Type: "         + syscode.type + " / "        +
                   "Description: "  + syscode.description + " / " +
                   "Comments: "     + syscode.comments
    .
 
  pResponse:logMessage(pRefType, pRefNum, pRefMessage).
 
  delete syscode.
  release syscode.
    
  /* This record is created to store syscode's Code which have been deleted successfully.
  It is used for creating a delete event. */
  create ttEvents.
  assign
      ttEvents.cCode = pRefNum 
      lDeleted       = true
      . 
end.

if not lLocked 
 then 
  pResponse:fault("3004", "System Code").
 else if not lDeleted
  then 
   pResponse:fault("3000", "Delete").

if pResponse:isFault()
 then 
  return.

pResponse:success("2008", "System Code").

/* This temp-table contains those syscode's ID and type which have been deleted successfully. */
for each ttEvents:
  pResponse:deleteEvent("SystemCode", tType {&id-add} ttEvents.cCode).
end.

