/*------------------------------------------------------------------------
@name sys/assignuserrole.p   
@action userRoleAssign
@description Assign/Unassign role to user

@throws 3005;No XML file attached

@author Rahul Sharma
@version 1.0
@created 10/17/2018
Modification:
Date          Name        Description
----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

define variable iNextSeq      as integer    no-undo.
define variable ichuserList as character  no-undo.
define variable ichRole       as character  no-undo.
define variable cRoleList     as character  no-undo.

{tt/file.i}
{lib/std-def.i}

pRequest:getParameter("userList", output ichuserList).
pRequest:getParameter("Roleid", output ichRole).

std-lo = false.

TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:
  
  for each sysuser no-lock:
    if can-do(sysuser.role,ichRole) then
    do:
      if not can-do(ichuserList,sysuser.UID) then
      do:
        buffer sysuser:handle:find-current (exclusive-lock,no-wait).
        assign cRoleList       = trim(sysuser.role,",") + ","
               sysuser.role = trim(replace(cRoleList, ichRole + ",",""), ",").
        buffer sysuser:handle:find-current (no-lock).
      end.
    end.
    else 
    do:
      if can-do(ichuserList,sysuser.UID) then
      do:
        buffer sysuser:handle:find-current (exclusive-lock,no-wait).
        assign sysuser.role = trim((sysuser.role + "," + ichRole), ",").
        buffer sysuser:handle:find-current (no-lock).
      end.
    end.
  end.
end.
if not std-lo
 then
   do: pResponse:fault("3000", "Assign").
       return.
   end.

pResponse:success("2012", "sysuser").

