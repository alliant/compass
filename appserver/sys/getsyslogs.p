/* sys/getsyslogs.p
@name SystemLogsGet
@description handler for LISTing system LOG
@param userid;char;
@param action;char;
@param startDate;date;optional (default = today)
@param endDate;date;optional (default = today)
@author D.Sinclair
@created 8.7.2012
@notes Returns a maximum of 250 records
Modification:
Date          Name        Description
10/17/2018    Rahul       Removed maximum results record limit, 
                          debugging messages added
03/07/2019    Anubha      Modified EndDate parameter
03/18/2019    Rahul       Modified to add stored procedure and parameters
04/15/2019    Gurvindar   Checked date range should be not more than year
04/26/2019    Rahul       Modified to remove statement assigning startdate as today
04/29/2019    Rahul       Modified to add validations 
01/25/2022    Shefali     Task:89648 Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"
07/29/2022    Shefali     Task:95509 Clob related changes
 */

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

/* stored procedure variables */
define variable pcActionType   as character  no-undo.
define variable pcAction       as character  no-undo.
define variable pcUID          as character  no-undo.
define variable pdtStartDate   as datetime   no-undo.
define variable pdtEndDate     as datetime   no-undo.

define variable daEndDate      as date       no-undo.

{lib/std-def.i}
{lib/callsp-defs.i }
{tt/sysLog.i &tableAlias="ttSysLog"}

def var dsHandle as handle no-undo.

create dataset dsHandle.
dsHandle:xml-node-name = 'data'.
dsHandle:set-buffers(buffer ttSysLog:handle).

pRequest:getParameter("ActionType", output pcActionType). /* (C)Called, (E)Error , (A)ALL actions */
pRequest:getParameter("Action",     output pcAction).
pRequest:getParameter("UID",        output pcUID).
pRequest:getParameter("StartDate",  output pdtStartDate).
pRequest:getParameter("EndDate",    output daEndDate).

if daEndDate = ? 
 then
  do:
    pResponse:fault("3005" , "End Date cannot be blank").
    return.
  end.

if daEndDate > today 
 then
  do:
    pResponse:fault("3005" , "End Date cannot be in future").
    return.
  end.

/* Get the records till the midnight of the provided end date. */
assign pdtEndDate   = datetime(string(daEndDate) + " " + "23:59:59.999").

if pdtStartDate = ? 
 then
  for first syslog use-index logID:
    pdtStartDate = syslog.createDate.
  end.

/* Checking date range  */
if interval(pdtEndDate,pdtStartDate,"months") > 6
 then
  do:
     pResponse:fault("3005" , "Start Date is " + string(date(pdtStartDate)) + ", Input Date Range cannot be more than a year").
     return.
  end.

{lib/callsp.i &name=spGetSysLogs &load-into=ttSysLog &params="input pcActionType, input pcUID, input pcAction, input pdtStartDate, input pdtEndDate"}

for each ttSysLog exclusive-lock:
  for first syslog fields(msg sysmsg) no-lock where syslog.logID = ttSysLog.logID:
    copy-lob syslog.msg to ttSysLog.msg.
    copy-lob syslog.sysmsg to ttSysLog.sysmsg.
  end.
end.

if csp-icount > 0 
 then  
  pResponse:setParameter(input dataset-handle dsHandle).

pResponse:success("2005", string(csp-icount) {&msg-add} "System Logs").
