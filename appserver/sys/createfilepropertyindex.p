/*----------------------------------------------------------------------
@file createfilepropertyindex.p
@description Generic routine to build a set of sysindex records based on the 
             key list of FileProperty Type.

@param pObjType:char,pobjAttribute, pObjID:char, pobjKeyList:char ,pStatecountyList 

@throws 3000;Build failed

@returns Nothing
@success 2005

@author S Chandu
@created 06.06.2023
@notes
@modified 
----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

def input  param pObjType         as char.
def input  param pObjAttribute    as char.
def input  param pObjID           as char.
def input  param pobjKeyList      as char no-undo.
def input  param pStateCountyList as char no-undo.
def output param plSuccess        as log  no-undo.
def output param pcMsg            as char no-undo.

def var cState     as char init "" no-undo.
def var cCounty    as char init "" no-undo.
def var cValue     as char init "" no-undo.
def var iCount     as int          no-undo.

{lib/std-def.i}

 define variable lSuccess        as logical   no-undo.
 define variable cMsg            as character no-undo.
 define variable cErrMessage     as character no-undo.

/* calling create sysindex for address */
 if pobjKeyList ne ''  
  then
   do:
     /* Build keyword index to use for searching */
     run sys/createsysindex.p (input PObjType,         /* ObjType */
                               input pObjAttribute,    /* ObjAttribute*/
                               input pObjID,          /* ObjID */
                               input pobjKeyList,    /* ObjKeyList*/
                               output lSuccess,
                               output cMsg).
     if not lSuccess
      then
       cErrMessage = cErrMessage + " " + cMsg.
   end. /* if pobjKeyList ne '' */

std-lo = false.
TRX-BLOCK:
do transaction
  on error undo TRX-BLOCK, leave TRX-BLOCK:

    /* deleting records */
   for each sysindex exclusive-lock
    where sysindex.objType = pObjType
    and sysindex.objAttribute = 'State,County'
    and sysindex.objID = pObjID:
    
    delete sysindex.
    release sysindex.
  end.
  
  
 /* state county index creation */
 if pStateCountyList ne ''
  then
   do:
     do iCount = 1 to num-entries(pStateCountyList,'|'):
         
       cstate = entry(1, entry(icount,pStateCountyList,'|'),'^') no-error.
  
       cCounty = entry(2, entry(icount,pStateCountyList,'|'),'^') no-error.
  
       cValue = Cstate + ',' + Ccounty.

     if cValue <> "" and cValue <> " " and cValue <> ? and cValue <> ','
      and not can-find(sysindex where sysindex.objType      = pObjType
                                  and sysindex.objAttribute = 'State,County'
                                  and sysindex.objKey       = cValue
                                  and sysindex.objID        = pObjID) 
     then
      do:
        create sysindex.
        assign sysindex.objType      = pObjType                                                     
               sysindex.objAttribute = 'State,county'
               sysindex.objKey       = cValue
               sysindex.objID        = pObjID.   
               
         validate sysindex.           
         release  sysindex.
     end.


     cValue = ''. 
     end. /* do iCount = 1 */
   end. 

 std-lo = true.

end.
if not std-lo
 then 
  do: 
    plSuccess = std-lo.
    pcMsg = "BuildFilePropertySysIndex failed.".
    return.
  end.

plSuccess = std-lo.
