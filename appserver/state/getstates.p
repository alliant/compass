/*------------------------------------------------------------------------
@name StatesGet
@action statesGet
@description Provides either a complete list of all states or a specific state

@param StateID;char;State identifier (optional)
@param Status;A)ctive, I)nactive, or blank=all (optional)

@returns States;complex;State dataset

@author Russ Kenny
@version 1.0 12/11/2013
@notes
@modification
Date          Name   Description
10/08/2020    VJ     Moved agent filter logic into sql
01-25-2022    SA     Task:89648 Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{lib/std-def.i}
{lib/callsp-defs.i }

{tt/state.i &tableAlias="ttState"}
{tt/agentfilter.i}

define variable pStateID   as character no-undo.
define variable pStatus    as character no-undo.
define variable pUID       as character no-undo.

pRequest:getParameter("StateID", OUTPUT pStateID).
pRequest:getParameter("Status", OUTPUT pStatus).

pUID = pRequest:uid.
     
{lib/callsp.i &name=spGetStates &load-into=ttState &params="input pStateID, input pStatus" &nocount=true}
{lib/callsp.i &name=spAgentFilter &load-into=agentfilter &params="input '', input pUID" &nocount=true}

std-in = 0.
for each ttState exclusive-lock:
  /* assume the user cannot see the agent */
  std-lo = false.
  for each agent no-lock
     where agent.stateID = ttState.stateID:
     
    /* see if they can */
    if not can-find(first agentfilter where agentfilter.agentID = "ALL") and 
       can-find(first agentfilter where agentfilter.agentID = agent.agentID)
     then std-lo = true.
  end.
  /* if they cannot, then delete the state */
  if not std-lo and not can-find(first agentfilter where agentfilter.agentID = "ALL") 
   then delete ttState.
   else std-in = std-in + 1.
end.

if std-in > 0
 then pResponse:setParameter("State", table ttState).
pResponse:success("2005", string(std-in) {&msg-add} "State").
