/*------------------------------------------------------------------------
@name  GetLicensedStates.cls
@action licensedStatesGet
@description  Get the Licensed states.
@returns Success;2005
@author Vignesh Rajan
@version 1.0
@created 04.04.2023
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/

class state.GetLicensedStates inherits framework.ActionBase:
  
  {lib/callsp-defs.i }
  {tt/state.i &tableAlias="State" &licensedStatesGet=true}

  constructor public GetLicensedStates ():
   super().
  end constructor.

  destructor public GetLicensedStates ( ):
  end destructor.

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}
    
    define variable dsHandle     as handle    no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer State:handle).

    {lib\callsp.i 
      &name=spGetLicensedStates 
      &load-into=State 
      &params="input pRequest:UID"
      &noResponse=true}
      
    if can-find(first State) 
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2005", string(csp-icount) {&msg-add} "State").
     
  end method.

end class.



