/*------------------------------------------------------------------------
@name NewStateSource.cls
@action stateSourceNew
@description  Creates a new State Source record.
@param ReturnOrder;logical  
@throws 3005;Data Parsing Failed 
@throws 3001; Source Type is invalid
@throws 3000;creating new product failed
@throws 3066;State does not exist
@throws 3066;RevenueType does not exist
@returns success;2002;State Source has been created.
@author Sachin
@version 1.0
@created 05.03.2024
@Modified :
Date        Name          Comments  

----------------------------------------------------------------------*/

class state.NewStateSource inherits framework.ActionBase:
    
  {tt/statesource.i &tableAlias="ttStatesource"}
  {lib/removeNullValues.i}
          
  constructor NewStateSource ():
    super().    
  end constructor.

  destructor public NewStateSource ( ):        
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
          
    {lib/std-def.i} 
    
    define variable lSuccess       as logical    no-undo. 
    define variable lReturnRecord  as logical    no-undo.
    define variable cStateID       as character  no-undo.
    define variable cSourceType    as character  no-undo.
    
    define variable dsHandle as handle no-undo.

    create dataset  dsHandle. 
    dsHandle:serialize-name = 'data'.       
    dsHandle:set-buffers(buffer ttStatesource:handle).
         
    pRequest:getParameter("returnRecord", output lReturnRecord).

    if lReturnRecord = ?
     then
      lReturnRecord = no.
    
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end. 
    
    for first ttStatesource no-lock:
      if not can-find (first state where state.stateID = ttStatesource.stateID)
       then
        do:
          pResponse:fault("3066", "StateID"). 
          return.
        end.

      if ttStatesource.sourceType <> 'P'
       then
        do:
          pResponse:fault("3001", "Source Type").
          return.
        end.

      if not can-find (first arrevenue where arrevenue.revenueType = ttStatesource.revenueType)
       then
        do:
          pResponse:fault("3066", "revenue Type"). 
          return.
        end.

      if can-find (first statesource where statesource.stateID    = ttStatesource.stateID and 
                                           statesource.sourcetype = ttStatesource.sourcetype)
       then
        do:
          pResponse:fault("3015", "State Source" {&msg-add} "State and Revenue Type").
          return.
        end.
    end.    
            
    lSuccess = false.

    removeNullValues("ttStatesource").
    
    trxBlock:
    do transaction
      on error undo trxBlock, leave trxBlock:
    
      for first ttStatesource: 
         
        create statesource.
        assign
          statesource.stateID          =  ttStatesource.stateID  
          statesource.sourceType       =  ttStatesource.sourceType 
          statesource.revenueType      =  ttStatesource.revenueType
          statesource.createddate      =  now
          statesource.createdby        =  pRequest:uid
          statesource.lastModifiedDate =  ?
          statesource.lastModifiedBy   =  ''
          cStateID                     = statesource.stateID
          cSourceType                  = statesource.sourceType
          .      
    
        validate statesource. 
        release  statesource.  
      end.  
      lSuccess = true.
    end.       
    
    if not lSuccess
     then
      do:
        pResponse:fault("3000", "Create").
        return.
      end.  
      
    if lReturnRecord
     then
      do:
        for first statesource no-lock
         where statesource.stateID    = cStateID and 
               statesource.sourceType = cSourceType:
          empty temp-table ttStatesource.
          create ttStatesource.
          assign
              ttStatesource.stateID          =  statesource.stateID  
              ttStatesource.sourceType       =  statesource.sourceType 
              ttStatesource.revenueType      =  statesource.revenueType
              .
            
          for first state no-lock where state.stateID = statesource.stateID:
            ttStatesource.stateName = state.description.  
          end.
          
        end.
    
        setContentDataSet(input dataset-handle dsHandle, input pRequest:FieldList).
      end.
    
    pResponse:success("2002","State Source").
                           
  end method.

end class.


