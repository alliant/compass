/*------------------------------------------------------------------------
@name  GetStateSource.cls
@action StateSourceGet
@description  Get the State Source data.
@returns Success;2005;State Source(s) were returned
@author Sachin
@version 1.0
@created 05-07-2024
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/


class state.GetstateSource inherits framework.ActionBase:
    
  {tt/statesource.i &tableAlias="ttStateSource"}
      
  constructor GetStateSource ():
    super().
  end constructor.

  destructor public GetStateSource ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
          
    {lib/std-def.i}
               
    define variable pcStateID    as character no-undo.
    define variable pcSourceType as character no-undo.
    
    define variable dsHandle     as handle    no-undo.
    
    create dataset dsHandle.  
    dsHandle:serialize-name = 'data'.      
    dsHandle:set-buffers(buffer ttStateSource:handle). 
    
    pRequest:getParameter("StateID",output pcStateID).
    pRequest:getParameter("SourceType", output pcSourceType).
    
    if pcStateID = "" or pcStateID = ?
     then 
      pcStateID = "ALL".

    if pcSourceType = "" or pcSourceType = ?
     then 
      pcSourceType = "ALL".

   for each statesource no-lock where statesource.stateID = (if pcStateID = "ALL" then statesource.stateID else pcStateID) and 
                                      statesource.sourcetype = (if pcSourceType = "ALL" then statesource.sourceType else pcSourceType):
     create ttStateSource.
     assign
         ttStateSource.stateID    = statesource.stateID
         ttstatesource.sourcetype = statesource.sourcetype
         ttstatesource.revenuetype  = statesource.revenuetype
         .

      for first state no-lock where state.stateID = ttstatesource.stateID:
        ttstatesource.stateName = state.description.
      end.

      std-in = std-in + 1.
    end.
    
    if can-find(first ttstatesource)
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
      
    pResponse:success("2005", string(std-in) {&msg-add} "State Source").
       
  end method.
    
end class.


