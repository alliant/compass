/*------------------------------------------------------------------------
@name StateFormsGet
@description Provides either a complete list of all forms for all states, all forms for a state, or a specific form in a state
@param StateID;char;State identifier comma-delimited list (optional)
@param FormID;char;Form identifier (optional)
@param Active;logical;Active status (optional)
@param InsuredType;char;Insured Type code comma-delimited list (optional)
@returns StateForms;complex;State form dataset
@returns Success;int;Record count
@returns Success;int;2005
@author Russ Kenny
@version 1.1 7.2.2014 D.Sinclair
@notes 1.0 12/16/2013
Modified
Date       Name    Description
01-25-2022 Shefali Task:89648 - Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"               

----------------------------------------------------------------------*/

define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

/* Local Variables */
{lib/std-def.i}
{lib/callsp-defs.i}

/* Parameters */
define variable tStateID as character no-undo.
define variable tFormID as character no-undo.
define variable tUseActive as logical no-undo.
define variable tActive as logical no-undo.
define variable tInsuredType as character no-undo.
define variable tFormType as character no-undo.

/* Stored Procedure */
define variable hTable as handle no-undo.

/* Temp Table */
{tt/stateform.i &tableAlias="ttStateForm"}

pRequest:getParameter("StateID", OUTPUT tStateID).
if tStateID = ""
 then tStateID = "ALL".
pRequest:getParameter("FormID", OUTPUT tFormID).
pRequest:getParameter("FormType", OUTPUT tFormType).
pRequest:getParameter("InsuredType", output tInsuredType).
tUseActive = pRequest:getParameter("Active", output tActive).
{lib\callsp.i &name=spGetStateForms &load-into=ttStateForm &params="input tStateID, input tFormID, input tFormType, input tInsuredType"  &noCount=true &noresponse=true}

std-in = 0.
for each ttStateForm exclusive-lock:
  std-lo = false.
  if tUseActive and ttStateForm.active <> tActive
   then std-lo = true.

  if std-lo
   then delete ttStateForm.
   else std-in = std-in + 1.
end.

IF std-in > 0
 THEN pResponse:setParameter("StateForm", table ttStateForm).

pResponse:success("2005", STRING(std-in) {&msg-add} "State Form").

