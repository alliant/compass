&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name deleteregion.p
@action regionDelete
@description Deletes a region

@param RegionID;char;The region ID

@author John Oliver
@created 09.17.2018
@modification
date         Name           Description
06/09/2022   SA             Task 93485 - Changed logics to use "syscode"
                            table instead of "sysprop" table. 
------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

/* Parameters */
define variable pRegionID as character no-undo.
define variable pDescription as character no-undo.
define variable pStateList as character no-undo.
{lib/std-def.i}

/* inserting variables */
&scoped-define appCode "AMD"
&scoped-define objAction "Region"

/* local variables */
define variable tBadState as character no-undo initial "".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

pRequest:getParameter("RegionID", output pRegionID).
{lib/validate-syscode.i &codeType="'Region'" &code=pRegionID}
if not std-lo
 then pResponse:fault("3027", "Region ID" {&msg-add} std-ch).

if pResponse:isFault()
 then return.

std-lo = false.
TRX-BLOCK:
for each syscode exclusive-lock
   where syscode.codeType = 'Region'
     and syscode.code = pRegionID
      on error undo TRX-BLOCK, leave TRX-BLOCK:
  
  for each state exclusive-lock 
    where state.region = syscode.code:
    state.region = ''.
  end.

  delete syscode.
  
  std-lo = true.
end.

if not std-lo
 then
  do: pResponse:fault("3000", "Delete Region").
      return.
  end.

pResponse:success("2008", "Delete Region").
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


