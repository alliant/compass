/*------------------------------------------------------------------------
@name ModifyStateSource.cls
@action StateSourceModify
@description  Modify the existing state Source record
@param StateID;Character
@param SourceTypeID;Character
@param RevenueType;Character
@throws 3066;State does not exist
@throws 3001; Source Type is invalid
@throws 3066;RevenueType does not exist

@throws 3000;State Source modify failed 
@returns Success;2000
@author Sachin
@created 05.06.2024
@Modified :
Date        Name          Comments   

----------------------------------------------------------------------*/

class state.ModifyStateSource inherits framework.ActionBase:
    
  {tt/statesource.i &tableAlias="ttStateSource"}
  {lib/removeNullValues.i}
      
  constructor ModifyStateSource ():
    super().
  end constructor.

  destructor public ModifyStateSource ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
          
    {lib/std-def.i}

    /* variable */
    define variable lSuccess        as logical   no-undo.
    define variable plReturnRecord  as logical   no-undo init false.  
    define variable cStateID        as character  no-undo.
    define variable cSourceType     as character  no-undo.
    
    /* dataset handle */
    define variable dsHandle        as handle no-undo.
    
    /* buffer */
    define buffer bStatesource for statesource.
 
    /* parameter get */
    pRequest:getParameter("returnRecord", output plReturnRecord).

    /* dataset creation */
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttStateSource:handle). 

    if plReturnRecord = ?
     then
      plReturnRecord = no.

    /*dataset get*/
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.
    
    for first ttStatesource no-lock:
      if not can-find (first state where state.stateID = ttStatesource.stateID)
       then
        do:
          pResponse:fault("3066", "StateID"). 
          return.
        end.

      if ttStatesource.sourceType <> 'P'
       then
        do:
          pResponse:fault("3001", "Source Type").
          return.
        end.

      if not can-find (first arrevenue where arrevenue.revenueType = ttStatesource.revenueType)
       then
        do:
          pResponse:fault("3066", "revenue Type"). 
          return.
        end.

      if not can-find (first statesource where statesource.stateID    = ttStatesource.stateID    and
                                               statesource.sourcetype = ttStatesource.sourcetype)
       then
        do:
          pResponse:fault("3067", "State Source" {&msg-add} "State and Revenue Type" {&msg-add} ttStatesource.stateID  + " and " +  ttStatesource.sourcetype).
          return.
        end.

      if can-find (first statesource where statesource.stateID     = ttStatesource.stateID and 
                                           statesource.sourcetype  = ttStatesource.sourcetype and 
                                           statesource.revenuetype = ttStatesource.revenuetype)
       then
        do:
          pResponse:fault("3015", "State Source" {&msg-add} "State, Source Type and Revenue Type").
          return.
        end.
    end.    
        

    removeNullValues("ttStatesource").

    trxBlock:
    for first statesource exclusive-lock
      where statesource.stateID    = ttStatesource.stateID   and
            statesource.sourceType = ttStatesource.sourceType transaction
      on error undo trxBlock, leave trxBlock:

      assign
          statesource.revenueType      =  ttStatesource.revenueType
          statesource.lastModifiedDate = now
          statesource.lastModifiedBy   = pRequest:uid
          cStateID                     = statesource.stateID
          cSourceType                  = statesource.sourceType
          .

      validate statesource.
      release statesource.

      lSuccess = true.

    end. /* for first statesource */
    
    if plReturnRecord
     then
      do:
        for first statesource no-lock
          where statesource.stateID    = cStateID and
               statesource.sourceType  = cSourceType:
          empty temp-table ttStatesource.
          create ttStatesource.
          assign
              ttStatesource.stateID          =  statesource.stateID
              ttStatesource.sourceType       =  statesource.sourceType
              ttStatesource.revenueType      =  statesource.revenueType
              .

          for first state no-lock where state.stateID = statesource.stateID:
            ttStatesource.stateName = state.description.
          end.

        end.
      end.

    if not lSuccess
     then
      do:
        presponse:fault("3000", "State Source").
        return.
      end. /* if not lSuccess */

    if plReturnRecord
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
    
    pResponse:success("2000", "State Source").
            
  end method.
        
end class.


