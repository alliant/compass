/*------------------------------------------------------------------------
@name  DeleteStateSource.cls
@action StateSourceDelete
@description  Delete the State Source record.
@returns Success;2008;State Source has been removed
@author Sachin
@version 1.0
@created 05-07-2024
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/


class state.DeleteStateSource inherits framework.ActionBase:
    
  {tt/statesource.i &tableAlias="ttStateSource"}
      
  constructor DeleteStateSource ():
    super().
  end constructor.

  destructor public DeleteStateSource ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
          
    {lib/std-def.i}
    
    /*parameter variables*/
    define variable pcStateID    as character no-undo.
    define variable pcSourceType as character no-undo.
                
    pRequest:getParameter("StateID", output pcStateID).
    pRequest:getParameter("SourceType", output pcSourceType).

    /* validation when no parameter is supplied */
    if (pcStateID = ""  or pcStateID = ?)
     then 
      do:
        pResponse:fault("3001", "StateID").
        return.
      end.

    if (pcSourceType = ""  or pcSourceType = ?)
     then 
      do:
        pResponse:fault("3001", "SourceType").
        return.
      end.

    /* validate statesource */
    if not can-find(first statesource where statesource.stateID    = pcStateID and
                                            statesource.sourceType = pcSourceType )
     then
      do: 
        pResponse:fault("3066", "State Source record").
        return.
      end.

    std-lo = false.
    
    trxBlock:
    for first statesource exclusive-lock
      where statesource.stateID    = pcStateID and
            statesource.sourceType = pcSourceType transaction
      on error undo trxBlock, leave trxBlock:
      
      delete statesource.
      release statesource.
      
      std-lo = true.
    end.
    
    if not std-lo
     then
      do: 
        pResponse:fault("3000", "Delete State Source").
        return.
      end.
    
    pResponse:success("2008", "State Source").
    
  end method.
      
end class.


