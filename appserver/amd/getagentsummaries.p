/*---------------------------------------------------------------------
@name Compass
@action crmAgentsGet
@description Get the agents for the CRM tool

@return agentsummary;complex;The agent crm temp-table

@author John Oliver
@version 1.0
@created 2017-03-03
@notes 
@modification
Date          Name          Description
09/22/2020    VJ            Moved agent filter logic into sql
01-25-2022    Shubham       Task:89648 - Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"
---------------------------------------------------------------------*/

define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

{tt/agentsummary.i}
{lib/std-def.i}
{lib/callsp-defs.i}

/* parameters */
define variable pAgentID as character no-undo.
define variable pDate as datetime no-undo.
define variable pFilter as character no-undo.
define variable pUID as character no-undo.

/* local variables */
define variable cStateList as character no-undo initial "ALL".

/* get the parameters */
pRequest:getParameter("AgentID", output pAgentID).
if pAgentID = ""
 then pAgentID = "ALL".

pRequest:getParameter("Date", output pDate).

pUID = pRequest:uid.

if pResponse:isFault()
 then return.
 
/* {lib/validate-agent-parameter.i} */ /* This logic is moved into stored procedure */

{lib\callsp.i 
    &name=spGetAgentSummaries 
    &load-into=agentsummary 
    &params="input pAgentID, input cStateList, input pDate, input pUID"}
    
pResponse:success("2005", string(csp-icount) {&msg-add} "Agent").
