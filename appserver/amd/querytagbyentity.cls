/*------------------------------------------------------------------------
@file querytagbyentity.p
@action  tagByEntityQuery
@description Get the tags by entity
@returns The table dataset

@author sagar K
@created 16.01.2023
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/


class amd.querytagbyentity inherits framework.ActionBase:

  constructor public querytagbyentity ():
   super().
  end constructor.

  destructor public querytagbyentity ( ):
  end destructor.
    
  
  {tt/tagbyentity.i &tableAlias="tagby"}

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    /* variables for getParameter */
    define variable pEntity as character no-undo.
    define variable iCount  as integer   no-undo.

    define variable dsHandle as handle no-undo.
    {lib/std-def.i}
    {lib/callsp-defs.i}

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer tagby:handle).

    /* Getting the parameters */
    pRequest:getParameter("Entity", output pEntity).
    if pEntity = "" or pEntity = "B"
     then pEntity = "ALL".

    /* if there is a fault thrown within the procedure, exit */
   if pResponse:isFault()
    then return.
    
   std-in = 0.

   for each tag no-lock
     where tag.entity = if pEntity = "ALL" then tag.entity else pEntity break by tag.name:

     if first-of(tag.name) then
      do:
        create tagby.
        assign 
           tagby.name = tag.name
           iCount    = 1
           std-in = std-in + 1 .
      end.
     else  
       assign iCount = iCount + 1.
        
     assign tagby.tagscount = iCount.

   end.
   
    if std-in > 0 
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2005", string(std-in) {&msg-add} " Tag"). 
     
  end method.

end class.


