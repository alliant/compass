/*------------------------------------------------------------------------
@file tagagents-p.p
@description create system tags

@author sagar K
@created 02.12.2024
@notes  
----------------------------------------------------------------------*/
define input  parameter piAgentID      as character no-undo.


{lib/callsp-defs.i }
/* parameter variables */
define variable cTempPath       as character no-undo.
define variable cToAddress      as character no-undo.

define temp-table duplicatetag
   field errmeg  as char.

/* Fetching Email address from INI file */ 
publish "GetSystemParameter" ("AlertsTo", output cToAddress).

empty temp-table duplicatetag.

{lib\callsp.i 
        &name=spTagAgents 
        &load-into=duplicatetag 
        &params="input piAgentID"
        &noResponse=true}
        
if not csp-lSucess and csp-cErr > ''
 then
  do:
    create duplicatetag.
    
    duplicatetag.errmeg = csp-cErr.
        
  end.

if can-find(first duplicatetag)
 then
  do:
    publish "GetSystemParameter" from (session:first-procedure) ("TempFolder", output cTempPath).

    cTempPath = cTempPath + "\Errors_systemTagsCreation.csv".

    output TO VALUE (cTempPath) .
        
    for  each  duplicatetag :
      export  delimiter  "," duplicatetag.
    end.
    output  close.
  

    /* Email not required as of now */
    /* send the email */
    if search(cTempPath) <> ?
     then
      do:
        run util/attachmail.p ("no-reply@alliantnational.com", /* from loggedIN user*/
                               cToAddress,     /* To loggedIN use "pRequest:uid"*/
                                "",                             /* CC */
                               "Error(s) creating system tags",   /* Subject */
                               "System Tag create is failed.",                             /* Body */
                                cTempPath).                     /* Attachment */
      end.
  end.
