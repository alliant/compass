/*----------------------------------------------------------------------
@file getagentnprolap.p
@action AgentNPROLAPGet
@description 
@returns The table dataset

@author sagar K
@created 18.04.2023
@notes
----------------------------------------------------------------------*/

define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

/* parameter variables */
define variable ddate as character no-undo.
define variable pyear as character no-undo.
define variable pmonth as character no-undo.
define variable polapDataSource as character no-undo.
define variable polapCatalog as character no-undo.
define variable pUID as character no-undo.
define variable dsHandle as handle no-undo.

{lib/callsp-defs.i}
{lib/std-def.i}
{tt/nprdetailsolap.i}

publish "GetSystemParameter" ("OLAPDataSource", output polapDataSource).
publish "GetSystemParameter" ("OLAPCatalog", output polapCatalog).

if polapDataSource = "" or polapDataSource  =  "NULL" 
  then do:
    pResponse:fault("3005", "DataSource cannot be blank.").
    return.
  end.

if polapCatalog = "" or polapCatalog  =  "NULL" 
  then do:
    pResponse:fault("3005", "Catalog cannot be blank.").
    return.
  end.

for last period where period.active = true by period.periodID desc:
   pmonth = string(period.periodmonth).
   pyear  = string(period.periodYear).
end.

if pmonth = "" or pyear = "" then
 do:
   for last period  by period.periodYear desc:
     pmonth = string(period.periodmonth).
     pyear  = string(period.periodYear).
   end.
 end. 

pUID = pRequest:UID.

{lib\callsp.i &name=OlapspGetAgentNPRDetails &load-into=nprdetailsolap &params="input pyear,input pmonth, input polapDataSource,input polapCatalog,input pUID"}

pResponse:setParameter("Agentnprolap", table nprdetailsolap). 
pResponse:setParameter("Month",pmonth).
pResponse:setParameter("Year",pyear).
pResponse:success("2005", string(csp-icount) {&msg-add} "NPR OLAP Netpremium details").
 
