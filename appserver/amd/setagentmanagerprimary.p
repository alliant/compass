&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file setagentmanagerprimary.p
@description Sets an agent manager primary

@param ManagerID;int;The manager id

@success 2002;The agent manager was set to primary

@throws 3000;The create failed
@throws 3066;Does not exist in table

@uthor John Oliver
@created 05.26.2017
@notes
Date        Name           Description
10/17/18    Naresh Chopra  Modfied to add new input Parameter "ExcludeFieldList" in buildsysindex.p call.
02/14/24    SRK            Modified to create tag
----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEF INPUT PARAM pRequest AS service.IRequest.
DEF INPUT PARAM pResponse AS service.IResponse.

{lib/std-def.i}
{lib/encrypt.i}

/* variables for getParameter */
define variable pManagerID as integer no-undo.
define variable pPrimary as logical no-undo.

/* local variables */
define variable cAgentID as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* Start getting the parameters */
pRequest:getParameter("ManagerID", OUTPUT pManagerID).
if not can-find(first agentmanager where managerID = pManagerID)
 then pResponse:fault("3066", "Manager " + string(pManagerID)).
 
pRequest:getParameter("Primary", OUTPUT pPrimary).

/* if there is a fault thrown within the procedure, exit */
if pResponse:isFault()
 then return.
 
/* all parameters are valid so start to insert the new row */
std-lo = false.
TRX-BLOCK:
for first agentmanager exclusive-lock
    where agentmanager.managerID = pManagerID TRANSACTION
    on error undo TRX-BLOCK, leave TRX-BLOCK:
  
  agentmanager.isPrimary = pPrimary.
  for first agent exclusive-lock
      where agent.agentID = agentmanager.agentID:
    cAgentID = agentmanager.agentID.
    if pPrimary
     then agent.manager = agentmanager.uid.
     else agent.manager = "".
  end.
  
  std-lo = true.
end.

if not std-lo
 then
  do: pResponse:fault("3002", "Set as Primary").
      return.
  end.
 else
  do:
    if pPrimary
     then
      for each agentmanager exclusive-lock
          where agentmanager.agentID = cAgentID and agentmanager.managerID <> pManagerID : 
        agentmanager.isPrimary = false.
      end.
    run amd/tagagents-p.p ( input cAgentID).
  end.

/* Build keyword index to use for searching
*/
std-ch = "for each agentmanager where agentID = '" + cAgentID + "' and managerID = " + string(pManagerID) + "  and stat = 'A'".
run sys/buildsysindex.p (input pRequest,
                         input pResponse,
                         input "AgentManager",  /* ObjType */
                         input pManagerID,      /* ObjAttribute */
                         input cAgentID,        /* ObjID */
                         input "agentmanager",  /* SourceTable */
                         input std-ch,          /* SourceQuery */
                         input "").             /* ExcludeFieldList */

pResponse:success("2000", "Set as Primary").
pResponse:updateEvent("Agent", cAgentID).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


