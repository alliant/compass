/*------------------------------------------------------------------------
@file getagentcommunicationolap.p
@action  agentCommunicationolapGet
@description 
@returns The table dataset

@author sagar K
@created 11.06.2023
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/


class amd.GetAgentCommunicationOlap inherits framework.ActionBase:

  constructor public GetAgentCommunicationOlap ():
   super().
  end constructor.

  destructor public GetAgentCommunicationOlap ( ):
  end destructor.
    
  {tt/agentcommunication.i}

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    define variable lError         as logical   no-undo.
    define variable cErrMessage    as character no-undo.
    define variable std-in         as integer   no-undo.
    define variable dsHandle as handle no-undo.

    run amd/getagentcommunicationolap-p.p(input pRequest:UID, 
                                         output dataset-handle dsHandle,
                                         output lError,
                                         output cErrMessage,
                                         output std-in).
    
    if lError 
     then
      do:
       pResponse:fault ("3005",  "Agent Communication Failed: " + cErrMessage).
       return.
      end.
   
    if std-in > 0 
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2000", string(std-in) + "OLAP Agent Communication"). 
     
  end method.

end class.


