/*------------------------------------------------------------------------
@file querytagbyentitydetail
@description Get the tag detail
@param Entity;char;Person/Agent or Both
@param tagName;char;Tag Name
@author sagar K
@created 02.01.2023
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/


class amd.querytagbyentitydetail inherits framework.ActionBase:

  constructor public querytagbyentitydetail ():
   super().
  end constructor.

  destructor public querytagbyentitydetail ( ):
  end destructor.
    
  
  {tt/tag.i &tableAlias="data"}

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    /* variables for getParameter */
    define variable pEntity  as character no-undo.
    define variable pTagName as character no-undo.
    define variable icount   as integer   no-undo.

    define variable dsHandle as handle no-undo.
    {lib/std-def.i}
    {lib/callsp-defs.i}

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer data:handle).

    /* Getting the parameters */
    pRequest:getParameter("Entity", output pEntity).
    pRequest:getParameter("TagName", output pTagName).
    if pEntity = "" or pEntity = "B"
     then pEntity = "ALL".

    /* if there is a fault thrown within the procedure, exit */
   if pResponse:isFault()
    then return.
    
   std-in = 0.

   {lib\callsp.i 
        &name=spGetTagByEntity 
        &load-into=data 
        &params="input pEntity,input pTagName"
        &noResponse=true}
        
    std-in = csp-icount.
   
    if std-in > 0 
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2005", string(std-in) {&msg-add} " Tag"). 
     
  end method.

end class.


