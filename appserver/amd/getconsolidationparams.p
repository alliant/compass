/*----------------------------------------------------------------------
@name amd/getconsolidationparams.p
@action consolidationparamsget
@description Returns a dataset related to consolidate 
@param cConfigType

@success 2005;char;sysuserconfig
@success 3000;char;Fail

@author Sachin Anthwal
@version 1.0
@created 11/03/2020
Modification:
Date          Name      Description

----------------------------------------------------------------------*/

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

/* parameters */
define variable cConfigType as character  no-undo.

/* local variables */
define variable cUID as character no-undo.
 
pRequest:getParameter("ConfigType",output cConfigType).
                                                                           
/* Standard lib files */
{lib/std-def.i}

/* Temp-table Definitions */
{tt/sysuserconfig.i &tableAlias="tsysuserconfig"}
  
/* validate the cConfigType */
if cConfigType = "" 
 then 
  pResponse:fault("3001", "Config Type").

if pResponse:isFault() 
 then 
  return. 

cUID = pRequest:UID.

for each sysuserconfig no-lock 
  where sysuserconfig.configType begins cConfigType 
    and sysuserconfig.uid = cUID:
  
  create tsysuserconfig.
  buffer-copy sysuserconfig to tsysuserconfig.
  
  std-in = std-in + 1. 
end.

if std-in > 0 
 then 
  pResponse:setParameter("sysuserconfig", table tsysuserconfig).

pResponse:success("2005", string(std-in) {&msg-add} "sysuserconfig").
