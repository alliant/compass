&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*
@action FormTypesGet
@description Return the set of form type codes
@returns SysCode;complex
@returns Success;int;2005
@author D.Sinclair
@date 5.11.2014
@notes
*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

{tt/syscode.i &tableAlias="ttSysCode"}

{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


std-in = 0.
for each syscode no-lock
  WHERE syscode.codeType = "form":
 create ttSysCode.
 buffer-copy syscode to ttSysCode.
 std-in = std-in + 1.
end.

if std-in > 0
 then pResponse:setParameter("FormType", table ttSysCode).
pResponse:success("2005", string(std-in) {&msg-add} "Form Type")

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


