/*------------------------------------------------------------------------
@file GetOpenTasks.cls
@action openTasksGet
@description Get Open Tasks
@returns Open Tasks
@success 2005;X Open Task(s) were returned
@author Shefali
@version 1.0
@created 04/05/2023
Modification:

----------------------------------------------------------------------*/

class file.GetOpenTasks inherits framework.ActionBase:
    
  {tt/filetask.i}

  constructor GetOpenTasks ():
    super().
  end constructor.

  destructor public GetOpenTasks ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
	define variable pcStatus       as character no-undo.
	define variable pcQueueName    as character no-undo.
	define variable pcTaskName     as character no-undo.
    define variable lError         as logical   no-undo.
    define variable cErrMessage    as character no-undo.
    define variable std-in         as integer   no-undo.
	
    pRequest:getParameter("Status",    output pcStatus).
    pRequest:getParameter("QueueName", output pcQueueName).
    pRequest:getParameter("TaskName",  output pcTaskName).	

    define variable dsHandle as handle no-undo.

    run file/getopentasks-p.p(input pRequest:UID, /* UserName - for Permission */
                              input '',           /* Assigned To */
                              input pcStatus,
							  input pcQueueName,
                              input pcTaskName,
                              output dataset-handle dsHandle,
                              output lError,
                              output cErrMessage,
                              output std-in).
    
    if lError 
     then
      do:
       pResponse:fault ("3005",  "GetOpentask Failed: " + cErrMessage).
       return.
      end.

    if std-in > 0 then
      setcontentdataset(input dataset-handle dsHandle,input pRequest:FieldList).

    pResponse:success("2000", string(std-in) + " Open Task").
             
  end method.
      
end class.


