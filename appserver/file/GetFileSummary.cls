/*------------------------------------------------------------------------
@name  GetFileSummary.cls
@action fileSummaryGet
@description  Get file summary
@param Agentfileid;int

@author  Sagar Koralli 
@Modified :
Date        Name          Comments   
05/16/2023  SB            Modified to add render functionality  
----------------------------------------------------------------------*/
USING Progress.Json.ObjectModel.ObjectModelParser.
USING progress.Json.ObjectModel.*.

class file.GetFileSummary inherits framework.ActionBase:

  constructor public GetFileSummary ():
    super().
  end constructor.

  destructor public GetFileSummary ( ):
  end destructor.

  /* Global Variables*/
  define variable pResponse         as framework.IRespondable no-undo.
  define variable pRequest          as framework.IRequestable no-undo.
  define variable cDataFileName     as character              no-undo.
  define variable cAgentFileNumber  as character              no-undo.

  {framework/action-setfield-in.i AssignedCount}
 
  method public override void act (input pActRequest  as framework.IRequestable,
                                   input pActResponse as framework.IRespondable):

    define variable piAgentfileid as integer     no-undo.
    define variable piLocalZone   as datetime-tz no-undo.
    define variable lErr          as logical     no-undo.
    define variable cErrMessage   as character   no-undo.
    
    /* dataset handle */
    define variable dsHandle      as handle    no-undo.
    define variable cTempPath     as character no-undo.

    {lib/std-def.i}

    assign 
        pResponse = pActResponse
        pRequest  = pActRequest
        .

    pRequest:getParameter("AgentFileID", output piAgentfileid).
    pRequest:getParameter("LocalZone", output piLocalZone).

    if piAgentfileid = 0 or piAgentfileid = ?
     then
      do:
        pResponse:fault("3005","AgentFileID is invalid").
        return.
      end.
    
    for first agentfile no-lock where agentfile.agentfileID = piAgentfileid:
      assign cAgentFileNumber = agentfile.filenumber.
    end.
    run file/getfilesummary-p.p(input  piAgentfileid,
                                input  pRequest:actionID,
                                input  piLocalZone,
                                output dataset-handle dsHandle,
                                output lErr,
                                output cErrMessage).

    dshandle:serialize-hidden = true.
    
    if lErr 
      then
       pResponse:fault("3005", cErrMessage).
    else
      do:  
        publish "GetSystemParameter" from (session:first-procedure) ("TempFolder", output cTempPath).
        cDataFileName = cTempPath + 'renderFileDocument-' + string(piAgentFileID) + '.json' .
        std-lo = dsHandle:write-json('file', cTempPath + 'renderFileDocument-' + string(piAgentFileID) + '.json',yes) no-error.

        if not std-lo or error-status:error
         then
          do:
            std-ch = "".
            do std-in = 1 to error-status:num-messages:
              std-ch = if std-ch = "" then error-status:get-message(std-in) else std-ch + "~n" + error-status:get-message(std-in).
            end.
    
            pResponse:fault("3005", "fileDocumentRender failed. " + std-ch).
            return.
          end.
      end.
      
  end method.

  method public override char render(input pFormat as char, input pTemplate as char):
    define variable cUrl          as character no-undo.
    define variable pMsg          as character no-undo.
    define variable pResponseFile as character no-undo.
    define variable pSuccess      as logical   no-undo.
    define variable cFileName     as character no-undo.
  
    if pResponse:isfault() then return ''.
  
    publish "GetSystemParameter" from (session:first-procedure) ("RenderDocumentAPI", output cUrl).
  
    for first sysaction where action = pRequest:actionID no-lock:
      for first template where template.TemplateId = sysaction.templateID:
        cUrl = cUrl + '/' + template.schemaname + '/' + template.filename + '/' + "html".
        run util/httppost.p("",
                            cUrl,
                            "",
                            "application/json",
                            cDataFileName,
                            output pSuccess,
                            output pMsg,
                            output pResponseFile).
        
        cFileName = "File" + cAgentFileNumber + '_' + template.filename + "_"  + replace(string(today,"99/99/9999"),"/","-") + "-" + replace(string(time,"HH:MM:SS AM"),":","-")  . 
      end.
    end.
    if not pSuccess 
     then
      do:
        if pMsg = '' 
          then 
            pMsg = ParseErrorResponse(pResponseFile).
        pResponse:fault("3005", "renderfiledocument failed." + pMsg).
  
        return ''.
      end.
    os-rename value(pResponseFile) value(cFileName + ".html").
    tContentFile = cFileName + ".html".
  
    pResponse:success("2000", "fileDocumentRender").
    return tContentFile.
  end. 
  
  method private character ParseErrorResponse(input pResponseFile as character):
    define variable oParser            as ObjectModelParser no-undo.
    define variable oJsonObjectParent  as JsonObject        no-undo.
    define variable oJsonObjectChild   as JsonObject        no-undo.
    define variable cFaultJsonText     as character         no-undo.
    define variable cErrMsg            as character         no-undo.
  
    if pResponseFile = '' or pResponseFile = ?
     then
       return 'Render Service Failed'.
  
    /* Parse the JSON file into a JSON object */
    oParser = NEW ObjectModelParser().
    oJsonObjectParent = CAST(oParser:ParseFile(pResponseFile), JsonObject).  
    
    /* Parse JSON text into a JSON object*/
    cFaultJsonText = oJsonObjectParent:GetJsonText("fault") no-error. 
    oJsonObjectChild = CAST(oParser:Parse(cFaultJsonText), JsonObject).  
    
    cErrMsg = oJsonObjectChild:getcharacter("message").
  
    delete object oParser.
    delete object oJsonObjectParent.
    delete object oJsonObjectChild.
  
    return if cErrMsg <> ? then cErrMsg else ''.
  end.
 
end class.

