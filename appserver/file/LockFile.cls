/*------------------------------------------------------------------------
@name  file/LockFile.cls
@action fileLock
@description  Locks the agentfileID of current user in syslock table, if 
             agentfileID is not locked and not assigned to another user.  

@param AgentID;char;ID of an Agent
@param FileNumber;char;The file number of an Agent
@param agentFileID;int;The unique sequence fileID of an Agent


@throws 3005;Invalid AgentID, FileNumber and FileID
            ;Cannot create lock
@throws 3001;AgentFileID does not exist
            ;AgentID does not exist
@throws 3005;Cannot create lock			
@throws 3081;AgentFileID is locked by other user
@throws 3085;The fileID is assigned to any other user
@throws 3000;Agent File Lock failed

@returns AgentFile;complex;File dataset for all child tables
@returns Success;int;2000;Agent File Lock was successful
			
@author Shweta Dhar

@Modified    :
    Date        Name                 Comments   
    04/18/23    Sachin Chaturvedi    Modified to add Task Creation
    07/04/23    Sagar K              Added agentfile tables new fields
    08/24/23    K.R                  Modified to run create mailbox program async manner
    01/17/2024  K.R                  Removed any orderid related code
    01/24/2024  K.R                  Modified to remove creating agentfile logic to seperate .p
    08/02/2024  K.R                  Modified to check for fulfillment lock
    08/07/2024  S.R                  Modified to add calculated duedate on creation of task.
----------------------------------------------------------------------*/

class file.LockFile inherits framework.ActionBase:
  {lib/nextkey-def.i} 

  define variable pRequest  as framework.IRequestable. 
  define variable pResponse as framework.IRespondable.

  define variable dsHandle         as handle    no-undo.
  define variable pcAgentID        as character no-undo.
  define variable pcFileNumber     as character no-undo.
  define variable piAgentFileID    as integer   no-undo.
  define variable piOrderID        as integer   no-undo.
  define variable cErrMessage      as character no-undo.
  define variable lSuccess         as logical   no-undo.
  define variable lLocked          as logical   no-undo.
  define variable lcreatetask      as logical.
  define variable cFileID          as character no-undo.
  define variable cFilePrefix      as character no-undo.
  define variable cUsername        as character no-undo.
  define variable cStateID         as character no-undo.
  define variable iTaskID          as integer   no-undo.
  define variable iFileTaskID      as integer   no-undo.
  define variable lReturnVal       as logical   no-undo. 
  define variable cErrorMsg        as character no-undo.
  define variable cMsg             as character no-undo.
  
  /* normalizeFileID method */
  {lib/normalizefileid.i &method="true"}

  constructor public LockFile():
    super().
  end constructor.

  destructor public LockFile():
  end destructor.

  method public override void act (input pActRequest  as framework.IRequestable,
                                   input pActResponse as framework.IRespondable):

    define variable oAsyncRun as framework.AsyncRun.
    
    {lib/std-def.i}
    
    pRequest  = pActRequest.
    pResponse = pActResponse.

    pRequest:getParameter("AgentFileID", output piAgentFileID).
    pRequest:getParameter("AgentID",     output pcAgentID).
    pRequest:getParameter("FileNumber",  output pcFileNumber).    
  

    /* validation when no parameters are supplied */
    if (piAgentFileID = 0  or piAgentFileID = ?) and
       (pcAgentID     = "" or pcAgentid     = ?) 
     then
      do:
        pResponse:fault("3005", "Lock Agent File Failed. Either supply Agent ID and File Number or supply AgentFileID or supply OrderID").
        return.
      end.

    /* validate agentfileID if supplied */
    if piAgentFileID > 0 then
     do:
      if not can-find(first agentFile where agentFile.agentFileID = piAgentFileID ) 
       then 
        do:
          pResponse:fault("3001", "AgentFileID").  
          return.
        end.

       /*check and lock file*/
      lockAgentFile(input  piAgentFileID, output lSuccess).
      if not lSuccess 
       then
        return.

      /* Create jobtran record */
      createJobTran(input piAgentFileID).
  
      /* Retrieve the file and set output parameter */
      getFile(input piAgentFileID).
      return.
    end.

    /* normalize filenumber */
    cFileID = normalizeFileID(pcFileNumber). 

    /* search for agentfile using agentid and filenumber 
      if found then set the ID, get the lock and return the file */
    for first agentFile no-lock
     where agentFile.agentID = pcAgentID
       and agentFile.fileID  = cFileID:

      piAgentFileID = agentFile.agentFileID.
    
      /* find, check and lock file */
      lockAgentFile(input  piAgentFileID,
                   output lSuccess).
      if not lSuccess 
       then
        return.
       else
         pResponse:success("2000", "Agent File Lock" ). /* Agent File Lock is successfull.*/
       
      /* Create jobtran record */   
      createJobTran(input piAgentFileID).
      /* Retrieve the file and set output parameter */
      getFile(input piAgentFileID).
      return. 
    end.

    /* if agentFile not found then validate agentid */
    if pcAgentID <> "" and pcAgentID <> ? and pcAgentID <> "?" and not can-find(first agent where agent.agentID = pcAgentID)
     then
      do:
        pResponse:fault("3001", "AgentID").
        return.
      end.

    lSuccess = false.
 
    /* Create the agentfile record for supplied agentid and fileID */
    TRX-BLOCK:
    do transaction
       on error undo TRX-BLOCK, leave TRX-BLOCK:

      /* Allow only active or prospect agents */
      for first agent fields (stat) where agent.agentID = pcAgentID no-lock:
          if agent.stat = 'A' or agent.stat = 'P'  
           then.
           else
            do:
              pResponse:fault("3005", "Agent should be active or prospect.").
              undo TRX-BLOCK, leave TRX-BLOCK.
            end.
      end.

      run file/createagentfile-p.p(input pcAgentID,
                                   input pcFileNumber,
                                   input pRequest:UID,
                                   output piAgentFileID,
                                   output lSuccess,                                        
                                   output cMsg).
      if not lSuccess
       then
        do:
           pResponse:fault("3005", string(cMsg)).
           undo TRX-BLOCK, leave TRX-BLOCK.
        end.

      assign lcreatetask = true. 

      /* Check and lock file */
      lockAgentFile(input  piAgentFileID,
                    output lReturnVal).

      if not lReturnVal 
       then
        undo TRX-BLOCK, leave TRX-BLOCK.

      release agentFile. 
      lSuccess = true.
    end. /* end transaction */ 

    if pResponse:isFault() or not lSuccess
     then 
      do:
        /* by any chance if lock has been taken, then release the lock. */
        run util/deletesyslock.p ("AgentFile", piAgentFileID, 0, output lLocked).
   
      end.

    if lsuccess
     then
      do:
        /* Create jobtran record */
        createJobTran(input piAgentFileID).

        /* Retrieve the file and set output parameter */
        getFile(input piAgentFileID).
      end.


    if lSuccess and lcreatetask
     then
      do:
         /* File Task creation in compass */
         run file/newfiletask-p.p(input piAgentFileID,
                                  input "Order Entry",
                                  input "Production", /* my change*/
                                  input pRequest:UID,
                                  input now,
                                  input pRequest:UID,
                                  input 0.00,
                                  output iFileTaskID,
                                  output lReturnVal,
                                  output cErrorMsg
                                    ). 
         if not lReturnVal
          then
           do:
             message 'Lock File is Successful but failed to create File Task in compass.' + cErrorMsg.
             return.
           end.

         /* ARC Task creation */
         for first agentfile no-lock
          where agentfile.agentFileID = piAgentFileID:
           run file/createarctask-p.p(input "Production",
                                     input "Order Entry",
                                     input agentFile.agentFileID,
                                     input agentFile.fileNumber,
                                     input agentFile.fileID,
                                     input pRequest:UID,
                                     input calculateDueDate(), /* DueDate: fetching today's 5PM time if server time is below 5PM else fetching next day 5PM date time */ 
                                     output iTaskID,
                                     output lReturnVal,
                                     output cErrorMsg
                                    ).
           if lReturnVal 
            then
             do:
               message 'File Task created in compass but failed to create Task in ARC.' + cErrorMsg.
               return.
             end.
      
           FILETASK_UPD:
           do transaction 
            on error undo FILETASK_UPD, leave FILETASK_UPD:
             for first filetask exclusive-lock where filetaskid = iFileTaskID:
               filetask.taskid = iTaskID.
             end.
           end.

         end. /* for first agentfile */

         oAsyncRun = new framework.AsyncRun ("emailBoxCreate", pRequest:UID, 1, "").
         oAsyncRun:setActionParameters("UserName", string(piAgentFileID)).
         oAsyncRun:runAsync().

         if valid-object (oAsyncRun)
           then delete object oAsyncRun no-error.

      end. /* lcreatetask */

  end method.

  method private void  lockAgentFile(input iAgentFileID as integer, output lSuccess as logical):

    define variable lCanLock            as logical no-undo.
    define variable lLocked             as logical   no-undo.
    define variable cLockBy             as character   no-undo.
    define variable dLockDate           as date   no-undo.
    define variable lHasFulFillmentLock as logical no-undo.

    /* first check if the agentfile has FileFullfilment lock in that case we cannot have AgentFile lock on the file*/
    {lib/islocked.i &obj='FileFulFillment' &id=iAgentFileID &lock=lHasFulFillmentLock &uid=cLockBy &lockdate=dLockDate &exclude-fault=true}
    
    if lHasFulFillmentLock
     then
      do:
        pResponse:success("3005", "Lock failed. Fulfillment is in process").
        lSuccess = true.
        return.
      end.

    {lib/islocked.i &obj='AgentFile' &id=iAgentFileID &lock=lLocked &uid=cLockBy &lockdate=dLockDate &exclude-fault=true}

    lCanLock = (not lLocked) or (lLocked and cLockBy = pRequest:uid) .

    if lCanLock 
     then
      do:
        /*Lock File*/
        run util/newsyslock.p ("AgentFile", piAgentFileID, 0, pRequest:Uid, output lLocked).
        if not lLocked
        then 
         do:
           pResponse:fault("3000", "Agent File Lock").
           lSuccess = false.
           return. /* Unable to get lock for the file */
         end.
      end.

  if lCanLock /* created the lock and got the lock */
   then
    pResponse:success("2000", "Agent File Lock" ). /* Agent File Lock is successfull. */ 
    else
     pResponse:success("3005", "Lock failed. File is currently in use").


    lSuccess = true.

  end method.

  method public void getFile(input iAgentFileID as integer):

    run file/getagentfile-p.p(input  iAgentFileID,
                              input  pRequest:Uid,
                              output dataset-handle dsHandle,
                			  output lSuccess,
    						  output cErrMessage).

    if valid-handle(dsHandle) 
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

  end method.

  method private void createJobTran(input iAgentFileID as integer):

    run file/createjobtran-p.p(input iAgentFileID,                    
                             input pRequest:actionID ,                    
						     input pRequest:Uid,
                             input 'AgentID=' + pcAgentID +               
						           '|agentFileNumber=' + pcFileNumber + 
						     	   '|agentFileID=' + string(piAgentFileID),    
                             input dataset-handle dsHandle).

  end method.

  method public datetime-tz calculateDueDate(): 

    define variable dtzDueToday    as datetime-tz no-undo initial now. 
    define variable dtDueTommorrow as datetime    no-undo.           

    define variable dtzToday5PM    as datetime-tz no-undo.
    define variable dtzTomorrow5PM as datetime-tz no-undo.

    dtzToday5PM = datetime(month(dtzDueToday),day(dtzDueToday),year(dtzDueToday),17,0,0).

    if dtzDueToday < dtzToday5PM
     then
      return dtzToday5PM.
    else
    do:
      dtDueTommorrow = today + 1.
      dtzTomorrow5PM  = datetime(month(dtDueTommorrow),day(dtDueTommorrow),year(dtDueTommorrow),17,0,0).
   
      return dtzTomorrow5PM.
    end.

  end method.

end class.
