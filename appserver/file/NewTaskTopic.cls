/*------------------------------------------------------------------------
@name file/NewTaskTopic.cls
@action  taskTopicNew
@description  Creates a new task record.
@param  
@throws 3005;Data Parsing Failed  
@throws 3066;Task name does not exist
@throws 3005;Task topic record not found.
@throws 3005;Task name already exists.
@throws 3005;Created in compass but failed to create in ARC 
@throws 3000;Task create failed
@author S chandu
@version 1.0
@created 04/27/2023
@Modified :
Date        Name          Comments  
01/12/2024  SB            Modified to add validation for isSecure field.
03/19/2024  SB            Modified to handle isDeleted field.
----------------------------------------------------------------------*/

class file.NewTaskTopic inherits framework.ActionBase:
    
  {tt/tasktopic.i &tableAlias="ttTasktopic"}
  {lib/removeNullValues.i}

  constructor NewTaskTopic ():
    super().
  end constructor.

  destructor public NewTaskTopic ():
  end destructor.
 

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable plReturnRecord as logical   no-undo.
    define variable lSuccess       as logical   no-undo.
    define variable cTaskName      as character no-undo.
    define variable cMsg           as character no-undo.
    define variable dsHandle       as handle    no-undo.

    define variable lError         as logical   no-undo.
    define variable cErrMessage    as character no-undo.

     /* dataset creation */
    create dataset  dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttTasktopic:handle).

    /*Parameters get*/
    pRequest:getParameter("ReturnRecord",plReturnRecord).

    if plReturnRecord = ?
     then
      plReturnRecord = false.

    empty temp-table ttTasktopic.
    
    /* get Dataset */
    pRequest:getContent(input-output dataset-handle dsHandle, output cMsg).
    if cMsg > '' 
     then
      do:
        pResponse:fault('3005', "Data Parsing failed. Reason: " + cMsg).
        return.
      end.

    if not can-find(first ttTasktopic) 
     then
      do:
        pResponse:fault('3005', 'Task topic record not provided').
        return.
      end.
    
    for first ttTasktopic:

      if ttTasktopic.topic =  "" or 
         ttTasktopic.topic =  ?  
       then 
        do:
          pResponse:fault("3005", "Task topic cannot be blank/Invalid value").
          return.
        end.

      if can-find(first tasktopic where tasktopic.topic = ttTasktopic.topic and tasktopic.isDeleted = no)
       then
        do:
          pResponse:fault("3005", "Task topic already exists").
          return.
        end.

      if ttTasktopic.isSecure = yes 
       then
        do:  
          pResponse:fault("3005","Secured task topic cannot be created from TPS application. Please contact System Administrator."). 
          return.
        end.
    end.
    

    lSuccess = false.
    removeNullValues("ttTaskTopic").
    
    TRX-BLOCK:
    do transaction
      on error undo TRX-BLOCK, leave TRX-BLOCK:
	  
	  for first ttTasktopic no-lock:
        if not can-find(first tasktopic where tasktopic.topic = ttTasktopic.topic)
         then
          do:
            create tasktopic.
            assign
                tasktopic.topic        = ttTasktopic.topic
                tasktopic.description  = ttTasktopic.description
                tasktopic.active       = ttTasktopic.active
                tasktopic.isSecure     = no
                tasktopic.createddate  = now
                tasktopic.createdby    = pRequest:uid
                taskTopic.isDeleted    = no
                cTaskName              = ttTasktopic.topic
                .
	         
            validate tasktopic. 
            release  tasktopic.
          end.
        else 
         for first tasktopic where tasktopic.topic     = ttTasktopic.topic
                               and tasktopic.isDeleted = yes exclusive-lock:
           assign 
               tasktopic.isDeleted    = no
               tasktopic.active       = ttTasktopic.active
               tasktopic.description  = ttTasktopic.description
               cTaskName              = ttTasktopic.topic
               .
	       
           validate tasktopic. 
           release  tasktopic.
         end.
	    
        lSuccess = true.
	    
	  end.
    
    end.
    
    if not lSuccess
     then
      do:
        pResponse:fault("3000", "Tasktopic create").
        return.
      end.  
      
    run file/updatetopiclist-p.p(output lError,
                                 output cErrMessage).

    if lError
     then 
      do:
        pResponse:fault("3005", "Created in Compass but failed to create in ARC. " + cErrMessage).
        return.
      end.

    if plReturnRecord 
     then
      do:   
        for first tasktopic no-lock
          where tasktopic.topic = cTaskName:
   
          empty temp-table ttTasktopic.
          create ttTasktopic.
          assign 
              ttTasktopic.topic        = tasktopic.topic
              ttTasktopic.description  = tasktopic.description
              ttTasktopic.active       = tasktopic.active
              ttTasktopic.isSecure     = tasktopic.isSecure
              .
        end.
        setContentDataset(input dataset-handle dsHandle, pRequest:FieldLIst).
      end.


   pResponse:success("2002"," Tasktopic").  /* taskTopic has been created */

  end method.
      
end class.




