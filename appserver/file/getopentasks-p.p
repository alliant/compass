/*------------------------------------------------------------------------
@name  getopentasks-p.p
@description  Get the Open Tasks from ARC.
@param cUID;char
@param cStatus;char
@param cQueueName;char
@param cTaskName;char
@returns Success
@author Shefali
@Modified :
Date        Name          Comments   
06/06/2023  Sagar K       Added Cost field  
07/04/2023  SB            Modified to remove reference to jobQueue and jobRoute
07/20/2023  Sagar K       Modified to add posted field in arc plyload
07/30/2024  S Chandu      Modified to remove special chars in ImportantNote field.
08/28/24    AG            Modified to return the stateID
----------------------------------------------------------------------*/

define input parameter ipcUserName    as character no-undo.
define input parameter ipcAssignedTo  as character no-undo.
define input parameter ipcStatus      as character no-undo.
define input parameter ipcQueueName   as character no-undo.
define input parameter ipcTaskName    as character no-undo.

define output parameter dataset-handle dsHandle.
define output parameter oplError    as logical   no-undo.
define output parameter opcErrorMsg as character no-undo.
define output parameter opiCount    as integer   no-undo.

{tt/filetask.i &tableAlias="ttFileTask"}

create dataset dsHandle.
dsHandle:serialize-name = 'data'.
dsHandle:set-buffers(buffer ttFileTask:handle). 

/* Local variables */
define variable cErrorMessage   as character no-undo.
define variable cAddress        as character no-undo.
define variable cStatusList     as character no-undo.

/* Used for getting the ARC Open task */
define variable lSuccess        as logical   no-undo.
define variable cMsg            as character no-undo.

define variable tDataFile       as character no-undo.
define variable iCount          as integer   no-undo.
define variable iStatusCount    as integer   no-undo.

{lib/arc-repository-procedures.i &exclude-startElement=true}

/* Functions and Procedures */
{lib/run-http.i}
{lib/arc-bearer-token.i}

if ipcStatus <> ""
 then
  do iStatusCount = 1 to num-entries(ipcStatus):

   if cStatusList = "" 
    then 
     assign 
         cStatusList = "~"" + entry(iStatusCount,ipcStatus,"," ) + "~"".
   else 
     assign
         cStatusList = cStatusList + "," + "~"" + entry(iStatusCount,ipcStatus,"," ) + "~"" .

  end.

/* create the data file */
tDataFile = guid(generate-uuid).
output to value(tDataFile).
put unformatted "~{" skip.
put unformatted "  ~"UserName~": ~"" + ipcUserName + "~"," skip.
put unformatted "  ~"AssignedTo~": ~"" + ipcAssignedTo + "~"," skip.
put unformatted "  ~"Status~": ~[" + cStatusList + "]~," skip.   /* "Status": ["New","Processing"], */
put unformatted "  ~"Category~": ~"" + ipcQueueName + "~"," skip.
put unformatted "  ~"Topic~": ~"" + ipcTaskName + "~"," skip.
put unformatted "  ~"Posted~": ~"" + "false" + "~"," skip.
put unformatted "~}" skip.
output close.

publish "AddTempFile" ("ARCActivity", tDataFile).
publish "GetSystemParameter" ("ARCGetOpenTasks", output tURL).
run HTTPPost ("openTasksGet", tURL, CreateArcHeader(""),"application/json", search(tDataFile), output lSuccess, output cMsg, output tResponseFile).
if not lSuccess  
 then
  do:
    assign oplError = true
           opcErrorMsg = if cMsg  = '' then "Failed to connect to the ARC" else cMsg.
    return.
  end.

lSuccess = temp-table ttFileTask:read-json("FILE",tResponseFile) no-error.

/*Validating parsing of response*/
if not lSuccess or error-status:error
 then 
  do: 
    oplError = true.
    do iCount = 1 to error-status:num-messages:
      opcErrorMsg = opcErrorMsg + error-status:get-message(iCount) + ' '. 
    end.
    return.
  end.

opiCount = 0.
for each ttFileTask exclusive-lock:
  for first agentfile where agentfile.agentfileID = ttFileTask.agentfileID no-lock:
    for first agent where agent.agentID = agentfile.agentID:
      assign 
          ttFileTask.AgentID = agent.agentID 
          ttFileTask.Agent   = agent.name 
          ttFileTask.stateID = agent.stateID no-error
          .
    end.

    assign
         cAddress         = if (agentfile.addr1 = "?") or (agentfile.addr1 = ?) or (agentfile.addr1 = "") then "" else agentfile.addr1
         cAddress         = cAddress + if (agentfile.addr2 = "?") or (agentfile.addr2 = ?) or (agentfile.addr2 = "")   then "" else ", " + agentfile.addr2
         cAddress         = cAddress + if (agentfile.addr3 = "?") or (agentfile.addr3 = ?) or (agentfile.addr3 = "")   then "" else ", " + agentfile.addr3
         cAddress         = cAddress + if (agentfile.addr4 = "?") or (agentfile.addr4 = ?) or (agentfile.addr4 = "")   then "" else ", " + agentfile.addr4
         cAddress         = cAddress + if (agentfile.city  = "?") or (agentfile.city  = ?) or (agentfile.city = "")    then "" else ", " + agentfile.city
         cAddress         = cAddress + if (agentfile.state = "?") or (agentfile.state = ?) or (agentfile.state = "")   then "" else ", " + agentfile.state
         cAddress         = cAddress + if (agentfile.zip   = "?") or (agentfile.zip   = ?) or (agentfile.zip = "")     then "" else ", " + agentfile.zip
         cAddress         =  trim(cAddress,',')
         ttFileTask.address =  trim(cAddress)
         .

      
      /* fileTaskID and assignedDate */
      for first fileTask where fileTask.TaskID = ttFileTask.ID no-lock:
        assign
            ttFileTask.fileTaskID   = fileTask.fileTaskID
            ttFileTask.assignedDate = fileTask.assignedDate
            ttFileTask.cost         = filetask.cost
            .
      end.
 

    if can-find (last job where job.agentFileID = agentFile.agentFileID and job.jobtype = "Search" and (job.stat = 'N' or job.stat = 'P'))
	 then 
       assign ttFileTask.productDesc = "Search".
	else if can-find (last job where job.agentFileID = agentFile.agentFileID and job.jobtype = "Policy" and (job.stat = 'N' or job.stat = 'P'))
	 then 
      assign ttFileTask.productDesc = "Policy".
	else 
	 assign ttFileTask.productDesc = "".

  /* it is decided that special characters will be removed at ARC side. but handling them for now in compass side. */
    ttFileTask.ImportantNote = trim(replace(ttFileTask.ImportantNote , "/r/n", "")).
  end. /* for first agentfile */

 if ttFileTask.fileTaskID = 0 
  then
   delete  ttFileTask.
 else
  opiCount = opiCount + 1.

end.  /* for each ttfiletask */





