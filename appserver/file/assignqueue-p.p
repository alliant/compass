/*------------------------------------------------------------------------
@name  assignqueue-p.p
@action queueAssign
@description  Assigns the queue to the task

@author   Spandana
@Modified : 04/24/2023
Date        Name          Comments  
05/01/2023  Sagar K       Changed as per new framework  
06/06/2023  Sagar K       Added cost field   
----------------------------------------------------------------------*/
define input  parameter piTaskID      as integer     no-undo.
define input  parameter pcQueueName   as character   no-undo.
define input  parameter pdtDueDate    as datetime-tz no-undo.
define output parameter piErrCode     as integer     no-undo.
define output parameter pcErrMessage  as character   no-undo.

/* Used for getting the ARC code */
define variable cDataFile       as character no-undo.
define variable lSuccess        as logical   no-undo initial false.
define variable cMsg            as character no-undo.

 /* converting local time into utc datetime-tz  */ 
define variable cCreateDateTime as datetime-tz no-undo. 
define variable utcMinutes      as integer     no-undo.
define variable utcMinutesdiff  as integer     no-undo.

define temp-table ttresponse no-undo
    field Timestamp as datetime.

/* Functions and Procedures */
{lib/arc-repository-procedures.i}
{lib/run-http.i}
{lib/arc-bearer-token.i}

/* Validate when FileTaskID is not supplied */
if piTaskID = 0 or piTaskID = ?
 then 
  do:
    assign 
      piErrCode = 3001
      pcErrMessage = "TaskID".
    return.
  end.

/* to pick local time zone */
assign
    cCreateDateTime = now
    utcMinutes      = timezone(cCreateDateTime)
    utcMinutesDiff  = int(- utcminutes) /* converted to utc required format */
    .

pdtDueDate = datetime-tz(pdtDueDate,utcminutesdiff).

pdtDueDate = datetime-tz(pdtDueDate,0).



  /* create the data file */
cDataFile = guid(generate-uuid).

output to value(cDataFile).
  put unformatted "~{" skip.
  put unformatted "  ~"Id~": " + string(piTaskID) + "," skip.
  put unformatted "  ~"Category~": ~"" + pcQueueName + "~"," skip. 
  put unformatted "  ~"DueDateTime~": ~"" + substring(iso-date(pdtDueDate),1,19) + "~""  skip.
  put unformatted "~}" skip.
output close.


/* send to ARC */
publish "GetSystemParameter" ("ARCAssignQueue", output tURL).

run HTTPPost ("queueAssign", tURL, CreateArcHeader(""), "application/json", search(cDataFile), output lSuccess, output cMsg, output tResponseFile).

if not lSuccess                                                            
 then do:               
    assign
          piErrCode = 3000                                                             
          pcErrMessage = if cmsg  =''  then "Failed to connect to the ARC" else  cmsg.                   
   return.                                                                 
 end.                                                                      
                                                                           
lSuccess = temp-table ttresponse:read-json("FILE",tResponseFile) no-error.
