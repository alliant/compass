/*------------------------------------------------------------------------
@name  ReopenFileTask.cls
@action fileTaskReopen
@description  Changes the status of FileTask to 'New'
@param fileTaskID;int
 

@returns ; success/failure returned from reopenfiletask-p.p

@author   Sachin
@Date     05/23/2023
@Modified :
Date        Name          Comments   
07/11/23    SB            Modified to add check that posted task cannot be reopen.
05/07/24    K.R           Modified to reset the completed date to null as the task is reopened  
07/17/2024  SRK           Modified validation message 
07/25/2024  SR            Added validation for vendorVoucherPost and vendorTaskFinalize locks.  
08/05/2024  SR            Added validation on VendorTaskCostUpdate lock.
11/04/2024  Vignesh R     Modified to return the updated record. 
----------------------------------------------------------------------*/

class file.ReopenFileTask inherits framework.ActionBase:
  
  {tt/filetask.i &tableAlias=ttfiletask}

  constructor public ReopenFileTask ():
    super().
  end constructor.

  destructor public ReopenFileTask ():
  end destructor.
 
  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    define variable piFileTaskID   as integer   no-undo.
    define variable plReturnRecord as logical   no-undo.

    define variable iErrCode      as integer   no-undo.
    define variable cErrMessage   as character no-undo.
    define variable cUsername     as character no-undo.
    define variable dsHandle      as handle    no-undo.
    define variable iTaskID       as integer   no-undo.

        /* converting local time into utc datetime-tz  */ 
    define variable cTodayDateTime  as datetime-tz no-undo. 
    define variable utcMinutes      as integer     no-undo.
    define variable utcMinutesdiff  as integer     no-undo.

    {lib/std-def.i}

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttfiletask:handle).
    
    pRequest:getParameter("FileTaskID",   output piFileTaskID). 
    pRequest:getParameter("returnRecord", output plReturnRecord).

     /* validate fileTaskID */
    if piFileTaskID = 0 or piFileTaskID = ?
     then
      do:
        pResponse:fault('3001', 'FileTaskID').
        return.
      end.

   
    if not can-find(first filetask where filetask.fileTaskID = piFileTaskID ) 
     then 
      do:
        pResponse:fault('3001', 'FileTaskID').
        return.
      end.

    if can-find(first filetask where filetask.fileTaskID = piFileTaskID and filetask.posted)
     then
      do:
        pResponse:fault('3005', 'Posted task cannot be reopened.').
        return.
      end.

     /* Make sure AP Posting is not locked (posting or finalizing) while reopening any task which is completed is past (< today) 
        All the tasks that are completed < today can be posted from AP posting screen. Tasks completed today cannot be posted. */
    for first filetask where filetask.fileTaskID = piFileTaskID and not filetask.posted no-lock:

      /* to pick local time zone */
      assign
          cTodayDateTime  = now
          utcMinutes      = timezone(cTodayDateTime)
          utcMinutesDiff  = int(- utcminutes) /* converted to utc required format */
          .
       
       
       /* now on server time changed to UTc time */
      cTodayDateTime = datetime-tz(cTodayDateTime,utcMinutesDiff).
      cTodayDateTime = datetime-tz(cTodayDateTime,0).
       
      if date(filetask.completeddate) < date(cTodayDateTime) /* server time to utc then compare ? */
       then
        do:
          {lib/islocked.i &obj='VendorVoucherPost' &id='' &exclude-fault=TRUE}
          if std-lo 
           then
            do: 
              {lib/getusername.i &var=cUsername &user=std-ch}
          
              /* if file is locked by another user,cannot unlock it, return fault */
              pResponse:fault("3005", "Task cannot be reopened as Voucher Posting is locked by " + cUsername).       
              return.
            end.
          
          {lib/islocked.i &obj='vendorTaskFinalize' &id='' &exclude-fault=TRUE}
          if std-lo 
           then
            do: 
              {lib/getusername.i &var=cUsername &user=std-ch}
          
              /* if file is locked by another user,cannot unlock it, return fault */
              pResponse:fault("3005","Task cannot be reopened as Task Posting is locked by " + cUsername).
              return.
            end.

         {lib/islocked.i &obj='VendorTaskCostUpdate' &id='' &exclude-fault=TRUE}
          if std-lo 
           then
            do: 
              {lib/getusername.i &var=cUsername &user=std-ch}
          
              /* if file is locked by another user,cannot unlock it, return fault */
              pResponse:fault("3005","Task cannot be reopened as Task Cost Updating is locked by " + cUsername).
              return.
            end.
        end.

    end. 
   
    for first filetask where filetask.filetaskID = piFileTaskID no-lock:
      iTaskID = filetask.taskID.
      run file/reopenfiletask-p.p(input  filetask.taskID,
                                  output iErrCode,
                                  output cErrMessage).

    end.
     
    if iErrCode > 0 
     then 
      do:
        pResponse:fault(string(iErrCode), cErrMessage).
        return.
      end.

    TRX-BLOCK:    
    for first filetask exclusive-lock 
        where filetask.filetaskID = piFileTaskID transaction
        on error undo TRX-BLOCK, leave TRX-BLOCK:

        filetask.completeddate = ?.

        validate filetask.
        release filetask.
    end.

    if plReturnRecord
     then
      do:
        run file/gettaskbyid-p.p(input  iTaskID,
                                 output dataset-handle dsHandle,
                                 output iErrCode,                             
                                 output cErrMessage).
        if iErrCode > 0 
         then 
          do:
            pResponse:fault('3005', 'Not able to retrieve the task from ARC.' + cErrMessage).
            return.
          end.

        if can-find(first ttFileTask)
         then 
          setContentDataSet(input dataset-handle dsHandle, input pRequest:FieldList).

      end.

    pResponse:success("2000", "Task Reopen").
    
  end method.
 
end class.

