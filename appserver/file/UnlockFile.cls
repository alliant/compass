/*------------------------------------------------------------------------
@name  UnlockFile.cls
@action fileUnlock
@description  Unlocks the locked Agent fileID from syslock.
@param agentFileID;int;The unique sequence fileID of an Agent
@throws 3001;Invalid AgentfileID
@throws 3066;AgentFileID does not exist
@throws 3081;Agentfile locked by another user
@throws 3000;Agentfile locked release failed
            ;Agentfile unlock failed  
            			
@success 2000;Agentfile unlock was successful

@author   Kasun Dhananjaya

@Modified:
    Date        Name          Comments   
    Shefali     11/22/2023    Modified to unlock the file as per the input 
                              parameter UserType A(dmin) or U(ser).   
    SB          03/18/2024    Modified to add validation for agentFileID must exists

----------------------------------------------------------------------*/

class file.UnlockFile inherits framework.ActionBase:
  constructor public UnlockFile():
    super().
  end constructor.

  destructor public UnlockFile():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    define variable piAgentFileID   as integer   no-undo.
    define variable cUsername       as character no-undo.
    define variable lSuccess        as logical   no-undo.
    define variable pUserType       as character no-undo.

    /*data set handle*/
    define variable dsHandle        as handle    no-undo.

    /*parameter get*/
    pRequest:getParameter("userType", output pUserType).
    pRequest:getParameter("agentFileID", output piAgentFileID).

    /* validation when no parameter is supplied */
    if (piAgentFileID = 0  or piAgentFileID = ?)
     then 
      do:
        pResponse:fault("3001", "AgentfileID").
        return.
    end.

    {lib/std-def.i}

    if not can-find(first agentFile where agentFile.agentFileID = piAgentFileID) 
     then 
      do:
        pResponse:fault("3066", "Agent File"). 
        return.
      end.

    if pUserType <> "A" then
     do:
       /* Check if file is locked or not */
       {lib/islocked.i &obj='AgentFile' &id=piAgentFileID &exclude-fault=TRUE}
       if std-lo and (std-ch <> pRequest:Uid)
        then
         do: 
           {lib/getusername.i &var=cUsername &user=std-ch}
    
           /* if file is locked by another user,cannot unlock it, return fault */
           pResponse:fault("3081", "Agent File" {&msg-add} cUsername).
           lSuccess = false.
           return.
         end.
     end.

    TRX-BLOCK:
    do on error undo TRX-BLOCK, leave TRX-BLOCK:
      /* Release lock */
      run util/deletesyslock.p ("AgentFile", piAgentFileID, 0, output lSuccess). 
      if not lSuccess 
       then
        do:
          pResponse:fault("3000", "Agent file lock release").
          return.
        end.

        /* create jobTran each time there is update action on a agentFile. */
        run file/createjobtran-p.p(input piAgentFileID,
                                   input pRequest:actionID,
                                   input pRequest:Uid,
                                   input 'agentFileID=' + string(piAgentFileID),
                                   input dataset-handle dsHandle
                                   ).
       						 
       
        lSuccess = true.
    end.

    if not lSuccess
     then 
      do:
        pResponse:fault("3000", "Agent file unlock"). /* Agentfile unlock failed*/
        return.
      end.
    
    pResponse:success("2000", "Agent file unlock").  /* Agent file unlock successful */

  end method.

end class.
