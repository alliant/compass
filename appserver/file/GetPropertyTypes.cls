/*------------------------------------------------------------------------
@file GetPropertyTypes.cls
@action propertyTypesGet
@description Get property types
@returns property types
@success 2005;X Property Type(s) were returned
@author Shefali
@version 1.0
@created 04/05/2023
Modification:

----------------------------------------------------------------------*/

class file.GetPropertyTypes inherits framework.ActionBase:
    
  {tt/sysprop.i &tableAlias="propertyTypes"}

  constructor GetPropertyTypes ():
    super().
  end constructor.

  destructor public GetPropertyTypes ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable dsHandle as handle no-undo.

    {lib/std-def.i}

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer propertyTypes:handle).

    std-in = 0.
    for each sysprop no-lock where sysprop.appCode     = "TPS" and 
                                   sysprop.objAction   = "Property" and
                                   sysprop.objProperty = "Type":
    
      create propertyTypes.
      buffer-copy sysprop to propertyTypes.
    
      std-in = std-in + 1.
    end.
    
    
    if std-in > 0
     then 
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).  
    
    pResponse:success("2005", string(std-in) {&msg-add} "Property Types").
             
  end method.
      
end class.


