/*------------------------------------------------------------------------
@file QueryOpenTasks.cls
@action openTasksQuery
@description Get Open Tasks
@returns Open Tasks
@success 2005;X Open Task(s) were returned
@author SRK
@version 1.0
@created 12/24/2024
Modification:

----------------------------------------------------------------------*/

class file.QueryOpenTasks inherits framework.ActionBase:
    
  {tt/filetask.i &tableAlias="ttFileTask"}
  {tt/queryTasks.i}

  constructor QueryOpenTasks ():
    super().
  end constructor.

  destructor public QueryOpenTasks ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
	define variable pcStateID         as character no-undo.
	define variable pcAssignedUser    as character no-undo.
	define variable pcAssignedQueue   as character no-undo.
    define variable pcMinElapsedHours as integer   no-undo. 
    define variable lError            as logical   no-undo.
    define variable cErrMessage       as character no-undo.
    define variable std-in            as integer   no-undo.
    define variable lelapsed          as integer   no-undo.
    define variable idays             as integer   no-undo.
    define variable itime             as integer   no-undo.


	
    pRequest:getParameter("StateID",         output pcStateID).
    pRequest:getParameter("AssignedUser",    output pcAssignedUser).
    pRequest:getParameter("AssignedQueue",   output pcAssignedQueue).	
    pRequest:getParameter("MinElapsedHours", output pcMinElapsedHours).

    if pcStateID = 'ALL' or pcStateID = ?
     then
      pcStateID = ''.

    if pcAssignedUser = 'ALL' or pcAssignedUser = ?
     then
      pcAssignedUser = ''.

    if pcAssignedQueue = 'ALL' or pcAssignedQueue = ?
     then
      pcAssignedQueue = ''.

    if pcMinElapsedHours < 0 or pcMinElapsedHours = ?
     then
      pcMinElapsedHours = 0.


    define variable dsHandle   as handle no-undo.
    define variable dsopentask as handle no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttfiletask:handle).

    create dataset dsopentask.
    dsopentask:serialize-name = 'data'.
    dsopentask:set-buffers(buffer queryTasks:handle).

    /*Get open tasks from ARC*/
    run file/getopentasks-p.p(input '', /* UserName - for Permission */
                              input '',           /* Assigned To */
                              input "New,Open",
		        	          input '',
                              input '',
                              output dataset-handle dsHandle,
                              output lError,
                              output cErrMessage,
                              output std-in).
    
    if lError 
     then
      do:
       pResponse:fault ("3005",  "GetOpentask Failed: " + cErrMessage).
       return.
      end.

    std-in = 0.
    for each ttfiletask no-lock where 
        (ttfiletask.stateID = if (pcStateID = '' or pcStateID = ? ) then ttfiletask.stateID else pcStateID) and
        (ttfiletask.Category = if (pcAssignedQueue = '' or pcAssignedQueue = ? ) then ttfiletask.Category else pcAssignedQueue ) and
        (ttfiletask.AssignedTo =  if (pcAssignedUser = '' or pcAssignedUser = ? ) then  ttfiletask.AssignedTo else pcAssignedUser) :

        lelapsed = integer(interval(now,ttfiletask.AssignedDate,'hours'))  .
        idays    = integer(lelapsed / 24).
        itime    = lelapsed MOD 24.

      if (lelapsed >= pcMinElapsedHours)
       then
        do:
          create queryTasks.
          assign
             queryTasks.fileTaskID    = ttfiletask.fileTaskID
             queryTasks.ID            = ttfiletask.ID
             queryTasks.topic         = ttfiletask.topic
             queryTasks.AgentFileId   = ttfiletask.AgentFileId
             queryTasks.FileNumber    = ttfiletask.FileNumber
             queryTasks.AssignedTo    = ttfiletask.AssignedTo
             queryTasks.AssignedDate  = ttfiletask.AssignedDate
             queryTasks.Category      = ttfiletask.Category
             queryTasks.TaskStatus    = ttfiletask.TaskStatus
             queryTasks.ImportantNote = ttfiletask.ImportantNote
             queryTasks.stateID       = ttfiletask.stateID
             queryTasks.ElapsedHrs    = lelapsed
             queryTasks.ElapsedTime   = string(idays) + ' Days ' + string(itime) + ' Hours'.

          for first agentfile fields(closingDate AgentFileId) where agentfile.agentfileID = ttfiletask.AgentFileId no-lock :
            queryTasks.closingDate = agentfile.closingDate.
          end.
          for first state fields(stateID description) where state.stateID = ttfiletask.stateID no-lock :
            queryTasks.stateDesc = state.description.
          end.
          for first agent fields(agentID name county) where agent.agentID = ttfiletask.agentID no-lock :
            queryTasks.agent = agent.name + " ( " + agent.agentID + ")" .
          end.
          /* county details from fileproperty */
          for first fileproperty fields(stateID countyID agentfileid) no-lock
                       where fileproperty.agentfileid = ttfiletask.AgentFileId
                         and fileproperty.stateID     = ttfiletask.stateID:
            queryTasks.county = getCountyDesc(fileProperty.countyID, fileProperty.stateID).
          end.
          for first sysuser fields(uid name) where sysuser.uid = ttfiletask.AssignedTo no-lock :
            queryTasks.AssignedToName = sysuser.name.
          end.
          std-in = std-in + 1.
        end.
    end.

    if std-in > 0 then
      setcontentdataset(input dataset-handle dsopentask,input pRequest:FieldList).

    pResponse:success("2000", string(std-in) + " Open Task").
             
  end method.

  method public character getCountyDesc(input cCountyID as character,input cStateID as character ):
     
     if cCountyID = ''
      then
       return ''.

     for first county field(countyID stateID description) no-lock
         where county.countyID = cCountyID
           and county.stateID  = cStateID:
       return county.description.
     end.
    
     return cCountyID.

  end method.
      
end class.


