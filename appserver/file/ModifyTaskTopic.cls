/*------------------------------------------------------------------------
@name file/ModifyTaskTopic.cls
@action  taskTopicModify
@description  Modify a tasktopic record.
@param name,description 
@author Spandana
@version 1.0
@created 04/17/2023
@Modified :
Date        Name          Comments  
05/04/2023  Sagar K       updating active status
01/12/2024  SB            Modified to add validation for isSecure field.
03/19/2024  SB            Modified to handle isDeleted field.
----------------------------------------------------------------------*/

class file.ModifyTaskTopic inherits framework.ActionBase:

  {tt/tasktopic.i &tableAlias="ttTasktopic"}

  constructor ModifyTaskTopic ():
    super().
  end constructor.

  destructor public ModifyTaskTopic ():
  end destructor.
 

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     
    
	define variable pcTopic         as character no-undo.
    define variable plReturnRecord as logical   no-undo.
    define variable lSuccess       as logical   no-undo.
    define variable dsHandle       as handle    no-undo.
    define variable lError         as logical   no-undo.
    define variable cErrMessage    as character no-undo.
       
    {lib/std-def.i}

    /* dataset creation */
    create dataset  dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttTasktopic:handle).

    pRequest:getParameter("Topic", output pcTopic). 
    pRequest:getParameter("ReturnRecord",plReturnRecord).
    
    /* validation when no parameter is supplied */
    if (pcTopic = ''  or pcTopic = ?)
     then 
      do:
        pResponse:fault("3005", "Task Name cannot be blank/Invalid value").
        return.
      end.

    if plReturnRecord = ?
     then
      plReturnRecord = false.

     empty temp-table ttTasktopic.
    
    /* get Dataset */
    pRequest:getContent(input-output dataset-handle dsHandle, output cErrMessage).
    if cErrMessage > '' 
     then
      do:
        pResponse:fault('3005', "Data Parsing failed. Reason: " + cErrMessage).
        return.
      end.

     if not can-find(first ttTasktopic) 
     then
      do:
        pResponse:fault('3005', 'Task topic record not provided').
        return.
      end.
   
    /* validate task Name */
    if not can-find(first tasktopic where tasktopic.topic = pcTopic )
     then
      do:
        pResponse:fault("3066", "Task Name").
        return.
      end.

    if can-find(first tasktopic where tasktopic.topic = pcTopic and tasktopic.isDeleted = yes) 
     then
      do:
        pResponse:fault("3005", "This task is already marked as deleted. Cannot be modified.").
        return.
      end.

    lSuccess = false.
    
    TRX-BLOCK:
    do transaction
      on error undo TRX-BLOCK, leave TRX-BLOCK:
	  
	  for first tasktopic where tasktopic.topic = pcTopic 
                            and tasktopic.isDeleted = no exclusive-lock:
         for first ttTasktopic no-lock where ttTasktopic.topic = tasktopic.topic:

            if tasktopic.isSecure = yes then
             do: 
               pResponse:fault("3005","This is a secured task topic, cannot be edited from TPS application."). 
               return.
             end.
            else
             do:
               if ttTasktopic.isSecure = yes then
                do:
                  pResponse:fault("3005","Secured task topic, cannot be edited from TPS application." + chr(13) + 
                                 "Please contact System Administrator."). 
                  return.
                end.
             end.

            assign
               tasktopic.description      = ttTasktopic.description
               tasktopic.active           = ttTasktopic.active
               tasktopic.isSecure         = ttTasktopic.isSecure
               tasktopic.lastModifiedDate = now
               tasktopic.lastModifiedBy   = pRequest:uid
               tasktopic.isDeleted        = no
               .

               validate tasktopic. 
               release  tasktopic.

               lSuccess = true.
        end.
	  end.
    
    end.
    
    if not lSuccess
     then
      do:
        pResponse:fault("3000", "Tasktopic Update").
        return.
      end. 

     run file/updatetopiclist-p.p(output lError,
                                 output cErrMessage).

    if lError
     then 
      do:
        pResponse:fault("3005", "Created in Compass but failed to create in ARC. " + cErrMessage).
        return.
      end.

    if plReturnRecord 
     then
      do:   
        for first tasktopic no-lock
          where tasktopic.topic = pcTopic:
   
          empty temp-table ttTasktopic.
          create ttTasktopic.
          assign 
              ttTasktopic.topic       = tasktopic.topic
              ttTasktopic.description = tasktopic.description
              ttTasktopic.active      = tasktopic.active
              ttTasktopic.isSecure    = tasktopic.isSecure
              .
        end.
        setContentDataset(input dataset-handle dsHandle, pRequest:FieldLIst).
      end.

    pResponse:success("3005"," Tasktopic update was successful").  /* taskTopic has been updated*/
         
  end method.
      
end class.





