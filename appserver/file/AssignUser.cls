/*------------------------------------------------------------------------
@name  AssignUser.cls
@action userAssign
@description  Assigns the user task
@param fileTaskID;int
@param UID;character
@param DueDate;datetime 

@returns ; success/failure returned from assignuser-p.p

@author   Shefali
@Modified :
Date         Name          Comments   
05/02/2023   Sagar K       changed as per new framework
05/05/2023   Sagar K       creating taskid if it is zero
07/21/2023   Sagar K       Added validation for posted field
09/07/2023   Sagar K       Added default for queue
12/06/2023   Vignesh R     Modified to skip the date valiation
12/12/2023   Vignesh R     Modified to add the date validation
02/27/2024   K.R           Modified to add validation for negative cost and completed task
04/25/2024   Shefali       Modified to validate duedate when there is a change in duedate
08/08/2024   Vignesh R     Modified to change the Error Message Codes.
----------------------------------------------------------------------*/

class file.AssignUser inherits framework.ActionBase:

  constructor public AssignUser ():
    super().
  end constructor.

  destructor public AssignUser ( ):
  end destructor.
 
  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    define variable piFileTaskID  as integer     no-undo.
    define variable pcUID         as character   no-undo.
    define variable pdtDueDate    as datetime-tz no-undo.
    define variable pdcost        as decimal     no-undo.
    define variable plDueDateChanged as logical  no-undo.
    define variable piTaskID      as integer     no-undo.
    define variable lReturnVal    as logical     no-undo.
    define variable lcreatetask   as logical     no-undo.
    define variable cQueue        as character   no-undo.

    define variable iErrCode      as integer   no-undo.
    define variable cErrMessage   as character no-undo.

    /* converting local time into utc datetime-tz  */ 
    define variable cTodayDateTime as datetime-tz no-undo. 
    define variable utcMinutes      as integer     no-undo.
    define variable utcMinutesdiff  as integer     no-undo.
    
    pRequest:getParameter("FileTaskID",  output piFileTaskID). 
    pRequest:getParameter("UID",         output pcUID). 
    pRequest:getParameter("DueDate",     output pdtDueDate).
    pRequest:getParameter("Cost",        output pdcost).
    pRequest:getParameter("DueDateChanged", output plDueDateChanged).

    /* validate agentfileID */
    if piFileTaskID = 0 or piFileTaskID = ?
     then
      do:
        pResponse:fault('3001', 'FileTaskID').
        return.
      end.

    if not can-find(first filetask where filetask.fileTaskID = piFileTaskID ) 
     then 
      do:
        pResponse:fault('3001', 'FileTaskID').
        return.
      end.

    if not can-find(first sysUser where sysUser.UID = pcUID and sysUser.isActive = true) 
     then 
      do:
        pResponse:fault("3001", "User"). 
        return.  
      end.

    if pdtDueDate = ?
     then
      do:
        pResponse:fault("3001", "Due Date"). 
        return.
      end.

    if can-find(first filetask where filetask.fileTaskID = piFileTaskID and filetask.posted = true) 
     then 
      do:
        pResponse:fault('3005', 'User cannot be assigned to a posted task').
        return.
      end.
	  
	if pdcost < 0
	 then
	  do:
	    pResponse:fault("3005", "Task cost cannot be less than zero."). 
        return.
      end.
	  
	if can-find(first filetask where filetask.fileTaskID = piFileTaskID and filetask.completedDate <> ?)
	 then
	  do:
	    pResponse:fault("3005", "Task cannot be assigned as it is already completed.").
	    return.
	  end.

    assign
        cTodayDateTime  = now
        utcMinutes      = timezone(cTodayDateTime)
        utcMinutesDiff  = int(- utcminutes) /* converted to utc required format */
        .
   
    /* client's due date is converting into UTC time */
    pdtDueDate = datetime-tz(pdtDueDate,utcMinutesDiff).
    pdtDueDate = datetime-tz(pdtDueDate,0).

    if plDueDateChanged then
     do:
      /* validate the date whether it is future date or not before creating the Task in ARC */
      if can-find(first fileTask where filetask.filetaskID = piFileTaskID /*and fileTask.taskID = 0*/) /*AND condition is not required.*/
       then
        do:
          /* now on server time changed to UTc time */
          cTodayDateTime = datetime-tz(cTodayDateTime,utcMinutesDiff).
          cTodayDateTime = datetime-tz(cTodayDateTime,0).
    
          if pdtDueDate < cTodayDateTime
           then
            do:
              pResponse:fault("3005", "Due Date must be in the future").
              return.
            end. 
        end.
     end.
   
    for first filetask where filetask.filetaskID = piFileTaskID no-lock:
      piTaskID =  filetask.taskID.
      if piTaskID = 0 or piTaskID = ?
       then do:
         /* ARC Task creation */
        lcreatetask = true.
        cQueue = filetask.category.
        if cQueue = "" or cQueue = ? 
         then
          for first sysprop where sysprop.appCode     = "TPS"           and 
                                  sysprop.objAction   = 'CreateArcTask' and 
                                  sysprop.objProperty = 'Queue'no-lock:
            cQueue = sysprop.objValue.
          end.

        for first agentfile no-lock
          where agentfile.agentFileID = filetask.agentfileID:
          run file/createarctask-p.p(input  cQueue,
                                     input  filetask.topic,
                                     input  agentfile.agentFileID,
                                     input  agentFile.fileNumber,
                                     input  agentFile.fileID,
                                     input  pcUID,
                                     input  pdtDueDate,
                                     output piTaskID,
                                     output lReturnVal,
                                     output cErrMessage
                                                        ).
          if lReturnVal 
           then do:
            pResponse:fault("3005", "Task cannot be created in ARC, user assign failed." + cErrMessage).
            return.
           end.
        end. /* for first agentfile */
      end.
    end.
    if lcreatetask
     then 
      FILETASK_UPD:
      do transaction 
       on error undo FILETASK_UPD, leave FILETASK_UPD:
        for first filetask exclusive-lock where filetaskid = piFileTaskID:
         filetask.taskid   = piTaskID.
         filetask.category = cQueue.
        end.
      end.

    FILETASK_UPD:
    do transaction 
     on error undo FILETASK_UPD, leave FILETASK_UPD:
      for first filetask exclusive-lock where filetask.filetaskid = piFileTaskID:
       filetask.cost = pdcost.
      end.
    end.
 
    run file/assignuser-p.p(input  piTaskID,
                            input  pcUID,
                            input  pdtDueDate,
                            output iErrCode,
                            output cErrMessage).
   
     
    if iErrCode > 0 
     then 
      do:
        pResponse:fault(string(iErrCode), cErrMessage).
        return.
      end.
        
    pResponse:success("2000", "User assignment").
    
  end method.
 
end class.

