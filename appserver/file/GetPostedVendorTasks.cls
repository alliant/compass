/*------------------------------------------------------------------------
@name  file.GetPostedVendorTasks.cls
@action postedVendorTasksGet
@description  Get Posted vendor and Tasks.
@throws 3005;
@returns Success;2005; X GetPostedVendorTasks was successful.
@author Sagar K
@Created     : 08/24/2023
@Modified :
Date        Name          Comments 
07/02/24   S Chandu      Modified to send valid assignedtoname,glaccount and topic fields.     
08/01/24   SRK           Modified to Add hasdocument 
----------------------------------------------------------------------*/

class file.GetPostedVendorTasks inherits framework.ActionBase:
      
  constructor public GetPostedVendorTasks ():
    super().
  end constructor.

  destructor public GetPostedVendorTasks ():
  end destructor. 

  {tt/apinv.i &tableAlias=ttapinv}
  {tt/filetask.i &tableAlias=ttFileTask}
  {lib/callsp-defs.i}

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
                      
    define variable pcvendorID   as character  no-undo.
    define variable pdtstartdate as datetime   no-undo.
    define variable pdenddate    as date       no-undo.
    define variable pdtenddate   as datetime   no-undo.
    define variable iCount       as integer    no-undo.
    define variable dsHandle     as handle     no-undo.
    define variable cUsername    as character  no-undo.

    {lib/std-def.i}

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttapinv:handle,
                         buffer ttFileTask:handle).

    pRequest:getParameter("VendorID",        output pcvendorID).
    pRequest:getParameter("startdate",       output pdtstartdate).
    pRequest:getParameter("enddate",         output pdenddate).

    pdtenddate = datetime(string(pdenddate) + " 23:59:59.998").

    for first sysuser where sysuser.uid = pRequest:uid no-lock:
      if pcvendorID = "" or pcvendorID = ?
       then
        do:

           if sysuser.vendorID <> '' and sysuser.vendorID <> ? 
            then pcvendorID = sysuser.vendorID.

        end.
      else if (sysuser.vendorID <> ? and sysuser.vendorID <> '') and pcvendorID <> sysuser.vendorID 
       then
        do:
          pResponse:fault("3005","You cannot view tasks for " + pcvendorID).
          return.
        end.
    end.


    {lib\callsp.i 
        &name=spGetPostedVendorTasks 
        &load-into=ttapinv 
        &params="input pcvendorID, input pdtstartdate, input pdtenddate" 
        &noresponse=true}
    

    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end.

    for each ttapinv no-lock:
      ttapinv.hasDocument = (can-find(first sysdoc where sysdoc.entityid = ttapinv.refID and sysdoc.entityType = 'APVoucher' and sysdoc.objType = 'pdf')) and
                             (can-find(first sysdoc where sysdoc.entityid = ttapinv.refID and sysdoc.entityType = 'APVoucher' and sysdoc.objType = 'csv')).
      for each filetask where filetask.apInvID = ttapinv.apinvID:
        create ttfiletask.
        assign
            ttfiletask.apInvID       = filetask.apInvID
            ttfiletask.AssignedDate  = filetask.assigneddate
            ttfiletask.AssignedTo    = filetask.AssignedTo
            ttfiletask.EndDateTime   = filetask.completeddate
            ttfiletask.cost          = filetask.cost
            ttfiletask.topic         = filetask.topic          
            ttfiletask.VendorID      = ttapinv.VendorID
            ttfiletask.VendorName    = ttapinv.VendorName
            ttfiletask.refID         = ttapinv.refID
            ttfiletask.invoiceNumber = ttapinv.invoiceNumber
            ttfiletask.invoiceDate   = ttapinv.invoiceDate
            ttfiletask.amount        = ttapinv.amount
            .

            for first agentfile where agentfile.agentfileID = filetask.agentfileID:
              ttfiletask.filenumber = agentfile.filenumber.
              ttfiletask.AgentID    = agentfile.AgentID.
            end.
            for first agent where agent.agentID = ttfiletask.agentID no-lock:
              ttfiletask.agent      = agent.name.

              for first topicExpense where topicExpense.stateID = agent.stateID
                                       and topicExpense.topic   = ttFileTask.topic no-lock:
                 ttFiletask.glaccount = topicExpense.GLAccount.
              end.
            end.

         {lib/getusername.i &var=cusername &user=ttfiletask.AssignedTo}
          ttfiletask.assignedToName = cusername.
      end.

      for first sysuser where sysuser.uid = ttapinv.uid :
       ttapinv.username = sysuser.name.         
      end.
    end.

    if csp-icount > 0
     then         
      setContentDataset(input dataset-handle dsHandle, pRequest:FieldList).          
    
   pResponse:success("2005", string(csp-icount) {&msg-add} "postedvendortask record").

  end method.
    
end class.
