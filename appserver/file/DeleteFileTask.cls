/*------------------------------------------------------------------------
@name  DeleteFileTask.cls
@action fileTaskDelete
@description  delete the task
@param fileTaskID;int
 

@returns ; success/fault

@author   sagar k
@Date     04/28/2023
@Modified :
Date        Name          Comments   
Shefali     05/29/2023    Modified to delete filetask from compass also.  
07/20/2023  S chandu      Added validation for posted field.
03/18/2024  Sachin        Added validation to return fault if trying to delete a filetask 
                          for which ARC task exists. Also deleted deletefiletask-p.p as it's not required. 
----------------------------------------------------------------------*/

class file.DeleteFileTask inherits framework.ActionBase:

  constructor public DeleteFileTask ():
    super().
  end constructor.

  destructor public DeleteFileTask ( ):
  end destructor.
 
  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    define variable piFileTaskID  as integer   no-undo.
    define variable lSuccess      as logical   no-undo.
   
    pRequest:getParameter("FileTaskID",  output piFileTaskID). 

    /* validate fileTaskID */
    if piFileTaskID = 0 or piFileTaskID = ?
     then
      do:
         pResponse:fault('3001', 'FileTaskID').
        return.
      end.

   
    if not can-find(first filetask where filetask.fileTaskID = piFileTaskID ) 
     then 
      do:
         pResponse:fault('3001', 'FileTaskID').
         return.
      end.

    if can-find(first filetask where filetask.fileTaskID = piFileTaskID and filetask.posted = true) 
     then 
      do:
        pResponse:fault('3005', 'Posted task cannot be deleted').
        return.
      end.
	  
    if can-find(first filetask where filetask.fileTaskID = piFileTaskID and filetask.taskID > 0 )
     then 
      do:
        pResponse:fault('3005', 'Cannot delete a task which has been replicated to ARC').
        return.
      end.
    
    lSuccess = false.
    
    TRX-BLOCK:
    for first filetask exclusive-lock
      where filetask.filetaskID = piFileTaskID
       on error undo TRX-BLOCK, leave TRX-BLOCK:
    
      delete filetask.
      release filetask.
       
      lSuccess = true.
    end. 
    
    if not lSuccess
     then 
      do:
        presponse:fault("3000", "Task delete").
        return.
      end.
        
    pResponse:success("2000", "Task delete").
    
  end method.
 
end class.

