/*------------------------------------------------------------------------
@name  template/ActivateOrder.cls
@action orderActivate
@description  Set the order status to “Processing”. 

@param orderID;char;ID of an order to activate

@returns Success;int;2000;Order activate was successful

@throws 3005;Fault description

@author   Sachin Chaturvedi
@Modified :
Date        Name          Comments     
----------------------------------------------------------------------*/

class file.ActivateOrder inherits framework.ActionBase:

  constructor public ActivateOrder ():
    super().
  end constructor.

  destructor public ActivateOrder ( ):
  end destructor.

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    define variable piOrderID        as integer   no-undo.
    define variable piAgentFileID    as integer   no-undo.
    {lib/std-def.i}
    
    /* parameter get */
    pRequest:getParameter("orderID", output piOrderID).
    if piOrderID = ?
     then
      piOrderID = 0.
    
    run file/activateorder-p.p (input  piOrderID,         /* Order ID to activate */
                                input  pRequest:UID,      /* Request UserID       */
                                input  pRequest:actionID, /* Request actionID     */
                                output std-lo,
                                output std-ch
                               ).

    if not std-lo
     then 
      do:
        presponse:fault("3005", std-ch).
        return.
      end.

    pResponse:success("2000", "Order activate").
 
  end method.
 
end class.

