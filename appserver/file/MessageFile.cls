/*------------------------------------------------------------------------
@name  MessageFile.cls
@action fileMessage
@description perform action according to the message received on file
@param agentfileID, description, message
@returns Fault; 
@returns Success;.
@author K.R
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/

class file.MessageFile inherits framework.ActionBase:

  constructor MessageFile ():
    super().
  end constructor.

  destructor public MessageFile ( ):
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    &scoped-define msg-add + "^" +
    
    define variable piAgentFileID as integer   no-undo.
    define variable pcDescription as character no-undo.
    define variable pcMessage     as character no-undo.

    pRequest:getparameter("agentFileID", output piAgentFileID). /* validate */
    pRequest:getparameter("description", output pcDescription).
    pRequest:getparameter("message",     output pcMessage).

    /* validate courseID */
    if piAgentFileID = ? or piAgentFileID = 0
     then
      do:
        pResponse:fault("3001", "agentFileID").
        return. 
      end.

    /* validate courseID */
    if not can-find(first agentfile where agentfile.agentFileID = piAgentFileID)
     then
      do:  
        pResponse:fault("3067", "agentfile" {&msg-add} "ID" {&msg-add} string(piAgentFileID)).
        return.
      end.

    /* To do : Actual implementation yet to be done , for now this is just skeleton for the api */

    pResponse:success("2000", "fileMessage").

  end method.
end class.   
