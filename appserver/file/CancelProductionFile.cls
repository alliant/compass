/*------------------------------------------------------------------------
@name  CancelProductionFile.cls
@action 
@description  Cancel the production file.
@param agentFileID;int
@throws 3001;Invalid agentfileID
@throws 3066;Agent File does not exist 
@returns Success;2000;Production File Cancel was successful.
@author S Chandu
@created 26/04/2024 
@Modified :
Date        Name          Comments     
      
----------------------------------------------------------------------*/

class file.CancelProductionFile inherits framework.ActionBase:
        
  constructor CancelProductionFile ():
    super().
  end constructor.

  destructor public CancelProductionFile ( ):
  end destructor.   

  {tt/proerr.i   &tableAlias="ttCancelFileErr"}
  {tt/productionfile.i &tableAlias=ttCancelProdFile}
  {tt/fileInvAr.i}
  {tt/fileInvArItem.i} 
  {tt/filear.i &tableAlias="ttFilear"}
      
  /* lib*/
  {lib/nextkey-def.i}

  /* Global Variables*/
  define variable iErrorSeq      as integer    no-undo.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
    {lib/std-def.i}
    {lib/nextkey-def.i}
	
    define variable dsHandle        as handle    no-undo.
    define variable dsHandleOutput  as handle    no-undo.
    define variable piAgentFileID   as integer   no-undo.
    define variable ifileInvID      as integer   no-undo.
    define variable l_Success       as logical   no-undo.
    define variable lValid          as logical   no-undo initial true.
    define variable lSuccess        as logical   no-undo.
    define variable lPosted         as logical   no-undo.

    define variable cAgentID             as character no-undo.
    define variable cFileID              as character no-undo.
    define variable cErrorMsg            as character no-undo.
    define variable deFileTotalInvAmt    as decimal   no-undo initial 0.00.
    define variable deFileWriteoffInvAmt as decimal   no-undo initial 0.00.
    define variable ifilearid            as integer   no-undo.

    empty temp-table ttCancelProdFile.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttCancelProdFile:handle).

    create dataset dsHandleOutput.
    dsHandleOutput:serialize-name = 'data'.
    dsHandleOutput:set-buffers(buffer fileInvAr:handle,
                               buffer fileInvArItem:handle,
                               buffer ttFilear:handle,
                               buffer ttCancelFileErr:handle ).
   
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > ""
     then
      do:
        lValid = false.
        createErrorRecord ('Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.
    
    if not can-find(first ttCancelProdFile) 
     then
      do:
        lValid = false.
        createErrorRecord ("Production File record not provided").
        return.
      end.
    
    for each ttCancelProdFile:
  
        /* validate agentfileID */
      if not can-find(first agentfile where agentfile.agentfileID = ttCancelProdFile.agentFileID)
       then
        do:
          lValid = false.
          createErrorRecord ("Agent File does not exist for AgentfileID: " + string(ttCancelProdFile.agentFileID)).
        end.
  
        /* validate for cancelling already cancelled File */
      if not can-find(first fileAR where fileAR.agentfileID = ttCancelProdFile.agentFileID and fileAR.seq = 0)
       then
        do:
          lValid = false.
          createErrorRecord ("File With agentfileID: " + string(ttCancelProdFile.agentFileID) + " does not have any balance.").
        end.

       /* cancelReason is mandatory */
      if ttCancelProdFile.cancelReason = " " or ttCancelProdFile.cancelReason = "?"
       then
        do:
          lValid = false.
          createErrorRecord ("Cancellation Reason cannot be blank for agentFileID: " + string(ttCancelProdFile.agentFileID) + "." ).
        end.

       /* cancelDate is mandatory */
      if ttCancelProdFile.cancelDate = ?
       then
        do:
          lValid = false.
          createErrorRecord ("Cancellation Date cannot be blank for agentFileID: " + string(ttCancelProdFile.agentFileID) + "." ).
        end.

      /* canceldate Date must be in open period*/
      if not can-find (first period where period.periodMonth = month(ttCancelProdFile.cancelDate)
                                    and period.periodYear    = year(ttCancelProdFile.cancelDate)
                                    and period.active)
       then
        do:
          lValid = false.
          createErrorRecord ("Cancellation Date must be in the open period for agentFileID: " + string(ttCancelProdFile.agentFileID) + "." ).
        end.

      for first agentFile fields(agentFileID agentID fileID) where agentFile.agentFileID = ttCancelProdFile.agentfileID no-lock:
        cAgentID = agentFile.agentID.     
        cFileID  = agentfile.fileID.
      end.

      /* cancel date cannot be prior to the last activity date in fileAR/fileInv/artran ... */
      if can-find(last fileAR where fileAR.agentFileID = ttCancelProdFile.agentFileID 
                                  and fileAR.tranDate > ttCancelProdFile.cancelDate)
         or
         can-find(last fileInv where fileInv.agentFileID = ttCancelProdFile.agentFileID 
                                 and fileInv.invoiceDate > date(ttCancelProdFile.cancelDate))
         or
         can-find(first artran where artran.entity    = "A" and artran.entityID  = cAgentID
                                 and artran.transType = "P" and artran.fileID    = cFileID
                                 and artran.type      = "A" 
                                 and artran.tranDate  > ttCancelProdFile.cancelDate)
       then
        do:
          lValid = false.
          createErrorRecord ("Cancellation Date cannot be prior to the last activity date for agentFileID: " + string(ttCancelProdFile.agentFileID) + ".").
        end.
    end.

    if not lValid 
     then
      do:
        setContentTable("CancelFileProductionErr",input table ttCancelFileErr, input pRequest:FieldList).
        pResponse:success("3005","Validations failed, check the error file generated for more details").
        return.
      end. 

    std-lo = false.

    TRX-BLOCK:
    for each ttCancelProdFile transaction
      on error undo TRX-BLOCK, next TRX-BLOCK:
    
      for first agentFile fields(agentFileID agentID fileID) where agentFile.agentFileID = ttCancelProdFile.agentfileID no-lock:
        cAgentID = agentFile.agentID.     
        cFileID  = agentfile.fileID.
      end.

     /* to calculate writeoff balance for the file seq = 0 record i,e have balance */ 
      deFileWriteoffInvAmt = 0.
      for first fileAR where fileAR.agentfileID = ttCancelProdFile.agentfileID
                        and fileAR.seq = 0 no-lock:

        deFileWriteoffInvAmt = fileAR.amount.
      end.
         
        /* create filear record with (W)riteoff type */                   
      run ar/createfilear-p.p(input ttCancelProdFile.agentfileID,   /* agentfileID */
                              input cAgentID,              /* AgentID */
                              input cFileID,              /* fielID */ 
                              input "P",             /* sourceType  */
                              input ( - deFileWriteoffInvAmt),   /* (+-) amount */
                              input "W",                        /* type */
							  input ttCancelProdFile.cancelDate,
                              output ifilearid,
                              output std-lo).
         
 
     if not std-lo
      then
       do:
         createErrorRecord("Create (W)rite-off record failed for AgentFileID: " + string(ttCancelProdFile.agentFileID)).
         next TRX-BLOCK. 
       end.

         /*setting cancel with its reason in fileAR "W" type record*/
      for last fileAR exclusive-lock where fileAR.agentFileID = ttCancelProdFile.agentFileID 
                        and fileAR.type  = "W" by fileAr.seq:
  
        assign
            fileAR.cancelreason      = ttCancelProdFile.CancelReason
            fileAR.cancel            = true.

        validate fileAR.
        release fileAR.
      end.

      std-lo = true.
    end. /* for each ttCancelProdFile and transaction */

    if not std-lo
     then
      do:
        pResponse:fault("3000", "ProductionFile Cancel").
        setContentTable("CancelFileProductionErr",input table ttCancelFileErr, input pRequest:FieldList).
        return.
      end.
    
    /* returning record */
    empty temp-table fileInvAr.
    empty temp-table fileInvArItem.
    empty temp-table ttFilear.
	  
    for each ttCancelProdFile:
    
      for each fileInv where fileInv.agentFileID = ttCancelProdFile.agentFileID no-lock:          
        create fileInvAr.
        assign
            fileInvAr.fileInvID     = fileInv.fileInvID
            fileInvAr.invoiceDate   = fileInv.invoiceDate
            fileInvAr.invoiceNumber = fileInv.invoiceNumber
            fileInvAr.invoiceAmount = fileInv.invoiceAmount
            fileInvAr.postedDate    = fileInv.postedDate
            fileInvAr.osAmt         = fileInvAr.invoiceAmount - fileInvAr.postedAmount
            fileInvAr.posted        = fileInv.posted
            .
    
        for each fileInvitem where fileInvItem.fileInvID = fileInv.fileInvID no-lock:          
          create fileInvArItem.
    
          assign
              fileInvArItem.fileInvID     = fileInvitem.fileInvID
              fileInvArItem.invNumber     = fileInv.invoiceNumber
              fileInvArItem.seq           = fileInvitem.seq
              fileInvArItem.description   = fileInvitem.description
              fileInvArItem.arInvID       = fileInvItem.arInvID
              fileInvArItem.amount        = fileInvitem.amount
              fileInvArItem.postedamount  = fileInvItem.postedamount
              .
        end.
      end.
      for each fileAR where fileAR.agentfileID = ttCancelProdFile.agentFileID no-lock:
        create ttFilear.
        buffer-copy fileAR to ttFilear.
        assign
            ttFilear.cancelDate = fileAR.tranDate.
      end.
    end.

    setContentDataset(input dataset-handle dsHandleOutput, input pRequest:FieldLIst).
    pResponse:success("2000", "ProductionFile Cancel").
  end method.

method private void createErrorRecord (input pcErrorMsg as character):
  create ttCancelFileErr.
  assign 
    iErrorSeq                   = iErrorSeq + 1
    ttCancelFileErr.err         = string(iErrorSeq)
    ttCancelFileErr.description = pcErrorMsg
    .
end method.
      
end class.



