/*------------------------------------------------------------------------
@name DeleteFile.cls
@action agentFileDelete
@description  Delete the agent file.
@param AgentFileID;int
@throws 3001;Invalid agentFileID
@throws 3066;Agent File does not exist 
@throws 3005;Agent file is not locked by current user
@throws 3015;Atleast one order exists for agent file 
@throws 3015;Atleast one filetask exists for agent file 
@throws 3081;Locked by other user 
@throws 3000;AgentFile lock release
            ;Jobtran creation
            ;Delete failed 
@returns Success;2000
@author Sachin Anthwal
@Modified :
Date        Name          Comments     
07/04/2023  SB            Modified to remove reference to jobQueue and jobRoute DB table   
   
----------------------------------------------------------------------*/

class file.DeleteFile inherits framework.ActionBase:

  constructor public DeleteFile ():
    super().
  end constructor.

  destructor public DeleteFile ():
  end destructor.
    
  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
            
    {lib/std-def.i}
    
    define variable lLocked       as logical   no-undo.
    define variable lSuccess      as logical   no-undo.
    define variable piAgentFileID as integer   no-undo.
    define variable dsHandle      as handle    no-undo.

    pRequest:getParameter("agentFileID", output piAgentFileID).    
    
    /* validation when no parameter is supplied */
    if (piAgentFileID = 0  or piAgentFileID = ?)
     then 
      do:
        pResponse:fault("3001", "AgentfileID").
        return.
      end.
    
    /* validate agentfileID */
    if not can-find(first agentFile where agentFile.agentFileID = piAgentFileID ) 
     then 
      do:
        pResponse:fault("3066", "Agent File").
        return.
      end.
    
    /* when agent file is not locked by current user or not locked */
    {lib/islocked.i &obj='agentFile' &id=piAgentFileID &exclude-fault=true}
    if not std-lo or (std-ch ne pRequest:uid)
     then 
      do:
        pResponse:fault("3005", "Agent file is not locked by current user.").
        return. 
      end.
      
    /* agent File must not be on hold  */
    if can-find(first agentFile where agentFile.agentFileID = piAgentFileID and agentFile.isHold = true)
     then
      do:
        pResponse:fault("3005", "Agent file is in hold.").
        return.
      end.
    
    /* there must be no Orders */
    if can-find(first job where job.agentFileID = piAgentFileID) 
     then
      do:
        pResponse:fault("3015", "order" {&msg-add} "Agent file").
        return. 
      end.

    /* there must be no filetask */
    if can-find(first fileTask where fileTask.agentFileID = piAgentFileID) 
     then
      do:
        pResponse:fault("3015", "filetask" {&msg-add} "Agent file").
        return. 
      end.

       
    /* there must be no Requirements, Exceptions, or Instruments */
    if can-find(first fileContent where fileContent.agentFileID = piAgentFileID)
     then
      do:
        pResponse:fault("3005", "Agent file contain Requirements or Exceptions or Intruments.").
        return. 
      end.
    
    /* open question there must be no un-deleted Documents */
     
    lSuccess = false.
    
    TRX-BLOCK:
    for first agentFile exclusive-lock
      where agentFile.agentFileID = piAgentFileID
        on error undo TRX-BLOCK, leave TRX-BLOCK:
    
      run util/deletesyslock.p ('AgentFile', agentFile.agentFileID, 0, output lLocked).
      if not lLocked
       then
        do:
          pResponse:fault("3000", "AgentFile lock release").
          return.
        end. /* if not lLocked */
    
      /* delete all child including the fileproperty, fileParty, job route.  */
      
      for each fileProperty exclusive-lock 
        where fileProperty.agentFileID = piAgentFileID:
        delete fileProperty.
        release fileProperty.
      end. /* for each fileProperty */
    
      for each fileParty exclusive-lock 
        where fileParty.agentFileID = piAgentFileID:
        delete fileParty.
        release fileParty.
      end. /* for each fileParty */
    
      delete agentFile.
      release agentFile.
    
      /*jobtran record creation*/
      run file/createjobtran-p.p (input  piAgentFileID,          /* Agent File ID */
                                  input  pRequest:actionID,      /* Action */
                                  input  pRequest:uID,           /* Created By */
                                  input  'agentFileID='+ string(piAgentFileID), /* Query String */
                                  input  dataset-handle dsHandle). /* Payload */
       
      lSuccess = true.
    end. 
    
    if not lSuccess
     then 
      do:
        presponse:fault("3000", "Delete").
        return.
    end.
    
    pResponse:success("2000", "Agentfile delete").   
    
  end method.
 
end class.

