/*------------------------------------------------------------------------
@name ModifyVendorTopic.cls
@action vendorTopicModify
@description  Modify the vendor topic record.
@param returnRecord;logical
@param vendortopic;dataset
@throws 3005;Data Parsing Failed
@throws 3066;VendorID does not exist
@throws 3000;Modify Vendor topic failed
@returns Success;2000;
@author Kasun Dhananjaya
@Modified :
Date        Name          Comments 
10/22/23    sagar k       modified to change vendorproduct to vendortopic  
------------------------------------------------------------------------*/

class file.ModifyVendorTopic inherits framework.ActionBase:
 
  constructor public ModifyVendorTopic ():
    super().
  end constructor.

  destructor public ModifyVendorTopic ():
  end destructor.

  /* Temp Tables */
  {tt/vendortopic.i &tableAlias=ttVendortopic}
  {lib/removeNullValues.i}

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    /* Variables */
    define variable pcVendorID      as character no-undo.
    define variable pctopic         as character  no-undo.
    define variable plReturnRecord  as logical   no-undo.

    define variable dsHandle as handle no-undo.

    /* Include Files */
    {lib/std-def.i}

    /* dataset creation */
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttVendortopic:handle).

    /*Parameters get*/
    pRequest:getParameter('returnRecord', output plReturnRecord).
    pRequest:getParameter('vendorID',     output pcVendorID).
    pRequest:getParameter('Topic',        output pctopic).

    if plReturnRecord = ?
     then 
      plReturnRecord = false.

    /* get Dataset */
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.

    if pcVendorID =  "" or 
       pcVendorID =  ?  or
       pcVendorID = "?" 
     then 
      do:
        pResponse:fault("3005", "VendorID cannot be blank/Invalid value").
        return.
      end.

    if pctopic = "" or 
       pctopic = ?
     then 
      do:
        pResponse:fault("3005", "Topic cannot be blank/Invalid value").
        return.
      end.

    if not can-find(first ttVendortopic where ttVendortopic.topic = pctopic
                                           and ttVendortopic.vendorID   = pcVendorID) 
     then
      do:
        pResponse:fault('3005', 'Vendor topic record not provided').
        return.
      end.

    /* validate vendorID */
     if not can-find(first vendor where vendor.vendorID = pcVendorID)
     then 
      do:
        pResponse:fault("3066", "VendorID " + pcVendorID).
        return.
      end.


    if not can-find(first Vendortopic where Vendortopic.topic = pctopic
                                       and Vendortopic.vendorID   = pcvendorID) 
      then
      do:
        pResponse:fault('3066', "Vendor ID " + pcVendorID + " Topic " + string(pctopic)).
        return.
      end.

    removeNullvalues("ttVendortopic").

    TRX-BLOCK:
    for first Vendortopic exclusive-lock
      where Vendortopic.topic     = pctopic
       and  Vendortopic.vendorID  = pcVendorID
      transaction on error undo TRX-BLOCK, leave TRX-BLOCK:
   
      for first ttVendortopic 
          where ttVendortopic.vendorID  = Vendortopic.vendorID
           and  ttVendortopic.topic     = Vendortopic.topic:
  
        assign 
  	        Vendortopic.vendorid         = ttVendortopic.vendorId
            Vendortopic.topic            = ttVendortopic.topic
            Vendortopic.Price            = ttVendortopic.Price
  	        Vendortopic.lastModifiedBy   = pRequest:uID
  	        Vendortopic.lastModifiedDate = now
           .
      end.
  
      validate Vendortopic.
      release Vendortopic.
    
      std-lo = true.

    end.

    if not std-lo 
     then
      do:
        pResponse:fault("3000", "Modify Vendor Topic"). /* Modify Vendor Topic failed */
        return.
      end.

    if plReturnRecord 
     then 
      do:
        empty temp-table ttVendortopic.
   
        for first Vendortopic where Vendortopic.vendorID   = pcVendorID and
                                    Vendortopic.topic = pctopic no-lock:
          create ttVendortopic.
          assign
              ttVendortopic.vendorID  = Vendortopic.vendorID
              ttVendortopic.topic     = Vendortopic.topic
              ttVendortopic.Price     = Vendortopic.Price
              .
		  
		  for first vendor no-lock where vendor.vendorid = Vendortopic.vendorid:
            ttVendortopic.vendorname = vendor.vendorname.
          end.
   
        end.
   
        setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

        pResponse:success("2000", "Vendor Topic update").

      end.

  end method.

end class.
