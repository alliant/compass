/*------------------------------------------------------------------------
@name  ModifyTaskCost.cls
@action taskCostModify
@description  Update task cost

@returns ; Success/Failure on updating task cost

@author   K.R
@Modified :
Date        Name          Comments   
07/21/23    S Chandu   Modified to add check that Cost cannot be updated for Posted task.
01/28/23    S Chandu   Changed sysaction to "taskCostModify" and 
                       Added validation cost cannot be less than zero(negative).
08/05/24    S Chandu   Added validation task must be completed.
----------------------------------------------------------------------*/

class file.ModifyTaskCost inherits framework.ActionBase:

  /* temp-table defination */
  {tt/filecost.i &tablealias="ttfilecost"}
  {tt/proerr.i   &tableAlias="ttfileCostErr"}

  constructor public ModifyTaskCost ():
    super().
  end constructor.

  destructor public ModifyTaskCost ( ):
  end destructor.

  /* Global Variables*/
  define variable iErrorSeq      as integer    no-undo.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    
    define variable dsHandle as handle    no-undo.
    define variable std-ch   as character no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttfilecost:handle).

    empty temp-table ttfilecost.

    pRequest:getcontent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
      then
       do:
         pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
         return.
       end.

   /* for first ttfilecost:
      if can-find(first filetask where filetask.filetaskID = ttfilecost.filetaskID and filetask.posted = true ) 
       then
      do:
        pResponse:fault('3005', 'Cost cannot be updated for Posted task').
        return.
      end.    
    end. */

    TRX-BLOCK:
    for each ttfilecost no-lock
      transaction on error undo TRX-BLOCK, leave TRX-BLOCK:

      if ttfilecost.cost < 0
       then
        do:
          createErrorRecord("Cost cannot be less than zero for file task  " + string(ttfilecost.filetaskID)).
          next TRX-BLOCK.
        end.

	  if can-find(first filetask where filetask.fileTaskID = ttfilecost.filetaskID and filetask.completedDate = ?)
	   then
	    do:
          createErrorRecord("Cost cannot be updated for open task  " + string(ttfilecost.filetaskID)).
          next TRX-BLOCK.
	    end.

      for first filetask where ttfilecost.filetaskID = filetask.filetaskID exclusive-lock:
        if can-find(first filetask where filetask.filetaskID = ttfilecost.filetaskID and filetask.posted = true) 
         then
          do:
            createErrorRecord("Cost cannot be updated for Posted task " + string(ttfilecost.filetaskID)).
            next TRX-BLOCK.
          end.
        filetask.cost = ttfilecost.cost.
      end.

    end.
    
    if can-find(first ttfileCostErr)
     then
      do:
        setContentTable("FileTaskCostErr",input table ttfileCostErr, input pRequest:FieldList).
        pResponse:success("3005","Validations failed, check the error file generated for more details").
        return.
      end.

    pResponse:success("2000", "Updating cost").
    
  end method.
 
  method private void createErrorRecord (input pcErrorMsg as character):
  create ttfileCostErr.
  assign 
    iErrorSeq                 = iErrorSeq + 1
    ttfileCostErr.err         = string(iErrorSeq)
    ttfileCostErr.description = pcErrorMsg
    .
  end method.

end class.

