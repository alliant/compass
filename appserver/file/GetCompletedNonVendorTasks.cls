/*------------------------------------------------------------------------
@name  file/GetCompletedNonVendorTasks.cls
@action CompletedNonVendorTasksGet
@description Returns all completed tasks with zero cost
@returns The temp-table having completed tasks with zero cost
@fault 
@success 2005;char;The count of the records in data set

@author Sachin
@version 1.0
@created 07/27/2023

@Modified    :
    Date        Name                 Comments   
----------------------------------------------------------------------*/

class file.GetCompletedNonVendorTasks inherits framework.ActionBase:

 constructor public GetCompletedNonVendorTasks():
  super().
 end constructor.

 destructor public GetCompletedNonVendorTasks():
 end destructor.
    

 {tt/filetask.i &tableAlias=ttFileTask}


 method public override void act (input pRequest  as framework.IRequestable,
                                  input pResponse as framework.IRespondable):
    define variable lError          as logical     no-undo.
    define variable cErrMessage     as character   no-undo.
    
    define variable dsHandle        as handle    no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttFileTask:handle). 

    {lib/std-def.i}
  
    run file/getclosedtasks-p.p(input "",            /* UserName */
                                input "",            /* AssignedTo */
                                input "Closed",     /* Status = complete */
	  						    input "",            /* Queue */
                                input "",           /* Task */
							    input ?,
							    input ?,
                                input false,     
                                output dataset-handle dsHandle,
                                output lError,
                                output cErrMessage,
                                output std-in).

    if lError 
     then
      do:
        pResponse:fault ("3005",  "GetClosedtasks Failed: " + cErrMessage).
        return.
      end.
    
    std-in = 0.

    for each ttFileTask:
      if (ttFiletask.cost = 0.00 or ttFileTask.cost = ?)   and 
         can-find (first filetask where filetask.taskID = ttfiletask.ID and (filetask.posted = ? or filetask.posted = false))
       then
        for first sysuser fields (vendorID) no-lock where sysuser.uid = ttFileTask.AssignedTo:
          if sysuser.vendorid = ? or sysuser.vendorid = ''
           then
            std-in = std-in + 1.          
           else
            delete ttFileTask.
        end.
       else
        delete ttFileTask.
        
    end.
            
    if std-in > 0
     then
      setContentDataset(input dataset-handle dsHandle, pRequest:FieldLIst).
    
    pResponse:success("2005", string(std-in) {&msg-add} "completed non vendor task").
 end method.
 
end class.

