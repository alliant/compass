/*------------------------------------------------------------------------
@file getpartyclauses.p
@action partyClausesGet
@description Get party clauses

@returns party clauses

@success 2005;SysProp returned X rows

@author AG
@version 1.0
@created 11/11/2022
Modification:

----------------------------------------------------------------------*/

class file.GetPartyClauses inherits framework.ActionBase:
    
  {tt/partyclause.i}

  constructor GetPartyClauses ():
    super().
  end constructor.

  destructor public GetPartyClauses ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable dsHandle as handle no-undo.

    {lib/std-def.i}

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer partyclause:handle).

    std-in = 0.
    for each sysprop no-lock where sysprop.appCode     = "TPS" and 
                                   sysprop.objAction   = "Party" and
                                   sysprop.objProperty = "Clause":
    
      create partyclause.
      assign
         partyclause.objID     = sysprop.objID
         partyclause.objvalue  = sysprop.objvalue.
    
      std-in = std-in + 1.
    end.
    
    
    if std-in > 0
     then 
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).  
    
    pResponse:success("2005", STRING(std-in) {&msg-add} "Party Clauses").
             
  end method.
      
end class.


