/*------------------------------------------------------------------------
@name  ConfirmOrder.cls
@action orderConfirm
@description  close order entry task
@param filetaskID;int
@returns Fault; 
@returns Success;2000;Task complete was successful.
@author S Chandu 
@Modified :
Date        Name          Comments   
01/29/2023  S Chandu      Changed sysaction from "orderEntryFileTaskComplete" to "orderConfirm"  
                          and Added validation 'Task must be Order Entry'. 
                          
01/04/2024  K.R           Modified to add sent date parameter
05/10/2024  S.R           Modifed to update agentfile table hasmailbox field.
08/08/2024  SRK           Modified to add email validation.
08/14/2024  V.R           Modified to add the validation in Email related fields.
09/19/2024  SRK           Modified to add input in completetask-p.p
11/04/2024  Vignesh R     Modified to return the updated record. 
----------------------------------------------------------------------*/

class file.ConfirmOrder inherits framework.ActionBase:
    
  {tt/filetask.i &tableAlias=ttfiletask}
  {tt/mailcompletetask.i    &tableAlias="MailCompleteTask"} 

  constructor ConfirmOrder ():
    super().
  end constructor.

  destructor public ConfirmOrder ( ):
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
    {lib/std-def.i}

    /* local varibales */
    define variable piFileTaskID   as integer   no-undo.
    define variable pcAction       as character no-undo.
    define variable plReturnRecord as logical   no-undo.
    define variable iErrCode       as integer   no-undo.
    define variable cErrMessage    as character no-undo.
    define variable iTaskID        as integer   no-undo.
    define variable cAgentFileID   as character no-undo.

    define variable cTo            as character   no-undo.
    define variable lcBody         as longchar    no-undo.
    define variable lCC            as logical     no-undo.
    define variable pUID           as character   no-undo.
    define variable cSubject       as character   no-undo.
    define variable dsHandle       as handle      no-undo.
    define variable dsHandle1      as handle      no-undo.

    define variable cJamesDomain   as character   no-undo.
    define variable ccUID          as character   no-undo.
    define variable lSuccess       as logical     no-undo.
    define variable cMsg           as character   no-undo.

    define variable lError         as logical     no-undo.
    define variable pFrom          as character   no-undo.
    define variable lValidate      as logical     no-undo.
    define variable iemail            as integer   no-undo.

    /* simple mail */
    def var tSent as logical.
    def var tMsg as char.
    def var tServer as char.
    def var tDomain as char no-undo.
    def var tFile as char no-undo.
    def var tExtension as char no-undo.
    def var tFileDef as char no-undo init "".
    def var tImageExt as char no-undo.
    
    &scoped-define imageTypes "png"

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttfiletask:handle).

    create dataset dsHandle1.
    dsHandle1:serialize-name = 'data'.
    dsHandle1:set-buffers(buffer MailCompleteTask:handle).
    
    pRequest:getParameter("FileTaskID",   output piFileTaskID). 
    pRequest:getParameter("Action",       output pcAction).
    pRequest:getParameter("returnRecord", output plReturnRecord).

     /* validate fileTaskID */
    if piFileTaskID = 0 or piFileTaskID = ?
     then
      do:
         pResponse:fault('3001', 'FileTaskID').
        return.
      end.

     /* validate Action */
    if pcAction = '' or pcAction = ?
     then
      do:
        pResponse:fault('3001', 'Action').
        return.
      end.
          
    if not can-find(first filetask where filetask.fileTaskID = piFileTaskID ) 
     then 
      do:
        pResponse:fault('3001', 'FileTaskID').
        return.
      end.

    if can-find(first filetask where filetask.fileTaskID = piFileTaskID and filetask.posted = true) 
     then 
      do:
        pResponse:fault('3005', 'Posted task cannot be closed').
        return.
      end.

    if can-find(first filetask where filetask.fileTaskID = piFileTaskID and filetask.topic ne "Order Entry") 
     then 
      do:
        pResponse:fault('3005', 'Task must be Order Entry').
        return.
      end.

    pRequest:getContent(input-output dataset-handle dsHandle1, output std-ch).
    if std-ch > ""
     then
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.
    
    if not can-find(first MailCompleteTask)
     then
      do:
        pResponse:fault('3005', 'Input data not provided').
        return.
      end.

    
    for first MailCompleteTask:

      if MailCompleteTask.mailTO = ? or MailCompleteTask.MailTo = "" 
       then
        do:
          pResponse:fault('3005', 'To Email address cannot be blank.').
          return.
        end.

      if MailCompleteTask.mailSubject = ? or MailCompleteTask.mailSubject = "" 
       then
        do:
          pResponse:fault('3005', 'Subject cannot be blank.').
          return.
        end.


      lcBody = MailCompleteTask.mailBody no-error.
      if lcBody = ? or lcBody = "" 
       then
        do:
          pResponse:fault('3005', 'Body cannot be blank.').
          return.
        end.

    end.

    for first filetask where filetask.filetaskID = piFileTaskID no-lock:

      assign 
        iTaskID      = filetask.taskID
        cAgentFileID = string(filetask.agentfileid)
          .

      run file/gettaskbyid-p.p(input  iTaskID,
                               output dataset-handle dsHandle,
                               output iErrCode,
                               output cErrMessage).

      if iErrCode > 0 
       then 
        do:
          pResponse:fault('3005', 'Not able to retrieve the task from ARC.' + cErrMessage).
          return.
        end.
    end.

    if pcAction = "B" 
     then
      do:
         for first ttfiletask no-lock:
       
           if (ttfiletask.assignedto ne ? and ttfiletask.assignedto ne "") and ttfiletask.assignedto ne prequest:UID
            then 
             do:
               pResponse:fault('3005', 'Task cannot be completed as you are not the assigned user').
               return.
             end.
       
           if (ttfiletask.assignedto = ? or ttfiletask.assignedto = "") 
            then
             do:
               run file/assignuser-p.p(input  ttfiletask.ID,
                                       input  prequest:UID,
                                       input  ttfiletask.DueDateTime,
                                       output iErrCode,
                                       output cErrMessage).
             
              
               if iErrCode > 0 
                then 
                 do:
                   pResponse:fault("3005", "Task cannot be completed as it cannot be assigned to you." + cErrMessage).
                   return.
                 end.
             end.  
         end.
       
         /* common file */
         run file/completefiletask-p.p(input  iTaskID,
                                       input  cAgentFileID,
                                       input  false,
                                       output iErrCode,
                                       output cErrMessage).
          
         if iErrCode > 0 
          then 
           do:
             pResponse:fault("3005", "Task cannot be completed." + cErrMessage).
             return.
           end.
     end.


    if pcAction = "A" or pcAction = "B"
     then
      do:
         for first MailCompleteTask no-lock:
         
            assign
                cTo      = MailCompleteTask.mailTo
                lcBody   = MailCompleteTask.mailBody
                lCC      = MailCompleteTask.mailCc
                cSubject = MailCompleteTask.mailSubject
                pFrom    = pRequest:uid
             .
         end.
       
         if lcc = true
          then
           pUID = pRequest:uid. /* getting UID to keep CC for the mail */
         else
           pUID = "".

         do iemail = 1 to num-entries(cTo,";"):
           if not util.CommonUtils:isValidEmailAddress(entry(iemail,cTo,";"))
            then
             do:
               pResponse:fault("3005", entry(iemail,cTo,";") + " is not a valid email address").
               return.
             end.            
         end.
       
         publish "GetSystemParameter" from (session:first-procedure) ("ImageExtensions", output tImageExt).
         if tImageExt = ""
          then tImageExt = {&imageTypes}.
             
         publish "GetSystemParameter" from (session:first-procedure) ("SmtpServer", output tServer).
         if tServer = "" 
          then return.
         
         publish "GetSystemParameter" from (session:first-procedure) ("emailDomain", output tDomain).
         if tDomain = "" 
          then return.
         
         if (pFrom > "" and index(pFrom, tDomain) = 0) or pFrom = ""
          then publish "GetSystemParameter" from (session:first-procedure) ("emailDefaultFrom", output pFrom).
         if pFrom = "" 
          then return.
       
        /* sending mail is true means order entry */
         run util/smtpmail.p
                 (tServer,
                  CTo,
                  pFrom,
                  pUID,
                  "",
                  "",
                  cSubject,
                  lcBody,
                  "type=text/html:charset=us-ascii:filetype=ascii",
                  "",
                  1,
                  false,
                  "",
                  "",
                  "",
                  output tSent,
                  output tMsg).

         /* james email send  */
         for first agentFile fields (agentFileID hasmailbox) 
           where agentFile.agentFileID = int(cAgentFileID) exclusive-lock:
         
           publish "GetSystemParameter" from (session:first-procedure) ("JamesMailDomain", output cJamesDomain).
         
           if not agentfile.hasmailbox
            then
             run mail/createemailbox-p.p(input string(agentFile.agentfileID),
                                        "",
                                             output lsuccess,
                                             output cMsg). 
          
		   if lSuccess
		    then
		     agentfile.hasmailbox = true.
			 
            if pUID <> ""
             then
              cCUID = pUID + ';' + cTo.
            else
              ccUID = cTo.
          
          
            run mail/sendemail-p.p("", 
                                  string(agentFile.agentfileiD) + cJamesDomain, /*  to  agentfileiD + cjamesDoamin */
                                  pRequest:uid,                                
                                  ccUID,                                         /* uid + cc */
                                  cSubject,
                                  ?, /* passing null for sent date as it will be set by the program*/
                                  lcBody, 
                                  "", 
                                  output lSuccess,
                                  output cMsg).
          
	    validate agentfile.
	    release agentfile.
          end. /* for first agentfile */ 
    end.


    if pcAction = "B" 
     then
      do:
        run file/gettaskbyid-p.p(input  iTaskID,
                                 output dataset-handle dsHandle,
                                 output iErrCode,                             
                                 output cErrMessage).
        if iErrCode > 0 
         then 
          do:
            pResponse:fault('3005', 'Not able to retrieve the task from ARC.' + cErrMessage).
            return.
          end.

        if can-find(first ttFileTask)
         then 
          setContentDataSet(input dataset-handle dsHandle, input pRequest:FieldList).

      end.


    pResponse:success("2000", "Task Complete").
  end method.
      
end class.


