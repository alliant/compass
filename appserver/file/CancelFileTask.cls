/*------------------------------------------------------------------------
@name  CancelFileTask.cls
@action fileTaskCancel
@description  cancel the task
@param fileTaskID;int
 

@returns ; success/failure returned from cancelfiletask-p.p

@author   sagar k
@Date     04/25/2023
@Modified :
Date        Name     Comments   
05/30/2023  SB       Modify to add rule - Only tasks assigned to you 
                     and those that are not assigned to anyone can be canceled. 
07/21/2023  SK       Modified to add Posted field  
02/28/2024  K.R      Modified to add validation for completed date
08/08/2024  VR       Modified to change the Error Message Codes.
----------------------------------------------------------------------*/

class file.CancelFileTask inherits framework.ActionBase:

  {tt/filetask.i &tableAlias=ttfiletask}

  constructor public CancelFileTask ():
    super().
  end constructor.

  destructor public CancelFileTask ( ):
  end destructor.
 
  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    define variable piFileTaskID  as integer   no-undo.

    define variable iErrCode      as integer   no-undo.
    define variable cErrMessage   as character no-undo.
    define variable iTaskID       as integer   no-undo.
    define variable dsHandle      as handle    no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttfiletask:handle).
    
    pRequest:getParameter("FileTaskID",  output piFileTaskID). 

     /* validate fileTaskID */
    if piFileTaskID = 0 or piFileTaskID = ?
     then
      do:
        pResponse:fault('3001', 'FileTaskID').
        return.
      end.

   
    if not can-find(first filetask where filetask.fileTaskID = piFileTaskID ) 
     then 
      do:
        pResponse:fault('3001', 'FileTaskID').
        return.
      end.

    if can-find(first filetask where filetask.fileTaskID = piFileTaskID and filetask.posted = true) 
     then 
      do:
        pResponse:fault('3005', 'Posted task cannot be cancelled').
        return.
      end.
	  
	if can-find(first filetask where filetask.fileTaskID = piFileTaskID and filetask.completedDate <> ?)
	 then
	  do:
	    pResponse:fault("3005", "Task cannot be cancelled as it is already completed.").
	    return.
	  end.
    
    for first filetask where filetask.filetaskID = piFileTaskID no-lock:

      assign iTaskID = filetask.taskID.

      run file/gettaskbyid-p.p(input  iTaskID,
                               output dataset-handle dsHandle,
                               output iErrCode,
                               output cErrMessage).

      if iErrCode > 0 
       then 
        do:
          pResponse:fault('3005', 'Not able to retrieve the task from ARC.' + cErrMessage).
          return.
        end.
    end.

    for first ttfiletask no-lock:

      if (ttfiletask.assignedto ne ? and ttfiletask.assignedto ne "") and ttfiletask.assignedto ne prequest:UID
       then 
        do:
          pResponse:fault('3005', 'Task cannot be canceled as you are not the assigned user.').
          return.
        end.

      if (ttfiletask.assignedto = ? or ttfiletask.assignedto = "") 
       then
        do:
          run file/assignuser-p.p(input  ttfiletask.ID,
                                  input  prequest:UID,
                                  input  ttfiletask.DueDateTime,
                                  output iErrCode,
                                  output cErrMessage).
        
         
          if iErrCode > 0 
           then 
            do:
              pResponse:fault("3005", "Task cannot be canceled as it cannot be assigned to you." + cErrMessage).
              return.
            end.
        end.  
    end.
     
    run file/cancelfiletask-p.p(input  iTaskID,
                                output iErrCode,
                                output cErrMessage).

    if iErrCode > 0 
     then 
      do:
        pResponse:fault("3005", "Task cannot be canceled." + cErrMessage).
        return.
      end.
        
    pResponse:success("2000", "Task Cancel").
    
  end method.
 
end class.


