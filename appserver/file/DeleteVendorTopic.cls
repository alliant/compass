/*------------------------------------------------------------------------
@name DeleteVendorTopic.cls
@action vendorTopicDelete
@description  Delete the vendor Topic record.
@param returnRecord;logical
@param vendortopic;dataset
@throws 3005;XML Parsing Failed
@throws 3066;VendorID does not exist
@throws 3000;Delete Vendor topic failed
@returns Success;2000;
@author S Chandu
@Modified :
Date        Name          Comments   
10/22/23    Sagar K       modified to change vendorproduct to vendortopic
------------------------------------------------------------------------*/

class file.DeleteVendorTopic inherits framework.ActionBase:

  constructor public DeleteVendorTopic ():
    super().
  end constructor.

  destructor public DeleteVendorTopic ():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}
    
    define variable pcVendorID   as character no-undo.
    define variable pctopic      as character no-undo.

    pRequest:getParameter("VendorID",  output pcVendorID).
    pRequest:getParameter("Topic", output pctopic).

    if pcVendorID = ""  or
       pcVendorID = "?" or
       pcVendorID =  ? 
     then
      do:
        pResponse:fault("3005", "VendorID cannot be blank/Invalid value").
        return.
     end.
   
   if pctopic = "" or 
      pctopic = ?
    then 
     do:
       pResponse:fault("3005", "Topic cannot be Blank/Invalid value").
       return.
     end.
   
   if not can-find(first vendortopic where       vendortopic.topic = pctopic
                                        and vendortopic.vendorID   = pcVendorID) 
    then
     do:
       pResponse:fault('3066', "VendorID " + pcVendorID + " Topic " + string(pctopic)).
       return.
     end.

    Trx-Block:
    for first vendortopic exclusive-lock
      where vendortopic.topic = pctopic
       and  vendortopic.vendorID  = pcVendorID
       transaction on error undo Trx-Block, leave Trx-Block:
    
      delete vendortopic.
      release vendortopic.
    
      std-lo = true.
    end.
     
    if not std-lo
     then
      do:
        pResponse:fault("3000", "Delete Vendor Topic"). /* Delete Vendor Topic failed */
        return.
      end.

    pResponse:success("2000", "Delete Vendor Topic"). /* Delete Vendor Topic was successful */ 
  end method.
  
end class.


