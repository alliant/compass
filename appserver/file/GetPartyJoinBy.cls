/*------------------------------------------------------------------------
@file GetPartyJoinBy.p
@action partyJoinByGet 
@description Get party Join By

@returns party Join

@success 2005;SysProp returned X rows

@author SRK
@version 1.0
@created 08/29/2024
Modification:

----------------------------------------------------------------------*/

class file.GetPartyJoinBy inherits framework.ActionBase:
    
  {tt/partyjoinby.i}

  constructor GetPartyJoinBy ():
    super().
  end constructor.

  destructor public GetPartyJoinBy ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable dsHandle as handle no-undo.

    {lib/std-def.i}

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer partyjoinby:handle).

    std-in = 0.
    for each sysprop fields(appCode objAction objProperty objvalue) no-lock 
                            where sysprop.appCode     = "TPS" and 
                                  sysprop.objAction   = "Party" and
                                  sysprop.objProperty = "JoinBy":
    
      create partyjoinby.
      assign
         partyjoinby.objvalue    = sysprop.objvalue.
    
      std-in = std-in + 1.
    end.
    
    
    if std-in > 0
     then 
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).  
    
    pResponse:success("2005", STRING(std-in) {&msg-add} "Party Join By").
             
  end method.
      
end class.


