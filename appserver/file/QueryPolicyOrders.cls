/*------------------------------------------------------------------------
@file QueryPolicyOrders.cls
@action PolicyOrdersQuery
@description Get Open Policy Orders
@returns Open Policy Orders
@success 2005;X Open Policy Orders(s) were returned
@author Shefali
@version 1.0
@created 12/24/2024
Modification:

----------------------------------------------------------------------*/

class file.QueryPolicyOrders inherits framework.ActionBase:
    
  {tt/queryOrders.i &tableAlias="queryPolicyOrders"}
  {tt/filetask.i &tableAlias="ttOpenTasks"}

  constructor QueryPolicyOrders ():
    super().
  end constructor.

  destructor public QueryPolicyOrders ():
  end destructor.   

    /* Global variables */
  define variable pcStateID         as character no-undo.
  define variable pcOpenTasks       as character no-undo.
  define variable piMinCreatedHours as integer   no-undo.
  define variable std-in            as integer   no-undo.

  define buffer bufOpenTasks for ttOpenTasks.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
    define variable lError            as logical   no-undo.
    define variable cErrMessage       as character no-undo.
    define variable ipriorFulfilments as integer   no-undo.
	define variable dtCreatedDate     as datetime  no-undo.
    define variable iCreatedHours     as integer   no-undo.
    define variable cAgent            as character no-undo.
    define variable cAgentID          as character no-undo.
    define variable cAgentStateID     as character no-undo.
    define variable iCount            as integer   no-undo.

    define variable dsHandle          as handle    no-undo.
    define variable dsHandle1         as handle    no-undo.
	
    pRequest:getParameter("StateID",         output pcStateID).
    pRequest:getParameter("OpenTasks",       output pcOpenTasks).	
    pRequest:getParameter("MinCreatedHours", output piMinCreatedHours).

    /* validate input params */
    if pcStateID = "ALL"
     then
      pcStateID = ''.

    if pcOpenTasks = ? or pcOpenTasks = '' 
     then
      pcOpenTasks  = 'B'.

    if piMinCreatedHours < 0
     then
      do:
        pResponse:fault("3005", "Created Hours cannot be less than zero").
        return. 
      end.

    create dataset dsHandle1.
    dsHandle1:serialize-name = 'data'.
    dsHandle1:set-buffers(buffer ttOpenTasks:handle).

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer queryPolicyOrders:handle).

    run file/getopentasks-p.p(input "", 
                              input "",          
                              input "New,Open",
							  input "",
                              input "",
                              output dataset-handle dsHandle1,
                              output lError,
                              output cErrMessage,
                              output iCount).


    if lError 
     then
      do:
       pResponse:fault ("3005",  "GetOpentask Failed: " + cErrMessage).
       return.
      end.

 /* Production Files which have atleast one Open Task and have atleast one open order */
    if pcOpenTasks = 'Y' 
     then
      for each ttOpenTasks no-lock
                 where (ttOpenTasks.stateID = if (pcStateID = '' or pcStateID = ? ) then ttOpenTasks.stateID else pcStateID)
                 break by ttOpenTasks.agentFileID:
      
        if first-of(ttOpenTasks.agentfileID)
         then
            /* get createddate and hours of latest job */
          for last job fields(createddate agentfileid) no-lock where job.agentfileid = ttOpenTasks.AgentFileId 
                                 and job.jobtype = 'Policy' 
                                 and (job.stat = 'N' or job.stat = 'P') 
                                 and integer(interval(now,job.createdDate,'hours')) >= piMinCreatedHours
                                  by job.suffix:
                    
                assign
                    dtCreatedDate  = job.createdDate 
                    iCreatedHours  = integer(interval(now,job.createdDate,'hours'))
                    cAgent         = ttOpenTasks.agent + " ( " + ttOpenTasks.agentID + ")" 
                    cAgentStateID  = ttOpenTasks.stateID
                    .

               CreatePolicyOrder(job.agentfileID, dtCreatedDate, iCreatedHours, cAgent, cAgentStateID).                       
             
          end. /* last job */
          
      end. /* for each ttOpentasks */
    else if pcOpenTasks = 'N' /* files with closed tasks but have open order */
     then
      for each job fields(createdDate agentfileid) no-lock
                where job.jobtype = "Policy" and (job.stat = 'N' or job.stat = 'P') and integer(interval(now,job.createdDate,'hours')) >= piMinCreatedHours
                  and not can-find(first ttOpenTasks where ttOpenTasks.agentFileID =  job.agentFileID):     
        
         /* Get agentID from agentfileID */
         for first agentfile fields (agentID) where agentfile.agentfileID = job.agentFileID no-lock:
           for first agent fields (agentID) no-lock:
             assign
                 cAgentID   = agent.agentID
                 .
             
           end.
         end.

         for first agent fields(agentID name stateID) no-lock where agent.agentID = cAgentID
                                   and (agent.stateID = if (pcStateID = '' or pcStateID = ? ) then agent.stateID else pcStateID):
                                   
           assign
               dtCreatedDate  = job.createdDate 
               iCreatedHours  = integer(interval(now,job.createdDate,'hours'))
               cAgent         = agent.name + " ( " + agent.agentID + ")" 
               cAgentStateID  = agent.stateID
               .

           CreatePolicyOrder(job.agentfileID, dtCreatedDate, iCreatedHours, cAgent, cAgentStateID).                       
         end.
      end. /* for each job */  

    else if pcOpenTasks = 'B' /* (B)oth can have open order file or open/closed task file */
     then
       for each job fields(createdDate agentfileid) no-lock
              where job.jobtype    = "Policy"
                and (job.stat = 'N' or job.stat = 'P') 
                and integer(interval(now,job.createdDate,'hours')) >= piMinCreatedHours:
     
         /* if found in ttOpentasks use its agent details else fetch normally */
         if can-find(first ttOpentasks where ttOpentasks.agentfileID = job.agentFileID )
          then
           for first ttOpentasks no-lock where ttOpentasks.agentfileID = job.agentFileID
                                   and (ttOpenTasks.stateID = if (pcStateID = '' or pcStateID = ? ) then ttOpenTasks.stateID else pcStateID) :
              
             assign
                 dtCreatedDate  = job.createdDate 
                 iCreatedHours  = integer(interval(now,job.createdDate,'hours'))
                 cAgent         = ttOpenTasks.agent + " ( " + ttOpenTasks.agentID + ")"  
                 cAgentStateID  = ttOpenTasks.stateID
                 .
             CreatePolicyOrder(job.agentfileID, dtCreatedDate, iCreatedHours, cAgent, cAgentStateID).
           end.
         else
          do:
            /* Get agentID from agentfileID */
            for first agentfile fields (agentID) where agentfile.agentfileID = job.agentFileID no-lock:
              for first agent fields (agentID) no-lock:
                assign
                    cAgentID   = agent.agentID
                    .
                
              end.
            end.

            for first agent fields(agentID name stateID) no-lock where agent.agentID = cAgentID
                                      and (agent.stateID = if (pcStateID = '' or pcStateID = ? ) then agent.stateID else pcStateID):
                                   
              assign
                  dtCreatedDate  = job.createdDate 
                  iCreatedHours  = integer(interval(now,job.createdDate,'hours'))
                  cAgent         = agent.name + " ( " + agent.agentID + ")" 
                  cAgentStateID  = agent.stateID
                  .
             
              CreatePolicyOrder(job.agentfileID, dtCreatedDate, iCreatedHours, cAgent, cAgentStateID).                       
            end.
         end.
     end. /* for each job */

    if std-in > 0 then
      setcontentdataset(input dataset-handle dsHandle,input pRequest:FieldList).

    pResponse:success("2000", string(std-in) + " Open Policy Order").
             
  end method.
      
  method public void CreatePolicyOrder(input iAgentFileID  as integer,
                                       input dtCreatedDate as datetime,
                                       input iCreatedHours as integer,
                                       input cAgent        as character,
                                       input CAgentStateID as character):
  

    for first agentfile fields(agentFileID FileNumber closingDate notes) no-lock 
                  where agentfile.agentfileID  = iAgentFileID:

      /* create temp-table queryPolicyOrders */
      create queryPolicyOrders.
      assign
          queryPolicyOrders.agentFileID   = agentFile.agentFileID
          queryPolicyOrders.FileNumber    = agentFile.FileNumber
          queryPolicyOrders.closingDate   = agentfile.closingDate
          queryPolicyOrders.notes         = agentfile.notes
          queryPolicyOrders.createdDate   = dtCreatedDate 
          queryPolicyOrders.createdHours  = iCreatedHours        
          .

      /* Oldest open tasks fields when file have atleast 1 open tasks */
      if can-find(first ttOpenTasks where ttOpenTasks.agentFileID = agentFile.agentFileID) and ( pcOpenTasks = 'Y' or pcOpenTasks = 'B' )
       then
        for first bufOpenTasks no-lock 
                        where bufOpenTasks.agentFileID = agentFile.agentFileID
                           by bufOpenTasks.ID:
          assign 
              queryPolicyOrders.AssignedTo    = bufOpenTasks.AssignedTo
              queryPolicyOrders.AssignedDate  = bufOpenTasks.AssignedDate
              queryPolicyOrders.elapsedHours  = integer(interval(now,bufOpenTasks.AssignedDate,'hours'))
              queryPolicyOrders.oldestOpenTask = bufOpenTasks.topic
              .

            
            /* to pick the assigned name */
           for first sysuser fields (name) where sysuser.uid = ttOpenTasks.AssignedTo no-lock :
                queryPolicyOrders.AssignedToName = sysuser.name.
           end.
        end.

      assign 
          queryPolicyOrders.agent     = cAgent           
          queryPolicyOrders.stateID   = cAgentStateID
          queryPolicyOrders.StateDesc = getStateDesc(cAgentStateID)
          .
          
      /* county details from fileproperty */
      for first fileproperty fields(stateID countyID agentfileid) no-lock
                       where fileproperty.agentfileid = agentfile.AgentFileId
                         and fileproperty.stateID     = cAgentStateID:
        queryPolicyOrders.county = getCountyDesc(fileProperty.countyID, fileProperty.stateID).
      end.

      /* to find the prior fulfillments on the file */
      queryPolicyOrders.priorFulfillments = getPriorFulfillments(agentfile.agentfileid).
	       
      std-in = std-in + 1 .
    end. /* for first agentfile */

  end method.

  method public character getStateDesc(input cStateID as character):

    define variable cStateDesc as character no-undo.

    for first state field (description) where state.stateID = cStateID no-lock:
      cStateDesc = if state.description > ''then state.description else cStateID.
    end.
    
    if cStateDesc = '' then
      cStateDesc = cStateID.
   
    return cStateDesc.

  end method.

  method public character getCountyDesc(input cCountyID as character,input cStateID as character ):

     for first county field(description) no-lock
         where county.countyID = cCountyID
           and county.stateID  = cStateID:
       return county.description.
     end.
    
     return cCountyID.

  end method.

  method public integer getPriorFulfillments(input iAgentfileID as integer):

    define variable ipriorFulfilments as integer initial 0 no-undo.

    for each job fields(orderID) no-lock where job.agentfileid = iAgentfileID and job.jobtype = "Policy" and job.stat = "F":
	  
       ipriorFulfilments = ipriorFulfilments + 1.
    end.

    return ipriorFulfilments.

  end method.

end class.


