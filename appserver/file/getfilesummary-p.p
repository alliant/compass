/*------------------------------------------------------------------------
@name  getfilesummary-p.p
@action fileSummaryGet
@description Get file summary 
@author   Sagar Koralli
@Modified :
Date        Name          Comments   
04/21/2023  Spandana      Changed as json format   
05/16/2023  SB            Modified to add render functionality    
07/04/2023  SB            Modified to remove reference to jobQueue and jobRoute   
08/01/2024  S chandu      Modified to resolve twice state issue and added closing date. 
08/09/2024  K.R           Modified to get deed information from deed table
10/07/2024  SRK           Modified to return County Desc
----------------------------------------------------------------------*/
define input  parameter piAgentFileID  as integer     no-undo.
define input  parameter pcAction       as character   no-undo.
define input  parameter pLocalZone     as datetime-tz no-undo.
define output parameter dataset-handle dsHandle.
define output parameter plError        as logical     no-undo.
define output parameter pcErrorMsg     as character   no-undo.
{lib/std-def.i}
function getStateDesc return char(cStateID as char) forward.
function getAgentName return char(cAgentID as char) forward.
function getCountyDesc return char(cCountyID as char, cStateID as char) forward.
function getFilePartyEntityTypeDesc return char(cEntityType as char) forward.
function getorderStatusDescription return char(cOrderStatus as char) forward.

/* Used for getting the ARC filesummary */
define variable lSuccess        as logical   no-undo.
define variable cMsg            as character no-undo.
define variable iCount          as integer   no-undo.

define variable utcMinutes      as integer   no-undo.

{lib/arc-repository-procedures.i}

/* Functions and Procedures */
{lib/run-http.i}
{lib/arc-bearer-token.i}

/*temp tables*/
{tt/filetask.i &tableAlias=ttfileTask}

{tt/renderFileDocument.i}

define buffer bfJob for job.

/* to pick local time zone */
assign
    utcMinutes      = timezone(pLocalZone) /* converted to utc required format */
    .

create dataset dsHandle.
dsHandle:serialize-hidden = true.
dsHandle:set-buffers(buffer ttAgentFile:handle,
                     buffer ttOffice:handle,
                     buffer ttBuyer:handle,
                     buffer ttSeller:handle,
                     buffer ttLender:handle,
                     buffer ttFileProperty:handle).

dsHandle:add-relation(dsHandle:get-buffer-handle(1), dsHandle:get-buffer-handle(2), "officeID,officeID,agentID,agentID",?,yes,?,?).
dsHandle:add-relation(dsHandle:get-buffer-handle(1), dsHandle:get-buffer-handle(3), "agentFileID,agentFileID",?,yes,?,?).
dsHandle:add-relation(dsHandle:get-buffer-handle(1), dsHandle:get-buffer-handle(4), "agentFileID,agentFileID",?,yes,?,?).
dsHandle:add-relation(dsHandle:get-buffer-handle(1), dsHandle:get-buffer-handle(5), "agentFileID,agentFileID",?,yes,?,?).
dsHandle:add-relation(dsHandle:get-buffer-handle(1), dsHandle:get-buffer-handle(6), "agentFileID,agentFileID",?,yes,?,?).

  /* validate agentfileID */
if not can-find(first agentfile where agentfile.agentfileID = piAgentFileID ) 
 then 
  do:
    assign plError    = true
           pcErrorMsg = "AgentFileID does not exist".
    return.
  end. 

publish "GetSystemParameter" ("ARCGetFileSummary", output tURL).
tURL = tURL + "/" + string(piAgentFileID).

run HTTPGet ("fileSummaryGet", tURL, CreateArcHeader(""),output lSuccess, output cMsg, output tResponseFile).

if not lSuccess  
 then
  do:
    assign plError = true
           pcErrorMsg = if cMsg = '' then "Failed to connect to the ARC" else cMsg.
    return.
  end.

lSuccess = temp-table ttFileTask:read-json("FILE",tResponseFile) no-error.

/*Validating parsing of response*/
if not lSuccess OR error-status:error
 then 
  do: 
    plError = true.
    do iCount = 1 to error-status:num-messages:
      pcErrorMsg = pcErrorMsg + error-status:get-message(iCount) + ' '. 
    end.
    return.
  end.
    
run getData in this-procedure.

procedure getData: 
  define variable lSuccess     as logical   no-undo.
  define variable cErrMessage  as character no-undo.
  define variable iTemplateID  as integer   no-undo.

  for first sysaction where action = pcAction no-lock:
      assign iTemplateID = sysaction.templateID.
  end.
  
  for each agentFile where agentFile.agentFileID = piAgentFileID:
    create ttAgentFile.
    assign 
      ttAgentFile.agentFileID                  = agentFile.agentFileID
      ttAgentFile.agentID                      = agentFile.agentID
      ttAgentFile.agentName                    = getAgentName(agentFile.agentID)
      ttAgentFile.fileNumber                   = agentFile.fileNumber
      ttAgentFile.commitmentNumber             = agentFile.agentfileID
      ttAgentFile.typeOfEstate                 = agentFile.transactionType
      ttAgentFile.buyerDescription             = agentFile.buyerDescription
      ttAgentFile.buyerDescriptionUnformatted  = agentFile.buyerDescription
      ttAgentFile.sellerDescription            = agentFile.sellerDescription
      ttAgentFile.sellerDescriptionUnformatted = agentFile.sellerDescription
      ttAgentFile.legalDescription             = agentFile.propertyDescription
      ttAgentFile.legalDescriptionUnformatted  = agentFile.propertyDescription
      ttAgentFile.officeID                     = agentFile.officeID
      ttAgentFile.templateID                   = iTemplateID
      ttAgentFile.orderedBy                    = agentFile.orderedBy
      ttAgentFile.orderedEmail                 = agentFile.orderedEmail
      ttAgentFile.orderedPhone                 = agentFile.orderedPhone
      ttAgentFile.orderedDate                  = agentFile.orderedDate
      ttAgentFile.searchOrdered                = agentFile.searchOrdered
      ttAgentFile.policyOrdered                = agentFile.policyOrdered
      .

    for last job where job.agentFileID = agentFile.agentFileID
                        and (job.stat = 'N' or job.stat = 'P') no-lock:

       assign
           ttAgentFile.orderID            = job.orderID
           ttAgentFile.orderDueDate       = string(date(agentfile.closingdate)) /* Estimated Closing Date */
           ttAgentFile.effectiveDate      = date(string(datetime-tz(job.effectiveDate,utcMinutes)))
           ttAgentFile.effectiveTime      = string(integer(truncate(mtime(datetime-tz(job.effectiveDate,utcMinutes)) / 1000, 0)), "HH:MM:SS")
           .

       if job.jobType = "Search" 
        then 
         assign ttAgentFile.productDescription = "Search".
        else if job.jobType = "Policy" 
         then
          do:
            if can-find(last bfJob where bfJob.agentFileID = agentFile.agentFileID and bfJob.jobtype = "Search" and (bfJob.stat = 'N' or bfJob.stat = 'P')) 
             then
              assign ttAgentFile.productDescription = "Search".
             else
              assign ttAgentFile.productDescription = "Policy".
          end.
         else 
          assign ttAgentFile.productDescription = "".
    end.

    release ttAgentFile.

    for first office where office.officeID = agentFile.officeID
                       and office.agentID  = agentFile.agentID no-lock:
        create ttOffice.
        assign
            ttOffice.officeID = office.officeID
            ttOffice.agentID  = office.agentID
            ttOffice.name     = office.name
            ttOffice.contact  = office.phone
            ttOffice.email    = office.email
            .

        if office.addr1 > "" 
         then
          ttOffice.address = office.addr1.

        if office.addr2 > ""
         then
          ttOffice.address = if ttOffice.address > "" then ttOffice.address + ", " + office.addr2 else office.addr2.

        if office.city > ""
         then
          ttOffice.address = if ttOffice.address > "" then ttOffice.address + ", " + office.city else office.city.
    
        if office.state > ""
         then
          ttOffice.address = if ttOffice.address > "" then ttOffice.address + ", " + office.state else office.state.

        if office.zip > ""
         then
          ttOffice.address = if ttOffice.address > "" then ttOffice.address + ", " + office.zip else office.zip.


        release ttOffice.
      end.
  end.

  for each fileParty where fileParty.agentFileID = piAgentFileID no-lock:

    if fileParty.partyType = 'B' 
     then
      do:
        create ttBuyer.
        assign
          ttBuyer.agentFileID = fileParty.agentFileID
          ttBuyer.name        = fileParty.name
          ttBuyer.joinBy      = fileParty.joinBy
          ttBuyer.type        = getFilePartyEntityTypeDesc(fileParty.entityType)
          ttBuyer.policyID    = fileParty.policyID
          .

      end.
     else if fileParty.partyType = 'S' 
      then
       do:
         create ttSeller.
         assign
           ttSeller.agentFileID = fileParty.agentFileID
           ttSeller.name        = fileParty.name
           ttSeller.joinBy      = fileParty.joinBy
           ttSeller.type        = getFilePartyEntityTypeDesc(fileParty.entityType)
           .

         if fileParty.addr1 > ""
          then
           ttSeller.address = fileParty.addr1.
     
         if fileParty.addr2 > ""
          then
           ttSeller.address = if ttSeller.address > "" then ttSeller.address + ", " + fileParty.addr2 else fileParty.addr2.
    
         if fileParty.addr3 > ""
          then
           ttSeller.address = if ttSeller.address > "" then ttSeller.address + ", " + fileParty.addr3 else fileParty.addr3.

         if fileParty.addr4 > ""
          then
           ttSeller.address = if ttSeller.address > "" then ttSeller.address + ", " + fileParty.addr4 else fileParty.addr4.

         if fileParty.city > "" 
          then
           ttSeller.address = if ttSeller.address > "" then ttSeller.address + ", " + fileParty.city else fileParty.city.

         if fileParty.state > ""
          then
           ttSeller.address = if ttSeller.address > "" then ttSeller.address + ", " + fileParty.state else fileParty.state.

         if fileParty.zip > ""
          then
           ttSeller.address = if ttSeller.address > "" then ttSeller.address + ", " + fileParty.zip else fileParty.zip.

         release ttSeller.
       end.
     else if fileParty.partyType = 'L' 
      then
       do:
         create ttLender.
         assign
           ttLender.agentFileID = fileParty.agentFileID
           ttLender.name        = fileParty.name
           ttLender.clause      = fileParty.clause
           ttLender.coverage    = fileParty.liabilityAmount
           ttLender.policyID    = fileParty.policyID
           .

         if fileParty.addr1 > ""
          then
           ttLender.address = fileParty.addr1.
     
         if fileParty.addr2 > ""
          then
           ttLender.address = if ttLender.address > "" then ttLender.address + ", " + fileParty.addr2 else fileParty.addr2.
    
         if fileParty.addr3 > ""
          then
           ttLender.address = if ttLender.address > "" then ttLender.address + ", " + fileParty.addr3 else fileParty.addr3.

         if fileParty.addr4 > ""
          then
           ttLender.address = if ttLender.address > "" then ttLender.address + ", " + fileParty.addr4 else fileParty.addr4.

         if fileParty.city > "" 
          then
           ttLender.address = if ttLender.address > "" then ttLender.address + ", " + fileParty.city else fileParty.city.

         if fileParty.state > ""
          then
           ttLender.address = if ttLender.address > "" then ttLender.address + ", " + fileParty.state else fileParty.state.

         if fileParty.zip > ""
          then
           ttLender.address = if ttLender.address > "" then ttLender.address + ", " + fileParty.zip else fileParty.zip.

         release ttLender.
       end.
  end.

  for each fileProperty where fileProperty.agentFileID = piAgentFileID no-lock:
    create ttFileProperty.
    assign
      ttFileProperty.agentFileID          = fileProperty.agentFileID
      ttFileProperty.parcelID             = fileProperty.parcelID
      ttFileProperty.county               = getCountyDesc(fileProperty.county,fileProperty.state)
      ttFileProperty.section              = fileProperty.section
      ttFileProperty.township             = fileProperty.township
      ttFileProperty.townshipDirection    = fileProperty.townshipDirection
      ttFileProperty.range                = fileProperty.range
      ttFileProperty.rangeDirection       = fileProperty.rangeDirection
      ttFileProperty.condominum           = fileProperty.condominum
      ttFileProperty.subdivision          = fileProperty.subdivision
      ttFileProperty.unitno               = fileProperty.unitno
      ttFileProperty.instrumentNumber     = fileProperty.instrumentNumber
      ttFileProperty.lot                  = fileProperty.lot
      ttFileProperty.block                = fileProperty.block
      ttFileProperty.book                 = fileProperty.book
      ttFileProperty.pageNumber           = fileProperty.pageNumber
      ttFileProperty.levels               = fileProperty.levels
      ttFileProperty.platDate             = fileProperty.platDate
      ttFileProperty.latitude             = fileProperty.latitude
      ttFileProperty.longitude            = fileProperty.longitude
      ttFileProperty.shortLegal           = fileProperty.shortLegal
      ttFileProperty.longLegal            = fileProperty.longLegal
      ttFileProperty.longLegalUnformatted = fileProperty.longLegal
      ttFileProperty.taXYear              = fileProperty.taxYear
      ttFileProperty.taxAmtDue            = fileProperty.taxAmtDue
      ttFileProperty.taxAmtPaid           = fileProperty.taxAmtPaid 
      .  


    if fileProperty.addr1 > "" 
     then
      ttFileProperty.address = fileProperty.addr1.
  
    if fileProperty.addr2 > ""
     then
      ttFileProperty.address = if ttFileProperty.address > "" then ttFileProperty.address + ", " + fileProperty.addr2 else fileProperty.addr2.

    if fileProperty.addr3 > ""
     then
      ttFileProperty.address = if ttFileProperty.address > "" then ttFileProperty.address + ", " + fileProperty.addr3 else fileProperty.addr3.

    if fileProperty.addr4 > ""
     then
      ttFileProperty.address = if ttFileProperty.address > "" then ttFileProperty.address + ", " + fileProperty.addr4 else fileProperty.addr4.

    if fileProperty.city > ""
     then
      ttFileProperty.address = if ttFileProperty.address > "" then ttFileProperty.address + ", " + fileProperty.city else fileProperty.city.

    if fileProperty.state > ""
     then
       ttFileProperty.address = if ttFileProperty.address > "" then ttFileProperty.address + ", " + fileProperty.state else fileProperty.state.


    for first deed no-lock where deed.agentfileID = piAgentFileID and deed.seq = fileproperty.seq:
      assign
        ttFileProperty.deedGrantor          = deed.Grantor
        ttFileProperty.deedGrantee          = deed.Grantee
        ttFileProperty.deedDate             = deed.deedDate
        ttFileProperty.deedREcordedDate     = deed.RecordedDate
        ttFileProperty.deedBook             = deed.Book
        ttFileProperty.deedPage             = deed.PageNumber
       .
  end.

    release ttFileProperty.

  end.

end procedure.

function getStateDesc return char(cStateID as char):

  define variable cStateDesc as character no-undo.

  for first state field (description) where state.stateID = cStateID no-lock:
    cStateDesc = if state.description > ''then state.description else cStateID.
  end.
  
  if cStateDesc = '' then
    cStateDesc = cStateID.

  return cStateDesc.

end function.


function getAgentName return char(cAgentID as char):
  
  define variable cAgentName as character no-undo.

  for first agent field (name) where agent.agentID = cAgentID no-lock:
    cAgentName = if agent.name > '' then agent.name else cAgentID.
  end.

  if cAgentName = '' then
    cAgentName = cAgentID.  

  return cAgentName.

end function.

function getCountyDesc return char(cCountyID as char, cStateID as char):

  for first county field(description) no-lock
      where county.countyID = cCountyID
        and county.stateID  = cStateID:
    return county.description.
  end.

  return cCountyID.

end function.

function getFilePartyEntityTypeDesc return char(cEntityType as char):
  if cEntityType = 'I' 
   then
    return "Individual".
   else if cEntityType = 'C' 
    then
     return "Corporate".
   else
    return cEntityType.
end function.

function getorderStatusDescription return char(cOrderStatus as char):
  if cOrderStatus = 'R' 
   then
    return "Rendered".
   else if cOrderStatus = 'X' 
    then
     return "Cancelled".
   else if cOrderStatus = 'C' 
    then
     return "Closed".
   else if cOrderStatus = 'P' 
    then
     return "Processing".
   else if cOrderStatus = 'N' 
    then
     return "New".
   else
      return cOrderStatus.
end function.

