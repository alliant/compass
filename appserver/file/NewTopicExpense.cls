/*------------------------------------------------------------------------
@name file.NewTopicExpense.cls
@action  topicExpenseNew
@description  Creates a new topicexpense.
@param  
@throws 3005;Data Parsing Failed  
@throws 3005;Topicexpense record not found.
@throws 3005;Topic already exists. 
@throws 3000;Topicexpense create failed
@author Sagar K
@version 1.0
@created 07/31/2023
@Modified :
Date        Name          Comments  

----------------------------------------------------------------------*/

class file.NewTopicExpense inherits framework.ActionBase:
    
  {tt/topicexpense.i &tableAlias="tttopicexpense"}
  {lib/removeNullValues.i}

  constructor NewTopicExpense ():
    super().
  end constructor.

  destructor public NewTopicExpense ():
  end destructor.
 

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable lSuccess       as logical   no-undo.
    define variable cMsg           as character no-undo.
    define variable dsHandle       as handle    no-undo.

     /* dataset creation */
    create dataset  dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer tttopicexpense:handle).

    empty temp-table tttopicexpense.
    
    /* get Dataset */
    pRequest:getContent(input-output dataset-handle dsHandle, output cMsg).
    if cMsg > '' 
     then
      do:
        pResponse:fault('3005', "Data Parsing failed. Reason: " + cMsg).
        return.
      end.

    if not can-find(first tttopicexpense) 
     then
      do:
        pResponse:fault('3005', 'Topicexpense record not provided').
        return.
      end.
    
    for first tttopicexpense:

      if (tttopicexpense.stateID =  "" or tttopicexpense.stateID =  ?) and (tttopicexpense.topic =  "" or tttopicexpense.topic =  ? ) and (tttopicexpense.GLAccount =  "" or tttopicexpense.GLAccount =  ?)/*GL accnt */
       then 
        do:
          pResponse:fault("3005", "State, Task and GL Account cannot be blank or null").
          return.
        end.

      if can-find(first topicexpense where topicexpense.stateID = tttopicexpense.stateID and topicexpense.topic = tttopicexpense.topic)
       then
        do:
          pResponse:fault("3005", "TopicExpense with given state and task already exists").
          return.
        end.

      if not can-find(first state where state.stateID = tttopicexpense.stateID and state.active)
       then
        do:
          pResponse:fault("1000", "State").
          return.
        end.

      if not can-find(first tasktopic where tasktopic.topic = tttopicexpense.topic and tasktopic.active)
       then
        do:
          pResponse:fault("1000", "Task").
          return.
        end.

      /*if not can-find(first GL00105 where GL00105.ACTNUMST = tttopicexpense.GLAccount)
       then
        do:
          pResponse:fault("3005", "GL account must exist in GP GL00105 Table").
          return.
        end.*/
    end.    

    lSuccess = false.
    removeNullValues("tttopicexpense").
    
    TRX-BLOCK:
    do transaction
      on error undo TRX-BLOCK, leave TRX-BLOCK:
	  
	  for first tttopicexpense no-lock:
      
        create topicexpense.
        assign
            topicexpense.topic           = tttopicexpense.topic
            topicexpense.stateID         = tttopicexpense.stateID
            topicexpense.GLAccount       = tttopicexpense.GLAccount
            topicexpense.GLAccountDesc   = tttopicexpense.GLAccountDesc
            topicexpense.createddate     = now
            topicexpense.createdby       = pRequest:uid
            .
        validate topicexpense. 
        release  topicexpense.
	    
        lSuccess = true.
	    
	  end.
    
    end.
    
    if not lSuccess
     then
      do:
        pResponse:fault("3000", "TopicExpense creation").
        return.
      end. 


   pResponse:success("2002"," TopicExpense").  /* topicexpense has been created */

  end method.
      
end class.




