/*------------------------------------------------------------------------
@name  GetTaskTopics.cls
@action taskTopicsGet
@description  Get the Tasktopics Details.
@throws 3005;
@returns Success;2000; X GetTaskTopics was successful.
@author S Chandu
@Created     : 5.04.2023
@Modified :
Date        Name          Comments      
----------------------------------------------------------------------*/

class file.GetTaskTopics inherits framework.ActionBase:
    
  {lib/callsp-defs.i}
  {tt/tasktopic.i &tableAlias="ttTaskTopic" &serializeName="tasktopic"}
      
  constructor GetTaskTopics ():
    super().
  end constructor.

  destructor public GetTaskTopics ():
  end destructor. 

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
                       
    define variable pcActive as character no-undo.
    define variable iCount   as integer   no-undo.
    define variable dsHandle as handle    no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttTaskTopic:handle).

    pRequest:getParameter("Active", output pcActive).

    {lib\callsp.i &name=spGetTaskTopics &load-into=ttTaskTopic &params="input pcActive" &noCount=true &noResponse=true}
    
    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end. 

    iCount = 0.
    for each ttTaskTopic no-lock:
      iCount = iCount + 1.
    end.

    if iCount > 0
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).          
    
    pResponse:success("2000", string(iCount) + " GetTaskTopics").
    
  end method.
    
end class.
