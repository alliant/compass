/*------------------------------------------------------------------------
@name file/DeleteFileInvoice.cls
@action fileinvoiceDelete
@description  Delete the existing file invoice and its item records for fileInvoiceID.
@param fileInvoiceID;int
@throws 3001;Invalid fileInvoiceID
@throws 3066;File Invoice does not exist
@throws 3005;You cannot delete the posted file invoice.
@throws 3000;file invoice delete failed 
@returns Success;2000
@author Shefali
@version 1.0
@created 02/06/2023
@Modified :
Date        Name          Comments   
  
----------------------------------------------------------------------*/

class file.DeleteFileInvoice inherits framework.ActionBase:
    
  constructor DeleteFileInvoice ():
    super().
  end constructor.

  destructor public DeleteFileInvoice ():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    {lib/std-def.i}
    
    /* local variable */
    define variable lSuccess        as logical no-undo.
    define variable piFileInvoiceID as integer no-undo.
    define variable iAgentFileID    as integer no-undo.

    /*dataset handle */
    define variable dsHandle        as handle no-undo.
     
    /* parameter get */
    pRequest:getParameter("fileInvoiceID", output piFileInvoiceID).
	
	/* validation when no parameter is supplied */
    if (piFileInvoiceID = 0  or piFileInvoiceID = ?)
     then 
      do:
        pResponse:fault("3001", "FileInvoiceID").
        return.
      end.

    /* validate FileInvoiceID */
    if not can-find(first fileInv where fileInv.FileInvID = piFileInvoiceID)
     then
      do: 
        pResponse:fault("3066", "File Invoice").
        return.
      end.

    /* validate fileInvoiceID */
    if can-find(first fileInv where fileInv.fileInvID = piFileInvoiceID and fileInv.posted)
     then  
      do:
        pResponse:fault("3005", "You cannot delete the posted file invoice.").
        return.
      end.
    
    TRX-BLOCK:
    do transaction
      on error undo TRX-BLOCK, leave TRX-BLOCK:
	  
	  for first fileInv fields(agentFileID fileInvID) where fileInv.fileInvID = piFileInvoiceID exclusive-lock:
        iAgentFileID  = fileInv.agentfileID.
        for each fileInvItem where fileInvItem.fileInvID = fileInv.fileInvID exclusive-lock:
          delete fileInvItem.
        end.

        delete fileInv.
      end.
	  
      validate fileInv. 
      release  fileInv.
    
      validate fileInvItem. 
      release  fileInvItem.
    	
      lSuccess = true.
    end.

      /* jobtran record creation */
    run file/createjobtran-p.p (input  iAgentFileID,    /* Agent File ID */
                                input  pRequest:actionID,      /* Action */
                                input  pRequest:uID,           /* Created By */
                                input  'fileInvoiceID='+ string(piFileInvoiceID), /* Query String */
                                input  dataset-handle dsHandle). /* Payload */
    
    if not lSuccess
     then 
      do:
        presponse:fault("3000", "File Invoice delete").
        return.
      end. /* if not lSuccess */
    
    pResponse:success("2000", "File Invoice delete").
         
  end method.
      
end class.







