/*-------------------------------------------------------------------------------
@name file/searchFileProperty.cls
@action FilePropertySearch
@description Returns a dataset of the file properties based on the criteria

@param pcState;char;The StateID
@param pcCounty;char;The CountyID
@param pcAddress;char; Space separated list of search keywords

@returns The fileproperty data
@success 2005;char;The count of the records returned

@author Sachin
@version 1.0
@created 06/13/2023
Date         Name          Description
09/18/24     SRK           Modified to make address or county is mandatory
10/23/24     SRK           Modified return records with AND condition
01/02/25     Chandu S      Modifed to search string with address(parcelID,condo.name and subdivison.name).
---------------------------------------------------------------------------------*/

class file.searchFileProperty inherits framework.ActionBase:

    {tt/searchfileproperty.i &tablealias=ttSearchFileProperty}
    {tt/searchfileproperty.i &tablealias=ttCommonSearchFileProperty}

    define buffer bufCommonSearchFileProperty for ttCommonSearchFileProperty.
    define buffer bufsysindex for sysindex.
    
    constructor searchFileProperty ():
      super().
    end constructor.

    destructor public searchFileProperty ():
    end destructor.


  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    /*----------Variable Definition------------------ */
    define variable pcState         as character no-undo.
    define variable pcCounty        as character no-undo.
    define variable pcAddress       as character no-undo.

    define variable iCount         as integer   no-undo.
    define variable cObjKey        as character no-undo.
    define variable cAddress       as character no-undo. 
    define variable iObjID         as integer   no-undo.
    define variable cCheck         as character no-undo.
    define variable cPAddress      as character no-undo.
    define variable cCheck1        as character no-undo.

    define variable cWordTemp      as character no-undo.
    define variable pcAddressTemp  as character no-undo.

    define variable dsHandle        as handle    no-undo.
    
    {lib/std-def.i}

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttCommonSearchFileProperty:handle).

    pRequest:getParameter("StateID", output pcState).
    pRequest:getParameter("County", output pcCounty).
    pRequest:getParameter("Address", output pcAddressTemp).

    /* Eliminating any extra comma or space in the input string */
    do icount = 1 to num-entries(pcAddressTemp,","):
      cWordTemp = entry(icount,pcAddressTemp,",").

      cWordTemp = trim(cWordTemp,' ').
  
      pcAddress = pcAddress + (if cWordTemp = '' then '' else (' ' + cWordTemp)).
    end.
    pcAddress = trim(trim(pcAddress,' '),',').
    
    if not can-find(first state where stateID = pcState)
     then
      do:
        pResponse:fault("3066", "State " + pcState).
        return.
      end.

    if (pcCounty = '' or pcCounty = ?) and (pcAddressTemp = '' and pcAddressTemp = ?)
     then
      do:
        pResponse:fault("3005", "County or Address is required.").
        return.
      end.

    if (pcCounty ne '' and pcCounty ne ?) and not can-find(first county where stateID = pcState and countyID = pcCounty)
     then
      do:
        pResponse:fault("3005", "County " + pcCounty + " does not exist for given state.").
        return.
      end.

    if pcState ne "" and pcState ne ? and pcCounty ne "" and pcCounty ne ?
     then
      for each sysindex no-lock where (objType         = 'fileProperty'
                                     and objAttribute  = 'State,county'
                                     and objKey        = pcState + ',' + pccounty):
        if not can-find(first fileProperty where fileProperty.agentfileID = integer(sysindex.objID)) 
         then
          do:
            create ttSearchFileProperty.
            ttSearchFileProperty.agentfileID = integer(sysindex.objID). 
          end.
         else
          for each fileProperty no-lock
           where fileProperty.agentfileID = integer(sysindex.objID) and 
                 fileproperty.stateID = pcState                     and 
                 fileproperty.countyID = pcCounty:
                 
            create ttSearchFileProperty.
            assign
                ttSearchFileProperty.agentFileID       = integer(sysindex.objID)
                cAddress                            = if (fileProperty.addr1 = "?") or (fileProperty.addr1 = "") then "" else fileProperty.addr1
                cAddress                            = cAddress + if (fileProperty.addr2 = "?")   or (fileProperty.addr2 = "")   then "" else ", " + fileProperty.addr2
                cAddress                            = cAddress + if (fileProperty.addr3 = "?")   or (fileProperty.addr3 = "")   then "" else ", " + fileProperty.addr3
                cAddress                            = cAddress + if (fileProperty.addr4 = "?")   or (fileProperty.addr4 = "")   then "" else ", " + fileProperty.addr4
                cAddress                            = cAddress + if (fileProperty.city  = "?")   or (fileProperty.city = "")    then "" else ", " + fileProperty.city
                cAddress                            = cAddress + if (fileProperty.stateID = "?") or (fileProperty.stateID = "") then "" else ", " + fileProperty.stateID
                cAddress                            = cAddress + if (fileProperty.zip   = "?")   or (fileProperty.zip = "")     then "" else ", " + fileProperty.zip
                . 

                for first county fields (description) no-lock 
                 where county.countyID = fileProperty.countyID and 
                  county.stateID  = fileProperty.stateID:

                  assign cAddress =  cAddress + if (county.description = "?") or (county.description = "") then "" else ", " +  county.description.
                end.


                assign 
                    cAddress                               =  trim(cAddress,',')
                    ttSearchFileProperty.propertyAddr      =  trim(cAddress)
                    ttSearchFileProperty.parcelID          = fileproperty.parcelID
                    ttSearchFileProperty.book              = fileproperty.book
                    ttSearchFileProperty.seq               = fileproperty.seq
                    ttSearchFileProperty.section           = fileproperty.section
                    ttSearchFileProperty.township          = fileproperty.township
                    ttSearchFileProperty.townshipDirection = fileproperty.townshipDirection
                    ttSearchFileProperty.range             = fileproperty.range
                    ttSearchFileProperty.rangeDirection    = fileproperty.rangeDirection
                    ttSearchFileProperty.lot               = fileproperty.lot
                    ttSearchFileProperty.block             = fileproperty.block
                    ttSearchFileProperty.levels            = fileproperty.levels
                    ttSearchFileProperty.pageNumber        = fileproperty.pageNumber
                    ttSearchFileProperty.condominum        = fileproperty.condominum
                    ttSearchFileProperty.subdivision       = fileproperty.subdivision
                    .
          end. /* for each fileProperty */
      end.

    if pcAddress ne "" and pcAddress ne ? and pcState ne "" and pcState ne ?
     then
      do icount = 1 to num-entries(pcAddress," "): 
        cObjKey = entry(icount,pcAddress," ").
      
        for each sysindex no-lock
          where sysindex.objType      = 'fileProperty'
            and sysindex.objAttribute = 'Address'
            and sysindex.objKey       =  cObjKey :
          iObjID = 0.  
          if can-find(first bufsysindex where bufsysindex.objType = 'fileProperty' and bufsysindex.objAttribute = 'State' and bufsysindex.objKey =  pcState and bufsysindex.objID = sysindex.objID)
           then
            iObjID = integer(sysindex.objID). /* contains agentFileID */
           else
            next.
          
          
          std-de = 100 / num-entries(pcAddress," ").

          for each fileproperty no-lock
            where fileProperty.agentfileID = iObjID   and 
                       fileproperty.stateID = pcState :
            cCheck = ''.
            cAddress = ''.
            assign    
               cAddress                               = if (fileProperty.addr1 = "?") or (fileProperty.addr1 = "") then "" else fileProperty.addr1
               cAddress                               = cAddress + if (fileProperty.addr2 = "?")   or (fileProperty.addr2 = "")   then "" else ", " + fileProperty.addr2
               cAddress                               = cAddress + if (fileProperty.addr3 = "?")   or (fileProperty.addr3 = "")   then "" else ", " + fileProperty.addr3
               cAddress                               = cAddress + if (fileProperty.addr4 = "?")   or (fileProperty.addr4 = "")   then "" else ", " + fileProperty.addr4
               cAddress                               = cAddress + if (fileProperty.city  = "?")   or (fileProperty.city = "")    then "" else ", " + fileProperty.city
               cAddress                               = cAddress + if (fileProperty.stateID = "?") or (fileProperty.stateID = "") then "" else ", " + fileProperty.stateID
               cAddress                               = cAddress + if (fileProperty.zip   = "?")   or (fileProperty.zip = "")     then "" else ", " + fileProperty.zip
                      . 

            for first county fields (description) no-lock 
              where county.countyID = fileProperty.countyID and 
                    county.stateID  = fileProperty.stateID:

              assign cAddress =  cAddress + if (county.description = "?") or (county.description = "") then "" else ", " +  county.description.

              assign
                  cPAddress =  if (fileProperty.Condominum = "?")  or (fileProperty.Condominum = "")  then "" else fileProperty.Condominum
                  cPAddress =  cPAddress + if (fileProperty.subdivision = "?") or (fileProperty.subdivision = "") then "" else ", " +  fileProperty.subdivision
                  cPAddress =  cPAddress + if (fileProperty.parcelID = "?")    or (fileProperty.parcelID = "")    then "" else ", " +  fileProperty.parcelID
                  .
            end.

            cCheck = trim(cAddress,",").
            cCheck = replace(ccheck," ",",").

            cCheck1  = trim(cPAddress,",").
            cCheck1  = replace(ccheck1," ",",").

            if lookup(cObjKey,cCheck) > 0 or lookup(Cobjkey,cCheck1) > 0  
             then
              do:
                if can-find(first ttCommonSearchFileProperty where ttCommonSearchFileProperty.agentFileID = iObjID and ttCommonSearchFileProperty.seq = fileproperty.seq and ttCommonSearchFileProperty.percentMatch > 0) 
                 then
                  for first ttCommonSearchFileProperty 
                    where ttCommonSearchFileProperty.agentFileID = iObjID and 
                          ttCommonSearchFileProperty.percentMatch > 0 and
                          ttCommonSearchFileProperty.seq = fileproperty.seq:                             
                    ttCommonSearchFileProperty.percentMatch = ttCommonSearchFileProperty.percentMatch + std-de.
                  end.
                else
                 do:
                   create ttCommonSearchFileProperty.
                   assign
                      ttCommonSearchFileProperty.agentFileID = iObjID
                      cAddress                                     =  trim(cAddress,',')
                      ttCommonSearchFileProperty.propertyAddr      =  trim(cAddress)
                      ttCommonSearchFileProperty.parcelID          = fileproperty.parcelID
                      ttCommonSearchFileProperty.seq               = fileproperty.seq
                      ttCommonSearchFileProperty.book              = fileproperty.book
                      ttCommonSearchFileProperty.section           = fileproperty.section
                      ttCommonSearchFileProperty.township          = fileproperty.township
                      ttCommonSearchFileProperty.townshipDirection = fileproperty.townshipDirection
                      ttCommonSearchFileProperty.range             = fileproperty.range
                      ttCommonSearchFileProperty.rangeDirection    = fileproperty.rangeDirection
                      ttCommonSearchFileProperty.lot               = fileproperty.lot
                      ttCommonSearchFileProperty.block             = fileproperty.block
                      ttCommonSearchFileProperty.levels            = fileproperty.levels
                      ttCommonSearchFileProperty.pageNumber        = fileproperty.pageNumber
                      ttCommonSearchFileProperty.condominum        = fileproperty.condominum
                      ttCommonSearchFileProperty.subdivision       = fileproperty.subdivision
                      ttCommonSearchFileProperty.percentMatch      = std-de
                      .      
                 end.
              end.

          end.
        end.
      end.
      
    /* We have to keep all fileproperty records also for agentfiles whose percent match is almost 100 */
    if pcAddress ne "" and pcAddress ne ?
     then
      for each ttCommonSearchFileProperty:
        if ttCommonSearchFileProperty.percentMatch < 99.50 
         then
          delete ttCommonSearchFileProperty.
      end.
     else
      for each ttSearchFileProperty:
        create ttCommonSearchFileProperty.
        buffer-copy ttSearchFileProperty to ttCommonSearchFileProperty.
      end. 

     if pcAddress ne "" and pcAddress ne ? and pcCounty ne "" and pcCounty ne ? 
      then
       for each ttCommonSearchFileProperty :
        if not can-find ( first ttSearchFileProperty  where ttSearchFileProperty.agentFileID = ttCommonSearchFileProperty.agentFileID and ttSearchFileProperty.seq = ttCommonSearchFileProperty.seq)
         then
          delete ttCommonSearchFileProperty.
       end.
      
    for each ttCommonSearchFileProperty:

      for first agentfile fields (agentID agentfileID fileNumber buyerDescription sellerDescription) no-lock where agentfile.agentfileid = integer(ttCommonSearchFileProperty.agentfileid):
        assign
            ttCommonSearchFileProperty.agentID           = agentfile.agentid
            ttCommonSearchFileProperty.fileNumber        = agentfile.fileNumber
            ttCommonSearchFileProperty.ipercentMatch     = round(ttCommonSearchFileProperty.percentMatch, 0)
            ttCommonSearchFileProperty.buyerDescription  = agentfile.buyerDescription
            ttCommonSearchFileProperty.sellerDescription = agentfile.sellerDescription
            .

        /* Agent (name + ID) */
        for first agent fields (agentID name) no-lock where agent.agentID = ttCommonSearchFileProperty.agentID:
          ttCommonSearchFileProperty.agentName = agent.name + " (" + agent.agentID + ")".
        end.

      end.

      std-in = std-in + 1.
        
    end.

    if std-in > 0 
     then
      setContentDataset(input dataset-handle dsHandle, pRequest:FieldLIst).
        
    pResponse:success("2005", string(std-in) {&msg-add} "fileProperty").

  end method.
end class.
        

      



