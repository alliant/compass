/*------------------------------------------------------------------------
@name RegenerateVendorRemittance.cls
@action VendorRemittanceRegenerate 

@returns company,apinv
@author Sagar R Koralli
@version 1.0
@created 07/29/2030
Modification:
Date          name      Description
----------------------------------------------------------------------*/

USING Progress.Json.ObjectModel.ObjectModelParser.
USING progress.Json.ObjectModel.*.

class file.RegenerateVendorRemittance inherits framework.ActionBase:
      
  {lib/callsp-defs.i}

  define temp-table taccountspayableinvoice no-undo serialize-name "vendorremittance"
    field vendorid               as character serialize-hidden
    .

  define temp-table tcontact no-undo serialize-name "contact"
    field vendorid        as character serialize-hidden
    field name            as character
    field phone           as character
    field email           as character
    field address         as character
    .

  define temp-table tInvoice no-undo serialize-name "vendor" 
    field reportdate          as date
    field activitythrough     as date
    field vendorid            as character
    field name                as character
    field address             as character 
    field city                as character
    field state               as character
    field zip                 as character
    field totalamount         as decimal
    .

  define temp-table tInvoiceitem no-undo serialize-name "vendoritems"
    field vendorid               as character serialize-hidden
    field amount                 as decimal
    field apinvid                as integer
    field count                  as integer
    field task                   as character
    .

  {framework/sysmailparameter.i &tableAlias=mailparameter}
  {tt/apvendor.i &tableAlias="gpvendordetails" &posttasks}

    /* Global Variables*/
  define variable pResponse         as framework.IRespondable no-undo.
  define variable pRequest          as framework.IRequestable no-undo.

  define variable pcRefId           as character              no-undo.
  define variable pcDocType         as character              no-undo.
  define variable cDataFileName     as character              no-undo.
  define variable cRenderedPDF      as character              no-undo.
  define variable cRenderedCSV      as character              no-undo.
  define variable cVendorID         as character              no-undo.
  define variable lcsvdoc           as logical                no-undo.
  define variable lpdfdoc           as logical                no-undo.
  define variable lsuccess          as logical                no-undo initial true.
  
  define stream str1.
    
  constructor RegenerateVendorRemittance ():
    super().
  end constructor.

  destructor public RegenerateVendorRemittance ():
  end destructor.   

  method public override void act (input pActRequest  as framework.IRequestable,
                                   input pActResponse as framework.IRespondable):

    define variable iCount             as integer   no-undo.
    define variable iFileTaskCount     as integer   no-undo.
    define variable cAPContactPhone    as character no-undo.
    define variable cAPContactEmail    as character no-undo.
    define variable cAPContactAddress  as character no-undo.
    define variable cAPContactName     as character no-undo.
    define variable iapInvID           as integer   no-undo.
    define variable dTotalAmount       as decimal   no-undo.
    define variable dEndDate           as date      no-undo.


    /*dataset handle */
    define variable dsHandle        as handle    no-undo.
    /* Variable */
    define variable cTempPath       as character no-undo.
    
    {lib/std-def.i}

    assign
      pResponse = pActResponse
      pRequest  = pActRequest
      .

   /* dataset creation */
    create dataset dsHandle.
    dsHandle:serialize-hidden = true.
    dsHandle:set-buffers(buffer taccountspayableinvoice:handle,
                         buffer tcontact:handle,
                         buffer tInvoice:handle,
                         buffer tInvoiceitem:handle).
    dsHandle:add-relation(dsHandle:get-buffer-handle(1), dsHandle:get-buffer-handle(2), "vendorID,vendorID",?,yes,?,?).
    dsHandle:add-relation(dsHandle:get-buffer-handle(1), dsHandle:get-buffer-handle(3), "vendorID,vendorID",?,yes,?,?).
    dsHandle:add-relation(dsHandle:get-buffer-handle(3), dsHandle:get-buffer-handle(4), "vendorID,vendorID",?,yes,?,?).

    empty temp-table taccountspayableinvoice.
    empty temp-table tcontact.
    empty temp-table tInvoice.
    empty temp-table tInvoiceItem.
	
    publish "GetSystemParameter" from (session:first-procedure) ("TempFolder", output cTempPath).
    publish "GetSystemParameter" from (session:first-procedure) ("ARContactPhone",   output cAPContactPhone). 
    publish "GetSystemParameter" from (session:first-procedure) ("ARContactEmail",   output cAPContactEmail). 
    publish "GetSystemParameter" from (session:first-procedure) ("ARContactAddress", output cAPContactAddress). 
    publish "GetSystemParameter" from (session:first-procedure) ("ARContactName",    output cAPContactName). 


    pRequest:getParameter("RefId",  output pcRefId).
    pRequest:getParameter("EndDate",  output dEndDate).
    pRequest:getParameter("DocType",output pcDocType).

    if pcDocType = '' or pcDocType = ?
     then
      pcDocType = "pdf".
    
    /* validation when no parameter is supplied */
    if (pcRefId = ''  or pcRefId = ?)
     then 
      do:
        pResponse:fault("3001", "RefId").
        return.
      end.

    if (can-find(first sysdoc where sysdoc.entityid = pcRefId and sysdoc.entityType = 'APVoucher' and sysdoc.objType = 'pdf')) and
       (can-find(first sysdoc where sysdoc.entityid = pcRefId and sysdoc.entityType = 'APVoucher' and sysdoc.objType = 'csv'))
     then
      do:
        pResponse:fault("3005", "Voucher documents already exist. ").
        return.
      end.
     
      /* validate doctype */
    for first sysaction fields(action templateID ) where sysaction.action = pRequest:actionid no-lock:
     
       for first template fields(templateID outputFormat ) where template.TemplateId = sysaction.templateID no-lock:
         if pcDocType ne outputFormat
          then
           do:
             pResponse:fault("3005", "Document type is not supported").
             return.
           end.          
       end.
   
    end.

    /* validate apInvoiceID */
    if not can-find(first apinv where apinv.refType = 'T' and apinv.refID = pcRefID)
     then 
      do:
        pResponse:fault("3066", "AP Invoice").
        return.
      end.
     iCount = 1.
     for each apinv fields(apInvID vendorid refType refID) where apinv.refType = 'T' and apinv.refID = pcRefID no-lock:
       if iCount = 1
        then
         cVendorID = apinv.vendorid.
       else
        do:       
          if apinv.vendorID ne cVendorID
           then
            do:
              pResponse:fault("3005", "Vendor IDs of all the respective apinv is not identical for refID " + pcRefID).
              return.           
            end.
       end.
       iCount = iCount + 1.
     end.

    for each apinv fields (apinvId refType refID) where refType = 'T' and refID = pcRefID no-lock:

      for each filetask fields(apInvID agentfileID ) where filetask.apinvID = apinv.apInvID no-lock:

       if can-find(first agentFile where agentFile.agentFileID = filetask.AgentFileID and (agentFile.fileID = ? or agentFile.fileID = ""))
         then 
          do:
            pResponse:fault("3005", "Agent File does not exists for AgentFileID: " + string(filetask.agentfileID)). 
            return.
          end.
      end.
    end.

    {lib\callsp.i &name=spGetVendorDetails &load-into=gpvendordetails &noResponse=true}
    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end.

    create taccountspayableinvoice.
    assign
        taccountspayableinvoice.vendorid = cVendorID.
     
    create tcontact.
    assign
        tcontact.vendorid  = cVendorID
        tcontact.name      = cAPContactName
        tcontact.phone     = cAPContactPhone
        tcontact.email     = cAPContactEmail
        tcontact.address   = cAPContactAddress
        iFileTaskCount     = 0
        iCount             = 1
        .
   
      for each apinv where apinv.refType = 'T' and apinv.refID = pcRefID no-lock:
        if icount = 1 
         then
          do:            
            create tinvoice.
            assign
                tinvoice.reportdate      = apinv.invoicedate
                tinvoice.activitythrough = dEndDate
                dTotalAmount             = 0
                .

            for first gpvendordetails no-lock where gpvendordetails.vendorid = apinv.vendorid:
              assign
                tinvoice.vendorid = gpvendordetails.vendorid
                tinvoice.name     = gpvendordetails.vendorname
                tinvoice.address  = gpvendordetails.vendoraddress
                tinvoice.city     = gpvendordetails.city
                tinvoice.state    = gpvendordetails.state
                tinvoice.zip      = gpvendordetails.zip
                .
            end.            
          end.

        create tInvoiceItem.
        assign
            tInvoiceItem.vendorid = apinv.vendorid
            tInvoiceItem.apinvID  = apinv.apinvID
            tInvoiceItem.amount   = apinv.amount
            dTotalAmount          = dTotalAmount + tInvoiceItem.amount.
      
        for each filetask fields(apinvID topic) where filetask.apinvID = apinv.apinvID no-lock:
          if iFileTaskCount = 0
           then
            assign
               tInvoiceItem.task = filetask.topic
               iFileTaskCount    = iFileTaskCount + 1.           
          else
           iFileTaskCount = iFileTaskCount + 1.
        end.

        assign 
            tInvoiceItem.count   = iFileTaskCount 
            tInvoice.totalamount = dTotalAmount
            iFileTaskCount       = 0.

       iCount = iCount + 1.
     end. /* for each apinv */

    cDataFileName = cTempPath + 'APInvoicePDFDocument-' + string(trim(cVendorID)) + '.json' .
    std-lo = dsHandle:write-json('file', cTempPath + 'APInvoicePDFDocument-' + string(trim(cVendorID)) + '.json',yes) no-error.

    if not std-lo or error-status:error
     then
      do:
        std-ch = "".
        do std-in = 1 to error-status:num-messages:
          std-ch = if std-ch = "" then error-status:get-message(std-in) else std-ch + "~n" + error-status:get-message(std-in).
        end.
    
        pResponse:fault("3005", "APInvoicePdfRender failed. " + std-ch).
        return.
      end.
   

  end method.
  
method public override char render(input pFormat as char, input pTemplate as char):
    define variable cUrl          as character no-undo.
    define variable pMsg          as character no-undo.
    define variable pResponseFile as character no-undo.
    define variable pSuccess      as logical   no-undo.
    define variable cFileName     as character no-undo.

    /*if pResponse:isFault()
      then
       return ''.*/

   /* Next line is just a placeholder for new .ini entry*/

    publish "GetSystemParameter" from (session:first-procedure) ("RenderDocumentAPI", output cUrl).
    
    for first sysaction where sysaction.action = pRequest:actionid no-lock:
    
      for first template where template.TemplateId = sysaction.templateID no-lock:
        cUrl = cUrl + '/' + template.schemaname + '/' + template.filename + '/' + pcDocType.

        run util/httppost.p("",
                          cUrl,
                          "",
                          "application/json",
                          cDataFileName,
                          output pSuccess,
                          output pMsg,
                          output pResponseFile).
      
        cFileName = string(trim(cVendorID)) + "-" + string(pcRefID) + "-Vendor_Remittance-" + String(Year(today), "9999") + "-" + String(MONTH(today), "99") + "-" + String(DAY(today), "99").
      end.
    end.
  
    if not pSuccess 
     then
      do:
        if pMsg = '' 
          then PMsg = ParseErrorResponse(pResponseFile).
        pResponse:fault("3005", "Vendor Remittance reports generation failed." + pMsg).

        return ''.
      end.
    os-rename value(pResponseFile) value(cFileName + "." + pcDocType).
     
    cRenderedPDF = cFileName + "." + pcDocType.
    
    GenerateCSV().

    if can-find(first sysdoc where sysdoc.entityid = pcRefId and sysdoc.entityType = 'APVoucher' and sysdoc.objType = 'pdf')
     then
      lpdfdoc = true.

    if can-find(first sysdoc where sysdoc.entityid = pcRefId and sysdoc.entityType = 'APVoucher' and sysdoc.objType = 'csv')
     then
      lcsvdoc = true.

    storeContent(input pRequest:uid, output pMsg).

    if lsuccess 
     then
      pResponse:success("3005", "Vendor Remittance reports generated successfully.").
     else
       pResponse:fault("3005", "Vendor Remittance reports generation failed.").

    return "".
  end.

  method private character ParseErrorResponse(input pResponseFile as character):
    define variable oParser            as ObjectModelParser no-undo.
    define variable oJsonObjectParent  as JsonObject        no-undo.
    define variable oJsonObjectChild   as JsonObject        no-undo.
    define variable cFaultJsonText     as character         no-undo.
    define variable cErrMsg            as character         no-undo.

    if pResponseFile = '' or pResponseFile = ?
     then
       return 'Render Service Failed'.

    /* Parse the JSON file into a JSON object */
    oParser = NEW ObjectModelParser().
    oJsonObjectParent = CAST(oParser:ParseFile(pResponseFile), JsonObject).  
    
    /* Parse JSON text into a JSON object*/
    cFaultJsonText = oJsonObjectParent:GetJsonText("fault") no-error. 
    oJsonObjectChild = CAST(oParser:Parse(cFaultJsonText), JsonObject).  
    
    cErrMsg = oJsonObjectChild:getcharacter("message").

    delete object oParser.
    delete object oJsonObjectParent.
    delete object oJsonObjectChild.

    return if cErrMsg <> ? then cErrMsg else ''.
  end.

  method public override logical storeContent(input pUID as character,
                                              output pMsg as character):

    define variable cErrorPDF as character no-undo.
    define variable cErrorCSV as character no-undo.

    file-info:filename = cRenderedPDF.
    if file-info:full-pathname = ?
     then
      do:
        cErrorPDF = "Voucher PDF not found for refID:" + pcRefID.
        run util/sysmail.p("Saving Voucher file to repository failed",
                           cErrorPDF).
      end.
    else
     do:
       if not lpdfdoc
        then
         StoreVouchers(pUID, cRenderedPDF, output cErrorPDF).
     end.
      
    file-info:filename = cRenderedCSV.
    if file-info:full-pathname = ?
     then
      do:
        cErrorCSV = "Voucher CSV not found for refID:" + pcRefID.
        run util/sysmail.p("Saving Voucher file to repository failed",
                           cErrorCSV).
      end.
    else
     do:
       if not lcsvdoc
        then
         StoreVouchers(pUID, cRenderedCSV, output cErrorCSV).
     end.
         

    pMsg = cErrorPDF + "," + cErrorCSV.

    if trim(pMsg,",") > "" 
     then return false.
    else return true.

  end method.


  method private void StoreVouchers (input pUID            as character,
                                     input cVoucherFile    as character,
                                     output pMsg           as character):


    define variable cStorageEntity   as character no-undo.
    define variable cStorageID       as character no-undo.
    define variable cStorageCategory as character no-undo.
    define variable lVoucherStored   as logical   no-undo.
    define variable iseq             as integer   no-undo.
    
    assign
      cStorageEntity = "APVoucher"
      cStorageID     = pcRefID
      .

    run util\UploadFile.p (pUID, true, cStorageEntity, cStorageID, cStorageCategory, cVoucherFile, output lVoucherStored, output pMsg).

    if not lVoucherStored 
     then
      do:
        lsuccess = false.
        pMsg = "Saving file " + cVoucherFile + " to repository failed with error " + pMsg + " for refID:" + pcRefID.
           run util/sysmail.p ("Saving file to repository failed",
                               "Action:" + pRequest:actionID + "<br>" + pMsg).
         end. 

    if can-find (first sysdoc where sysdoc.entityid = pcRefID and sysdoc.entityType = cStorageEntity)
     then
      iseq = 2.
     else
      iseq  = 1.

    create sysdoc.
    assign
      sysdoc.entityType = cStorageEntity
      sysdoc.entityID   = pcRefID
      sysdoc.objType    = substring(cVoucherFile, r-index(cVoucherFile, "." ) + 1)
      sysdoc.docDate    = now
      sysdoc.uid        = pUID
      sysdoc.entitySeq  = iseq
      sysdoc.objID      = trim(cVendorID) + '-' + pcRefID + '-' + 'Vendor_Remittance-' + String(Year(today), "9999") + "-" + String(MONTH(today), "99") + "-" + String(DAY(today), "99")
      sysdoc.objAttr    = '' .

    validate sysdoc.
    release  sysdoc.

  end method.

  method private logical GenerateCSV ():

    define variable cAgentID       as character no-undo.
    define variable cFileNumber    as character no-undo.
    define variable cDateFormat    as character no-undo.
    define variable dAssignDate    as date      no-undo.
    define variable dCompletedDate as date      no-undo.
    define variable cusername      as character no-undo.
    define variable today-dt-tz    as datetime-tz no-undo initial now.
    define variable utcMinutes     as integer   no-undo.

    /* to pick sever time zone */
    utcMinutes = timezone(today-dt-tz).

    GetFileName().

    cDateFormat = session:date-format.
    session:date-format = "ymd".

    output stream str1 to value(cRenderedCSV).

    /* Creating report header*/
    export stream str1 delimiter "," "Task" "Agent ID" "Agent Name" "File Number" "Assigned" "Assigned To" "Completed" "Price".

    for each apinv fields (apinvId refType refID) where refType = 'T' and refID = pcRefID no-lock:

        for each filetask fields(topic cost apInvID assignedDate agentfileID assignedTo completeddate) where filetask.apinvID = apinv.apInvID no-lock:

          for first agentfile fields (agentID filenumber) where agentfile.agentFileID = filetask.agentfileID no-lock:

            for first agent fields (name agentID) where agent.agentID = agentfile.agentID no-lock:

              assign
                cAgentID       = '="' + agent.agentID + '"'
                cFileNumber    = '="' + agentfile.filenumber + '"'
                dAssignDate    = date(datetime-tz(filetask.AssignedDate,utcMinutes))
                dCompletedDate = add-interval(filetask.completeddate, utcMinutes ,'Minutes')
                .

              {lib/getusername.i &var=cusername &user=filetask.AssignedTo}
              session:date-format = "mdy".
              export stream str1 delimiter "," filetask.topic cAgentID agent.name cFileNumber dAssignDate cusername dCompletedDate filetask.cost.
            end.
          end.
        end.
    end.

    output stream str1 close.
    session:date-format = cDateFormat.
    publish "AddTempFile" from (session:first-procedure) ("FileTaskSummary", cRenderedCSV).  

  end method.

  method private void GetFileName ():

    define variable cTempFolder as character no-undo.
    publish "GetSystemParameter" from (session:first-procedure) ("TempFolder", output cTempFolder).

    if cTempFolder = ? or cTempFolder = ""
     then
      cTempFolder = os-getenv("TEMP").
  
    if cTempFolder = ? or cTempFolder = ""
     then
      cTempFolder = os-getenv("TMP").
  
    if cTempFolder = ? or cTempFolder = ""
     then
      cTempFolder = "d:\temp\".

    cRenderedCSV = cTempFolder + "\" + trim(cVendorID) + "-" + string(pcRefID) + "-Vendor_Remit_Detail-" + String(Year(today), "9999") + "-" + String(MONTH(today), "99") + "-" + String(DAY(today), "99") + ".csv".

  end method.
      
end class.
