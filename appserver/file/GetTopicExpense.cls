/*------------------------------------------------------------------------
@name  file.GetTopicExpense.cls
@action topicExpenseGet
@description  Get the topic expense.
@throws 3005;
@returns Success;2005; X GetTaskTopics was successful.
@author Sagar K
@Created     : 07/31/2023
@Modified :
Date        Name          Comments      
----------------------------------------------------------------------*/

class file.GetTopicExpense inherits framework.ActionBase:
    
  {lib/callsp-defs.i}
  {tt/topicexpense.i &tableAlias="tttopicexpense"}
      
  constructor GetTopicExpense ():
    super().
  end constructor.

  destructor public GetTopicExpense ():
  end destructor. 

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
                       
    define variable iCount   as integer   no-undo.
    define variable dsHandle as handle    no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer tttopicexpense:handle).

    {lib\callsp.i &name=spGetTopicExpense &load-into=tttopicexpense   &noResponse=true}
    
    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end. 

    iCount = 0.
    for each tttopicexpense no-lock:
      iCount = iCount + 1.
    end.

    if iCount > 0
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).          
    
    pResponse:success("2005", string(iCount) + " TopicExpense record").
    
  end method.
    
end class.
