/*------------------------------------------------------------------------
@name  getfulfilledordersdocs-p.p
@description  Get the documents for fulfilled Orders.
@param ipcAgentFileID;int
@param ipcCategory;char
@returns Success
@author Shefali
@Modified :
Date        Name          Comments   

----------------------------------------------------------------------*/

define input parameter ipcAgentFileID      as integer     no-undo.
define input parameter ipcCategory         as character   no-undo.

define output parameter dataset-handle dsHandle.
define output parameter oplError    as logical   no-undo.
define output parameter opcErrorMsg as character no-undo.
define output parameter opiCount    as integer   no-undo.

{tt/fulfilledOrderDocument.i}

create dataset dsHandle.
dsHandle:serialize-name = 'data'.
dsHandle:set-buffers(buffer fulfilledOrderDocument:handle). 

/* Local variables */
define variable cErrorMessage   as character no-undo.
define variable cAddress        as character no-undo.
define variable cStatusList     as character no-undo.

/* Used for getting the ARC Open task */
define variable lSuccess        as logical   no-undo.
define variable cMsg            as character no-undo.

define variable tDataFile       as character no-undo.
define variable iCount          as integer   no-undo.
define variable iStatusCount    as integer   no-undo. 

{lib/arc-repository-procedures.i &exclude-startElement=true}

/* Functions and Procedures */
{lib/run-http.i}
{lib/arc-bearer-token.i}

    /* validation when no parameter is supplied */
if (ipcAgentFileID = 0  or ipcAgentFileID = ?)
 then 
  do:
    assign lSuccess    = no
           opcErrorMsg = "AgentfileID is invalid".
    return.
  end.

/* validate agentfileID */
if not can-find(first agentfile where agentfile.agentfileID = ipcAgentFileID ) 
 then 
  do:
    assign lSuccess    = no
           opcErrorMsg = "AgentFileID does not exist".
    return.
  end.

/* create the data file */
tDataFile = guid(generate-uuid).

output to value(tDataFile).
put unformatted "~{" skip.
put unformatted "  ~"Entity~": ~"Production~"," skip.
put unformatted "  ~"EntityId~": ~"" + string(ipcAgentFileID) + "~"," skip.
put unformatted "  ~"Category~": ~"" + ipcCategory + "~"," skip.
put unformatted "  ~"RefType~": ~"Job~"," skip.
put unformatted "  ~"RefId~": ~"~"," skip.
put unformatted "  ~"RefGroup~": ~"fulfillment~"," skip.
put unformatted "  ~"FileId~": ~"~" " skip.
put unformatted "~}" skip.
output close.

publish "AddTempFile" ("ARCActivity", tDataFile).

publish "GetSystemParameter" ("ARCGetFulfilledOrdersDocs", output tURL).

run HTTPPost ("getAgentFile", tURL, CreateArcHeader(""),"application/json", search(tDataFile), output lSuccess, output cMsg, output tResponseFile).

if not lSuccess  
 then
  do:
    assign oplError = true
           opcErrorMsg = if cMsg  = '' then "Failed to connect to the ARC" else cMsg.
    return.
  end.

lSuccess = temp-table fulfilledOrderDocument:read-json("FILE",tResponseFile) no-error.

/*Validating parsing of response*/
if not lSuccess or error-status:error
 then 
  do: 
    oplError = true.
    do iCount = 1 to error-status:num-messages:
      opcErrorMsg = opcErrorMsg + error-status:get-message(iCount) + ' '. 
    end.
    return.
  end.





