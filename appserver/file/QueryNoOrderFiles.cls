/*------------------------------------------------------------------------
@file QueryNoOrderFiles.cls
@action noOrderFilesQuery Get files without any order
@returns #Files without any order
@success 2005;X File(s) without an were returned
@author Sachin Chaturvedi
@version 1.0
@created 12/29/2024
Modification:

----------------------------------------------------------------------*/

class file.QueryNoOrderFiles inherits framework.ActionBase:
    
  {tt/queryfiles.i} 
  {lib/callsp-defs.i}

  constructor QueryNoOrderFiles ():
    super().
  end constructor.

  destructor public QueryNoOrderFiles ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
	define variable pcStateID          as character no-undo.
	define variable pdMinCreateDate    as date      no-undo.
    define variable lError             as logical   no-undo.
    define variable cErrMessage        as character no-undo.
    define variable iOpenTaskCnt       as integer   no-undo.
    define variable cLastCompletedTask as character no-undo.
    define variable cCompletedBy       as character no-undo.    
    define variable dtCompletedDate    as datetime  no-undo.
    {lib/std-def.i}
	
    pRequest:getParameter("StateID",         output pcStateID).
    pRequest:getParameter("MinCreateDate",   output pdMinCreateDate).

    if pcStateID = ?
     then
      pcStateID = 'ALL'.

    define variable dsFileWithoutOrder as handle no-undo.

    create dataset dsFileWithoutOrder.
    dsFileWithoutOrder:serialize-name = 'data'.
    dsFileWithoutOrder:set-buffers(buffer queryfiles:handle).

    {lib\callsp.i &name=spGetFilesWithoutOrder &load-into=queryfiles &params="input pcStateID, input pdMinCreateDate" &noCount=true &noResponse=true}
    
    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end.
    
    std-in = 0.

    for each queryfiles no-lock:
      std-in = std-in + 1. 
    end.

    if std-in > 0 then
      setcontentdataset(input dataset-handle dsFileWithoutOrder,input pRequest:FieldList).

    pResponse:success("2005", string(std-in) {&msg-add} " Files with no order").
             
  end method.
      
end class.


