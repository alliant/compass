/*------------------------------------------------------------------------
@name  NewFile.cls
@action fileNew
@description  creates new agentfile 
@param agentID ,SoftwareID
@returns Fault; 
@returns Success;.
@author S Chandu 
@Modified :
Date        Name          Comments   
02/02/2024   K.R           Modified to send agentfileid of the created agentfile as response     
02/20/24     K.R           Modified to handle invalid json and call new method to get the payload
03/12/24     S.R           Modified to call programs on specific software versions.
05/10/24     S.R           Modified to call createmailbox-p.p instead of asycn framework.
07/01/24     K.R           Modified to update agentfileID in arc side 
08/07/24     S.R           Modified to add calculated duedate on creation of task.
09/19/24     SRK           Modified to add input in completetask-p.p
11/19/24     SRK           Modified to update agentfile.softwareid
12/16/2024   K.R           Modified to create sys index
12/27/2024   K.R           Modified to fix the creation of tasks
----------------------------------------------------------------------*/
USING Progress.Json.ObjectModel.ObjectModelParser.
USING progress.Json.ObjectModel.*.

class file.NewFile inherits framework.ActionBase:

 {tt/proerr.i   &tableAlias="ttFileErr"}
 
 {lib/normalizefileid.i &method="true"}
  constructor NewFile ():
    super().
  end constructor.

  destructor public NewFile ( ):
  end destructor.   

   /* Global Variables*/
  define variable iErrorSeq     as integer   no-undo.
  define variable piAgentFileID as integer   no-undo.
  define variable cFileName     as character no-undo. 

  define stream str1.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
    {lib/std-def.i}
   
    /* local varibales */           
  
    define variable cDateFormat      as character   no-undo.
    define variable pcAgentID        as character   no-undo.
    define variable pcVersion        as character   no-undo.
    define variable cProgramName     as character   no-undo.
    define variable cType            as character   no-undo.
    define variable lSuccess         as logical     no-undo.
    define variable cErrorMsg        as character   no-undo.
    define variable pMsg             as character   no-undo.
    define variable cMsg             as character   no-undo.
    define variable cEmailAddr       as character   no-undo.
    define variable lCreateFileTask  as logical     no-undo.
    define variable iErrCode         as integer     no-undo.
    define variable cErrMessage      as character   no-undo.     
    define variable cTempPath        as character   no-undo.
    define variable cFileNumber      as character   no-undo.
    define variable cTaskID          as character   no-undo.
    define variable lMailboxCreated  as logical     no-undo.
    define variable cMailSubject     as character   no-undo.
    define variable isoftwareid      as integer     no-undo.
    define variable cIndexValueList  as character   no-undo.
    define variable cAddressList     as character   no-undo.
    define variable cstatelist       as character   no-undo.
    define variable cStateCountyList as character   no-undo.


    /* JSON Objects */
    define variable oJsonObjectParent      as JsonObject no-undo.
    define variable oJsonObjectParentClone as JsonObject no-undo.
    /* handle */
    define variable dsHandle  as handle no-undo.
    define variable xdocument as handle no-undo.

    pRequest:getParameter("AgentID", pcAgentID).
    if pcAgentID = ""
     then 
      do:
        pResponse:fault("3051", "Agent" {&msg-add} "").
        return.
      end.

    if not can-find(first agent where agent.agentID = pcAgentID)
     then
      do:
         pResponse:fault("3066", "Agent " + pcAgentID).
         return.
      end.

    pRequest:getParameter("Version", pcVersion).
    if pcVersion = ""
     then
      do:
        pResponse:fault("3001", "Version").
        return.
      end.

    pRequest:getParameter("TaskID", cTaskID).

   for first titlesoftware where EDICode = pcVersion no-lock :
      assign
           cProgramName = titlesoftware.progexec
           cType        = titlesoftware.EDIPayloadFormat
           isoftwareid  = titlesoftware.softwareid
           .
   end.
 
   if search(cProgramName + ".p") = ? and search(cProgramName + ".r") = ? 
    then
     do:   
        cErrorMsg = "File not found: " + cProgramName.
        publish "GetSystemParameter" from (session:first-procedure) ("ediErrorNotificationEmail", output cEmailAddr).
        run util/simplemail.p (input "no-reply@alliantnational.com", 
                               input  cEmailAddr,
                               input "Error(s) while creating order", /* subject */
                               input cErrorMsg). /* body */

        pResponse:fault("3005","New File Creation Failed: " + cErrorMsg ).
        return.
     end.
    
    cProgramName = cProgramName + ".p".
   /* according the ctype getpayload calls */
    if cType = 'json'
     then
      do:
        oJsonObjectParent = new JsonObject ().
   
        pRequest:getPayloadJSON(input-output oJsonObjectParent, output std-ch).
        if std-ch > ""
         then
          do:
             pResponse:fault("3005", "Data Parsing Failed. Reason: " + std-ch).       
             return.
          end.
    
        oJsonObjectParentClone = cast(oJsonObjectParent:clone(), JsonObject).
        run value(cProgramName) (input oJsonObjectParentClone, 
                                 input pRequest:uid, 
                                 input pcAgentID, 
                                 output piAgentFileID, 
                                 output cIndexValueList,
                                 output cAddressList,
                                 output cStateCountyList,
                                 output cstatelist,
                                 output lSuccess, 
                                 output cErrorMsg).


      end.
    else if cType = 'xml' then
     do: /* XML */   
       create x-document xDocument.
   
       pRequest:getPayloadXML(input-output xDocument, output std-ch).
       if std-ch > ""
        then
         do:
            pResponse:fault("3005", "Data Parsing Failed. Reason: " + std-ch).       
            return.
         end.

       run value(cProgramName) (input xDocument, 
                                input pRequest:uid, 
                                input pcAgentID, 
                                output piAgentFileID, 
                                output cIndexValueList,
                                output cAddressList,
                                output cStateCountyList,
                                output cstatelist,
                                output lSuccess, 
                                output cErrorMsg).

     end.
   

     if cErrorMsg <> '' and cErrormsg <> "?"
      then
       do:
         cErrorMsg = trim(cErrorMsg,",").
         do std-in = 1 to num-entries(cErrorMsg,","):
                
            CreateErrorRecord(entry(std-in,cErrorMsg,",")).
         end.
       end.
 
    if piAgentFileID <> 0 and  piAgentFileID <> ?
     then
      do:
        
        CreateIndex(cIndexValueList,
                    cstatelist,
                    cAddressList,
                    cStateCountyList
                    ).

        for first agentfile exclusive-lock where agentfile.agentFileID = piAgentFileID:
          cFileNumber = agentfile.fileNumber.

          agentfile.softwareid = isoftwareid.
         
          /* creating mail box */
          if not agentfile.hasmailbox
           then
            run mail/createemailbox-p.p(input string(agentFile.agentfileID),
                                             "",
                                        output lMailboxCreated,
                                        output cMsg). 
         
           if lMailboxCreated 
            then
             agentfile.hasmailbox = true.
         
          validate agentfile.
          release agentfile.
          
          if cTaskID <> "" and cTaskID <> "?" and cTaskID <> ?
           then SetAgentFileID (cTaskID). /* setting back agentfileid in arc*/
        end.


        /* Create "Order Entry"  task and complete the task  */
        run file/newfile-createtask-p.p(input  piAgentFileID,
                                        input  integer(cTaskID),
                                        input  "",
                                        input  "Order Entry",
                                        input  "Production",
                                        input  false,
                                        output lCreateFileTask,
                                        output pMsg
                                        ).

        if not lCreateFileTask and pMsg <> "" 
         then createErrorRecord (pMsg).
        else
         do:
           for first filetask where filetask.agentfileID = piAgentFileID no-lock:

             run file/completefiletask-p.p(input  filetask.TaskID,
                                           input  string(piAgentFileID),
                                           input  false,
                                           output iErrCode,
                                           output cErrMessage).
             if cErrMessage <> ""
              then
               createErrorRecord ("Complete FileTask is failed: " + cErrMessage).
           end.
         end.
      
        /* creation of order review task. */
        run file/newfile-createtask-p.p(input  piAgentFileID,
                                        input  0,
                                        input  "",
                                        input  "Order Review",
                                        input  "Production",
                                        input  true,
                                        output lCreateFileTask,
                                        output pMsg
                                        ).

        if not lCreateFileTask and pMsg <> ""
         then
          createErrorRecord ("FileTask Order Review Create is failed: " + pMsg).
      end.
  
      /* create csv file. */
     if temp-table ttFileErr:has-records
      then
       do:
         publish "GetSystemParameter" from (session:first-procedure) ("TempFolder", output cTempPath).

         cTempPath = cTempPath + "\Errors_newFileCreation.csv".

         output stream str1 to value(cTempPath).
         export stream str1 delimiter "," "Sequence" "Error".
         for each ttFileErr:
            export stream str1 delimiter "," ttFileErr.err ttFileErr.description.
         end.
         output stream str1 close.
         publish "AddTempFile" from (session:first-procedure) ("NewFileError", cTempPath).
         publish "GetSystemParameter" from (session:first-procedure) ("ediErrorNotificationEmail", output cEmailAddr).
         
         /* mail the excel sheet errors */
         if search(cTempPath) <> ?
         then
          do:
            if cFileNumber <> ""
             then cMailSubject = "Error(s) while creating Agent File For Agent #" + pcAgentID + " and FileNumber #" + cFileNumber.
            else
              cMailSubject = "Error(s) while creating Agent File For Agent #" + pcAgentID.

            run util/attachmail.p ("no-reply@alliantnational.com",
                                    cEmailAddr,     
                                    "",                             /* CC */
                                    cMailSubject,   /* Subject */
                                   "action: " + pRequest:actionID + "<br>",                             /* Body */
                                    cTempPath).                     /* Attachment */
          end.
      end.

     if not lSuccess 
      then
       do:
         if valid-object(oJsonObjectParent)
          then
           delete object oJsonObjectParent.
  
         if valid-object(oJsonObjectParentClone)
          then
           delete object oJsonObjectParentClone.
  
         if valid-handle(xDocument)
          then
           delete object xDocument.
  
         pResponse:fault("3005","New File Creation Failed: " + cErrorMsg ).
         return.
       end.

   cFileName = GetFileName(string(piAgentFileID),cType).
   oJsonObjectParent:WriteFile(cFileName, yes).
   /* stores the payload into Azure */
   std-lo = storeContents(input piAgentFileID, input pRequest:UID, output pMsg).
   if not std-lo
    then
     createErrorRecord ("Uploading File is failed: " + pMsg).

   publish "AddTempFile" from (session:first-procedure) ("NewFile", cFileName).


   /* deleting objects */
  if valid-object(oJsonObjectParent)
   then
    delete object oJsonObjectParent.

  if valid-object(oJsonObjectParentClone)
   then
    delete object oJsonObjectParentClone.
 
  if valid-handle(xDocument)
   then
    delete object xDocument.

   pResponse:success("2000", "New File creation " + cErrorMsg + pMsg).

end method.

method public override char render(input pFormat as char, input pTemplate as char):
  define variable oAgentFileID as JsonObject no-undo.
  define variable cFileName    as character  no-undo.

  if piAgentFileID <> 0 and piAgentFileID <> -1 and piAgentFileID <> ?
    then
     do:
       oAgentFileID = new JsonObject ().
       oAgentFileID:add("AgentFileID", piAgentFileID).
       cFileName = GetFileName("ResponseAgentFileID","json").
       oAgentFileID:WriteFile(cFileName).
       delete object oAgentFileID.
       return cFileName.
     end.
  return ''.
end method.

method private character GetFileName ( input pcfilename as character,
                                       input pcType      as character):

  define variable cTempFolder as character no-undo.
  publish "GetSystemParameter" from (session:first-procedure) ("TempFolder", output cTempFolder).

  if cTempFolder = ? or cTempFolder = ""
   then
    cTempFolder = os-getenv("TEMP").

  if cTempFolder = ? or cTempFolder = ""
   then
    cTempFolder = os-getenv("TMP").

  if cTempFolder = ? or cTempFolder = ""
   then
    cTempFolder = "d:\temp\".

  return cTempFolder + "\" + trim(string(pcfilename)) + "-" + String(Year(today), "9999") + "-" + String(MONTH(today), "99") + "-" + String(DAY(today), "99") + "." + lc(pcType).

end method.

method logical storeContents(input piAgentFileID as integer,
                             input  pUID as character,
                             output pMsg as character):

  define variable cStorageEntity   as character no-undo.
  define variable cStorageID       as character no-undo.
  define variable cStorageCategory as character no-undo.
  define variable lStored          as logical   no-undo.

 assign
      cStorageEntity = "Production"
      cStorageID     =  string(piAgentFileID)
      .

 run util\UploadFile.p (pUID, true, cStorageEntity, cStorageID, cStorageCategory, cFileName, output lStored, output pMsg).    


 if pMsg > "" 
   then return false.
  else return true.
end method. 
  
method private void createErrorRecord (input pcErrorMsg as character):
  create ttFileErr.
  assign 
    iErrorSeq             = iErrorSeq + 1
    ttFileErr.err         = string(iErrorSeq)
    ttFileErr.description = pcErrorMsg
    .
end method.

method private void SetAgentFileID (input pcTaskID as character):
  define variable oPayload         as JsonObject no-undo.
  define variable cDataFilePath    as character  no-undo.
  define variable cURL             as character  no-undo.
  define variable pSuccess         as logical    no-undo.
  define variable pMsg             as character  no-undo.
  define variable pResponseFile    as character  no-undo.

  publish "GetSystemParameter" from (session:first-procedure) ("ARCSetAgentFileID", output cUrl).

  oPayload = new JsonObject ().
  oPayload:add("TaskId", integer(pcTaskID)).
  oPayload:add("AgentFileId", piAgentFileID).

  cDataFilePath = GetFileName("SetAgentFileID" + string(time) , "json").

  publish "AddTempFile" from (session:first-procedure) ("SetAgentFileIDDataFile" + string(mtime), cDataFilePath).

  oPayload:WriteFile(cDataFilePath).
    
  run util/httppost.p("",
                      cURL,
                      "",
                      "application/json",
                      cDataFilePath,
                      output pSuccess,
                      output pMsg,
                      output pResponseFile).

  publish "AddTempFile" from (session:first-procedure) ("SetAgentFileIDRes" + string(mtime), pResponseFile).

  if not pSuccess
   then
    do:
      if pMsg <> "" 
       then CreateErrorRecord(pMsg).
    end.

  if valid-object(oPayload)
   then delete object oPayload.

end.

  method public datetime-tz calculateDueDate(): 

    define variable dtzDueToday    as datetime-tz no-undo initial now. 
    define variable dtDueTommorrow as datetime    no-undo.           

    define variable dtzToday5PM    as datetime-tz no-undo.
    define variable dtzTomorrow5PM as datetime-tz no-undo.

    dtzToday5PM = datetime(month(dtzDueToday),day(dtzDueToday),year(dtzDueToday),17,0,0).

    if dtzDueToday < dtzToday5PM
     then
      return dtzToday5PM.
    else
    do:
      dtDueTommorrow = today + 1.
      dtzTomorrow5PM  = datetime(month(dtDueTommorrow),day(dtDueTommorrow),year(dtDueTommorrow),17,0,0).
   
      return dtzTomorrow5PM.
    end.

  end method.

  method private void CreateIndex(input ipcIndexValueList  as character,
                                  input ipcStateList       as character,
                                  input ipcAddressList     as character,
                                  input ipcStateCountyList as character):
    
    define variable lSuccess as logical   no-undo.
    define variable cMsg     as character no-undo.
    /* creating sysindex */
    if ipcIndexValueList ne ''  
     then
      do:
        /* Build keyword index to use for searching */
        run sys/createsysindex.p (input 'AgentFile',           /* ObjType */
                                  input '',                    /* ObjAttribute*/
                                  input string(piAgentFileID), /* ObjID */
                                  input ipcIndexValueList,       /* ObjKeyList*/
                                  output lSuccess,
                                  output cMsg).
        if not lSuccess
         then
          CreateErrorRecord(cMsg).
    end. /* if cIndexValueList ne '' */

    /* creating sysindex */
    if ipcStateList ne ''  
     then
      do:
        /* Build keyword index to state */
        run sys/createsysindex.p (input 'FileProperty',            /* ObjType */
                                  input 'State',                    /* ObjAttribute*/
                                  input string(piAgentFileID), /* ObjID */
                                  input ipcStateList,            /* ObjKeyList*/
                                  output lSuccess,
                                  output cMsg).
        if not lSuccess
         then
          CreateErrorRecord(cMsg).
      end. /* if cStateList ne '' */

       /* creating sysindex file property */
    if ipcAddressList ne ''  
     then
      do:
        /* Build keyword index to use for searching */
        run sys/createfilepropertyindex.p (input 'FileProperty',         /* ObjType */
                                           input 'Address',        /* ObjAttribute*/
                                           input string(piAgentFileID), /* ObjID */
                                           input ipcAddressList,       /* ObjKeyList*/
                                           input ipcStateCountyList,
                                           output lSuccess,
                                           output cMsg).
        if not lSuccess
         then
          CreateErrorRecord(cMsg).
    end. /* if cAddressList ne '' */  

  end method.

end class.

