/*------------------------------------------------------------------------
@name RenderFileInvoice.cls
@action fileInvoiceRender 

@returns company,fileinv,fileinvItem,agentfile
@author Shefali
@version 1.0
@created 03/22/2023
Modification:
Date          name      Description
5/25/2023     Sagar K   Changed as per new framework
07/07/2023    SB        Modified to remove the logic to get orderID.
07/25/2023    S Chandu  Modified logic to get contact details from job table.
03/15/2024    SB        Modified to add validation for agentfileID and lock.
05/23/2024    SB        Modified to remove the balance and paymentCredits columns from PDF.
08/27/2024    S Chandu  Modified to call the renderfileinvoice-p.p.
----------------------------------------------------------------------*/

class file.RenderFileInvoice inherits framework.ActionBase:
     

  define variable cDataFileName  as character no-undo.
  define variable cContentFileName as character no-undo.
 
    /* Global Variables*/
  define variable pResponse         as framework.IRespondable no-undo.
  define variable pRequest          as framework.IRequestable no-undo.

  define variable piFileInvoiceID   as integer                no-undo.
  define variable pcDocType         as character              no-undo.
  define variable cAgentID          as character              no-undo.
  define variable cAgentFileID      as integer                no-undo.
  define variable cAgentFileNumber  as character              no-undo.
  define variable iofficeID         as integer                no-undo.
  
  
    
  constructor RenderFileInvoice ():
    super().
  end constructor.

  destructor public RenderFileInvoice ():
  end destructor.   

  method public override void act (input pActRequest  as framework.IRequestable,
                                   input pActResponse as framework.IRespondable):

    define variable cAgentOffAddr  as character no-undo.
    define variable cBorrowerAddr  as character no-undo.

    /*dataset handle */
    define variable dsHandle      as handle    no-undo.
    /* Variable */
    define variable cTempPath     as character no-undo.
    define variable cUsername     as character   no-undo.
    
    {lib/std-def.i}

    assign
      pResponse = pActResponse
      pRequest  = pActRequest
      .
    
    pRequest:getParameter("fileInvID",  output piFileInvoiceID).
    pRequest:getParameter("DocType",    output pcDocType).

    
    /* validation when no parameter is supplied */
    if (piFileInvoiceID = 0  or piFileInvoiceID = ?)
     then 
      do:
        pResponse:fault("3001", "fileInvoiceID").
        return.
      end.
     
    /* validation of pcDocType */
    if (pcDocType = "" or (pcDocType <> "html" and pcDocType <> "pdf"))
     then
      do:
        pResponse:fault("3005", "Document type is not supports").
        return.
      end.
    
    /* validate fileInvoiceID */
    if not can-find(first fileInv where fileInv.fileInvID = piFileInvoiceID ) 
     then 
      do:
        pResponse:fault("3066", "File Invoice").
        return.
      end.

    for first fileInv no-lock where fileInv.fileInvID = piFileInvoiceID:
      if not can-find(first agentFile where agentFile.agentFileID = fileInv.agentFileID) 
       then 
        do:
          pResponse:fault("3066", "Agent File"). 
          return.
        end.

      /* Check if file is locked or not */
      {lib/islocked.i &obj='AgentFile' &id=fileInv.agentFileID &exclude-fault=TRUE}
      if std-lo and (std-ch <> pRequest:Uid)
       then
        do: 
          {lib/getusername.i &var=cUsername &user=std-ch}
   
          /* if file is locked by another user,cannot unlock it, return fault */
          pResponse:fault("3081", "Agent File" {&msg-add} cUsername).
          return.
        end.
    end.
 
    run file/renderfileinvoice-p.p( input piFileInvoiceID,
                                    input pRequest:actionid,
                                    input pRequest:uid,
                                    input pcDocType,
                                    input no, /* do not store to azure */
                                    input "",
                                    output cContentFileName,                                         
                                    output std-lo,
                                    output std-ch).
    if std-lo and std-ch <> ""
     then
      do:
         pResponse:fault("3005", "fileInvoiceRender failed. " + std-ch).
         return.
      end.

  end method.
  
method public override char render(input pFormat as char, input pTemplate as char):
  
    tContentFile = cContentFileName.

    pResponse:success("2000", "fileInvoiceRender").
    return tContentFile.
end.
      
end class.
