/*------------------------------------------------------------------------
@name file/NewFileInvoice.cls
@action fileInvoiceNew
@description  Creates a new fileInv and fileInvItem record.
@param AgentFileID;int  
@throws 3005;Data Parsing Failed  
@throws 3001;Invalid agentFileID
@throws 3066;Agent File does not exist
@throws 3005;Product is mandatory when there is a non-zero price.
@throws 3005;Either product or description is mandatory.
@throws 3000;File Invoice create failed
@author Shefali
@version 1.0
@created 02/06/2023
@Modified :
Date        Name          Comments  
07/07/2023  SB            Modified to correct the logic to get last order.
05/20/2024  S Chandu     Task:112512 Modified logic and added validations .
07/08/2024  S Chandu     Removed agent lock  validation.
08/27/2024  S Chandu      Modified to send back agentfileid.
12/04/2024  S Chandu     Modified to add modify stamp.
----------------------------------------------------------------------*/

class file.NewFileInvoice inherits framework.ActionBase:
    
  constructor NewFileInvoice ():
    super().
  end constructor.

  destructor public NewFileInvoice ():
  end destructor.

  {tt/fileInvAr.i}
  {tt/fileInvArItem.i} 

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable ifileInvID     as integer         no-undo.
    define variable piAgentFileID  as integer         no-undo.
    define variable lSuccess       as logical         no-undo. 
    define variable lReturnRecord  as logical         no-undo.
    define variable invNumSeq      as integer init 1  no-undo.
    define variable cUsername      as character       no-undo.
    define variable lError         as logical         no-undo.
    define variable cAgentID       as character       no-undo.
    define variable cFileID        as character       no-undo.

    {lib/std-def.i}
    {lib/nextkey-def.i}
    
    define buffer bFileInv for fileInv.
    
    define variable dsHandle as handle no-undo.
    create dataset  dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer fileInvAr:handle,
                         buffer fileInvArItem:handle).
    
    /* Getting temptable */
    empty temp-table fileInvArItem.
    
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    pRequest:getParameter("AgentFileID", output piAgentFileID).
    pRequest:getParameter("returnRecord", output lReturnRecord).
    
    if std-ch > '' 
     then
      do:
        pResponse:fault('3005', "Data Parsing failed. Reason: " + std-ch).
        return.
      end.
    
    /* validation when no parameter is supplied */
    if (piAgentFileID = 0  or piAgentFileID = ?)
     then 
      do:
        pResponse:fault("3001", "AgentFileID").
        return.
      end.
    
    /* validate piAgentFileID */
    if not can-find(first agentFile where agentFile.agentFileID = piAgentFileID ) 
     then 
      do:
        pResponse:fault("3066", "Agent File").
        return.
      end.

    if can-find(first fileInv where fileInv.agentFileID = piAgentFileID and fileInv.posted = false) 
     then
      do:
        pResponse:fault("3005", "There is already an open File Invoice.").
        return.
      end.

     /* atleast one invoiceitem should be there */
    if not can-find(first fileInvArItem)
     then
      do:
        pResponse:fault("3005", "Atleast one File InvoiceItem should be there.").  
        return.
      end.

    /*validate the file invoice items*/
    for each fileInvArItem no-lock:
  
      if fileInvArItem.description = ""
       then
        do:
          pResponse:fault("3005", "Description is required.").
          return.
        end.
    end.

    lSuccess = false.
    
    TRX-BLOCK:
    do transaction
      on error undo TRX-BLOCK, leave TRX-BLOCK:
      
      {lib/nextkey.i &type='fileInvID' &var=ifileInvID &err="undo TRX-BLOCK, leave TRX-BLOCK."}
      create fileInv.
      assign
          fileInv.fileInvID    = ifileInvID
          fileInv.agentFileID  = piAgentFileID
          fileInv.posted       = false
          fileInv.CreatedDate  = now
          fileInv.CreatedBy    = pRequest:uid
          .

      for first agentFile fields(agentFileID fileNumber agentID fileID) where agentFile.agentFileID = piAgentFileID no-lock:
        for last bFileInv where bFileInv.fileinvID <> ifileInvID and 
                                bFileInv.agentFileID = agentFile.agentFileID and 
                                bFileInv.invoiceNumber <> ? no-lock:
          assign invNumSeq = integer(entry(2,bFileInv.invoiceNumber,"_")) + integer(invNumSeq).	 
        end.

        assign fileInv.invoiceNumber = agentFile.fileNumber + "_" + string(invNumSeq).
        cAgentID = agentfile.agentID.
        cFileID  = agentfile.fileID.
      end.

      
      for each fileInvArItem:
        create fileInvItem.
        assign
            fileInvItem.fileInvID        = ifileInvID
            fileInvItem.seq              = fileInvArItem.seq
            fileInvItem.amount           = fileInvArItem.amount
            fileInvItem.description      = fileInvArItem.description
            fileinvitem.arInvID          = 0 /*Not used*/
            fileInvItem.createdDate      = now
            fileInvItem.createdBy        = pRequest:uid
            fileInvItem.postedamount     = 0
            fileInvItem.lastmodifiedby   = pRequest:uid
            fileInvItem.lastmodifieddate = now
            .

       if fileInv.invoiceAmount ne ? and fileInv.invoiceAmount ne 0 then
        assign fileInv.invoiceAmount = fileInv.invoiceAmount + fileInvItem.amount.
	   else 
         assign fileInv.invoiceAmount = fileInvItem.amount.

      end.

      validate fileInv. 
      release  fileInv.
    
      validate fileInvitem. 
      release  fileInvitem.

      lSuccess = true.
    
    end.

    /* jobtran record creation */
    run file/createjobtran-p.p (input  piAgentFileID,          /* File Invoice ID */
                                input  pRequest:actionID,      /* Action */
                                input  pRequest:uID,           /* Created By */
                                input  'agentFileID='+ string(piAgentFileID), /* Query String */
                                input  dataset-handle dsHandle). /* Payload */
    
    if pResponse:isFault()
     then
      return.
    
    if not lSuccess
     then
      do:
        pResponse:fault("3000", "File Invoice create").
        return.
      end.  
      
    if lReturnRecord
     then
      do:
        empty temp-table fileInvAr.
        empty temp-table fileInvArItem.
		
        for first fileInv where fileInv.fileInvID = ifileInvID no-lock:          
          create fileInvAr.
          assign
              fileInvAr.fileInvID     = fileInv.fileInvID
              fileInvAr.agentfileID   = fileInv.agentfileID 
              fileInvAr.invoiceDate   = fileInv.invoiceDate
              fileInvAr.invoiceNumber = fileInv.invoiceNumber
              fileInvAr.invoiceAmount = fileInv.invoiceAmount
              fileInvAr.postedDate    = fileInv.postedDate
              fileInvAr.osAmt         = fileInvAr.invoiceAmount - fileInvAr.postedAmount
              fileInvAr.posted        = fileInv.posted
              .

          for each fileInvitem where fileInvItem.fileInvID = fileInv.fileInvID no-lock:          
            create fileInvArItem.

            assign
                fileInvArItem.fileInvID     = fileInvitem.fileInvID
                fileInvArItem.invNumber     = fileInv.invoiceNumber
                fileInvArItem.seq           = fileInvitem.seq
                fileInvArItem.description   = fileInvitem.description
                fileInvArItem.arInvID       = fileInvItem.arInvID
                fileInvArItem.amount        = fileInvitem.amount
                fileInvArItem.postedamount  = fileInvItem.postedamount
                .
          end.

        end.
    
        setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
      end.
    
    pResponse:success("2002"," fileInvArItem").
         
  end method.
      
end class.




