/*------------------------------------------------------------------------
@file GetClosedTasks.cls
@action closedTasksGet
@description Get Closed Tasks
@returns Closed Tasks
@success 2005;X Closed Task(s) were returned
@author Sachin Chaturvedi
@version 1.0
@created 05/04/2023
Modification:
Date        Name          Comments
07/20/2023  SK            Modified to add field Posted
----------------------------------------------------------------------*/

class file.GetClosedTasks inherits framework.ActionBase:
    
  {tt/filetask.i}

  constructor GetClosedTasks ():
    super().
  end constructor.

  destructor public GetClosedTasks ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
	define variable pcStatus          as character   no-undo.
	define variable pcQueueName       as character   no-undo.
	define variable pcTaskName        as character   no-undo.
	define variable pdtEndRangeStart  as datetime-tz no-undo.
	define variable pdtEndRangeEnd    as datetime-tz no-undo.
    define variable plPosted          as logical     no-undo.
    define variable lError            as logical     no-undo.
    define variable cErrMessage       as character   no-undo.
    define variable std-in            as integer     no-undo.
	
    pRequest:getParameter("Status",     output pcStatus).
    pRequest:getParameter("QueueName",  output pcQueueName).
    pRequest:getParameter("TaskName",   output pcTaskName).
    pRequest:getParameter("EndDateRangeStart", output pdtEndRangeStart).
    pRequest:getParameter("EndDateRangeEnd",   output pdtEndRangeEnd). 
    pRequest:getParameter("Posted",   output plPosted).

    /* validate End date range starting */
    if pdtEndRangeStart = ?
     then 
      do:
        pResponse:fault("3001", "End date range start").  
        return.
      end.
    
    if pdtEndRangeEnd = ?
     then
      pdtEndRangeEnd = now.

    define variable dsHandle as handle no-undo.

    run file/getclosedtasks-p.p(input "",            /* UserName */
                                input pRequest:UID,  /* AssignedTo */
                                input pcStatus,
	  						    input pcQueueName,
                                input pcTaskName,
							    input pdtEndRangeStart,
							    input pdtEndRangeEnd,
                                input plPosted,
                                output dataset-handle dsHandle,
                                output lError,
                                output cErrMessage,
                                output std-in).
    
    if lError 
     then
      do:
        pResponse:fault ("3005",  "GetClosedtasks Failed: " + cErrMessage).
        return.
      end.

    if std-in > 0 then
      setcontentdataset(input dataset-handle dsHandle,input pRequest:FieldList).

    pResponse:success("2000", string(std-in) + " Closed Task").
             
  end method.
      
end class.


