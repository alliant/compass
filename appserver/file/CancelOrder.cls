/*------------------------------------------------------------------------
@name  CancelOrder.cls
@action fileOrderCancel
@description  Cancel the order which set order status to "Cancelled" with reason.
@param orderID;int 
@returns Fault; Gets from updateorderstatus-p.p
@returns Success;2000;Order Cancel was successful.
@author Shefali 
@Modified :
Date        Name          Comments   
07-12-2022  SC            Added logic to activate next open order      
07/07/2023  SB            Modified to remove the logic to activate next open order.      
----------------------------------------------------------------------*/

class file.CancelOrder inherits framework.ActionBase:
    
  {tt/holdfilenote.i &tablealias = "cancelOrderNote" &serializeName="cancelOrderNote"}
        
  constructor CancelOrder ():
    super().
  end constructor.

  destructor public CancelOrder ( ):
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
    {lib/std-def.i}

    define variable piOrderID      as integer   no-undo.
    define variable dsHandle       as handle    no-undo.
    define buffer   bufJob         for Job.
    
    pRequest:getParameter("OrderID", output piOrderID).

    if piOrderID = ?
     then
      piOrderID  = 0.
               
    create dataset dsHandle.  
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer cancelOrderNote:handle).
             
    /* get Dataset */
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.
      
    run file/updateorderstatus-p.p (input piOrderID,
                                    input dataset-handle dsHandle,
                                    input pRequest:UID,
                                    input 'X',
                                    output std-lo,
                                    output std-ch).
    if not std-lo
     then
      do:
        pResponse:fault("3005", std-ch).
        return.
      end.
    
    for first job fields (agentFileID) where job.orderID = piOrderID no-lock:
      /*jobtran record creation*/
      run file/createjobtran-p.p (input  job.agentFileID,        /* Agent File ID */
                                  input  pRequest:actionID,      /* Action        */
                                  input  pRequest:UID,           /* Created By    */
                                  input  'OrderID='+ string(piOrderID),   /* Query String  */
                                  input  dataset-handle dsHandle /* Payload       */
                                  ).
    end.
    
    pResponse:success("2000", "Order Cancel").
  end method.
      
end class.


