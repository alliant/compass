/*----------------------------------------------------------------------
@name getfileinvoices.cls
@action fileInvoicesGet
@description Provides a complete list of file invoices
@param FileID;char;File ID
@returns File Invoices
@author Shefali
@created 12.21.2022
@modified
NAME       DATE                Comments
Sagae K    09/13/2023          Datetime added
S chandu   05/20/2024          Task:112512 Modified logic to update fileAR table.
----------------------------------------------------------------------*/

class file.GetFileInvoices inherits framework.ActionBase:
       
  
  {tt/fileinvoice.i &tableAlias="ttFileInvoice"}
  {tt/product.i &tableAlias="ttProduct"}
  {tt/fileinvar.i}
  {tt/fileinvaritem.i}
  {tt/filear.i &tableAlias="ttFilear"}
  {lib/callsp-defs.i}
    
  
  constructor GetFileInvoices ():
    super().
  end constructor.

  destructor public GetFileInvoices ( ):
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    {lib/std-def.i}
    
    define variable iAgentFileID     as integer no-undo.
    define variable cPosted          as character no-undo.
    define variable cAgentID         as character no-undo.
	define variable dInvStartDate    as datetime  no-undo.
    define variable dInvEndDate      as datetime  no-undo.
	define variable ddDate           as date      no-undo.
	define variable dFromPostDate    as datetime  no-undo.
	define variable dToPostDate      as datetime  no-undo.
    define variable dsHandle         as handle    no-undo.
    

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer fileinvar:handle,
                         buffer fileinvaritem:handle,
                         buffer ttproduct:handle,
                         buffer ttfilear:handle).
    
    pRequest:getParameter("agentFileID", output iAgentFileID).
    pRequest:getParameter("posted", output cPosted).
    pRequest:getParameter("agentID", output cAgentID).
	pRequest:getParameter("invoiceStartDate", output dInvStartDate).
	pRequest:getParameter("invoiceEndDate", output dInvEndDate).
	pRequest:getParameter("fromPostDate", output dFromPostDate).
	pRequest:getParameter("toPostDate", output ddDate).

    dToPostDate = datetime(string(ddDate) + " 23:59:59.998" ).
    
    if iAgentFileID ne 0 and 
       not can-find(first fileInv where fileInv.agentFileID = iAgentFileID) 
     then
      pResponse:success("2005", string(csp-icount) {&msg-add} "File Invoice").

    if cPosted = ''
     then
      cPosted = 'B'.
   if cAgentID = ''
     then
      cAgentID = 'ALL'.

    {lib\callsp.i &name=spGetfileInvoices &load-into=ttFileInvoice &params="input iAgentFileID, 
                                                                            input cPosted, 
                                                                            input cAgentID,  
                                                                            input dInvStartDate,
                                                                            input dInvEndDate,
                                                                            input dFromPostDate,
                                                                            input dToPostDate" &noResponse=true &nocount=true}

    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end.
    
    empty temp-table fileinvar.
    empty temp-table fileinvaritem.
      
    for each ttfileinvoice group by ttfileinvoice.fileInvID:
      if first-of(ttfileinvoice.fileInvID) then
       do:
         create fileinvar.
         assign
             fileinvar.fileinvID     = ttfileinvoice.fileinvID
             fileinvar.agentfileID   = ttfileinvoice.agentfileID
             fileinvar.invoiceNumber = ttfileinvoice.invoiceNumber
             fileinvar.invoiceDate   = ttfileinvoice.invoiceDate
             fileinvar.invoiceAmount = ttfileinvoice.invoiceAmt
             fileinvar.posted        = ttfileinvoice.posted
             fileinvar.postedDate    = ttfileinvoice.postedDate
             fileinvar.agentID       = ttfileinvoice.agentID
             fileinvar.agentName     = ttfileinvoice.agentName
             fileinvar.fileNumber    = ttfileinvoice.fileNumber 
             .

         for first agent where agent.agentID = ttfileinvoice.agentID:
             assign fileinvar.stateID   = agent.stateID.
         end.
       end.
     
       create fileinvaritem.
       assign
           fileinvaritem.fileInvID     = fileinvar.fileInvID
           fileinvaritem.invNumber     = fileinvar.invoiceNumber
           fileinvaritem.seq           = ttfileInvoice.seq
           fileinvaritem.description   = ttfileinvoice.description
           fileinvaritem.amount        = ttfileinvoice.amount
           fileinvaritem.postedamount  = ttfileinvoice.itemAmount
           std-in                      = std-in + 1
           .

    end.
    
    if iAgentFileID <> 0
     then
      for first agentfile where agentfile.agentfileID = iAgentFileID no-lock:
        if agentfile.stateID = ? or agentfile.stateID = ""
         then
          for first agent field(stateID) where agent.agentID = agentfile.agentID no-lock:
            for each product where product.stateID = agent.stateID and
                                   product.active no-lock:
              create ttproduct.
              buffer-copy product to ttproduct.
              for first agentproduct where agentproduct.agentID   = agentfile.agentID and
                                           agentproduct.productID = product.productID no-lock:
                assign ttproduct.price = agentproduct.price.
              end.
            end.
          end.
        else 
         for each product where product.stateID = agentfile.stateID and
                                product.active no-lock:
           create ttproduct.
           buffer-copy product to ttproduct.
           for first agentproduct where agentproduct.agentID   = agentfile.agentID and
                                        agentproduct.productID = product.productID no-lock:
             assign ttproduct.price = agentproduct.price.
           end.
         end.
      end.
    else /* iAgentFileID = 0 */
     for each ttfileinvoice:
       for first agentfile where agentfile.agentfileID = ttfileinvoice.agentFileID no-lock:
         if agentfile.stateID = ? or agentfile.stateID = ""
          then
           for first agent field(stateID) where agent.agentID = agentfile.agentID no-lock:
             for each product where product.stateID = agent.stateID and
                                    product.active no-lock:
               if not can-find(first ttproduct where ttproduct.productID = product.productID ) 
                then
                 do:
                 
                   create ttproduct.
                   buffer-copy product to ttproduct.
                   for first agentproduct where agentproduct.agentID   = agentfile.agentID and
                                                agentproduct.productID = product.productID no-lock:
                      assign ttproduct.price = agentproduct.price.
                   end.

               end.
             end.
           end.
         else 
          for each product where product.stateID = agentfile.stateID and
                                 product.active no-lock:
            if not can-find(first ttproduct where ttproduct.productID = product.productID ) 
                then
                 do:
                 
                   create ttproduct.
                   buffer-copy product to ttproduct.
                   for first agentproduct where agentproduct.agentID   = agentfile.agentID and
                                                agentproduct.productID = product.productID no-lock:
                      assign ttproduct.price = agentproduct.price.
                   end.

               end.
          end.
       end.
     end.

    for each filear where filear.agentFileID = iAgentFileID no-lock:
      create ttFilear.
      buffer-copy filear to ttfilear.
      assign
          ttFilear.cancelDate = fileAR.tranDate.
    end.
  
    if csp-lSucess and (can-find(first fileinvar) or can-find(first fileinvaritem) or can-find(first ttproduct))
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldLIst).
    
    pResponse:success("2005", string(std-in) {&msg-add} "File Invoice").
             
  end method.
      
end class.





