/*------------------------------------------------------------------------
@name  GetFileTasks.p
@action fileTasksGet
@description  Get the Agent File.
@param AgentFileID;int
@returns Success;2000
@returns Fault;3005
@author Vignesh Rajan
@Modified :
Date        Name          Comments   
   
------------------------------------------------------------------------*/

class file.GetFileTasks inherits framework.ActionBase:
    
  constructor GetFileTasks ():
    super().
  end constructor.

  destructor public GetFileTasks ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     
    {lib/std-def.i}

    define variable piAgentFileID as integer   no-undo.
    define variable lError        as logical   no-undo.
    define variable cErrMessage   as character no-undo.
    define variable dsHandle      as handle    no-undo.

    pRequest:getParameter("agentFileID", output piAgentFileID). 
                     
    run file/getfiletasks-p.p(input piAgentFileID,
                              output dataset-handle dsHandle,
                              output lError,
                              output cErrMessage,
                              output std-in).

    if lError 
     then
      do:
       pResponse:fault ("3005",  "GetFileTask Failed: " + cErrMessage).
       return.
      end.
    
    if std-in > 0 then
     setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2000", string(std-in) + " File Task").
         
  end method.
      
end class.


