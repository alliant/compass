/*------------------------------------------------------------------------
@file QuerySearchOrders.cls
@action searchOrdersQuery
@description Gets all unfulfilled Search Orders 
@returns Open search orders
@success 2005;X Open serach order(s) were returned
@author S Chandu
@version 1.0
@created 12/23/2024
Modification:

----------------------------------------------------------------------*/

class file.QuerySearchOrders inherits framework.ActionBase:
    
  {tt/filetask.i &tableAlias="ttOpenTasks"}
  {tt/queryorders.i  &tableAlias="querySearchOrders"}

  constructor QuerySearchOrders ():
    super().
  end constructor.

  destructor public QuerySearchOrders ():
  end destructor.   

  /* Global variables */
  define variable pcStateID         as character no-undo.
  define variable pcOpenTasks       as character no-undo.
  define variable piMinCreatedHours as integer   no-undo.
  define variable std-in            as integer   no-undo.

  define buffer bufOpenTasks for ttOpenTasks.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
     
    define variable lError            as logical   no-undo.
    define variable cErrMessage       as character no-undo.
    define variable ipriorFulfilments as integer   no-undo.
	define variable dtCreatedDate     as datetime  no-undo.
    define variable iCreatedHours     as integer   no-undo.
    define variable cAgent            as character no-undo.
    define variable cAgentID          as character no-undo.
    define variable cAgentStateID     as character no-undo.
    define variable iCount            as integer   no-undo.


    define variable dsHandle  as handle no-undo.
    define variable dsHandle1 as handle no-undo.

    pRequest:getParameter("StateID",         output pcStateID).
    pRequest:getParameter("OpenTasks",       output pcOpenTasks). /* (Y)es (N)o (B)oth  */	  
    pRequest:getParameter("MinCreatedHours", output piMinCreatedHours).


    /* validate input params */
    if pcStateID = "ALL"
     then
      pcStateID = ''.

    if pcOpenTasks = ? or pcOpenTasks = '' 
     then
      pcOpenTasks  = 'B'.

    if piMinCreatedHours < 0
     then
      do:
        pResponse:fault("3005", "Created Hours cannot be less than zero").
        return. 
      end.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttOpenTasks:handle).


    create dataset dsHandle1.
    dsHandle1:serialize-name = 'data'.
    dsHandle1:set-buffers(buffer querySearchOrders:handle).

    /*Get open tasks from ARC*/
    run file/getopentasks-p.p(input '', /* UserName - for Permission */
                              input '',           /* Assigned To */
                              input "New,Open",  /*  Status */
		        	          input '',
                              input '',
                              output dataset-handle dsHandle,
                              output lError,
                              output cErrMessage,
                              output iCount).
    
    if lError 
     then
      do:
       pResponse:fault ("3005",  "GetOpentask Failed: " + cErrMessage).
       return.
      end.

      /* create the search orders dataset */
    /* Production Files which have atleast one Open Task and have atleast one open order */
    if pcOpenTasks = 'Y' 
     then
      for each ttOpenTasks no-lock
                 where (ttOpenTasks.stateID = if (pcStateID = '' or pcStateID = ? ) then ttOpenTasks.stateID else pcStateID)
                 break by ttOpenTasks.agentFileID:
      
        if first-of(ttOpenTasks.agentfileID)
         then
            /* get createddate and hours of latest job */
          for last job fields(createddate agentfileid) no-lock where job.agentfileid = ttOpenTasks.AgentFileId 
                                 and job.jobtype = 'Search' 
                                 and (job.stat = 'N' or job.stat = 'P') 
                                 and integer(interval(now,job.createdDate,'hours')) >= piMinCreatedHours
                                  by job.suffix:
                    
                assign
                    dtCreatedDate  = job.createdDate 
                    iCreatedHours  = integer(interval(now,job.createdDate,'hours'))
                    cAgent         = ttOpenTasks.agent + " ( " + ttOpenTasks.agentID + ")" 
                    cAgentStateID  = ttOpenTasks.stateID
                    .

               CreateSearchOrder(job.agentfileID, dtCreatedDate, iCreatedHours, cAgent, cAgentStateID).                       
             
          end. /* last job */
          
      end. /* for each ttOpentasks */
    else if pcOpenTasks = 'N' /* files with closed tasks but have open order */
     then
      for each job fields(createdDate agentfileid) no-lock
                where job.jobtype = "Search" and (job.stat = 'N' or job.stat = 'P') and integer(interval(now,job.createdDate,'hours')) >= piMinCreatedHours
                  and not can-find(first ttOpenTasks where ttOpenTasks.agentFileID =  job.agentFileID):     
        
         /* Get agentID from agentfileID */
         for first agentfile fields (agentID) where agentfile.agentfileID = job.agentFileID no-lock:
           for first agent fields (agentID) no-lock:
             assign
                 cAgentID   = agent.agentID
                 .
             
           end.
         end.

         for first agent fields(agentID name stateID) no-lock where agent.agentID = cAgentID
                                   and (agent.stateID = if (pcStateID = '' or pcStateID = ? ) then agent.stateID else pcStateID):
                                   
           assign
               dtCreatedDate  = job.createdDate 
               iCreatedHours  = integer(interval(now,job.createdDate,'hours'))
               cAgent         = agent.name + " ( " + agent.agentID + ")" 
               cAgentStateID  = agent.stateID
               .

           CreateSearchOrder(job.agentfileID, dtCreatedDate, iCreatedHours, cAgent, cAgentStateID).                       
         end.
      end. /* for each job */  

    else if pcOpenTasks = 'B' /* (B)oth can have open order file or open/closed task file */
     then
       for each job fields(createdDate agentfileid) no-lock
              where job.jobtype    = "Search"
                and (job.stat = 'N' or job.stat = 'P') 
                and integer(interval(now,job.createdDate,'hours')) >= piMinCreatedHours:
     
         /* if found in ttOpentasks use its agent details else fetch normally */
         if can-find(first ttOpentasks where ttOpentasks.agentfileID = job.agentFileID )
          then
           for first ttOpentasks no-lock where ttOpentasks.agentfileID = job.agentFileID
                                   and (ttOpenTasks.stateID = if (pcStateID = '' or pcStateID = ? ) then ttOpenTasks.stateID else pcStateID) :
              
             assign
                 dtCreatedDate  = job.createdDate 
                 iCreatedHours  = integer(interval(now,job.createdDate,'hours'))
                 cAgent         = ttOpenTasks.agent + " ( " + ttOpenTasks.agentID + ")"  
                 cAgentStateID  = ttOpenTasks.stateID
                 .
             CreateSearchOrder(job.agentfileID, dtCreatedDate, iCreatedHours, cAgent, cAgentStateID).
           end.
         else
          do:
            /* Get agentID from agentfileID */
            for first agentfile fields (agentID) where agentfile.agentfileID = job.agentFileID no-lock:
              for first agent fields (agentID) no-lock:
                assign
                    cAgentID   = agent.agentID
                    .
                
              end.
            end.

            for first agent fields(agentID name stateID) no-lock where agent.agentID = cAgentID
                                      and (agent.stateID = if (pcStateID = '' or pcStateID = ? ) then agent.stateID else pcStateID):
                                   
              assign
                  dtCreatedDate  = job.createdDate 
                  iCreatedHours  = integer(interval(now,job.createdDate,'hours'))
                  cAgent         = agent.name + " ( " + agent.agentID + ")" 
                  cAgentStateID  = agent.stateID
                  .
             
              CreateSearchOrder(job.agentfileID, dtCreatedDate, iCreatedHours, cAgent, cAgentStateID).                       
            end.
         end.
     end. /* for each job */

    if std-in > 0
     then
      setcontentdataset(input dataset-handle dsHandle1,input pRequest:FieldList).

    pResponse:success("2000", string(std-in) + " Open Search Order").
             
  end method.
      
  method public void CreateSearchOrder(input iAgentFileID  as integer,
                                       input dtCreatedDate as datetime,
                                       input iCreatedHours as integer,
                                       input cAgent        as character,
                                       input CAgentStateID as character):
  

    for first agentfile fields(agentFileID FileNumber closingDate notes) no-lock 
                  where agentfile.agentfileID  = iAgentFileID:

      /* create temp-table querySearchOrders */
      create querySearchOrders.
      assign
          querySearchOrders.agentFileID   = agentFile.agentFileID
          querySearchOrders.FileNumber    = agentFile.FileNumber
          querySearchOrders.closingDate   = agentfile.closingDate
          querySearchOrders.notes         = agentfile.notes
          querySearchOrders.createdDate   = dtCreatedDate 
          querySearchOrders.createdHours  = iCreatedHours        
          .

      /* Oldest open tasks fields when file have atleast 1 open tasks */
      if can-find(first ttOpenTasks where ttOpenTasks.agentFileID = agentFile.agentFileID) and ( pcOpenTasks = 'Y' or pcOpenTasks = 'B' )
       then
        for first bufOpenTasks no-lock 
                        where bufOpenTasks.agentFileID = agentFile.agentFileID
                           by bufOpenTasks.ID:
          assign 
              querySearchOrders.AssignedTo    = bufOpenTasks.AssignedTo
              querySearchOrders.AssignedDate  = bufOpenTasks.AssignedDate
              querySearchOrders.elapsedHours  = integer(interval(now,bufOpenTasks.AssignedDate,'hours'))
              querySearchOrders.oldestOpenTask = bufOpenTasks.topic
              .

            
            /* to pick the assigned name */
           for first sysuser fields (name) where sysuser.uid = ttOpenTasks.AssignedTo no-lock :
                querySearchOrders.AssignedToName = sysuser.name.
           end.
        end.

      assign 
          querySearchOrders.agent     = cAgent           
          querySearchOrders.stateID   = cAgentStateID
          querySearchOrders.StateDesc = getStateDesc(cAgentStateID)
          .
          
      /* county details from fileproperty */
      for first fileproperty fields(stateID countyID agentfileid) no-lock
                       where fileproperty.agentfileid = agentfile.AgentFileId
                         and fileproperty.stateID     = cAgentStateID:
        querySearchOrders.county = getCountyDesc(fileProperty.countyID, fileProperty.stateID).
      end.

      /* to find the prior fulfillments on the file */
      querySearchOrders.priorFulfillments = getPriorFulfillments(agentfile.agentfileid).
	       
      std-in = std-in + 1 .
    end. /* for first agentfile */

  end method.

  method public character getStateDesc(input cStateID as character):

    define variable cStateDesc as character no-undo.

    for first state field (description) where state.stateID = cStateID no-lock:
      cStateDesc = if state.description > ''then state.description else cStateID.
    end.
    
    if cStateDesc = '' then
      cStateDesc = cStateID.
   
    return cStateDesc.

  end method.

  method public character getCountyDesc(input cCountyID as character,input cStateID as character ):

     for first county field(description) no-lock
         where county.countyID = cCountyID
           and county.stateID  = cStateID:
       return county.description.
     end.
    
     return cCountyID.

  end method.

  method public integer getPriorFulfillments(input iAgentfileID as integer):

    define variable ipriorFulfilments as integer initial 0 no-undo.

    for each job fields(orderID) no-lock where job.agentfileid = iAgentfileID and job.jobtype = "Search" and job.stat = "F":
	  
       ipriorFulfilments = ipriorFulfilments + 1.
    end.

    return ipriorFulfilments.

  end method.

end class.


