/*------------------------------------------------------------------------
@name file/ModifyFileInvoice.cls
@action fileinvoiceModify
@description  Modify the existing file invoice item record for fileInvoiceID.
@param fileInvoiceID;int
@throws 3005;Data Parsing Failed
@throws 3001;Invalid fileInvoiceID
@throws 3066;File Invoice does not exist
@throws 3000;File Invoice modify failed
@throws 3005;You cannot modify items for a posted file invoice.
@throws 3005;Product is mandatory when there is a non-zero price.
@throws 3005;Either product or description is mandatory.
@throws 3000;File Invoice update failed 
@returns Success;2000
@author Shefali
@version 1.0
@created 02/06/2023
@Modified :
Date        Name          Comments   
07/07/2023  SB            Modified to correct the logic to get last order.
05/20/2024  S Chandu      Task:112512 Modified logic and added validations. 
07/08/2024  S Chandu      Removed agent lock  validation.
08/27/2024  S Chandu      Modified to send back agentfileid.
12/04/2024  S Chandu      Modified to add modify stamp.
----------------------------------------------------------------------*/

class file.ModifyFileInvoice inherits framework.ActionBase:
    
  constructor ModifyFileInvoice ():
    super().
  end constructor.

  destructor public ModifyFileInvoice ():
  end destructor.

  {tt/fileInvAr.i}
  {tt/fileInvArItem.i} 

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    {lib/std-def.i}
    
    /* local variable */
    define variable lSuccess        as logical            no-undo.
    define variable piFileInvoiceID as integer            no-undo.
    define variable iAgentFileID    as integer            no-undo.
    define variable lReturnRecord   as logical init false no-undo.
    define variable cUsername       as character          no-undo.

    define variable lError    as logical   no-undo.
    define variable cAgentID  as character no-undo.
    define variable cFileID   as character no-undo.
    define variable cErrorMsg as character no-undo.
    
    
    /*dataset handle */
    define variable dsHandle        as handle no-undo.
     
    /* parameter get */
    pRequest:getParameter("fileInvoiceID", output piFileInvoiceID).
    pRequest:getParameter("returnRecord", output lReturnRecord).

     
    /* dataset creation */
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer fileInvAr:handle,
                         buffer fileInvArItem:handle).  
    
	/* Getting temptable */
    empty temp-table fileInvArItem.
	
    /*dataset get*/
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.

    /* validation when no parameter is supplied */
    if (piFileInvoiceID = 0  or piFileInvoiceID = ?)
     then 
      do:
        pResponse:fault("3001", "fileInvoiceID").
        return.
      end.
    
    /* validate fileInvoiceID */
    if not can-find(first fileInv where fileInv.fileInvID = piFileInvoiceID ) 
     then 
      do:
        pResponse:fault("3066", "File Invoice").
        return.
      end.

    /* validate fileInvoiceID */
    if can-find(first fileInv where fileInv.fileInvID = piFileInvoiceID and fileInv.posted)
     then  
      do:
        pResponse:fault("3005", "You cannot modify items for a posted file invoice.").
        return.
      end.
    
    /* atleast one invoiceitem should be there */
    if not can-find(first fileInvArItem)
     then
      do:
        pResponse:fault("3005", "Atleast one File InvoiceItem should be there.").  
        return.
      end.

    /* validate the file invoice items */
    for each fileInvArItem no-lock:

      if (fileInvArItem.description = "")
       then
        do:
          pResponse:fault("3005", "Description is required.").
          return.
        end.
    end.

    for first fileInv where fileInv.fileInvID = piFileInvoiceID no-lock:
      assign 
          iAgentFileID          = fileInv.agentfileID
          .
    end.
    for first agentfile fields(agentID fileID) where agentfile.agentfileID = iAgentFileID no-lock:
      assign
          cAgentID     = agentfile.agentID
          cFileID      = agentfile.fileID
          .
    end.

    lSuccess = false.
    
    TRX-BLOCK:
    do transaction
      on error undo TRX-BLOCK, leave TRX-BLOCK:

      for each fileInvItem where fileInvItem.fileInvID = piFileInvoiceID exclusive-lock:
        delete fileInvItem.
      end.
      
      for each fileInvArItem:
        create fileInvItem.
        assign
            fileInvItem.fileInvID        = piFileInvoiceID
    	    fileInvItem.seq              = fileInvArItem.seq
    		fileInvItem.amount           = fileInvArItem.amount
    		fileInvItem.description      = fileInvArItem.description
            fileInvItem.arInvID          = 0  /* Not used */
            fileInvItem.createdDate      = now
            fileInvItem.createdBy        = pRequest:uid
            fileInvItem.postedamount     = 0
            fileInvItem.lastmodifiedby   = pRequest:uid
            fileInvItem.lastmodifieddate = now
            .
      end.

      for first fileInv where fileInv.fileInvID = piFileInvoiceID exclusive-lock:
        assign 
            fileInv.invoiceAmount = 0.00
            .

        for each fileInvItem fields(amount) where fileInvItem.fileInvID = fileInv.fileInvID no-lock:
          assign fileInv.invoiceAmount = fileInv.invoiceAmount + fileInvItem.amount.
        end.
             
        assign
            fileInv.lastmodifiedDate  = now
            fileInv.lastmodifiedBy    = pRequest:uid.
      end.
	

      validate fileInv. 
      release  fileInv.
    
      validate fileInvitem. 
      release  fileInvitem.

      lSuccess = true.
    end.
    
      /* jobtran record creation */
    run file/createjobtran-p.p (input  iAgentFileID,           /* Agent File ID */
                                input  pRequest:actionID,      /* Action */
                                input  pRequest:uID,           /* Created By */
                                input  'fileInvoiceID='+ string(piFileInvoiceID), /* Query String */
                                input  dataset-handle dsHandle). /* Payload */

    if lReturnRecord
     then
      do:
        empty temp-table fileInvAr.
        empty temp-table fileInvArItem.
		
        for first fileInv where fileInv.fileInvID = piFileInvoiceID no-lock:          
          create fileInvAr.
          assign
              fileInvAr.fileInvID     = fileInv.fileInvID
              fileInvAr.agentfileID   = fileInv.agentfileID 
              fileInvAr.invoiceDate   = fileInv.invoiceDate
              fileInvAr.invoiceNumber = fileInv.invoiceNumber
              fileInvAr.invoiceAmount = fileInv.invoiceAmount
              fileInvAr.postedDate    = fileInv.postedDate
              fileInvAr.osAmt         = fileInvAr.invoiceAmount - fileInvAr.postedAmount
              fileInvAr.posted        = fileInv.posted   
              .
     
          for each fileInvitem where fileInvItem.fileInvID = fileInv.fileInvID no-lock:          
            create fileInvArItem.
            assign
                fileInvArItem.fileInvID    = fileInvitem.fileInvID
                fileInvArItem.invNumber    = fileInv.invoiceNumber
                fileInvArItem.seq          = fileInvitem.seq
                fileInvArItem.description  = fileInvitem.description
                fileInvArItem.arInvID      = fileInvItem.arInvID
                fileInvArItem.amount       = fileInvitem.amount
                fileInvArItem.postedamount = fileInvItem.postedamount   	  
                .
          end.

        end.
     
        setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
      end.
    
    if not lSuccess
     then 
      do:
        presponse:fault("3000", "File Invoice update").
        return.
      end. /* if not lSuccess */
    
    pResponse:success("2000", "File Invoice update").
         
  end method.
      
end class.







