/*------------------------------------------------------------------------
@name  AssignQueue.cls
@action queueAssign
@description  Assigns the queue to the task
@param fileTaskID;int
@param Queuename;character
@param DueDate;datetime 

@returns ; success/failure returned from assignqueue-p.p

@author   Spandana
@Modified :
Date        Name          Comments   
05/02/2023  Sagar K       Changed as per new framework
05/05/2023  Sagar K       creating taskid if it is zero
12/06/2023  Vignesh R     Modified to skip the date valiation
12/12/2023  Vignesh R     Modified to add the date validation
02/27/2024  K.R           Modified to add validation for negative cost and completed task
04/25/2024  Shefali       Modified to validate duedate when there is a change in duedate
08/08/2024  Vignesh R     Modified to change the Error Message Codes.
----------------------------------------------------------------------*/

class file.AssignQueue inherits framework.ActionBase:

  constructor public AssignQueue ():
    super().
  end constructor.

  destructor public AssignQueue ( ):
  end destructor.
 
  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    define variable piFileTaskID  as integer     no-undo.
    define variable pcQueueName   as character   no-undo.
    define variable pdtDueDate    as datetime-tz no-undo.
    define variable pdcost        as decimal     no-undo.
    define variable plDueDateChanged as logical  no-undo.
    define variable piTaskID      as integer     no-undo.
    define variable lReturnVal    as logical     no-undo.
    define variable lcreatetask   as logical     no-undo.

    define variable iErrCode      as integer   no-undo.
    define variable cErrMessage   as character no-undo.

     /* converting local time into utc datetime-tz  */ 
    define variable cTodayDateTime as datetime-tz no-undo. 
    define variable utcMinutes      as integer    no-undo.
    define variable utcMinutesdiff  as integer    no-undo.
        
    pRequest:getParameter("FileTaskID",  output piFileTaskID). 
    pRequest:getParameter("QueueName",   output pcQueueName). 
    pRequest:getParameter("DueDate",     output pdtDueDate).
    pRequest:getParameter("cost",        output pdcost).
    pRequest:getParameter("DueDateChanged", output plDueDateChanged).

    /* validate FileTaskID*/
    if piFileTaskID = 0 or piFileTaskID = ?
     then
      do:
        pResponse:fault('3001', 'FileTaskID').
        return.
      end.

    if not can-find(first filetask where filetask.fileTaskID = piFileTaskID ) 
     then 
      do:
        pResponse:fault('3001', 'FileTaskID').
        return.
      end.

     if can-find(first filetask where filetask.fileTaskID = piFileTaskID and filetask.posted = true) 
     then 
      do:
        pResponse:fault('3005', 'Queue cannot be assigned as task is already posted').
        return.
      end.

    if pdtDueDate = ?
     then
      do:
        pResponse:fault("3001", "Due Date"). 
        return.
      end.
    
	if pdcost < 0
	 then
	  do:
	    pResponse:fault("3005", "Task cost cannot be less than zero."). 
        return.
      end.
	  
	if can-find(first filetask where filetask.fileTaskID = piFileTaskID and filetask.completedDate <> ?)
	 then
	  do:
	    pResponse:fault("3005", "Task cannot be assigned as it is already completed.").
	    return.
	  end.

    assign
        cTodayDateTime  = now
        utcMinutes      = timezone(cTodayDateTime)
        utcMinutesDiff  = int(- utcminutes) /* converted to utc required format */
        .
   
    /* client's due date is converting into UTC time */
    pdtDueDate = datetime-tz(pdtDueDate,utcMinutesDiff).
    pdtDueDate = datetime-tz(pdtDueDate,0).
   
    if plDueDateChanged then
    do:
      /* validate the date whether it is future date or not before creating the Task in ARC */
      if can-find(first fileTask where filetask.filetaskID = piFileTaskID /*and fileTask.taskID = 0*/) 
       then
        do:
          /* now on server time changed to UTc time */
          cTodayDateTime = datetime-tz(cTodayDateTime,utcMinutesDiff).
          cTodayDateTime = datetime-tz(cTodayDateTime,0).
    
          if pdtDueDate < cTodayDateTime
           then
            do:
              pResponse:fault("3005", "Due Date must be in the future").  
              return.
            end.
        end.
    end.

    if (pcQueueName ne ?) and (pcQueueName ne "") and not can-find(first queuecategory where queuecategory.category = pcQueueName and queuecategory.active = true) 
     then 
      do:
        pResponse:fault("3001", "Category").  
        return.
      end.


    for first filetask where filetask.filetaskID = piFileTaskID  no-lock:
      piTaskID =  filetask.taskID.
      if piTaskID = 0 or piTaskID = ?
       then do:
          /* ARC Task creation */
        lcreatetask = true.

        for first agentfile no-lock
          where agentfile.agentFileID = filetask.agentfileID:
          run file/createarctask-p.p(input  pcQueueName,
                                     input  filetask.topic,
                                     input  agentfile.agentFileID,
                                     input  agentFile.fileNumber,
                                     input  agentFile.fileID,
                                     input  filetask.assignedto,
                                     input  pdtDueDate,
                                     output piTaskID,
                                     output lReturnVal,
                                     output cErrMessage
                                                        ).
          if lReturnVal 
           then do:
            pResponse:fault("3005", "Task cannot be created in ARC, queue assign failed." + cErrMessage).
            return.
           end.
        end. /* for first agentfile */
       end.
    end.
    if lcreatetask
     then  
      FILETASK_UPD:
      do transaction 
       on error undo FILETASK_UPD, leave FILETASK_UPD:
       for first filetask exclusive-lock where filetaskid = piFileTaskID:
         filetask.taskid = piTaskID.
       end.
      end.

    FILETASK_UPD:
    do transaction 
     on error undo FILETASK_UPD, leave FILETASK_UPD:
      for first filetask exclusive-lock where filetask.filetaskid = piFileTaskID:
       filetask.cost = pdcost.
      end.
    end.

    run file/assignqueue-p.p(input  piTaskID,
                             input  pcQueueName,
                             input  pdtDueDate,
                             output iErrCode,
                             output cErrMessage).

    if iErrCode > 0 
     then 
      do:
        pResponse:fault("3000", cErrMessage).
        return.
      end.
        
    pResponse:success("2000", "Queue assignment").
    
  end method.
 
end class.

