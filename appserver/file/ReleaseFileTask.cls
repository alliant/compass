/*------------------------------------------------------------------------
@name  ReleaseFileTask.cls
@action fileTaskRelease
@description  release the task owner
@param fileTaskID;int
 

@returns ; success/failure returned from releasefiletask-p.p

@author   sagar k
@Date     05/01/2023
@Modified :
Date        Name          Comments  
07/20/2023  S chandu      Added validation for posted field. 
02/28/2024  K.R           Added validation for task is completed 
07/17/2024  SRK           Modified validation message  
----------------------------------------------------------------------*/

class file.ReleaseFileTask inherits framework.ActionBase:

  constructor public ReleaseFileTask ():
    super().
  end constructor.

  destructor public ReleaseFileTask ( ):
  end destructor.
 
  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    define variable piFileTaskID  as integer   no-undo.

    define variable iErrCode      as integer   no-undo.
    define variable cErrMessage   as character no-undo.
   
    pRequest:getParameter("FileTaskID",  output piFileTaskID). 

     /* validate fileTaskID */
    if piFileTaskID = 0 or piFileTaskID = ?
     then
      do:
         pResponse:fault('3001', 'FileTaskID').
        return.
      end.

   
   if not can-find(first filetask where filetask.fileTaskID = piFileTaskID ) 
    then 
     do:
        pResponse:fault('3001', 'FileTaskID').
        return.
     end.
    
	if can-find(first filetask where filetask.fileTaskID = piFileTaskID and filetask.completedDate <> ?)
	 then
	  do:
	    pResponse:fault("3005", "Ownership of the completed task cannot be released.").
	    return.
	  end.
    
     /* filetask with posted true cannot be released */
    if can-find(first filetask where filetask.fileTaskID = piFileTaskID and filetask.posted = true) 
     then 
      do:
        pResponse:fault('3005', 'Ownership of the Posted task cannot be released').
        return.
      end.

    for first filetask where filetask.filetaskID = piFileTaskID no-lock:
      run file/releasefiletask-p.p(input  filetask.taskID,
                                   output iErrCode,
                                   output cErrMessage).
    end.
     
    if iErrCode > 0 
     then 
      do:
        pResponse:fault(string(iErrCode), cErrMessage).
        return.
      end.
        
    pResponse:success("2000", "Task owner release").
    
  end method.
 
end class.

