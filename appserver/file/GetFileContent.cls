/*------------------------------------------------------------------------
@name  GetFileContent.cls
@action fileContentGet
@description  Get the File Content.
@param AgentFileID;int
@returns Success;2000
@returns Fault;3005
@author Shefali
@Modified :
Date        Name          Comments   
   
------------------------------------------------------------------------*/

class file.GetFileContent inherits framework.ActionBase:
  
 {lib/callsp-defs.i}  
 {tt/fileContent.i}
  
  constructor GetFileContent ():
    super().
  end constructor.

  destructor public GetFileContent ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    {lib/std-def.i}

    define variable piAgentFileID as integer   no-undo.
    define variable lSuccess      as logical   no-undo.
    define variable cErrMessage   as character no-undo.
    define variable dsHandle      as handle    no-undo.
    define variable pcType        as character no-undo.  

    create dataset dsHandle.  
    dsHandle:serialize-name = 'data'.      
    dsHandle:set-buffers(buffer fileContent:handle).

    pRequest:getParameter("agentFileID", output piAgentFileID).
    pRequest:getParameter("contentType", output pcType).

    if pcType = ? or pcType = "" then
      assign pcType = "A".
	  
	/* validation when no parameter is supplied */
    if (piAgentFileID = 0  or piAgentFileID = ?)
     then 
      do:
        pResponse:fault("3001", "AgentFileID").
        return.
      end.
    
    /* validate piAgentFileID */
    if not can-find(first agentFile where agentFile.agentFileID = piAgentFileID ) 
     then 
      do:
        pResponse:fault("3066", "Agent File").
        return.
      end.
     
    	 
    {lib\callsp.i &name=spGetFileContent &load-into=fileContent &params="input piAgentFileID,input pcType" &noResponse=true &nocount=true}

    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end.
                 

    if csp-lSucess 
     then
      do:
        setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
        pResponse:success("2000","Get File Content").
      end.
         
  end method.
      
end class.



