/*------------------------------------------------------------------------
@file ResendFulfillEmail.cls
@action fulfillEmailResend
@description Resend the mail used for fulfilling the order
@returns 
@success 2000;
@author K.R
@version 1.0
@created 11/04/2023
Modification:
Date        Name          Comments
02/22/2024  K.R           Modified to add validation for valid email address
04/01/2024  K.R           Modidfied to add sent date parameter
----------------------------------------------------------------------*/

class file.ResendFulfillEmail inherits framework.ActionBase:
    
  define temp-table ttMail no-undo serialize-name "Mail"
      fields mailTo                     as character
      fields mailFrom                   as character
      fields mailSubject                as character
      fields mailboxUserName            as character
      fields mailExistingAttachmentName as character
      fields mailMessageUID             as character
      fields mailBody                   as clob.

  define temp-table ttAttachment no-undo serialize-name "Attachment"
      fields attachmentName as character
      fields attachmentRaw  as blob.

  constructor ResendFulfillEmail ():
    super().
  end constructor.

  destructor public ResendFulfillEmail ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}
    define variable dsHandle            as handle    no-undo.
    define variable cSubject            as character no-undo.
    define variable cJamesEmailID       as character no-undo.
    define variable cExternalEmailList  as character no-undo.
    define variable cJamesDomain        as character no-undo.
    define variable cEmailID            as character no-undo.
    define variable cBody               as longchar  no-undo.
    define variable cAttachmentDir      as character no-undo.
    define variable cAttachmentPath     as character no-undo.
    define variable cAttachmentPathList as character no-undo.
    define variable lSuccess            as logical   no-undo.
    define variable cMsg                as character no-undo.
    define variable cActualMailFrom     as character no-undo.
    define variable iCounter            as integer   no-undo.
    define variable cErrorMsg           as character no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = "data".
    dsHandle:set-buffers(buffer ttMail:handle,
                         buffer ttAttachment:handle).

     /*dataset get*/
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    
    if std-ch > ''
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.

    if not temp-table ttMail:has-records
      then
       do:
         pResponse:fault('3005', 'No Email found to send').
         return.
       end.
   
    publish "GetSystemParameter" from (session:first-procedure) ("JamesMailDomain", output cJamesDomain).

    for each ttMail no-lock:
      copy-lob ttMail.mailBody to cBody.
      assign
        cSubject           = ttMail.mailSubject
        cExternalEmailList = ttMail.mailTo
        cJamesEmailID      = ttMail.mailboxUserName + cJamesDomain
        cActualMailFrom    = ttMail.mailFrom
      .
      do iCounter = 1 to num-entries(ttMail.mailExistingAttachmentName):
        run mail/getemailfile-p.p(input ttMail.mailboxUserName,
                                  input ttMail.mailMessageUID,
                                  input entry(iCounter, ttMail.mailExistingAttachmentName),
                                  output cAttachmentPath,
                                  output lSuccess,
                                  output cErrorMsg).
        
        cAttachmentPathList = cAttachmentPathList + cAttachmentPath + ",".
      end.
    end.

    if cExternalEmailList = "" 
     then
      do:
        pResponse:fault("3005", "Recipient email address cannot be blank").
        return.
      end.

    do iCounter = 1 to num-entries(cExternalEmailList, ";"):
      if not Util.CommonUtils:isValidEmailAddress(entry(iCounter, cExternalEmailList, ";"))
       then
        do:
          pResponse:fault("3005", entry(iCounter, cExternalEmailList, ";") + " is Invalid email address").
          leave.
        end.
    end.

    if pResponse:isFault() 
     then return.      

    cAttachmentPathList = trim(cAttachmentPathList, ",").
    publish "GetTempDir" from (session:first-procedure) (output cAttachmentDir).
    if cAttachmentDir = ""
     then publish "GetSystemParameter" from (session:first-procedure) ("dataRoot", OUTPUT cAttachmentDir).

    for each ttAttachment no-lock:
      cAttachmentPathList = cAttachmentPathList + ",".
      cAttachmentPath = cAttachmentDir + "/" + ttAttachment.attachmentName.
      copy-lob ttAttachment.attachmentRaw to file cAttachmentPath.
      publish "AddTempFile" from (session:first-procedure) (cAttachmentPath).
      cAttachmentPathList = cAttachmentPathList + cAttachmentPath + ",".
      cAttachmentPath = "".
    end.
    cAttachmentPathList = trim(cAttachmentPathList, ",").

    if cExternalEmailList <> "" 
     then
      do:
        if cAttachmentPathList > ""
          then run util/attachmail.p(cActualMailFrom,
                                    cExternalEmailList,
                                    "",
                                    cSubject,
                                    cBody,     
                                    cAttachmentPathList).
        else
         run util/simplemail.p(cActualMailFrom,
                               cExternalEmailList,
                               cSubject,
                               cBody
                               ).
      end.

    if cJamesEmailID <> ""
     then
      run mail/sendemail-p.p("",
                             cJAmesEmailID,
                             cActualMailFrom,
                             cExternalEmailList,
                             cSubject,
                             ?,   /* setting sent date value as unknown as it will be set by the program*/
                             cBody,
                             cAttachmentPathList,
                             output lSuccess,
                             output cMsg).

    pResponse:success("2000","Sending Email").
  end method.
      
end class.


