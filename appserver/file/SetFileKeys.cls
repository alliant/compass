/*------------------------------------------------------------------------
@name  file/SetFileKeys.cls
@action filekeysSet
@description  Change the agentID and/or fileNumber on the File.  

@param AgentID;char;ID of an Agent
@param FileNumber;char;The file number of an Agent
@param agentFileID;int;The unique sequence fileID of an Agent

@returns Success;int;2000;File Keys set was successful

@throws 3001;AgentFileID is invalid
@throws 3005; Either suppy AgentID and/or FileNumber.
@throws 3067;AgentID/AgentFileID does not exist
@throws 3015 ;At least one Agentfile exists for supplied agent ID and file ID 
@throws 3005;Agent file is not locked by current user.
@throws 3005;FileKeysSet was not successful

@author   Sachin Chaturvedi

@Modified    :
    Date        Name          Comments  
   07/05/2023   S Chandu      Modified to remove reference to jobQueue.
   02/21/2024   K.R           Modified to add validations for posted invoices, posted vouchers and fulfilled orders
   06/03/2024   Sachin        Task 112908- Changes in Agentfile related to Notes, Source and CPLAddr1, etc.
----------------------------------------------------------------------*/

class file.SetFileKeys inherits framework.ActionBase:

  constructor public SetFileKeys ():
    super().
  end constructor.

  destructor public SetFileKeys ( ):
  end destructor.
  

  /* normalizeFileID method */
  {lib/normalizefileid.i &method="true"}

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    define variable pcAgentID        as character no-undo.
    define variable pcFileNumber     as character no-undo.
    define variable cFileID          as character no-undo.
    define variable cStateID         as character no-undo.
    define variable cMsg             as character no-undo.
    define variable piAgentFileID    as integer   no-undo.
    define variable lSuccess         as logical   no-undo.
    define variable lError           as logical   no-undo.
    define variable dsHandle         as handle    no-undo.
    define variable iErrCode         as integer   no-undo.
    define variable cErrMessage      as character no-undo.
    define variable cErrorMsg        as character no-undo.
    define buffer   bufAgentFile     for agentFile.
    
    /* parameter get */
    pRequest:getParameter("AgentID", output pcAgentID).
    pRequest:getParameter("FileNumber", output pcfileNumber).
    pRequest:getParameter("agentFileID", output piAgentFileID).
    
    {lib/std-def.i}

    /* validation when AgentFileID is not supplied */
    if (piAgentFileID <= 0  or piAgentFileID = ?)
     then 
      do:
        pResponse:fault("3001", "AgentFileID"). 
        return.
      end.
  
    /* validation when both AgentID and FileID are blank */
    if (pcAgentID     = "" or pcAgentid     = ?) and 
       (pcfileNumber  = "" or pcfileNumber  = ?) 
     then 
      do:
        pResponse:fault("3005", "File Keys set failed. Either supply Agent ID and/or File Number"). 
        return.
      end.  

    /* validate agentID if supplied */
    if pcAgentID <> "" and not can-find(first agent where agent.agentID = pcAgentID) 
     then 
      do:
        pResponse:fault("3067", "Agent" {&msg-add} "ID" {&msg-add} pcAgentID).  
        return.
      end.

    /* validate agentfileID if supplied */
    if not can-find(first agentfile where agentfile.agentfileID = piAgentFileID ) 
     then 
      do:
        pResponse:fault("3067", "AgentFile" {&msg-add} "agentFileID" {&msg-add} string(piAgentFileID)).  
        return.
      end.

    /* convert file Number into file ID. */
    cFileID = normalizeFileID(pcfileNumber) . 

    for first agentFile fields (agentID fileid fileNumber) no-lock 
     where agentFile.agentFileID = piAgentFileID:
	
      if pcAgentID = "" or pcAgentID = ? 
       then 
        pcAgentID = agentFile.agentID.
    
      if pcfileNumber = "" or pcfileNumber = ?
       then 
        assign 
            cFileID      = agentFile.fileID 
	        pcfileNumber = agentfile.fileNumber.
        
      /* Validate if agentfile record already exists for supplied agent ID and/or file ID combination */
      if can-find(first bufAgentFile where bufAgentFile.agentID      = pcAgentID and
                                           bufAgentFile.fileID       = cfileid   and
                                           bufAgentFile.agentfileID  <> piAgentFileID) 
       then
        do:
          pResponse:fault("3015", "AgentFile" {&msg-add} "supplied Agent ID and File Number").
          return.  
        end.
  
      /* Check if there is any child table of current agentFile */
  
      /* CPL */
      if can-find(first cpl where cpl.agentID = agentFile.agentID and
                                  cpl.fileID  = agentFile.fileid) 
       then
        do:
          pResponse:fault("3015", "CPL" {&msg-add} "AgentFile").
          return.  
        end.
    
      /* policy */  
      if can-find(first policy where policy.agentID = agentFile.agentID and
                                     policy.fileID  = agentFile.fileid) 
       then
        do:
          pResponse:fault("3015", "Policy" {&msg-add} "AgentFile").
          return.  
        end. 
    

      /* artran */  
      if can-find(first artran where artran.entityID = agentFile.agentID and
                                     artran.fileID   = agentFile.fileid) 
       then
        do:
          pResponse:fault("3015", "Posted Transaction" {&msg-add} "AgentFile").
          return.  
        end. 
 
    end.

    /* when agent file is not locked by current user then return fault. */
    {lib/islocked.i &obj='AgentFile' &id=piAgentFileID &exclude-fault=true}
    if std-lo and (std-ch <> pRequest:UID)
     then 
      do:
        pResponse:fault("3005", "Agent file is not locked by current user.").
        return. 
      end.
  
    /* Agent File must not be on hold  */
    if can-find(first agentfile where agentfile.agentfileID = piAgentFileID and
                                     agentfile.ishold = true) 
     then                                                                                               
      do:                                                                                               
        pResponse:fault("3005", "Agent file is on hold.").                                             
        return.                                                                                        
      end.
 
    /* There should be no posted invoices*/
    if can-find(first fileinv where fileinv.agentFileID = piAgentFileID and fileinv.posted = true)
     then
      do:
        pResponse:fault("3015", "Posted Invoice" {&msg-add} "AgentFile").
        return.
      end.

    /* There should be no posted vouchers */
    if can-find(first filetask where filetask.agentfileID = piAgentFileID and filetask.posted = true) 
     then
      do:
        pResponse:fault("3015", "Posted Voucher" {&msg-add} "AgentFile").
        return.
      end.

    /* There should be no fulfilled orders*/
    if can-find(first job where job.agentfileID = piAgentFileID and job.stat = 'F') 
     then
      do:
        pResponse:fault("3015", "Fulfilled Order" {&msg-add} "AgentFile").
        return.
      end.

    /* Change the stateID of agentfile as per agent's stateID */
    find first agent no-lock where agent.agentID = pcAgentID no-error.
    if available agent
     then
      cStateID = agent.stateID.
    
    lSuccess = false.

    TRX-BLOCK:
    for first agentFile exclusive-lock 
     where agentFile.agentFileID = piAgentFileID transaction
      on error undo TRX-BLOCK, leave TRX-BLOCK:
      assign
          agentFile.agentID     =  pcAgentID
          agentFile.fileNumber  =  pcfileNumber
          agentFile.fileID      =  cfileID
          agentFile.stateID     = cStateID
          agentfile.source      = 'P'  /* Production */
          .
      validate agentfile.
      release agentfile.
                   
     /* Arc File Keys Set Creation */
     run file/setfilekeys-p.p(input piAgentFileID,
                              input pcfileNumber,
                              input cfileID,
                              input pcAgentID,
                              output iErrCode,
                              output cErrorMsg
                               ).

     if iErrCode > 0 
      then
       pResponse:fault("3005", "filekeysSet was not successful " + cErrorMsg).

     if pResponse:isFault()
      then
       undo TRX-BLOCK, leave TRX-BLOCK.
      
     lSuccess = true.    
    end.

    if not lSuccess
     then
      do:
        pResponse:fault("3002", 'filekeysSet').
        return.
      end.

    /* create jobTran each time there is update action on a agentFile. */
    run file/createjobtran-p.p(input piAgentFileID,       
                               input pRequest:actionID,      
                               input prequest:UID,          
                               input 'AgentFileID=' + string(piAgentFileID) + '|' + 'AgentID=' + pcAgentID + '|' + 'FileNumber=' + pcfileNumber,   
                               input dataset-handle dsHandle).


    pResponse:success("2000", "filekeysSet").
 
  end method.
 
end class.

