/*------------------------------------------------------------------------
@name  ConfirmTaskComplete.cls
@action taskCompleteConfirm
@description  Sends task complete confirmation email
@returns ; success/failure returned 

@author   K.R
@Date     10/15/2024
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/

class file.ConfirmTaskComplete inherits framework.ActionBase:
  
  /* temp-table defination*/
  {tt/filetask.i &tableAlias=ttfiletask}
  constructor public ConfirmTaskComplete ():
    super().
  end constructor.

  destructor public ConfirmTaskComplete ( ):
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}

    define variable iTaskID        as integer   no-undo.
    define variable cAgentFileID   as character no-undo.
    define variable cErrorMsg      as character no-undo.
    define variable iFileTaskID    as integer   no-undo. /* compass filetask id*/

    define variable cMailBody      as character no-undo.
    define variable cMailSubject   as character no-undo.
    define variable cHtmlEmailBody as character no-undo.

    /* Getting parameters*/
    pRequest:getParameter("TaskID", output iTaskID).
    pRequest:getParameter("AgentFileID", output cAgentFileID).

    /* validations */
    if iTaskID = 0 or iTaskID = ?
     then
      do:
        pResponse:fault("3005", "Task ID cannot be zero or invalid").
        return.
      end.
    if cAgentFileID = "" or cAgentFileID = ? 
     then
      do:
        pResponse:fault("3005", "Task ID cannot be blank or invalid").
        return.
      end.
    
    if not IsTaskCompleted(iTaskID, output cErrorMsg) 
     then
      do:
        /* there was some error while checking if task was completed in arc or not*/
        if cErrorMsg > "" 
         then
          do:
            pResponse:fault("3005", substitute('Not able to retrieve the task from ARC. &1',cErrorMsg)).
            return.
          end.

        pResponse:fault("3005", substitute('Task ID: &1 is not completed in ARC.', iTaskID)).
        return.
      end.

    for first filetask where filetask.taskID = iTaskID no-lock:
      iFileTaskID = filetask.filetaskID.
    end.
    
   /* Creating email body and subject for senidng email*/
   if not  CreateEmailParts (iFileTaskID, output cMailBody, output cMailSubject)
    then 
     do:
       pResponse:fault("3005", substitute("Failed to create task complete comfirmation email body for task id: &1", iTaskID)).
       return.
     end.

   /* Sending email*/
   if not SendEmail(cMailBody, cMailSubject, output cErrorMsg) 
    then 
      do:
        pResponse:fault("3005", substitute("Failed to send task complete comfirmation email for task id: &1", iTaskID)).
        return.
      end.

   /* Archiving sent email to james*/
   if not ArchiveEmail (cMailBody, cMailSubject, cAgentFileId, output cErrorMsg)
    then 
     do:
       pResponse:fault("3005", "Failed to archive confirmation email to james").
       return.
     end. 
 
   pResponse:success("3005", "taskCompleteConfirmation is successful").
  end method.


  /* This method return true if task is completed , false if task is not completed 
     or false if there was some error in api call and sets cMsg
  */
  method private logical IsTaskCompleted (input piTaskID as integer, 
                                          output cMsg as character):

    define variable iErrCode      as integer   no-undo.
    define variable cErrorMessage as character no-undo.
    define variable dsHandle      as handle    no-undo.
    
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttfiletask:handle).

    run file/gettaskbyid-p.p(input piTaskID,
                             output dataset-handle dsHandle,
                             output iErrCode,
                             output cErrorMessage).
    if iErrCode > 0 
     then
      do:
        cMsg = cErrorMessage.
        return false.
      end.

    if can-find(first ttfiletask where ttfiletask.taskStatus = "closed" and ttfiletask.EndDateTime <> ?)
     then return true.
    return false.
  end.

  method private logical sendEmail (input  pcMailBody    as character,
                                    input  pcMailSubject as character,
                                    output pcErrorMsg    as character):

    /* variables for all required parameters used in sending email*/

    define variable tSent      as logical   no-undo.
    define variable tMsg       as character no-undo.
    define variable tServer    as character no-undo.
    define variable tDomain    as character no-undo.
    define variable tFile      as character no-undo.
    define variable tExtension as character no-undo.
    define variable tFileDef   as character no-undo init "".
    define variable tImageExt  as character no-undo.
    define variable pFrom      as character no-undo.
    define variable cEmailAddrTo   as character  no-undo.

    &scoped-define imageTypes "png"
    
    MESSAGE "Mail: " pcMailBody
        .

    /*  generating mail body and its subject*/


    /* Getting all required parameters*/

    publish "GetSystemParameter" from (session:first-procedure) ("ImageExtensions", output tImageExt).
    if tImageExt = ""
     then tImageExt = {&imageTypes}.
     
    publish "GetSystemParameter" from (session:first-procedure) ("SmtpServer", output tServer).
    if tServer = "" 
     then
      do: 
        pcErrorMsg = "Configuration error: SmtpServer address not found".
        return false.
      end.
  
    publish "GetSystemParameter" from (session:first-procedure) ("emailDomain", output tDomain).
    if tDomain = "" 
     then
      do: 
        pcErrorMsg = "Configuration error: emailDomain  not found".
        return false.
      end.
 
    if (pFrom > "" and index(pFrom, tDomain) = 0) or pFrom = ""
     then publish "GetSystemParameter" from (session:first-procedure) ("emailDefaultFrom", output pFrom).
    if pFrom = "" 
     then
      do: 
        pcErrorMsg = "Configuration error: From address or emailDefaultFrom not found".
        return false.
      end.

    publish "GetSystemParameter" from (session:first-procedure) ("TaskAlert", output cEmailAddrTo).
    if cEmailAddrTo = "" 
     then
      do: 
        pcErrorMsg = "Configuration error: TaskAlert not found".
        return false.
      end.


    /* sending internal mail */
    run util/smtpmail.p
                      (tServer,
                       cEmailAddrTo, /* which configured  from ini - to mail */
                       pFrom, /* uid no-replay */
                       "", 
                       "",
                       "",
                       pcMailSubject,
                       pcMailBody,
                       "type=text/html:charset=us-ascii:filetype=ascii",
                       "",
                       1,
                       false,
                       "",
                       "",
                       "",
                       output tSent,
                       output tMsg).

    if not tSent
     then
      do:
        pcErrorMsg = tMsg.
        return tSent.
      end.

    return true.
  end.

  method private logical ArchiveEmail (input  pcMailBody    as character,
                                       input  pcMailSubject as character,
                                       input  pcAgentFileID as character,
                                       output pcErrorMsg    as character):
     
    define variable cJamesDomain as character no-undo.
    define variable lSuccess     as logical   no-undo.
    define variable cErrorMsg    as character no-undo.
    define variable pFrom        as character no-undo.
    define variable cEmailAddrTo as character no-undo.

    publish "GetSystemParameter" from (session:first-procedure) ("JamesMailDomain", output cJamesDomain).
    if cJamesDomain = "" 
     then
      do:
        pcErrorMsg = "Configuration error: JamesMailDomain not found".
        return false.
      end.

    publish "GetSystemParameter" from (session:first-procedure) ("emailDefaultFrom", output pFrom).
    if pFrom = "" 
     then
      do: 
        pcErrorMsg = "Configuration error: emailDefaultFrom not found".
        return false.
      end.

    publish "GetSystemParameter" from (session:first-procedure) ("TaskAlert", output cEmailAddrTo).
    if cEmailAddrTo = "" 
     then
      do: 
        pcErrorMsg = "Configuration error: TaskAlert not found".
        return false.
      end.

    /* james email send ... for every case internal template should send to james */
    for first agentFile fields (agentFileID hasmailbox) exclusive-lock 
        where agentFile.agentFileID = int(pcAgentfileid):

       
      if not agentfile.hasmailbox
       then
        run mail/createemailbox-p.p(input string(agentFile.agentfileID),
                                    "",
                                    output lsuccess,
                                    output cErrorMsg). 

 
      if lSuccess
	   then
	    agentfile.hasmailbox = true.
	  
      run mail/sendemail-p.p("", 
                            string(agentFile.agentfileiD) + cJamesDomain, /*  to  agentfileiD + cjamesDoamin */
                            PFrom, /* no-replay */                                
                            cEmailAddrTo,    
                            pcMailSubject, 
                             ?,							
                            pcMailBody, 
                            "", 
                            output lSuccess,
                            output cErrorMsg).
 
      validate agentfile.
      release agentfile.
    end. /* for first agentfile */

    return lSuccess.
  end.

  method private logical CreateEmailParts (input  piFileTaskID  as integer,
                                           output pcMailBody    as character,
                                           output pcMailSubject as character):

    define variable htmlElementText as character no-undo.
    define variable iAgentFileID    as integer   no-undo.
    define variable lfileproperty   as logical   no-undo.
    define variable cAddress        as character no-undo.
    define variable iseq            as integer   no-undo.
    define variable iFiletask       as integer   no-undo.

    for first filetask where filetask.filetaskID = piFileTaskID  no-lock:

      iAgentFileID    = filetask.agentfileID.
      htmlElementText = htmlElementText + '<table style="padding:5px;text-align:left;"> <tr>'.

      for each fileproperty where fileproperty.agentfileID = filetask.agentfileID no-lock:

        cAddress = "".
        assign             
            cAddress            = if (fileProperty.addr1 = "?") or (fileProperty.addr1 = "")                then "" else fileProperty.addr1
            cAddress            = cAddress + if (fileProperty.addr2 = "?")   or (fileProperty.addr2 = "")   then "" else ", " + fileProperty.addr2
            cAddress            = cAddress + if (fileProperty.addr3 = "?")   or (fileProperty.addr3 = "")   then "" else ", " + fileProperty.addr3
            cAddress            = cAddress + if (fileProperty.addr4 = "?")   or (fileProperty.addr4 = "")   then "" else ", " + fileProperty.addr4
            cAddress            = cAddress + if (fileProperty.city  = "?")   or (fileProperty.city = "")    then "" else ", " + fileProperty.city
            cAddress            = cAddress + if (fileProperty.stateID = "?") or (fileProperty.stateID = "") then "" else ", " + fileProperty.stateID
            cAddress            = cAddress + if (fileProperty.zip   = "?")   or (fileProperty.zip = "")     then "" else ", " + fileProperty.zip
          . 

        for first county fields (description) no-lock 
         where county.countyID = fileProperty.countyID and 
          county.stateID  = fileProperty.stateID:

          assign cAddress =  cAddress + if (county.description = "?") or (county.description = "") then "" else ", " +  county.description.
        end.

        if iseq = 0
         then
         htmlElementText = htmlElementText +  '<td style="text-align:right;vertical-align: top;Width:13%"> <p> <b>Properties:</b> </p> </td> <td> '.

        cAddress = trim(cAddress,',').
        htmlElementText = htmlElementText + ' <p style="margin : 0; padding-top: 0;line-height:1.7">'  + if cAddress <> "?" or cAddress = "" then string(cAddress) else ' ' + " <p/>".
        lfileproperty = true.
        iseq = iseq + 1 .

      end.

      if iseq > 1
       then
         htmlElementText = htmlElementText + " </td> </tr>".

      if not lfileproperty
       then
        htmlElementText = htmlElementText  +  ' <td style="text-align:right;Width:13%"><p> <b> Property : </b> </p> </td> <td > </td> </tr>'.

      for first agentfile field(filenumber agentID) where agentfile.agentfileID = filetask.agentfileID no-lock:

        htmlElementText =  htmlElementText + ' <tr> <td style="text-align:right;Width:15%"><p><b> File Number:</b> </p> </td> <td> <p> ' + string(agentfile.filenumber) + " </p> </td> </tr>".

        for first agent field (name) where agent.agentID = agentfile.agentID no-lock:

          htmlElementText =  htmlElementText + '<tr> <td style="text-align:right;Width:13%"> <p><b> Agent:</b> </p> </td> <td> <p> ' + string(agent.name) + " </p> </td> </tr>".
          /* subject creation */
          pcMailSubject = pcMailSubject + " Completed " + string(filetask.topic) + " for " + string(agent.name) + "(" + string(agentfile.filenumber) + ")".
        end.
      end.

      htmlElementText =  htmlElementText + '<tr> <td style="text-align:right;Width:13%"> <p><b>Task:</b> </p> </td> <td> <p> ' + string(filetask.topic) + ' </p> </td> </tr>'.

      for first ttfiletask where ttfiletask.filetaskID = piFileTaskID:

        htmlElementText =  htmlElementText  + ' <tr> <td style="text-align:right;Width:13%"> <p><b> Started: </b> </p> </td> <td> <p> '   + if ttfiletask.startDateTime = ? then ' ' else string(ttfiletask.startDateTime,"99/99/9999 HH:MM AM")  + ' </p> </td> </tr>  '. 
        htmlElementText =  htmlElementText  + ' <tr> <td style="text-align:right;Width:13%"> <p><b> Completed: </b> </p> </td> <td> <p> ' + if ttfiletask.endDateTime   = ? then ' ' else string(ttfiletask.endDateTime,"99/99/9999 HH:MM AM")  + " </p> </td> </tr>".
        htmlElementText =  htmlElementText  + ' <tr> <td style="text-align:right;vertical-align: top;Width:13%"> <p><b> Notes: </b></p> </td> <td> <p> ' + if ttfiletask.ImportantNote = ? or ttfiletask.ImportantNote = "" then '</td> ' else string(substring(ttfiletask.ImportantNote,4,length(ttfiletask.ImportantNote) - 7))  + " </p> </td> </tr>".

      end.

    end. /* for first filetask */

    htmlElementText = htmlElementText + " </table>".

    /* table 2 */
    for each ttfiletask where ttfiletask.agentfileID = iAgentFileID and (ttfiletask.taskStatus = 'Open' or ttfiletask.taskStatus = 'New') no-lock:

      if iFiletask = 0 
       then
        do:     
          htmlElementText = htmlElementText + '<p><u> Open Tasks: </u></p>'.
          htmlElementText = htmlElementText + ' <table style="padding: 5px;text-align: left;width:100%"> <tr> <th style="text-align: left;">Task</th> <th style="text-align: left;">Assigned To</th><th style="text-align: left;">Created</th><th style="text-align: left;">Notes</th></tr>'.
        end.

      htmlElementText = htmlElementText + ' <tr style="text-align: left;"> ' + ' <td >' + if ttfiletask.topic = '?' or ttfiletask.topic = '' then ' ' else string(ttfiletask.topic) + ' </td>'.
      htmlElementText = htmlElementText + ' <td >' + if ttfiletask.assignedToName = '?' or ttfiletask.assignedToName = '' then ' ' else string(ttfiletask.assignedToName) + ' </td>'.
      htmlElementText = htmlElementText + ' <td >' + if ttfiletask.startDateTime  = ? then ' ' else string(ttfiletask.StartDateTime,"99/99/9999 HH:MM AM")  + ' </td>'.
      htmlElementText = htmlElementText + ' <td >' + if ttfiletask.ImportantNote  = ? or ttfiletask.ImportantNote = '' then ' ' else string(substring(ttfiletask.ImportantNote,4,length(ttfiletask.ImportantNote) - 7)) + ' </td>'.

      htmlElementText = htmlElementText + ' </tr> '.
      iFiletask = iFiletask + 1 .
    end.


    if iFiletask >= 1
     then
      htmlElementText = htmlElementText + " </table>".
    else
     htmlElementText = htmlElementText + " <p> <b> No Open Tasks </b> </p>".

    pcMailBody = htmlElementText.
    return true.

  end.
   
end class.


