/*------------------------------------------------------------------------
@name  getagentfile.p
@action getAgentFile
@description  Get the Agent File.
@param AgentFileID;int
@returns Success;2000
@returns Fault;3005
@author Vignesh Rajan
@Modified :
Date        Name          Comments   
   
------------------------------------------------------------------------*/

class file.GetAgentFile inherits framework.ActionBase:
    
  constructor GetAgentFile ():
    super().
  end constructor.

  destructor public GetAgentFile ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    {lib/std-def.i}

    define variable piAgentFileID as integer   no-undo.
    define variable lSuccess      as logical   no-undo.
    define variable cErrMessage   as character no-undo.
    define variable dsHandle      as handle    no-undo.

    pRequest:getParameter("agentFileID", output piAgentFileID). 

    if piAgentFileID = ?
     then
      piAgentFileID = 0.
                     
    run file/getagentfile-p.p(input piAgentFileID,
                              input pRequest:UID,
                              output dataset-handle dsHandle,
                              output lSuccess,
                              output cErrMessage).
                 

    if lSuccess 
     then
      do:
        setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
        pResponse:success("2000","Get Agent File").
      end.
     else
      pResponse:fault("3005", cErrMessage).
         
  end method.
      
end class.


