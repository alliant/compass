/*------------------------------------------------------------------------
@name  SearchAgentFile.cls
@action AgentFileSearch
@description  Search the Agent File.
@param searchString;char
@returns Success;2005
@returns Fault;3005
@author Sachin Anthwal
@Modified :
Date        Name     Comments 
06-03-2024  Sachin   Task 112908- Changes in Agentfile related to Notes, Source and CPLAddr1, etc.
08/30/2024  AG       Modified to return the stateID
----------------------------------------------------------------------*/

class file.SearchAgentFile inherits framework.ActionBase:


    
  constructor SearchAgentFile ():
    super().
  end constructor.

  destructor public SearchAgentFile ():
  end destructor.   

  {tt/searchagentfile.i &tableAlias="tsearchagentfile"}

  define temp-table ttsearchagentfile like tsearchagentfile
  field tempPercent as decimal serialize-hidden.

  {lib/normalizefileid.i &method=true}

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable cSearchString  as character no-undo.
    define variable iCount         as integer   no-undo.
    define variable cObjKey        as character no-undo.
    define variable cstateID       as character no-undo.
    define variable cAddress       as character no-undo. 
    define variable cSearchCode    as character no-undo.
    define variable cSearchValue   as character no-undo.
    define variable iTaskID        as integer   no-undo.
    define variable cAgentID       as character no-undo.
    define variable iOrderID       as integer   no-undo.
    define variable iNumRecords    as integer   no-undo.
    define variable lFilteredSearch as logical no-undo.

    define variable dsHandle      as handle    no-undo.
    
    {lib/std-def.i}
   
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttsearchagentfile:handle).


    pRequest:getParameter("searchString", output cSearchString).
    pRequest:getParameter("stateID", output cstateID).

    if cstateID = "" or cstateID = ?
     then
      cstateID = "ALL".
    
    cSearchString = trim(cSearchString).
    if cSearchString = ''
     then
      return.
      
    if pResponse:isFault()
     then 
      return.

    cSearchCode = substring(cSearchString,1,2).
    case cSearchCode:
      when "F:"
       then
         do:
           lFilteredSearch = true.
           cSearchValue = substring(cSearchString, 3).
           cSearchValue = normalizeFileID(cSearchValue).
           for each agentfile where agentfile.fileid = cSearchValue no-lock:
             FindAgentFile(agentfile.agentFileID, input-output iNumRecords). 
           end.
         end.
      when "T:"
       then
         do:
           lFilteredSearch = true.
           cSearchValue = substring(cSearchString, 3).
           iTaskID = integer(cSearchValue) no-error.
           for each filetask where filetask.taskid = iTaskID no-lock: /* Need to come back*/
             FindAgentFile(filetask.agentFileID, input-output iNumRecords). 
           end.
        end.

     when "O:"
      then
        do:
          lFilteredSearch = true.
          cSearchValue = substring(cSearchString, 3).
          iOrderID = integer(cSearchValue) no-error.

          for each job where job.orderid = iOrderID no-lock:
            FindAgentFile(job.agentFileID, input-output iNumRecords).   
          end.
       end.

     when "A:"
      then
        do:
          lFilteredSearch = true.
          cSearchValue = substring(cSearchString, 3).
          cAgentID = cSearchValue no-error.

          for each agentfile fields (agentfileID) where agentfile.agentID = cAgentID and agentfile.source = "P" no-lock:
            for first job fields (agentfileID) where job.agentfileID = agentfile.agentfileID and (job.stat = 'N' or job.stat = 'P'):
              FindAgentFile(job.agentFileID, input-output iNumRecords).
            end.
          end.
       end.

    end case.

    if lFilteredSearch
     then
      do:
        if iNumRecords > 0
         then
          setContentDataset(input dataset-handle dsHandle, pRequest:FieldLIst).

        pResponse:success("2005", string(iNumRecords) {&msg-add} "searchagentfile").
        return.
      end.
    
    do icount = 1 to num-entries(cSearchString," "): 
      cObjKey = entry(icount,cSearchString," ").
      
      for each sysindex no-lock
        where sysindex.objType      = 'AgentFile'
          and sysindex.objAttribute = ""
          and sysindex.objKey       begins cObjKey :    
                   
        std-de = (length(entry(icount,cSearchString," ")) / length(sysindex.objKey)) * (100 / num-entries(cSearchString," ")).
      
        if can-find(first ttsearchagentfile where ttsearchagentfile.agentFileID = integer(sysindex.objID)) 
         then
          do:
            for first ttsearchagentfile 
              where ttsearchagentfile.agentFileID = integer(sysindex.objID):                           
              if std-de > ttsearchagentfile.tempPercent  
               then
                ttsearchagentfile.tempPercent = std-de.
            end. /* for first ttsearchagentfile */
          end. /* if can-find(first ttsearchagentfile where ttsearchagentfile.agentFileID = integer(sysindex.objID))  */
        else
         do:
           for first agentfile no-lock
             where agentfile.agentFileID = integer(sysindex.objID) and
                   agentfile.stateID     = if cStateID = "ALL" then agentfile.stateID else cStateID:
             
             if not can-find(first fileProperty where fileProperty.agentfileID = agentfile.agentFileID ) 
              then
               do:
                create ttsearchagentfile.
                assign
                    ttsearchagentfile.agentFileID       = agentfile.agentFileID
                    ttsearchagentfile.fileID            = agentfile.fileID
                    ttsearchagentfile.fileNumber        = agentfile.fileNumber
                    ttsearchagentfile.buyerDescription  = agentfile.buyerDescription
                    ttsearchagentfile.sellerDescription = agentfile.sellerDescription
                    . 
                for first agent no-lock where agent.agentID = agentfile.agentID:          
                  ttsearchagentfile.agentName = agent.name.
                end.
               end. /*  if not can-find(first fileProperty */
             else
              do:
                for each fileProperty no-lock
                  where fileProperty.agentfileID = agentfile.agentFileID:
                  
                  create ttsearchagentfile.
                  assign
                      ttsearchagentfile.agentFileID       = agentfile.agentFileID
                      ttsearchagentfile.fileID            = agentfile.fileID
                      ttsearchagentfile.fileNumber        = agentfile.fileNumber
                      ttsearchagentfile.buyerDescription  = agentfile.buyerDescription
                      ttsearchagentfile.sellerDescription = agentfile.sellerDescription
                      ttsearchagentfile.parcelID          = fileProperty.parcelID   
                      ttsearchagentfile.book              = fileProperty.book       
                      ttsearchagentfile.pageNumber        = fileProperty.pageNumber 
                      ttsearchagentfile.Condominum        = fileProperty.Condominum 
                      ttsearchagentfile.subdivision       = fileProperty.subdivision
                      cAddress                            = if (fileProperty.addr1 = "?") or (fileProperty.addr1 = "") then "" else fileProperty.addr1
                      cAddress                            = cAddress + if (fileProperty.addr2 = "?")   or (fileProperty.addr2 = "")   then "" else ", " + fileProperty.addr2
                      cAddress                            = cAddress + if (fileProperty.addr3 = "?")   or (fileProperty.addr3 = "")   then "" else ", " + fileProperty.addr3
                      cAddress                            = cAddress + if (fileProperty.addr4 = "?")   or (fileProperty.addr4 = "")   then "" else ", " + fileProperty.addr4
                      cAddress                            = cAddress + if (fileProperty.city  = "?")   or (fileProperty.city = "")    then "" else ", " + fileProperty.city
                      cAddress                            = cAddress + if (fileProperty.stateID = "?") or (fileProperty.stateID = "") then "" else ", " + fileProperty.stateID
                      cAddress                            = cAddress + if (fileProperty.zip   = "?")   or (fileProperty.zip = "")     then "" else ", " + fileProperty.zip
                      .

                  for first county field (description) no-lock 
                   where county.countyID = fileProperty.countyID and 
                    county.stateID  = fileProperty.stateID:

                    assign cAddress =  cAddress + if (county.description = "?") or (county.description = "") then "" else ", " +  county.description.
                  end.

                  assign 
                      cAddress                       =  trim(cAddress,',')
                      ttsearchagentfile.propertyAddr =  trim(cAddress)
                      .
                  for first agent no-lock where agent.agentID = agentfile.agentID:          
                    ttsearchagentfile.agentName = agent.name + ' (' + agent.agentID + ')'.
                  end.
                end. /* for each fileProperty */
              end. /* if not can-find(first fileProperty */
              
             assign
                 ttsearchagentfile.tempPercent = std-de.
                 std-in = std-in + 1.
    
           end. /* for first searchagentfile no-lock */
         end. /* else */
      end. /* for each sysindex no-lock */
      
      for each ttsearchagentfile:
        assign
              ttsearchagentfile.percentMatch = ttsearchagentfile.percentMatch + ttsearchagentfile.tempPercent
              ttsearchagentfile.tempPercent  = 0
              .
      end. /* for each ttsearchagentfile: */
    end. /*  do icount = 1 to num */

    for each ttsearchagentfile:
      assign
          ttsearchagentfile.ipercentMatch = round(ttsearchagentfile.percentMatch, 0).
          .

    end. /* for each ttsearchagentfile: */
    
    if std-in > 0 
     then
      setContentDataset(input dataset-handle dsHandle, pRequest:FieldLIst).
        
    pResponse:success("2005", string(std-in) {&msg-add} "searchagentfile").
  end method.

  method public void FindAgentFile (input iAgentfileID as integer, input-output iNumRecords as integer):

    define variable cstateID     as character no-undo.
    define variable cAddress     as character no-undo.

    for first agentfile no-lock 
      where agentfile.agentfileID = iAgentfileID:

      create ttsearchagentfile.
      assign
          ttsearchagentfile.agentFileID       = agentfile.agentFileID
          ttsearchagentfile.fileID            = agentfile.fileID
          ttsearchagentfile.fileNumber        = agentfile.fileNumber
          ttsearchagentfile.buyerDescription  = agentfile.buyerDescription
          ttsearchagentfile.sellerDescription = agentfile.sellerDescription
          .

       for first agent no-lock where agent.agentID = agentfile.agentID:          
         assign
             ttsearchagentfile.agentName = agent.name + ' (' + agent.agentID + ')'
             ttsearchagentfile.stateID   = agent.stateID.
       end.

      for each fileproperty no-lock
        where fileproperty.agentfileID = agentfile.agentfileID:

        assign
            ttsearchagentfile.parcelID          = fileProperty.parcelID   
            ttsearchagentfile.book              = fileProperty.book       
            ttsearchagentfile.pageNumber        = fileProperty.pageNumber 
            ttsearchagentfile.Condominum        = fileProperty.Condominum 
            ttsearchagentfile.subdivision       = fileProperty.subdivision
            cAddress                            = if (fileProperty.addr1 = "?") or (fileProperty.addr1 = "") then "" else fileProperty.addr1
            cAddress                            = cAddress + if (fileProperty.addr2 = "?")   or (fileProperty.addr2 = "")   then "" else ", " + fileProperty.addr2
            cAddress                            = cAddress + if (fileProperty.addr3 = "?")   or (fileProperty.addr3 = "")   then "" else ", " + fileProperty.addr3
            cAddress                            = cAddress + if (fileProperty.addr4 = "?")   or (fileProperty.addr4 = "")   then "" else ", " + fileProperty.addr4
            cAddress                            = cAddress + if (fileProperty.city  = "?")   or (fileProperty.city = "")    then "" else ", " + fileProperty.city
            cAddress                            = cAddress + if (fileProperty.stateID = "?") or (fileProperty.stateID = "") then "" else ", " + fileProperty.stateID
            cAddress                            = cAddress + if (fileProperty.zip   = "?")   or (fileProperty.zip = "")     then "" else ", " + fileProperty.zip
            .
           
        for first county field (description) no-lock 
          where county.countyID = fileProperty.countyID and 
           county.stateID  = fileProperty.stateID:

           assign cAddress =  cAddress + if (county.description = "?") or (county.description = "") then "" else ", " +  county.description.
        end.

        assign 
            cAddress                       =  trim(cAddress,',')
            ttsearchagentfile.propertyAddr =  trim(cAddress)
            .


      end. /* for each fileProperty */
      iNumRecords = iNumRecords + 1.      
    end. /* for first agentfile */

  end method.
      
end class.


