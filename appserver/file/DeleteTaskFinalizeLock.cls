/*------------------------------------------------------------------------
@file DeleteTaskFinalizeLock.cls
@action taskFinalizeLockDelete 
@description Delete Vendor Task Finalize

@author SRK
@version 1.0
@created 09/02/2024
Modification:

----------------------------------------------------------------------*/

class file.DeleteTaskFinalizeLock inherits framework.ActionBase:
   

  constructor DeleteTaskFinalizeLock ():
    super().
  end constructor.

  destructor public DeleteTaskFinalizeLock ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable pSuccess  as logical no-undo.

    TRX-BLOCK:
    for first syslock exclusive-lock
      where   syslock.entityType = "VendorTaskFinalize" TRANSACTION
        on error undo TRX-BLOCK, leave TRX-BLOCK:
      delete syslock.
      release syslock.
      pSuccess = true.
    end.


    if not pSuccess
     then
      do: 
        pResponse:fault("3000", "VendorTaskFinalize Unlock").
        return.
      end.
    
    pResponse:success("2000", "VendorTaskFinalize Unlock").
             
  end method.
      
end class.


