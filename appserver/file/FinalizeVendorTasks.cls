/*------------------------------------------------------------------------
@name  file/FinalizeVendorTasks.cls
@action VendorTasksFinalize
@description Finalize Zero Cost Vendor Tasks
@parameters
@returns 
@fault 3000:Finalize Vendor Tasks failed.
@success 2000;char;file task records partially posted was successful.
         2000: Finalize vendor Tasks was successful.

@author Vignesh Rajan
@version 1.0
@created 08/04/2023

@Modified    :
    Date        Name                 Comments   
    08/29/2023   K.R                 Modified to show date and UID in generated csv
    02/26/2024   K.R                 Modified to remove code to set the completeddate in filetask table
    07/29/2024   S.B                 Modified to change arctaskpost as Sync process instead of queued process
----------------------------------------------------------------------*/

class file.FinalizeVendorTasks inherits framework.ActionBase:

 constructor public FinalizeVendorTasks ():
  super().
 end constructor.

 destructor public FinalizeVendorTasks ():
 end destructor.
    

 {tt/filetask.i &tableAlias=ttFileTask}
 {tt/filetask.i &tableAlias=ttarcfiletask}
 {tt/proerr.i   &tableAlias="ttTaskPostErr"}

 {lib/nextkey-def.i}
 {lib/callsp-defs.i}

 /* Global Variables*/
 define variable iErrorSeq      as integer    no-undo.
 define variable cVendorList    as character  no-undo.
 define variable cFileList      as character  no-undo.
 define variable cVoucherList   as character  no-undo.

 define stream str1.
 
 method public override void act (input pRequest  as framework.IRequestable,
                                  input pResponse as framework.IRespondable):
   {lib/std-def.i}

   define variable dsHandle       as handle    no-undo.
   define variable cUserName      as character no-undo.
   define variable lLocked        as logical   no-undo.
   define variable lSuccess       as logical   no-undo.
   define variable pcTaskIDList   as character no-undo.
   define variable lValidated     as logical   no-undo initial true.
   define variable notPOsted      as integer   no-undo.
   define variable dsHandle2      as handle    no-undo.
   define variable lError         as logical   no-undo.
   define variable cErrMessage    as character no-undo.
   define variable pEndDate       as date      no-undo.
   define variable iApInvID       as integer   no-undo.
   define variable iApVoucherID   as integer   no-undo.
   
   define variable pcStatus          as character no-undo.
   define variable pcStatusMessage   as character no-undo.
   define variable pcFailedIdList    as character no-undo.

   empty temp-table ttfiletask.
   empty temp-table ttarcfiletask.
   empty temp-table ttTaskPostErr.

   create dataset dsHandle.
   dsHandle:serialize-name = 'data'.
   dsHandle:set-buffers(buffer ttFileTask:handle).

   create dataset dsHandle2.
   dsHandle2:serialize-name = 'data'.
   dsHandle2:set-buffers(buffer ttarcfiletask:handle).

   pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
   if std-ch > ""
    then
     do:
       lValidated = false.
       createErrorRecord ('Data Parsing Failed. Reason: ' + std-ch).
       return.
     end.

   if not can-find(first ttFileTask) 
    then
     do:
       lValidated = false.
       createErrorRecord ("FileTask record not provided").
       return.
     end.
 
   if pEndDate = ?
    then
     pEndDate = today.

   /* Validate filetask should be completed and not posted */
   run file/getclosedtasks-p.p(input "",            /* UserName */
                               input "",            /* AssignedTo */
                               input "Closed",     /* Status = complete */
                               input "",            /* Queue */
                               input "",           /* Task */
                               input '',           /* EndDateRangeStart */
                               input pEnddate,     /* EndDateRangeEnd */
                               input false,        /* posted flag*/
                               output dataset-handle dsHandle2,
                               output lError,
                               output cErrMessage,
                               output std-in).
   if lError 
    then
     do:
       lValidated = false.
       createErrorRecord ("Posting Failed: " + cErrMessage).
     end.

   for each ttfiletask:
     
     /* validate the filetaskID */
     if not can-find(first filetask where filetask.filetaskID = ttfiletask.filetaskID)
      then
       do:
         lValidated = false.
         createErrorRecord("FileTask DB record does not exist for FileTaskID: " + string(ttfiletask.filetaskID)).
      end.
     else
      for first filetask fields (filetaskID posted taskID) 
        where filetask.filetaskID = ttfiletask.fileTaskID:
     
        /* validate from arc */
        if not can-find(first ttarcfiletask where ttarcfiletask.ID = filetask.taskID)
         then
          do:
            lValidated = false.
            createErrorRecord ("Task with ID " + string(ttfiletask.filetaskID) + " is not completed in ARC").
          end.
     
        /* validate the posted flag*/
       if filetask.posted = true 
        then
         do:
           lValidated = false.
           createErrorRecord("File Task " + string(filetask.filetaskID) + " is already posted").
         end. 
     
        /* validate cost */
       if filetask.cost ne 0
        then
         do:
          lValidated = false.
          createErrorRecord("The cost of fileTask " + string(filetask.fileTaskID) + " should be zero").
        end.
     
      end.

   end. /* for each ttfiletask */

   /* When filetaskposting is locked by any user */
   {lib/islocked.i &obj='FileTaskAPPosting' &id='' &exclude-fault=true}
   if std-lo and (std-ch <> pRequest:Uid)
    then
     do:
       {lib/getusername.i &var=cUsername &user=std-ch}  
       /* if file is locked by another user,cannot unlock it, return fault */
       lValidated = false.
       createErrorRecord ("AP Voucher Posting is locked by " + cUsername).
     end.

   if lValidated
    then
     do:
       /* Lock Posting */
       run util/newsyslock.p ("FileTaskAPPosting", "", 0, pRequest:Uid, output lLocked).
       if not lLocked
        then
          do:
            lValidated = false.
            createErrorRecord("Unable to lock FileTaskAPPosting"). 
            return.
          end.
     end.

   if not lValidated 
    then
     do:
       setContentTable("FileTaskPostErr",input table ttTaskPostErr, input pRequest:FieldList).
       pResponse:success("3005","Validations failed, check the error file generated for more details").
       return.
     end.   

   assign
       lSuccess     = false
       pcTaskIDList = ""
       .

   TRX-BLOCK:
   for each ttFiletask break by ttFiletask.vendorID transaction
     on error undo TRX-BLOCK, next TRX-BLOCK:

     if first-of(ttFiletask.vendorID)
      then
       do:
         assign
             iApInvID     = 0
             iApVoucherID = 0
             .
         
         {lib/nextkey.i &type='apInvoice' &var=iApInvID &err="undo TRX-BLOCK, next TRX-BLOCK."}
         /* creating apinv record */
         create apinv.
         assign
             apinv.apinvID         = iApInvID
             apinv.refType         = "T"
             apinv.refID           = '' 
             apinv.refSeq          = 0
             apinv.refCategory     = ''  
             apinv.invoiceNumber   = string(iApInvID)
             apinv.invoiceDate     = today 
             apinv.dateReceived    = today
             apinv.amount          = 0
             apinv.dueDate         = today 
             apinv.deptID          = ''
             apinv.notes           = "AP posted by " + pRequest:uid + " on " + string(today) 
             apinv.stat            = "C"
             apinv.PONumber        = ''
             apinv.uid             = pRequest:uid
             apinv.contention      = false
             apinv.reduceLiability = false
             apinv.vendorid        = ttfiletask.vendorID
             apinv.vendorname      = ttfiletask.vendorname
             apinv.vendoraddress   = ''
             .
       
         /* creating apvoucherID */
         {lib/nextkey.i &type='apVoucher' &var=iApVoucherID &err="undo TRX-BLOCK, next TRX-BLOCK."}
         assign
             apinv.refID = string(iApVoucherID).

         validate apinv.
         release  apinv.

       end.

        
     for first filetask fields(filetaskID taskID posted apinvID) 
       where filetask.filetaskID = ttFiletask.filetaskID exclusive-lock:
    
       assign
           filetask.posted        = true          
           filetask.apinvID       = iApInvID 
           ttFiletask.posted      = true     /* updating temp-table */
           ttFiletask.apInvID     = iApInvID
           ttFiletask.apVoucherID = iApVoucherID
           .

       for first ttarcfiletask where ttarcfiletask.ID = filetask.taskID: 
         ttfiletask.completeddate = ttarcfiletask.enddatetime. /* revisit- confirm this while generating CSV should show UTC or client's local time */
       end.

       validate filetask. 
       release  filetask.

     end.
    lSuccess = true.
   end. /* for each ttfiletask */

   if not lSuccess 
    then
     do:
        presponse:fault("3000", "Finalize Vendor Tasks").
        return.
     end.

   /* Creating taskID list */
   for each ttFiletask:
  
     if ttFiletask.posted = true
      then
       pcTaskIDList = pcTaskIDList + string(ttfiletask.ID) + ",".
     else
      notPosted = notPosted + 1.
  
   end.

   /*All the errors are handled in postarctask-p.p program*/
   run ap/postarctask-p.p(input trim(pcTaskIDList, ","), output pcStatus, output pcStatusMessage, output pcFailedIdList).

   setContentTable("ttFileTask",input table ttFileTask, input pRequest:FieldList).
   if notPosted > 1
    then
     pResponse:success("2000","file task records partially posted").     
   else
    pResponse:success("2000", "Finalize Vendor Tasks").

 end method.

method public override char render(input pFormat as char, input pTemplate as char):

  define variable cVendorID      as character no-undo.
  define variable cAgentID       as character no-undo.
  define variable cFileNumber    as character no-undo.
  define variable cGeneratedCSVFilePath as character  no-undo.
  define variable iCount         as integer   no-undo.
  define variable dAssignDate    as date      no-undo.
  define variable dCompletedDate as date      no-undo.
  define variable cDateFormat    as character no-undo.
  define variable cUsername      as character no-undo.

 /* if error in act validations..sending to super class..to
  get the error table running base render in frame work */
  if temp-table ttTaskPostErr:has-records
   then
    do:     
      return super:render(pFormat, pTemplate).
    end.

  cDateFormat = session:date-format.
  session:date-format = "ymd".

  assign
      cVoucherList = ""
      cVendorList  = ""
      cFileList    = ""
      .

  for each ttfiletask where ttfiletask.posted = true break by ttfiletask.VendorID:
    assign
        cAgentID        = ""
        cFileNumber     = ""
        cGeneratedCSVFilePath = ""
        cVendorID = ttfiletask.vendorID
        iCount = iCount + 1.
        .


    if first-of(ttFiletask.vendorID)
     then
      do:
        cVendorList = cVendorList + ttfiletask.vendorID + ",".
        cVoucherList = cVoucherList + string(ttfiletask.apVoucherID) + ",".
        cGeneratedCSVFilePath = GetFileName(input ttfiletask.vendorID, input string(ttfiletask.apvoucherID)). /* gets filename for vendorID */
        cFileList = cFileList + cGeneratedCSVFilePath + ",".

        output stream str1 to value(cGeneratedCSVFilePath).
         /* Creating report header*/
        export stream str1 delimiter "," "Task" "Agent ID" "Agent Name" "File Number" "Assigned" "Assigned To" "Completed" "Price".
       
      end.

    assign
        cAgentID    = '="' + ttfiletask.agentID + '"'
        cFileNumber = '="' + ttfiletask.filenumber + '"'
        dAssignDate    = date(ttfiletask.AssignedDate)
        dCompletedDate = date(ttfiletask.completeddate)
        .

    {lib/getusername.i &var=cusername &user=ttfiletask.AssignedTo}
    session:date-format = "mdy".
    export stream str1 delimiter "," ttfiletask.topic cAgentID ttfiletask.agent cFileNumber dAssignDate cusername dCompletedDate ttfiletask.cost.

    if last-of(ttfiletask.VendorID)
     then
      do:
        output stream str1 close.
        publish "AddTempFile" from (session:first-procedure) ("FinalizeVendorTasks" + string(iCount), cGeneratedCSVFilePath).
      end.
    
  end.

  session:date-format = cDateFormat.

  cVendorList  = trim(cVendorList,",").    /* list of vendorIDs */
  cFileList    = trim(cFileList,",").     /* list of csv filenames generated */
  cvoucherList = trim(cvoucherList,","). /* voucherList contains keys  */

  return super:render(pFormat, pTemplate).  /* Getting the filetask table */
  
end.

method public override logical distribute(input pRequest as framework.IRequestable):

  define variable pMsg         as character no-undo.
  define variable std-lo       as logical   no-undo.

  for first sysdest where sysdest.action = pRequest:actionID 
                     and sysdest.entityID = " " no-lock:    
    
   /* The distribute method gets sydestinations from sysdesttable
      and according to the desttype we can call the related methods like storecontent,email..etc */
   if sysdest.destType = "S"
    then
     std-lo = storecontents(input pRequest:actionID, input pRequest:UID, output pMsg).
     
  end.

  if not std-lo  
   then return false.
  else return true.

end method.

method logical storeContents(input pActionID as character,
                             input pUID as character,
                             output pMsg as character):

  define variable cErrorPDF        as character no-undo.
  define variable cErrorCSV        as character no-undo.
  define variable iCount           as integer   no-undo.
  define variable cVendorID        as character no-undo.
  define variable cVoucherID       as character no-undo.
  define variable cVoucherFile     as character no-undo.
  define variable cStorageEntity   as character no-undo.
  define variable cStorageID       as character no-undo.
  define variable cStorageCategory as character no-undo.
  define variable lVoucherStored   as logical   no-undo.


  do iCount = 1 to num-entries(cVendorList,","):

    assign
        cVendorID    = ""
        cVoucherID   = ""
        cVoucherFile = ""
        .
  
    cvendorID    = entry(icount,cVendorList,",") no-error.
    cVoucherID   = entry(iCount,cVoucherList,",") no-error.
    cVoucherFile = entry(iCount,cFileList,",")   no-error.
   
    file-info:filename = cVoucherFile.
    if file-info:full-pathname = ?
     then
      do:
        cErrorCSV = "Voucher CSV not found for vendorID:" + cVendorID.
        run util/sysmail.p("Saving Voucher file to repository failed",
                           cErrorCSV).
       end.
    else
     do: 
       assign
            cStorageEntity = "APVoucher"
            cStorageID     = cVoucherID
            .
       run util\UploadFile.p (pUID, true, cStorageEntity, cStorageID, cStorageCategory, cVoucherFile, output lVoucherStored, output pMsg).    
       if not lVoucherStored 
        then
         do:
           pMsg = "Saving file " + cVoucherFile + " to repository failed with error " + pMsg + " for vendorID:" + cVendorID.
              run util/sysmail.p ("Saving file to repository failed",
                                  "Action:" + pActionID + "<br>" + pMsg).
         end.
   
     end.
   
  end. /* do icount = 1 */ 

  pMsg = cErrorCSV.
  if pMsg > "" 
   then return false.
  else return true.

end method.

method private character GetFileName ( input cVendorID as char,input pcRefID as char):

  define variable cTempFolder as character no-undo.
  publish "GetSystemParameter" from (session:first-procedure) ("TempFolder", output cTempFolder).

  if cTempFolder = ? or cTempFolder = ""
   then
    cTempFolder = os-getenv("TEMP").

  if cTempFolder = ? or cTempFolder = ""
   then
    cTempFolder = os-getenv("TMP").

  if cTempFolder = ? or cTempFolder = ""
   then
    cTempFolder = "d:\temp\".

  return cTempFolder + "\" + trim(cVendorID) + "-" + pcRefID + "-Payment_Remit-" + String(Year(today), "9999") + "-" + String(MONTH(today), "99") + "-" + String(DAY(today), "99") + ".csv".

end method.

method private void createErrorRecord (input pcErrorMsg as character):
  create ttTaskPostErr.
  assign 
    iErrorSeq                 = iErrorSeq + 1
    ttTaskPostErr.err         = string(iErrorSeq)
    ttTaskPostErr.description = pcErrorMsg
    .
end method.
 
end class.

