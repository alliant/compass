/*------------------------------------------------------------------------
@file GetOfficeNotes.cls
@action officeNotesGet
@description Get Office Notes
@returns Office Notes
@success 2005;X Office Note(s) were returned
@author Vignesh Rajan
@version 1.0
@created 10/30/2023
Modification:

----------------------------------------------------------------------*/

class file.GetOfficeNotes inherits framework.ActionBase:

  {lib/callsp-defs.i }
  {tt/officeNote.i &tableAlias="ttOfficeNote"}

  constructor GetOfficeNotes ():
    super().
  end constructor.

  destructor public GetOfficeNotes ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}

    define variable piOfficeID as integer   no-undo.
    define variable pcUID      as character no-undo.
    define variable piSeq      as integer   no-undo.

    define variable dsHandle as handle no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttOfficeNote:handle).

    pRequest:getParameter("OfficeID", output piOfficeID).
    pRequest:getParameter("UID", output pcUID).
    pRequest:getParameter("Seq", output piSeq).


    for each officeNote no-lock 
       where officeNote.officeID = if piOfficeID > 0 then piOfficeID else officeNote.officeID:    
      
      if pcUID > "" and pcUID <> officeNote.uid 
       then 
        next.

      if piSeq > 0 and piSeq <> officeNote.seq 
       then
        next.

      create ttOfficeNote.
      buffer-copy officeNote to ttOfficeNote.

      std-in = std-in + 1.

    end.

    if std-in > 0 
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2005", string(std-in) {&msg-add} "OfficeNote").
             
  end method.
      
end class.


