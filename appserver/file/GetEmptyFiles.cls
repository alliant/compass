/*------------------------------------------------------------------------
@file GetEmptyFiles.cls
@action emptyFilesGet
@description Get empty files Order or Tasks
@param Action;char 
@throws 3005;Invalid Action type
@throws 3005;Action is not one of: (O)rders or (T)asks.
@returns empty files Orders or Tasks
@success 2000;X empty files were returned
@author S Chandu
@version 1.0
@created 04/12/2023
Modification:
Date        Name          Comments   
08/14/2024  Shefali       Correct the logic to return "Files with an open order and no open tasks"  
----------------------------------------------------------------------*/

class file.GetEmptyFiles inherits framework.ActionBase:
    
  {lib/callsp-defs.i}
  {tt/searchagentfile.i &tableAlias="ttsearchagentfile"}
  {tt/filetask.i &tableAlias="ttFileTask"}

  constructor GetEmptyFiles ():
    super().
  end constructor.

  destructor public GetEmptyFiles ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
    define variable pcAction     as character no-undo.
    define variable iCount       as integer   no-undo.
    define variable dsHandle     as handle    no-undo.
    define variable dsOpenTasksHandle as handle no-undo.
    define variable lError       as logical   no-undo.
    define variable cErrMessage  as character no-undo.
    define variable std-in       as integer   no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttsearchagentfile:handle).

    create dataset dsOpenTasksHandle.
    dsOpenTasksHandle:serialize-name = 'data'.
    dsOpenTasksHandle:set-buffers(buffer ttFileTask:handle).

    pRequest:getParameter("Action", output pcAction). 

    /* validate action */
    if pcAction = ? or pcAction = ''
     then
      do:
        pResponse:fault("3005", " Action Type is not valid").
        return. 
      end.

    /* validate the action is one of O or T*/
    if lookup(pcAction, "O,T") = 0
     then 
      pResponse:fault("3005", "Action is not one of (O)rders or (T)asks").

    {lib\callsp.i &name=spGetEmptyFiles &load-into=ttsearchagentfile &params="input pcAction" &noCount=true &noResponse=true}
    
    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end. 

    if pcAction = "T" then
     do:
       /*Get open tasks from ARC*/
       run file/getopentasks-p.p(input '', /* UserName - for Permission */
                                 input '',           /* Assigned To */
                                 input "New,Open",
		        	          	 input '',
                                 input '',
                                 output dataset-handle dsOpenTasksHandle,
                                 output lError,
                                 output cErrMessage,
                                 output std-in).
    
       if lError 
        then
         message "GetOpentask Failed: " + cErrMessage.

       for each ttsearchagentfile:
         if can-find(first ttFileTask where ttFileTask.agentFileID = ttsearchagentfile.agentFileID) then
           delete ttsearchagentfile.
       end.
     end.

    iCount = 0.
    for each ttsearchagentfile no-lock:
      iCount = iCount + 1.
    end.

    if  can-find(first ttsearchagentfile) 
     then
      setcontentdataset(input dataset-handle dsHandle,input pRequest:FieldList).

    pResponse:success("2000", string(iCount) + ( if pcAction = "T" then " Get EmptyFiles Tasks" else " Get EmptyFiles Orders")).
          
  end method.

end class.


