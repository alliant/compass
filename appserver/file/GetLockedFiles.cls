/*------------------------------------------------------------------------
@name  GetLockedFiles.cls
@action lockedFilesGet
@description  Get the Locked Files.
@returns Success;2000
@returns Fault;3005
@author Shefali
@Modified :
Date        Name          Comments   
Shefali     11/22/2023    Modified to get the locked files dataset   
   
------------------------------------------------------------------------*/

class file.GetLockedFiles inherits framework.ActionBase:
   
 {tt/lockedfile.i}
  
  constructor GetLockedFiles ():
    super().
  end constructor.

  destructor public GetLockedFiles ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    {lib/callsp-defs.i}
    {lib/std-def.i}

    define variable lSuccess      as logical   no-undo.
    define variable cErrMessage   as character no-undo.
    define variable dsHandle      as handle    no-undo.  

    create dataset dsHandle.  
    dsHandle:serialize-name = 'data'.      
    dsHandle:set-buffers(buffer lockedfile:handle).

    {lib\callsp.i &name=spGetLockedFiles &load-into=lockedfile &noResponse=true}
    
    for each lockedfile:
      assign lockedfile.agent = lockedfile.agentName + " (" + lockedfile.agentID + ")".
    end.

    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end. 

    if can-find(first lockedfile) 
     then
      do:
        setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
        pResponse:success("2005", string(csp-iCount) {&msg-add} " Locked Files").
      end.
         
  end method.
      
end class.


