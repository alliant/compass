/*------------------------------------------------------------------------
    File        : file/edi/newfile-params.i    
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : S Chandu
    Created     : 03/08/24
    Notes       :
  ----------------------------------------------------------------------

----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
&if defined(payLoadType) = 0 &then
&scoped-define  payLoadType  Json
&endif   
      
    
&IF "{&payLoadType}" = "Json" &THEN
  define input  parameter oJsonObjectParent  as JsonObject no-undo.
  define input  parameter ipcUid             as character  no-undo.
  define input  parameter ipcAgentID         as character  no-undo.
  define output parameter opiAgentFileID     as integer    no-undo.
  define output parameter opcIndexValueList  as character  no-undo.
  define output parameter opcAddressList     as character  no-undo.
  define output parameter opcstatelist       as character  no-undo.
  define output parameter opcStateCountyList as character  no-undo.
  define output parameter oplSuccess         as logical    no-undo.
  define output parameter opcErrorMsg        as character  no-undo.
&elseif "{&payLoadType}" = "Xml" &then
  define input  parameter xDocument          as  handle    no-undo.
  define input  parameter ipcUid             as character  no-undo.
  define input  parameter ipcAgentID         as character  no-undo.
  define output parameter opiAgentFileID     as integer    no-undo.
  define output parameter opcIndexValueList  as character  no-undo.
  define output parameter opcAddressList     as character  no-undo.
  define output parameter opcstatelist       as character  no-undo.
  define output parameter opcStateCountyList as character  no-undo.
  define output parameter oplSuccess         as logical    no-undo.
  define output parameter opcErrorMsg        as character  no-undo.
&else
 &message "Invalid Payload Type ".
&endif
