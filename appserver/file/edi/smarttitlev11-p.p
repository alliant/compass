/*------------------------------------------------------------------------
@name  ordertitle-p.p
@description  Creates the order into the compass system.
@author S Chandu
@Created : 03/05/2024
@Modified :
Date        Name          Comments   
07/18/2024  K.R           Modified to create job of type search
08/09/2024  K.R           Modified to store deed related information to deed table instead of fileproperty table
09/18/24    SRK           Modified to add output in modifyfile-p
10/17/2024  SRK           Modified to change error message
----------------------------------------------------------------------*/
USING Progress.Json.ObjectModel.ObjectModelParser.
USING progress.Json.ObjectModel.*.

/* input and output parameters */
{file/edi/newfile-params.i &payLoadType="Json"}

 /* local varibales */           
define variable pcAction          as character   no-undo.
define variable cDateFormat       as character   no-undo.
define variable pcVersion         as character   no-undo.
define variable cProgramName      as character   no-undo.
define variable pcFileNumber      as character   no-undo.
define variable cAgentProductList as character   no-undo.   
define variable cFileID           as character   no-undo.
define variable cErrorMsg         as character   no-undo.
define variable pMsg              as character   no-undo.


define variable cIndexValueList  as character   no-undo.
define variable cAddressList     as character   no-undo.
define variable cStateCountyList as character   no-undo.
define variable cStateList       as character   no-undo.
define variable lNewJobCreated   as logical     no-undo.
define variable cEmailAddr       as character   no-undo.
define variable dClosingDate     as datetime-tz no-undo.
define variable iDeedSeq         as integer     no-undo initial 0.
define variable lCreateagentfile as logical     no-undo initial true.

 /* converting local time into utc datetime-tz  */ 
define variable cCreateDateTime as datetime-tz no-undo. 
define variable utcMinutes      as integer     no-undo.
define variable utcMinutesdiff  as integer     no-undo.

 /* JSON Objects */ 
define variable ojsonArray         as JsonArray  no-undo.
define variable ojsonArrayProduct  as JsonArray  no-undo.
 
 /* handle */
define variable dsHandle  as handle no-undo.

{lib/normalizefileid.i }
{lib/std-def.i}

{tt/agentfile.i &tableAlias="ttagentfile" &updateFile=true}
{tt/fileproperty.i &tableAlias="ttfileproperty" &updateFile=true}
{tt/fileParty.i   &tableAlias="ttBuyer" &updateFile=true}
{tt/fileParty.i   &tableAlias="ttSeller" &updateFile=true}
{tt/fileParty.i   &tableAlias="ttLender" &updateFile=true}
{tt/fileContent.i  &tableAlias="ttRequirement"  &updateFile=true }
{tt/fileContent.i  &tableAlias="ttException"   &updateFile=true }
{tt/fileContent.i  &tableAlias="ttInstrument"  &updateFile=true }
{tt/job.i          &tableAlias="ttSearch"      }
{tt/job.i          &tableAlias="ttPolicy"      }
{tt/deed.i         &tableAlias="ttDeed"        &updateFile=true}

create dataset dsHandle.
dsHandle:serialize-name = 'data'.
dsHandle:set-buffers(buffer ttAgentFile:handle,
                     buffer ttFileProperty:handle,
                     buffer ttBuyer:handle,
                     buffer ttSeller:handle,
                     buffer ttLender:handle,
                     buffer ttRequirement:handle,
                     buffer ttException:handle,
                     buffer ttInstrument:handle,
                     buffer ttSearch:handle,
                     buffer ttPolicy:handle,
                     buffer ttDeed:handle).

if not oJsonObjectParent:has("titleOrder")
 then
  do:
    assign
        opcErrorMsg = "Invalid Payload Json"
        oplSuccess  = false.
    return.
  end.

/* changing  session formate of date to json matching format */
cDateFormat = session:date-format.

if ojsonobjectparent:getjsonobject("titleOrder"):getjsonobject("detail"):Has("fileNumber") and not ojsonobjectparent:getjsonobject("titleOrder"):getjsonobject("detail"):isnull("fileNumber") 
 then
  do:
    create ttagentfile.
    assign
        pcFileNumber  = ojsonobjectparent:getjsonobject("titleOrder"):getjsonobject("detail"):getcharacter("fileNumber")
        .
  end.

cFileID = normalizeFileID(pcFileNumber).
oplSuccess = true. 
  /* Allow only active or prospect agents */
for first agent fields (stat) where agent.agentID = ipcAgentID no-lock:
  if agent.stat = 'A' or agent.stat = 'P'  
   then.
   else
    do:
      opcErrorMsg = "Agent should be active or prospect.".
      oplSuccess = false.
    end.
end.

if can-find (first agentfile where agentfile.agentID = ipcAgentID and agentfile.fileID = cFileID)
 then
  do: 
 	for first agentfile fields (agentfileid) where agentfile.agentID = ipcAgentID and agentfile.fileID = cFileID no-lock:
 	  assign
 	      oplSuccess = false
 	      opcErrorMsg = opcErrorMsg + "," + substitute("Agent File already exists with Agent ID &1 and File Number &2", string(ipcAgentID),string(cFileID)).
 	end.
  end.
  
if oplSuccess
 then                                           /* newfile record creation */
  run file/createagentfile-p.p(input  ipcAgentID,    /* AgentID */
                               input  pcFileNumber, /* FileNumber */
                               input  ipcUid,                              
                               output opiAgentFileID,
                               output oplSuccess,
                               output cErrorMsg). 

if not oplSuccess
 then
  do:
    if cErrorMsg ne "" and cErrorMsg ne "?" 
     then
      opcErrorMsg =  opcErrorMsg + "," + cErrorMsg.

    return.
  end.

ttagentfile.agentfileID = opiAgentFileID.

session:date-format = "ymd".
if ojsonobjectparent:getjsonobject("titleOrder"):getjsonobject("detail"):Has("dates") and not ojsonobjectparent:getjsonobject("titleOrder"):getjsonobject("detail"):isnull("dates") 
 then
  do:
    oJsonArray = ojsonobjectparent:getjsonobject("titleOrder"):getjsonobject("detail"):getJsonarray("dates").
     
    if oJsonArray:length >= 3
     then
      do:
        /* to pick time zone */
        assign
            dClosingDate = datetime-tz(oJsonArray:getjsonobject(3):getcharacter("Value"))
            utcMinutes      = timezone(dClosingDate)
            utcMinutesDiff  = int(- utcminutes) /* converted to utc required format */
            .
     
         /* converting into Utc fromat and setting offset(+00:00) UTC */
         dClosingDate = datetime-tz(dClosingDate,utcminutesdiff).
         dClosingDate = datetime-tz(dClosingDate,0).
      
         ttagentfile.closingDate = date(dClosingDate).
     end.
  end.
session:date-format = cDateFormat.

    /* properties */
if ojsonobjectparent:getjsonobject("titleOrder"):Has("properties") and not ojsonobjectparent:getjsonobject("titleOrder"):isnull("properties") 
 then
  do:
    
    oJsonArray  = ojsonobjectparent:getjsonobject("titleOrder"):getJsonarray("properties").

    do std-in = 1 to oJsonArray:length:

      create ttfileproperty.
      ttfileproperty.agentFileID = opiAgentFileID.

      if oJsonArray:getjsonobject(std-in):Has("propertyType") and not oJsonArray:getjsonobject(std-in):isnull("propertyType")
       then
        ttfileproperty.propertyType  = oJsonArray:getjsonobject(std-in):getcharacter("propertyType").

      if oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):Has("parcel") and not oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):isnull("parcel")
       then
        ttfileproperty.parcelid         = oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):getcharacter("parcel").

      if oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):Has("block") and not oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):isnull("block")
       then
        ttfileproperty.block            = oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):getcharacter("block").

      if oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):Has("lot") and not oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):isnull("lot")
       then
        ttfileproperty.lot              = oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):getcharacter("lot").

      if oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):Has("instrNum") and not oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):isnull("instrNum")
       then
        ttfileproperty.instrumentNumber = oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):getcharacter("instrNum").

      if oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):Has("range") and not oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):isnull("range")
       then
        ttfileproperty.township         = oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):getcharacter("range").

      if oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):Has("township") and not oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):isnull("township")
       then
        ttfileproperty.township         = oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):getcharacter("township").

      if oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):Has("section") and not oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):isnull("section")
       then
            ttfileproperty.section          = oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):getcharacter("section").
    
       if oJsonArray:getjsonobject(std-in):Has("legalDescription") and not oJsonArray:getjsonobject(std-in):isnull("legalDescription")
        then
         ttfileproperty.shortlegal      = oJsonArray:getjsonobject(std-in):getcharacter("legalDescription").
    
       if oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):Has("subDivName") and not oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):isnull("subDivName")
        then
         ttfileproperty.subdivision     = oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):getcharacter("subDivName").
    
       if oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):Has("unit") and not oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):isnull("unit")
        then
         ttfileproperty.unitno          = oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):getcharacter("unit").
    
       if oJsonArray:getjsonobject(std-in):Has("address") and not oJsonArray:getjsonobject(std-in):isnull("address")
         then
          do:
            if oJsonArray:getjsonobject(std-in):getjsonobject("address"):Has("line1") and not oJsonArray:getjsonobject(std-in):getjsonobject("address"):isnull("line1")
             then
              ttfileproperty.addr1          = oJsonArray:getjsonobject(std-in):getjsonobject("address"):getcharacter("line1").
            if oJsonArray:getjsonobject(std-in):getjsonobject("address"):Has("city") and not oJsonArray:getjsonobject(std-in):getjsonobject("address"):isnull("city")
             then
              ttfileproperty.city          = oJsonArray:getjsonobject(std-in):getjsonobject("address"):getcharacter("city").
            if oJsonArray:getjsonobject(std-in):getjsonobject("address"):Has("state") and not oJsonArray:getjsonobject(std-in):getjsonobject("address"):isnull("state")
             then
              ttfileproperty.stateID          = oJsonArray:getjsonobject(std-in):getjsonobject("address"):getcharacter("state").
            if oJsonArray:getjsonobject(std-in):getjsonobject("address"):Has("zip") and not oJsonArray:getjsonobject(std-in):getjsonobject("address"):isnull("zip")
             then
              ttfileproperty.zip          = oJsonArray:getjsonobject(std-in):getjsonobject("address"):getcharacter("zip").
          end.
     
       if oJsonArray:getjsonobject(std-in):getjsonobject("address"):Has("county") and not oJsonArray:getjsonobject(std-in):getjsonobject("address"):isnull("county")
        then
         do:
           if oJsonArray:getjsonobject(std-in):getjsonobject("address"):getjsonobject("county"):Has("countycode") and not oJsonArray:getjsonobject(std-in):getjsonobject("address"):getjsonobject("county"):isnull("countycode")
            then
             ttfileproperty.countyID          = string(oJsonArray:getjsonobject(std-in):getjsonobject("address"):getjsonobject("county"):getinteger("countycode")).
       
         end.

       create ttDeed.
       assign
        iDeedSeq           = iDeedSeq + 1
        ttDeed.agentFileId = opiAgentFileID
        ttDeed.Seq         = iDeedSeq
        .
      if oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):Has("page") and not oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):isnull("page")
       then
        ttDeed.pageNumber       = oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):getcharacter("page").

      if oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):Has("book") and not oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):isnull("book")
       then
        ttDeed.book       = oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):getcharacter("book").

   end. /* do end */   
 end. /* if end */
    
  /*  fileparties  */
if ojsonobjectparent:getjsonobject("titleOrder"):getjsonobject("transactionParties"):Has("sellers") and not ojsonobjectparent:getjsonobject("titleOrder"):getjsonobject("transactionParties"):isnull("sellers")
 then
  do:
 
    oJsonArray = ojsonobjectparent:getjsonobject("titleOrder"):getjsonobject("transactionParties"):getJsonarray("sellers").
    
    do std-in = 1 to oJsonArray:length:

    create ttSeller.
    assign
        ttSeller.agentFileID = opiAgentFileID
        .

    if oJsonArray:getjsonobject(std-in):Has("attributes") and not oJsonArray:getjsonobject(std-in):isnull("attributes")
     then
      do:    
        if oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):Has("formalName") and not oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):isnull("formalName") 
         then
          ttSeller.name    = oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):getcharacter("formalName").
      end.
    
    if oJsonArray:getjsonobject(std-in):Has("currentAddress") and not oJsonArray:getjsonobject(std-in):isnull("currentAddress") 
     then
      do:
        if oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):Has("city") and not oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):isnull("city")
         then
          ttSeller.city    = oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):getcharacter("city").
      
        if oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):Has("state") and not oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):isnull("state")
         then
          ttSeller.state    = oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):getcharacter("state").
      
        if oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):Has("zip") and not oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):isnull("zip")
         then
          ttSeller.zip    = oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):getcharacter("zip").
      
        if oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):Has("line1") and not oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):isnull("line1") 
         then
          ttSeller.addr1    = oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):getcharacter("line1").
      
     end.

    if oJsonArray:getjsonobject(std-in):Has("relationship")  and not oJsonArray:getjsonobject(std-in):isnull("relationship")
     then
      do:
        if oJsonArray:getjsonobject(std-in):getcharacter("relationship") = "Wife" or oJsonArray:getjsonobject(std-in):getcharacter("relationship") = "Husband"
         then
          ttSeller.spouse = true.
        else
          ttSeller.spouse = false.
      end.
      
    end.
end.


if ojsonobjectparent:getjsonobject("titleOrder"):getjsonobject("transactionParties"):Has("buyers") and not ojsonobjectparent:getjsonobject("titleOrder"):getjsonobject("transactionParties"):isnull("buyers") 
 then
  do:
    oJsonArray = ojsonobjectparent:getjsonobject("titleOrder"):getjsonobject("transactionParties"):getJsonarray("buyers").

    do std-in = 1 to oJsonArray:length:
    create ttBuyer.
    assign
        ttBuyer.agentFileID = opiAgentFileID .

    if oJsonArray:getjsonobject(std-in):Has("attributes") and not oJsonArray:getjsonobject(std-in):isnull("attributes")
    then
     do:
       if oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):Has("formalName") and not oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):isnull("formalName") 
        then
         ttBuyer.name    = oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):getcharacter("formalName").
     end.

   if oJsonArray:getjsonobject(std-in):Has("currentAddress") and not oJsonArray:getjsonobject(std-in):isnull("currentAddress")
    then
     do:
       if oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):Has("city") and not oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):isnull("city")
        then
         ttBuyer.city    = oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):getcharacter("city").
       if oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):Has("state") and not oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):isnull("state") 
        then
         ttBuyer.state    = oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):getcharacter("state").
       if oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):Has("zip") and not oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):isnull("zip")
        then
         ttBuyer.zip    = oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):getcharacter("zip").
       if oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):Has("line1") and not oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):isnull("line1")
        then
         ttBuyer.addr1    = oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):getcharacter("line1").
    end.

     if oJsonArray:getjsonobject(std-in):Has("relationship") and not oJsonArray:getjsonobject(std-in):isnull("relationship")
      then
       do:
         if oJsonArray:getjsonobject(std-in):getcharacter("relationship") = "Wife" or oJsonArray:getjsonobject(std-in):getcharacter("relationship") = "Husband"
          then
           ttBuyer.spouse = true.
         else
           ttBuyer.spouse = false.
       end.

    end.
  end.

if ojsonobjectparent:getjsonobject("titleOrder"):getjsonobject("transactionParties"):Has("lenders") and not ojsonobjectparent:getjsonobject("titleOrder"):getjsonobject("transactionParties"):isnull("lenders")
 then
  do:
    oJsonArray = ojsonobjectparent:getjsonobject("titleOrder"):getjsonobject("transactionParties"):getJsonarray("lenders").

    do std-in = 1 to oJsonArray:length:
    create ttLender.
    assign
        ttLender.agentFileID = opiAgentFileID 
        .
    if oJsonArray:getjsonobject(std-in):Has("attributes") and not oJsonArray:getjsonobject(std-in):isnull("attributes")
     then
      do:
        if oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):Has("formalName") and not oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):isnull("formalName") 
         then
          ttLender.name    = oJsonArray:getjsonobject(std-in):getjsonobject("attributes"):getcharacter("formalName").
      end.

   if oJsonArray:getjsonobject(std-in):Has("currentAddress") and not oJsonArray:getjsonobject(std-in):isnull("attributes")
    then
     do:
       if oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):Has("city") and not oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):isnull("city") 
        then
         ttLender.city    = oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):getcharacter("city").
       if oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):Has("state") and not oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):isnull("state") 
        then
         ttLender.state    = oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):getcharacter("state").
       if oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):Has("zip") and not oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):isnull("zip")
        then
         ttLender.zip    = oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):getcharacter("zip").
       if oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):Has("line1")and not oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):isnull("line1") 
        then
         ttLender.addr1    = oJsonArray:getjsonobject(std-in):getjsonobject("currentAddress"):getcharacter("line1").
     end.

    end.
  end.


  /* job */
if ojsonobjectparent:getjsonobject("titleOrder"):Has("products") and not ojsonobjectparent:getjsonobject("titleOrder"):isnull("products")
 then
  do:
    ojsonArrayProduct = ojsonobjectparent:getjsonobject("titleOrder"):getJsonarray("products").   

    do std-in = 1 to ojsonArrayProduct:length:
     if ojsonArrayProduct:getjsonobject(std-in):Has("description") and not ojsonArrayProduct:getjsonobject(std-in):isnull("description")
      then
       cAgentProductList = cAgentProductList + ojsonArrayProduct:getjsonobject(std-in):getcharacter("description") + ",".
    end.

    if oJsonArrayProduct:length >= 1 
     then
      do:
        create ttSearch.
        ttSearch.agentFileID = opiAgentFileID.

        cDateFormat = session:date-format.
        session:date-format = "ymd".
        oJsonArray = ojsonobjectparent:getjsonobject("titleOrder"):getjsonobject("detail"):getJsonarray("dates").
    
        if oJsonArray:getjsonobject(1):getcharacter("Type") = "Due"
         then
          do:
               /* to pick time zone */
            assign
                cCreateDateTime = datetime-tz(oJsonArray:getjsonobject(1):getcharacter("Value"))
                utcMinutes      = timezone(cCreateDateTime)
                utcMinutesDiff  = int(- utcminutes) /* converted to utc required format */
                .

            /* converting into Utc fromat and setting offset(+00:00) UTC */
            cCreateDateTime = datetime-tz(cCreateDateTime,utcminutesdiff).
            cCreateDateTime = datetime-tz(cCreateDateTime,0).
         
           ttSearch.duedate = date(oJsonArray:getjsonobject(1):getcharacter("Value")).
           
          end.
        session:date-format = cDateFormat.

        if ojsonArrayProduct:getjsonobject(1):Has("description") and not ojsonArrayProduct:getjsonobject(1):isnull("description")
         then
          ttSearch.productname = ojsonArrayProduct:getjsonobject(1):getcharacter("description").
     

        for first product no-lock where product.description = ttSearch.productname:
          assign
            ttSearch.productID = product.productID
          .           
        end.
    
        if ojsonobjectparent:Has("submittingUser") and not ojsonobjectparent:isnull("submittingUser")
         then
          do:
            if ojsonobjectparent:Has("name") and not ojsonobjectparent:isnull("name")
             then
              ttSearch.contactname  = ojsonobjectparent:getjsonobject("submittingUser"):getcharacter("name").
         
            if ojsonobjectparent:Has("emailAddress") and not ojsonobjectparent:isnull("emailAddress")
             then
              ttSearch.contactEMail = ojsonobjectparent:getjsonobject("submittingUser"):getcharacter("emailAddress").

          end.

        oJsonArray = ojsonobjectparent:getjsonobject("titleOrder"):getJsonarray("notes"). 

        if oJsonArray:getjsonobject(1):Has("message") and not oJsonArray:getjsonobject(1):isnull("message")
         then
          ttSearch.notes = oJsonArray:getjsonobject(1):getcharacter("message").

        ttSearch.AgentProduct = trim(cAgentProductList, ",").
      end.
  end.

error-status:error = false.
oplSuccess = false.
run file/modifyfile-p.p (input opiAgentFileID,
                         input ipcUid,
                         input dataset-handle dsHandle,
                         output cIndexValueList,
                         output cAddressList,
                         output cStateCountyList,
                         output cStateList,
                         output lNewJobCreated,
                         output oplSuccess,
                         output cErrorMsg).

if not oplSuccess 
 then
  do:
    
    if error-status:error 
     then cErrorMsg = cErrorMsg + "," + error-status:get-message(1).
   
    if cErrorMsg ne "" and cErrorMsg ne "?" 
     then
       opcErrorMsg = if opcErrorMsg <> " " then opcErrorMsg + "," else '' + trim(cErrorMsg,",").

  end.

delete object ojsonArrayProduct.
delete object ojsonArray.
oplSuccess = true.



