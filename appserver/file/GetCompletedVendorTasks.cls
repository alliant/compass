/*------------------------------------------------------------------------
@name  file/GetCompletedVendorTasks.cls
@action CompletedVendorTasksGet
@description Returns tasks completed by Vendors
@parameters cScenario;  'P' - Get records for (P)osting the vendor specific task records
                        'R' - Get records for the (R)eport
                        'C' - Get records for task (C)ost update utility
                        'F' - Get records to (F)inalize zero cost vendor tasks
@returns The temp-table having completed tasks
@fault 
@success 2005;char;The count of the records in data set

@author Sachin
@version 1.0
@created 06/20/2023

@Modified    :
    Date        Name                 Comments   
    07/10/23    K.R                  Modified to add permission check
    07/20/23    SK                   Modified to add posted field value
    07/25/23    S Chandu             Modified date validations.
----------------------------------------------------------------------*/

class file.GetCompletedVendorTasks inherits framework.ActionBase:

 constructor public GetCompletedVendorTasks ():
  super().
 end constructor.

 destructor public GetCompletedVendorTasks ():
 end destructor.
    

 {tt/filetask.i &tableAlias=ttFileTask}
 {tt/consolidatefileTask.i}
 {lib/removeNullValues.i}


 method public override void act (input pRequest  as framework.IRequestable,
                                  input pResponse as framework.IRespondable):
    define variable dtStartdate       as datetime  no-undo.
    define variable dtEnddate         as datetime  no-undo.
    define variable cVendorID         as character no-undo.
    define variable cScenario         as character no-undo.

    define variable cUsername         as character no-undo.
    define variable lLocked           as logical   no-undo.
    define variable icount            as integer  no-undo.   
    define variable dsum              as decimal  no-undo.

    define variable lError            as logical   no-undo.
    define variable cErrMessage       as character no-undo.
    
    define variable dsHandle          as handle    no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttFileTask:handle,
                         buffer consolidatefileTask:handle). 

    {lib/std-def.i}
    
    pRequest:getParameter("VendorID",         output cVendorID).
    pRequest:getParameter("StartDate",        output dtStartdate).
    pRequest:getParameter("EndDate",          output dtEnddate). 
    pRequest:getParameter("Scenario",         output cScenario).
    
    if cVendorID = "" or cVendorID = ?
     then
      cVendorID = "ALL".


    for first sysuser where sysuser.uid = pRequest:uid no-lock:
      if cVendorID = "ALL"
       then
        do:

           if sysuser.vendorID <> '' and sysuser.vendorID <> ? 
            then cVendorID = sysuser.vendorID.

        end.
      else if (sysuser.vendorID <> ? and sysuser.vendorID <> '') and cVendorID <> sysuser.vendorID 
       then
        do:
          pResponse:fault("3005","You cannot view tasks for " + cVendorID).
          return.
        end.
    end.
    
    /* 'P' - Get records for (P)osting the vendor specific task records
       'C' - Get records for task (C)ost update utility
       'F' - Get records to (F)inalize zero cost vendor tasks 
    */ 
    if cScenario = 'P' or cScenario = 'C' or cScenario = 'F'
     then
      do:
        run file/getscreenlock-p.p (input  cScenario,
                               input  pRequest:Uid,
                               output lError,
                               output cErrMessage).
        if lError 
         then
          do:
            pResponse:fault ("3005", cErrMessage).
            return.
          end.
      end.
    
   
   if dtEnddate = ?
    then
     dtEnddate = now. 
  
    run file/getclosedtasks-p.p(input "",            /* UserName */
                                input "",            /* AssignedTo */
                                input "Closed",     /* Status = complete */
	  						    input "",            /* Queue */
                                input "",           /* Task */
							    input dtStartdate,
							    input dtEnddate,
                                input false,     
                                output dataset-handle dsHandle,
                                output lError,
                                output cErrMessage,
                                output std-in).

    if lError 
     then
      do:
        pResponse:fault ("3005",  "GetClosedtasks Failed: " + cErrMessage).
        return.
      end.
    
    std-in = 0.
 
    /* get records for the (R)eport    OR   
       get records for task (C)ost update utility */
    if cScenario = 'R'    or
       cScenario = 'C'     
     then
        for each ttFileTask:
          if can-find (first sysuser where sysuser.uid = ttFileTask.AssignedTo and sysuser.vendorID ne ? and sysuser.vendorID ne '') and
             can-find (first filetask where filetask.taskID = ttfiletask.ID and (filetask.posted = ? or filetask.posted = false))
           then
            for first sysuser fields (uid vendorID) no-lock where sysuser.uid = ttFileTask.AssignedTo:
              if (cVendorID = "ALL" or sysuser.vendorID = cVendorID) and 
                  can-find(first vendor where vendor.vendorid = sysuser.vendorid) /* validating vendorID */
               then
                do:
                  ttFileTask.vendorID = sysuser.vendorID.
          
                  for first vendor fields (vendorname) no-lock where vendor.vendorid = ttFileTask.vendorID:
                    ttFileTask.vendorname = vendor.vendorName.
                  end.
          
                  for first filetask fields(cost) no-lock where filetask.taskID = ttFiletask.ID:
                    ttFiletask.cost = filetask.cost.
                  end.
          
                  std-in = std-in + 1.
                end.          
               else
                delete ttFileTask.
            end.
           else
            delete ttFileTask.        
        end.

    /* get records to (F)inalize zero cost vendor tasks */
     else if cScenario = 'F'
      then
     do:

       for each ttFileTask:
        if ttFiletask.cost = 0.00   and
           can-find (first filetask where filetask.taskID = ttfiletask.ID and (filetask.posted = ? or filetask.posted = false) ) and
           can-find(first sysuser where sysuser.uid = ttFileTask.AssignedTo) /* Validating sysuser */
         then
          for first sysuser fields (vendorID) no-lock where sysuser.uid = ttFileTask.AssignedTo:
            if sysuser.vendorid <> ? and sysuser.vendorid <> '' and 
               can-find(first vendor where vendor.vendorid = sysuser.vendorid)  /* Validating vendor */
             then
              do:
                ttFileTask.vendorID = sysuser.vendorid.

                for first vendor fields (vendorname) no-lock where vendor.vendorid = ttFileTask.vendorID:
                  ttFileTask.vendorname = vendor.vendorName.
                end.

                std-in = std-in + 1.
              end.             
             else
              delete ttFileTask.             
          end.
         else
          delete ttFileTask.
          
       end.

     end.
       

    /* Get records for (P)osting the vendor specific task records */
    else if cScenario = 'P'   
     then
      do:
        /* AG: double check the record in Compass. show to client only if its NOT posted on compass as well. */
        ARC_Loop:
        for each ttFileTask:

          if can-find (first sysuser where sysuser.uid = ttFileTask.AssignedTo and sysuser.vendorID ne ? and sysuser.vendorID ne '') and
             can-find (first filetask where filetask.taskID = ttfiletask.ID and (filetask.posted = ? or filetask.posted = false))
           then
            for first sysuser fields (uid vendorID) no-lock where sysuser.uid = ttFileTask.AssignedTo:
              if can-find(first vendor where vendor.vendorid = sysuser.vendorid) /* validating vendorID */
               then
                do:
                  ttFileTask.vendorID = sysuser.vendorID.

                  for first vendor fields (vendorname) no-lock where vendor.vendorid = ttFileTask.vendorID:
                    ttFileTask.vendorname = vendor.vendorName.
                  end.

                  for first filetask where filetask.taskID = ttfiletask.ID no-lock:
                   if filetask.posted and not ttFiletask.posted
                     then
                      do:    
                        delete ttFileTask.
                        next ARC_Loop.
                      end.
                  end.

                  for first filetask fields(cost) no-lock where filetask.taskID = ttFiletask.ID:
                    if Filetask.cost = 0 or Filetask.cost = ?
                     then
                      do:    
                         delete ttFileTask.
                         next ARC_Loop.
                      end.
                    else
                     ttFiletask.cost = filetask.cost.
                  end.

                for first agent where agent.agentID = ttfiletask.agentID no-lock:
                  ttFiletask.stateID   = agent.stateID.
                end.

                for first topicExpense where topicExpense.stateID = ttFiletask.stateID and
                                             topicExpense.topic   = ttFileTask.topic no-lock:
                  ttFiletask.glaccount = topicExpense.GLAccount.
                end.

                  std-in = std-in + 1.
                end.          
               else
                delete ttFileTask.
            end.
           else
            delete ttFileTask.

        end.

    

        for each ttFileTask group by ttFileTask.stateID by ttFileTask.vendorID by ttFileTask.topic by ttFileTask.glaccount:
          if first-of(ttFileTask.glaccount)
           then
            do: 
              assign  
                icount = 0
                dsum   = 0.

              create consolidatefileTask.
              assign
                  consolidatefiletask.stateid    = ttFileTask.stateID
                  consolidatefiletask.vendor     = ttFileTask.vendorID	
                  consolidatefiletask.vendorName = ttFileTask.vendorname
                  consolidatefiletask.topic      = ttFileTask.topic
                consolidatefiletask.glaccount  = ttFileTask.glaccount
                  consolidatefiletask.glaccountDesc = ttFileTask.glaccountDesc
                .
            end.

          assign 
              icount  = icount + 1
              dsum    = dsum + ttFileTask.cost.

          if last-of(ttFileTask.glaccount)
           then
            assign 
                 consolidatefiletask.filetaskcount = icount
                 consolidatefiletask.costSum       = dsum
                 .

        end.

    end.
      
    if std-in > 0
     then
      setContentDataset(input dataset-handle dsHandle, pRequest:FieldLIst).
    
    pResponse:success("2005", string(std-in) {&msg-add} "completed task").
 end method.
 
end class.

