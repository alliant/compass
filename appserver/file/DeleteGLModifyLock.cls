/*------------------------------------------------------------------------
@file DeleteGLModifyLock.p
@action glModifyLockDelete 
@description Delete GL Modify

@author SRK
@version 1.0
@created 09/02/2024
Modification:

----------------------------------------------------------------------*/

class file.DeleteGLModifyLock inherits framework.ActionBase:
   

  constructor DeleteGLModifyLock ():
    super().
  end constructor.

  destructor public DeleteGLModifyLock ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable pSuccess  as logical no-undo.

    TRX-BLOCK:
    for first syslock exclusive-lock
      where   syslock.entityType = "GLModify" TRANSACTION
        on error undo TRX-BLOCK, leave TRX-BLOCK:
      delete syslock.
      release syslock.
      pSuccess = true.
    end.


    if not pSuccess
     then
      do: 
        pResponse:fault("3000", "GLModify Unlock").
        return.
      end.
    
    pResponse:success("2000", "GLModify Unlock").
             
  end method.
      
end class.


