/*------------------------------------------------------------------------
@name  file/HoldFile.cls
@action fileHold
@description  Create a  File Hold.
@param AgentFileID;int
@throws 3001;Invalid agentFileID
@throws 3066;agent File does not exist 
@throws 3005;Agent file is not locked by current user
@throws 3005;Job record is not exist for an agent file.
@throws 3005;Jobqueue record is not exist for an agent file.
@throws 3005;Agent file is on hold.  
@throws 3000;Agent File hold failed 
@returns Success;2000;Agent File hold was successful.
@author Shefali 
@Modified :
Date        Name          Comments   
   
----------------------------------------------------------------------*/

class file.HoldFile inherits framework.ActionBase:

  {tt/holdfilenote.i}

  constructor public HoldFile ():
    super().
  end constructor.

  destructor public HoldFile ():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}

    define variable lSuccess      as logical   no-undo.
    define variable iAgentFileID  as integer   no-undo. 
    define variable dsHandle      as handle    no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer holdFileNote:handle).
    
    pRequest:getParameter("agentFileID", output iAgentFileID).

    if iAgentFileID = ?
     then
      iAgentFileID = 0.

    /* get Dataset */
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.

    /* Validation when no parameter is supplied */
    if (iAgentFileID = 0  or iAgentFileID = ?)
     then 
      do:
        pResponse:fault("3001", "AgentfileID").
        return.
      end.
    
    /* validate agentfileID */
    if not can-find(first agentFile where agentFile.agentFileID = iAgentFileID ) 
     then 
      do:
        pResponse:fault("3066", "Agent File ").
        return.
      end.
    
    /* when agent file is not locked by current user then return fault. */
    {lib/islocked.i &obj='AgentFile' &id=iAgentFileID &exclude-fault=true}
    if std-lo and (std-ch <> pRequest:Uid)
     then 
      do:
        pResponse:fault("3005", "Agent file is not locked by current user.").
        return. 
      end.
      
    if not can-find(first job where job.agentfileID = iAgentFileID)
     then
      do:                                                                                               
        pResponse:fault("3005", "Job record does not exist for the agent file.").                                             
        return.                                                                                        
      end.
      
    /* Agent File must not be on hold  */
    if can-find(first agentfile where agentfile.agentFileID = iAgentFileID and
                                      agentfile.isHold      = true) 
     then                                                                                               
      do:                                                                                               
        pResponse:fault("3005", "Agent file is on hold.").                                             
        return.                                                                                        
      end.                                                                                           
    
    lSuccess = false.
    
    TRX-BLOCK:
    for first agentFile exclusive-lock
      where agentFile.agentFileID = iAgentFileID
      on error undo TRX-BLOCK, leave TRX-BLOCK:
    
      assign
           agentFile.isHold           = yes
           agentFile.holdBy           = pRequest:Uid
           agentFile.holdDate         = now
           agentFile.lastModifiedDate = now
           agentFile.lastModifiedBy   = pRequest:Uid
           .

      find first holdFileNote no-error.
      if available holdFileNote 
       then
        agentFile.holdDescription  = holdFileNote.note.
       
      validate agentFile.
      release agentFile.
    
      /*jobtran record creation*/
      run file/createjobtran-p.p (input  iAgentFileID,           /* Agent File ID */
                                  input  pRequest:actionID,      /* Action        */
                                  input  pRequest:Uid,           /* Created By    */
                                  input  'agentFileID='+ string(iAgentFileID),   /* Query String  */
                                  input  dataset-handle dsHandle /* Payload       */
                                  ).
    
      lSuccess = true.
    end. 
    
    if not lSuccess
     then 
      do:
        presponse:fault("3000", "Agent File hold").
        return.
      end.
    
    pResponse:success("2000", "Agent File hold").   
  end method.
 
end class.

