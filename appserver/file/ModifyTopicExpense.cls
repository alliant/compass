/*------------------------------------------------------------------------
@name file.ModifyTopicExpense.cls
@action  topicExpenseModify
@description  Modify a topicexpense record.
@param name, Temp-Table tttopicexpense dataset
@throws 3005;Topicexpense record not found.
@throws 3005;Task cannot be blank/Invalid value.
@throws 3005;Data Parsing failed.
@throws 3005;GL account must exist in GP GL00105 Table.
@author sagar K
@version 1.0
@created 07/31/2023
@Modified :
Date        Name          Comments  
----------------------------------------------------------------------*/

class file.ModifyTopicExpense inherits framework.ActionBase:

  {tt/topicexpense.i &tableAlias="tttopicexpense"}

  constructor ModifyTopicExpense ():
    super().
  end constructor.

  destructor public ModifyTopicExpense ():
  end destructor.
 

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     
    
	define variable pcTopic        as character no-undo.
    define variable lSuccess       as logical   no-undo.
    define variable dsHandle       as handle    no-undo.
    define variable lError         as logical   no-undo.
    define variable cErrMessage    as character no-undo.
    define variable cUsername      as character no-undo.
    define variable lLocked        as logical   no-undo.
       
    {lib/std-def.i}

    /* dataset creation */
    create dataset  dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer tttopicexpense:handle).

    pRequest:getParameter("Topic", output pcTopic). 
    
    /* validation when no parameter is supplied */
    if (pcTopic = ''  or pcTopic = ?)
     then 
      do:
        pResponse:fault("3005", "Task cannot be blank/Invalid value").
        return.
      end.

     empty temp-table tttopicexpense.
    
    /* get Dataset */
    pRequest:getContent(input-output dataset-handle dsHandle, output cErrMessage).
    if cErrMessage > '' 
     then
      do:
        pResponse:fault('3005', "Data Parsing failed. Reason: " + cErrMessage).
        return.
      end.

     if not can-find(first tttopicexpense) 
     then
      do:
        pResponse:fault('3005', 'TopicExpense record not provided').
        return.
      end.
      
    run file/getscreenlock-p.p (input 'M',  /* scenario = (M)odify GL Account*/
                           input pRequest:Uid,
                           output lError,
                           output cErrMessage).
    if lError 
     then
      do:
        pResponse:fault ("3005", cErrMessage).
        return.
      end.

    lSuccess = false.

    
    TRX-BLOCK:
    do transaction
      on error undo TRX-BLOCK, leave TRX-BLOCK:
	  
	  for first tttopicexpense no-lock:
        for first topicexpense where topicexpense.stateID = tttopicexpense.stateID and topicexpense.topic = tttopicexpense.topic exclusive-lock:
          assign
            topicexpense.GLAccount        = tttopicexpense.GLAccount
            topicexpense.GLAccountDesc    = tttopicexpense.GLAccountDesc
            topicexpense.lastmodifieddate = now
            topicexpense.lastmodifiedby   = pRequest:uid.

          validate topicexpense. 
          release  topicexpense.
          lSuccess = true.
           
        end.
      end.
    end.
    
    if not lSuccess
     then
      do:
        /* If not success, release current screen lock also */
        run util/deletesyslock.p ("GLModify", "", 0, output std-lo).
        if not std-lo
         then
          message "ModifyGL lock release failed. Please contact System administrator.".

        pResponse:fault("3000", "TopicExpense Update").
        return.
      end. 

    pResponse:success("3005"," TopicExpense update was successful").  /* TopicExpense has been updated*/
         
  end method.
      
end class.





