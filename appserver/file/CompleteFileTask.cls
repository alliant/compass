/*------------------------------------------------------------------------
@name  CompleteFileTask.cls
@action fileTaskComplete
@description  close the task
@param fileTaskID;int
 

@returns ; success/failure returned from completefiletask-p.p

@author   sagar k
@Date     04/25/2023
@Modified :
Date        Name          Comments   
05/30/2023  SB            Modify to add rule - Only tasks assigned to you 
                          and those that are not assigned to anyone can be completed.
07/21/2023  SK            Modified to add Posted field  
12/07/2023  S Chandu      Added functionality to send mail on completing a task.
02/28/2024  K.R           Modified to add validation if task is already completed
05/07/2024  K.R           Modified to change the error code
08/21/2024  SRK           Modified to add taskid as input parameter.
09/19/2024  SRK           Modified to check close task from ARC
11/04/2024  Vignesh R     Modified to return the updated record. 
12/23/2024  SRK           Modified to remover return record
----------------------------------------------------------------------*/

class file.CompleteFileTask inherits framework.ActionBase:

  {tt/filetask.i &tableAlias=ttfiletask}
  {tt/mailcompletetask.i    &tableAlias="MailCompleteTask"}

  constructor public CompleteFileTask ():
    super().
  end constructor.

  destructor public CompleteFileTask ( ):
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}

    define variable piFileTaskID   as integer   no-undo.
    define variable piTaskID       as integer   no-undo.
    define variable plReturnRecord as logical   no-undo.
    define variable iErrCode       as integer   no-undo.
    define variable cErrMessage    as character no-undo.
    define variable iTaskID        as integer   no-undo.
    define variable dsHandle       as handle    no-undo.
    define variable cAgentFileID   as character no-undo.
    define variable lClosedinARC   as logical   no-undo.

    define variable lError         as logical   no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttfiletask:handle).
   
    pRequest:getParameter("FileTaskID",   output piFileTaskID). 
    pRequest:getParameter("TaskID",       output piTaskID).
    pRequest:getParameter("returnRecord", output plReturnRecord).


     /* validate fileTaskID */
    if (piFileTaskID = 0 or piFileTaskID = ?) and (piTaskID = 0 or piTaskID = ? )
     then
      do:
         pResponse:fault('3005', 'FiletaskID or TaskID is required.'). 
        return.
      end.

    if plReturnRecord = ?
     then 
      plReturnRecord = false.
    
    if (piFileTaskID > 0) 
     then
      do:
        if not can-find(first filetask where filetask.fileTaskID = piFileTaskID ) 
         then 
          do:
            pResponse:fault('3001', 'FileTaskID').
            return.
          end.

        if can-find(first filetask where filetask.fileTaskID = piFileTaskID and filetask.posted = true) 
         then 
          do:
            pResponse:fault('3005', 'Posted task cannot be closed').
            return.
          end.

        if can-find(first filetask where filetask.fileTaskID = piFileTaskID and filetask.completedDate <> ?)
	     then
	      do:
	        pResponse:fault("3005", "Task cannot be completed as it is already completed").
	        return.
	      end.
        for first filetask where filetask.filetaskID = piFileTaskID no-lock:

          assign 
              iTaskID      = filetask.taskID
              cAgentFileID = string(filetask.agentfileid)
          .
        end.
 
      end.

    else if (piTaskID > 0)
     then
      do:
        if not can-find(first filetask where filetask.taskID = piTaskID ) 
         then 
          do:
            pResponse:fault('3001', 'TaskID').
            return.
          end.

        if can-find(first filetask where filetask.TaskID = piTaskID and filetask.posted = true) 
         then 
          do:
            pResponse:fault('3005', 'Posted task cannot be closed').
            return.
          end.

        if can-find(first filetask where filetask.TaskID = piTaskID and filetask.completedDate <> ?)
	     then
	      do:
	        pResponse:fault("3005", "Task cannot be completed as it is already completed").
	        return.
	      end.

        for first filetask where filetask.taskID = piTaskID no-lock:

          assign 
              iTaskID      = filetask.taskID
              cAgentFileID = string(filetask.agentfileid)
          .
        end.
      end.
    

    run file/gettaskbyid-p.p(input  iTaskID,
                             output dataset-handle dsHandle,
                             output iErrCode,
                             output cErrMessage).
    if iErrCode > 0 
     then 
      do:
        pResponse:fault('3005', 'Not able to retrieve the task from ARC.' + cErrMessage).
        return.
      end.

    if piTaskID > 0 and can-find(first ttfiletask where ttfiletask.TaskStatus = "Closed" )
     then
      lClosedinARC = true. 

    if not lClosedinARC
     then
      for first ttfiletask no-lock:

        if (ttfiletask.assignedto ne ? and ttfiletask.assignedto ne "") and ttfiletask.assignedto ne prequest:UID
         then 
          do:
            pResponse:fault('3005', 'Task cannot be completed as you are not the assigned user').
            return.
          end.

        if (ttfiletask.assignedto = ? or ttfiletask.assignedto = "") 
         then
          do:
            run file/assignuser-p.p(input  ttfiletask.ID,
                                    input  prequest:UID,
                                    input  ttfiletask.DueDateTime,
                                    output iErrCode,
                                    output cErrMessage).
        
         
            if iErrCode > 0 
             then 
              do:
                pResponse:fault("3005", "Task cannot be completed as it cannot be assigned to you." + cErrMessage).
                return.
              end.
            ttfiletask.assignedto = prequest:UID.
          end. 
      end.

    run file/completefiletask-p.p(input  iTaskID,
                                  input  cAgentFileID,
                                  input  lClosedinARC,
                                  output iErrCode,
                                  output cErrMessage).
     
    if iErrCode > 0 
     then 
      do:
        pResponse:fault("3005", "Task cannot be completed." + cErrMessage).
        return.
      end.

    for first ttfiletask no-lock :
      for first filetask where filetask.taskid = iTaskID no-lock :
        ttfiletask.completeddate = filetask.completeddate.
      end.
    end.

    if can-find(first ttfiletask)
     then 
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList). 

    pResponse:success("3005", "Task Complete" + cErrMessage).
    
  end method.
   
end class.


