/*------------------------------------------------------------------------
@name  GetVendorTopics.cls
@action vendorTopicsGet
@description  Get the vendor Topic Details.
@returns Success;2005;Vendor Topic(s) were returned
@author 
@version 1.0
@created 11.01.2023
@Modified :
Date        Name          Comments   
----------------------------------------------------------------------*/


class file.GetVendorTopics inherits framework.ActionBase:

  constructor public GetVendorTopics ():
   super().
  end constructor.

  destructor public GetVendorTopics ( ):
  end destructor.
    

  {lib/callsp-defs.i}
  {tt/vendortopic.i &tableAlias="ttVendortopic"}

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    define variable pcVendorID  as character no-undo.
    define variable pctopic     as character no-undo.

    define variable dsHandle as handle no-undo.
   
    {lib/std-def.i}

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttVendortopic:handle).

    pRequest:getParameter("VendorID",  output pcVendorID).
    pRequest:getParameter("Topic",     output pctopic).

    if pcVendorID = "ALL" or pcVendorID = ?
     then
      pcVendorID = "".

    if pctopic = ?
     then
      pctopic = "".

    {lib\callsp.i 
        &name=spGetVendorTopics 
        &load-into=ttVendortopic 
        &params="input pcVendorID, input pctopic"
        &noResponse=true}
    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end. 

    if can-find(first ttVendortopic) 
     then
      setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).

    pResponse:success("2005", string(csp-icount) {&msg-add} "Vendor Topic"). 
     
  end method.

end class.


