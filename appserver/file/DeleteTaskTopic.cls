/*------------------------------------------------------------------------
@name DeleteTaskTopic.cls
@action taskTopicDelete
@description  Delete the tasktopic record.
@param Name;char
@throws 3001;Invalid Task Name
@throws 3066;Task Name does not exist
@throws 3005;you cannot delete the task name in Use
@throws 3000;Delete failed 
@returns Success;2000
@author S Chandu
@Modified :
Date        Name          Comments    
05/05/2023  Sagar K       deleted in arc url
01/12/2024  SB            Modified to add validation for isSecure field.
03/19/2024  SB            Modified to set isDeleted true and make it inactive
                          instead of deleting task from DB.
----------------------------------------------------------------------*/

class file.DeleteTaskTopic inherits framework.ActionBase:

  constructor public DeleteTaskTopic ():
    super().
  end constructor.

  destructor public DeleteTaskTopic ():
  end destructor.
    
  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
            
    {lib/std-def.i}
    
    define variable pcTopic      as character no-undo.
    define variable lSuccess     as logical   no-undo.
    define variable lError       as logical   no-undo.
    define variable cErrMessage  as character no-undo.

    pRequest:getParameter("Topic", output pcTopic).    
    
    /* validation when no parameter is supplied */
    if (pcTopic = ''  or pcTopic = ?)
     then 
      do:
        pResponse:fault("3001", "Task Topic").
        return.
      end.
    
    /* validate task Name */
    if not can-find(first tasktopic where tasktopic.topic = pcTopic and tasktopic.isDeleted = no) 
     then 
      do:
        pResponse:fault("3066", "Task Topic").
        return.
      end.
   
       
    /* there must be no file Task record */
    if can-find(first filetask where filetask.topic = pcTopic)
     then
      do:
        pResponse:fault("3005", " Task Topic cannot be deleted as it is already in use").
        return. 
      end.

    /* there must be isSecure no to delete the record */
    if can-find(first tasktopic where tasktopic.topic = pcTopic and tasktopic.isSecure = true)
     then
      do:
        pResponse:fault("3005", "This is a secured task topic, cannot be deleted from TPS application.").
        return. 
      end.
     
    lSuccess = false.
    
    TRX-BLOCK:
    for first tasktopic exclusive-lock
      where tasktopic.topic     = pcTopic
        and taskTopic.isDeleted = no
       on error undo TRX-BLOCK, leave TRX-BLOCK:

      assign 
          tasktopic.isDeleted = yes
          tasktopic.active    = no
          .

      validate tasktopic.
      release tasktopic.
       
      lSuccess = true.
    end. 
    
    if not lSuccess
     then 
      do:
        presponse:fault("3000", "Task topic delete").
        return.
    end.

    run file/updatetopiclist-p.p(output lError,
                                 output cErrMessage).

    if lError
     then 
      do:
        pResponse:fault("3005", "Deleted the task topic record in Compass but failed to delete in ARC. " + cErrMessage).
        return.
      end.
    
    pResponse:success("2000", "Task topic delete").   
    
  end method.
 
end class.

