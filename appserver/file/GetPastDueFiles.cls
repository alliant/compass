/*------------------------------------------------------------------------
@file GetPastDueFiles.cls
@action pastDueFilesGet
@description Get past due files Order or Tasks
@param Action;char 
@throws 3005;Invalid Action type
@throws 3027;Action is not one of: (O)rders or (T)asks.
@returns past due files 
@success 2000;X past due files were returned
@author S Chandu
@version 1.0
@created 04/07/2023
Modification:
05/29/2023  Sagar K   changed as per new framework
08/14/2024  Vignesh R Modified to return the "Files with an open order
                      and past due task(s)" records.
08/22/2024  SB        Modified to comment logic for "Files with past due open order" as it is no longer required.
08/30/2024  AG        Modified to return the stateID
----------------------------------------------------------------------*/

class file.GetPastDueFiles inherits framework.ActionBase:
    
  {tt/searchagentfile.i &tableAlias="ttsearchagentfile"}
  {tt/filetask.i  &tableAlias="ttFileTask"}

  constructor GetPastDueFiles ():
    super().
  end constructor.

  destructor public GetPastDueFiles ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
    
	define variable pcUID        as character no-undo.
    define variable pcAction     as character no-undo.
    define variable pcStateID    as character no-undo.

    define variable lError       as logical   no-undo.
    define variable cErrMessage  as character no-undo.
    define variable iCount       as integer   no-undo.

    define variable dsHandle     as handle    no-undo.
    define variable dsHandle1    as handle    no-undo.

    define variable cproductDesc as character no-undo.

    /* converting local time into utc datetime-tz  */ 
    define variable cTodayDateTime  as datetime-tz no-undo.
    define variable cnow            as datetime    no-undo.
    define variable utcMinutes      as integer     no-undo.
    define variable utcMinutesdiff  as integer     no-undo.

    /* to pick local time zone */
    assign
        cTodayDateTime = now
        utcMinutes     = timezone(cTodayDateTime)
        utcMinutesDiff = int(- utcminutes) /* converted to utc required format */
        .

     /* now on server time changed to UTc time */
    cTodayDateTime = datetime-tz(cTodayDateTime,utcMinutesDiff).
    cTodayDateTime = datetime-tz(cTodayDateTime,0).
    cnow           = datetime(substring(string(cTodayDateTime),1,19)).

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttsearchagentfile:handle).

    create dataset dsHandle1.
    dsHandle1:serialize-name = 'data'.
    dsHandle1:set-buffers(buffer ttFileTask:handle).

    pcUID = pRequest:UID.
    pRequest:getParameter("Action", output pcAction). 
    pRequest:getParameter("StateID", output pcStateID).

    /* validate action */
    if pcAction = ? or pcAction = ''
     then
      do:
        pResponse:fault("3005", " Action Type is not valid").
        return.
      end.
	  
   if pcAction = 'o' and pcStateID <> ? and pcStateID <> ''  /*  files with Orders for particular state */
    then
     do:
       for each job no-lock
         where (job.stat = 'N' or job.stat = 'P'):

         for first agentFile no-lock
           where agentFile.agentFIleID = job.agentFIleID and agentFile.stateId = pcStateID:
           cproductDesc = ''.
           for first product where product.productID = job.productID no-lock:
             cproductDesc = product.description.
           end.
           searchAgentFile(input agentFile.agentfileID, input cproductDesc).
         end.
       end.
     end.
    /*Below code is no longer in use because Files with past due open order Option(PO) is removed from Manage Tab*/
   /*else if pcAction = 'O'  /* files with past due (O)rders  */
    then
     do:
       for each job no-lock
         where job.duedate < cTodayDateTime and (job.stat = 'N' or job.stat = 'P'):
         cproductDesc = ''.
         for first product where product.productID = job.productID no-lock:
           cproductDesc = product.description.
         end.
         searchAgentFile(input job.agentfileID, input cproductDesc).
       end. /* for each job */

     end.*/
   else if pcAction = 'T'  /* files with past due (T)asks  */
    then
     do:
       run file/getopentasks-p.p(input pRequest:UID, /* UserName */
                                 input "",           /* AssignedTo */
                                 input "New,Open",
	    				         input "",
                                 input "",
                                 output dataset-handle dsHandle1,
                                 output lError,
                                 output cErrMessage,
                                 output iCount).
        
       if lError 
        then
         do:
           pResponse:fault ("3005",  "GetPastduefileTasks Failed: " + cErrMessage).
           return.
         end.

       for each ttFileTask no-lock
         where datetime(ttFileTask.DueDateTime) < datetime(cNow) :
           for first job no-lock
             where job.agentfileid = ttFileTask.agentfileid and (job.stat = 'N' or job.stat = 'P'):
               searchAgentFile(input ttFileTask.agentfileID, input ttFileTask.topic).
           end.

       end. /* for each ttFileTask */

     end.   /* do end else if */
    
    iCount = 0.
    for each ttsearchagentfile no-lock:
      iCount = iCount + 1.
    end.

    if iCount > 0 then
      setcontentdataset(input dataset-handle dsHandle,input pRequest:FieldList).

    pResponse:success("2000", string(iCount) + ( if pcAction = "T" then " PastDueFileTasks" else " PastDueFileOrders")).
          
  end method.

  
  /*---------------------- searchAgentFile ----------------------*/
   method public void searchAgentFile (input iAgentfileID as integer, input productDesc as character):

    define variable cstateID     as character no-undo.
    define variable cAddress     as character no-undo.

    for first agentfile no-lock 
      where agentfile.agentfileID = iAgentfileID:

      create ttsearchagentfile.
      assign
          ttsearchagentfile.agentFileID       = agentfile.agentFileID
          ttsearchagentfile.fileID            = agentfile.fileID
          ttsearchagentfile.fileNumber        = agentfile.fileNumber
          ttsearchagentfile.buyerDescription  = agentfile.buyerDescription
          ttsearchagentfile.sellerDescription = agentfile.sellerDescription
          ttsearchagentfile.productInfo       = productDesc
          .

       for first agent no-lock where agent.agentID = agentfile.agentID:          
         assign
             ttsearchagentfile.agentName = agent.name + ' (' + agent.agentID + ')'
             ttsearchagentfile.stateID   = agent.stateID.
       end.

      for each fileproperty no-lock
        where fileproperty.agentfileID = agentfile.agentfileID:

        assign
            ttsearchagentfile.parcelID          = fileProperty.parcelID   
            ttsearchagentfile.book              = fileProperty.book       
            ttsearchagentfile.pageNumber        = fileProperty.pageNumber 
            ttsearchagentfile.Condominum        = fileProperty.Condominum 
            ttsearchagentfile.subdivision       = fileProperty.subdivision
            cAddress                            = if (fileProperty.addr1 = "?") or (fileProperty.addr1 = "") then "" else fileProperty.addr1
            cAddress                            = cAddress + if (fileProperty.addr2 = "?")   or (fileProperty.addr2 = "")   then "" else ", " + fileProperty.addr2
            cAddress                            = cAddress + if (fileProperty.addr3 = "?")   or (fileProperty.addr3 = "")   then "" else ", " + fileProperty.addr3
            cAddress                            = cAddress + if (fileProperty.addr4 = "?")   or (fileProperty.addr4 = "")   then "" else ", " + fileProperty.addr4
            cAddress                            = cAddress + if (fileProperty.city  = "?")   or (fileProperty.city = "")    then "" else ", " + fileProperty.city
            cAddress                            = cAddress + if (fileProperty.stateID = "?") or (fileProperty.stateID = "") then "" else ", " + fileProperty.stateID
            cAddress                            = cAddress + if (fileProperty.zip   = "?")   or (fileProperty.zip = "")     then "" else ", " + fileProperty.zip
            .
           
        for first county field (description) no-lock 
          where county.countyID = fileProperty.countyID and 
           county.stateID  = fileProperty.stateID:

           assign cAddress =  cAddress + if (county.description = "?") or (county.description = "") then "" else ", " +  county.description.
        end.

        assign 
            cAddress                       =  trim(cAddress,',')
            ttsearchagentfile.propertyAddr =  trim(cAddress)
            .


      end. /* for each fileProperty */
    end. /* for first agentfile */

  end method.   
      
end class.


