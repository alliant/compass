/*------------------------------------------------------------------------
@name ModifyFile.cls
@action fileModify
@description  Update the agent file.
@param agentFileID;int
@throws 3001;Invalid agentFileID
@throws 3066;Agent File does not exist
@throws 3005;Agent file is not locked by current user
            ;Agent file is in hold
            ;seq is invalid or duplicate
@throws 3000;agentFile modify failed 
@returns Success;2000
@author Sachin Anthwal
@Modified :
Date        Name          Comments 
01/27/23    SA            Task:99003 changed as per new framework
03/09/23    SA            Modified index creation logic
04/24/23    VR            Modified to add deed table record creation
07/05/23    SK            Added default value
07/25/23    S Chandu      Modified job to update status 'P' from 'X'.
01/24/24    K.R           Modified to remove logic of updating agent file to seperate .p
                          and removed state validation of the fileproperty
03/01/23    SK            Changed file and sysaction name
08/08/24    SRK           Modified to add validation for email,phone
09/18/2024  SRK           Modified to create state index  
----------------------------------------------------------------------*/

CLASS file.ModifyFile INHERITS framework.ActionBase:
 
  CONSTRUCTOR PUBLIC ModifyFile ():
    SUPER().
  END CONSTRUCTOR.

  DESTRUCTOR PUBLIC ModifyFile ():
  END DESTRUCTOR.
    

  /* temp table */
  {tt/agentFile.i    &tableAlias="ttAgentFile"    &updateFile=true &serializeName=agentFile}   
  {tt/fileProperty.i &tableAlias="ttFileProperty" &updateFile=true &serializeName=property}   
  {tt/fileParty.i    &tableAlias="ttBuyer"        &updateFile=true &serializeName=buyer}
  {tt/fileParty.i    &tableAlias="ttSeller"       &updateFile=true &serializeName=seller}
  {tt/fileParty.i    &tableAlias="ttLender"       &updateFile=true &serializeName=lender}
  {tt/fileContent.i  &tableAlias="ttRequirement"  &updateFile=true &serializeName=requirement}
  {tt/fileContent.i  &tableAlias="ttException"    &updateFile=true &serializeName=exception}
  {tt/fileContent.i  &tableAlias="ttInstrument"   &updateFile=true &serializeName=instrument}
  {tt/job.i          &tableAlias="ttSearch"       &serializeName=search}
  {tt/job.i          &tableAlias="ttPolicy"       &serializeName=policy}
  {tt/job.i          &tableAlias="ttJob"          }
  {tt/deed.i         &tableAlias="ttDeed"         &updateFile=true &serializeName=deed}
  {tt/filetask.i     &tableAlias=ttfiletask}

  /* lib*/
  {lib/nextkey-def.i}
  {lib/removeNullValues.i}
     
  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):
  
    /* local variable */
    define variable lSuccess        as logical   no-undo.
    define variable piAgentFileID   as integer   no-undo. /* parameter variable */
    define variable plreturnRecord  as logical   no-undo. /* parameter variable */
    define variable olSuccess       as logical   no-undo.
    define variable ocMsg           as character no-undo.
    define variable ifileInvID      as integer   no-undo.
    define variable olInvSuccess    as logical   no-undo. 
    define variable cErrMessage     as character no-undo.
    define variable cIndexValueList as character no-undo.
    define variable cAddressList    as character no-undo.
    define variable cstatelist      as character no-undo.
    define variable cStateCountyList as character no-undo.
    define variable cMsg            as character no-undo.
    define variable lNewJobCreated  as logical   no-undo.
    define variable lJobStatUpdated as logical   no-undo.
    define variable lError          as logical   no-undo.

    define variable iErrCode        as integer   no-undo.
    define variable iTasksCount     as integer   no-undo. 
    define variable iemail          as integer   no-undo. 
    define variable csearchphone    as character no-undo.
    define variable cpolicyphone    as character no-undo.

      
    
    /* library file */
    {lib/std-def.i}
        
    /* handle */
    define variable dsHandle  as handle no-undo. /* handle for update data*/
    define variable dsHandle2 as handle no-undo. /* handle for get data */
    
    
    /* buffer */
     define buffer bfJob for job. 
    define buffer bfileInvItem  for fileInvItem.
    define buffer bffileInvItem for fileInvItem.


    define variable dsHandle3      as handle    no-undo.

    create dataset dsHandle3.
    dsHandle3:serialize-name = 'data'.
    dsHandle3:set-buffers(buffer ttfiletask:handle).
    
    /* parameter get */
    pRequest:getParameter("agentFileID", output piAgentFileID). 
    pRequest:getParameter("returnRecord", output plreturnRecord ).
    
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttAgentFile:handle,
                         buffer ttFileProperty:handle,
                         buffer ttBuyer:handle,
                         buffer ttSeller:handle,
                         buffer ttLender:handle,
                         buffer ttRequirement:handle,
                         buffer ttException:handle,
                         buffer ttInstrument:handle,
                         buffer ttSearch:handle,
                         buffer ttPolicy:handle,
                         buffer ttDeed:handle).
    
    /*dataset get*/
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    
    if std-ch > ''
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.
    
    if not can-find(first ttAgentFile)
     then
      do:
        pResponse:fault('3005', 'Input data not provided').
        return.
      end.
    
    /* validation when no parameter is supplied */
    if (piAgentFileID = 0  or piAgentFileID= ?)
     then 
      do:
        pResponse:fault("3001", "agentFileID").
        return.
      end.
    
    /* validate agentFileID */
    if not can-find(first agentFile where agentFile.agentFileID = piAgentFileID ) 
     then 
      do:
        pResponse:fault("3066", "Agent File").
        return.
      end.
    
    /* when agent file is not locked */
    {lib/islocked.i &obj='agentFile' &id=piAgentFileID &exclude-fault=true}
    if not std-lo or (std-ch ne pRequest:uid)
     then 
      do:
        pResponse:fault("3005", "Agent file is not locked by current user.").
        return. 
      end.  
    
   /* agent File must not be on hold */
    if can-find(first agentfile where agentfile.agentFileID = piAgentFileID and agentfile.ishold = true)
     then
      do:
        pResponse:fault("3005", "Agent file is on hold.").
        return.
      end.     
    
    removeNullValues("ttAgentFile").
    removeNullValues("ttFileProperty").
    removeNullValues("ttBuyer").
    removeNullValues("ttSeller").
    removeNullValues("ttLender").
    removeNullValues("ttRequirement").
    removeNullValues("ttException").
    removeNullValues("ttInstrument").
    removeNullValues("ttSearch").
    removeNullValues("ttPolicy").
    removeNullValues("ttDeed").
    
    /* validationg  client side seq */
    std-lo =  validateSeq(output olSuccess,
                    output ocMsg).
    
    if not olSuccess
     then
      do:
        pResponse:fault("3005", ocMsg).
        return.
      end.
    
    
      /* converting local time into utc datetime-tz  */ 
    define variable cCreateDateTime as datetime-tz no-undo. 
    define variable utcMinutes      as integer     no-undo.
    define variable utcMinutesdiff  as integer     no-undo.

        /* to pick local time zone */
    assign
        cCreateDateTime = now
        utcMinutes      = timezone(cCreateDateTime)
        utcMinutesDiff  = int(- utcminutes) /* converted to utc required format */
        .

    for each ttSearch:
      if ttSearch.contactemail = '' or ttSearch.contactemail = ? then.
      else
        do iemail = 1 to num-entries(ttSearch.contactEmail,";"):
         if not util.CommonUtils:isValidEmailAddress(entry(iemail,ttSearch.contactEmail,";"))
          then 
           do:
             pResponse:fault("3005", entry(iemail,ttSearch.contactEmail,";") + " is not a valid email address").
             return.
           end.
        end.

      if ttSearch.contactPhone = '' or ttSearch.contactPhone = ? then.
      else
       do:
         run util/validatecontactnumber-p.p(input ttSearch.contactPhone,output csearchphone,output lError,output ocMsg).
         if lError then
          do:
            pResponse:fault("3005", ocMsg).
            return.
          end.
          ttSearch.contactPhone = csearchphone.
       end.

     if ttSearch.duedate ne ?
      then
       assign
          ttSearch.duedate = datetime-tz(ttSearch.duedate,utcminutesdiff)
          ttSearch.duedate = datetime-tz(ttSearch.duedate,0)
             .
    
     if ttSearch.effectivedate ne ?
      then
       assign
           ttSearch.effectivedate = datetime-tz(ttSearch.effectivedate,utcminutesdiff)
           ttSearch.effectivedate = datetime-tz(ttSearch.effectivedate,0)
           .
  
    end.
    for each ttPolicy:
     
     if ttPolicy.contactemail = '' or ttPolicy.contactemail = ? then.
      else
        do iemail = 1 to num-entries(ttPolicy.contactEmail,";"):
         if not util.CommonUtils:isValidEmailAddress(entry(iemail,ttPolicy.contactEmail,";"))
          then 
           do:
             pResponse:fault("3005", entry(iemail,ttPolicy.contactEmail,";") + " is not a valid email address").
             return.
           end.
        end.

      if ttPolicy.contactPhone = '' or ttPolicy.contactPhone = ? then.
      else
       do:
         run util/validatecontactnumber-p.p(input ttPolicy.contactPhone,output cpolicyphone,output lError,output ocMsg).
         if lError then
          do:
            pResponse:fault("3005", ocMsg).
            return.
          end.
          ttPolicy.contactPhone = cpolicyphone.
       end.
  
      if ttPolicy.duedate ne ?
       then
        assign
            ttPolicy.duedate = datetime-tz(ttPolicy.duedate,utcminutesdiff)
            ttPolicy.duedate = datetime-tz(ttPolicy.duedate,0)
             .
    
     if ttPolicy.effectivedate ne ?
      then
       assign
           ttPolicy.effectivedate = datetime-tz(ttPolicy.effectivedate,utcminutesdiff)
           ttPolicy.effectivedate = datetime-tz(ttPolicy.effectivedate,0)
           .
  
    end.
  
    error-status:error = false. 
    run file/modifyfile-p.p (input piAgentFileID,
                             input pRequest:UID,
                             input dataset-handle dsHandle,
                             output cIndexValueList,
                             output cAddressList,
                             output cStateCountyList,
                             output cstatelist,
                             output lNewJobCreated,
                             output lSuccess,
                             output cMsg).

    if not lSuccess or error-status:error
     then 
      do:
        if error-status:error 
         then cMsg = cMsg + "," + error-status:get-message(1). 
        pResponse:fault("3005", cMsg).
        return.
      end.
   
    /* jobtran record creation */
    run file/createjobtran-p.p (input  piAgentFileID,          /* Agent File ID */
                                input  pRequest:actionID,      /* Action */
                                input  pRequest:uID,           /* Created By */
                                input  'agentFileID='+ string(piAgentFileID), /* Query String */
                                input  dataset-handle dsHandle). /* Payload */
        
    /* If the non-order entry task is already there and we add a job, 
       the status of newly created job should change to processing as soon as it's added, 
       provided it is current job. */
    if lSuccess and lNewJobCreated
     then
      do:
        run file/getfiletasks-p.p(input  piAgentFileID, 
                                  output dataset-handle dsHandle3,
                                  output iErrCode,
                                  output cErrMessage,
                                  output iTasksCount).

        for first ttfiletask no-lock where ttfiletask.topic       ne 'Order Entry'  and 
                                           ttfiletask.topic       ne 'Order Review' and
                                           ttFiletask.taskstatus  ne 'Canceled':
            JOB_TRX_SEARCH:
            for last job where job.agentfileid = piAgentFileID and
                               job.jobtype = "Search" and job.stat = 'N' 
                               exclusive-lock transaction on error undo JOB_TRX_SEARCH, leave JOB_TRX_SEARCH by suffix:
                job.stat = 'P'.
                lJobStatUpdated = true.
            end.
            if not ljobStatUpdated then
            do:
                JOB_TRX_POLICY:
                for last job where job.agentfileid = piAgentFileID and 
                    job.jobtype = "Policy" and job.stat = 'N'
                    exclusive-lock transaction on error undo JOB_TRX_POLICY, leave JOB_TRX_POLICY by suffix:
                    job.stat = 'P'.
                end.

            end.

        end.

        dsHandle3:clear().
      end.
    
    if not lSuccess
     then 
      do:
        presponse:fault("3000", "Agent File update").
        return.
      end. /* if not lSuccess */
    
    
    if plreturnRecord
     then
      do:
        run file/getagentfile-p.p(input piAgentFileID,
                                  input pRequest:UID,
                                  output dataset-handle dsHandle2,
                                  output lSuccess,
                                  output cErrMessage).
    
       if lSuccess 
        then 
         setContentDataset(input dataset-handle dsHandle2, pRequest:FieldLIst).
       else
        cErrMessage = " but error occured while fetching file".
      end. /* end if plreturnRecord */
    
    /* creating sysindex */
    if cIndexValueList ne ''  
     then
      do:
        /* Build keyword index to use for searching */
        run sys/createsysindex.p (input 'AgentFile',           /* ObjType */
                                  input '',                    /* ObjAttribute*/
                                  input string(piAgentFileID), /* ObjID */
                                  input cIndexValueList,       /* ObjKeyList*/
                                  output lSuccess,
                                  output cMsg).
        if not lSuccess
         then
          cErrMessage = cErrMessage + " " + cMsg.
      end. /* if cIndexValueList ne '' */

    /* creating sysindex */
    if cStateList ne ''  
     then
      do:
        /* Build keyword index to state */
        run sys/createsysindex.p (input 'FileProperty',            /* ObjType */
                                  input 'State',                    /* ObjAttribute*/
                                  input string(piAgentFileID), /* ObjID */
                                  input cStateList,            /* ObjKeyList*/
                                  output lSuccess,
                                  output cMsg).
        if not lSuccess
         then
          cErrMessage = cErrMessage + " " + cMsg.
      end. /* if cStateList ne '' */

         /* creating sysindex file property */
    if cAddressList ne ''  
     then
      do:
        /* Build keyword index to use for searching */
        run sys/createfilepropertyindex.p (input 'FileProperty',         /* ObjType */
                                              input 'Address',        /* ObjAttribute*/
                                              input string(piAgentFileID), /* ObjID */
                                              input cAddressList,       /* ObjKeyList*/
                                              input cStateCountyList,
                                              output lSuccess,
                                              output cMsg).
        if not lSuccess
         then
          cErrMessage = cErrMessage + " " + cMsg.
      end. /* if cAddressList ne '' */  
    
    pResponse:success("3005", "Agent File update was successful" + cErrMessage + "." ).   
    
  end method.
 
  /*------------------------validateSeq----------------------*/
  method public logical validateSeq (output oplsucess as logical,
                                     output opcMsg as character):

  /* temptable buffer */
  define buffer bfttFileProperty for ttFileProperty .
  define buffer bfttBuyer        for ttBuyer        .
  define buffer bfttSeller       for ttSeller       .
  define buffer bfttLender       for ttLender       .
  define buffer bfttRequirement  for ttRequirement  .
  define buffer bfttException    for ttException    .
  define buffer bfttInstrument   for ttInstrument   .

  /* validating fileproperty seq */
  for each ttFileProperty no-lock:
    if ttFileProperty.seq = 0 or ttFileProperty.seq = ? 
     then
      do:
        opcMsg = "File Property seq is invalid.".
        return false.
      end. /* if ttFileProperty.seq = 0 */
  
    if can-find(first bfttFileProperty
	              where bfttFileProperty.agentFileID = ttFileProperty.agentfileID
				    and bfttFileProperty.seq = ttFileProperty.seq
					and rowid(bfttFileProperty) <> rowid(ttFileProperty))
	 then
      do:
       opcMsg = "Duplicate FileProperty Seq.".
       return false.
      end. /* can-find(first bfttFileProperty */
  end. /* for each ttFileProperty */
  
  /* validating buyer seq */
  for each ttBuyer no-lock:
    if ttBuyer.seq = 0 or ttBuyer.seq = ? 
     then
      do:
        opcMsg = "Buyer seq is invalid".
        return false.
      end. /* if ttBuyer.seq = 0 */
  
    if can-find(first bfttBuyer
	              where bfttBuyer.agentFileID = ttBuyer.agentfileID
				    and bfttBuyer.seq = ttBuyer.seq
					and rowid(bfttBuyer) <> rowid(ttBuyer))
	 then
      do:
        opcMsg = "Duplicate Buyer Seq".
        return false.
      end. /* if can-find(first bfttBuyer */
  end. /* for each ttBuyer */
  
  /* validating seller seq */
  for each ttSeller no-lock:
    if ttSeller.seq = 0 or ttSeller.seq = ? 
     then
      do:
        opcMsg = "Seller seq is invalid.".
        return false.
      end. /* if ttSeller.seq = 0  */
  
    if can-find(first bfttSeller
	              where bfttSeller.agentFileID = ttSeller.agentfileID
				    and bfttSeller.seq = ttSeller.seq
					and rowid(bfttSeller) <> rowid(ttSeller))
	 then
      do:
        opcMsg = "Duplicate Seller Seq.".
        return false.
      end. /* can-find(first bfttSeller */
  end. /* for each ttSeller */
  
  /* validating lender seq */
  for each ttLender no-lock:
    if ttLender.seq = 0 or ttLender.seq = ? 
     then
      do:
        opcMsg = "Lender seq is invalid.".
        return false.
      end. /* if ttLender.seq = 0 */
  
    if can-find(first bfttLender
	              where bfttLender.agentFileID = ttLender.agentfileID
				    and bfttLender.seq = ttLender.seq
					and rowid(bfttLender) <> rowid(ttLender))
     then
      do:
        opcMsg = "Duplicate Lender Seq.".
        return false.
      end. /* if can-find(first bfttLender */
  end. /*for each ttLender */
  
  /* validating requirement seq */
  for each ttRequirement no-lock:
    if ttRequirement.seq = 0 or ttRequirement.seq = ? 
     then
      do:
        opcMsg = "Requirement seq is invalid.".
        return false.
      end. /* if ttRequirement.seq = 0 */
  
    if can-find(first bfttRequirement
	              where bfttRequirement.agentFileID = ttRequirement.agentfileID
				    and bfttRequirement.seq = ttRequirement.seq
					and rowid(bfttRequirement) <> rowid(ttRequirement))
     then
      do:
        opcMsg = "Duplicate Requirement Seq.".
        return false.
      end. /* if can-find(first bfttRequirement */
  end. /* for each ttRequirement */ 
  
  /* validating exception seq */
  for each ttException no-lock:
    if ttException.seq = 0 or ttException.seq = ? 
     then
      do:
        opcMsg = "Exception seq is invalid.".
        return false.
      end. /* if ttException.seq = 0 */
  
    if can-find(first bfttException
	              where bfttException.agentFileID = ttException.agentfileID
				    and bfttException.seq = ttException.seq
					and rowid(bfttException) <> rowid(ttException))
     then
      do:
        opcMsg = "Duplicate Exception Seq.".
        return false.
      end. /* if can-find(first bfttException */
  end. /* for each ttException */ 
  
  /* validating instrument seq */
  for each ttInstrument no-lock:
    if ttInstrument.seq = 0 or ttInstrument.seq = ? 
     then
      do:
        opcMsg = "Instrument seq is invalid.".
        return false.
      end. /* if ttInstrument.seq = 0  */
  
    if can-find(first bfttInstrument
	              where bfttInstrument.agentFileID = ttInstrument.agentfileID
				    and bfttInstrument.seq = ttInstrument.seq
					and rowid(bfttInstrument) <> rowid(ttInstrument))
     then
      do:
        opcMsg = "Duplicate Instrument Seq.".
        return false.
      end. /* if can-find(first bfttInstrument */
  end. /* for each ttInstrument */

  oplsucess = true.
                      
  return oplsucess.

 end method.

end class.
