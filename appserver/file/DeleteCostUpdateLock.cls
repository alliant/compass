/*------------------------------------------------------------------------
@file DeleteCostUpdateLock.p
@action costUpdateLockDelete 
@description Delete Vendor Task Cost Update lock

@author SRK
@version 1.0
@created 09/02/2024
Modification:

----------------------------------------------------------------------*/

class file.DeleteCostUpdateLock inherits framework.ActionBase:
   

  constructor DeleteCostUpdateLock ():
    super().
  end constructor.

  destructor public DeleteCostUpdateLock ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable pSuccess  as logical no-undo.

    TRX-BLOCK:
    for first syslock exclusive-lock
      where   syslock.entityType = "VendorTaskCostUpdate" TRANSACTION
        on error undo TRX-BLOCK, leave TRX-BLOCK:
      delete syslock.
      release syslock.
      pSuccess = true.
    end.


    if not pSuccess
     then
      do: 
        pResponse:fault("3000", "DeleteCostUpdate Unlock").
        return.
      end.
    
    pResponse:success("2000", "DeleteCostUpdate Unlock").
             
  end method.
      
end class.


