/*------------------------------------------------------------------------
@name  file/NewFileTask.cls
@action fileTaskNew
@description Create file task in compass and arc.  

@param AgentID;char;ID of an Agent
@param FileNumber;char;The file number of an Agent
@param agentFileID;int;The unique sequence fileID of an Agent

----------------------------------------------------------------------*/

class file.NewFileTask inherits framework.ActionBase:
  {lib/nextkey-def.i} 

  define variable dsHandle         as handle      no-undo.
  define variable pcTopic          as character   no-undo.
  define variable pcQueue          as character   no-undo.
  define variable piAgentFileID    as integer     no-undo.
  define variable pdcost           as decimal     no-undo.
  define variable pcAssignedTo     as character   no-undo.
  define variable cErrMessage      as character   no-undo.
  define variable lSuccess         as logical     no-undo.
  define variable lcreatetask      as logical.
  define variable iTaskID          as integer     no-undo.
  define variable iFileTaskID      as integer     no-undo.
  define variable lReturnVal       as logical     no-undo. 
  define variable cErrorMsg        as character   no-undo.
  define variable pdtDueDate       as datetime-tz no-undo.

   /* converting local time into utc datetime-tz  */ 
  define variable cTodayDateTime as datetime-tz no-undo. 
  define variable utcMinutes     as integer     no-undo.
  define variable utcMinutesdiff as integer     no-undo.

  constructor public NewFileTask():
    super().
  end constructor.

  destructor public NewFileTask():
  end destructor.

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}

    pRequest:getParameter("AgentFileID", output piAgentFileID).
    pRequest:getParameter("Topic",       output pcTopic).
    pRequest:getParameter("Queue",       output pcQueue). 
    pRequest:getParameter("AssignedTo",  output pcAssignedTo).   
    pRequest:getParameter("DueDate",     output pdtDueDate). 
    pRequest:getParameter("cost",        output pdcost).


    /* validate agentfileID */
    if not can-find(first agentFile where agentFile.agentFileID = piAgentFileID) 
     then 
      do:
        pResponse:fault("3001", "AgentFileID").  
        return.
      end.

    /* validate topic */
    if not can-find(first tasktopic where tasktopic.topic = pcTopic ) 
     then 
      do:
        pResponse:fault("3001", "Topic").  
        return.
      end.
       
    for first tasktopic where tasktopic.topic = pcTopic:
      if not tasktopic.active 
       then
        do:
          pResponse:fault("3021", "Topic" {&msg-add} "active").
          return.
        end.
    end.

    /* validate queue */
    if (pcQueue ne ?) and (pcQueue ne "") and not can-find(first queuecategory where queuecategory.category = pcQueue) 
     then 
      do:
        pResponse:fault("3001", "Category").  
        return.
      end.
       
    for first queuecategory where queuecategory.category = pcQueue:
      if not queuecategory.active
       then
        do:
          pResponse:fault("3021", "Category" {&msg-add} "active").
          return.
        end.
    end.

    /* validate assignedTo */
    if (pcAssignedTo ne ?) and (pcAssignedTo ne "") and not can-find(first sysuser where sysuser.uid = pcAssignedTo) 
     then 
      do:
        pResponse:fault("3001", "Assigned user").  
        return.
      end.
       
    for first sysuser fields(isActive) where sysuser.uid = pcAssignedTo:
      if not sysuser.isActive
       then
        do: 
          pResponse:fault("3021", "Assigned user" {&msg-add} "active").
          return.
        end.
    end.
    
    /* If Queue or assignedTo is provided, then due date is mandatory. */
    if ((pcQueue ne ? and pcQueue ne "") or 
        (pcAssignedTo ne ? and pcAssignedTo ne "")
       ) 
       and                                                  
       (pdtDueDate = ?)
     then
      do:
        pResponse:fault("3001", "Due Date"). 
        return.
      end.

    if pdtDueDate ne ? 
     then
      do:
        assign
            cTodayDateTime  = now
            utcMinutes      = timezone(cTodayDateTime)
            utcMinutesDiff  = int(- utcminutes) /* converted to utc required format */
            .
   
        /* client's due date is converting into UTC time */
        pdtDueDate = datetime-tz(pdtDueDate,utcMinutesDiff).
        pdtDueDate = datetime-tz(pdtDueDate,0).
   
        /* now on server time changed to UTc time */
        cTodayDateTime = datetime-tz(cTodayDateTime,utcMinutesDiff).
        cTodayDateTime = datetime-tz(cTodayDateTime,0).

        if pdtDueDate < cTodayDateTime
         then
          do:
            pResponse:fault("3005", "Due Date must be in the future").  
            return.
          end.
      end.

    /* File Task creation in compass */
    run file/newfiletask-p.p(input piAgentFileID,
                             input pcTopic,
                             input pcQueue, 
                             input pRequest:UID,
                             input now,
                             input pcAssignedTo,
                             input pdcost,
                             output iFileTaskID,
                             output lReturnVal,
                             output cErrorMsg
                               ). 
    if not lReturnVal
     then
      do:

        pResponse:fault("3005", 'Failed to create File Task in Compass.' + cErrorMsg).
        return.
      end.

    if (pcAssignedTo = '' or pcAssignedTo = ?) and (pcQueue = '' or pcQueue = ?)
      then 
       do:
         presponse:success("2000","File Task created in Compass").
         return.
       end.

    if (pcAssignedTo <> '' and pcAssignedTo <> ?) and (pcQueue = '' or pcQueue = ?) 
     then pcQueue = 'Production'.

    /* ARC Task creation */
    for first agentfile no-lock
     where agentfile.agentFileID = piAgentFileID:
      run file/createarctask-p.p(input pcQueue,
                                 input pcTopic,
                                 input agentfile.agentFileID,
                                 input agentFile.fileNumber,
                                 input agentFile.fileID,
                                 input pcAssignedTo,
                                 input pdtDueDate,
                                 output iTaskID,
                                 output lReturnVal,
                                 output cErrorMsg
                                ).
      if lReturnVal 
       then
        do:
          pResponse:fault("3005", "File Task created in compass but failed to create Task in ARC." + cErrorMsg).
          return.
        end.
  
      FILETASK_UPD:
      do transaction 
       on error undo FILETASK_UPD, leave FILETASK_UPD:
        for first filetask exclusive-lock where filetaskid = iFileTaskID:
          filetask.taskid = iTaskID.
        end.
      end.
    end. /* for first agentfile */
  pResponse:success("2000","File Task creation in Compass and ARC").
  end method.
end class.
