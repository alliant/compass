/*------------------------------------------------------------------------
@name  GetFilesWithBalance.cls
@action productionFilesWithBalanceGet
@description  Get the Production Files with balance.
@returns Success;2000
@returns Fault;3005
@author Shefali
@Modified :
Date        Name          Comments     
   
------------------------------------------------------------------------*/

class file.GetFilesWithBalance inherits framework.ActionBase:
   
 {tt/fileAr.i &tableAlias="tFileAr"}
 {lib/callsp-defs.i}
  
  constructor GetFilesWithBalance ():
    super().
  end constructor.

  destructor public GetFilesWithBalance ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    {lib/std-def.i}

    define variable lSuccess      as logical   no-undo.
    define variable cErrMessage   as character no-undo.
    define variable dsHandle      as handle    no-undo.
	define variable pcAgentID      as character no-undo.

    create dataset dsHandle.  
    dsHandle:serialize-name = 'data'.      
    dsHandle:set-buffers(buffer tFileAr:handle).
	
	pRequest:getParameter("agentID", output pcAgentID).

    if pcAgentID = "" then pcAgentID = "ALL".

    {lib/callsp.i  &name=spGetFilesWithBalance &load-into=tFileAr &params="input pcAgentID" &noResponse=true}

    if not csp-lSucess and csp-cErr > ''
     then
      do:
        pResponse:fault("3005", csp-cErr).
        return.    
      end.

    if csp-lSucess and can-find(first tFileAr)
     then
        setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
		
    pResponse:success("2005", string(csp-icount) {&msg-add} " Production Files having balance").
         
  end method.
      
end class.


