/*------------------------------------------------------------------------
@name  file/FinalizeNonVendorTasks.cls
@action nonVendorTasksFinalize
@description Finalize Non Vendor Tasks
@parameters
@returns 
@fault 3000:Finalize Non Vendor Tasks failed.
@success 2000;char;file task records partially posted was successful.
         2000: Finalize Non vendor Tasks was successful.

@author S chandu
@version 1.0
@created 08/07/2023

@Modified    :
    Date        Name                 Comments   
    02/25/2024  K.R                  Modified to remove code to set the completeddate in filetask table
    07/29/2024  S.B                  Modified to change arctaskpost as Sync process instead of queued process
----------------------------------------------------------------------*/

class file.FinalizeNonVendorTasks inherits framework.ActionBase:

 constructor public FinalizeNonVendorTasks ():
  super().
 end constructor.

 destructor public FinalizeNonVendorTasks ():
 end destructor.
    
 {tt/filetask.i &tableAlias=ttFileTask}
 {tt/filetask.i &tableAlias=ttarcfiletask}
 {tt/proerr.i   &tableAlias="ttTaskPostErr"}

 define variable iErrorSeq as integer no-undo.
 
 method public override void act (input pRequest  as framework.IRequestable,
                                  input pResponse as framework.IRespondable):
   {lib/std-def.i}

   define variable dsHandle       as handle    no-undo.
   define variable lSuccess       as logical   no-undo.
   define variable pcTaskIDList   as character no-undo.
   define variable lValidated     as logical   no-undo initial true.
   define variable notPOsted      as integer   no-undo.
   define variable dsHandle2      as handle    no-undo.
   define variable lError         as logical   no-undo.
   define variable cErrMessage    as character no-undo.
   define variable pEndDate       as date      no-undo.
   
   define variable pcStatus          as character no-undo.
   define variable pcStatusMessage   as character no-undo.
   define variable pcFailedIdList    as character no-undo.

   empty temp-table ttfiletask.
   empty temp-table ttarcfiletask.
   empty temp-table ttTaskPostErr.

   create dataset dsHandle.
   dsHandle:serialize-name = 'data'.
   dsHandle:set-buffers(buffer ttFileTask:handle).

   create dataset dsHandle2.
   dsHandle2:serialize-name = 'data'.
   dsHandle2:set-buffers(buffer ttarcfiletask:handle).

   pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
   if std-ch > ""
    then
     do:
       lValidated = false.
       createErrorRecord ('Data Parsing Failed. Reason: ' + std-ch).
       return.
     end.

   if not can-find(first ttFileTask) 
    then
      do:
        lValidated = false.
        createErrorRecord ("FileTask record not provided").
        return.
      end.

   if pEndDate = ?
    then
     pEndDate = today.

    /* Validate filetask should be completed and not posted */
   run file/getclosedtasks-p.p(input "",            /* UserName */
                               input "",            /* AssignedTo */
                               input "Closed",     /* Status = complete */
                               input "",            /* Queue */
                               input "",           /* Task */
                               input '',           /* EndDateRangeStart */
                               input pEnddate,     /* EndDateRangeEnd */
                               input false,        /* posted flag*/
                               output dataset-handle dsHandle2,
                               output lError,
                               output cErrMessage,
                               output std-in).
   if lError 
    then
     do:
       lValidated = false.
       createErrorRecord ("Posting Failed: " + cErrMessage).
     end.

   for each ttfiletask:
  
     /* validate filetaskID */
     if not can-find(first filetask where filetask.filetaskID = ttfiletask.filetaskID)
      then
       do:
         lValidated = false.
         createErrorRecord("FileTask DB record does not exist for FileTaskID: " + string(ttfiletask.filetaskID)).
      end.
     else
      for first filetask fields (filetaskID posted taskID) 
        where filetask.filetaskID = ttfiletask.fileTaskID no-lock:
    
    
        /* validate from arc */
        if not can-find(first ttarcfiletask where ttarcfiletask.ID = filetask.taskID)
         then
          do:
            lValidated = false.
            createErrorRecord ("Task with ID " + string(ttfiletask.filetaskID) + " is not completed in ARC").
          end.
    
        /* validate the posted flag*/
        if filetask.posted = true 
         then
          do:
            lValidated = false.
            createErrorRecord("File Task " + string(filetask.filetaskID) + " is already posted").
          end. 
    
      end.

   end. /* for each ttfiletask */
 
   if not lValidated 
     then
      do:
        setContentTable("FileTaskPostErr",input table ttTaskPostErr, input pRequest:FieldList).
        pResponse:success("3005","Validations failed, check the error file generated for more details").
        return.
      end.

   assign
       lSuccess     = false
       pcTaskIDList = ''
       .

   TRX-BLOCK:
   for each ttFiletask transaction
       on error undo TRX-BLOCK, next TRX-BLOCK:

     for first filetask fields(filetaskID taskID posted apinvID) 
       where filetask.filetaskID = ttFiletask.filetaskID exclusive-lock:

       assign
           filetask.posted  = true
           filetask.apinvID = 0         
           .
      
       validate filetask. 
       release  filetask.

  
       ttFiletask.posted = true.
       lSuccess = true.
     end.
    
   end.

   if not lSuccess
    then
     do:
        presponse:fault("3000", "Finalize Non Vendor Tasks").
        return.
     end.

  /* taskID list */
  for each ttFiletask:

    if ttFiletask.posted = true
     then
      pcTaskIDList = pcTaskIDList + string(ttfiletask.ID) + ",".
    else
     notPosted = notPosted + 1.

  end.

  /*All the errors are handled in postarctask-p.p program*/
  run ap/postarctask-p.p(input trim(pcTaskIDList, ","), output pcStatus, output pcStatusMessage, output pcFailedIdList).

  setContentDataset(input dataset-handle dsHandle, pRequest:FieldLIst).

  if notPosted > 1
   then
    pResponse:success("2000","file task records partially posted").
  else
   pResponse:success("2000", "Finalize Non Vendor Tasks").

end method.

method private void createErrorRecord (input pcErrorMsg as character):
  create ttTaskPostErr.
  assign 
    iErrorSeq                 = iErrorSeq + 1
    ttTaskPostErr.err         = string(iErrorSeq)
    ttTaskPostErr.description = pcErrorMsg
    .
end method.

end class.

