/*------------------------------------------------------------------------
@name file.ModifyOfficeNote.cls
@action  officeNoteModify
@description  Create/Modify a Office Note record.
@param name, Temp-Table ttOfficeNote dataset
@throws 3005;Data Parsing failed.
@author Vignesh Rajan
@version 1.0
@created 11/15/2023
@Modified :
Date        Name          Comments  
----------------------------------------------------------------------*/

class file.ModifyOfficeNote inherits framework.ActionBase:

  {tt/officeNote.i &tableAlias=ttOfficeNote}

  constructor ModifyOfficeNote ():
    super().
  end constructor.

  destructor public ModifyOfficeNote ():
  end destructor.
 

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    {lib/std-def.i}

    define variable dsHandle as handle no-undo.

    define variable cAction as character no-undo.

    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttOfficeNote:handle).

    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.

    if not can-find(first ttOfficeNote) 
     then
      do:
        pResponse:fault('3066','Office Note').
        return.
      end.

    for first ttOfficeNote:

      if ttOfficeNote.officeID = 0 or ttOfficeNote.office = ? 
       then
        do:
          pResponse:fault('3005', 'officeID cannot be zero or null.').
          return.
        end.
      
    end.

    if pResponse:isFault() 
     then
      return.
    
    TRX-BLOCK:
    for first ttOfficeNote transaction on error undo TRX-BLOCK, leave TRX-BLOCK:
      if not can-find(first officeNote where officeNote.officeID = ttOfficeNote.officeID and officeNote.seq = ttOfficeNote.seq) 
       then
        do:

          create officeNote.
          assign
            officeNote.officeID = ttOfficeNote.officeID
            officeNote.seq      = ttOfficeNote.seq
            officeNote.noteDate = now
            officeNote.uid      = pRequest:uid
            officeNote.category = ttOfficeNote.category
            officeNote.subject  = ttOfficeNote.subject
            officeNote.noteType = ttOfficeNote.noteType
            officeNote.notes    = ttOfficeNote.notes.

          validate officeNote.
          release officeNote.

          assign
            std-lo  = true
            cAction = "Create".

        end.
       else 
        do:
          for first officeNote where officeNote.officeID = ttOfficeNote.officeID 
                                 and ttOfficeNote.seq = ttOfficeNote.seq exclusive-lock:
            officeNote.notes = ttOfficeNote.notes.

            assign
              std-lo  = true
              cAction = "Update".

          end.
        end.
    end.

    if not std-lo 
     then
      do:
        pResponse:fault("3000", "Update").
        return.
      end.


    pResponse:success("2000", "Office Note " + cAction). 
    
         
  end method.
      
end class.





