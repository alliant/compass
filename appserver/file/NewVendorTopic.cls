/*------------------------------------------------------------------------
@name NewVendorTopic.cls
@action vendorTopicNew
@description  Create the new vendor Topic record.
@param returnRecord;logical
@param vendortopic;dataset
@throws 3005;Data Parsing Failed
@throws 3066;Vendor ID does not exist
@throws 3000;vendor topic creation failed
@returns Success;2002;Vendortopic has been created
@author Sachin Chaturvedi
@Modified :
Date        Name          Comments  
10/22/23    Sagar K       Modified to change vendorproduct to vendortopic
------------------------------------------------------------------------*/

class file.NewVendorTopic inherits framework.ActionBase:
 
  constructor public NewVendorTopic ():
    super().
  end constructor.

  destructor public NewVendorTopic ():
  end destructor.

  /* Temp Tables */
  {tt/vendortopic.i &tableAlias=ttVendortopic}
  {lib/removeNullValues.i}

  method public override void act (input pRequest as framework.IRequestable,
                                   input pResponse as framework.IRespondable):

    /* Variables */
    define variable plReturnRecord  as logical   no-undo.
    define variable cVendorID       as character no-undo.
    define variable ctopic          as character no-undo.

    define variable dsHandle as handle no-undo.

    /* Include Files */
    {lib/std-def.i}

    /* dataset creation */
    create dataset dsHandle.
    dsHandle:serialize-name = 'data'.
    dsHandle:set-buffers(buffer ttVendortopic:handle).

    /*Parameters get*/
    empty temp-table ttVendortopic.
    pRequest:getParameter('returnRecord', output plReturnRecord).

    if plReturnRecord = ?
     then 
      plReturnRecord = false.

    /* get Dataset */
    pRequest:getContent(input-output dataset-handle dsHandle, output std-ch).
    if std-ch > '' 
     then 
      do:
        pResponse:fault('3005', 'Data Parsing Failed. Reason: ' + std-ch).
        return.
      end.

    if not can-find(first ttVendortopic) 
     then
      do:
        pResponse:fault('3005', 'Vendor Product record not provided').
        return.
      end.


    for first ttVendortopic:

      if ttVendortopic.vendorid =  "" or 
         ttVendortopic.vendorid =  ?  or
         ttVendortopic.vendorid = "?"
       then 
        do:
          pResponse:fault("3005", "Vendor ID cannot be blank/Invalid value").
          return.
        end.

      if ttVendortopic.topic = ? or
         ttVendortopic.topic = ""
       then 
        do:
          pResponse:fault("3005", "Topic cannot be Invalid value").
          return.
        end.

      if not can-find(first Vendor where Vendor.vendorid = ttVendortopic.vendorid)
       then 
        do:
          pResponse:fault("3066", "VendorId " + ttVendortopic.vendorid).
          return.
        end.

      if can-find(first vendortopic where vendortopic.vendorid = ttVendortopic.vendorid and vendortopic.topic = ttVendortopic.topic)
       then
        do:
          pResponse:fault("3005", "Vendor topic already exists").
          return.
        end.

      if not can-find(first tasktopic where tasktopic.topic = ttVendortopic.topic and tasktopic.active)
       then
        do:
          pResponse:fault("1000", "Task").
          return.
        end.
    end.

    removeNullValues("ttVendortopic").

    TRX-BLOCK:
    for first ttVendortopic 
        transaction on error undo TRX-BLOCK, leave TRX-BLOCK:         

      create Vendortopic.
      assign 
          cVendorID                  = ttVendortopic.vendorId
          ctopic                     = ttVendortopic.topic
          Vendortopic.vendorid       = ttVendortopic.vendorId
          Vendortopic.topic          = ttVendortopic.topic
          Vendortopic.Price          = ttVendortopic.Price
          Vendortopic.createdby      = pRequest:uid
          Vendortopic.createdDate    = now
          Vendortopic.lastModifiedBy = ""
          .

      validate Vendortopic.
      release Vendortopic.

      std-lo = true.
    end.

    if not std-lo
     then
      do:
        pResponse:fault("3000", "Vendor Topic creation"). /* Vendor Topic creation failed */
        return.
      end.

    if plReturnRecord 
     then 
      do:
        empty temp-table ttVendortopic.
  
        for first Vendortopic where Vendortopic.vendorID   = cVendorID and
                                      Vendortopic.topic  = ctopic no-lock:
          create ttVendortopic.
          assign
              ttVendortopic.vendorID  = Vendortopic.vendorID
              ttVendortopic.topic     = Vendortopic.topic
              ttVendortopic.Price     = Vendortopic.Price
              .

        end.

        setContentDataset(input dataset-handle dsHandle, input pRequest:FieldList).
      end.

    pResponse:success("2002", "Vendor Topic"). /* Vendor Topic has been created */

  end method.

end class.
