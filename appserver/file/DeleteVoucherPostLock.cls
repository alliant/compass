/*------------------------------------------------------------------------
@file DeleteVoucherPostLock.cls
@action voucherPostLockDelete 
@description Delete Vendor Voucher Post

@author SRK
@version 1.0
@created 09/02/2024
Modification:

----------------------------------------------------------------------*/

class file.DeleteVoucherPostLock inherits framework.ActionBase:
   

  constructor DeleteVoucherPostLock ():
    super().
  end constructor.

  destructor public DeleteVoucherPostLock ():
  end destructor.   

  method public override void act (input pRequest  as framework.IRequestable,
                                   input pResponse as framework.IRespondable):     

    define variable pSuccess  as logical no-undo.

    TRX-BLOCK:
    for first syslock exclusive-lock
      where   syslock.entityType = "VendorVoucherPost" TRANSACTION
        on error undo TRX-BLOCK, leave TRX-BLOCK:
      delete syslock.
      release syslock.
      pSuccess = true.
    end.


    if not pSuccess
     then
      do: 
        pResponse:fault("3000", "VendorVoucherPost Unlock").
        return.
      end.
    
    pResponse:success("2000", "VendorVoucherPost Unlock").
             
  end method.
      
end class.


