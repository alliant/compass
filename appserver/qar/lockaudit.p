/*------------------------------------------------------------------------
@name lockaudit.p
@action auditLock
@description Locks an audit record
@param AuditID;int;The id of the audit (required)

@throws 3004;The audit was not locked
@throws 3067;The audit is not found in the audit table
@throws 3081;The Audit is locked by another user

@success 2000;The audit has been locked
@success 2018;The audit is locked by user

@author John Oliver
@version 1.0
@created 11/20/2015
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

def var piAuditID as int no-undo.
def var iAuditNoteID as int no-undo.

{lib/std-def.i}
{tt/qar.i &tableAlias="ttAudit"}

pRequest:getParameter("AuditID", OUTPUT piAuditID).
/* validate the audit id is valid */
if not can-find(first qar where qar.qarID = piAuditID)
 then pResponse:fault("3067", "Audit" {&msg-add} "ID" {&msg-add} string(piAuditID)).
 
/* validate that the audit is not locked already locked */
run util/issyslock.p ("Audit", piAuditID, 0, OUTPUT std-lo, OUTPUT std-ch, OUTPUT std-dt).
if std-lo
 then
  do: if std-ch = pRequest:uid
       then pResponse:success("2018", "Audit").
       else pResponse:fault("3081", "Audit" {&msg-add} std-ch).
      return.
  end.
  
if pResponse:isFault()
 then return.

std-lo = false.
/* all parameters are valid so start to modify the row */
run util/newsyslock.p ("Audit", string(piAuditID), 0, pRequest:Uid, output std-lo).

if not std-lo
 then
  do: pResponse:fault("3004", "Audit").
      return.
  end.

pResponse:success("2000", "Lock").
