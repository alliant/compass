&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name prequeueaudit.p
@action auditPrequeue
@description Retrieve information for the auditing questions

@param qarID

@returns 1) "Required bit" for agent to carry E & O or professional liability insurance.
         2) Agent policy issuing limit amount.
         3) "Y", "N", or "NA" for open corrective actions from the last audit.
         4) List of question ids (delimited by semicolon)for open corrective actions.
         5) YTDnetpremium for prior year.
         6) YTDgrosspremium for prior year.
         7) Average time 'gap' between the effective date of the policy and NOC processing
            date.
         8) Last audit score.
         9) "Y" or "N" for minimum annual amount remitted by agent to remain an agent.
         10) "Y", "N", or "NA" for greater than 20% drop in last ytd (12 months) remittances.
         11) IF there's 20% drop in last ytd remittances, then the cause is "Unknown" else "NA".
         12) List of claims in process(format: claimid - claimdescription delimited by semicolon).
         
 @faultcode 3066 Audit ID: xxxxx does not exist
 
 @author Yoke Sam C.
 @version 1.0
 @created 05/2017
 @modified Yoke Sam C. 08/04/2017 - Return audit year.
           Yoke Sam C. 09/25/2017 - Use cutoff date (month) from sysprop table
                                    to calculate the net premium and gross premium for the
                                    calendar year.
           Shefali    01/25/2022  - Task:89648 Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"			     
           Shefali    04/04/2023  - Task:103627- Send NULL value to teoRequired variable                            
--------------------------------------------------------------------------------------- */

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

def var iqarID as int no-undo.

def var pcAgentID as char no-undo.
def var auditYear as int no-undo.
def var qarauditYr as int no-undo.
def var auditCutoffdate as date no-undo.
def var auditStartDate as date no-undo.

def var teoRequired as log init no no-undo.
def var tmaxCoverage as decimal init 0.00 no-undo.
def var tlastAuditScore as int no-undo.
def var topenCorrectAct as char no-undo.
def var tauditid as char no-undo.
def var tytdnetpremium as decimal init 0.00 no-undo.
def var tytdgrosspremium as decimal init 0.00 no-undo.
def var taveragecnt as int no-undo.
def var tytdgap as decimal no-undo.
def var tavggap as decimal no-undo.
def var tisMinAmtRemitted as logical init no no-undo.
def var ttwentypercentdropamt as decimal no-undo.
def var ttwentypercentdrop as char no-undo.
def var tcausetwentypercentdrop as char no-undo.
def var tcurrytdnetpremium as decimal init 0.00 no-undo.
def var tprevytdnetpremium as decimal init 0.00 no-undo.
def var tquestionID as char no-undo.
def var claimsprocess as char no-undo.
def var offsetdate as date no-undo.
def var loopmth as int no-undo.
def var netpremendmonth as int no-undo.
def var tmthytdnetpremium as decimal init 0.00 no-undo.
def var hasaudityr as log init false no-undo.
def var pUID  as character no-undo.
def var hTable as handle.


&scoped-define minAmtRemitted 7500
&scoped-define percentytdremitdrop 0.8 /* 80% */

{tt/ops18.i &tableAlias="auditdata"}

{lib/std-def.i}
{lib/callsp-defs.i}

pRequest:getParameter("AuditID", iqarID).
/* validate qarID */
if string(iqarID) = ? or string(iqarID) = ""
  then pResponse:fault("3001", "Audit ID").
/* validate audit ID exists */
if not can-find(first qar where qar.qarid = iqarID) 
  then pResponse:fault("3066", "Audit ID: " + string(iqarID)).

if pResponse:isFault() 
  then return.
 
assign
pcAgentID = ?
auditYear = year(today)
auditcutoffdate = today
auditStartDate = today
.

for first sysprop where sysprop.appCode = "QAR"
                            and sysprop.objAction = "Prequeueaudit"
                            and sysprop.objProperty = "AuditCutoff" no-lock:

   auditcutoffdate = date(entry(1,sysprop.objvalue,"/") + "-" + entry(2,sysprop.objValue,"/") + "-"  + STRING(year(today))).

end.
if month(auditcutoffdate) >= month(today)  then
       auditYear = auditYear - 1.

qarauditYr = auditYear.

for first qar no-lock
    where qar.qarID = iqarID:
    assign pcAgentID = qar.agentID
           auditStartDate = (if qar.auditStartDate = ? then auditStartDate else qar.auditStartDate)
           /*auditYear = qar.auditYear*/
           .

end.

if pcAgentID = ? then
do:
   pResponse:fault("3001", "Agent").
   return.
end.


for first agent no-lock
    where agent.agentID = pcAgentID:
    assign
/*      /* Agency Agreement require that the agent carry E & O or Professional Liability */
/*         insurance.                                                                    */
/*      */                                                                               */
/*                                                                                       */
     teoRequired = ?
/*      /* Agency policy issuing limit amount */                                         */
     tmaxCoverage = agent.liabilityLimit
     .

end.


/* Last audit score */
tlastAuditScore = ?.
for first audit no-lock
    where audit.agentID = pcAgentID
     by audit.auditdate desc:

    assign
      tlastAuditScore = audit.score
      tauditid = audit.auditID
      .
end.


/*Any open corrective actions since the last audit */
if can-find(first auditaction
             where auditaction.auditID = tauditid
             and caps(auditaction.stat) = "O")
    then tOpenCorrectAct = "Y".
else
    tOpenCorrectAct = "N".

if tlastAuditScore = ? then tOpenCorrectAct = "NA".

/* If there're open corrective actions since the last audit, get the question ids */
tquestionID = "".
if tOpenCorrectAct = "Y" then
do:

  for each audit no-lock
      where audit.agentID = pcAgentID
         and audit.auditID = tauditid,
         each auditaction where auditaction.auditid = tauditid and auditaction.stat = "O",
         each auditfinding where auditfinding.findingID =  auditaction.findingID:

      tquestionID = tquestionID + auditfinding.questionID + (if auditfinding.questionID > "" then ";" else "").

  end.
  tquestionID = trim(tquestionID,";").

end.

/* Get the ytd (full year) net and gross (The calculation is based on the calendar year) */

/* if month(auditstartdate) < 12 then audityear = audityear - 1. */

run GetNetGrossandReportGap in this-procedure.

for first auditdata no-lock
    where auditdata.agentID = pcAgentID
         and auditdata.periodYear = audityear:

     /* Get YTD Net Premium */                                                       
    tytdnetpremium = auditdata.janNet + auditdata.febNet + auditdata.marNet
                   + auditdata.aprNet + auditdata.mayNet + auditdata.junNet
                   + auditdata.julNet + auditdata.augNet + auditdata.sepNet
                   + auditdata.octNet + auditdata.novNet + auditdata.decNet
                   .

     /* Get YTD Gross Premium */                                                     
    tytdgrosspremium = auditdata.janGross + auditdata.febGross + auditdata.marGross
                    + auditdata.aprGross + auditdata.mayGross + auditdata.junGross
                    + auditdata.julGross + auditdata.augGross + auditdata.sepGross
                    + auditdata.octGross + auditdata.novGross + auditdata.decGross
                    .


/*     /* Get average report gap - time 'gap' between the effective date of the policy */
/*        and the NOC processing date */                                               */

      taveragecnt = 0.
      if auditdata.janGap > 0 then taveragecnt = taveragecnt + 1.
      if auditdata.febGap > 0 then taveragecnt = taveragecnt + 1.
      if auditdata.marGap > 0 then taveragecnt = taveragecnt + 1.
      if auditdata.aprGap > 0 then taveragecnt = taveragecnt + 1.
      if auditdata.mayGap > 0 then taveragecnt = taveragecnt + 1.
      if auditdata.junGap > 0 then taveragecnt = taveragecnt + 1.
      if auditdata.julGap > 0 then taveragecnt = taveragecnt + 1.
      if auditdata.augGap > 0 then taveragecnt = taveragecnt + 1.
      if auditdata.sepGap > 0 then taveragecnt = taveragecnt + 1.
      if auditdata.octGap > 0 then taveragecnt = taveragecnt + 1.
      if auditdata.novGap > 0 then taveragecnt = taveragecnt + 1.
      if auditdata.decGap > 0 then taveragecnt = taveragecnt + 1.

      if taveragecnt > 0
          then
           do:
               tytdgap = auditdata.janGap + auditdata.febGap + auditdata.marGap
               + auditdata.aprGap + auditdata.mayGap + auditdata.junGap
               + auditdata.julGap + auditdata.augGap + auditdata.sepGap
               + auditdata.octGap + auditdata.novGap + auditdata.decGap
               .

               tavggap = integer(tytdgap / taveragecnt) no-error.

           end.
      else tavggap = 0.

      /* Yes/No - Minimum annual amount remitted by agent to remain an agent*/
      
      if  max(tytdnetpremium, {&minAmtRemitted}) = tytdnetpremium then
           tisMinAmtRemitted = yes.
      else
           tisMinAmtRemitted = no.

end.


/* Brief list of claims in process for the agent */
claimsprocess = "".
for each claim no-lock
    where claim.agentID = pcAgentID,
    each claimcode where claimcode.claimID = claim.claimID and claimcode.codetype = "ClaimDescription",
    each syscode where syscode.code = claimcode.code:
    claimsprocess = claimsprocess + string(claim.claimID) + (if string(claim.claimID) > "" then (" - " + syscode.description) + ";"  else ""). 
end.

/* Calculate net premium for the ending ytd month */

offsetdate = auditStartDate.
run GetMonthYieldToDateNet in this-procedure.
tcurrytdnetpremium = tmthytdnetpremium.

tmthytdnetpremium = 0.00.
/* net premium for year before  */
run GetMonthYieldToDateNet in this-procedure.
tprevytdnetpremium  = tmthytdnetpremium.


/* Yes / No / N/A   - Greater than 20% drop in remittances over the last 12 months*/

if integer(tcurrytdnetpremium) > 0 and integer(tprevytdnetpremium) > 0 and hasaudityr then
do:
   ttwentypercentdropamt =  ({&percentytdremitdrop}) * tprevytdnetpremium.
   if tcurrytdnetpremium < ttwentypercentdropamt then
       ttwentypercentdrop = "Y".
   else
       ttwentypercentdrop = "N".
end.
else
    ttwentypercentdrop = "NA".

/* if there has been 20% drop, determine the driving cause. if yes 20% drop then "Unknown" else "NA" */

tcausetwentypercentdrop = (if ttwentypercentdrop = "Y" then "Unknown" else "NA").

pResponse:setParameter("auditYear", qarauditYr).
pResponse:setParameter("eoRequired", teoRequired).
pResponse:setParameter("AgentIssueAmt", tmaxCoverage).
pResponse:setParameter("LastAuditScore", tlastAuditScore).
pResponse:setparameter("OpenCorrAct", topenCorrectAct).
pResponse:setparameter("YTDNetPremium", tytdnetpremium).
pResponse:setparameter("YTDGrossPremium", tytdgrosspremium).
pResponse:setparameter("AvgGap", tavggap).
pResponse:setparameter("IsMinAmtRemitted", tisMinAmtRemitted).
pResponse:setparameter("TwentyPercentDrop", ttwentypercentdrop).
pResponse:setparameter("TwentyPercentDropCause", tcausetwentypercentdrop).
pResponse:setparameter("OpenCorrQuestionsID", tquestionID).
pResponse:setparameter("ClaimsInProcess", claimsprocess).

procedure GetNetGrossandReportGap:
/* Get the net, gross, and report gap */
  
  pUID = pRequest:uid.
  
  {lib/callsp.i  &name=spReportBatchActivity &load-into=auditdata &params="input audityear, input pcAgentID, input 'ALL', input pUID" &nocount=TRUE &noresponse=TRUE}

end procedure.

procedure MonthYieldToDateNet:
    
    std-in = 0.
    run GetNetGrossandReportGap in this-procedure.
    for each auditdata no-lock
       where auditdata.agentID = pcAgentID
         and auditdata.periodYear = audityear:
    
        hasaudityr = true.
        do while loopmth <= netpremendmonth  :
            case loopmth:
                when 1 then std-in = std-in + auditdata.janNet.
                when 2 then std-in = std-in + auditdata.febNet.
                when 3 then std-in = std-in + auditdata.marNet.
                when 4 then std-in = std-in + auditdata.aprNet.
                when 5 then std-in = std-in + auditdata.mayNet.
                when 6 then std-in = std-in + auditdata.junNet.
                when 7 then std-in = std-in + auditdata.julNet.
                when 8 then std-in = std-in + auditdata.augNet.
                when 9 then std-in = std-in + auditdata.sepNet.
                when 10 then std-in = std-in + auditdata.octNet.
                when 11 then std-in = std-in + auditdata.novNet.
                when 12 then std-in = std-in + auditdata.decNet.
        
            end case.
            loopmth = loopmth + 1.
        
        end.
    end.
end procedure.

procedure GetMonthYieldToDateNet:

    def var offsetmonth as int no-undo.
    
    offsetdate = add-interval(offsetdate, -12, "months").
    offsetmonth = 13 - month(offsetdate).
    loopmth = month(offsetdate).
    audityear = year(offsetdate).
    netpremendmonth = 12.
    if offsetmonth = 1 then 
    do:
        loopmth = 1.
        audityear = year(offsetdate) + 1.
    end.

   hasaudityr = false.
   run MonthYieldToDateNet in this-procedure.
   tmthytdnetpremium = std-in.
   
    if offsetmonth > 1 and hasaudityr then
    do:
        netpremendmonth = month(offsetdate) - 1.
        loopmth = 1.
        audityear = year(offsetdate) + 1.
        run MonthYieldToDateNet in this-procedure.
        tmthytdnetpremium = tmthytdnetpremium + std-in.
    end.


end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 2.1
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


