/*------------------------------------------------------------------------
@name getscheduleagents.p
@action scheduleAgentsGet
@description Fetch agent information to schedule QAR
@param Year;int;The Year of which agents audited (required)

@success 2005;The count of the records returned

@author Anjly Chanana
@version 1.0
@created 10/03/2017
@modified  10/22/20  AC  Modified to get data of all audit types
           07/21/21  SA  Task 83510 modified to add grade and year data.
           08/11/23  AG  Task#:106419 Modified to show agent legal name
                         instead of short name.
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse. 

def var iYear as int no-undo.
def var pcscheduleType as char no-undo.
def var iCount as int no-undo.

{lib/std-def.i}
{tt/scheduleQARdata.i}

pRequest:getParameter("Year", OUTPUT iYear).
pRequest:getParameter("scheduleType", OUTPUT pcscheduleType).

/* validate if the year is valid */
date(1, 1, iYear) no-error.
if error-status:error
 then pResponse:fault("3001", "Year").

if pResponse:isFault()
 then return.

define buffer qarbuf1 for qar.
define buffer qarsectionbuf1 for qarsection.
define buffer qarbuf2 for qar.
define buffer qarsectionbuf2 for qarsection.
define buffer qarbuf3 for qar.
define buffer qarsectionbuf3 for qarsection.

assign std-in = 0.
for each agent no-lock
   where agent.stat <> "P":

  create scheduleQARdata.
  assign
    scheduleQARdata.agentName    = agent.legalname
    scheduleQARdata.agentID      = agent.agentID
    scheduleQARdata.agentStatus  = agent.stat
    scheduleQARdata.agentManager = agent.manager
    scheduleQARdata.agentAddress = agent.addr1 + (if agent.addr2 = "" then "" else agent.addr2)
    scheduleQARdata.agentCity    = agent.city
    scheduleQARdata.agentState   = agent.stateID
    scheduleQARdata.agentZip     = agent.zip
    .
  /* get the agent's status */
  {lib/get-sysprop.i &appCode="'AMD'" &objAction="'Agent'" &objProperty="'Status'" &objID=agent.stat &out=scheduleQARdata.agentStatusDesc}
  /* data for current QAR */
  for first qar no-lock
      where qar.agentID = agent.agentID
        and qar.auditYear = iYear
        and lookup(qar.stat, "P,Q") > 0: 
    
    assign
      scheduleQARdata.proposedAuditDate = qar.schedStartDate
      scheduleQARdata.proposedAuditor = qar.uid
      scheduleQARdata.proposedAuditID = qar.qarID
      scheduleQARdata.proposedAuditType = qar.auditType
      .
  end.
  iCount = 0.
  /* data for last year's QAR */
  for EACH  qar no-lock
      where qar.agentID = agent.agentID
        and qar.auditYear <= iYear 
        and qar.stat = "C"
        BREAK BY audityear DESC :
    iCount = iCount + 1.
    if iCount > 3 
     then
      leave.
    
    /* data for third last year's QAR */
    if icount = 1 
    then 
     do:
    assign
      scheduleQARdata.Year1AuditScore = qar.score
      scheduleQARdata.Year1AuditGrade = qar.grade
      scheduleQARdata.qarID1 = qar.qarID
      scheduleQARdata.Auditor1 = qar.uid
      scheduleQARdata.AuditType1 = qar.audittype
      scheduleQARdata.AuditFinishDate1 = qar.auditfinishdate
      .
    for first qarsection no-lock
        where qarsection.qarID = qar.qarID
          and qarsection.sectionID = 6:
       
       scheduleQARdata.year1ERRScore = qarsection.score.
    end.
  end.
    /* data for second last year's QAR */
    if icount = 2 
     then 
      do:
    assign
      scheduleQARdata.Year2AuditScore =  qar.score
            scheduleQARdata.Year2AuditGrade = qar.grade
      scheduleQARdata.qarID2 = qar.qarID
      scheduleQARdata.Auditor2 = qar.uid
      scheduleQARdata.AuditType2 = qar.audittype
      scheduleQARdata.AuditFinishDate2 = qar.auditfinishdate
      .
    for first qarsection no-lock
        where qarsection.qarID = qar.qarID
          and qarsection.sectionID = 6:
       
       scheduleQARdata.year2ERRScore = qarsection.score.
    end.
  end.
    
    /* data for last year's QAR */
    if icount = 3
     then 
      do:
    assign
      scheduleQARdata.Year3AuditScore =  qar.score
            scheduleQARdata.Year3AuditGrade =  qar.grade
      scheduleQARdata.qarID3 = qar.qarID
      scheduleQARdata.Auditor3 = qar.uid
      scheduleQARdata.AuditType3 = qar.audittype
      scheduleQARdata.AuditFinishDate3 = qar.auditfinishdate
      .
    for first qarsection no-lock
        where qarsection.qarID = qar.qarID
          and qarsection.sectionID = 6:
       
       scheduleQARdata.year3ERRScore = qarsection.score.
    end.
  end.
  end.   

  /* auditor */
  scheduleQARdata.lastAuditor = scheduleQARdata.auditor1.
  if scheduleQARdata.lastAuditor = ""
   then
    do:
      scheduleQARdata.lastAuditor = scheduleQARdata.auditor2.
      if scheduleQARdata.lastAuditor = ""
       then scheduleQARdata.lastAuditor = scheduleQARdata.auditor3.
    end.
  /* date */
  scheduleQARdata.lastAuditDate = scheduleQARdata.auditFinishDate1.
  if scheduleQARdata.lastAuditDate = ?
   then
    do:
      scheduleQARdata.lastAuditDate = scheduleQARdata.auditFinishDate2.
      if scheduleQARdata.lastAuditDate = ?
       then scheduleQARdata.lastAuditDate = scheduleQARdata.auditFinishDate3.
    end.
  std-in = std-in + 1.
end.

IF std-in > 0
 then pResponse:setParameter("scheduleQARdata", table scheduleQARdata).

pResponse:success("2005", STRING(std-in) {&msg-add} "scheduleQARdata").
