&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
    File        : qardefs.i (copied from webdefs.i)
    Purpose     : WEB-related DEFinitionS and base functionality
    Author(s)   : D.Sinclair
    Created     : 4.4.2012
 */


def {&shared} shared var currentIpAddress as char no-undo.

def {&shared} shared var currentUserid as char no-undo.
def {&shared} shared var currentUserName as char no-undo.
def {&shared} shared var currentUserInitials as char no-undo.
def {&shared} shared var currentUserRole as char no-undo.
def {&shared} shared var currentAuditing as logical init false no-undo.

def {&shared} shared var currentAction as char no-undo.
def {&shared} shared var currentActionFault as logical init false no-undo.
def {&shared} shared var currentActionFeedback as char no-undo.

def {&shared} shared var currentDataPath as char no-undo.
def {&shared} shared var currentDocPath as char no-undo.
def {&shared} shared var currentDocURL as char no-undo.
 
{lib/std-def.i} 

/* Webspeed related variables and functions */
def shared var web-utilities-hdl as handle no-undo.
def shared stream webStream.
&global-define out put stream webstream unformatted

function format-datetime returns character
  (input p_format  as character,
   input p_date    as date,
   input p_time    as integer,
   input p_options as character) in web-utilities-hdl.

function get-cgi returns character
  (input p_name as char) in web-utilities-hdl.

function get-cookie returns character
  (input p_name as char) in web-utilities-hdl.

function get-value returns character
  (input p_name as char) in web-utilities-hdl.

function url-decode returns character
  (input pvalue as char) in web-utilities-hdl.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD decodeFromXml Include 
FUNCTION decodeFromXml RETURNS CHARACTER PRIVATE
  ( pEncoded as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD encodeForXml Include 
FUNCTION encodeForXml RETURNS CHARACTER PRIVATE
  ( pUnencoded as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD outputFault Include 
FUNCTION outputFault RETURNS LOGICAL
  ( input pCode as char,
    input pMsg as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD outputHeaders Include 
FUNCTION outputHeaders RETURNS LOGICAL PRIVATE
  ( /*pMimeType as char*/ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE destroy Include 
PROCEDURE destroy :
/*------------------------------------------------------------------------------
  Purpose:     To allow Webspeed to call an enclosing routine directly
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE error Include 
PROCEDURE error :
/*------------------------------------------------------------------------------
  Purpose:     Used during SAX parsing
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pErr as char no-undo.
 return error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fatalError Include 
PROCEDURE fatalError :
/*------------------------------------------------------------------------------
  Purpose:     Used during SAX parsing
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pErr as char no-undo.
 return error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION decodeFromXml Include 
FUNCTION decodeFromXml RETURNS CHARACTER PRIVATE
  ( pEncoded as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def var pResult as char no-undo.
 def var tCnt as int no-undo.
 def var tChar as char no-undo.

 pEncoded = replace(pEncoded, "&apos;", "'").
 pEncoded = replace(pEncoded, "&quot;", '"').
 pEncoded = replace(pEncoded, "&lsquo;", "'").
 pEncoded = replace(pEncoded, "&rsquo;", "'").
 pEncoded = replace(pEncoded, "&ldquo;", '"').
 pEncoded = replace(pEncoded, "&rdquo;", '"').
 pEncoded = replace(pEncoded, "&gt;", ">").
 pEncoded = replace(pEncoded, "&lt;", "<").
 pEncoded = replace(pEncoded, "&amp;", "&").
 pEncoded = replace(pEncoded, "&br;", "&#10;").

 pResult = pEncoded.
 do tCnt = 1 to length(pEncoded):
  tChar = substring(pEncoded, tCnt, 1).
  if asc(tChar) < 32
   then pResult = replace(pResult, tChar, ""). 
 end.

 /* At this point, we have & and &#10; both with an ampersand */
 RETURN pResult.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION encodeForXml Include 
FUNCTION encodeForXml RETURNS CHARACTER PRIVATE
  ( pUnencoded as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  pUnencoded = replace(pUnencoded, "&", "&amp;").
  /* The & would have been replaced in the original up above */
  pUnencoded = replace(pUnencoded, "&amp;#10;", "&#10;").
  
  pUnencoded = replace(pUnencoded, "'", "&apos;").
  pUnencoded = replace(pUnencoded, '"', "&quot;").
  pUnencoded = replace(pUnencoded, ">", "&gt;").
  pUnencoded = replace(pUnencoded, "<", "&lt;").

  pUnencoded = replace(pUnencoded, chr(145), "&apos;").
  pUnencoded = replace(pUnencoded, chr(146), "&apos;").
  pUnencoded = replace(pUnencoded, chr(147), "&quot;").
  pUnencoded = replace(pUnencoded, chr(148), "&quot;").
  RETURN pUnencoded.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION outputFault Include 
FUNCTION outputFault RETURNS LOGICAL
  ( input pCode as char,
    input pMsg as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
/*  
  {&out} '<?xml version="1.0"?>' skip.
  {&out} '<Fault faultCode="' encodeForXml(pCode) 
            '" faultString="' encodeForXml(pMsg) '"/>' skip.
  currentActionFault = true.
  */
  RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION outputHeaders Include 
FUNCTION outputHeaders RETURNS LOGICAL PRIVATE
  ( /*pMimeType as char*/ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def var pMimeType as char.

 if pMimeType = "" 
  then pMimeType = "text/xml".
 
 run outputhttpheader in web-utilities-hdl ("Content-Type":U, pMimeType).
 run outputhttpheader in web-utilities-hdl ("Cache-Control":U, "no-store, no-cache, must-revalidate").
 run outputhttpheader in web-utilities-hdl ("Expires":U, "Sat, 01 Jan 2000 01:00:00 GMT").
 run outputhttpheader in web-utilities-hdl ("Last-Modified":U, format-datetime("HTTP",today,time,"LOCAL")).
 run outputhttpheader in web-utilities-hdl ("Pragma":U, "no-cache").
 run outputhttpheader in web-utilities-hdl ("", "").
 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

