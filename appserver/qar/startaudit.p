/*------------------------------------------------------------------------
@name startaudit.p
@action auditStart
@description Starts the audit
@param AuditID;int;The id of the audit (required)

@throws 3000;The record did not get started
@throws 3021;The Audit Status is not queued
@throws 3067;The audit is not found in the audit table
@throws 3081;The Audit is locked
@throws 3006;Audit file failed to load
@throws 3066;Does not exist

@success 2012;The audit was processed

@author John Oliver
@version 1.0
@created 11/20/2015
@modified 07/18/17 - Anjly Chanana - Modified to send answers 
                                     of Background question
          11/14/18 - Anjly Chanana - Modified to fetch auditstartdate
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

def var piAuditID as int no-undo.
def var iAuditNoteID as int no-undo.

def var tAuditFile as char no-undo.
def var currentDataPath as char no-undo.

def var hPDS as handle no-undo.

{lib/std-def.i}
{tt/qaraudit.i}
{tt/qarbkgquestion.i}


DEFINE DATASET QAR FOR audit, bkgQuestion.

hPDS = DATASET QAR:HANDLE.

pRequest:getParameter("AuditID", OUTPUT piAuditID).
/* validate the audit id is valid */
if not can-find(first qar where qar.qarID = piAuditID)
  then pResponse:fault("3067", "Audit" {&msg-add} "ID" {&msg-add} string(piAuditID)).
 
/* validate that the status is Q */
if not can-find(first qar where qar.qarID = piAuditID and qar.stat = "Q")
  then pResponse:fault("3021", "Audit Status" {&msg-add} "Queued").
 
/* validate that the audit is not locked */
run util/issyslock.p ("Audit", piAuditID, 0, OUTPUT std-lo, OUTPUT std-ch, OUTPUT std-dt).
if std-lo and std-ch <> pRequest:Uid
  then pResponse:fault("3081", "Audit" {&msg-add} std-ch).

PUBLISH "GetSystemParameter" ("dataRoot", OUTPUT currentDataPath).
tAuditFile = currentDataPath + "audit\" + string(piAuditID) + ".xml".

file-info:file-name = tAuditFile.
if file-info:full-pathname = ?
 then
  do: 
    pResponse:fault("3066", "Audit " + string(piAuditID)).
    return.
  end.

std-lo = hPDS:read-xml("file",tAuditFile,"empty",?,false).
if not std-lo
 then
  do: 
    pResponse:fault("3006", "Audit file" {&msg-add} "loading").
    return.
  end.

if pResponse:isFault()
 then return.

std-lo = false.
/* all parameters are valid so start to modify the row */
TRX-BLOCK:
for first qar exclusive-lock
    where qar.qarID = piAuditID TRANSACTION
    on error undo TRX-BLOCK, leave TRX-BLOCK:
      
  assign
    qar.stat = "A"
    qar.auditStartDate = now
    qar.uid = pRequest:Uid
    .

  for first audit where audit.qarID = qar.qarID:
    audit.auditStartDate = qar.auditStartDate.
  end.

  validate qar.
  release qar.
  
  iAuditNoteID = 1.
  for first qarnote no-lock
    where qarnote.qarID = piAuditID
    by qarnote.seq desc:
    iAuditNoteID = iAuditNoteID + qarnote.seq.
  end.
  
  create qarnote.
  assign
    qarnote.qarID = piAuditID
    qarnote.seq = iAuditNoteID
    qarnote.dateCreated = now
    qarnote.uid = pRequest:Uid
    qarnote.secured = true
    qarnote.stat = "A"
    qarnote.comments = "Active"
    .
    
  validate qarnote.
  release qarnote.
  std-lo = true.
end.

if not std-lo
 then
  do: 
    pResponse:fault("3000", "Start").
    return.
  end.

pResponse:setParameter("audit",table audit).
pResponse:setParameter("bkgQuestion",table bkgQuestion).

pResponse:success("2012", "Audit").
delete object hPDS.
