&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : qarrecall.p
    Purpose     : handler for RECALLting a QAR
    Description : Returns the Audit QAR XML document
    Author(s)   : D.Sinclair
    Created     : 9.15.2011
    Input       : XML content
                  <audit auditID="..."/>
  ----------------------------------------------------------------------*/

{qar/qardefs.i}

def var iAuditID as char no-undo.
def var tAuditFilename as char no-undo.
def var isAudit as logical init false no-undo.
def var xDoc as handle.
def var hParser as handle no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

outputHeaders().

if not web-context:is-xml or not valid-handle(web-context:x-document)
 then
  do: outputFault("1", "No XML document posted (text/xml).").
      return.
  end.

 create sax-reader hParser.
 hParser:handler = this-procedure.
 hParser:SET-INPUT-SOURCE("handle", WEB-CONTEXT).
 hParser:suppress-namespace-processing = true.
 hParser:SAX-PARSE() NO-ERROR.
 std-in = hParser:parse-status.
 DELETE OBJECT hParser.

 if std-in = 4
  then
   do: outputFault("2", "Error parsing XML document.").
       return.
   end.

 if not isAudit 
  then
   do: outputFault("3", "Request is not for a QAR.").
       return.
   end.
 
 if iAuditID = ""
  then
   do: outputFault("4", "Audit ID must be set.").
       return.
   end.
   
 tAuditFilename = currentDataPath + "audit\" + iAuditID + ".xml".
 file-info:file-name = tAuditFilename.
 if file-info:full-pathname = ?
  then
   do: outputFault("5", "Audit " + iAuditID + " does not exist.").
       return.
   end.
 

 create x-document xDoc.
 std-lo = xDoc:load("file", tAuditFilename, false).

 if not std-lo
  then
   do: outputFault("6", "Unable to load audit " + tAuditFilename + ".").
       delete object xDoc.
       return.
   end.

 xDoc:save("stream","webstream").
 delete object xDoc.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 DEFINE INPUT PARAMETER namespaceURI AS CHARACTER.
 DEFINE INPUT PARAMETER localName AS CHARACTER.
 DEFINE INPUT PARAMETER qName AS CHARACTER.
 DEFINE INPUT PARAMETER attributes AS HANDLE.

 DEF VAR i AS INT.
 DEF VAR fldvalue AS CHAR.

 case qName:
  when "audit" 
   then 
    do: isAudit = true.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
             WHEN "auditID" THEN
                 iAuditID = fldvalue.
         END CASE.
        END.
    end.
 end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

