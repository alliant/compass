/*------------------------------------------------------------------------
@name GetAuditAccounts.p
@action auditAccountsGet
@description fetch accounts for specific auditId
@param AuditID;int;The id of the audit (required)

@throws 3067;The audit is not found in the audit table

@success 2005;The count of the records returned

@author Anjly Chanana
@version 1.0
@created 06/15/2015
@Modified Yoke Sam C. 06/27/2017 
                      Corrected the statement: "create ttAuditNote" to "create ttAccount"
@modified 08/09/2017 - AC - Midified to show all qarAccount records.
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

def var piAuditID as int no-undo.

{lib/std-def.i}
{tt/qaraccount.i &tableAlias="ttAccount"}

pRequest:getParameter("AuditID", OUTPUT piAuditID).
/* validate the audit id is valid */
if not can-find(first qar where qar.qarID = piAuditID)
 then pResponse:fault("3067", "Audit" {&msg-add} "ID" {&msg-add} string(piAuditID)).

if pResponse:isFault()
 then return.


std-in = 0.
for each QarAccount no-lock
   where QarAccount.qarID = if piAuditID = 0 then QarAccount.qarID else piAuditID:
   
  create ttAccount.
  buffer-copy QarAccount to ttAccount.
  
  std-in = std-in + 1.
end.

IF std-in > 0
 THEN pResponse:setParameter("QarAccount", table ttAccount).

pResponse:success("2005", STRING(std-in) {&msg-add} "QAR Account").
