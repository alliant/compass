&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* qar/actionlist.i
   output of audit ACTION data
   D.Sinclair
   11.8.2012

   MUST BE wrapped by a block with both audit and auditaction records
   
   exclude-note: Defined to not output ActionNote elements
   exclude-stat: Defined to not output ActionStat elements
   */


 {&out} '<Action'.

 std-ch = audit.auditID.
 {&out} ' auditID="' std-ch '"'.

 std-ch = audit.agentID.
 {&out} ' agentID="' std-ch '"'.

 std-ch = encodeForXml(audit.name).
 {&out} ' name="' std-ch '"'.
 
 std-ch = encodeForXml(audit.auditor).
 {&out} ' auditor="' std-ch '"'.
 
 {&out} ' auditDate="' audit.auditDate '"'
        ' auditScore="' audit.score '"'.
 
 std-ch = encodeForXml(audit.city).
 {&out} ' city="' std-ch '"'.
 std-ch = audit.state.
 {&out} ' state="' std-ch '"'.
 std-ch = audit.zip.
 {&out} ' zip="' std-ch '"'.
  
 for each auditsection no-lock
   where auditsection.auditID = audit.auditID:
  std-ch = ' section' + string(auditsection.sectionID, "9") + 'score="'.
  {&out} std-ch auditsection.score '"'.
 end.

 for first auditfinding no-lock
   where auditfinding.findingID = auditaction.findingID:
  {&out} ' sectionID="' auditfinding.sectionID '"'.
  {&out} ' questionID="' auditfinding.questionID '"'.
  {&out} ' priority="' auditfinding.severity '"'.
  {&out} ' description="' encodeForXml(auditfinding.description) '"'.
  {&out} ' finding="' encodeForXml(auditfinding.comments) '"'.
  {&out} ' files="' encodeForXml(auditfinding.files) '"'.
  {&out} ' reference="' encodeForXml(auditfinding.reference) '"'.
 end.
 
 {&out} ' actionID="' auditaction.actionID '"'.
 {&out} ' findingID="' auditaction.findingID '"'.
 
 {&out} ' actionType="' auditaction.actiontype '"'.

 {&out} ' dueDate="' (if auditaction.duedate = ? 
                       then '' 
                       else string(auditaction.duedate)) '"'.
 {&out} ' followupDate="' (if auditaction.followupdate = ? 
                           then '' 
                           else string(auditaction.followupdate)) '"'.

 {&out} ' status="' auditaction.stat '"'.
 
 std-ch = encodeForXml(auditaction.comments).
 {&out} ' comments="' std-ch '"'.

 {&out} '/>' skip.

 &IF defined(exclude-note) = 0 &THEN
 for each auditactionnote no-lock
   where auditactionnote.actionID = auditaction.actionID:
  {&out} '<ActionNote'.
  {&out} ' actionID="' auditactionnote.actionID '"'.
  {&out} ' seq="' auditactionnote.noteseq '"'.
  {&out} ' noteDate="' auditactionnote.noteDate '"'.
  {&out} ' takenBy="' encodeForXml(auditactionnote.takenby) '"'.
  {&out} ' userid="' auditactionnote.uid '"'.
  std-ch = encodeForXml(auditactionnote.comments).
  {&out} ' comments="' std-ch '"'.
  {&out} ' secured="' auditactionnote.secured '"'.
  {&out} '/>' skip.
 end.
 &ENDIF

 &IF defined(exclude-stat) = 0 &THEN
 for each auditactionstat no-lock
   where auditactionstat.actionID = auditaction.actionID:
  {&out} '<ActionStatus'.
  {&out} ' actionID="' auditactionstat.actionID '"'.
  {&out} ' seq="' auditactionstat.statseq '"'.
  {&out} ' status="' auditactionstat.stat '"'.
  {&out} ' statDate="' auditactionstat.statDate '"'.
  {&out} ' userid="' auditactionstat.uid '"'.
  std-ch = encodeForXml(auditactionstat.comments).
  {&out} ' comments="' std-ch '"'.
  {&out} '/>' skip.
 end.
 &ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


