&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file deleteqarauditor.p
@description Delete a qar auditor

@param QarID;int;The QAR ID
@param Uid;char;The Uid

@success 2000;The auditor was deleted

@uthor John Oliver
@created 10.23.2017
@notes
----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEF INPUT PARAM pRequest AS service.IRequest.
DEF INPUT PARAM pResponse AS service.IResponse.

{lib/std-def.i}
{lib/encrypt.i}

/* variables for getParameter */
define variable pQarID as integer no-undo.
define variable pUid as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* Start getting the parameters */
pRequest:getParameter("QarID", OUTPUT pQarID).
if not can-find(first qar where qarID = pQarID)
 then pResponse:fault("3066", "Audit " + string(pQarID)).
 
pRequest:getParameter("Uid", output pUid).
if not can-find(first sysuser where uid = pUid)
 then pResponse:fault("3066", "System User " + pUid).

/* if there is a fault thrown within the procedure, exit */
if pResponse:isFault()
 then return.
 
/* all parameters are valid so start to insert the new row */
std-lo = false.
TRX-BLOCK:
for first qarauditor exclusive-lock
    where qarauditor.qarID = pQarID
      and qarauditor.uid = pUid TRANSACTION
    on error undo TRX-BLOCK, leave TRX-BLOCK:
  
  delete qarauditor.  
  std-lo = true.
end.

if not std-lo
 then
  do: pResponse:fault("3002", "Delete").
      return.
  end.

pResponse:success("2000", "Delete").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME