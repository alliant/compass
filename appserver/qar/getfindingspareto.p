/*------------------------------------------------------------------------
@name GetFindingsPareto.p
@action GetFindingsPareto
@description Fetch all findings depending upon parameters
@param Year;int;The year of the audit (optional)
@param StateID;char;The state the audit is in (optional)
@param AgentID;char;The agent the audit is regarding (optional)
@param UID;char;The userid that is conducting the audit (optional)
@param FindingType;int; (optional)

@throws 3067;The audit is not found in the audit table

@success 2005;The count of the records returned

@author Anjly Chanana
@version 1.0
@created 06/19/2017
@modified
16/12/21  SA  Task#:86696 finding report related field added
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.


def var piYear    as int no-undo.
def var pcAgentID as char no-undo.
def var pcUID     as char no-undo.
def var pcState   as char no-undo.
def var piFindingType as int no-undo.
def var totalCnt  as int no-undo.

{lib/std-def.i}
{tt/findingpareto.i}

pRequest:getParameter("Year", OUTPUT piYear).
pRequest:getParameter("Agent", OUTPUT pcAgentID).
pRequest:getParameter("Auditor", OUTPUT pcUID).
pRequest:getParameter("State", OUTPUT pcState).
pRequest:getParameter("FindingType", OUTPUT piFindingType).

/*validate if the year is valid*/
if piYear = 0 then.
else
do:
 date(1, 1, piYear) NO-ERROR.
 IF ERROR-STATUS:ERROR
  then pResponse:fault("3001", "Year").
end.


if pResponse:isFault()
 then return.

std-in = 0.
for each qar fields(qarID version stateID agentID uid) no-lock
  where qar.auditYear = if piYear > 0 then piYear else qar.auditYear :
    
  if (pcState > "" and lookup(qar.stateID, pcState) = 0)
   then next.
    
  if (pcAgentID > "" and lookup(qar.agentID, pcAgentID) = 0)
   then next.
    
  if (pcUid > "" and lookup(qar.uid, pcUid) = 0)
   then next.

  for each qarfinding fields(severity questionSeq questionID description ) no-lock 
    where qarfinding.qarID = qar.qarID and qarfinding.questionID ne ? :
    
     if piFindingType > 0 and qarfinding.severity <> piFindingType 
      then next. 

    find findingpareto 
      where findingpareto.questionSeq = qarfinding.questionSeq  no-error.
    
    if not available findingpareto 
    then
    do:
      create findingpareto.
      assign
        findingpareto.questionID  = qarfinding.questionID
        findingpareto.description = qarfinding.description
        findingpareto.priority    = qarfinding.severity
        findingpareto.type        = entry(qarfinding.severity, "Minor,Intermediate,Major")
        findingpareto.ver         = qar.version
        findingpareto.questionSeq = qarfinding.questionSeq
        .
    end.
    findingpareto.cnt = findingpareto.cnt + 1.
    totalCnt = totalCnt + 1.
    std-in = std-in + 1.
  end.
  if totalCnt > 0 
  then
    for each findingpareto:
      findingpareto.pct = (findingpareto.cnt / totalCnt) * 100.
    end.
end.
  if std-in > 0
  then pResponse:setParameter("Finding", table findingpareto).
  
  pResponse:success("2005", STRING(std-in) {&msg-add} "Finding").
