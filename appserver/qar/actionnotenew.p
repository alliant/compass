&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : actionnotenew.p
    Purpose     : Entry point for a NOTE to an existing audit ACTION
    Syntax      :
    Description : 
    Author(s)   : D.Sinclair
    Created     : 3.20.2012
  ----------------------------------------------------------------------*/
{qar/qardefs.i}

/* Variables to hold results of parsing */
def var iActionID as int no-undo.
def var iComments as char no-undo.

def var isValid as logical init false no-undo.

def var xDoc as handle.
def var hParser as handle no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

 outputHeaders().
 
 run parsePayload in this-procedure (output std-lo). /* Sets vars only */
 if std-lo 
  then return. /* error while parsing */
 
 std-lo = false.
 for first auditaction exclusive-lock
   where auditaction.actionID = iActionID:

  std-in = 1.
  for last auditactionnote no-lock
    where auditactionnote.actionID = iActionID
    by auditactionnote.noteseq:
   std-in = auditactionnote.noteseq + 1.
  end.

  create auditactionnote.
  auditactionnote.actionID = iActionID.
  auditactionnote.noteseq = std-in.
  validate auditactionnote.

  assign
    auditactionnote.notedate = now
    auditactionnote.uid = currentUserid
    auditactionnote.takenby = currentUserName
    auditactionnote.comments = iComments
    .

  validate auditactionnote.
  std-lo = true.
 end.

 if not std-lo
  then
   do: outputFault("10", "Action does not exist").
       return.
   end.


 if currentAuditing 
  then .

 /* Return content that indicates it was successfully posted */
 {&out} '<?xml version="1.0"?>' skip.
 {&out} '<ActionNote status="created"/>' skip.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-parsePayload) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parsePayload Procedure 
PROCEDURE parsePayload :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pErr as logical init true.
 def var tParseStatus as int.

 if not web-context:is-xml or not valid-handle(web-context:x-document)
  then
   do: outputFault("1", "No XML document posted (text/xml).").
       return.
   end.

 create sax-reader hParser.
 hParser:handler = this-procedure.
 hParser:SET-INPUT-SOURCE("handle", WEB-CONTEXT).
 hParser:suppress-namespace-processing = true.
 hParser:SAX-PARSE() NO-ERROR.
 tParseStatus = hParser:parse-status.
 DELETE OBJECT hParser.

 if tParseStatus = 4
  then
   do: outputFault("2", "Error parsing XML document.").
       return.
   end.

 if not isValid
  then
   do: outputFault("3", "Input data is not valid.").
       return.
   end.
 
 if iActionID = 0 
  then
   do: outputFault("4", "Action must be set.").
       return.
   end.

 if iComments = ? or iComments = "" 
  then
   do: outputFault("5", "Comments must be set.").
       return.
   end.

 pErr = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 DEFINE INPUT PARAMETER namespaceURI AS CHARACTER.
 DEFINE INPUT PARAMETER localName AS CHARACTER.
 DEFINE INPUT PARAMETER qName AS CHARACTER.
 DEFINE INPUT PARAMETER attributes AS HANDLE.

 DEF VAR i AS INT.
 DEF VAR fldvalue AS CHAR.

 /* These should decodeFromXml() to facilitate the shared functions that produce
    the subsequent XML doc in saveAudit */
 case qName:
  when "ActionNote" 
   then 
    do: isValid = true.
        assign
          iActionID = 0
          iComments = ""
          .
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
             WHEN "actionID" THEN
                 iActionID = int(fldvalue).
             WHEN "comments" THEN
                 iComments = decodeFromXml(fldvalue).
         END CASE.
        END.
    end.
 end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

