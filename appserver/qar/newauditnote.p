/*------------------------------------------------------------------------
@name newauditnote.p
@action auditNoteNew
@description Creates a new audit note in the qarnote table
@param AuditID;int;The audit id (required)
@param Comments;char;The comments for the note

@throws 3000;The record did not get created
@throws 3067;The audit record is not found in the qar table

@success 2002;The record was created

@author John Oliver
@version 1.0
@created 11/20/2015
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

def var piAuditID as int no-undo.
def var pcComments as char no-undo.

def var iAuditNoteID as int no-undo.

{lib/std-def.i}

pRequest:getParameter("AuditID", OUTPUT piAuditID).
/* validate the audit id is valid */
if not can-find(first qar where qar.qarID = piAuditID)
 then pResponse:fault("3067", "Audit" {&msg-add} "ID" {&msg-add} string(piAuditID)).
 
pRequest:getParameter("Comments", OUTPUT pcComments).
 
if pResponse:isFault()
 then return.

/* all parameters are valid so start to insert the new row */
std-lo = false.
TRX-BLOCK:
do TRANSACTION
  on error undo TRX-BLOCK, leave TRX-BLOCK:
  
  iAuditNoteID = 1.
  for first qarnote no-lock
      where qarnote.qarID = piAuditID
         by qarnote.seq desc:
    iAuditNoteID = iAuditNoteID + qarnote.seq.
  end.
  
  create qarnote.
  assign
    qarnote.qarID = piAuditID
    qarnote.seq = iAuditNoteID
    qarnote.dateCreated = now
    qarnote.uid = pRequest:Uid
    qarnote.secured = false
    qarnote.comments = pcComments
    .
    
  validate qarnote.
  release qarnote.
  
  std-lo = true.
end.

if not std-lo
 then
  do: pResponse:fault("3000", "Create").
      return.
  end.

pResponse:success("2002", "Audit Note").
