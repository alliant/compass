&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : actionget.p
    Purpose     : handler for GETting an audit ACTION
    Author(s)   : D.Sinclair
    Created     : 9.15.2011
    Notes       :
  ----------------------------------------------------------------------*/

{qar/qardefs.i}

def var iActionID as int no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

iActionID = int(get-value("actionID")) no-error.

outputHeaders().
{&out} '<?xml version="1.0"?>' skip.
{&out} '<Data>' skip.
{&out} '<Parameter name="actionID" value="' get-value("actionID") '"/>' skip.
/* {&out} '<Parameter name="actionIDderived" value="' iActionID '"/>' skip. */

for first auditaction no-lock
  where auditaction.actionID = iActionID:
 for first audit no-lock
   where audit.auditID = auditaction.auditID:
 end.
 {qar/actionlist.i}
end.

{&out} '</Data>' skip.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


