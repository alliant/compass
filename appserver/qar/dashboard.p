&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : dashboardca.p
    Purpose     : handler for aggregating Corrective Actions in a DASHBOARD
    Author(s)   : D.Sinclair
    Created     : 9.15.2011
    Notes       :
  ----------------------------------------------------------------------*/

{qar/qardefs.i}

def temp-table ttstate
 field state as char
 field periodStart as date
 field numAudits as int
 field numMajor as int
 field numInter as int
 field numMinor as int
 field numSuggestions as int      /* severity = 1 */
 field numRecommendations as int  /* severity = 2 */
 field numCA as int               /* severity = 3 */
 field numOpen as int
 field numInProcess as int
 field numComplete as int
 field numRejected as int
 field totalScore as int
 field minScore as int
 field maxScore as int
 index pi-state state periodStart
 .

def temp-table ttauditor
 field auditor as char
 field periodStart as date
 field numAudits as int
 field numMajor as int
 field numInter as int
 field numMinor as int
 field numSuggestions as int      /* severity = 1 */
 field numRecommendations as int  /* severity = 2 */
 field numCA as int               /* severity = 3 */
 field numOpen as int
 field numInProcess as int
 field numComplete as int
 field numRejected as int
 field totalScore as int
 field minScore as int
 field maxScore as int
 index pi-auditor auditor periodStart
 .

def var tStartDate as date no-undo.

def var tTotalAudits as int no-undo.
def var tTotalFindings as int no-undo.
def var tTotalActions as int no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

def buffer allstate for ttstate.

for each audit fields (auditID auditDate state auditor score) no-lock:

 tStartDate = date(month(audit.auditDate), 1, year(audit.auditDate)).

 find allstate
   where allstate.state = "All"
     and allstate.periodStart = tStartDate no-error.
 if not available allstate 
  then
   do: create allstate.
       allstate.state = "All".
       allstate.periodStart = tStartDate.
       allstate.minScore = -1.
   end.

 allstate.numAudits = allstate.numAudits + 1.
 allstate.totalScore = allstate.totalScore + audit.score.

 if allstate.minScore = -1 
  then assign
         allstate.minScore = audit.score
         allstate.maxScore = audit.score
         .
  else
   do: if audit.score < allstate.minScore
        then allstate.minScore = audit.score.
       if audit.score > allstate.maxScore 
        then allstate.maxScore = audit.score.
   end.


 find ttstate
   where ttstate.state = audit.state
     and ttstate.periodStart = tStartDate no-error.
 if not available ttstate 
  then
   do: create ttstate.
       ttstate.state = audit.state.
       ttstate.periodStart = tStartDate.
       ttstate.minScore = -1.
   end.

 find ttauditor 
   where ttauditor.auditor = audit.auditor
     and ttauditor.periodStart = tStartDate no-error.
 if not available ttauditor 
  then
   do: create ttauditor.
       ttauditor.auditor = audit.auditor.
       ttauditor.periodStart = tStartDate.
       ttauditor.minScore = -1.
   end.

 ttstate.numAudits = ttstate.numAudits + 1.
 ttstate.totalScore = ttstate.totalScore + audit.score.

 if ttstate.minScore = -1 
  then assign
         ttstate.minScore = audit.score
         ttstate.maxScore = audit.score
         .
  else
   do: if audit.score < ttstate.minScore
        then ttstate.minScore = audit.score.
       if audit.score > ttstate.maxScore 
        then ttstate.maxScore = audit.score.
   end.


 ttauditor.numAudits = ttauditor.numAudits + 1.
 ttauditor.totalScore = ttauditor.totalScore + audit.score.

 if ttauditor.minScore = -1 
  then assign
         ttauditor.minScore = audit.score
         ttauditor.maxScore = audit.score
         .
  else
   do: if audit.score < ttauditor.minScore
        then ttauditor.minScore = audit.score.
       if audit.score > ttauditor.maxScore 
        then ttauditor.maxScore = audit.score.
   end.


 tTotalAudits = tTotalAudits + 1.

 for each auditfinding fields (auditID findingID questionID severity) no-lock
  where auditfinding.auditID = audit.auditID:

  case auditfinding.severity:
   when 1 then assign
                 allstate.numMinor = allstate.numMinor + 1
                 ttstate.numMinor = ttstate.numMinor + 1
                 ttauditor.numMinor = ttauditor.numMinor + 1
                 .
   when 2 then assign
                 allstate.numInter = allstate.numInter + 1
                 ttstate.numInter = ttstate.numInter + 1
                 ttauditor.numInter = ttauditor.numInter + 1
                 .
   when 3 then assign
                 allstate.numMajor = allstate.numMajor + 1
                 ttstate.numMajor = ttstate.numMajor + 1
                 ttauditor.numMajor = ttauditor.numMajor + 1
                 .
  end case.
  tTotalFindings = tTotalFindings + 1.

  for each auditaction fields (auditID actionID findingID stat) no-lock
    where auditaction.auditID = audit.auditID
      and auditaction.findingID = auditfinding.findingID:

   case auditfinding.severity:
    when 1 then assign
                  allstate.numSuggestions = allstate.numSuggestions + 1
                  ttstate.numSuggestions = ttstate.numSuggestions + 1
                  ttauditor.numSuggestions = ttauditor.numSuggestions + 1
                  .
    when 2 then assign
                  allstate.numRecommendations = allstate.numRecommendations + 1
                  ttstate.numRecommendations = ttstate.numRecommendations + 1
                  ttauditor.numRecommendations = ttauditor.numRecommendations + 1
                  .
    when 3 then assign
                  allstate.numCA = allstate.numCA + 1
                  ttstate.numCA = ttstate.numCA + 1
                  ttauditor.numCA = ttauditor.numCA + 1
                  .
   end case.
   tTotalActions = tTotalActions + 1.

   if auditfinding.severity < 3 
    then next.

   case auditaction.stat:
    when "O" then assign
                    allstate.numOpen = allstate.numOpen + 1
                    ttstate.numOpen = ttstate.numOpen + 1
                    ttauditor.numOpen = ttauditor.numOpen + 1
                    .
    when "I" then assign
                    allstate.numInProcess = allstate.numInProcess + 1
                    ttstate.numInProcess = ttstate.numInProcess + 1
                    ttauditor.numInProcess = ttauditor.numInProcess + 1
                    .
    when "C" then assign
                    allstate.numComplete = allstate.numComplete + 1
                    ttstate.numComplete = ttstate.numComplete + 1
                    ttauditor.numComplete = ttauditor.numComplete + 1
                    .
    when "R" then assign
                    allstate.numRejected = allstate.numRejected + 1
                    ttstate.numRejected = ttstate.numRejected + 1
                    ttauditor.numRejected = ttauditor.numRejected + 1
                    .
   end case.
  end.
 end.
end.

outputHeaders().

{&out} '<?xml version="1.0"?>' skip.
{&out} '<Data>' skip.
{&out} '<Parameter name="totalAudits" value="' string(tTotalAudits) '"/>' skip.
{&out} '<Parameter name="totalFindings" value="' string(tTotalFindings) '"/>' skip.
{&out} '<Parameter name="totalActions" value="' string(tTotalActions) '"/>' skip.

for each ttstate
  by ttstate.state
  by ttstate.periodStart:
 {&out} '<State'.
 {&out} ' state="' ttstate.state '"'.
 {&out} ' period="' ttstate.periodStart '"'.
 {&out} ' numAudits="' ttstate.numAudits '"'.
 {&out} ' numMajor="' ttstate.numMajor '"'.
 {&out} ' numInter="' ttstate.numInter '"'.
 {&out} ' numMinor="' ttstate.numMinor '"'.
 {&out} ' numSuggestions="' ttstate.numSuggestions '"'.
 {&out} ' numRecommendations="' ttstate.numRecommendations '"'.
 {&out} ' numCorrective="' ttstate.numCA '"'.
 {&out} ' numOpen="' ttstate.numOpen '"'.
 {&out} ' numInProcess="' ttstate.numInProcess '"'.
 {&out} ' numComplete="' ttstate.numComplete '"'.
 {&out} ' numRejected="' ttstate.numRejected '"'.
 {&out} ' totalScore="' ttstate.totalScore '"'.
 {&out} ' minScore="' ttstate.minScore '"'.
 {&out} ' maxScore="' ttstate.maxScore '"/>'.
end.

for each ttauditor
  by ttauditor.auditor
  by ttauditor.periodStart:
 {&out} '<Auditor'.
 {&out} ' auditor="' encodeForXml(ttauditor.auditor) '"'.
 {&out} ' period="' ttauditor.periodStart '"'.
 {&out} ' numAudits="' ttauditor.numAudits '"'.
 {&out} ' numMajor="' ttauditor.numMajor '"'.
 {&out} ' numInter="' ttauditor.numInter '"'.
 {&out} ' numMinor="' ttauditor.numMinor '"'.
 {&out} ' numSuggestions="' ttauditor.numSuggestions '"'.
 {&out} ' numRecommendations="' ttauditor.numRecommendations '"'.
 {&out} ' numCorrective="' ttauditor.numCA '"'.
 {&out} ' numOpen="' ttauditor.numOpen '"'.
 {&out} ' numInProcess="' ttauditor.numInProcess '"'.
 {&out} ' numComplete="' ttauditor.numComplete '"'.
 {&out} ' numRejected="' ttauditor.numRejected '"'.
 {&out} ' totalScore="' ttauditor.totalScore '"'.
 {&out} ' minScore="' ttauditor.minScore '"'.
 {&out} ' maxScore="' ttauditor.maxScore '"/>'.
end.

{&out} '</Data>' skip.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


