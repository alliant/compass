/*------------------------------------------------------------------------
@name sendactionalerts.p
@action ActionAlertSend
@description send notification mail for actions that are past due or due within X days.

@param ttAct - Temp-table,Contains actionID and Requester Comments.  
@Return iCountEmail;int;Number of emails sent 
        tAction; Temp-table for action.
@success 2000;The reminder notification was successful.

@author Sachin Chaturvedi
@version 1.0
@created 09/26/2017
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

def var hParser     as handle no-undo.

{tt/file.i}
{lib/xmlencode.i}
{lib/std-def.i}
{tt/action.i &tableAlias="ttAction"} 
def temp-table ttAct
    field actionID  as int
    field lastNote  as char.

def temp-table taction like ttaction.

def var pcActionList       as char     no-undo.
def var cComments          as char     no-undo.
def var cEntity            as char     no-undo.
def var cEntityID          as char     no-undo.
def var cEntityname        as char     no-undo.
def var cEntityNameID      as char     no-undo.
def var cOwnerID           as char     no-undo.
def var cOwnerName         as char     no-undo.
def var cLastUserName      as char     no-undo.
def var cLastUserID        as char     no-undo.
def var cLastUserNote      as char     no-undo.
def var cBody              as char     no-undo.
def var RequestEmail       as char     no-undo.
def var iCountLoop         as int      no-undo.
def var iCountEmail        as int      no-undo.
def var iActionID          as int      no-undo.
def var iActionNoteID      as int      no-undo.
def var dDueDate           as datetime no-undo.
def var cRequesterComments as char     no-undo. 
def var cDueMsg            as char     no-undo.

if not web-context:is-xml or not valid-handle(web-context:x-document) then
do: 
  pResponse:fault("3005", "No XML file attached.").
  return.
end.
  
create sax-reader hParser.
hParser:handler = this-procedure.
hParser:SET-INPUT-SOURCE("handle", WEB-CONTEXT).
hParser:suppress-namespace-processing = true.
hParser:SAX-PARSE() NO-ERROR.
std-in = hParser:parse-status.
DELETE OBJECT hParser.

TRX-BLOCK:
for each ttAct transaction
      on error undo TRX-BLOCK, next TRX-BLOCK:
  iActionID = ttAct.actionID.

  /* validate the action id is valid */
  if not can-find(first action where action.actionID = iActionID)
    then pResponse:fault("3067", "Action" {&msg-add} "ID" {&msg-add} string(iActionID)).

  if pResponse:isFault()
    then next TRX-BLOCK.
  
  /* all parameters are valid so start to send the notification */
  
  for first action exclusive-lock
      where action.actionID = iActionID :
    find first finding no-lock where finding.findingID = action.findingID no-error.
    if available finding then do:
      {lib/getentityname.i &entity=finding.entity &entityId=finding.entityId &entityname=cEntityname}
      assign
        cEntity   = finding.entity
        cEntityID = finding.entityID.
    end.
     
    cRequesterComments = if ttAct.lastNote ne "" then ttAct.lastNote else " ".

    assign
      cOwnerID   = action.ownerID
      dDueDate   = action.dueDate
      dDueDate   = if dDueDate = ? then action.dueDate else dDueDate
      cComments  = if action.comments ne "" then action.comments else "".
    
    cDueMsg = if (date(dDueDate) - today) ge 0 then "coming due" else "past due".

    for first sysuser where sysuser.uid = pRequest:uid no-lock:
      assign
       RequestEmail = sysuser.email.
    end.

    /* to get last note by any user */
    for first actionnote no-lock
      where actionnote.actionID = iActionID and
            actionnote.method  = "User"
         by actionnote.seq desc:
      assign 
          cLastUserID  =  actionnote.uid
          cLastUserNote = if actionnote.comments ne "?" then actionnote.comments else "".
    end.
    {lib/getusername.i &var=cLastUserName &user=cLastUserID}

    cEntityNameID = if cEntity = "Company" then "" else (' ' + cEntityName + ' (' + cEntityID + ')').
    cBody =  'The action' +  
              ' for ' + cEntity + ':' + cEntityNameID  + 
              ' is ' + cDueMsg + ' on ' + string(dDueDate, "99/99/99") + '.' + ' <br> <br> '  + 
              'Action:' + ' <br> ' + cComments + ' <br> <br> ' +
               
              'Last Note by ' + cLastUserName + ': <br>' + 
               cLastUserNote + ' <br> <br> ' +   
              'For any questions please contact ' + 
               RequestEmail + '.' + '<br><br>' +
               cRequesterComments.
    
    run util/simplemail(input RequestEmail,
                        input cOwnerID,
                        input "Reminder that Action #" + string(iActionID) + " is " + cDueMsg,
                        input cBody ).

    {lib/getusername.i &var=cOwnerName &user=cOwnerId}

    iActionNoteID = 1.
    for first actionnote no-lock
        where actionnote.actionID = iActionID
           by actionnote.seq desc:
      iActionNoteID = iActionNoteID + actionnote.seq.
    end.
    
    create actionnote.
    assign
      actionnote.actionID    = iActionID
      actionnote.seq         = iActionNoteID
      actionnote.category    = "System"
      actionnote.method      = "System"
      actionnote.dateCreated = now
      actionnote.uid         = pRequest:Uid
      actionnote.secured     = true
      actionnote.stat        = action.stat
      actionnote.comments    = 'Reminder sent to owner "' + cOwnerName + '" for action due on ' + string(dDueDate, "99/99/99") + '. ' + cRequesterComments.
    validate actionnote.
    release actionnote.
    validate action.
    run action/getaction.p(input action.actionId , output table ttaction). 

    find first ttaction no-error.  
    if available ttaction then
    do:
      create taction.
      buffer-copy ttaction to taction.
    end.

    release action.
    iCountEmail = iCountEmail + 1.
  end. 
end. /* TRX-BLOCK */

pResponse:setParameter("Count", iCountEmail).
pResponse:setParameter("Action", table tAction).
pResponse:success("2000", "Reminder email alert").

/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var parseCode as char no-undo.
  def var parseStatus as logical no-undo.
  def var parseMsg as char no-undo.
  def var oldFormat as char no-undo.

  {lib/srvstartelement.i}

  if qName = "Action"
  then
    do: 
      create ttAct.
      DO  i = 1 TO  attributes:NUM-ITEMS:
        fldvalue = attributes:get-value-by-index(i).
        CASE attributes:get-qname-by-index(i):	
          when "actionID" then ttAct.actionID = int(fldvalue) no-error.
          when "lastNote" then ttAct.lastNote = fldvalue.
        end case.
      end.
  end.
END PROCEDURE.
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF
