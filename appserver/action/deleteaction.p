/*------------------------------------------------------------------------
@name deleteaction.p
@action actionDelete
@description Deletes an finding and action from the database
@param FindingID;int;The id of the finding (required)

@throws 3000;The delete failed
@throws 3067;The Finding ID is invalid

@success 2008;The Finding was deleted

@author Anjly Chanana
@version 1.0
@created 09/04/2017
@modified 02/2018 - YS - Activated log message
----------------------------------------------------------------------*/

def input parameter pRequest as service.IRequest.
def input parameter pResponse as service.IResponse.

def var piFindingID as int no-undo.

def var pRefType as char no-undo.
def var pRefNum as char no-undo.
def var pRefMessage as char no-undo.

{lib/std-def.i}

pRequest:getParameter("FindingID", OUTPUT piFindingID).


/* validate the audit id is valid */
if not can-find(first finding where finding.findingID = piFindingID)
 then pResponse:fault("3067", "Finding" {&msg-add} "ID" {&msg-add} string(piFindingID)).

if pResponse:isFault()
 then return.

find first finding no-lock where finding.findingID = piFindingID no-error.

pRefType = finding.refType.
pRefNum = string(finding.findingID).
pRefMessage = "Entity: " + finding.entity + " / " +
              "Entity ID: " + finding.entityID + " / " +
              "Source: " + finding.source + " / " +
              "Source ID: " + finding.sourceID
              .
pResponse:logMessage(pRefType, pRefNum, pRefMessage).

std-lo = false.
TRX-BLOCK:
for first finding exclusive-lock
    where finding.findingID = piFindingID TRANSACTION
    on error undo TRX-BLOCK, leave TRX-BLOCK:
      
  for each findingNote exclusive-lock
     where findingNote.findingID = findingNote.findingID:
     
     delete findingNote.
     release findingNote.
  end.

  for each action exclusive-lock
    where action.findingID = finding.findingID:
      
    for each actionNote exclusive-lock
      where actionNote.actionID = action.actionID:
     
      delete actionNote.
      release actionNote.
    end.
  
    delete action.
    release action.
  end.
  delete finding.
  release finding.
  std-lo = true.
end.

if std-lo
 then pResponse:success("2008", "Finding").
 else pResponse:fault("3000", "Delete").
