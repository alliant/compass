&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file loadarcoffices.p
@description Load the Compass arcoffice table

@created 02.12.2020
@author John Oliver
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

define input parameter pRequest  as service.IRequest.
define input parameter pResponse as service.IResponse.

/* parmaeters */

/* variables */
define variable tNewOffice as character no-undo.
define variable tUpdOffice as character no-undo.
define variable tDelOffice as character no-undo.
define variable tCount     as integer   no-undo.
{lib/std-def.i}

/* temp tables */
{tt/arcoffice.i}
{tt/office.i &tableAlias="tempoffice"}

/* functions */
{lib/add-delimiter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

run agent/getarcoffices.p ("", /* agentID, blank=All */
                           output table arcoffice,
                           output std-lo,
                           output std-ch).
                           
if not std-lo
 then
  do:
    pResponse:fault("3005", std-ch).
    return.
  end.
  
/* get all ARC offices into the office temp-table */
for each arcoffice no-lock:
  /* only take offices with a valid agent ID */
  if not can-find(first agent where agentID = arcoffice.AgentID)
   then next.

  create tempoffice.
  assign
    tempoffice.officeID = arcoffice.OfficeID
    tempoffice.name     = arcoffice.OfficeName
    tempoffice.agentId  = arcoffice.AgentID
    tempoffice.stat     = arcoffice.Stat
    tempoffice.addr1    = arcoffice.Address1   
    tempoffice.addr2    = arcoffice.Address2  
    tempoffice.city     = arcoffice.City   
    tempoffice.state    = arcoffice.State
    tempoffice.zipcode  = arcoffice.Zipcode
    tempoffice.phone    = arcoffice.Phone  
    tempoffice.fax      = arcoffice.Fax    
    tempoffice.website  = arcoffice.Website
    tempoffice.email    = arcoffice.Email
    .
end.
    
/* new and update */
TRX-BLOCK:
for each tempoffice TRANSACTION
      on error undo TRX-BLOCK, leave TRX-BLOCK:
      
  /* create the office if not there */
  if not can-find(first office where officeID = tempoffice.OfficeID)
   then
    do:
      tNewOffice = addDelimiter(tNewOffice, ",") + string(tempoffice.officeID).
      create office.
      buffer-copy tempoffice to office.
    end.
   else
    do:
      /* update any fields on the office */
      for first office exclusive-lock
          where office.officeID = tempoffice.OfficeID.
         
        buffer-compare tempoffice to office save result in std-lo.
        if not std-lo
         then 
          do:
            tUpdOffice = addDelimiter(tUpdOffice, ",") + string(tempoffice.officeID).
            buffer-copy tempoffice to office.
          end.
      end.
    end.
end.

/* delete */
DEL-BLOCK:
for each office TRANSACTION
      on error undo DEL-BLOCK, leave DEL-BLOCK:
  
  if not can-find(first tempoffice where officeID = office.officeID)
   then 
    do:
      tDelOffice = addDelimiter(tDelOffice, ",") + string(office.officeID).
      delete office.
    end.
end.

tCount = num-entries(tNewOffice) + num-entries(tUpdOffice) + num-entries(tDelOffice).
pResponse:logMessage("", string(tCount) + " office(s) processed", "New Offices: " + (if tNewOffice = "" then "None" else tNewOffice) + ";Updated Offices: " + (if tUpdOffice = "" then "None" else tUpdOffice) + ";Deleted Offices: " + (if tDelOffice = "" then "None" else tDelOffice)).

pResponse:success("2013", string(tCount) {&msg-add} "Office").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

