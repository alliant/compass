&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file agencyreport.p
@purpose Create a CSV report of of agency reporting

@author John Oliver
@created 02.19.2018
@modification
Date          Name          Description
10/08/2020    VJ            Moved agent filter logic into sql
11/26/2021    Vignesh       Modified to get the html file path value from 
                            pRequest:emailHtml instead of .ini file
01/25/2022    Shefali       Task:89648 Stored procedure call changed form "appserver\call-stored-procedure.i" to "appserver\lib\callsp.i"			      
------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
define input parameter pRequest as service.IRequest.
define input parameter pResponse as service.IResponse.

/* parameters */
define variable pDate    as datetime no-undo.
define variable pTo      as character no-undo.
define variable pSuccess as logical no-undo initial false.
define variable pMsg     as character no-undo.
define variable pAgentID as character no-undo.

/* variables */
{lib/std-def.i}
{lib/callsp-defs.i }
define variable cAgentList as character no-undo.
define variable iWorkDays  as integer no-undo.
define variable cHTML      as character no-undo.
define variable dWorkday   as datetime no-undo.
define variable iWorkMonth as integer no-undo.
define variable iDayCount  as integer no-undo.

/* the current month counter */
define variable iCounter   as integer no-undo.
define variable iPos       as integer no-undo.
define variable iPrevStart as integer no-undo.

&scoped-define TotalLines 100

/* constants */
&scoped-define FormatNumber "-ZZZ,ZZZ"
&scoped-define FormatNumberZero ""
&scoped-define FormatPercent "-ZZ,ZZ9.9%"
&scoped-define FormatPercentZero "-  %"
&scoped-define FormatMoney "$-ZZ,ZZZ,ZZZ"        
&scoped-define FormatMoneyZero "$       -   "

/* used for the To: loop */
define variable i      as integer no-undo.
define variable cEmail as character no-undo.
define variable cUser  as character no-undo.

define variable tUrl            as character no-undo initial "".
define variable tDataFile       as character no-undo.
define variable tTraceFile      as character no-undo.
define variable tResponseFile   as character no-undo.
define variable tValue          as character no-undo.
define variable tCurrentElement as character no-undo.

/* to identify the current agent manager */
define variable cManager        as character no-undo.
define variable cManagerName   as character no-undo.

/* These are used inside xml/xmlparse.i */
&scoped-define traceFile tTraceFile
&scoped-define xmlFile tResponseFile

/* procedures/functions */
{lib/add-delimiter.i}
{lib/count-days.i}
{lib/getmonthname.i}
{lib/get-last-day.i}
{lib/rpt-def.i &ID="'Agency'" &rptOrientation="'Landscape'" &no-startpage=true &lines={&TotalLines}}
{lib/xmlparse.i &exclude-startElement=true}
{lib/arc-bearer-token.i}

/* temp table */
{tt/agencyreport.i}
{tt/agencyreport.i &tableAlias="tempagencyreport"}
{tt/dailycount.i}
{tt/mailparameter.i}
{tt/agentfilter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

pRequest:getParameter("To", output pTo).
pRequest:getParameter("Date", output pDate).
if pDate = ?
 then pDate = today.

publish "GetSystemParameter" ("ARCGetDailyCount", output tUrl).
if tUrl = ""
 then pResponse:fault("3001", "ARC Daily Count URL").
   
cHTML = search(pRequest:emailHtml).
if search(cHTML) = ?
 then pResponse:fault("3001", "Agency Report Email").
 
if pResponse:isFault()
 then return.

assign
  dWorkday       = getLastWorkday(pDate)
  iWorkMonth     = month(dWorkday)
  iWorkDays      = countDays(date(month(dWorkday),1,year(dWorkday)),date(month(dWorkday),getLastDay(date(dWorkday)),year(dWorkday)))
  iDayCount      = countDays(date(month(dWorkday),1,year(dWorkday)),dWorkday)
  activeFont     = "Helvetica"
  activeSize     = 6.0
  .
  
/* create the mail parameter */
create mailparameter.
mailparameter.mailDate = today.
  
/* get the daily counts */
tDataFile = guid(generate-uuid).
output to value(tDataFile).
put unformatted "~{ ~"Date~": ~"" + string(dWorkday,"9999-99-99") + "~" ~}".
output close.
publish "AddTempFile" ("DailyCount", tDataFile).

run util/httppost.p 
  ("",
  tUrl,             /* URL for ARC Get Daily Count */
  CreateArcHeader("Accept:application/xml"),
  "text/json",
  tDataFile,
  output pSuccess,
  output pMsg,
  output tResponseFile).

if not pSuccess 
 then return.
pSuccess = parseXML(output pMsg).

/* get the stored procedure data */
{lib/callsp.i  &name=spReportAgency &load-into=agencyreport &params="input dWorkday"}
  
/* loop through the email addresses */
do i = 1 to num-entries(pTo, ";"):
  assign
    cEmail     = ""
    cUser      = ""
    cAgentList = ""
    cManager   = ""
    .
  for first sysuser fields (uid email) no-lock
      where sysuser.uid = entry(i, pTo, ";"):
      
    assign
      cEmail = sysuser.email
      cUser  = sysuser.uid
      .
  end.
  if cEmail = "" 
   then pResponse:fault("3005", "Email address not set for user: " + cUser).
   else activeFilename = "Agency_Report_" + substring(cEmail, 1, index(cEmail, "@") - 1) + "_" + string(year(today),"9999") + string(month(today),"99") + string(day(today),"99") + ".pdf".
   
  if not pResponse:isFault()
   then
    do:
      {lib/rpt-open.i}
      
      empty temp-table agentfilter.
    
      {lib/callsp.i  &name=spAgentFilter &load-into=agentfilter &params="input '', input cUser" &noCount=true}
      for each agentfilter:
        cAgentList = addDelimiter(cAgentList, ",") + agentfilter.agentID.
      end.   
      
      for each agentmanager no-lock
         where agentmanager.stat = "A"
           and agentmanager.isPrimary = true
      break by agentmanager.uid:
  
        if cAgentList <> "ALL" and lookup(agentmanager.agentID, cAgentList) = 0
         then next.
      
        /* must be valid */
        if not can-find(first agent where agentID = agentmanager.agentID)
         then next.
        
        {lib/getusername.i &user=agentmanager.uid &var=cManagerName}
        if lookup(cManagerName, cManager) = 0
         then
          do:
            newPage().
            run agentDetail in this-procedure (agentmanager.uid, cAgentList).
            cManager = addDelimiter(cManager, ",") + cManagerName.
          end.
      end.
      {lib/rpt-cloz.i &exclude-pdfview=true &no-deleteproc=true}
      
      /* send the email */
      run util/htmlmail.p (cHTML,
                           "",
                           cEmail,
                           "Agency Report for " + string(today,"99/99/9999"),
                           search(activeFilename),
                           table mailparameter,
                           output std-lo).
    end.
   else return.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-agentDetail) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE agentDetail Procedure 
PROCEDURE agentDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pManager as character no-undo.
  define input parameter pAgentFilter as character no-undo.
  
  define variable cAgentOrder as character no-undo.
  define variable cAgent      as character no-undo.
  define variable iLoop       as integer no-undo.
  
  setFont("Courier", 5.0).
  for each agencyreport no-lock
     where agencyreport.manager = pManager
       and agencyreport.category = "NPP":
  
    if cAgentList <> "ALL" and lookup(agencyreport.agentID, pAgentFilter) = 0
     then next.
  
    cAgentOrder = addDelimiter(cAgentOrder, ",") + agencyreport.agentID.
  end.
      
  do iLoop = 1 to num-entries(cAgentOrder):
    cAgent = entry(iLoop, cAgentOrder).
    
    /* copy the agency report for the agent into a temporary table */
    empty temp-table tempagencyreport.
    for each agencyreport no-lock
       where agencyreport.agentID = cAgent:
       
      create tempagencyreport.
      buffer-copy agencyreport to tempagencyreport.
    end.
          
    for first agent no-lock
        where agent.agentID = cAgent:
    
      /* if the agent details would span pages, just create a new page */
      if (agent.stat = "P" and iLine + 2 >= {&TotalLines}) or (agent.stat <> "P" and iLine + 6 >= {&TotalLines})
       then newPage().
      
      std-ch = "".
      {lib/get-sysprop.i &appCode="'AMD'" &objAction="'Agent'" &objProperty="'Status'" &objID=agent.stat}
      textAt(std-ch, 4).
      textAt(agent.agentID, 14).
      std-ch = agent.corporationID.
      if std-ch = ""
       then std-ch = agent.name.
      
      if length(std-ch) > 15
       then std-ch = substring(std-ch, 1, r-index(std-ch, " ", 15)).
      textAt(substring(std-ch, 1, 18), 25).
      
      /* put the net premium remitted plan */
      run agentDetailNPRPlan in this-procedure (agent.agentID, table tempagencyreport).
    
      /* put the net premium remitted actual */
      if agent.stat <> "P"
       then run agentDetailNPRActual in this-procedure (agent.agentID, agent.city, agent.state, table tempagencyreport).
      
      /* put the net premium remitted percent */
      if agent.stat <> "P"
       then run agentDetailNPRPercent in this-procedure (agent.agentID, table tempagencyreport).
      
      /* put the CPL data */
      if agent.stat <> "P"
       then run agentDetailCPL in this-procedure (agent.agentID, table tempagencyreport).
      
      /* put the batch counts */
      if agent.stat <> "P"
       then run agentDetailBatch in this-procedure (agent.agentID, table tempagencyreport).
      
      newLine(1).
    end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-agentDetailAdd) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE agentDetailAdd Procedure 
PROCEDURE agentDetailAdd :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID  as character no-undo.
  define input parameter pCategory as character no-undo.
  define input parameter table for tempagencyreport.
  
  define variable hQuery      as handle no-undo.
  define variable hBuffer     as handle no-undo.
  define variable hField      as handle no-undo.
  
  define variable cFormat     as character no-undo.
  define variable cFormatZero as character no-undo.
  case pCategory:
   when "B"   then
    assign
      cFormat     = {&FormatNumber}
      cFormatZero = {&FormatNumberZero}
      .
   when "NP%" then
    assign
      cFormat     = {&FormatPercent}
      cFormatZero = {&FormatPercentZero}
      .
   otherwise
    assign
      cFormat     = {&FormatMoney}
      cFormatZero = {&FormatMoneyZero}
      .
  end case.
  
  /* put the title */
  std-ch = "".
  {lib/get-sysprop.i &appCode="'AMD'" &objAction="'AgencyReport'" &objProperty="'Category'" &objID=pCategory}
  textAt(std-ch, 45).
  
  /* place the year and the monthly data */
  create buffer hBuffer for table "tempagencyreport".
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each tempagencyreport no-lock where category = '" + pCategory + "'").
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    /* the yearly count */
    hField = hBuffer:buffer-field("yearCount") no-error.
    if valid-handle(hField)
     then
      if hField:buffer-value() > -9999999 and hField:buffer-value() <> 0
       then textTo(string(hField:buffer-value(),cFormat), 71).
       else textTo(cFormatZero, 71).
     
    /* add last 24 hours */
    hField = hBuffer:buffer-field("lastDay") no-error.
    if valid-handle(hField)
     then
      if hField:buffer-value() > -9999999 and hField:buffer-value() <> 0
       then textTo(string(hField:buffer-value(),cFormat), 84).
       else textTo(cFormatZero, 84).
        
    /* the monthly count (current year) */
    iCounter = 0.
    do std-in = iWorkMonth to 1 by -1:
      iCounter = iCounter + 1.
      iPos     = 84 + (13 * iCounter).
      hField = hBuffer:buffer-field("month" + string(std-in) + "Curr") no-error.
      if valid-handle(hField) and hField:buffer-value() <> 0
       then textTo(string(hField:buffer-value(),cFormat), iPos).
       else textTo(cFormatZero, iPos).
    end.
    /* the monthly count (previous year) */
    iCounter = 0.
    iPrevStart = iPos.
    do std-in = 12 to iWorkMonth by -1:
      iCounter = iCounter + 1.
      iPos     = iPrevStart + (13 * iCounter).
      hField = hBuffer:buffer-field("month" + string(std-in) + "Prev") no-error.
      if valid-handle(hField) and hField:buffer-value() <> 0
       then textTo(string(hField:buffer-value(),cFormat), iPos).
       else textTo(cFormatZero, iPos).
    end.
    hQuery:get-next().
  end.
  hQuery:query-close().
  newLine(1).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-agentDetailBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE agentDetailBatch Procedure 
PROCEDURE agentDetailBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID  as character no-undo.
  define input parameter table for tempagencyreport.
  
  run agentDetailAdd in this-procedure (pAgentID, "B", table tempagencyreport).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-agentDetailCPL) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE agentDetailCPL Procedure 
PROCEDURE agentDetailCPL :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter table for tempagencyreport.
  
  define variable hQuery  as handle no-undo.
  define variable hBuffer as handle no-undo.
  define variable hField  as handle no-undo.
  
  /* put the title */
  std-ch = "".
  {lib/get-sysprop.i &appCode="'AMD'" &objAction="'AgencyReport'" &objProperty="'Category'" &objID="'C'"}
  textAt(std-ch, 45).
  
  /* place the last 24 hour value from the ARC */
  if not can-find(first dailycount where agentID = pAgentID)
   then textTo(string(0, {&FormatNumber}), 84).
   else
    for first dailycount no-lock
        where dailycount.agentID = pAgentID:
      
      textTo(string(dailycount.cplCount, {&FormatNumber}), 84).
    end.
  
  /* place the year and the monthly data */
  create buffer hBuffer for table "tempagencyreport".
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each tempagencyreport no-lock where category = 'C'").
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    /* the yearly count */
    hField = hBuffer:buffer-field("yearCount") no-error.
    if valid-handle(hField)
     then textTo(string(hField:buffer-value(), {&FormatNumber}), 71).
    
    /* the monthly count (current year) */
    iCounter = 0.
    do std-in = iWorkMonth to 1 by -1:
      iCounter = iCounter + 1.
      iPos     = 84 + (13 * iCounter).
      hField = hBuffer:buffer-field("month" + string(std-in) + "Curr") no-error.
      if valid-handle(hField) and hField:buffer-value() <> 0
       then textTo(string(hField:buffer-value(), {&FormatNumber}), iPos).
       else textTo("", iPos).
    end.
    
    /* the monthly count (previous year) */
    iCounter = 0.
    iPrevStart = iPos.
    do std-in = 12 to iWorkMonth by -1:
      iCounter = iCounter + 1.
      iPos     = iPrevStart + (13 * iCounter).
      hField = hBuffer:buffer-field("month" + string(std-in) + "Prev") no-error.
      if valid-handle(hField) and hField:buffer-value() <> 0
       then textTo(string(hField:buffer-value(), {&FormatNumber}), iPos).
       else textTo("", iPos).
    end.
    hQuery:get-next().
  end.
  hQuery:query-close().
  newLine(1).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-agentDetailNPRActual) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE agentDetailNPRActual Procedure 
PROCEDURE agentDetailNPRActual :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pCity    as character no-undo.
  define input parameter pStateID as character no-undo.
  define input parameter table for tempagencyreport.
  
  if pCity > ""
   then
    do:
      std-ch = pCity + ", " + pStateID.
      if length(std-ch) > 15
       then 
        do:
          std-ch = pCity.
          if length(std-ch) > 15
           then std-ch = substring(std-ch, 1, r-index(std-ch, " ", 15)).
        end.
      textAt(substring(std-ch, 1, 18), 25).
    end.
  
  run agentDetailAdd in this-procedure (pAgentID, "NPR", table tempagencyreport).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-agentDetailNPRPercent) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE agentDetailNPRPercent Procedure 
PROCEDURE agentDetailNPRPercent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter table for tempagencyreport.
  
  run agentDetailAdd in this-procedure (pAgentID, "NP%", table tempagencyreport).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-agentDetailNPRPlan) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE agentDetailNPRPlan Procedure 
PROCEDURE agentDetailNPRPlan :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter table for tempagencyreport.
  
  run agentDetailAdd in this-procedure (pAgentID, "NPP", table tempagencyreport).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-Characters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Characters Procedure 
PROCEDURE Characters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEFINE INPUT PARAMETER ppText AS MEMPTR NO-UNDO.
  DEFINE INPUT PARAMETER piNumChars AS INTEGER NO-UNDO.

  tValue = tValue + GET-STRING(ppText,1,piNumChars).
  if error-status:error 
   then assign tValue = ?.

  if available dailycount then
  do:
    case tCurrentElement:
     when "AgentID" then dailycount.agentID = tValue.
     when "CPLCount" then dailycount.cplCount = integer(tValue) no-error.
     when "PolicyCount" then dailycount.policyCount = integer(tValue) no-error.
    end case.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-EndElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE EndElement Procedure 
PROCEDURE EndElement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 DEFINE INPUT PARAMETER namespaceURI AS CHARACTER.
 DEFINE INPUT PARAMETER localName AS CHARACTER.
 DEFINE INPUT PARAMETER qName AS CHARACTER.

 if qName = "DailyCountItem"
  then release dailycount.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reportFooter) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reportFooter Procedure 
PROCEDURE reportFooter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/rpt-ftr.i &no-pages=true}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reportHeader) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reportHeader Procedure 
PROCEDURE reportHeader :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tTitle as character no-undo.
  define variable tTitle2 as character no-undo.
  define variable tXpos as integer no-undo.
  define variable tYpos as integer no-undo.
  
  assign
    tTitle = cManagerName
    tTitle2 = getMonthName(month(dWorkday)) + " " + string(year(dWorkday),"9999") + " - Weekday " + string(iDayCount) + " of " + string(iWorkDays)
    tXPos = 500
    tYpos = 570
    .
  {lib/rpt-hdr.i &title=tTitle &title2=tTitle2 &title-x=tXpos &title-y=tYpos &titleSize=16.0}
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
  run pdf_rect ({&rpt}, 18, 522, 750, 8, 5.0).
  
  setFont("Courier", 5.0).
  textColor(1.0, 1.0, 1.0).
  newLine(2).
  textTo("Last", 84).
  newLine(1).
  textAt("Status", 4).
  textAt("Agent ID", 14).
  textAt("Agent", 25).
  textTo(string(year(dWorkday)), 71).
  textTo("Weekday", 84).
  textTo(substring(getMonthName(month(dWorkday)),1,3), 97).
  textTo(substring(getMonthName(month(add-interval(dWorkday, -1, "months"))), 1, 3), 110).
  textTo(substring(getMonthName(month(add-interval(dWorkday, -2, "months"))), 1, 3), 123).
  textTo(substring(getMonthName(month(add-interval(dWorkday, -3, "months"))), 1, 3), 136).
  textTo(substring(getMonthName(month(add-interval(dWorkday, -4, "months"))), 1, 3), 149).
  textTo(substring(getMonthName(month(add-interval(dWorkday, -5, "months"))), 1, 3), 162).
  textTo(substring(getMonthName(month(add-interval(dWorkday, -6, "months"))), 1, 3), 175).
  textTo(substring(getMonthName(month(add-interval(dWorkday, -7, "months"))), 1, 3), 188).
  textTo(substring(getMonthName(month(add-interval(dWorkday, -8, "months"))), 1, 3), 201).
  textTo(substring(getMonthName(month(add-interval(dWorkday, -9, "months"))), 1, 3), 214).
  textTo(substring(getMonthName(month(add-interval(dWorkday, -10, "months"))), 1, 3), 227).
  textTo(substring(getMonthName(month(add-interval(dWorkday, -11, "months"))), 1, 3), 240).
  textTo(substring(getMonthName(month(add-interval(dWorkday, -12, "months"))), 1, 3), 253).
  newLine(2).
  textColor(0.0, 0.0, 0.0).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

  tCurrentElement = qName.
  tValue = "". 

  if qName = "DailyCountItem"
   then create dailycount.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

