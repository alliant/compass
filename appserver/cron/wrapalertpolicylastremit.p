&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*---------------------------------------------------------------------
@name wrapalertpolicylastremit.p
@action N/A
@description Runs the policy last remit alert from the scheduler

@author John Oliver
@version 1.0
@created 2018/02/07

@modified
Date       Name      Description
08/30/2019 Gurvindar Added presponse parameter in alert call
---------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
define input parameter pRequest as service.IRequest no-undo.
define input parameter pResponse as service.IResponse no-undo.
{lib/std-def.i}
{tt/alert.i &tableAlias="ttAlert"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

run alert/addalertpolicylastremit.p (input pResponse, "ALL", pRequest:uid, false, output table ttAlert).

pResponse:success("2000","Alert: Policy Last Remit").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


