&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@name stomp-event.i
@description Creates Compass events for displaying on the client

@author John Oliver
@version 1.0
@created 8/29/2017
----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

define temp-table mqData
  field seq AS integer
  field businessObject as character
  field keyID as character
  field action as character
  field description AS character
  index iSeq IS PRIMARY UNIQUE seq
  .
  
&scoped-define logfile "stomp-event.log"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD alertEvent Procedure
FUNCTION alertEvent RETURNS LOGICAL
  ( input pName as character,
    input pKey as character ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD completeEvent Include 
FUNCTION completeEvent RETURNS LOGICAL
  ( input pName as character,
    input pKey as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createEvent Include 
FUNCTION createEvent RETURNS LOGICAL
  ( input pName as character,
    input pKey as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doEventLog Include 
FUNCTION doEventLog RETURNS LOGICAL
  ( input pText as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD errorEvent Include 
FUNCTION errorEvent RETURNS LOGICAL
  ( input pName as character,
    input pKey as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD otherEvent Include 
FUNCTION otherEvent RETURNS LOGICAL
  ( input pName as char, input pKey as char, input pAction as char, input pMsg as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD sendEvent Include 
FUNCTION sendEvent RETURNS LOGICAL
  ( input pUserID as character ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION errorEvent Include 
FUNCTION alertEvent RETURNS LOGICAL
  ( input pName as character,
    input pKey as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable lSuccess as logical no-undo.
  lSuccess = otherEvent(pName, pKey, "Alert", pName + " created an alert: " + pKey).
  
  return lSuccess.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION completeEvent Include 
FUNCTION completeEvent RETURNS LOGICAL
  ( input pName as character,
    input pKey as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable lSuccess as logical no-undo.
  lSuccess = otherEvent(pName, pKey, "Complete", pName + " complete with key " + pKey).
  
  return lSuccess.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createEvent Include 
FUNCTION createEvent RETURNS LOGICAL
  ( input pName as character,
    input pKey as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable lSuccess as logical no-undo.
  lSuccess = otherEvent(pName, pKey, "Created", pName + " created with key " + pKey).
   
  return lSuccess.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doEventLog Include 
FUNCTION doEventLog RETURNS LOGICAL
  ( input pText as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  output to value({&logfile}) append.
  put unformatted pText skip.
  output close.

  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION errorEvent Include 
FUNCTION errorEvent RETURNS LOGICAL
  ( input pName as character,
    input pKey as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable lSuccess as logical no-undo.
  lSuccess = otherEvent(pName, pKey, "Error", pName + " generated an error: " + pKey).
  
  return lSuccess.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION otherEvent Include 
FUNCTION otherEvent RETURNS LOGICAL
  ( input pName as char, input pKey as char, input pAction as char, input pMsg as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define buffer mqData for mqData.
  
  define variable tSeq as integer no-undo.
  define variable tFound as logical no-undo.
  
  /* It makes no sense to notify of an event that we cannot identify */
  if pName = "" or pName = ? or pKey = ? or pAction = ?
   then return false.
   
  if pMsg = ?
   then pMsg = "Unknown".
   
  tSeq = 1.
  for last mqData 
        by mqData.seq:
   
    tSeq = mqData.seq + 1.
  end.
  
  create mqData.
  assign
    mqData.seq = tSeq
    mqData.businessObject = pName
    mqData.keyID = pKey
    mqData.action = pAction
    mqData.description = pMsg
    .
  
  RETURN can-find(first mqData where seq = tSeq).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION sendEvent Include 
FUNCTION sendEvent RETURNS LOGICAL
  ( input pUserID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable iStompClient as dotr.Stomp.StompClient no-undo.
  define variable iStompConfig as dotr.Stomp.StompConfig no-undo.
  define variable tEventHost as character no-undo.
  define variable tEventPort as character no-undo.
  
  &scoped-define defaultuser "compass@alliantnational.com"
  
  PUBLISH "GetSystemParameter" ("EventServerHost", OUTPUT tEventHost).
  PUBLISH "GetSystemParameter" ("EventServerPort", OUTPUT tEventPort).
  
  iStompConfig = new dotr.Stomp.StompConfig(tEventHost, tEventPort).
  iStompClient = new dotr.Stomp.StompClient(iStompConfig).
  
  if pUserID > ""
   then pUserID = {&defaultuser}.

  /* additional headers */
  iStompClient:WithHeader("ApplicationCode", "OPS").
  iStompClient:WithHeader("User", pUserID).
  for each mqData
        by mqData.seq:
        
    iStompClient:WithHeader("Entity", mqData.businessObject).
    iStompClient:WithHeader("Key", mqData.keyID).
    iStompClient:WithHeader("Action", mqData.action).
    iStompClient:SendTopic(mqData.businessObject, mqData.description).
    
    if mqData.action = "Error" and pUserID <> {&defaultuser}
     then
      for first sysuser no-lock
          where sysuser.uid = pUserID:
          
        run util/simplemail.p ("", sysuser.email, "Error with Batch Import", mqData.keyID).
      end.
  end.
  
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

