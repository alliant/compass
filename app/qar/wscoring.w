&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wunanswered.w
    Modification:
    Date          Name      Description
    02/03/2017    AG        Changed to implement sorting in browse.
    03/24/2017    AG        Browser read-only, removed sorting, 
    08/09/2017    AG        Changes related to sectionID
    09/11/2017    AG        Alternate color code in browse.
    07/15/2021    SA        Task 83510 modified to add points and score 
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

{lib/std-def.i}

def temp-table score
 field sectionID as int
 field sectionTitle as char
 field rawScore as int 
 field weight as decimal
 field adjScore as decimal
 .
 def var iBgcolor as int no-undo.
 def var cAuditType as char no-undo.
{tt/qarsectiond.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwQuestions

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES score

/* Definitions for BROWSE brwQuestions                                  */
&Scoped-define FIELDS-IN-QUERY-brwQuestions score.sectionID score.sectionTitle score.rawScore score.weight score.adjScore   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQuestions   
&Scoped-define SELF-NAME brwQuestions
&Scoped-define QUERY-STRING-brwQuestions FOR EACH score by score.sectionID
&Scoped-define OPEN-QUERY-brwQuestions OPEN QUERY {&SELF-NAME} FOR EACH score by score.sectionID.
&Scoped-define TABLES-IN-QUERY-brwQuestions score
&Scoped-define FIRST-TABLE-IN-QUERY-brwQuestions score


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwQuestions}

/* Standard List Definitions                                            */
&Scoped-Define DISPLAYED-OBJECTS tAnswered tUnanswered tMajor tCorrective ~
tIntermediate tRecommended tMinor tSuggested tBestPractices 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getGrade C-Win 
FUNCTION getGrade RETURNS INTEGER
  ( input ipoints as integer /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE VARIABLE tAnswered AS INTEGER FORMAT ">>>,>>9":U INITIAL 0 
     LABEL "Answered" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE tBestPractices AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Best Practices" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE VARIABLE tCorrective AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Corrective" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE VARIABLE tIntermediate AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Intermediate" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE VARIABLE tMajor AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Major" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE VARIABLE tMinor AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Minor" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE VARIABLE tRecommended AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Recommended" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE VARIABLE tScorePct AS INTEGER FORMAT "zzz":U INITIAL 0 
      VIEW-AS TEXT 
     SIZE 7 BY .62
     FONT 16 NO-UNDO.

DEFINE VARIABLE tSuggested AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Suggested" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalScore AS CHARACTER FORMAT "xxx":U 
      VIEW-AS TEXT 
     SIZE 5 BY .62
     FONT 16 NO-UNDO.

DEFINE VARIABLE tUnanswered AS INTEGER FORMAT ">>>,>>9":U INITIAL 0 
     LABEL "Unanswered" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-27
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 70 BY 2.62.

DEFINE RECTANGLE RECT-28
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 70 BY 4.29.

DEFINE RECTANGLE RECT-29
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 70 BY 2.14.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwQuestions FOR 
      score SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwQuestions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQuestions C-Win _FREEFORM
  QUERY brwQuestions DISPLAY
      score.sectionID label "Section" 
 score.sectionTitle label "Title" format "x(33)"
 score.rawScore label "Score" format "zz"
 score.weight label "Weight" format "9.9"
 score.adjScore label "Adj Score" format "z9.9" width 10
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-SCROLLBAR-VERTICAL SIZE 70 BY 7.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tAnswered AT ROW 1.95 COL 41 COLON-ALIGNED WIDGET-ID 50
     tUnanswered AT ROW 3.14 COL 41 COLON-ALIGNED WIDGET-ID 52
     tMajor AT ROW 5.52 COL 16 COLON-ALIGNED WIDGET-ID 36
     tCorrective AT ROW 5.52 COL 41 COLON-ALIGNED WIDGET-ID 42
     tIntermediate AT ROW 6.71 COL 16 COLON-ALIGNED WIDGET-ID 38
     tRecommended AT ROW 6.71 COL 41 COLON-ALIGNED WIDGET-ID 44
     tMinor AT ROW 7.91 COL 16 COLON-ALIGNED WIDGET-ID 40
     tSuggested AT ROW 7.91 COL 41 COLON-ALIGNED WIDGET-ID 46
     tBestPractices AT ROW 10.05 COL 41 COLON-ALIGNED WIDGET-ID 48
     brwQuestions AT ROW 14.24 COL 2 WIDGET-ID 200
     tTotalScore AT ROW 12.62 COL 69.6 RIGHT-ALIGNED NO-LABEL WIDGET-ID 6
     tScorePct AT ROW 13.38 COL 71.2 RIGHT-ALIGNED NO-LABEL WIDGET-ID 56
     "Review Points:" VIEW-AS TEXT
          SIZE 21.2 BY .62 AT ROW 12.62 COL 44 WIDGET-ID 8
          FONT 16
     "Best Practices" VIEW-AS TEXT
          SIZE 17 BY .62 AT ROW 9.33 COL 4 WIDGET-ID 34
          FONT 6
     "Score%:" VIEW-AS TEXT
          SIZE 12.4 BY .62 AT ROW 13.38 COL 53 WIDGET-ID 54
          FONT 16
     "Findings/Actions" VIEW-AS TEXT
          SIZE 20 BY .62 AT ROW 4.57 COL 4 WIDGET-ID 30
          FONT 6
     "Questions" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 1.48 COL 4 WIDGET-ID 26
          FONT 6
     RECT-27 AT ROW 1.71 COL 2 WIDGET-ID 24
     RECT-28 AT ROW 4.81 COL 2 WIDGET-ID 28
     RECT-29 AT ROW 9.57 COL 2 WIDGET-ID 32
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 72 BY 21.38 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Properties"
         HEIGHT             = 21.33
         WIDTH              = 72
         MAX-HEIGHT         = 23.19
         MAX-WIDTH          = 112
         VIRTUAL-HEIGHT     = 23.19
         VIRTUAL-WIDTH      = 112
         SHOW-IN-TASKBAR    = no
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwQuestions tBestPractices fMain */
/* SETTINGS FOR BROWSE brwQuestions IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwQuestions:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwQuestions:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

/* SETTINGS FOR RECTANGLE RECT-27 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-28 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-29 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAnswered IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tBestPractices IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tCorrective IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tIntermediate IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tMajor IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tMinor IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tRecommended IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tScorePct IN FRAME fMain
   NO-DISPLAY NO-ENABLE ALIGN-R                                         */
/* SETTINGS FOR FILL-IN tSuggested IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tTotalScore IN FRAME fMain
   NO-DISPLAY NO-ENABLE ALIGN-R                                         */
/* SETTINGS FOR FILL-IN tUnanswered IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQuestions
/* Query rebuild information for BROWSE brwQuestions
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH score by score.sectionID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQuestions */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Properties */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Properties */
DO:
  /* This event will close the window and terminate the procedure.  */
  run hideWindow.
/*   APPLY "CLOSE":U TO THIS-PROCEDURE. */
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQuestions
&Scoped-define SELF-NAME brwQuestions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQuestions C-Win
ON DEFAULT-ACTION OF brwQuestions IN FRAME fMain
DO:
 if available score
  then publish "SelectAuditPage" (integer(score.sectionID)).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQuestions C-Win
ON ROW-DISPLAY OF brwQuestions IN FRAME fMain
DO:
  
  if current-result-row("brwQuestions") modulo 2 = 0 then
    iBgColor = 17.
  else
    iBgColor = 15.
  score.sectionID:bgcolor in browse brwQuestions     = iBgColor.
  score.sectionTitle :bgcolor in browse brwQuestions = iBgColor.
  score.rawScore:bgcolor in browse brwQuestions      = iBgColor.
  score.weight:bgcolor in browse brwQuestions        = iBgColor.
  score.adjScore:bgcolor in browse brwQuestions      = iBgColor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/win-main.i}

subscribe to "DisplayScore" anywhere.
subscribe to "AuditOpened" anywhere.
subscribe to "AuditClosed" anywhere.
subscribe to "AuditChanged" anywhere.

/* publish "getcurrentvalue"("currentAuditType", output cAuditType ). */
publish "GetCurrentAuditType"(output cAuditType ).

publish "GetSectionDefaults" (output table sectionDefault).
for each sectionDefault:
 create score.
 buffer-copy sectionDefault to score.
end.

run AuditChanged in this-procedure (false).


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  brwQuestions:DESELECT-FOCUSED-ROW ().
   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditChanged C-Win 
PROCEDURE AuditChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pNew as logical.

 {lib/getattrin.i answeredCount tAnswered}
 {lib/getattrin.i unansweredCount tUnanswered}

 {lib/getattrin.i correctiveActionsCount tCorrective}
 {lib/getattrin.i recommendedActionsCount tRecommended}
 {lib/getattrin.i suggestedActionsCount tSuggested}

 {lib/getattrin.i majorFindingsCount tMajor}
 {lib/getattrin.i intermediateFindingsCount tIntermediate}
 {lib/getattrin.i minorFindingsCount tMinor}

 {lib/getattrin.i bestPracticesCount tBestPractices}

 assign
   tAnswered:screen-value in frame fMain = string(tAnswered)
   tUnanswered:screen-value = string(tUnanswered)
   tCorrective:screen-value = string(tCorrective)
   tRecommended:screen-value = string(tRecommended)
   tSuggested:screen-value = string(tSuggested)
   tMajor:screen-value = string(tMajor)
   tIntermediate:screen-value = string(tIntermediate)
   tMinor:screen-value = string(tMinor)
   tBestPractices:screen-value = string(tBestPractices)
   .

 for each score:
   publish "GetSectionAnswer" (score.sectionID,
                               output score.rawScore,
                               output std-ch).
   score.adjScore = score.rawScore * score.weight.
 end.
 publish "GetAuditAnswer" ("Score", output std-ch).
 tTotalScore:screen-value in frame fMain = std-ch.
 
 if cAuditType ne "" 
  then
   tScorePct:screen-value  in frame fmain =  string(getGrade(integer(std-ch))).
 
 close query brwQuestions.
 open query brwQuestions for each score by score.sectionID.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditClosed C-Win 
PROCEDURE AuditClosed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 run AuditChanged in this-procedure (false).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditOpened C-Win 
PROCEDURE AuditOpened :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 run AuditChanged in this-procedure (false).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DisplayScore C-Win 
PROCEDURE DisplayScore :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 run AuditChanged in this-procedure (false).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tAnswered tUnanswered tMajor tCorrective tIntermediate tRecommended 
          tMinor tSuggested tBestPractices 
      WITH FRAME fMain IN WINDOW C-Win.
  VIEW FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hideWindow C-Win 
PROCEDURE hideWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:visible = false.
 publish "WindowClosed" (input "Scoring").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 run AuditChanged in this-procedure (false).
 {&window-name}:visible = true.
 {&window-name}:move-to-top().
 publish "WindowOpened" (input "Scoring").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getGrade C-Win 
FUNCTION getGrade RETURNS INTEGER
  ( input ipoints as integer /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable iGrade as integer no-undo.
  define variable iTotalPoints as integer no-undo.
  
  if cAuditType = "QAR"
   then
    iTotalPoints = 120.
  else if cAuditType = "ERR"
   then
    iTotalPoints = 30.
  else if cAuditType = "TOR"
   then
    iTotalPoints = 90.
    
  igrade = integer((ipoints / iTotalPoints) * 100).

  RETURN igrade.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

