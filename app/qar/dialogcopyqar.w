&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fOpen 
/* dialogcopyqar.w.w
  Anjly 2-25-2019

  @Modification
  Date         Name    Description
  05/02/2022    VR     Task 93436 - Modified to display all type of audits in 'Copy Active Review'
                       window and enable/disable the sections options and pass the active audit main 
					   screen data for copy.
--------------------------------------------------------------------------------------------------------*/


def output parameter pSelected as logical init false no-undo.
def output parameter pFullFile as char no-undo.

def var  iBgColor as in no-undo.

{lib/std-def.i}
{tt/doc.i }

define temp-table ttlocalQarFileInfo like document
fields audittype as char.

define temp-table localQarFileInfo like ttlocalQarFileInfo
fields tSelect as logical.

def var selectedAudit    as character.
def var selectedRowId as rowid.
def var SelectedRow   as integer.
def var cAuditType as char.
def var selectedAuditType as char.

/* All below teamp tables are required, to build dataset so that audit XML can be read. */
{tt/qaraudit.i}          /* audit     */
{tt/qarauditsection.i}   /* section   */
{tt/qarauditquestion.i}  /* question  */
{tt/qarauditfile.i}      /* agentFile */
{tt/qarauditfinding.i}   /* finding */
{tt/qarauditbp.i}        /* bestPractice */
{tt/qarauditaction.i}    /* action */
{tt/qarsectiond.i}       /* sectionDefault */
{tt/qarquestiond.i}      /* questionDefault */
{tt/qarrptfinding.i}     /* rptFinding */
{tt/qarrptbp.i}          /* rptBestPractice */
{tt/qarbkgquestiond.i}   /* bkgQuestionDefault */
{tt/qarbkgquestion.i}    /* bkgQuestion */
{tt/qarbkguw.i}          /* underwriter */
{tt/qarbkgbranch.i}      /* branch */
{tt/qaraccount.i}        /* account */


def dataset activeAuditData
 for audit, 
     section, 
     question,
     agentFile,
     finding, 
     bestPractice, 
     action, 
     bkgQuestion, 
     underwriter,
     branch,
     account,
     document.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fOpen
&Scoped-define BROWSE-NAME brwFiles

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES localQarFileInfo

/* Definitions for BROWSE brwFiles                                      */
&Scoped-define FIELDS-IN-QUERY-brwFiles localQarFileInfo.tselect localQarFileInfo.identifier localQarFileInfo.auditType localQarFileInfo.name localQarFileInfo.createdDate localQarFileInfo.modifiedDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFiles tselect   
&Scoped-define SELF-NAME brwFiles
&Scoped-define QUERY-STRING-brwFiles FOR EACH localQarFileInfo by localQarFileInfo.modifiedDate descending
&Scoped-define OPEN-QUERY-brwFiles OPEN QUERY {&SELF-NAME} FOR EACH localQarFileInfo by localQarFileInfo.modifiedDate descending.
&Scoped-define TABLES-IN-QUERY-brwFiles localQarFileInfo
&Scoped-define FIRST-TABLE-IN-QUERY-brwFiles localQarFileInfo


/* Definitions for DIALOG-BOX fOpen                                     */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fOpen ~
    ~{&OPEN-QUERY-brwFiles}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bReset RECT-1 imAlert brwFiles tbEscrow ~
tbSection1 tbSection2 tbSection6 warnInfo bCancel 
&Scoped-Define DISPLAYED-OBJECTS tbEscrow tbSection1 tbSection2 tbSection6 ~
warnInfo 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getStatus fOpen 
FUNCTION getStatus RETURNS CHARACTER
  (input pstat as char /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.13.

DEFINE BUTTON bOpen 
     LABEL "Copy" 
     SIZE 15 BY 1.13.

DEFINE BUTTON bReset  NO-FOCUS
     LABEL "Reset" 
     SIZE 4.8 BY 1.13 TOOLTIP "Reset local active files list".

DEFINE VARIABLE warnInfo AS CHARACTER 
     VIEW-AS EDITOR NO-WORD-WRAP NO-BOX
     SIZE 96 BY .78 NO-UNDO.

DEFINE IMAGE imAlert
     FILENAME "adeicon/blank":U TRANSPARENT
     SIZE 3 BY .78.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 104 BY 4.09.

DEFINE VARIABLE tbEscrow AS LOGICAL INITIAL no 
     LABEL "Escrow accounts" 
     VIEW-AS TOGGLE-BOX
     SIZE 29 BY .83 NO-UNDO.

DEFINE VARIABLE tbSection1 AS LOGICAL INITIAL no 
     LABEL "Section 1 answers + findings + best practices" 
     VIEW-AS TOGGLE-BOX
     SIZE 58 BY .83 NO-UNDO.

DEFINE VARIABLE tbSection2 AS LOGICAL INITIAL no 
     LABEL "Section 2 answers + findings + best practices" 
     VIEW-AS TOGGLE-BOX
     SIZE 49 BY .83 NO-UNDO.

DEFINE VARIABLE tbSection6 AS LOGICAL INITIAL no 
     LABEL "Section 6 answers + findings + best practices" 
     VIEW-AS TOGGLE-BOX
     SIZE 49 BY .83 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwFiles FOR 
      localQarFileInfo SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwFiles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFiles fOpen _FREEFORM
  QUERY brwFiles DISPLAY
      localQarFileInfo.tselect column-label "Select" view-as toggle-box 
 localQarFileInfo.identifier label "QAR ID" format "x(20)" width 10  
 localQarFileInfo.auditType label "Type" format "x(5)" width 5 
 localQarFileInfo.name label "Agent Name"  format "x(55)" width 40
 localQarFileInfo.createdDate label "Created" format "x(15)" width 15
 localQarFileInfo.modifiedDate label "Modified" format "x(20)" width 20
 
enable tselect
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 104 BY 8.7 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fOpen
     bReset AT ROW 1.17 COL 101.2 WIDGET-ID 10 NO-TAB-STOP 
     brwFiles AT ROW 2.52 COL 2 WIDGET-ID 200
     tbEscrow AT ROW 12 COL 4 WIDGET-ID 12
     tbSection1 AT ROW 12.87 COL 4 WIDGET-ID 18
     tbSection2 AT ROW 13.78 COL 4 WIDGET-ID 16
     tbSection6 AT ROW 14.78 COL 4 WIDGET-ID 14
     warnInfo AT ROW 15.87 COL 6.6 NO-LABEL WIDGET-ID 28
     bOpen AT ROW 16.87 COL 38 WIDGET-ID 4
     bCancel AT ROW 16.87 COL 55.2 WIDGET-ID 8
     "Select a Review" VIEW-AS TEXT
          SIZE 17.6 BY .61 AT ROW 1.65 COL 2.2 WIDGET-ID 6
     "Copy from selected audit" VIEW-AS TEXT
          SIZE 24 BY .61 AT ROW 11.3 COL 3 WIDGET-ID 22
     RECT-1 AT ROW 11.61 COL 2 WIDGET-ID 20
     imAlert AT ROW 15.87 COL 3 WIDGET-ID 26
     SPACE(100.79) SKIP(1.53)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Copy Active Review" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fOpen
   FRAME-NAME                                                           */
/* BROWSE-TAB brwFiles imAlert fOpen */
ASSIGN 
       FRAME fOpen:SCROLLABLE       = FALSE
       FRAME fOpen:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bOpen IN FRAME fOpen
   NO-ENABLE                                                            */
ASSIGN 
       brwFiles:ALLOW-COLUMN-SEARCHING IN FRAME fOpen = TRUE
       brwFiles:COLUMN-RESIZABLE IN FRAME fOpen       = TRUE.

ASSIGN 
       imAlert:HIDDEN IN FRAME fOpen           = TRUE.

ASSIGN 
       warnInfo:HIDDEN IN FRAME fOpen           = TRUE
       warnInfo:READ-ONLY IN FRAME fOpen        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFiles
/* Query rebuild information for BROWSE brwFiles
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH localQarFileInfo by localQarFileInfo.modifiedDate descending.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwFiles */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fOpen fOpen
ON WINDOW-CLOSE OF FRAME fOpen /* Copy Active Review */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOpen fOpen
ON CHOOSE OF bOpen IN FRAME fOpen /* Copy */
DO:
  run copyData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReset
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReset fOpen
ON CHOOSE OF bReset IN FRAME fOpen /* Reset */
DO:
  std-lo = false.
  message "The list of active reviews will be updated and names may be lost. Continue?"
      view-as alert-box warning buttons ok-cancel update std-lo.
  if not std-lo 
   then return.

  publish "ResetLocalQarFiles".
  run loadReviews in this-procedure.
  
  assign
    tbEscrow:sensitive   in frame {&frame-name} = true
    tbSection1:sensitive in frame {&frame-name} = true
    tbSection2:sensitive in frame {&frame-name} = true
    tbSection6:sensitive in frame {&frame-name} = true
    tbEscrow:checked   in frame {&frame-name} = false
    tbSection1:checked in frame {&frame-name} = false
    tbSection2:checked in frame {&frame-name} = false
    tbSection6:checked in frame {&frame-name} = false.
    
    
  imAlert:hidden in frame {&frame-name} = true.
  warnInfo:hidden in frame {&frame-name} = true.
    
            
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFiles
&Scoped-define SELF-NAME brwFiles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFiles fOpen
ON ROW-DISPLAY OF brwFiles IN FRAME fOpen
DO:
  do with frame fOpen:
  end.
  
  localQarFileInfo.auditType:screen-value in browse brwFiles   = getStatus(localQarFileInfo.auditType).
  
  if current-result-row("brwFiles") modulo 2 = 0 then
    iBgColor = 17.
  else
    iBgColor = 15.

  localQarFileInfo.tselect:bgcolor in browse brwFiles      = iBgcolor.
  localQarFileInfo.identifier:bgcolor in browse brwFiles   = iBgcolor.
  localQarFileInfo.auditType:bgcolor in browse brwFiles    = iBgcolor.
  localQarFileInfo.name:bgcolor in browse brwFiles         = iBgcolor.
  localQarFileInfo.createdDate:bgcolor in browse brwFiles  = iBgcolor.
  localQarFileInfo.modifiedDate:bgcolor in browse brwFiles = iBgcolor.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFiles fOpen
ON U1 OF brwFiles IN FRAME fOpen
DO:  
  for each localQarFileInfo where selectedAudit ne localQarFileInfo.identifier:
    localQarFileInfo.tSelect = false.
  end.
  
  for each localQarFileInfo where selectedAudit eq localQarFileInfo.identifier:
    localQarFileInfo.tSelect = true.
    selectedRowId = rowid(localQarFileInfo).
    selectedAuditType = localQarFileInfo.auditType.
  end.
  
  do SelectedRow = 1 TO brwFiles:num-iterations: 
    if brwFiles:is-row-selected(SelectedRow) then leave. 
  end.
  
  {&OPEN-BROWSERS-IN-QUERY-fOpen}
  brwFiles:set-repositioned-row(SelectedRow).
  reposition brwFiles to rowid selectedRowId. 
  
  if selectedAuditType <> '' and cAuditType <> selectedAuditType then do:
    imAlert:hidden = false.
    warnInfo:hidden = false.
    warnInfo:screen-value in frame fOpen = "Information will be copied from " + getStatus(selectedAuditType) 
                                            + " type audit to " + getStatus(cAuditType) + " type audit" .
  end.
  else do:
    imAlert:hidden = true.
    warnInfo:hidden = true.
  end.
  
  if cAuditType = 'Q' then do:
    if selectedAuditType = 'Q' then do:
      assign
        tbEscrow:sensitive   in frame {&frame-name} = true     
        tbSection1:sensitive in frame {&frame-name} = true
        tbSection2:sensitive in frame {&frame-name} = true
        tbSection6:sensitive in frame {&frame-name} = true.     
    end.
    else if selectedAuditType = 'E' then do:
      assign
        tbEscrow:sensitive   in frame {&frame-name} = true     
        tbSection1:sensitive in frame {&frame-name} = true
        tbSection2:sensitive in frame {&frame-name} = true
        tbSection6:sensitive in frame {&frame-name} = true.     
    end.
    else if selectedAuditType = 'T' then do:
      assign
        tbEscrow:sensitive   in frame {&frame-name} = false     
        tbSection1:sensitive in frame {&frame-name} = true
        tbSection2:sensitive in frame {&frame-name} = true
        tbSection6:sensitive in frame {&frame-name} = false.    
    end.
  end.
  else if cAuditType = 'E' then do:
    if selectedAuditType = 'Q' then do:
      assign
        tbEscrow:sensitive   in frame {&frame-name} = true     
        tbSection1:sensitive in frame {&frame-name} = true
        tbSection2:sensitive in frame {&frame-name} = true
        tbSection6:sensitive in frame {&frame-name} = true.     
    end.
    else if selectedAuditType = 'E' then do:
      assign
        tbEscrow:sensitive   in frame {&frame-name} = true     
        tbSection1:sensitive in frame {&frame-name} = true
        tbSection2:sensitive in frame {&frame-name} = true
        tbSection6:sensitive in frame {&frame-name} = true.     
    end.
    else if selectedAuditType = 'T' then do:
      assign
        tbEscrow:sensitive   in frame {&frame-name} = false     
        tbSection1:sensitive in frame {&frame-name} = true
        tbSection2:sensitive in frame {&frame-name} = true
        tbSection6:sensitive in frame {&frame-name} = false.            
    end.    
  end.
  else if cAuditType = 'T' then do:
    if selectedAuditType = 'Q' then do:
      assign
        tbEscrow:sensitive   in frame {&frame-name} = false     
        tbSection1:sensitive in frame {&frame-name} = true
        tbSection2:sensitive in frame {&frame-name} = true
        tbSection6:sensitive in frame {&frame-name} = false.    
    end.
    else if selectedAuditType = 'E' then do:
      assign
        tbEscrow:sensitive   in frame {&frame-name} = false     
        tbSection1:sensitive in frame {&frame-name} = true
        tbSection2:sensitive in frame {&frame-name} = true
        tbSection6:sensitive in frame {&frame-name} = false.            
    end.
    else if selectedAuditType = 'T' then do:
      assign
        tbEscrow:sensitive   in frame {&frame-name} = false     
        tbSection1:sensitive in frame {&frame-name} = true
        tbSection2:sensitive in frame {&frame-name} = true
        tbSection6:sensitive in frame {&frame-name} = false.           
    end.
  end.
  
  if tbEscrow:checked and not tbEscrow:sensitive then tbEscrow:checked = no.
  if tbSection1:checked and not tbSection1:sensitive then tbSection1:checked = no.
  if tbSection2:checked and not tbSection2:sensitive then tbSection2:checked = no.
  if tbSection6:checked and not tbSection6:sensitive then tbSection6:checked = no.  
  
  
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fOpen 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bReset:load-image("images/s-refresh.bmp").
imAlert:load-image("images/exclamation.gif").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
  {lib/getattrch.i AuditType cAuditType}
  
  run loadReviews in this-procedure.
  
  RUN enable_UI.
  
  imAlert:hidden in frame {&frame-name} = true.
  warnInfo:hidden in frame {&frame-name} = true.

  ON 'value-changed' OF localQarFileInfo.tSelect in browse brwFiles 
  DO:
    find current localQarFileInfo no-error.
    if available localQarFileInfo  then
      selectedAudit = localQarFileInfo.identifier.
    apply 'U1' to browse brwFiles.
    bOpen:sensitive in frame {&frame-name} = can-find(first localQarFileInfo where localQarFileInfo.tSelect).
  END.

  bOpen:sensitive in frame {&frame-name} = can-find(first localQarFileInfo where localQarFileInfo.tSelect).

  OPEN QUERY brwFiles FOR EACH localQarFileInfo  by localQarFileInfo.modifiedDate descending.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.         
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copyData fOpen 
PROCEDURE copyData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable pSuccess as logical.
    
  do with frame fOpen:
  end.
  
  find first localQarFileInfo where  localQarFileInfo.tselect = true no-error.
    if available localQarFileInfo 
     then assign
            pSelected = true
            pFullFile = localQarFileInfo.fullpath.
  
  if search(pFullFile) = ?
   then
    do:
      message "Quality Assurance Review is not found." skip pFullFile view-as alert-box warning buttons ok.
      return.
    end.

  std-lo = dataset ActiveAuditData:read-xml("FILE", pFullFile, "EMPTY", "", ?, ?).

  publish "copyexistingdata" (input tbEscrow:checked,
                              input tbSection1:checked,
                              input tbSection2:checked,
                              input tbSection6:checked,
                              input table audit,
                              input table account,     
                              input table section,
                              input table question,    
                              input table finding,     
                              input table action,
                              input table bestPractice,
                              output pSuccess
                              ).
if  pSuccess 
 then
  apply "END-ERROR":U to self.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fOpen  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fOpen.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fOpen  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tbEscrow tbSection1 tbSection2 tbSection6 warnInfo 
      WITH FRAME fOpen.
  ENABLE bReset RECT-1 imAlert brwFiles tbEscrow tbSection1 tbSection2 
         tbSection6 warnInfo bCancel 
      WITH FRAME fOpen.
  VIEW FRAME fOpen.
  {&OPEN-BROWSERS-IN-QUERY-fOpen}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE loadReviews fOpen 
PROCEDURE loadReviews :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cAuditID as character.
  define variable cAuditType as character. 
  
  close query brwFiles. 
  
  empty temp-table ttlocalQarFileInfo.
  empty temp-table localQarFileInfo.
  
  publish "GetLocalQarFiles" (output table ttlocalQarFileInfo).

  publish "getcurrentvalue"("currentOpenAudit" , output cAuditID).
  publish "getcurrentvalue"("currentAuditType", output cAuditType).

  for each ttlocalQarFileInfo where ttlocalQarFileInfo.identifier ne cAuditID :
    create localQarFileInfo.
    buffer-copy ttlocalQarFileInfo to localQarFileInfo.
  end.

  {&OPEN-BROWSERS-IN-QUERY-fOpen}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData fOpen 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getStatus fOpen 
FUNCTION getStatus RETURNS CHARACTER
  (input pstat as char /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if pstat = "E" then
  RETURN "ERR".   /* Function return value. */
  else if pstat = "Q" then
  RETURN "QAR".   /* Function return value. */
  else if pstat = "U" then
  RETURN "Underwriter".   /* Function return value. */
  else if pstat = "T" then
  RETURN "TOR".   /* Function return value. */
  else  
  return "".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

