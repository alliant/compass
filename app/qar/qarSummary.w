&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: QARSummary.w

  Description: Quick snap-shot of complete QAR. 

  Input Parameters: QarId of the QAR
      

  Output Parameters:
      <none>

  Author: Anjly Chanana

  Created: 04-24-2017

  @modified  09/16/2020    AC        Changed to implement TOR Audits
             07/15/2021    SA        Task 83510 modified to add points and score

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
{tt/qaraudit.i &tableAlias="ttaudit" }  
def input parameter table for ttaudit.

/* Local Variable Definitions ---                                       */

{lib/std-def.i}

define variable hTabFrame as handle no-undo .
define variable hData as handle no-undo .
define variable hBut1 as handle no-undo .
define variable hBut2 as handle no-undo .
define variable hBut3 as handle no-undo .
define variable hBut4 as handle no-undo .

define variable pSuccess as logical no-undo.
define variable pIntegerValue as int no-undo.
define variable numminorfinding as integer no-undo.
define variable numintermediatefinding as integer no-undo.
define variable nummajorfinding as integer no-undo.
define variable seletedfile as char no-undo.
define variable seletedaccount as char no-undo.
define variable tAddress as char no-undo.
define variable cParentAddr as char no-undo.
define variable hColumn as widget-handle no-undo.
define variable hBrw as widget-handle no-undo.
define variable icount as integer no-undo.

define temp-table openDocs
 field qarid     as integer
 field hInstance as handle.

{tt/qaraudit.i &tableAlias="qarauditgot" }        /* audit     */
{tt/qarauditsection.i &tableAlias="sectiongot"}   /* section   */
{tt/qarquestiond.i &tableAlias="questiongot"} /* question  */
{tt/qarauditanswer.i &tableAlias="answergot"} /* question  */
{tt/qarauditfinding.i &tableAlias="findinggot"}   /* finding */
{tt/qarauditbp.i &tableAlias="bestpracticegot"}   /* bestPractice */
{tt/qarauditfile.i &tableAlias="agentFilegot"}    /* agentFile */
{tt/qarauditaction.i &tableAlias="qaractiongot" } /* action */
{tt/qaraccount.i  &tableAlias="escrowaccounts"}
{tt/qarnote.i &tableAlias="qarnote"}
{tt/sysuser.i}

define temp-table findingOfQuestions no-undo
  field questionseq as int
  field description as char
  field questionid as char
  field findingdescription as char
  field files as char
  field filesnamelist as char
  field accounts as char.

define temp-table BestPracticeOfQuestions no-undo
  field questionseq as int
  field description as char
  field questionid as char
  field bestPracticedescription as char.

define temp-table fileswithfinding no-undo
  field fileid as int
  field filename as char
  field closingdate as datetime
  field minorfinding as int
  field intermediatefinding as int
  field majorfinding as int.

define temp-table accountswithfinding no-undo
  field acctnumber as char
  field closingdate as datetime
  field minorfinding as int
  field intermediatefinding as int
  field majorfinding as int.

define temp-table answeredquestions no-undo
  field QuestionSeq as int
  field QuestionID as char
  field Questiondescription as char
  field OriginalSeverity as char
  field changedpriority as char
  field answer as char
  field fileID as int
  field fileNumber as char
  field uID as char
  field auditorname as char
  field dateAnswered as date .

define temp-table qarnotes like qarnote
  field username as char.

{tt/file.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwActions

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES qaractiongot fileswithfinding ~
answeredquestions bestPracticeOfQuestions accountswithfinding ~
findingOfQuestions qarnotes sectiongot

/* Definitions for BROWSE brwActions                                    */
&Scoped-define FIELDS-IN-QUERY-brwActions qaractiongot.description qaractiongot.stat qaractiongot.dueDate qaractiongot.note   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwActions   
&Scoped-define SELF-NAME brwActions
&Scoped-define OPEN-QUERY-brwActions if cbFilterActions:screen-value = "ALL" then OPEN QUERY brwActions FOR EACH qaractiongot . if cbFilterActions:screen-value = "Selected Question" then OPEN QUERY brwActions FOR EACH qaractiongot where qaractiongot.questionseq = findingOfQuestions.questionseq .
&Scoped-define TABLES-IN-QUERY-brwActions qaractiongot
&Scoped-define FIRST-TABLE-IN-QUERY-brwActions qaractiongot


/* Definitions for BROWSE brwAgentFiles                                 */
&Scoped-define FIELDS-IN-QUERY-brwAgentFiles fileswithfinding.filename fileswithfinding.closingDate fileswithfinding.minorfinding fileswithfinding.intermediatefinding fileswithfinding.majorfinding   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAgentFiles   
&Scoped-define SELF-NAME brwAgentFiles
&Scoped-define QUERY-STRING-brwAgentFiles preselect EACH fileswithfinding
&Scoped-define OPEN-QUERY-brwAgentFiles OPEN QUERY {&SELF-NAME} preselect EACH fileswithfinding.
&Scoped-define TABLES-IN-QUERY-brwAgentFiles fileswithfinding
&Scoped-define FIRST-TABLE-IN-QUERY-brwAgentFiles fileswithfinding


/* Definitions for BROWSE brwAnsweredQuestions                          */
&Scoped-define FIELDS-IN-QUERY-brwAnsweredQuestions answeredquestions.QuestionID answeredquestions.Questiondescription answeredquestions.OriginalSeverity answeredquestions.changedpriority answeredquestions.answer answeredquestions.fileNumber   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAnsweredQuestions   
&Scoped-define SELF-NAME brwAnsweredQuestions
&Scoped-define QUERY-STRING-brwAnsweredQuestions for EACH answeredquestions
&Scoped-define OPEN-QUERY-brwAnsweredQuestions OPEN QUERY brwAnsweredQuestions for EACH answeredquestions.
&Scoped-define TABLES-IN-QUERY-brwAnsweredQuestions answeredquestions
&Scoped-define FIRST-TABLE-IN-QUERY-brwAnsweredQuestions answeredquestions


/* Definitions for BROWSE brwBestPracticeOfQuestions                    */
&Scoped-define FIELDS-IN-QUERY-brwBestPracticeOfQuestions bestPracticeOfQuestions.questionID bestPracticeOfQuestions.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwBestPracticeOfQuestions   
&Scoped-define SELF-NAME brwBestPracticeOfQuestions
&Scoped-define QUERY-STRING-brwBestPracticeOfQuestions for EACH bestPracticeOfQuestions
&Scoped-define OPEN-QUERY-brwBestPracticeOfQuestions OPEN QUERY brwBestPracticeOfQuestions for EACH bestPracticeOfQuestions.
&Scoped-define TABLES-IN-QUERY-brwBestPracticeOfQuestions ~
bestPracticeOfQuestions
&Scoped-define FIRST-TABLE-IN-QUERY-brwBestPracticeOfQuestions bestPracticeOfQuestions


/* Definitions for BROWSE brwEscrowAccounts                             */
&Scoped-define FIELDS-IN-QUERY-brwEscrowAccounts accountswithfinding.acctnumber accountswithfinding.minorfinding accountswithfinding.intermediatefinding accountswithfinding.majorfinding   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwEscrowAccounts   
&Scoped-define SELF-NAME brwEscrowAccounts
&Scoped-define QUERY-STRING-brwEscrowAccounts preselect EACH accountswithfinding
&Scoped-define OPEN-QUERY-brwEscrowAccounts OPEN QUERY {&SELF-NAME} preselect EACH accountswithfinding.
&Scoped-define TABLES-IN-QUERY-brwEscrowAccounts accountswithfinding
&Scoped-define FIRST-TABLE-IN-QUERY-brwEscrowAccounts accountswithfinding


/* Definitions for BROWSE brwfindingofquestionaction                    */
&Scoped-define FIELDS-IN-QUERY-brwfindingofquestionaction findingOfQuestions.questionID findingOfQuestions.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwfindingofquestionaction   
&Scoped-define SELF-NAME brwfindingofquestionaction
&Scoped-define QUERY-STRING-brwfindingofquestionaction for EACH findingOfQuestions
&Scoped-define OPEN-QUERY-brwfindingofquestionaction OPEN QUERY brwfindingofquestionaction for EACH findingOfQuestions.
&Scoped-define TABLES-IN-QUERY-brwfindingofquestionaction ~
findingOfQuestions
&Scoped-define FIRST-TABLE-IN-QUERY-brwfindingofquestionaction findingOfQuestions


/* Definitions for BROWSE brwfindingofquestionfinding                   */
&Scoped-define FIELDS-IN-QUERY-brwfindingofquestionfinding findingOfQuestions.questionID findingOfQuestions.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwfindingofquestionfinding   
&Scoped-define SELF-NAME brwfindingofquestionfinding
&Scoped-define OPEN-QUERY-brwfindingofquestionfinding  if cbFilterQuestions:screen-value = "ALL" then  do:    brwAgentFiles:DESELECT-FOCUSED-ROW () no-error.    brwEscrowAccounts:DESELECT-FOCUSED-ROW () no-error.    OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions.  end.  else if cbFilterQuestions:screen-value = "Selected Agent File" then  do:    brwAgentFiles:SELECT-FOCUSED-ROW () no-error.    brwEscrowAccounts:DESELECT-FOCUSED-ROW () no-error.    OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions where lookup(seletedfile, ~
       findingOfQuestions.files, ~
       "|") <> 0.  end.  else if cbFilterQuestions:screen-value = "Selected Escrow Account" then  do:    brwEscrowAccounts:SELECT-FOCUSED-ROW () no-error.    brwAgentFiles:DESELECT-FOCUSED-ROW () no-error.    OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions where lookup(seletedaccount, ~
       findingOfQuestions.accounts, ~
       "|") <> 0.  end.
&Scoped-define TABLES-IN-QUERY-brwfindingofquestionfinding ~
findingOfQuestions
&Scoped-define FIRST-TABLE-IN-QUERY-brwfindingofquestionfinding findingOfQuestions


/* Definitions for BROWSE brwQarHistory                                 */
&Scoped-define FIELDS-IN-QUERY-brwQarHistory qarnotes.qarID qarnotes.qarID qarnotes.seq date(qarnotes.dateCreated) replace (qarnotes.username,"?", " ") @ qarnotes.username entry(index("ACQXP", qarnotes.stat), "Active,Completed,Queued,Cancelled,Planned") @ qarnotes.stat replace (qarnotes.comments,"?", " ") @ qarnotes.comments   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQarHistory   
&Scoped-define SELF-NAME brwQarHistory
&Scoped-define QUERY-STRING-brwQarHistory for EACH qarnotes
&Scoped-define OPEN-QUERY-brwQarHistory OPEN QUERY bewQarHistory for EACH qarnotes.
&Scoped-define TABLES-IN-QUERY-brwQarHistory qarnotes
&Scoped-define FIRST-TABLE-IN-QUERY-brwQarHistory qarnotes


/* Definitions for BROWSE brwScores                                     */
&Scoped-define FIELDS-IN-QUERY-brwScores sectiongot.sectionID sectiongot.sectionScore sectiongot.weight sectiongot.comments   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwScores   
&Scoped-define SELF-NAME brwScores
&Scoped-define QUERY-STRING-brwScores FOR EACH sectiongot by sectiongot.sectionID
&Scoped-define OPEN-QUERY-brwScores OPEN QUERY {&SELF-NAME} FOR EACH sectiongot by sectiongot.sectionID.
&Scoped-define TABLES-IN-QUERY-brwScores sectiongot
&Scoped-define FIRST-TABLE-IN-QUERY-brwScores sectiongot


/* Definitions for FRAME FR_1                                           */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FR_1 ~
    ~{&OPEN-QUERY-brwScores}

/* Definitions for FRAME FR_2                                           */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FR_2 ~
    ~{&OPEN-QUERY-brwAgentFiles}~
    ~{&OPEN-QUERY-brwEscrowAccounts}~
    ~{&OPEN-QUERY-brwfindingofquestionfinding}

/* Definitions for FRAME FR_3                                           */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FR_3 ~
    ~{&OPEN-QUERY-brwBestPracticeOfQuestions}

/* Definitions for FRAME FR_4                                           */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FR_4 ~
    ~{&OPEN-QUERY-brwActions}~
    ~{&OPEN-QUERY-brwfindingofquestionaction}

/* Definitions for FRAME FR_5                                           */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FR_5 ~
    ~{&OPEN-QUERY-brwQarHistory}

/* Definitions for FRAME FR_6                                           */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FR_6 ~
    ~{&OPEN-QUERY-brwAnsweredQuestions}

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE VARIABLE tAddr AS CHARACTER 
     VIEW-AS EDITOR NO-BOX
     SIZE 77.4 BY .76 TOOLTIP "Agency's street address" NO-UNDO.

DEFINE VARIABLE tDeliveredTo AS CHARACTER 
     VIEW-AS EDITOR NO-BOX
     SIZE 35 BY 1.52 TOOLTIP "List of recipients of the preliminary and final reports" NO-UNDO.

DEFINE VARIABLE tNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 103.8 BY 6.62 TOOLTIP "Miscellaneous Notes" NO-UNDO.

DEFINE VARIABLE tParentAddr AS CHARACTER 
     VIEW-AS EDITOR NO-BOX
     SIZE 34.2 BY 1.86 NO-UNDO.

DEFINE VARIABLE tSectioncomments AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 63 BY 6.62 TOOLTIP "Miscellaneous Notes" NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER FORMAT "X(10)":U 
     LABEL "Agent ID" 
      VIEW-AS TEXT 
     SIZE 14 BY .62 TOOLTIP "Agency's identifier assigned by Alliant" NO-UNDO.

DEFINE VARIABLE tAnswered AS CHARACTER FORMAT "X(256)":U 
     LABEL "Answered" 
      VIEW-AS TEXT 
     SIZE 7.6 BY .62 TOOLTIP "Number of answered questions in audit" NO-UNDO.

DEFINE VARIABLE tAuditEnd AS DATE FORMAT "99/99/99":U 
      VIEW-AS TEXT 
     SIZE 10 BY .62 NO-UNDO.

DEFINE VARIABLE tAuditor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Reviewer(s)" 
      VIEW-AS TEXT 
     SIZE 35 BY .62 NO-UNDO.

DEFINE VARIABLE tAuditScore AS INTEGER FORMAT ">>>":U INITIAL 0 
     LABEL "Points" 
      VIEW-AS TEXT 
     SIZE 6 BY .62 NO-UNDO.

DEFINE VARIABLE tBestPractices AS CHARACTER FORMAT "X(256)":U 
     LABEL "Best Practices" 
      VIEW-AS TEXT 
     SIZE 7 BY .62 TOOLTIP "Number of best practices" NO-UNDO.

DEFINE VARIABLE tContactEmail AS CHARACTER FORMAT "x(40)":U 
     LABEL "Email" 
      VIEW-AS TEXT 
     SIZE 38 BY .62 TOOLTIP "Contact's primary email address" NO-UNDO.

DEFINE VARIABLE tContactFax AS CHARACTER FORMAT "x(14)":U 
     LABEL "Fax" 
      VIEW-AS TEXT 
     SIZE 38 BY .62 NO-UNDO.

DEFINE VARIABLE tContactName AS CHARACTER FORMAT "X(60)":U 
     LABEL "Name" 
      VIEW-AS TEXT 
     SIZE 38 BY .62 TOOLTIP "Contact's full name" NO-UNDO.

DEFINE VARIABLE tContactOther AS CHARACTER FORMAT "x(14)":U 
     LABEL "Other" 
      VIEW-AS TEXT 
     SIZE 38 BY .62 TOOLTIP "Contact's secondary phone number" NO-UNDO.

DEFINE VARIABLE tContactPhone AS CHARACTER FORMAT "x(14)":U 
     LABEL "Phone" 
      VIEW-AS TEXT 
     SIZE 38 BY .62 TOOLTIP "Contact's primary phone number" NO-UNDO.

DEFINE VARIABLE tContactPosition AS CHARACTER FORMAT "X(256)":U 
     LABEL "Job Title" 
      VIEW-AS TEXT 
     SIZE 38 BY .62 NO-UNDO.

DEFINE VARIABLE tCorrective AS CHARACTER FORMAT "X(256)":U 
     LABEL "Corrective Actions" 
      VIEW-AS TEXT 
     SIZE 7 BY .62 TOOLTIP "Number of corrective actions" NO-UNDO.

DEFINE VARIABLE tIntermediate AS CHARACTER FORMAT "X(256)":U 
     LABEL "Intermediate Findings" 
      VIEW-AS TEXT 
     SIZE 7 BY .62 TOOLTIP "Number of intermediate findings" NO-UNDO.

DEFINE VARIABLE tLastRevenue AS DECIMAL FORMAT ">>>,>>>,>>9":U INITIAL 0 
     LABEL "Last Revenue $" 
      VIEW-AS TEXT 
     SIZE 14 BY .62 TOOLTIP "The annual gross revenue as reported for this agency" NO-UNDO.

DEFINE VARIABLE tMajor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Major Findings" 
      VIEW-AS TEXT 
     SIZE 7 BY .62 TOOLTIP "Number of major findings" NO-UNDO.

DEFINE VARIABLE tMinor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Minor Findings" 
      VIEW-AS TEXT 
     SIZE 6 BY .62 TOOLTIP "Number of minor findings" NO-UNDO.

DEFINE VARIABLE tName AS CHARACTER FORMAT "X(150)":U 
     LABEL "Name" 
      VIEW-AS TEXT 
     SIZE 77.4 BY .62 TOOLTIP "Full business name of the agency" NO-UNDO.

DEFINE VARIABLE tNumEmployees AS INTEGER FORMAT ">>,>>>":U INITIAL 0 
     LABEL "Employees" 
      VIEW-AS TEXT 
     SIZE 14 BY .62 TOOLTIP "Total number of employees for the agency" NO-UNDO.

DEFINE VARIABLE tNumOffices AS INTEGER FORMAT ">>>":U INITIAL 0 
     LABEL "Offices" 
      VIEW-AS TEXT 
     SIZE 8 BY .62 TOOLTIP "Total number of locations for this agency" NO-UNDO.

DEFINE VARIABLE tNumUnderwriters AS INTEGER FORMAT ">>":U INITIAL 0 
     LABEL "Underwriters" 
      VIEW-AS TEXT 
     SIZE 14 BY .62 TOOLTIP "Total number of title insurance underwriters working with this agency" NO-UNDO.

DEFINE VARIABLE tOnsiteFinishDate AS DATE FORMAT "99/99/99":U 
      VIEW-AS TEXT 
     SIZE 10 BY .62 NO-UNDO.

DEFINE VARIABLE tOnsiteStartDate AS DATE FORMAT "99/99/99":U 
      VIEW-AS TEXT 
     SIZE 10 BY .62 NO-UNDO.

DEFINE VARIABLE tParentName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
      VIEW-AS TEXT 
     SIZE 35 BY .62 NO-UNDO.

DEFINE VARIABLE tQarID AS CHARACTER FORMAT "X(10)":U 
     LABEL "Audit ID" 
      VIEW-AS TEXT 
     SIZE 10 BY .62 TOOLTIP "Agency's identifier assigned by Alliant" NO-UNDO.

DEFINE VARIABLE tRanking AS INTEGER FORMAT ">>,>>>":U INITIAL 0 
     LABEL "Ranking" 
      VIEW-AS TEXT 
     SIZE 35 BY .62 NO-UNDO.

DEFINE VARIABLE tRecommended AS CHARACTER FORMAT "X(256)":U 
     LABEL "Recommended Actions" 
      VIEW-AS TEXT 
     SIZE 7 BY .62 TOOLTIP "Number of recomended actions" NO-UNDO.

DEFINE VARIABLE tSchEndDate AS DATE FORMAT "99/99/99":U 
      VIEW-AS TEXT 
     SIZE 10 BY .62 NO-UNDO.

DEFINE VARIABLE tSchStartDate AS DATE FORMAT "99/99/99":U 
      VIEW-AS TEXT 
     SIZE 10 BY .62 NO-UNDO.

DEFINE VARIABLE tScorepct AS INTEGER FORMAT "zz9%":U INITIAL 0 
     LABEL "Score" 
      VIEW-AS TEXT 
     SIZE 9.2 BY .62 NO-UNDO.

DEFINE VARIABLE tServices AS CHARACTER FORMAT "X(256)":U 
     LABEL "Services" 
      VIEW-AS TEXT 
     SIZE 35 BY .62 NO-UNDO.

DEFINE VARIABLE tStartDate AS DATE FORMAT "99/99/99":U 
      VIEW-AS TEXT 
     SIZE 10 BY .62 NO-UNDO.

DEFINE VARIABLE tStat AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
      VIEW-AS TEXT 
     SIZE 18 BY .62 NO-UNDO.

DEFINE VARIABLE tSuggested AS CHARACTER FORMAT "X(256)":U 
     LABEL "Suggested Actions" 
      VIEW-AS TEXT 
     SIZE 7 BY .62 TOOLTIP "Number of suggested actions" NO-UNDO.

DEFINE VARIABLE tType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Audit Type" 
      VIEW-AS TEXT 
     SIZE 18 BY .62 NO-UNDO.

DEFINE VARIABLE tUnanswered AS CHARACTER FORMAT "X(256)":U 
     LABEL "Unanswered" 
      VIEW-AS TEXT 
     SIZE 7.6 BY .62 TOOLTIP "Number of unanswered questions in audit" NO-UNDO.

DEFINE RECTANGLE RECT-27
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 50.4 BY 8.91.

DEFINE RECTANGLE RECT-28
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 63 BY 5.62.

DEFINE RECTANGLE RECT-30
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 51.8 BY 5.62.

DEFINE RECTANGLE RECT-31
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 50.4 BY 5.62.

DEFINE RECTANGLE RECT-32
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 51.8 BY 8.91.

DEFINE RECTANGLE RECT-33
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 62.6 BY 4.05.

DEFINE VARIABLE tMainOffice AS LOGICAL INITIAL yes 
     LABEL "Main Office" 
     VIEW-AS TOGGLE-BOX
     SIZE 15 BY .81 TOOLTIP "If the location you are auditing is the main office" NO-UNDO.

DEFINE VARIABLE cbFilterQuestions AS CHARACTER FORMAT "X(256)":U 
     LABEL "Filter Questions On" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "ALL","Selected Agent File","Selected Escrow Account" 
     DROP-DOWN-LIST
     SIZE 44 BY 1 NO-UNDO.

DEFINE VARIABLE eFindings AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 164 BY 4 NO-UNDO.

DEFINE VARIABLE flFileOrAccounts AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent Files / Escrow Accounts" 
      VIEW-AS TEXT 
     SIZE 130 BY .62 NO-UNDO.

DEFINE RECTANGLE RECT-34
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 169 BY 14.95.

DEFINE VARIABLE eBestPractice AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 169.2 BY 4.05 NO-UNDO.

DEFINE VARIABLE cbFilterActions AS CHARACTER FORMAT "X(256)":U 
     LABEL "Filter Actions On" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "ALL","Selected Question" 
     DROP-DOWN-LIST
     SIZE 44 BY 1 NO-UNDO.

DEFINE VARIABLE eFindingsFR_3 AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 169 BY 3.67 NO-UNDO.

DEFINE VARIABLE tActions AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 169 BY 4.19 NO-UNDO.

DEFINE VARIABLE eHistoryComments AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 169.2 BY 4.62 NO-UNDO.

DEFINE VARIABLE eAnswer AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 40.6 BY 4 NO-UNDO.

DEFINE VARIABLE eQuestions AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 126 BY 4 NO-UNDO.

DEFINE VARIABLE answeredBy AS CHARACTER FORMAT "X(256)":U 
     LABEL "Answered By" 
     VIEW-AS FILL-IN 
     SIZE 33.4 BY 1 NO-UNDO.

DEFINE VARIABLE answeredDate AS CHARACTER FORMAT "X(256)":U 
     LABEL "Answered Date" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE BUTTON BUTTON-1 
     LABEL "General" 
     SIZE 15 BY 1.14.

DEFINE BUTTON BUTTON-2 
     LABEL "Findings" 
     SIZE 15 BY 1.14.

DEFINE BUTTON BUTTON-3 
     LABEL "Best Practices" 
     SIZE 16.6 BY 1.14.

DEFINE BUTTON BUTTON-4 
     LABEL "Actions" 
     SIZE 15 BY 1.14.

DEFINE BUTTON BUTTON-5 
     LABEL "History" 
     SIZE 15 BY 1.14.

DEFINE BUTTON BUTTON-6 
     LABEL "Questions" 
     SIZE 15.8 BY 1.14.

DEFINE BUTTON BUTTON-7 
     LABEL "Report" 
     SIZE 15.8 BY 1.14 TOOLTIP "Generate the final report".

DEFINE BUTTON BUTTON-8 
     LABEL "Documents" 
     SIZE 15.8 BY 1.14.

DEFINE RECTANGLE RECT-FR_1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 16 BY 1.38.

DEFINE RECTANGLE RECT-FR_2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 16 BY 1.38.

DEFINE RECTANGLE RECT-FR_3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17.6 BY 1.38.

DEFINE RECTANGLE RECT-FR_4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 16 BY 1.38.

DEFINE RECTANGLE RECT-FR_5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 16 BY 1.38.

DEFINE RECTANGLE RECT-FR_6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17 BY 1.38.

DEFINE RECTANGLE RECT-FR_7
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17 BY 1.38.

DEFINE RECTANGLE RECT-FR_8
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17 BY 1.38.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwActions FOR 
      qaractiongot SCROLLING.

DEFINE QUERY brwAgentFiles FOR 
      fileswithfinding SCROLLING.

DEFINE QUERY brwAnsweredQuestions FOR 
      answeredquestions SCROLLING.

DEFINE QUERY brwBestPracticeOfQuestions FOR 
      bestPracticeOfQuestions SCROLLING.

DEFINE QUERY brwEscrowAccounts FOR 
      accountswithfinding SCROLLING.

DEFINE QUERY brwfindingofquestionaction FOR 
      findingOfQuestions SCROLLING.

DEFINE QUERY brwfindingofquestionfinding FOR 
      findingOfQuestions SCROLLING.

DEFINE QUERY brwQarHistory FOR 
      qarnotes SCROLLING.

DEFINE QUERY brwScores FOR 
      sectiongot SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwActions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwActions C-Win _FREEFORM
  QUERY brwActions DISPLAY
      qaractiongot.description     format "x(60)"   label "Description"
qaractiongot.stat                             label "Status"
qaractiongot.dueDate         format 99/99/99  label "Due Date"
qaractiongot.note            format "x(100)"  label "Note"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 169 BY 5.81 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwAgentFiles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAgentFiles C-Win _FREEFORM
  QUERY brwAgentFiles DISPLAY
      fileswithfinding.filename               label "File Number"
fileswithfinding.closingDate          label "Closing Date" format "99/99/99"
fileswithfinding.minorfinding         label "Minor Findings"
fileswithfinding.intermediatefinding  label "Intermediate Findings"
fileswithfinding.majorfinding         label "Major Findings"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 83 BY 8.81 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwAnsweredQuestions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAnsweredQuestions C-Win _FREEFORM
  QUERY brwAnsweredQuestions DISPLAY
      answeredquestions.QuestionID          label "Question ID"               format "x(10)"
answeredquestions.Questiondescription label "Description"  format "x(80)"
answeredquestions.OriginalSeverity   column-label "Original!Severity"  format "x(12)"
answeredquestions.changedpriority    column-label "Changed!Severity"  format "x(12)"
answeredquestions.answer             label "Answer"  format "x(8)"
answeredquestions.fileNumber         label "File Number"  format "x(15)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 169.2 BY 19.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwBestPracticeOfQuestions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwBestPracticeOfQuestions C-Win _FREEFORM
  QUERY brwBestPracticeOfQuestions DISPLAY
      bestPracticeOfQuestions.questionID  label "Question ID"
bestPracticeOfQuestions.description label "Description"  format "x(120)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 169.2 BY 21.86 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwEscrowAccounts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwEscrowAccounts C-Win _FREEFORM
  QUERY brwEscrowAccounts DISPLAY
      accountswithfinding.acctnumber          label "Account Number"         format "x(25)"
accountswithfinding.minorfinding         label "Minor Findings"              width 15
accountswithfinding.intermediatefinding  label "Intermediate Findings"       width 20 
accountswithfinding.majorfinding         label "Major Findings"               width 12
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 83 BY 8.81 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwfindingofquestionaction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwfindingofquestionaction C-Win _FREEFORM
  QUERY brwfindingofquestionaction DISPLAY
      findingOfQuestions.questionID  label "Question ID"
findingOfQuestions.description label "Description"  format "x(120)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 169 BY 8.81 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwfindingofquestionfinding
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwfindingofquestionfinding C-Win _FREEFORM
  QUERY brwfindingofquestionfinding DISPLAY
      findingOfQuestions.questionID  label "Question ID"
findingOfQuestions.description label "Description"  format "x(120)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 164 BY 7.81 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwQarHistory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQarHistory C-Win _FREEFORM
  QUERY brwQarHistory DISPLAY
      qarnotes.qarID label "ID" format "zzzzzzzzzz"      qarnotes.qarID       label "QAR ID"
qarnotes.seq label "Sequence" 
date(qarnotes.dateCreated) label "Date Created"
replace (qarnotes.username,"?", " ")  @ qarnotes.username format "x(35)" label "User" 
entry(index("ACQXP", qarnotes.stat), "Active,Completed,Queued,Cancelled,Planned") @ qarnotes.stat label "Status" format "x(12)" 
replace (qarnotes.comments,"?", " ")  @ qarnotes.comments  format "x(100)" label "Comments"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 169.2 BY 21.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwScores
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwScores C-Win _FREEFORM
  QUERY brwScores DISPLAY
      sectiongot.sectionID    label "Section" 
sectiongot.sectionScore label "Score"    format "zz"
sectiongot.weight       label "Weight"   format "9.9"
sectiongot.comments     label "Comments" format "x(50)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-SCROLLBAR-VERTICAL SIZE 63.2 BY 8.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 179 BY 30.95 WIDGET-ID 100.

DEFINE FRAME tabframe
     BUTTON-1 AT ROW 1.24 COL 2.8 WIDGET-ID 2
     BUTTON-2 AT ROW 1.24 COL 18.6 WIDGET-ID 4
     BUTTON-3 AT ROW 1.24 COL 34.4 WIDGET-ID 6
     BUTTON-4 AT ROW 1.24 COL 51.8 WIDGET-ID 8
     BUTTON-5 AT ROW 1.24 COL 67.6 WIDGET-ID 18
     BUTTON-6 AT ROW 1.24 COL 83.2 WIDGET-ID 22
     BUTTON-7 AT ROW 1.24 COL 99.8 WIDGET-ID 26
     BUTTON-8 AT ROW 1.24 COL 116 WIDGET-ID 28
     RECT-FR_1 AT ROW 1.14 COL 2.4 WIDGET-ID 10
     RECT-FR_2 AT ROW 1.14 COL 18.2 WIDGET-ID 12
     RECT-FR_3 AT ROW 1.14 COL 34 WIDGET-ID 14
     RECT-FR_4 AT ROW 1.14 COL 51.4 WIDGET-ID 16
     RECT-FR_5 AT ROW 1.14 COL 67.2 WIDGET-ID 20
     RECT-FR_6 AT ROW 1.14 COL 82.8 WIDGET-ID 24
     RECT-FR_7 AT ROW 1.14 COL 99.4 WIDGET-ID 30
     RECT-FR_8 AT ROW 1.14 COL 115.8 WIDGET-ID 32
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 179 BY 30.95 WIDGET-ID 200.

DEFINE FRAME FR_4
     cbFilterActions AT ROW 1.62 COL 18.2 COLON-ALIGNED WIDGET-ID 58
     brwfindingofquestionaction AT ROW 3.57 COL 3.6 WIDGET-ID 1600
     eFindingsFR_3 AT ROW 13.24 COL 3.6 NO-LABEL WIDGET-ID 50
     brwActions AT ROW 17.71 COL 3.6 WIDGET-ID 800
     tActions AT ROW 23.91 COL 3.6 NO-LABEL WIDGET-ID 22
     "Actions" VIEW-AS TEXT
          SIZE 10 BY .62 AT ROW 17.1 COL 3.6 WIDGET-ID 24
     "Finding" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 12.62 COL 3.6 WIDGET-ID 56
     "Questions with Findings" VIEW-AS TEXT
          SIZE 29 BY .62 AT ROW 2.95 COL 3.6 WIDGET-ID 54
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.4 ROW 2.76
         SIZE 175 BY 28.71
         TITLE "Actions" WIDGET-ID 700.

DEFINE FRAME FR_3
     brwBestPracticeOfQuestions AT ROW 2.05 COL 3.6 WIDGET-ID 600
     eBestPractice AT ROW 24.24 COL 3.6 NO-LABEL WIDGET-ID 32
     "Questions with Best Practices" VIEW-AS TEXT
          SIZE 33.2 BY .62 AT ROW 1.38 COL 3.6 WIDGET-ID 36
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.4 ROW 2.76
         SIZE 175 BY 28.71
         TITLE "Best Practices" WIDGET-ID 1200.

DEFINE FRAME FR_6
     brwAnsweredQuestions AT ROW 1.52 COL 3.6 WIDGET-ID 600
     eQuestions AT ROW 22.24 COL 4 NO-LABEL WIDGET-ID 2
     eAnswer AT ROW 22.24 COL 132.4 NO-LABEL WIDGET-ID 4
     answeredBy AT ROW 26.71 COL 17 COLON-ALIGNED WIDGET-ID 10
     answeredDate AT ROW 26.71 COL 72 COLON-ALIGNED WIDGET-ID 12
     "Question" VIEW-AS TEXT
          SIZE 9.4 BY .62 AT ROW 21.52 COL 4.6 WIDGET-ID 6
     "Answer" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 21.48 COL 132.8 WIDGET-ID 8
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.4 ROW 2.76
         SIZE 175 BY 28.71
         TITLE "Questions" WIDGET-ID 2000.

DEFINE FRAME FR_5
     brwQarHistory AT ROW 1.48 COL 3.6 WIDGET-ID 1900
     eHistoryComments AT ROW 23.76 COL 3.6 NO-LABEL WIDGET-ID 32
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.4 ROW 2.76
         SIZE 175 BY 28.71
         TITLE "History" WIDGET-ID 1800.

DEFINE FRAME FR_2
     cbFilterQuestions AT ROW 1.62 COL 21.2 COLON-ALIGNED WIDGET-ID 64
     brwAgentFiles AT ROW 3.62 COL 4 WIDGET-ID 1500
     brwEscrowAccounts AT ROW 3.62 COL 89.8 WIDGET-ID 1100
     brwfindingofquestionfinding AT ROW 13.76 COL 6.4 WIDGET-ID 600
     eFindings AT ROW 21.95 COL 6.4 NO-LABEL WIDGET-ID 22
     flFileOrAccounts AT ROW 26.76 COL 35.6 COLON-ALIGNED WIDGET-ID 62
     "Questions with Findings" VIEW-AS TEXT
          SIZE 22.4 BY .62 AT ROW 12.86 COL 4.8 WIDGET-ID 36
     "Agent Files" VIEW-AS TEXT
          SIZE 13.4 BY .62 AT ROW 2.95 COL 4 WIDGET-ID 58
     "Escrow Accounts" VIEW-AS TEXT
          SIZE 20 BY .62 AT ROW 2.95 COL 89.8 WIDGET-ID 56
     RECT-34 AT ROW 13.19 COL 4 WIDGET-ID 28
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.4 ROW 2.76
         SIZE 175 BY 28.71
         TITLE "Findings" WIDGET-ID 600.

DEFINE FRAME FR_1
     tAddr AT ROW 4.29 COL 13.6 NO-LABEL WIDGET-ID 158
     tMainOffice AT ROW 7.38 COL 7 WIDGET-ID 48
     tParentAddr AT ROW 8.38 COL 70.8 NO-LABEL WIDGET-ID 162
     brwScores AT ROW 12.05 COL 109 WIDGET-ID 1400
     tDeliveredTo AT ROW 13.52 COL 70.4 NO-LABEL WIDGET-ID 46
     tNotes AT ROW 21.86 COL 3.6 NO-LABEL WIDGET-ID 114
     tSectioncomments AT ROW 21.86 COL 109.2 NO-LABEL WIDGET-ID 152
     tStat AT ROW 1.24 COL 67.4 COLON-ALIGNED WIDGET-ID 208 NO-TAB-STOP 
     tScorepct AT ROW 1.24 COL 95.2 COLON-ALIGNED WIDGET-ID 118 NO-TAB-STOP 
     tQarID AT ROW 1.29 COL 11.6 COLON-ALIGNED WIDGET-ID 112 NO-TAB-STOP 
     tType AT ROW 1.29 COL 37.4 COLON-ALIGNED WIDGET-ID 188 NO-TAB-STOP 
     tAgentID AT ROW 2.29 COL 11.6 COLON-ALIGNED WIDGET-ID 122
     tSchStartDate AT ROW 2.67 COL 129.8 COLON-ALIGNED NO-LABEL WIDGET-ID 176 NO-TAB-STOP 
     tSchEndDate AT ROW 2.67 COL 152.2 COLON-ALIGNED NO-LABEL WIDGET-ID 182 NO-TAB-STOP 
     tName AT ROW 3.29 COL 11.6 COLON-ALIGNED WIDGET-ID 4
     tOnsiteStartDate AT ROW 3.57 COL 129.8 COLON-ALIGNED NO-LABEL WIDGET-ID 192 NO-TAB-STOP 
     tOnsiteFinishDate AT ROW 3.57 COL 152.2 COLON-ALIGNED NO-LABEL WIDGET-ID 194 NO-TAB-STOP 
     tStartDate AT ROW 4.48 COL 129.8 COLON-ALIGNED NO-LABEL WIDGET-ID 186 NO-TAB-STOP 
     tAuditEnd AT ROW 4.48 COL 152.2 COLON-ALIGNED NO-LABEL WIDGET-ID 180 NO-TAB-STOP 
     tAuditScore AT ROW 6.38 COL 129.4 COLON-ALIGNED WIDGET-ID 156 NO-TAB-STOP 
     tAnswered AT ROW 6.38 COL 158.8 COLON-ALIGNED WIDGET-ID 128
     tNumEmployees AT ROW 7.38 COL 36.6 COLON-ALIGNED WIDGET-ID 38
     tParentName AT ROW 7.38 COL 68.6 COLON-ALIGNED WIDGET-ID 52
     tMajor AT ROW 7.38 COL 129.4 COLON-ALIGNED WIDGET-ID 136
     tUnanswered AT ROW 7.38 COL 158.8 COLON-ALIGNED WIDGET-ID 144
     tNumOffices AT ROW 8.38 COL 12 COLON-ALIGNED WIDGET-ID 36
     tNumUnderwriters AT ROW 8.38 COL 36.6 COLON-ALIGNED WIDGET-ID 40
     tIntermediate AT ROW 8.38 COL 129.4 COLON-ALIGNED WIDGET-ID 134
     tCorrective AT ROW 8.38 COL 158.8 COLON-ALIGNED WIDGET-ID 132
     tLastRevenue AT ROW 9.38 COL 36.6 COLON-ALIGNED WIDGET-ID 44
     tMinor AT ROW 9.38 COL 129.4 COLON-ALIGNED WIDGET-ID 138
     tRecommended AT ROW 9.38 COL 158.8 COLON-ALIGNED WIDGET-ID 140
     tBestPractices AT ROW 10.38 COL 129.4 COLON-ALIGNED WIDGET-ID 130
     tSuggested AT ROW 10.38 COL 158.8 COLON-ALIGNED WIDGET-ID 142
     tContactName AT ROW 13.52 COL 13 COLON-ALIGNED WIDGET-ID 84
     tContactPhone AT ROW 14.52 COL 13 COLON-ALIGNED WIDGET-ID 18
     tContactFax AT ROW 15.52 COL 13 COLON-ALIGNED WIDGET-ID 20
     tAuditor AT ROW 15.52 COL 68.4 COLON-ALIGNED WIDGET-ID 80
     tContactOther AT ROW 16.52 COL 13 COLON-ALIGNED WIDGET-ID 22
     tRanking AT ROW 16.52 COL 68.4 COLON-ALIGNED WIDGET-ID 88
     tContactEmail AT ROW 17.52 COL 13 COLON-ALIGNED WIDGET-ID 24
     tServices AT ROW 17.52 COL 68.4 COLON-ALIGNED WIDGET-ID 150
     tContactPosition AT ROW 18.52 COL 13 COLON-ALIGNED WIDGET-ID 66
     "Parent Company" VIEW-AS TEXT
          SIZE 16 BY .62 AT ROW 5.71 COL 58 WIDGET-ID 64
     "Address:" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 4.29 COL 5 WIDGET-ID 160
     "Start" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.86 COL 133.8 WIDGET-ID 196
     "Address:" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 8.38 COL 62.2 WIDGET-ID 164
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.4 ROW 2.76
         SIZE 175 BY 28.71 WIDGET-ID 400.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME FR_1
     "Contact" VIEW-AS TEXT
          SIZE 8.2 BY .62 AT ROW 11.76 COL 4.8 WIDGET-ID 86
     "Section Comments" VIEW-AS TEXT
          SIZE 21.2 BY .62 AT ROW 21.24 COL 109.2 WIDGET-ID 154
     "Dates" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.14 COL 110 WIDGET-ID 184
     "Results" VIEW-AS TEXT
          SIZE 7.8 BY .62 AT ROW 5.76 COL 110 WIDGET-ID 26
     "Finish" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.86 COL 156.2 WIDGET-ID 198
     "Schedule:" VIEW-AS TEXT
          SIZE 10 BY .62 AT ROW 2.67 COL 117.2 WIDGET-ID 200
     "Notes" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 21.24 COL 4 WIDGET-ID 148
     "Onsite:" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 3.57 COL 120.2 WIDGET-ID 202
     "Audit:" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 4.48 COL 121.4 WIDGET-ID 204
     "General" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 11.71 COL 58 WIDGET-ID 74
     "Delivered To:" VIEW-AS TEXT
          SIZE 13 BY .62 AT ROW 13.52 COL 57.2 WIDGET-ID 120
     "Location" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 5.71 COL 4.8 WIDGET-ID 70
     RECT-27 AT ROW 12.05 COL 4 WIDGET-ID 78
     RECT-30 AT ROW 6 COL 55.8 WIDGET-ID 62
     RECT-31 AT ROW 6 COL 4 WIDGET-ID 68
     RECT-32 AT ROW 12.05 COL 55.8 WIDGET-ID 72
     RECT-28 AT ROW 6 COL 109.2 WIDGET-ID 126
     RECT-33 AT ROW 1.48 COL 109.4 WIDGET-ID 174
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.4 ROW 2.76
         SIZE 175 BY 28.71
         TITLE "General" WIDGET-ID 400.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Audit Inspect"
         HEIGHT             = 30.95
         WIDTH              = 179
         MAX-HEIGHT         = 33.67
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.67
         VIRTUAL-WIDTH      = 273.2
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME FR_1:FRAME = FRAME tabframe:HANDLE
       FRAME FR_2:FRAME = FRAME tabframe:HANDLE
       FRAME FR_3:FRAME = FRAME tabframe:HANDLE
       FRAME FR_4:FRAME = FRAME tabframe:HANDLE
       FRAME FR_5:FRAME = FRAME tabframe:HANDLE
       FRAME FR_6:FRAME = FRAME tabframe:HANDLE
       FRAME tabframe:FRAME = FRAME DEFAULT-FRAME:HANDLE.

/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* SETTINGS FOR FRAME FR_1
                                                                        */
/* BROWSE-TAB brwScores tParentAddr FR_1 */
ASSIGN 
       brwScores:ALLOW-COLUMN-SEARCHING IN FRAME FR_1 = TRUE
       brwScores:COLUMN-RESIZABLE IN FRAME FR_1       = TRUE.

/* SETTINGS FOR RECTANGLE RECT-28 IN FRAME FR_1
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR tAddr IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tAddr:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tAgentID IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tAgentID:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tAnswered IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tAnswered:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tAuditEnd IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tAuditEnd:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tAuditor IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tAuditor:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tAuditScore IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tAuditScore:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tBestPractices IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tBestPractices:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tContactEmail IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tContactEmail:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tContactFax IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tContactFax:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tContactName IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tContactName:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tContactOther IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tContactOther:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tContactPhone IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tContactPhone:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tContactPosition IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tContactPosition:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tCorrective IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tCorrective:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR EDITOR tDeliveredTo IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tDeliveredTo:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tIntermediate IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tIntermediate:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tLastRevenue IN FRAME FR_1
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       tLastRevenue:HIDDEN IN FRAME FR_1           = TRUE
       tLastRevenue:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR TOGGLE-BOX tMainOffice IN FRAME FR_1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tMajor IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tMajor:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tMinor IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tMinor:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tName IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tName:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR EDITOR tNotes IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tNotes:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tNumEmployees IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tNumEmployees:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tNumOffices IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tNumOffices:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tNumUnderwriters IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tNumUnderwriters:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tOnsiteFinishDate IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tOnsiteFinishDate:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tOnsiteStartDate IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tOnsiteStartDate:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR EDITOR tParentAddr IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tParentAddr:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tParentName IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tParentName:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tQarID IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tQarID:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tRanking IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tRanking:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tRecommended IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tRecommended:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tSchEndDate IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tSchEndDate:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tSchStartDate IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tSchStartDate:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tScorepct IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tScorepct:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR EDITOR tSectioncomments IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tSectioncomments:READ-ONLY IN FRAME FR_1        = TRUE.

ASSIGN 
       tServices:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tStartDate IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tStartDate:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tStat IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tStat:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tSuggested IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tSuggested:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tType IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tType:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FILL-IN tUnanswered IN FRAME FR_1
   NO-ENABLE                                                            */
ASSIGN 
       tUnanswered:READ-ONLY IN FRAME FR_1        = TRUE.

/* SETTINGS FOR FRAME FR_2
                                                                        */
/* BROWSE-TAB brwAgentFiles cbFilterQuestions FR_2 */
/* BROWSE-TAB brwEscrowAccounts brwAgentFiles FR_2 */
/* BROWSE-TAB brwfindingofquestionfinding brwEscrowAccounts FR_2 */
/* SETTINGS FOR BROWSE brwAgentFiles IN FRAME FR_2
   NO-ENABLE                                                            */
ASSIGN 
       brwAgentFiles:ALLOW-COLUMN-SEARCHING IN FRAME FR_2 = TRUE
       brwAgentFiles:COLUMN-RESIZABLE IN FRAME FR_2       = TRUE.

/* SETTINGS FOR BROWSE brwEscrowAccounts IN FRAME FR_2
   NO-ENABLE                                                            */
ASSIGN 
       brwEscrowAccounts:ALLOW-COLUMN-SEARCHING IN FRAME FR_2 = TRUE
       brwEscrowAccounts:COLUMN-RESIZABLE IN FRAME FR_2       = TRUE.

ASSIGN 
       brwfindingofquestionfinding:ALLOW-COLUMN-SEARCHING IN FRAME FR_2 = TRUE
       brwfindingofquestionfinding:COLUMN-RESIZABLE IN FRAME FR_2       = TRUE.

ASSIGN 
       eFindings:READ-ONLY IN FRAME FR_2        = TRUE.

/* SETTINGS FOR FILL-IN flFileOrAccounts IN FRAME FR_2
   NO-ENABLE                                                            */
ASSIGN 
       flFileOrAccounts:READ-ONLY IN FRAME FR_2        = TRUE.

/* SETTINGS FOR FRAME FR_3
                                                                        */
/* BROWSE-TAB brwBestPracticeOfQuestions TEXT-22 FR_3 */
ASSIGN 
       brwBestPracticeOfQuestions:ALLOW-COLUMN-SEARCHING IN FRAME FR_3 = TRUE
       brwBestPracticeOfQuestions:COLUMN-RESIZABLE IN FRAME FR_3       = TRUE.

ASSIGN 
       eBestPractice:READ-ONLY IN FRAME FR_3        = TRUE.

/* SETTINGS FOR FRAME FR_4
                                                                        */
/* BROWSE-TAB brwfindingofquestionaction cbFilterActions FR_4 */
/* BROWSE-TAB brwActions eFindingsFR_3 FR_4 */
ASSIGN 
       brwActions:ALLOW-COLUMN-SEARCHING IN FRAME FR_4 = TRUE
       brwActions:COLUMN-RESIZABLE IN FRAME FR_4       = TRUE.

ASSIGN 
       brwfindingofquestionaction:ALLOW-COLUMN-SEARCHING IN FRAME FR_4 = TRUE
       brwfindingofquestionaction:COLUMN-RESIZABLE IN FRAME FR_4       = TRUE.

ASSIGN 
       eFindingsFR_3:READ-ONLY IN FRAME FR_4        = TRUE.

ASSIGN 
       tActions:READ-ONLY IN FRAME FR_4        = TRUE.

/* SETTINGS FOR FRAME FR_5
                                                                        */
/* BROWSE-TAB brwQarHistory 1 FR_5 */
ASSIGN 
       brwQarHistory:ALLOW-COLUMN-SEARCHING IN FRAME FR_5 = TRUE
       brwQarHistory:COLUMN-RESIZABLE IN FRAME FR_5       = TRUE.

ASSIGN 
       eHistoryComments:READ-ONLY IN FRAME FR_5        = TRUE.

/* SETTINGS FOR FRAME FR_6
                                                                        */
/* BROWSE-TAB brwAnsweredQuestions TEXT-21 FR_6 */
/* SETTINGS FOR FILL-IN answeredBy IN FRAME FR_6
   NO-ENABLE                                                            */
ASSIGN 
       answeredBy:READ-ONLY IN FRAME FR_6        = TRUE.

/* SETTINGS FOR FILL-IN answeredDate IN FRAME FR_6
   NO-ENABLE                                                            */
ASSIGN 
       answeredDate:READ-ONLY IN FRAME FR_6        = TRUE.

ASSIGN 
       brwAnsweredQuestions:ALLOW-COLUMN-SEARCHING IN FRAME FR_6 = TRUE
       brwAnsweredQuestions:COLUMN-RESIZABLE IN FRAME FR_6       = TRUE.

/* SETTINGS FOR EDITOR eAnswer IN FRAME FR_6
   NO-ENABLE                                                            */
ASSIGN 
       eAnswer:READ-ONLY IN FRAME FR_6        = TRUE.

/* SETTINGS FOR EDITOR eQuestions IN FRAME FR_6
   NO-ENABLE                                                            */
ASSIGN 
       eQuestions:READ-ONLY IN FRAME FR_6        = TRUE.

/* SETTINGS FOR FRAME tabframe
                                                                        */
/* SETTINGS FOR RECTANGLE RECT-FR_1 IN FRAME tabframe
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-FR_2 IN FRAME tabframe
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-FR_3 IN FRAME tabframe
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-FR_4 IN FRAME tabframe
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-FR_5 IN FRAME tabframe
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-FR_6 IN FRAME tabframe
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-FR_7 IN FRAME tabframe
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-FR_8 IN FRAME tabframe
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwActions
/* Query rebuild information for BROWSE brwActions
     _START_FREEFORM
if cbFilterActions:screen-value = "ALL" then
OPEN QUERY brwActions FOR EACH qaractiongot .
if cbFilterActions:screen-value = "Selected Question" then
OPEN QUERY brwActions FOR EACH qaractiongot where qaractiongot.questionseq = findingOfQuestions.questionseq .
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwActions */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAgentFiles
/* Query rebuild information for BROWSE brwAgentFiles
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} preselect EACH fileswithfinding
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAgentFiles */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAnsweredQuestions
/* Query rebuild information for BROWSE brwAnsweredQuestions
     _START_FREEFORM
OPEN QUERY brwAnsweredQuestions for EACH answeredquestions
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAnsweredQuestions */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwBestPracticeOfQuestions
/* Query rebuild information for BROWSE brwBestPracticeOfQuestions
     _START_FREEFORM
OPEN QUERY brwBestPracticeOfQuestions for EACH bestPracticeOfQuestions
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwBestPracticeOfQuestions */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwEscrowAccounts
/* Query rebuild information for BROWSE brwEscrowAccounts
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} preselect EACH accountswithfinding
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwEscrowAccounts */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwfindingofquestionaction
/* Query rebuild information for BROWSE brwfindingofquestionaction
     _START_FREEFORM
OPEN QUERY brwfindingofquestionaction for EACH findingOfQuestions
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwfindingofquestionaction */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwfindingofquestionfinding
/* Query rebuild information for BROWSE brwfindingofquestionfinding
     _START_FREEFORM
 if cbFilterQuestions:screen-value = "ALL" then
 do:
   brwAgentFiles:DESELECT-FOCUSED-ROW () no-error.
   brwEscrowAccounts:DESELECT-FOCUSED-ROW () no-error.
   OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions.
 end.
 else if cbFilterQuestions:screen-value = "Selected Agent File" then
 do:
   brwAgentFiles:SELECT-FOCUSED-ROW () no-error.
   brwEscrowAccounts:DESELECT-FOCUSED-ROW () no-error.
   OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions where lookup(seletedfile, findingOfQuestions.files, "|") <> 0.
 end.
 else if cbFilterQuestions:screen-value = "Selected Escrow Account" then
 do:
   brwEscrowAccounts:SELECT-FOCUSED-ROW () no-error.
   brwAgentFiles:DESELECT-FOCUSED-ROW () no-error.
   OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions where lookup(seletedaccount, findingOfQuestions.accounts, "|") <> 0.
 end.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwfindingofquestionfinding */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQarHistory
/* Query rebuild information for BROWSE brwQarHistory
     _START_FREEFORM
OPEN QUERY bewQarHistory for EACH qarnotes
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQarHistory */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwScores
/* Query rebuild information for BROWSE brwScores
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH sectiongot by sectiongot.sectionID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwScores */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Audit Inspect */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Audit Inspect */
DO:
  /* This event will close the window and terminate the procedure.  */
  if ttaudit.stat = "C" then
  do:
    run emptyDataset in hData.
    IF VALID-HANDLE(hData) THEN 
    DELETE WIDGET hData. 
  end.

  run deleteMaintainDocs(input ttaudit.QarID).

  empty temp-table ttaudit.
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwActions
&Scoped-define FRAME-NAME FR_4
&Scoped-define SELF-NAME brwActions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwActions C-Win
ON ROW-DISPLAY OF brwActions IN FRAME FR_4
DO:
  if current-result-row("brwActions") modulo 2 = 0 then
  assign
    qaractiongot.description:bgcolor in browse brwActions = 17
    qaractiongot.description:fgcolor in browse brwActions = 0
    qaractiongot.stat:bgcolor in browse brwActions = 17
    qaractiongot.stat:fgcolor in browse brwActions = 0
    qaractiongot.dueDate:bgcolor in browse brwActions = 17
    qaractiongot.dueDate:fgcolor in browse brwActions = 0
    qaractiongot.note:bgcolor in browse brwActions = 17   
    qaractiongot.note:fgcolor in browse brwActions = 0.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwActions C-Win
ON VALUE-CHANGED OF brwActions IN FRAME FR_4
DO:
  find current qaractiongot no-error.
  if available qaractiongot then
    assign
      tActions:screen-value = qaractiongot.description    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAgentFiles
&Scoped-define FRAME-NAME FR_2
&Scoped-define SELF-NAME brwAgentFiles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAgentFiles C-Win
ON ROW-DISPLAY OF brwAgentFiles IN FRAME FR_2
DO:
  if current-result-row("brwAgentFiles") modulo 2 = 0 then
  assign
    fileswithfinding.filename:bgcolor in browse brwAgentFiles = 17
    fileswithfinding.filename:fgcolor in browse brwAgentFiles = 0
    fileswithfinding.closingDate:bgcolor in browse brwAgentFiles = 17
    fileswithfinding.closingDate:fgcolor in browse brwAgentFiles = 0
    fileswithfinding.minorfinding:bgcolor in browse brwAgentFiles = 17
    fileswithfinding.minorfinding:fgcolor in browse brwAgentFiles = 0
    fileswithfinding.intermediatefinding:bgcolor in browse brwAgentFiles = 17
    fileswithfinding.intermediatefinding:fgcolor in browse brwAgentFiles = 0
    fileswithfinding.majorfinding:bgcolor in browse brwAgentFiles = 17
    fileswithfinding.majorfinding:fgcolor in browse brwAgentFiles = 0.       

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAgentFiles C-Win
ON VALUE-CHANGED OF brwAgentFiles IN FRAME FR_2
DO:
  find current fileswithfinding no-error.
  if available fileswithfinding then
    seletedfile = string(fileswithfinding.fileid).

  find current accountswithfinding no-error.
  if available accountswithfinding then
    seletedaccount = string(accountswithfinding.acctnumber).

  if cbFilterQuestions:screen-value = "ALL" then
  do:
    brwAgentFiles:DESELECT-FOCUSED-ROW () no-error.
    brwEscrowAccounts:DESELECT-FOCUSED-ROW () no-error.
    OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions.
  end.
  else if cbFilterQuestions:screen-value = "Selected Agent File" then
  do:
    brwAgentFiles:SELECT-FOCUSED-ROW () no-error.
    brwEscrowAccounts:DESELECT-FOCUSED-ROW () no-error.
    OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions where lookup(seletedfile, findingOfQuestions.files, "|") <> 0.
  end.
  else if cbFilterQuestions:screen-value = "Selected Escrow Account" then
  do:
    brwEscrowAccounts:SELECT-FOCUSED-ROW () no-error.
    brwAgentFiles:DESELECT-FOCUSED-ROW () no-error.
    OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions where lookup(seletedaccount, findingOfQuestions.accounts, "|") <> 0.
  end.
  apply 'value-changed' to brwfindingofquestionfinding.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAnsweredQuestions
&Scoped-define FRAME-NAME FR_6
&Scoped-define SELF-NAME brwAnsweredQuestions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAnsweredQuestions C-Win
ON ROW-DISPLAY OF brwAnsweredQuestions IN FRAME FR_6
DO:
  if current-result-row("brwAnsweredQuestions") modulo 2 = 0 then
  assign
    answeredquestions.QuestionID         :bgcolor in browse brwAnsweredQuestions = 17
    answeredquestions.Questiondescription:bgcolor in browse brwAnsweredQuestions = 17
    answeredquestions.OriginalSeverity   :bgcolor in browse brwAnsweredQuestions = 17
    answeredquestions.changedpriority    :bgcolor in browse brwAnsweredQuestions = 17
    answeredquestions.answer             :bgcolor in browse brwAnsweredQuestions = 17
    answeredquestions.fileNumber         :bgcolor in browse brwAnsweredQuestions = 17
    answeredquestions.QuestionID         :fgcolor in browse brwAnsweredQuestions = 0
    answeredquestions.Questiondescription:fgcolor in browse brwAnsweredQuestions = 0
    answeredquestions.OriginalSeverity   :fgcolor in browse brwAnsweredQuestions = 0
    answeredquestions.changedpriority    :fgcolor in browse brwAnsweredQuestions = 0
    answeredquestions.answer             :fgcolor in browse brwAnsweredQuestions = 0 
    answeredquestions.fileNumber         :fgcolor in browse brwAnsweredQuestions = 0  .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAnsweredQuestions C-Win
ON VALUE-CHANGED OF brwAnsweredQuestions IN FRAME FR_6
DO:
  find current answeredquestions no-error.
  if available answeredquestions then
    assign
      eQuestions:screen-value in frame FR_6 = answeredquestions.Questiondescription
      eAnswer:screen-value in frame FR_6 = answeredquestions.answer
      answeredBy:screen-value in frame FR_6 = answeredquestions.auditorname
      answeredDate:screen-value in frame FR_6 = if answeredquestions.Dateanswered = ? then "" else string(answeredquestions.Dateanswered)     .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwBestPracticeOfQuestions
&Scoped-define FRAME-NAME FR_3
&Scoped-define SELF-NAME brwBestPracticeOfQuestions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBestPracticeOfQuestions C-Win
ON ROW-DISPLAY OF brwBestPracticeOfQuestions IN FRAME FR_3
DO:
  if current-result-row("brwBestPracticeOfQuestions") modulo 2 = 0 then
  assign
    bestPracticeOfQuestions.questionID:bgcolor in browse brwBestPracticeOfQuestions = 17
    bestPracticeOfQuestions.questionID:fgcolor in browse brwBestPracticeOfQuestions = 0
    bestPracticeOfQuestions.description:bgcolor in browse brwBestPracticeOfQuestions = 17
    bestPracticeOfQuestions.description:fgcolor in browse brwBestPracticeOfQuestions = 0.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBestPracticeOfQuestions C-Win
ON VALUE-CHANGED OF brwBestPracticeOfQuestions IN FRAME FR_3
DO:
  find current bestPracticeOfQuestions no-error.
  if available bestPracticeOfQuestions then
      eBestPractice:screen-value in frame FR_3 = bestPracticeOfQuestions.bestPracticedescription.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwEscrowAccounts
&Scoped-define FRAME-NAME FR_2
&Scoped-define SELF-NAME brwEscrowAccounts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwEscrowAccounts C-Win
ON DEFAULT-ACTION OF brwEscrowAccounts IN FRAME FR_2
DO:
  find current findinggot no-error.
    if available findinggot then
      eFindings:screen-value =  findinggot.comments.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwEscrowAccounts C-Win
ON ROW-DISPLAY OF brwEscrowAccounts IN FRAME FR_2
DO:
  if current-result-row("brwEscrowAccounts") modulo 2 = 0 then
  assign
    accountswithfinding.acctnumber:bgcolor in browse brwEscrowAccounts = 17
    accountswithfinding.acctnumber:fgcolor in browse brwEscrowAccounts = 0
    accountswithfinding.minorfinding:bgcolor in browse brwEscrowAccounts = 17
    accountswithfinding.minorfinding:fgcolor in browse brwEscrowAccounts = 0
    accountswithfinding.intermediatefinding:bgcolor in browse brwEscrowAccounts = 17
    accountswithfinding.intermediatefinding:fgcolor in browse brwEscrowAccounts = 0
    accountswithfinding.majorfinding:bgcolor in browse brwEscrowAccounts = 17
    accountswithfinding.majorfinding:fgcolor in browse brwEscrowAccounts = 0.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwEscrowAccounts C-Win
ON VALUE-CHANGED OF brwEscrowAccounts IN FRAME FR_2
DO:
  find current fileswithfinding no-error.
  if available fileswithfinding then
    seletedfile = string(fileswithfinding.fileid).

  find current accountswithfinding no-error.
  if available accountswithfinding then
    seletedaccount = string(accountswithfinding.acctnumber).


  if cbFilterQuestions:screen-value = "ALL" then
  do:
    brwAgentFiles:DESELECT-FOCUSED-ROW () no-error.
    brwEscrowAccounts:DESELECT-FOCUSED-ROW () no-error.
    OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions.
  end.
  else if cbFilterQuestions:screen-value = "Selected Agent File" then
  do:
    brwAgentFiles:SELECT-FOCUSED-ROW () no-error.
    brwEscrowAccounts:DESELECT-FOCUSED-ROW () no-error.
    OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions where lookup(seletedfile, findingOfQuestions.files, "|") <> 0.
  end.
  else if cbFilterQuestions:screen-value = "Selected Escrow Account" then
  do:
    brwEscrowAccounts:SELECT-FOCUSED-ROW () no-error.
    brwAgentFiles:DESELECT-FOCUSED-ROW () no-error.
    OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions where lookup(seletedaccount, findingOfQuestions.accounts, "|") <> 0.
  end.
       apply 'value-changed' to brwfindingofquestionfinding.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwfindingofquestionaction
&Scoped-define FRAME-NAME FR_4
&Scoped-define SELF-NAME brwfindingofquestionaction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwfindingofquestionaction C-Win
ON ROW-DISPLAY OF brwfindingofquestionaction IN FRAME FR_4
DO:
  if current-result-row("brwfindingofquestionaction") modulo 2 = 0 then
  assign
    findingOfQuestions.questionID:bgcolor in browse brwfindingofquestionaction = 17
    findingOfQuestions.questionID:fgcolor in browse brwfindingofquestionaction = 0
    findingOfQuestions.description:bgcolor in browse brwfindingofquestionaction = 17
    findingOfQuestions.description:fgcolor in browse brwfindingofquestionaction = 0.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwfindingofquestionaction C-Win
ON VALUE-CHANGED OF brwfindingofquestionaction IN FRAME FR_4
DO:
  find current findingOfQuestions no-error.
  if available findingOfQuestions then
  do:
    eFindingsFR_3:screen-value in frame FR_4 = findingOfQuestions.findingdescription.
    if cbFilterActions:screen-value = "ALL" then
    if cbFilterActions:screen-value = "ALL" then
    do:
      brwfindingofquestionaction:DESELECT-FOCUSED-ROW () no-error.
      OPEN QUERY brwActions FOR EACH qaractiongot .
    end.
    if cbFilterActions:screen-value = "Selected Question" then
    do:
      brwfindingofquestionaction:select-focused-row () no-error.
      OPEN QUERY brwActions FOR EACH qaractiongot where qaractiongot.questionseq = findingOfQuestions.questionseq .
    end.
    apply 'value-changed' to   brwActions .
  end.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwfindingofquestionfinding
&Scoped-define FRAME-NAME FR_2
&Scoped-define SELF-NAME brwfindingofquestionfinding
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwfindingofquestionfinding C-Win
ON ROW-DISPLAY OF brwfindingofquestionfinding IN FRAME FR_2
DO:
if current-result-row("brwfindingofquestionfinding") modulo 2 = 0 then
assign
  findingOfQuestions.questionID:bgcolor in browse brwfindingofquestionfinding = 17
  findingOfQuestions.questionID:fgcolor in browse brwfindingofquestionfinding = 0
  findingOfQuestions.description:bgcolor in browse brwfindingofquestionfinding = 17
  findingOfQuestions.description:fgcolor in browse brwfindingofquestionfinding = 0. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwfindingofquestionfinding C-Win
ON VALUE-CHANGED OF brwfindingofquestionfinding IN FRAME FR_2
DO:
 find current findingOfQuestions no-error.
 if available findingOfQuestions then
 do:
   do icount = 1 to num-entries(findingOfQuestions.files, "|"):
      find first agentfilegot where agentfilegot.fileid = int(entry(icount,findingOfQuestions.files, "|")) no-error.
      if available agentfilegot then
         findingOfQuestions.filesnamelist = findingOfQuestions.filesnamelist + agentfilegot.filenumber + "|".
   end.
   findingOfQuestions.filesnamelist = trim(findingOfQuestions.filesnamelist,"|")   .
   if cbFilterQuestions:screen-value = "Selected Agent File" or findingOfQuestions.filesnamelist ne "" then
      flFileOrAccounts:screen-value = replace(findingOfQuestions.filesnamelist, "|", ", ").
    else if cbFilterQuestions:screen-value = "Selected Escrow Account" or findingOfQuestions.accounts ne "" then
      flFileOrAccounts:screen-value = replace(findingOfQuestions.accounts, "|", ", ").
    else 
        flFileOrAccounts:screen-value = "".
    eFindings:screen-value in frame FR_2 = findingOfQuestions.findingdescription.
    findingOfQuestions.filesnamelist = "".
  end.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQarHistory
&Scoped-define FRAME-NAME FR_5
&Scoped-define SELF-NAME brwQarHistory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQarHistory C-Win
ON ROW-DISPLAY OF brwQarHistory IN FRAME FR_5
DO:
 if current-result-row("brwQarHistory") modulo 2 = 0 then
  assign
    qarnotes.qarID :bgcolor in browse brwQarHistory = 17
    qarnotes.qarID :fgcolor in browse brwQarHistory = 0
    qarnotes.seq:bgcolor in browse brwQarHistory = 17
    qarnotes.seq :fgcolor in browse brwQarHistory = 0
    qarnotes.username:bgcolor in browse brwQarHistory = 17        
    qarnotes.username:fgcolor in browse brwQarHistory = 0
    qarnotes.stat:bgcolor in browse brwQarHistory = 17       
    qarnotes.stat :fgcolor in browse brwQarHistory = 0
    qarnotes.comments:bgcolor in browse brwQarHistory = 17
    qarnotes.comments:fgcolor in browse brwQarHistory = 0
    hcolumn:bgcolor = 17
    hcolumn:fgcolor = 0.    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQarHistory C-Win
ON VALUE-CHANGED OF brwQarHistory IN FRAME FR_5
DO:
find current qarnotes no-error.
if available qarnotes then
    eHistoryComments:screen-value in frame FR_5 = qarnotes.comments.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwScores
&Scoped-define FRAME-NAME FR_1
&Scoped-define SELF-NAME brwScores
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwScores C-Win
ON ROW-DISPLAY OF brwScores IN FRAME FR_1
DO:
  if current-result-row("brwScores") modulo 2 = 0 then
  assign
   sectiongot.sectionID:bgcolor in browse brwScores = 17
   sectiongot.sectionID:fgcolor in browse brwScores = 0
   sectiongot.sectionScore:bgcolor in browse brwScores = 17
   sectiongot.sectionScore:fgcolor in browse brwScores = 0
   sectiongot.weight:bgcolor in browse brwScores = 17
   sectiongot.weight:fgcolor in browse brwScores = 0
   sectiongot.comments:bgcolor in browse brwScores = 17
   sectiongot.comments:fgcolor in browse brwScores = 0.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwScores C-Win
ON VALUE-CHANGED OF brwScores IN FRAME FR_1
DO:
  find current sectiongot no-error.
  if available sectiongot then
      tSectioncomments:screen-value = sectiongot.comments.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME tabframe
&Scoped-define SELF-NAME BUTTON-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-1 C-Win
ON CHOOSE OF BUTTON-1 IN FRAME tabframe /* General */
DO:
  view frame FR_1.
  hide frame FR_2.
  hide frame FR_3.
  hide frame FR_4.
  hide frame FR_5.
  hide frame FR_6.

  enable RECT-FR_1 with frame tabframe in window C-Win.
  disable RECT-FR_2 with frame tabframe .
  disable RECT-FR_3 with frame tabframe .
  disable RECT-FR_4 with frame tabframe .
  disable RECT-FR_5 with frame tabframe .
  disable RECT-FR_6 with frame tabframe .
  disable RECT-FR_7 with frame tabframe .
  disable RECT-FR_8 with frame tabframe .
 
 assign 
   RECT-FR_1:visible = true
   RECT-FR_1:hidden = false
   RECT-FR_2:visible = false
   RECT-FR_2:hidden = true
   RECT-FR_3:visible = false
   RECT-FR_3:hidden = true
   RECT-FR_4:visible = false
   RECT-FR_4:hidden = true
   RECT-FR_5:visible = false
   RECT-FR_5:hidden = true
   RECT-FR_6:visible = false
   RECT-FR_6:hidden = true
   RECT-FR_7:visible = false
   RECT-FR_7:hidden = true
   RECT-FR_8:visible = false
   RECT-FR_8:hidden = true.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-2 C-Win
ON CHOOSE OF BUTTON-2 IN FRAME tabframe /* Findings */
DO:
  view frame FR_2.
  hide frame FR_1.
  hide frame FR_3.
  hide frame FR_4.
  hide frame FR_5.
  hide frame FR_6.

  enable RECT-FR_2 with frame tabframe in window C-Win.
  disable RECT-FR_1 with frame tabframe .
  disable RECT-FR_3 with frame tabframe .
  disable RECT-FR_4 with frame tabframe .
  disable RECT-FR_5 with frame tabframe .
  disable RECT-FR_6 with frame tabframe .
  disable RECT-FR_7 with frame tabframe .
  disable RECT-FR_8 with frame tabframe .

 assign
   RECT-FR_2:visible = true
   RECT-FR_2:hidden = false
   RECT-FR_1:visible = false
   RECT-FR_1:hidden = true
   RECT-FR_3:visible = false
   RECT-FR_3:hidden = true
   RECT-FR_4:visible = false
   RECT-FR_4:hidden = true
   RECT-FR_5:visible = false
   RECT-FR_5:hidden = true
   RECT-FR_6:visible = false
   RECT-FR_6:hidden = true
   RECT-FR_7:visible = false
   RECT-FR_7:hidden = true
   RECT-FR_8:visible = false
   RECT-FR_8:hidden = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-3 C-Win
ON CHOOSE OF BUTTON-3 IN FRAME tabframe /* Best Practices */
DO:
  view frame FR_3.
  hide frame FR_2.
  hide frame FR_4.
  hide frame FR_1.
  hide frame FR_5.
  hide frame FR_6.

  enable RECT-FR_3 with frame tabframe in window C-Win.
  disable RECT-FR_2 with frame tabframe .
  disable RECT-FR_4 with frame tabframe .
  disable RECT-FR_1 with frame tabframe .
  disable RECT-FR_5 with frame tabframe .
  disable RECT-FR_6 with frame tabframe .
  disable RECT-FR_7 with frame tabframe .
  disable RECT-FR_8 with frame tabframe .

 assign
   RECT-FR_3:visible = true
   RECT-FR_3:hidden = false
   RECT-FR_2:visible = false
   RECT-FR_2:hidden = true
   RECT-FR_4:visible = false
   RECT-FR_4:hidden = true
   RECT-FR_1:visible = false
   RECT-FR_1:hidden = true
   RECT-FR_5:visible = false
   RECT-FR_5:hidden = true
   RECT-FR_6:visible = false
   RECT-FR_6:hidden = true
   RECT-FR_7:visible = false
   RECT-FR_7:hidden = true
   RECT-FR_8:visible = false
   RECT-FR_8:hidden = true.  .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-4 C-Win
ON CHOOSE OF BUTTON-4 IN FRAME tabframe /* Actions */
DO:
  view frame FR_4.
  hide frame FR_2.
  hide frame FR_1.
  hide frame FR_3.
  hide frame FR_5.
  hide frame FR_6.

  enable RECT-FR_4 with frame tabframe in window C-Win.
  disable RECT-FR_2 with frame tabframe .
  disable RECT-FR_1 with frame tabframe .
  disable RECT-FR_3 with frame tabframe .
  disable RECT-FR_5 with frame tabframe .
  disable RECT-FR_6 with frame tabframe .
  disable RECT-FR_7 with frame tabframe .
  disable RECT-FR_8 with frame tabframe .

 assign
   RECT-FR_4:visible = true
   RECT-FR_4:hidden = false
   RECT-FR_2:visible = false
   RECT-FR_2:hidden = true
   RECT-FR_1:visible = false
   RECT-FR_1:hidden = true
   RECT-FR_3:visible = false
   RECT-FR_3:hidden = true
   RECT-FR_5:visible = false
   RECT-FR_5:hidden = true
   RECT-FR_6:visible = false
   RECT-FR_6:hidden = true
   RECT-FR_7:visible = false
   RECT-FR_7:hidden = true
   RECT-FR_8:visible = false
   RECT-FR_8:hidden = true  .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-5 C-Win
ON CHOOSE OF BUTTON-5 IN FRAME tabframe /* History */
DO:
  view frame FR_5.
  hide frame FR_1.
  hide frame FR_2.
  hide frame FR_3.
  hide frame FR_4.
  hide frame FR_6.

  enable RECT-FR_5 with frame tabframe in window C-Win.
  disable RECT-FR_2 with frame tabframe .
  disable RECT-FR_1 with frame tabframe .
  disable RECT-FR_3 with frame tabframe .
  disable RECT-FR_4 with frame tabframe .
  disable RECT-FR_6 with frame tabframe .
  disable RECT-FR_7 with frame tabframe .
  disable RECT-FR_8 with frame tabframe .

 assign
   RECT-FR_5:visible = true
   RECT-FR_5:hidden = false
   RECT-FR_1:visible = false
   RECT-FR_1:hidden = true
   RECT-FR_2:visible = false
   RECT-FR_2:hidden = true
   RECT-FR_3:visible = false
   RECT-FR_3:hidden = true
   RECT-FR_4:visible = false
   RECT-FR_4:hidden = true
   RECT-FR_6:visible = false
   RECT-FR_6:hidden = true
   RECT-FR_7:visible = false
   RECT-FR_7:hidden = true
   RECT-FR_8:visible = false
   RECT-FR_8:hidden = true.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-6
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-6 C-Win
ON CHOOSE OF BUTTON-6 IN FRAME tabframe /* Questions */
DO:
  view frame FR_6.
  hide frame FR_1.
  hide frame FR_2.
  hide frame FR_3.
  hide frame FR_4.
  hide frame FR_5.

  enable RECT-FR_6 with frame tabframe in window C-Win.
  disable RECT-FR_1 with frame tabframe .
  disable RECT-FR_2 with frame tabframe .
  disable RECT-FR_3 with frame tabframe .
  disable RECT-FR_4 with frame tabframe .
  disable RECT-FR_5 with frame tabframe .
  disable RECT-FR_7 with frame tabframe .
  disable RECT-FR_8 with frame tabframe .
 
 assign
   RECT-FR_6:visible = true
   RECT-FR_6:hidden = false
   RECT-FR_1:visible = false
   RECT-FR_1:hidden = true
   RECT-FR_2:visible = false
   RECT-FR_2:hidden = true
   RECT-FR_3:visible = false
   RECT-FR_3:hidden = true
   RECT-FR_4:visible = false
   RECT-FR_4:hidden = true
   RECT-FR_5:visible = false
   RECT-FR_5:hidden = true
   RECT-FR_7:visible = false
   RECT-FR_7:hidden = true
   RECT-FR_8:visible = false
   RECT-FR_8:hidden = true  .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-7
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-7 C-Win
ON CHOOSE OF BUTTON-7 IN FRAME tabframe /* Report */
DO:
  enable RECT-FR_7 with frame tabframe in window C-Win.
  disable RECT-FR_1 with frame tabframe .
  disable RECT-FR_2 with frame tabframe .
  disable RECT-FR_3 with frame tabframe .
  disable RECT-FR_4 with frame tabframe .
  disable RECT-FR_5 with frame tabframe .
  disable RECT-FR_6 with frame tabframe .
  disable RECT-FR_8 with frame tabframe .
 
 assign
   RECT-FR_7:visible = true
   RECT-FR_7:hidden = false
   RECT-FR_1:visible = false
   RECT-FR_1:hidden = true
   RECT-FR_2:visible = false
   RECT-FR_2:hidden = true
   RECT-FR_3:visible = false
   RECT-FR_3:hidden = true
   RECT-FR_4:visible = false
   RECT-FR_4:hidden = true
   RECT-FR_5:visible = false
   RECT-FR_5:hidden = true
   RECT-FR_6:visible = false
   RECT-FR_6:hidden = true
   RECT-FR_8:visible = false
   RECT-FR_8:hidden = true  .

  find first ttaudit no-error.
  if available ttaudit then
   do:
    define variable tFile as character no-undo.
    run server/getauditreport.p (input ttaudit.qarID,
                                 output tFile,
                                 output std-lo,
                                 output std-ch).
    
    if search(tFile) <> ? then
     run util/openfile.p (tFile).
   end.
                               
                               
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-8
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-8 C-Win
ON CHOOSE OF BUTTON-8 IN FRAME tabframe /* Documents */
DO:
define variable hDocWindow as handle no-undo.

enable RECT-FR_8 with frame tabframe in window C-Win.
  disable RECT-FR_1 with frame tabframe .
  disable RECT-FR_2 with frame tabframe .
  disable RECT-FR_3 with frame tabframe .
  disable RECT-FR_4 with frame tabframe .
  disable RECT-FR_5 with frame tabframe .
  disable RECT-FR_6 with frame tabframe .
  disable RECT-FR_7 with frame tabframe .
 
 assign
   RECT-FR_8:visible = true
   RECT-FR_8:hidden = false
   RECT-FR_1:visible = false
   RECT-FR_1:hidden = true
   RECT-FR_2:visible = false
   RECT-FR_2:hidden = true
   RECT-FR_3:visible = false
   RECT-FR_3:hidden = true
   RECT-FR_4:visible = false
   RECT-FR_4:hidden = true
   RECT-FR_5:visible = false
   RECT-FR_5:hidden = true
   RECT-FR_6:visible = false
   RECT-FR_6:hidden = true
   RECT-FR_7:visible = false
   RECT-FR_7:hidden = true  .

find first openDocs
  where openDocs.qarid = ttAudit.qarID no-error.

if available openDocs and valid-handle(openDocs.hInstance)
  then run ShowWindow in openDocs.hInstance no-error.
    
else
  run documents.w persistent set hDocWindow
    ("QAM",
     "/qar/" + string(ttaudit.auditYear) + "/" + string(ttAudit.qarID) + "/",
     "Quality Assurance Management " + string(date(ttAudit.auditFinishDate)),
     "new,delete,open,modify,send,request",
     "qar",
     string(ttAudit.qarID),
     "").

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FR_4
&Scoped-define SELF-NAME cbFilterActions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbFilterActions C-Win
ON VALUE-CHANGED OF cbFilterActions IN FRAME FR_4 /* Filter Actions On */
DO:
  if cbFilterActions:screen-value = "ALL" then
  do:
    brwfindingofquestionaction:DESELECT-FOCUSED-ROW () no-error.
    OPEN QUERY brwActions FOR EACH qaractiongot .
  end.
  if cbFilterActions:screen-value = "Selected Question" then
  do:
    brwfindingofquestionaction:select-focused-row () no-error.
    OPEN QUERY brwActions FOR EACH qaractiongot where qaractiongot.questionseq = findingOfQuestions.questionseq .
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FR_2
&Scoped-define SELF-NAME cbFilterQuestions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbFilterQuestions C-Win
ON VALUE-CHANGED OF cbFilterQuestions IN FRAME FR_2 /* Filter Questions On */
DO:
  find current fileswithfinding no-error.
  if available fileswithfinding then
    seletedfile = string(fileswithfinding.fileid).

  find current accountswithfinding no-error.
  if available accountswithfinding then
    seletedaccount = string(accountswithfinding.acctnumber).


  if cbFilterQuestions:screen-value = "ALL" then
  do:
    brwAgentFiles:DESELECT-FOCUSED-ROW () no-error.
    brwEscrowAccounts:DESELECT-FOCUSED-ROW () no-error.
    brwAgentFiles:sensitive = false no-error.
    brwEscrowAccounts:sensitive = false no-error.
    OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions.
  end.
  else if cbFilterQuestions:screen-value = "Selected Agent File" then
  do:
    brwAgentFiles:sensitive = true no-error.
    brwEscrowAccounts:sensitive = false no-error.
    brwAgentFiles:SELECT-FOCUSED-ROW () no-error.
    brwEscrowAccounts:DESELECT-FOCUSED-ROW () no-error.
    OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions where lookup(seletedfile, findingOfQuestions.files, "|") <> 0.
  end.
  else if cbFilterQuestions:screen-value = "Selected Escrow Account" then
  do:
    brwAgentFiles:sensitive = false no-error.
    brwEscrowAccounts:sensitive = true no-error.
    brwEscrowAccounts:SELECT-FOCUSED-ROW () no-error.
    brwAgentFiles:DESELECT-FOCUSED-ROW () no-error.
    OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions where lookup(seletedaccount, findingOfQuestions.accounts, "|") <> 0.
  end.
       apply 'value-changed' to brwfindingofquestionfinding.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwActions
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

 FRAME tabframe:frame = FRAME DEFAULT-FRAME:handle.
 ENABLE ALL WITH FRAME tabframe.
 ENABLE ALL WITH FRAME FR_2.
 ENABLE ALL WITH FRAME FR_1.
 ENABLE ALL WITH FRAME FR_3.
 ENABLE ALL WITH FRAME FR_4.
 ENABLE ALL WITH FRAME FR_5.
 ENABLE ALL WITH FRAME FR_6.
 frame FR_1:move-to-top().

 view frame FR_1.
 hide frame FR_2.
 hide frame FR_3.
 hide frame FR_4.
 hide frame FR_5.
 hide frame FR_6.

 ENABLE RECT-FR_1 WITH FRAME tabframe IN WINDOW C-Win.
 disable RECT-FR_2 WITH FRAME tabframe .
 disable RECT-FR_3 WITH FRAME tabframe .
 disable RECT-FR_4 WITH FRAME tabframe .
 disable RECT-FR_5 WITH FRAME tabframe .
 disable RECT-FR_6 WITH FRAME tabframe .
 disable RECT-FR_7 WITH FRAME tabframe .
 disable RECT-FR_8 WITH FRAME tabframe .

 RECT-FR_1:visible = true.
 RECT-FR_1:hidden = false.
 RECT-FR_2:visible = false.
 RECT-FR_2:hidden = true.
 RECT-FR_3:visible = false.
 RECT-FR_3:hidden = true.
 RECT-FR_4:visible = false.
 RECT-FR_4:hidden = true.
 RECT-FR_5:visible = false.
 RECT-FR_5:hidden = true.
 RECT-FR_6:visible = false.
 RECT-FR_6:hidden = true.
 RECT-FR_7:visible = false.
 RECT-FR_7:hidden = true.
 RECT-FR_8:visible = false.
 RECT-FR_8:hidden = true.

cbFilterQuestions:screen-value = "ALL" .
cbFilterActions:screen-value = "Selected Question".

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

subscribe to "OpenMaintainDocs" anywhere.
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   RUN enable_UI.
   C-Win:move-to-top().

  find first ttaudit no-error.
  if available ttaudit then
  do:
    publish "OpenMaintainQar" (string(ttaudit.QarID), this-procedure) .
    C-Win:title = C-Win:title + " - " + string(ttaudit.QarID) + " - " + string(ttaudit.name) + " (" + string(ttaudit.agentID) + ")" .
    run qardatasummary.p persistent set hData.
    run ViewWindow in this-procedure.
    run InspectAudit in hData
                        (input string(ttaudit.QarID),
                         output table qarauditgot,     
                         output table sectiongot,      
                         output table questiongot,   
                         output table answergot, 
                         output table agentFilegot,    
                         output table findinggot,      
                         output table qaractiongot,    
                         output table bestPracticegot, 
                         output table escrowaccounts,
                         output table qarnote, 
                         output table sysuser,
                         output pSuccess).
   
    if not pSuccess then
      {lib/pbhide.i}
    {lib/pbupdate.i "'Fetching Audit, please wait...'" 80}
    find first qarauditgot where qarauditgot.QarID = integer(string(ttaudit.QarID)) no-error.
    if available qarauditgot then
    do:
      assign
        tAddress =  if (qarauditgot.addr = "?") or (qarauditgot.addr = "") then "" else qarauditgot.addr
        tAddress =  tAddress + if (qarauditgot.city = "?") or (qarauditgot.city = "") then "" else ", " + qarauditgot.city
        tAddress =  tAddress + if (qarauditgot.state = "?") or (qarauditgot.state = "") then "" else ", " +  qarauditgot.state
        tAddress =  tAddress + if (qarauditgot.zip = "?") or (qarauditgot.zip = "") then "" else "  " + qarauditgot.zip .

      assign
        cParentAddr =  if (qarauditgot.parentaddr = "?") or (qarauditgot.parentaddr = "") then "" else qarauditgot.parentaddr
        cParentAddr =  cParentAddr + if (qarauditgot.parentcity = "?") or (qarauditgot.parentcity = "") then "" else ", " + qarauditgot.parentcity
        cParentAddr =  cParentAddr + if (qarauditgot.parentstate = "?") or (qarauditgot.parentstate = "") then "" else ", " +  qarauditgot.parentstate
        cParentAddr =  cParentAddr + if (qarauditgot.parentzip = "?") or (qarauditgot.parentzip = "") then "" else "  " + qarauditgot.parentzip.
              
     
      assign
        tQarID:screen-value            =   if qarauditgot.QarID = ?             then "" else string(qarauditgot.QarID)  
        tAgentID:screen-value          =   if qarauditgot.agentID = "?"         then "" else qarauditgot.agentID        
        tStat:screen-value             =   if qarauditgot.stat = "A"            then "Active" else if qarauditgot.stat = "X" then "Cancelled" else if qarauditgot.stat = "P" then "Planned"  else if qarauditgot.stat = "C" then "Completed" else if qarauditgot.stat = "Q" then "Queued" else ""    
        tauditScore:screen-value       =   if qarauditgot.auditScore = ?        then "" else string(qarauditgot.auditScore)  
        tScorepct:screen-value         =   if qarauditgot.grade = ?             then "" else string(qarauditgot.grade)
        tName:screen-value             =   if qarauditgot.name = "?"            then "" else qarauditgot.name                              
        tAddr:screen-value             =   trim(tAddress)                            
        tMainOffice:screen-value       =   if qarauditgot.mainOffice = ?        then string(true) else string(qarauditgot.mainOffice)          
        tNumOffices:screen-value       =   if qarauditgot.numOffices = ?        then "" else string(qarauditgot.numOffices)                  
        tNumEmployees:screen-value     =   if qarauditgot.numEmployees = ?      then "" else string(qarauditgot.numEmployees)               
        tNumUnderwriters:screen-value  =   if qarauditgot.numUnderwriters = ?   then "" else string(qarauditgot.numUnderwriters)           
        tParentName:screen-value       =   if qarauditgot.parentname = "?"      then "" else qarauditgot.parentname               
        tParentAddr:screen-value       =   trim(cParentAddr)                    
        tContactName:screen-value      =   if qarauditgot.contactName = "?"     then "" else qarauditgot.contactName         
        tContactPhone:screen-value     =   if qarauditgot.contactPhone = "?"    then "" else qarauditgot.contactPhone      
        tContactFax:screen-value       =   if qarauditgot.contactFax = "?"      then "" else qarauditgot.contactFax        
        tContactOther:screen-value     =   if qarauditgot.ContactOther = "?"    then "" else qarauditgot.ContactOther     
        tContactEmail:screen-value     =   if qarauditgot.contactEmail = "?"    then "" else qarauditgot.contactEmail    
        tContactPosition:screen-value  =   if qarauditgot.contactPosition = "?" then "" else qarauditgot.contactPosition   
        tDeliveredTo:screen-value      =   if qarauditgot.deliveredTo = "?"     then "" else qarauditgot.deliveredTo    
        tSchStartDate:screen-value     =   if qarauditgot.schedstartdate = ?    then "" else string(date(qarauditgot.schedstartdate)) 
        tSchEndDate:screen-value       =   if qarauditgot.schedfinishdate = ?   then "" else string(date(qarauditgot.schedfinishdate)) 
        tStartDate:screen-value        =   if qarauditgot.auditstartDate = ?    then "" else string(date(qarauditgot.auditstartDate)) 
        tAuditEnd:screen-value         =   if qarauditgot.auditfinishDate = ?   then "" else string(date(qarauditgot.auditfinishDate))   
        tauditor:screen-value          =   if qarauditgot.auditor = "?"         then "" else qarauditgot.auditor              
        tRanking:screen-value          =   if qarauditgot.ranking = ?           then "" else string(qarauditgot.ranking)         
        tServices:screen-value         =   if qarauditgot.services = "?"        then "" else qarauditgot.services 
        tOnsiteStartDate:screen-value  =   if qarauditgot.onsitestartdate = ?    then "" else string(date(qarauditgot.onsitestartdate)) 
        tOnsiteFinishDate:screen-value =   if qarauditgot.onsitefinishdate = ?   then "" else string(date(qarauditgot.onsitefinishdate))
        tNotes:screen-value            =   if qarauditgot.comments = "?"        then "" else qarauditgot.comments        
        tType:screen-value             =   if qarauditgot.Audittype = "?"       then "" else if qarauditgot.Audittype = "Q" then "QAR"
                                                                                        else if qarauditgot.Audittype = "T" then "TOR"
                                                                                        else if qarauditgot.Audittype = "E" then "ERR" +
                                                                                          (if qarauditgot.Errtype = "?"     then "" 
                                                                                          else if qarauditgot.Errtype = "P" then " - Pre-sign" 
                                                                                          else if qarauditgot.Errtype = "I" then " - Interval" 
                                                                                          else if qarauditgot.Errtype = "C" then " - Corrective action" else "")    
                                                                                        else "Underwriting"  
       .

    for each qarnote :
         create qarnotes.
         buffer-copy qarnote to qarnotes.
        find first sysuser where qarnotes.uid = sysuser.uid no-error.
        if available sysuser then
          qarnotes.username = sysuser.name.
    end.

   run GetCalculatedFields.

    /* Agent files with calculated fields numminorfinding, numintermediatefinding, nummajorfinding*/
   run GetAgentfilegot.
  
   /* Escrow Accounts with calculated fields numminorfinding, numintermediatefinding, nummajorfinding*/
   run GetAccountswithfinding.
  
   /* questions with finding */
   run GetFindingofquestion.
  
   /* questions with best practices */
   run GetBestpracticeofquestions.
  
   /* questions with answers*/
   run GetAnsweredquestions .

   OPEN QUERY brwfindingofquestionaction for EACH findingOfQuestions .
   OPEN QUERY brwAgentFiles preselect EACH fileswithfinding.
   OPEN QUERY brwBestPracticeOfQuestions for EACH bestPracticeOfQuestions.
   OPEN query brwEscrowAccounts preselect EACH accountswithfinding .
   OPEN QUERY brwscores FOR EACH sectiongot .
   OPEN QUERY brwAnsweredQuestions for EACH answeredquestions.

   find current fileswithfinding no-error.
   if available fileswithfinding then
     seletedfile = string(fileswithfinding.fileid).

   find current accountswithfinding no-error.
   if available accountswithfinding then
     seletedaccount = string(accountswithfinding.acctnumber).

   if cbFilterQuestions:screen-value = "ALL" then
   do:
     brwAgentFiles:DESELECT-FOCUSED-ROW () no-error.
     brwEscrowAccounts:DESELECT-FOCUSED-ROW () no-error.
     brwAgentFiles:sensitive = false no-error.
     brwEscrowAccounts:sensitive = false no-error.
     OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions.
   end.
   else if cbFilterQuestions:screen-value = "Selected Agent File" then
   do:
     brwAgentFiles:sensitive = true no-error.
     brwEscrowAccounts:sensitive = false no-error.
     brwAgentFiles:select-focused-row () no-error.
     brwEscrowAccounts:DESELECT-FOCUSED-ROW () no-error.

     OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions where lookup(seletedfile, findingOfQuestions.files, "|") <> 0.
   end.
   else if cbFilterQuestions:screen-value = "Selected Escrow Account" then
   do:
     brwAgentFiles:sensitive = false no-error.
     brwEscrowAccounts:sensitive = true no-error.
     brwEscrowAccounts:SELECT-FOCUSED-ROW () no-error.
     brwAgentFiles:DESELECT-FOCUSED-ROW () no-error.

     OPEN QUERY brwfindingofquestionfinding for EACH findingOfQuestions where lookup(seletedaccount, findingOfQuestions.accounts, "|") <> 0.
   end.
   if cbFilterActions:screen-value = "ALL" then
   do:
     brwfindingofquestionaction:DESELECT-FOCUSED-ROW () no-error.
     OPEN QUERY brwActions FOR EACH qaractiongot .
   end.
   if cbFilterActions:screen-value = "Selected Question" then
   do:
     brwfindingofquestionaction:select-focused-row () no-error.
     OPEN QUERY brwActions FOR EACH qaractiongot where qaractiongot.questionseq = findingOfQuestions.questionseq .
   end.

   apply 'value-changed' to brwfindingofquestionfinding.
   apply 'value-changed' to brwBestPracticeOfQuestions .
   apply 'value-changed' to brwfindingofquestionaction .
   apply 'value-changed' to brwscores .
   apply 'value-changed' to brwAnsweredQuestions.

   end.
 end.
 hBrw = BROWSE brwQarHistory:handle.
 hColumn = hBrw:GET-BROWSE-COLUMN(3).
 OPEN QUERY brwQarHistory for EACH qarnotes.

 apply 'value-changed' to brwQarHistory .
    IF NOT THIS-PROCEDURE:PERSISTENT then
      WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteMaintainDocs C-Win 
PROCEDURE deleteMaintainDocs :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pAuditId as integer no-undo.

 for each openDocs where openDocs.qarid = pAuditId:
   if valid-handle(openDocs.hInstance) then
     delete object openDocs.hInstance.
 end.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE emptytemp-tables C-Win 
PROCEDURE emptytemp-tables :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
empty temp-table opendocs.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  VIEW FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  ENABLE BUTTON-1 BUTTON-2 BUTTON-3 BUTTON-4 BUTTON-5 BUTTON-6 BUTTON-7 
         BUTTON-8 
      WITH FRAME tabframe IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-tabframe}
  DISPLAY tAddr tMainOffice tParentAddr tDeliveredTo tNotes tSectioncomments 
          tStat tScorepct tQarID tType tAgentID tSchStartDate tSchEndDate tName 
          tOnsiteStartDate tOnsiteFinishDate tStartDate tAuditEnd tAuditScore 
          tAnswered tNumEmployees tParentName tMajor tUnanswered tNumOffices 
          tNumUnderwriters tIntermediate tCorrective tMinor tRecommended 
          tBestPractices tSuggested tContactName tContactPhone tContactFax 
          tAuditor tContactOther tRanking tContactEmail tServices 
          tContactPosition 
      WITH FRAME FR_1 IN WINDOW C-Win.
  ENABLE RECT-27 RECT-30 RECT-31 RECT-32 RECT-33 brwScores tServices 
      WITH FRAME FR_1 IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-FR_1}
  DISPLAY cbFilterQuestions eFindings flFileOrAccounts 
      WITH FRAME FR_2 IN WINDOW C-Win.
  ENABLE RECT-34 cbFilterQuestions brwfindingofquestionfinding eFindings 
      WITH FRAME FR_2 IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-FR_2}
  DISPLAY eBestPractice 
      WITH FRAME FR_3 IN WINDOW C-Win.
  ENABLE brwBestPracticeOfQuestions eBestPractice 
      WITH FRAME FR_3 IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-FR_3}
  DISPLAY cbFilterActions eFindingsFR_3 tActions 
      WITH FRAME FR_4 IN WINDOW C-Win.
  ENABLE cbFilterActions brwfindingofquestionaction eFindingsFR_3 brwActions 
         tActions 
      WITH FRAME FR_4 IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-FR_4}
  DISPLAY eHistoryComments 
      WITH FRAME FR_5 IN WINDOW C-Win.
  ENABLE brwQarHistory eHistoryComments 
      WITH FRAME FR_5 IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-FR_5}
  DISPLAY eQuestions eAnswer answeredBy answeredDate 
      WITH FRAME FR_6 IN WINDOW C-Win.
  ENABLE brwAnsweredQuestions 
      WITH FRAME FR_6 IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-FR_6}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAccountswithfinding C-Win 
PROCEDURE GetAccountswithfinding :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 for each escrowaccounts:  
   numminorfinding = 0.
   numintermediatefinding = 0.
   nummajorfinding = 0.
   for each findinggot where lookup (string(escrowaccounts.acctnumber), findinggot.accounts, "|") <> 0  :
     if findinggot.priority = 1 then
       numminorfinding = numminorfinding + 1 .
     else if findinggot.priority = 2 then
       numintermediatefinding = numintermediatefinding + 1 .
     else if findinggot.priority = 3 then
       nummajorfinding = nummajorfinding + 1 .
   end.
   create accountswithfinding.
   assign 
     accountswithfinding.acctnumber            = escrowaccounts.acctnumber
     accountswithfinding.minorfinding          = numminorfinding
     accountswithfinding.intermediatefinding   = numintermediatefinding
     accountswithfinding.majorfinding          = nummajorfinding.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAgentfilegot C-Win 
PROCEDURE GetAgentfilegot :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
for each agentfilegot:   
  numminorfinding = 0.
  numintermediatefinding = 0.
  nummajorfinding = 0.
  for each findinggot where lookup (string(agentFilegot.fileID), findinggot.files, "|") <> 0  :
    if findinggot.priority = 1 then
      numminorfinding = numminorfinding + 1 .
    else if findinggot.priority = 2 then
      numintermediatefinding = numintermediatefinding + 1 .
    else if findinggot.priority = 3 then
      nummajorfinding = nummajorfinding + 1 .
  end.
  create fileswithfinding.
  assign 
    fileswithfinding.fileid                = agentfilegot.fileId
    fileswithfinding.filename             = agentfilegot.fileNumber
    fileswithfinding.closingdate           = agentfilegot.closingdate
    fileswithfinding.minorfinding          = numminorfinding
    fileswithfinding.intermediatefinding   = numintermediatefinding
    fileswithfinding.majorfinding          = nummajorfinding.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAnsweredquestions C-Win 
PROCEDURE GetAnsweredquestions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
for each questiongot:
  create answeredquestions.
  assign 
    answeredquestions.QuestionSeq         = questiongot.seq   
    answeredquestions.QuestionID          = questiongot.questionid      
    answeredquestions.Questiondescription = questiongot.description
    answeredquestions.OriginalSeverity    = if questiongot.priority = ?   then "" else entry( questiongot.priority, "Minor,Intemediate,Major")  no-error.
  if  questiongot.priority = 0 then answeredquestions.OriginalSeverity = "Minor".
  find first answergot where answergot.seq = questiongot.seq no-error.
    if available answergot then
    do:
      assign
        answeredquestions.fileID              = int(answergot.fileID)
        answeredquestions.answer              = answergot.answer
        answeredquestions.dateAnswered        = answergot.dateAnswered
        answeredquestions.uID                 = answergot.uid.
        answeredquestions.changedpriority     = if answergot.changedpriority = ?   then "" else entry(answergot.changedpriority, "Minor,Intemediate,Major")  no-error.
      if  answergot.changedpriority = 0 then answeredquestions.changedpriority = "Minor".

      find first agentfilegot where agentfilegot.fileid = answeredquestions.fileID no-error.
      if available agentfilegot then
        answeredquestions.fileNumber          = agentfilegot.fileNumber.
      if answergot.dateAnswered ne ? and answeredquestions.uID eq "" then
        find first qarauditgot where qarauditgot.QarID = integer(string(ttaudit.QarID)) no-error.
          if available qarauditgot then
            answeredquestions.auditorname =  if qarauditgot.auditor = "?"         then "" else qarauditgot.auditor   .
    end.
  for each answergot where answergot.seq = questiongot.seq:
    find first answeredquestions where answeredquestions.QuestionSeq = answergot.seq and answeredquestions.fileID = answergot.fileID no-error.
    if not available answeredquestions then
    do:
      create answeredquestions.
      assign 
        answeredquestions.QuestionID          = questiongot.questionid      
        answeredquestions.Questiondescription = questiongot.description
        answeredquestions.answer              = answergot.answer
        answeredquestions.fileID              = int(answergot.fileID)
        answeredquestions.dateAnswered        = answergot.dateAnswered
        answeredquestions.uID                 = answergot.uid.

      answeredquestions.OriginalSeverity    = if questiongot.priority = ?   then "" else entry( questiongot.priority, "Minor,Intemediate,Major")  no-error.
      answeredquestions.changedpriority     = if answergot.changedpriority = ?   then "" else entry(answergot.changedpriority, "Minor,Intemediate,Major")  no-error.

      if questiongot.priority      = 0 then answeredquestions.OriginalSeverity = "Minor".
      if answergot.changedpriority = 0 then answeredquestions.changedpriority = "Minor".

      find first agentfilegot where agentfilegot.fileid = answeredquestions.fileID no-error.
      if available agentfilegot then
        answeredquestions.fileNumber = agentfilegot.fileNumber.

      find first sysuser where sysuser.uid = answeredquestions.uid no-error.
      if available sysuser then
        answeredquestions.auditorname = sysuser.name.
    end.
  end.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetBestpracticeofquestions C-Win 
PROCEDURE GetBestpracticeofquestions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
for each questiongot break by questiongot.seq:
  for each bestpracticegot where questiongot.seq = bestpracticegot.questionseq :
    if first-of(questiongot.seq)  then
      do:
        create bestPracticeOfQuestions .
        assign
          bestPracticeOfQuestions.questionseq             = questiongot.seq
          bestPracticeOfQuestions.description             = questiongot.description
          bestPracticeOfQuestions.questionid              = questiongot.questionid
          bestPracticeOfQuestions.bestPracticedescription = bestpracticegot.comments .
      end.             
  end.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCalculatedFields C-Win 
PROCEDURE GetCalculatedFields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
do with frame FR_1:
end.
pIntegerValue = 0.
for each answergot 
  where answergot.answer <> "":
    pIntegerValue = pIntegerValue + 1.
end.
 tAnswered:screen-value = string(pIntegerValue) .
 pIntegerValue = 0.
 for each answergot 
   where answergot.answer = "":
   pIntegerValue = pIntegerValue + 1.
 end.
 tUnanswered:screen-value = string(pIntegerValue) .
 pIntegerValue = 0.
 for each qaractiongot,
   each findinggot 
  where findinggot.questionSeq = qaractiongot.questionSeq
    and findinggot.priority = 1:
  pIntegerValue = pIntegerValue + 1.
 end.
 tSuggested:screen-value = string(pIntegerValue). 
 pIntegerValue = 0.
 for each qaractiongot,
   each findinggot 
  where findinggot.questionSeq = qaractiongot.questionSeq
    and findinggot.priority = 2:
  pIntegerValue = pIntegerValue + 1.
 end.
  tRecommended:screen-value = string(pIntegerValue).
  pIntegerValue = 0.
 for each qaractiongot,
   each findinggot 
  where findinggot.questionSeq = qaractiongot.questionSeq
    and findinggot.priority = 3:
  pIntegerValue = pIntegerValue + 1.
 end.
 tCorrective:screen-value = string(pIntegerValue).
  pIntegerValue = 0.
 for each findinggot 
  where findinggot.priority = 1:
   pIntegerValue = pIntegerValue + 1.
 end.
   tMinor:screen-value =  string(pIntegerValue).
   pIntegerValue = 0.
 for each findinggot
   where findinggot.priority = 2:
   pIntegerValue = pIntegerValue + 1.
 end.
 tIntermediate:screen-value =  string(pIntegerValue) .
 pIntegerValue = 0.
 for each findinggot
   where findinggot.priority = 3:
   pIntegerValue = pIntegerValue + 1.
 end.
  tMajor:screen-value = string(pIntegerValue).
  pIntegerValue = 0.
 for each bestPracticegot:
   pIntegerValue = pIntegerValue + 1.
 end.
 tBestPractices:screen-value = string(pIntegerValue) .


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetFindingofquestion C-Win 
PROCEDURE GetFindingofquestion :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var i as int.
for each questiongot break by questiongot.seq:
  for each findinggot where questiongot.seq = findinggot.questionseq :
    if first-of(questiongot.seq)  then
      do:
        create findingOfQuestions .
        assign
          findingOfQuestions.questionseq        = questiongot.seq
          findingOfQuestions.description        = questiongot.description
          findingOfQuestions.files              = findinggot.files
          findingOfQuestions.accounts           = findinggot.accounts
          findingOfQuestions.questionid         = questiongot.questionid
          findingOfQuestions.findingdescription = findinggot.comments .
          findingOfQuestions.files   = findinggot.files.
      end.             
  end.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openMaintainDocs C-Win 
PROCEDURE openMaintainDocs :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter pAuditId as integer no-undo.
define input parameter pWindow as handle no-undo.

find openDocs 
  where openDocs.QarID = pAuditId no-error.
if not available openDocs 
 then
  do:
    create openDocs.
    openDocs.QarID = pAuditId.
  end.
openDocs.hInstance = pWindow.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewWindow C-Win 
PROCEDURE ViewWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
if frame FR_1:visible then
  apply 'entry' to BUTTON-1 in frame tabframe.

if frame FR_2:visible then
  apply 'entry' to BUTTON-2 in frame tabframe.

if frame FR_3:visible then
  apply 'entry' to BUTTON-3 in frame tabframe.

if frame FR_4:visible then
  apply 'entry' to BUTTON-4 in frame tabframe.

if frame FR_5:visible then
  apply 'entry' to BUTTON-5 in frame tabframe.

if frame FR_6:visible then
  apply 'entry' to BUTTON-6 in frame tabframe.

  if {&window-name}:window-state eq window-minimized  then
    {&window-name}:window-state = window-normal .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

