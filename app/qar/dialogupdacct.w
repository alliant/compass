&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Update-File
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Update-File 
/* dialogupdacct.w
   UPDate a bank ACCounT
   
    Modification:
    Date          Name      Description
    02/10/2017    AG        Account number mandatory, default button to Non-default
    02/13/2017    AC        Implement local inspect functionality.
 */

def input parameter pTitle as char.
def input-output parameter pBankName as char.
def input-output parameter pAcctTitle as char.
def input-output parameter pAcctNumber as char.
def input-output parameter pReconDate as date.
def input-output parameter pReconCurrent as logical.

def input-output parameter pReconAdjBankBal as deci.
def input-output parameter pReconCheckbookBal as deci.
def input-output parameter pReconTrialBal as deci.

def input-output parameter pStmtBal as deci.
def input-output parameter pDipBal as deci.
def input-output parameter pOutChecksBal as deci.
def input-output parameter pNetAdj as deci.
def input-output parameter pNetNegAdj as deci.
def input-output parameter pCalcBal as deci.
def input-output parameter pActualCheckbookBal as deci.
def input-output parameter pTrialBal as deci.

def input-output parameter pReconciled as logical.
def input-output parameter pComments as char.

def output parameter pSuccess as logical init false.

define variable pAuditStatus as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Update-File

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tBankName tAcctTitle tAcctNumber tReconDate ~
tReconCurrent tReconAdjBankBal tReconCheckbookBal tReconTrialBal tStmtBal ~
tDipBal tOutChecksBal tNetAdj tNetNegAdj tActualCheckbookBal tTrialBal ~
tReconciled tComments Btn_OK Btn_Cancel RECT-30 RECT-33 RECT-34 
&Scoped-Define DISPLAYED-OBJECTS tBankName tAcctTitle tAcctNumber ~
tReconDate tReconCurrent tReconAdjBankBal tReconCheckbookBal tReconTrialBal ~
tStmtBal tDipBal tOutChecksBal tNetAdj tNetNegAdj tCalcBal ~
tActualCheckbookBal tTrialBal tReconciled tComments 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tComments AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 99 BY 3.57 NO-UNDO.

DEFINE VARIABLE tAcctNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "Account Number" 
     VIEW-AS FILL-IN 
     SIZE 27 BY 1 NO-UNDO.

DEFINE VARIABLE tAcctTitle AS CHARACTER FORMAT "X(256)":U 
     LABEL "Title of Account" 
     VIEW-AS FILL-IN 
     SIZE 27 BY 1 NO-UNDO.

DEFINE VARIABLE tActualCheckbookBal AS DECIMAL FORMAT "->>>,>>>,>>9.99":U INITIAL 0 
     LABEL "Actual Checkbook Balance" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1
     FONT 6 NO-UNDO.

DEFINE VARIABLE tBankName AS CHARACTER FORMAT "X(40)":U 
     LABEL "Name of Bank" 
     VIEW-AS FILL-IN 
     SIZE 27 BY 1 TOOLTIP "The name of the banking institution"
     FONT 6 NO-UNDO.

DEFINE VARIABLE tCalcBal AS DECIMAL FORMAT "->>>,>>>,>>9.99":U INITIAL 0 
     LABEL "Calculated Bank Balance" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1
     FONT 6 NO-UNDO.

DEFINE VARIABLE tDipBal AS DECIMAL FORMAT ">>>,>>>,>>9.99":U INITIAL 0 
     LABEL "Deposits in Transit List" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE tNetAdj AS DECIMAL FORMAT ">>>,>>>,>>9.99":U INITIAL 0 
     LABEL "Net Adjustment" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE tNetNegAdj AS DECIMAL FORMAT ">>>,>>>,>>9.99":U INITIAL 0 
     LABEL "(Net Adjustment)" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1
     FGCOLOR 12  NO-UNDO.

DEFINE VARIABLE tOutChecksBal AS DECIMAL FORMAT ">>>,>>>,>>9.99":U INITIAL 0 
     LABEL "(Outstanding Checks List)" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1
     FGCOLOR 12  NO-UNDO.

DEFINE VARIABLE tReconAdjBankBal AS DECIMAL FORMAT "->>>,>>>,>>9.99":U INITIAL 0 
     LABEL "Adjusted Bank Balance" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tReconCheckbookBal AS DECIMAL FORMAT "->>>,>>>,>>9.99":U INITIAL 0 
     LABEL "Adjusted Checkbook Balance" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tReconDate AS DATE FORMAT "99/99/99":U 
     LABEL "Reconciliation Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tReconTrialBal AS DECIMAL FORMAT "->>>,>>>,>>9.99":U INITIAL 0 
     LABEL "Trial Balance" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tStmtBal AS DECIMAL FORMAT "->>>,>>>,>>9.99":U INITIAL 0 
     LABEL "Bank Statement Balance" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE tTrialBal AS DECIMAL FORMAT "->>>,>>>,>>9.99":U INITIAL 0 
     LABEL "Trial Balance" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1
     FONT 6 NO-UNDO.

DEFINE RECTANGLE RECT-30
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 58 BY 5.24.

DEFINE RECTANGLE RECT-33
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 58 BY 8.57.

DEFINE RECTANGLE RECT-34
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 117 BY 4.52.

DEFINE VARIABLE tReconciled AS LOGICAL INITIAL no 
     LABEL "3-Way Balanced" 
     VIEW-AS TOGGLE-BOX
     SIZE 22 BY .81
     FONT 6 NO-UNDO.

DEFINE VARIABLE tReconCurrent AS LOGICAL INITIAL no 
     LABEL "Current" 
     VIEW-AS TOGGLE-BOX
     SIZE 11 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Update-File
     tBankName AT ROW 1.95 COL 26 COLON-ALIGNED WIDGET-ID 2
     tAcctTitle AT ROW 3.14 COL 26 COLON-ALIGNED WIDGET-ID 48
     tAcctNumber AT ROW 4.33 COL 26 COLON-ALIGNED WIDGET-ID 50
     tReconDate AT ROW 7.43 COL 23 COLON-ALIGNED WIDGET-ID 52
     tReconCurrent AT ROW 7.43 COL 42 WIDGET-ID 54
     tReconAdjBankBal AT ROW 10.29 COL 35 COLON-ALIGNED WIDGET-ID 72
     tReconCheckbookBal AT ROW 11.48 COL 35 COLON-ALIGNED WIDGET-ID 58
     tReconTrialBal AT ROW 12.67 COL 35 COLON-ALIGNED WIDGET-ID 76
     tStmtBal AT ROW 3.62 COL 94 COLON-ALIGNED WIDGET-ID 62
     tDipBal AT ROW 4.81 COL 94 COLON-ALIGNED WIDGET-ID 64
     tOutChecksBal AT ROW 6 COL 94 COLON-ALIGNED WIDGET-ID 66
     tNetAdj AT ROW 7.19 COL 94 COLON-ALIGNED WIDGET-ID 68
     tNetNegAdj AT ROW 8.38 COL 94 COLON-ALIGNED WIDGET-ID 70
     tCalcBal AT ROW 10.29 COL 94 COLON-ALIGNED WIDGET-ID 94 NO-TAB-STOP 
     tActualCheckbookBal AT ROW 11.48 COL 94 COLON-ALIGNED WIDGET-ID 74
     tTrialBal AT ROW 12.67 COL 94 COLON-ALIGNED WIDGET-ID 96
     tReconciled AT ROW 13.86 COL 96 WIDGET-ID 102
     tComments AT ROW 15.76 COL 18 NO-LABEL WIDGET-ID 104
     Btn_OK AT ROW 20.29 COL 43
     Btn_Cancel AT ROW 20.29 COL 65
     "Comments/Observations" VIEW-AS TEXT
          SIZE 28 BY .62 AT ROW 15.05 COL 5 WIDGET-ID 108
          FONT 6
     "Reconciliation Review" VIEW-AS TEXT
          SIZE 27 BY .62 AT ROW 9.57 COL 64 WIDGET-ID 86
          FONT 6
     "Agent's Reconciliation" VIEW-AS TEXT
          SIZE 27 BY .62 AT ROW 6.24 COL 5 WIDGET-ID 100
          FONT 6
     RECT-30 AT ROW 9.81 COL 62 WIDGET-ID 80
     RECT-33 AT ROW 6.48 COL 3 WIDGET-ID 98
     RECT-34 AT ROW 15.29 COL 3 WIDGET-ID 106
     SPACE(1.99) SKIP(2.09)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "<Parameterized>"
         CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Update-File
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Update-File:SCROLLABLE       = FALSE
       FRAME Update-File:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN tCalcBal IN FRAME Update-File
   NO-ENABLE                                                            */
ASSIGN 
       tCalcBal:READ-ONLY IN FRAME Update-File        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Update-File
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Update-File Update-File
ON WINDOW-CLOSE OF FRAME Update-File /* <Parameterized> */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Update-File
ON CHOOSE OF Btn_OK IN FRAME Update-File /* OK */
DO:
  if tAcctNumber:screen-value = ""  then
  do: 
    message "Account Number cannot be blank."
           view-as alert-box info buttons ok.
    apply "entry" to tAcctNumber.
    return no-apply.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDipBal
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDipBal Update-File
ON LEAVE OF tDipBal IN FRAME Update-File /* Deposits in Transit List */
DO:
  run calculateBalance.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNetAdj
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNetAdj Update-File
ON LEAVE OF tNetAdj IN FRAME Update-File /* Net Adjustment */
DO:
  run calculateBalance.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNetNegAdj
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNetNegAdj Update-File
ON LEAVE OF tNetNegAdj IN FRAME Update-File /* (Net Adjustment) */
DO:
  run calculateBalance.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tOutChecksBal
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tOutChecksBal Update-File
ON LEAVE OF tOutChecksBal IN FRAME Update-File /* (Outstanding Checks List) */
DO:
  run calculateBalance.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tReconTrialBal
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tReconTrialBal Update-File
ON LEAVE OF tReconTrialBal IN FRAME Update-File /* Trial Balance */
DO:
  if decimal(tTrialBal:screen-value) <> 0
   then tTrialBal:screen-value = self:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStmtBal
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStmtBal Update-File
ON LEAVE OF tStmtBal IN FRAME Update-File /* Bank Statement Balance */
DO:
  run calculateBalance.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Update-File 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

frame {&frame-name}:title = pTitle + " Account Reconciliation".
assign
  tBankName = pBankName
  tAcctTitle = pAcctTitle
  tAcctNumber = pAcctNumber
  tReconDate = pReconDate
  tReconCurrent = pReconCurrent
  tReconAdjBankBal = pReconAdjBankBal
  tReconCheckbookBal = pReconCheckbookBal
  tReconTrialBal = pReconTrialBal
  tStmtBal = pStmtBal
  tDipBal = pDipBal
  tOutChecksBal = pOutChecksBal
  tNetAdj = pNetAdj
  tNetNegAdj = pNetNegAdj
  tCalcBal = pCalcBal
  tActualCheckbookBal = pActualCheckbookBal
  tTrialBal = pTrialBal
  tReconciled = pReconciled
  tComments = pComments
  .

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  run calculateBalance.
  publish "GetAuditStatus" (output pAuditStatus).
  
  if pAuditStatus = "C"then
    run AuditReadOnly.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  /* OK */
  assign
    pBankName = input tBankName
    pAcctTitle = input tAcctTitle
    pAcctNumber = input tAcctNumber
    pReconDate = input tReconDate
    pReconCurrent = input tReconCurrent
    pReconAdjBankBal = input tReconAdjBankBal
    pReconCheckbookBal = input tReconCheckbookBal
    pReconTrialBal = input tReconTrialBal
    pStmtBal = input tStmtBal
    pDipBal = input tDipBal
    pOutChecksBal = input tOutChecksBal
    pNetAdj = input tNetAdj
    pNetNegAdj = input tNetNegAdj
    pCalcBal = input tCalcBal
    pActualCheckbookBal = input tActualCheckbookBal
    pTrialBal = input tTrialBal
    pReconciled = input tReconciled
    pComments = input tComments
    pSuccess = true
    .
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditReadOnly Update-File 
PROCEDURE AuditReadOnly :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
do with frame Update-File: 
end.
assign
  tBankName:sensitive           = false
  tAcctTitle:sensitive          = false
  tAcctNumber:sensitive         = false
  tReconDate:sensitive          = false
  tReconCurrent:sensitive       = false
  tReconAdjBankBal:sensitive    = false
  tReconCheckbookBal:sensitive  = false
  tReconTrialBal:sensitive      = false
  tStmtBal:sensitive            = false
  tDipBal:sensitive             = false
  tOutChecksBal:sensitive       = false
  tNetAdj:sensitive             = false
  tNetNegAdj:sensitive          = false
  tCalcBal:sensitive            = false
  tActualCheckbookBal:sensitive = false
  tTrialBal:sensitive           = false
  tReconciled:sensitive         = false
  tComments:sensitive           = false
  Btn_OK:sensitive              = false
  .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calculateBalance Update-File 
PROCEDURE calculateBalance :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var qStmt as decimal.
 def var qDIP as decimal.
 def var qOC as decimal.
 def var qAdj as decimal.
 def var qNegAdj as decimal.
 def var calcBal as decimal.

 qStmt = decimal(tStmtBal:screen-value in frame Update-File).
 qDIP = decimal(tDipBal:screen-value).
 qOC = decimal(tOutChecksBal:screen-value).
 qAdj = decimal(tNetAdj:screen-value).
 qNegAdj = decimal(tNetNegAdj:screen-value).

 calcBal = qStmt + qDIP - qOC + qAdj - qNegAdj.
 tCalcBal:screen-value = string(calcBal, "->>>,>>>,>>9.99").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Update-File  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Update-File.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Update-File  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tBankName tAcctTitle tAcctNumber tReconDate tReconCurrent 
          tReconAdjBankBal tReconCheckbookBal tReconTrialBal tStmtBal tDipBal 
          tOutChecksBal tNetAdj tNetNegAdj tCalcBal tActualCheckbookBal 
          tTrialBal tReconciled tComments 
      WITH FRAME Update-File.
  ENABLE tBankName tAcctTitle tAcctNumber tReconDate tReconCurrent 
         tReconAdjBankBal tReconCheckbookBal tReconTrialBal tStmtBal tDipBal 
         tOutChecksBal tNetAdj tNetNegAdj tActualCheckbookBal tTrialBal 
         tReconciled tComments Btn_OK Btn_Cancel RECT-30 RECT-33 RECT-34 
      WITH FRAME Update-File.
  VIEW FRAME Update-File.
  {&OPEN-BROWSERS-IN-QUERY-Update-File}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

