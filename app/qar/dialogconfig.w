&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fDialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fDialog 
/* clm/dialogconfig.w
Dialog to set CONFIGuration options
D.Sinclair 2.15.2015
 */

{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fDialog

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tTempDir tReportsSearch tConfirmClose ~
tConfirmFileUpload tConfirmExit tAutoViewRecent tAutoViewSearch tNoteView ~
tDefaultCategory tDormant tStates tOtherStates chkLoadStates chkLoadAgents ~
chkLoadTasks chkLoadPeriods chkLoadCodes chkLoadAll bAccounts bStates ~
bAgent bTasks bPeriods bCodes bVendors bAll Btn_OK Btn_Cancel tExportType ~
RECT-33 
&Scoped-Define DISPLAYED-OBJECTS tTempDir tConfirmClose tConfirmFileUpload ~
tConfirmExit tAutoViewRecent tAutoViewSearch tNoteView tDefaultCategory ~
tDormant chkLoadAccounts chkLoadStates chkLoadAgents chkLoadTasks ~
chkLoadPeriods chkLoadCodes chkLoadVendors chkLoadAll tExportType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAccounts 
     LABEL "Accounts" 
     SIZE 12 BY 1.14 TOOLTIP "Click to reload Accounts now".

DEFINE BUTTON bAddState 
     LABEL "<--" 
     SIZE 8 BY .95 TOOLTIP "Add a State to the Search options".

DEFINE BUTTON bAgent 
     LABEL "Agents" 
     SIZE 12 BY 1.14 TOOLTIP "Click to reload Agents now".

DEFINE BUTTON bAll 
     LABEL "ALL" 
     SIZE 12 BY 1.14 TOOLTIP "Click to reload all references now".

DEFINE BUTTON bCodes 
     LABEL "Codes" 
     SIZE 12 BY 1.14 TOOLTIP "Click to reload Codes now".

DEFINE BUTTON bDeleteState 
     LABEL "-->" 
     SIZE 8 BY .95 TOOLTIP "Remove a State from the Search options".

DEFINE BUTTON bPeriods 
     LABEL "Periods" 
     SIZE 12 BY 1.14 TOOLTIP "Click to reload Periods now".

DEFINE BUTTON bStates 
     LABEL "States" 
     SIZE 12 BY 1.14 TOOLTIP "Click to reload States now".

DEFINE BUTTON bTasks 
     LABEL "Tasks" 
     SIZE 12 BY 1.14 TOOLTIP "Click to reload Default Tasks now".

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 17 BY 1.19
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 17 BY 1.19
     BGCOLOR 8 .

DEFINE BUTTON bVendors 
     LABEL "Vendors" 
     SIZE 12 BY 1.14 TOOLTIP "Click to reload Vendors now".

DEFINE BUTTON tReportsSearch 
     LABEL "..." 
     SIZE 4 BY .95.

DEFINE VARIABLE tDefaultCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 24 BY 1 TOOLTIP "Select the default Category when adding a new note" NO-UNDO.

DEFINE VARIABLE tDormant AS INTEGER FORMAT "zz9":U INITIAL 0 
     LABEL "Dormant" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 TOOLTIP "Enter the default for the dormant file report" NO-UNDO.

DEFINE VARIABLE tTempDir AS CHARACTER FORMAT "X(200)":U 
     LABEL "Temporary Directory" 
     VIEW-AS FILL-IN 
     SIZE 56.6 BY 1 TOOLTIP "Enter the default directory to save files; blank is session working directory" NO-UNDO.

DEFINE VARIABLE tExportType AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "XLSX", "X",
"CSV", "C"
     SIZE 12 BY 1.43 TOOLTIP "Select the type of Excel export to use" NO-UNDO.

DEFINE VARIABLE tNoteView AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Page", "P",
"Browse", "B"
     SIZE 12 BY 1.43 TOOLTIP "Select the style of notes window to use" NO-UNDO.

DEFINE RECTANGLE RECT-31
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 84 BY 14.05.

DEFINE RECTANGLE RECT-33
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 84 BY 7.14.

DEFINE VARIABLE tOtherStates AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SORT SCROLLBAR-VERTICAL 
     LIST-ITEM-PAIRS "Alabama","AL",
                     "Alaska","AK",
                     "Arizona","AZ",
                     "Arkansas","AR",
                     "California","CA",
                     "Connecticut","CT",
                     "Delaware","DE",
                     "Florida","FL",
                     "Georgia","GA",
                     "Hawaii","HI",
                     "Idaho","ID",
                     "Illinois","IL",
                     "Indiana","IN",
                     "Iowa","IA",
                     "Kansas","KS",
                     "Kentucky","KY",
                     "Louisiana","LA",
                     "Maine","ME",
                     "Maryland","MD",
                     "Massachusetts","MA",
                     "Michigan","MI",
                     "Minnesota","MN",
                     "Mississippi","MS",
                     "Missouri","MO",
                     "Montana","MT",
                     "Nebraska","NE",
                     "Nevada","NV",
                     "New Hampshire","NH",
                     "New Jersey","NJ",
                     "New Mexico","NM",
                     "New York","NY",
                     "North Carolina","NC",
                     "North Dakota","ND",
                     "Ohio","OH",
                     "Oklahoma","OK",
                     "Oregon","OR",
                     "Pennsylvania","PA",
                     "Rhode Island","RI",
                     "South Carolina","SC",
                     "South Dakota","SD",
                     "Tennessee","TN",
                     "Texas","TX",
                     "Utah","UT",
                     "Vermont","VT",
                     "Virginia","VA",
                     "Washington","WA",
                     "West Virginia","WV",
                     "Wisconsin","WI",
                     "Wyoming","WY" 
     SIZE 24 BY 5.95 NO-UNDO.

DEFINE VARIABLE tStates AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE NO-DRAG SORT SCROLLBAR-VERTICAL 
     LIST-ITEM-PAIRS "Colorado","CO" 
     SIZE 25 BY 5.95 NO-UNDO.

DEFINE VARIABLE chkLoadAccounts AS LOGICAL INITIAL yes 
     LABEL "Accounts" 
     VIEW-AS TOGGLE-BOX
     SIZE 13 BY .81 NO-UNDO.

DEFINE VARIABLE chkLoadAgents AS LOGICAL INITIAL no 
     LABEL "Agents" 
     VIEW-AS TOGGLE-BOX
     SIZE 11 BY .81 NO-UNDO.

DEFINE VARIABLE chkLoadAll AS LOGICAL INITIAL no 
     LABEL "ALL" 
     VIEW-AS TOGGLE-BOX
     SIZE 9 BY .81 NO-UNDO.

DEFINE VARIABLE chkLoadCodes AS LOGICAL INITIAL no 
     LABEL "Codes" 
     VIEW-AS TOGGLE-BOX
     SIZE 11 BY .81 NO-UNDO.

DEFINE VARIABLE chkLoadPeriods AS LOGICAL INITIAL no 
     LABEL "Periods" 
     VIEW-AS TOGGLE-BOX
     SIZE 13 BY .81 NO-UNDO.

DEFINE VARIABLE chkLoadStates AS LOGICAL INITIAL no 
     LABEL "States" 
     VIEW-AS TOGGLE-BOX
     SIZE 10 BY .81 NO-UNDO.

DEFINE VARIABLE chkLoadTasks AS LOGICAL INITIAL no 
     LABEL "Tasks" 
     VIEW-AS TOGGLE-BOX
     SIZE 12 BY .81 NO-UNDO.

DEFINE VARIABLE chkLoadVendors AS LOGICAL INITIAL yes 
     LABEL "Vendors" 
     VIEW-AS TOGGLE-BOX
     SIZE 12 BY .81 NO-UNDO.

DEFINE VARIABLE tAutoViewRecent AS LOGICAL INITIAL no 
     LABEL "Auto-view last file viewed on startup" 
     VIEW-AS TOGGLE-BOX
     SIZE 39 BY .81 TOOLTIP "Automatically open the last claim that was viewed" NO-UNDO.

DEFINE VARIABLE tAutoViewSearch AS LOGICAL INITIAL no 
     LABEL "Auto-view the file when only one result" 
     VIEW-AS TOGGLE-BOX
     SIZE 41 BY .81 TOOLTIP "Automatically view the file if there is only a single result when searching" NO-UNDO.

DEFINE VARIABLE tConfirmClose AS LOGICAL INITIAL no 
     LABEL "Confirm Close" 
     VIEW-AS TOGGLE-BOX
     SIZE 20.4 BY .81 TOOLTIP "Uncheck to prevent confirmation message" NO-UNDO.

DEFINE VARIABLE tConfirmExit AS LOGICAL INITIAL no 
     LABEL "Confirm Exit" 
     VIEW-AS TOGGLE-BOX
     SIZE 29.2 BY .81 NO-UNDO.

DEFINE VARIABLE tConfirmFileUpload AS LOGICAL INITIAL no 
     LABEL "Confirm File Upload" 
     VIEW-AS TOGGLE-BOX
     SIZE 25 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fDialog
     tTempDir AT ROW 2.19 COL 21.4 COLON-ALIGNED WIDGET-ID 2
     tReportsSearch AT ROW 2.19 COL 80 WIDGET-ID 30
     tConfirmClose AT ROW 3.86 COL 4 WIDGET-ID 110
     tConfirmFileUpload AT ROW 4.62 COL 4 WIDGET-ID 112
     tConfirmExit AT ROW 5.38 COL 4 WIDGET-ID 132
     tAutoViewRecent AT ROW 6.91 COL 4 WIDGET-ID 134
     tAutoViewSearch AT ROW 7.67 COL 4 WIDGET-ID 164
     tNoteView AT ROW 4.57 COL 50 NO-LABEL WIDGET-ID 166
     tDefaultCategory AT ROW 6.76 COL 58 COLON-ALIGNED WIDGET-ID 178
     tDormant AT ROW 7.81 COL 58 COLON-ALIGNED WIDGET-ID 174
     tStates AT ROW 9.29 COL 14.6 NO-LABEL WIDGET-ID 108
     bAddState AT ROW 11.19 COL 40.6 WIDGET-ID 100
     bDeleteState AT ROW 12.14 COL 40.6 WIDGET-ID 102
     tOtherStates AT ROW 9.29 COL 49.6 NO-LABEL WIDGET-ID 82
     chkLoadAccounts AT ROW 17.62 COL 9 WIDGET-ID 148
     chkLoadStates AT ROW 18.38 COL 9 WIDGET-ID 152
     chkLoadAgents AT ROW 17.62 COL 23 WIDGET-ID 144
     chkLoadTasks AT ROW 18.38 COL 23 WIDGET-ID 160
     chkLoadPeriods AT ROW 17.62 COL 37 WIDGET-ID 156
     chkLoadCodes AT ROW 18.38 COL 37 WIDGET-ID 158
     chkLoadVendors AT ROW 17.67 COL 52 WIDGET-ID 146
     chkLoadAll AT ROW 18.43 COL 52 WIDGET-ID 162
     bAccounts AT ROW 20.29 COL 9 WIDGET-ID 92
     bStates AT ROW 21.48 COL 9 WIDGET-ID 128
     bAgent AT ROW 20.29 COL 21.2 WIDGET-ID 72
     bTasks AT ROW 21.48 COL 21.2 WIDGET-ID 74
     bPeriods AT ROW 20.29 COL 33.4 WIDGET-ID 130
     bCodes AT ROW 21.48 COL 33.4 WIDGET-ID 138
     bVendors AT ROW 20.29 COL 45.6 WIDGET-ID 136
     bAll AT ROW 21.48 COL 45.6 WIDGET-ID 78
     Btn_OK AT ROW 23.62 COL 27
     Btn_Cancel AT ROW 23.62 COL 45
     tExportType AT ROW 4.57 COL 68.2 NO-LABEL WIDGET-ID 182
     "Options" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 1.24 COL 4 WIDGET-ID 70
          FONT 6
     "Refresh" VIEW-AS TEXT
          SIZE 18 BY .62 TOOLTIP "Click to update the associated information" AT ROW 19.57 COL 9.2 WIDGET-ID 80
     "States:" VIEW-AS TEXT
          SIZE 7.6 BY .62 AT ROW 9.33 COL 7 WIDGET-ID 106
     "Load on Startup" VIEW-AS TEXT
          SIZE 16 BY .62 TOOLTIP "Check to load the references during startup" AT ROW 16.91 COL 9 WIDGET-ID 142
     "Notes View" VIEW-AS TEXT
          SIZE 14.8 BY .62 AT ROW 3.86 COL 50 WIDGET-ID 170
     "References" VIEW-AS TEXT
          SIZE 13 BY .62 AT ROW 15.76 COL 4 WIDGET-ID 172
          FONT 6
     "days" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 8 COL 66.2 WIDGET-ID 176
     "Export To" VIEW-AS TEXT
          SIZE 12.6 BY .62 TOOLTIP "Export to Excel as Spreadsheet or CSV file" AT ROW 3.86 COL 68.4 WIDGET-ID 180
     RECT-31 AT ROW 1.48 COL 2 WIDGET-ID 68
     RECT-33 AT ROW 16 COL 2 WIDGET-ID 140
     SPACE(0.79) SKIP(1.95)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Configuration"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fDialog
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fDialog:SCROLLABLE       = FALSE
       FRAME fDialog:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bAddState IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDeleteState IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX chkLoadAccounts IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX chkLoadVendors IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-31 IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR SELECTION-LIST tOtherStates IN FRAME fDialog
   NO-DISPLAY                                                           */
/* SETTINGS FOR SELECTION-LIST tStates IN FRAME fDialog
   NO-DISPLAY                                                           */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fDialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDialog fDialog
ON WINDOW-CLOSE OF FRAME fDialog /* Configuration */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAccounts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAccounts fDialog
ON CHOOSE OF bAccounts IN FRAME fDialog /* Accounts */
DO:
  publish "LoadAccounts".
  MESSAGE "Accoutns updated"
   VIEW-AS ALERT-BOX INFO BUTTONS OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddState fDialog
ON CHOOSE OF bAddState IN FRAME fDialog /* <-- */
DO:
  if tOtherStates:input-value = ?
   then return.
  tStates:add-last(entry(lookup(tOtherStates:input-value, tOtherStates:list-item-pairs) - 1, tOtherStates:list-item-pairs), 
                   tOtherStates:input-value).
  tOtherStates:delete(tOtherStates:input-value).
  bAddState:sensitive = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgent fDialog
ON CHOOSE OF bAgent IN FRAME fDialog /* Agents */
DO:
  publish "LoadAgents".
  
  MESSAGE "Agents updated"
   VIEW-AS ALERT-BOX INFO BUTTONS OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAll fDialog
ON CHOOSE OF bAll IN FRAME fDialog /* ALL */
DO:
  publish "LoadAccounts".
  publish "LoadAgents".
  publish "LoadStates".
  publish "LoadVendors".
  publish "LoadPeriods".
  publish "LoadDefaultTexts".
  publish "LoadDefaultTasks".
  publish "LoadSysCodes".

  /* Not visibile on screen, but should refresh as well */
  publish "LoadSysUsers".
  publish "LoadSysProps".
  publish "LoadEntities".
  publish "LoadLists".

  MESSAGE "All references updated."
   VIEW-AS ALERT-BOX INFO BUTTONS OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCodes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCodes fDialog
ON CHOOSE OF bCodes IN FRAME fDialog /* Codes */
DO:
  publish "LoadSysCodes".
  MESSAGE "Codes updated"
   VIEW-AS ALERT-BOX INFO BUTTONS OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteState fDialog
ON CHOOSE OF bDeleteState IN FRAME fDialog /* --> */
DO:
 if tStates:input-value = ?
  then return.
 tOtherStates:add-last(entry(lookup(tStates:input-value, tStates:list-item-pairs) - 1, tStates:list-item-pairs), 
                  tStates:input-value).
 tStates:delete(tStates:input-value).
 bDeleteState:sensitive = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPeriods
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPeriods fDialog
ON CHOOSE OF bPeriods IN FRAME fDialog /* Periods */
DO:
  publish "LoadDefaultTexts".
  MESSAGE "Default Texts updated"
   VIEW-AS ALERT-BOX INFO BUTTONS OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bStates
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bStates fDialog
ON CHOOSE OF bStates IN FRAME fDialog /* States */
DO:
  publish "LoadStates".
  MESSAGE "States updated"
   VIEW-AS ALERT-BOX INFO BUTTONS OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bTasks
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bTasks fDialog
ON CHOOSE OF bTasks IN FRAME fDialog /* Tasks */
DO:
  publish "LoadDefaultTasks".
  MESSAGE "Default Tasks updated"
   VIEW-AS ALERT-BOX INFO BUTTONS OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bVendors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bVendors fDialog
ON CHOOSE OF bVendors IN FRAME fDialog /* Vendors */
DO:
  publish "LoadVendors".
  MESSAGE "Vendors updated"
   VIEW-AS ALERT-BOX INFO BUTTONS OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME chkLoadAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL chkLoadAll fDialog
ON VALUE-CHANGED OF chkLoadAll IN FRAME fDialog /* ALL */
DO:
  do with frame {&frame-name}:
    assign
      std-lo = chkLoadAll:checked
      chkLoadAccounts:checked = std-lo
      chkLoadAgents:checked = std-lo
      chkLoadCodes:checked = std-lo
      chkLoadStates:checked = std-lo
      chkLoadPeriods:checked = std-lo
      chkLoadTasks:checked = std-lo
      chkLoadVendors:checked = std-lo
      .
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tOtherStates
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tOtherStates fDialog
ON DEFAULT-ACTION OF tOtherStates IN FRAME fDialog
DO:
  apply "CHOOSE" to bAddState.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tOtherStates fDialog
ON VALUE-CHANGED OF tOtherStates IN FRAME fDialog
DO:
  bAddState:sensitive = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tReportsSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tReportsSearch fDialog
ON CHOOSE OF tReportsSearch IN FRAME fDialog /* ... */
DO:
  std-ch = tTempDir:screen-value.
  if tTempDir:screen-value > ""
    then system-dialog get-dir std-ch
           initial-dir std-ch.
    else system-dialog get-dir std-ch.
  if std-ch > "" and std-ch <> ?
   then tTempDir:screen-value = std-ch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStates
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStates fDialog
ON DEFAULT-ACTION OF tStates IN FRAME fDialog
DO:
 apply "CHOOSE" to bDeleteState.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStates fDialog
ON VALUE-CHANGED OF tStates IN FRAME fDialog
DO:
  bDeleteState:sensitive = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fDialog 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* config */
publish "GetTempDir" (output tTempDir).
publish "GetConfirmClose" (output tConfirmClose).
publish "GetConfirmFileUpload" (output tConfirmFileUpload).
publish "GetConfirmExit" (output tConfirmExit).
publish "GetAutoViewRecent" (output tAutoViewRecent).
publish "GetAutoViewSearch" (output tAutoViewSearch).
publish "GetNoteWindow" (output tNoteView).
publish "GetDormantDays" (output tDormant).
publish "GetExportType" (output tExportType).

/* reference load at startup */
publish "GetLoadAccounts" (output chkLoadAccounts).
publish "GetLoadAgents" (output chkLoadAgents).
publish "GetLoadCodes" (output chkLoadCodes).
publish "GetLoadPeriods" (output chkLoadPeriods).
publish "GetLoadStates" (output chkLoadStates).
publish "GetLoadTasks" (output chkLoadTasks).
publish "GetLoadVendors" (output chkLoadVendors).

publish "GetSearchStates" (output std-ch).
do std-in = 2 to num-entries(std-ch) by 2:
 tOtherStates:delete(entry(std-in, std-ch)).
end.
if num-entries(std-ch) >= 2
 then tStates:list-item-pairs = std-ch.

if lookup("CO", tStates:list-item-pairs) = 0
 then 
  do: tOtherStates:add-last("Colorado", "CO").
      tStates:delete("CO").             /* default for AppBuilder to work */
  end.

publish "GetNoteCategoryUserList" (output std-ch).
tDefaultCategory:list-item-pairs = std-ch.

publish "GetDefaultNoteCategory" (output std-ch).
if std-ch = "" 
 then std-ch = entry(2, tDefaultCategory:list-item-pairs).
tDefaultCategory:screen-value = std-ch.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  if chkLoadAccounts:checked and
     chkLoadVendors:checked and
     chkLoadAgents:checked and
     chkLoadStates:checked and
     chkLoadCodes:checked and
     chkLoadPeriods:checked and
     chkLoadTasks:checked 
   then chkLoadAll:checked = true.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  do with frame {&frame-name}:
    assign
      tTempDir = tTempDir:screen-value
      .
  
    if tTempDir:screen-value > ""
     then
      do: file-info:file-name = tTempDir:screen-value.
          if file-info:full-pathname = ?
           or index(file-info:file-type, "D") = 0 
           or index(file-info:file-type, "W") = 0
           then
            do: 
                MESSAGE "Invalid Directory"
                 VIEW-AS ALERT-BOX error BUTTONS OK.
                undo MAIN-BLOCK, retry MAIN-BLOCK.
            end.
           else tTempDir:screen-value = file-info:full-pathname.
      end.
  
    publish "SetTempDir" (tTempDir:screen-value).
    publish "SetConfirmClose" (tConfirmClose:checked).
    publish "SetConfirmFileUpload" (tConfirmFileUpload:checked).
    publish "SetConfirmExit" (tConfirmExit:checked).
    publish "SetAutoViewRecent" (tAutoViewRecent:checked).
    publish "SetAutoViewSearch" (tAutoViewSearch:checked).
    publish "SetSearchStates" (tStates:list-item-pairs).
    publish "SetNoteWindow" (tNoteView:screen-value).
    publish "SetDormantDays" (tDormant:input-value).
    publish "SetExportType" (tExportType:screen-value).

    publish "SetLoadAccounts" (chkLoadAccounts:checked).
    publish "SetLoadAgents" (chkLoadAgents:checked).
    publish "SetLoadCodes" (chkLoadCodes:checked).
    publish "SetLoadPeriods" (chkLoadPeriods:checked).
    publish "SetLoadStates" (chkLoadStates:checked).
    publish "SetLoadTasks" (chkLoadTasks:checked).
    publish "SetLoadVendors" (chkLoadVendors:checked).

    publish "SetDefaultNoteCategory" (tDefaultCategory:screen-value).
  end.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fDialog  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fDialog.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fDialog  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tTempDir tConfirmClose tConfirmFileUpload tConfirmExit tAutoViewRecent 
          tAutoViewSearch tNoteView tDefaultCategory tDormant chkLoadAccounts 
          chkLoadStates chkLoadAgents chkLoadTasks chkLoadPeriods chkLoadCodes 
          chkLoadVendors chkLoadAll tExportType 
      WITH FRAME fDialog.
  ENABLE tTempDir tReportsSearch tConfirmClose tConfirmFileUpload tConfirmExit 
         tAutoViewRecent tAutoViewSearch tNoteView tDefaultCategory tDormant 
         tStates tOtherStates chkLoadStates chkLoadAgents chkLoadTasks 
         chkLoadPeriods chkLoadCodes chkLoadAll bAccounts bStates bAgent bTasks 
         bPeriods bCodes bVendors bAll Btn_OK Btn_Cancel tExportType RECT-33 
      WITH FRAME fDialog.
  VIEW FRAME fDialog.
  {&OPEN-BROWSERS-IN-QUERY-fDialog}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

