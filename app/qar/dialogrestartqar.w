&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fOpen 
/* dialogopenqar.w
   D.Sinclair 10.25.2015
    Modification    :
    Date          Name      Description
    01/04/2017    AG        Modified to refresh the browse after "Reset".
    09/11/2017    AG        Alternate color code in browse.
----------------------------------------------------------------------*/


def output parameter pSelected as logical init false no-undo.
def output parameter pIdentifier as char no-undo.
def output parameter pFullFile as char.
def var iBgcolor as int no-undo.
{lib/std-def.i}
{tt/doc.i &tableAlias="localQarFileInfo"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fOpen
&Scoped-define BROWSE-NAME brwFiles

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES localQarFileInfo

/* Definitions for BROWSE brwFiles                                      */
&Scoped-define FIELDS-IN-QUERY-brwFiles localQarFileInfo.identifier localQarFileInfo.name localQarFileInfo.createdDate localQarFileInfo.modifiedDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFiles   
&Scoped-define SELF-NAME brwFiles
&Scoped-define QUERY-STRING-brwFiles FOR EACH localQarFileInfo by localQarFileInfo.name
&Scoped-define OPEN-QUERY-brwFiles OPEN QUERY {&SELF-NAME} FOR EACH localQarFileInfo by localQarFileInfo.name.
&Scoped-define TABLES-IN-QUERY-brwFiles localQarFileInfo
&Scoped-define FIRST-TABLE-IN-QUERY-brwFiles localQarFileInfo


/* Definitions for DIALOG-BOX fOpen                                     */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fOpen ~
    ~{&OPEN-QUERY-brwFiles}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwFiles bCancel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bRestart AUTO-GO 
     LABEL "Restart" 
     SIZE 15 BY 1.14.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwFiles FOR 
      localQarFileInfo SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwFiles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFiles fOpen _FREEFORM
  QUERY brwFiles DISPLAY
      localQarFileInfo.identifier label "QAR ID" format "x(20)" width 10
 localQarFileInfo.name label "Agent Name" format "x(50)" width 40

localQarFileInfo.createdDate label "Created" format "x(20)" width 20
localQarFileInfo.modifiedDate label "Modified" format "x(20)" width 20
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 104 BY 9 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fOpen
     brwFiles AT ROW 2.52 COL 2 WIDGET-ID 200
     bRestart AT ROW 12.05 COL 38 WIDGET-ID 4
     bCancel AT ROW 12.05 COL 55.2 WIDGET-ID 8
     "Select a Review" VIEW-AS TEXT
          SIZE 17.6 BY .62 AT ROW 1.67 COL 2.2 WIDGET-ID 6
     SPACE(86.99) SKIP(11.61)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Restart Active Review" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fOpen
   FRAME-NAME                                                           */
/* BROWSE-TAB brwFiles TEXT-1 fOpen */
ASSIGN 
       FRAME fOpen:SCROLLABLE       = FALSE
       FRAME fOpen:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bRestart IN FRAME fOpen
   NO-ENABLE                                                            */
ASSIGN 
       brwFiles:ALLOW-COLUMN-SEARCHING IN FRAME fOpen = TRUE
       brwFiles:COLUMN-RESIZABLE IN FRAME fOpen       = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFiles
/* Query rebuild information for BROWSE brwFiles
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH localQarFileInfo by localQarFileInfo.name.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwFiles */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fOpen fOpen
ON WINDOW-CLOSE OF FRAME fOpen /* Restart Active Review */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFiles
&Scoped-define SELF-NAME brwFiles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFiles fOpen
ON DEFAULT-ACTION OF brwFiles IN FRAME fOpen
DO:
   if not available localQarFileInfo
    then return.
    
  apply "GO" to frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFiles fOpen
ON ROW-DISPLAY OF brwFiles IN FRAME fOpen
DO:
  {lib/brw-rowDisplay.i}

  if current-result-row("brwFiles") modulo 2 = 0 then
    iBgColor = 17.
  else
    iBgColor = 15.

  localQarFileInfo.identifier:bgcolor in browse brwFiles   = iBgcolor.
  localQarFileInfo.name:bgcolor in browse brwFiles         = iBgcolor.
  localQarFileInfo.createdDate:bgcolor in browse brwFiles  = iBgcolor.
  localQarFileInfo.modifiedDate:bgcolor in browse brwFiles = iBgcolor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFiles fOpen
ON START-SEARCH OF brwFiles IN FRAME fOpen
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fOpen 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.



/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
  run loadReviews in this-procedure.
  RUN enable_UI.
  
  bRestart:sensitive in frame {&frame-name} = can-find(first localQarFileInfo).

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  if available localQarFileInfo 
   then assign
          pSelected = true
          pIdentifier = localQarFileInfo.identifier
          pFullFile = localQarFileInfo.fullpath
          .
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fOpen  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fOpen.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fOpen  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE brwFiles bCancel 
      WITH FRAME fOpen.
  VIEW FRAME fOpen.
  {&OPEN-BROWSERS-IN-QUERY-fOpen}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE loadReviews fOpen 
PROCEDURE loadReviews :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwFiles.
  empty temp-table localQarFileInfo.
  publish "GetLocalQarFiles" (output table localQarFileInfo).
  {&OPEN-BROWSERS-IN-QUERY-fOpen}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData fOpen 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

