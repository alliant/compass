&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File:         wfindbp.w 

  Description:  Show Finding and Best practice of each question

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

  Modification:
  Date          Name      Description
  01/09/2017    AC        Implement finding best practice functionality   
  20/14/2017    AC        Implement Inspect functionality.    
  08/09/2017    AG        Changes related to sectionID           
  09/24/2021    SA        Task#:86696 Defects raised by QA      
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

{tt/qaraction.i}
{tt/qaraccount.i}
{tt/qarauditfile.i &tableAlias=affectedFile}
{tt/qarauditfile.i &tableAlias=unAffectedFile}
{tt/qaraccount.i &tableAlias=affectedAccount} 
{tt/qaraccount.i &tableAlias=unAffectedAccount}
{lib/std-def.i}

def input parameter pQuestionSeq as int.
def input parameter pQuestionID as char.
def input parameter pDescription as char.
def input parameter pPriority as int.
def input parameter pUsesFiles as logical.
def input parameter pCanChangePriority as logical.
def input parameter pButton as widget-handle.
def input parameter pSectionId as int.
def input parameter pParentWindow as char.
def var cStatus as char.
def var cNote as char.
def var tModified as logical initial false.
def var tPriority as int .
def var tAction as char.
def var tDueDate as date.
def var tfiles as char.
def var mov as decimal .
def var qText as char.
def var pAuditStatus as char.
def var iBgcolor as int.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME bActions

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES action affectedFile affectedAccount ~
unaffectedFile unaffectedAccount

/* Definitions for BROWSE bActions                                      */
&Scoped-define FIELDS-IN-QUERY-bActions action.description entry(index("ICOR0",action.stat),"In Process,Completed,Open,Rejected, " ) @ action.stat action.dueDate action.note   
&Scoped-define ENABLED-FIELDS-IN-QUERY-bActions   
&Scoped-define SELF-NAME bActions
&Scoped-define QUERY-STRING-bActions FOR EACH action
&Scoped-define OPEN-QUERY-bActions OPEN QUERY {&SELF-NAME} FOR EACH action .
&Scoped-define TABLES-IN-QUERY-bActions action
&Scoped-define FIRST-TABLE-IN-QUERY-bActions action


/* Definitions for BROWSE bAffected                                     */
&Scoped-define FIELDS-IN-QUERY-bAffected affectedFile.fileNumber   
&Scoped-define ENABLED-FIELDS-IN-QUERY-bAffected   
&Scoped-define SELF-NAME bAffected
&Scoped-define QUERY-STRING-bAffected FOR EACH affectedFile by affectedFile.fileNumber
&Scoped-define OPEN-QUERY-bAffected OPEN QUERY {&SELF-NAME} FOR EACH affectedFile by affectedFile.fileNumber.
&Scoped-define TABLES-IN-QUERY-bAffected affectedFile
&Scoped-define FIRST-TABLE-IN-QUERY-bAffected affectedFile


/* Definitions for BROWSE bAffectedAccount                              */
&Scoped-define FIELDS-IN-QUERY-bAffectedAccount affectedAccount.acctNumber   
&Scoped-define ENABLED-FIELDS-IN-QUERY-bAffectedAccount   
&Scoped-define SELF-NAME bAffectedAccount
&Scoped-define QUERY-STRING-bAffectedAccount FOR EACH affectedAccount by affectedAccount.acctNumber
&Scoped-define OPEN-QUERY-bAffectedAccount OPEN QUERY {&SELF-NAME} FOR EACH affectedAccount by affectedAccount.acctNumber.
&Scoped-define TABLES-IN-QUERY-bAffectedAccount affectedAccount
&Scoped-define FIRST-TABLE-IN-QUERY-bAffectedAccount affectedAccount


/* Definitions for BROWSE bUnaffected                                   */
&Scoped-define FIELDS-IN-QUERY-bUnaffected unaffectedFile.fileNumber   
&Scoped-define ENABLED-FIELDS-IN-QUERY-bUnaffected   
&Scoped-define SELF-NAME bUnaffected
&Scoped-define QUERY-STRING-bUnaffected FOR EACH unaffectedFile by unaffectedFile.fileNumber
&Scoped-define OPEN-QUERY-bUnaffected OPEN QUERY {&SELF-NAME} FOR EACH unaffectedFile by unaffectedFile.fileNumber.
&Scoped-define TABLES-IN-QUERY-bUnaffected unaffectedFile
&Scoped-define FIRST-TABLE-IN-QUERY-bUnaffected unaffectedFile


/* Definitions for BROWSE bUnaffectedAccount                            */
&Scoped-define FIELDS-IN-QUERY-bUnaffectedAccount unaffectedAccount.acctNumber   
&Scoped-define ENABLED-FIELDS-IN-QUERY-bUnaffectedAccount   
&Scoped-define SELF-NAME bUnaffectedAccount
&Scoped-define QUERY-STRING-bUnaffectedAccount FOR EACH unaffectedAccount by unaffectedAccount.acctNumber
&Scoped-define OPEN-QUERY-bUnaffectedAccount OPEN QUERY {&SELF-NAME} FOR EACH unaffectedAccount by unaffectedAccount.acctNumber.
&Scoped-define TABLES-IN-QUERY-bUnaffectedAccount unaffectedAccount
&Scoped-define FIRST-TABLE-IN-QUERY-bUnaffectedAccount unaffectedAccount


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-bActions}~
    ~{&OPEN-QUERY-bAffected}~
    ~{&OPEN-QUERY-bAffectedAccount}~
    ~{&OPEN-QUERY-bUnaffected}~
    ~{&OPEN-QUERY-bUnaffectedAccount}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tType tFinding tReference bAdd bActions ~
bEdit bDelete Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tPriorityOverride tType tFinding ~
tReference qLabel qNumber qText1 qText2 tPriorityLabel disLabel ~
findDetailLabel refLabel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAdd 
     LABEL "Add..." 
     SIZE 8 BY 1.14 TOOLTIP "Add a new action".

DEFINE BUTTON bDelete 
     LABEL "Delete" 
     SIZE 8 BY 1.14 TOOLTIP "Delete the currently selected action".

DEFINE BUTTON bEdit 
     LABEL "Edit..." 
     SIZE 8 BY 1.14 TOOLTIP "Delete the currently selected action".

DEFINE BUTTON bMoveToAffected 
     LABEL "Move -->" 
     SIZE 12 BY 1.14.

DEFINE BUTTON bMoveToUnaffected 
     LABEL "<-- Move" 
     SIZE 12 BY 1.14.

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.38.

DEFINE BUTTON Btn_OK 
     LABEL "OK" 
     SIZE 15 BY 1.38.

DEFINE VARIABLE tBestPractice AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 108 BY 3.81 NO-UNDO.

DEFINE VARIABLE tFinding AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 108 BY 3.81 NO-UNDO.

DEFINE VARIABLE tReference AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 103.4 BY 3.29 NO-UNDO.

DEFINE VARIABLE disLabel AS CHARACTER FORMAT "X(60)":U INITIAL "Description Of Finding" 
      VIEW-AS TEXT 
     SIZE 43.4 BY .81
     FONT 16 NO-UNDO.

DEFINE VARIABLE findDetailLabel AS CHARACTER FORMAT "X(20)":U INITIAL "Finding Details" 
      VIEW-AS TEXT 
     SIZE 17 BY .81
     FONT 16 NO-UNDO.

DEFINE VARIABLE qLabel AS CHARACTER FORMAT "X(12)":U INITIAL "Question:" 
      VIEW-AS TEXT 
     SIZE 13 BY .95
     FONT 16 NO-UNDO.

DEFINE VARIABLE qNumber AS CHARACTER FORMAT "X(12)":U 
      VIEW-AS TEXT 
     SIZE 10 BY .95 NO-UNDO.

DEFINE VARIABLE qText1 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 105 BY .95
     FONT 16 NO-UNDO.

DEFINE VARIABLE qText2 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 77 BY .95
     FONT 16 NO-UNDO.

DEFINE VARIABLE refLabel AS CHARACTER FORMAT "X(20)":U INITIAL " Reference(s):" 
      VIEW-AS TEXT 
     SIZE 17 BY .62
     FONT 16 NO-UNDO.

DEFINE VARIABLE tActionType AS CHARACTER FORMAT "X(256)":U INITIAL "Recommended Action" 
      VIEW-AS TEXT 
     SIZE 30 BY .76
     FONT 18 NO-UNDO.

DEFINE VARIABLE tFilesLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Files:" 
      VIEW-AS TEXT 
     SIZE 17.8 BY .71 NO-UNDO.

DEFINE VARIABLE tPriorityLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Priority" 
      VIEW-AS TEXT 
     SIZE 6.8 BY .62 NO-UNDO.

DEFINE IMAGE iPriorityMismatch TRANSPARENT
     SIZE 4 BY .95 TOOLTIP "The priority has been changed from the default assigned to the question".

DEFINE VARIABLE tPriorityOverride AS INTEGER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Minor", 1,
"Intermediate", 2,
"Major", 3
     SIZE 16 BY 3 NO-UNDO.

DEFINE VARIABLE tType AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Finding", "F",
"Best Practice", "B"
     SIZE 20 BY 1.91 NO-UNDO.

DEFINE RECTANGLE tFilesRect
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 108 BY 10.38.

DEFINE RECTANGLE tPriorityRect
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 24 BY 3.81.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY bActions FOR 
      action SCROLLING.

DEFINE QUERY bAffected FOR 
      affectedFile SCROLLING.

DEFINE QUERY bAffectedAccount FOR 
      affectedAccount SCROLLING.

DEFINE QUERY bUnaffected FOR 
      unaffectedFile SCROLLING.

DEFINE QUERY bUnaffectedAccount FOR 
      unaffectedAccount SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE bActions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS bActions C-Win _FREEFORM
  QUERY bActions DISPLAY
      action.description     format "x(30)"  label "Description"
  entry(index("ICOR0",action.stat),"In Process,Completed,Open,Rejected, " )  @ action.stat  label "Status" width 15
  action.dueDate         format 99/99/99 label "Due Date"
  action.note            format "x(30)"  label "Note"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 95 BY 4.05 FIT-LAST-COLUMN.

DEFINE BROWSE bAffected
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS bAffected C-Win _FREEFORM
  QUERY bAffected DISPLAY
      affectedFile.fileNumber label "Affected File"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 30 BY 5 ROW-HEIGHT-CHARS .62 FIT-LAST-COLUMN.

DEFINE BROWSE bAffectedAccount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS bAffectedAccount C-Win _FREEFORM
  QUERY bAffectedAccount DISPLAY
      affectedAccount.acctNumber label "Affected Account"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 30 BY 5 ROW-HEIGHT-CHARS .62 FIT-LAST-COLUMN.

DEFINE BROWSE bUnaffected
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS bUnaffected C-Win _FREEFORM
  QUERY bUnaffected DISPLAY
      unaffectedFile.fileNumber label "Unaffected File"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 30 BY 5 FIT-LAST-COLUMN.

DEFINE BROWSE bUnaffectedAccount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS bUnaffectedAccount C-Win _FREEFORM
  QUERY bUnaffectedAccount DISPLAY
      unaffectedAccount.acctNumber label "Unaffected Account"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 30 BY 5 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     tPriorityOverride AT ROW 4.57 COL 88.2 NO-LABEL WIDGET-ID 28
     tType AT ROW 5.29 COL 3 NO-LABEL WIDGET-ID 60
     tBestPractice AT ROW 8.14 COL 3 NO-LABEL WIDGET-ID 14
     tFinding AT ROW 8.14 COL 3 NO-LABEL WIDGET-ID 56
     bUnaffected AT ROW 13.29 COL 21.4 WIDGET-ID 300
     bAffectedAccount AT ROW 13.29 COL 67.4 WIDGET-ID 700
     bAffected AT ROW 13.29 COL 67.4 WIDGET-ID 400
     bUnaffectedAccount AT ROW 13.33 COL 21.2 WIDGET-ID 800
     bMoveToAffected AT ROW 13.62 COL 53.4 WIDGET-ID 48
     tReference AT ROW 14.14 COL 5 NO-LABEL WIDGET-ID 58
     bMoveToUnaffected AT ROW 15.05 COL 53.4 WIDGET-ID 50
     bAdd AT ROW 18.62 COL 4 WIDGET-ID 44
     bActions AT ROW 18.62 COL 14 WIDGET-ID 200
     bEdit AT ROW 20.05 COL 4 WIDGET-ID 42
     bDelete AT ROW 21.48 COL 4 WIDGET-ID 46
     Btn_OK AT ROW 23.86 COL 37 WIDGET-ID 10
     Btn_Cancel AT ROW 23.86 COL 56 WIDGET-ID 12
     qLabel AT ROW 1.48 COL 3 NO-LABEL WIDGET-ID 144
     qNumber AT ROW 1.48 COL 15 COLON-ALIGNED NO-LABEL WIDGET-ID 2
     qText1 AT ROW 2.91 COL 1 COLON-ALIGNED NO-LABEL WIDGET-ID 4
     qText2 AT ROW 3.86 COL 1 COLON-ALIGNED NO-LABEL WIDGET-ID 6
     tPriorityLabel AT ROW 3.86 COL 86.2 COLON-ALIGNED NO-LABEL WIDGET-ID 40 NO-TAB-STOP 
     disLabel AT ROW 7.33 COL 3 NO-LABEL WIDGET-ID 146
     findDetailLabel AT ROW 12.48 COL 4.2 NO-LABEL WIDGET-ID 148
     tFilesLabel AT ROW 13.38 COL 1.2 COLON-ALIGNED NO-LABEL WIDGET-ID 26 NO-TAB-STOP 
     refLabel AT ROW 13.38 COL 4 NO-LABEL WIDGET-ID 66
     tActionType AT ROW 17.67 COL 4 NO-LABEL WIDGET-ID 18 NO-TAB-STOP 
     tFilesRect AT ROW 13 COL 2.4 WIDGET-ID 22
     tPriorityRect AT ROW 4.1 COL 87.2 WIDGET-ID 34
     iPriorityMismatch AT ROW 4.33 COL 106 WIDGET-ID 38
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1.05
         SIZE 111.2 BY 30.19 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Finding/Best Practice"
         HEIGHT             = 24.71
         WIDTH              = 110.8
         MAX-HEIGHT         = 33.62
         MAX-WIDTH          = 272.8
         VIRTUAL-HEIGHT     = 33.62
         VIRTUAL-WIDTH      = 272.8
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB bUnaffected tFinding DEFAULT-FRAME */
/* BROWSE-TAB bAffectedAccount bUnaffected DEFAULT-FRAME */
/* BROWSE-TAB bAffected bAffectedAccount DEFAULT-FRAME */
/* BROWSE-TAB bUnaffectedAccount bAffected DEFAULT-FRAME */
/* BROWSE-TAB bActions bAdd DEFAULT-FRAME */
ASSIGN 
       bActions:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       bActions:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

/* SETTINGS FOR BROWSE bAffected IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bAffected:HIDDEN  IN FRAME DEFAULT-FRAME                = TRUE.

/* SETTINGS FOR BROWSE bAffectedAccount IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bAffectedAccount:HIDDEN  IN FRAME DEFAULT-FRAME                = TRUE.

/* SETTINGS FOR BUTTON bMoveToAffected IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bMoveToAffected:HIDDEN IN FRAME DEFAULT-FRAME           = TRUE.

/* SETTINGS FOR BUTTON bMoveToUnaffected IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bMoveToUnaffected:HIDDEN IN FRAME DEFAULT-FRAME           = TRUE.

/* SETTINGS FOR BROWSE bUnaffected IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bUnaffected:HIDDEN  IN FRAME DEFAULT-FRAME                = TRUE.

/* SETTINGS FOR BROWSE bUnaffectedAccount IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bUnaffectedAccount:HIDDEN  IN FRAME DEFAULT-FRAME                = TRUE.

/* SETTINGS FOR FILL-IN disLabel IN FRAME DEFAULT-FRAME
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN findDetailLabel IN FRAME DEFAULT-FRAME
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR IMAGE iPriorityMismatch IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       iPriorityMismatch:HIDDEN IN FRAME DEFAULT-FRAME           = TRUE.

/* SETTINGS FOR FILL-IN qLabel IN FRAME DEFAULT-FRAME
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN qNumber IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN qText1 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN qText2 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN refLabel IN FRAME DEFAULT-FRAME
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN tActionType IN FRAME DEFAULT-FRAME
   NO-DISPLAY NO-ENABLE ALIGN-L                                         */
/* SETTINGS FOR EDITOR tBestPractice IN FRAME DEFAULT-FRAME
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       tBestPractice:HIDDEN IN FRAME DEFAULT-FRAME           = TRUE.

/* SETTINGS FOR FILL-IN tFilesLabel IN FRAME DEFAULT-FRAME
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       tFilesLabel:HIDDEN IN FRAME DEFAULT-FRAME           = TRUE.

/* SETTINGS FOR RECTANGLE tFilesRect IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tPriorityLabel IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR RADIO-SET tPriorityOverride IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE tPriorityRect IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE bActions
/* Query rebuild information for BROWSE bActions
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH action .
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE bActions */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE bAffected
/* Query rebuild information for BROWSE bAffected
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH affectedFile by affectedFile.fileNumber.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE bAffected */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE bAffectedAccount
/* Query rebuild information for BROWSE bAffectedAccount
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH affectedAccount by affectedAccount.acctNumber.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE bAffectedAccount */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE bUnaffected
/* Query rebuild information for BROWSE bUnaffected
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH unaffectedFile by unaffectedFile.fileNumber.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE bUnaffected */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE bUnaffectedAccount
/* Query rebuild information for BROWSE bUnaffectedAccount
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH unaffectedAccount by unaffectedAccount.acctNumber.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE bUnaffectedAccount */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Finding/Best Practice */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Finding/Best Practice */
DO:
  /* This event will close the window and terminate the procedure.  */
  tModified = false .
  publish "SetFnBPChanged" (input "").
  publish "LockOff".
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME bActions
&Scoped-define SELF-NAME bActions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActions C-Win
ON DEFAULT-ACTION OF bActions IN FRAME DEFAULT-FRAME
DO:
 apply "CHOOSE" to bEdit in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActions C-Win
ON ROW-DISPLAY OF bActions IN FRAME DEFAULT-FRAME
DO:
   if current-result-row("bActions") modulo 2 = 0 then
  iBgColor = 17.
 else
  iBgColor = 15.
   action.description:bgcolor in browse bActions = iBgColor.
   action.stat :bgcolor in browse bActions = iBgColor.
   action.dueDate   :bgcolor in browse bActions = iBgColor.
   action.note  :bgcolor in browse bActions = iBgColor.
   if action.stat = "" then
       action.stat = "0".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActions C-Win
ON ROW-LEAVE OF bActions IN FRAME DEFAULT-FRAME
DO:
 /*
 def var tSeq as int.
 IF bActions:NEW-ROW IN FRAME {&frame-name} 
  THEN 
   DO: if input browse bActions action.description = ""
        then 
         do: close query bActions.
             open query bActions for each action by action.dueDate.
             return.
         end.
       DO ON ERROR UNDO, RETURN NO-APPLY:
         CREATE action.
         ASSIGN input browse bActions action.description action.dueDate.
         bActions:CREATE-RESULT-LIST-ENTRY().
         tModified = true.
       END.
   end.
   */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActions C-Win
ON START-SEARCH OF bActions IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAdd C-Win
ON CHOOSE OF bAdd IN FRAME DEFAULT-FRAME /* Add... */
DO:
  assign
    std-ch = ""
    std-da = ?
    cStatus = ""
    cNote = ""
    .
  if tActionType:screen-value = "Corrective Action"
   then 
    do: cStatus = "O".
        run dialogca.w (input-output std-ch,  /*action*/
                        input-output std-da,
                        input-output cStatus,
                        input-output cNote,
                        output std-lo).
    end.
   else run dialogaction.w (tActionType:screen-value,
                            input-output std-ch,
                            output std-lo).

  if std-lo then return.

  CREATE action.
  ASSIGN 
   action.description = std-ch
   action.dueDate = std-da
   action.stat = cStatus
   action.note = cNote
   .
  release action.

  close query bActions.
  open query bActions 
    for each action by action.note.
  tModified = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME bAffected
&Scoped-define SELF-NAME bAffected
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAffected C-Win
ON DEFAULT-ACTION OF bAffected IN FRAME DEFAULT-FRAME
DO:
  apply "CHOOSE" to bMoveToUnaffected.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME bAffectedAccount
&Scoped-define SELF-NAME bAffectedAccount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAffectedAccount C-Win
ON DEFAULT-ACTION OF bAffectedAccount IN FRAME DEFAULT-FRAME
DO:
  apply "CHOOSE" to bMoveToUnaffected.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME DEFAULT-FRAME /* Delete */
DO:
   if bActions:num-selected-rows = 0 
    then return.
   bActions:fetch-selected-row(1).
   get current bActions exclusive-lock.
   MESSAGE "Action will be removed."
    VIEW-AS ALERT-BOX warning BUTTONS ok-cancel update std-lo.
   if std-lo
    then 
     do: delete action.
         bActions:delete-selected-rows().
         tModified = true.
     end.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEdit C-Win
ON CHOOSE OF bEdit IN FRAME DEFAULT-FRAME /* Edit... */
DO:
 if bActions:num-selected-rows = 0 
  then return.
 bActions:fetch-selected-row(1).
 get current bActions exclusive-lock.
 assign
   std-ch = action.description
   std-da = action.dueDate
   cStatus = action.stat
   cNote =  action.note
   .
 if tActionType:screen-value = "Corrective Action"
  then run dialogca.w (input-output std-ch,
                       input-output std-da,
                       input-output cStatus,
                       input-output cNote,
                       output std-lo).
  else run dialogaction.w (tActionType:screen-value,
                           input-output std-ch,
                           output std-lo).
 if std-lo 
  then return.

 assign 
  action.description = std-ch
  action.dueDate = std-da
  action.stat = cStatus
  action.note = cNote
  .
 release action.

 close query bActions.
 open query bActions for each action by action.note.
 tModified = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bMoveToAffected
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bMoveToAffected C-Win
ON CHOOSE OF bMoveToAffected IN FRAME DEFAULT-FRAME /* Move --> */
DO:
  if pSectionId = 6 then
  do:
    if bUnaffectedAccount:num-selected-rows = 0 
     then return.
    bUnaffectedAccount:fetch-selected-row(1).
    get current bUnaffectedAccount exclusive-lock.
    create affectedAccount.
    buffer-copy unaffectedAccount to affectedAccount.
    
    delete unaffectedAccount.
    bUnaffectedAccount:delete-selected-rows().
    
    close query bAffectedAccount.
    open query bAffectedAccount for each affectedAccount by affectedAccount.acctNumber.
    tModified = true.
  end.
  else
  do:
    if bUnaffected:num-selected-rows = 0 
     then return.
    bUnaffected:fetch-selected-row(1).
    get current bUnaffected exclusive-lock.
    create affectedFile.
    buffer-copy unaffectedFile to affectedFile.
    
    delete unaffectedFile.
    bUnaffected:delete-selected-rows().
    
    close query bAffected.
    open query bAffected for each affectedFile by affectedFile.fileNumber.
    tModified = true.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bMoveToUnaffected
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bMoveToUnaffected C-Win
ON CHOOSE OF bMoveToUnaffected IN FRAME DEFAULT-FRAME /* <-- Move */
DO:
 if pSectionId = 6 then
 do:
   if bAffectedAccount:num-selected-rows = 0 
    then return.
   bAffectedAccount:fetch-selected-row(1).
   get current bAffectedAccount exclusive-lock.
   create unaffectedAccount.
   buffer-copy affectedAccount to unaffectedAccount.
   
   delete affectedAccount.
   bAffectedAccount:delete-selected-rows().
   
   close query bUnaffectedAccount.
   open query bUnaffectedAccount for each unaffectedAccount by unaffectedAccount.acctNumber.
   tModified = true.
 end.
 else
 do:
   if bAffected:num-selected-rows = 0 
    then return.
   bAffected:fetch-selected-row(1).
   get current bAffected exclusive-lock.
   create unaffectedFile.
   buffer-copy affectedFile to unaffectedFile.
   
   delete affectedFile.
   bAffected:delete-selected-rows().
   
   close query bUnaffected.
   open query bUnaffected for each unaffectedFile by unaffectedFile.fileNumber.
   tModified = true.
 end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel C-Win
ON CHOOSE OF Btn_Cancel IN FRAME DEFAULT-FRAME /* Cancel */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK C-Win
ON CHOOSE OF Btn_OK IN FRAME DEFAULT-FRAME /* OK */
DO:
  run GetModifiedBestPractice (output std-lo, output std-ch).

  if std-lo 
   then publish "SetBestPractice" (pQuestionSeq, std-ch).
  
  def var tDesc as char.
  def var tRef as char.
  def var tFiles as char.
  def var tAccounts as char.
  def var tPriority as int.

  run GetModifiedFinding (output std-lo,
                          output tDesc,
                          output tRef,
                          output tFiles,
                          output tAccounts,
                          output tPriority,
                          output table action,
                          output table account).
  if std-lo
   then
    do: 
      publish "SetFinding" (pQuestionSeq, tDesc, tRef, tFiles, tAccounts, tPriority).
      publish "DeleteActions" (pQuestionSeq).
      for each action:
        publish "SetAction" (pQuestionSeq,
                             action.description, 
                             action.dueDate, 
                             action.stat, 
                             action.note ).
      end.
    end.
  if pParentWindow = "Section" then
  do:
    publish "GetHasFinding" (pQuestionSeq, output std-lo).
    if not std-lo 
      then publish "GetHasBestPractice" (pQuestionSeq, output std-lo).
    if std-lo 
      then 
      pButton:load-image("images/notes.bmp").
    else pButton:load-image("images/notesblank.bmp").
  end.
  apply "window-close" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME bUnaffected
&Scoped-define SELF-NAME bUnaffected
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bUnaffected C-Win
ON DEFAULT-ACTION OF bUnaffected IN FRAME DEFAULT-FRAME
DO:
  apply "CHOOSE" to bMoveToAffected.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME bUnaffectedAccount
&Scoped-define SELF-NAME bUnaffectedAccount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bUnaffectedAccount C-Win
ON DEFAULT-ACTION OF bUnaffectedAccount IN FRAME DEFAULT-FRAME
DO:
  apply "CHOOSE" to bMoveToAffected.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tBestPractice
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tBestPractice C-Win
ON LEAVE OF tBestPractice IN FRAME DEFAULT-FRAME
DO:
  if self:modified then tModified = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tFinding
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFinding C-Win
ON LEAVE OF tFinding IN FRAME DEFAULT-FRAME
DO:
  if self:modified then tModified = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tPriorityOverride
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tPriorityOverride C-Win
ON VALUE-CHANGED OF tPriorityOverride IN FRAME DEFAULT-FRAME
DO:
  tModified = true.
  iPriorityMismatch:visible = (tPriorityOverride:input-value <> pPriority).
  tActionType:screen-value  = entry(int(tPriorityOverride:input-value), "Suggested,Recommended,Corrective") + " Action".
  publish "EnableSave".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tReference
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tReference C-Win
ON LEAVE OF tReference IN FRAME DEFAULT-FRAME
DO:
  if self:modified then tModified = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tType C-Win
ON VALUE-CHANGED OF tType IN FRAME DEFAULT-FRAME
DO:
 if tType:screen-value = "B" then
  assign
   tFinding:visible = false
   tFinding:sensitive = false
   tFinding:hidden = true
   tBestPractice:visible = true
   tBestPractice:sensitive = true
   tBestPractice:hidden = false
   c-Win:height = 14.24
   disLabel:screen-value = "Description of Best-practice" 
   Btn_OK:row = 13
   Btn_Cancel:row = 13
   tReference:visible = false
   tReference:sensitive = false
   tReference:hidden = true
   refLabel:visible =false
   tFilesRect:hidden = true
   findDetailLabel:visible = false
   bAffected:visible = false
   bAffected:sensitive = false
   bAffectedAccount:visible = false
   bAffectedAccount:sensitive = false
   bUnaffected:visible = false
   bUnaffected:sensitive = false
   bUnaffectedAccount:visible = false
   bUnaffectedAccount:sensitive = false
   bMoveToAffected:visible = false
   bMoveToAffected:sensitive = false
   bMoveToUnaffected:visible = false
   bMoveToUnaffected:sensitive = false
   tFilesLabel:visible = false
   tFilesLabel:sensitive = false
   tActionType:visible = false
   tActionType:sensitive = false 
   bActions:visible = false
   bAdd:visible = false
   bActions:sensitive = false 
   bAdd:sensitive = false 
   bEdit:visible = false
   bEdit:sensitive = false 
   bDelete:visible = false
   bDelete:sensitive = false 
   .
 else if tType:screen-value = "F" then
  do:
   assign
    tBestPractice:visible = false
    tBestPractice:sensitive = false
    tBestPractice:hidden = true   
    tFinding:visible = true
    tFinding:sensitive = true
    tFinding:hidden = false
    disLabel:screen-value = "Description of Findings" 
    tReference:visible = true
    tReference:sensitive = true
    tReference:hidden = false
    refLabel:visible = true
    tFilesRect:hidden = false
    findDetailLabel:visible = true
    tActionType:visible = true
    tActionType:sensitive = true 
    bAdd:visible = true
    bActions:visible = true
    bAdd:sensitive = true 
    bActions:sensitive = true 
    bEdit:visible = true
    bEdit:sensitive = true 
    bDelete:visible = true
    bDelete:sensitive = true 
   .

   if pUsesFiles 
    then
     assign
      c-Win:height = 30.18
      Btn_OK:row = 29.3
      Btn_Cancel:row = 29.3         
      bAffected:visible = true
      bAffected:sensitive = true
      bUnaffected:visible = true
      bUnaffected:sensitive = true
      bMoveToAffected:visible = true
      bMoveToAffected:sensitive = true
      bMoveToUnaffected:visible = true
      bMoveToUnaffected:sensitive = true
      tFilesLabel:visible = true
      tFilesLabel:sensitive = true  .
   else if pSectionId = 6
    then
     assign
      c-Win:height = 30.18 
      Btn_OK:row =29.3
      Btn_Cancel:row = 29.3         
      bAffectedAccount:visible = true
      bAffectedAccount:sensitive = true
      bUnaffectedAccount:visible = true
      bUnaffectedAccount:sensitive = true
      bMoveToAffected:visible = true
      bMoveToAffected:sensitive = true
      bMoveToUnaffected:visible = true
      bMoveToUnaffected:sensitive = true
      tFilesLabel:visible = true
      tFilesLabel:sensitive = true  .
    
   else
    assign                       
     c-Win:height = 24.71
     Btn_OK:row = 23.86
     Btn_Cancel:row = 23.86        
   .
  end.
  publish "GetAuditStatus" (output pAuditStatus).
  if pAuditStatus = "C" then
    run AuditReadOnly.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME bActions
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */  
assign 
  c-Win:max-width  = 110.80
  c-Win:max-height = 30.18
  c-Win:min-width  = 14.24
  c-Win:title = "Finding/Best Practice for Question: " + pQuestionID
  .       

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

subscribe to "CloseFindBp" anywhere.
subscribe to "SetFocusFindBp" anywhere.
subscribe to "IsModifiedFindBp" anywhere .
subscribe to "AuditReadOnly" anywhere.
pDescription = pDescription + fill("*", pPriority - 1).
if length(pDescription) > 70 
 then
  do: 
    std-in = r-index(pDescription, " ", 70).
    if std-in = 0 then std-in = 70.
    qText1 = substring(pDescription, 1, std-in - 1).
    qText = substring(pDescription, std-in + 1).
    if length(qText) gt 50 then
    do:
      qText2 = substring(pDescription, std-in + 1,  r-index(qText, " ", 55 ) - 1 ).
      qText2 = trim(qText2 + "" + fill(".", 3)).
      qText2:tooltip = "..." + substring(qText, r-index(qText, " ", 55 ) + 1 ).
    end.
    else
      qText2 = substring(pDescription, std-in + 1).
  end.
else 
  assign
    qText1 = pDescription
    qText2 = "".

assign
    qText1 = replace(qText1,'&','&&') 
    qText2 = replace(qText2,'&','&&')  
    .
    
case pPriority:
 when 3 then
 do: 
   assign
     qLabel:font in frame {&frame-name} = 18
     qNumber:font in frame {&frame-name} = 18
     qText1:font in frame {&frame-name} = 18
     qText2:font in frame {&frame-name} = 18
     .
 end.
 when 2 then
 do:
   assign
     qLabel:font in frame {&frame-name} = 18
     qNumber:font in frame {&frame-name} = 17
     qText1:font in frame {&frame-name} = 17
     qText2:font in frame {&frame-name} = 17
     .
 end.
end case.

qNumber = pQuestionID.
/* Best Practice InitializeObject */                    
publish "GetBestPractice" (pQuestionSeq, output tBestPractice).

tBestPractice:screen-value = tBestPractice. 
tBestPractice:modified     = false.
 
publish "GetFinding" (pQuestionSeq, 
                      output tFinding,   /* UI variable */ 
                      output tReference, /* UI variable */
                      output tfiles,     /* List of files */
                      output std-ch,     /* List of Accounts */
                      output tPriorityOverride).    /* Priority (overridden) */
if tPriorityOverride = 0 
 then tPriorityOverride = max(1, pPriority).

if pSectionId = 6 then
do:
  publish "GetAccounts" (output table unaffectedAccount).
  /* Move to "affected" side */
  ACCOUNT-LOOP: 
  for each unaffectedAccount:
    do std-in = 1 to num-entries(std-ch, "|"):
      if unaffectedAccount.acctNumber = entry(std-in, std-ch, "|")
      then
       do:                                     
         create affectedAccount.
         buffer-copy unaffectedAccount to affectedAccount.
         delete unaffectedAccount.
         next ACCOUNT-LOOP.
       end.
    end.
  end.
end.

publish "GetAgentFiles" (output table unaffectedFile).                                    
/* Move to "affected" side */
FILE-LOOP:
for each unaffectedFile:                  
  do std-in = 1 to num-entries(tfiles, "|"):
    if unaffectedFile.fileID = int(entry(std-in, tfiles, "|"))
     then
      do: 
        create affectedFile.
        buffer-copy unaffectedFile to affectedFile.
        delete unaffectedFile.
        next FILE-LOOP.
      end.
  end.
end.

publish "GetActions" (input integer(pQuestionSeq),
                      output table action).
mov = 5.47 .
if pUsesFiles 
then
  assign
    bUnaffected:visible  = true
    bUnaffected:sensitive = true
    bAffected:visible = true
    bAffected:sensitive = true
    bMoveToAffected:visible = true
    bMoveToAffected:sensitive = true
    bMoveToUnaffected:visible = true
    bMoveToUnaffected:sensitive = true
    tFilesLabel:visible = true
    tFilesLabel:screen-value = "                   Files:"
    tFilesRect:visible = true  
    tReference:row = tReference:row + mov
    tActionType:row = tActionType:row + mov
    bActions:row = bActions:row + mov
    bAdd:row = bAdd:row + mov
    bEdit:row = bEdit:row + mov
    bDelete:row = bDelete:row + mov
    Btn_OK:row = 29.33
    Btn_Cancel:row = 29.33
    {&window-name}:height = 30.18
    tFilesRect:height = tFilesRect:height + mov
    refLabel:row = refLabel:row + mov
    .
else if pSectionId = 6
then
  assign
    bUnaffectedAccount:visible  = true
    bUnaffectedAccount:sensitive = true
    bAffectedAccount:visible = true
    bAffectedAccount:sensitive = true
    bMoveToAffected:visible = true
    bMoveToAffected:sensitive = true
    bMoveToUnaffected:visible = true
    bMoveToUnaffected:sensitive = true
    tFilesLabel:visible = true
    tFilesLabel:screen-value = "Escrow Accounts:" 
    tFilesRect:visible = true
    tReference:row = tReference:row + mov
    tActionType:row = tActionType:row + mov
    bActions:row = bActions:row + mov
    bAdd:row = bAdd:row + mov
    bEdit:row = bEdit:row + mov
    bDelete:row = bDelete:row + mov
    Btn_OK:row = 29.33
    Btn_Cancel:row = 29.33
    {&window-name}:height = 30.18
    tFilesRect:height = tFilesRect:height + mov
    refLabel:row = refLabel:row + mov
    .

 tActionType:screen-value = entry(tPriorityOverride, "Suggested,Recommended,Corrective") + " Action".
 iPriorityMismatch:load-image("images/exclamation.gif").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  publish "LockOn".
  publish "GetAuditStatus" (output pAuditStatus).
  if pAuditStatus = "C" then
    run AuditReadOnly.
  publish "SetFnBPChanged" (input ""). 

 iPriorityMismatch:visible = (tPriorityOverride:input-value <> pPriority).
 tPriorityOverride:sensitive = pCanChangePriority.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditReadOnly C-Win 
PROCEDURE AuditReadOnly :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*--------------not used--------------*/
/*do with frame DEFAULT-FRAME :
end.
assign
  Btn_OK:sensitive = false
  bDelete:sensitive = false
  bEdit:sensitive = false
  bAdd:sensitive = false
  bMoveToAffected:sensitive = false
  bMoveToUnaffected:sensitive = false
  tPriorityOverride:sensitive = false
  tReference:sensitive = false
  tBestPractice:sensitive = false
  tFinding:sensitive = false
  .*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseFindBp C-Win 
PROCEDURE CloseFindBp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pclosesecid as char .
 if (string(pSectionId) = pclosesecid) or (pclosesecid eq "auditclose")  or (pclosesecid eq "autoclose") then
 do:
   apply "window-close" to {&window-name}.
   return no-apply.
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tPriorityOverride tType tFinding tReference qLabel qNumber qText1 
          qText2 tPriorityLabel disLabel findDetailLabel refLabel 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE tType tFinding tReference bAdd bActions bEdit bDelete Btn_OK 
         Btn_Cancel 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetModifiedBestPractice C-Win 
PROCEDURE GetModifiedBestPractice :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pModified as logical.
 def output parameter pComments as char.

 pModified = tBestPractice:modified in frame {&FRAME-NAME}.
 pComments = tBestPractice:screen-value in frame {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetModifiedFinding C-Win 
PROCEDURE GetModifiedFinding :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&FRAME-NAME}:
 end.
 def output parameter pModified as logical.
 def output parameter pFinding as char.
 def output parameter pReference as char.
 def output parameter pFileList as char.
 def output parameter pAccountList as char.
 def output parameter pPriority as int.
 def output parameter table for action.
 def output parameter table for account.
 
 def buffer affectedFile for affectedFile.
 def buffer affectedAccount for affectedAccount.

 assign 
  pModified = tModified
  pFinding = tFinding:screen-value 
  pReference = tReference:screen-value 
  pPriority = tPriorityOverride:input-value 
  pFileList = ""
  pAccountList = ""
  .
 if pSectionId = 6 then
 for each affectedAccount:
  pAccountList = pAccountList + (if pAccountList > "" then "|" else "")
                + string(affectedAccount.acctNumber).
 end.
 for each affectedFile:
  pFileList = pFileList + (if pFileList > "" then "|" else "")
                + string(affectedFile.fileID).
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsModifiedFindBP C-Win 
PROCEDURE IsModifiedFindBP :
/*------------------------------------------------------------------------------
Purpose:     
Parameters:  <none>
Notes:       
------------------------------------------------------------------------------*/
 define input  parameter pclosesecid  as character .
 define output parameter pFnBPChanged as character .
 define output parameter pcorrectCase as character .

 define var pGetFindbp as character.
 
 run LeaveAllFindBp.

 if (string(pSectionId) = pclosesecid)then
   if tmodified = true then       
     publish "SetFnBPChanged" (input pclosesecid).
   else
     publish "SetFnBPChanged" (input "").
 else 
   pcorrectCase = "Not".
        
 if (pclosesecid = "auditclose") or 
    (pclosesecid = "autoclose") then
   if tmodified = true then       
     publish "SetFnBPChanged" (input pclosesecid).
   else
     publish "SetFnBPChanged" (input "").
 else 
   pcorrectCase = "Not".
 
 publish "GetFnBPChanged" (output pGetFindbp ) .
 pFnBPChanged = pGetFindbp .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LeaveAllFindBP C-Win 
PROCEDURE LeaveAllFindBP :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
apply 'leave' to tFinding in frame DEFAULT-FRAME.
apply 'leave' to tBestPractice in frame DEFAULT-FRAME.
apply 'leave' to tReference in frame DEFAULT-FRAME.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetFocusFindBp C-Win 
PROCEDURE SetFocusFindBp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter pQSeq as integer.
if pQSeq = pQuestionSeq then
do:
  apply "ENTRY" to tFinding in frame DEFAULT-FRAME.
  if {&window-name}:window-state eq window-minimized  then
   {&window-name}:window-state = window-normal .
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

