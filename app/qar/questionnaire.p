&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* questionnaire.p

 Modification:
    Date          Name      Description
    01/04/2017    AG        Changed Report title.
    01/10/2017    AG        Fixed Tempfile for working of report
    02/10/2017    AG        Modified to show blank if review date is ?.
    02/20/2017    AC        Modified to show headers in standard font.
    10/01/2020    AC        Modified for TOR Audits
  ----------------------------------------------------------------------*/
{lib/rpt-def.i}

/* The following are used in Section1, but we may need tAgentID for filename */
def var tAgentID as char.
def var tName as char.
def var tAddr as char.
def var tCity as char.
def var tState as char.
def var tZip as char.
def var tContactName as char.
def var tContactPhone as char.
def var tContactFax as char.
def var tContactOther as char.
def var tContactEmail as char.
def var tAuditDate as date.
def var tMainOffice as logical.
def var tNumOffices as int.
def var tNumEmployees as int.
def var tNumUnderwriters as int.
def var tLastRevenue as decimal.
def var tRanking as int.
def var tAuditor as char.
def var tDeliveredTo as char.
def var taudittype as char.
{tt/qarauditquestion.i}  /* question  */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

assign
  activeFont = "Helvetica"
  activeSize = 12.0
  .

{lib/getattrch.i agentID tAgentID}
{lib/getattrch.i name tName}
{lib/getattrch.i addr tAddr}
{lib/getattrch.i city tCity}
{lib/getattrch.i state tState}
{lib/getattrch.i zip tZip}
{lib/getattrch.i contactName tContactName}
{lib/getattrch.i contactPhone tContactPhone}
{lib/getattrch.i contactFax tContactFax}
{lib/getattrch.i contactOther tContactOther}
{lib/getattrch.i contactEmail tContactEmail}
{lib/getattrda.i auditDate tAuditDate}
{lib/getattrlo.i mainOffice tMainOffice}
{lib/getattrin.i numOffices tNumOffices}
{lib/getattrin.i numEmployees tNumEmployees}
{lib/getattrin.i numUnderwriters tNumUnderwriters}
{lib/getattrde.i lastRevenue tLastRevenue}
{lib/getattrin.i ranking tRanking}
{lib/getattrch.i auditor tAuditor}
{lib/getattrch.i deliveredTo tDeliveredTo}
{lib/getattrch.i audittype taudittype}

run getFilename in this-procedure no-error.
if error-status:error 
 then return.

{lib/rpt-open.i &tempID='QuestionnaireRpt'}

run doSummary in this-procedure.

empty temp-table question.
publish "GetQuestionsAnswered" (output table question).
run doQuestions in this-procedure ("QUESTIONS").

empty temp-table question.
publish "GetQuestionsUnanswered" (output table question).
run doQuestions in this-procedure ("OPEN QUESTIONS").

{lib/rpt-cloz.i &tempID='QuestionnaireRpt' &no-save=true}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-doQuestions) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doQuestions Procedure 
PROCEDURE doQuestions PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pTitle as char no-undo.
 def var tAnswer as char no-undo.
 def var tCnt as int.

 blockTitle(pTitle).
 textColor(0.0, 0.0, 0.0).
 setFont ("Helvetica", 12.0).
 
 tCnt = 0.
 for each question 
  break by question.seq:
  case question.answer:
   when "Y" then tAnswer = "Yes".
   when "N" then tAnswer = "No".
   when "A" then tAnswer = "N/A".
   when "U" then tAnswer = "Unk".
   otherwise tAnswer = question.answer.
  end case.

  if first-of(question.seq)
   then
    do: textAt(question.questionID, 14).
        if last-of(question.seq) 
           and question.fileNumber = ""
           and length(tAnswer) <= 3
         then
          do: textAt(tAnswer, 159).
              paragraph(question.description, 75, 30, 30, true).
              newLine(1).
              tCnt = tCnt + 1.
              next.
          end.
        paragraph(question.description, 75, 30, 30, true).
    end.
  if length(tAnswer) <= 3 
   then 
    do: if question.fileNumber = ""
         then textAt(tAnswer, 159).
         else textAt(question.fileNumber + ": " + tAnswer, 30).
    end.
   else 
    do: 
         textAt(question.fileNumber + 
                 (if question.fileNumber > "" then ":" else ""), 30).
         paragraph(tAnswer, 40, 60, 60, true).
    end.
  newLine(1).
  if last-of(question.seq)
   then newLine(1).
  tCnt = tCnt + 1.
 end.
 if tCnt > 0 
  then return.
 newLine(1).
 setFont("Helvetica", 12.0).
 textAt("None", 14).
 newLine(1).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-doSummary) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doSummary Procedure 
PROCEDURE doSummary PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var tAuditScore as char.

 publish "GetAuditAnswer" ("Score", output tAuditScore).

 blockTitle("AUDIT DETAILS").
 
 textColor(0.0, 0.0, 0.0).
 setFont("Helvetica-BoldOblique", 12.0).
 textAt (tName, 14).
 setFont("Helvetica", 12.0).
 newLine(1).
 textAt (tAddr, 14).
 newLine(1).       
 textAt (tCity + (if tCity > "" then ", " else "") + tState + "  " + tZip, 14).
 newLine(1).
 newLine(1).
 newLine(1).
 
 setFont("Helvetica-Bold", 12.0).
 if tAuditDate = ? then
 do:
   textAt ("Audit Date: ", 100).
   newLine(1).
 end.
 else
 do:
   textAt ("Audit Date: " + string(tAuditDate), 100).
   newLine(1).
 end.
 setFont("Helvetica", 12.0).
 textAt ("Agency Contact: " + tContactName, 14).
 setFont("Helvetica-Bold", 12.0).
 textAt ("Audit Score: " + tAuditScore, 100).
 newLine(1).
 
 setFont("Helvetica", 12.0).
 textAt ("Auditor: " + tAuditor, 14).
 setFont("Helvetica-Bold", 12.0).
 textAt ("Agent Ranking: " + string(tRanking), 100).
 newLine(2).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getFilename) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getFilename Procedure 
PROCEDURE getFilename PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tFile as char.
 def var doSave as logical init true.
 def var tReportDir as char.

 publish "GetReportDir" (output tReportDir).
 publish "GetFilenameFormatted" ("questionnaireReport", output activeFilename).
       
  if taudittype = "E" then
   activeFilename = replace(activeFilename,"QAR","ERR").
   
  if taudittype = "T" then
   activeFilename = replace(activeFilename,"QAR","TOR").
   
 system-dialog get-file activeFilename
  filters "PDF Files" "*.pdf"
  initial-dir tReportDir
  ask-overwrite
  create-test-file
  default-extension ".pdf"
  use-filename
  save-as
   update doSave.

 if not doSave 
  then return error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reportFooter) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reportFooter Procedure 
PROCEDURE reportFooter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/rpt-ftr.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reportHeader) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reportHeader Procedure 
PROCEDURE reportHeader :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tTitle as char no-undo.
 if taudittype = "E" then
   tTitle = "ESCROW RECONCILIATION REVIEW - QUESTIONNAIRE".
 else if taudittype = "T" then
   tTitle = "TITLE ONLY REVIEW - QUESTIONNAIRE".
 else
   tTitle = "QUALITY ASSURANCE REVIEW - QUESTIONNAIRE".
 {lib/rpt-hdr.i &title=tTitle &title-x=260 &title-y=740}
 newLine(1).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

