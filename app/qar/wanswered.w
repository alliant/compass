&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wunanswered.w
    Modification:
    Date          Name      Description
    02/06/2017    AG        Changed to implement sorting in browse.
    09/11/2017    AG        Alternate color code in browse.
    09/24/2021    SA        Task#:86696 Defects raised by david
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

{lib/std-def.i}

{tt/qarauditquestion.i}

def var hData as handle no-undo.
def var iBgcolor as int no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwQuestions

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES question

/* Definitions for BROWSE brwQuestions                                  */
&Scoped-define FIELDS-IN-QUERY-brwQuestions question.questionID question.fileNumber question.description question.answer   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQuestions   
&Scoped-define SELF-NAME brwQuestions
&Scoped-define QUERY-STRING-brwQuestions preselect EACH question by question.seq                                           by question.fileID
&Scoped-define OPEN-QUERY-brwQuestions OPEN QUERY {&SELF-NAME} preselect EACH question by question.seq                                           by question.fileID.
&Scoped-define TABLES-IN-QUERY-brwQuestions question
&Scoped-define FIRST-TABLE-IN-QUERY-brwQuestions question


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwQuestions}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bExport tAuto brwQuestions bRefresh 
&Scoped-Define DISPLAYED-OBJECTS tAuto 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshBrowse C-Win 
FUNCTION refreshBrowse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE VARIABLE tAuto AS LOGICAL INITIAL no 
     LABEL "Auto Refresh" 
     VIEW-AS TOGGLE-BOX
     SIZE 17 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwQuestions FOR 
      question SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwQuestions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQuestions C-Win _FREEFORM
  QUERY brwQuestions DISPLAY
      question.questionID label "Question" format "x(15)"
 question.fileNumber label "File" format "x(30)"
 question.description label "Description" format "x(250)" width 50
 question.answer label "Answer" format "x(500)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 110 BY 13.33 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 1.1 COL 2 WIDGET-ID 6 NO-TAB-STOP 
     tAuto AT ROW 1.52 COL 17.4 WIDGET-ID 2
     brwQuestions AT ROW 2.91 COL 2 WIDGET-ID 200
     bRefresh AT ROW 1.1 COL 9.4 WIDGET-ID 8 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 112 BY 15.38 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Answered Questions"
         HEIGHT             = 15.38
         WIDTH              = 112
         MAX-HEIGHT         = 16.86
         MAX-WIDTH          = 112
         VIRTUAL-HEIGHT     = 16.86
         VIRTUAL-WIDTH      = 112
         SHOW-IN-TASKBAR    = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwQuestions tAuto fMain */
ASSIGN 
       brwQuestions:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwQuestions:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQuestions
/* Query rebuild information for BROWSE brwQuestions
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} preselect EACH question by question.seq
                                          by question.fileID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQuestions */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Answered Questions */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Answered Questions */
DO:
  /* This event will close the window and terminate the procedure.  */
 run hideWindow.
/*   APPLY "CLOSE":U TO THIS-PROCEDURE. */
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Answered Questions */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  refreshBrowse().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQuestions
&Scoped-define SELF-NAME brwQuestions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQuestions C-Win
ON DEFAULT-ACTION OF brwQuestions IN FRAME fMain
DO:
  if available question
   then publish "SelectAuditPage" (integer(entry(1, question.questionID, "."))).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQuestions C-Win
ON ROW-DISPLAY OF brwQuestions IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
  if current-result-row("brwQuestions") modulo 2 = 0 then
    iBgColor = 17.
  else
    iBgColor = 15.
  question.questionID :bgcolor in browse brwQuestions = iBgColor.
  question.fileNumber :bgcolor in browse brwQuestions = iBgColor.
  question.description :bgcolor in browse brwQuestions = iBgColor.
  question.answer :bgcolor in browse brwQuestions = iBgColor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQuestions C-Win
ON START-SEARCH OF brwQuestions IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAuto
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAuto C-Win
ON VALUE-CHANGED OF tAuto IN FRAME fMain /* Auto Refresh */
DO:
  if self:checked 
   then 
    do: subscribe to "AuditChanged" anywhere.
        bRefresh:sensitive in frame fMain = false.
    end.
   else 
     do: unsubscribe to "AuditChanged".
         bRefresh:sensitive in frame fMain = true.
     end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

PAUSE 0 BEFORE-HIDE.

bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").

subscribe to "AuditOpened" anywhere.
subscribe to "AuditClosed" anywhere.
refreshBrowse().
{lib/win-main.i}

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditChanged C-Win 
PROCEDURE AuditChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 refreshBrowse().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditClosed C-Win 
PROCEDURE AuditClosed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 refreshBrowse().
 run hideWindow.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditOpened C-Win 
PROCEDURE AuditOpened :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 refreshBrowse().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tAuto 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bExport tAuto brwQuestions bRefresh 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var th as handle no-undo.

 if query brwQuestions:num-results = 0 
  then
   do: 
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.

 publish "GetReportDir" (output std-ch).
 
 th = temp-table question:handle.
 
 run util/exporttable.p (table-handle th,
                         "question",
                         "for each question ",
                         "questionID,fileNumber,description,answer",
                         "Question,File,Description,Answer",
                         std-ch,
                         "AnsweredQuestions.csv",
                         true,
                         output std-ch,
                         output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hideWindow C-Win 
PROCEDURE hideWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:visible = false.
 publish "WindowClosed" (input "Answered").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 refreshBrowse().
 {&window-name}:visible = true.
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i &pre-by-clause="(if pName = 'questionID' 
                                      then ' by seq ' + (if dataSortDesc then ' descending' else '')
                                      else '') + "}  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

 /* fMain components */
 brwQuestions:width-pixels = frame fmain:width-pixels - 10.
 brwQuestions:height-pixels = frame fMain:height-pixels - 43.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshBrowse C-Win 
FUNCTION refreshBrowse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 close query brwQuestions.
 publish "GetQuestionsAnswered" (output table question).

 std-in = 0.
 for each question:
  std-in = std-in + 1.
  question.answer = replace(question.answer, "&br;", " ").
 end.
 std-ch = string(std-in) + " answered questions".
 status default std-ch in window {&window-name}.
 status input std-ch in window {&window-name}.

 open query brwQuestions preselect each question by question.seq by question.fileID.

 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

