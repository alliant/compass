20000 "1.1" "Current remittances" 0
20005 "1.1a" "Placeholder (Net)" 0
20007 "1.1b" "Placeholder (Year)" 0
20010 "1.1.1" "Is the agent remitting the minimum amount to remain an agent ($7,500)?" 0
20015 "1.2" "Brief list of claims in process:" 0
20020 "1.2.1" "Placeholder (Claims)" 0
20035 "1.3" "Last audit score. (ANTIC QA Manager)" 0
20040 "1.4" "Are there any open corrective actions since the last audit? (ANTIC QA Manager)" 0
20045 "1.4.1" "Placeholder (Corrective Actions)" 0
20050 "1.5" "Over the last 90 days, has the agent been slow in remitting (payment of statements)?" 0
20055 "1.6" "Has there been a greater than 20% drop in remittances over the last 12 months?" 0
20060 "1.6.1" "If there has been a 20% drop, what is driving the change?" 0
20065 "1.7" "Is the agent calculating premiums accurately?" 0
20070 "1.8" "What is the agency policy issuing limit amount?" 0
20085 "1.9" "Is the policy reported and premium remitted in accordance with the Agency Agreement and state regulations?" 85
20090 "1.10" "What is the typical time 'gap' between the effective date of the policy and the NOC processing date?" 84
20095 "1.10.1" "Is the agent remitting in a timely fashion?" 0
20100 "1.11" "Is there a current Agency Agreement in place?" 0
20105 "1.12" "Does the Agency Agreement require that the agent carry E&O or Professional Liability insurance?" 85
20110 "1.13" "Placeholder (Comments)" 0
21000 "1.1" "Any change of ownership since the last Alliant National audit?" 0
21005 "1.2" "Has there been any significant change in the key personnel in the agency?" 0
21010 "1.3" "Any change in insurance coverage (E&O, Bond, and Professional Liability)?" 0
21015 "1.4" "Describe the level of business over the last 12 months:" 0
21020 "1.4.1" "Placeholder" 0
21025 "1.5" "Any change in the financial position of the company?" 0
21030 "1.5.1" "Placeholder" 0
21035 "1.6" "Has the agency been recently terminated by another underwriter?" 0
21040 "1.6.1" "Placeholder" 0
21045 "1.7" "Has the agency entered into any affiliated business arrangements since the last audit?" 0
21050 "1.7.1" "Placeholder" 0
21055 "1.8" "What is the current employee turnover rate over the last 12 months?" 0
21060 "1.8.1" "Placeholder" 0
21065 "1.9" "Do you use employees from temporary agencies?" 0
21070 "1.9.1" "In what capacity?" 0
21075 "1.10" "Current average Orders per month (last 6 months)?" 0
21080 "1.11" "Current average Closings per month (last 6 months)?" 0
21085 "1.12" "Choose which of the following best identifies the agent's procedures for an outgoing wire:" 0
21090 "1.13" "Are underwriter premiums held in a separate account or amount identified (not commingled w/operating accounts)?" 90
21095 "1.14" "Are signed waivers regarding the FDIC insurance limits obtained for individual investment accounts?" 90
21100 "1.15" "Does the agent have an up to date ANTIC rate manual?" 0
21105 "1.16" "How are the rates maintained and updated in the computer system?" 0
21110 "1.16.1" "Is there evidence that shows that rates are being updated?" 0
21115 "1.17" "Does the agent have an established QC program that includes a regular review of working files, compared against UW and internal closing practices?" 89
21120 "1.18.1" "Who provides the title information?" 0
21125 "1.18.2" "Do they have E&O insurance?" 0
21130 "1.18.3" "Are they recognized by Alliant National as a reliable source?" 0
22000 "1.1" "Please provide a brief history of this agency:" 0
22005 "1.1.1" "In rough terms, what is the percentage of your business in the following categories?" 0
22010 "1.1.2" "Placeholder" 0
22015 "1.1.3" "Placeholder" 0
22020 "1.1.4" "Placeholder" 0
22025 "1.1.5" "Placeholder" 0
22030 "1.1.6" "Placeholder" 0
22035 "1.1.7" "Placeholder" 0
22040 "1.2" "Total number of company employees" 0
22045 "1.3" "Title production software" 0
22050 "1.4" "Escrow software" 0
22055 "2.1" "List all accounts (escrow/trust and related investment accounts) held by the company" 0
22060 "2.2" "Current Bank Statements (3 months) for each escrow/trust account listed in 2.1" 0
22065 "2.3" "Corresponding bank account reconciliations (last 3 months)" 0
22070 "2.4" "List of all Escrow Agents (or licensed employees) by location (include license numbers and license renewal dates)" 91
22075 "2.5" "Copy of the agency's state license" 0
22080 "2.6" "Copy of the most recent state audit (TDI, etc.)" 0
22085 "2.6.1" "Copy of the agent's response to the most recent state audit." 0
22090 "2.7" "Copy of the agent's Employee Privacy Plan" 0
22095 "2.8" "Copy of E&O Insurance (showing ANTIC as a certificate holder)" 0
22100 "2.9" "Copy of Fidelity Bond" 0
22105 "2.10" "Copy of Surety Bond" 0
22110 "2.11" "Copy of Policy Log/Register/Report (report should include current inventory, stock on hand, and policies issued)" 92
22115 "2.11.1" "Includes list Remittances sent, but not as yet received at ANTIC NOC" 0
22120 "2.11.2" "Includes Policies prepared but not remitted" 0
22125 "2.11.3" "Are any outstanding Remittances due to Alliant National" 0
22130 "2.12" "Copy of State Compliance Report (NM only)" 0
22135 "2.13" "Copy of agent's current fee filings (CO only)" 0
22140 "2.14" "Provide a list of all ABAs associated with this agency" 0
22145 "2.15" "Be able to show the auditors any written procedures, process flow charts or operations manuals developed by the agency" 88
.