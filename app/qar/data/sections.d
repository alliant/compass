"1" "Overview" "Does the Agency appear to be run professionally and staffed adequately?" no 0
"2" "General" "Is there a formal system or process in place that establishes and maintains control of the agency's general administration and operations?" no 1.0
"3" "Title Search" "Is there a system or process in place that assures a timely, effective and accurate title search and examination?" yes 1.0
"4" "Closing" "Is there a system or process in place that assures that all files are settled and all transactions are closed properly?" yes 2.0
"5" "Post Closing" "Is there a system or process in place that assures that all post closing activities are performed correctly?" yes 3.0
"6" "Escrow" "Is there a system or process in place that assures that all escrow accounts are reconciled on a monthly basis?" no 3.0
"7" "Policy Production" "Is there a system or process in place that assures that all policy generation and underwriting is being performed adequately?" yes 2.0
.
