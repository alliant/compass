&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* werr.w
------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
    File        : werr.w
    Description : Show agents bank account detail
    Modification:
    Date          Name      Description                       
    12/29/2016    AC        Handle save button enabe in case of any change                               
    02/07/2017    AG        Modified to implement sorting in browse
    02/10/2017    AG        Fixed the screen-values when audit is closed or opened.  
    09/11/2017    AG        Alternate color code in browse.
    06/17/2022    Shefali   Task #93436 -- Modified to save "ReviewBy" and "ReviewDate" 
                            on value-change of the fill-ins.
  ----------------------------------------------------------------------*/

CREATE WIDGET-POOL.

def var tOk as logical.
define variable pAuditStatus as character no-undo.
def var iBgColor as int no-undo.
{lib/std-def.i}
{tt/qaraccount.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwAccounts

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES account

/* Definitions for BROWSE brwAccounts                                   */
&Scoped-define FIELDS-IN-QUERY-brwAccounts account.bankName account.acctNumber account.reconDate account.calcBal account.actualCheckbookBal account.trialBal account.reconciled   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAccounts   
&Scoped-define SELF-NAME brwAccounts
&Scoped-define QUERY-STRING-brwAccounts for each account by account.bankName                                          by account.acctNumber                                          by account.reconDate
&Scoped-define OPEN-QUERY-brwAccounts OPEN QUERY {&SELF-NAME} for each account by account.bankName                                          by account.acctNumber                                          by account.reconDate.
&Scoped-define TABLES-IN-QUERY-brwAccounts account
&Scoped-define FIRST-TABLE-IN-QUERY-brwAccounts account


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwAccounts}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tReviewer tReviewDate brwAccounts bAdd 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshBrowse C-Win 
FUNCTION refreshBrowse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAdd 
     LABEL "Add..." 
     SIZE 11.8 BY 1.14.

DEFINE BUTTON bCopy 
     LABEL "Copy..." 
     SIZE 11.8 BY 1.14.

DEFINE BUTTON bDelete 
     LABEL "Delete" 
     SIZE 12.2 BY 1.14.

DEFINE BUTTON bModify 
     LABEL "Modify..." 
     SIZE 11.8 BY 1.14.

DEFINE VARIABLE tAddr AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 40 BY 1 NO-UNDO.

DEFINE VARIABLE tAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 40 BY 1 NO-UNDO.

DEFINE VARIABLE tReviewDate AS DATE FORMAT "99/99/99":U 
     LABEL "Review Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tReviewer AS CHARACTER FORMAT "X(256)":U 
     LABEL "Reviewed By" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-35
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 122 BY 14.05.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAccounts FOR 
      account SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAccounts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAccounts C-Win _FREEFORM
  QUERY brwAccounts DISPLAY
      account.bankName label "Bank" format "x(30)"
 account.acctNumber label "Account" format "x(15)"
 account.reconDate label "Reconciled" format "99/99/9999"
 account.calcBal label "Calc Bank Bal" format "-zz,zzz,zz9"
 account.actualCheckbookBal label "Checkbook" format "-zz,zzz,zz9"
 account.trialBal label "Trial Bal" format "-zz,zzz,zz9"
 account.reconciled label "3-W" format "/No"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 107 BY 12.86 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tAgent AT ROW 1.95 COL 15 COLON-ALIGNED WIDGET-ID 20 NO-TAB-STOP 
     tReviewer AT ROW 1.95 COL 89 COLON-ALIGNED WIDGET-ID 24
     tAddr AT ROW 3.14 COL 15 COLON-ALIGNED NO-LABEL WIDGET-ID 22 NO-TAB-STOP 
     tReviewDate AT ROW 3.14 COL 89 COLON-ALIGNED WIDGET-ID 26
     brwAccounts AT ROW 6 COL 4 WIDGET-ID 200
     bAdd AT ROW 6 COL 112 WIDGET-ID 6
     bModify AT ROW 7.43 COL 112 WIDGET-ID 10
     bCopy AT ROW 8.86 COL 112 WIDGET-ID 12
     bDelete AT ROW 10.29 COL 112 WIDGET-ID 8
     "Bank Account Reconciliations" VIEW-AS TEXT
          SIZE 35 BY .62 AT ROW 5.05 COL 4 WIDGET-ID 18
          FONT 6
     RECT-35 AT ROW 5.29 COL 3 WIDGET-ID 16
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 125.2 BY 18.57 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Escrow Reconciliation Review"
         HEIGHT             = 18.57
         WIDTH              = 125.2
         MAX-HEIGHT         = 35.14
         MAX-WIDTH          = 256
         VIRTUAL-HEIGHT     = 35.14
         VIRTUAL-WIDTH      = 256
         SHOW-IN-TASKBAR    = no
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwAccounts tReviewDate fMain */
/* SETTINGS FOR BUTTON bCopy IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDelete IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bModify IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwAccounts:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwAccounts:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

/* SETTINGS FOR RECTANGLE RECT-35 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAddr IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       tAddr:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tAgent IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       tAgent:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tReviewDate IN FRAME fMain
   NO-DISPLAY                                                           */
/* SETTINGS FOR FILL-IN tReviewer IN FRAME fMain
   NO-DISPLAY                                                           */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAccounts
/* Query rebuild information for BROWSE brwAccounts
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} for each account by account.bankName
                                         by account.acctNumber
                                         by account.reconDate.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAccounts */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Escrow Reconciliation Review */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Escrow Reconciliation Review */
DO:
  /* This event will close the window and terminate the procedure.  */
  run hideWindow.
/*   APPLY "CLOSE":U TO THIS-PROCEDURE. */
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAdd C-Win
ON CHOOSE OF bAdd IN FRAME fMain /* Add... */
DO:
  run doAdd.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCopy C-Win
ON CHOOSE OF bCopy IN FRAME fMain /* Copy... */
DO:
  if not available account
   then return.
   run doCopy.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME fMain /* Delete */
DO:
  if not available account 
   then 
    do: bDelete:sensitive in frame fMain = false.
        return.
    end.

  tOk = false.
  MESSAGE "Account" account.bankName "will be removed."
    VIEW-AS ALERT-BOX warning BUTTONS ok-cancel update tOk.
   if tOk
    then 
     do: 
       publish "DeleteAccount" (account.bankID).
       DELETE account.
       brwAccounts:DELETE-SELECTED-ROWS().
       refreshBrowse().
     end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME fMain /* Modify... */
DO:
  if not available account 
   then return.
  run doModify.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAccounts
&Scoped-define SELF-NAME brwAccounts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAccounts C-Win
ON DEFAULT-ACTION OF brwAccounts IN FRAME fMain
DO:
  if available account
   then run doModify.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAccounts C-Win
ON ROW-DISPLAY OF brwAccounts IN FRAME fMain
DO:
  
  if current-result-row("brwAccounts") modulo 2 = 0 then
    iBgColor = 17.
  else
    iBgColor = 15.
  account.bankName:bgcolor in browse brwAccounts           = iBgColor.
  account.acctNumber:bgcolor in browse brwAccounts         = iBgColor.
  account.reconDate:bgcolor in browse brwAccounts          = iBgColor.
  account.calcBal:bgcolor in browse brwAccounts            = iBgColor.
  account.actualCheckbookBal:bgcolor in browse brwAccounts = iBgColor.
  account.trialBal:bgcolor in browse brwAccounts           = iBgColor.
  account.reconciled:bgcolor in browse brwAccounts         = iBgColor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAccounts C-Win
ON START-SEARCH OF brwAccounts IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tReviewDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tReviewDate C-Win
ON VALUE-CHANGED OF tReviewDate IN FRAME fMain /* Review Date */
DO:
  {lib/setattrda.i escrowReviewDate date(self:screen-value)}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tReviewer
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tReviewer C-Win
ON VALUE-CHANGED OF tReviewer IN FRAME fMain /* Reviewed By */
DO:
  {lib/setattrch.i escrowReviewer self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.
/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "AuditCopied" anywhere.
subscribe to "AuditOpened" anywhere.
subscribe to "AuditClosed" anywhere.
refreshBrowse().

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  publish "GetAuditStatus" (output pAuditStatus).
  if pAuditStatus = "C" then
   run AuditReadOnly.
    on value-changed anywhere
     publish "enableSave" .
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditChanged C-Win 
PROCEDURE AuditChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pNew as logical.

 refreshBrowse().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditClosed C-Win 
PROCEDURE AuditClosed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
do with frame fMain:
end.
refreshBrowse().

std-ch = "".
assign
  tAgent:screen-value = ""
  tAddr:screen-value = ""
  tReviewer:screen-value = ""
  tReviewDate:screen-value = ""
  tReviewer:sensitive = false
  tReviewDate:sensitive = false
  bAdd:sensitive = false .


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditCopied C-Win 
PROCEDURE AuditCopied :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
refreshBrowse(). 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditOpened C-Win 
PROCEDURE AuditOpened :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 refreshBrowse().
 publish "GetAuditStatus" (output pAuditStatus).
 if pAuditStatus = "C"then
  run AuditReadOnly.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditReadOnly C-Win 
PROCEDURE AuditReadOnly :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*-------------not used-------------*/
/*do with frame fMain:
end.
assign
tReviewer:sensitive = false
tReviewDate:sensitive = false
bAdd:sensitive = false
bModify:sensitive = false
bCopy:sensitive = false
bDelete:sensitive = false.*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doAdd C-Win 
PROCEDURE doAdd :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var pBankName as char.
def var pAcctTitle as char.
def var pAcctNumber as char.
def var pReconDate as date init today.
def var pReconCurrent as logical.

def var pReconAdjBankBal as deci.
def var pReconCheckbookBal as deci.
def var pReconTrialBal as deci.

def var pStmtBal as deci.
def var pDipBal as deci.
def var pOutChecksBal as deci.
def var pNetAdj as deci.
def var pNetNegAdj as deci.
def var pCalcBal as deci.
def var pActualCheckbookBal as deci.
def var pTrialBal as deci.

def var pReconciled as logical.
def var pComments as char.

  run dialogupdacct.w ("Add",
                      input-output pBankName,
                      input-output pAcctTitle,
                      input-output pAcctNumber,
                      input-output pReconDate,
                      input-output pReconCurrent,
                      input-output pReconAdjBankBal,
                      input-output pReconCheckbookBal,
                      input-output pReconTrialBal,
                      input-output pStmtBal,
                      input-output pDipBal,
                      input-output pOutChecksBal,
                      input-output pNetAdj,
                      input-output pNetNegAdj,
                      input-output pCalcBal,
                      input-output pActualCheckbookBal,
                      input-output pTrialBal,
                      input-output pReconciled,
                      input-output pComments,
                      output std-lo).

                                                                                                             

  if not std-lo 
   then return.
  publish "AddAccount" (0,
                        pBankName,
                        pAcctTitle,
                        pAcctNumber,
                        pReconDate,
                        pReconCurrent,
                        pReconAdjBankBal,
                        pReconCheckbookBal,
                        pReconTrialBal,
                        pStmtBal,
                        pDipBal,
                        pOutChecksBal,
                        pNetAdj,
                        pNetNegAdj,
                        pCalcBal,
                        pActualCheckbookBal,
                        pTrialBal,
                        pReconciled,
                        pComments).

refreshBrowse().  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doCopy C-Win 
PROCEDURE doCopy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var pBankName as char.
def var pAcctTitle as char.
def var pAcctNumber as char.
def var pReconDate as date init today.
def var pReconCurrent as logical.

def var pReconAdjBankBal as deci.
def var pReconCheckbookBal as deci.
def var pReconTrialBal as deci.

def var pStmtBal as deci.
def var pDipBal as deci.
def var pOutChecksBal as deci.
def var pNetAdj as deci.
def var pNetNegAdj as deci.
def var pCalcBal as deci.
def var pActualCheckbookBal as deci.
def var pTrialBal as deci.

def var pReconciled as logical.
def var pComments as char.

  
assign
  pBankName = account.bankName
  pAcctTitle = account.acctTitle
  pAcctNumber = account.acctNumber
  pReconDate = account.reconDate
  pReconCurrent = account.reconCurrent
  pReconAdjBankBal = account.reconAdjBankBal
  pReconCheckbookBal = account.reconCheckbookBal
  pReconTrialBal = account.reconTrialBal
  pStmtBal = account.StmtBal
  pDipBal = account.dipBal
  pOutChecksBal = account.outChecksBal
  pNetAdj = account.netAdj
  pNetNegAdj = account.netNegAdj
  pCalcBal = account.calcBal
  pActualCheckbookBal = account.actualCheckbookBal
  pTrialBal = account.trialBal
  pReconciled = account.reconciled
  pComments = account.comments
  .
run dialogupdacct.w ("Add",
                      input-output pBankName,
                      input-output pAcctTitle,
                      input-output pAcctNumber,
                      input-output pReconDate,
                      input-output pReconCurrent,
                      input-output pReconAdjBankBal,
                      input-output pReconCheckbookBal,
                      input-output pReconTrialBal,
                      input-output pStmtBal,
                      input-output pDipBal,
                      input-output pOutChecksBal,
                      input-output pNetAdj,
                      input-output pNetNegAdj,
                      input-output pCalcBal,
                      input-output pActualCheckbookBal,
                      input-output pTrialBal,
                      input-output pReconciled,
                      input-output pComments,
                      output std-lo).
  if not std-lo 
   then return.
  publish "AddAccount" (0,
                        pBankName,
                        pAcctTitle,
                        pAcctNumber,
                        pReconDate,
                        pReconCurrent,
                        pReconAdjBankBal,
                        pReconCheckbookBal,
                        pReconTrialBal,
                        pStmtBal,
                        pDipBal,
                        pOutChecksBal,
                        pNetAdj,
                        pNetNegAdj,
                        pCalcBal,
                        pActualCheckbookBal,
                        pTrialBal,
                        pReconciled,
                        pComments).
  refreshBrowse().  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doModify C-Win 
PROCEDURE doModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var pBankName as char.
def var pAcctTitle as char.
def var pAcctNumber as char.
def var pReconDate as date init today.
def var pReconCurrent as logical.

def var pReconAdjBankBal as deci.
def var pReconCheckbookBal as deci.
def var pReconTrialBal as deci.

def var pStmtBal as deci.
def var pDipBal as deci.
def var pOutChecksBal as deci.
def var pNetAdj as deci.
def var pNetNegAdj as deci.
def var pCalcBal as deci.
def var pActualCheckbookBal as deci.
def var pTrialBal as deci.

def var pReconciled as logical.
def var pComments as char.

  
assign
  pBankName = account.bankName
  pAcctTitle = account.acctTitle
  pAcctNumber = account.acctNumber
  pReconDate = account.reconDate
  pReconCurrent = account.reconCurrent
  pReconAdjBankBal = account.reconAdjBankBal
  pReconCheckbookBal = account.reconCheckbookBal
  pReconTrialBal = account.reconTrialBal
  pStmtBal = account.StmtBal
  pDipBal = account.dipBal
  pOutChecksBal = account.outChecksBal
  pNetAdj = account.netAdj
  pNetNegAdj = account.netNegAdj
  pCalcBal = account.calcBal
  pActualCheckbookBal = account.actualCheckbookBal
  pTrialBal = account.trialBal
  pReconciled = account.reconciled
  pComments = account.comments
  .
run dialogupdacct.w ("Modify",
                      input-output pBankName,
                      input-output pAcctTitle,
                      input-output pAcctNumber,
                      input-output pReconDate,
                      input-output pReconCurrent,
                      input-output pReconAdjBankBal,
                      input-output pReconCheckbookBal,
                      input-output pReconTrialBal,
                      input-output pStmtBal,
                      input-output pDipBal,
                      input-output pOutChecksBal,
                      input-output pNetAdj,
                      input-output pNetNegAdj,
                      input-output pCalcBal,
                      input-output pActualCheckbookBal,
                      input-output pTrialBal,
                      input-output pReconciled,
                      input-output pComments,
                      output std-lo).
  if not std-lo 
   then return.
  publish "AddAccount" (account.bankID,
                        pBankName,
                        pAcctTitle,
                        pAcctNumber,
                        pReconDate,
                        pReconCurrent,
                        pReconAdjBankBal,
                        pReconCheckbookBal,
                        pReconTrialBal,
                        pStmtBal,
                        pDipBal,
                        pOutChecksBal,
                        pNetAdj,
                        pNetNegAdj,
                        pCalcBal,
                        pActualCheckbookBal,
                        pTrialBal,
                        pReconciled,
                        pComments).
  refreshBrowse().  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE tReviewer tReviewDate brwAccounts bAdd 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hideWindow C-Win 
PROCEDURE hideWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:visible = false.
 publish "WindowClosed" (input "Escrow").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LeaveAll C-Win 
PROCEDURE LeaveAll :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 apply "leave" to tReviewer in frame {&frame-name}.
 apply "leave" to tReviewDate in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 refreshBrowse().
 {&window-name}:visible = true.
 {&window-name}:move-to-top().
 publish "WindowOpened" (input "Escrow").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i }
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshBrowse C-Win 
FUNCTION refreshBrowse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 {lib/getattrch.i escrowReviewer std-ch}
 tReviewer:screen-value in frame fMain = std-ch.
 {lib/getattrda.i escrowReviewDate std-da}
 tReviewDate:screen-value = string(std-da).

 {lib/getattrch.i name std-ch}
 tAgent:screen-value = std-ch.
 {lib/getattrch.i city tAddr}
 {lib/getattrch.i state std-ch}
 tAddr = tAddr + (if tAddr > "" then ", " else "") + std-ch + " ".
 {lib/getattrch.i zip std-ch}
 tAddr = tAddr + std-ch.
 tAddr:screen-value = tAddr.

 close query brwAccounts.
 publish "GetAccounts" (output table account).

 open query brwAccounts for each account
                           by account.bankName
                           by account.acctNumber
                           by account.reconDate.
 assign
   bModify:sensitive in frame fMain = available account
   bCopy:sensitive = available account
   bDelete:sensitive = available account
   bAdd:sensitive = true
   tReviewer:sensitive = true
   tReviewDate:sensitive = true
   bAdd:sensitive = true.

 publish "SaveAfterEveryAnswer".
 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

