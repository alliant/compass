&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Update-File
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Update-File 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
  
  Modification:
  Date          Name      Description
  12/06/2016    AC        Fixed tab order on this screen.
  02/14/2017    AC        Implement local inspect functionality.
------------------------------------------------------------------------*/

def input parameter pType as char.

def input-output parameter pFileNumber as char.
def input-output parameter pstate as char.
def input-output parameter pcounty as char.
def input-output parameter pownerLiability as decimal.
def input-output parameter pownerPolicy as char.
def input-output parameter plender1Liability as decimal.
def input-output parameter plender1Policy as char.
def input-output parameter plender2Liability as decimal.
def input-output parameter plender2Policy as char.
def input-output parameter ppremiumStatement as decimal.
def input-output parameter prate as decimal.
def input-output parameter pdelta as decimal.
def input-output parameter porigSearchDate as date.
def input-output parameter ppreliminaryDate as date.
def input-output parameter pupdateDate as date.
def input-output parameter pfinalDate as date.
def input-output parameter pclosingDate as date.
def input-output parameter pfundingDate as date.
def input-output parameter precordingDate as date.
def input-output parameter pOwnerDeliveryDate as date.
def input-output parameter pLender1DeliveryDate as date.
def input-output parameter pLender2DeliveryDate as date.

def output parameter pCancelled as logical init true.

define variable pAuditStatus as character no-undo.

{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Update-File

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tFileNumber tSearchGap tCounty tState ~
tOwnerPolicy tOwnerLiability tLender1Policy tLender1Liability ~
tLender2Policy tLender2Liability tPremiumStatement tRate tOrigSearchDate ~
tPreliminaryDate tUpdateDate tFinalDate tClosingDate tFundingDate ~
tRecordingDate tOwnerDeliveryDate tLender1DeliveryDate tLender2DeliveryDate ~
tTypeofGap Btn_OK tFundingGap tRecordingGap tDeliveryOwnerGap ~
tDeliveryLenderGap tDeliveryLender2Gap 
&Scoped-Define DISPLAYED-OBJECTS tFileNumber tCounty tState tOwnerPolicy ~
tOwnerLiability tLender1Policy tLender1Liability tLender2Policy ~
tLender2Liability tPremiumStatement tRate tOrigSearchDate tPreliminaryDate ~
tUpdateDate tFinalDate tClosingDate tFundingDate tRecordingDate ~
tOwnerDeliveryDate tLender1DeliveryDate tLender2DeliveryDate tTypeofGap 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayGaps Update-File 
FUNCTION displayGaps RETURNS LOGICAL PRIVATE
  ( pType as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tClosingDate AS DATE FORMAT "99/99/99":U 
     LABEL "Closing" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tCounty AS CHARACTER FORMAT "X(30)":U 
     LABEL "County" 
     VIEW-AS FILL-IN 
     SIZE 33 BY 1 TOOLTIP "Enter the county where the property exists" NO-UNDO.

DEFINE VARIABLE tDeliveryLender2Gap AS INTEGER FORMAT "zzzz":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 TOOLTIP "Funding to Delivery Gap" NO-UNDO.

DEFINE VARIABLE tDeliveryLenderGap AS INTEGER FORMAT "zzzz":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 TOOLTIP "Funding to Delivery Gap" NO-UNDO.

DEFINE VARIABLE tDeliveryOwnerGap AS INTEGER FORMAT "zzzz":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 TOOLTIP "Funding to Delivery Gap" NO-UNDO.

DEFINE VARIABLE tFileNumber AS CHARACTER FORMAT "X(40)":U 
     LABEL "File" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 TOOLTIP "The agent's file identification number"
     FONT 6 NO-UNDO.

DEFINE VARIABLE tFinalDate AS DATE FORMAT "99/99/99":U 
     LABEL "Final Commitment" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tFundingDate AS DATE FORMAT "99/99/99":U 
     LABEL "Funding" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tFundingGap AS INTEGER FORMAT "zzzz":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 TOOLTIP "Closing to Funding Gap" NO-UNDO.

DEFINE VARIABLE tLender1DeliveryDate AS DATE FORMAT "99/99/99":U 
     LABEL "Delivered to Lender" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tLender1Liability AS DECIMAL FORMAT ">>>,>>>,>>9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tLender1Policy AS CHARACTER FORMAT "x(10)":U 
     LABEL "Lender" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tLender2DeliveryDate AS DATE FORMAT "99/99/99":U 
     LABEL "Delivered to Lender 2" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tLender2Liability AS DECIMAL FORMAT ">>>,>>>,>>9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tLender2Policy AS CHARACTER FORMAT "x(10)":U 
     LABEL "Lender 2" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tOrigSearchDate AS DATE FORMAT "99/99/99":U 
     LABEL "Original Search" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tOwnerDeliveryDate AS DATE FORMAT "99/99/99":U 
     LABEL "Delivered to Owner" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tOwnerLiability AS DECIMAL FORMAT ">>>,>>>,>>9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tOwnerPolicy AS CHARACTER FORMAT "x(10)":U 
     LABEL "Owner" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tPreliminaryDate AS DATE FORMAT "99/99/99":U 
     LABEL "Preliminary Commitment" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tPremiumStatement AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "Statement" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tRate AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "Rate" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tRecordingDate AS DATE FORMAT "99/99/99":U 
     LABEL "Recording" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tRecordingGap AS INTEGER FORMAT "zzzz":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 TOOLTIP "Funding to Recording Gap" NO-UNDO.

DEFINE VARIABLE tSearchGap AS INTEGER FORMAT "zzzz":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 TOOLTIP "Update to Closing Gap" NO-UNDO.

DEFINE VARIABLE tState AS CHARACTER FORMAT "!!":U 
     LABEL "State" 
     VIEW-AS FILL-IN 
     SIZE 5 BY 1 TOOLTIP "Enter the state where the property exists" NO-UNDO.

DEFINE VARIABLE tUpdateDate AS DATE FORMAT "99/99/99":U 
     LABEL "Updated" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tTypeofGap AS INTEGER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Calendar days", 1,
"Work days", 2
     SIZE 17 BY 1.91 TOOLTIP "Select the type of Gap days to display" NO-UNDO.

DEFINE RECTANGLE RECT-29
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 55 BY 5.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 55 BY 2.86.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Update-File
     tFileNumber AT ROW 1.48 COL 9 COLON-ALIGNED WIDGET-ID 2
     tSearchGap AT ROW 6.29 COL 105.8 COLON-ALIGNED NO-LABEL WIDGET-ID 58 NO-TAB-STOP 
     tCounty AT ROW 2.67 COL 9 COLON-ALIGNED WIDGET-ID 4
     tState AT ROW 2.67 COL 51 COLON-ALIGNED WIDGET-ID 6
     tOwnerPolicy AT ROW 6.24 COL 14.6 COLON-ALIGNED WIDGET-ID 8
     tOwnerLiability AT ROW 6.24 COL 54 RIGHT-ALIGNED NO-LABEL WIDGET-ID 34
     tLender1Policy AT ROW 7.43 COL 14.6 COLON-ALIGNED WIDGET-ID 10
     tLender1Liability AT ROW 7.43 COL 54 RIGHT-ALIGNED NO-LABEL WIDGET-ID 36
     tLender2Policy AT ROW 8.62 COL 14.6 COLON-ALIGNED WIDGET-ID 12
     tLender2Liability AT ROW 8.62 COL 54 RIGHT-ALIGNED NO-LABEL WIDGET-ID 38
     tPremiumStatement AT ROW 10.86 COL 21 COLON-ALIGNED WIDGET-ID 14
     tRate AT ROW 12.05 COL 21 COLON-ALIGNED WIDGET-ID 16
     tOrigSearchDate AT ROW 1.52 COL 90 COLON-ALIGNED WIDGET-ID 18
     tPreliminaryDate AT ROW 2.71 COL 90 COLON-ALIGNED WIDGET-ID 20
     tUpdateDate AT ROW 3.91 COL 90 COLON-ALIGNED WIDGET-ID 22
     tFinalDate AT ROW 5.1 COL 90 COLON-ALIGNED WIDGET-ID 24
     tClosingDate AT ROW 6.29 COL 90 COLON-ALIGNED WIDGET-ID 26
     tFundingDate AT ROW 7.48 COL 90 COLON-ALIGNED WIDGET-ID 28
     tRecordingDate AT ROW 8.67 COL 90 COLON-ALIGNED WIDGET-ID 30
     tOwnerDeliveryDate AT ROW 9.86 COL 90 COLON-ALIGNED WIDGET-ID 32
     tLender1DeliveryDate AT ROW 11.05 COL 90 COLON-ALIGNED WIDGET-ID 50
     tLender2DeliveryDate AT ROW 12.24 COL 90 COLON-ALIGNED WIDGET-ID 48
     tTypeofGap AT ROW 3.86 COL 108 NO-LABEL WIDGET-ID 70
     Btn_OK AT ROW 13.71 COL 57.6
     tFundingGap AT ROW 7.48 COL 105.8 COLON-ALIGNED NO-LABEL WIDGET-ID 60 NO-TAB-STOP 
     tRecordingGap AT ROW 8.67 COL 105.8 COLON-ALIGNED NO-LABEL WIDGET-ID 62 NO-TAB-STOP 
     tDeliveryOwnerGap AT ROW 9.86 COL 105.8 COLON-ALIGNED NO-LABEL WIDGET-ID 64 NO-TAB-STOP 
     tDeliveryLenderGap AT ROW 11.05 COL 105.8 COLON-ALIGNED NO-LABEL WIDGET-ID 66 NO-TAB-STOP 
     tDeliveryLender2Gap AT ROW 12.24 COL 105.8 COLON-ALIGNED NO-LABEL WIDGET-ID 68 NO-TAB-STOP 
     "Liability" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 5.38 COL 47 WIDGET-ID 44
     "Policy Number" VIEW-AS TEXT
          SIZE 15 BY .62 AT ROW 5.38 COL 16.6 WIDGET-ID 42
     "Policies" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 4.81 COL 5 WIDGET-ID 52
     "Premiums" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 10.29 COL 5 WIDGET-ID 56
     "Gaps shown in" VIEW-AS TEXT
          SIZE 15.8 BY .62 AT ROW 2.95 COL 108.2 WIDGET-ID 74
     RECT-29 AT ROW 5.05 COL 3 WIDGET-ID 46
     RECT-37 AT ROW 10.52 COL 3 WIDGET-ID 54
     SPACE(67.99) SKIP(1.71)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "File"
         DEFAULT-BUTTON Btn_OK WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Update-File
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Update-File:SCROLLABLE       = FALSE
       FRAME Update-File:HIDDEN           = TRUE.

/* SETTINGS FOR RECTANGLE RECT-29 IN FRAME Update-File
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-37 IN FRAME Update-File
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tDeliveryLender2Gap IN FRAME Update-File
   NO-DISPLAY                                                           */
ASSIGN 
       tDeliveryLender2Gap:READ-ONLY IN FRAME Update-File        = TRUE.

/* SETTINGS FOR FILL-IN tDeliveryLenderGap IN FRAME Update-File
   NO-DISPLAY                                                           */
ASSIGN 
       tDeliveryLenderGap:READ-ONLY IN FRAME Update-File        = TRUE.

/* SETTINGS FOR FILL-IN tDeliveryOwnerGap IN FRAME Update-File
   NO-DISPLAY                                                           */
ASSIGN 
       tDeliveryOwnerGap:READ-ONLY IN FRAME Update-File        = TRUE.

/* SETTINGS FOR FILL-IN tFundingGap IN FRAME Update-File
   NO-DISPLAY                                                           */
ASSIGN 
       tFundingGap:READ-ONLY IN FRAME Update-File        = TRUE.

/* SETTINGS FOR FILL-IN tLender1Liability IN FRAME Update-File
   ALIGN-R                                                              */
/* SETTINGS FOR FILL-IN tLender2Liability IN FRAME Update-File
   ALIGN-R                                                              */
/* SETTINGS FOR FILL-IN tOwnerLiability IN FRAME Update-File
   ALIGN-R                                                              */
/* SETTINGS FOR FILL-IN tRecordingGap IN FRAME Update-File
   NO-DISPLAY                                                           */
ASSIGN 
       tRecordingGap:READ-ONLY IN FRAME Update-File        = TRUE.

/* SETTINGS FOR FILL-IN tSearchGap IN FRAME Update-File
   NO-DISPLAY                                                           */
ASSIGN 
       tSearchGap:READ-ONLY IN FRAME Update-File        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Update-File
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Update-File Update-File
ON WINDOW-CLOSE OF FRAME Update-File /* File */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tTypeofGap
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tTypeofGap Update-File
ON VALUE-CHANGED OF tTypeofGap IN FRAME Update-File
DO:
  displayGaps(self:input-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tUpdateDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tUpdateDate Update-File
ON LEAVE OF tUpdateDate IN FRAME Update-File /* Updated */
or 'LEAVE' of tClosingDate
or 'LEAVE' of tFundingDate
or 'LEAVE' of tRecordingDate
or 'LEAVE' of tOwnerDeliveryDate
or 'LEAVE' of tLender1DeliveryDate
or 'LEAVE' of tLender2DeliveryDate
DO:
  if self:modified 
   then displayGaps(tTypeOfGap:input-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Update-File 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

assign
  tFileNumber = pFileNumber
  tState = pstate
  tCounty = pcounty
  tOwnerLiability = pownerLiability
  tOwnerPolicy = pownerPolicy
  tLender1Liability = plender1Liability
  tLender1Policy = plender1Policy
  tLender2Liability = plender2Liability
  tLender2Policy = plender2Policy
  tPremiumStatement = ppremiumStatement
  tRate = prate
  tOrigSearchDate = porigSearchDate 
  tPreliminaryDate = ppreliminaryDate 
  tUpdateDate = pupdateDate 
  tFinalDate = pfinalDate 
  tClosingDate = pclosingDate 
  tFundingDate = pfundingDate 
  tRecordingDate = precordingDate 
  tOwnerDeliveryDate = pOwnerDeliveryDate
  tLender1DeliveryDate = pLender1DeliveryDate
  tLender2DeliveryDate = pLender2DeliveryDate
  .


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  frame {&frame-name}:title = pType + " " + frame {&frame-name}:title.
  RUN enable_UI.
  displayGaps(1).
  
  publish "GetAuditStatus" (output pAuditStatus).
  if pAuditStatus = "C"then
    run AuditReadOnly.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  assign
    pFileNumber = input tFileNumber
    pState = input tstate
    pCounty = input tcounty
    pOwnerLiability = input townerLiability
    pOwnerPolicy = input townerPolicy
    pLender1Liability = input tlender1Liability
    pLender1Policy = input tlender1Policy
    pLender2Liability = input tlender2Liability
    pLender2Policy = input tlender2Policy
    pPremiumStatement = input tpremiumStatement
    pRate = input trate
    pOrigSearchDate = input torigSearchDate 
    pPreliminaryDate = input tpreliminaryDate 
    pUpdateDate = input tupdateDate 
    pFinalDate = input tfinalDate 
    pClosingDate = input tclosingDate 
    pFundingDate = input tfundingDate 
    pRecordingDate = input trecordingDate 
    pOwnerDeliveryDate = input tOwnerDeliveryDate
    pLender1DeliveryDate = input tLender1DeliveryDate
    pLender2DeliveryDate = input tLender2DeliveryDate
    .
  pCancelled = false.

END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditReadOnly Update-File 
PROCEDURE AuditReadOnly :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
do with frame Update-File: 
end.
assign
  tFileNumber:sensitive          = false
  tCounty:sensitive              = false
  tState:sensitive               = false
  tOwnerPolicy:sensitive         = false
  tLender1Policy:sensitive       = false
  tLender2Policy:sensitive       = false
  tOwnerLiability:sensitive      = false
  tLender1Liability:sensitive    = false
  tLender2Liability:sensitive    = false
  tPremiumStatement:sensitive    = false
  tRate:sensitive                = false
  tOrigSearchDate:sensitive      = false
  tPreliminaryDate:sensitive     = false
  tUpdateDate:sensitive          = false
  tFinalDate:sensitive           = false
  tClosingDate:sensitive         = false
  tSearchGap:sensitive           = false
  tFundingDate:sensitive         = false
  tFundingGap:sensitive          = false
  tRecordingDate:sensitive       = false
  tRecordingGap:sensitive        = false
  tOwnerDeliveryDate:sensitive   = false
  tDeliveryOwnerGap:sensitive    = false
  tLender1DeliveryDate:sensitive = false
  tDeliveryLenderGap:sensitive   = false
  tLender2DeliveryDate:sensitive = false
  tDeliveryLender2Gap:sensitive  = false
  tTypeofGap:sensitive           = false
  Btn_OK:sensitive               = false
  .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Update-File  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Update-File.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Update-File  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tFileNumber tCounty tState tOwnerPolicy tOwnerLiability tLender1Policy 
          tLender1Liability tLender2Policy tLender2Liability tPremiumStatement 
          tRate tOrigSearchDate tPreliminaryDate tUpdateDate tFinalDate 
          tClosingDate tFundingDate tRecordingDate tOwnerDeliveryDate 
          tLender1DeliveryDate tLender2DeliveryDate tTypeofGap 
      WITH FRAME Update-File.
  ENABLE tFileNumber tSearchGap tCounty tState tOwnerPolicy tOwnerLiability 
         tLender1Policy tLender1Liability tLender2Policy tLender2Liability 
         tPremiumStatement tRate tOrigSearchDate tPreliminaryDate tUpdateDate 
         tFinalDate tClosingDate tFundingDate tRecordingDate tOwnerDeliveryDate 
         tLender1DeliveryDate tLender2DeliveryDate tTypeofGap Btn_OK 
         tFundingGap tRecordingGap tDeliveryOwnerGap tDeliveryLenderGap 
         tDeliveryLender2Gap 
      WITH FRAME Update-File.
  VIEW FRAME Update-File.
  {&OPEN-BROWSERS-IN-QUERY-Update-File}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayGaps Update-File 
FUNCTION displayGaps RETURNS LOGICAL PRIVATE
  ( pType as int ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

do with frame {&frame-name}:
 if pType = 1 
  then
   do:
    publish "GetGaps" (tUpdateDate:input-value, tClosingDate:input-value, output tSearchGap, output std-in).
    publish "GetGaps" (tClosingDate:input-value, tFundingDate:input-value, output tFundingGap, output std-in).
    publish "GetGaps" (tFundingDate:input-value, tRecordingDate:input-value, output tRecordingGap, output std-in).
    publish "GetGaps" (tFundingDate:input-value, tOwnerDeliveryDate:input-value, output tDeliveryOwnerGap, output std-in).
    publish "GetGaps" (tFundingDate:input-value, tLender1DeliveryDate:input-value, output tDeliveryLenderGap, output std-in).
    publish "GetGaps" (tFundingDate:input-value, tLender2DeliveryDate:input-value, output tDeliveryLender2Gap, output std-in).
   end.
  else
   do:
    publish "GetGaps" (tUpdateDate:input-value, tClosingDate:input-value, output std-in, output tSearchGap).
    publish "GetGaps" (tClosingDate:input-value, tFundingDate:input-value, output std-in, output tFundingGap).
    publish "GetGaps" (tFundingDate:input-value, tRecordingDate:input-value, output std-in, output tRecordingGap).
    publish "GetGaps" (tFundingDate:input-value, tOwnerDeliveryDate:input-value, output std-in, output tDeliveryOwnerGap).
    publish "GetGaps" (tFundingDate:input-value, tLender1DeliveryDate:input-value, output std-in, output tDeliveryLenderGap).
    publish "GetGaps" (tFundingDate:input-value, tLender2DeliveryDate:input-value, output std-in, output tDeliveryLender2Gap).
   end.

 display
   tSearchGap
   tFundingGap
   tRecordingGap
   tDeliveryOwnerGap
   tDeliveryLenderGap
   tDeliveryLender2Gap.
end.

 RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

