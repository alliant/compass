&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
    File        : wsection1.w
    Description : Show questions of section1
    Modification:
    Date          Name      Description
    11/09/2016    AC        Widgets are properly aligned
    12/07/2016    AC        Handle save of last focused widget if leave 
                            is not fire
    12/12/2016    AG        New IP Added ShowWindow for setting focus on
                            entry of window.
    12/21/2016    AG        Bring Window on top/restore window if section
                            button is selected.
    01/18/2016    AC        Implement findbp window close functionality when
                            section window is closed                         
    02/13/2017    AC        Implement local inspect functionality.
    03/28/2017    AC        Handle validation for score in sections.
    07/15/2021    SA        Task 83510 modified UI and question according  
                            to new questions.d
    09/24/2021    SA        Task#:86696 Defects raised by david
  ----------------------------------------------------------------------*/
 

CREATE WIDGET-POOL.

{lib/std-def.i}
{lib/winshowscrollbars.i}

{lib/questiondef.i &seq=1000}

define variable pFnBPChanged as character no-undo .
define variable pCorrectCase as character no-undo .
define variable pAuditStatus as character no-undo .
define variable pValidScore as logical no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tScore tComments 
&Scoped-Define DISPLAYED-OBJECTS tScore tComments 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE VARIABLE tComments AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 134.4 BY 3
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE title1 AS CHARACTER FORMAT "X(256)":U INITIAL "title1" 
      VIEW-AS TEXT 
     SIZE 132 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE tScore AS INTEGER FORMAT "zz":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE q1-1 AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 134 BY 13.1 NO-UNDO.

DEFINE VARIABLE tDetails AS CHARACTER 
     VIEW-AS EDITOR NO-BOX
     SIZE 132 BY 4.05
     FONT 16 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tScore AT ROW 2.33 COL 144.4 COLON-ALIGNED NO-LABEL WIDGET-ID 16
     tComments AT ROW 4 COL 20 NO-LABEL WIDGET-ID 26
     title1 AT ROW 1.48 COL 3.2 COLON-ALIGNED NO-LABEL WIDGET-ID 28 NO-TAB-STOP 
     "Comments:" VIEW-AS TEXT
          SIZE 14.6 BY .62 AT ROW 4.57 COL 4.4 WIDGET-ID 24
     "Section" VIEW-AS TEXT
          SIZE 12.8 BY .62 AT ROW 3.86 COL 4.4 WIDGET-ID 22
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 158 BY 26.29
         FONT 18 WIDGET-ID 100.

DEFINE FRAME fSection
     tDetails AT ROW 2.91 COL 13 NO-LABEL WIDGET-ID 22 NO-TAB-STOP 
     q1-1 AT ROW 7.43 COL 13 NO-LABEL WIDGET-ID 20
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 7.29
         SCROLLABLE SIZE 158 BY 20 WIDGET-ID 200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Section 1"
         HEIGHT             = 26.29
         WIDTH              = 158
         MAX-HEIGHT         = 26.29
         MAX-WIDTH          = 158
         VIRTUAL-HEIGHT     = 26.29
         VIRTUAL-WIDTH      = 158
         SHOW-IN-TASKBAR    = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         PRIVATE-DATA       = "1"
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fSection:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* SETTINGS FOR FILL-IN title1 IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FRAME fSection
                                                                        */
ASSIGN 
       FRAME fSection:HEIGHT           = 20
       FRAME fSection:WIDTH            = 158.

/* SETTINGS FOR EDITOR q1-1 IN FRAME fSection
   NO-DISPLAY                                                           */
/* SETTINGS FOR EDITOR tDetails IN FRAME fSection
   NO-DISPLAY NO-ENABLE                                                 */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Section 1 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Section 1 */
DO:
 pFnBPChanged = "" .
 pCorrectCase = "".
  if valid-handle(focus) /* maybe not if the window was just closed */
   and (focus:type = "fill-in" or focus:type = "editor") /* all other types ok */
   and focus:modified /* inconsistently set... */
  then apply "leave" to focus.
 publish "IsModifiedFindBp" (input self:private-data,
                            output pFnBPChanged,
                            output pCorrectCase).
 if (pFnBPChanged = "1" and pCorrectCase = "") or (pCorrectCase = "Not" and pFnBPChanged = "1" ) then 
 do:
   MESSAGE "Information on Finding and Best Practices has been modified. All changes will be discarded." skip 
           "Do you want to continue?"
     VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE lChoice AS LOGICAL.
   if lChoice = true then
   do:
    publish "CloseFindBp" (self:private-data).
    publish "SetFnBPChanged" (input "").
   end.
   else return.
    pFnBPChanged = "" .
    pCorrectCase = "".
 end.
 else
   publish "CloseFindBp" (self:private-data).
   publish "SetFnBPChanged" (input "").
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Section 1 */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fSection
&Scoped-define SELF-NAME q1-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL q1-1 C-Win
ON LEAVE OF q1-1 IN FRAME fSection
DO:
   publish "SetQuestionAnswer" (1000, self:screen-value).
   publish "SaveAfterEveryAnswer" .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME tComments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tComments C-Win
ON LEAVE OF tComments IN FRAME fMain
DO:
  if not self:modified then return.
  publish "SetSectionAnswer" ("1", "Comments", self:screen-value).
  publish "SaveAfterEveryAnswer" .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tScore
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tScore C-Win
ON LEAVE OF tScore IN FRAME fMain
DO: 
  {lib/dispscore.i &s="1"}
  publish "SaveAfterEveryAnswer" .       
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

PAUSE 0 BEFORE-HIDE.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.
subscribe to "Close" anywhere.  
run initializeFrame in this-procedure.
subscribe to "LeaveAll" anywhere .
subscribe to "CheckSectionScore" anywhere.
subscribe to "AuditCopied"       anywhere.
on 'value-changed':u anywhere
do:
  publish "EnableSave".
  publish "SetClickSave".
end.

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditClosed C-Win 
PROCEDURE AuditClosed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 {lib/unsetquestion.i &seq=1000}

 assign
   tComments:screen-value in frame fMain = ""
   tComments:sensitive in frame fMain = false
   tScore:sensitive in frame fMain = false
   tScore:screen-value in frame fMain = ""
   q1-1:screen-value in frame fSection = ""
   frame fSection:sensitive = false
   .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditCopied C-Win 
PROCEDURE AuditCopied :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/dispsectiont.i &id="1"}
   run AuditOpened.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditOpened C-Win 
PROCEDURE AuditOpened :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 {lib/dispanswer.i &seq=1000 &ex-y=true &ex-n=true &ex-a=true &ex-u=true}
 publish "GetQuestionAnswer" (1000, output std-ch).
 q1-1:screen-value in frame fSection = std-ch.
  
 {lib/dispsection.i &id="1"}

  assign
    tComments:sensitive in frame fMain = true
    tScore:sensitive in frame fMain = false
    frame fSection:sensitive = true
    .
  
  publish "GetAuditStatus" (output pAuditStatus).
  
  if pAuditStatus = "C" then
    run AuditReadOnly.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditReadOnly C-Win 
PROCEDURE AuditReadOnly :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /*-------------notused-----------*/
 /*do with frame {&frame-name}:
 end.
 assign
   tComments:sensitive = false
   tScore:sensitive    = false.
 
 do with frame fSection:
 end.
 q1-1:sensitive = false.*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CheckSectionScore C-Win 
PROCEDURE CheckSectionScore :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input  parameter SectionNumber as int.
def output parameter SectionScore  as char.
if sectionnumber = integer(C-Win:private-data)  and ( tScore:screen-value in frame fMain ne "" ) then
  SectionScore = tScore:screen-value in frame fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Close C-Win 
PROCEDURE Close :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tautosave as logical no-undo.
  publish "GetAutosave" (output tautosave).
  if tautosave then                                          
  publish "SaveAfterEveryAnswer".
  apply 'close' to this-procedure .
  return no-apply.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doButton C-Win 
PROCEDURE doButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/dobutton.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doCheckbox C-Win 
PROCEDURE doCheckbox :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/docheckbox.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doFillin C-Win 
PROCEDURE doFillin :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/dofillin.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tScore tComments 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE tScore tComments 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  ENABLE q1-1 
      WITH FRAME fSection IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fSection}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initializeFrame C-Win 
PROCEDURE initializeFrame PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  {lib/question.i &seq=1000 &r="1.5" &ex-u=true &ex-y=true &ex-n=true &ex-a=true &ex-r=true}

  {lib/dispsectiont.i &id="1"}

tDetails:screen-value = "(Were the offices organized and clean? ~
Were you professionally greeted, etc.? ~
What is your overall impression of the agency staff? ~
Was staff knowledgeable, trained for their positions, dressed professionally, etc.? ~
Does the agency appear to be staffed adequately? ~
Are there enough people to handle their current level of orders/closings? ~
Are there any key positions open? ~
Record any responses to 'Is there anything you'd like us to know prior to starting the audit?')"
  .
  
  RUN ShowScrollBars(FRAME fSection:HANDLE, NO, NO).

  run AuditOpened in this-procedure.    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LeaveAll C-Win 
PROCEDURE LeaveAll :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 apply "leave" to tComments in frame {&frame-name}.
 apply "leave" to tScore in frame {&frame-name}.
 apply "leave" to q1-1 in frame fSection.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var tFlag as logical.
tFlag = tComments:sensitive in frame fMain.
tComments:sensitive in frame fMain  = true.
apply "ENTRY" to tComments in frame fMain.
tComments:sensitive in frame fMain = tFlag.
if {&window-name}:window-state eq window-minimized  then
 {&window-name}:window-state = window-normal .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
  frame fMain:virtual-height-pixels = {&window-name}:height-pixels.
  frame fMain:width-pixels = {&window-name}:width-pixels.
  frame fMain:height-pixels = {&window-name}:height-pixels.

  title1:width = frame fMain:width - 16.
  tScore:X = frame fMain:width-pixels - 62 .
  tComments:width-pixels = frame fMain:width-pixels - 115.

  if {&window-name}:width-pixels > frame fSection:width-pixels 
   then
    do:
      frame fSection:width-pixels = {&window-name}:width-pixels.
      frame fSection:virtual-width-pixels = {&window-name}:width-pixels.
    end.
   else
    do:
      frame fSection:virtual-width-pixels = {&window-name}:width-pixels.
      frame fSection:width-pixels = {&window-name}:width-pixels.
      /* das: For some reason, shrinking a window size MAY cause the horizontal
         scroll bar.  The above sequence of widget setting should resolve it,
         but it doesn't every time.  So... */
      RUN ShowScrollBars(FRAME fSection:HANDLE, NO, YES).
   end.

  frame fSection:height-pixels = {&window-name}:height-pixels - 132.

  {lib/dispsectiont.i &id="1"}
  {lib/resizequestion.i &seq=1000}

  tDetails:width-pixels = frame fSection:width-pixels - 80.
  q1-1:width-pixels = frame fSection:width-pixels - 80.
  q1-1:height-pixels = frame fSection:height-pixels - 140.

  RUN ShowScrollBars(FRAME fSection:HANDLE, NO, NO).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

