&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wreferences.w
   my REFERENCES window
   D.Sinclair 10.15.2015
   Modified:
    Date          Name      Description
    03/17/2017    AG        Added spell check functionality.
    09/11/2017    AG        Alternate color code in browse.

 */

CREATE WIDGET-POOL.


&scoped-define data-file references.dat

{tt/qarreference.i}

{lib/std-def.i}

def var tModified as logical init false.
def var tNew as logical init false.
def var tReferencesFile as char no-undo.
def var tLocked as logical init false.
def var iBgcolor as int no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwReferences

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES reference

/* Definitions for BROWSE brwReferences                                 */
&Scoped-define FIELDS-IN-QUERY-brwReferences reference.isChecked reference.category reference.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwReferences isChecked   
&Scoped-define SELF-NAME brwReferences
&Scoped-define QUERY-STRING-brwReferences FOR EACH reference                           by reference.category                           by reference.description
&Scoped-define OPEN-QUERY-brwReferences OPEN QUERY {&SELF-NAME} FOR EACH reference                           by reference.category                           by reference.description.
&Scoped-define TABLES-IN-QUERY-brwReferences reference
&Scoped-define FIRST-TABLE-IN-QUERY-brwReferences reference


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwReferences}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bBtnSpellCheck bAdd bImport bExport ~
brwReferences 
&Scoped-Define DISPLAYED-OBJECTS tCat tRef 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resetBrowse C-Win 
FUNCTION resetBrowse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resetDetail C-Win 
FUNCTION resetDetail RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAdd 
     LABEL "Add" 
     SIZE 7.2 BY 1.71 TOOLTIP "Create a new reference".

DEFINE BUTTON bBtnSpellCheck  NO-FOCUS
     LABEL "Spl" 
     SIZE 7.2 BY 1.71 TOOLTIP "Check Spelling".

DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 7.2 BY 1.71 TOOLTIP "Cancel changes".

DEFINE BUTTON bDelete 
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Remove the currently selected reference".

DEFINE BUTTON bExport 
     LABEL "Export..." 
     SIZE 7.2 BY 1.71 TOOLTIP "Export references to a file".

DEFINE BUTTON bImport 
     LABEL "Import..." 
     SIZE 7.2 BY 1.71 TOOLTIP "Import references from a file".

DEFINE BUTTON bSave 
     LABEL "Save" 
     SIZE 7.2 BY 1.71 TOOLTIP "Save changes".

DEFINE VARIABLE tRef AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 90 BY 5 NO-UNDO.

DEFINE VARIABLE tCat AS CHARACTER FORMAT "X(256)":U 
     LABEL "Category" 
     VIEW-AS FILL-IN 
     SIZE 36 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-34
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 31 BY 1.91.

DEFINE RECTANGLE RECT-35
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 16.6 BY 1.91.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 8 BY 1.91.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwReferences FOR 
      reference SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwReferences
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwReferences C-Win _FREEFORM
  QUERY brwReferences DISPLAY
      reference.isChecked column-label "" view-as toggle-box
 reference.category label "Category" format "x(30)"
 reference.description label "Reference" format "x(60)"

 enable isChecked
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 90 BY 15.24 ROW-HEIGHT-CHARS .86 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bBtnSpellCheck AT ROW 1.05 COL 49.4 WIDGET-ID 120 NO-TAB-STOP 
     bAdd AT ROW 1.05 COL 2 WIDGET-ID 12
     bDelete AT ROW 1.05 COL 9.2 WIDGET-ID 14
     bSave AT ROW 1.05 COL 16.4 WIDGET-ID 16
     bCancel AT ROW 1.05 COL 23.6 WIDGET-ID 18
     bImport AT ROW 1.05 COL 32.8 WIDGET-ID 20
     bExport AT ROW 1.05 COL 40.2 WIDGET-ID 22
     tCat AT ROW 3.33 COL 10 COLON-ALIGNED WIDGET-ID 6
     tRef AT ROW 4.57 COL 2 NO-LABEL WIDGET-ID 2
     brwReferences AT ROW 9.81 COL 2 WIDGET-ID 200
     RECT-34 AT ROW 1 COL 1 WIDGET-ID 24
     RECT-35 AT ROW 1 COL 31.6 WIDGET-ID 26
     RECT-36 AT ROW 1 COL 49 WIDGET-ID 122
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 91.8 BY 24.19 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "My References"
         HEIGHT             = 24.19
         WIDTH              = 91.8
         MAX-HEIGHT         = 27
         MAX-WIDTH          = 173.4
         VIRTUAL-HEIGHT     = 27
         VIRTUAL-WIDTH      = 173.4
         SHOW-IN-TASKBAR    = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwReferences tRef fMain */
/* SETTINGS FOR BUTTON bCancel IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDelete IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bSave IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-34 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-35 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-36 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tCat IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR tRef IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwReferences
/* Query rebuild information for BROWSE brwReferences
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH reference
                          by reference.category
                          by reference.description.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwReferences */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* My References */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* My References */
DO:
  /* This event will close the window and terminate the procedure.  */
  if tNew or tModified 
   then
  do:
   MESSAGE "Your current changes will be discarded." skip
           "Do you want to continue?"
    VIEW-AS ALERT-BOX warning BUTTONS yes-no
     update std-lo.
   if not std-lo 
    then return no-apply.
  end.
  run exportReferences in this-procedure.
  publish "WindowClosed" (input "References").
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* My References */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAdd C-Win
ON CHOOSE OF bAdd IN FRAME fMain /* Add */
DO:
  disable bAdd bDelete bImport bExport brwReferences 
    with frame {&frame-name}.
  enable tCat tRef bSave bCancel with frame {&frame-name}.
  assign
    tCat:screen-value in frame {&frame-name} = ""
    tRef:screen-value in frame {&frame-name} = ""
    .
  tNew = true.
  apply "ENTRY" to tCat in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnSpellCheck
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnSpellCheck C-Win
ON CHOOSE OF bBtnSpellCheck IN FRAME fMain /* Spl */
DO:
  run SpellChecker in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME fMain /* Cancel */
DO:
  resetBrowse().
  resetDetail().
  tNew = false.
  tModified = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME fMain /* Delete */
DO:
  if available reference 
   then 
    do: 
        MESSAGE "Reference with category " reference.category " will be permanently deleted." skip
                "Do you want to continue?"
         VIEW-AS ALERT-BOX question BUTTONS yes-no
          update STD-LO.
        if not std-lo 
         then return.
        delete reference.
    end.
  resetBrowse().
  resetDetail().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export... */
DO:
  run doExport.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bImport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bImport C-Win
ON CHOOSE OF bImport IN FRAME fMain /* Import... */
DO:
  run doImport.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwReferences
&Scoped-define SELF-NAME brwReferences
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReferences C-Win
ON ROW-DISPLAY OF brwReferences IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}

  if current-result-row("brwReferences") modulo 2 = 0 then
    iBgColor = 17.
  else
    iBgColor = 15.
  reference.isChecked :bgcolor in browse brwReferences = iBgColor.
  reference.category :bgcolor in browse brwReferences = iBgColor.
  reference.description :bgcolor in browse brwReferences = iBgColor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReferences C-Win
ON START-SEARCH OF brwReferences IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReferences C-Win
ON VALUE-CHANGED OF brwReferences IN FRAME fMain
DO:
    if not available reference 
     then
      do: assign
            tCat:screen-value in frame {&frame-name} = ""
            tRef:screen-value in frame {&frame-name} = ""
            .
          return.
      end.

    assign
      tCat:screen-value in frame {&frame-name} = reference.category 
      tRef:screen-value in frame {&frame-name} = reference.description
      .
    enable tCat tRef bAdd bDelete with frame {&frame-name}.
    disable bSave bCancel with frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME fMain /* Save */
DO:
  if tNew
   then create reference.
  if available reference 
   then assign
         reference.category = tCat:screen-value in frame {&frame-name}
         reference.description = tRef:screen-value in frame {&frame-name}
         .
  assign
    tNew = false
    tModified = false
    .
  resetBrowse().
  resetDetail().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tCat
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tCat C-Win
ON ANY-PRINTABLE OF tCat IN FRAME fMain /* Category */
DO:
  disable bAdd bDelete bImport bExport brwReferences with frame {&frame-name}.
  enable bSave bCancel with frame {&frame-name}.
  tModified = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tRef
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tRef C-Win
ON ANY-PRINTABLE OF tRef IN FRAME fMain
or "BACKSPACE" of tRef
DO:
   disable bAdd bDelete bImport bExport brwReferences with frame {&frame-name}.
   enable bSave bCancel with frame {&frame-name}.
   if keylabel(lastkey) = "BACKSPACE"
    then 
     do: 
         if self:cursor-char > 1
          then 
           do: self:cursor-char = self:cursor-char - 1.
               self:delete-char().
           end.
          else
         if self:cursor-line > 1 
          then 
           do: 
               self:cursor-line = self:cursor-line - 1.
               do std-in = 1 to self:width-chars:
                self:cursor-char = std-in no-error.
               end.
               self:delete-char().
           end.
     end.
   tModified = true.    
   return.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

PAUSE 0 BEFORE-HIDE.
 
subscribe to "LockOn" anywhere.
subscribe to "LockOff" anywhere.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

bAdd:load-image("images/add.bmp").
bAdd:load-image-insensitive("images/add-i.bmp").
bDelete:load-image("images/delete.bmp").
bDelete:load-image-insensitive("images/delete-i.bmp").
bSave:load-image("images/save.bmp").
bSave:load-image-insensitive("images/save-i.bmp").
bCancel:load-image("images/erase.bmp").
bCancel:load-image-insensitive("images/erase-i.bmp").

bExport:load-image("images/download.bmp").
bExport:load-image-insensitive("images/download-i.bmp").
bImport:load-image("images/import.bmp").
bImport:load-image-insensitive("images/import-i.bmp").
bBtnSpellCheck:load-image("images/spellcheck.bmp").
tQueryString = "preselect each reference no-lock by reference.category by reference.description ".

tReferencesFile = search("{&data-file}").

run importReferences in this-procedure.
{lib/win-main.i}

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  if available reference
   then apply "VALUE-CHANGED" to brwReferences in frame {&frame-name}.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doExport C-Win 
PROCEDURE doExport PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tFile as char.
 def var tOK as logical no-undo.
 def var iCnt as int no-undo.

 def buffer reference for reference.

 if not can-find(first reference
                   where reference.isChecked = true)
  then
   do: 
       MESSAGE "Please select at least one reference for export."
           VIEW-AS ALERT-BOX INFO BUTTONS OK.
       return.
   end.

 system-dialog get-file tFile
   filters "Data Files" "*.dat"
   ask-overwrite
   create-test-file
   default-extension ".dat"
   save-as
  update tOK.

 if not tOK 
  then return.

 output to value(tFile) page-size 0.
 for each reference
   where reference.isChecked = true:
  export reference.category 
         replace(reference.description, chr(10), "&br;").
  iCnt = iCnt + 1.
 end.
 output close.

 MESSAGE iCnt " references exported."
  VIEW-AS ALERT-BOX INFO BUTTONS OK.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doImport C-Win 
PROCEDURE doImport PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tFile as char.
 def var tOK as logical no-undo.
 def var iCnt as int no-undo.
 def var iRej as int no-undo.
 def var iCat as char no-undo.
 def var iDesc as char no-undo.

 system-dialog get-file tFile
     filters "Data Files" "*.dat"
     default-extension ".dat"
    update tOK.

 if not tOK 
  then return.

 tFile = search(tFile).
 if tFile = ?
  then return.

 input from value(tFile).
 repeat:
  import iCat iDesc.
  iDesc = replace(iDesc, "&br;", chr(10)).
  if can-find(reference 
    where reference.category = iCat
      and reference.description = iDesc)
   then 
    do: iRej = iRej + 1.
        next.
    end.
  create reference.
  reference.category = iCat.
  reference.description = iDesc.
  iCnt = iCnt + 1.
 end.
 input close.

 resetBrowse().
 resetDetail().
 tNew = false.
 tModified = false.
 
 MESSAGE iCnt " references imported." skip
         iRej " duplicate references rejected."
  VIEW-AS ALERT-BOX INFO BUTTONS OK.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tCat tRef 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bBtnSpellCheck bAdd bImport bExport brwReferences 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportReferences C-Win 
PROCEDURE exportReferences PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer reference for reference.

 output to value("{&data-file}").
 for each reference:
  export reference.category
         replace(reference.description, chr(10), "&br;")
         .
 end.
 output close.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hideWindow C-Win 
PROCEDURE hideWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:visible = false.
 publish "WindowClosed" (input "References").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE importReferences C-Win 
PROCEDURE importReferences PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def buffer reference for reference.

 if tReferencesfile = ?
  then return.

 input from value(tReferencesFile).
 repeat:
   create reference.
   import reference.category
          reference.description
          .
   reference.description = replace(reference.description, "&br;", chr(10)).
 end.
 input close.
 for each reference
   where reference.category = ""
     and reference.description = "":
  delete reference.
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LockOff C-Win 
PROCEDURE LockOff :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 assign
   bImport:sensitive in frame {&frame-name} = true
   bExport:sensitive in frame {&frame-name} = true
   tLocked = false
   .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LockOn C-Win 
PROCEDURE LockOn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 assign
   bImport:sensitive in frame {&frame-name} = false
   bExport:sensitive in frame {&frame-name} = false
   tLocked = true
   .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:visible = true.
 {&window-name}:move-to-top().
 publish "WindowOpened" (input "References").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  pName = name of field clicked
  Notes:       Although named "sortData", we're using to check/uncheck the
               isChecked flag for export.
------------------------------------------------------------------------------*/
def buffer reference for reference.

{lib/brw-sortData.i 
    &pre-code="if pName = 'isChecked' 
                then 
                 do: 
                    std-lo = can-find(first reference
                                        where reference.isChecked = false).
                    for each reference:
                     reference.isChecked = std-lo.
                    end.
                    resetBrowse().
                    return.
                 end.
              "}
apply "VALUE-CHANGED" to browse brwReferences.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SpellChecker C-Win 
PROCEDURE SpellChecker :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var checktext as char no-undo.

def buffer reference for reference.

 if valid-handle(focus) 
   and (focus:type = "editor")
   and focus:modified 
 then
   checktext = tRef:screen-value in frame {&frame-name}.
 else
 do:
  for each reference by reference.category:
    checktext =  checktext + "|" + reference.description.
  end.
 end.

 run util/spellcheck.p ({&window-name}:handle, input-output checktext, output std-lo).

 if valid-handle(focus) 
  and (focus:type = "editor") 
  and focus:modified 
 then 
   tRef:screen-value in frame {&frame-name} = entry(1, checktext , chr(7)). 
 else
 do:
  for each reference by reference.category:
    std-in = std-in + 1 .
    reference.description = entry(std-in , checktext , chr(7)). 
    
  end.
 end.
 if can-find(first reference) then
   brwReferences:refresh() in frame fMain.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

 /* fMain components */
 tRef:width-pixels = frame fMain:width-pixels - 10.
 brwReferences:width-pixels = frame fmain:width-pixels - 10.
 brwReferences:height-pixels = frame fMain:height-pixels - 188.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resetBrowse C-Win 
FUNCTION resetBrowse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def var tRecid as recid.

 if available reference 
  then tRecid = recid(reference).


 hQueryHandle = browse {&browse-name}:query.
 hQueryHandle:query-close().
 hQueryHandle:query-prepare(tQueryString).
 hQueryHandle:query-open().  

 if tRecid <> ?
  then reposition brwReferences to recid tRecid.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resetDetail C-Win 
FUNCTION resetDetail RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 assign
   tCat:sensitive in frame {&frame-name} = available reference
   tRef:sensitive in frame {&frame-name} = available reference
   .
 if available reference
  then assign
         tCat:screen-value in frame {&frame-name} = reference.category
         tRef:screen-value in frame {&frame-name} = reference.description
         .
  else assign
        tCat:screen-value in frame {&frame-name} = ""
        tRef:screen-value in frame {&frame-name} = ""
        .

 assign
   bDelete:sensitive in frame {&frame-name} = available reference
   bAdd:sensitive in frame {&frame-name} = true
   bSave:sensitive in frame {&frame-name} = false
   bCancel:sensitive in frame {&frame-name} = false
   brwReferences:sensitive in frame {&frame-name} = true
   bImport:sensitive in frame {&frame-name} = not tLocked
   bExport:sensitive in frame {&frame-name} = not tLocked
   .
 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

