&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
    File        : wsection4.w
    Description : Show questions of section4
    Modification:
    Date          Name      Description
    11/09/2016    AC        Frame resized and new questions implemented
    11/28/2016    AC        q-4 buyer changed to buyer/borrower and N/A 
                            button added
    12/06/2016    AG        Changed font of comment and score for consistency
    12/07/2016    AC        Handle save of last focused widget if leave 
                            is not fire
    12/12/2016    AG        New IP Added ShowWindow for setting focus on
                            entry of window.
    12/16/2016    AC        Implement file specific section title  
    12/19/2016    AC        Seprate  N/A button added for buyer and seller 
                            and disable other in question 4.2            
    12/21/2016    AG        Bring Window on top/restore window if section
                            button is selected.
    01/18/2017    AC        Handel findbp window close when section window is 
                            closed                         
    02/07/2017    AC        Modified to apply leave trigger to q1385 q1395
    02/13/2017    AC        Implement local inspect functionality.
    03/28/2017    AC        Handle validation for score in sections.
    10/22/2019    AG        Fixed bug in difference field in question 4.12.
    07/15/2021    SA        Task 83510 modified UI and question according  
                            to new questions.d
    09/24/2021    SA        Task#:86696 Defects raised by QA
    11/12/2021    SC        Task#:86696 Updated the questionSeq
    02/07/2022    SA        Task#:90943 modified question 4.3 and 4.3a 
    11/28/2022    SA        Task#:99694 Enhancement in qar
  ----------------------------------------------------------------------*/

CREATE WIDGET-POOL.

{lib/std-def.i}
{lib/winshowscrollbars.i}

def var activeFileID as int no-undo.

{lib/questiondef.i &seq=1400} 
def var q2r as widget-handle.
def var q2pc as widget-handle.
def var q2pl as widget-handle.
def var q2l as widget-handle.
def var q2o as widget-handle.

{lib/questiondef.i &seq=1405} 

{lib/questiondef.i &seq=1410} 
def var q4s AS widget-handle NO-UNDO.
def var q4si as widget-handle no-undo.
def var q4so as widget-handle no-undo.
def var q4sna as widget-handle no-undo.

{lib/questiondef.i &seq=1412}
def var q4b AS widget-handle NO-UNDO.
def var q4bi as widget-handle no-undo.
def var q4bo as widget-handle no-undo.
def var q4bna as widget-handle no-undo.

define variable pFnBPChanged as character no-undo .
define variable pCorrectCase as character no-undo .
define variable pValidScore as logical no-undo.
 
{lib/questiondef.i &seq=1415}
{lib/questiondef.i &seq=1416}
{lib/questiondef.i &seq=1419}
{lib/questiondef.i &seq=1421}
{lib/questiondef.i &seq=1420}
{lib/questiondef.i &seq=1425}
{lib/questiondef.i &seq=1430}
{lib/questiondef.i &seq=1440}
{lib/questiondef.i &seq=1445}
{lib/questiondef.i &seq=1460}
{lib/questiondef.i &seq=1465}
{lib/questiondef.i &seq=1470}
{lib/questiondef.i &seq=1480}
{lib/questiondef.i &seq=1490}
{lib/questiondef.i &seq=1500}
{lib/questiondef.i &seq=1510}
{lib/questiondef.i &seq=1515}
{lib/questiondef.i &seq=1520}
{lib/questiondef.i &seq=1530}
{lib/questiondef.i &seq=1535}
{lib/questiondef.i &seq=1545}
{lib/questiondef.i &seq=1546}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tScore tComments 
&Scoped-Define DISPLAYED-OBJECTS tScore tComments tScoreLabel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE VARIABLE tComments AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 134.4 BY 3
     BGCOLOR 15 FONT 18 NO-UNDO.

DEFINE VARIABLE title1 AS CHARACTER FORMAT "X(256)":U INITIAL "title1" 
      VIEW-AS TEXT 
     SIZE 126 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE tScore AS INTEGER FORMAT "zz":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1
     FONT 18 NO-UNDO.

DEFINE VARIABLE tScoreLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Section Score" 
      VIEW-AS TEXT 
     SIZE 16 BY .62
     FONT 6 NO-UNDO.

DEFINE VARIABLE q4-11 AS CHARACTER FORMAT "X(256)":U INITIAL "4.11" 
      VIEW-AS TEXT 
     SIZE 5.6 BY 1
     FONT 16 NO-UNDO.

DEFINE VARIABLE qCounty AS CHARACTER FORMAT "X(256)":U 
     LABEL "County" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE qLender1Liability AS DECIMAL FORMAT ">>,>>>,>>9":U INITIAL 0 
     LABEL "Lender" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE qLender2Liability AS DECIMAL FORMAT ">>,>>>,>>9":U INITIAL 0 
     LABEL "Lender 2" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE qOwnerLiability AS DECIMAL FORMAT ">>,>>>,>>9":U INITIAL 0 
     LABEL "Owner" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE qPremiumStatement AS DECIMAL FORMAT ">>,>>9.99":U INITIAL 0 
     LABEL "Statement" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Premium as shown on statement" NO-UNDO.

DEFINE VARIABLE qRate AS DECIMAL FORMAT ">>,>>9.99":U INITIAL 0 
     LABEL "Rate" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "State, Filed, or Underwriter" NO-UNDO.

DEFINE VARIABLE qRateDifference AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "Difference" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     FONT 6 NO-UNDO.

DEFINE VARIABLE qState AS CHARACTER FORMAT "x(2)":U 
     LABEL "State" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE VARIABLE t4-14 AS CHARACTER FORMAT "X(256)":U INITIAL "RATE ANALYSIS" 
      VIEW-AS TEXT 
     SIZE 24 BY 1
     FONT 16 NO-UNDO.

DEFINE RECTANGLE RECT-28
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 27 BY 4.05.

DEFINE RECTANGLE RECT-29
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 31 BY 4.05.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tScore AT ROW 2 COL 144.4 COLON-ALIGNED NO-LABEL WIDGET-ID 16
     tComments AT ROW 4 COL 20 NO-LABEL WIDGET-ID 26
     tScoreLabel AT ROW 1.24 COL 139 COLON-ALIGNED NO-LABEL WIDGET-ID 32
     title1 AT ROW 1.48 COL 3.2 COLON-ALIGNED NO-LABEL WIDGET-ID 28 NO-TAB-STOP 
     "Comments:" VIEW-AS TEXT
          SIZE 15 BY .62 AT ROW 4.57 COL 4.4 WIDGET-ID 24
          FONT 18
     "Section" VIEW-AS TEXT
          SIZE 11.2 BY .62 AT ROW 3.86 COL 4.4 WIDGET-ID 22
          FONT 18
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 158 BY 26.33 WIDGET-ID 100.

DEFINE FRAME fSection
     qState AT ROW 58.14 COL 49 COLON-ALIGNED WIDGET-ID 320
     qOwnerLiability AT ROW 58.38 COL 82 COLON-ALIGNED WIDGET-ID 312
     qPremiumStatement AT ROW 58.38 COL 117 COLON-ALIGNED WIDGET-ID 314
     qCounty AT ROW 59.33 COL 49 COLON-ALIGNED WIDGET-ID 306
     qLender1Liability AT ROW 59.57 COL 82 COLON-ALIGNED WIDGET-ID 308
     qRate AT ROW 59.57 COL 117 COLON-ALIGNED WIDGET-ID 316
     qLender2Liability AT ROW 60.76 COL 82 COLON-ALIGNED WIDGET-ID 310
     qRateDifference AT ROW 60.76 COL 117 COLON-ALIGNED WIDGET-ID 318
     q4-11 AT ROW 58.14 COL 2 NO-LABEL WIDGET-ID 304 NO-TAB-STOP 
     t4-14 AT ROW 58.14 COL 13 COLON-ALIGNED NO-LABEL WIDGET-ID 326 NO-TAB-STOP 
     " Premium" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 57.67 COL 108 WIDGET-ID 328
     " Liability Amount" VIEW-AS TEXT
          SIZE 16 BY .62 AT ROW 57.67 COL 75 WIDGET-ID 330
     RECT-28 AT ROW 57.81 COL 73 WIDGET-ID 322
     RECT-29 AT ROW 57.81 COL 105 WIDGET-ID 324
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 7.48
         SCROLLABLE SIZE 158 BY 68 WIDGET-ID 200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Section 4"
         HEIGHT             = 26.33
         WIDTH              = 158
         MAX-HEIGHT         = 34.05
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.05
         VIRTUAL-WIDTH      = 273.2
         SHOW-IN-TASKBAR    = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         PRIVATE-DATA       = "4"
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fSection:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* SETTINGS FOR FILL-IN title1 IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tScoreLabel IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tScoreLabel:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FRAME fSection
                                                                        */
ASSIGN 
       FRAME fSection:HEIGHT           = 18
       FRAME fSection:WIDTH            = 157.9.

/* SETTINGS FOR FILL-IN q4-11 IN FRAME fSection
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN qRateDifference IN FRAME fSection
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-28 IN FRAME fSection
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-29 IN FRAME fSection
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN t4-14 IN FRAME fSection
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Section 4 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Section 4 */
DO:
 pFnBPChanged = "".
 pCorrectCase = "".
  if valid-handle(focus) /* maybe not if the window was just closed */
   and (focus:type = "fill-in" or focus:type = "editor") /* all other types ok */
   and focus:modified /* inconsistently set... */
  then apply "leave" to focus.

 publish "IsModifiedFindBp" (input self:private-data,
                             output pFnBPChanged,
                             output pCorrectCase).
 if (pFnBPChanged = "4" and pCorrectCase = "") or (pCorrectCase = "Not" and pFnBPChanged = "4" ) then 
 do:
   MESSAGE "Information on Finding and Best Practices has been modified. All changes will be discarded." skip 
           "Do you want to continue?"
     VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO-CANCEL UPDATE lChoice AS LOGICAL.
   if lChoice = true then
   do:
    publish "CloseFindBp" (self:private-data).
    publish "SetFnBPChanged" (input "").
   end.
   else return.
    pFnBPChanged = "".
    pCorrectCase = "".
 end.
 else
   publish "CloseFindBp" (self:private-data).
   publish "SetFnBPChanged" (input "").
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Section 4 */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fSection
&Scoped-define SELF-NAME qCounty
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qCounty C-Win
ON LEAVE OF qCounty IN FRAME fSection /* County */
DO:
  if not self:modified then return.
  publish "SetFileAttribute" (activeFileID,
                              "County", 
                              self:screen-value).
  if error-status:error 
   then self:screen-value = "".

  publish "SaveAfterEveryAnswer" .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qLender1Liability
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qLender1Liability C-Win
ON LEAVE OF qLender1Liability IN FRAME fSection /* Lender */
DO:
  if not self:modified then return.
  publish "SetFileAttribute" (activeFileID,
                              "Lender1Liability", 
                              self:screen-value).
  if error-status:error 
   then self:screen-value = "".  

  publish "SaveAfterEveryAnswer" .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qLender2Liability
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qLender2Liability C-Win
ON LEAVE OF qLender2Liability IN FRAME fSection /* Lender 2 */
DO:
  if not self:modified then return.
  publish "SetFileAttribute" (activeFileID,
                              "Lender2Liability", 
                              self:screen-value).
  if error-status:error 
   then self:screen-value = "".  

  publish "SaveAfterEveryAnswer" .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qOwnerLiability
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qOwnerLiability C-Win
ON LEAVE OF qOwnerLiability IN FRAME fSection /* Owner */
DO:
  if not self:modified then return.
  publish "SetFileAttribute" (activeFileID,
                              "OwnerLiability", 
                              self:screen-value).
  if error-status:error 
   then self:screen-value = "".  

  publish "SaveAfterEveryAnswer" .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qPremiumStatement
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qPremiumStatement C-Win
ON LEAVE OF qPremiumStatement IN FRAME fSection /* Statement */
DO:
  if not self:modified then return.
  publish "SetFileAttribute" (activeFileID,
                              "PremiumStatement", 
                              self:screen-value).
  if error-status:error 
   then self:screen-value = "".  
  publish "GetFileAttribute" (activeFileID,
                              "RateDifference", 
                              output std-ch).
  qRateDifference:screen-value = std-ch.

  publish "SaveAfterEveryAnswer" .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qRate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qRate C-Win
ON LEAVE OF qRate IN FRAME fSection /* Rate */
DO:
  if not self:modified then return.
  publish "SetFileAttribute" (activeFileID,
                              "Rate", 
                              self:screen-value).
  if error-status:error 
   then self:screen-value = "".  
  publish "GetFileAttribute" (activeFileID,
                              "RateDifference", 
                              output std-ch).
  qRateDifference:screen-value = std-ch.

  publish "SaveAfterEveryAnswer" .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qState C-Win
ON LEAVE OF qState IN FRAME fSection /* State */
DO:
  if not self:modified then return.
  publish "SetFileAttribute" (activeFileID,
                              "State", 
                              self:screen-value).
  if error-status:error 
   then self:screen-value = "".  

  publish "SaveAfterEveryAnswer" .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME tComments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tComments C-Win
ON LEAVE OF tComments IN FRAME fMain
DO:
  if not self:modified then return.
  publish "SetSectionAnswer" ("4", "Comments", self:screen-value).
  publish "SaveAfterEveryAnswer" .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tScore
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tScore C-Win
ON LEAVE OF tScore IN FRAME fMain
DO: 
  {lib/dispscore.i &s="4"}
  publish "SaveAfterEveryAnswer" .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

PAUSE 0 BEFORE-HIDE.

subscribe to "ShowTitle" anywhere.
{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.
subscribe to "LeaveAll" anywhere .
subscribe to "Close" anywhere.  
subscribe to "CheckSectionScore" anywhere.
run initializeFrame in this-procedure.
on 'value-changed':u anywhere
do:
  publish "EnableSave".
  publish "SetClickSave".
end.

ON 'leave':U OF q1405
  run dofillin(1405, q1405:screen-value). /*??*/

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  frame fSection:height-pixels = {&window-name}:height-pixels - 132.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActiveFileChanged C-Win 
PROCEDURE ActiveFileChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pID as int.
 
 activeFileID = pID.
 if activeFileID = 0
  then run auditClosed in this-procedure.
  else run auditOpened in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditClosed C-Win 
PROCEDURE AuditClosed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 assign
    qOwnerLiability:screen-value in frame fSection = ""
    qLender1Liability:screen-value in frame fSection = ""
    qLender2Liability:screen-value in frame fSection = ""
    qState:screen-value in frame fSection = ""
    qCounty:screen-value in frame fSection = ""
    qPremiumStatement:screen-value in frame fSection = ""
    qRate:screen-value in frame fSection = ""
    qRateDifference:screen-value in frame fSection = ""
    .
  
 assign
/*    q1395:screen-value = "" */
   q2r:checked = false
   q2pc:checked = false
   q2pl:checked = false
   q2l:checked = false
   q2o:checked = false
   q1405:screen-value = "" 
   q4si:checked = false
   q4so:checked = false
   q4sna:checked = false
   q4bi:checked = false
   q4bo:checked = false
   q4bna:checked = false
   .

 {lib/unsetquestion.i &seq=1400}
 {lib/unsetquestion.i &seq=1405}
 {lib/unsetquestion.i &seq=1410}
 {lib/unsetquestion.i &seq=1412}
 {lib/unsetquestion.i &seq=1415}
 {lib/unsetquestion.i &seq=1416}
 {lib/unsetquestion.i &seq=1419}
 {lib/unsetquestion.i &seq=1421}
 {lib/unsetquestion.i &seq=1420}
 {lib/unsetquestion.i &seq=1425}
 {lib/unsetquestion.i &seq=1430}
 {lib/unsetquestion.i &seq=1440}
 {lib/unsetquestion.i &seq=1445}
 {lib/unsetquestion.i &seq=1460}
 {lib/unsetquestion.i &seq=1465}
 {lib/unsetquestion.i &seq=1470}
 {lib/unsetquestion.i &seq=1480}
 {lib/unsetquestion.i &seq=1490}
 {lib/unsetquestion.i &seq=1500}
 {lib/unsetquestion.i &seq=1510}
 {lib/unsetquestion.i &seq=1515}
 {lib/unsetquestion.i &seq=1520}
 {lib/unsetquestion.i &seq=1530}
 {lib/unsetquestion.i &seq=1535}
 {lib/unsetquestion.i &seq=1545}
 {lib/unsetquestion.i &seq=1546}

 assign
   tComments:screen-value in frame fMain = ""
   tComments:sensitive in frame fMain = false
   tScore:sensitive in frame fMain = false
   tScore:screen-value in frame fMain = ""
   frame fSection:sensitive = false
   {&WINDOW-NAME}:title = "Section 4"
   .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditOpened C-Win 
PROCEDURE AuditOpened :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
                                                                                  
 /* Use to set button image correctly and get the answer in std-ch */                 
 assign                                                                               
   q2r:checked = false                                                                
   q2pc:checked = false                                                               
   q2pl:checked = false                                                               
   q2l:checked = false                                                                
   q2o:checked = false                                                                
   .                                                                                  
  {lib/dispfileanswer.i &seq=1400}                                                    
  case std-ch:                                                                        
    when "R" then q2r:checked  = true.                                                
    when "PC" then q2pc:checked = true.                                               
    when "PL" then q2PL:checked = true.                                               
    when "L" then q2L:checked = true.                                                 
    when "O" then q2o:checked = true.                                                 
  end case.                                                                           
                                                                                      
  {lib/dispfileanswer.i &seq=1405}                                                    
 assign                                                                               
   q4si:checked = false                                                               
   q4so:checked = false                                                               
   q4sna:checked = false                                                              
   .                                                                                  

  {lib/dispfileanswer.i &seq=1410} 
 case std-ch:
  when "I" then q4si:checked = true.
  when "O" then q4so:checked = true.
  when "A" then q4sna:checked = true.
 end case.

assign
   q4bi:checked = false
   q4bo:checked = false
   q4bna:checked = false
   .
{lib/dispfileanswer.i &seq=1412} 
 case std-ch:
  when "I" then q4bi:checked = true.
  when "O" then q4bo:checked = true.
  when "A" then q4bna:checked = true.
 end case.


 {lib/dispfileanswer.i &seq=1415} 
 {lib/dispfileanswer.i &seq=1416}
 {lib/dispfileanswer.i &seq=1419}
 {lib/dispfileanswer.i &seq=1421}
 {lib/dispfileanswer.i &seq=1420}
 {lib/dispfileanswer.i &seq=1425}
 {lib/dispfileanswer.i &seq=1430}
 {lib/dispfileanswer.i &seq=1440}
 {lib/dispfileanswer.i &seq=1445}
 {lib/dispfileanswer.i &seq=1460}
 {lib/dispfileanswer.i &seq=1465}
 {lib/dispfileanswer.i &seq=1470}
 {lib/dispfileanswer.i &seq=1480}
 {lib/dispfileanswer.i &seq=1490}
 {lib/dispfileanswer.i &seq=1500}
 {lib/dispfileanswer.i &seq=1510}
 {lib/dispfileanswer.i &seq=1515}
 {lib/dispfileanswer.i &seq=1520}
 {lib/dispfileanswer.i &seq=1530}
 {lib/dispfileanswer.i &seq=1535}
 {lib/dispfileanswer.i &seq=1545}
 {lib/dispfileanswer.i &seq=1546}

  
 publish "GetFileAttribute" (activeFileID, "State", output std-ch).
 qState:screen-value in frame fSection = std-ch.
 publish "GetFileAttribute" (activeFileID, "County", output std-ch).
 qCounty:screen-value in frame fSection = std-ch.
 
 publish "GetFileAttribute" (activeFileID, "OwnerLiability", output std-ch).
 qOwnerLiability:screen-value in frame fSection = std-ch.
 publish "GetFileAttribute" (activeFileID, "Lender1Liability", output std-ch).
 qLender1Liability:screen-value in frame fSection = std-ch.
 publish "GetFileAttribute" (activeFileID, "Lender2Liability", output std-ch).
 qLender2Liability:screen-value in frame fSection = std-ch.
 publish "GetFileAttribute" (activeFileID, "PremiumStatement", output std-ch).
 qPremiumStatement:screen-value in frame fSection = std-ch.
 publish "GetFileAttribute" (activeFileID, "Rate", output std-ch).
 qRate:screen-value in frame fSection = std-ch.
 publish "GetFileAttribute" (activeFileID, "RateDifference", output std-ch).
 qRateDifference:screen-value in frame fSection = std-ch.
  
 {lib/dispsection.i &id="4"}

  assign
    tComments:sensitive in frame fMain = true
    tScore:sensitive in frame fMain = true
    frame fSection:sensitive = true
    .
    
  apply 'leave' to qRate in frame fsection.
  

  publish "GetAuditStatus" (output pAuditStatus).
  if pAuditStatus = "C" then
    run AuditReadOnly.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditReadOnly C-Win 
PROCEDURE AuditReadOnly :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /*----------not used------------*/
 /*do with frame {&frame-name}:
 end.
 assign
   tComments:sensitive = false
   tScore:sensitive    = false.
 
 do with frame fSection :
 end.
 assign
   tComments:sensitive         = false
   tScore:sensitive            = false
   qState:sensitive            = false
   qCounty:sensitive           = false
   qOwnerLiability:sensitive   = false
   qLender1Liability:sensitive = false 
   qLender2Liability:sensitive = false 
   qPremiumStatement:sensitive = false 
   qRate:sensitive             = false
   qRateDifference:sensitive   = false
   q1395:sensitive             = false            
   q1385:sensitive             = false
   q2r:sensitive               = false
   q2pc:sensitive              = false
   q2pl:sensitive              = false
   q2l:sensitive               = false
   q2o:sensitive               = false
   q4s:sensitive               = false
   q4si:sensitive              = false
   q4so:sensitive              = false 
   q4sna:sensitive             = false
   q4b:sensitive               = false
   q4bi:sensitive              = false 
   q4bo:sensitive              = false
   q4bna:sensitive             = false
   .*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CheckSectionScore C-Win 
PROCEDURE CheckSectionScore :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input  parameter SectionNumber as int.
def output parameter SectionScore  as char.
if sectionnumber = integer(C-Win:private-data)  and ( tScore:screen-value in frame fMain ne "" ) then
  SectionScore = tScore:screen-value in frame fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Close C-Win 
PROCEDURE Close :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tautosave as logical no-undo.
  publish "GetAutosave" (output tautosave).
  if tautosave then                                          
    publish "SaveAfterEveryAnswer".
  apply 'CLOSE' to this-procedure .
  return no-apply .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doButton C-Win 
PROCEDURE doButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/dobutton.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doCheckbox C-Win 
PROCEDURE doCheckbox :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/docheckboxf.i}
  /*if pQuestionSeq = 1395  /*AC: change 1405 to 1395 */
    and focus:checked
  then
   do:*/
/*      case focus:name:                                                             */
/*         when "q2r" or when "q2pc" or when "q2pl" or when "q2l"                    */
/*          then                                                                     */
/*           do:                                                                     */
/*             publish "SetFileQuestionAnswer" (activeFileID,                        */
/*                                              1400,                                */
/*                                              "N/A"). /*AC: change 1405 to 1395 */ */
/*                                                                                   */
/*             q1400:read-only = true.                                               */
/*             if not error-status:error                                             */
/*              then q1400:screen-value = "N/A".                                     */
/*           end.                                                                    */
/*        when "q2O"                                                                 */
/*          then                                                                     */
/*        do:                                                                        */
/*          q1400:screen-value = "".                                                 */
/*          q1400:read-only = false.                                                 */
/*        end.                                                                       */
/*                                                                                   */
/*        end case.                                                                  */
       
/*    end. */

/*    publish "SaveAfterEveryAnswer" . */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doFillin C-Win 
PROCEDURE doFillin :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/dofillinf.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tScore tComments tScoreLabel 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE tScore tComments 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  DISPLAY qState qOwnerLiability qPremiumStatement qCounty qLender1Liability 
          qRate qLender2Liability qRateDifference q4-11 t4-14 
      WITH FRAME fSection IN WINDOW C-Win.
  ENABLE qState qOwnerLiability qPremiumStatement qCounty qLender1Liability 
         qRate qLender2Liability 
      WITH FRAME fSection IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fSection}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initializeFrame C-Win 
PROCEDURE initializeFrame PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
&global-define ex-frameresize true

  
  /* No default checkboxes on question 2 */
  {lib/question.i &seq=1400  &r="1.82" &ex-u=true &ex-y=true &ex-n=true &ex-a=true}
  
   create toggle-box q2R
    assign
      name = "q2r"
      private-data = "R"
      frame = frame fSection:handle
      column = 117.0
      row = 1.82 - 0.5
      width-chars = 6.0
      height-chars = 1.0
      sensitive = true
      visible = true
      label = "R"
      tooltip = "Refinance"
      tab-stop = true
      checked = false
      triggers:
       ON value-changed persistent run doCheckbox in this-procedure (1400).
      end triggers.
      .
   create toggleset.
   assign
     toggleset.seq = 1400 
     toggleset.togglebox = q2r
     .
   
   create toggle-box q2PC
    assign
      name = "q2pc"
      private-data = "PC"
      frame = frame fSection:handle
      column = 124.0
      row = 1.82 - 0.5
      width-chars = 6.0
      height-chars = 1.0
      sensitive = true
      visible = true
      label = "PC"
      tooltip = "Purchase - Cash"
      tab-stop = true
      checked = false
      triggers:
       ON value-changed persistent run doCheckbox in this-procedure (1400).
      end triggers.
   create toggleset.
   assign
     toggleset.seq = 1400
     toggleset.togglebox = q2PC
     .
   
   create toggle-box q2PL
    assign
      name = "q2pl"
      private-data = "PL" 
      frame = frame fSection:handle
      column = 131.0
      row = 1.82 - 0.5
      width-chars = 8.0
      height-chars = 1.0
      sensitive = true
      visible = true
      label = "PL"
      tooltip = "Purchase - Loan"
      tab-stop = true
      checked = false
      triggers:
       ON value-changed persistent run doCheckbox in this-procedure (1400).
      end triggers.
   create toggleset.
   assign
     toggleset.seq = 1400 
     toggleset.togglebox = q2PL
     .
   
   create toggle-box q2L
    assign
      name = "q2L"
      private-data = "L"
      frame = frame fSection:handle
      column = 140.0
      row = 1.82 - 0.5 
      width-chars = 6.0
      height-chars = 1.0
      sensitive = true
      visible = true
      label = "L"
      tooltip = "Loan"
      tab-stop = true
      checked = false
      triggers:
       ON value-changed persistent run doCheckbox in this-procedure (1400).
      end triggers.   
   create toggleset.
   assign
     toggleset.seq = 1400
     toggleset.togglebox = q2L
     .
   
   create toggle-box q2O
    assign
      name = "q2O"
      private-data = "O"
      frame = frame fSection:handle
      column = 124.0
      row = 1.82 + 0.5
      width-chars = 10.0
      height-chars = 1.0
      sensitive = true
      visible = true
      label = "Other"
      tooltip = "Other"
      tab-stop = true
      checked = false
      triggers:
       ON value-changed persistent run doCheckbox in this-procedure (1400).
      end triggers.   
   create toggleset.
   assign
     toggleset.seq = 1400
     toggleset.togglebox = q2O
     .

  {lib/questionf.i &seq=1405 &r="1.72 + 2.8"} 
  
  /* No default checkboxes on question 4 */
  {lib/question.i &seq=1410 &r="1.72 + 6" &ex-u=true &ex-y=true &ex-n=true &ex-a=true}
                  
   create text q4s
    assign
      name = "q4s"
      frame = frame fSection:handle
      column = 113
      row = 7.22
      width-chars = 8.0
      height-chars = 1.0
      sensitive = false
      visible = true
      screen-value = "Seller:"
      .

   create toggle-box q4si
    assign
      name = "q4si"
      private-data = "I"
      frame = frame fSection:handle
      column = 119.0
      row = 7.22
      width-chars = 6.0
      height-chars = 1.0
      sensitive = true
      visible = true
      label = "In"
      tooltip = "Seller - In office"
      tab-stop = true
      checked = false
      triggers:
       ON value-changed persistent run doCheckbox in this-procedure (1410).
      end triggers.
      .
   create toggleset.
   assign
     toggleset.seq = 1410
     toggleset.togglebox = q4si
     .
   
   create toggle-box q4so
    assign
      name = "q4so"
      private-data = "O"
      frame = frame fSection:handle
      column = 126.0 
      row = 7.22
      width-chars = 7.0
      height-chars = 1.0
      sensitive = true
      visible = true
      label = "Out"
      tooltip = "Seller - Out of office"
      tab-stop = true
      checked = false
      triggers:
       ON value-changed persistent run doCheckbox in this-procedure (1410).
      end triggers.
   create toggleset.
   assign
     toggleset.seq = 1410
     toggleset.togglebox = q4so
     .
      create toggle-box q4sna
    assign
      name = "q4sna"
      private-data = "A"
      frame = frame fSection:handle
      column = 133.0 
      row = 7.22
      width-chars = 8.0
      height-chars = 1.0
      sensitive = true
      visible = true
      label = "N/A"
      tooltip = " "
      tab-stop = true
      checked = false
      triggers:
       ON value-changed persistent run doCheckbox in this-procedure (1410).
      end triggers.
   create toggleset.
   assign
     toggleset.seq = 1410 
     toggleset.togglebox = q4sna
     .

   create text q4b
    assign
      name = "q4b"
      frame = frame fSection:handle
      format = "x(18)"
      column = 103
      row = 8.22
      width-chars = 18.0
      height-chars = 1.0
      sensitive = false
      visible = true
      screen-value ="Buyer/Borrower:"
      .

   create toggle-box q4bi
    assign
      name = "q4bi"
      private-data = "I"
      frame = frame fSection:handle
      column = 119.0  /*124 to 119*/
      row = 8.22
      width-chars = 6.0
      height-chars = 1.0
      sensitive = true
      visible = true
      label = "In"
      tooltip = "Buyer - In office"
      tab-stop = true
      checked = false
      triggers:
       ON value-changed persistent run doCheckbox in this-procedure (1412).
      end triggers.
      .
   create toggleset.
   assign
     toggleset.seq = 1412
     toggleset.togglebox = q4bi
     .

   create toggle-box q4bo
    assign
      name = "q4bo"
      private-data = "O"
      frame = frame fSection:handle
      column = 126.0  /*131 to 126 */
      row = 8.22
      width-chars =7.0
      height-chars = 1.0
      sensitive = true
      visible = true
      label = "Out"
      tooltip = "Buyer - Out of office"
      tab-stop = true
      checked = false
      triggers:
       ON value-changed persistent run doCheckbox in this-procedure (1412).
      end triggers.
   create toggleset.
   assign
     toggleset.seq = 1412
     toggleset.togglebox = q4bo
     .
      create toggle-box q4bna
    assign
      name = "q4bna"
      private-data = "A"
      frame = frame fSection:handle
      column = 133.0  /*131 to 126 */
      row = 8.22
      width-chars =8.0
      height-chars = 1.0
      sensitive = true
      visible = true
      label = "N/A"
      tooltip = " "
      tab-stop = true
      checked = false
      triggers:
       ON value-changed persistent run doCheckbox in this-procedure (1412).
      end triggers.
   create toggleset.
   assign
     toggleset.seq = 1412                                                                   
     toggleset.togglebox = q4bna                                                            
     .                                                                                      
                                                                                            
  {lib/question.i &seq=1415 &r="1.72 + 8.5"  &ex-u=true}                                      
  {lib/question.i &seq=1416 &r="1.72 + 11" &ex-u=true}                                    
  {lib/question.i &seq=1419 &r="1.72 + 13.5" &ex-u=true}                                    
  {lib/question.i &seq=1421 &r="1.72 + 16" &ex-u=true}                                    
  {lib/question.i &seq=1420 &r="1.72 + 18.5" &ex-u=true}                                    
  {lib/question.i &seq=1425 &r="1.72 + 21" &ex-u=true}                                    
  {lib/question.i &seq=1430 &r="1.72 + 23.5"   &ex-u=true}                                      
  {lib/question.i &seq=1440 &r="1.72 + 26" &ex-u=true}                                    
  {lib/question.i &seq=1445 &r="1.72 + 28.5" &ex-u=true}                                    
  {lib/question.i &seq=1460 &r="1.72 + 31" &ex-u=true}                                    
  {lib/question.i &seq=1465 &r="1.72 + 33.5" &ex-u=true}                                    
  {lib/question.i &seq=1470 &r="1.72 + 36" &ex-u=true}                                    
  {lib/question.i &seq=1480 &r="1.72 + 38.5" &ex-u=true}                                    
  {lib/question.i &seq=1490 &r="1.72 + 41" &ex-u=true}                                    
  {lib/question.i &seq=1500 &r="1.72 + 43.5" &ex-u=true}                                    
  {lib/question.i &seq=1510 &r="1.72 + 46" &ex-u=true}                                    
  {lib/question.i &seq=1515 &r="1.72 + 48.5"   &ex-u=true}                                      
  {lib/question.i &seq=1520 &r="1.72 + 51" &ex-u=true}                                    
  {lib/question.i &seq=1530 &r="1.72 + 53.5" &ex-u=true}                                    
  {lib/question.i &seq=1535 &r="1.72 + 61" &ex-u=true}                                    
  {lib/question.i &seq=1545 &r="1.72 + 63.5" &ex-u=true}                                    
  {lib/question.i &seq=1546 &r="1.72 + 66" &ex-u=true &ex-r=true}                         
                                                                             
  {lib/dispsectiont.i &id="4"}                                               
                                                                             
  RUN ShowScrollBars(FRAME fSection:HANDLE, NO, YES).
  
  run AuditOpened in this-procedure.    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LeaveAll C-Win 
PROCEDURE LeaveAll :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 apply "leave" to tComments in frame {&frame-name}.
 apply "leave" to tScore in frame {&frame-name}.
 apply "leave" to qState in frame fSection.
 apply "leave" to qCounty in frame fSection.
 apply "leave" to qOwnerLiability in frame fSection.
 apply "leave" to qLender1Liability in frame fSection.
 apply "leave" to qLender2Liability in frame fSection.
 apply "leave" to qPremiumStatement in frame fSection.
 apply "leave" to qRate in frame fSection.

 apply "leave" to q1405.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowTitle C-Win 
PROCEDURE ShowTitle :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter fname as character .
  if fname = ? and activeFileID = 0 then
    assign {&WINDOW-NAME}:title = "Section 4 " .
  else
    assign {&WINDOW-NAME}:title = "Section 4 File - " + fname .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var tFlag as logical.
  tFlag = tComments:sensitive in frame fMain.
  tComments:sensitive in frame fMain  = true.
  apply "ENTRY" to tComments in frame fMain.
  tComments:sensitive in frame fMain = tFlag.
  if {&window-name}:window-state eq window-minimized  then
   {&window-name}:window-state = window-normal .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.

 title1:width = frame fMain:width - 26.
 tScoreLabel:X = frame fMain:width-pixels - 100.
 tScore:X = frame fMain:width-pixels - 62.
 tComments:width-pixels = frame fMain:width-pixels - 115.

 if {&window-name}:width-pixels > frame fSection:width-pixels 
  then
   do:
     frame fSection:width-pixels = {&window-name}:width-pixels.
     frame fSection:virtual-width-pixels = {&window-name}:width-pixels.
   end.
  else
   do:
     frame fSection:virtual-width-pixels = {&window-name}:width-pixels.
     frame fSection:width-pixels = {&window-name}:width-pixels.
         /* das: For some reason, shrinking a window size MAY cause the horizontal
            scroll bar.  The above sequence of widget setting should resolve it,
            but it doesn't every time.  So... */
     RUN ShowScrollBars(FRAME fSection:HANDLE, NO, YES).
   end.

 frame fSection:height-pixels = {&window-name}:height-pixels - 132.
 
   {lib/dispsectiont.i &id="4"}

 {lib/resizequestion.i &seq=1400}
 {lib/resizequestion.i &seq=1405}
 {lib/resizequestion.i &seq=1410}
 {lib/resizequestion.i &seq=1415}
 {lib/resizequestion.i &seq=1416}
 {lib/resizequestion.i &seq=1419}
 {lib/resizequestion.i &seq=1421}
 {lib/resizequestion.i &seq=1420}
 {lib/resizequestion.i &seq=1425}
 {lib/resizequestion.i &seq=1430}
 {lib/resizequestion.i &seq=1440}
 {lib/resizequestion.i &seq=1445}
 {lib/resizequestion.i &seq=1460}
 {lib/resizequestion.i &seq=1465}
 {lib/resizequestion.i &seq=1470}
 {lib/resizequestion.i &seq=1480}
 {lib/resizequestion.i &seq=1490}
 {lib/resizequestion.i &seq=1500}
 {lib/resizequestion.i &seq=1510}
 {lib/resizequestion.i &seq=1515}
 {lib/resizequestion.i &seq=1520}
 {lib/resizequestion.i &seq=1530}
 {lib/resizequestion.i &seq=1535}
 {lib/resizequestion.i &seq=1545}
 {lib/resizequestion.i &seq=1546}

 assign q2R:x  = frame fSection:width-pixels - 200
        q2PC:x = frame fSection:width-pixels - 165
        q2PL:x = frame fSection:width-pixels - 130
        q2L:x  = frame fSection:width-pixels - 85
        q2O:x  = frame fSection:width-pixels - 165
        q4s:x  = frame fSection:width-pixels - 235
        q4si:x = frame fSection:width-pixels - 200
        q4so:x = frame fSection:width-pixels - 165
        q4sna:x = frame fSection:width-pixels - 130
        q4b:x  = frame fSection:width-pixels - 280
        q4bi:x = frame fSection:width-pixels - 200
        q4bo:x = frame fSection:width-pixels - 165
        q4bna:x = frame fSection:width-pixels - 130
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

