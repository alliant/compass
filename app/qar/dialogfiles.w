&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/* dialogfiles.w
  
Modification:
  Date          Name      Description
  01/16/2017    AG        Enable save button if any change is done to 
                          list of agent files.
  02/14/2017    AC        Implement local inspect functionality.
  09/11/2017    AG        Alternate color code in browse.
------------------------------------------------------------------------*/
def input-output parameter pActiveFile as int.
def output parameter pList as char.
def var pAuditStatus as char.

{lib/std-def.i}
{tt/qarauditfile.i}
def temp-table activefile like agentFile.

/* Local Variable Definitions ---                                       */
def buffer newFile for agentFile.
def var iBgcolor as int no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME bFiles

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES agentFile

/* Definitions for BROWSE bFiles                                        */
&Scoped-define FIELDS-IN-QUERY-bFiles agentFile.fileNumber agentFile.state agentFile.county   
&Scoped-define ENABLED-FIELDS-IN-QUERY-bFiles   
&Scoped-define SELF-NAME bFiles
&Scoped-define QUERY-STRING-bFiles FOR EACH agentFile by agentFile.fileNumber. run refreshBrowse in this-procedure
&Scoped-define OPEN-QUERY-bFiles OPEN QUERY bFiles FOR EACH agentFile by agentFile.fileNumber. run refreshBrowse in this-procedure.
&Scoped-define TABLES-IN-QUERY-bFiles agentFile
&Scoped-define FIRST-TABLE-IN-QUERY-bFiles agentFile


/* Definitions for DIALOG-BOX Dialog-Frame                              */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bAdd bFiles bCopy bEdit bDelete Btn_OK ~
Btn_Cancel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAdd 
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add a new agency file"
     BGCOLOR 15 .

DEFINE BUTTON bCopy 
     LABEL "Copy..." 
     SIZE 7.2 BY 1.71 TOOLTIP "Copy the county/state to a new agency file"
     BGCOLOR 15 .

DEFINE BUTTON bDelete 
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete the selected agency file".

DEFINE BUTTON bEdit 
     LABEL "Edit..." 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify the selected agency file"
     BGCOLOR 15 .

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 14 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 14 BY 1.14
     BGCOLOR 8 .

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY bFiles FOR 
      agentFile SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE bFiles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS bFiles Dialog-Frame _FREEFORM
  QUERY bFiles DISPLAY
      agentFile.fileNumber label "Agent File" format "x(40)"
      agentFile.state label "State"
      agentFile.county label "County"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS NO-COLUMN-SCROLLING SEPARATORS SIZE 81 BY 17.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bAdd AT ROW 1.81 COL 83.8 WIDGET-ID 2
     bFiles AT ROW 1.86 COL 2 WIDGET-ID 200
     bCopy AT ROW 3.67 COL 83.8 WIDGET-ID 20
     bEdit AT ROW 5.52 COL 83.8 WIDGET-ID 10
     bDelete AT ROW 7.38 COL 83.8 WIDGET-ID 4
     Btn_OK AT ROW 20.14 COL 35
     Btn_Cancel AT ROW 20.14 COL 51
     "Files" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.14 COL 2.4 WIDGET-ID 8
     SPACE(83.39) SKIP(19.85)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Agency Files"
         DEFAULT-BUTTON bAdd CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
/* BROWSE-TAB bFiles bAdd Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       bFiles:ALLOW-COLUMN-SEARCHING IN FRAME Dialog-Frame = TRUE
       bFiles:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE bFiles
/* Query rebuild information for BROWSE bFiles
     _START_FREEFORM
OPEN QUERY bFiles FOR EACH agentFile by agentFile.fileNumber.
run refreshBrowse in this-procedure.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE bFiles */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Agency Files */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAdd Dialog-Frame
ON CHOOSE OF bAdd IN FRAME Dialog-Frame /* New */
DO:
  run addFile in this-procedure ("", "").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCopy Dialog-Frame
ON CHOOSE OF bCopy IN FRAME Dialog-Frame /* Copy... */
DO:
  if available agentFile 
   then run addFile in this-procedure (agentFile.state, agentFile.county).
   else run addFile in this-procedure ("", "").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete Dialog-Frame
ON CHOOSE OF bDelete IN FRAME Dialog-Frame /* Delete */
DO:
   def var tOk as logical.
   def var tText1 as char.
   def var tText2 as char.
   
   if bFiles:num-selected-rows = 0 then return.
   bFiles:fetch-selected-row(1).
   get current bFiles exclusive-lock.
   tText1 = "File: " + agentFile.fileNumber + " will be removed.".
   tText2 = (if agentFile.answered > 0 then "All answers will be removed." else "").
   MESSAGE tText1 skip(1) tText2
    VIEW-AS ALERT-BOX warning BUTTONS ok-cancel update tOk.
   if tOk
    then 
     do: delete agentFile.
         bFiles:delete-selected-rows().
     end.

   publish "EnableSave".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEdit Dialog-Frame
ON CHOOSE OF bEdit IN FRAME Dialog-Frame /* Edit... */
DO:
  if available agentFile
    then run updateFile in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME bFiles
&Scoped-define SELF-NAME bFiles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFiles Dialog-Frame
ON DEFAULT-ACTION OF bFiles IN FRAME Dialog-Frame
DO:
  if available agentFile 
   then run updateFile in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFiles Dialog-Frame
ON ROW-DISPLAY OF bFiles IN FRAME Dialog-Frame
DO:
  {lib/brw-rowdisplay.i}

  if current-result-row("bFiles") modulo 2 = 0 then
    iBgColor = 17.
  else
    iBgColor = 15.
  agentFile.fileNumber:bgcolor in browse bFiles = iBgColor.
  agentFile.state:bgcolor in browse bFiles      = iBgColor.
  agentFile.county:bgcolor in browse bFiles     = iBgColor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFiles Dialog-Frame
ON ROW-LEAVE OF bFiles IN FRAME Dialog-Frame
DO:
 def var tSeq as int.
 IF bFiles:NEW-ROW IN FRAME {&frame-name} 
  THEN 
   DO: if input browse bFiles agentFile.fileNumber = ""
        then 
         do: close query bFiles.
             open query bFiles for each agentFile by agentFile.fileNumber.
             return.
         end.
       DO ON ERROR UNDO, RETURN NO-APPLY:
         if can-find(newFile where newFile.fileNumber = input browse bFiles agentFile.fileNumber)
          then
           do:
               MESSAGE "File Number must be unique"
                VIEW-AS ALERT-BOX INFO BUTTONS OK.
               return no-apply.
           end.
         find last newFile no-error.
         tSeq = if available newFile then newFile.fileID + 1 else 1.

         CREATE agentFile.
         ASSIGN input browse bFiles agentFile.fileNumber.
         agentFile.fileID = tSeq.
         bFiles:CREATE-RESULT-LIST-ENTRY().
       END.
   end.
  else 
   do:
    if can-find(newFile 
         where newFile.fileNumber = input browse bFiles agentFile.fileNumber
           and newFile.fileID <> agentFile.fileID)
     then
      do:
          MESSAGE "File Number must be unique"
           VIEW-AS ALERT-BOX INFO BUTTONS OK.
          return no-apply.
      end.
      if available agentFile and input browse bFiles agentFile.fileNumber = "" 
       then
      do:
       MESSAGE "File Number cannot be blank"
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
       return no-apply.
      end.
   end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFiles Dialog-Frame
ON START-SEARCH OF bFiles IN FRAME Dialog-Frame
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFiles Dialog-Frame
ON VALUE-CHANGED OF bFiles IN FRAME Dialog-Frame
DO:
  publish "GetAuditStatus" (output pAuditStatus).
  if pAuditStatus = "C" then 
    return.
  
  bCopy:sensitive in frame {&frame-name} = available agentFile.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bAdd:load-image("images/add.bmp").
bAdd:load-image-insensitive("images/add-i.bmp").
bCopy:load-image("images/split.bmp").
bCopy:load-image-insensitive("images/split-i.bmp").
bEdit:load-image("images/update.bmp").
bEdit:load-image-insensitive("images/update-i.bmp").
bDelete:load-image("images/delete.bmp").
bDelete:load-image-insensitive("images/delete-i.bmp").


publish "GetAgentFiles" (output table activeFile).
for each activeFile
  where activeFile.fileID < 1000:
 create agentFile.
 buffer-copy activeFile to agentFile.
end.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  run refreshBrowse.
  
  publish "GetAuditStatus" (output pAuditStatus).
  
  if pAuditStatus = "C"then
    run AuditReadOnly.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  
  /* Remove "deleted" files from datastore */
  for each activeFile
    where activeFile.fileID < 1000:
   if can-find(agentFile where agentFile.fileID = activeFile.fileID)
    then next.
   publish "DeleteAgentFile" (activeFile.fileID).
   delete activeFile.
  end.

  for each agentFile:
    publish "AddAgentFile" (agentFile.fileID,
                       agentFile.fileNumber,
                       agentFile.state,
                       agentFile.county,
                       agentFile.ownerLiability,
                       agentFile.ownerPolicy,
                       agentFile.lender1Liability,
                       agentFile.lender1Policy,
                       agentFile.lender2Liability,
                       agentFile.lender2Policy,
                       agentFile.premiumStatement,
                       agentFile.rate,
                       agentFile.delta,
                       agentFile.originalDate,
                       agentFile.preliminaryDate,
                       agentFile.updateDate,
                       agentFile.finalDate,
                       agentFile.closingDate,
                       agentFile.fundingDate,
                       agentFile.recordingDate,
                       agentFile.ownerDeliveryDate,
                       agentFile.lender1DeliveryDate,
                       agentFile.lender2DeliveryDate).
  end.
END.

pList = "".
std-in = 0.

publish "GetAgentFiles" (output table activeFile).
for each activeFile
  where activeFile.fileID < 1000
     by activeFile.fileNumber:
 pList = pList + (if pList > "" then "|" else "")
             + activeFile.fileNumber
             + "|" + string(activeFile.fileID).
 if activeFile.fileID = pActiveFile
  then std-in = pActiveFile.
end.
  publish "GetAuditStatus" (output pAuditStatus).
  if pAuditStatus = "C"then
    run AuditReadOnly.
if pList = "" 
 then pList = "|0". /* No files */
if std-in = 0 /* Existing file has been deleted, assign to first */
 then pActiveFile = int(entry(2, pList, "|")).

RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addFile Dialog-Frame 
PROCEDURE addFile PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pState as char.
def input parameter pCounty as char.

def var tFileNumber as char no-undo.
def var tState as char no-undo.
def var tCounty as char no-undo.
def var tOwnerLiability as decimal no-undo.
def var tOwnerPolicy as char no-undo.
def var tLender1Liability as decimal no-undo.
def var tLender1Policy as char no-undo.
def var tLender2Liability as decimal no-undo.
def var tLender2Policy as char no-undo.
def var tPremiumStatement as decimal no-undo.
def var tRate as decimal no-undo.
def var tDelta as decimal no-undo.
def var tOriginalDate as date no-undo.
def var tPreliminaryDate as date no-undo.
def var tUpdateDate as date no-undo.
def var tFinalDate as date no-undo.
def var tClosingDate as date no-undo.
def var tFundingDate as date no-undo.
def var tRecordingDate as date no-undo.
def var tOwnerDeliveryDate as date no-undo.
def var tLender1DeliveryDate as date no-undo.
def var tLender2DeliveryDate as date no-undo.

def var tSeq as int no-undo.

assign
  tState = pState
  tCounty = pCounty
  .

repeat:
 run dialogupdfile.w 
   ("Add",
    input-output tFileNumber,
    input-output tstate,
    input-output tcounty,
    input-output townerLiability,
    input-output townerPolicy,
    input-output tlender1Liability,
    input-output tlender1Policy,
    input-output tlender2Liability,
    input-output tlender2Policy,
    input-output tpremiumStatement,
    input-output trate,
    input-output tdelta,
    input-output toriginalDate,
    input-output tpreliminaryDate,
    input-output tupdateDate,
    input-output tfinalDate,
    input-output tclosingDate,
    input-output tfundingDate,
    input-output trecordingDate,
    input-output townerDeliveryDate,
    input-output tlender1DeliveryDate,
    input-output tlender2DeliveryDate,
    output std-lo).
 if std-lo 
  then leave.

 if can-find(agentFile where agentFile.fileNumber = tFileNumber)
  then
   do: 
       MESSAGE "File Number must be unique."
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
       next.
   end.
 if tFileNumber = ""
  then
   do: 
       MESSAGE "File Number cannot be blank."
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
       next.
   end.

 find last newFile no-error.
 tSeq = if available newFile then newFile.fileID + 1 else 1.

 create newFile.
 assign
  newFile.fileNumber = tFileNumber
  newFile.stateID = tstate
  newFile.countyID = tcounty
  newFile.ownerLiability = townerLiability
  newFile.ownerPolicy = integer(townerPolicy)
  newFile.lender1Liability = tlender1Liability
  newFile.lender1Policy = integer(tlender1Policy)
  newFile.lender2Liability = tlender2Liability
  newFile.lender2Policy = integer(tlender2Policy)
  newFile.premiumStatement = tpremiumStatement
  newFile.rate = trate
  newFile.delta = tdelta
  newFile.originalDate = toriginalDate
  newFile.preliminaryDate = tpreliminaryDate
  newFile.updateDate = tupdateDate
  newFile.finalDate = tfinalDate
  newFile.closingDate = tclosingDate
  newFile.fundingDate = tfundingDate
  newFile.recordingDate = trecordingDate
  newFile.ownerDeliveryDate = townerDeliveryDate
  newFile.lender1DeliveryDate = tlender1DeliveryDate
  newFile.lender2DeliveryDate = tlender2DeliveryDate
  newFile.fileID = tSeq
  .
 release newFile.

 run refreshBrowse in this-procedure.
 
 publish "EnableSave".

 leave.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditReadOnly Dialog-Frame 
PROCEDURE AuditReadOnly :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
do with frame Dialog-Frame:
end.
assign
  bAdd:sensitive = false
  bCopy:sensitive = false
  bEdit:sensitive = false
  bDelete:sensitive = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE bAdd bFiles bCopy bEdit bDelete Btn_OK Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshBrowse Dialog-Frame 
PROCEDURE refreshBrowse PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 close query bFiles.
 OPEN QUERY bFiles FOR EACH agentFile by agentFile.fileNumber.
 bEdit:sensitive in frame {&frame-name} = available agentFile.
 bDelete:sensitive in frame {&frame-name} = available agentFile.
 bCopy:sensitive in frame {&frame-name} = available agentFile.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData Dialog-Frame 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE updateFile Dialog-Frame 
PROCEDURE updateFile PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var tFileNumber as char no-undo.
def var tState as char no-undo.
def var tCounty as char no-undo.
def var tOwnerLiability as decimal no-undo.
def var tOwnerPolicy as char no-undo.
def var tLender1Liability as decimal no-undo.
def var tLender1Policy as char no-undo.
def var tLender2Liability as decimal no-undo.
def var tLender2Policy as char no-undo.
def var tPremiumStatement as decimal no-undo.
def var tRate as decimal no-undo.
def var tDelta as decimal no-undo.
def var tOriginalDate as date no-undo.
def var tPreliminaryDate as date no-undo.
def var tUpdateDate as date no-undo.
def var tFinalDate as date no-undo.
def var tClosingDate as date no-undo.
def var tFundingDate as date no-undo.
def var tRecordingDate as date no-undo.
def var tOwnerDeliveryDate as date no-undo.
def var tLender1DeliveryDate as date no-undo.
def var tLender2DeliveryDate as date no-undo.

def buffer duplicateFile for agentFile.

find newFile where newFile.fileID = agentFile.fileID.
assign
  tFileNumber = newFile.fileNumber
  tState = newFile.stateID
  tCounty = newFile.countyID
  tOwnerLiability = newFile.ownerLiability
  tOwnerPolicy = string(newfile.ownerPolicy)
  tLender1Liability = newFile.lender1Liability
  tLender1Policy = string(newFile.lender1Policy)
  tLender2Liability = newFile.lender2Liability
  tLender2Policy = string(newFile.lender2Policy)
  tPremiumStatement = newfile.premiumStatement
  tRate = newFile.rate
  tDelta = newFile.delta
  tOriginalDate = newFile.originalDate
  tPreliminaryDate = newFile.preliminaryDate
  tUpdateDate = newfile.updateDate
  tFinalDate = newfile.finalDate
  tClosingDate = newfile.closingDate
  tFundingDate = newFile.fundingDate
  tRecordingDate = newFile.recordingDate
  tOwnerDeliveryDate = newFile.ownerDeliveryDate
  tLender1DeliveryDate = newFile.lender1DeliveryDate
  tLender2DeliveryDate = newFile.lender2DeliveryDate
  .
repeat:
 run dialogupdfile.w 
   ("Modify",
    input-output tFileNumber,
    input-output tstate,
    input-output tcounty,
    input-output townerLiability,
    input-output townerPolicy,
    input-output tlender1Liability,
    input-output tlender1Policy,
    input-output tlender2Liability,
    input-output tlender2Policy,
    input-output tpremiumStatement,
    input-output trate,
    input-output tdelta,
    input-output toriginalDate,
    input-output tpreliminaryDate,
    input-output tupdateDate,
    input-output tfinalDate,
    input-output tclosingDate,
    input-output tfundingDate,
    input-output trecordingDate,
    input-output townerDeliveryDate,
    input-output tlender1DeliveryDate,
    input-output tlender2DeliveryDate,
    output std-lo).
 if std-lo 
  then leave.

 if can-find(duplicateFile where duplicateFile.fileNumber = tFileNumber 
                             and duplicateFile.fileID <> newFile.fileID)
  then
   do: 
       MESSAGE "File Number must be unique."
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
       next.
   end.
 if tFileNumber = ""
  then
   do: 
       MESSAGE "File Number cannot be blank."
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
       next.
   end.

 assign
  newFile.fileNumber = tFileNumber
  newFile.stateID = tstate
  newFile.countyID = tcounty
  newFile.ownerLiability = townerLiability
  newFile.ownerPolicy = integer(townerPolicy)
  newFile.lender1Liability = tlender1Liability
  newFile.lender1Policy = integer(tlender1Policy)
  newFile.lender2Liability = tlender2Liability
  newFile.lender2Policy = integer(tlender2Policy)
  newFile.premiumStatement = tpremiumStatement
  newFile.rate = trate
  newFile.delta = tdelta
  newFile.originalDate = toriginalDate
  newFile.preliminaryDate = tpreliminaryDate
  newFile.updateDate = tupdateDate
  newFile.finalDate = tfinalDate
  newFile.closingDate = tclosingDate
  newFile.fundingDate = tfundingDate
  newFile.recordingDate = trecordingDate
  newFile.ownerDeliveryDate = townerDeliveryDate
  newFile.lender1DeliveryDate = tlender1DeliveryDate
  newFile.lender2DeliveryDate = tlender2DeliveryDate
  .
 release newFile.

 run refreshBrowse in this-procedure.

 publish "EnableSave".
 leave.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

