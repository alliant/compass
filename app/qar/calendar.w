&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: adc/calendar.w

  Description: modal CALENDAR control

  Input-output Parameters:  <date>

  Author: D.Sinclair

  Created: 8.1.2007
*/

create widget-pool.

def var pDate as date.

def var curMonth as int.
def var curYear as int.

def var tMonthDesc as char
  init "  January,  February,   March,    April,    May,    June,    July,  August,September,~
  October, November, December" no-undo.
def var tButtons as handle extent 37.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME dCal

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bClear RECT-2 bNextMo bNextYr bPreMo bPreYr ~
BUTTON-10 BUTTON-11 BUTTON-12 BUTTON-13 BUTTON-14 BUTTON-15 BUTTON-7 ~
BUTTON-8 BUTTON-9 BUTTON-16 BUTTON-17 BUTTON-18 BUTTON-19 BUTTON-20 ~
BUTTON-21 BUTTON-22 BUTTON-23 BUTTON-24 BUTTON-25 BUTTON-26 BUTTON-27 ~
BUTTON-28 
&Scoped-Define DISPLAYED-OBJECTS tDate1 tDate2 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearDates C-Win 
FUNCTION clearDates RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFirstDow C-Win 
FUNCTION getFirstDow RETURNS INTEGER
  ( )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getNumDays C-Win 
FUNCTION getNumDays RETURNS INTEGER
  ( )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resetDisplay C-Win 
FUNCTION resetDisplay RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setDate C-Win 
FUNCTION setDate RETURNS LOGICAL PRIVATE
  ( input pLabel as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bClear  NO-FOCUS
     LABEL "Clear" 
     SIZE 8 BY .91 TOOLTIP "Clear Gap Calculation".

DEFINE BUTTON bFri  NO-FOCUS
     LABEL "Fr" 
     SIZE 5 BY .86.

DEFINE BUTTON bMon  NO-FOCUS
     LABEL "Mo" 
     SIZE 5 BY .86.

DEFINE BUTTON bNextMo  NO-FOCUS
     LABEL ">" 
     SIZE 4 BY 1 TOOLTIP "Next Month".

DEFINE BUTTON bNextYr  NO-FOCUS
     LABEL ">>" 
     SIZE 4 BY 1 TOOLTIP "Next Year".

DEFINE BUTTON bPreMo  NO-FOCUS
     LABEL "<" 
     SIZE 4 BY 1 TOOLTIP "Previous Month".

DEFINE BUTTON bPreYr  NO-FOCUS
     LABEL "<<" 
     SIZE 4 BY 1 TOOLTIP "Previous Year".

DEFINE BUTTON bSat  NO-FOCUS
     LABEL "Sa" 
     SIZE 5 BY .86.

DEFINE BUTTON bSun  NO-FOCUS
     LABEL "Su" 
     SIZE 5 BY .86.

DEFINE BUTTON bThu  NO-FOCUS
     LABEL "Th" 
     SIZE 5 BY .86.

DEFINE BUTTON bToday  NO-FOCUS
     LABEL "Today" 
     SIZE 8 BY .91 TOOLTIP "Go to the current date".

DEFINE BUTTON bTue  NO-FOCUS
     LABEL "Tu" 
     SIZE 5 BY .86.

DEFINE BUTTON BUTTON-1  NO-FOCUS FLAT-BUTTON
     LABEL "1" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-10  NO-FOCUS FLAT-BUTTON
     LABEL "10" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-11  NO-FOCUS FLAT-BUTTON
     LABEL "11" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-12  NO-FOCUS FLAT-BUTTON
     LABEL "12" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-13  NO-FOCUS FLAT-BUTTON
     LABEL "13" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-14  NO-FOCUS FLAT-BUTTON
     LABEL "14" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-15  NO-FOCUS FLAT-BUTTON
     LABEL "15" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-16  NO-FOCUS FLAT-BUTTON
     LABEL "16" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-17  NO-FOCUS FLAT-BUTTON
     LABEL "17" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-18  NO-FOCUS FLAT-BUTTON
     LABEL "18" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-19  NO-FOCUS FLAT-BUTTON
     LABEL "19" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-2  NO-FOCUS FLAT-BUTTON
     LABEL "2" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-20  NO-FOCUS FLAT-BUTTON
     LABEL "20" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-21  NO-FOCUS FLAT-BUTTON
     LABEL "21" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-22  NO-FOCUS FLAT-BUTTON
     LABEL "22" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-23  NO-FOCUS FLAT-BUTTON
     LABEL "23" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-24  NO-FOCUS FLAT-BUTTON
     LABEL "24" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-25  NO-FOCUS FLAT-BUTTON
     LABEL "25" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-26  NO-FOCUS FLAT-BUTTON
     LABEL "26" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-27  NO-FOCUS FLAT-BUTTON
     LABEL "27" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-28  NO-FOCUS FLAT-BUTTON
     LABEL "28" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-29  NO-FOCUS FLAT-BUTTON
     LABEL "29" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-3  NO-FOCUS FLAT-BUTTON
     LABEL "3" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-30  NO-FOCUS FLAT-BUTTON
     LABEL "30" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-31  NO-FOCUS FLAT-BUTTON
     LABEL "31" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-32  NO-FOCUS FLAT-BUTTON
     LABEL "32" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-33  NO-FOCUS FLAT-BUTTON
     LABEL "33" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-34  NO-FOCUS FLAT-BUTTON
     LABEL "34" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-35  NO-FOCUS FLAT-BUTTON
     LABEL "35" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-36  NO-FOCUS FLAT-BUTTON
     LABEL "36" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-37  NO-FOCUS FLAT-BUTTON
     LABEL "37" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-4  NO-FOCUS FLAT-BUTTON
     LABEL "4" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-5  NO-FOCUS FLAT-BUTTON
     LABEL "5" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-6  NO-FOCUS FLAT-BUTTON
     LABEL "6" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-7  NO-FOCUS FLAT-BUTTON
     LABEL "7" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-8  NO-FOCUS FLAT-BUTTON
     LABEL "8" 
     SIZE 5 BY 1.1.

DEFINE BUTTON BUTTON-9  NO-FOCUS FLAT-BUTTON
     LABEL "9" 
     SIZE 5 BY 1.1.

DEFINE BUTTON bWed  NO-FOCUS
     LABEL "We" 
     SIZE 5 BY .86.

DEFINE VARIABLE tCalGap AS INTEGER FORMAT ">>>":U INITIAL 0 
     LABEL "Calendar Days" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 NO-UNDO.

DEFINE VARIABLE tDate1 AS DATE FORMAT "99/99/99":U 
      VIEW-AS TEXT 
     SIZE 10.6 BY .62 NO-UNDO.

DEFINE VARIABLE tDate2 AS DATE FORMAT "99/99/99":U 
      VIEW-AS TEXT 
     SIZE 10.6 BY .62 NO-UNDO.

DEFINE VARIABLE tMonth AS CHARACTER FORMAT "X(12)":U 
      VIEW-AS TEXT 
     SIZE 13.8 BY .86
     FONT 6 NO-UNDO.

DEFINE VARIABLE tWorkGap AS INTEGER FORMAT ">>>":U INITIAL 0 
     LABEL "Work days" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 NO-UNDO.

DEFINE VARIABLE tYear AS INTEGER FORMAT "zzzz":U INITIAL 0 
      VIEW-AS TEXT 
     SIZE 8 BY .62
     FONT 6 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 34.6 BY 3.81.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME dCal
     bClear AT ROW 13.71 COL 27.2 WIDGET-ID 120
     tCalGap AT ROW 11.48 COL 27 COLON-ALIGNED WIDGET-ID 116
     tWorkGap AT ROW 12.52 COL 27 COLON-ALIGNED WIDGET-ID 118
     bFri AT ROW 3.24 COL 26 WIDGET-ID 4 NO-TAB-STOP 
     bMon AT ROW 3.24 COL 6 WIDGET-ID 6 NO-TAB-STOP 
     bNextMo AT ROW 1.52 COL 28 WIDGET-ID 8
     bNextYr AT ROW 1.52 COL 32 WIDGET-ID 10
     bPreMo AT ROW 1.52 COL 5.4 WIDGET-ID 12
     bPreYr AT ROW 1.52 COL 1.4 WIDGET-ID 14
     bSat AT ROW 3.24 COL 31 WIDGET-ID 16 NO-TAB-STOP 
     bSun AT ROW 3.24 COL 1 WIDGET-ID 18 NO-TAB-STOP 
     bThu AT ROW 3.24 COL 21 WIDGET-ID 20 NO-TAB-STOP 
     bToday AT ROW 9.81 COL 27.2 WIDGET-ID 22
     bTue AT ROW 3.24 COL 11 WIDGET-ID 24 NO-TAB-STOP 
     bWed AT ROW 3.24 COL 16 WIDGET-ID 100 NO-TAB-STOP 
     BUTTON-1 AT ROW 4.1 COL 1 WIDGET-ID 26 NO-TAB-STOP 
     BUTTON-2 AT ROW 4.1 COL 6 WIDGET-ID 48 NO-TAB-STOP 
     BUTTON-29 AT ROW 8.52 COL 1 WIDGET-ID 68 NO-TAB-STOP 
     BUTTON-3 AT ROW 4.1 COL 11 WIDGET-ID 70 NO-TAB-STOP 
     BUTTON-30 AT ROW 8.52 COL 6 WIDGET-ID 72 NO-TAB-STOP 
     BUTTON-31 AT ROW 8.52 COL 11 WIDGET-ID 74 NO-TAB-STOP 
     BUTTON-32 AT ROW 8.52 COL 16 WIDGET-ID 76 NO-TAB-STOP 
     BUTTON-33 AT ROW 8.52 COL 21 WIDGET-ID 78 NO-TAB-STOP 
     BUTTON-34 AT ROW 8.52 COL 26 WIDGET-ID 80 NO-TAB-STOP 
     BUTTON-35 AT ROW 8.52 COL 31 WIDGET-ID 82 NO-TAB-STOP 
     BUTTON-36 AT ROW 9.62 COL 1 WIDGET-ID 84 NO-TAB-STOP 
     BUTTON-37 AT ROW 9.62 COL 6 WIDGET-ID 86 NO-TAB-STOP 
     BUTTON-4 AT ROW 4.1 COL 16 WIDGET-ID 88 NO-TAB-STOP 
     BUTTON-5 AT ROW 4.1 COL 21 WIDGET-ID 90 NO-TAB-STOP 
     BUTTON-6 AT ROW 4.1 COL 26 WIDGET-ID 92 NO-TAB-STOP 
     BUTTON-10 AT ROW 5.19 COL 11 WIDGET-ID 28 NO-TAB-STOP 
     BUTTON-11 AT ROW 5.19 COL 16 WIDGET-ID 30 NO-TAB-STOP 
     BUTTON-12 AT ROW 5.19 COL 21 WIDGET-ID 32 NO-TAB-STOP 
     BUTTON-13 AT ROW 5.19 COL 26 WIDGET-ID 34 NO-TAB-STOP 
     BUTTON-14 AT ROW 5.19 COL 31 WIDGET-ID 36 NO-TAB-STOP 
     BUTTON-15 AT ROW 6.29 COL 1 WIDGET-ID 38 NO-TAB-STOP 
     BUTTON-7 AT ROW 4.1 COL 31 WIDGET-ID 94 NO-TAB-STOP 
     BUTTON-8 AT ROW 5.19 COL 1 WIDGET-ID 96 NO-TAB-STOP 
     BUTTON-9 AT ROW 5.19 COL 6 WIDGET-ID 98 NO-TAB-STOP 
     BUTTON-16 AT ROW 6.29 COL 6 WIDGET-ID 40 NO-TAB-STOP 
     BUTTON-17 AT ROW 6.29 COL 11 WIDGET-ID 42 NO-TAB-STOP 
     BUTTON-18 AT ROW 6.29 COL 16 WIDGET-ID 44 NO-TAB-STOP 
     BUTTON-19 AT ROW 6.29 COL 21 WIDGET-ID 46 NO-TAB-STOP 
     BUTTON-20 AT ROW 6.29 COL 26 WIDGET-ID 50 NO-TAB-STOP 
     BUTTON-21 AT ROW 6.29 COL 31 WIDGET-ID 52 NO-TAB-STOP 
     BUTTON-22 AT ROW 7.38 COL 1 WIDGET-ID 54 NO-TAB-STOP 
     BUTTON-23 AT ROW 7.38 COL 6 WIDGET-ID 56 NO-TAB-STOP 
     BUTTON-24 AT ROW 7.38 COL 11 WIDGET-ID 58 NO-TAB-STOP 
     BUTTON-25 AT ROW 7.38 COL 16 WIDGET-ID 60 NO-TAB-STOP 
     BUTTON-26 AT ROW 7.38 COL 21 WIDGET-ID 62 NO-TAB-STOP 
     BUTTON-27 AT ROW 7.38 COL 26 WIDGET-ID 64 NO-TAB-STOP 
     BUTTON-28 AT ROW 7.38 COL 31 WIDGET-ID 66 NO-TAB-STOP 
     tYear AT ROW 1.43 COL 13 COLON-ALIGNED NO-LABEL WIDGET-ID 106
     tMonth AT ROW 2 COL 10.2 COLON-ALIGNED NO-LABEL WIDGET-ID 104
     tDate1 AT ROW 11.71 COL 2.6 NO-LABEL WIDGET-ID 108
     tDate2 AT ROW 12.67 COL 2.6 NO-LABEL WIDGET-ID 110
     "Calculated Gap" VIEW-AS TEXT
          SIZE 15.6 BY .62 AT ROW 10.71 COL 2 WIDGET-ID 114
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 35.8 BY 13.95 WIDGET-ID 100.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME dCal
     RECT-2 AT ROW 11 COL 1.4 WIDGET-ID 112
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 35.8 BY 13.95 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Gap Calendar"
         HEIGHT             = 13.95
         WIDTH              = 35.8
         MAX-HEIGHT         = 20.95
         MAX-WIDTH          = 80
         VIRTUAL-HEIGHT     = 20.95
         VIRTUAL-WIDTH      = 80
         SHOW-IN-TASKBAR    = no
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME dCal
   FRAME-NAME                                                           */
/* SETTINGS FOR BUTTON bFri IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bMon IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bSat IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bSun IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bThu IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bToday IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bTue IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BUTTON-1 IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BUTTON-2 IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BUTTON-29 IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BUTTON-3 IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BUTTON-30 IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BUTTON-31 IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BUTTON-32 IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BUTTON-33 IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BUTTON-34 IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BUTTON-35 IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BUTTON-36 IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BUTTON-37 IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BUTTON-4 IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BUTTON-5 IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BUTTON-6 IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bWed IN FRAME dCal
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tCalGap IN FRAME dCal
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tDate1 IN FRAME dCal
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN tDate2 IN FRAME dCal
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN tMonth IN FRAME dCal
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tWorkGap IN FRAME dCal
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tYear IN FRAME dCal
   NO-DISPLAY NO-ENABLE                                                 */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME dCal
/* Query rebuild information for FRAME dCal
     _Query            is NOT OPENED
*/  /* FRAME dCal */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Gap Calendar */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Gap Calendar */
DO:
  apply "Close" to this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dCal
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dCal C-Win
ON WINDOW-CLOSE OF FRAME dCal
DO:
  APPLY "close":U TO this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear C-Win
ON CHOOSE OF bClear IN FRAME dCal /* Clear */
DO:
 clearDates().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNextMo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNextMo C-Win
ON CHOOSE OF bNextMo IN FRAME dCal /* > */
DO:
  if curMonth = 12
   then assign curMonth = 1 
               curYear = curYear + 1.
   else curMonth = curMonth + 1.
  resetDisplay().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNextYr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNextYr C-Win
ON CHOOSE OF bNextYr IN FRAME dCal /* >> */
DO:
  curYear = curYear + 1.
  resetDisplay().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPreMo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPreMo C-Win
ON CHOOSE OF bPreMo IN FRAME dCal /* < */
DO:
  if curMonth = 1
   then assign curMonth = 12
               curYear = curYear - 1.
   else curMonth = curMonth - 1.
  resetDisplay().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPreYr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPreYr C-Win
ON CHOOSE OF bPreYr IN FRAME dCal /* << */
DO:
  curYear = curYear - 1.
  resetDisplay().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bToday
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bToday C-Win
ON CHOOSE OF bToday IN FRAME dCal /* Today */
DO:
  curMonth = month(today).
  curYear = year(today).
  resetDisplay().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-1 C-Win
ON CHOOSE OF BUTTON-1 IN FRAME dCal /* 1 */
or choose of button-2
or choose of button-3
or choose of button-4
or choose of button-5
or choose of button-6
or choose of button-7
or choose of button-8
or choose of button-9
or choose of button-10
or choose of button-11
or choose of button-12
or choose of button-13
or choose of button-14
or choose of button-15
or choose of button-16
or choose of button-17
or choose of button-18
or choose of button-19
or choose of button-20
or choose of button-21
or choose of button-22
or choose of button-23
or choose of button-24
or choose of button-25
or choose of button-26
or choose of button-27
or choose of button-28
or choose of button-29
or choose of button-30
or choose of button-31
or choose of button-32
or choose of button-33
or choose of button-34
or choose of button-35
or choose of button-36
or choose of button-37

DO:
  setDate(self:label).
/*   APPLY "END-ERROR":U TO frame dCal.  */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

assign
 tButtons[1] = button-1:handle in frame {&frame-name}
 tButtons[2] = button-2:handle in frame {&frame-name}
 tButtons[3] = button-3:handle in frame {&frame-name}
 tButtons[4] = button-4:handle in frame {&frame-name}
 tButtons[5] = button-5:handle in frame {&frame-name}
 tButtons[6] = button-6:handle in frame {&frame-name}
 tButtons[7] = button-7:handle in frame {&frame-name}
 tButtons[8] = button-8:handle in frame {&frame-name}
 tButtons[9] = button-9:handle in frame {&frame-name}
 tButtons[10] = button-10:handle in frame {&frame-name}
 tButtons[11] = button-11:handle in frame {&frame-name}
 tButtons[12] = button-12:handle in frame {&frame-name}
 tButtons[13] = button-13:handle in frame {&frame-name}
 tButtons[14] = button-14:handle in frame {&frame-name}
 tButtons[15] = button-15:handle in frame {&frame-name}
 tButtons[16] = button-16:handle in frame {&frame-name}
 tButtons[17] = button-17:handle in frame {&frame-name}
 tButtons[18] = button-18:handle in frame {&frame-name}
 tButtons[19] = button-19:handle in frame {&frame-name}
 tButtons[20] = button-20:handle in frame {&frame-name}
 tButtons[21] = button-21:handle in frame {&frame-name}
 tButtons[22] = button-22:handle in frame {&frame-name}
 tButtons[23] = button-23:handle in frame {&frame-name}
 tButtons[24] = button-24:handle in frame {&frame-name}
 tButtons[25] = button-25:handle in frame {&frame-name}
 tButtons[26] = button-26:handle in frame {&frame-name}
 tButtons[27] = button-27:handle in frame {&frame-name}
 tButtons[28] = button-28:handle in frame {&frame-name}
 tButtons[29] = button-29:handle in frame {&frame-name}
 tButtons[30] = button-30:handle in frame {&frame-name}
 tButtons[31] = button-31:handle in frame {&frame-name}
 tButtons[32] = button-32:handle in frame {&frame-name}
 tButtons[33] = button-33:handle in frame {&frame-name}
 tButtons[34] = button-34:handle in frame {&frame-name}
 tButtons[35] = button-35:handle in frame {&frame-name}
 tButtons[36] = button-36:handle in frame {&frame-name}
 tButtons[37] = button-37:handle in frame {&frame-name}
 .


if pDate = ?
 then assign
        curMonth = month(today)
        curYear = year(today).
 else assign
        curMonth = month(pDate)
        curYear = year(pDate).
resetDisplay().


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tDate1 tDate2 
      WITH FRAME dCal IN WINDOW C-Win.
  ENABLE bClear RECT-2 bNextMo bNextYr bPreMo bPreYr BUTTON-10 BUTTON-11 
         BUTTON-12 BUTTON-13 BUTTON-14 BUTTON-15 BUTTON-7 BUTTON-8 BUTTON-9 
         BUTTON-16 BUTTON-17 BUTTON-18 BUTTON-19 BUTTON-20 BUTTON-21 BUTTON-22 
         BUTTON-23 BUTTON-24 BUTTON-25 BUTTON-26 BUTTON-27 BUTTON-28 
      WITH FRAME dCal IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-dCal}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearDates C-Win 
FUNCTION clearDates RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  assign
    tDate1:screen-value in frame {&frame-name} = ""
    tDate2:screen-value = ""
    tCalGap:screen-value = ""
    tWorkGap:screen-value = ""
    tDate1 = ?
    tDate2 = ?
    .
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFirstDow C-Win 
FUNCTION getFirstDow RETURNS INTEGER
  ( ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def var tDate as date.
 tDate = date(curMonth, 1, curYear).
 return weekday(tDate).
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getNumDays C-Win 
FUNCTION getNumDays RETURNS INTEGER
  ( ) :
/*------------------------------------------------------------------------------
  Purpose:  Return the number of days in any month of any year
    Notes:  
------------------------------------------------------------------------------*/
 def var tDate as date.
 
 tDate = date(curMonth, 1, curYear).

 if month(tDate) = 12
  then return 31.
 tDate = date(month(tDate) + 1, 1, year(tDate)) - 1.
 return day(tDate).
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resetDisplay C-Win 
FUNCTION resetDisplay RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def var i as int.
 def var tMin as int.
 def var tMax as int.
 def var tDay as int.

 tMonth = entry(curMonth, tMonthDesc).

 display
   tMonth
   curYear @ tYear
   with frame {&frame-name}.

 tMin = getFirstDow().
 tMax = getFirstDow() + getNumDays() - 1.

 do i = 1 to extent(tButtons):
  tDay = i - tMin + 1.
  if curMonth = month(today) and tDay = day(today) and curYear = year(today)
   then tButtons[i]:font = 6.
   else tButtons[i]:font = ?.
  if i < tMin or i > tMax
   then assign
          tButtons[i]:visible = false
          tButtons[i]:sensitive = false.
   else assign
          tButtons[i]:visible = true
          tButtons[i]:sensitive = true.
  tButtons[i]:label = string(tDay).
 end.

 bToday:sensitive in frame {&frame-name} = (curMonth <> month(today)).

 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setDate C-Win 
FUNCTION setDate RETURNS LOGICAL PRIVATE
  ( input pLabel as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 if pLabel = "" or pLabel = ? or int(pLabel) <= 0
  then .
  else pDate = date(curMonth, int(pLabel), curYear).

 /* Reset if both are already set */
 if tDate1 <> ? and tDate2 <> ? 
  then clearDates().

 if tDate1 = ?
  then
   do: tDate1 = pDate.
       display tDate1 with frame dCal.
       return true.
   end.
 if tDate2 = ?
  then
   do: tDate2 = pDate.
       display tDate2 with frame dCal.
       publish "GetGaps" (tDate1, tDate2, output tCalGap, output tWorkGap).
       display tCalGap tWorkGap
         with frame dCal.
   end.

 RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

