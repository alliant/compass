&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : server.p
    Purpose     : Functionality to interact with the AES server
    Description :
    Author(s)   : D.Sinclair
    Created     : 9.13.2011
    Notes       :
  ----------------------------------------------------------------------*/

{lib/std-def.i}

/* Used by all services to capture success/failure */
def var responseGood as logical no-undo.
def var responseCode as char no-undo.
def var responseString as char no-undo.

/* Used by listAgentsService only */
def temp-table agent
 field agentId as char
 field name as char
 field addr as char
 field city as char
 field state as char
 field zip as char
 .

/* Used by both submitService and listAuditsService */
def temp-table audit
 field auditId as char
 field agentId as char
 field name as char
 field auditDate as char
 field auditor as char
 field auditScore as char
 field submittedBy as char
 field submittedAt as char
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-createURL) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createURL Procedure 
FUNCTION createURL RETURNS LOGICAL PRIVATE
  ( input tHost as char,
    input tService as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parseResponse) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD parseResponse Procedure 
FUNCTION parseResponse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parseTrace) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD parseTrace Procedure 
FUNCTION parseTrace RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reset) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD reset Procedure 
FUNCTION reset RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-trace) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD trace Procedure 
FUNCTION trace RETURNS LOGICAL PRIVATE
  ( input pError as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

subscribe to "submitService" anywhere.
subscribe to "recallService" anywhere.
subscribe to "testService" anywhere.
subscribe to "listAuditsService" anywhere.
subscribe to "listAgentsService" anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-error) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE error Procedure 
PROCEDURE error :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  pErr as char
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pErr as char.

 assign
   responseGood = false
   responseCode = "C8"
   responseString = pErr
   .

 trace(pErr + " (Error " + responseCode + ")").
 return error. /* Stop parsing */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-fatalError) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fatalError Procedure 
PROCEDURE fatalError :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  pErr as char
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pErr as char.

 assign
   responseGood = false
   responseCode = "C9"
   responseString = pErr
   .
 trace(pErr + " (Error " + responseCode + ")").
 return error. /* Stop parsing */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-listAuditsService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE listAuditsService Procedure 
PROCEDURE listAuditsService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter table for audit.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 def var tHost as char no-undo.
 def var tService as char no-undo.
 def var httpTool as char no-undo.
 

 reset().
 
 httpTool = search("tools/wget.exe").
 if httpTool = ? 
  then 
   do: trace("tools/wget.exe was not found.").
       pMsg = "HTTP executable not found. (Error C1)".
       return.
   end.

 /* Create the Server URL specification file qarurl.dat */
 publish "GetServerHost" (output tHost).
 publish "GetListAuditsService" (output tService).

 std-lo = createURL(tHost, tService).
 if not std-lo 
  then 
   do: pMsg = responseString + " (Error " + responseCode + ")". 
       return.
   end.

 /* Perform the HTTP GET */
 os-command 
  silent
  /*   no-console  */
  value(httpTool) 
  --debug
  --tries=2
  --output-document=qarresponse.dat 
  --output-file=qartrace.dat
  --input-file=qarurl.dat
  .

 /* Parse the wget trace file (basically) */
 std-lo = parseTrace().
 if not std-lo 
  then
   do: pMsg = responseString + " (Error " + responseCode + ")".
       return.
   end.

 /* Parse the wget response file (basically) */
 std-lo = parseResponse().
 if not std-lo 
  then 
   do: pMsg = responseString + " (Error " + responseCode + ")".
       return.
   end.

 pSuccess = responseGood.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-recallService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE recallService Procedure 
PROCEDURE recallService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAudit as char.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 def var tHost as char no-undo.
 def var tService as char no-undo.
 def var httpTool as char no-undo.
 

 reset().
 
 httpTool = search("tools/wget.exe").
 if httpTool = ? 
  then 
   do: trace("tools/wget.exe was not found.").
       pMsg = "HTTP executable not found. (Error C1)".
       return.
   end.

 /* Create the Server URL specification file qarurl.dat */
 publish "GetServerHost" (output tHost).
 publish "GetRecallService" (output tService).

 std-lo = createURL(tHost, tService).
 if not std-lo 
  then 
   do: pMsg = responseString + " (Error " + responseCode + ")". 
       return.
   end.

 /* Create a "request" qarxml.dat for posting */
 output to qarxml.dat page-size 0.
 put unformatted '<?xml version="1.0"?>' skip.
 put unformatted '<Envelope><Body>' skip.
 put unformatted ' <audit auditID="' pAudit '"/>' skip.
 put unformatted '</Body></Envelope>' skip.
 output close.

 /* Perform the HTTP POST */
 os-command 
  silent
  /*   no-console  */
  value(httpTool) 
  --debug
  --tries=2
  --output-document=qarresponse.dat 
  --output-file=qartrace.dat
  --post-file=qarxml.dat
  --header='"Content-Type: text/xml"'
  --input-file=qarurl.dat
  .

 /* Parse the wget trace file (basically) */
 std-lo = parseTrace().
 if not std-lo 
  then
   do: pMsg = responseString + " (Error " + responseCode + ")".
       return.
   end.

 /* Parse the wget response file (basically) */
 std-lo = parseResponse().
 if not std-lo 
  then 
   do: pMsg = responseString + " (Error " + responseCode + ")".
       return.
   end.

 pSuccess = responseGood.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
  Notes:       This is not clean as it's all mushed together in this single
               procedure.  Ideally, each service should have it's own parser.
------------------------------------------------------------------------------*/
 DEFINE INPUT PARAMETER namespaceURI AS CHARACTER.
 DEFINE INPUT PARAMETER localName AS CHARACTER.
 DEFINE INPUT PARAMETER qName AS CHARACTER.
 DEFINE INPUT PARAMETER attributes AS HANDLE.

 DEF VAR i AS INT.
 DEF VAR fldvalue AS CHAR.

 def buffer audit for audit.
 def buffer agent for agent.

 case qName: 
  when "Fault" 
   then 
    do:
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
             WHEN "faultCode" THEN
                 responseCode = fldvalue.
             WHEN "faultString" THEN
                 responseString = fldvalue.
         END CASE.
        END.
        responseGood = false.
    end.
  /* Used by listAgentsService */
  when "Agent" 
   then 
    do: create agent.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
             WHEN "agentID" THEN
                 agent.agentID = fldvalue.
             WHEN "name" THEN
                 agent.name = fldvalue.
             WHEN "addr" THEN
                 agent.addr = fldvalue.
             WHEN "city" THEN
                 agent.city = fldvalue.
             WHEN "state" THEN
                 agent.state = fldvalue.
             WHEN "zip" THEN
                 agent.zip = fldvalue.
         END CASE.
        END.
        release agent.
    end.
  /* Used for submitService, listAuditsService */
  /* Used but not needed by recallService */
  when "Audit"
   then 
    do: create audit.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
             WHEN "auditID" THEN
                 audit.auditId = fldvalue.
             WHEN "agentID" THEN
                 audit.agentId = fldvalue.
             WHEN "name" THEN
                 audit.name = fldvalue.
             WHEN "auditDate" THEN
                 audit.auditDate = fldvalue.
             WHEN "auditScore" THEN
                 audit.auditScore = fldvalue.
             WHEN "auditor" THEN
                 audit.auditor = fldvalue.
             WHEN "submittedBy" THEN
                 audit.submittedBy = fldvalue.
             WHEN "submittedAt" THEN
                 audit.submittedAt = fldvalue.
         END CASE.
        END.
        release audit.
    end.
 end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-submitService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE submitService Procedure 
PROCEDURE submitService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAudit as char.
 def output parameter pNewAuditId as char.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 def var tHost as char no-undo.
 def var tService as char no-undo.
 def var httpTool as char no-undo.
 

 reset().
 
 httpTool = search("tools/wget.exe").
 if httpTool = ? 
  then 
   do: trace("tools/wget.exe was not found.").
       pMsg = "HTTP executable not found. (Error C1)".
       return.
   end.

 /* Create the Server URL specification file qarurl.dat */
 publish "GetServerHost" (output tHost).
 publish "GetSubmitService" (output tService).

 std-lo = createURL(tHost, tService).
 if not std-lo 
  then 
   do: pMsg = responseString + " (Error " + responseCode + ")". 
       return.
   end.

 /* Copy the active audit to qarxml.dat for posting */
 if search(pAudit) = ?
  then
   do: trace("Audit file " + string(pAudit) + " was not found.").
       pMsg = "Audit file not found. (Error C3)".
       return.
   end.

 os-copy value(pAudit) qarxml.dat.
 if os-error <> 0
  then 
   do: trace("Failed to create temporary audit file from " 
               + pAudit + " (" + string(os-error) + ").").
       pMsg = "Unable to create temporary audit file. (Error C4)".
       return.
   end.

 /* Perform the HTTP POST */
 os-command 
  silent
  /*   no-console  */
  value(httpTool) 
  --debug
  --tries=2
  --output-document=qarresponse.dat 
  --output-file=qartrace.dat
  --post-file=qarxml.dat
  --header='"Content-Type: text/xml"'
  --input-file=qarurl.dat
  .

 /* Parse the wget trace file (basically) */
 std-lo = parseTrace().
 if not std-lo 
  then
   do: pMsg = responseString + " (Error " + responseCode + ")".
       return.
   end.

 /* Parse the wget response file (basically) */
 std-lo = parseResponse().
 if not std-lo 
  then 
   do: pMsg = responseString + " (Error " + responseCode + ")".
       return.
   end.

 find first audit no-error.
 if not available audit
  then
   do: trace("Audit record was not found.").
       pMsg = "Internal record failure. (Error C6)".
       return.
   end.

 pNewAuditID = audit.auditId.
 pSuccess = responseGood.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-testService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE testService Procedure 
PROCEDURE testService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter tHost as char.
 def input parameter tService as char.
 def output parameter pSuccess as logical init false.

 def var httpTool as char no-undo.
 def var tTemp as char no-undo.

 reset().
 
 httpTool = search("tools/wget.exe").
 if httpTool = ? 
  then 
   do: trace("tools/wget.exe was not found.").
       return.
   end.

 /* Create the Server URL specification file qarurl.dat */
 std-lo = createURL(tHost, tService).
 if not std-lo 
  then return.

 /* Create a "ping" qarxml.dat for posting */
 output to qarxml.dat page-size 0.
 put unformatted '<?xml version="1.0"?>' skip.
 put unformatted '<Ping>' skip.
 put unformatted ' <Date>' string(today) '</Date>' skip.
 put unformatted ' <Time>' string(time, "HH:MM:SS") '</Time>' skip.
 put unformatted '</Ping>' skip.
 output close.

 /* Perform the HTTP POST */
 os-command 
  silent
  /*   no-console  */
  value(httpTool) 
  --debug
  --tries=2
  --output-document=qarresponse.dat 
  --output-file=qartrace.dat
  --post-file=qarxml.dat
  --header='"Content-Type: text/xml"'
  --input-file=qarurl.dat
  .

 /* Parse the wget trace file (basically) */
 std-lo = parseTrace().
 if not std-lo 
  then return.

 /* Parse the wget response file (basically) */
 std-lo = parseResponse().
 if not std-lo 
  then return.

 pSuccess = responseGood.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-createURL) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createURL Procedure 
FUNCTION createURL RETURNS LOGICAL PRIVATE
  ( input tHost as char,
    input tService as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  Should result in a file containing a line such as:
            http://localhost/do/cgiip.exe/aesping.p
------------------------------------------------------------------------------*/

 if tHost = "" and tService = "" 
  then 
   do: trace("Host and Service are not set.").
       responseCode = "C2".
       responseString = "URL configuration failed.".
       return false.
   end.

 output to "qarurl.dat" page-size 0.
 put unformatted tHost tService skip.
 output close.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parseResponse) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION parseResponse Procedure 
FUNCTION parseResponse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 
 assign
   responseGood = true
   responseCode = ""
   responseString = ""
   .

 def var hParser as handle no-undo.
 
 create sax-reader hParser.
 hParser:handler = this-procedure.
 hParser:SET-INPUT-SOURCE("FILE", "qarresponse.dat").
 hParser:suppress-namespace-processing = true.
 hParser:SAX-PARSE( ) NO-ERROR.

 DELETE OBJECT hParser.

 trace("Response good : " + string(responseGood)).
 trace("Parse response: " + responseString + " (Error " + responseCode + ")").
 RETURN responseGood.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parseTrace) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION parseTrace Procedure 
FUNCTION parseTrace RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  file-info:file-name = "qarresponse.dat".
  
  if index(file-info:file-type, "F") = 0 or file-info:file-type = ?
   then
    do: trace("Unable to qarresponse.dat file.").
        responseCode = "C5".
        responseString = "Unable to locate response.".
        return false.
    end.
  if index(file-info:file-type, "R") = 0
   then
    do: trace("Unable to read qarresponse.dat file.").
        responseCode = "C6".
        responseString = "Unable to read response.".
        return false.
    end.
  if file-info:file-size = 0
   then
    do: trace("Unable to connect to Host/Service; check qarurl.dat file.").
        responseCode = "C7".
        responseString = "Unable to connect to Service.".
        return false.
    end.
  RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reset) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION reset Procedure 
FUNCTION reset RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 empty temp-table audit.
 empty temp-table agent.

 assign
   responseGood = true
   responseCode = ""
   responseString = ""
   .

 os-delete qarurl.dat.
 os-delete qarxml.dat.
 os-delete qarresponse.dat.
 os-delete qartrace.dat.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-trace) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION trace Procedure 
FUNCTION trace RETURNS LOGICAL PRIVATE
  ( input pError as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 output to qartrace.dat append.
 put unformatted pError skip.
 output close.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

