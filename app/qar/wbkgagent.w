&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
    File        :   wbkgagent.w
    Modification:
    Date          Name      Description
    01/12/2017    AG        Fixed resizing of widgets and data persist
                            and fixed all questions got dissappears during
                            window resizing.      
    02/13/2017    AC        Implement local inspect functionality.
  ----------------------------------------------------------------------*/

CREATE WIDGET-POOL.

{lib/std-def.i}
{lib/winshowscrollbars.i}

def var tOk as logical.
def var iBgColor as int no-undo.
{lib/questiondef.i &seq=22000}
{lib/questiondef.i &seq=22005}
{lib/questiondef.i &seq=22040}
{lib/questiondef.i &seq=22045}
{lib/questiondef.i &seq=22050}
{lib/questiondef.i &seq=22055}
{lib/questiondef.i &seq=22060}
{lib/questiondef.i &seq=22065}

{lib/questiondef.i &seq=22070}
{lib/questiondef.i &seq=22075}
{lib/questiondef.i &seq=22080}
{lib/questiondef.i &seq=22085}
{lib/questiondef.i &seq=22090}
{lib/questiondef.i &seq=22095}
{lib/questiondef.i &seq=22100}
{lib/questiondef.i &seq=22105}
{lib/questiondef.i &seq=22110}
{lib/questiondef.i &seq=22115}

{lib/questiondef.i &seq=22120}
{lib/questiondef.i &seq=22125}
{lib/questiondef.i &seq=22130}
{lib/questiondef.i &seq=22135}
{lib/questiondef.i &seq=22140}
{lib/questiondef.i &seq=22145}

{tt/qarbkgbranch.i} /* branch */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwBranches

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES branch

/* Definitions for BROWSE brwBranches                                   */
&Scoped-define FIELDS-IN-QUERY-brwBranches branch.name branch.addr branch.contactName branch.contactEmail branch.contactPhone branch.numEmployees branch.numLicensed   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwBranches   
&Scoped-define SELF-NAME brwBranches
&Scoped-define QUERY-STRING-brwBranches FOR EACH branch by branch.name
&Scoped-define OPEN-QUERY-brwBranches OPEN QUERY {&SELF-NAME} FOR EACH branch by branch.name.
&Scoped-define TABLES-IN-QUERY-brwBranches branch
&Scoped-define FIRST-TABLE-IN-QUERY-brwBranches branch


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwBranches}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bAddBranch brwBranches 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshBrowse C-Win 
FUNCTION refreshBrowse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddBranch 
     LABEL "Add..." 
     SIZE 7.2 BY 1.71 TOOLTIP "Add a new branch".

DEFINE BUTTON bDeleteBranch 
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete the selected branch".

DEFINE VARIABLE qHistory AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 80 BY 1.91
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qBuilder AS INTEGER FORMAT ">>9 %":U INITIAL 0 
     LABEL "Builder" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qCommercial AS INTEGER FORMAT ">>9 %":U INITIAL 0 
     LABEL "Commercial" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qEmployees AS INTEGER FORMAT ">,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 9 BY 1
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qEscrow AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 32 BY 1
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qForeclosure AS INTEGER FORMAT ">>9 %":U INITIAL 0 
     LABEL "Foreclosure" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qOther AS INTEGER FORMAT ">>9 %":U INITIAL 0 
     LABEL "Other" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qPurchase AS INTEGER FORMAT ">>9 %":U INITIAL 0 
     LABEL "Purchase" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qRefinance AS INTEGER FORMAT ">>9 %":U INITIAL 0 
     LABEL "Refinance" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qShortSale AS INTEGER FORMAT ">>9 %":U INITIAL 0 
     LABEL "Short Sale" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qTitle AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 32 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwBranches FOR 
      branch SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwBranches
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwBranches C-Win _FREEFORM
  QUERY brwBranches DISPLAY
      branch.name label "Branch Name" format "x(30)"
 branch.addr label "Address" format "x(60)"
 branch.contactName label "Contact" format "x(30)"
 branch.contactEmail label "Email" format "x(40)"
 branch.contactPhone label "Phone" format "x(15)"
 branch.numEmployees label "Employees" format ">>,>>9"
 branch.numLicensed label "Licensed" format ">>,>>9"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 147.8 BY 3.81 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bAddBranch AT ROW 1.24 COL 3 WIDGET-ID 38
     brwBranches AT ROW 1.24 COL 11.2 WIDGET-ID 300
     bDeleteBranch AT ROW 2.91 COL 3 WIDGET-ID 40
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 158 BY 26.81 WIDGET-ID 100.

DEFINE FRAME fSection
     qHistory AT ROW 1.48 COL 32 NO-LABEL WIDGET-ID 278
     qRefinance AT ROW 4.81 COL 20 COLON-ALIGNED WIDGET-ID 324
     qPurchase AT ROW 4.81 COL 33.6 WIDGET-ID 326
     qBuilder AT ROW 4.81 COL 54.4 WIDGET-ID 328
     qForeclosure AT ROW 4.81 COL 72.6 WIDGET-ID 330
     qCommercial AT ROW 4.81 COL 95.4 WIDGET-ID 332
     qShortSale AT ROW 4.81 COL 118 WIDGET-ID 334
     qOther AT ROW 4.81 COL 144 COLON-ALIGNED WIDGET-ID 336
     qEmployees AT ROW 6.71 COL 117 COLON-ALIGNED NO-LABEL WIDGET-ID 284
     qTitle AT ROW 9.1 COL 117 COLON-ALIGNED NO-LABEL WIDGET-ID 286
     qEscrow AT ROW 11.71 COL 117 COLON-ALIGNED NO-LABEL WIDGET-ID 292
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 5.1
         SCROLLABLE SIZE 158 BY 60 WIDGET-ID 200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Background - Agent"
         HEIGHT             = 26.81
         WIDTH              = 158
         MAX-HEIGHT         = 27.95
         MAX-WIDTH          = 158
         VIRTUAL-HEIGHT     = 27.95
         VIRTUAL-WIDTH      = 158
         SHOW-IN-TASKBAR    = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fSection:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */

DEFINE VARIABLE XXTABVALXX AS LOGICAL NO-UNDO.

ASSIGN XXTABVALXX = FRAME fSection:MOVE-AFTER-TAB-ITEM (bDeleteBranch:HANDLE IN FRAME fMain)
/* END-ASSIGN-TABS */.

/* BROWSE-TAB brwBranches bAddBranch fMain */
/* SETTINGS FOR BUTTON bDeleteBranch IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwBranches:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwBranches:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

/* SETTINGS FOR FRAME fSection
                                                                        */
ASSIGN 
       FRAME fSection:HEIGHT           = 22.71
       FRAME fSection:WIDTH            = 158.

/* SETTINGS FOR FILL-IN qBuilder IN FRAME fSection
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN qCommercial IN FRAME fSection
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN qEmployees IN FRAME fSection
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN qEscrow IN FRAME fSection
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN qForeclosure IN FRAME fSection
   ALIGN-L                                                              */
/* SETTINGS FOR EDITOR qHistory IN FRAME fSection
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN qPurchase IN FRAME fSection
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN qShortSale IN FRAME fSection
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN qTitle IN FRAME fSection
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwBranches
/* Query rebuild information for BROWSE brwBranches
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH branch by branch.name.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwBranches */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Background - Agent */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Background - Agent */
DO:
  if valid-handle(focus) /* maybe not if the window was just closed */
     and (focus:type = "fill-in" or focus:type = "editor") /* all other types ok */
     and focus:modified /* inconsistently set... */
  then apply "leave" to focus.
    
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Background - Agent */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddBranch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddBranch C-Win
ON CHOOSE OF bAddBranch IN FRAME fMain /* Add... */
DO:
  run dialogbranch.w (output std-lo).
  if std-lo 
   then refreshBrowse().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteBranch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteBranch C-Win
ON CHOOSE OF bDeleteBranch IN FRAME fMain /* Delete */
DO:
  if not available branch 
   then
  do: bDeleteBranch:sensitive = false.
      return.
  end.
  tOk = false.
  MESSAGE "Branch" branch.name "will be removed."
    VIEW-AS ALERT-BOX warning BUTTONS ok-cancel update tOk.
   if tOk
    then 
     do: 
       publish "DeleteBranch" (branch.name).
       DELETE branch.
       brwBranches:DELETE-SELECTED-ROWS().
       refreshBrowse().
     end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwBranches
&Scoped-define SELF-NAME brwBranches
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBranches C-Win
ON ROW-DISPLAY OF brwBranches IN FRAME fMain
DO:
  if current-result-row("brwBranches") modulo 2 = 0 then
    iBgColor = 17.
  else
    iBgColor = 15.
  branch.name:bgcolor in browse brwBranches = iBgColor.
  branch.addr:bgcolor in browse brwBranches = iBgColor.
  branch.contactName:bgcolor in browse brwBranches = iBgColor.
  branch.contactEmail:bgcolor in browse brwBranches = iBgColor.
  branch.contactPhone:bgcolor in browse brwBranches = iBgColor.
  branch.numEmployees:bgcolor in browse brwBranches = iBgColor.
  branch.numLicensed:bgcolor in browse brwBranches = iBgColor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBranches C-Win
ON START-SEARCH OF brwBranches IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fSection
&Scoped-define SELF-NAME qBuilder
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qBuilder C-Win
ON LEAVE OF qBuilder IN FRAME fSection /* Builder */
DO:
 if self:modified
  then run doFillin (22015, self:screen-value).
  publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qCommercial
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qCommercial C-Win
ON LEAVE OF qCommercial IN FRAME fSection /* Commercial */
DO:  
 if self:modified
  then run doFillin (22025, self:screen-value).
  publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qEmployees
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qEmployees C-Win
ON LEAVE OF qEmployees IN FRAME fSection
DO:
  if self:modified
   then run doFillin (22040, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qEscrow
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qEscrow C-Win
ON LEAVE OF qEscrow IN FRAME fSection
DO:
  if self:modified 
   then run doFillin (22050, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qForeclosure
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qForeclosure C-Win
ON LEAVE OF qForeclosure IN FRAME fSection /* Foreclosure */
DO:
  if self:modified
   then run doFillin (22020, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qHistory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qHistory C-Win
ON LEAVE OF qHistory IN FRAME fSection
DO:
   if self:modified
    then run doFillin (22000, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qOther
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qOther C-Win
ON LEAVE OF qOther IN FRAME fSection /* Other */
DO:
  if self:modified
   then run doFillin (22035, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qPurchase
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qPurchase C-Win
ON LEAVE OF qPurchase IN FRAME fSection /* Purchase */
DO:
  if self:modified
   then run doFillin (22010, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qRefinance
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qRefinance C-Win
ON LEAVE OF qRefinance IN FRAME fSection /* Refinance */
DO:
  if self:modified
   then run doFillin (22005, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qShortSale
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qShortSale C-Win
ON LEAVE OF qShortSale IN FRAME fSection /* Short Sale */
DO:
  if self:modified
   then run doFillin (22030, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qTitle
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qTitle C-Win
ON LEAVE OF qTitle IN FRAME fSection
DO:
  if self:modified 
   then run doFillin (22045, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

PAUSE 0 BEFORE-HIDE.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

bAddBranch:load-image("images/add.bmp").
bAddBranch:load-image-insensitive("images/add-i.bmp").
bDeleteBranch:load-image("images/delete.bmp").
bDeleteBranch:load-image-insensitive("images/delete-i.bmp").
subscribe to "AuditOpened" anywhere.
subscribe to "Close" anywhere.
subscribe to "AuditClosed" anywhere.
subscribe to "LeaveAll" anywhere.
on 'value-changed':u anywhere
do:
  publish "EnableSave".
  publish "SetClickSave".
end.
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  run initializeFrame in this-procedure.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditClosed C-Win 
PROCEDURE AuditClosed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 assign
   qHistory:screen-value in frame fSection = ""
   qRefinance:screen-value = ""
   qPurchase:screen-value = ""
   qBuilder:screen-value = ""
   qForeclosure:screen-value = ""
   qCommercial:screen-value = ""
   qShortSale:screen-value = ""
   qOther:screen-value = ""
   qEmployees:screen-value = ""
   qTitle:screen-value = ""
   qEscrow:screen-value = ""
   .

 assign
   qHistory:sensitive = false
   qRefinance:sensitive = false
   qPurchase:sensitive = false
   qBuilder:sensitive = false
   qForeclosure:sensitive = false
   qCommercial:sensitive = false
   qShortSale:sensitive = false
   qOther:sensitive = false
   qEmployees:sensitive = false
   qTitle:sensitive = false
   qEscrow:sensitive = false
   .


 {lib/unsetquestion.i &seq=22055}
 {lib/unsetquestion.i &seq=22060}
 {lib/unsetquestion.i &seq=22065}
 {lib/unsetquestion.i &seq=22070}
 {lib/unsetquestion.i &seq=22075}
 {lib/unsetquestion.i &seq=22080}
 {lib/unsetquestion.i &seq=22085}
 {lib/unsetquestion.i &seq=22090}
 {lib/unsetquestion.i &seq=22095}
 {lib/unsetquestion.i &seq=22100}
 {lib/unsetquestion.i &seq=22105}
 {lib/unsetquestion.i &seq=22110}
 {lib/unsetquestion.i &seq=22115}
 {lib/unsetquestion.i &seq=22120}
 {lib/unsetquestion.i &seq=22125}
 {lib/unsetquestion.i &seq=22130}
 {lib/unsetquestion.i &seq=22135}
 {lib/unsetquestion.i &seq=22140}
 {lib/unsetquestion.i &seq=22145}

 refreshBrowse().

 frame fSection:sensitive = false.
 frame fMain:sensitive = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditOpened C-Win 
PROCEDURE AuditOpened :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 publish "GetBackgroundAnswer" (22000, output std-ch).
 qHistory:screen-value in frame fSection = std-ch.

 publish "GetBackgroundAnswer" (22005, output std-ch).
 qRefinance:screen-value in frame fSection = std-ch.
 publish "GetBackgroundAnswer" (22010, output std-ch).
 qPurchase:screen-value in frame fSection = std-ch.
 publish "GetBackgroundAnswer" (22015, output std-ch).
 qBuilder:screen-value in frame fSection = std-ch.
 publish "GetBackgroundAnswer" (22020, output std-ch).
 qForeclosure:screen-value in frame fSection = std-ch.
 publish "GetBackgroundAnswer" (22025, output std-ch).
 qCommercial:screen-value in frame fSection = std-ch.
 publish "GetBackgroundAnswer" (22030, output std-ch).
 qShortSale:screen-value in frame fSection = std-ch.
 publish "GetBackgroundAnswer" (22035, output std-ch).
 qOther:screen-value in frame fSection = std-ch.

 publish "GetBackgroundAnswer" (22040, output std-ch).
 qEmployees:screen-value in frame fSection = std-ch.
 publish "GetBackgroundAnswer" (22045, output std-ch).
 qTitle:screen-value in frame fSection = std-ch.
 publish "GetBackgroundAnswer" (22050, output std-ch).
 qEscrow:screen-value in frame fSection = std-ch.

 &scoped-define questionType Background
 {lib/dispanswer.i &seq=22055}
 {lib/dispanswer.i &seq=22060}
 {lib/dispanswer.i &seq=22065}
 {lib/dispanswer.i &seq=22070}
 {lib/dispanswer.i &seq=22075}
 {lib/dispanswer.i &seq=22080}
 {lib/dispanswer.i &seq=22085}
 {lib/dispanswer.i &seq=22090}
 {lib/dispanswer.i &seq=22095}
 {lib/dispanswer.i &seq=22100}
 {lib/dispanswer.i &seq=22105}
 {lib/dispanswer.i &seq=22110}
 {lib/dispanswer.i &seq=22115}
 {lib/dispanswer.i &seq=22120}
 {lib/dispanswer.i &seq=22125}
 {lib/dispanswer.i &seq=22130}
 {lib/dispanswer.i &seq=22135}
 {lib/dispanswer.i &seq=22140}
 {lib/dispanswer.i &seq=22145}
 &undefine questionType

 assign
  qHistory:sensitive in frame fSection = true
  qRefinance:sensitive = true
  qPurchase:sensitive = true
  qBuilder:sensitive = true
  qForeclosure:sensitive = true
  qCommercial:sensitive = true
  qShortSale:sensitive = true
  qOther:sensitive = true
  qEmployees:sensitive in frame fSection = true
  qTitle:sensitive in frame fSection = true
  qEscrow:sensitive in frame fSection = true
  .
 
 refreshBrowse().
 frame fSection:sensitive = true.
 frame fMain:sensitive = true.
 
 publish "GetAuditStatus" (output pAuditStatus).
 
 if pAuditStatus = "C" then
   run AuditReadOnly.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditReadOnly C-Win 
PROCEDURE AuditReadOnly :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /*---------------not used--------------*/
     /*do with frame fSection:
 end.
 assign
   qHistory:sensitive     = false
   qRefinance:sensitive   = false
   qPurchase:sensitive    = false
   qBuilder:sensitive     = false 
   qForeclosure:sensitive = false
   qCommercial:sensitive  = false 
   qShortSale:sensitive   = false
   qOther:sensitive       = false
   qEmployees:sensitive   = false
   qTitle:sensitive       = false
   qEscrow:sensitive      = false
   .
 
 do with frame fMain:
 end.
 assign
   bAddBranch:sensitive    = false
   bDeleteBranch:sensitive = false
   .  */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Close C-Win 
PROCEDURE Close :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tautosave as logical no-undo.
  publish "GetAutosave" (output tautosave).
  if tautosave then                                          
    publish "SaveAfterEveryAnswer".
  apply 'WINDOW-CLOSE' to {&window-name} .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doButton C-Win 
PROCEDURE doButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/dobutton.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doCheckbox C-Win 
PROCEDURE doCheckbox :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/docheckbox.i &questionType="Background"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doFillin C-Win 
PROCEDURE doFillin :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/dofillin.i &questionType="Background" screenvalue}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE bAddBranch brwBranches 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  DISPLAY qHistory qRefinance qPurchase qBuilder qForeclosure qCommercial 
          qShortSale qOther qEmployees qTitle qEscrow 
      WITH FRAME fSection IN WINDOW C-Win.
  ENABLE qRefinance qPurchase qBuilder qForeclosure qCommercial qShortSale 
         qOther 
      WITH FRAME fSection IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fSection}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initializeFrame C-Win 
PROCEDURE initializeFrame PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  &scoped-define ex-frameresize true

  &scoped-define ex-u true
  &scoped-define ex-b true
  &scoped-define y-label H
  &scoped-define y-tip Hold for onsite audit 
  &scoped-define n-label S
  &scoped-define n-tip Send to auditor in advance 
  &scoped-define questionType Background

  {lib/question.i &seq=22040 &r="1.75 + 5.0" &ex-y=true &ex-n=true &ex-a=true} 
  {lib/question.i &seq=22045 &r="1.75 + 7.5" &ex-y=true &ex-n=true &ex-a=true }
  {lib/question.i &seq=22050 &r="1.75 + 10.0" &ex-y=true &ex-n=true &ex-a=true} 
  {lib/question.i &seq=22055 &r="1.75 + 12.5"} 
  {lib/question.i &seq=22060 &r="1.75 + 15"} 
  {lib/question.i &seq=22065 &r="1.75 + 17.5"} 
  {lib/question.i &seq=22070 &r="1.75 + 20"} 
  {lib/question.i &seq=22075 &r="1.75 + 22.5"} 
  {lib/question.i &seq=22080 &r="1.75 + 25"} 
  {lib/question.i &seq=22085 &r="1.75 + 27.5"} 
  {lib/question.i &seq=22090 &r="1.75 + 30"}
  {lib/question.i &seq=22095 &r="1.75 + 32.5"}
  {lib/question.i &seq=22100 &r="1.75 + 35"} 
  {lib/question.i &seq=22105 &r="1.75 + 37.5"}
  {lib/question.i &seq=22110 &r="1.75 + 40"}
  {lib/question.i &seq=22115 &r="1.75 + 42.5"}
  {lib/question.i &seq=22120 &r="1.75 + 45"}
  {lib/question.i &seq=22125 &r="1.75 + 47.5"}
  {lib/question.i &seq=22130 &r="1.75 + 50"}
  {lib/question.i &seq=22135 &r="1.75 + 52.5"}
  {lib/question.i &seq=22140 &r="1.75 + 55"}
  {lib/question.i &seq=22145 &r="1.75 + 57.5" &ex-r=true}

  
  RUN ShowScrollBars(FRAME fSection:HANDLE, NO, YES).


  run AuditOpened in this-procedure.    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE leaveAll C-Win 
PROCEDURE leaveAll :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
apply "leave" to qHistory in frame fSection.
apply "leave" to qRefinance in  frame fSection.
apply "leave" to qPurchase in  frame fSection.
apply "leave" to qBuilder in  frame fSection.
apply "leave" to qForeclosure in  frame fSection.
apply "leave" to qCommercial in  frame fSection.
apply "leave" to qShortSale in  frame fSection.
apply "leave" to qOther in  frame fSection.
apply "leave" to qEmployees in frame fSection.
apply "leave" to qTitle in  frame fSection.
apply "leave" to qEscrow in  frame fSection.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
if {&window-name}:window-state eq window-minimized  then
  {&window-name}:window-state = window-normal .
{&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortdata.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.

 browse brwBranches:width-pixels = frame fMain:width-pixels - 51.

 if {&window-name}:width-pixels > frame fSection:width-pixels 
  then
   do:
     frame fSection:width-pixels = {&window-name}:width-pixels.
     frame fSection:virtual-width-pixels = {&window-name}:width-pixels.
   end.
  else
   do:
     frame fSection:virtual-width-pixels = {&window-name}:width-pixels.
     frame fSection:width-pixels = {&window-name}:width-pixels.
         /* das: For some reason, shrinking a window size MAY cause the horizontal
            scroll bar.  The above sequence of widget setting should resolve it,
            but it doesn't every time.  So... */
     RUN ShowScrollBars(FRAME fSection:HANDLE, NO, YES).
   end.

 frame fSection:height-pixels = {&window-name}:height-pixels - 86.
 
 {lib/resizequestion.i &seq=22040}
 {lib/resizequestion.i &seq=22045}
 {lib/resizequestion.i &seq=22050}
 {lib/resizequestion.i &seq=22055}
 {lib/resizequestion.i &seq=22060}
 {lib/resizequestion.i &seq=22065}
 {lib/resizequestion.i &seq=22070}
 {lib/resizequestion.i &seq=22075}
 {lib/resizequestion.i &seq=22080}
 {lib/resizequestion.i &seq=22085}
 {lib/resizequestion.i &seq=22090}
 {lib/resizequestion.i &seq=22095}
 {lib/resizequestion.i &seq=22100}
 {lib/resizequestion.i &seq=22105}
 {lib/resizequestion.i &seq=22110}
 {lib/resizequestion.i &seq=22115}
 {lib/resizequestion.i &seq=22120}
 {lib/resizequestion.i &seq=22125}
 {lib/resizequestion.i &seq=22130}
 {lib/resizequestion.i &seq=22135}
 {lib/resizequestion.i &seq=22140}
 {lib/resizequestion.i &seq=22145}

 assign
   qEmployees:x = frame fSection:width-pixels - 200
   qTitle:x     = frame fsection:width-pixels - 200
   qEscrow:x    = frame fsection:width-pixels - 200.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshBrowse C-Win 
FUNCTION refreshBrowse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 close query brwBranches.
 publish "GetBranches" (output table branch).
 open query brwBranches for each branch by branch.name.
 bDeleteBranch:sensitive in frame fMain = available branch.
 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

