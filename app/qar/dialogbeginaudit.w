&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------
  File: dialogbeginaudit.w
  Description: Start audit form server
  Author: AG
  Created: 12.19.2016
  Modification:
    Date          Name      Description
    03/20/2017    AG        Filters Added.
    04/03/2017    AG        Modified filters
    09/11/2017    AG        Alternate color code in browse.
    10/06/2020    AC        Modified logic for TOR audits.
------------------------------------------------------------------------*/
def output parameter pOpen as logical init false.

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{tt/qaraudit.i &tableAlias="ttallqueuedaudit"}
{tt/qaraudit.i}          /* audit     */
def temp-table allqueuedaudit like ttallqueuedaudit
    fields caudittype as char.
{tt/agent.i}
{tt/state.i}

def var agentpair as char no-undo.
def var pState    as char no-undo.
def var tAgent    as char no-undo.
def var iBgcolor  as int  no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwAudit

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES allqueuedaudit

/* Definitions for BROWSE brwAudit                                      */
&Scoped-define FIELDS-IN-QUERY-brwAudit allqueuedaudit.cqarID allqueuedaudit.caudittype allqueuedaudit.agentId allqueuedaudit.name allqueuedaudit.addr allqueuedaudit.stateID   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAudit   
&Scoped-define SELF-NAME brwAudit
&Scoped-define QUERY-STRING-brwAudit for each allqueuedaudit by allqueuedaudit.auditStartDate descending                                        by allqueuedaudit.agentID
&Scoped-define OPEN-QUERY-brwAudit open query {&SELF-NAME} for each allqueuedaudit by allqueuedaudit.auditStartDate descending                                        by allqueuedaudit.agentID.
&Scoped-define TABLES-IN-QUERY-brwAudit allqueuedaudit
&Scoped-define FIRST-TABLE-IN-QUERY-brwAudit allqueuedaudit


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwAudit}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bReset RECT-33 cSelectionYear tStateId ~
Agents brwAudit Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS cSelectionYear tStateId Agents 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getAudittype Dialog-Frame 
FUNCTION getAudittype RETURNS CHARACTER
  ( input audittype as char /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshBrowse Dialog-Frame 
FUNCTION refreshBrowse RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bReset  NO-FOCUS
     LABEL "Reset" 
     SIZE 4.8 BY 1.14 TOOLTIP "Reset local active files list".

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Open" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE Agents AS CHARACTER INITIAL "ALL" 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 65 BY 1 NO-UNDO.

DEFINE VARIABLE tStateId AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 19 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-33
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 121.2 BY 2.14.

DEFINE VARIABLE cSelectionYear AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE 
     SIZE 7 BY 1.57 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAudit FOR 
      allqueuedaudit SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAudit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAudit Dialog-Frame _FREEFORM
  QUERY brwAudit DISPLAY
      allqueuedaudit.cqarID label "Audit ID" format "x(10)"
 allqueuedaudit.caudittype label "Audit Type" format "x(13)"
 allqueuedaudit.agentId label "Agent ID" format "x(11)"
 allqueuedaudit.name label "Agent Name" width 35 format "x(80)"
 allqueuedaudit.addr label "Address" width 35 format "x(80)"
 allqueuedaudit.stateID label "State" format "x(8)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 121 BY 8.57 ROW-HEIGHT-CHARS .76 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bReset AT ROW 2 COL 117.6 WIDGET-ID 14 NO-TAB-STOP 
     cSelectionYear AT ROW 1.76 COL 10.4 NO-LABEL WIDGET-ID 16
     tStateId AT ROW 2.1 COL 18.8 WIDGET-ID 42
     Agents AT ROW 2.1 COL 45.2 WIDGET-ID 44
     brwAudit AT ROW 3.81 COL 3 WIDGET-ID 200
     Btn_Cancel AT ROW 12.67 COL 65
     Btn_OK AT ROW 12.71 COL 44
     "Year:" VIEW-AS TEXT
          SIZE 5.4 BY .62 AT ROW 2.29 COL 4.8 WIDGET-ID 18
     RECT-33 AT ROW 1.48 COL 3 WIDGET-ID 2
     SPACE(1.39) SKIP(10.43)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Start Audit"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
/* BROWSE-TAB brwAudit Agents Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR COMBO-BOX Agents IN FRAME Dialog-Frame
   ALIGN-L                                                              */
ASSIGN 
       brwAudit:ALLOW-COLUMN-SEARCHING IN FRAME Dialog-Frame = TRUE
       brwAudit:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX tStateId IN FRAME Dialog-Frame
   ALIGN-L                                                              */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAudit
/* Query rebuild information for BROWSE brwAudit
     _START_FREEFORM
open query {&SELF-NAME} for each allqueuedaudit by allqueuedaudit.auditStartDate descending
                                       by allqueuedaudit.agentID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAudit */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Start Audit */
DO:
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Agents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Agents Dialog-Frame
ON VALUE-CHANGED OF Agents IN FRAME Dialog-Frame /* Agent */
DO:  
/*   if Agents:screen-value = "ALL" then                                                                                   */
/*     Agents:tooltip = "ALL".                                                                                             */
/*   else                                                                                                                  */
/*     Agents:tooltip = entry( lookup(Agents:screen-value,Agents:list-item-pairs, ",") - 1 , Agents:list-item-pairs, ","). */
  
  if self:screen-value = ? then
       self:screen-value = " ".    
  if Agents:screen-value = tAgent then.
  else
    refreshBrowse().
    tAgent = Agents:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReset
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReset Dialog-Frame
ON CHOOSE OF bReset IN FRAME Dialog-Frame /* Reset */
DO:
  close query brwAudit.
  empty temp-table allqueuedaudit.
  if cSelectionYear:screen-value = ? then
  do:
    message "Please enter Valid year."
      view-as alert-box info buttons ok.
    return no-apply.
  end.
  publish "LoadAudits" (input integer(cSelectionYear:screen-value),
                        input tStateId:screen-value,
                        input Agents:screen-value,
                        output table ttallqueuedaudit).

  for each ttallqueuedaudit:
    create allqueuedaudit.
    buffer-copy ttallqueuedaudit to allqueuedaudit.
    assign
     allqueuedaudit.cqarID = string(ttallqueuedaudit.qarID)
     allqueuedaudit.caudittype = getAudittype(ttallqueuedaudit.audittype).
  end.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
  if can-find(first allqueuedaudit) then 
    Btn_OK:sensitive in frame {&frame-name} = true.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAudit
&Scoped-define SELF-NAME brwAudit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAudit Dialog-Frame
ON DEFAULT-ACTION OF brwAudit IN FRAME Dialog-Frame
DO: 
  apply "choose":U to btn_OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAudit Dialog-Frame
ON ROW-DISPLAY OF brwAudit IN FRAME Dialog-Frame
DO:
   if current-result-row("brwAudit") modulo 2 = 0 then
     iBgColor = 17.
   else
     iBgColor = 15.

   allqueuedaudit.cqarID:bgcolor in browse brwAudit  = iBgcolor.
   allqueuedaudit.agentId:bgcolor in browse brwAudit = iBgcolor.
   allqueuedaudit.name:bgcolor in browse brwAudit    = iBgcolor.
   allqueuedaudit.addr:bgcolor in browse brwAudit    = iBgcolor.
   allqueuedaudit.StateId:bgcolor in browse brwAudit = iBgcolor.
   allqueuedaudit.caudittype:bgcolor in browse brwAudit = iBgcolor.    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAudit Dialog-Frame
ON START-SEARCH OF brwAudit IN FRAME Dialog-Frame
DO:
 {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAudit Dialog-Frame
ON VALUE-CHANGED OF brwAudit IN FRAME Dialog-Frame
DO:
  enable Btn_OK with frame Dialog-Frame.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Open */
DO:
  if available allqueuedaudit then
    run doOpen (allqueuedaudit.qarid).
      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cSelectionYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cSelectionYear Dialog-Frame
ON VALUE-CHANGED OF cSelectionYear IN FRAME Dialog-Frame
DO:
  if cSelectionYear:screen-value = std-ch then.
  else
    refreshBrowse().
    std-ch = cSelectionYear:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateId
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateId Dialog-Frame
ON VALUE-CHANGED OF tStateId IN FRAME Dialog-Frame /* State */
DO:
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bReset:load-image("images/s-refresh.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  {lib/get-state-list.i &combo=tStateId &addAll=true}
  {lib/get-agent-list.i &combo=Agents &state=tStateId &addAll=true}
  
  run enable_UI.
  {lib/set-current-value.i &state=tStateId}
  
  assign 
    cSelectionYear:list-items = string(year(today) + 1) + "," + string(year(today))
    cSelectionYear:screen-value = string(year(now))
    tAgent =  Agents:screen-value
    pState = tStateId:screen-value
    .
  
  if can-find(first allqueuedaudit) then 
    Btn_OK:sensitive in frame {&frame-name} = true.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doOpen Dialog-Frame 
PROCEDURE doOpen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  def input parameter pID as char no-undo.

  find first allqueuedaudit 
    where allqueuedaudit.qarid = integer(pid) 
    exclusive-lock no-error.
  
  if available(allqueuedaudit) then
  do:
    create audit . 
    buffer-copy allqueuedaudit to audit.
    publish "StartAudit" (input pid,
                          input table audit,
                          output std-lo).
    pOpen = std-lo.
  end.
  else
  do:
    message "Unable to start audit " pID "." skip(1)
            std-ch
      view-as alert-box info buttons ok.
  
    apply "window-close" to frame dialog-frame. /* Leave this procedure */ 
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cSelectionYear tStateId Agents 
      WITH FRAME Dialog-Frame.
  ENABLE bReset RECT-33 cSelectionYear tStateId Agents brwAudit Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData Dialog-Frame 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i } 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getAudittype Dialog-Frame 
FUNCTION getAudittype RETURNS CHARACTER
  ( input audittype as char /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if audittype = "E" then
  RETURN "ERR".   /* Function return value. */
  else if audittype = "Q"  then 
  RETURN "QAR".   /* Function return value. */
  else if audittype = "U"  then
  RETURN "Underwriter".   /* Function return value. */
  else if audittype = "T"  then
  RETURN "TOR".   /* Function return value. */
  else
  return "".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshBrowse Dialog-Frame 
FUNCTION refreshBrowse RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  empty temp-table allqueuedaudit.
  close query brwAudit.
  open query brwAudit for each allqueuedaudit by allqueuedaudit.auditStartDate descending
                                              by allqueuedaudit.agentID.
  return "".   
  /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

