&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wqar.w
   main QAR window
   D.Sinclair 10.15.2015
 */
 /*------------------------------------------------------------------------
    File        :   wqar.w
    Modification:
    Date          Name      Description
    11/28/2016    AG        Handle file specific section when file is
                            selected or not, remove max button and
                            modify resizeable property.
    12/05/2016    AC        Enable save button if there is any change 
                            in widgets's screen-value.
    12/12/2016    AC        Save contactOther, Notes and Status fields, 
                            changed "Delivered To" widget to editor.
    12/16/2016    AC        Modified to show selected file in title of file
                            specific section window.
    12/19/2016    AC        Remove focus from buttons when section screens are open
    12/20/2016    AG        Implement start\open\close\save functionality.   
    12/23/2016    AG        Implement close all section, autosave functionality.
    12/29/2016    AC        New IP created to enable Save Button
    01/11/2017    AC        Cancel Functionaliy
    01/18/2017    AC        Msg on audit close if findbp window is not closed
    02/14/2017    AC        Implement Inspect functionality.
    03/27/2017    AC        Handle validation for score in sections.
    03/28/2017    AC        Handle close of document on close of Audit.
    03/31/2017    AG        Modified to set review date and auditor in new function.   
    09/16/2020    AC        Changed to implement TOR Audits.
    07/15/2021    SA        Task 83510 modified to add points,score and type change.
    08/13/2021    SA        Add logic to update .dat files 
    02/07/2022    SA        Task#:90943 modified view finding according to sequence
    05/02/2022    VR        Task 93436 Enable 'Copy Active Review' menu item for all type
                            of audits and reflect the copied data after 'Copy Active Review'.
    11/28/2022    SA        Task#:99694 Enhancement in qar
    08/11/2023    AG        Task#:106419 Modified to show agent legal name instead of short name.
    11/20/2024    SC        Add last completed audit note while creating/scheduling an audit
  ----------------------------------------------------------------------*/

CREATE WIDGET-POOL.
{tt/doc.i &tableAlias="localQarFileInfo"}
{tt/agent.i}
{tt/qarrecentaudit.i}
{tt/qarauditfinding.i}   /* finding */
{tt/qarauditbp.i}        /* bestPractice */
{tt/qarauditaction.i}    /* action */
{tt/qaraccount.i}        /* account */
{tt/openwindow.i}

def var pAgentFileNumbers as char no-undo.
def var pAgentFileIDs as char no-undo.
def var activeAgentFileID as int no-undo.
def var activeAgentFileNumber as char no-undo.
define variable activeAuditID as int no-undo .
define variable tclicksave    as logical initial true.
define variable pSuccess as logical no-undo .

/* handles to auxiliary windows */
def var wUnanswered as handle.
def var wAnswered as handle.
def var wScoring as handle.
def var wFindings as handle.
def var wEscrow as handle.
def var wBkgNoc as handle.
def var wBkgAuditor as handle.
def var wBkgAgent as handle.
def var wReferences as handle no-undo.
def var hDocWindow  as handle no-undo.
def var hAgents  as handle no-undo.


def var wCalculator as handle no-undo.
def var wCalendar as handle no-undo.
def var wSection as handle extent 8 no-undo.
define variable hFindbp as handle extent 250 no-undo .
def var tLockOn as logical no-undo init false.
def var cFile as char no-undo.

{lib/std-def.i}
{lib/winlaunch.i}
{lib/winshowscrollbars.i}
{lib/ar-def.i}
{lib/rpt-defs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tAgentLookup RECT-27 RECT-30 RECT-31 RECT-32 
&Scoped-Define DISPLAYED-OBJECTS tScorePct tAgentID tName tAddr tCity ~
tState tZip tMainOffice tNumOffices tNumEmployees tNumUnderwriters ~
tContactName tContactPhone tContactFax tContactOther tContactEmail ~
tContactPosition tStat tParentName tParentAddr tParentCity tParentState ~
tParentZip tDeliveredTo tRanking tServices tAuditor tAuditDate ~
tAuditStartDate fOnsiteStDt fOnsiteFnDt tQarID fAuditType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getGrade C-Win 
FUNCTION getGrade RETURNS INTEGER
  ( input ipoints as integer /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getstatus C-Win 
FUNCTION getstatus RETURNS CHARACTER
  (input pstat as char /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD hideReview C-Win 
FUNCTION hideReview RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openWindowForAgent C-Win 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveCurrentField C-Win 
FUNCTION saveCurrentField RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setActionWidgetState C-Win 
FUNCTION setActionWidgetState RETURNS LOGICAL PRIVATE
  ( pIsOpen as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD showReview C-Win 
FUNCTION showReview RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_Module 
       MENU-ITEM m_Configure    LABEL "Configure..."  
       MENU-ITEM m_About        LABEL "About..."      
       RULE
       MENU-ITEM m_Exit         LABEL "Exit"          .

DEFINE SUB-MENU m_Review 
       MENU-ITEM m_Start        LABEL "Start..."      
       MENU-ITEM m_Restart      LABEL "Restart..."    
       RULE
       MENU-ITEM m_Open         LABEL "Open..."       
       MENU-ITEM m_Close        LABEL "Close..."      
       MENU-ITEM m_Save         LABEL "Save"          
       RULE
       MENU-ITEM m_Set_Audit_Type_to_QAR LABEL "Set Audit Type to QAR"
       MENU-ITEM m_Set_Audit_Type_to_ERR LABEL "Set Audit Type to ERR"
       MENU-ITEM m_Set_Audit_Type_to_TOR LABEL "Set Audit Type to TOR"
       RULE
       MENU-ITEM m_Finish       LABEL "Finish..."     .

DEFINE SUB-MENU m_Section 
       MENU-ITEM m_1            LABEL "1 - Overview"   ACCELERATOR "ALT-1"
       MENU-ITEM m_2            LABEL "2 - General"    ACCELERATOR "ALT-2"
       MENU-ITEM m_3            LABEL "3 - Search"     ACCELERATOR "ALT-3"
       MENU-ITEM m_4            LABEL "4 - Closing"    ACCELERATOR "ALT-4"
       MENU-ITEM m_5            LABEL "5 - Post Closing" ACCELERATOR "ALT-5"
       MENU-ITEM m_6            LABEL "6 - Escrow"     ACCELERATOR "ALT-6"
       MENU-ITEM m_7            LABEL "7 - Production" ACCELERATOR "ALT-7".

DEFINE SUB-MENU m_View 
       MENU-ITEM m_Documents    LABEL "Documents"     
              DISABLED
       MENU-ITEM m_Agent_Files  LABEL "Agent Files"   
       RULE
       MENU-ITEM m_Escrow_Reconciliation LABEL "Escrow Reconciliation"
       RULE
       MENU-ITEM m_Background_Operations LABEL "Background - Operations"
       MENU-ITEM m_Background_Auditor LABEL "Background - Auditor"
       MENU-ITEM m_Background_Agent LABEL "Background - Agent".

DEFINE SUB-MENU m_Progress 
       MENU-ITEM m_Unanswered_Questions LABEL "Unanswered Questions"
       MENU-ITEM m_Answered_Questions LABEL "Answered Questions"
       MENU-ITEM m_Findings_and_Best_Practices LABEL "Findings and Best Practices"
       RULE
       MENU-ITEM m_Scoring_and_Properties LABEL "Scoring and Properties".

DEFINE SUB-MENU m_Tools 
       MENU-ITEM m_Gap_Calendar LABEL "Gap Calendar"  
       MENU-ITEM m_References   LABEL "References"    
       MENU-ITEM m_Agents       LABEL "Agents"        
       MENU-ITEM m_Copy_Active_Review LABEL "Copy Active Review".

DEFINE SUB-MENU m_Reports 
       MENU-ITEM m_Preliminary_Report LABEL "Preliminary Report"
       RULE
       MENU-ITEM m_Internal_Review LABEL "Internal Review"
       MENU-ITEM m_Questionnaire LABEL "Questionnaire" 
       RULE
       MENU-ITEM m_Claims_for_Agent LABEL "Claims by Agent"
       RULE
       MENU-ITEM m_Agent_Summary_Report LABEL "Agent Summary Report".

DEFINE SUB-MENU m_Actions 
       MENU-ITEM m_Cancel       LABEL "Cancel..."     
       RULE
       MENU-ITEM m_Import       LABEL "Import..."     
       MENU-ITEM m_Export       LABEL "Export..."     
       RULE
       MENU-ITEM m_Inspect      LABEL "Inspect..."    .

DEFINE SUB-MENU m_Informal 
       MENU-ITEM m_Informal_New LABEL "New"           
       MENU-ITEM m_Informal_Open LABEL "Open"          
       MENU-ITEM m_Informal_Close LABEL "Close"         
       MENU-ITEM m_Informal_Save LABEL "Save"          .

DEFINE MENU MENU-BAR-C-Win MENUBAR
       SUB-MENU  m_Module       LABEL "Module"        
       SUB-MENU  m_Review       LABEL "Review"        
       SUB-MENU  m_Section      LABEL "Section"       
       SUB-MENU  m_View         LABEL "View"          
       SUB-MENU  m_Progress     LABEL "Progress"      
       SUB-MENU  m_Tools        LABEL "Tools"         
       SUB-MENU  m_Reports      LABEL "Reports"       
       SUB-MENU  m_Actions      LABEL "Actions"       
       SUB-MENU  m_Informal     LABEL "Informal"      .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bBtnCalendar  NO-FOCUS
     LABEL "Cal" 
     SIZE 7.2 BY 1.71 TOOLTIP "Gap Calendar".

DEFINE BUTTON bBtnClose  NO-FOCUS
     LABEL "Close" 
     SIZE 7.2 BY 1.71 TOOLTIP "Close".

DEFINE BUTTON bBtnFiles  NO-FOCUS
     LABEL "Files" 
     SIZE 7.2 BY 1.71 TOOLTIP "Agent files".

DEFINE BUTTON bBtnOpen  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open".

DEFINE BUTTON bBtnPrelimRpt  NO-FOCUS
     LABEL "Prelim" 
     SIZE 7.2 BY 1.71 TOOLTIP "Produce a preliminary summary report".

DEFINE BUTTON bBtnReferences  NO-FOCUS
     LABEL "Refs" 
     SIZE 7.2 BY 1.71 TOOLTIP "References".

DEFINE BUTTON bBtnSave  NO-FOCUS
     LABEL "Save" 
     SIZE 7.2 BY 1.71 TOOLTIP "Save".

DEFINE BUTTON bBtnSpellCheck  NO-FOCUS
     LABEL "Spl" 
     SIZE 7.2 BY 1.71 TOOLTIP "Check Spelling".

DEFINE BUTTON bBtnStart  NO-FOCUS
     LABEL "Start" 
     SIZE 7.2 BY 1.71 TOOLTIP "Start a queued review".

DEFINE BUTTON bBtnSubmit  NO-FOCUS
     LABEL "Finish" 
     SIZE 7.2 BY 1.71 TOOLTIP "Finalize the review (no further changes allowed)".

DEFINE VARIABLE tActiveFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "File","File"
     DROP-DOWN-LIST
     SIZE 32 BY 1 TOOLTIP "Select the agency file that is being reviewed at this time"
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE tAuditScore AS INTEGER FORMAT ">>>":U INITIAL 0 
     LABEL "Points" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-bfile
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 30.4 BY 1.86.

DEFINE RECTANGLE RECT-bfile-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 47.2 BY 1.86.

DEFINE RECTANGLE RECT-bfile-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 23.4 BY 1.86.

DEFINE RECTANGLE RECT-bfile-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 15.8 BY 1.86.

DEFINE RECTANGLE RECT-bfile-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17 BY 1.86.

DEFINE BUTTON bSection-1  NO-FOCUS
     LABEL "1-Overview" 
     SIZE 15 BY 1.14 TOOLTIP "Open Section 1 (Alt+1)".

DEFINE BUTTON bSection-2  NO-FOCUS
     LABEL "2-General" 
     SIZE 15 BY 1.14 TOOLTIP "Open Section 2 (Alt+2)".

DEFINE BUTTON bSection-3  NO-FOCUS
     LABEL "3-Search" 
     SIZE 15 BY 1.14 TOOLTIP "Open Section 3 (Alt+3)".

DEFINE BUTTON bSection-4  NO-FOCUS
     LABEL "4-Closing" 
     SIZE 15 BY 1.14 TOOLTIP "Open Section 4 (Alt+4)".

DEFINE BUTTON bSection-5  NO-FOCUS
     LABEL "5-Post Closing" 
     SIZE 15 BY 1.14 TOOLTIP "Open Section 5 (Alt+5)".

DEFINE BUTTON bSection-6  NO-FOCUS
     LABEL "6-Escrow" 
     SIZE 15 BY 1.14 TOOLTIP "Open Section 6 (Alt+6)".

DEFINE BUTTON bSection-7  NO-FOCUS
     LABEL "7-Production" 
     SIZE 15 BY 1.14 TOOLTIP "Open Section 7 (Alt+7)".

DEFINE BUTTON tAgentLookup 
     LABEL "..." 
     SIZE 4.4 BY 1.1.

DEFINE VARIABLE tServices AS CHARACTER FORMAT "X(30)":U 
     LABEL "Services" 
     VIEW-AS COMBO-BOX INNER-LINES 3
     LIST-ITEM-PAIRS "Title and Escrow","Both",
                     "Title only","Title",
                     "Escrow only","Escrow"
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tDeliveredTo AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 45.6 BY 2.1 TOOLTIP "List the recipients of the preliminary and final reports" NO-UNDO.

DEFINE VARIABLE tNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 128 BY 4.48 TOOLTIP "Miscellaneous Notes" NO-UNDO.

DEFINE VARIABLE fAuditType AS CHARACTER FORMAT "X(10)":U 
     LABEL "AuditType" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Audit Type" NO-UNDO.

DEFINE VARIABLE fOnsiteFnDt AS DATE FORMAT "99/99/99":U 
     LABEL "Onsite Finish" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fOnsiteStDt AS DATE FORMAT "99/99/99":U 
     LABEL "Onsite Start" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tAddr AS CHARACTER FORMAT "X(40)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 118.8 BY 1 TOOLTIP "Enter the agency's street address" NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER FORMAT "X(10)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Enter the agency's identifier assigned by Alliant" NO-UNDO.

DEFINE VARIABLE tAuditDate AS DATE FORMAT "99/99/99":U 
     LABEL "Review" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tAuditor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Reviewer(s)" 
     VIEW-AS FILL-IN 
     SIZE 45.6 BY 1 NO-UNDO.

DEFINE VARIABLE tAuditStartDate AS DATE FORMAT "99/99/99":U 
     LABEL "Audit Start" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tCity AS CHARACTER FORMAT "x(40)":U 
     VIEW-AS FILL-IN 
     SIZE 34.8 BY 1 TOOLTIP "Enter the city" NO-UNDO.

DEFINE VARIABLE tContactEmail AS CHARACTER FORMAT "x(40)":U 
     LABEL "Email" 
     VIEW-AS FILL-IN 
     SIZE 50 BY 1 TOOLTIP "Enter the contact's primary email address" NO-UNDO.

DEFINE VARIABLE tContactFax AS CHARACTER FORMAT "x(14)":U 
     LABEL "Fax" 
     VIEW-AS FILL-IN 
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE tContactName AS CHARACTER FORMAT "X(60)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 50 BY 1 TOOLTIP "Enter the contact's full name" NO-UNDO.

DEFINE VARIABLE tContactOther AS CHARACTER FORMAT "x(14)":U 
     LABEL "Other" 
     VIEW-AS FILL-IN 
     SIZE 26 BY 1 TOOLTIP "Enter the contact's secondary phone number" NO-UNDO.

DEFINE VARIABLE tContactPhone AS CHARACTER FORMAT "x(14)":U 
     LABEL "Phone" 
     VIEW-AS FILL-IN 
     SIZE 26 BY 1 TOOLTIP "Enter the contact's primary phone number" NO-UNDO.

DEFINE VARIABLE tContactPosition AS CHARACTER FORMAT "X(256)":U 
     LABEL "Job Title" 
     VIEW-AS FILL-IN 
     SIZE 50 BY 1 NO-UNDO.

DEFINE VARIABLE tLastRevenue AS DECIMAL FORMAT "->>,>>>,>>>.99":U INITIAL 0 
     LABEL "Net Revenue $" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 TOOLTIP "Enter the annual net revenue as reported for this agency" NO-UNDO.

DEFINE VARIABLE tName AS CHARACTER FORMAT "X(150)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 118.8 BY 1 TOOLTIP "Enter the full business name of the agency" NO-UNDO.

DEFINE VARIABLE tNumEmployees AS INTEGER FORMAT ">>,>>>":U INITIAL 0 
     LABEL "Employees" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Enter the total number of employees for the agency" NO-UNDO.

DEFINE VARIABLE tNumOffices AS INTEGER FORMAT ">>>":U INITIAL 0 
     LABEL "Offices" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 TOOLTIP "Enter the total number of locations for this agency" NO-UNDO.

DEFINE VARIABLE tNumUnderwriters AS INTEGER FORMAT ">>":U INITIAL 0 
     LABEL "Underwriters" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Enter the total number of title insurance underwriters working with this agency" NO-UNDO.

DEFINE VARIABLE tParentAddr AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1 NO-UNDO.

DEFINE VARIABLE tParentCity AS CHARACTER FORMAT "x(40)":U 
     VIEW-AS FILL-IN 
     SIZE 32 BY 1 TOOLTIP "Enter the city" NO-UNDO.

DEFINE VARIABLE tParentName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1 NO-UNDO.

DEFINE VARIABLE tParentState AS CHARACTER FORMAT "X(2)":U 
     VIEW-AS FILL-IN 
     SIZE 5 BY 1 NO-UNDO.

DEFINE VARIABLE tParentZip AS CHARACTER FORMAT "x(10)":U 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 TOOLTIP "Enter the Zipcode" NO-UNDO.

DEFINE VARIABLE tQarID AS CHARACTER FORMAT "X(10)":U 
     LABEL "Audit ID" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Enter the agency's identifier assigned by Alliant" NO-UNDO.

DEFINE VARIABLE tRanking AS INTEGER FORMAT ">>,>>>":U INITIAL 0 
     LABEL "Ranking" 
     VIEW-AS FILL-IN 
     SIZE 11.8 BY 1 NO-UNDO.

DEFINE VARIABLE tScorePct AS INTEGER FORMAT "zzz":U INITIAL 0 
     LABEL "Score%" 
      VIEW-AS TEXT 
     SIZE 5 BY .62 NO-UNDO.

DEFINE VARIABLE tStat AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tState AS CHARACTER FORMAT "X(2)":U 
     VIEW-AS FILL-IN 
     SIZE 5 BY 1 NO-UNDO.

DEFINE VARIABLE tZip AS CHARACTER FORMAT "x(10)":U 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 TOOLTIP "Enter the Zipcode" NO-UNDO.

DEFINE RECTANGLE RECT-27
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 64 BY 8.1.

DEFINE RECTANGLE RECT-30
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 63 BY 4.52.

DEFINE RECTANGLE RECT-31
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 64 BY 4.52.

DEFINE RECTANGLE RECT-32
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 63 BY 8.1.

DEFINE VARIABLE tMainOffice AS LOGICAL INITIAL yes 
     LABEL "Main Office" 
     VIEW-AS TOGGLE-BOX
     SIZE 15 BY .81 TOOLTIP "Select if the location you are auditing is the main office" NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 133 BY 32.05 WIDGET-ID 100.

DEFINE FRAME fMain
     bSection-1 AT ROW 1 COL 5 WIDGET-ID 92
     bSection-2 AT ROW 1 COL 23 WIDGET-ID 94
     bSection-3 AT ROW 1 COL 41 WIDGET-ID 96
     bSection-4 AT ROW 1 COL 59 WIDGET-ID 98
     bSection-5 AT ROW 1 COL 77 WIDGET-ID 100
     bSection-6 AT ROW 1 COL 95 WIDGET-ID 102
     bSection-7 AT ROW 1 COL 113 WIDGET-ID 104
     tScorePct AT ROW 2.67 COL 117.6 WIDGET-ID 130 NO-TAB-STOP 
     tAgentID AT ROW 3.76 COL 10 COLON-ALIGNED WIDGET-ID 2
     tName AT ROW 4.86 COL 10 COLON-ALIGNED WIDGET-ID 4
     tAddr AT ROW 5.95 COL 10 COLON-ALIGNED WIDGET-ID 6
     tCity AT ROW 7.05 COL 10 COLON-ALIGNED NO-LABEL WIDGET-ID 82
     tState AT ROW 7.05 COL 46 COLON-ALIGNED NO-LABEL WIDGET-ID 90
     tZip AT ROW 7.05 COL 52 COLON-ALIGNED NO-LABEL WIDGET-ID 10
     tMainOffice AT ROW 9.38 COL 5.8 WIDGET-ID 48
     tNumOffices AT ROW 10.33 COL 11 COLON-ALIGNED WIDGET-ID 36
     tNumEmployees AT ROW 9.14 COL 42.8 COLON-ALIGNED WIDGET-ID 38
     tNumUnderwriters AT ROW 10.33 COL 42.8 COLON-ALIGNED WIDGET-ID 40
     tLastRevenue AT ROW 11.52 COL 42.8 COLON-ALIGNED WIDGET-ID 44
     tContactName AT ROW 13.67 COL 11.8 COLON-ALIGNED WIDGET-ID 84
     tContactPhone AT ROW 14.86 COL 11.8 COLON-ALIGNED WIDGET-ID 18
     tContactFax AT ROW 16.05 COL 11.8 COLON-ALIGNED WIDGET-ID 20
     tContactOther AT ROW 17.24 COL 11.8 COLON-ALIGNED WIDGET-ID 22
     tContactEmail AT ROW 18.43 COL 11.8 COLON-ALIGNED WIDGET-ID 24
     tContactPosition AT ROW 19.62 COL 11.8 COLON-ALIGNED WIDGET-ID 66
     tStat AT ROW 2.67 COL 88 COLON-ALIGNED WIDGET-ID 118 NO-TAB-STOP 
     tParentName AT ROW 9.1 COL 75.8 COLON-ALIGNED WIDGET-ID 52
     tParentAddr AT ROW 10.29 COL 75.8 COLON-ALIGNED WIDGET-ID 54
     tParentCity AT ROW 11.48 COL 75.8 COLON-ALIGNED NO-LABEL WIDGET-ID 56
     tParentState AT ROW 11.48 COL 108.2 COLON-ALIGNED NO-LABEL WIDGET-ID 58
     tParentZip AT ROW 11.48 COL 113.8 COLON-ALIGNED NO-LABEL WIDGET-ID 60
     tDeliveredTo AT ROW 13.71 COL 83.2 NO-LABEL WIDGET-ID 46
     tRanking AT ROW 16.05 COL 81.2 COLON-ALIGNED WIDGET-ID 88
     tServices AT ROW 16.05 COL 104.8 COLON-ALIGNED WIDGET-ID 76
     tAuditor AT ROW 17.29 COL 81.2 COLON-ALIGNED WIDGET-ID 80
     tAuditDate AT ROW 18.52 COL 81.2 COLON-ALIGNED WIDGET-ID 42
     tAuditStartDate AT ROW 18.52 COL 112.8 COLON-ALIGNED WIDGET-ID 128
     fOnsiteStDt AT ROW 19.76 COL 81.2 COLON-ALIGNED WIDGET-ID 124
     tAgentLookup AT ROW 3.71 COL 26.4 WIDGET-ID 50 NO-TAB-STOP 
     fOnsiteFnDt AT ROW 19.76 COL 112.8 COLON-ALIGNED WIDGET-ID 126
     tNotes AT ROW 22.24 COL 2.6 NO-LABEL WIDGET-ID 114
     tQarID AT ROW 2.67 COL 10 COLON-ALIGNED WIDGET-ID 112 NO-TAB-STOP 
     fAuditType AT ROW 2.67 COL 53.2 COLON-ALIGNED WIDGET-ID 122 NO-TAB-STOP 
     "Location" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 8.19 COL 3.6 WIDGET-ID 70
          FONT 6
     "General" VIEW-AS TEXT
          SIZE 10 BY .62 AT ROW 12.95 COL 68.8 WIDGET-ID 74
          FONT 6
     "Notes" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 21.52 COL 2.6 WIDGET-ID 116
     "Contact" VIEW-AS TEXT
          SIZE 10 BY .62 AT ROW 12.95 COL 3.6 WIDGET-ID 86
          FONT 6
     "Parent Company" VIEW-AS TEXT
          SIZE 19 BY .62 AT ROW 8.19 COL 68.8 WIDGET-ID 64
          FONT 6
     "Delivered To:" VIEW-AS TEXT
          SIZE 13 BY .62 AT ROW 13.91 COL 69.8 WIDGET-ID 120
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2 ROW 2.95 SCROLLABLE  WIDGET-ID 200.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME fMain
     RECT-27 AT ROW 13.19 COL 2.8 WIDGET-ID 78
     RECT-30 AT ROW 8.43 COL 67.8 WIDGET-ID 62
     RECT-31 AT ROW 8.43 COL 2.8 WIDGET-ID 68
     RECT-32 AT ROW 13.19 COL 67.8 WIDGET-ID 72
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2 ROW 2.95 SCROLLABLE  WIDGET-ID 200.

DEFINE FRAME fButtons
     bBtnSpellCheck AT ROW 1.05 COL 62 WIDGET-ID 120 NO-TAB-STOP 
     tActiveFile AT ROW 1.38 COL 73.8 COLON-ALIGNED WIDGET-ID 28
     tAuditScore AT ROW 1.38 COL 123.6 COLON-ALIGNED WIDGET-ID 114 NO-TAB-STOP 
     bBtnClose AT ROW 1.05 COL 16.2 WIDGET-ID 8 NO-TAB-STOP 
     bBtnStart AT ROW 1.05 COL 1.8 WIDGET-ID 24 NO-TAB-STOP 
     bBtnFiles AT ROW 1.05 COL 108.6 WIDGET-ID 34 NO-TAB-STOP 
     bBtnSubmit AT ROW 1.05 COL 38.8 WIDGET-ID 22 NO-TAB-STOP 
     bBtnCalendar AT ROW 1.05 COL 54.6 WIDGET-ID 40 NO-TAB-STOP 
     bBtnOpen AT ROW 1.05 COL 9 WIDGET-ID 6 NO-TAB-STOP 
     bBtnPrelimRpt AT ROW 1.05 COL 31.6 WIDGET-ID 14 NO-TAB-STOP 
     bBtnReferences AT ROW 1.05 COL 47.2 WIDGET-ID 118 NO-TAB-STOP 
     bBtnSave AT ROW 1.05 COL 23.4 WIDGET-ID 10 NO-TAB-STOP 
     "File:" VIEW-AS TEXT
          SIZE 4.8 BY .62 AT ROW 1.57 COL 70.6 WIDGET-ID 44
     "Points:" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.57 COL 118.8 WIDGET-ID 116
     RECT-bfile AT ROW 1 COL 1 WIDGET-ID 4
     RECT-bfile-2 AT ROW 1 COL 69.8 WIDGET-ID 46
     RECT-bfile-3 AT ROW 1 COL 46.6 WIDGET-ID 48
     RECT-bfile-5 AT ROW 1 COL 31 WIDGET-ID 52
     RECT-bfile-6 AT ROW 1 COL 116.6 WIDGET-ID 112
    WITH 1 DOWN NO-BOX NO-HIDE KEEP-TAB-ORDER NO-HELP 
         NO-LABELS SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 1 ROW 1
         SIZE 133 BY 1.86 WIDGET-ID 300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = ""
         HEIGHT             = 27.71
         WIDTH              = 132
         MAX-HEIGHT         = 39.86
         MAX-WIDTH          = 288
         VIRTUAL-HEIGHT     = 39.86
         VIRTUAL-WIDTH      = 288
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-C-Win:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fButtons:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME fMain:FRAME = FRAME DEFAULT-FRAME:HANDLE.

/* SETTINGS FOR FRAME DEFAULT-FRAME
                                                                        */

DEFINE VARIABLE XXTABVALXX AS LOGICAL NO-UNDO.

ASSIGN XXTABVALXX = FRAME fButtons:MOVE-BEFORE-TAB-ITEM (FRAME fMain:HANDLE)
/* END-ASSIGN-TABS */.

/* SETTINGS FOR FRAME fButtons
                                                                        */
/* SETTINGS FOR BUTTON bBtnClose IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBtnFiles IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBtnPrelimRpt IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBtnSave IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBtnSpellCheck IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBtnSubmit IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-bfile IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-bfile-2 IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-bfile-3 IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-bfile-5 IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-bfile-6 IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX tActiveFile IN FRAME fButtons
   NO-DISPLAY NO-ENABLE LABEL "Agent:"                                  */
/* SETTINGS FOR FILL-IN tAuditScore IN FRAME fButtons
   NO-ENABLE LABEL "Points:"                                            */
ASSIGN 
       tAuditScore:READ-ONLY IN FRAME fButtons        = TRUE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME Size-to-Fit Custom                                        */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE.

/* SETTINGS FOR BUTTON bSection-1 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bSection-1:PRIVATE-DATA IN FRAME fMain     = 
                "1".

/* SETTINGS FOR BUTTON bSection-2 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bSection-2:PRIVATE-DATA IN FRAME fMain     = 
                "2".

/* SETTINGS FOR BUTTON bSection-3 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bSection-3:PRIVATE-DATA IN FRAME fMain     = 
                "3".

/* SETTINGS FOR BUTTON bSection-4 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bSection-4:PRIVATE-DATA IN FRAME fMain     = 
                "4".

/* SETTINGS FOR BUTTON bSection-5 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bSection-5:PRIVATE-DATA IN FRAME fMain     = 
                "5".

/* SETTINGS FOR BUTTON bSection-6 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bSection-6:PRIVATE-DATA IN FRAME fMain     = 
                "6".

/* SETTINGS FOR BUTTON bSection-7 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bSection-7:PRIVATE-DATA IN FRAME fMain     = 
                "7".

/* SETTINGS FOR FILL-IN fAuditType IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       fAuditType:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN fOnsiteFnDt IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fOnsiteStDt IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAddr IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tAddr:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tAgentID IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tAgentID:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tAgentLookup:HIDDEN IN FRAME fMain           = TRUE.

/* SETTINGS FOR FILL-IN tAuditDate IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAuditor IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAuditStartDate IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tAuditStartDate:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tCity IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tCity:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tContactEmail IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tContactFax IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tContactName IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tContactOther IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tContactPhone IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tContactPosition IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR tDeliveredTo IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tLastRevenue IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR TOGGLE-BOX tMainOffice IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tName IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tName:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR EDITOR tNotes IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tNumEmployees IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tNumOffices IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tNumUnderwriters IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tParentAddr IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tParentCity IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tParentName IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tParentState IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tParentZip IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tQarID IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tQarID:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tRanking IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tScorePct IN FRAME fMain
   NO-ENABLE ALIGN-L                                                    */
ASSIGN 
       tScorePct:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR COMBO-BOX tServices IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tStat IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tStat:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tState IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tState:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tZip IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tZip:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fButtons
/* Query rebuild information for FRAME fButtons
     _Query            is NOT OPENED
*/  /* FRAME fButtons */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fMain
/* Query rebuild information for FRAME fMain
     _Query            is NOT OPENED
*/  /* FRAME fMain */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win
DO:
  if tLockOn 
   then
    do: 
      MESSAGE "Please complete the Finding or Best Practice." VIEW-AS ALERT-BOX warning BUTTONS OK.
      return no-apply.
    end.
  
  run closeAudit in this-procedure no-error.
  if error-status:error 
   then return no-apply.

  publish "SetRecentAudits" (input table recentAudit).
  publish "ExitApplication".

  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fButtons
&Scoped-define SELF-NAME bBtnCalendar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnCalendar C-Win
ON CHOOSE OF bBtnCalendar IN FRAME fButtons /* Cal */
DO:
    run showCalendar in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnClose
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnClose C-Win
ON CHOOSE OF bBtnClose IN FRAME fButtons /* Close */
DO: 
   run closeAudit in this-procedure.
   publish "ResetLocalQarFiles".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnFiles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnFiles C-Win
ON CHOOSE OF bBtnFiles IN FRAME fButtons /* Files */
DO:
    def var tFileList as char.
    def var tActiveFileID as int.
    
    tActiveFileID = activeAgentFileID.
    run dialogfiles.w (input-output tActiveFileID,
                      output tFileList).
    
    /* tActiveFile is the combo-box */
    tActiveFile:list-item-pairs in frame fButtons = tFileList.
    if tFileList = "|0"
    then tActiveFile:sensitive = false.
    else tActiveFile:sensitive = true.
    tActiveFile:screen-value = string(tActiveFileID).
    
    /* Always publish as though "changed" as attributes may have been updated */
    activeAgentFileID = tActiveFileID.
    publish "ActiveFileChanged" (activeAgentFileID).
    
    publish "IsNew" (output std-lo).
    publish "AuditChanged" (std-lo).
    publish "ShowTitle" ( ENTRY( LOOKUP(tActiveFile:SCREEN-VALUE,tActiveFile:LIST-ITEM-PAIRS, "|") - 1, tActiveFile:LIST-ITEM-PAIRS, "|")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnOpen C-Win
ON CHOOSE OF bBtnOpen IN FRAME fButtons /* Open */
DO:  
  publish "ResetLocalQarFiles".
  run openAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnPrelimRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnPrelimRpt C-Win
ON CHOOSE OF bBtnPrelimRpt IN FRAME fButtons /* Prelim */
DO: apply "CHOOSE" to menu-item m_Preliminary_Report in menu m_Reports.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnReferences
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnReferences C-Win
ON CHOOSE OF bBtnReferences IN FRAME fButtons /* Refs */
DO:
 run showReferences in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnSave C-Win
ON CHOOSE OF bBtnSave IN FRAME fButtons /* Save */
DO:
  run saveAudit in this-procedure.
  publish "ResetLocalQarFiles".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnSpellCheck
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnSpellCheck C-Win
ON CHOOSE OF bBtnSpellCheck IN FRAME fButtons /* Spl */
DO:
  run SpellChecker in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnStart
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnStart C-Win
ON CHOOSE OF bBtnStart IN FRAME fButtons /* Start */
DO:
  run startAudit in this-procedure.
  publish "ResetLocalQarFiles".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnSubmit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnSubmit C-Win
ON CHOOSE OF bBtnSubmit IN FRAME fButtons /* Finish */
DO:
   run finishAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bSection-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSection-1 C-Win
ON CHOOSE OF bSection-1 IN FRAME fMain /* 1-Overview */
or 'choose' of bSection-2
 or 'choose' of bSection-3
 or 'choose' of bSection-4
 or 'choose' of bSection-5
 or 'choose' of bSection-6
 or 'choose' of bSection-7
DO:
  if valid-handle(wSection[integer(self:private-data)]) then
  do:
      run ShowWindow IN wSection[integer(self:private-data)].
      publish "ShowTitle" ( ENTRY( LOOKUP(tActiveFile:SCREEN-VALUE in frame fButtons,tActiveFile:LIST-ITEM-PAIRS in frame fButtons, "|") - 1, tActiveFile:LIST-ITEM-PAIRS in frame fButtons, "|")).
  end.
  else
    if not valid-handle(wSection[integer(self:private-data)]) then 
      run ViewSection (int(self:private-data)).
  showReview().
  RETURN.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAuditType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAuditType C-Win
ON VALUE-CHANGED OF fAuditType IN FRAME fMain /* AuditType */
DO:
   if self:modified 
  then
   {lib/setattrch.i audittype substring(self:screen-value,1,1)}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fOnsiteFnDt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fOnsiteFnDt C-Win
ON VALUE-CHANGED OF fOnsiteFnDt IN FRAME fMain /* Onsite Finish */
DO:
 if self:modified 
  then
    {lib/setattrda.i onsiteFnDate self:input-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fOnsiteStDt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fOnsiteStDt C-Win
ON VALUE-CHANGED OF fOnsiteStDt IN FRAME fMain /* Onsite Start */
DO:
 if self:modified 
  then
    {lib/setattrda.i onsiteStDate self:input-value}                                                         
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_1 C-Win
ON CHOOSE OF MENU-ITEM m_1 /* 1 - Overview */
or 'choose' of menu-item m_2 in menu m_Section
 or 'choose' of menu-item m_3 in menu m_Section
 or 'choose' of menu-item m_4 in menu m_Section
 or 'choose' of menu-item m_5 in menu m_Section
 or 'choose' of menu-item m_6 in menu m_Section
 or 'choose' of menu-item m_7 in menu m_Section
DO:
   if valid-handle(wSection[integer(substring(self:label, 1, 1))]) then
     run ShowWindow IN wSection[integer(substring(self:label,1,1))].
  else
     run ViewSection (int(substring(self:label,1,1))).
 RETURN.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_About
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_About C-Win
ON CHOOSE OF MENU-ITEM m_About /* About... */
DO:
 publish "AboutApplication".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agents C-Win
ON CHOOSE OF MENU-ITEM m_Agents /* Agents */
DO:
  if valid-handle(hAgents)
   then run ShowWindow in hAgents.
   else run referenceagent.w persistent set hAgents.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agent_Files
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agent_Files C-Win
ON CHOOSE OF MENU-ITEM m_Agent_Files /* Agent Files */
DO:
  apply "CHOOSE" to bBtnFiles in frame fButtons.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agent_Summary_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agent_Summary_Report C-Win
ON CHOOSE OF MENU-ITEM m_Agent_Summary_Report /* Agent Summary Report */
DO:
  run getAgentSummaryReport in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Answered_Questions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Answered_Questions C-Win
ON CHOOSE OF MENU-ITEM m_Answered_Questions /* Answered Questions */
DO:
 if valid-handle(wAnswered)
  then run showWindow in wAnswered.
  else run wanswered.w persistent set wAnswered. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Background_Agent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Background_Agent C-Win
ON CHOOSE OF MENU-ITEM m_Background_Agent /* Background - Agent */
DO:
    if valid-handle(wBkgAgent)
     then run ShowWindow in wBkgAgent.
     else run wbkgagent.w persistent set wBkgAgent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Background_Auditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Background_Auditor C-Win
ON CHOOSE OF MENU-ITEM m_Background_Auditor /* Background - Auditor */
DO:
    if valid-handle(wBkgAuditor)
     then run ShowWindow in wBkgAuditor.
     else run wbkgauditor.w persistent set wBkgAuditor.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Background_Operations
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Background_Operations C-Win
ON CHOOSE OF MENU-ITEM m_Background_Operations /* Background - Operations */
DO:
    if valid-handle(wBkgNoc)
     then
         run ShowWindow IN wBkgNoc.
     else run wbkgnoc.w persistent set wBkgNoc.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Cancel C-Win
ON CHOOSE OF MENU-ITEM m_Cancel /* Cancel... */
DO:
 run cancelAudit in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Claims_for_Agent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Claims_for_Agent C-Win
ON CHOOSE OF MENU-ITEM m_Claims_for_Agent /* Claims by Agent */
DO:
  do with frame {&frame-name}:
  end.
  run rpt\claimsbyagent.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Close
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Close C-Win
ON CHOOSE OF MENU-ITEM m_Close /* Close... */
DO:
 run closeAudit in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Configure
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Configure C-Win
ON CHOOSE OF MENU-ITEM m_Configure /* Configure... */
DO:
  run dialogoptions.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Copy_Active_Review
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Copy_Active_Review C-Win
ON CHOOSE OF MENU-ITEM m_Copy_Active_Review /* Copy Active Review */
DO:
   def var tFile as char.
   run dialogcopyqar.w (output std-lo, output tFile).
   showreview().
   tclicksave = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Documents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Documents C-Win
ON CHOOSE OF MENU-ITEM m_Documents /* Documents */
DO:
  publish "IsActive" (output std-lo).
  if not std-lo
   then return.

  if not valid-handle(hDocWindow)
   then run repository.w persistent set hDocWindow ("Audit", string(activeAuditID)).
   else run ShowWindow in hDocWindow.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Escrow_Reconciliation
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Escrow_Reconciliation C-Win
ON CHOOSE OF MENU-ITEM m_Escrow_Reconciliation /* Escrow Reconciliation */
DO:
 if valid-handle(wEscrow)
  then run showWindow in wEscrow.
  else run werr.w persistent set wEscrow.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Exit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Exit C-Win
ON CHOOSE OF MENU-ITEM m_Exit /* Exit */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Export
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Export C-Win
ON CHOOSE OF MENU-ITEM m_Export /* Export... */
DO:
  run exportAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Findings_and_Best_Practices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Findings_and_Best_Practices C-Win
ON CHOOSE OF MENU-ITEM m_Findings_and_Best_Practices /* Findings and Best Practices */
DO:
 if valid-handle(wFindings)
  then run showWindow in wFindings.
  else run wfindings.w persistent set wFindings.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Finish
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Finish C-Win
ON CHOOSE OF MENU-ITEM m_Finish /* Finish... */
DO:
  run finishAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Gap_Calendar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Gap_Calendar C-Win
ON CHOOSE OF MENU-ITEM m_Gap_Calendar /* Gap Calendar */
DO:
    run showCalendar in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Import
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Import C-Win
ON CHOOSE OF MENU-ITEM m_Import /* Import... */
DO:
  run importAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Informal_Close
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Informal_Close C-Win
ON CHOOSE OF MENU-ITEM m_Informal_Close /* Close */
DO:
  run closeInformal in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Informal_New
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Informal_New C-Win
ON CHOOSE OF MENU-ITEM m_Informal_New /* New */
DO:
  run newInformal in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Informal_Open
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Informal_Open C-Win
ON CHOOSE OF MENU-ITEM m_Informal_Open /* Open */
DO:
  run openInformal in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Informal_Save
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Informal_Save C-Win
ON CHOOSE OF MENU-ITEM m_Informal_Save /* Save */
DO:
  run saveInformal in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Inspect
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Inspect C-Win
ON CHOOSE OF MENU-ITEM m_Inspect /* Inspect... */
DO:
    run serverInspect in this-procedure.
  /*run recallAudit in this-procedure. */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Internal_Review
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Internal_Review C-Win
ON CHOOSE OF MENU-ITEM m_Internal_Review /* Internal Review */
DO:
  run generateReport in this-procedure ("InternalReport").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Module
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Module C-Win
ON MENU-DROP OF MENU m_Module /* Module */
or menu-drop of menu m_Review 
    or menu-drop of menu m_Section
    or menu-drop of menu m_View
    or menu-drop of menu m_Progress
    or menu-drop of menu m_Tools
    or menu-drop of menu m_Reports
    or menu-drop of menu m_Actions
DO:
/*     saveCurrentField(). */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Open
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Open C-Win
ON CHOOSE OF MENU-ITEM m_Open /* Open... */
DO:
  run openAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Preliminary_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Preliminary_Report C-Win
ON CHOOSE OF MENU-ITEM m_Preliminary_Report /* Preliminary Report */
DO:
  run generateReport in this-procedure ("PreliminaryReport").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Questionnaire
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Questionnaire C-Win
ON CHOOSE OF MENU-ITEM m_Questionnaire /* Questionnaire */
DO:
  run generateReport in this-procedure ("QuestionnaireReport").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_References
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_References C-Win
ON CHOOSE OF MENU-ITEM m_References /* References */
DO:
    run showReferences in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Restart
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Restart C-Win
ON CHOOSE OF MENU-ITEM m_Restart /* Restart... */
DO:
  run restartAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Save
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Save C-Win
ON CHOOSE OF MENU-ITEM m_Save /* Save */
DO:
  run saveAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Scoring_and_Properties
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Scoring_and_Properties C-Win
ON CHOOSE OF MENU-ITEM m_Scoring_and_Properties /* Scoring and Properties */
DO:
 if valid-handle(wScoring)
  then run showWindow in wScoring.
  else run wscoring.w persistent set wScoring.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Set_Audit_Type_to_ERR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Set_Audit_Type_to_ERR C-Win
ON CHOOSE OF MENU-ITEM m_Set_Audit_Type_to_ERR /* Set Audit Type to ERR */
DO:
 message "All the answers related to sections 1, 3, 4, 5, 7 and 8 including their Findings, Best Practices, Actions and Agent files will be lost." + chr(10) +
         "Are you sure you want to continue?"
       VIEW-AS ALERT-BOX question BUTTONS ok-cancel update lchoice as logical.
       
 if lchoice then
 do:
  activeAuditID = integer(tQarID:screen-value in frame fmain ).
  publish "EditAuditType" (input int(activeAuditID),
                           input "E",
                           input "I",
                           input "",
                           input "QAR",
                           output pSuccess).
   

   if pSuccess then
   do:
     fAuditType:screen-value in frame fMain = getstatus("E") .
     publish "SetAuditAttribute" ("audittype", input "E", input ?, input ?, input ?, input ?).
     publish "DeleteDataOnERR" .
     showReview() .
     publish "LeaveAll" .
     publish "close" .
     session:set-wait-state("general") .
     
     publish "AddLocalQarFile" (string(activeAuditID),"E", "", "") .
     setActionWidgetState(true) .
     session:set-wait-state("") .

     apply "choose" to  bBtnSave in frame fButtons.
   end. 
 end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Set_Audit_Type_to_QAR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Set_Audit_Type_to_QAR C-Win
ON CHOOSE OF MENU-ITEM m_Set_Audit_Type_to_QAR /* Set Audit Type to QAR */
DO:
 MESSAGE "Audit will be changed to type QAR.  Continue?."
       VIEW-AS ALERT-BOX question BUTTONS ok-cancel update lchoice as logical.
       
 if lchoice then
 do:
  activeAuditID = integer(tQarID:screen-value in frame fmain ).

   publish "EditAuditType" (input int(activeAuditID) ,
                         input "Q",
                         input "I",
                         input "",
                         input "QAR",
                         output pSuccess).
   
   if pSuccess then
   do:
     fAuditType:screen-value in frame fMain = getstatus("Q").
     publish "SetAuditAttribute" ("audittype", input "Q", input ?, input ?, input ?, input ?).     
     publish "DeleteDataOnQAR" .
     showReview() .
     publish "LeaveAll" .
     publish "close" .
     session:set-wait-state("general").
     publish "AddLocalQarFile" (string(activeAuditID),"Q", "", "").
     setActionWidgetState(true).
     session:set-wait-state("").

     apply "choose" to  bBtnSave in frame fButtons.
   end. 
 end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Set_Audit_Type_to_TOR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Set_Audit_Type_to_TOR C-Win
ON CHOOSE OF MENU-ITEM m_Set_Audit_Type_to_TOR /* Set Audit Type to TOR */
DO:
 message "All the answers related to section 6 including their Findings, Best Practices, Actions and Agent files will be lost." + chr(10) +
         "Are you sure you want to continue?"
       view-as alert-box question buttons ok-cancel update lchoice as logical.
 if lchoice then
 do:
  activeAuditID = integer(tQarID:screen-value in frame fmain ).
  publish "EditAuditType" (input int(activeAuditID),
                           input "T",
                           input "I",
                           input "",
                           input "QAR",
                           output pSuccess).
    
   if pSuccess then
   do:
     fAuditType:screen-value in frame fMain = getstatus("T").
     publish "SetAuditAttribute" ("audittype", input "T", input ?, input ?, input ?, input ?).     
     publish "DeleteDataOnTOR" .
     showReview().
     publish "LeaveAll" .
     publish "close" .
     session:set-wait-state("general").
     
     publish "AddLocalQarFile" (string(activeAuditID),"T", "", "").
     setActionWidgetState(true).
     session:set-wait-state("").
     apply "choose" to  bBtnSave in frame fButtons.
   end. 
 end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Start
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Start C-Win
ON CHOOSE OF MENU-ITEM m_Start /* Start... */
DO:
  run startAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Unanswered_Questions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Unanswered_Questions C-Win
ON CHOOSE OF MENU-ITEM m_Unanswered_Questions /* Unanswered Questions */
DO:
 if valid-handle(wUnanswered)
  then run showWindow in wUnanswered.
  else run wunanswered.w persistent set wUnanswered.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fButtons
&Scoped-define SELF-NAME tActiveFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tActiveFile C-Win
ON VALUE-CHANGED OF tActiveFile IN FRAME fButtons /* Agent */
DO:
  do WITH FRAME fButtons:
  end.
  activeAgentFileID = int(self:screen-value).
  publish "ActiveFileChanged" (activeAgentFileID).
  publish "ShowTitle" ( ENTRY( LOOKUP(tActiveFile:SCREEN-VALUE,tActiveFile:LIST-ITEM-PAIRS, "|") - 1, tActiveFile:LIST-ITEM-PAIRS, "|")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME tAddr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAddr C-Win
ON VALUE-CHANGED OF tAddr IN FRAME fMain /* Address */
DO:
 if self:modified 
  then
   {lib/setattrch.i addr self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentID C-Win
ON VALUE-CHANGED OF tAgentID IN FRAME fMain /* Agent ID */
DO:
 if self:modified 
  then
   {lib/setattrch.i agentID self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentLookup C-Win
ON CHOOSE OF tAgentLookup IN FRAME fMain /* ... */
DO:
  run server/getagents.p (tAgentID:screen-value in frame {&frame-name},
                          output table agent,
                          output std-lo,
                          output std-ch).
  
  if std-lo
   then
    for first agent no-lock
        where agent.agentID = tAgentID:screen-value:
          
      do with frame {&frame-name}:
        assign
          /* default the agent info */
          tName:screen-value = ""
          tAddr:screen-value = ""
          tCity:screen-value = ""
          tState:screen-value = ""
          tZip:screen-value = ""
          /* get the agent info */
          tName:screen-value = agent.name
          tAddr:screen-value = agent.addr1 +
                              (if agent.addr2 > "" then " " else "") + agent.addr2 +
                              (if agent.addr3 > "" then " " else "") + agent.addr3 +
                              (if agent.addr4 > "" then " " else "") + agent.addr4
          tCity:screen-value = agent.city
          tState:screen-value = agent.state
          tZip:screen-value = agent.zip
          .
        {lib/setattrch.i name tName:screen-value}
        {lib/setattrch.i addr tAddr:screen-value}
        {lib/setattrch.i city tCity:screen-value}
        {lib/setattrch.i state tState:screen-value}
        {lib/setattrch.i zip tZip:screen-value}
      end.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAuditDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAuditDate C-Win
ON VALUE-CHANGED OF tAuditDate IN FRAME fMain /* Review */
DO:
 if self:modified 
  then
  do:
    {lib/setattrda.i auditDate self:input-value}
  end.
  else do:
    publish "GetReviewAuditor" (output std-da , output std-ch).
    date(tAuditdate:screen-value) = std-da.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAuditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAuditor C-Win
ON VALUE-CHANGED OF tAuditor IN FRAME fMain /* Reviewer(s) */
DO:
 if self:modified 
  then
   {lib/setattrch.i auditor self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tCity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tCity C-Win
ON VALUE-CHANGED OF tCity IN FRAME fMain
DO:
 if self:modified 
  then
   {lib/setattrch.i city self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tContactEmail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tContactEmail C-Win
ON VALUE-CHANGED OF tContactEmail IN FRAME fMain /* Email */
DO:
 if self:modified 
  then
   {lib/setattrch.i contactEmail self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tContactFax
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tContactFax C-Win
ON VALUE-CHANGED OF tContactFax IN FRAME fMain /* Fax */
DO:
 if self:modified 
  then
   {lib/setattrch.i contactFax self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tContactName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tContactName C-Win
ON VALUE-CHANGED OF tContactName IN FRAME fMain /* Name */
DO:
 if self:modified 
  then
   do:
    {lib/setattrch.i contactName self:screen-value}
    if tDeliveredTo:screen-value = "" 
    then 
    do: tDeliveredTo:screen-value = self:screen-value.
        {lib/setattrch.i deliveredTo self:screen-value}
    end.
   end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tContactOther
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tContactOther C-Win
ON VALUE-CHANGED OF tContactOther IN FRAME fMain /* Other */
DO:
 if self:modified 
  then
   {lib/setattrch.i contactOther self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tContactPhone
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tContactPhone C-Win
ON VALUE-CHANGED OF tContactPhone IN FRAME fMain /* Phone */
DO:
 if self:modified 
  then
   {lib/setattrch.i contactPhone self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tContactPosition
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tContactPosition C-Win
ON VALUE-CHANGED OF tContactPosition IN FRAME fMain /* Job Title */
DO:
 if self:modified 
  then
   {lib/setattrch.i contactPosition self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDeliveredTo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDeliveredTo C-Win
ON VALUE-CHANGED OF tDeliveredTo IN FRAME fMain
DO:
 if self:modified 
  then
   {lib/setattrch.i deliveredTo self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tMainOffice
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tMainOffice C-Win
ON VALUE-CHANGED OF tMainOffice IN FRAME fMain /* Main Office */
DO:
  if self:modified then
  do:
    {lib/setattrlo.i mainOffice self:checked}
    run EnableSave.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tName C-Win
ON VALUE-CHANGED OF tName IN FRAME fMain /* Name */
DO:
 if self:modified 
  then
   {lib/setattrch.i name self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNotes C-Win
ON VALUE-CHANGED OF tNotes IN FRAME fMain
DO:
 if self:modified 
  then publish "SetAuditAnswer" ("Comments", input self:screen-value).
 publish "SaveAfterEveryAnswer".
 run EnableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNumEmployees
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNumEmployees C-Win
ON VALUE-CHANGED OF tNumEmployees IN FRAME fMain /* Employees */
DO:
 if self:modified 
  then
  {lib/setattrin.i numEmployees self:input-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNumOffices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNumOffices C-Win
ON VALUE-CHANGED OF tNumOffices IN FRAME fMain /* Offices */
DO:
 if self:modified 
  then
   {lib/setattrin.i numOffices self:input-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNumUnderwriters
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNumUnderwriters C-Win
ON VALUE-CHANGED OF tNumUnderwriters IN FRAME fMain /* Underwriters */
DO:
 if self:modified 
  then
   {lib/setattrin.i numUnderwriters self:input-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tParentAddr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tParentAddr C-Win
ON VALUE-CHANGED OF tParentAddr IN FRAME fMain /* Address */
DO:
 if self:modified 
  then
   {lib/setattrch.i parentAddr self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tParentCity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tParentCity C-Win
ON VALUE-CHANGED OF tParentCity IN FRAME fMain
DO:
 if self:modified 
  then
   {lib/setattrch.i parentCity self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tParentName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tParentName C-Win
ON VALUE-CHANGED OF tParentName IN FRAME fMain /* Name */
DO:
 if self:modified 
  then
   {lib/setattrch.i parentName self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tParentState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tParentState C-Win
ON VALUE-CHANGED OF tParentState IN FRAME fMain
DO:
 if self:modified 
  then
   {lib/setattrch.i parentState self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tParentZip
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tParentZip C-Win
ON VALUE-CHANGED OF tParentZip IN FRAME fMain
DO:
 if self:modified 
  then
   {lib/setattrch.i parentZip self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tRanking
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tRanking C-Win
ON VALUE-CHANGED OF tRanking IN FRAME fMain /* Ranking */
DO:
 if self:modified 
  then
   {lib/setattrin.i ranking self:input-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tScorePct
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tScorePct C-Win
ON VALUE-CHANGED OF tScorePct IN FRAME fMain /* Score% */
DO:
  if self:modified 
   then
    {lib/setattrch.i grade trim(self:screen-value,'%')}
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tServices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tServices C-Win
ON VALUE-CHANGED OF tServices IN FRAME fMain /* Services */
DO:
  if self:modified then
  do:
    {lib/setattrch.i services self:screen-value}
    run EnableSave.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tState C-Win
ON VALUE-CHANGED OF tState IN FRAME fMain
DO:
 if self:modified 
  then
   {lib/setattrch.i state self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tZip
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tZip C-Win
ON VALUE-CHANGED OF tZip IN FRAME fMain
DO:
 if self:modified 
  then
   {lib/setattrch.i zip self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

ON CLOSE OF THIS-PROCEDURE RUN disable_UI.

PAUSE 0 BEFORE-HIDE.

{lib/win-main.i}

subscribe to "startAudit" anywhere.
subscribe to "AuditChanged" anywhere.
subscribe to "ViewFinding" anywhere.
subscribe to "SelectAuditPage" anywhere.
subscribe to "DisplayScore" anywhere.
subscribe to "WindowClosed" anywhere.
subscribe to "WindowOpened" anywhere.
subscribe to "SetClickSave" anywhere.
subscribe to "AuditReadOnly" anywhere.
subscribe to "GetCurrentAuditType" anywhere.

subscribe to "LockOn" anywhere.
subscribe to "LockOff" anywhere.
subscribe to "SaveAfterEveryAnswer" anywhere .
subscribe to "StartNewAudit" anywhere.
subscribe to "EnableSave" anywhere.
subscribe to "LeaveAll" anywhere.
subscribe to "ActionWindowForAgent" anywhere.

publish "SetCurrentValue" ("Report","").
{&window-name}:window-state = 2.

bBtnOpen:load-image("images/open.bmp") in frame fButtons.
bBtnOpen:load-image-insensitive("images/open-i.bmp") in frame fButtons.
bBtnClose:load-image("images/close.bmp") in frame fButtons.
bBtnClose:load-image-insensitive("images/close-i.bmp") in frame fButtons.
bBtnSave:load-image("images/save.bmp") in frame fButtons.
bBtnSave:load-image-insensitive("images/save-i.bmp") in frame fButtons.

bBtnPrelimRpt:load-image("images/report.bmp") in frame fButtons.
bBtnPrelimRpt:load-image-insensitive("images/report-i.bmp") in frame fButtons.

bBtnSubmit:load-image("images/upload.bmp") in frame fButtons.
bBtnSubmit:load-image-insensitive("images/upload-i.bmp") in frame fButtons.
bBtnStart:load-image("images/download.bmp") in frame fButtons.
bBtnStart:load-image-insensitive("images/download-i.bmp") in frame fButtons.

bBtnFiles:load-image("images/docs.bmp") in frame fButtons.
bBtnFiles:load-image-insensitive("images/docs-i.bmp") in frame fButtons.

bBtnReferences:load-image("images/book.bmp") in frame fButtons.
bBtnReferences:load-image-insensitive("images/book-i.bmp") in frame fButtons.
bBtnCalendar:load-image("images/cal.bmp") in frame fButtons.
bBtnSpellCheck:load-image("images/spellcheck.bmp") in frame fButtons.
bBtnSpellCheck:load-image-insensitive("images/spellcheck-i.bmp") in frame fButtons.
tActiveFile:delimiter in frame fButtons = "|".

hideReview().
setActionWidgetState(false).
frame fMain:sensitive = false.

status input " " in window {&window-name}. 
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN enable_UI.
  setActionWidgetState(false).
  
  ON VALUE-CHANGED OF FRAME fMain ANYWHERE 
  do:
    run EnableSave.
    tclicksave = false.
  end.
  
  /* Sets the maximum width and height to that of the current
     Windows Resolution*/
  assign {&window-name}:max-width  = 133.00
         {&window-name}:max-height = 28
         {&window-name}:min-width  = 133.00
         {&window-name}:height     = 28
         {&window-name}:width      = 133
         tAgentLookup:hidden = true
         .
  
  {&window-name}:window-state = 3.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindowForAgent C-Win 
PROCEDURE ActionWindowForAgent :
/*------------------------------------------------------------------------------
@description Opens a window for an agent
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pType as character no-undo.
  define input parameter pWindow as character no-undo.
  
  openWindowForAgent(pAgentID, pType, pWindow).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditChanged C-Win 
PROCEDURE AuditChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pNew as logical.

 if not pNew
   then 
    assign
      menu-item m_Save:sensitive in menu m_Review = true
      bBtnSave:sensitive in frame fButtons = true
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditReadOnly C-Win 
PROCEDURE AuditReadOnly :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*----------not used-----------*/
/*    do with frame fButtons:
end.
assign
  bBtnFiles:sensitive  = false
  bBtnSave:sensitive   = false
  bBtnSubmit:sensitive = false
  bBtnSpellCheck:sensitive = false.

do with frame fMain:
end.
assign
  tAgentID:sensitive         = false
  tName:sensitive            = false
  tAddr:sensitive            = false
  tCity:sensitive            = false
  tState:sensitive           = false
  tZip:sensitive             = false
  tMainOffice:sensitive      = false
  tNumEmployees:sensitive    = false
  tNumUnderwriters:sensitive = false
  tNumOffices:sensitive      = false
  tLastRevenue:sensitive     = false
  tParentName:sensitive      = false
  tParentAddr:sensitive      = false
  tParentCity:sensitive      = false
  tParentState:sensitive     = false
  tParentZip:sensitive       = false
  tContactName:sensitive     = false
  tContactPhone:sensitive    = false
  tContactFax:sensitive      = false
  tContactOther:sensitive    = false
  tContactEmail:sensitive    = false
  tContactPosition:sensitive = false
  tDeliveredTo:sensitive     = false
  tAuditDate:sensitive       = false
  tAuditor:sensitive         = false
  tRanking:sensitive         = false
  tServices:sensitive        = false
  tNotes:sensitive           = false.

 /* Review */
 assign   
   menu-item m_Start:sensitive   in menu m_Review = false
   menu-item m_Restart:sensitive in menu m_Review = false
   menu-item m_Open:sensitive    in menu m_Review = false
   menu-item m_Save:sensitive    in menu m_Review = false
   menu-item m_Finish:sensitive  in menu m_Review = false.

  /* Actions */
 assign
   menu-item m_Cancel:sensitive in menu m_Actions = false
   menu-item m_Import:sensitive in menu m_Actions = false
   menu-item m_Export:sensitive in menu m_Actions = false
 /*  menu m_Inspect:sensitive     in menu m_Actions = false*/
   .
*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancelAudit C-Win 
PROCEDURE cancelAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var pReason as char .
 def var pCancel as logical.
 def var pSuccess as logical.
 run dialogcancel.w (input tQarID:screen-value in frame fMain,
                     input tAgentID:screen-value in frame fMain,
                     input tName:screen-value in frame fMain, 
                     output pReason,
                     output pCancel).
 if pCancel then
   return.
 publish "cancelAudit" (input tQarID:screen-value in frame fMain,
                        input pReason,
                        output pSuccess).
 if not pSuccess then
   return.
 publish "AuditClosed".
 publish "ActiveFileChanged" (0).
 hideReview().
 setActionWidgetState(false).
 frame fMain:sensitive = false.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseAudit C-Win 
PROCEDURE CloseAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var tOK as logical.
 define variable tAutoSave    as logical   no-undo.
 define variable pFnBPChanged as character no-undo.
 define variable pCorrectCase as character no-undo.
 define variable tCloseAll as logical      no-undo.
 define variable pAuditStatus as character no-undo.
 define variable iCount       as integer   no-undo.
 define variable sectionscore as character no-undo .
 define variable tInvalidSec  as character initial "f,f,f,f,f,f,f,f" no-undo.

 /* Informal */
 if fAuditType:screen-value in frame fMain = ""
  then
   do: run closeInformal in this-procedure.
       return. 
   end.

 publish "IsActive" (output tOk).
 if not tOk 
   then return.

 publish "SetCurrentValue" ("Report", "").
 publish "GetCloseSections" (output tCloseAll) .
 publish "GetAutoSave" (output tAutoSave ).
 publish "GetAuditStatus" (output pAuditStatus).
 status default "" in window {&window-name}.
 status input "" in window {&window-name}.

 if (tAutoSave) and (pAuditStatus ne "C") and (not tclicksave)then
 do:
   publish "LeaveAll" .
   do iCount = 1 to 8:
     if valid-handle(wSection[iCount]) then
     do:
       run CheckSectionScore in wSection[iCount] (input iCount,
                                                  output sectionscore).
       if integer(sectionscore) > 10 then
         entry(iCount,tInvalidSec,",") = "t".
       else
         entry(iCount,tInvalidSec,",") = "f".
     end.
       else
         entry(iCount,tInvalidSec,",") = "f".
   end.
   if lookup("t",tInvalidSec,"," ) gt 0 then
      return error .
 end.


  if (not tclicksave) and (not tAutoSave )then
  do:
    publish "LeaveAll" .
    do iCount = 1 to 8:
      if valid-handle(wSection[iCount]) then
      do:
        run CheckSectionScore in wSection[iCount] (input iCount,
                                                   output sectionscore).
     
        if integer(sectionscore) > 10 then
          entry(iCount,tInvalidSec,",") = "t".
        else
          entry(iCount,tInvalidSec,",") = "f".
      end.
      else
        entry(iCount,tInvalidSec,",") = "f".
    end.
    if lookup("t",tInvalidSec,"," ) gt 0 then
      return error .      
  end.


 if tclicksave then
 do:
   do iCount = 1 to 8:
     if valid-handle(wSection[iCount]) then
     do:
       run CheckSectionScore in wSection[iCount](input iCount,
                                                 output sectionscore).
       if integer(sectionscore) > 10 then
         entry(iCount,tInvalidSec,",") = "t".
       else
         entry(iCount,tInvalidSec,",") = "f".
     end.
      else
         entry(iCount,tInvalidSec,",") = "f".
    end.
    if lookup("t",tInvalidSec,"," ) gt 0 then
    do:
      MESSAGE "Score must be 0 - 10"
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return error .
    end.
 end.

 tCloseAll = false.
 if not tAutoSave then
 do:
   publish "IsChanged" (output tOk).
   if tOk 
    then
     do:
       publish "IsNew" (output tOk).
       if not tOk
         then 
         assign
           menu-item m_Save:sensitive in menu m_Review = true
           bBtnSave:sensitive in frame fButtons = true
            .

         MESSAGE "Audit has been modified. All changes will be discarded."
          VIEW-AS ALERT-BOX question BUTTONS ok-cancel set tOK.
       if not tOK 
        then return error.
       else
        run closewindows .
     end.
     else 
       run closewindows .
 end.
 else 
   run closewindows .

 pFnBPChanged = "" . 
 pCorrectCase = "".
 publish "IsModifiedFindBp" (input "auditclose",
                            output pFnBPChanged,
                            output pCorrectCase).
 if tCloseAll = false then
 do:
   if (pFnBPChanged = "auditclose" and pCorrectCase = "") or (pCorrectCase = "Not" and pFnBPChanged = "auditclose" )then 
   do:
     MESSAGE "Information on Finding and Best Practices has been modified. All changes will be discarded." skip 
             "Do you want to continue?"
       VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE lChoice AS LOGICAL.
     if lChoice = true then
     publish "CloseFindBp" ("auditclose").
     else 
     do:
       run EnableSave .
       return error.
     end.
     pFnBPChanged = "".
   end.
   else
     publish "CloseFindBp" ("auditclose").
 end.
 session:set-wait-state("general").
 run WindowClosed ("audit").
 publish "CloseAudit".
 session:set-wait-state("").

 hideReview().
 setActionWidgetState(false).
 frame fMain:sensitive = false.

 publish "AuditClosed".
 publish "ActiveFileChanged" (0).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeInformal C-Win 
PROCEDURE closeInformal :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tOK as logical.
/*  define variable tAutoSave    as logical   no-undo. */
 define variable pFnBPChanged as character no-undo.
 define variable pCorrectCase as character no-undo.
 define variable tCloseAll as logical      no-undo.
 define variable pAuditStatus as character no-undo.
 define variable iCount       as integer   no-undo.
 define variable sectionscore as character no-undo .
 define variable tInvalidSec  as character initial "f,f,f,f,f,f,f,f" no-undo.

 publish "IsActive" (output tOk).
 if not tOk 
   then return.

 publish "SetCurrentValue" ("Report", "").
/*  publish "GetAutoSave" (output tAutoSave ). */
 publish "GetAuditStatus" (output pAuditStatus).
 status default "" in window {&window-name}.
 status input "" in window {&window-name}.

/*  if (tAutoSave) and (pAuditStatus ne "C") and (not tclicksave)then */

 do iCount = 1 to 8:
  if valid-handle(wSection[iCount]) 
   then
    do:
        run CheckSectionScore in wSection[iCount] (input iCount,
                                          output sectionscore).
        if integer(sectionscore) > 10 
         then entry(iCount,tInvalidSec,",") = "t".
         else entry(iCount,tInvalidSec,",") = "f".
    end.
   else entry(iCount,tInvalidSec,",") = "f".
 end.
 if lookup("t",tInvalidSec,"," ) gt 0 
  then 
   do: if tClickSave
        then MESSAGE "Section score(s) must be in the range 0 - 10."
               VIEW-AS ALERT-BOX ERROR BUTTONS OK.
       return error.
   end.

   tCloseAll = false.
   publish "IsChanged" (output tOk).
   if tOk 
    then
     do: 
         MESSAGE "Audit has been modified. All changes will be discarded."
           VIEW-AS ALERT-BOX question BUTTONS ok-cancel set tOK.
         if not tOK 
          then return error.
     end.

 run closeWindows in this-procedure.

 pFnBPChanged = "" . 
 pCorrectCase = "".
 publish "IsModifiedFindBp" (input "auditclose",
                            output pFnBPChanged,
                            output pCorrectCase).
 if tCloseAll = false 
  then
   do:
   if (pFnBPChanged = "auditclose" and pCorrectCase = "") or (pCorrectCase = "Not" and pFnBPChanged = "auditclose" )then 
   do:
     MESSAGE "Information on Finding and Best Practices has been modified. All changes will be discarded." skip 
             "Do you want to continue?"
       VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE lChoice AS LOGICAL.
     if lChoice = true then
     publish "CloseFindBp" ("auditclose").
     else 
     do:
       run EnableSave .
       return error.
     end.
     pFnBPChanged = "".
   end.
   else
     publish "CloseFindBp" ("auditclose").
 end.

 session:set-wait-state("general").
 run WindowClosed ("audit").
 publish "CloseAudit".
 session:set-wait-state("").

 hideReview().
 assign
   tAgentID:read-only in frame {&frame-name} = true
   tName:read-only in frame {&frame-name} = true
   tAddr:read-only in frame {&frame-name} = true
   tCity:read-only in frame {&frame-name} = true
   tState:read-only in frame {&frame-name} = true
   tZip:read-only in frame {&frame-name} = true
   .

 setActionWidgetState(false).

 frame fMain:sensitive = false.

 publish "AuditClosed".
 publish "ActiveFileChanged" (0).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closewindows C-Win 
PROCEDURE closewindows :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define var tCloseAll as logical .
  def var pActive as logical .
  define variable pFnBPChanged as character.
  define variable pCorrectCase as character.
  tCloseAll = false.
  pFnBPChanged = "".
  pCorrectCase = "".

  publish "GetCloseSections" (output tCloseAll) .
  publish "IsModifiedFindBp" (input "autoclose",
                              output pFnBPChanged,
                              output pCorrectCase).
  
  if tCloseAll then
  do:
    publish "isActive" (output pActive).
    if pActive = true then
      if (pFnBPChanged = "autoclose" and pCorrectCase = "") or (pCorrectCase = "Not" and pFnBPChanged = "autoclose" ) then 
      do:
        MESSAGE "Information on Finding and Best Practices has been modified. All changes will be discarded." skip 
               "Do you want to continue?"
          VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE lChoice AS LOGICAL.
        if lChoice = true then
        do:
          publish "CloseFindBp" ("autoclose").
          publish "Close" .                
        end.
        else return error.
        pFnBPChanged = "" .
      end.
    else
   do:
     publish "CloseFindBp" ("autoclose").
     publish "Close" .                                    
   end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayHeader C-Win 
PROCEDURE displayHeader :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var pQarID as char.
 def var pStat as char.
 def var pAgentID as char.
 def var pName as char.
 def var pAddr as char.
 def var pCity as char.
 def var pState as char.
 def var pZip as char.
 def var pAuditType as char.
 def var pNotes as char.

 {lib/getattrch.i qarID pQarID}
 {lib/getattrch.i stat pStat}
 {lib/getattrch.i agentID pAgentID}
 {lib/getattrch.i name pName}
 {lib/getattrch.i addr pAddr}
 {lib/getattrch.i city pCity}
 {lib/getattrch.i state pState}
 {lib/getattrch.i zip pZip}
 {lib/getattrch.i AuditType pAuditType}
 {lib/getattrch.i comments pNotes}
 
 if pStat = "A" then 
   pStat = "Active" .
 else if pStat = "C" then 
   pStat = "Completed".

 assign
   tQarID:screen-value in frame fMain   = pQarID
   tStat:screen-value in frame fMain    = pStat
   tAgentID:screen-value in frame fMain = pAgentID
   tName:screen-value in frame fMain    =  pName
   tAddr:screen-value in frame fMain    = pAddr
   tCity:screen-value in frame fMain    = pCity
   tState:screen-value in frame fMain   = pState
   tZip:screen-value in frame fMain     = pZip 
   fAuditType:screen-value in frame fMain = getstatus(pAuditType)
   tNotes:screen-value in frame fMain     = pNotes
   .

 run DisplayScore.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DisplayScore C-Win 
PROCEDURE DisplayScore :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 publish "GetAuditAnswer" ("Score", output std-ch).
 tAuditScore:screen-value in frame fButtons = std-ch.
 if fAuditType:screen-value in frame fMain ne '' 
  then
   do: 
     tScorePct:screen-value in frame fMain = string(getGrade(input integer(std-ch))).
     {lib/setattrch.i grade trim(tScorePct:screen-value,'%')}
   end.
 publish "SaveAfterEveryAnswer".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE EnableSave C-Win 
PROCEDURE EnableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if tStat:screen-value in frame fMain = "Completed" then 
    return.
  assign
    menu-item m_Save:sensitive in menu m_Review = true
    bBtnSave:sensitive in frame fButtons = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  VIEW FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  DISPLAY tAuditScore 
      WITH FRAME fButtons IN WINDOW C-Win.
  ENABLE bBtnStart bBtnCalendar bBtnOpen bBtnReferences 
      WITH FRAME fButtons IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fButtons}
  DISPLAY tScorePct tAgentID tName tAddr tCity tState tZip tMainOffice 
          tNumOffices tNumEmployees tNumUnderwriters tContactName tContactPhone 
          tContactFax tContactOther tContactEmail tContactPosition tStat 
          tParentName tParentAddr tParentCity tParentState tParentZip 
          tDeliveredTo tRanking tServices tAuditor tAuditDate tAuditStartDate 
          fOnsiteStDt fOnsiteFnDt tQarID fAuditType 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE tAgentLookup RECT-27 RECT-30 RECT-31 RECT-32 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportAudit C-Win 
PROCEDURE exportAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  def var tOK as logical.

  publish "IsActive" (output tOk).
  if not tOk 
   then return.

  publish "IsChanged" (output tOk).
  if tOk 
  then
   do:
     MESSAGE "Audit has been modified. All changes will be saved." view-as alert-box question buttons ok-cancel set tOK.
     if not tOK 
     then return error.
   end.


  def var tFile as char.
  def var doSave as logical.

  publish "GetAuditDir" (output std-ch).
  publish "GetFilenameFormatted" ("audit", output tFile).

  system-dialog get-file tFile
    filters "QAR Files" "*.qarn"
    initial-dir std-ch
    ask-overwrite
    create-test-file
    default-extension ".qarn"
    save-as
   update doSave.

  if not doSave 
  then return.

  session:set-wait-state("general").
  publish "SaveAudit".
  publish "ExportAudit" (tFile).
  session:set-wait-state("").

  hideReview().
  setActionWidgetState(false).
  frame fMain:sensitive = false.

  publish "AuditClosed".
  publish "ActiveFileChanged" (0).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE finishAudit C-Win 
PROCEDURE finishAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var tID as char no-undo.
 def var pSuccess as logical no-undo.
 define variable cMsgBody as character no-undo. 
 define variable cAuditStartDate as datetime no-undo.

 {lib/getattrda.i auditstartDate cAuditStartDate}


{lib/getattrin.i unansweredCount std-in}
 if std-in > 0 
  then
     MESSAGE "There are still " std-in " unanswered questions." skip
              "Do you want to continue and submit the audit?"
        VIEW-AS ALERT-BOX warning BUTTONS yes-no update std-lo.
  else MESSAGE "Audit will be submitted." skip(1)
         "Do you want to continue?"
        VIEW-AS ALERT-BOX INFO BUTTONS yes-no update std-lo.
 if not std-lo 
  then return.

 run saveAudit in this-procedure.

 publish "GetActiveFilename" (output std-ch).
 std-ch = search(std-ch).
 if std-ch = ? 
  then return.
  
 assign
     rpt-behavior        = "Q"
     rpt-destination     = "C"
     .

 session:set-wait-state("general").
 publish "FinishAudit" (input tQarID:screen-value in frame fMain,
                        input std-ch,
                        {lib/rpt-setparams.i},
                        output pSuccess,
                        output std-ch).
 session:set-wait-state("").

 if not pSuccess 
  then
   do:
       MESSAGE "Audit submission failed." skip(1)
               std-ch
        VIEW-AS ALERT-BOX error BUTTONS OK.
       return.
   end.

 
 session:set-wait-state("general").
 run closeAudit in this-procedure.
 session:set-wait-state("").

 message "Audit successfully finished."
   view-as alert-box info buttons ok.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE generateReport C-Win 
PROCEDURE generateReport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pReportFormat as character no-undo.

  def var doSave as logical init true.
  def var tReportDir as char.
  
  std-ch = "".
  publish "GetReportDir" (output tReportDir).
  publish "GetFilenameFormatted" (pReportFormat, output std-ch).
  
  case fAuditType:screen-value in frame {&frame-name}:
   when "ERR" then std-ch = replace(std-ch,"QAR","ERR").
   when "TOR" then std-ch = replace(std-ch,"QAR","TOR").
   when "" then std-ch = replace(std-ch,"QAR","Informal").
  end case.
  
  system-dialog get-file std-ch
   filters "PDF Files" "*.pdf"
   initial-dir tReportDir
   ask-overwrite
   create-test-file
   default-extension ".pdf"
   use-filename
   save-as
  update doSave.
  
  if not doSave 
   then return.
   
  publish "RunReport" (pReportFormat, std-ch).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getAgentSummaryReport C-Win 
PROCEDURE getAgentSummaryReport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 define variable lChoice as logical no-undo.
 
 
  do with frame fmain:
  end.

   if tAgentID:screen-value <> ""
   then
    do:
      message "Generating the report may take a few minutes. " skip
          "Continue?"
         view-as alert-box question buttons ok-cancel update lChoice.

      if not lChoice
       then
        return.

      run server/agentreportpdf.p(input tAgentID:screen-value,
                                  {lib/rpt-setparams.i},
                                  output cFile,
                                  output std-lo,
                                  output std-ch).
                            
      if not std-lo
       then
        do:
          if std-ch = ""
           then
            std-ch = "Report not saved.".
          message std-ch
            view-as alert-box error buttons ok.
          return.
        end.
       else if cFile <> ""
        then
         run util/openfile.p (cFile).

/*       if std-lo and rpt-behavior = "Q"                */
/*        then                                           */
/*         do:                                           */
/*           message "Agent Summary is queued."          */
/*             view-as alert-box information buttons ok. */
/*         end.                                          */
    end.

  /*server call for agent report pdf*/
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCurrentAuditType C-Win 
PROCEDURE GetCurrentAuditType :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define output parameter ctype as character no-undo.
 
 do with frame fmain:
 end.
 
 ctype = fAuditType:screen-value . 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE importAudit C-Win 
PROCEDURE importAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var tOk as logical.
 def var tFile as char.

 publish "IsActive" (output tOk).
 if tOk
  then
   do:
     MESSAGE "Please close the open Audit"
       VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return.
   end.

 publish "GetAuditDir" (output std-ch).
 system-dialog get-file tFile
    filters "QAR Files" "*.qarn"
    default-extension ".qarn"
    initial-dir std-ch
   update tOK.

 if not tOK
  then return.

 tFile = search(tFile).
 if tFile = ?
  then return.
   
 session:set-wait-state("general").
 publish "ImportAudit" (tFile).
 session:set-wait-state("").

 publish "IsActive" (output tOk).
 if not tOk /* Error opening audit */
  then return.
  
 publish "GetActiveFilename" (output tFile).
 session:set-wait-state("general").
 publish "OpenAudit" (tFile).
 session:set-wait-state("").

 showReview().
 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "AuditOpened".
 publish "ShowTitle" ( ENTRY( LOOKUP(tActiveFile:SCREEN-VALUE in frame fButtons,tActiveFile:LIST-ITEM-PAIRS in frame fButtons, "|") - 1, tActiveFile:LIST-ITEM-PAIRS in frame fButtons, "|")).
 publish "ActiveFileChanged" (activeAgentFileID).
 publish "setcurrentvalue"("currentOpenAudit", tQarID:screen-value).
 publish "setcurrentvalue"("currentAuditType", fAuditType:screen-value).
 publish "SetCurrentValue" ("AgentID", tAgentID:screen-value).
 publish "SetCurrentValue" ("StateID", tState:screen-value).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LeaveAll C-Win 
PROCEDURE LeaveAll :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
apply "leave" to tAgentID in frame {&frame-name}.
apply "leave" to tName in frame {&frame-name}.
apply "leave" to tAddr in frame {&frame-name}.
apply "leave" to tCity in frame {&frame-name}.
apply "leave" to tState in frame {&frame-name}.
apply "leave" to tZip in frame {&frame-name}.
apply "leave" to fAuditType in frame {&frame-name}.
apply "leave" to tNumEmployees in frame {&frame-name}.
apply "leave" to tNumUnderwriters in frame {&frame-name}.
apply "leave" to tLastRevenue in frame {&frame-name}.
apply "leave" to tNumOffices in frame {&frame-name}.
apply "leave" to tContactName in frame {&frame-name}.
apply "leave" to tContactPhone in frame {&frame-name}.
apply "leave" to tContactFax in frame {&frame-name}.
apply "leave" to tContactOther in frame {&frame-name}.
apply "leave" to tContactEmail in frame {&frame-name}.
apply "leave" to tContactPosition in frame {&frame-name}.
apply "leave" to tDeliveredTo in frame {&frame-name}.
apply "leave" to tAuditDate in frame {&frame-name}.
apply "leave" to tAuditor in frame {&frame-name}.
apply "leave" to tRanking in frame {&frame-name}.
apply "leave" to fOnsiteStDt in frame {&frame-name}.
apply "leave" to fOnsiteFnDt in frame {&frame-name}.
apply "leave" to tNotes in frame {&frame-name}.
apply "leave" to tParentName in frame {&frame-name}.
apply "leave" to tParentAddr in frame {&frame-name}.
apply "leave" to tParentCity in frame {&frame-name}.
apply "leave" to tParentState in frame {&frame-name}.
apply "leave" to tParentZip in frame {&frame-name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE localInspect C-Win 
PROCEDURE localInspect :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* -----not used ---------*/

/* def var tOk as logical.
 def var tFile as char.

 publish "IsActive" (output tOk).
 if tOk
  then
   do:
       MESSAGE "Please close the open Audit"
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
       return.
   end.

 run dialogopencompletedqar.w(output std-lo,
                              output tFile).
 if not std-lo /* Cancelled */
  then return.

 session:set-wait-state("general").
 publish "OpenAudit" (tFile).
 session:set-wait-state("").

 publish "IsActive" (output tOk).
 if not tOk /* Error opening audit */
  then return.

 showReview().
 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "AuditOpened".
 publish "ShowTitle" ( entry( lookup(tActiveFile:screen-value in frame fbuttons,tactivefile:list-item-pairs in frame fbuttons, "|") - 1, tactivefile:list-item-pairs in frame fButtons, "|")).
 publish "ActiveFileChanged" (activeAgentFileID).
 
 activeAuditID = integer(tQarID:screen-value) no-error.

 if tStat:screen-value = "Completed" then
 publish "AuditReadOnly".*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LockOff C-Win 
PROCEDURE LockOff :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign
    tLockOn = false
    frame fMain:sensitive = true
    frame fButtons:sensitive = true
    menu m_Review:sensitive = true
    menu m_Reports:sensitive = true
    menu-item m_Escrow_Reconciliation:sensitive in menu m_View = true
    menu-item m_References:sensitive in menu m_Tools = true
    .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LockOn C-Win 
PROCEDURE LockOn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if valid-handle(wEscrow)
   then run hideWindow in wEscrow.

  assign
    tLockOn = true
    frame fMain:sensitive = false
    frame fButtons:sensitive = false
    menu m_Review:sensitive = false
    menu m_Reports:sensitive = false
    menu-item m_Escrow_Reconciliation:sensitive in menu m_View = false
    menu-item m_References:sensitive in menu m_Tools = false
    .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyAudit C-Win 
PROCEDURE modifyAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var pCancelled as logical.

 def var pAgentID as char.
 def var pName as char.
 def var pAddr as char.
 def var pCity as char.
 def var pState as char.
 def var pZip as char.
 def var pContactName as char.
 def var pContactPhone as char.
 def var pContactFax as char.
 def var pContactOther as char.
 def var pContactEmail as char.
 def var pContactPosition as char.
 def var pParentName as char.
 def var pParentAddr as char.
 def var pParentCity as char.
 def var pParentState as char.
 def var pParentZip as char.
 def var pAuditDate as date.
 def var pAuditScore as int.
 def var pMainOffice as logical.
 def var pNumOffices as int.
 def var pNumEmployees as int.
 def var pNumUnderwriters as int.
 def var pLastRevenue as decimal.
 def var pAuditor as char.
 def var pRanking as int.
 def var pDeliveredTo as char.
 def var pServices as char no-undo.
  
 {lib/getattrch.i agentID pAgentID}
 {lib/getattrch.i name pName}
 {lib/getattrch.i addr pAddr}
 {lib/getattrch.i city pCity}
 {lib/getattrch.i state pState}
 {lib/getattrch.i zip pZip}
 {lib/getattrch.i contactName pContactName}
 {lib/getattrch.i contactPhone pContactPhone}
 {lib/getattrch.i contactFax pContactFax}
 {lib/getattrch.i contactOther pContactOther}
 {lib/getattrch.i contactEmail pContactEmail}
 {lib/getattrch.i contactPosition pContactPosition}
 {lib/getattrch.i parentName pParentName}
 {lib/getattrch.i parentAddr pParentAddr}
 {lib/getattrch.i parentCity pParentCity}
 {lib/getattrch.i parentState pParentState}
 {lib/getattrch.i parentZip pParentZip}
 {lib/getattrda.i auditDate pAuditDate}
 {lib/getattrlo.i mainOffice pMainOffice}
 {lib/getattrin.i numOffices pNumOffices}
 {lib/getattrin.i numEmployees pNumEmployees}
 {lib/getattrin.i numUnderwriters pNumUnderwriters}
 {lib/getattrde.i lastRevenue pLastRevenue}
 {lib/getattrin.i ranking pRanking}
 {lib/getattrch.i auditor pAuditor}
 {lib/getattrch.i deliveredTo pDeliveredTo}
 {lib/getattrch.i services pServices}

 run dialognew.w
    ("Modify Review",
     "AZ,CO,FL,KS,MO,SC,TX",
     input-output pAgentID,
     input-output pName,
     input-output pAddr,
     input-output pCity,
     input-output pState,
     input-output pZip,
     input-output pContactName,
     input-output pContactPhone,
     input-output pContactFax,
     input-output pContactOther,
     input-output pContactEmail,
     input-output pContactPosition,
     input-output pParentName,
     input-output pParentAddr,
     input-output pParentCity,
     input-output pParentState,
     input-output pParentZip,
     input-output pMainOffice,
     input-output pNumOffices,
     input-output pNumEmployees,
     input-output pNumUnderwriters,
     input-output pLastRevenue,
     input-output pAuditor,
     input-output pAuditDate,
     input-output pRanking,
     input-output pDeliveredTo,
     input-output pServices,
     output pCancelled).
 if pCancelled 
  then return.

 {lib/setattrch.i agentID pAgentID}
 {lib/setattrch.i name pName}
 {lib/setattrch.i addr pAddr}
 {lib/setattrch.i city pCity}
 {lib/setattrch.i state pState}
 {lib/setattrch.i zip pZip}
 {lib/setattrch.i contactName pContactName}
 {lib/setattrch.i contactPhone pContactPhone}
 {lib/setattrch.i contactFax pContactFax}
 {lib/setattrch.i contactOther pContactOther}
 {lib/setattrch.i contactEmail pContactEmail}
 {lib/setattrch.i contactPosition pContactPosition}
 {lib/setattrch.i parentName pParentName}
 {lib/setattrch.i parentAddr pParentAddr}
 {lib/setattrch.i parentCity pParentCity}
 {lib/setattrch.i parentState pParentState}
 {lib/setattrch.i parentZip pParentZip}
 {lib/setattrda.i auditDate pAuditDate}
 {lib/setattrlo.i mainOffice pMainOffice}
 {lib/setattrin.i numOffices pNumOffices}
 {lib/setattrin.i numEmployees pNumEmployees}
 {lib/setattrin.i numUnderwriters pNumUnderwriters}
 {lib/setattrde.i lastRevenue pLastRevenue}
 {lib/setattrin.i ranking pRanking}
 {lib/setattrch.i auditor pAuditor}
 {lib/setattrch.i deliveredTo pDeliveredTo}
 {lib/setattrch.i services pServices}

 run displayHeader in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newInformal C-Win 
PROCEDURE newInformal PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tActive as logical.
 def var pAuditor as char.

 publish "IsActive" (output tActive).
 if tActive
  then
   do:
     MESSAGE "Please close the open Audit"
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return.
   end.

 publish "NewAudit". 
   
 {lib/setattrlo.i mainOffice true}
 {lib/setattrin.i numOffices 1}
 {lib/setattrda.i auditDate today}.
 {lib/setattrch.i services 'Both'}

 publish "GetCredentialsName" (output pAuditor).
 {lib/setattrch.i auditor pAuditor}

 assign
   tMainOffice:checked in frame fMain = true
   tNumOffices:screen-value in frame fMain = "1"
   tAuditDate:screen-value in frame fMain = string(today)
   tServices:screen-value in frame fMain = "Both"
   tAuditor:screen-value in frame fMain = pAuditor
   tAuditStartDate:screen-value = string(today)
   .

 enable
   tAgentID
   tName
   tAddr
   tCity
   tState
   tZip
   tMainOffice
   tNumOffices
   tNumEmployees
   tNumUnderwriters
   tLastRevenue
   tParentName
   tParentAddr
   tParentCity
   tParentState
   tParentZip
   tContactName
   tContactPhone
   tContactFax
   tContactOther
   tContactEmail
   tContactPosition
   tDeliveredTo
   tAuditDate
   fOnsiteStDt
   fOnsiteFnDt
   tAuditor
   tRanking
   tServices
   tNotes
  with frame fMain.

 assign
   tAgentID:read-only in frame {&frame-name} = false
   tName:read-only in frame {&frame-name} = false
   tAddr:read-only in frame {&frame-name} = false
   tCity:read-only in frame {&frame-name} = false
   tState:read-only in frame {&frame-name} = false
   tZip:read-only in frame {&frame-name} = false
   .

 activeAgentFileID = 0.
 run displayHeader in this-procedure.

 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "AuditOpened".
 publish "setcurrentvalue"("currentOpenAudit", tQarID:screen-value).
 publish "SetCurrentValue" ("AgentID", tAgentID:screen-value).
 publish "SetCurrentValue" ("StateID", tState:screen-value).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openAudit C-Win 
PROCEDURE openAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var tOk as logical.
 def var tFile as char.

 publish "IsActive" (output tOk).
 if tOk
  then
   do:
     MESSAGE "Please close the open Audit"
       VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return.
   end.

 run dialogopenqar.w (output std-lo, output tFile).
 if not std-lo /* Cancelled */
  then return.

 session:set-wait-state("general").
 publish "OpenAudit" (tFile).
 session:set-wait-state("").

 activeAuditID = integer(tQarID:screen-value in frame fmain) no-error.
 publish "IsActive" (output tOk).
 if not tOk /* Error opening audit */
  then return.

 showReview().
 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "GetAgentFileList" (output std-ch).
 tActiveFile:list-item-pairs in frame fButtons = std-ch.
 activeAgentFileID = 0.
 if std-ch = "|0"
   then tActiveFile:sensitive = false.
   else 
    do:
      tActiveFile:sensitive = true.
      activeAgentFileID = 1.
    end.
 tActiveFile:screen-value = string(activeAgentFileID).
 
 publish "AuditOpened".
 publish "ShowTitle" ( entry( lookup(tactivefile:screen-value in frame fbuttons,tactivefile:list-item-pairs in frame fbuttons, "|") - 1, tactivefile:list-item-pairs in frame fbuttons, "|")).
 publish "ActiveFileChanged" (activeAgentFileID).
 publish "GetLocalQarFiles" (output table localQarFileInfo).
 find localQarFileInfo where localQarFileInfo.identifier = string(activeAuditID) no-error.
 if available (localQarFileInfo) then
 do: 
   status default "Last saved:" + "  " + string(localQarFileInfo.modifieddate) in window {&window-name}. 
   status input "Last saved:" + "  " + string(date(localQarFileInfo.modifieddate)) + "  " +  string(time,"hh:mm:ss AM") in window {&window-name}. 
 end.
 else
 do:
  status default "" in window {&window-name}.
  status input "" in window {&window-name}.
 end.

 if tStat:screen-value = "Completed" then
 publish "AuditReadOnly".
 publish "setcurrentvalue"("currentOpenAudit", tQarID:screen-value).
 publish "setcurrentvalue"("currentAuditType", fAuditType:screen-value).
 publish "SetCurrentValue" ("AgentID", tAgentID:screen-value).
 publish "SetCurrentValue" ("StateID", tState:screen-value).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openInformal C-Win 
PROCEDURE openInformal :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tOk as logical.
 def var tFile as char.
 define variable tInitDir as character no-undo.

 publish "IsActive" (output tOk).
 if tOk
  then
   do:
     MESSAGE "Please close the open Audit"
       VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return.
   end.
  
 tInitDir = os-getenv("userprofile") + "\Documents\".
 if search(tInitDir) = ?
  then os-create-dir value(tInitDir).

 system-dialog get-file tFile
   title "Select an Informal Audit"
   initial-dir tInitDir
   filters "QAR Version M" "*.qarn"
   must-exist
   update std-lo.

 if not std-lo /* Cancelled */
  then return.
  
 session:set-wait-state("general").
 publish "OpenAudit" (tFile).
 session:set-wait-state("").

 publish "IsActive" (output tOk).
 if not tOk /* Error opening audit */
  then return.
  
 publish "IsInformal" (output tOk).
 if not tOk /* not informal */
  then
   do:
    message "Please select an Informal Audit file" view-as alert-box information buttons ok.
    publish "CloseAudit".
    return.
   end.
  
 showReview().
 assign
   tAgentID:read-only in frame {&frame-name} = false
   tAgentLookup:hidden in frame {&frame-name} = true
   tName:read-only in frame {&frame-name} = false
   tAddr:read-only in frame {&frame-name} = false
   tCity:read-only in frame {&frame-name} = false
   tState:read-only in frame {&frame-name} = false
   tZip:read-only in frame {&frame-name} = false
   .

 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "GetAgentFileList" (output std-ch).
 tActiveFile:list-item-pairs in frame fButtons = std-ch.
 activeAgentFileID = 0.
 if std-ch = "|0"
   then tActiveFile:sensitive = false.
   else 
    do:
      tActiveFile:sensitive = true.
      activeAgentFileID = 1.
    end.
 tActiveFile:screen-value = string(activeAgentFileID).

 publish "AuditOpened".
 publish "ShowTitle" ( entry( lookup(tactivefile:screen-value in frame fbuttons,tactivefile:list-item-pairs in frame fbuttons, "|") - 1, tactivefile:list-item-pairs in frame fbuttons, "|")).
 publish "ActiveFileChanged" (activeAgentFileID).
 
 activeAuditID = integer(tQarID:screen-value) no-error.

 status default "" in window {&window-name}.
 status input "" in window {&window-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE recallAudit C-Win 
PROCEDURE recallAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*-----------not-used---------------*/
/* def var tOk as logical.
 def var tFile as char.

 publish "IsActive" (output tOk).
 if tOk
  then
   do:
     MESSAGE "Please close the open Audit"
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return.
   end.

 run dialogrecall.w (output std-lo).
 if not std-lo /* Cancel */
  then return.

 showReview().
 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "AuditOpened".
 publish "ActiveFileChanged" (activeAgentFileID).*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE restartAudit C-Win 
PROCEDURE restartAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var lChoice as logical.
 def var tFile as char.
 def var pAuditID as char.
 def var pAuditor as char.
 def var pSuccess as logical.
 def var pSelected as logical.
 def var pFullFile as char.
 def var tOk as logical.

 publish "IsActive" (output tOk).
 if tOk
  then
   do:
     MESSAGE "Please close the open Audit"
       VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return.
   end.

 run dialogrestartqar.w (output pSelected,
                         output pAuditID,
                         output pFullFile).
 if pSelected then
  do:
    publish "RestartAudit"(input pAuditId,
                           input pFullFile,
                           output pSuccess,
                           output lChoice) .
    if not pSuccess or not lChoice  then
      return.
 end.
 else return.
 {lib/setattrlo.i mainOffice true}
 {lib/setattrin.i numOffices 1}
 {lib/setattrda.i auditDate today}
 {lib/setattrch.i services 'Both'}

 publish "GetCredentialsName" (output pAuditor).
 {lib/setattrch.i auditor pAuditor}
 assign
   tMainOffice:checked in frame fMain = true
   tNumOffices:screen-value in frame fMain = "1"
   tAuditDate:screen-value in frame fMain = string(today)
   tServices:screen-value in frame fMain = "Both"
   tAuditor:screen-value in frame fMain = pAuditor
   .
 enable
   tAgentID
   tName
   tAddr
   tCity
   tState
   tZip
   tMainOffice
   tNumOffices
   tNumEmployees
   tNumUnderwriters
   tLastRevenue
   tParentName
   tParentAddr
   tParentCity
   tParentState
   tParentZip
   tContactName
   tContactPhone
   tContactFax
   tContactOther
   tContactEmail
   tContactPosition
   tDeliveredTo
   tAuditDate
   tAuditor
   tRanking
   tServices
   tNotes
   with frame fMain.
 activeAgentFileID = 0.
 run displayHeader in this-procedure.
 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "AuditOpened".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveAfterEveryAnswer C-Win 
PROCEDURE SaveAfterEveryAnswer :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable tAutoSave as logical no-undo.
 define variable tActive as logical no-undo .
 define variable tChanged as logical no-undo .


 /* Informal -- Don't do this */
 if fAuditType:screen-value in frame fMain = ""
  then return.
 
 publish "GetAutoSave" (output tAutoSave).
 if tAutoSave then
 do:
   publish "isActive" (output tActive).
   publish "isChanged" (output tChanged).
   if tActive = true and tChanged = true then
   do:
     activeAuditID = integer(tQarID:screen-value in frame fmain) .
     publish "SaveAudit" (input activeAuditID).
   end. 
 status default  "Last saved:"  + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss am") in window {&window-name}.
 status input  "Last saved:"  + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss am") in window {&window-name}.
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveAudit C-Win 
PROCEDURE saveAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var tOk as logical.

 /* Informal */
 if fAuditType:screen-value in frame fMain = ""
  then
   do: run saveInformal in this-procedure.
       showReview().
       return.
   end.

 apply 'value-changed' to tScorePct .
 
 publish "LeaveAll" .
 session:set-wait-state("general").
 activeAuditID = integer(tQarID:screen-value in frame fmain ).
 
 publish "SaveAudit" (input integer(activeAuditID)).
 session:set-wait-state("").
 status default  "Last saved:"  + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss AM") in window {&window-name}.
 status input  "Last saved:"  + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss AM") in window {&window-name}.
 assign
   menu-item m_Save:sensitive in menu m_Review = false
   bBtnSave:sensitive in frame fButtons = false
   .
 showReview().
 tclicksave = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveInformal C-Win 
PROCEDURE saveInformal :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tOk as logical.
 def var tFile as char no-undo.
 define variable tInitDir as character no-undo.

 publish "LeaveAll" .

 publish "GetActiveFilename" (output tFile).

 std-lo = false.
 file-info:file-name = tFile.
 if index(file-info:file-type, "F") > 0 
  then std-lo = true.
  
 tInitDir = os-getenv("userprofile") + "\Documents\".
 if search(tInitDir) = ?
  then os-create-dir value(tInitDir).

 if not std-lo
  then system-dialog get-file tFile
             save-as
             initial-dir tInitDir
             default-extension ".qarn"
             ask-overwrite
             filters "QAR Version N" "*.qarn"
             update std-lo.

 if not std-lo /* Cancelled */
  then return.

 session:set-wait-state("general").
 publish "SaveAuditInformal" (0, tFile).
 session:set-wait-state("").

 assign
   menu-item m_Informal_Save:sensitive in sub-menu m_Informal = false
   bBtnSave:sensitive in frame fButtons = false
   .
 tclicksave = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SelectAuditPage C-Win 
PROCEDURE SelectAuditPage :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pPage as int.

 publish "IsActive" (output std-lo).
 if not std-lo 
  then return.

 if pPage < 1 or pPage > 8 
  then return.
 if valid-handle(wSection[pPage]) then
   run ShowWindow IN wSection[pPage].
 else
   run ViewSection (pPage).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE serverInspect C-Win 
PROCEDURE serverInspect :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tOk as logical.
 def var tFile as char.

 /*publish "IsActive" (output tOk).
 if tOk
  then
   do:
     MESSAGE "Please close the open Audit"
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return.
   end.*/

 run dialogserverinspectqar.w(output  std-lo).
 if not std-lo /* Cancelled */
  then return.


 /*publish "IsActive" (output tOk).
 if not tOk /* Error opening audit */
  then return./

 showReview().
 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "AuditOpened".
 publish "ShowTitle" ( entry( lookup(tactivefile:screen-value in frame fbuttons,tactivefile:list-item-pairs in frame fbuttons, "|") - 1, tactivefile:list-item-pairs in frame fbuttons, "|")).
 publish "ActiveFileChanged" (activeAgentFileID).
 
 activeAuditID = integer(tQarID:screen-value) no-error.

 if tStat:screen-value = "Completed" then
  publish "AuditReadOnly".*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetClickSave C-Win 
PROCEDURE SetClickSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
tclicksave = false .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showCalendar C-Win 
PROCEDURE showCalendar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
if not valid-handle(wCalendar) 
 then run calendar.w persistent set wCalendar.
 else wCalendar:current-window:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showReferences C-Win 
PROCEDURE showReferences :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if valid-handle(wReferences)
  then run showWindow in wReferences.
  else run wreferences.w persistent set wReferences.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SpellChecker C-Win 
PROCEDURE SpellChecker :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 define variable pcomment  as char no-undo.
 define variable pscore    as char no-undo.
 define variable checktext as char no-undo.
 define variable checktextnew as char    no-undo.
 define variable iCount       as int     no-undo.
 define variable actstart     as integer no-undo.
 define variable acctstart    as integer no-undo.
 define variable bpstart      as integer no-undo.
 define variable totalentries as integer no-undo.
 define variable panswer      as char    no-undO.
 define variable currentAction as char   no-undo.
 define variable widgetlist as char initial "22000,21020,21030,21040,21060,21070,21105,20045,20110" no-undo.
 

 do with frame  fMain:
 end.

 publish "GetFindBpAction" (output table finding,
                            output table bestpractice,
                            output table action,
                            output table account ).

 assign tNotes.
 checktext = tNotes.
 
 do iCount = 1 to 8:
   publish "GetSectionAnswer" (input string(iCount),
                               output pscore,
                               output pcomment).
   checktext = checktext + "|" + pcomment.
 end.

 do iCount = 1 to 9:
   publish "GetBackgroundAnswer"(input integer(entry(iCount ,widgetlist, "," )),
                                 output panswer).
   checktext = checktext + "|" + panswer                                                                                                                                                                                                                                                               .
 end.
 
 publish "GetQuestionAnswer" (1000, output panswer).
 checktext = checktext + "|" + panswer.

 iCount = 20.
 for each finding  by finding.questionSeq : 
   checktext = checktext + "|" +  replace( finding.comments, "&br;" , chr(10)).
   checktext = checktext + "|" +  replace( finding.reference, "&br;" , chr(10)).
   iCount = iCount + 2. 
 end.

 bpstart = iCount .

 for each bestpractice  by bestpractice.questionSeq : 
   checktext = checktext + "|" +  replace( bestpractice.comments, "&br;" , chr(10)).
   iCount = iCount + 1 .
 end.

 actstart = iCount . 

 for each action by  action.questionSeq :
   checktext = checktext + "|" +  replace( action.description, "&br;" , chr(10)).
   checktext = checktext + "|" + replace( action.note, "&br;" , chr(10)).
   iCount = iCount + 2 .
 end.

 acctstart = iCount.

 for each account by  account.acctNumber :
   checktext = checktext + "|" +  replace( account.comments, "&br;" , chr(10)).
   iCount = iCount + 1.
 end.

 totalentries = iCount.

 run util/spellcheck.p ({&window-name}:handle, input-output checktext, output std-lo).


 publish "SetAuditAnswer" ("Comments",  trim(entry(1,checktext, chr(7)), chr(13))).
 tNotes =  trim(entry(1,checktext, chr(7)), chr(13)).
 display tNotes with frame fmain .

 do iCount = 1 to 8:
   publish "SetSectionAnswer" (input string(iCount),
                               input "Comments",
                               input trim(entry(iCount + 1 ,checktext, chr(7)),chr(13))).
 end.

 do iCount = 1 to 9:
   publish "SetBackgroundAnswer"(input integer(entry(iCount,widgetlist, "," )),
                                 input trim(entry(iCount + 9,checktext, chr(7)),chr(13))).
 end.


 publish "SetQuestionAnswer" (1000,
                              input trim(entry(19,checktext, chr(7)),chr(13))).

 iCount = 20.

 for each finding  by finding.questionSeq : 
   finding.comments = trim( replace( entry(iCount,checktext, chr(7)), chr(10), "&br;"),chr(13)).
   iCount = iCount + 1.
   finding.reference =  trim(replace( entry(iCount,checktext, chr(7)), chr(10), "&br;"),chr(13)).
   iCount = iCount + 1. 
 end.

 iCount = bpstart .

 for each bestpractice  by bestpractice.questionSeq : 
   bestpractice.comments = trim( replace( entry(iCount,checktext, chr(7)), chr(10), "&br;"),chr(13)).
   iCount = iCount + 1 .
 end.

 iCount = actstart . 

 for each action by  action.questionSeq :
   action.description = trim(replace( entry(iCount,checktext, chr(7)), chr(10), "&br;"),chr(13)).
   iCount = iCount + 1 .
   action.note =  trim(replace( entry(iCount,checktext, chr(7)), chr(10), "&br;"),chr(13)).
   iCount = iCount + 1 .
 end.

 iCount = acctstart.

 for each account by  account.acctNumber :
   account.comments = trim( replace( entry(iCount,checktext, chr(7)), chr(10), "&br;"),chr(13)).
   iCount = iCount + 1.
 end.

 publish "SetFindBpAction" (input table finding,
                            input table bestpractice,
                            input table action,
                            input table account ).
 publish "SetAuditChanged"(true).

 run EnableSave.
 publish "SaveAfterEveryAnswer".
 checktext = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startAudit C-Win 
PROCEDURE startAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tActive as logical.
 def var pAuditor as char.

 publish "IsActive" (output tActive).
 if tActive
  then
   do:
     MESSAGE "Please close the open Audit"
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return.
   end.
 run dialogbeginaudit.w (output std-lo). 

 if std-lo = false then
   return.

 {lib/setattrlo.i mainOffice true}
 {lib/setattrin.i numOffices 1}
 {lib/setattrda.i auditDate today}.
 {lib/setattrch.i services 'Both'}
 publish "GetBackgroundAnswer" (20005, output std-ch).

 publish "GetCredentialsName" (output pAuditor).
 {lib/setattrch.i auditor pAuditor}

 assign
   tMainOffice:checked in frame fMain = true
   tNumOffices:screen-value in frame fMain = "1"
   tAuditDate:screen-value in frame fMain = string(today)
   tServices:screen-value in frame fMain = "Both"
   tAuditor:screen-value in frame fMain = pAuditor
   tLastRevenue:screen-value in frame fMain = std-ch
   tLastRevenue:sensitive in frame fMain = false
   .

 enable
   tAgentID
   tName
   tAddr
   tCity
   tState
   tZip
   tMainOffice
   tNumOffices
   tNumEmployees
   tNumUnderwriters
   tLastRevenue
   tParentName
   tParentAddr
   tParentCity
   tParentState
   tParentZip
   tContactName
   tContactPhone
   tContactFax
   tContactOther
   tContactEmail
   tContactPosition
   tDeliveredTo
   tAuditDate
   fOnsiteStDt
   fOnsiteFnDt
   tAuditor
   tRanking
   tServices
   tNotes
  with frame fMain.

 activeAgentFileID = 0.
 run displayHeader in this-procedure.

 {lib/getattrda.i auditStartDate std-da}
 tAuditStartDate:screen-value = string(std-da).
 
 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "AuditOpened".
 publish "setcurrentvalue"("currentOpenAudit", tQarID:screen-value).
 publish "setcurrentvalue"("currentAuditType", fAuditType:screen-value).
 publish "SetCurrentValue" ("AgentID", tAgentID:screen-value).
 publish "SetCurrentValue" ("StateID", tState:screen-value).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StartNewAudit C-Win 
PROCEDURE StartNewAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tActive as logical.
 def var pAuditor as char.
 def var pReviewDate as date.

 publish "IsActive" (output tActive).
 if tActive
  then
   do:
     MESSAGE "Please close the open Audit"
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return.
   end.

  publish "NewAudit". 
  run dialognewaudit.w (output std-lo,output pReviewdate,output pAuditor). 

 if std-lo = false then
   return.
   

 {lib/setattrlo.i mainOffice true}
 {lib/setattrin.i numOffices 1}
 {lib/setattrda.i auditDate today}.
 {lib/setattrch.i services 'Both'}


 assign
   tMainOffice:checked in frame fMain = true
   tNumOffices:screen-value in frame fMain = "1"
   tAuditDate:screen-value in frame fMain  = string(pReviewdate)
   tServices:screen-value in frame fMain = "Both"
   tAuditor:screen-value in frame fMain = pAuditor
   tAuditStartDate:screen-value = string(today)
   .

 enable
   tAgentID
   tName
   tAddr
   tCity
   tState
   tZip
   tMainOffice
   tNumOffices
   tNumEmployees
   tNumUnderwriters
   tLastRevenue
   tParentName
   tParentAddr
   tParentCity
   tParentState
   tParentZip
   tContactName
   tContactPhone
   tContactFax
   tContactOther
   tContactEmail
   tContactPosition
   tDeliveredTo
   tAuditDate
   fOnsiteStDt
   fOnsiteFnDt
   tAuditor
   tRanking
   tServices
   tNotes
  with frame fMain.

 activeAgentFileID = 0.
 run displayHeader in this-procedure.

 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "AuditOpened".
 publish "setcurrentvalue"("currentOpenAudit", tQarID:screen-value).
 publish "setcurrentvalue"("currentAuditType", fAuditType:screen-value).
 publish "SetCurrentValue" ("AgentID", tAgentID:screen-value).
 publish "SetCurrentValue" ("StateID", tState:screen-value).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewFinding C-Win 
PROCEDURE ViewFinding :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def input parameter pQuestionSeq as int.
 def input parameter pParentWindow as char.
 def output parameter pHasFinding as logical.

 def var pQuestionID as char.
 def var pDesc as char.
 def var pPriority as int.
 def var pCanChangePriority as logical.
 def var hValue as int.

 publish "GetQuestionAttributes" (input pQuestionSeq,
                                  output pQuestionID, 
                                  output pDesc,
                                  output pPriority,
                                  output pCanChangePriority,
                                  output std-ch,
                                  output std-ch,
                                  output std-ch).

 publish "GetSectionAttribute" (input substring(pQuestionID, 1, 1), 
                                input "UsesFiles",
                                output std-ch).
 if pDesc <> ""
 then 
   do:
     hValue = integer((pQuestionSeq / 5) - 199 ).
     if  not valid-handle(hFindbp[hValue]) then
     run wfindbp.w persistent set hFindbp[hValue]
                          (pQuestionSeq,
                           pQuestionID,
                           pDesc,
                           pPriority,
                           std-ch = "Y",
                           pCanChangePriority,
                           focus,
                           substring(pQuestionID, 1, 1),
                           pParentWindow).
     else 
       publish "SetFocusFindBp" (input pQuestionSeq).
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewSection C-Win 
PROCEDURE ViewSection :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pSection as int.
 def var pAuditStatus as char no-undo.
 
 if not tclicksave then
   publish "LeaveAll" .
 if valid-handle(wSection[pSection]) 
  then
   do: run MoveToTop in wSection[pSection] no-error.
       return.
   end.

 case pSection:
  when 1 
   then
    do: run wsection1.w persistent set wSection[1].
        subscribe procedure wSection[1] to "AuditOpened" in this-procedure.
        subscribe procedure wSection[1] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[1].
    end.
  when 2 
   then
    do: run wsection2.w persistent set wSection[2].
        subscribe procedure wSection[2] to "AuditOpened" in this-procedure.
        subscribe procedure wSection[2] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[2].
    end.
  when 3 
   then
    do: run wsection3.w persistent set wSection[3].
        subscribe procedure wSection[3] to "ActiveFileChanged" in this-procedure.
        subscribe procedure wSection[3] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[3].
    end.
  when 4 
   then
    do: run wsection4.w persistent set wSection[4].
        subscribe procedure wSection[4] to "ActiveFileChanged" in this-procedure.
        subscribe procedure wSection[4] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[4].
    end.
  when 5 
   then
    do: run wsection5.w persistent set wSection[5].
        subscribe procedure wSection[5] to "ActiveFileChanged" in this-procedure.
        if valid-handle(wSection[7]) 
         then subscribe procedure wSection[5] to "ActiveFileChanged" in wSection[7].
        subscribe procedure wSection[5] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[5].
    end.
  when 6 
   then
    do: run wsection6.w persistent set wSection[6].
        subscribe procedure wSection[6] to "AuditOpened" in this-procedure.
        subscribe procedure wSection[6] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[6].
    end.
  when 7 
   then
    do: run wsection7.w persistent set wSection[7].
        subscribe procedure wSection[7] to "ActiveFileChanged" in this-procedure.
        if valid-handle(wSection[5]) 
         then subscribe procedure wSection[7] to "ActiveFileChanged" in wSection[5].
        subscribe procedure wSection[7] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[7].
    end.
  when 8 
   then
    do: run wsection8.w persistent set wSection[8].
        subscribe procedure wSection[8] to "AuditOpened" in this-procedure.
        subscribe procedure wSection[8] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[8].
    end.
 end case.
 do WITH FRAME fButtons:
 end.
 publish "IsActive" (output std-lo).
 if std-lo 
  then run AuditOpened in wSection[pSection] no-error.
 publish "ActiveFileChanged" (activeAgentFileID).
 publish "ShowTitle" ( ENTRY( LOOKUP(tActiveFile:SCREEN-VALUE,tActiveFile:LIST-ITEM-PAIRS, "|") - 1, tActiveFile:LIST-ITEM-PAIRS, "|")).
 publish "GetAuditStatus" (output pAuditStatus).
 
 if pAuditStatus = "C" then
  run AuditReadOnly.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE WindowClosed C-Win 
PROCEDURE WindowClosed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter pWinName as character no-undo.

case pWinName: 
  when "Escrow"     then if valid-handle(wEscrow) then     delete procedure wEscrow.
  when "Answered"   then if valid-handle(wAnswered)then    delete procedure wAnswered.
  when "Scoring"    then if valid-handle(wScoring)then     delete procedure wScoring.
  when "Findings"   then if valid-handle(wFindings)then    delete procedure wFindings.
  when "References" then if valid-handle(wReferences)then  delete procedure wReferences.
  when "DOCUMENTS"  then if valid-handle(hDocWindow)then   delete procedure hDocWindow.
  when "audit"      then
  do:
    if valid-handle(wEscrow) then     delete procedure wEscrow.     
    if valid-handle(wAnswered)then    delete procedure wAnswered.   
    if valid-handle(wScoring)then     delete procedure wScoring.    
    if valid-handle(wFindings)then    delete procedure wFindings.   
    if valid-handle(wReferences)then  delete procedure wReferences. 
    if valid-handle(hDocWindow)then   delete procedure hDocWindow.  
  end.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getGrade C-Win 
FUNCTION getGrade RETURNS INTEGER
  ( input ipoints as integer /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable iGrade as integer no-undo.
  define variable iTotalPoints as integer no-undo.
  
  do with frame fmain:
  end.
  
  if fAuditType:screen-value = "QAR"
   then
    iTotalPoints = 120.
  else if fAuditType:screen-value = "ERR"
   then
    iTotalPoints = 30.
  else if fAuditType:screen-value = "TOR"
   then
    iTotalPoints = 90.
    
  igrade = integer((ipoints / iTotalPoints) * 100).

  return igrade.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getstatus C-Win 
FUNCTION getstatus RETURNS CHARACTER
  (input pstat as char /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if pstat = "E" then
  RETURN "ERR".   /* Function return value. */
  else if pstat = "Q" then
  RETURN "QAR".   /* Function return value. */
  else if pstat = "U" then
  RETURN "Underwriter".   /* Function return value. */
  else if pstat = "T" then
  RETURN "TOR".   /* Function return value. */
  else  
  return "".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION hideReview C-Win 
FUNCTION hideReview RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  clear frame fMain no-pause.
  clear frame fButtons no-pause.
  disable tAuditScore   with frame fButtons.
 
  clear frame {&frame-name}.
  pause 0.

  disable
    tAgentID
    tAgentLookup
    tName
    tAddr
    tCity
    tState
    tZip
    tMainOffice
    tNumOffices
    tNumEmployees
    tNumUnderwriters
    tLastRevenue
    tParentName
    tParentAddr
    tParentCity
    tParentState
    tParentZip
    tContactName
    tContactPhone
    tContactFax
    tContactOther
    tContactEmail
    tContactPosition
    tDeliveredTo
    tAuditDate
    tAuditor
    tRanking
    tServices
    fOnsiteStDt
    fOnsiteFnDt
    tNotes
    tScorePct
   with frame {&frame-name}.
  tDeliveredTo:screen-value = "".
  tNotes:screen-value = "" .
  tActiveFile:list-item-pairs in frame fButtons = "|0".
  tActiveFile:sensitive in frame fButtons = false.
  tAgentLookup:hidden in frame {&frame-name} = true.
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openWindowForAgent C-Win 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
@note The second parameter is the handle to the agentdatasrv.p
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo.
  define variable hFileDataSrv as handle no-undo.
  define variable cAgentWindow as character no-undo.
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  assign
    hWindow = ?
    hFileDataSrv = ?
    cAgentWindow = pAgentID + " " + pType
    .

  if pAgentID <> "" then
    publish "OpenAgent" (pAgentID, output hFileDataSrv).

  for first openwindow no-lock
      where openwindow.procFile = cAgentWindow:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow) then
  do:
    run value(pFile) persistent set hWindow (hFileDataSrv).

    create openwindow.
    assign openwindow.procFile   = cAgentWindow
           openwindow.procHandle = hWindow.
  end.

  run ShowWindow in hWindow no-error.
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveCurrentField C-Win 
FUNCTION saveCurrentField RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 if valid-handle(focus)   /* maybe not if the window was just closed */
   and (focus:type = "fill-in" or focus:type = "editor") /* all other types ok */
   and focus:modified     /* inconsistently set... */
  then apply "leave" to focus.

 RETURN true. 
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setActionWidgetState C-Win 
FUNCTION setActionWidgetState RETURNS LOGICAL PRIVATE
  ( pIsOpen as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  Set the sensitive state of the menus/buttons that allow for a
            review to be opened.

    Notes:  pIsOpen = true when a review file is active
------------------------------------------------------------------------------*/
 /* Review */
 assign   
   menu-item m_Start:sensitive in menu m_Review = not pIsOpen
   menu-item m_Restart:sensitive in menu m_Review = not pIsOpen
   menu-item m_Open:sensitive in menu m_Review = not pIsOpen
   menu-item m_Close:sensitive in menu m_Review = pIsOpen
   menu-item m_Save:sensitive in menu m_Review = pIsOpen
   menu-item m_Finish:sensitive in menu m_Review = pIsOpen
   menu-item m_Set_Audit_Type_to_ERR:sensitive in menu m_Review = false
   menu-item m_Set_Audit_Type_to_QAR:sensitive in menu m_Review = false
   menu-item m_Set_Audit_Type_to_TOR:sensitive in menu m_Review = false

   bBtnStart:sensitive in frame fButtons = not pIsOpen
   bBtnOpen:sensitive = not pIsOpen
   bBtnClose:sensitive = pIsOpen
   bBtnSave:sensitive = pIsOpen
   bBtnSubmit:sensitive = pIsOpen
   bBtnSpellCheck:sensitive = pIsOpen
   .


  /* View */
 assign   
   menu-item m_Documents:sensitive in menu m_View = pIsOpen
   menu-item m_Agent_Files:sensitive in menu m_View = pIsOpen
   menu-item m_Escrow_Reconciliation:sensitive in menu m_View = pIsOpen
   menu-item m_background_operations:sensitive in menu m_View = pIsOpen
   menu-item m_background_agent:sensitive in menu m_View = pIsOpen
   menu-item m_background_auditor:sensitive in menu m_View = pIsOpen
   bBtnFiles:sensitive = pIsOpen
   .
 if fAuditType:screen-value in frame fMain = "TOR" then
  menu-item m_Escrow_Reconciliation:sensitive in menu m_View = false.

 /* Section */
 assign
   menu-item m_1:sensitive in menu m_section = pIsOpen
   menu-item m_2:sensitive in menu m_section = pIsOpen
   menu-item m_3:sensitive in menu m_section = pIsOpen
   menu-item m_4:sensitive in menu m_section = pIsOpen
   menu-item m_5:sensitive in menu m_section = pIsOpen
   menu-item m_6:sensitive in menu m_section = pIsOpen
   menu-item m_7:sensitive in menu m_section = pIsOpen
   
   bSection-1:sensitive in frame fMain = pIsOpen
   bSection-2:sensitive in frame fMain = pIsOpen
   bSection-3:sensitive in frame fMain = pIsOpen
   bSection-4:sensitive in frame fMain = pIsOpen
   bSection-5:sensitive in frame fMain = pIsOpen
   bSection-6:sensitive in frame fMain = pIsOpen
   bSection-7:sensitive in frame fMain = pIsOpen
   .

 {lib/getattrch.i errtype std-ch}
  

 if std-ch = "I" then  
   assign menu-item m_Set_Audit_Type_to_ERR:sensitive in menu m_Review = pIsOpen
          menu-item m_Set_Audit_Type_to_QAR:sensitive in menu m_Review = pIsOpen.

 if fAuditType:screen-value = "ERR" then
   assign
      menu-item m_3:sensitive in menu m_section = false
      menu-item m_4:sensitive in menu m_section = false
      menu-item m_5:sensitive in menu m_section = false
      menu-item m_7:sensitive in menu m_section = false
     
      bSection-3:sensitive in frame fMain       = false
      bSection-4:sensitive in frame fMain       = false
      bSection-5:sensitive in frame fMain       = false 
      bSection-7:sensitive in frame fMain       = false
      bBtnFiles:sensitive in frame fbuttons     = false
      menu-item m_Set_Audit_Type_to_QAR:sensitive in menu m_Review = true
      menu-item m_Set_Audit_Type_to_TOR:sensitive in menu m_Review = true
      .
 else if fAuditType:screen-value = "QAR" 
  then
   assign 
       menu-item m_Set_Audit_Type_to_ERR:sensitive in menu m_Review = true
       menu-item m_Set_Audit_Type_to_TOR:sensitive in menu m_Review = true.

 else if fAuditType:screen-value = "TOR" 
  then
   assign
      menu-item m_6:sensitive in menu m_section = false
      bSection-6:sensitive in frame fMain       = false
      menu-item m_Set_Audit_Type_to_ERR:sensitive in menu m_Review = true
      menu-item m_Set_Audit_Type_to_QAR:sensitive in menu m_Review = true
      .
 /* Progress */
 assign   
   menu-item m_Unanswered_questions:sensitive in menu m_Progress = pIsOpen
   menu-item m_Answered_questions:sensitive in menu m_Progress = pIsOpen
   menu-item m_Findings_and_Best_Practices:sensitive in menu m_Progress = pIsOpen
   menu-item m_Scoring_and_Properties:sensitive in menu m_Progress = pIsOpen
   .

 /* Tools */
 menu-item m_Copy_Active_Review:sensitive in menu m_Tools = pIsOpen.

 /* Reports */
 assign
   menu-item m_Preliminary_Report:sensitive in menu m_Reports = pIsOpen
   menu-item m_Questionnaire:sensitive in menu m_Reports = pIsOpen
   menu-item m_Internal_Review:sensitive in menu m_Reports = pIsOpen
   menu-item m_Claims_for_Agent:sensitive in menu m_Reports = pIsOpen
   menu-item m_Agent_Summary_Report:sensitive in menu m_Reports = pIsOpen
   
   bBtnPrelimRpt:sensitive = pIsOpen
   .


 /* Actions */
 assign
   menu-item m_Cancel:sensitive in menu m_Actions = pIsOpen
   menu-item m_Import:sensitive in menu m_Actions = not pIsOpen
   menu-item m_Export:sensitive in menu m_Actions = pIsOpen
   menu-item m_Inspect:sensitive in menu m_Actions = not pIsOpen
   menu-item m_Inspect:sensitive in menu m_Actions = true
   .

     /* Informal Audit */
     if fAuditType:screen-value in frame fMain = "" and tQarID:screen-value in frame fMain = "0"
      then 
       do: assign
             sub-menu m_Review:sensitive = not pIsOpen
             menu-item m_Cancel:sensitive in menu m_Actions = not pIsOpen
             menu-item m_Import:sensitive in menu m_Actions = not pIsOpen
             menu-item m_Export:sensitive in menu m_Actions = pIsOpen
             menu-item m_Informal_New:sensitive in sub-menu m_Informal = not pIsOpen
             menu-item m_Informal_Open:sensitive in sub-menu m_Informal = not pIsOpen
             menu-item m_Informal_Close:sensitive in sub-menu m_Informal = pIsOpen
             menu-item m_Informal_Save:sensitive in sub-menu m_Informal = pIsOpen
             bBtnSubmit:sensitive = not pIsOpen
             bBtnPrelimRpt:sensitive = not pIsOpen
             .
       end.
      else assign
             menu-item m_Informal_Close:sensitive in sub-menu m_Informal = not pIsOpen
             menu-item m_Informal_Save:sensitive in sub-menu m_Informal = not pIsOpen
             .

   /*menu-item m_Start:sensitive in menu m_Review = false.
   menu-item m_Restart:sensitive in menu m_Review = false.*/
 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION showReview C-Win 
FUNCTION showReview RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable lIsInformal as logical no-undo.
  do with frame {&frame-name}:
    {lib/getattrch.i agentID std-ch}
    tAgentID:screen-value = std-ch.
    {lib/getattrch.i name std-ch}
    tName:screen-value = std-ch.
    {lib/getattrch.i addr std-ch}
    tAddr:screen-value = std-ch.
    {lib/getattrch.i city std-ch}
    tCity:screen-value = std-ch.
    {lib/getattrch.i state std-ch}
    tState:screen-value = std-ch.
    {lib/getattrch.i zip std-ch}
    tZip:screen-value = std-ch.

    {lib/getattrch.i contactName std-ch}
    tContactName:screen-value = std-ch.
    {lib/getattrch.i contactPhone std-ch}
    tContactPhone:screen-value = std-ch.
    {lib/getattrch.i contactFax std-ch}
    tContactFax:screen-value = std-ch.
    {lib/getattrch.i contactOther std-ch}
    tContactOther:screen-value = std-ch.
    {lib/getattrch.i contactEmail std-ch}
    tContactEmail:screen-value = std-ch.
    {lib/getattrch.i contactPosition std-ch}
    tContactPosition:screen-value = std-ch.

    {lib/getattrch.i parentName std-ch}
    tParentName:screen-value = std-ch.
    {lib/getattrch.i parentAddr std-ch}
    tParentAddr:screen-value = std-ch.
    {lib/getattrch.i parentCity std-ch}
    tParentCity:screen-value = std-ch.
    {lib/getattrch.i parentState std-ch}
    tParentState:screen-value = std-ch.
    {lib/getattrch.i parentZip std-ch}
    tParentZip:screen-value = std-ch.

    {lib/getattrch.i deliveredTo std-ch}
    tDeliveredTo:screen-value = std-ch.
    {lib/getattrda.i auditDate std-da}
    tAuditDate:screen-value = string(std-da).
    {lib/getattrch.i auditor std-ch}
    tAuditor:screen-value = std-ch.
    {lib/getattrin.i ranking std-in}
    tRanking:screen-value = string(std-in).
    {lib/getattrda.i onsiteStDate std-da}
    fOnsiteStDt:screen-value = string(std-da).
    {lib/getattrda.i onsiteFnDate std-da}
    fOnsiteFnDt:screen-value = string(std-da).
    {lib/getattrda.i auditStartDate std-da}
    tAuditStartDate:screen-value = string(std-da).
    {lib/getattrin.i numOffices std-in}
    tNumOffices:screen-value = string(std-in).
    {lib/getattrin.i numEmployees std-in}
    tNumEmployees:screen-value = string(std-in).
    {lib/getattrin.i numUnderwriters std-in}
    tNumUnderwriters:screen-value = string(std-in).

    publish "GetBackgroundAnswer" (20005, output std-ch).
    tLastRevenue:screen-value = std-ch.

    {lib/getattrlo.i mainOffice std-lo}
    tMainOffice:checked = std-lo.

    {lib/getattrch.i services std-ch}
    tServices:screen-value = std-ch.

    publish "GetAuditAnswer" ("Comments", output tNotes).
    tNotes:screen-value = tNotes.

    enable
      tAgentID
      tAgentLookup
      tName
      tAddr
      tCity
      tState
      tZip
      tMainOffice
      tNumOffices
      tNumEmployees
      tNumUnderwriters
      tParentName
      tParentAddr
      tParentCity
      tParentState
      tParentZip
      tContactName
      tContactPhone
      tContactFax
      tContactOther
      tContactEmail
      tContactPosition
      tDeliveredTo
      tAuditDate
      tAuditor
      tRanking
      tServices
      fOnsiteStDt
      fOnsiteFnDt
      tNotes
    with frame {&frame-name}.
  
  publish "IsInformal" (output lIsInformal).
  if lIsInformal
   then tAgentLookup:hidden = true.
  end.

  run displayHeader in this-procedure.

  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

