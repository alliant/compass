&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
    File        :   wbkgauditor.w
    Modification:
    Date          Name      Description
    01/12/2017    AG        Fixed resizing of widgets and data persist
    02/14/2017    AC        Implement local inspect functionality.
    09/11/2017    AG        Alternate color code in browse.
 ----------------------------------------------------------------------*/
CREATE WIDGET-POOL.

{lib/std-def.i}
{lib/winshowscrollbars.i}

def var tOk as logical.
def var iBgcolor as int no-undo.
{lib/questiondef.i &seq=21000}
{lib/questiondef.i &seq=21005}
{lib/questiondef.i &seq=21010}
{lib/questiondef.i &seq=21015}
{lib/questiondef.i &seq=21025}
{lib/questiondef.i &seq=21035}
{lib/questiondef.i &seq=21045}
{lib/questiondef.i &seq=21055}
{lib/questiondef.i &seq=21065}

{lib/questiondef.i &seq=21075}
{lib/questiondef.i &seq=21080}
{lib/questiondef.i &seq=21085}
{lib/questiondef.i &seq=21090}
{lib/questiondef.i &seq=21095}
{lib/questiondef.i &seq=21100}
{lib/questiondef.i &seq=21105}
{lib/questiondef.i &seq=21110}
{lib/questiondef.i &seq=21115}
{lib/questiondef.i &seq=21120}

{lib/questiondef.i &seq=21125}
{lib/questiondef.i &seq=21130}

{tt/qarbkguw.i} /* underwriter */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwUnderwriters

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES underwriter

/* Definitions for BROWSE brwUnderwriters                               */
&Scoped-define FIELDS-IN-QUERY-brwUnderwriters underwriter.uwPct underwriter.uwName /*   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwUnderwriters */ /*  underwriter.uwName */ /*  underwriter.uwPct */   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwUnderwriters underwriter
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwUnderwriters underwriter
&Scoped-define SELF-NAME brwUnderwriters
&Scoped-define QUERY-STRING-brwUnderwriters FOR EACH underwriter by underwriter.uwName
&Scoped-define OPEN-QUERY-brwUnderwriters OPEN QUERY {&SELF-NAME} FOR EACH underwriter by underwriter.uwName.
&Scoped-define TABLES-IN-QUERY-brwUnderwriters underwriter
&Scoped-define FIRST-TABLE-IN-QUERY-brwUnderwriters underwriter


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwUnderwriters}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwUnderwriters 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshBrowse C-Win 
FUNCTION refreshBrowse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAdd 
     LABEL "Add." 
     SIZE 7.2 BY 1.71 TOOLTIP "Create a new underwriter".

DEFINE BUTTON bDelete 
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete the selected underwriter".

DEFINE VARIABLE qLevel AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 80 BY 1.91
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qPosition AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 79 BY 1.91
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qRates AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 30 BY 1.91
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qTemps AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 79 BY 1.91
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qTermination AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 79 BY 1.91
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qTurnoverExplain AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 79 BY 1.91
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qABA AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 79 BY 1 NO-UNDO.

DEFINE VARIABLE qClosings AS INTEGER FORMAT ">>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1 TOOLTIP "Average closings per month"
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qOrders AS INTEGER FORMAT ">>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1 TOOLTIP "Average Orders per month"
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qSearcher AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qTurnover AS INTEGER FORMAT ">>9%":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1
     BGCOLOR 15  NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwUnderwriters FOR 
      underwriter SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwUnderwriters
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwUnderwriters C-Win _FREEFORM
  QUERY brwUnderwriters DISPLAY
      underwriter.uwPct label "Percent OfBusiness" format "zzz%"
 underwriter.uwName label "Underwriter" format "x(80)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 148 BY 3.57 ROW-HEIGHT-CHARS .71 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bAdd AT ROW 1.19 COL 3 WIDGET-ID 46
     brwUnderwriters AT ROW 1.24 COL 11 WIDGET-ID 300
     bDelete AT ROW 2.91 COL 3 WIDGET-ID 44
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 158 BY 31.38 WIDGET-ID 100.

DEFINE FRAME fSection
     qLevel AT ROW 11.24 COL 32 NO-LABEL WIDGET-ID 338
     qPosition AT ROW 16.24 COL 32 NO-LABEL WIDGET-ID 280
     qTermination AT ROW 21.24 COL 32 NO-LABEL WIDGET-ID 282
     qABA AT ROW 26.71 COL 30 COLON-ALIGNED NO-LABEL WIDGET-ID 340
     qTurnover AT ROW 29.1 COL 117 COLON-ALIGNED NO-LABEL WIDGET-ID 342
     qTurnoverExplain AT ROW 31.24 COL 32 NO-LABEL WIDGET-ID 288
     qTemps AT ROW 36.24 COL 32 NO-LABEL WIDGET-ID 290
     qOrders AT ROW 39.1 COL 117 COLON-ALIGNED NO-LABEL WIDGET-ID 292
     qClosings AT ROW 41.95 COL 117 COLON-ALIGNED NO-LABEL WIDGET-ID 294
     qRates AT ROW 53.86 COL 121 NO-LABEL WIDGET-ID 296
     qSearcher AT ROW 62.1 COL 119 COLON-ALIGNED NO-LABEL WIDGET-ID 298
     "1.18" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 62.1 COL 2 WIDGET-ID 322
          FONT 16
     "Explanation:" VIEW-AS TEXT
          SIZE 15 BY .62 AT ROW 31.24 COL 11 WIDGET-ID 310
          FONT 6
     "If you are using another company for title searches," VIEW-AS TEXT
          SIZE 69 BY 1 AT ROW 62.1 COL 13.8 WIDGET-ID 324
          FONT 16
     "List new ABAs:" VIEW-AS TEXT
          SIZE 18 BY .62 AT ROW 26.95 COL 11 WIDGET-ID 316
          FONT 6
     "Reason:" VIEW-AS TEXT
          SIZE 10 BY .62 AT ROW 21.24 COL 11 WIDGET-ID 306
          FONT 6
     "Apparent Reason:" VIEW-AS TEXT
          SIZE 20 BY .62 AT ROW 11.48 COL 11 WIDGET-ID 302
          FONT 6
     "Explanation:" VIEW-AS TEXT
          SIZE 14 BY .62 AT ROW 16.24 COL 11 WIDGET-ID 304
          FONT 6
     "In what capacity?" VIEW-AS TEXT
          SIZE 21 BY .62 AT ROW 36.24 COL 11 WIDGET-ID 312
          FONT 6
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 5.05
         SCROLLABLE SIZE 158 BY 69.91 WIDGET-ID 200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Background - Auditor"
         HEIGHT             = 31.38
         WIDTH              = 158
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 273.2
         SHOW-IN-TASKBAR    = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fSection:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */

DEFINE VARIABLE XXTABVALXX AS LOGICAL NO-UNDO.

ASSIGN XXTABVALXX = FRAME fSection:MOVE-AFTER-TAB-ITEM (bDelete:HANDLE IN FRAME fMain)
/* END-ASSIGN-TABS */.

/* BROWSE-TAB brwUnderwriters bAdd fMain */
/* SETTINGS FOR BUTTON bAdd IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDelete IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwUnderwriters:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwUnderwriters:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

/* SETTINGS FOR FRAME fSection
                                                                        */
ASSIGN 
       FRAME fSection:HEIGHT           = 22.62
       FRAME fSection:WIDTH            = 158.

/* SETTINGS FOR FILL-IN qABA IN FRAME fSection
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN qClosings IN FRAME fSection
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR qLevel IN FRAME fSection
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN qOrders IN FRAME fSection
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR qPosition IN FRAME fSection
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR qRates IN FRAME fSection
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN qSearcher IN FRAME fSection
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR qTemps IN FRAME fSection
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR qTermination IN FRAME fSection
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN qTurnover IN FRAME fSection
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR qTurnoverExplain IN FRAME fSection
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwUnderwriters
/* Query rebuild information for BROWSE brwUnderwriters
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH underwriter by underwriter.uwName.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwUnderwriters */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Background - Auditor */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Background - Auditor */
DO:
  if valid-handle(focus) /* maybe not if the window was just closed */
    and (focus:type = "fill-in" or focus:type = "editor") /* all other types ok */
    and focus:modified /* inconsistently set... */
  then apply "leave" to focus.

    /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Background - Auditor */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAdd C-Win
ON CHOOSE OF bAdd IN FRAME fMain /* Add. */
DO:
  run dialoguw.w (output std-lo).
  if std-lo 
    then refreshBrowse().

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME fMain /* Delete */
DO:
  if not available underwriter 
   then
    do: bDelete:sensitive = false.
        return.
    end.

  tOk = false.
  MESSAGE "Underwriter " underwriter.uwName " will be removed."
    VIEW-AS ALERT-BOX warning BUTTONS ok-cancel update tOk.
   if tOk
    then 
     do: 
       publish "DeleteUnderwriter" (underwriter.uwName).
       DELETE underwriter.
       brwUnderwriters:DELETE-SELECTED-ROWS().
       refreshBrowse().
     end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwUnderwriters
&Scoped-define SELF-NAME brwUnderwriters
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwUnderwriters C-Win
ON ROW-DISPLAY OF brwUnderwriters IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
  if current-result-row("brwUnderwriters") modulo 2 = 0 then
    iBgColor = 17.
  else
    iBgColor = 15.
  underwriter.uwPct:bgcolor in browse brwUnderwriters   = iBgColor.
  underwriter.uwName :bgcolor in browse brwUnderwriters = iBgColor.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwUnderwriters C-Win
ON ROW-LEAVE OF brwUnderwriters IN FRAME fMain
DO:
/*  if not frame fMain:sensitive then
    return.
  if self:new-row in frame fMain
   then
    do: create underwriter.
        assign input browse brwUnderwriters underwriter.uwName underwriter.uwPct.
        self:CREATE-RESULT-LIST-ENTRY().
        
    end.
   else
  if self:current-row-modified 
   then
    do: get current brwUnderwriters.
        assign input browse brwUnderwriters  underwriter.uwName underwriter.uwPct.
    end.

  publish "AddUnderwriter" (input underwriter.uwName, input underwriter.uwPct).
  
  publish "SaveAfterEveryAnswer".
  
  bDelete:sensitive in frame {&frame-name} = available underwriter. */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwUnderwriters C-Win
ON START-SEARCH OF brwUnderwriters IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fSection
&Scoped-define SELF-NAME qABA
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qABA C-Win
ON LEAVE OF qABA IN FRAME fSection
DO:
  if self:modified 
   then run doFillin (21050, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qClosings
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qClosings C-Win
ON LEAVE OF qClosings IN FRAME fSection
DO:
  if self:modified 
   then run doFillin (21080, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qLevel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qLevel C-Win
ON LEAVE OF qLevel IN FRAME fSection
DO:
  if self:modified
   then run doFillin (21020, self:screen-value).
  publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qOrders
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qOrders C-Win
ON LEAVE OF qOrders IN FRAME fSection
DO:
  if self:modified 
   then run doFillin (21075, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qPosition
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qPosition C-Win
ON LEAVE OF qPosition IN FRAME fSection
DO:
  if self:modified 
   then run doFillin (21030, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qRates
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qRates C-Win
ON LEAVE OF qRates IN FRAME fSection
DO:
  if self:modified 
   then run doFillin (21105, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qSearcher
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qSearcher C-Win
ON LEAVE OF qSearcher IN FRAME fSection
DO:
  if self:modified 
   then run doFillin (21120, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qTemps
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qTemps C-Win
ON LEAVE OF qTemps IN FRAME fSection
DO:
  if self:modified
   then run doFillin (21070, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qTermination
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qTermination C-Win
ON LEAVE OF qTermination IN FRAME fSection
DO:
  if self:modified
   then run doFillin (21040, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qTurnover
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qTurnover C-Win
ON LEAVE OF qTurnover IN FRAME fSection
DO:
  if self:input-value > 100 
   then
  do: 
    MESSAGE "Turnover cannot be greater than 100%"
     VIEW-AS ALERT-BOX INFO BUTTONS OK.
    return no-apply.
  end.
  if self:modified 
   then run doFillin (21055, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qTurnoverExplain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qTurnoverExplain C-Win
ON LEAVE OF qTurnoverExplain IN FRAME fSection
DO:
  if self:modified 
   then run doFillin (21060, self:screen-value).
   publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

PAUSE 0 BEFORE-HIDE.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

bAdd:load-image("images/add.bmp").
bAdd:load-image-insensitive("images/add-i.bmp").
bDelete:load-image("images/delete.bmp").
bDelete:load-image-insensitive("images/delete-i.bmp").
subscribe to "AuditOpened" anywhere.
subscribe to "AuditClosed" anywhere.
subscribe to "Close" anywhere.
subscribe to "LeaveAll" anywhere.

on 'value-changed':u anywhere
do:
  publish "EnableSave".
  publish "SetClickSave".
end.

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  run initializeFrame in this-procedure.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditClosed C-Win 
PROCEDURE AuditClosed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*  publish "SetUnderwriters" (input table underwriter).  */
 assign
   qLevel:screen-value in frame fSection = ""
   qPosition:screen-value in frame fSection = ""
   qTermination:screen-value in frame fSection = ""
   qABA:screen-value in frame fSection = ""
   qTurnover:screen-value in frame fSection = ""
   qTurnoverExplain:screen-value in frame fSection = ""
   qTemps:screen-value in frame fSection = ""
   qOrders:screen-value in frame fSection = ""
   qClosings:screen-value in frame fSection = ""
   qRates:screen-value in frame fSection = ""
   qSearcher:screen-value in frame fSection = ""
   .
 assign
   qLevel:sensitive in frame fSection = false
   qPosition:sensitive in frame fSection = false
   qTermination:sensitive in frame fSection = false
   qABA:sensitive in frame fSection = false
   qTurnover:sensitive in frame fSection = false
   qTurnoverExplain:sensitive in frame fSection = false
   qTemps:sensitive in frame fSection = false
   qOrders:sensitive in frame fSection = false
   qClosings:sensitive in frame fSection = false
   qRates:sensitive in frame fSection = false
   qSearcher:sensitive in frame fSection = false
   .

 &scoped-define ex-u true
 &scoped-define ex-b true
 {lib/unsetquestion.i &seq=21000}
 {lib/unsetquestion.i &seq=21005}
 {lib/unsetquestion.i &seq=21010}
 {lib/unsetquestion.i &seq=21025}
 {lib/unsetquestion.i &seq=21035}
 {lib/unsetquestion.i &seq=21045}
 {lib/unsetquestion.i &seq=21065}
 {lib/unsetquestion.i &seq=21090}
 {lib/unsetquestion.i &seq=21095}
 {lib/unsetquestion.i &seq=21100}
 {lib/unsetquestion.i &seq=21110}
 {lib/unsetquestion.i &seq=21115}
 {lib/unsetquestion.i &seq=21125}
 {lib/unsetquestion.i &seq=21130}

 &undefine ex-u
 {lib/unsetquestion.i &seq=21015}
 {lib/unsetquestion.i &seq=21085}

 &undefine ex-b

 empty temp-table underwriter.
 refreshBrowse().

 bAdd:sensitive in frame {&frame-name} = false.
 bDelete:sensitive in frame {&frame-name} = false.

 frame fSection:sensitive = false.
 frame fMain:sensitive = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditOpened C-Win 
PROCEDURE AuditOpened :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 &scoped-define questionType Background
 {lib/dispanswer.i &seq=21000}
 {lib/dispanswer.i &seq=21005}
 {lib/dispanswer.i &seq=21010}
 {lib/dispanswer.i &seq=21025}
 {lib/dispanswer.i &seq=21035}
 {lib/dispanswer.i &seq=21045}
 {lib/dispanswer.i &seq=21065}
 {lib/dispanswer.i &seq=21090}
 {lib/dispanswer.i &seq=21095}
 {lib/dispanswer.i &seq=21100}
 {lib/dispanswer.i &seq=21110}
 {lib/dispanswer.i &seq=21115}
 {lib/dispanswer.i &seq=21125}
 {lib/dispanswer.i &seq=21130}
 {lib/dispanswer.i &seq=21015}
 {lib/dispanswer.i &seq=21085}
 &undefine questionType

 
 publish "GetBackgroundAnswer" (21020, output std-ch).
 qLevel:screen-value in frame fSection = std-ch.

 publish "GetBackgroundAnswer" (21030, output std-ch).
 qPosition:screen-value = std-ch.

 publish "GetBackgroundAnswer" (21040, output std-ch).
 qTermination:screen-value = std-ch.

 publish "GetBackgroundAnswer" (21050, output std-ch).
 qABA:screen-value = std-ch.

 publish "GetBackgroundAnswer" (21055, output std-ch).
 qTurnover:screen-value = std-ch.

 publish "GetBackgroundAnswer" (21060, output std-ch).
 qTurnoverExplain:screen-value = std-ch.
 
 publish "GetBackgroundAnswer" (21070, output std-ch).
 qTemps:screen-value = std-ch.
 
 publish "GetBackgroundAnswer" (21075, output std-ch).
 qOrders:screen-value = std-ch.
 
 publish "GetBackgroundAnswer" (21080, output std-ch).
 qClosings:screen-value = std-ch.
 
 publish "GetBackgroundAnswer" (21105, output std-ch).
 qRates:screen-value = std-ch.
 
 publish "GetBackgroundAnswer" (21120, output std-ch).
 qSearcher:screen-value = std-ch.
 

 assign
  qLevel:sensitive in frame fSection = true
  qPosition:sensitive in frame fSection = true
  qTermination:sensitive in frame fSection = true
  qABA:sensitive in frame fSection = true
  qTurnover:sensitive in frame fSection = true
  qTurnoverExplain:sensitive in frame fSection = true
  qTemps:sensitive in frame fSection = true
  qOrders:sensitive in frame fSection = true
  qClosings:sensitive in frame fSection = true
  qRates:sensitive in frame fSection = true
  qSearcher:sensitive in frame fSection = true
  .
 
 publish "GetUnderwriters" (output table underwriter).
 refreshBrowse().
 bAdd:sensitive in frame {&frame-name} = true.

 frame fSection:sensitive = true.
 frame fMain:sensitive = true.
 publish "GetAuditStatus" (output pAuditStatus).

 if pAuditStatus = "C" then
   run AuditReadOnly.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditReadOnly C-Win 
PROCEDURE AuditReadOnly :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /*----------------not used----------------*/
 /*do with frame fSection:
 end.
 assign
   qLevel:sensitive           = false
   qPosition:sensitive        = false
   qTermination:sensitive     = false
   qABA:sensitive             = false
   qTurnover:sensitive        = false
   qTurnOverExplain:sensitive = false
   qTemps:sensitive           = false
   qOrders:sensitive          = false
   qClosings:sensitive        = false
   qRates:sensitive           = false
   qSearcher:sensitive        = false.

 do with frame fMain:
 end.
 assign
   bAdd:sensitive    = false
   bDelete:sensitive = false.*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Close C-Win 
PROCEDURE Close :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define variable tautosave as logical no-undo.
publish "GetAutosave" (output tautosave).
if tautosave then                                          
  publish "SaveAfterEveryAnswer".
apply 'WINDOW-CLOSE' to {&window-name} .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doButton C-Win 
PROCEDURE doButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/dobutton.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doCheckbox C-Win 
PROCEDURE doCheckbox :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/docheckbox.i &questionType="Background"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doFillin C-Win 
PROCEDURE doFillin :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/dofillin.i &questionType="Background" fieldname}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE brwUnderwriters 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  DISPLAY qLevel qPosition qTermination qABA qTurnover qTurnoverExplain qTemps 
          qOrders qClosings qRates qSearcher 
      WITH FRAME fSection IN WINDOW C-Win.
  VIEW FRAME fSection IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fSection}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initializeFrame C-Win 
PROCEDURE initializeFrame PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  &scoped-define ex-frameresize true

  &scoped-define ex-u true
  &scoped-define ex-b true
  &scoped-define questionType Background
  {lib/question.i &seq=21000 &r="1.75"}
  {lib/question.i &seq=21005 &r="1.75 + 2.50"}
  {lib/question.i &seq=21010 &r="1.75 + 5.0"}

  &undefine ex-u
  {lib/question.i &seq=21015 &r="1.75 + 7.5" &y-label="Gt" &y-tip="Great" &n-label="Gd" &n-tip="Good" &a-label="Av" &a-tip="Average" &u-label="Pr" &u-tip="Poor"} 
  
  &scoped-define ex-u true
  {lib/question.i &seq=21025 &r="1.75 + 12.5"} 
  {lib/question.i &seq=21035 &r="1.75 + 17.5"} 
  {lib/question.i &seq=21045 &r="1.75 + 22.5"} 
  {lib/question.i &seq=21055 &r="1.75 + 27.5"  &ex-y=true &ex-n=true &ex-a=true} 
  {lib/question.i &seq=21065 &r="1.75 + 32.5"} 
  {lib/question.i &seq=21075 &r="1.75 + 37.5" &ex-y=true &ex-n=true &ex-a=true} 
  {lib/question.i &seq=21080 &r="1.75 + 40.0" &ex-y=true &ex-n=true &ex-a=true} 
  
  &undefine ex-u
  {lib/question.i &seq=21085 &r="1.75 + 42.5" &y-label="P" &y-tip="Two party by phone (one initiates/one confirms)" &n-label="W" &n-tip="Password protected with two party verification for email or online request" &a-label="Fx" &a-tip="Fax to bank with authorized return bank confirmation" &u-label="O" &u-tip="Other"} 
  
  &scoped-define ex-u true
  {lib/question.i &seq=21090 &r="1.75 + 45.0"} 
  {lib/question.i &seq=21095 &r="1.75 + 47.5"}
  {lib/question.i &seq=21100 &r="1.75 + 50.0"}
  {lib/question.i &seq=21105 &r="1.75 + 52.5" &ex-y=true &ex-n=true &ex-a=true} 
  {lib/question.i &seq=21110 &r="1.75 + 55.0"}
  {lib/question.i &seq=21115 &r="1.75 + 57.5"}

  {lib/question.i &seq=21120 &r="1.75 + 62.5" &ex-y=true &ex-n=true &ex-a=true}
  {lib/question.i &seq=21125 &r="1.75 + 65.0"}
  {lib/question.i &seq=21130 &r="1.75 + 67.5" &ex-r=true}

  RUN ShowScrollBars(FRAME fSection:HANDLE, NO, YES).

  run AuditOpened in this-procedure.    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LeaveAll C-Win 
PROCEDURE LeaveAll :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 apply "leave" to qLevel in  frame fSection.
 apply "leave" to qPosition in  frame fSection.
 apply "leave" to qTermination in  frame fSection.
 apply "leave" to qABA in  frame fSection.
 apply "leave" to qTurnover in  frame fSection.
 apply "leave" to qTurnOverExplain in  frame fSection.
 apply "leave" to qTemps in  frame fSection.
 apply "leave" to qOrders in  frame fSection.
 apply "leave" to qClosings in frame fSection.
 apply "leave" to qRates in  frame fSection.
 apply "leave" to qSearcher in  frame fSection.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
if {&window-name}:window-state eq window-minimized  then
  {&window-name}:window-state = window-normal .
{&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/brw-sortdata.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.

 browse brwUnderwriters:width-pixels = frame fMain:width-pixels - 50.

 if {&window-name}:width-pixels > frame fSection:width-pixels 
  then
   do:
     frame fSection:width-pixels = {&window-name}:width-pixels.
     frame fSection:virtual-width-pixels = {&window-name}:width-pixels.
   end.
 else
   do:
     frame fSection:virtual-width-pixels = {&window-name}:width-pixels.
     frame fSection:width-pixels = {&window-name}:width-pixels.
         /* das: For some reason, shrinking a window size MAY cause the horizontal
            scroll bar.  The above sequence of widget setting should resolve it,
            but it doesn't every time.  So... */
     RUN ShowScrollBars(FRAME fSection:HANDLE, NO, YES).
   end.

 frame fSection:height-pixels = {&window-name}:height-pixels - 86.
 
 {lib/resizequestion.i &seq=21000}
 {lib/resizequestion.i &seq=21005}
 {lib/resizequestion.i &seq=21010}
 {lib/resizequestion.i &seq=21015}
 {lib/resizequestion.i &seq=21025}
 {lib/resizequestion.i &seq=21035}
 {lib/resizequestion.i &seq=21045}
 {lib/resizequestion.i &seq=21055}
 {lib/resizequestion.i &seq=21065}
 {lib/resizequestion.i &seq=21075}
 {lib/resizequestion.i &seq=21080}
 {lib/resizequestion.i &seq=21085}
 {lib/resizequestion.i &seq=21090}
 {lib/resizequestion.i &seq=21095}
 {lib/resizequestion.i &seq=21100}
 {lib/resizequestion.i &seq=21105}
 {lib/resizequestion.i &seq=21110}
 {lib/resizequestion.i &seq=21115}
 {lib/resizequestion.i &seq=21120}
 {lib/resizequestion.i &seq=21125}
 {lib/resizequestion.i &seq=21130}

 assign
   qTurnover:x = frame fSection:width-pixels - 200
   qOrders:x   = frame fSection:width-pixels - 200
   qClosings:x = frame fSection:width-pixels - 200
   qRates:x    = frame fSection:width-pixels - 200
   qSearcher:x = frame fsection:width-pixels - 200.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshBrowse C-Win 
FUNCTION refreshBrowse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 close query brwUnderwriters.
 publish "GetUnderwriters" (output table underwriter).
 open query brwUnderwriters for each underwriter by underwriter.uwName.
 bDelete:sensitive in frame fMain = available underwriter.
 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

