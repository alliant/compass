&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

File: wstatement.w 

Description: 

Input Parameters:
<none>

Output Parameters:
<none>

Author: Rahul

Created:

Modified: 
Date Name Comments
07/23/2019 
04/16/21    MK    Updated to "view" file for agent with no activity.
------------------------------------------------------------------------*/
/* This .W file was created with the Progress AppBuilder. */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
by this procedure. This is a good default which assures
that this procedure's triggers and internal procedures 
will execute in this procedure's storage, and that proper
cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ************************** Definitions ************************* */
{tt/agent.i}
{tt/period.i}
{lib/std-def.i}
{lib/ar-def.i}
{lib/get-column.i}
{lib/winlaunch.i}
{lib/rpt-defs.i}

{lib/getperiodname.i} /* Include function: getPeriodName */

define temp-table ttagent no-undo like agent
  field isSelect as logical.

define temp-table tagent no-undo like ttagent. 
  
define variable hDestination          as handle    no-undo.
define variable cAgentID              as character no-undo.  
define variable cAgentList            as character no-undo.
define variable dColumnWidth          as decimal   no-undo.
define variable cAgentManager         as character no-undo.
define variable iMonth                as integer   no-undo.
define variable iYear                 as integer   no-undo.                    
define variable dtStartDate           as date      no-undo.
define variable dtEndDate             as date      no-undo.
define variable chOutFile             as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttagent

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData ttagent.isSelect ttagent.stateID ttagent.agentID ttagent.name getStatus(ttagent.stat) @ ttagent.stat ttagent.addr1 ttagent.city ttagent.State   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData ttagent.isSelect   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwData ttagent
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwData ttagent
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each ttagent
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each ttagent.
&Scoped-define TABLES-IN-QUERY-brwData ttagent
&Scoped-define FIRST-TABLE-IN-QUERY-brwData ttagent


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bAgentLookup bPeriod bResetFilter tSearch ~
cbState cbStatus cbManager fstatementDate tbIsEmptyReport brwData ~
rbTranType RECT-4 RECT-2 RECT-5 
&Scoped-Define DISPLAYED-OBJECTS tSearch cbState cbStatus cbManager fPeriod ~
fstatementDate tbIsEmptyReport rbTranType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getStatus C-Win 
FUNCTION getStatus RETURNS CHARACTER
  ( input cAuth as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openFile C-Win 
FUNCTION openFile RETURNS logical
  (input pFilename as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup  NO-FOCUS
     LABEL "agentlookup" 
     SIZE 7.2 BY 1.71 TOOLTIP "Agent specific destinations".

DEFINE BUTTON bPeriod  NO-FOCUS
     LABEL "Period" 
     SIZE 5 BY 1.19 TOOLTIP "Select Period".

DEFINE BUTTON bResetFilter  NO-FOCUS
     LABEL "Reset" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reset filters".

DEFINE BUTTON bRunReport  NO-FOCUS
     LABEL "Run Report" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run Report".

DEFINE VARIABLE cbManager AS CHARACTER FORMAT "X(256)":U 
     LABEL "Manager" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE cbStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE fPeriod AS CHARACTER FORMAT "X(256)":U INITIAL "Select Period" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 22.6 BY 1 NO-UNDO.

DEFINE VARIABLE fstatementDate AS DATE FORMAT "99/99/99":U 
     LABEL "As Of" 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 TOOLTIP "As Of date should be greater then or equal to period end." NO-UNDO.

DEFINE VARIABLE tSearch AS CHARACTER FORMAT "X(10)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 45 BY 1 TOOLTIP "Enter data to be searched" NO-UNDO.

DEFINE VARIABLE rbTranType AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Files", "F",
"Misc.", "I",
"Both", "B"
     SIZE 11.4 BY 3.19 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 147 BY 2.38.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 157 BY 5.24.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 10.6 BY 2.38.

DEFINE VARIABLE tbIsEmptyReport AS LOGICAL INITIAL no 
     LABEL "Generate statement with no activity" 
     VIEW-AS TOGGLE-BOX
     SIZE 36.6 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      ttagent SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      ttagent.isSelect         column-label "Select"  width 10  view-as toggle-box
ttagent.stateID          label "State ID"             format "x(20)"     width 10      
ttagent.agentID          label "Agent ID"             format "x(20)"     width 10      
ttagent.name             label "Name"                 format "x(100)"    width 50      
getStatus(ttagent.stat)
@ ttagent.stat    column-label "Status"               format "x(20)"     width 12      
ttagent.addr1            label "Address"              format "x(100)"    width 30  
ttagent.city             label "City"                 format "x(20)"     width 14  
ttagent.State            label "State"                format "x(20)"     width 10  
enable  ttagent.isSelect
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 157 BY 11.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bRunReport AT ROW 17.91 COL 101.2 WIDGET-ID 488 NO-TAB-STOP 
     bAgentLookup AT ROW 1.67 COL 151.4 WIDGET-ID 492 NO-TAB-STOP 
     bPeriod AT ROW 17 COL 35.4 WIDGET-ID 402 NO-TAB-STOP 
     bResetFilter AT ROW 1.67 COL 141.4 WIDGET-ID 474 NO-TAB-STOP 
     tSearch AT ROW 2 COL 9.6 COLON-ALIGNED WIDGET-ID 354
     cbState AT ROW 2 COL 62 COLON-ALIGNED WIDGET-ID 464
     cbStatus AT ROW 2 COL 80.6 COLON-ALIGNED WIDGET-ID 440
     cbManager AT ROW 2 COL 112.6 COLON-ALIGNED WIDGET-ID 466
     fPeriod AT ROW 17.1 COL 10.4 COLON-ALIGNED NO-LABEL WIDGET-ID 44
     fstatementDate AT ROW 17.19 COL 51 COLON-ALIGNED WIDGET-ID 414
     tbIsEmptyReport AT ROW 18.86 COL 12 WIDGET-ID 470
     brwData AT ROW 4.1 COL 3 WIDGET-ID 200
     rbTranType AT ROW 17.33 COL 79.4 NO-LABEL WIDGET-ID 482
     "Output" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 16.19 COL 4.6 WIDGET-ID 436
     "Period:" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 17.33 COL 5 WIDGET-ID 480
     "Content" VIEW-AS TEXT
          SIZE 8.6 BY .62 AT ROW 16.71 COL 72.8 WIDGET-ID 486
     RECT-4 AT ROW 16.48 COL 3 WIDGET-ID 422
     RECT-2 AT ROW 1.33 COL 3 WIDGET-ID 478
     RECT-5 AT ROW 1.33 COL 149.6 WIDGET-ID 490
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 198.8 BY 24.38 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Generate Agent Statements"
         HEIGHT             = 20.76
         WIDTH              = 160.2
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData tbIsEmptyReport fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bRunReport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR FILL-IN fPeriod IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       fPeriod:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
  open query {&SELF-NAME} for each ttagent.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Generate Agent Statements */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Generate Agent Statements */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Generate Agent Statements */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup C-Win
ON CHOOSE OF bAgentLookup IN FRAME fMain /* agentlookup */
DO:
  publish "SetCurrentValue" ("DestinationAction", "arAgentStatementQuery").
  publish "ReferenceDestination".
  publish "SetCurrentValue" ("DestinationAction", "").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPeriod C-Win
ON CHOOSE OF bPeriod IN FRAME fMain /* Period */
DO:
  run getPeriod in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bResetFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bResetFilter C-Win
ON CHOOSE OF bResetFilter IN FRAME fMain /* Reset */
DO:
  run resetFilters in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRunReport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRunReport C-Win
ON CHOOSE OF bRunReport IN FRAME fMain /* Run Report */
DO:
  run checkValidation in this-procedure (output std-ch).
  if std-ch ne ""
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return no-apply.
    end.
  
  run runReport in this-procedure.
  
  browse brwData:refresh().
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  define buffer ttagent for ttagent.
  
  std-ha = brwData:current-column.
  if std-ha:label = "Select"
   then
    do:     
      std-lo = can-find(first ttagent where not(ttagent.isSelect)).   
      for each ttagent:            
        ttagent.isSelect = std-lo.
        /* Retaining selected record */  
        for first tagent where tagent.agentID = ttagent.agentID:
          tagent.isSelect = ttagent.isSelect.
        end.
      end.    
      run setButtons in this-procedure.
    end.
   else
    do:
      {lib/brw-startsearch.i}
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSearch C-Win
ON LEAVE OF tSearch IN FRAME fMain /* Search */
OR 'RETURN'        of tSearch
OR 'VALUE-CHANGED' of cbState
OR 'VALUE-CHANGED' of cbStatus
OR 'VALUE-CHANGED' of cbManager
DO:
  run filterData      in this-procedure.
  
  /* Enable reset filter button when filter applies */
  run setFilterButton in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */ 
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

 
assign 
  {&window-name}:min-height-pixels = {&window-name}:height-pixels
  {&window-name}:min-width-pixels  = {&window-name}:width-pixels
  {&window-name}:max-height-pixels = session:height-pixels
  {&window-name}:max-width-pixels  = session:width-pixels
  .

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME} .

setStatusMessage("").       
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow"  anywhere.

bAgentLookup :load-image            ("images/arrow_divide_right.bmp").
bAgentLookup :load-image-insensitive("images/arrow_divide_right-i.bmp").

bRunReport   :load-image            ("images/completed.bmp").
bRunReport   :load-image-insensitive("images/completed-i.bmp").

bPeriod      :load-image            ("images/s-calendar.bmp").
bPeriod      :load-image-insensitive("images/s-calendar-i.bmp").

bResetFilter :load-image            ("images/filtererase.bmp").
bResetFilter :load-image-insensitive("images/filtererase-i.bmp").

/* Fetch agent data */
run getAgentData in this-procedure.

publish "getAgentManagerList"(output cAgentManager).
                             
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK: 
  RUN enable_UI. 
  
  rbTranType:screen-value = "B".
  
  {lib/get-column-width.i &col="'Name'"    &var=dColumnWidth} 
  assign
      cbManager:list-item-pairs = {&ALL} + "," + {&ALL} + (if cAgentManager > "" then "," else "") + cAgentManager
      cbManager:screen-value    = {&ALL}
      .
    
  publish "getDefaultPeriod"(output iMonth,output iYear). 
  fPeriod:screen-value = getPeriodName(iMonth,iYear).
  /* Calculate date range of selected period */
  publish "getSelectedPeriodDate" (input iMonth,
                                   input iYear,
                                   output dtStartDate,
                                   output dtEndDate).
     
  on value-changed of ttagent.isSelect in browse brwData
  do:
    assign 
        std-ch = ttagent.agentID
        std-ro = rowid(ttagent)
        std-in = 0
        . 
    
    for each ttagent where ttagent.agentID = std-ch:
      if (rowid(ttagent) = std-ro)
       then       
        ttagent.isSelect = not ttagent.isSelect.
      /* Retaining selected record */  
      for first tagent where tagent.agentID = ttagent.agentID:
        tagent.isSelect = ttagent.isSelect.
      end.  
    end.    
    run setButtons in this-procedure.    
  end.
 
  fstatementDate:screen-value = string(dtEndDate).
  
  run setData    in this-procedure.
  run showWindow in this-procedure.  
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE checkValidation C-Win 
PROCEDURE checkValidation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter opError as character no-undo.
  
  do with frame {&frame-name}:
  end.
  
  if fstatementDate:input-value = ""
   then
    do:
      opError = "As of Date cannot be blank.".   
      return.
    end.
  
  if date(fstatementDate:input-value) < dtEndDate
   then
    do:
      opError =  "As of Date cannot be prior to the period end date.".
      return.
    end.
    

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tSearch cbState cbStatus cbManager fPeriod fstatementDate 
          tbIsEmptyReport rbTranType 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bAgentLookup bPeriod bResetFilter tSearch cbState cbStatus cbManager 
         fstatementDate tbIsEmptyReport brwData rbTranType RECT-4 RECT-2 RECT-5 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tagent for tagent.
  
  close query brwData.
  empty temp-table ttagent.

  do with frame {&frame-name}:
  end.
  
  for each tagent  
    where tagent.state    = (if cbState:screen-value   = "ALL" then tagent.state   else cbState:screen-value)
      and tagent.stat     = (if cbStatus:screen-value  = "ALL" then tagent.stat    else cbStatus:screen-value)
      and tagent.manager  = (if cbManager:screen-value = "ALL" then tagent.manager else cbManager:screen-value):
     
    if tagent.stat = "P" or tagent.stat = "W"   
     then
      next.
      
    if tSearch:screen-value <> "" and
      not ((tagent.stateID          matches "*" + tSearch:screen-value + "*") or
           (tagent.agentID          matches "*" + tSearch:screen-value + "*") or
           (lc(tagent.name)         matches "*" + lc(tSearch:screen-value) + "*") or
           (getStatus(tagent.stat)  matches "*" + tSearch:screen-value + "*") or
           (tagent.addr1            matches "*" + tSearch:screen-value + "*") or
           (tagent.city             matches "*" + tSearch:screen-value + "*") or
           (tagent.State            matches "*" + tSearch:screen-value + "*") 
           ) then
      next.
    
    create ttagent.
    buffer-copy tagent to ttagent. 
  end.
 
  open query brwData for each ttagent by ttagent.name.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getAgentData C-Win 
PROCEDURE getAgentData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer agent for agent.
  
  empty temp-table agent.
  empty temp-table tagent.
  
  publish "getAgents"  (output table agent).

  /* Storing original agent data */
  for each agent:
    create tagent.
    buffer-copy agent to tagent.
  end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getPeriod C-Win 
PROCEDURE getPeriod :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  run dialogperiod.w (input-output iMonth,
                      input-output iYear,
                      output std-ch).
                     
  fPeriod:screen-value = getPeriodName(iMonth,iYear).
  /* Calculate date range of selected period */
  publish "getSelectedPeriodDate" (input iMonth,
                                   input iYear,
                                   output dtStartDate,
                                   output dtEndDate).
                                   
  fstatementDate:screen-value = string(dtEndDate).                                   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resetFilters C-Win 
PROCEDURE resetFilters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Reset filters to initial state */
  assign
      bResetFilter:tooltip    = "Reset filters"
      cbState:screen-value    = {&ALL}
      cbStatus:screen-value   = {&ALL}
      cbManager:screen-value  = {&ALL}
      tSearch:screen-value    = ""
      .
        
  run filterData      in this-procedure.
  
  /* Disable reset filter button when filters reset to ALL */
  run setFilterButton in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE runReport C-Win 
PROCEDURE runReport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end. 
  
  define variable iFileCount    as integer   no-undo.
  define variable iSelected     as integer   no-undo.
  define variable cReportFormat as character no-undo.
  define variable cDestination  as character no-undo.
  
  assign
   iSelected     = 0
   cReportFormat = ""
   cDestination  = ""
   cAgentList    = "".

  for each ttagent where ttagent.isSelect = yes:
   assign
    iSelected  = iSelected + 1
    cAgentList = cAgentList + "," + ttagent.agentID
    .
  end.
  
  /* possible output formats */
  cReportFormat = {&rpt-pdf} + "," + {&rpt-csv}. 
  
  cDestination  = {&rpt-destConfigured} + "," + {&rpt-destEmail} + "," +
                  {&rpt-destPrinter}. 

  /* possible output destinations */
  if (iSelected = 1) then
   cDestination  = cDestination + "," + {&rpt-destView}.
  
  
  {lib/rpt-dialogDestination.i &rpt-format = cReportFormat &rpt-destinationType = cDestination}
    
  run server/generateagentstatement.p (input  trim(cAgentList,","),
                                       input  dtStartDate, /* das period.startDate */
                                       input  dtEndDate,   /* das period.endDate */
                                       input  fstatementDate:input-value,
                                       input  rbTranType:input-value, /* Transaction Type: File(s), Invoice(s), Both */
                                       input  tbIsEmptyReport:input-value,
                                       {lib/rpt-setparams.i},
                                       output chOutFile,
                                       output std-lo,
                                       output std-ch).
                                       
  if not std-lo
   then
    message std-ch view-as alert-box error buttons ok.
  else if rpt-behavior = "" /* View Report */
   then
    do:
     if chOutFile = "" 
      then
       message "Nothing to print." view-as alert-box warning buttons ok.
     else
      do iFileCount = 1 to num-entries(chOutFile):
       openFile(entry(iFileCount,chOutFile)).
      end. 
    end.
  else if std-ch <> ""
   then
    message std-ch view-as alert-box information buttons ok.
  else
   .
  
  open query brwData for each ttagent by ttagent.isselect desc.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setButtons C-Win 
PROCEDURE setButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttagent for ttagent.
  
  do with frame {&frame-name}:
  end. 
  
  std-in = 0.
  for each tagent where tagent.isselect = yes:
    std-in  = std-in + 1.       
  end.

  setStatusMessage(if std-in > 0 then (string(std-in) + if std-in > 1 then " records selected" else " record selected") else "").        
  bRunReport:sensitive = std-in > 0.

  browse brwData:refresh(). 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData C-Win 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer agent for agent.
  
  do with frame {&frame-name}:
  end.

  std-ch = "ALL,ALL".
  for each agent 
    where agent.stat ne "P"
      and agent.stat ne "W" break by agent.stat:
           
   if first-of(agent.stat) 
    then
     std-ch = std-ch + "," + getStatus(agent.stat) + "," + agent.stat.
  end.
  cbStatus:list-item-pairs = trim(std-ch,",").
  
  std-ch = "ALL".
  for each agent 
    where agent.stat ne "P"
      and agent.stat ne "W" break by agent.state:
           
   if first-of(agent.state) and agent.state ne "" 
    then
     std-ch = std-ch + "," + agent.state.
  end.  
  
  assign
      cbState:list-items    = trim(std-ch,",")  
      cbStatus:screen-value = {&all}
      cbState:screen-value  = {&all}
   .
  
  /* Set widgets enable/disable after data loads */
  assign
      cbState:sensitive      = can-find(first tagent)
      cbStatus:sensitive     = can-find(first tagent)
      cbManager:sensitive    = can-find(first tagent)
      tSearch:sensitive      = can-find(first tagent)
      bResetFilter:sensitive = false
      .
  
  run filterData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterButton C-Win 
PROCEDURE setFilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    bResetFilter:sensitive = not (cbState:screen-value  = {&all}
                             and cbStatus:screen-value  = {&all}
                             and cbManager:screen-value = {&all}
                             and tSearch:screen-value   = "").
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by ttagent.name ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  frame fMain:width-pixels = {&window-name}:width-pixels.
  frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
  frame fMain:height-pixels = {&window-name}:height-pixels.
  frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

  /* fMain components */
  brwData:width-pixels = frame fmain:width-pixels - 25.
  brwData:height-pixels = frame fMain:height-pixels - 120.
  {lib/resize-column.i &col="'Name'"    &var=dColumnWidth} 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getStatus C-Win 
FUNCTION getStatus RETURNS CHARACTER
  ( input cAuth as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable chSysPropDesc  as character   no-undo.
  publish "GetSysPropDesc" (input "AMD",
                            input "Agent",
                            input "Status", 
                            input cAuth,
                            output chSysPropDesc). 

  return chSysPropDesc.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openFile C-Win 
FUNCTION openFile RETURNS logical
  (input pFilename as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if search(pFilename) = ? 
   then
    do:
      message "Export file was not created"
          view-as alert-box error buttons ok.
        return false.
    end.

  RUN ShellExecuteA in this-procedure (0,
                                       "open",
                                       pFilename,
                                       "",
                                       "",
                                       1,
                                       OUTPUT std-in).
 
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

