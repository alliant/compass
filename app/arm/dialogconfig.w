&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/*------------------------------------------------------------------------
 File: dialogconfig.w

 Description: Create a configuration file to select states at beginning
 
 Input Parameters:
     <none>
 
 Output Parameters:
     <none>
 
 Author: Anjly Chanana
 
 Created:
  
 Modified    :
  Date        Name     Comments
  08/12/2020  AC       Added new CheckBox "View Report".
------------------------------------------------------------------------*/
 /* Standard Library */
{lib/std-def.i}
{lib/ar-def.i}

/* local variables definitions  */ 
define variable cTrackReportDir        as character no-undo.
define variable cSelectedPostingOption as character no-undo.
define variable cTranDateOption        as character no-undo.
define variable cTrackExportType       as character no-undo.
define variable lDefaultPosting        as logical   no-undo.
define variable lTrackConfirmExit      as logical   no-undo.
define variable lViewReport            as logical   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tConfirmExit tViewReport tDefaultDate rs1 ~
tReportDir bReportsSearch Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tSelectAgent tConfirmExit tViewReport ~
tDefaultDate rs1 tExportType tReportDir 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bReportsSearch 
     LABEL "..." 
     SIZE 4 BY 1.

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tReportDir AS CHARACTER FORMAT "X(200)":U 
     LABEL "Reports Directory" 
     VIEW-AS FILL-IN 
     SIZE 54.4 BY 1 TOOLTIP "Enter the default directory to save report PDF documents and CSV export files" NO-UNDO.

DEFINE VARIABLE rs1 AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Use Posting Date", "P",
"Use Invoice Date", "T"
     SIZE 23.4 BY 1.67 NO-UNDO.

DEFINE VARIABLE tExportType AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "XLSX", "X",
"CSV", "C"
     SIZE 20.8 BY .62 TOOLTIP "Select the type of Excel export to use" NO-UNDO.

DEFINE RECTANGLE RECT-31
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 81.4 BY 8.95.

DEFINE VARIABLE tConfirmExit AS LOGICAL INITIAL no 
     LABEL "Confirm Application Exit" 
     VIEW-AS TOGGLE-BOX
     SIZE 25.8 BY .81 NO-UNDO.

DEFINE VARIABLE tDefaultDate AS LOGICAL INITIAL no 
     LABEL "Default posting date" 
     VIEW-AS TOGGLE-BOX
     SIZE 25.8 BY .81 NO-UNDO.

DEFINE VARIABLE tSelectAgent AS LOGICAL INITIAL no 
     LABEL "Auto Default Agent" 
     VIEW-AS TOGGLE-BOX
     SIZE 24 BY .81 NO-UNDO.

DEFINE VARIABLE tViewReport AS LOGICAL INITIAL no 
     LABEL "View Posted Journal PDF" 
     VIEW-AS TOGGLE-BOX
     SIZE 31.4 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tSelectAgent AT ROW 1.62 COL 23.6 WIDGET-ID 394
     tConfirmExit AT ROW 2.62 COL 23.6 WIDGET-ID 132
     tViewReport AT ROW 3.62 COL 23.6 WIDGET-ID 402
     tDefaultDate AT ROW 4.62 COL 23.6 WIDGET-ID 398
     rs1 AT ROW 5.76 COL 23.6 NO-LABEL WIDGET-ID 390
     tExportType AT ROW 7.71 COL 23.6 NO-LABEL WIDGET-ID 208
     tReportDir AT ROW 8.81 COL 21.6 COLON-ALIGNED WIDGET-ID 2
     bReportsSearch AT ROW 8.81 COL 78.2 WIDGET-ID 30
     Btn_OK AT ROW 10.67 COL 26.6
     Btn_Cancel AT ROW 10.67 COL 44.2
     "Options" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.05 COL 3.6 WIDGET-ID 70
     "Export To:" VIEW-AS TEXT
          SIZE 10 BY .62 TOOLTIP "Export to Excel as Spreadsheet or CSV file" AT ROW 7.67 COL 12.2 WIDGET-ID 214
     "Transaction Date:" VIEW-AS TEXT
          SIZE 16.6 BY .62 AT ROW 5.81 COL 6.4 WIDGET-ID 396
     RECT-31 AT ROW 1.33 COL 2.4 WIDGET-ID 68
     SPACE(0.99) SKIP(1.81)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Configuration"
         CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-31 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RADIO-SET tExportType IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tSelectAgent IN FRAME fMain
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Configuration */
DO:
  std-lo = false.
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReportsSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReportsSearch fMain
ON CHOOSE OF bReportsSearch IN FRAME fMain /* ... */
DO:
  run getReportDirectory in this-procedure.  
  run enableDisableSave  in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel fMain
ON CHOOSE OF Btn_Cancel IN FRAME fMain /* Cancel */
DO:
  std-lo = no. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK fMain
ON CHOOSE OF Btn_OK IN FRAME fMain /* Save */
DO:
  run saveConfiguration in this-procedure.
  
  /* If there was any error then do not close the dialog. */
  if not std-lo
   then
    return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rs1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rs1 fMain
ON VALUE-CHANGED OF rs1 IN FRAME fMain
DO:
  run enableDisableSave in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tConfirmExit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tConfirmExit fMain
ON VALUE-CHANGED OF tConfirmExit IN FRAME fMain /* Confirm Application Exit */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDefaultDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDefaultDate fMain
ON VALUE-CHANGED OF tDefaultDate IN FRAME fMain /* Default posting date */
DO:
  /* Enable/disable transaction date radio button based on the value of default postion option */
  run enableTransactionDate in this-procedure.
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tExportType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tExportType fMain
ON VALUE-CHANGED OF tExportType IN FRAME fMain
DO:
  run enableDisableSave in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tReportDir
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tReportDir fMain
ON VALUE-CHANGED OF tReportDir IN FRAME fMain /* Reports Directory */
DO:
  run enableDisableSave in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tViewReport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tViewReport fMain
ON VALUE-CHANGED OF tViewReport IN FRAME fMain /* View Posted Journal PDF */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.
    
 publish "GetConfirmExit" (output tConfirmExit).
 publish "GetReportDir"   (output tReportDir).
 publish "GetViewPdf"     (output tViewReport).
 
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN enable_UI.
  
  publish "SetExportType" ({&csv}).             
  publish 'GetExportType' (output std-ch).
  tExportType:screen-value = std-ch.   
  
  /* Restricted to use only these options as per requirement, can be removed later when needed */
  publish "SetDefaultPostingOption" (false). 
  publish 'SetPostingConfig' (input "P").
  
  publish 'GetPostingConfig' (output cTranDateOption).
  publish 'GetDefaultPostingOption' (output std-lo).
  tDefaultDate:checked = std-lo.
 
  /* Enable/disable transaction date radio button based on the value of default postion option */
  run enableTransactionDate in this-procedure. 
  
  /* Disabled as per requirement, portion can be removed later on as per need */
  assign
      rs1:sensitive          = false
      tDefaultDate:sensitive = false
      .
  
  /* Setting initial values of widgets in variable to be used for enable/disable save */
  assign
    lDefaultPosting        = tDefaultDate:checked
    lTrackConfirmExit      = tConfirmExit:checked
    cTrackReportDir        = tReportDir:input-value    
    cTrackExportType       = tExportType:input-value
    cSelectedPostingOption = rs1:input-value 
    lViewReport            = tViewReport:checked   
    .
    
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  
END.
               
run disable_UI.

return string(std-lo).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave fMain 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Enabling the save button only when user made any modification on this dialog after its loading. */
  Btn_OK:sensitive = not (lTrackConfirmExit         = tConfirmExit:checked     and
                          lDefaultPosting           = tDefaultDate:checked     and
                          cTrackReportDir           = tReportDir:input-value   and
                          cTrackExportType          = tExportType:input-value  and                           
                          cSelectedPostingOption    = rs1:input-value          and
                          lViewReport               = tViewReport:checked) no-error
                          .
                          
                          
                          
                     
                                                                                                         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableTransactionDate fMain 
PROCEDURE enableTransactionDate :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
    
  if tDefaultDate:checked
   then
    assign        
        rs1:screen-value = "P"
        rs1:sensitive    = false
        .    
   else
    assign
        rs1:sensitive    = true
        rs1:screen-value = cTranDateOption        
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tSelectAgent tConfirmExit tViewReport tDefaultDate rs1 tExportType 
          tReportDir 
      WITH FRAME fMain.
  ENABLE tConfirmExit tViewReport tDefaultDate rs1 tReportDir bReportsSearch 
         Btn_Cancel 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getReportDirectory fMain 
PROCEDURE getReportDirectory :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name} :
  end.
  
  std-ch = tReportDir:input-value.
  
  if tReportDir:input-value > ""
   then 
    system-dialog get-dir std-ch
     initial-dir std-ch.
   else 
    system-dialog get-dir std-ch.
  
  if std-ch > "" and std-ch <> ?
   then 
    tReportDir:screen-value = std-ch.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveConfiguration fMain 
PROCEDURE saveConfiguration :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name} :
  end.
  
  if cTrackReportDir <> tReportDir:input-value
   then
    do:    
      if tReportDir:input-value > ""
       then
        do:
          publish "SetReportDir" (tReportDir:input-value).
          if error-status:error
           then
            do:
              message "Invalid Directory"
                  view-as alert-box error buttons OK.
              std-lo = false. 
          
              run enableDisableSave in this-procedure.
          
              apply "ENTRY" to tReportDir.
              return.
            end.
        end.
       else publish "SetReportDir" ("").
    end.   

  publish "SetAutoDefaultAgent" (tSelectAgent:checked).
  
  if cTrackExportType <> tExportType:input-value
   then   
    publish "SetExportType" (tExportType:input-value).
  
  if lDefaultPosting <> tDefaultDate:checked
   then
    publish "SetDefaultPostingOption" (tDefaultDate:checked).
  
  if cSelectedPostingOption <> rs1:input-value
   then  
    publish 'SetPostingConfig' (input rs1:input-value).
  
  if lTrackConfirmExit <> tConfirmExit:checked
   then
    publish "SetConfirmExit" (tConfirmExit:checked).
    
  if lViewReport <> tViewReport:checked
   then
    publish "SetViewPdf" (input tViewReport:checked). 
          
  std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

