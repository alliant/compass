&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wproductionapply.w

  Description: Window for apply amount to file/misc invoices

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Sagar R Koralli

  Created: 05.21.2024
  Modified:
  Date        Name     Description   
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/ar-def.i}
{lib/winlaunch.i} 
{lib/winshowscrollbars.i}
{lib/ar-getentitytype.i} /* Include function: getEntityType */
{lib/ar-gettrantype.i}   /* Include function: getTranType */

/* include file to normalize file number */
{lib/normalizefileid.i}

/* Temp-table Definition */
{tt/arbatch.i}
{tt/arbatch.i &tableAlias=ttarbatch}
{tt/artran.i  &tableAlias=ttartran}
{tt/artran.i  &tableAlias=tartran}
{tt/artran.i  &tableAlias=artranref}
{tt/artran.i  &tableAlias=ttartranref}
{tt/artran.i  &tableAlias=tempartran}

define temp-table artran no-undo like ttartran
  field notetype     as character
  field tempnote     as character.

/* Local Variables */
define variable cStateID             as character no-undo. 
define variable cRefType             as character no-undo. 
define variable deOldAppliedAmt      as decimal   no-undo.
define variable deTempOldAppliedAmt  as decimal   no-undo.
define variable dtDefaultPostingDate as date      no-undo.
define variable cCodeList            as character no-undo.
define variable cUserName            as character no-undo.
define variable lDataChanged         as logical   no-undo.

define variable lLocked              as logical   no-undo.
define variable lDefaultAgent        as logical   no-undo.
define variable lGetRefData          as logical   no-undo.
define variable lGetAgentData        as logical   no-undo.

define variable iBatchID             as integer   no-undo.
define variable cFileNum             as character no-undo.
define variable cInvoiceID           as character no-undo.
define variable iSelectedRef         as integer   no-undo.
define variable cFileNumber          as character no-undo.
define variable selectedAgent        as character no-undo.
define variable lAuto                as logical   no-undo.  
define variable lGet                 as logical   no-undo.
define variable dcRemainAmt          as decimal   no-undo.

/* variables used to store original values for resize */
define variable dColumnWidth as decimal no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwartran

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES artran

/* Definitions for BROWSE brwartran                                     */
&Scoped-define FIELDS-IN-QUERY-brwartran getTranType(artran.type) @ artran.type "Type" artran.filenumber "File" artran.tranDate "File Date" artran.tranamt "Original" artran.appliedamt "Applied" artran.remainingamt "Remaining" artran.appliedamtbyref "Apply" artran.isRefapplied artran.stat   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwartran artran.isRefapplied artran.appliedamtbyref   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwartran artran
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwartran artran
&Scoped-define SELF-NAME brwartran
&Scoped-define QUERY-STRING-brwartran for each artran
&Scoped-define OPEN-QUERY-brwartran open query {&SELF-NAME} for each artran.
&Scoped-define TABLES-IN-QUERY-brwartran artran
&Scoped-define FIRST-TABLE-IN-QUERY-brwartran artran


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwartran}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bReferenceLk flAgentID bAgentLookup bAgentLk ~
btExport flFileNo flPostingdate brwartran flTotalamt flAppliedAmt ~
flUnappliedAmt flNote flOther RECT-78 RECT-84 RECT-85 RECT-87 
&Scoped-Define DISPLAYED-OBJECTS flAgentID flFileParam flFileNo ~
flPostingdate flFile cbNotes edNotes flReference flName flTotalamt ~
flAppliedAmt flUnappliedAmt flNote flTotal fFileAmt fRemainAmt flOther 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFormattedNumber C-Win 
FUNCTION getFormattedNumber RETURNS CHARACTER
  ( input deTotal as decimal,
    input hWidget as handle)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resetScreen C-Win 
FUNCTION resetScreen RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwartran 
       MENU-ITEM m_Transaction_Details LABEL "Transaction Details"
       MENU-ITEM m_View_Notes   LABEL "View Notes"    
       RULE
       MENU-ITEM m_Export       LABEL "Export to Excel".


/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLk  NO-FOCUS
     LABEL "Lock" 
     SIZE 4.8 BY 1.14 TOOLTIP "Agent is locked".

DEFINE BUTTON bAgentLookup 
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Agent lookup".

DEFINE BUTTON bFileParamLp 
     LABEL "FileLookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "File lookup".

DEFINE BUTTON bReferenceLk  NO-FOCUS
     LABEL "Lock" 
     SIZE 4.8 BY 1.14 TOOLTIP "Payment is locked".

DEFINE BUTTON bRefLookup 
     LABEL "RefLookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Select reference".

DEFINE BUTTON btAutoApply  NO-FOCUS
     LABEL "Auto" 
     SIZE 7.2 BY 1.71 TOOLTIP "Auto apply".

DEFINE BUTTON btAutoUnapply  NO-FOCUS
     LABEL "Auto Unapply" 
     SIZE 7.2 BY 1.71 TOOLTIP "Auto unapply".

DEFINE BUTTON btExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export to excel".

DEFINE BUTTON btFilter  NO-FOCUS
     LABEL "Filter" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reset filters".

DEFINE BUTTON btGetDataOnAgent  NO-FOCUS
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data on agent".

DEFINE BUTTON btPost  NO-FOCUS
     LABEL "Post" 
     SIZE 7.2 BY 1.71 TOOLTIP "Post changes".

DEFINE BUTTON btRefGet  NO-FOCUS
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE BUTTON btReset  NO-FOCUS
     LABEL "Reset" 
     SIZE 4.8 BY 1.14 TOOLTIP "Undo".

DEFINE VARIABLE cbNotes AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 24.6 BY 1 NO-UNDO.

DEFINE VARIABLE edNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 111.2 BY 2.05 NO-UNDO.

DEFINE VARIABLE fFileAmt AS DECIMAL FORMAT "(z,zzz,zzz)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 19.2 BY 1 NO-UNDO.

DEFINE VARIABLE flAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE flAppliedAmt AS DECIMAL FORMAT "-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Production" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE flFile AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE flFileNo AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 34.8 BY 1 NO-UNDO.

DEFINE VARIABLE flFileParam AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 52.8 BY 1 NO-UNDO.

DEFINE VARIABLE flNote AS CHARACTER FORMAT "X(256)":U INITIAL "Note:" 
      VIEW-AS TEXT 
     SIZE 5.6 BY .62 NO-UNDO.

DEFINE VARIABLE flOther AS DECIMAL FORMAT "-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Other" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE flPostingdate AS DATE FORMAT "99/99/99":U 
     LABEL "Use Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE flReference AS CHARACTER FORMAT "X(256)":U 
     LABEL "Payment" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE flTotal AS CHARACTER FORMAT "X(256)":U INITIAL "Totals" 
     VIEW-AS FILL-IN 
     SIZE 7.8 BY 1 NO-UNDO.

DEFINE VARIABLE flTotalamt AS DECIMAL FORMAT "-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Total" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE flUnappliedAmt AS DECIMAL FORMAT "-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Unapplied" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE fRemainAmt AS DECIMAL FORMAT "(z,zzz,zzz)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 19.2 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-78
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 56 BY 2.81.

DEFINE RECTANGLE RECT-84
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 38.6 BY 2.81.

DEFINE RECTANGLE RECT-85
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 24.4 BY 2.81.

DEFINE RECTANGLE RECT-87
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 36 BY 2.52.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwartran FOR 
      artran SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwartran
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwartran C-Win _FREEFORM
  QUERY brwartran DISPLAY
      getTranType(artran.type) @ artran.type  label         "Type"          format "x(9)"      
artran.filenumber                       label         "File"          format "x(30)" width 20 
artran.tranDate                         label         "File Date"       format "99/99/99" width 10
artran.tranamt                          label         "Original"        format "->>,>>>,>>>,>>9.99"
artran.appliedamt                       label         "Applied"         format "->,>>>,>>>,>>9.99"
artran.remainingamt                     label         "Remaining"       format "->,>>>,>>>,>>9.99" 
artran.appliedamtbyref                  label         "Apply"           format ">,>>>,>>>,>>9.99"
artran.isRefapplied                     column-label  "Select" width 9 view-as toggle-box
artran.stat                             label  "Status"
enable artran.isRefapplied artran.appliedamtbyref
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 155.4 BY 16.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bReferenceLk AT ROW 1.19 COL 109.2 WIDGET-ID 478 NO-TAB-STOP 
     btAutoUnapply AT ROW 4.52 COL 75 WIDGET-ID 496 NO-TAB-STOP 
     btGetDataOnAgent AT ROW 1.33 COL 65.8 WIDGET-ID 482 NO-TAB-STOP 
     flAgentID AT ROW 1.24 COL 10.2 COLON-ALIGNED WIDGET-ID 352
     bAgentLookup AT ROW 1.19 COL 28.2 WIDGET-ID 350
     btRefGet AT ROW 1.33 COL 116 WIDGET-ID 266 NO-TAB-STOP 
     bAgentLk AT ROW 1.19 COL 33.2 WIDGET-ID 476 NO-TAB-STOP 
     flFileParam AT ROW 1.24 COL 42 COLON-ALIGNED WIDGET-ID 488 NO-TAB-STOP 
     bFileParamLp AT ROW 1.19 COL 60.2 WIDGET-ID 486
     btExport AT ROW 6.95 COL 2 WIDGET-ID 2 NO-TAB-STOP 
     bRefLookup AT ROW 1.19 COL 104.2 WIDGET-ID 360
     btReset AT ROW 7 COL 162.6 WIDGET-ID 268 NO-TAB-STOP 
     flFileNo AT ROW 4.86 COL 12.2 COLON-ALIGNED WIDGET-ID 316
     flPostingdate AT ROW 4.76 COL 96.8 COLON-ALIGNED WIDGET-ID 296
     flFile AT ROW 2.38 COL 84 COLON-ALIGNED NO-LABEL WIDGET-ID 374 NO-TAB-STOP 
     brwartran AT ROW 7 COL 7 WIDGET-ID 200
     cbNotes AT ROW 25.57 COL 10.8 COLON-ALIGNED WIDGET-ID 342
     edNotes AT ROW 25.57 COL 51.2 NO-LABEL WIDGET-ID 338
     btPost AT ROW 4.43 COL 116.4 WIDGET-ID 304 NO-TAB-STOP 
     flReference AT ROW 1.24 COL 84 COLON-ALIGNED WIDGET-ID 362 NO-TAB-STOP 
     btFilter AT ROW 4.52 COL 51 WIDGET-ID 272 NO-TAB-STOP 
     btAutoApply AT ROW 4.52 COL 67.8 WIDGET-ID 322 NO-TAB-STOP 
     flName AT ROW 2.43 COL 10.2 COLON-ALIGNED NO-LABEL WIDGET-ID 354 NO-TAB-STOP 
     flTotalamt AT ROW 1.24 COL 135.4 WIDGET-ID 204 NO-TAB-STOP 
     flAppliedAmt AT ROW 2.57 COL 129.8 WIDGET-ID 206 NO-TAB-STOP 
     flUnappliedAmt AT ROW 5.1 COL 130.6 WIDGET-ID 208 NO-TAB-STOP 
     flNote AT ROW 26.19 COL 43.2 COLON-ALIGNED NO-LABEL WIDGET-ID 346 NO-TAB-STOP 
     flTotal AT ROW 24.1 COL 4.2 COLON-ALIGNED NO-LABEL WIDGET-ID 314 NO-TAB-STOP 
     fFileAmt AT ROW 24.1 COL 105.8 COLON-ALIGNED NO-LABEL WIDGET-ID 306 NO-TAB-STOP 
     fRemainAmt AT ROW 24.1 COL 128 COLON-ALIGNED NO-LABEL WIDGET-ID 504 NO-TAB-STOP 
     flOther AT ROW 3.76 COL 134.6 WIDGET-ID 506 NO-TAB-STOP 
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 3.57 COL 8.2 WIDGET-ID 276
     "Post" VIEW-AS TEXT
          SIZE 4.6 BY .62 AT ROW 3.57 COL 88.4 WIDGET-ID 366
     "Auto Apply/Unapply" VIEW-AS TEXT
          SIZE 19.4 BY .62 AT ROW 3.67 COL 64.6 WIDGET-ID 470
     "Applied" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 2.05 COL 128.8 WIDGET-ID 510
     RECT-78 AT ROW 3.91 COL 7 WIDGET-ID 214
     RECT-84 AT ROW 3.91 COL 86.8 WIDGET-ID 336
     RECT-85 AT ROW 3.91 COL 62.6 WIDGET-ID 468
     RECT-87 AT ROW 2.43 COL 127.2 WIDGET-ID 508
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 167.8 BY 27.71 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Apply Payment to Production Files"
         HEIGHT             = 27.86
         WIDTH              = 169.6
         MAX-HEIGHT         = 33.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwartran flFile DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bFileParamLp IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bRefLookup IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwartran:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwartran:HANDLE
       brwartran:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwartran:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR BUTTON btAutoApply IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btAutoUnapply IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btFilter IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btGetDataOnAgent IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btPost IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btRefGet IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btReset IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbNotes IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR edNotes IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fFileAmt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flAppliedAmt IN FRAME DEFAULT-FRAME
   ALIGN-L                                                              */
ASSIGN 
       flAppliedAmt:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN flFile IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flFileParam IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flName IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flName:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN flOther IN FRAME DEFAULT-FRAME
   ALIGN-L                                                              */
ASSIGN 
       flOther:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN flReference IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flTotal IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flTotal:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN flTotalamt IN FRAME DEFAULT-FRAME
   ALIGN-L                                                              */
ASSIGN 
       flTotalamt:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN flUnappliedAmt IN FRAME DEFAULT-FRAME
   ALIGN-L                                                              */
ASSIGN 
       flUnappliedAmt:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN fRemainAmt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwartran
/* Query rebuild information for BROWSE brwartran
     _START_FREEFORM
open query {&SELF-NAME} for each artran.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwartran */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Apply Payment to Production Files */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Apply Payment to Production Files */
DO:  
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Apply Payment to Production Files */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup C-Win
ON CHOOSE OF bAgentLookup IN FRAME DEFAULT-FRAME /* agentlookup */
DO:
  define variable cAgentID  as character no-undo.
  define variable cName     as character no-undo.
    
  run dialogagentlookup.w(input flAgentID:input-value,
                          input "",        /* Selected State ID */
                          input false,     /* Allow 'ALL' */
                          output cAgentID,
                          output cStateID, /* Agent state ID */
                          output cName,
                          output std-lo).
   
  if not std-lo
   then
    return no-apply.
     
  assign
      flAgentID:screen-value     = cAgentID
      flName:screen-value        = cName
      btGetDataOnAgent:sensitive = true            
      btRefGet:sensitive         = false
      flFileParam:sensitive      = false 
      bFileParamLp:sensitive     = (flAgentID:input-value <> "")  
           
      . 
   
  if selectedAgent <> cAgentID  
   then
    do:
      selectedAgent = flAgentID:input-value.
      
      if lDefaultAgent and flAgentID:input-value <> ""
       then
        /* Set default AgentID */
        publish "SetDefaultAgent" (input flAgentID:input-value).               
    end.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileParamLp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileParamLp C-Win
ON CHOOSE OF bFileParamLp IN FRAME DEFAULT-FRAME /* FileLookup */
DO:  
  if flFileParam:input-value > "" and
     cFileNum <> flFileParam:input-value
   then cFileNum =  flFileParam:input-value no-error. 
   
  if flFileParam:input-value = "" 
   then 
    cFileNum = "".   
    
  cFileNum = normalizefileID(cFileNum). 
  run dialogtranlookup.w(input cFileNum,               /* File ID */  
                         input flAgentID:input-value, /* AgentID */             
                         input {&File},               /* Type */
                         output std-ch,               /* File ID */                                
                         output std-lo).
     
  if not std-lo or flFileParam:input-value = std-ch  
   then
     return no-apply.
          
  flFileParam:screen-value = std-ch.
  
  run clearscreen in this-procedure.
  
  cFileNum = flFileParam:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefLookup C-Win
ON CHOOSE OF bRefLookup IN FRAME DEFAULT-FRAME /* RefLookup */
DO: 
  run dialogreferencelookup.w(input iSelectedRef,              /* arTranID */
                              input {&Agent},                  /* Entity */
                              input flAgentID:input-value,     /* EntityID */
                              input "P",     /* Ref Type */                              
                              output table artranref,
                              output std-lo).   
  
  find first artranref no-error.
  if not available artranref
   then
    do:
      find first ttartranref no-error.
      if available ttartranref
       then
        do:
          create artranref.
          buffer-copy ttartranref to artranref.
        end.
       else
        return no-apply.
    end.  
  
  empty temp-table ttartranref.
  
  /* Temp table to store original values before any operation */    
  create ttartranref.
  buffer-copy artranref to ttartranref.
  assign
      cFileNumber              = artranref.fileNumber
      flReference:screen-value = artranref.reference
      flFile:screen-value      = artranref.fileNumber
      .
  
  if iSelectedRef = artranref.artranID 
   then
    return no-apply .
      
  iSelectedRef = artranref.artranID.
  
  empty temp-table artranref.
  
  /* Enable Get button only when Check is selected */
  btRefGet:sensitive = (flReference:input-value <> "").
    
  /* Clears the payment/credit header section */
  run emptyRefHeader in this-procedure.
  
  if btRefGet:sensitive
   then
    apply 'choose' to btRefGet.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwartran
&Scoped-define SELF-NAME brwartran
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwartran C-Win
ON ROW-DISPLAY OF brwartran IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}
  
  assign
      artran.tranamt:fgcolor in browse brwartran = if (artran.tranamt < 0)         then 12 
                                                   else ?
      artran.remainingamt:fgcolor                = if (artran.remainingamt < 0)    then 12 
                                                   else ?
      artran.appliedamt:fgcolor                  = if (artran.appliedamt < 0)      then 12 
                                                   else ?                                                          
      artran.appliedamtbyref:fgcolor             = if (artran.appliedamtbyref < 0) then 12 
                                                   else ?                                                                                                                    
      .                                   
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwartran C-Win
ON START-SEARCH OF brwartran IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwartran C-Win
ON VALUE-CHANGED OF brwartran IN FRAME DEFAULT-FRAME
DO:
  if available artran and can-find(first ttartran where ttartran.artranID = artran.artranID
                                                    and ttartran.arnotes  <> "")
   then
    menu-item m_View_Notes:sensitive in menu POPUP-MENU-brwartran = true.
   else
    menu-item m_View_Notes:sensitive in menu POPUP-MENU-brwartran = false.
  
  if available artran and artran.type = {&file}
   then
    do:
      /* Set Note type combo-box depending on the note selected */
      assign
          cbNotes:sensitive  = flReference:input-value <> ""
          edNotes:sensitive  = flReference:input-value <> ""
          .      
      run setNoteCombo in this-procedure.
    end.  
   else
    do:
      /* Disable notes for misc invoices */
      assign
          cbNotes:sensitive  = false
          edNotes:sensitive  = false
          .
      run resetNoteCombo in this-procedure.    
    end.
    
  if lDataChanged
   then
    run refreshBrowserData in this-procedure.
    
  lDataChanged = false.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btAutoApply
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btAutoApply C-Win
ON CHOOSE OF btAutoApply IN FRAME DEFAULT-FRAME /* Auto */
do:
  run setNoteField in this-procedure.
  run autoApply in this-procedure.
  run updateTotalRemainingAmt in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btAutoUnapply
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btAutoUnapply C-Win
ON CHOOSE OF btAutoUnapply IN FRAME DEFAULT-FRAME /* Auto Unapply */
do:
  run autoUnApply in this-procedure.
  run updateTotalRemainingAmt in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btExport C-Win
ON CHOOSE OF btExport IN FRAME DEFAULT-FRAME /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btFilter C-Win
ON CHOOSE OF btFilter IN FRAME DEFAULT-FRAME /* Filter */
do:  
  run resetFilterButton in this-procedure. 
  run resetNoteCombo in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGetDataOnAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGetDataOnAgent C-Win
ON CHOOSE OF btGetDataOnAgent IN FRAME DEFAULT-FRAME /* Get */
DO: 
  if not lGetAgentData
   then
    do:
      run getAgentData in this-procedure.
      
      lGetAgentData = lGet.
      
      if not lGetAgentData 
       then
        return.
        
      btGetDataOnAgent:load-image ("images/cancel.bmp").
      btGetDataOnAgent:load-image-insensitive("images/cancel-i.bmp").
    end.
   else
    do:
      /* cancel agent */
      run cancelAgent in this-procedure.
 
      lGetAgentData = false.
       
      btGetDataOnAgent:load-image ("images/completed.bmp").
      btGetDataOnAgent:load-image-insensitive("images/completed-i.bmp").
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btPost
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btPost C-Win
ON CHOOSE OF btPost IN FRAME DEFAULT-FRAME /* Post */
do:
  run setNoteField in this-procedure.
  
  apply 'leave' to artran.appliedamtbyref in browse brwartran.
  
  /* Applying/Unapplying AR invoices against payment records */
  run applyAmount in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btRefGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btRefGet C-Win
ON CHOOSE OF btRefGet IN FRAME DEFAULT-FRAME /* Get */
DO:
  if not lGetRefData
   then
    do:
      
      /* Get transaction records based on selected reference */
      run getReferenceData in this-procedure.
      
      if lLocked
       then
        lGetRefData = true.
       else
        return.
      
      btRefGet:load-image ("images/cancel.bmp").
      btRefGet:load-image-insensitive("images/cancel-i.bmp").
    end.
   else
    do:
      
      /* Clears the payment/credit and cancel applying */
      run cancelReference in this-procedure.
      
      if not std-lo /* if dont want to change the status */
       then
        return.
       else
        lGetRefData = false.
        
      btRefGet:load-image ("images/completed.bmp").
      btRefGet:load-image-insensitive("images/completed-i.bmp").
    end.
  run updateTotalRemainingAmt in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btReset
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btReset C-Win
ON CHOOSE OF btReset IN FRAME DEFAULT-FRAME /* Reset */
do:
  /* Resetting all the unsaved changes to the original state*/
  run undoAllChanges in this-procedure.
  run updateTotalRemainingAmt in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbNotes C-Win
ON VALUE-CHANGED OF cbNotes IN FRAME DEFAULT-FRAME /* Type */
DO:        
  edNotes:screen-value = if self:screen-value = {&selectcombo} then "" else self:screen-value.
    
  if available artran 
   then
    do:
      artran.notetype = self:screen-value.
      apply 'entry' to edNotes.
      edNotes:move-to-eof().           
    end.    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edNotes C-Win
ON LEAVE OF edNotes IN FRAME DEFAULT-FRAME
DO: 
  run setNoteField in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edNotes C-Win
ON VALUE-CHANGED OF edNotes IN FRAME DEFAULT-FRAME
DO:
  cbNotes:sensitive = self:screen-value = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID C-Win
ON LEAVE OF flAgentID IN FRAME DEFAULT-FRAME /* Agent ID */
DO:
  if flAgentID:input-value <> "" and not validAgent()
   then return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID C-Win
ON RETURN OF flAgentID IN FRAME DEFAULT-FRAME /* Agent ID */
DO:
  if not validAgent()
   then
    return no-apply. 
  apply 'choose' to btGetDataOnAgent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID C-Win
ON VALUE-CHANGED OF flAgentID IN FRAME DEFAULT-FRAME /* Agent ID */
DO:
  assign
      btGetDataOnAgent:sensitive = false      
      flName:screen-value        = ""
      .   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flFileNo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flFileNo C-Win
ON RETURN OF flFileNo IN FRAME DEFAULT-FRAME /* File */
OR 'LEAVE'  of flFileNo
DO:
  run filterData in this-procedure.
  run setFilterButton in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flFileNo C-Win
ON VALUE-CHANGED OF flFileNo IN FRAME DEFAULT-FRAME /* File */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Export
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Export C-Win
ON CHOOSE OF MENU-ITEM m_Export /* Export to Excel */
DO:
  apply "choose" to btExport in frame {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Transaction_Details
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Transaction_Details C-Win
ON CHOOSE OF MENU-ITEM m_Transaction_Details /* Transaction Details */
DO:
   /* Show all artran records related to the file/misc invoice */
   if available artran
    then
     publish "OpenWindow" (input "wtransactiondetail",      /*childtype*/
                           input string(artran.artranID),   /*childid*/
                           input "wtransactiondetail.w",    /*window*/
                           input "integer|input|" + string(artran.artranID)  + "^integer|input|0^character|input|" + "Production",    /*parameters*/                               
                           input this-procedure).           /*cuttentProcedure handle*/ 

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Notes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Notes C-Win
ON CHOOSE OF MENU-ITEM m_View_Notes /* View Notes */
DO:
  /* Show ar notes */
  run showArNotes in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Trigger for auto applying amount on the selected row against payment/credit */   
on value-changed of artran.isRefapplied in browse brwartran
do:
  if flReference:input-value in frame {&frame-name} = ""
   then return no-apply.
   
  run applyAmtOnSelect in this-procedure.
end.   
   
on entry of artran.appliedamtbyref in browse brwartran
do:
  if available artran 
   then
    do: 
      find first ttartran where ttartran.artranID = artran.artranID no-error.
      if available ttartran 
       then
        /* Retain original value of applied amount before any changes */ 
        deOldAppliedAmt = ttartran.appliedamtbyref. 
      
      /* Retain temporary changed value of applied amount */
      deTempOldAppliedAmt = artran.appliedamtbyref.
    end.  
    run updateTotalRemainingAmt in this-procedure.
end.
      
on leave, tab, return of artran.appliedamtbyref in browse brwartran
do:
  run updateTranAmount in this-procedure. 
end. 

on value-changed of artran.appliedamtbyref in browse brwartran
do:
  lDataChanged = (deOldAppliedAmt     <> artran.appliedamtbyref:input-value in browse brwartran) or 
                 (deTempOldAppliedAmt <> artran.appliedamtbyref:input-value in browse brwartran).  
end.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow" anywhere.

btGetDataOnAgent:load-image ("images/completed.bmp").
btGetDataOnAgent:load-image-insensitive("images/completed-i.bmp").
btRefGet        :load-image ("images/completed.bmp").
btRefGet        :load-image-insensitive("images/completed-i.bmp").
btPost          :load-image ("images/check.bmp").
btPost          :load-image-insensitive("images/check-i.bmp").
btReset         :load-image ("images/s-undo.bmp").              
btReset         :load-image-insensitive("images/s-undo-i.bmp").
btExport        :load-image ("images/s-excel.bmp").
btExport        :load-image-insensitive("images/s-excel-i.bmp").
btAutoApply     :load-image ("images/table_relationship.bmp").              
btAutoApply     :load-image-insensitive("images/table_relationship-i.bmp").
bFileParamLp    :load-image("images/s-lookup.bmp").
bFileParamLp    :load-image-insensitive("images/s-lookup-i.bmp").
bAgentLookup    :load-image("images/s-lookup.bmp").
bAgentLookup    :load-image-insensitive("images/s-lookup-i.bmp").
bRefLookup      :load-image("images/s-lookup.bmp").
bRefLookup      :load-image-insensitive("images/s-lookup-i.bmp").
btFilter        :load-image("images/filtererase.bmp").
btFilter        :load-image-insensitive("images/filtererase-i.bmp").
bAgentLk        :load-image("images/s-lock.bmp").
bAgentLk        :load-image-insensitive("images/s-lock-i.bmp").
bReferenceLk    :load-image("images/s-lock.bmp").
bReferenceLk    :load-image-insensitive("images/s-lock-i.bmp").
btAutoUnApply   :load-image ("images/table_relationship_erase.bmp").              
btAutoUnApply   :load-image-insensitive("images/table_relationship_erase-i.bmp").
    
/* Curent User Name */
publish "GetCredentialsName" (output cUserName).
 
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  {&window-name}:window-state = window-minimized.
   
  run enable_UI.
  
  assign
      bAgentLk:hidden        = true
      bReferenceLk:hidden    = true
    /*  flFile:x               = bRefLookup:x + bRefLookup:width-pixels + 2
      btRefGet:x             = flFile:x + flFile:width-pixels + 5   */

      .
      
  /* Populate "cbNotes" combo-box */
  publish "getSysCodeListItemPair" (input "ARNoteType",
                                    output cCodeList,
                                    output std-lo,
                                    output std-ch). 
                                                                                                            
  cbNotes:list-item-pairs in frame {&frame-name} = {&selectcombo} + "," + {&selectcombo} + if cCodeList > "" then "," + cCodeList else "".
    
  resetScreen().
  
  /* Get default StateID and AgentID */
  run getDefaultValues in this-procedure. 

  /* Run screen when lanched from other screen using params */
  run launchScreenOnParams in this-procedure.
  
  /* change reference button tooltip to payment/credit when selecting from 'Type' combo */
  run changeRefTooltip in this-procedure.
  
  {lib/get-column-width.i &col="'file'" &var=dColumnWidth}
  
  /* Restore the current window after fetching data */
  run showWindow in this-procedure.  
  
  apply 'entry' to flAgentID.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adjustTotals C-Win 
PROCEDURE adjustTotals :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* Setting position of totals widget */
  assign       
      flTotal:y      = brwartran:y + brwArtran:height-pixels + 0.01
      fFileAmt:y     = brwartran:y + brwartran:height-pixels + 0.01
      fRemainAmt:y   = brwartran:y + brwartran:height-pixels + 0.01
      flTotal:x      = brwartran:x + brwartran:get-browse-column(1):x
      fFileAmt:x     = brwartran:x + brwartran:get-browse-column(4):x + (brwartran:get-browse-column(4):width-pixels - fFileAmt:width-pixels) + 6.8
      fRemainAmt:x   = brwartran:x + brwartran:get-browse-column(6):x + (brwartran:get-browse-column(6):width-pixels - fRemainAmt:width-pixels) + 6.8
      no-error.  
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE applyAmount C-Win 
PROCEDURE applyAmount :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Buffer Definition */
  define buffer artran   for artran.
  define buffer tartran  for tartran.
  define buffer ttartran for ttartran.
  
  empty temp-table tartran.
  
  do with frame {&frame-name}:
  end.
  
  if flPostingDate:input-value = ?
   then
    do:
      message "Post date cannot be blank."
          view-as alert-box error buttons ok.
      apply 'entry' to flPostingDate.  
      return.      
    end.
  
  if flPostingDate:input-value > today
   then
    do:
      message "Post date cannot be in future."
          view-as alert-box error buttons ok.
      apply 'entry' to flPostingDate.     
      return.
    end.
  
  publish "validatePostingDate" (input flPostingDate:input-value,
                                 output std-lo).
  
  if not std-lo
   then
    do:
      message "Apply post date must be within an open period."
          view-as alert-box error buttons ok.
      apply 'entry' to flPostingDate.     
      return.
    end.
  
  if not available artranref 
   then
    find first artranref where artranref.artranID = iSelectedRef no-error.
     
  if flPostingDate:input-value < date(artranref.trandate)
   then
    do:
      message "Apply post date cannot be prior to the payment post date"
        view-as alert-box error buttons ok.
      apply 'entry' to flPostingDate.    
      return.    
    end.
  
  for each tempartran :
    if not can-find (first artran where artran.artranID = tempartran.artranID)
     then
      do:
        create artran.
        buffer-copy tempartran to artran.
      end.
  end.
  
  for each artran:
    find first ttartran where ttartran.artranID = artran.artranID no-error.
    if available ttartran
     then
      do:
        buffer-compare ttartran using ttartran.selectrecord ttartran.remainingamt ttartran.appliedamt ttartran.appliedamtbyref to artran
        save result in std-lo.
        if not std-lo                 
         then
          do:
            if flPostingDate:input-value < date(ttartran.trandate)
             then
              do:
                message "Apply post date cannot be prior to the " + getTranType(ttartran.type) +
                        " (" +  ttartran.filenumber  + ") " + " post date"
                  view-as alert-box error buttons ok.
                apply 'entry' to flPostingDate.
                return.
              end.
            create tartran.
            buffer-copy artran except artran.revenuetype to tartran.                   
          end.
      end.  
  end.
  
  run server/applyproductionpayment.p (input iSelectedRef,              /* Payment/Credit ArtanID */
                                       input flPostingdate:input-value, /* Posting Date */
                                       input true,                      /* Validate */
                                       input table tartran,                             
                                       output std-lo,
                                       output std-ch).
 
  if not std-lo
   then
    do:
      if std-ch <> {&RemoveDistribution}
       then
        do:
          message std-ch 
              view-as alert-box error buttons ok.
          return.
        end.
 
       message "Since payment has been distributed on this file. Any change to the applied payment "
                  "will reset the distribution for this file. " 
             skip "Are you sure you want to continue?"                 
          view-as alert-box question buttons yes-no update std-lo.
      
       if not std-lo /* if dont want to change the status */
        then return.
    
       run server/applyproductionpayment.p (input iSelectedRef,              /* Payment/Credit ArtanID */
                                            input flPostingdate:input-value, /* Posting Date */
                                            input false,                     /* Validate */
                                            input table tartran,                                   
                                            output std-lo,
                                            output std-ch).  
     end.

  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
  
  /* Clears the screen */
  close query brwartran.
  empty temp-table artran.
  
  message "Select another payment for the chosen agent combination?"                 
      view-as alert-box question buttons yes-no update std-lo.
  if std-lo 
   then
    do:
      /* Updating orginal files and misc invoices record after successfully applying/unapplying payment/credit */ 
      for each tartran:    
        find first ttartran where ttartran.artranID = tartran.artranID no-error.
         if available ttartran
          then
           do:
             buffer-copy tartran except tartran.selectrecord tartran.trandate tartran.revenuetype  to ttartran. 
             assign      
                 ttartran.appliedamtbyref = if ttartran.appliedamtbyref < 0 then absolute(ttartran.appliedamtbyref) else ttartran.appliedamtbyref
                 ttartran.isRefapplied    = ttartran.appliedamtbyref > 0
                  .          
           end.  
      end.
      lGetRefData = false.
      
      /* Clears the payment/credit and cancel applying */
      run cancelReference in this-procedure.
      
      btRefGet:load-image ("images/completed.bmp").
      btRefGet:load-image-insensitive("images/completed-i.bmp").
    end.
   else 
    do:
      lGetAgentData = false.
      
      /* cancel agent */
      run cancelAgent in this-procedure.
      
      btGetDataOnAgent:load-image ("images/completed.bmp").
      btGetDataOnAgent:load-image-insensitive("images/completed-i.bmp").
    end.
    
  run enableDisableWidgets in this-procedure.    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE applyAmtOnSelect C-Win 
PROCEDURE applyAmtOnSelect :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available artran
   then return.
  
  do with frame {&frame-name}:
  end.
  
  find first ttartran where ttartran.artranID = artran.artranID no-error.
  if available ttartran 
   then
    /* Retain original value of applied amount before any changes */ 
    deOldAppliedAmt = ttartran.appliedamtbyref. 
  
  artran.isRefapplied = not artran.isRefapplied.   
     
  if artran.isRefapplied 
   then
    run applyDefaultTranAmt in this-procedure.      
   else
    run resetReference in this-procedure.      
  
  assign
      lDataChanged        = true
      .
  if artran.isRefapplied and artran.appliedamtbyref = deOldAppliedAmt
   then
    assign
        artran.Stat = "Applied"
        artran.selectrecord = false
        .
   else if artran.appliedamtbyref <> deOldAppliedAmt
    then
     assign
         artran.Stat = "Pending"
         artran.selectrecord = true
         .
    else
     assign 
         artran.stat = ""
         artran.selectrecord = false
         .
             
  run enableDisableWidgets in this-procedure.
  
  apply 'value-changed' to browse brwartran.
  
  if not can-find (first tempartran where tempartran.artranID = artran.artranID)    
   then
    do:
      create tempartran.
      buffer-copy artran to tempartran.
    end.
   else
    for first tempartran where tempartran.artranID = artran.artranID:
      buffer-copy artran to tempartran.
    end.
   run updateTotalRemainingAmt in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE applyDefaultTranAmt C-Win 
PROCEDURE applyDefaultTranAmt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  if not available artran
   then return.
  
  do with frame {&frame-name}:
  end.
  
  if not available artranref
   then
    find first artranref
    where artranref.artranID = iSelectedRef no-error.
  
  if available artranref
   then
    do:
      if artran.remainingamt    > 0 and
         artranref.remainingamt > 0
       then
        do:
          artran.appliedamtbyref = if artran.remainingamt >= artranref.remainingamt then artranref.remainingamt 
                                      else artran.remainingamt.        
        end.                           
       else
        artran.appliedamtbyref = deTempOldAppliedAmt.
    
      assign        
          artran.appliedamt           = artran.appliedamt + artran.appliedamtbyref                              
          artran.remainingamt         = artran.remainingamt - artran.appliedamtbyref
          artran.fullypaid            = artran.remainingamt = 0
          artran.selectrecord         = not artran.appliedamtbyref = deOldAppliedAmt
          artran.stat                 = if artran.isRefapplied and artran.appliedamtbyref = deOldAppliedAmt then "Applied" else if artran.appliedamtbyref <> deOldAppliedAmt then "Pending" else "" 
          artran.isRefapplied         = artran.appliedamtbyref > 0
          artranref.appliedamt        = artranref.appliedamt + artran.appliedamtbyref
          artranref.remainingamt      = artranref.tranamt - artranref.appliedamt           
          flAppliedAmt:screen-value   = string(artranref.appliedamt)
          flUnappliedAmt:screen-value = string(artranref.remainingamt)                               
          . 
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE autoApply C-Win 
PROCEDURE autoApply :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  define buffer ttartran for ttartran.
  
  do with frame {&frame-name}:
  end.
  
  if flReference:input-value = ""
   then return.
  
  for each artran:
    
    deTempOldAppliedAmt = artran.appliedamtbyref.
    
    find first ttartran where ttartran.artranID = artran.artranID no-error.
    if available ttartran 
     then
      /* Retain original value of applied amount before any changes */
      deOldAppliedAmt = ttartran.appliedamtbyref.       
    
    /* Reset payment/credit values to the initial values when selected file/misc invoice is not applied */
    run resetReference in this-procedure.
    
    run applyDefaultTranAmt in this-procedure.
    lAuto = true.
  end.
  
  run enableDisableWidgets in this-procedure. 
  
  open query brwartran for each artran.
  
  apply 'value-changed' to browse brwartran.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE autoUnApply C-Win 
PROCEDURE autoUnApply :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/   
  define buffer ttartran for ttartran.
  
  do with frame {&frame-name}:
  end.
  
  if flReference:input-value = ""
   then 
    return.
    
  deTempOldAppliedAmt = 0.
  deOldAppliedAmt = 0.
  
  for each artran 
    where artran.isRefapplied
      and artran.stat = "Applied":
      
    for first artranref
      where artranref.artranID = iSelectedRef:
          
      assign  
          artranref.appliedamt        = artranref.appliedamt   - artran.appliedamtbyref
          artranref.remainingamt      = artranref.remainingamt + artran.appliedamtbyref           
          flAppliedAmt:screen-value   = string(artranref.appliedamt)
          flUnappliedAmt:screen-value = string(artranref.remainingamt)
          .
      assign        
          artran.appliedamt           = artran.appliedamt - artran.appliedamtbyref                              
          artran.remainingamt         = artran.remainingamt + artran.appliedamtbyref
          artran.fullypaid            = artran.remainingamt = 0
          artran.selectrecord         = true
          artran.stat                 = "Pending" 
          artran.appliedamtbyref      = 0
          artran.isRefapplied         = artran.appliedamtbyref > 0
          .
          
      lAuto = true. 
      
    end. /* for first artranref */
    
  end. /* for each artran */
  
  run enableDisableWidgets in this-procedure. 
  
  open query brwartran for each artran.
  
  apply 'value-changed' to browse brwartran.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancelAgent C-Win 
PROCEDURE cancelAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end.
  
  if can-find (first artran where artran.selectrecord)
   then
    do:
      message "Changes are pending to save. Want to continue?"                      
          view-as alert-box warning buttons yes-no update std-lo.
      
      if not std-lo /* if dont want to change the status */
       then return.
    end.
    
  if can-find (first arbatch)
   then
    empty temp-table arbatch.
    
  assign
      browse brwartran:sensitive         = false            
             btRefGet:sensitive          = false
             bRefLookup:sensitive        = false
             bAgentLookup:sensitive      = true
             flAgentID:sensitive         = true                                      
             bFileParamLp:sensitive      = true                     
             flFileParam:screen-value    = ""
             cInvoiceID                  = ""
             iBatchID                    = 0
             cFileNum                    = ""
             .
  
  assign       
      fFileAmt :format  = getFormattedNumber(input 0, input fFileAmt:handle)
      fRemainAmt:format = getFormattedNumber(input 0, input fRemainAmt:handle)
      fFileAmt :screen-value  = string(0)
      fRemainAmt:screen-value = string(0)
      no-error.
      
  run UnlockTransaction in this-procedure (input flAgentID:screen-value,
                                           input cRefType,
                                           input iSelectedRef).
  
  /* Clears all data from screen */
  run clearScreen in this-procedure.

  lGetRefData = false.
      
  /* Clears the payment/credit and cancel applying */
  run cancelReference in this-procedure.
  
  btRefGet:load-image ("images/completed.bmp").
  btRefGet:load-image-insensitive("images/completed-i.bmp").
  
  assign
      bRefLookup:sensitive  = false
      .
  
  /* Clears the payment/credit header section */
  run emptyRefHeader in this-procedure.  
  
  apply 'value-changed' to browse brwartran.
  
  empty temp-table tempartran.
  lAuto = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancelReference C-Win 
PROCEDURE cancelReference :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end.
  
  define variable lAppliedamtbyref as logical no-undo.
  
  if can-find (first artran where artran.selectrecord)
   then
    do:
      message "Changes are pending to save. Want to continue?"                      
          view-as alert-box warning buttons yes-no update std-lo.
      
      if not std-lo /* if dont want to change the status */
       then return.
    end.

  assign       
      bRefLookup:sensitive   = true
      .  
    
  run UnlockTransaction in this-procedure (input flAgentID:screen-value,
                                           input cRefType,
                                           input iSelectedRef).
    
    /* Clears payment related applied data from screen */
    run changeReference in this-procedure. 
  
  /* Clears all payment data */
  run clearReferenceData in this-procedure.
      
  /* Clears the payment/credit header section */
  run emptyRefHeader in this-procedure.
  
  /* Enable Get button only when Check is selected */
  btRefGet:sensitive = (flReference:input-value <> "").
  
  if lAppliedamtbyref
   then
    brwartran:get-browse-column(7):READ-ONLY = false.  
   else
    brwartran:get-browse-column(7):READ-ONLY = query brwartran:num-results > 0 and flReference:input-value = "".

  empty temp-table tempartran.
  lAuto = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changeReference C-Win 
PROCEDURE changeReference :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Rebuilding original temp-table */
  for each ttartran:
    if ttartran.appliedAmtByRef > 0 and 
       ttartran.remainingAmt <= 0
     then delete ttartran.
     else
      assign
          ttartran.appliedAmtByRef = 0
          ttartran.isRefapplied    = ttartran.appliedamtbyref > 0
          ttartran.stat            = (if ttartran.isRefapplied then "Applied" else "")
          .     
  end.
    
  resetScreen().
  
  resultsChanged(false).

  /* Enable Get button only when Check is selected */
  btRefGet:sensitive = (flReference:input-value <> "").
    
  /* Sets payment/credit details on the screen */
  run setReferenceDetails in this-procedure.
    
  close query brwartran.
  empty temp-table artran.
    
  for each ttartran by ttartran.isRefapplied desc by ttartran.type by ttartran.latestBatch: 
           
    create artran.
    buffer-copy ttartran to artran. 
  end.

  open query brwartran preselect each artran.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changeRefTooltip C-Win 
PROCEDURE changeRefTooltip :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  bRefLookup:tooltip =  "Select payment" .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clearReferenceData C-Win 
PROCEDURE clearReferenceData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table artranref.  
  empty temp-table ttartranref.
  
  do with frame {&frame-name}:
  end.
  
  assign
      iSelectedRef             = 0
      cFileNumber              = "" 
      flFile:screen-value      = ""
      flReference:screen-value = ""
      flOther:screen-value     = ""
      .
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clearScreen C-Win 
PROCEDURE clearScreen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  empty temp-table artran.
  empty temp-table ttartran.
  empty temp-table tartran.
  
  /* Empties the browser when state is changed */ 
  run emptyBrowser in this-procedure. 
  
  resetScreen().
  
  resultsChanged(false).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if can-find (first artran where artran.selectrecord)
   then
    do:
      message "Changes are pending to save. Want to continue?"                      
          view-as alert-box warning buttons yes-no update std-lo.
      
      if not std-lo /* if dont want to change the status */
       then return.
    end.

   
  run UnlockTransaction in this-procedure (input flAgentID:screen-value,
                                           input cRefType,
                                           input iSelectedRef).
  
  publish "WindowClosed" (input this-procedure).  
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE distributeAmount C-Win 
PROCEDURE distributeAmount :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  if not available artran 
   then return.
  
  do with frame {&frame-name}:
  end.
  
  run dialogamountdistribute.w (input artran.artranID,
                                input artran.filenumber,
                                input artran.fileID,
                                input artran.entityID,
                                input artran.entityname,
                                output std-lo).

  if not std-lo 
   then return.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE emptyBrowser C-Win 
PROCEDURE emptyBrowser :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Empties the browser when parameters changed */ 
  close query brwartran.
  empty temp-table artran.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE emptyRefHeader C-Win 
PROCEDURE emptyRefHeader :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  assign        
      flTotalamt:screen-value     = ""
      flAppliedAmt:screen-value   = ""
      flUnappliedAmt:screen-value = ""
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableWidgets C-Win 
PROCEDURE enableDisableWidgets :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if can-find(first artran where artran.selectrecord)
   then
    assign        
        btPost:sensitive        = flReference:input-value <> ""
        btReset:sensitive       = flReference:input-value <> ""       
        flPostingdate:sensitive = flReference:input-value <> ""       
        .     
   else
    assign
        btPost:sensitive        = false
        btReset:sensitive       = false        
        flPostingdate:sensitive = false
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flAgentID flFileParam flFileNo flPostingdate flFile cbNotes edNotes 
          flReference flName flTotalamt flAppliedAmt flUnappliedAmt flNote 
          flTotal fFileAmt fRemainAmt flOther 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bReferenceLk flAgentID bAgentLookup bAgentLk btExport flFileNo 
         flPostingdate brwartran flTotalamt flAppliedAmt flUnappliedAmt flNote 
         flOther RECT-78 RECT-84 RECT-85 RECT-87 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer artran for artran.
  
  empty temp-table tartran.
  
  if query brwartran:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
   
  for each artran:
    create tartran.
    buffer-copy artran except artran.type artran.transtype artran.entity to tartran.
    assign
        tartran.type       = getTranType(artran.type)
        tartran.transtype  = getTranType(artran.transtype)
        tartran.entity     = getEntityType(artran.entity)
        .
  end.
 
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table tartran:handle.
  run util/exporttable.p (table-handle std-ha,
                          "tartran",
                          "for each tartran ",
                          "revenuetype,tranID,entity,entityid,entityname,filenumber,fileID,type,sourceID,artranID,trandate,duedate,tranamt,remainingamt,appliedamt,reference,void,voiddate,voidby,notes,createddate,createdby,fullypaid,transtype,postdate,postby,accumbalance,appliedamtbyref,selectrecord,username,arnotes",
                          "Revenue Type,Invoice ID,Entity,Entity ID,Entity Name,File Number,File ID,Type,Source ID,Artran ID,Transaction Date,Due Date,Transaction Amount,Remaining Amount,Applied Amount,Reference,Void,Void Date,Void By,Notes,Created Date,Created By,Fullypaid,Trans Type,Batch Date,Post By,Accum Balance,Applied Amt by Ref.,Select Record,Username,Notes",
                          std-ch,
                          "Apply-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  define variable cFileID    as character no-undo.
  define variable cType      as character no-undo.
  define variable dcFileAmt  as decimal   no-undo.
  
  do with frame {&frame-name}:
  end.

  close query brwartran.
  empty temp-table artran.
  
  define buffer ttartran for ttartran.

  cFileID = if flFileNo:input-value <> "" then normalizeFileID(input flFileNo:input-value) else "".
    
  for each ttartran   
    where (if cFileID <> "" then ttartran.fileID matches ("*" + cFileID + "*") else true)
      by ttartran.isRefapplied desc by ttartran.type : 
     
    if ttartran.isRefapplied 
     then 
      ttartran.stat = "Applied".
     else
      ttartran.stat = "".
       
    create artran.
    buffer-copy ttartran to artran. 
         
    assign
        dcFileAmt  = dcFileAmt  + ttartran.tranAmt
        .    
  end.

  for each tempartran:
    for first artran 
      where artran.artranID = tempartran.artranID :
      buffer-copy tempartran to artran.
    end.
  end.
  
  open query brwartran preselect each artran.
  
  assign
      cbNotes:sensitive       = query brwartran:num-results > 0 and flReference:input-value <> ""
      edNotes:sensitive       = query brwartran:num-results > 0 and flReference:input-value <> ""
      btAutoApply:sensitive   = query brwartran:num-results > 0 and flReference:input-value <> ""
      btAutoUnApply:sensitive = query brwartran:num-results > 0 and flReference:input-value <> ""
      brwartran:get-browse-column(7):READ-ONLY = query brwartran:num-results > 0 and flReference:input-value = ""
      .
      
  assign       
      fFileAmt :format = getFormattedNumber(input dcFileAmt,  input fFileAmt:handle)  
      no-error.
  
  assign
      fFileAmt :screen-value = string(dcFileAmt)
      .
  
  if can-find (first ttartran where ttartran.isRefapplied)
   then
    btAutoUnapply:sensitive = true.
   else
    btAutoUnapply:sensitive = false.
  
  run updateTotalRemainingAmt in this-procedure.

  /* Display no. of records on status bar */
  setStatusCount(query brwartran:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getAgentData C-Win 
PROCEDURE getAgentData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if flAgentID:input-value = ""
   then return.
   
  assign               
      flFileNo:screen-value      = ""      
      bAgentLookup:sensitive     = false
      flAgentID:sensitive        = false
      flFileParam:sensitive      = false
      bFileParamLp:sensitive     = false      
      btAutoApply:sensitive      = false  
      btAutoUnApply:sensitive    = false  
      btPost:sensitive           = false
      btReset:sensitive          = false
      cbNotes:sensitive          = false
      edNotes:sensitive          = false
      btFilter:sensitive         = false 
      flPostingdate:sensitive    = false
      btRefGet:sensitive         = false             
      bRefLookup:sensitive       = true
      .
  
  run resetNoteCombo in this-procedure.
  
  /* Get AR file/misc invoices transactions records from the server */
  run getTransactions in this-procedure. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getDefaultValues C-Win 
PROCEDURE getDefaultValues :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cAgentID  as character no-undo.
  
  do with frame {&frame-name}:
  end.
  
  /* When default posting option from config screen is allowed */ 
  publish 'GetDefaultPostingOption' (output std-lo).
  if std-lo
   then
    do:
      publish "getDefaultPostingDate" (output dtDefaultPostingDate).    
      flPostingdate:screen-value = string(dtDefaultPostingDate).
    end.
  
  publish "GetAutoDefaultAgent" (output lDefaultAgent).
  
  /* function to set default agentid is switch off now*/  
  lDefaultAgent = false.
  
  if lDefaultAgent
   then
    do:
      /* Get default AgentID */
      publish "GetDefaultAgent" (output cAgentID).
      
      flAgentID:screen-value = cAgentID.
      
      publish "getAgentName" (input flAgentID:input-value,
                              output std-ch,
                              output std-lo).
      
      publish "getAgentStateID" (input flAgentID:input-value,
                                 output cStateID,
                                 output std-lo).
      
      flName:screen-value = std-ch.                              
    end.
    
  selectedagent = cAgentID.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getReferenceData C-Win 
PROCEDURE getReferenceData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if flReference:input-value = "" 
   then return.
    
  run LockTransaction(input flAgentID:screen-value,
                      input iSelectedRef).
  if lLocked 
   then
    do:
      resetScreen().
          
      assign
          bRefLookup:sensitive  = false
          .
          
      /* Building main temp-table */
      find first ttartranref no-error.
      if available ttartranref
       then
        do:
          create artranref.
          buffer-copy ttartranref to artranref.
        end.
          
      /* Sets payment/credit details on the screen */
      run setReferenceDetails in this-procedure.
          
      /* Get AR file/misc invoices transactions records from the server */
      run getTransactions in this-procedure.  
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getTransactions C-Win 
PROCEDURE getTransactions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
      
  /* Buffer Definition */
  define buffer tartran for tartran.
  define var icount as int no-undo.
  if flAgentID:input-value = ""
   then
    do:
      message "Please select valid agent first" 
          view-as alert-box error buttons ok.   
      return.
    end.
    
  pause 0.1.

  run server/getopenproductionfile.p (input flAgentID:input-value,                    /* EntityID */                                   
                                      input normalizeFileID(flFileParam:input-value), /* Normalised file number */
                                      input iSelectedRef,
                                      output table tartran,
                                      output std-lo,
                                      output std-ch).   
                                    
  lGet = std-lo.
  
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
  
  /* Rebuilding original temp-table */
  for each tartran:
         
    if not can-find(first ttartran where ttartran.artranID = tartran.artranID or (ttartran.entityID = tartran.entityID and ttartran.fileID = tartran.fileID))
     then
      do:
        create ttartran.
        buffer-copy tartran except tartran.isRefapplied tartran.appliedamtbyref tartran.netProcessed to ttartran.
         
        assign
            ttartran.appliedamtbyref = if tartran.appliedamtbyref < 0 then absolute(tartran.appliedamtbyref) else tartran.appliedamtbyref
            ttartran.isRefapplied    = ttartran.appliedamtbyref > 0        
               . 
            /* ttartran.latestBatch = ttartran.reference.   */
      end. /* if not can-find(first ttartran...*/
     else if flReference:input-value <> ""
      then
       for first ttartran where ttartran.artranID = tartran.artranID or (ttartran.entityID = tartran.entityID and ttartran.fileID = tartran.fileID):
         assign
             ttartran.appliedamtbyref = if tartran.appliedamtbyref < 0 then absolute(tartran.appliedamtbyref) else tartran.appliedamtbyref     
             ttartran.isRefapplied    = ttartran.appliedamtbyref > 0
                .  
       end.
  end. /* for each tartran */
  
  /* Auto applying file filter on file records when reference has file number */
  if flReference:input-value <> "" and can-find(first ttartran where ttartran.fileID = normalizeFileID(cFileNumber))
   then flFileNo:screen-value = cFileNumber.
  
  /* This will use the screen-value of the filters which is ALL */
  run filterData in this-procedure.
    
  /* Makes widget enable-disable based on the data */ 
  if query brwartran:num-results > 0 
   then
    assign 
        browse brwartran:sensitive        = true                               
               flFileNo:sensitive         = true                 
               btExport:sensitive         = true               
               menu-item m_export:sensitive in menu POPUP-MENU-brwartran = true
               .      
   else
    assign 
        browse brwartran:sensitive        = false               
               flFileNo:sensitive         = false               
               btReset:sensitive          = false               
               btExport:sensitive         = false                              
               menu-item m_export:sensitive in menu POPUP-MENU-brwartran = false
               .
  
  /* Enable/disable filter button based on the filter value */
  run setFilterButton in this-procedure.
  
  run enableDisableWidgets in this-procedure. 
  
  apply 'value-changed' to browse brwartran.
  
  /* Display no. of records with date and time on status bar */
  setStatusRecords(query brwartran:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE launchScreenOnParams C-Win 
PROCEDURE launchScreenOnParams :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  define variable cParamList   as character no-undo.
  define variable iArtranRefID as integer   no-undo.
    
  do with frame {&frame-name}:
  end.

  /* Launch screen data based on the parameter list */
  publish "GetCurrentValue" ("ApplyParams", output cParamList).
  
  if cParamList = ""
   then return.
  
  assign
      flAgentID:screen-value  = entry(1,cParamList,"|")
      cRefType                = "P"
      iArtranRefID            = integer(entry(3,cParamList,"|")) no-error      
      .
  
  if flAgentID:input-value = "" or iArtranRefID = 0
   then return.
  
  publish "getAgentName" (input flAgentID:input-value,
                          output std-ch,
                          output std-lo).
  
  if not std-lo 
   then 
    do:
      assign 
          flAgentID:screen-value = "" 
          flName:screen-value    = ""
          .
       return. 
    end.
  
  flName:screen-value = std-ch.
  
  publish "getAgentStateID" (input flAgentID:input-value,
                             output cStateID,
                             output std-lo).
        
  selectedagent = flAgentID:input-value.
  
  apply 'choose' to btGetDataOnAgent.
  
  /* Get Payment/Credit record from the server*/
  run server/getpostedtransactions.p (input  iArtranRefID, /* arTranID */
                                      input  "",           /* Entity */ 
                                      input  "",           /* Entity ID */
                                      input  "",           /* Type: (C)redit,(D)ebit,(F)ile */
                                      input  "",           /* Search String */
                                      input  false,        /* Include All */
                                      input  ?,            /* FromPostDate */
                                      input  ?,            /* ToPostDate */
                                      output table artranref,
                                      output std-lo,
                                      output std-ch).                                          
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box info buttons ok.
      return.
    end.
    
  std-ch = "". 
  
  empty temp-table ttartranref.
  
  find first artranref no-error.
  if not available artranref
   then return.
   
  artranref.appliedamt = artranref.tranamt - artranref.remainingamt.
  
  /* Temp table to store original values before any operation */    
  create ttartranref.
  buffer-copy artranref to ttartranref.
  
  /* Used to apply filter based on file number of selected reference */
  assign
      cFileNumber              = artranref.fileNumber
      flReference:screen-value = artranref.reference
      .
  
  assign
      btRefGet:sensitive         = (flReference:input-value <> "")
      iSelectedRef               = artranref.artranID
      flReference:screen-value   = artranref.reference            
      flAgentID:sensitive        = false
      bAgentLookup:sensitive     = false
      bRefLookup:sensitive       = false
      .
  
  /* Sets payment/credit details on the screen */
  run setReferenceDetails in this-procedure.
  
  /* Get artran file/misc invoice type records */ 
  apply 'choose' to btRefGet.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LockTransaction C-Win 
PROCEDURE LockTransaction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcAgentID     as character.
  define input parameter ipcReferenceID as character.
  
  define variable ipcLockedByUser as character.
  define variable lAgentLock      as logical.
  define variable lRefLock        as logical.
  
  do with frame {&frame-name}:
  end.
 
  if lLocked 
   then
    return.
    
  if ipcAgentID = "" or ipcAgentID = "?" or ipcAgentID = ? or ipcAgentID = {&ALL} or
     ipcReferenceID = "" or ipcReferenceID = "?" or ipcReferenceID = ? or ipcReferenceID = {&selectcombo}
   then
    return.
    
    run server\locktransaction.p( input  {&ArAgent},
                                  input  ipcAgentID, 
                                  output ipcLockedByUser,
                                  output lAgentLock,
                                  output std-ch).
    if ipcLockedByUser > ""
     then
      do:
         message "Agent is locked by " + ipcLockedByUser + "."
           view-as alert-box info buttons ok.
         lAgentLock = false.
         return no-apply.
      end.
     else if not lAgentLock
      then
       do:
         message std-ch
           view-as alert-box info buttons ok.
         return no-apply.
       end.
       
    if lAgentLock
     then
      run server\locktransaction.p(input  {&ArPayment} , /* (P)ayment or (C)redit */
                                   input  ipcReferenceID, 
                                   output ipcLockedByUser,
                                   output lRefLock,
                                   output std-ch).
               
    if lAgentLock  
     then
      do:
        bAgentLk:hidden = false.
    
        if lRefLock
         then
          assign
              lLocked              = true
              bReferenceLk:hidden  = false
              bReferenceLk:tooltip = "Payment is locked"
            /*  flFile:x             =  bReferenceLk:x + bReferenceLk:width-pixels + 2
              btRefGet:x           = flFile:x + flFile:width-pixels + 5   */
              .        
      end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshBrowserData C-Win 
PROCEDURE refreshBrowserData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable selectedRow as integer no-undo.
  
  do with frame {&frame-name}:
  end.
  
  std-ro = if available artran then rowid(artran) else ?.
  
  do selectedRow = 1 TO brwartran:num-iterations: 
    if brwartran:is-row-selected(selectedRow) then leave. 
  end.
    
  open query brwartran for each artran.
  
  {&browse-name}:set-repositioned-row(selectedRow) no-error.
  reposition {&browse-name} to rowid std-ro no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resetFilterButton C-Win 
PROCEDURE resetFilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
      
  assign
      flFileNo:sensitive     = true 
      .
     
  /* Reset filter widgets */
  assign
      flFileNo:screen-value   = "" 
      . 
     
  /* Show data based on the default values on the screen */
  run filterData in this-procedure. 
  
  /* Sets payment/credit details on the screen */
  if lAuto 
   then
    do:
      run setReferenceDetails in this-procedure.
      lAuto = false.
    end.
  
  run enableDisableWidgets in this-procedure.
  
  /* Disable reset filter button when filters reset to ALL */
  run setFilterButton in this-procedure.         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resetNoteCombo C-Win 
PROCEDURE resetNoteCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  assign
      cbNotes:screen-value  = {&selectcombo}       
      edNotes:screen-value  = "" 
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resetReference C-Win 
PROCEDURE resetReference :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end.
  
  for first artranref
    where artranref.artranID = iSelectedRef:
          
    assign           
        artranref.appliedamt        = if artranref.appliedamt > 0 then artranref.appliedamt - artran.appliedamtbyref 
                                      else artranref.appliedamt
        artranref.remainingamt      = artranref.tranamt - (artranref.appliedamt +  artranref.accumbalance)         
        flAppliedAmt:screen-value   = string(artranref.appliedamt)
        flUnappliedAmt:screen-value = string(artranref.remainingamt)
        .
  end.
  assign           
      artran.remainingamt    = artran.remainingamt + artran.appliedamtbyref
      artran.appliedamt      = artran.appliedamt - artran.appliedamtbyref 
      artran.appliedamtbyref = 0
      artran.fullypaid       = false
      .

  for first tempartran where tempartran.artranID = artran.artranID:
    delete tempartran.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterButton C-Win 
PROCEDURE setFilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    btFilter:sensitive = not (flFileNo:screen-value = "").
                              
    
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setNoteCombo C-Win 
PROCEDURE setNoteCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/    
  do with frame {&frame-name}:
  end.
  
  if available artran        and 
     artran.tempnote <> ""   and
     lookup(artran.notetype,cbNotes:list-item-pairs) > 0 
   then
    assign
        cbNotes:screen-value = artran.notetype
        edNotes:screen-value = artran.tempnote        
        .       
   else
    /* Reset Note Type combo-box */
    run resetNoteCombo in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setNoteField C-Win 
PROCEDURE setNoteField :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cOldNote as character no-undo.
  define variable cNote    as character no-undo.
  
  define buffer ttartran for ttartran. 
  
  do with frame {&frame-name}:
  end.
  
  if not available artran
   then return.
    
  find first ttartran where ttartran.artranID = artran.artranID no-error.
  if available ttartran 
   then
    /* Retain original value of arNotes before any changes */ 
    cOldNote = ttartran.arNotes.
  
  assign
      /* build the note */
      cNote           = if edNotes:screen-value > "" then "[  " + string(now,"99/99/9999 HH:MM AM  ")
                                                                + "|  " + caps(cUserName) + "  ]"                            
                                                                + chr(10) + chr(10)
                                                                + edNotes:screen-value + chr(10) + chr(10)
                        else ""  
      artran.notetype = if artran.notetype > "" then artran.notetype else {&selectcombo}
      artran.tempnote = edNotes:screen-value       
      artran.arNotes  = cOldNote + cNote
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setReferenceDetails C-Win 
PROCEDURE setReferenceDetails :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer artranref   for artranref.
  define buffer ttartranref for ttartranref.
  
  do with frame {&frame-name}:
  end.
  
  /* Clears the payment/credit header section */
  run emptyRefHeader in this-procedure.
    
  for first artranref
    where artranref.artranID = iSelectedRef:
    
    /* Setting applied and remaining amount from original payment record */
    find first ttartranref where ttartranref.artranID = artranref.artranID no-error.
    if available ttartranref
     then
      assign
          artranref.appliedamt   = ttartranref.appliedamt
          artranref.remainingamt = ttartranref.remainingamt
          .
    assign        
        flTotalamt:screen-value     = string(artranref.tranamt)
        flAppliedAmt:screen-value   = string(artranref.appliedamt)
        flUnappliedAmt:screen-value = string(artranref.remainingamt) 
        flOther:screen-value        = string(artranref.accumbalance)
        .
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showArNotes C-Win 
PROCEDURE showArNotes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttartran for ttartran.
  
  if not available artran 
   then return.
  
  do with frame {&frame-name}:
  end.
  
  find first ttartran where ttartran.artranID = artran.artranID no-error.
  if not available ttartran
   then return.
  
  run dialogviewnote.w (input ttartran.filenumber, /* File Number */
                        input ttartran.entityID,   /* Agent ID */
                        input ttartran.entityname, /* Name */
                        input ttartran.arnotes).   /* AR Notes */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by artran.isRefapplied desc by artran.latestBatch ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE undoAllChanges C-Win 
PROCEDURE undoAllChanges :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttartran for ttartran.
  
  do with frame {&frame-name}:
  end.
  
  /* Sets payment/credit details on the screen */
  run setReferenceDetails in this-procedure.
  
  for each artran:
    find first ttartran where ttartran.artranID = artran.artranID no-error.
    if available ttartran
     then
      do:
        buffer-compare ttartran using ttartran.selectrecord ttartran.remainingamt ttartran.appliedamt ttartran.appliedamtbyref to artran
        save result in std-lo.
        if not std-lo
         then          
          buffer-copy ttartran except ttartran.arnotes ttartran.latestBatch ttartran.netProcessed to artran.
        
        assign
            artran.arnotes  = ttartran.arnotes
            artran.notetype = ""
            artran.tempnote = ""
            .

      end.  
  end. 
  
  lAuto = false.
  
  empty temp-table tempartran.
  
  run enableDisableWidgets in this-procedure. 
  
  open query brwartran for each artran.
  
  apply 'value-changed' to browse brwartran.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE UnlockTransaction C-Win 
PROCEDURE UnlockTransaction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcAgentID      as character.
  define input parameter ipcRefType      as character.  
  define input parameter ipcReferenceID  as character.
 
  define variable lAgentUnLock as logical.
  define variable lRefUnLock   as logical.
 
  do with frame {&frame-name} :
  end.
 
  if not lLocked
   then
    return.
 
  ipcRefType = (if ipcRefType = {&Payment} then {&ArPayment} else {&ArCredit}).
 
  if ipcAgentID ne "" and ipcAgentID ne "?" and ipcAgentID ne ? and ipcAgentID ne {&ALL} 
   then
    run server\unlocktransaction.p({&ArAgent},
                                   input ipcAgentID,
                                   output lAgentUnLock,
                                   output std-ch).

 
  if ipcReferenceID ne "" and ipcReferenceID ne "?" and ipcReferenceID ne ? and ipcReferenceID ne {&selectcombo} 
   then
    run server\unlocktransaction.p(ipcRefType, /* (P)ayment or (C)redit */
                                   input ipcReferenceID,
                                   output lRefUnLock,
                                   output std-ch).
 
 /* Hide locking images when unlock is successful */
  if lAgentUnLock or lRefUnLock 
   then
    assign
        lLocked             = false
        bAgentLk:hidden     = if lAgentUnLock then true                                       else bAgentLk:hidden
        bReferenceLk:hidden = if lRefUnLock   then true                                       else bReferenceLk:hidden
     /*   flFile:x            = if lRefUnLock   then bRefLookup:x + bRefLookup:width-pixels + 2 else flFile:x
        btRefGet:x          = flFile:x + flFile:width-pixels + 5  */
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE updateTotalRemainingAmt C-Win 
PROCEDURE updateTotalRemainingAmt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   define buffer artran for artran.
   
   do with frame {&frame-name}:
   end.
   
   assign 
       dcRemainAmt              = 0
       fRemainAmt :screen-value = ""
       .
   
   for each artran:
     dcRemainAmt = dcRemainAmt + artran.remainingAmt.
   end.       
   assign 
       fRemainAmt :format       = getFormattedNumber(input dcRemainAmt,  input fRemainAmt:handle)
       fRemainAmt :screen-value = string(dcRemainAmt)
       .
   run adjustTotals in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE updateTranAmount C-Win 
PROCEDURE updateTranAmount :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  define buffer artranref for artranref.
  
  if not available artran
   then return.
  
  do with frame {&frame-name}:
  end.
    
  /* Reset payment/credit values to the initial values when selected file/misc invoice is not applied */
  run resetReference in this-procedure.
  
  PMT-BLOCK:
  for first artranref
    where artranref.artranID = iSelectedRef:
    
    artran.appliedamtbyref = artran.appliedamtbyref:input-value in browse brwartran.
    
    if artran.appliedamtbyref > artranref.remainingamt 
     then
      do:
         message "Applied amount of the file cannot be more than remaining amount of check/credit" 
             view-as alert-box error buttons ok-cancel.
              
        run applyDefaultTranAmt in this-procedure.
        
        leave PMT-BLOCK.
      end.
        
    if artran.remainingamt <  artran.appliedamtbyref  and
       deOldAppliedAmt     <> artran.appliedamtbyref  and
       deTempOldAppliedAmt <> artran.appliedamtbyref 
     then 
      do:
        message "File applied amount exceeding file remaining amount" 
          view-as alert-box warning buttons ok.
        artran.appliedamtbyref   = artran.remainingamt   .
      end.
           

           
      
    assign                          
        artran.remainingamt         = artran.remainingamt - artran.appliedamtbyref
        artran.appliedamt           = artran.appliedamt + artran.appliedamtbyref
        artran.fullypaid            = artran.remainingamt = 0
        artran.selectrecord         = not artran.appliedamtbyref = deOldAppliedAmt
        artran.stat                 = if artran.isRefapplied and artran.appliedamtbyref = deOldAppliedAmt then "Applied" else if artran.appliedamtbyref <> deOldAppliedAmt then "Pending" else "" 
        artran.isRefapplied         = artran.appliedamtbyref > 0
        artranref.appliedamt        = artranref.appliedamt + artran.appliedamtbyref
        artranref.remainingamt      = artranref.tranamt - ( artranref.appliedamt + artranref.accumbalance ) 
        flAppliedAmt:screen-value   = string(artranref.appliedamt)
        flUnappliedAmt:screen-value = string(artranref.remainingamt)        
        .  
        
    if not can-find (first tempartran where tempartran.artranID = artran.artranID)    
     then
      do:
        create tempartran.
        buffer-copy artran to tempartran.
      end.
     else
      for first tempartran where tempartran.artranID = artran.artranID:
        buffer-copy artran to tempartran.
      end.
     
     
  end. /* for first artranref..*/
    
  run enableDisableWidgets in this-procedure.

  apply 'value-changed' to browse brwartran.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hNoteTypeLabel   as handle no-undo.
  define variable hNoteEditorLabel as handle no-undo.
  
  assign      
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      edNotes:width-pixels                      = frame {&frame-name}:width-pixels  - 284
      hNoteTypeLabel                            = cbNotes:side-label-handle in frame {&frame-name}
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 63
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 77
      btReset:x                                 = {&browse-name}:x + {&browse-name}:width-pixels + 1
      .
  
  assign
      cbNotes:y            = frame {&frame-name}:height-pixels - 47
      edNotes:y            = frame {&frame-name}:height-pixels - 47      
      flNote:y             = frame {&frame-name}:height-pixels - 42
      hNoteTypeLabel:y     = frame {&frame-name}:height-pixels - 47
      .
  assign       
      flTotal:y    = frame {&frame-name}:height-pixels - 72
      fFileAmt:y   = frame {&frame-name}:height-pixels - 72
      fRemainAmt:y = frame {&frame-name}:height-pixels - 72
      fFileAmt:x   = frame {&frame-name}:width-pixels - 497
      fRemainAmt:x = frame {&frame-name}:width-pixels - 397
      no-error.  
                                       
  {lib/resize-column.i &col="'file'" &var=dColumnWidth}
 
  run ShowScrollBars(frame {&frame-name}:handle, no, no).  
  run adjustTotals in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFormattedNumber C-Win 
FUNCTION getFormattedNumber RETURNS CHARACTER
  ( input deTotal as decimal,
    input hWidget as handle) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cNumberFormat  as character no-undo.
  define variable cTotalScrValue as character no-undo.
  define variable deWidgetWidth  as decimal   no-undo.
  
  cTotalScrValue = string(deTotal).
     
  /* account for negative numbers */
  if deTotal < 0
   then 
    cNumberFormat = cNumberFormat + "(".
        
  /* loop through the absolute value of the number cast as an int64 */
  do std-in = length(string(int64(absolute(deTotal)))) to 1 by -1:
    if std-in modulo 3 = 0
     then 
      cNumberFormat = cNumberFormat + (if std-in = length(string(int64(absolute(deTotal)))) then ">" else ",>").
     else 
      cNumberFormat = cNumberFormat + (if std-in = 1 then "Z" else ">").
  end.
     
  /* if the number had a decimal value */
  if index(cTotalScrValue, ".") > 0 
   then 
    cNumberFormat = cNumberFormat + ".99".
         
  /* account for negative numbers */
  if deTotal < 0
   then 
    cNumberFormat = cNumberFormat + ")".
  
  do std-in = 1 to length(cNumberFormat):
    std-ch = substring(cNumberFormat,std-in,1).
    case std-ch:
      when "(" then deWidgetWidth = deWidgetWidth + 5.75.
      when ")" then deWidgetWidth = deWidgetWidth + 5.75.      
      when "Z" then deWidgetWidth = deWidgetWidth + 12.
      when ">" then deWidgetWidth = deWidgetWidth + 8.
      when "," then deWidgetWidth = deWidgetWidth + 4.
      when "." then deWidgetWidth = deWidgetWidth + 4.
      when "9" then deWidgetWidth = deWidgetWidth + 10.
    end case.
  end.
  
  /* set width of the widget as per format */
  hWidget:width-pixels = deWidgetWidth no-error.      
        
  RETURN cNumberFormat.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resetScreen C-Win 
FUNCTION resetScreen RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  do with frame {&frame-name}: 
 
    /* Initialise filters to default values */
    assign
        iBatchID                    = 0              
        flFileNo:screen-value       = ""
        flFileNo:sensitive          = false
        btPost:sensitive            = false
        btReset:sensitive           = false
        flPostingdate:sensitive     = false
        btExport:sensitive          = false
        btFilter:sensitive          = false         
        cbNotes:sensitive           = false
        edNotes:sensitive           = false
        btAutoApply:sensitive       = false
        btAutoUnApply:sensitive     = false
        menu-item m_export:sensitive in menu POPUP-MENU-brwartran = false
        .
    
    run resetNoteCombo in this-procedure.    
  end.
  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage("").
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cName  as character no-undo.
  
  do with frame {&frame-name}:
  end.
  
  if flAgentID:input-value = ""
   then return false. /* Function return value. */

  else if flAgentID:input-value <> ""
   then
    do:
      publish "getAgentName" (input flAgentID:input-value,
                              output cName,
                              output std-lo).
  
      if not std-lo 
       then 
        do:
          assign 
              flAgentID:screen-value     = "" 
              flName:screen-value        = ""
              btGetDataOnAgent:sensitive = false
              .
          return false. /* Function return value. */
        end.
        
      flName:screen-value = cName.
      
      publish "getAgentStateID" (input flAgentID:input-value,
                                 output cStateID, /* Used in revenue Type lookup */
                                 output std-lo). 
                                                              
    end.
  
  assign
      btRefGet:sensitive         = false
      btGetDataOnAgent:sensitive = true
      bFileParamLp:sensitive     = (flAgentID:input-value <> "")
      .
      
  if flAgentID:input-value <> selectedAgent
   then
    do:  
      selectedAgent = flAgentID:input-value.
      
      if lDefaultAgent and flAgentID:input-value <> ""
       then
        /* Set default AgentID */
        publish "SetDefaultAgent" (input flAgentID:input-value).            
    end.
  return true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

