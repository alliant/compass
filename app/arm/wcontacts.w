&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wPersons.w

  Description:Person Maintainance

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Shubham

  Created: 05.14.2020  
  Modifications:
   Date        Name     Comments
   05/05/22   Sachin C  Task# 80191 Added the parameters while editing,deleting 
                        and while creating new system destination.
   05/25/22   Sachin C  Task #92440 Modified tagStat value after
                        newperson created
   09/08/22   Shefali   Task #97315 Modified to fix an error while adding new destination.
   12/12/22   S Chandu  Task #100564 Added deactivate personagent and modified New and modify dialogs.
   12/21/22   Sachin C  Task #100776 Validation and Format on Phone number
   10/06/23   SK        Task #105743 Changed Tool tip of deactivation button
   04/03/24   SB        Task #111689 Modified to use new dialog to create a new person or link existing person to an agent
   04/04/24   SRK       Task #111689 Modified related to update person associated with agent
   04/04/24   S Chandu  Task #111689 Modified fields to contactPhone,contactEmail and  contactMobile.
   05/14/24   Sachin    Task #112772 Fixed Bug in system destination CRUD function in AR Contact screen
------------------------------------------------------------------------*/
create widget-pool.

{tt/person.i}
{tt/person.i &tableAlias="tPerson"}
{tt/person.i &tableAlias="ttPerson"}

{tt/agentcontact.i}
{tt/agentcontact.i &tableAlias="tAgentContact"}

{tt/sysdest.i &tableAlias="data"}
{tt/sysdest.i &tableAlias="origdata"}
{tt/sysdest.i &tableAlias="tempdata"}

define input parameter ipcStateID        as character  no-undo.
define input parameter ipcAgentID        as character  no-undo.
define input parameter ipcAgentName      as character  no-undo.
define input parameter ipcCategory       as character  no-undo.
define input parameter ipcOrgID          as character  no-undo.

define variable dColumnWidth          as decimal    no-undo.
define variable cPersonID             as character  no-undo.

/* track status change to set status in filter*/
define variable cStatusDateTime       as logical    no-undo. 

define variable chTrackName  as character no-undo.
define variable chTrackEmail as character no-undo.
define variable chTrackPhone as character no-undo.
define variable chTrackNotes as character no-undo.
define variable cDestination as character no-undo.

{lib/std-def.i}
{lib/ar-def.i}
{lib/winlaunch.i}

{lib/get-column.i}
{lib/winshowscrollbars.i}
{lib/validEmailList.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES person data

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData person.tagStat person.dispname person.personID getStatDesc(person.personagentActive) @ person.personagentActive person.jobTitle person.contactPhone person.contactEmail   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData person.tagStat   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwData person
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwData person
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData preselect each person
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} preselect each person.
&Scoped-define TABLES-IN-QUERY-brwData person
&Scoped-define FIRST-TABLE-IN-QUERY-brwData person


/* Definitions for BROWSE brwDataDest                                   */
&Scoped-define FIELDS-IN-QUERY-brwDataDest data.actionDesc data.destTypeDesc data.destName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDataDest   
&Scoped-define SELF-NAME brwDataDest
&Scoped-define QUERY-STRING-brwDataDest preselect each data by data.action by data.destType
&Scoped-define OPEN-QUERY-brwDataDest open query {&SELF-NAME} preselect each data by data.action by data.destType.
&Scoped-define TABLES-IN-QUERY-brwDataDest data
&Scoped-define FIRST-TABLE-IN-QUERY-brwDataDest data


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}~
    ~{&OPEN-QUERY-brwDataDest}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-67 fAgent fName bPeopleRefresh brwData ~
bExport bNew bEdit bPersonAgentDeactivate bEmail fPersonName fPhone fEmail ~
bContactEmail eNotes bDestRefresh brwDataDest bDestExport bNewDest ~
bDestEdit bDeleteDest 
&Scoped-Define DISPLAYED-OBJECTS fAgent fName fPersonName fPhone fEmail ~
eNotes 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getStatDesc C-Win 
FUNCTION getStatDesc RETURNS CHARACTER
  ( cStat as LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 4.8 BY 1.14 TOOLTIP "Cancel".

DEFINE BUTTON bContactEmail 
     LABEL "Email" 
     SIZE 4.8 BY 1.14 TOOLTIP "Send email".

DEFINE BUTTON bContactSave 
     LABEL "Save" 
     SIZE 4.8 BY 1.14 TOOLTIP "Save contact person".

DEFINE BUTTON bDeleteDest 
     LABEL "Delete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete destination".

DEFINE BUTTON bDestEdit 
     LABEL "Modify" 
     SIZE 4.8 BY 1.14 TOOLTIP "Modify destination".

DEFINE BUTTON bDestExport 
     LABEL "Export" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bDestRefresh 
     LABEL "Refresh" 
     SIZE 4.8 BY 1.14 TOOLTIP "Reload data".

DEFINE BUTTON bEdit 
     LABEL "Modify" 
     SIZE 4.8 BY 1.14 TOOLTIP "Modify Job Title".

DEFINE BUTTON bEmail 
     LABEL "Email" 
     SIZE 4.8 BY 1.14 TOOLTIP "Send email".

DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bNew 
     LABEL "New" 
     SIZE 4.8 BY 1.14 TOOLTIP "New person".

DEFINE BUTTON bNewDest 
     LABEL "New" 
     SIZE 4.8 BY 1.14 TOOLTIP "New destination".

DEFINE BUTTON bPeopleRefresh 
     LABEL "Refresh" 
     SIZE 4.8 BY 1.14 TOOLTIP "Reload data".

DEFINE BUTTON bPersonAgentDeactivate 
     LABEL "PersonAgentDeactivate" 
     SIZE 4.8 BY 1.14.

DEFINE VARIABLE eNotes AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 124.2 BY 1.91 NO-UNDO.

DEFINE VARIABLE fAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 10.6 BY 1 NO-UNDO.

DEFINE VARIABLE fEmail AS CHARACTER FORMAT "X(256)":U 
     LABEL "Email" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 124.2 BY 1 NO-UNDO.

DEFINE VARIABLE fName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 112 BY 1 NO-UNDO.

DEFINE VARIABLE fPersonName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 124.2 BY 1 NO-UNDO.

DEFINE VARIABLE fPhone AS CHARACTER FORMAT "X(256)":U 
     LABEL "Phone" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 124.2 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-67
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 140.8 BY 6.33.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      person SCROLLING.

DEFINE QUERY brwDataDest FOR 
      data SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      person.tagStat    width 6 view-as toggle-box 
person.dispname          format "x(40)"       label "Name"         width 30
person.personID          format "x(10)"       label "ID"           width 6
getStatDesc(person.personagentActive)                                                 
  @ person.personagentActive         format "x(15)"       label "Status"       width 9 
  
person.jobTitle          format "x(15)"       label "Job Title"    width 15
person.contactPhone             format "x(40)"       label "Phone"        width 20
person.contactEmail             format "x(100)"      label "Email"
enable person.tagStat
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 134.8 BY 7.81
         TITLE "Association People" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwDataDest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDataDest C-Win _FREEFORM
  QUERY brwDataDest DISPLAY
      data.actionDesc width 20
  data.destTypeDesc width 15
  data.destName format "x(100)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 134.8 BY 5.81
         TITLE "Output Destinations" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     fAgent AT ROW 1.48 COL 17.4 COLON-ALIGNED WIDGET-ID 66 NO-TAB-STOP 
     fName AT ROW 1.48 COL 29 COLON-ALIGNED NO-LABEL WIDGET-ID 88 NO-TAB-STOP 
     bPeopleRefresh AT ROW 2.91 COL 2.4 WIDGET-ID 82 NO-TAB-STOP 
     brwData AT ROW 2.95 COL 142.2 RIGHT-ALIGNED WIDGET-ID 200
     bExport AT ROW 4.05 COL 2.4 WIDGET-ID 80 NO-TAB-STOP 
     bNew AT ROW 5.19 COL 2.4 WIDGET-ID 84 NO-TAB-STOP 
     bEdit AT ROW 6.33 COL 2.4 WIDGET-ID 78 NO-TAB-STOP 
     bPersonAgentDeactivate AT ROW 7.52 COL 2.4 WIDGET-ID 410
     bEmail AT ROW 8.67 COL 2.4 WIDGET-ID 86 NO-TAB-STOP 
     bContactSave AT ROW 12 COL 4 WIDGET-ID 102 NO-TAB-STOP 
     fPersonName AT ROW 12.05 COL 14.8 COLON-ALIGNED WIDGET-ID 94
     fPhone AT ROW 13.1 COL 14.8 COLON-ALIGNED WIDGET-ID 98
     bCancel AT ROW 13.14 COL 4 WIDGET-ID 110 NO-TAB-STOP 
     fEmail AT ROW 14.14 COL 14.8 COLON-ALIGNED WIDGET-ID 96
     bContactEmail AT ROW 14.29 COL 4 WIDGET-ID 116 NO-TAB-STOP 
     eNotes AT ROW 15.19 COL 16.8 NO-LABEL WIDGET-ID 112
     bDestRefresh AT ROW 18.24 COL 2.4 WIDGET-ID 16 NO-TAB-STOP 
     brwDataDest AT ROW 18.29 COL 142.2 RIGHT-ALIGNED WIDGET-ID 300
     bDestExport AT ROW 19.38 COL 2.4 WIDGET-ID 18 NO-TAB-STOP 
     bNewDest AT ROW 20.52 COL 2.4 WIDGET-ID 118 NO-TAB-STOP 
     bDestEdit AT ROW 21.67 COL 2.4 WIDGET-ID 72 NO-TAB-STOP 
     bDeleteDest AT ROW 22.81 COL 2.4 WIDGET-ID 120 NO-TAB-STOP 
     "Notes:" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 15.71 COL 10 WIDGET-ID 114
     "General Contact" VIEW-AS TEXT
          SIZE 15.8 BY .62 AT ROW 11.1 COL 3.8 WIDGET-ID 104
     RECT-67 AT ROW 11.38 COL 2.6 WIDGET-ID 106
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 144 BY 23.38 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Contacts"
         HEIGHT             = 23.38
         WIDTH              = 144
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData bPeopleRefresh fMain */
/* BROWSE-TAB brwDataDest bDestRefresh fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bCancel IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bContactSave IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BROWSE brwData IN FRAME fMain
   ALIGN-R                                                              */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BROWSE brwDataDest IN FRAME fMain
   ALIGN-R                                                              */
ASSIGN 
       brwDataDest:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwDataDest:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

ASSIGN 
       fAgent:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       fName:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} preselect each person.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDataDest
/* Query rebuild information for BROWSE brwDataDest
     _START_FREEFORM
open query {&SELF-NAME} preselect each data by data.action by data.destType.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwDataDest */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Contacts */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Contacts */
do:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Contacts */
do:
  run windowResized in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME fMain /* Cancel */
do:
  run setAgentPersonContact in this-procedure.
  
  /* Enable/disable agent person contact buttons */
  run setContactButtons in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bContactEmail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bContactEmail C-Win
ON CHOOSE OF bContactEmail IN FRAME fMain /* Email */
do:
  define variable emailTo as character no-undo.
  
  if fEmail:screen-value eq "" 
   then
    return.
    
  emailTo = fEmail:input-value.  
  
  emailTo = replace(emailTo,",",";").
  
  run openURL("mailto:" + emailTo).  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bContactSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bContactSave C-Win
ON CHOOSE OF bContactSave IN FRAME fMain /* Save */
do:
  
  run savePersonContact in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteDest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteDest C-Win
ON CHOOSE OF bDeleteDest IN FRAME fMain /* Delete */
DO:
  run deleteSysDest in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDestEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDestEdit C-Win
ON CHOOSE OF bDestEdit IN FRAME fMain /* Modify */
DO:
  run modifySysDest in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDestExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDestExport C-Win
ON CHOOSE OF bDestExport IN FRAME fMain /* Export */
DO:
  run exportDestData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDestRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDestRefresh C-Win
ON CHOOSE OF bDestRefresh IN FRAME fMain /* Refresh */
DO:
  run refreshSysDest in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEdit C-Win
ON CHOOSE OF bEdit IN FRAME fMain /* Modify */
do:
  run actionModify in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEmail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEmail C-Win
ON CHOOSE OF bEmail IN FRAME fMain /* Email */
do:
  define variable emailTo as character no-undo.
  
  for each person:
    if person.tagStat 
     then
      emailTo = if emailTo = "" then person.contactEmail 
                 else emailTo + ";" + person.contactEmail.
  end.
  
  if emailTo eq "" 
   then
    return.

  run openURL("mailto:" + emailTo).  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportPerson in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
do:
  run actionNew in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNewDest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewDest C-Win
ON CHOOSE OF bNewDest IN FRAME fMain /* New */
do:
  run newSysDest in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPeopleRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPeopleRefresh C-Win
ON CHOOSE OF bPeopleRefresh IN FRAME fMain /* Refresh */
do:
  run refreshData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPersonAgentDeactivate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPersonAgentDeactivate C-Win
ON CHOOSE OF bPersonAgentDeactivate IN FRAME fMain /* PersonAgentDeactivate */
DO:
  if not available person
   then
    return.

  run deactivatepersonagent in this-procedure.
  
  run refreshData in this-procedure.
  apply 'value-changed' to brwData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain /* Association People */
do:
  run actionModify in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain /* Association People */
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain /* Association People */
do:
  {lib/brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain /* Association People */
DO:
  if not available person
   then
    return.
 
  if not person.personAgentactive
   then
    do:                                                            
      bPersonAgentDeactivate    :load-image("images/s-flag_green.bmp").
      bPersonAgentDeactivate    :tooltip   = "Activate association".
    end.
  else 
   do:
      bPersonAgentDeactivate    :load-image("images/s-flag_red.bmp").
      bPersonAgentDeactivate    :tooltip   = "Deactivate association".
   end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDataDest
&Scoped-define SELF-NAME brwDataDest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataDest C-Win
ON DEFAULT-ACTION OF brwDataDest IN FRAME fMain /* Output Destinations */
do:
  run modifySysDest in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataDest C-Win
ON ROW-DISPLAY OF brwDataDest IN FRAME fMain /* Output Destinations */
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataDest C-Win
ON START-SEARCH OF brwDataDest IN FRAME fMain /* Output Destinations */
do:
  {lib/brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataDest C-Win
ON VALUE-CHANGED OF brwDataDest IN FRAME fMain /* Output Destinations */
DO:
  assign
      bDeleteDest:sensitive  = (available data and data.entityID = ipcAgentID)
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eNotes C-Win
ON VALUE-CHANGED OF eNotes IN FRAME fMain
DO:
  resultsChanged(false).
  run setContactButtons in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fEmail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fEmail C-Win
ON LEAVE OF fEmail IN FRAME fMain /* Email */
DO:
  assign
   cDestination = "".
  
  if fEmail:input-value <> "" 
   then
    do:
      fEmail:screen-value = replace(replace(trim(fEmail:input-value),",",";")," ",";").
      
      if lookup("", fEmail:screen-value, ";") > 0
       then
        do std-in = 1 to num-entries(fEmail:screen-value, ";"):
         if entry(std-in,fEmail:screen-value, ";") = ""
          then
           next.
         cDestination = cDestination + entry(std-in,fEmail:screen-value, ";") + ";".  
        end.
        cDestination = trim(cDestination, ";").
        
      if cDestination <> ""
       then
        fEmail:screen-value = cDestination. 
      
      if not validEmailAddr(fEmail:screen-value)
       then
        do:
         message "Invalid Email Address" view-as alert-box warning buttons ok.
         return no-apply.
        end.
    end.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fEmail C-Win
ON VALUE-CHANGED OF fEmail IN FRAME fMain /* Email */
do:
  resultsChanged(false).
  run setContactButtons in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fPersonName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fPersonName C-Win
ON VALUE-CHANGED OF fPersonName IN FRAME fMain /* Name */
do:
  resultsChanged(false).
  run setContactButtons in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fPhone
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fPhone C-Win
ON VALUE-CHANGED OF fPhone IN FRAME fMain /* Phone */
do:
  resultsChanged(false).
  run setContactButtons in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i }

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .
{&window-name}:title = ipcCategory + " Contacts".

on close of this-procedure 
  run disable_UI.

pause 0 before-hide.
subscribe to "closeWindow"  anywhere.

bExport                 :load-image("images/s-excel.bmp").
bExport                 :load-image-insensitive("images/s-excel-i.bmp").
bPeopleRefresh          :load-image("images/s-sync.bmp").
bPeopleRefresh          :load-image-insensitive("images/s-sync-i.bmp").
bnew                    :load-image("images/s-add.bmp").
bnew                    :load-image-insensitive("images/s-add-i.bmp").
bEdit                   :load-image("images/s-update.bmp").
bEdit                   :load-image-insensitive("images/s-update-i.bmp").
bPersonAgentDeactivate  :load-image("images/s-flag_red.bmp").
bPersonAgentDeactivate  :load-image-insensitive("images/s-flag-i.bmp").
bEmail                  :load-image("images/s-email.bmp").
bEmail                  :load-image-insensitive("images/s-email-i.bmp").

bContactSave  :load-image("images/s-save.bmp").
bContactSave  :load-image-insensitive("images/s-save-i.bmp").
bCancel       :load-image("images/s-cancel.bmp").
bCancel       :load-image-insensitive("images/s-cancel-i.bmp"). 
bContactEmail :load-image("images/s-email.bmp").
bContactEmail :load-image-insensitive("images/s-email-i.bmp").

bDestExport   :load-image("images/s-excel.bmp").
bDestExport   :load-image-insensitive("images/s-excel-i.bmp").
bDestRefresh  :load-image("images/s-sync.bmp").
bDestRefresh  :load-image-insensitive("images/s-sync-i.bmp").
bNewDest      :load-image("images/s-add.bmp").
bNewDest      :load-image-insensitive("images/s-add-i.bmp").
bDestEdit     :load-image("images/s-update.bmp").
bDestEdit     :load-image-insensitive("images/s-update-i.bmp").
bDeleteDest   :load-image("images/s-delete.bmp").
bDeleteDest   :load-image-insensitive("images/s-delete-i.bmp").

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
 
  run getSysDest in this-procedure.
  
  run getAgentPersonContacts in this-procedure.
  
  run enable_UI.
  
  /* Enable/disable works only after enable_UI */
  run setSysDestButtons in this-procedure.
  
  /* set agent person contact details on the screen */
  run setAgentPersonContact in this-procedure.
  
  /* Enable/disable works only after enable_UI */
  run setContactButtons in this-procedure.
  
  assign
      fAgent:screen-value   = ipcAgentID
      fName :screen-value   = ipcAgentName
      bDeleteDest:sensitive = (available data and data.entityID = ipcAgentID)
      .
  
  {lib/get-column-width.i &col="'dispname'" &var=dColumnWidth}
   
  ON 'value-changed':U OF  person.tagStat in browse  brwData  
  DO:
    if available person 
     then
      person.tagStat = person.tagStat:checked in browse brwData. 
      
    run modifyTag in this-procedure.  
  END.
 
  apply "entry":U to fPersonName.
  
  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.
  
  if not this-procedure:persistent 
   then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionDelete C-Win 
PROCEDURE actionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  define variable lChoice as logical no-undo.
  
  if not available person 
   then
    return.
 
  lchoice = false.

  message "Highlighted Person will be deleted." skip "Do you want to continue?"
      view-as alert-box question buttons yes-no title "Delete Person" update lChoice.
 
  if not lChoice 
   then
    return.

  cPersonID = person.personID .
 
  publish "deleteAgentPerson" (input cPersonID,
                               output std-lo,
                               output std-ch).

  if not std-lo 
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
                               
  run getAgentPersonContacts in this-procedure.
  
  /* set agent person contact details on the screen */
  run setAgentPersonContact in this-procedure.
  
  /* Enable/disable agent person contact buttons */
  run setContactButtons in this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionModify C-Win 
PROCEDURE actionModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
  
  define variable iCount     as integer   no-undo.
  define variable cpersonID  as character no-undo.
  
  if not available person
   then 
    return.
   else
    do with frame {&frame-name}:
      cpersonID = person.personID.
      do iCount = 1 to {&browse-name}:num-iterations:
         if {&browse-name}:is-row-selected(iCount) 
          then leave.
      end.
    end.
  run dialogmodifypersonagent.w(input person.agentID,
                                input person.personID,
                                input person.dispname,
                                input person.jobTitle ,
                                input person.notes,
                                output std-lo).     
                                                           
  if not std-lo
   then
    return.
    
  /* Update the data from the data model. */
  run getAgentPersonContacts in this-procedure.
  
  /* set agent person contact details on the screen */
  run setAgentPersonContact in this-procedure.
  
  /* Enable/disable agent person contact buttons */
  run setContactButtons in this-procedure.
  
  for first person 
    where person.personID = cpersonID no-lock:
     std-ro = rowid(person).
  end.

 if std-ro <> ? and iCount > 0
   then
    do with frame {&frame-name}:
      {&browse-name}:set-repositioned-row(iCount, "ALWAYS") no-error.
      reposition {&browse-name} to rowid std-ro no-error.
    end.   
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionNew C-Win 
PROCEDURE actionNew :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
  
  define variable iCount    as integer   no-undo. 
  define variable olsuccess as logical   no-undo.
  define variable cPersonId as character no-undo.

  run dialognewperson.w(input ipcAgentID,
                        output cPersonId,
                        output olsuccess).

  if not olsuccess and cPersonID = ''
   then
    return.
    
  /* To get the latest personagent record. */
  run refreshData in this-procedure.
  
  /* Update the data from the data model. */
  run getAgentPersonContacts in this-procedure.
  
  /* set agent person contact details on the screen */
  run setAgentPersonContact in this-procedure.
  
  /* Enable/disable agent person contact buttons */
  run setContactButtons in this-procedure.

  find first person 
    where person.personID = cPersonID 
    no-error.

  if available person
   then
    std-ro = rowid(person).
   else 
    std-ro = ?.
                                                     
  if std-ro <> ? 
   then
    do:
      do iCount = 1 TO brwdata:num-iterations:
        if brwdata:is-row-selected(iCount)
         then
          leave.
      end.   
      brwdata:set-repositioned-row(iCount,"ALWAYS").
      reposition brwdata to rowid std-ro.
      brwdata:get-repositioned-row().  
    end. 
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deactivatePersonAgent C-Win 
PROCEDURE deactivatePersonAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------ */
 define variable pipersonagentID as integer no-undo.
 define variable olsuccess       as logical no-undo.
 
 if not available person
  then
   return.
 
 pipersonagentID = person.personagentID.


 if person.expirationdate = ?
  then
   do:
     publish "deactivateagentpersons" (input pipersonagentID,
                                       input today,
                                       output olsuccess,
                                       output std-ch).
     person.expirationdate = today.

   end.
  else
   do:

     publish "deactivateagentpersons" (input pipersonagentID,
                                       input ?,
                                       output olsuccess,
                                       output std-ch).
     person.expirationdate = ?.

   end.



 if not olsuccess
  then
   return.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteSysDest C-Win 
PROCEDURE deleteSysDest :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cMsg as character no-undo.
  
  if not available data
   then return.

  {lib/confirm-delete.i "Destination"}
  
  if not std-lo 
   then return. 
   
  publish "deletecontactSysDest" (input data.destID, output std-lo, output cMsg).
  
  if not std-lo 
   then 
    do:
      message cMsg
       view-as alert-box error buttons ok.
      return.  
    end.

  run getSysDest in this-procedure.
  
  run setSysDestButtons in this-procedure.
  
  apply 'value-changed' to brwDataDest in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fAgent fName fPersonName fPhone fEmail eNotes 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-67 fAgent fName bPeopleRefresh brwData bExport bNew bEdit 
         bPersonAgentDeactivate bEmail fPersonName fPhone fEmail bContactEmail 
         eNotes bDestRefresh brwDataDest bDestExport bNewDest bDestEdit 
         bDeleteDest 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportDestData C-Win 
PROCEDURE exportDestData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
    
  if query brwDataDest:num-results = 0 
   then
    do: 
      message "There is nothing to export"
         view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table data:handle.
  run util/exporttable.p (table-handle std-ha,
                          "data",
                          "for each data by data.action by data.destType",
                          "actionDesc,destTypeDesc,destName",
                          "Action,Destination,Value",
                          std-ch,
                          "SysDestAgent" + ipcAgentID + "_" + replace(string(now,"99-99-99"),"-","") + "_" + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).   
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportPerson C-Win 
PROCEDURE exportPerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
      
  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
         view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table person:handle.
  run util/exporttable.p (table-handle std-ha,
                          "person",
                          "for each person ",
                          "dispname,personID,tempStat,jobTitle,contactPhone,contactEmail",
                          "Name,Person ID,Status,Job Title,Phone,Email",
                          std-ch,
                          "PersonsOfAgent" + ipcAgentID + "_" + replace(string(now,"99-99-99"),"-","") + "_" + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).   
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getAgentPersonContacts C-Win 
PROCEDURE getAgentPersonContacts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tPerson       for tPerson.
  define buffer tAgentContact for tAgentContact.
  
  empty temp-table tPerson.
  empty temp-table tAgentContact.
  
  close query brwData.
  empty temp-table person.
  
  do with frame {&Frame-name}:
  end.
  
  publish "getagentpersons" (input ipcStateID, 
                             input ipcAgentID,
                             input ipcCategory,
                             output table tPerson,
                             output table tAgentContact).
                             
 for each tPerson:
   if(date(tPerson.expirationdate) > today or date(tPerson.expirationdate) = ?)
    then
     assign
         tPerson.tempStat          = "Active"
         tPerson.personAgentActive = true.
   else
    assign
        tPerson.tempStat          = "Inactive"
        tPerson.personAgentActive = false.

 end.

  /* if mobile is not empty showing mobile on UI, otherwise phone */
  for each tPerson:
    if tPerson.contactMobile ne "" and tPerson.contactMobile ne ?
     then
      tPerson.contactPhone = tPerson.contactMobile.      
  end.
  
  for each tPerson by tPerson.tagStat by tPerson.dispname:
    create person.
    buffer-copy tPerson to person.
  end.

  brwData:GET-BROWSE-COLUMN(1):label = ipcCategory.
  
  open query brwData preselect each person .
  
  apply 'value-changed' to brwData.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getSysDest C-Win 
PROCEDURE getSysDest :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer origdata for origdata.
  
  empty temp-table origdata.
  
  close query brwDataDest.
  empty temp-table data.
  
  do with frame {&frame-name}:
  end.
  
  publish "getContactSysDest" (input "G", 
                               input ipcAgentID,  
                               output table origdata).
                               
  for each origdata no-lock:
    create data.
    buffer-copy origdata to data.
  end. 
  
  open query brwDataDest preselect each data by data.action by data.destType. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifySysDest C-Win 
PROCEDURE modifySysDest :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iDestID as integer   no-undo.
  define variable cMsg    as character no-undo.
  define variable cAction as character no-undo.
  
  define buffer tempdata for tempdata.
  
  if not available data
   then return.
    
  empty temp-table tempdata.
  
  create tempdata.
  buffer-copy data to tempdata.
  
  for first tempdata:
    cAction = if tempdata.entityID <> '' then 'modify' else 'new'.
  end.

  run dialogdestination.w (input        true,
                           input        false,
                           input        "G",
                           input        ipcAgentID,
                           input        "",
                           input-output table tempdata,
                           output std-lo).
  if not std-lo
   then return.

  if cAction = 'modify' 
   then
    publish "modifyContactSysDest" (table tempdata, output std-lo, output cMsg).
   else
    publish "newContactSysDest" (input ipcAgentID, table tempdata,output iDestID, output std-lo, output cMsg).
  
  if not std-lo 
   then 
    do:
      message cMsg
       view-as alert-box error buttons ok.
      return.  
    end.

  run getSysDest in this-procedure.
  
  run setSysDestButtons in this-procedure.

  find first tempdata no-error.
  if not available tempdata
   then return.
   
  find first data where data.destID = (if iDestID = 0 then tempdata.destID else iDestID) no-error.
  if not available data
   then return.
   
  assign
      data.actionDesc   = tempdata.actionDesc
      data.destTypeDesc = tempdata.destTypeDesc
      .
      
  reposition brwDataDest to rowid rowid(data).
  
  apply 'value-changed' to brwDataDest in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyTag C-Win 
PROCEDURE modifyTag :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available person
   then return.
   
  publish "modifyTag" (input  person.personID,
                       input  ipcOrgID,
                       input  ipcCategory,
                       input  person.tagStat,
                       output std-lo,
                       output std-ch).
      
  if not std-lo
   then
    message std-ch
        view-as alert-box info buttons ok.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newSysDest C-Win 
PROCEDURE newSysDest :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iDestID as integer   no-undo.
  define variable cMsg    as character no-undo.
  
  define buffer tempdata for tempdata.
  empty temp-table tempdata.
  
  do with frame {&frame-name}:
  end.
    
  run dialogdestination.w (input        true,
                           input        true,
                           input        "G",
                           input        ipcAgentID,
                           input        ipcAgentName,
                           input-output table tempdata,
                                 output std-lo).
    
  if not std-lo 
   then return.
      
  publish "newContactSysDest" (input ipcAgentID, input table tempdata,output iDestID, output std-lo, output cMsg).

  if not std-lo 
   then 
    do:
      message cMsg
       view-as alert-box error buttons ok.
      return.  
    end.
    
  run getSysDest in this-procedure.
  
  run setSysDestButtons in this-procedure.
  
  find first data where data.destID = iDestID no-error.
  if not available data
   then return.
  
  find first tempdata no-error.
  if not available tempdata
   then return.
   
  assign
      data.actionDesc   = tempdata.actionDesc
      data.destTypeDesc = tempdata.destTypeDesc
      .
 
  reposition brwDataDest to rowid rowid(data). 
  
  
  apply 'value-changed' to brwDataDest.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openURL C-Win 
PROCEDURE openURL :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter ipcURL as character no-undo.

 run ShellExecuteA in this-procedure (0,
                                      "open",
                                      ipcURL,
                                      "",
                                      "",
                                      1,
                                      output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData C-Win 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "refreshAgentPersons"(input  ipcStateID,
                                input  ipcAgentID,
                                input  ipcCategory,
                                output std-lo,
                                output std-ch).
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
 
  run getAgentPersonContacts in this-procedure.
 
  /* set agent person contact details on the screen */
  run setAgentPersonContact in this-procedure.
   
  /* Enable/disable agent person contact buttons */
  run setContactButtons in this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshSysDest C-Win 
PROCEDURE refreshSysDest :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "LoadcontactSysDests".
  
  run getSysDest in this-procedure.

  run setSysDestButtons in this-procedure.
  
  apply 'value-changed' to brwDataDest in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE savePersonContact C-Win 
PROCEDURE savePersonContact :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  empty temp-table agentContact.
  
  create agentContact.
  assign
      agentContact.agentID  =  fAgent:input-value
      agentContact.category =  ipcCategory
      agentContact.name     =  fPersonName:input-value
      agentContact.phone    =  fPhone:input-value 
      agentContact.email    =  fEmail:input-value
      agentContact.notes    =  eNotes:input-value
      .
   
  publish "modifyAgentContact" (input table agentContact,
                                output std-lo,
                                output std-ch).
  if not std-lo            
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.
  
  assign
        chTrackName  = fPersonName:input-value
        chTrackEmail = fEmail:input-value
        chTrackPhone = fPhone:input-value
        chTrackNotes = eNotes:input-value
        .
        
  /* Enable/disable agent person contact buttons */
  run setContactButtons in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setAgentPersonContact C-Win 
PROCEDURE setAgentPersonContact :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&Frame-name}:
  end.
  
  for each tAgentContact:
    assign
        fPersonName:screen-value = tAgentContact.name
        fEmail     :screen-value = tAgentContact.email
        fPhone     :screen-value = tAgentContact.phone
        eNotes     :screen-value = tAgentContact.notes
        .
    assign
        chTrackName  = fPersonName:input-value
        chTrackEmail = fEmail:input-value
        chTrackPhone = fPhone:input-value
        chTrackNotes = eNotes:input-value
        .
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setContactButtons C-Win 
PROCEDURE setContactButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  assign 
      bExport:sensitive  = query brwData:num-results > 0
      bEdit  :sensitive  = query brwData:num-results > 0
      brwData:sensitive  = query brwData:num-results > 0
      bEmail :sensitive  = query brwData:num-results > 0
      .
       
  bContactSave:sensitive =  (chTrackName  <> fPersonName:input-value or
                             chTrackEmail <> fEmail:input-value or
                             chTrackPhone <> fPhone:input-value or
                             chTrackNotes <> eNotes:input-value ).
                                
  assign
      bCancel:sensitive       = bContactSave:sensitive
      bContactEmail:sensitive = fEmail:input-value <> ""
      .
      
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setSysDestButtons C-Win 
PROCEDURE setSysDestButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  assign
      bDestExport:sensitive = (query brwDataDest:num-results > 0)
      brwDataDest:sensitive = (query brwDataDest:num-results > 0)
      bDestEdit:sensitive   = (query brwDataDest:num-results > 0)
      .
  
  find current data no-error.
  if available data
   then
    assign
        bDeleteDest:sensitive  = (data.entityID <> '')
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i  &post-by-clause=" + ' by personID' "}

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame fMain:width-pixels          = {&window-name}:width-pixels
      frame fMain:virtual-width-pixels  = {&window-name}:width-pixels
      frame fMain:height-pixels         = {&window-name}:height-pixels
      frame fMain:virtual-height-pixels = {&window-name}:height-pixel
      /* fMain Components */
      brwData:width-pixels              = frame fmain:width-pixels - 45
      
      brwData:height-pixels             = frame fmain:height-pixels - brwData:y - 330
      .
      
  {lib/resize-column.i &col="'dispname'" &var=dColumnWidth}
  run ShowScrollBars(browse brwData:handle, no, yes).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getStatDesc C-Win 
FUNCTION getStatDesc RETURNS CHARACTER
  ( cStat as LOGICAL ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cStatReturn as character no-undo.
  if cStat 
   then 
    cStatReturn = "Active".
  else
   cStatReturn  = "Inactive".
    
  return cStatReturn.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 setStatusMessage({&ResultNotMatch}).
 cStatusDateTime = false.
 return true.
 
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

