&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

File: wledger.w 

Description:  

Input Parameters:
<none>

Output Parameters:
<none>

Author: Anjly 

Created: 07/28/20

Modified: 
Date        Name     Comments
01/18/2021  Shefali  Added new pop-up menu "View Detail".
03/15/2021  Shefali  Remove posting date sorting criteria.
06/02/2021  SA       Modified to add refund functionality
07/02/2021  Shefali  Modified to add write-off functionality
------------------------------------------------------------------------*/
/* This .W file was created with the Progress AppBuilder. */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
by this procedure. This is a good default which assures
that this procedure's triggers and internal procedures 
will execute in this procedure's storage, and that proper
cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ************************** Definitions ************************* */
{lib/std-def.i}
{lib/ar-def.i}
{lib/get-column.i}
{lib/winlaunch.i}
{lib/ar-gettrantype.i}

/* Temp-table Definition */

/* data recive from server*/
{tt/ledger.i}

/* descriptive data to expport*/
{tt/ledger.i &tableAlias="ttExportledger"}

/* detail transaction*/
{tt/arpmt.i &tablealias=ttArPmt}
{tt/arMisc.i &tableAlias=ttArMisc}
{tt/artran.i}
{tt/artran.i &tableAlias=ttArTran}

/*GL reporting*/
{tt/ledgerreport.i}
{tt/ledgerreport.i &tableAlias=glBatchDetail}
{tt/ledgerreport.i &tableAlias="glpaymentdetail"}
{tt/glbatchrpt.i}

/* filtered data */
define temp-table ttledger no-undo like ledger
  field rowColor as integer. /* Used to group records based on row color */

define variable dColumnWidth    as decimal    no-undo.
define variable cAccountID      as character  no-undo.
define variable cAcctDesc       as character  no-undo.
define variable cLedgerNotes    as character  no-undo.
define variable dtFromDate      as date       no-undo.
define variable dtToDate        as date       no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttledger

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData ttledger.transDate ttledger.ledgerID ttledger.agentID ttledger.agentname ttledger.accountID ttledger.debitAmount ttledger.creditAmount getTranType(ttledger.source) @ ttledger.source ttledger.sourceID getVoid(ttledger.void) @ ttledger.void   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each ttledger
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each ttledger.
&Scoped-define TABLES-IN-QUERY-brwData ttledger
&Scoped-define FIRST-TABLE-IN-QUERY-brwData ttledger


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fAccount bAccountLookup fStart fEnd cbSource ~
cbAccount cbAgent brwData bRefresh cbAccount2 RECT-1 RECT-3 RECT-2 
&Scoped-Define DISPLAYED-OBJECTS fAccount fStart fEnd cbSource cbAccount ~
cbAgent edNotes flNotesLbl cbAccount2 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getVoid C-Win 
FUNCTION getVoid RETURNS CHARACTER
( ipVoid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validateParams C-Win 
FUNCTION validateParams RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwData 
       MENU-ITEM m_View_Detail  LABEL "View Detail"   .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bAccountLookup 
     LABEL "accountookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Agent Lookup".

DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 4.8 BY 1.14 TOOLTIP "Cancel".

DEFINE BUTTON bCSV  NO-FOCUS
     LABEL "CSV" 
     SIZE 7.2 BY 1.67 TOOLTIP "Export".

DEFINE BUTTON bFilterClear  NO-FOCUS
     LABEL "FilterClear" 
     SIZE 7.2 BY 1.71 TOOLTIP "Clear filters".

DEFINE BUTTON bNoteSave 
     LABEL "Save" 
     SIZE 4.8 BY 1.14 TOOLTIP "Save Ledger Notes".

DEFINE BUTTON bOpen  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.67 TOOLTIP "Open detail".

DEFINE BUTTON bPDF  NO-FOCUS
     LABEL "PDF" 
     SIZE 7.2 BY 1.67 TOOLTIP "PDF".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.67 TOOLTIP "Get Data".

DEFINE VARIABLE cbAccount AS CHARACTER FORMAT "X(256)":U 
     LABEL "Account" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE cbAccount2 AS CHARACTER FORMAT "X(256)":U 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE cbAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 50.6 BY 1 NO-UNDO.

DEFINE VARIABLE cbSource AS CHARACTER FORMAT "X(256)":U 
     LABEL "Source" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE edNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 173 BY 2.91 NO-UNDO.

DEFINE VARIABLE fAccount AS CHARACTER FORMAT "X(256)":U 
     LABEL "Account" 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE fEnd AS DATE FORMAT "99/99/99":U 
     LABEL "End" 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE flNotesLbl AS CHARACTER FORMAT "X(256)":U INITIAL "Notes" 
     VIEW-AS FILL-IN 
     SIZE 7 BY .71 NO-UNDO.

DEFINE VARIABLE fStart AS DATE FORMAT "99/99/99":U 
     LABEL "Start" 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 53.4 BY 3.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 101.6 BY 3.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 24 BY 3.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      ttledger SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      ttledger.transDate         label "Post Date"           format "99/99/99"          width 12        
ttledger.ledgerID          label "Ledger ID"           format "x(12)"             width 12
ttledger.agentID           label "Agent ID"            format "x(15)"             width 12
ttledger.agentname         label "Name"                format "x(70)"             width 40
ttledger.accountID         label "Account"             format "x(35)"             width 18
ttledger.debitAmount       label "Debit"               format "->,>>>,>>9.99"     width 17
ttledger.creditAmount      label "Credit"              format "->,>>>,>>9.99"     width 18
getTranType(ttledger.source) @ 
ttledger.source            label "Source"              format "x(25)"             width 12
ttledger.sourceID          label "Source ID"           format "x(25)"             width 12
getVoid(ttledger.void) @ 
ttledger.void              label "Void"                format "x(8)"              width 12
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 178.4 BY 13.91 ROW-HEIGHT-CHARS .8 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     fAccount AT ROW 1.81 COL 11.6 COLON-ALIGNED WIDGET-ID 430
     bAccountLookup AT ROW 1.71 COL 26.8 WIDGET-ID 434
     bFilterClear AT ROW 2 COL 148.4 WIDGET-ID 262 NO-TAB-STOP 
     fStart AT ROW 2.91 COL 11.6 COLON-ALIGNED WIDGET-ID 452
     fEnd AT ROW 2.91 COL 30.6 COLON-ALIGNED WIDGET-ID 454
     bPDF AT ROW 2 COL 172.4 WIDGET-ID 456
     cbSource AT ROW 1.81 COL 64.4 COLON-ALIGNED WIDGET-ID 250
     cbAccount AT ROW 2.95 COL 64.4 COLON-ALIGNED WIDGET-ID 462
     bCSV AT ROW 2 COL 158.4 WIDGET-ID 442
     cbAgent AT ROW 1.81 COL 94.4 COLON-ALIGNED WIDGET-ID 246
     brwData AT ROW 4.76 COL 2.6 WIDGET-ID 200
     bRefresh AT ROW 2 COL 46.8 WIDGET-ID 4
     edNotes AT ROW 19.76 COL 8 NO-LABEL WIDGET-ID 36
     flNotesLbl AT ROW 18.95 COL 5.8 COLON-ALIGNED NO-LABEL WIDGET-ID 314 NO-TAB-STOP 
     bOpen AT ROW 2 COL 165.4 WIDGET-ID 450
     bNoteSave AT ROW 19.71 COL 2.6 WIDGET-ID 102 NO-TAB-STOP 
     bCancel AT ROW 20.86 COL 2.6 WIDGET-ID 110 NO-TAB-STOP 
     cbAccount2 AT ROW 2.95 COL 94.6 COLON-ALIGNED WIDGET-ID 464
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.14 COL 3.8 WIDGET-ID 48
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.14 COL 158.6 WIDGET-ID 446
     "Filters" VIEW-AS TEXT
          SIZE 5.8 BY .62 AT ROW 1.14 COL 58.2 WIDGET-ID 460
     RECT-1 AT ROW 1.38 COL 2.6 WIDGET-ID 46
     RECT-3 AT ROW 1.38 COL 157 WIDGET-ID 444
     RECT-2 AT ROW 1.38 COL 55.6 WIDGET-ID 458
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 195.2 BY 21.76 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Account Ledger"
         HEIGHT             = 17.86
         WIDTH              = 181.6
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData cbAgent fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bCancel IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bCSV IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bFilterClear IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bNoteSave IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bOpen IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPDF IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:POPUP-MENU IN FRAME fMain             = MENU POPUP-MENU-brwData:HANDLE
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR EDITOR edNotes IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       edNotes:RETURN-INSERTED IN FRAME fMain  = TRUE.

/* SETTINGS FOR FILL-IN flNotesLbl IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       flNotesLbl:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each ttledger.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Account Ledger */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Account Ledger */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Account Ledger */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAccountLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAccountLookup C-Win
ON CHOOSE OF bAccountLookup IN FRAME fMain /* accountookup */
DO:
  run dialogglaccountslookup.w(input fAccount:input-value,
                               output cAccountID,
                               output cAcctDesc,
                               output std-lo).
  
  if not std-lo
   then
    return no-apply.
  
  fAccount:screen-value = cAccountID.
  
  resultsChanged(false).
  
  if fAccount:input-value <> ""  
   then  
    /* Get ledger data */
    run getData in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME fMain /* Cancel */
DO:
  edNotes:screen-value = cLedgerNotes.
  run setNotesButton in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCSV
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCSV C-Win
ON CHOOSE OF bCSV IN FRAME fMain /* CSV */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilterClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilterClear C-Win
ON CHOOSE OF bFilterClear IN FRAME fMain /* FilterClear */
DO:  
  /* Reset filters to initial state */
  assign
      cbSource:screen-value   = {&ALL}
      cbAccount:screen-value  = {&ALL}
      cbAccount2:screen-value = {&ALL}
      cbAgent:screen-value    = {&ALL}      
      .
 
  /* Enable/disable filter button based on selected values */
  run setFilterButton in this-procedure.
  
  /* This will use the screen-value of the filters which is ALL */
  run filterData  in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNoteSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNoteSave C-Win
ON CHOOSE OF bNoteSave IN FRAME fMain /* Save */
do:
  run saveLedgerNotes in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOpen C-Win
ON CHOOSE OF bOpen IN FRAME fMain /* Open */
DO:
  run viewDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPDF
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPDF C-Win
ON CHOOSE OF bPDF IN FRAME fMain /* PDF */
DO:
  run postpdfrpt in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Go */
or 'RETURN' of fAccount
DO: 
  if not validateParams()
   then return no-apply.
   
  run getData in this-procedure.           
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  run viewDetail in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  do std-in = 1 to num-entries(colHandleList):
    colHandle = handle(entry(std-in, colHandleList)).
    if valid-handle(colHandle) 
     then colHandle:bgcolor = ttledger.rowColor.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  assign
      cLedgerNotes         = if available ttledger then ttledger.notes else ""
      edNotes:screen-value = if available ttledger then ttledger.notes else ""
      .
  
  run setNotesButton in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbSource
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbSource C-Win
ON VALUE-CHANGED OF cbSource IN FRAME fMain /* Source */
OR 'VALUE-CHANGED' of cbAgent
OR 'VALUE-CHANGED' of cbAccount
OR 'VALUE-CHANGED' of cbAccount2
DO:
  /* Enable/disable filter button based on selected values */
  run setFilterButton in this-procedure.
  
  /* This will use the screen-value of the filters which is ALL */
  run filterData in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edNotes C-Win
ON VALUE-CHANGED OF edNotes IN FRAME fMain
DO:
  run setNotesButton in this-procedure.      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAccount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAccount C-Win
ON VALUE-CHANGED OF fAccount IN FRAME fMain /* Account */
DO:
  resultsChanged(false).
  assign      
      bOpen:sensitive      = false
      bCSV:sensitive       = false
      bPDF:sensitive       = false
      . 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fEnd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fEnd C-Win
ON VALUE-CHANGED OF fEnd IN FRAME fMain /* End */
DO:
  resultsChanged(false).
  assign      
      bOpen:sensitive      = false
      bCSV:sensitive       = false
      bPDF:sensitive       = false
      . 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStart
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStart C-Win
ON VALUE-CHANGED OF fStart IN FRAME fMain /* Start */
DO:
  resultsChanged(false).
  assign      
      bOpen:sensitive      = false
      bCSV:sensitive       = false
      bPDF:sensitive       = false
      . 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Detail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Detail C-Win
ON CHOOSE OF MENU-ITEM m_View_Detail /* View Detail */
DO:
  run viewDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */ 
{&window-name}:window-state = window-minimized. 

{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}
 
assign 
    {&window-name}:min-height-pixels = {&window-name}:height-pixels
    {&window-name}:min-width-pixels  = {&window-name}:width-pixels
    {&window-name}:max-height-pixels = session:height-pixels
    {&window-name}:max-width-pixels  = session:width-pixels
    .

ASSIGN 
    CURRENT-WINDOW                = {&WINDOW-NAME} 
    THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME} .


setStatusMessage("").       
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bPDF          :load-image            ("images/pdf.bmp").
bPDF          :load-image-insensitive("images/pdf-i.bmp").

bCSV          :load-image            ("images/excel.bmp").
bCSV          :load-image-insensitive("images/excel-i.bmp").

bopen         :load-image            ("images/open.bmp").
bopen         :load-image-insensitive("images/open-i.bmp").

bNoteSave     :load-image            ("images/s-save.bmp").
bNoteSave     :load-image-insensitive("images/s-save-i.bmp").

bCancel       :load-image            ("images/s-cancel.bmp").
bCancel       :load-image-insensitive("images/s-cancel-i.bmp").

bRefresh      :load-image            ("images/completed.bmp").
bRefresh      :load-image-insensitive("images/completed-i.bmp").

bFilterClear  :load-image            ("images/filtererase.bmp").
bFilterClear  :load-image-insensitive("images/filtererase-i.bmp").

bAccountLookup:load-image            ("images/s-lookup.bmp").
bAccountLookup:load-image-insensitive("images/s-lookup-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
  RUN enable_UI.
  
  {lib/get-column-width.i &col="'Name'"  &var=dColumnWidth} 
 /* 
  publish "GetAutoDefaultAgent" (output lDefaultAgent).
  
  if lDefaultAgent
   then
    do:
      publish "GetDefaultAgent"(output cAgentID).
      fAgent:screen-value = cAgentID.         
    end.
  */ 
  
  /* Initialise filters to default values */
  assign
      cbAgent:list-item-pairs   = {&ALL} + "," + {&ALL}
      cbSource:list-item-pairs  = {&ALL} + "," + {&ALL}
      cbAccount:list-items      = {&ALL} 
      cbAccount2:list-items     = {&ALL}
      cbAgent:screen-value      = {&ALL}
      cbSource:screen-value     = {&ALL}
      cbAccount:screen-value    = {&ALL}
      cbAccount2:screen-value   = {&ALL}
      cbAgent:sensitive         = false
      cbAccount:sensitive       = false
      cbAccount2:sensitive      = false
      cbSource:sensitive        = false
      faccount:screen-value     = {&ALL}
      .
  
  /* Getting date range from first and last open active period */   
  publish "getOpenPeriod" (output dtFromDate,output dtToDate).    
  
  fStart:screen-value = string(dtFromDate).                                   
  fEnd:screen-value   = string(dtToDate).      
  
  run ShowWindow in this-procedure.
  
  apply "entry" to fAccount.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "WindowClosed" (input this-procedure).   
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fAccount fStart fEnd cbSource cbAccount cbAgent edNotes flNotesLbl 
          cbAccount2 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE fAccount bAccountLookup fStart fEnd cbSource cbAccount cbAgent brwData 
         bRefresh cbAccount2 RECT-1 RECT-3 RECT-2 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttledger for ttledger.
  
  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
    
  publish "GetReportDir" (output std-ch).
  
  empty temp-table ttExportledger.
  
  for each ttledger by ttledger.ledgerID desc:
    create ttExportledger.
    buffer-copy ttledger to ttExportledger.
    ttExportledger.source = getTranType(ttledger.source)
    .
  end.
 
  std-ha = temp-table ttExportledger:handle.
  run util/exporttable.p (table-handle std-ha,
                          "ttExportledger",
                          "for each ttExportledger",
                          "transdate,ledgerID,seq,agentID,agentname,accountID,debitamount,creditamount,void,source,sourceID,createdBy,createdByName,createDate,notes",
                          "Post Date,Ledger ID,Seq,Agent ID,Agent Name,Account ID,Debit,Credit,Void,Source,Source ID,Created By,Created By Name,Create Date,Notes.",
                          std-ch,
                          "Ledger_" + replace(string(today), "/", "") + "_" + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
                                              
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwData.
  empty temp-table ttledger.
  
  do with frame {&frame-name}:
  end.

  define buffer ledger for ledger.

  rowColor = {&oddColor}.
  
  for each ledger 
    where ((ledger.accountID ge cbAccount:input-value and ledger.accountID le  cbAccount2:input-value ) or (cbAccount:input-value = {&ALL} or cbAccount2:input-value = {&ALL}))
      and ledger.source    = (if cbSource:input-value   = {&ALL} then ledger.source  else (if (ledger.source = "VCR" or ledger.source = "VPR") and cbSource:input-value = "RF" then ledger.source else  cbSource:input-value))
      and ledger.agentid   = (if cbAgent:input-value    = {&ALL} then ledger.agentid else cbAgent:input-value)
      break by ledger.ledgerID:
    
    if first-of(ledger.ledgerID)
     then rowColor = (if rowColor = {&evenColor} then {&oddColor} else {&evenColor}).
       
    create ttledger.
    buffer-copy ledger to ttledger.
    ttledger.rowColor = rowColor.    
  end.
  
  open query brwData preselect each ttledger by ttledger.ledgerID desc.
    
  run setWidgetState in this-procedure.
    
  setStatusCount(query brwData:num-results).
  
  apply 'value-changed' to brwData. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end.
  
  empty temp-table ledger.

  run server/getledger.p (input 0,  /* ledgerID */
                          input fAccount:screen-value,
                          input fStart:input-value,
                          input fEnd:input-value,
                          output table ledger,
                          output std-lo,
                          output std-ch).
    
  run initialiseFilter in this-procedure.
  
  /* Enable/disable filter button based on selected values */
  run setFilterButton in this-procedure.
  
  /* This will use the screen-value of the filters */
  run filterData in this-procedure. 
  
  /* Makes filters enable-disable based on the data */ 
  assign 
      cbAgent:sensitive    = query brwData:num-results > 0
      cbAccount:sensitive  = query brwData:num-results > 0
      cbAccount2:sensitive = query brwData:num-results > 0
      cbSource:sensitive   = query brwData:num-results > 0              
      .
  
  setStatusRecords(query brwData:num-results).    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initialiseFilter C-Win 
PROCEDURE initialiseFilter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cSelectedAgent   as character no-undo.
  define variable cSelectedSource  as character no-undo.
  define variable cSelectedAccount as character no-undo.
  define variable cSelectedAccount2 as character no-undo.
  
  do with frame {&frame-name}:
  end.
  
  define buffer ledger for ledger.
  
  /* Retaining previous selected values of filters */
  assign 
      cSelectedAgent    = if cbAgent:screen-value    = ? then "" else cbAgent:screen-value      
      cSelectedAccount  = if cbAccount:screen-value  = ? then "" else cbAccount:screen-value
      cSelectedAccount2 = if cbAccount2:screen-value = ? then "" else cbAccount2:screen-value
      cSelectedSource   = if cbSource:screen-value   = ? then "" else cbSource:screen-value 
      .
  
  /* Initialise filters to default values */
  assign
      cbAgent:list-item-pairs   = {&ALL} + "," + {&ALL}
      cbSource:list-item-pairs  = {&ALL} + "," + {&ALL}
      cbAccount:list-items      = {&ALL}
      cbAccount2:list-items     = {&ALL}
      cbAgent:screen-value      = {&ALL}
      cbSource:screen-value     = {&ALL}
      cbAccount:screen-value    = {&ALL}
      cbAccount2:screen-value   = {&ALL}
      .
  /* Set the filters based on the data returned, with ALL as the first option */
  for each ledger 
    break by ledger.agentname:
    if first-of(ledger.agentname) and ledger.agentname <> ""
     then 
      cbAgent:add-last(replace((ledger.agentname + " (" + ledger.agentID + ")" ),",",""),ledger.agentID).
  end.
    
  for each ledger 
    break by ledger.source:
    if (ledger.source = {&VoidPaymentRefund}) or (ledger.source= {&VoidCreditRefund}) or (ledger.source= {&Refund}) 
     then 
      if not can-do(cbSource:list-item-pairs,getTranType({&Refund})) 
       then
        cbSource:add-last(getTranType({&Refund}), {&Refund}).
      else
      next.
    else if first-of(ledger.source) and ledger.source <> ""
     then 
      cbSource:add-last(getTranType(ledger.source), ledger.source).
  end.
  
  for each ledger 
    break by ledger.accountID:
    if first-of(ledger.accountID) and ledger.accountID <> ""
     then 
      do:
        cbAccount:add-last(ledger.accountID).
        cbAccount2:add-last(ledger.accountID).
      end.
  end.
  
  /* Setting previous selected values of filters */
  if cSelectedAgent <> "" and cSelectedAgent <> {&ALL} and lookup(cSelectedAgent,cbAgent:list-item-pairs) > 0  
   then
    cbAgent:screen-value = cSelectedAgent.
  
  if cSelectedSource <> "" and cSelectedSource <> {&ALL} and lookup(cSelectedSource,cbSource:list-item-pairs) > 0  
   then
    cbSource:screen-value = cSelectedSource.
    
  if cSelectedAccount <> "" and cSelectedAccount <> {&ALL} and lookup(cSelectedAccount,cbAccount:list-items) > 0  
   then
    cbAccount:screen-value = cSelectedAccount.
  
  if cSelectedAccount2 <> "" and cSelectedAccount2 <> {&ALL} and lookup(cSelectedAccount2,cbAccount2:list-items) > 0  
   then
    cbAccount2:screen-value = cSelectedAccount2.
    
END PROCEDURE.
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyvoidrefund C-Win 
PROCEDURE modifyvoidrefund :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable iarrefundID  as integer  no-undo.
 do with frame {&frame-name}:
 end.                        
 if not available ttledger
  then
   return.
 /* Client Server Call */
 run server/gettransaction.p (input integer(ttledger.sourceID), /* Type */
                              input  0,      /*tranID*/
                              input  '',       /*Type*/
                              output table ARTran,
                              output std-lo,
                              output std-ch).
 if not std-lo
  then
   do:
      message std-ch
         view-as alert-box error buttons ok.
      return.
   end.
 run dialogviewrefund.w (input {&ModifyPosted}, /* notes modify */
                         input-output table arTran,
                         output std-lo).
 if not std-lo   
  then           
   return.       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE postpdfrpt C-Win 
PROCEDURE postpdfrpt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable cAction as character no-undo.

 if not available ttledger
  then return.
 
 run server\querypostgl.p (input ttledger.ledgerID,
                           output cAction ,
                           output table ledgerreport,
                           output table glpaymentdetail,
                           output table glBatchDetail,
                           output table glbatchrpt,
                           output std-lo,
                           output std-ch).
                           
 if not std-lo
  then
   do:
      message std-ch
         view-as alert-box.
      return.
   end.   
   
 case cAction:
   when {&HistoryTransaction}
    then
     run util\arhistorictranspdf.p (input {&view},
                                    input table ledgerreport,
                                    output std-ch).
   when {&PostBatch}
    then 
      run util\arbatchpdf.p (input {&view},
                             input table ledgerreport,
                             input table glBatchDetail,        
                             input table glbatchrpt,
                             output std-ch). /* filename */  
   when {&PostMiscInvoice}
    then 
     run util\arinvoicepdf.p (input {&view},
                              input table ledgerreport,
                              output std-ch). /* filename */  
   when {&VoidMiscInvoice}
    then 
     run util\arinvoicevoidpdf.p (input {&view},
                                  input table ledgerreport,
                                  output std-ch). /* filename */  
   when {&PostCredit}
    then 
     run util\arcreditpdf.p (input {&view},
                             input table ledgerreport,
                             output std-ch). /* filename */  
   when {&VoidCredit}
    then 
     run util\arcreditvoidpdf.p (input {&view},
                                 input table ledgerreport,
                                 output std-ch). /* filename */  
   when {&PostPayment}
    then 
     run util\arpaymentpdf.p (input {&view},
                              input table ledgerreport,
                              output std-ch). /* filename */  
   when {&VoidPayment}
    then 
     run util\arpaymentvoidpdf.p (input {&view},
                                  input table ledgerreport,
                                  output std-ch). /* filename */  

   when {&PostDeposit}
    then 
     run util\ardepositpdf.p (input {&view},      
                            input table glpaymentdetail, 
                            input table ledgerreport,
                            output std-ch). /* filename */  
   when {&VoidDeposit}
    then 
     run util\ardepositvoidpdf.p (input {&view},  
                                  input table glpaymentdetail, 
                                  input table ledgerreport, 
                                  output std-ch). /* filename */ 
  when {&PostRefund}
   then 
    run util\arpostrefundpdf.p (input {&view}, 
                                input table glpaymentdetail,         
                                input table ledgerreport,
                                output std-ch). /* filename */  
  when {&VoidPaymentRefund}
   then 
    run util\arpaymentrefundvoidpdf.p (input {&view},           
                                       input table ledgerreport,
                                       output std-ch).          
  
  when {&VoidCreditRefund}
   then
    run util\arcreditrefundvoidpdf.p (input {&view},            
                                       input table ledgerreport,
                                       output std-ch).          
  
  when {&PostWriteOff}
   then
     run util\arwriteoffpdf.p (input {&view},            
                               input table ledgerreport,
                               output std-ch).
                                  
   when {&VoidWriteOff}
    then
     run util\arwriteoffvoidpdf.p (input {&view},            
                                   input table ledgerreport,
                                   output std-ch).   
  
 end case.
 
                                         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveLedgerNotes C-Win 
PROCEDURE saveLedgerNotes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer bufledger for ttledger.
  
  if not available ttledger
   then return.
   
  do with frame {&frame-name}:
  end.
   
  run server/modifyledgernotes.p (input integer(ttledger.ledgerID),
                                  input edNotes:input-value,
                                  output std-lo,
                                  output std-ch).
  if not std-lo            
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.
  
  /* pdating notes for all records having same ledger ID */
  for each bufledger where bufledger.ledgerID = ttledger.ledgerID:  
    bufledger.notes = edNotes:input-value.
  end.
  
  cLedgerNotes = edNotes:input-value.
  
  run setNotesButton in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterButton C-Win 
PROCEDURE setFilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if cbSource:input-value   ne {&ALL} or
     cbAccount:input-value  ne {&ALL} or
     cbAccount2:input-value ne {&ALL} or
     cbAgent:input-value    ne {&ALL}  
   then
    bFilterClear:sensitive = true.
   else   
    bFilterClear:sensitive = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setNotesButton C-Win 
PROCEDURE setNotesButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  assign
      bCancel:sensitive   = cLedgerNotes <> edNotes:screen-value
      bNoteSave:sensitive = cLedgerNotes <> edNotes:screen-value
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end.
  
  assign
      bOpen:sensitive     = can-find(first ttledger)
      bCSV:sensitive      = can-find(first ttledger)
      bPDF:sensitive      = can-find(first ttledger)
      edNotes:sensitive   = can-find(first ttledger)
      .
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  do with frame {&frame-name}:
  end.
  
  if brwData:current-column:label = "Post Date" or 
     brwData:current-column:label = "Ledger ID" 
   then
    do:
      tWhereClause = " by ttledger.ledgerID desc".
      {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}  
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewDetail C-Win 
PROCEDURE viewDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 case ttledger.source:
   when {&ArBatch}
    then
     publish "OpenWindow" (input "wBatchDetail", 
                           input ttledger.SourceID, 
                           input "wbatchdetail.w", 
                           input "character|input|" + ttledger.SourceID + "^character|input|" + {&All} + "^character|input|" + string(ttledger.agentID),                                   
                           input this-procedure). 
   
   when {&Payment} /* 'P' type Payment */
    then
     do:
       /* Client Server Call */
       run server\getpayment.p (input 0,                            /* artranID */
                                input  integer(ttledger.sourceID),  /* arPmtID */
                                output table ttarpmt,
                                output std-lo,
                                output std-ch).

       if not std-lo
        then
         do:
           message std-ch
               view-as alert-box.
           return.
         end.
        
       run dialogmodifypayment.w (input {&ModifyPosted},  /* notes modify */
                                  input-output table ttarpmt,
                                  output std-lo).
     end.
    
   when {&Invoice} /* 'I' type Misc Invoice */
    then
     do:       
       /* Client Server Call */
       run server/getinvoices.p (input {&Invoice}, /* Type */
                                 input integer(ttledger.sourceID),
                                 output table ttARMisc,
                                 output std-lo,
                                 output std-ch).
                            
       if not std-lo
        then
         do:
           message std-ch 
               view-as alert-box error buttons ok.
           return.
         end.
        
       run dialoginvoice.w (input-output table ttARMisc,
                            input {&Invoice},
                            input {&ModifyPosted}, /* notes modify */
                            output std-lo).

     end.
     
   when {&Credit} /* 'C' type Misc Credit */
    then
     do:
       /* Client Server Call */
       run server/getinvoices.p (input {&Credit}, /* Type */
                                 input integer(ttledger.sourceID),
                                 output table ttARMisc,
                                 output std-lo,
                                 output std-ch).
                            
       if not std-lo
        then
         do:
            message std-ch 
               view-as alert-box error buttons ok.
            return. 
         end.
    
       run dialoginvoice.w (input-output table ttARMisc,
                            input {&Credit},
                            input {&ModifyPosted}, /* notes modify */
                            output std-lo).
     end.
     
   when {&deposit} /* 'D' type Deposit */
    then       
     publish "OpenWindow" (input "wpaymentreport",                                                       /*childtype*/
                           input ttledger.SourceID,                                                      /*childid*/
                           input "wpaymentreport.w",                                                     /*window*/
                           input "integer|input|" + string(ttledger.SourceID),    /*parameters*/                               
                           input this-procedure).                                                        /*currentProcedure handle*/ 
                           
   when {&refund} /* 'RF'  */
    then
     publish "OpenWindow" (input "wpostedrefund",                                                       /*childtype*/
                           input ttledger.SourceID,                                                      /*childid*/
                           input "wpostedrefund.w",                                                     /*window*/
                           input "character|input|" + string(ttledger.SourceID),    /*parameters*/                               
                           input this-procedure). 
  when {&writeoff} /* 'W'  */
    then
      publish "OpenWindow" (input "wpostedwriteoff",                                                       /*childtype*/
                           input ttledger.SourceID,                                                      /*childid*/
                           input "wpostedwriteoff.w",                                                     /*window*/
                           input "integer|input|" + string(ttledger.SourceID),    /*parameters*/                               
                           input this-procedure).
                                     
  when {&VoidPaymentRefund} /* 'VPR' */
   then
    run modifyvoidrefund in this-procedure.
      
  when {&VoidCreditRefund}  /* 'VCR' */
   then
    run modifyvoidrefund in this-procedure.
                                        
 
 end case.
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign
      frame fMain:width-pixels          = {&window-name}:width-pixels
      frame fMain:virtual-width-pixels  = {&window-name}:width-pixels
      frame fMain:height-pixels         = {&window-name}:height-pixels
      frame fMain:virtual-height-pixels = {&window-name}:height-pixels
      edNotes:width-pixels              = frame {&frame-name}:width-pixels  - 43                              
      /* fMain components */
      brwData:width-pixels              = frame fmain:width-pixels - 16
      brwData:height-pixels             = frame fMain:height-pixels - 161
      .

  assign
      bNoteSave:y  = frame {&frame-name}:height-pixels - 64
      bCancel:y    = frame {&frame-name}:height-pixels - 40
      edNotes:y    = frame {&frame-name}:height-pixels - 63      
      flNotesLbl:y = frame {&frame-name}:height-pixels - 78
      .    
      
  {lib/resize-column.i &col="'Name'"    &var=dColumnWidth} 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getVoid C-Win 
FUNCTION getVoid RETURNS CHARACTER
( ipVoid as logical ) :
/*------------------------------------------------------------------------------
Purpose:
Notes:
------------------------------------------------------------------------------*/
if ttledger.void
then
return "Yes".
else
return "". /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validateParams C-Win 
FUNCTION validateParams RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if fStart:input-value = ? or fStart:input-value > today    
   then
    do:
      message "Please enter a valid start date that is not in the future."
          view-as alert-box.
      return false.
    end. 
  
  if fEnd:input-value = ?  
   then
    do:
      message "Please enter valid end date."
          view-as alert-box.
      return false.  
    end.
  
  if fEnd:input-value < fStart:input-value 
   then
    do:
      message "End date cannot be less than start date."
          view-as alert-box.
      return false.  
    end. 

  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

