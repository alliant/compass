&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*---------------------------------------------------------------------
@name State Source
@description add/modify State Source

@author S Chandu
@version 1.0
@created 05/20/2024
@notes 
@modified
---------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
  {tt/statesource.i}

/* Parameters Definitions ---                                           */
define input  parameter ipcActionType       as character no-undo.
define input  parameter table               for stateSource.
define output parameter chStateID           as character   no-undo.
define output parameter oplSuccess          as logical   no-undo. 

{tt/statesource.i &tableAlias=ttstatesource}
/* --Temp-Table Definitions ---                                         */
{tt/state.i}
{lib/make-required-def.i}

/* Local Variable Definitions ---                                       */
define variable chGLAcct     as character no-undo.
define variable chRevType    as character no-undo.
define variable cSourceList  as character no-undo.
define variable chGLAcctDesc as character no-undo.

define variable cCode  as character no-undo.
define variable cStateId as character no-undo.
define variable cSourceType as character no-undo.
define variable cRevType as character no-undo.
define variable cAction as character no-undo.

{lib/std-def.i}
{lib/add-delimiter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tState tSourceType bSysCode Btn_Link 
&Scoped-Define DISPLAYED-OBJECTS tState tSourceType tRevType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bSysCode 
     LABEL "sysCode" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON Btn_Link AUTO-GO 
     LABEL "Save" 
     SIZE 14 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tSourceType AS CHARACTER FORMAT "X(256)" 
     LABEL "Source Type" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "--Select Source Type--","None"
     DROP-DOWN-LIST
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE tState AS CHARACTER FORMAT "X(36)" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "item","item"
     DROP-DOWN-LIST
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE tRevType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Revenue Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 35 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     tState AT ROW 1.24 COL 14.6 COLON-ALIGNED WIDGET-ID 104
     tSourceType AT ROW 2.33 COL 14.6 COLON-ALIGNED WIDGET-ID 4
     bSysCode AT ROW 3.48 COL 51.8 WIDGET-ID 252
     tRevType AT ROW 3.52 COL 14.6 COLON-ALIGNED WIDGET-ID 64 NO-TAB-STOP 
     Btn_Link AT ROW 5 COL 23
     SPACE(21.99) SKIP(0.42)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "State Source" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN tRevType IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       tRevType:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* State Source */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSysCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSysCode Dialog-Frame
ON CHOOSE OF bSysCode IN FRAME Dialog-Frame /* sysCode */
DO:
  run dialogrevenuelookup.w(input tRevType:screen-value,
                            input tState:screen-value,
                            output cCode,                            
                            output std-lo).
  if not std-lo 
   then
    return no-apply.
  
  tRevType:screen-value = cCode.
 
   if ipcActionType = "Create"
    then
     if tstate:screen-value <> ? and tsourcetype:screen-value <> ? and tRevType:screen-value <> ""
      then
       Btn_Link:sensitive = true.  
   else
     do:
       if cRevType <> cCode then
           Btn_Link:sensitive = true.
       else
          Btn_Link:sensitive = false.
     end. 
     
   apply "VALUE-CHANGED" to tRevType.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Link
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Link Dialog-Frame
ON CHOOSE OF Btn_Link IN FRAME Dialog-Frame /* Save */
DO:
  run savestatesource in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tRevType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tRevType Dialog-Frame
ON VALUE-CHANGED OF tRevType IN FRAME Dialog-Frame /* Revenue Type */
DO:
  run enableDisableSave in this-procedure.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSourceType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSourceType Dialog-Frame
ON VALUE-CHANGED OF tSourceType IN FRAME Dialog-Frame /* Source Type */
DO:
   run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tState Dialog-Frame
ON VALUE-CHANGED OF tState IN FRAME Dialog-Frame /* State */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */
{lib/win-icon.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
       
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.
   
/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
/* get state list */
{lib/get-state-list.i &combo=tState}
bSysCode  :load-image("images/s-lookup.bmp").
bSysCode  :load-image-insensitive("images/s-lookup-i.bmp").

 
/*get sourcetype list */
publish "GetStateSourceList" (output cSourceList).
assign
    tSourceType:list-item-pairs = tSourceType:list-item-pairs + ',' + cSourceList no-error.     
       
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  /* add required symbol */

  if ipcActionType = "New"
   then
    do:
      {lib/make-required.i tState:handle}
      {lib/make-required.i tSourceType:handle}
    end.
  
  {lib/make-required.i bSysCode:handle}
  setRequired().
  do with frame {&frame-name}:
  end.
  
 
  run displayData       in this-procedure.

  run enableDisableSave in this-procedure.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseWindow Dialog-Frame 
PROCEDURE CloseWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    APPLY "END-ERROR":U TO SELF.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  if ipcActionType = 'Modify' 
   then
    do:
      find first statesource no-error. 
      if available statesource 
       then
        do:
          assign 
              frame Dialog-Frame:title          = "Modify State Source"
              Btn_Link          :label          = "Save"
              Btn_Link          :tooltip        = "Save"
              tSourceType       :sensitive      = false
              tstate            :sensitive      = false
              tSourceType       :screen-value   = if statesource.sourceType = "Production" then "P"  else "P"     /* keeping default to (P)roduction  */
              tRevType          :screen-value   = statesource.revenueType 
              tstate            :screen-value   = statesource.stateID.
        end.  /* if available statesource */
 
        chStateID  = tstate         :screen-value.
    end.
  else if ipcActionType = 'New' 
   then
    do:
     tState:add-first("--Select State--","None").
     assign
         frame Dialog-Frame:title          = "New State Source"
         Btn_Link          :label          = "Create"  
         Btn_Link          :tooltip        = "Create"  .
         tState            :screen-value   = "None"  . 
         tSourceType       :screen-value   = "None"   .
         .  
    end. 
  
 assign   
     chRevType        = tRevType        :screen-value     
     .          
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if ipcActionType = 'New'
   then
    do: 
       Btn_Link:sensitive = if (tSourceType:screen-value ne "None") and (tState:screen-value ne ?) and tRevType:screen-value <> ""  then true else false.
    end.
  else 
     Btn_Link:sensitive = not(( tRevType:screen-value = "" or chRevType = tRevType:screen-value ) and 
                               chStateID    = tState:screen-value ) no-error.    

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tState tSourceType tRevType 
      WITH FRAME Dialog-Frame.
  ENABLE tState tSourceType bSysCode Btn_Link 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveStateSource Dialog-Frame 
PROCEDURE saveStateSource :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if ipcActionType = 'New'
   then
    do:
      empty temp-table statesource.
      create statesource.
      assign
          statesource.sourceType    = tSourceType:screen-value  
          statesource.stateID       = tstate     :input-value 
          statesource.revenuetype   = tRevType   :screen-value.
          chStateID                 = tstate     :input-value .      
    end. 
   else if ipcActionType = 'Modify'
    then
     do:
       for first statesource no-lock:
         assign
             statesource.revenuetype         = tRevType:screen-value
             statesource.sourceType          = tSourceType:screen-value
              .
       end.
     end. 
  
  if ipcActionType = 'New'
   then
    do:
      if tSourceType:input-value = "ALL" or tSourceType:input-value = ?  and  tstate:input-value = "ALL" or tstate:input-value = ?  
       then
        do:
          message "Source\state cannot be blank"
             view-as alert-box error buttons ok.
          return. 
        end.
        publish "newstatesource"(input-output table  statesource,
                                  output oplSuccess,
                                  output std-ch ).         
    end.
   else if ipcActionType = 'Modify'
    then 
      do:
        publish "modifystatesource"(input-output table  statesource,
                                     output oplSuccess,
                                     output std-ch ).
      end.
  if oplSuccess
    then run CloseWindow in this-procedure.
  else
    message  std-ch
        view-as alert-box error buttons ok.
                             
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

