&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME eC-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS eC-Win 
/*------------------------------------------------------------------------
  File: wrefunds.w

  Description: Window for AR Payment

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Sachin Anthwal

  Created: 02.07.2020
  
  @Modified :
  Date        Name     Description   

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/ar-def.i}
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/winshowscrollbars.i}
{lib/get-column.i}
{lib/brw-totalData-def.i}
{lib/ar-getentitytype.i} /* Include function: getEntityType */

/* Temp-table Definition */
{tt/artran.i }
{tt/unappliedtran.i        &tableAlias="unappliedtran"}    /* table used to show data on browse */
{tt/unappliedtran.i        &tableAlias="tunappliedtran"}   /* used to keep original data to update change pending field */
{tt/unappliedtran.i        &tableAlias="ttunappliedtran"}
{tt/ledgerreport.i}
{tt/artran.i &tableAlias="tartran"}
{tt/ledgerreport.i &tableAlias="glPaymentDetail"}

define input parameter refundID  as character  no-undo. 

define variable dColumnWidth    as decimal   no-undo.
define variable lDefaultAgent   as logical   no-undo.
define variable dtFromDate      as date      no-undo.
define variable dtToDate        as date      no-undo.    
define variable lcheck          as logical   no-undo.
define variable iDepositID      as integer   no-undo.
define variable lSelect         as logical   no-undo.
define variable haPmtRecord     as handle     no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwrefunds

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES unappliedtran

/* Definitions for BROWSE brwrefunds                                    */
&Scoped-define FIELDS-IN-QUERY-brwrefunds unappliedtran.entityID unappliedtran.entityName getType(unappliedtran.type) @ unappliedtran.type unappliedtran.reference unappliedtran.referenceamt unappliedtran.filenumber unappliedtran.sourceID unappliedtran.transDate unappliedtran.refundAmt getVoid(unappliedtran.void) @ unappliedtran.void   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwrefunds   
&Scoped-define SELF-NAME brwrefunds
&Scoped-define QUERY-STRING-brwrefunds for each unappliedtran
&Scoped-define OPEN-QUERY-brwrefunds open query {&SELF-NAME} for each unappliedtran.
&Scoped-define TABLES-IN-QUERY-brwrefunds unappliedtran
&Scoped-define FIRST-TABLE-IN-QUERY-brwrefunds unappliedtran


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwrefunds}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bPrelimRpt btVoid brwrefunds btSetPeriod ~
btClear btGo bAgentLookup flAgentID fPostFrom fPostTo fSearch tbIncludeVoid ~
RECT-77 RECT-85 RECT-80 RECT-81 RECT-83 
&Scoped-Define DISPLAYED-OBJECTS fVoidDate flAgentID flName fPostFrom ~
fPostTo fSearch tbIncludeVoid 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getType eC-Win 
FUNCTION getType RETURNS CHARACTER
  ( ipctype as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getVoid eC-Win 
FUNCTION getVoid RETURNS CHARACTER
  ( ipVoid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged eC-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validAgent eC-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR eC-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwarpayments 
       MENU-ITEM m_View_Detail  LABEL "Transaction Details"
       MENU-ITEM m_Edit_Refund_Note LABEL "Edit Refund Note".


/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup  NO-FOCUS
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Agent lookup".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.67 TOOLTIP "Export Data".

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.67 TOOLTIP "Modify refund".

DEFINE BUTTON bOpen  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.67 TOOLTIP "Open detail".

DEFINE BUTTON bPrelimRpt  NO-FOCUS
     LABEL "Prelim" 
     SIZE 7.2 BY 1.71 TOOLTIP "Preliminary Report".

DEFINE BUTTON btClear 
     IMAGE-UP FILE "images/s-cross.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 4.8 BY 1.14 TOOLTIP "Blank out the date range".

DEFINE BUTTON btGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE BUTTON btSetPeriod 
     IMAGE-UP FILE "images/s-calendar.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 4.8 BY 1.14 TOOLTIP "Set current open period as date range".

DEFINE BUTTON btVoid  NO-FOCUS
     LABEL "Void" 
     SIZE 7.2 BY 1.71 TOOLTIP "Void transaction".

DEFINE VARIABLE flAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE fPostFrom AS DATE FORMAT "99/99/99":U 
     LABEL "Posted" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fPostTo AS DATE FORMAT "99/99/99":U 
     LABEL "To" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 25 BY .95 TOOLTIP "Enter Check/Reference or Refund ID" NO-UNDO.

DEFINE VARIABLE fVoidDate AS DATE FORMAT "99/99/99":U 
     LABEL "Use Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-77
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 24.8 BY 3.1.

DEFINE RECTANGLE RECT-80
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 127 BY 3.1.

DEFINE RECTANGLE RECT-81
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE .6 BY .05.

DEFINE RECTANGLE RECT-83
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE .6 BY 2.29.

DEFINE RECTANGLE RECT-85
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 42 BY 3.1.

DEFINE VARIABLE tbIncludeVoid AS LOGICAL INITIAL no 
     LABEL "Include Voided" 
     VIEW-AS TOGGLE-BOX
     SIZE 18 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwrefunds FOR 
      unappliedtran SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwrefunds
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwrefunds eC-Win _FREEFORM
  QUERY brwrefunds DISPLAY
      unappliedtran.entityID                                      column-label  "Agent ID"            format "x(15)"
unappliedtran.entityName                                    column-label  "Name"                format "x(65)"
getType(unappliedtran.type) @  unappliedtran.type           column-label  "Type"                format "x(15)   "
unappliedtran.reference                                     column-label  "Check/!Reference"    format "x(14)"  
unappliedtran.referenceamt                                  column-label  "Check!Amount"        format ">,>>>,>>9.99"   width 15  
unappliedtran.filenumber                                    column-label  "File"                format "x(15)" width 20
unappliedtran.sourceID                                      column-label  "Refund ID"           format "x(15)"  
unappliedtran.transDate                                     column-label  "Posted"              format "99/99/99   "   width 15  
unappliedtran.refundAmt                                     column-label  "Refunded!Amount"     format "->,>>>,>>9.99" width 15  
getVoid(unappliedtran.void) @ unappliedtran.void            column-label  "Voided"              format "x(8)"  width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 217 BY 17.67 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bPrelimRpt AT ROW 2.19 COL 186.6 WIDGET-ID 430 NO-TAB-STOP 
     btVoid AT ROW 2.19 COL 179.6 WIDGET-ID 418 NO-TAB-STOP 
     bModify AT ROW 2.29 COL 137.6 WIDGET-ID 462 NO-TAB-STOP 
     bOpen AT ROW 2.29 COL 144.8 WIDGET-ID 446
     brwrefunds AT ROW 4.81 COL 2 WIDGET-ID 200
     bExport AT ROW 2.29 COL 130.4 WIDGET-ID 8
     fVoidDate AT ROW 2.52 COL 163.2 COLON-ALIGNED WIDGET-ID 438
     btSetPeriod AT ROW 3.05 COL 53 WIDGET-ID 472 NO-TAB-STOP 
     btClear AT ROW 3.05 COL 48.4 WIDGET-ID 470 NO-TAB-STOP 
     btGo AT ROW 2.14 COL 96.4 WIDGET-ID 474
     bAgentLookup AT ROW 1.95 COL 29.6 WIDGET-ID 454 NO-TAB-STOP 
     flAgentID AT ROW 2 COL 13.4 COLON-ALIGNED WIDGET-ID 352
     flName AT ROW 2 COL 32.4 COLON-ALIGNED NO-LABEL WIDGET-ID 424
     fPostFrom AT ROW 3.1 COL 13.4 COLON-ALIGNED WIDGET-ID 360
     fPostTo AT ROW 3.1 COL 32.4 COLON-ALIGNED WIDGET-ID 362
     fSearch AT ROW 3.1 COL 67.4 COLON-ALIGNED WIDGET-ID 426
     tbIncludeVoid AT ROW 2.57 COL 107 WIDGET-ID 396
     "Parameters" VIEW-AS TEXT
          SIZE 11.6 BY .62 AT ROW 1.29 COL 3 WIDGET-ID 482
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.24 COL 129.8 WIDGET-ID 194
     "Void" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1.24 COL 154.6 WIDGET-ID 440
     RECT-77 AT ROW 1.52 COL 128.8 WIDGET-ID 190
     RECT-85 AT ROW 1.52 COL 153.2 WIDGET-ID 420
     RECT-80 AT ROW 1.52 COL 2 WIDGET-ID 476
     RECT-81 AT ROW 1.86 COL 73.4 WIDGET-ID 478
     RECT-83 AT ROW 1.86 COL 104.4 WIDGET-ID 406
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 219.4 BY 24.05 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW eC-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Posted Refunds"
         HEIGHT             = 21.62
         WIDTH              = 219.2
         MAX-HEIGHT         = 34.43
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.43
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW eC-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwrefunds bOpen DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bExport IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bModify IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bOpen IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwrefunds:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwarpayments:HANDLE
       brwrefunds:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwrefunds:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR FILL-IN flName IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flName:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN fVoidDate IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(eC-Win)
THEN eC-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwrefunds
/* Query rebuild information for BROWSE brwrefunds
     _START_FREEFORM
open query {&SELF-NAME} for each unappliedtran.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwrefunds */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME eC-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eC-Win eC-Win
ON END-ERROR OF eC-Win /* Posted Refunds */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eC-Win eC-Win
ON WINDOW-CLOSE OF eC-Win /* Posted Refunds */
DO:
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eC-Win eC-Win
ON WINDOW-RESIZED OF eC-Win /* Posted Refunds */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup eC-Win
ON CHOOSE OF bAgentLookup IN FRAME DEFAULT-FRAME /* agentlookup */
DO:
  define variable cAgentID  as character no-undo.
  define variable cName     as character no-undo.
    
  run dialogagentlookup.w(input flAgentID:input-value,
                          input "",      /* Selected State ID */
                          input true,    /* Allow 'ALL' */
                          output cAgentID,
                          output std-ch, /* Agent state ID */
                          output cName,
                          output std-lo).
   
  if not std-lo 
   then
    return no-apply.
         
  empty temp-table artran.
  close query brwrefunds.
    
  run setWidgetState in this-procedure.
  
  assign
      flAgentID:screen-value = cAgentID
      flName:screen-value    = cName
      . 
  
  resultsChanged(false).
  
  if lDefaultAgent               and
     flAgentID:input-value <> "" and
     flAgentID:input-value <> {&ALL}
   then
    /* Set default AgentID */
    publish "SetDefaultAgent" (input flAgentID:input-value).
  
  if flAgentID:input-value <> ""   
   then                            
     run getData in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport eC-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify eC-Win
ON CHOOSE OF bModify IN FRAME DEFAULT-FRAME /* Modify */
DO:
  run modifyRefund in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOpen eC-Win
ON CHOOSE OF bOpen IN FRAME DEFAULT-FRAME /* Open */
DO:
  run viewDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrelimRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrelimRpt eC-Win
ON CHOOSE OF bPrelimRpt IN FRAME DEFAULT-FRAME /* Prelim */
do:
   run prelimRpt in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwrefunds
&Scoped-define SELF-NAME brwrefunds
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwrefunds eC-Win
ON DEFAULT-ACTION OF brwrefunds IN FRAME DEFAULT-FRAME
DO:
  run viewDetail in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwrefunds eC-Win
ON ROW-DISPLAY OF brwrefunds IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwrefunds eC-Win
ON START-SEARCH OF brwrefunds IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwrefunds eC-Win
ON VALUE-CHANGED OF brwrefunds IN FRAME DEFAULT-FRAME
DO:
  run setWidgetState in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClear eC-Win
ON CHOOSE OF btClear IN FRAME DEFAULT-FRAME
DO:
 fPostFrom:screen-value = "".
 fPostTo:screen-value = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGo eC-Win
ON CHOOSE OF btGo IN FRAME DEFAULT-FRAME /* Go */
OR 'RETURN' of flAgentID
DO:
  if not validAgent()
   then
    return no-apply.
    
  run getData in this-procedure.           
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btSetPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btSetPeriod eC-Win
ON CHOOSE OF btSetPeriod IN FRAME DEFAULT-FRAME
DO:
  fPostFrom:screen-value = string(dtFromDate).
  fPostTo:screen-value   = string(dtToDate).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btVoid
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btVoid eC-Win
ON CHOOSE OF btVoid IN FRAME DEFAULT-FRAME /* Void */
do:
  run voidRefunds in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID eC-Win
ON VALUE-CHANGED OF flAgentID IN FRAME DEFAULT-FRAME /* Agent ID */
DO:
  resultsChanged(false).
  
  assign
      flName:screen-value  = ""
      bOpen:sensitive      = false
      bExport:sensitive    = false
      btVoid:sensitive     = false
      bPrelimRpt:sensitive = false
      fVoidDate:sensitive  = false.
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fPostFrom
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fPostFrom eC-Win
ON VALUE-CHANGED OF fPostFrom IN FRAME DEFAULT-FRAME /* Posted */
DO:  
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fPostTo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fPostTo eC-Win
ON VALUE-CHANGED OF fPostTo IN FRAME DEFAULT-FRAME /* To */
DO:  
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch eC-Win
ON RETURN OF fSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch eC-Win
ON VALUE-CHANGED OF fSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Edit_Refund_Note
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Edit_Refund_Note eC-Win
ON CHOOSE OF MENU-ITEM m_Edit_Refund_Note /* Edit Refund Note */
DO:
  find current unappliedtran no-error.
  if available unappliedtran 
   then
    run modifyRefund in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Detail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Detail eC-Win
ON CHOOSE OF MENU-ITEM m_View_Detail /* Transaction Details */
DO:
  run viewDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tbIncludeVoid
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tbIncludeVoid eC-Win
ON VALUE-CHANGED OF tbIncludeVoid IN FRAME DEFAULT-FRAME /* Include Voided */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK eC-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow" anywhere.

bExport    :load-image             ("images/excel.bmp").
bExport    :load-image-insensitive ("images/excel-i.bmp").

bOpen      :load-image             ("images/open.bmp").
bOpen      :load-image-insensitive ("images/open-i.bmp").

bModify     :load-image             ("images/update.bmp").
bModify     :load-image-insensitive ("images/update-i.bmp").

btVoid      :load-image            ("images/rejected.bmp").
btVoid      :load-image-insensitive("images/rejected-i.bmp").

bPrelimRpt  :load-image            ("images/pdf.bmp").
bPrelimRpt  :load-image-insensitive("images/pdf-i.bmp").

btGo        :load-image             ("images/completed.bmp").
btGo        :load-image-insensitive ("images/completed-i.bmp").

bAgentLookup:load-image             ("images/s-lookup.bmp").
bAgentLookup:load-image-insensitive ("images/s-lookup-i.bmp").

btSetPeriod  :load-image             ("images/s-calendar.bmp").
btSetPeriod  :load-image-insensitive ("images/s-calendar-i.bmp").
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  {&window-name}:window-state = window-minimized.   
 
  run enable_UI.   
  
    /* Getting date range from first and last open active period */   
  publish "getOpenPeriod" (output dtFromDate,output dtToDate).    
  
  fPostFrom:screen-value = "".                                   
  fPostTo:screen-value   = "". 
  
  /* When default posting option from config screen is allowed */ 
  publish 'GetDefaultPostingOption' (output std-lo).
  if std-lo
   then
    do:
      /* Set default posting date on screen */
      publish "getDefaultPostingDate"(output std-da).
      fVoidDate:screen-value = string(std-da, "99/99/99").
    end.
    
  {lib/get-column-width.i &col="'entityname'"    &var=dColumnWidth}
  if refundID ne '0'
   then
    do:
      assign
          flAgentID      :sensitive = false
          fPostFrom      :sensitive = false
          fPostTo        :sensitive = false
          fSearch        :sensitive = false
          bAgentLookup   :sensitive = false
          btClear        :sensitive = false
          btSetPeriod    :sensitive = false
          tbIncludeVoid  :checked   = true
          fSearch     :screen-value  = refundID
          .
      
      run getData in this-procedure.
    end.
  /* Procedure restores the window and move it to top */
  
  run showWindow in this-procedure.
  run enabledisablevoid in this-procedure. 
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow eC-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI eC-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(eC-Win)
  THEN DELETE WIDGET eC-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enabledisablevoid eC-Win 
PROCEDURE enabledisablevoid :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
   
  if (query brwrefunds:num-results > 0)
   then
    assign
        fVoidDate:sensitive  = true
        btVoid:sensitive     = true
        bPrelimRpt:sensitive = true 
        .
  else
   assign
       fVoidDate:sensitive  = false
       btVoid:sensitive     = false
       bPrelimRpt:sensitive = false 
       .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI eC-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fVoidDate flAgentID flName fPostFrom fPostTo fSearch tbIncludeVoid 
      WITH FRAME DEFAULT-FRAME IN WINDOW eC-Win.
  ENABLE bPrelimRpt btVoid brwrefunds btSetPeriod btClear btGo bAgentLookup 
         flAgentID fPostFrom fPostTo fSearch tbIncludeVoid RECT-77 RECT-85 
         RECT-80 RECT-81 RECT-83 
      WITH FRAME DEFAULT-FRAME IN WINDOW eC-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW eC-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData eC-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwrefunds:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
 
  
  for each unappliedtran:
    assign
        unappliedtran.type = getType(unappliedtran.type)
        unappliedtran.entity =  getEntityType(unappliedtran.entity)
        .
  end.
  
  publish "GetReportDir" (output std-ch).
  
  std-ha = temp-table unappliedtran:handle.
  run util/exporttable.p (table-handle std-ha,
                          "unappliedtran",
                          "for each unappliedtran",
                          "entity,entityID,entityname,type,reference,receiptdate,referencedate,transdate,referenceamt,appliedamt,remainingamt,refundamt,filenumber,revenuetype,totalrefundAmt",
                          "Entity,Entity ID,Entity Name,Type,Check/Reference,Receipt Date,Check Date,Post Date,Check Amount,Applied,Unapplied,Refund Amount,File Number,Revenue Type,Total Refund",
                          std-ch,
                          "Payments-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData eC-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwrefunds.
  empty temp-table unappliedtran.
  
  do with frame {&frame-name}:
  end.

  define buffer ttunappliedtran for ttunappliedtran.

  empty temp-table unappliedtran.

  for each ttunappliedtran:
    create unappliedtran.
    buffer-copy ttunappliedtran to unappliedtran.
  end.

  dataSortDesc = not dataSortDesc.
  if dataSortBy = ""
   then dataSortBy = "entityID".
   
  run sortData in this-procedure (dataSortBy).

  if can-find(first unappliedtran) 
   then
    find first unappliedtran no-error.
  
  setStatusCount(query brwrefunds:num-results).    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData eC-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  empty temp-table ttunappliedtran .
    
  if flAgentID:screen-value = "" or flAgentID:screen-value = "ALL" /* using date filter only when particular agentId is not entered*/
   then
    do:
     if (fPostFrom:input-value = ? or fPostTo:input-value = ?) and fSearch:input-value = ""
      then
       do:
        message "You must enter date range and/or search string when searching for all agents."
             view-as alert-box.
         return.
       end.
    end.
  
 {lib/brw-totalData.i &noShow=true}
  close query brwrefunds.
  
  
  run server\queryRefunds.p(input '',
                            input flAgentID:screen-value,
                            input fPostFrom:input-value, /* Post from date */
                            input fPostTo:input-value,   /* Post to date   */
                            input fSearch:input-value,
                            input tbIncludeVoid:checked,
                            output table ttunappliedtran,    
                            output std-lo,                  
                            output std-ch).
         
  if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box info buttons ok.
      return.
    end.
 
  run filterdata in this-procedure.
  run setWidgetState in this-procedure.
           
  setStatusRecords(query brwrefunds:num-results). 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyRefund eC-Win 
PROCEDURE modifyRefund :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable iarrefundID  as integer  no-undo.
 
 do with frame {&frame-name}:
 end.                        
 
 if not available unappliedtran
  then
   return.
                                  
 /* Client Server Call */
 run server/gettransaction.p (input integer(unappliedtran.arTranID), /* Type */
                              input /*unappliedtran.tranID*/ 0,      /*tranID*/
                              input /*unappliedtran.type*/ '',       /*Type*/
                              output table ARTran,
                              output std-lo,
                              output std-ch).
                              
 if not std-lo
  then
   do:
      message std-ch
         view-as alert-box error buttons ok.
      return.
   end.
 
 for first artran exclusive-lock where artran.artranId = unappliedtran.arTranID:
   assign 
       artran.totalrefundamt = abs(unappliedtran.totalrefundamt)
       artran.appliedamtbyref = abs(unappliedtran.appliedamt)
       .
 end.
 
 run dialogviewrefund.w (input {&ModifyPosted}, /* notes modify */
                         input-output table arTran,
                         output std-lo).
                         
 if not std-lo   
  then           
   return.       
                 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openApply eC-Win 
PROCEDURE openApply :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available unappliedtran
   then 
    return.
   
  /* Show records based on the parameter list */
  publish "SetCurrentValue" ("ApplyParams", unappliedtran.entityID + "|" + getType(unappliedtran.type) + "|" + string(unappliedtran.artranID)).
  
  publish "OpenWindow" (input "wapply", 
                        input getType(unappliedtran.type) + "|" + string(unappliedtran.artranID), 
                        input "wapply.w", 
                        input ?,                                   
                        input this-procedure).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE prelimRpt eC-Win 
PROCEDURE prelimRpt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   define variable cTranList as character no-undo.
 
  if not available unappliedtran
   then
    return.
    
  run server/prelimrefundvoidgl.p (input integer(unappliedtran.unappliedtranID),
                                   input  unappliedtran.artranID,
                                   input  abs(unappliedtran.refundAmt),
                                   output table ledgerreport,
                                   output std-lo,
                                   output std-ch).
      if not std-lo
       then
        do:
          message std-ch
             view-as alert-box error buttons ok.
          return.
        end.


      if  unappliedtran.type = {&Payment}
       then
        run util\arpaymentrefundvoidpdf.p (input {&view},
                                           input table ledgerreport,
                                           output std-ch).

      else  /*unappliedtran.reference begins "Credit"*/
       run util\arcreditrefundvoidpdf.p (input {&view},
                                         input table ledgerreport,
                                         output std-ch).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState eC-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
 if not available unappliedtran
    then
     return.
    
  assign
        bExport:sensitive = if (query brwrefunds:num-results = ?) then false else (query brwrefunds:num-results > 0)
        bOpen  :sensitive = if (query brwrefunds:num-results = ?) then false else (query brwrefunds:num-results > 0)
        bModify:sensitive = if (query brwrefunds:num-results = ?) then false else (query brwrefunds:num-results > 0)
        fVoidDate:sensitive = if (query brwrefunds:num-results = ?) or unappliedtran.void = true then false else (query brwrefunds:num-results > 0)
        btVoid:sensitive = if (query brwrefunds:num-results = ?) or unappliedtran.void = true then false else (query brwrefunds:num-results > 0)
        bPrelimRpt:sensitive = if (query brwrefunds:num-results = ?) or unappliedtran.void = true then false else (query brwrefunds:num-results > 0)
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow eC-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData eC-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by unappliedtran.entityID by unappliedtran.type".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
  {lib/brw-totalData.i &excludeColumn="2,3,4,5,6,7,10,11,12"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewDetail eC-Win 
PROCEDURE viewDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if available unappliedtran
    then
     publish "OpenWindow" (input "wtransactiondetail",     /*childtype*/
                           input string(unappliedtran.unappliedtranID),    /*childid*/
                           input "wtransactiondetail.w",   /*window*/
                           input "integer|input|0^integer|input|" + string(unappliedtran.unappliedtranID) + "^character|input|" + unappliedtran.type,    /*parameters*/
                           input this-procedure).          /*currentProcedure handle*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE voidRefunds eC-Win 
PROCEDURE voidRefunds :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lViewPdf          as logical no-undo.
  define variable iSelectedArTranID as integer no-undo.
  define variable cType             as character no-undo.
 
  do with frame {&frame-name}:
  end.
  
  if not available(unappliedtran) 
   then
    return.
    
  assign
      cType = unappliedtran.type
      iSelectedArTranID = unappliedtran.artranID
      .
  
  if fVoidDate:input-value = ?
   then
    do:
      message "Void date cannot be blank."
        view-as alert-box error buttons ok.
      return.      
    end.
    
  if fVoidDate:input-value > today
   then
    do:
      message "Void date cannot be in future."
          view-as alert-box error buttons ok.    
      return.
    end.
    
  if fVoidDate:input-value < date(unappliedtran.transdate)
    then
      do:
         message  "Void date can not be prior to posting date."
          view-as alert-box error buttons ok.
         return.
     end. 
  
  run dialogviewledger.w (input "Void refund",output std-lo).  /*post*/
     
  if not std-lo
   then
    return.
    
  publish "GetViewPdf" (output lViewPdf).  
  

  run server\voidtransaction.p(input unappliedtran.ArTranID,
                               input 0,
                               input fVoidDate:input-value,
                               input lViewPdf,
                               output table ledgerreport,
                               output table glPaymentDetail, /* needed for deposit void */
                               output table tArTran,
                               output std-lo,
                               output std-ch).
  if not std-lo 
  then
   do:
     message std-ch
         view-as alert-box error buttons ok.
     return.
   end.
  else
   message "Void was successful."
       view-as alert-box information buttons ok.
           
  /*Viewing Pdf*/
  if lViewPdf
   then
    do:
      if cType = {&Payment}
       then
        run util\arpaymentrefundvoidpdf.p (input {&view},
                                           input table ledgerreport,
                                           output std-ch).

      else  /*unappliedtran.reference begins "Credit"*/
       run util\arcreditrefundvoidpdf.p (input {&view},
                                         input table ledgerreport,
                                         output std-ch).
      
    end.                                           
    
            
  find first  unappliedtran where unappliedtran.artranid = iSelectedArTranID no-error.
  if available unappliedtran
   then
    do:
      if tbIncludeVoid:checked in frame {&frame-name}
       then
        unappliedtran.void = true .
      else
       delete unappliedtran.
    end.

  open query brwrefunds preselect each unappliedtran by unappliedtran.entityid by unappliedtran.type by unappliedtran.tranID.
 run setWidgetState in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized eC-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 14
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 28
      .
 
  run ShowScrollBars(frame {&frame-name}:handle, no, no).  
  {lib/resize-column.i &col="'entityName'"    &var=dColumnWidth}
  {lib/brw-totalData.i &excludeColumn="2,3,4,5,6,7,10,11,12"}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getType eC-Win 
FUNCTION getType RETURNS CHARACTER
  ( ipctype as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if ipctype = 'P'
   then 
    return "Payment".
  else
   return "Credit".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getVoid eC-Win 
FUNCTION getVoid RETURNS CHARACTER
  ( ipVoid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if unappliedtran.void 
   then 
    return "Yes".
  else
   return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged eC-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validAgent eC-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if flAgentID:input-value = {&ALL}
   then
    flName:screen-value = {&NotApplicable}.
       
  else if flAgentID:input-value <> {&ALL} and
      flAgentID:input-value <> ""
   then
    do:
      publish "getAgentName" (input flAgentID:input-value,
                              output std-ch,
                              output std-lo).                                               
      if not std-lo 
       then 
        do:
          assign 
              flAgentID:screen-value = "" 
              flName:screen-value    = ""
              .
          return false. /* Function return value. */
        end.
      flName:screen-value = std-ch.
    end. 
  
  resultsChanged(false).  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

