&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME eC-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS eC-Win 
/*------------------------------------------------------------------------
  File: wpayments.w

  Description: Window for AR Payment

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Shubham

  Created: 02.07.2020
  
  @Modified :
  Date        Name     Description   
  07/27/2020  AG       Modified to use arpmt.tranDate instead of arpmt.postDate
  01/21/2021  Shefali  Modified to add pop-up menu "View Transaction Detail".
  02/22/2021  Shefali  Modified to show yes/blank in void column in grid"
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/ar-def.i}
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/winshowscrollbars.i}
{lib/get-column.i}
{lib/brw-totalData-def.i}
{lib/ar-getentitytype.i} /* Include function: getEntityType */

/* Temp-table Definition */
{tt/arpmt.i }
{tt/arpmt.i &tableAlias="tarpmt"}
{tt/arpmt.i &tableAlias="ttarpmt"}

define input parameter depositID  as integer   no-undo. 

define variable dColumnWidth    as decimal   no-undo.
define variable lDefaultAgent   as logical   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwarpayments

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES arpmt

/* Definitions for BROWSE brwarpayments                                 */
&Scoped-define FIELDS-IN-QUERY-brwarpayments getEntityType(arpmt.entity) @ arpmt.entity arpmt.entityID arpmt.entityName arpmt.checknum arpmt.receiptdate arpmt.checkdate arpmt.transDate arpmt.checkamt arpmt.appliedAmt arpmt.remainingAmt abs(arpmt.refundAmt) @ arpmt.refundAmt getVoid(arpmt.void) @ arpmt.void arpmt.filenumber arpmt.revenuetype   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwarpayments   
&Scoped-define SELF-NAME brwarpayments
&Scoped-define QUERY-STRING-brwarpayments for each arpmt
&Scoped-define OPEN-QUERY-brwarpayments open query {&SELF-NAME} for each arpmt.
&Scoped-define TABLES-IN-QUERY-brwarpayments arpmt
&Scoped-define FIRST-TABLE-IN-QUERY-brwarpayments arpmt


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwarpayments}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btFilter cbAgent brwarpayments RECT-77 ~
RECT-79 RECT-88 
&Scoped-Define DISPLAYED-OBJECTS fDepositRef cbAgent 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getVoid eC-Win 
FUNCTION getVoid RETURNS CHARACTER
  ( ipVoid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged eC-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR eC-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwarpayments 
       MENU-ITEM m_View_Detail  LABEL "Transaction Details"
       MENU-ITEM m_Edit_Payment_Note LABEL "Edit Payment Note"
       MENU-ITEM m_Apply_Payment LABEL "Apply Payment" .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.67 TOOLTIP "Export Data".

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.67 TOOLTIP "Modify payment".

DEFINE BUTTON bOpen  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.67 TOOLTIP "Open detail".

DEFINE BUTTON btFilter  NO-FOCUS
     LABEL "Filter" 
     SIZE 7.2 BY 1.67 TOOLTIP "Reset filters".

DEFINE VARIABLE cbAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 61.8 BY 1 NO-UNDO.

DEFINE VARIABLE fDepositRef AS CHARACTER FORMAT "X(256)":U 
     LABEL "Deposit Ref" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 24 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-77
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 26.8 BY 2.38.

DEFINE RECTANGLE RECT-79
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 81.6 BY 2.38.

DEFINE RECTANGLE RECT-88
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 40.4 BY 2.38.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwarpayments FOR 
      arpmt SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwarpayments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwarpayments eC-Win _FREEFORM
  QUERY brwarpayments DISPLAY
      getEntityType(arpmt.entity) @ arpmt.entity    label  "Agent/Org."       format "x(10)"  
arpmt.entityID                                      label  "ID"               format "x(15)"
arpmt.entityName                                    label  "Name"             format "x(42)"
arpmt.checknum                               column-label  "Check/!Reference" format "x(12)"
arpmt.receiptdate                                   label  "Receipt"          format "99/99/99   "
arpmt.checkdate                              column-label  "Check"            format "99/99/99   "
arpmt.transDate                              column-label  "Post"             format "99/99/99   "
arpmt.checkamt                               column-label  "Check!Amount"     format ">,>>>,>>9.99"
arpmt.appliedAmt                             column-label  "Applied"          format "->,>>>,>>9.99"
arpmt.remainingAmt                           column-label  "Unapplied"        format ">,>>>,>>9.99"
abs(arpmt.refundAmt) @ arpmt.refundAmt       column-label  "Refund!Amount"    format "->,>>>,>>9.99"
getVoid(arpmt.void) @ arpmt.void             column-label  "Voided"           format "x(8)"
arpmt.filenumber                                    label  "File"             format "x(30)" width 15
arpmt.revenuetype                            column-label  "Revenue!Type"     format "x(20)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 226.6 BY 18.55 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bModify AT ROW 1.81 COL 133.6 WIDGET-ID 462 NO-TAB-STOP 
     bOpen AT ROW 1.81 COL 141.6 WIDGET-ID 446
     fDepositRef AT ROW 2.19 COL 14.8 COLON-ALIGNED WIDGET-ID 274
     btFilter AT ROW 1.81 COL 115 WIDGET-ID 262
     cbAgent AT ROW 2.19 COL 49.8 COLON-ALIGNED WIDGET-ID 246
     brwarpayments AT ROW 4.24 COL 2.4 WIDGET-ID 200
     bExport AT ROW 1.81 COL 125.6 WIDGET-ID 8
     "Filter" VIEW-AS TEXT
          SIZE 5.2 BY .62 AT ROW 1.19 COL 43.8 WIDGET-ID 266
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.19 COL 125 WIDGET-ID 194
     RECT-77 AT ROW 1.48 COL 124 WIDGET-ID 190
     RECT-79 AT ROW 1.48 COL 42.6 WIDGET-ID 270
     RECT-88 AT ROW 1.48 COL 2.6 WIDGET-ID 276
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 228.6 BY 24.05 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW eC-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Payments"
         HEIGHT             = 22.05
         WIDTH              = 229
         MAX-HEIGHT         = 34.43
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.43
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW eC-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwarpayments cbAgent DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bExport IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bModify IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bOpen IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwarpayments:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwarpayments:HANDLE
       brwarpayments:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwarpayments:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR FILL-IN fDepositRef IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(eC-Win)
THEN eC-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwarpayments
/* Query rebuild information for BROWSE brwarpayments
     _START_FREEFORM
open query {&SELF-NAME} for each arpmt.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwarpayments */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME eC-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eC-Win eC-Win
ON END-ERROR OF eC-Win /* Payments */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eC-Win eC-Win
ON WINDOW-CLOSE OF eC-Win /* Payments */
DO:
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eC-Win eC-Win
ON WINDOW-RESIZED OF eC-Win /* Payments */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport eC-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify eC-Win
ON CHOOSE OF bModify IN FRAME DEFAULT-FRAME /* Modify */
DO:
  run modifyPayment in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOpen eC-Win
ON CHOOSE OF bOpen IN FRAME DEFAULT-FRAME /* Open */
DO:
  run viewDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwarpayments
&Scoped-define SELF-NAME brwarpayments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarpayments eC-Win
ON DEFAULT-ACTION OF brwarpayments IN FRAME DEFAULT-FRAME
DO:
  run viewDetail in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarpayments eC-Win
ON ROW-DISPLAY OF brwarpayments IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarpayments eC-Win
ON START-SEARCH OF brwarpayments IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btFilter eC-Win
ON CHOOSE OF btFilter IN FRAME DEFAULT-FRAME /* Filter */
DO:
  cbAgent:screen-value = {&ALL}.
  
  run filterData in this-procedure.
  run setwidgetstate  in this-procedure. 
  
  if cbAgent:screen-value ne {&ALL}
   then
    btFilter:sensitive = true.
   else  
    btFilter:sensitive = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgent eC-Win
ON VALUE-CHANGED OF cbAgent IN FRAME DEFAULT-FRAME /* Agent */
DO:
  run setfilterButton in this-procedure.
  run filterdata      in this-procedure.
  run setwidgetstate  in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Apply_Payment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Apply_Payment eC-Win
ON CHOOSE OF MENU-ITEM m_Apply_Payment /* Apply Payment */
DO:
  run openApply in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Edit_Payment_Note
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Edit_Payment_Note eC-Win
ON CHOOSE OF MENU-ITEM m_Edit_Payment_Note /* Edit Payment Note */
DO:
  find current arpmt no-error.
  if available arpmt 
   then
    run modifyPayment in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Detail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Detail eC-Win
ON CHOOSE OF MENU-ITEM m_View_Detail /* Transaction Details */
DO:
  run viewDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK eC-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow" anywhere.

btFilter   :load-image            ("images/filtererase.bmp").              
btFilter   :load-image-insensitive("images/filtererase-i.bmp").

bExport    :load-image             ("images/excel.bmp").
bExport    :load-image-insensitive ("images/excel-i.bmp").

bOpen      :load-image             ("images/open.bmp").
bOpen      :load-image-insensitive ("images/open-i.bmp").

bModify     :load-image             ("images/update.bmp").
bModify     :load-image-insensitive ("images/update-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  {&window-name}:window-state = window-minimized.   
 
  run enable_UI.   
  
  {lib/get-column-width.i &col="'entityname'"    &var=dColumnWidth}
      
  run getData in this-procedure.
  /* Procedure restores the window and move it to top */
  
  run showWindow in this-procedure.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow eC-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI eC-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(eC-Win)
  THEN DELETE WIDGET eC-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData eC-Win 
PROCEDURE displayData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define buffer ttarpmt for ttarpmt.
  
  /* Set the filters based on the data returned, with ALL as the first option */
  for each ttarpmt 
    break by ttarpmt.entityID:
    if first-of(ttarpmt.entityID) and ttarpmt.entityID <> ""
     then 
      cbAgent:add-last(replace(ttarpmt.entityname,",","") + " (" + ttarpmt.entityID + ")",ttarpmt.entityID).
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI eC-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fDepositRef cbAgent 
      WITH FRAME DEFAULT-FRAME IN WINDOW eC-Win.
  ENABLE btFilter cbAgent brwarpayments RECT-77 RECT-79 RECT-88 
      WITH FRAME DEFAULT-FRAME IN WINDOW eC-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW eC-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData eC-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwarpayments:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table arpmt:handle.
  run util/exporttable.p (table-handle std-ha,
                          "arpmt",
                          "for each arpmt",
                          "entity,entityID,entityname,checknum,receiptdate,checkdate,transdate,checkamt,appliedamt,remainingamt,refundamt,filenumber,revenuetype",
                          "Entity,Entity ID,Entity Name,Check Number,Receipt Date,Check Date,Post Date,Check Amount,Applied,Unapplied,Refund Amount,File Number,Revenue Type",
                          std-ch,
                          "Payments-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData eC-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwarpayments.
  empty temp-table arpmt.
  
  do with frame {&frame-name}:
  end.

  define buffer ttarpmt for ttarpmt.

  empty temp-table arpmt.

  for each ttarpmt 
    where ttarpmt.entityID  = (if cbAgent:input-value = {&all} then ttarpmt.entityID else cbAgent:input-value):
    create arpmt.
    buffer-copy ttarpmt to arpmt.
  end.

  dataSortDesc = not dataSortDesc.
  if dataSortBy = ""
   then dataSortBy = "entityID".
   
  run sortData in this-procedure (dataSortBy).

  if can-find(first arpmt) 
   then
    find first arpmt no-error.
  
  setStatusCount(query brwarpayments:num-results).    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData eC-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define buffer ttarpmt for ttarpmt.
 
 {lib/brw-totalData.i &noShow=true}
  close query brwarpayments.
  
  run server\querypayments.p (input depositID,                            
                              output table ttarpmt,
                              output std-lo,
                              output std-ch).
             
  if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box info buttons ok.
      return.
    end.
    
  for first ttarpmt:
    fDepositRef:screen-value = ttarpmt.depositref.
  end.
  
  run resetFilter in this-procedure.
  run setfilterButton in this-procedure.
  run filterdata in this-procedure.
  run displayData in this-procedure.
  run setWidgetState in this-procedure.
           
  setStatusRecords(query brwarpayments:num-results). 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyPayment eC-Win 
PROCEDURE modifyPayment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iarpmtID  as integer  no-undo.
  
  do with frame {&frame-name}:
  end.
  
  define buffer tarpmt for tarpmt.
  empty temp-table tarpmt.

  find current arpmt no-error.
  if not available arpmt 
   then
    return.
  
  create tarpmt.
  buffer-copy arpmt to tarpmt.
  
  iArPmtID = arpmt.arpmtID.   
  
  run dialogmodifypayment.w (input {&ModifyPosted},  /* Only notes modify */
                             input-output table tarpmt,                                                        
                             output std-lo).

  if not std-lo 
   then
    return.
    
  find first tarpmt no-error.
  if (not available tarpmt)
   then
    return.    
    
  find first ttarpmt where ttarpmt.arpmtID = iArPmtID no-error.
  if available ttarpmt
   then
    buffer-copy tarpmt to ttarpmt no-error.

  run filterData in this-procedure.  
   
  find first arpmt where arpmt.arPmtID = ttarpmt.arPmtID no-error.
  
  if not available arpmt
   then
    return.
  
  reposition {&browse-name} to rowid rowid(arpmt).
  
  apply 'value-changed' to browse brwarpayments.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openApply eC-Win 
PROCEDURE openApply :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available arpmt
   then 
    return.
   
  /* Show records based on the parameter list */
  publish "SetCurrentValue" ("ApplyParams", arpmt.entityID + "|" + {&Payment} + "|" + string(arpmt.artranID)).
  
  publish "OpenWindow" (input "wapply", 
                        input {&Payment} + "|" + string(arpmt.artranID), 
                        input "wapply.w", 
                        input ?,                                   
                        input this-procedure).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resetFilter eC-Win 
PROCEDURE resetFilter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  /* Initialise filters to default values */
  assign
      cbAgent:list-item-pairs        = {&ALL} + "," + {&ALL}
      cbAgent:screen-value           = {&all}                 
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setfilterButton eC-Win 
PROCEDURE setfilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
  
 if cbAgent:screen-value ne {&ALL}
  then
   btFilter:sensitive = true.
  else   
   btFilter:sensitive = false.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState eC-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  assign
        bExport:sensitive = if (query brwarpayments:num-results = ?) then false else (query brwarpayments:num-results > 0)
        bOpen  :sensitive = if (query brwarpayments:num-results = ?) then false else (query brwarpayments:num-results > 0)
        bModify:sensitive = if (query brwarpayments:num-results = ?) then false else (query brwarpayments:num-results > 0)
        .
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow eC-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData eC-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by arpmt.entityID ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
  {lib/brw-totalData.i &excludeColumn="2,3,4,5,6,7,12,13"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewDetail eC-Win 
PROCEDURE viewDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if available arPmt
    then
     publish "OpenWindow" (input "wtransactiondetail",     /*childtype*/
                           input string(arPmt.arPmtID),    /*childid*/
                           input "wtransactiondetail.w",   /*window*/
                           input "integer|input|0^integer|input|" + string(arPmt.arPmtID) + "^character|input|" + {&Payment},    /*parameters*/                               
                           input this-procedure).          /*currentProcedure handle*/ 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized eC-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 14
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 28
      .
 
  run ShowScrollBars(frame {&frame-name}:handle, no, no).  
  {lib/resize-column.i &col="'entityName'"    &var=dColumnWidth}
  {lib/brw-totalData.i &excludeColumn="2,3,4,5,6,7,12,13"}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getVoid eC-Win 
FUNCTION getVoid RETURNS CHARACTER
  ( ipVoid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if arpmt.void 
   then 
    return "Yes".
  else
   return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged eC-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

