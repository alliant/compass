&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: war.w

  Description: Main window for AR application

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul Sharma

  Created: 07.10.2019
  
  Modified :
  Date          Name           Description
  01/13/2021    Shefali        Modified to add filter "Type" combo-box in UI.
  03/30/2021    Shefali        Modified to display agents that are of status:
                               Active, Closed, or Cancelled in main screen.
  04/20/2021    Shefali        Modified to add new utility "Policy Audit" 
                               functionality.
  07/02/2021    Shefali        Modified to add new utility "Write-off" 
                               functionality.
  02/17/2023    Sachin Anthwal Task-102072-Implement posted for order invoices.
  05/14/2023    Shefali        GL Report for Production Invoices.
  02/17/2023    Shefali        Remove changes for - Task-102072-Implement posted for order invoices.
  05/20/2024    S Chandu       Modified to add new reference "State Source" UI.
  06/10/2024    s Chandu       Modified to add new report "Production File Activity"
                               functionality.
  07/12/2024    S Chandu       Modified Production File screen.
  11/21/2024    S Chandu       Modified to add new report "Generate Production Activity".
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/winshowscrollbars.i}

{lib/ar-def.i}
{tt/state.i}
{tt/agent.i}
{tt/period.i}
{tt/agent.i &tableAlias="ttAgent"}
{lib/get-column.i}

/* Handles for persistently called procedures */
define variable hPeriod             as handle    no-undo.
define variable hAgent              as handle    no-undo. 
define variable hDestination        as handle    no-undo. 

/* Local Variables */
define variable dColumnWidth        as decimal    no-undo.
define variable chSysPropDesc       as character  no-undo.

define variable cLastState          as character  no-undo.
define variable cLastStatus         as character  no-undo.
define variable cLastManager        as character  no-undo.
define variable cLastSearchString   as character  no-undo.
define variable lRefresh            as logical    no-undo.
define variable lTypeChanged        as logical    no-undo.
define variable lApplySearchString  as logical    no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwarAring

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttagent

/* Definitions for BROWSE brwarAring                                    */
&Scoped-define FIELDS-IN-QUERY-brwarAring ttagent.stateID ttagent.stat ttagent.agentID ttagent.name ttagent.managerDesc ttagent.Contactname ttagent.ContactPhone ttagent.ContactEmail   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwarAring   
&Scoped-define SELF-NAME brwarAring
&Scoped-define QUERY-STRING-brwarAring for each ttagent
&Scoped-define OPEN-QUERY-brwarAring open query {&SELF-NAME} for each ttagent.
&Scoped-define TABLES-IN-QUERY-brwarAring ttagent
&Scoped-define FIRST-TABLE-IN-QUERY-brwarAring ttagent


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwarAring}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbState bExport cbManager cbStatus ~
bApplyCredit fSearch brwarAring bApplyPayment bLedgerGL bpostedBatch ~
bPostedCredit bPostedDeposit bPostedInvoice bPostedPayment bUnpostedBatch ~
bUnpostedCredit bUnpostedDeposit bUnpostedInvoice bUnpostedPayment bRefresh ~
RECT-88 RECT-78 
&Scoped-Define DISPLAYED-OBJECTS cbState cbManager cbStatus fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getInvoiceStatus C-Win 
FUNCTION getInvoiceStatus RETURNS CHARACTER
  (cStat as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getManagerName C-Win 
FUNCTION getManagerName RETURNS CHARACTER
  ( input ipcUID as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VARIABLE C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_Module 
       MENU-ITEM m_Configure    LABEL "Configure"     
       MENU-ITEM m_About        LABEL "About"         
       RULE
       MENU-ITEM m_Exit         LABEL "E&xit"         .

DEFINE SUB-MENU m_Batches 
       MENU-ITEM m_Unposted_Batches LABEL "Unposted Batches"
       MENU-ITEM m_Posted_Batches LABEL "Posted Batches"
       RULE
       MENU-ITEM m_File_Details LABEL "File Detail"   
       MENU-ITEM m_Policy_Details LABEL "Policy Detail" .

DEFINE SUB-MENU m_Deposits 
       MENU-ITEM m_Unposted_Deposits LABEL "Unposted Deposits"
       MENU-ITEM m_Posted_Deposits LABEL "Posted Deposits".

DEFINE SUB-MENU m_Payments 
       MENU-ITEM m_Unposted_Payments LABEL "Unposted Payments"
       MENU-ITEM m_Posted_Payments LABEL "Posted Payments"
       RULE
       MENU-ITEM m_Apply_Payment LABEL "Apply Payment" 
       MENU-ITEM m_Production_Apply_Payment LABEL "Apply Payment to Production Files"
       MENU-ITEM m_Unapplied_Payment LABEL "Unapplied Payments"
       MENU-ITEM m_Apply_Multiple_Payments LABEL "Bulk Apply to Single Invoice".

DEFINE SUB-MENU m_Credits 
       MENU-ITEM m_Unposted_Credits LABEL "Unposted Credits"
       MENU-ITEM m_Posted_Credits LABEL "Posted Credits"
       RULE
       MENU-ITEM m_Apply_Credit LABEL "Apply Credit"  
       MENU-ITEM m_Unapplied_Credits LABEL "Unapplied Credits".

DEFINE SUB-MENU m_Invoices 
       MENU-ITEM m_Unposted_Invoices LABEL "Unposted Invoices"
       MENU-ITEM m_Posted_Invoices LABEL "Posted Invoices".

DEFINE SUB-MENU m_Adjustments 
       MENU-ITEM m_Refund       LABEL "Refund"        
       MENU-ITEM m_Posted_Refunds LABEL "Posted Refunds"
       RULE
       MENU-ITEM m_Write-Off    LABEL "Write-Off"     
       MENU-ITEM m_Posted_Write-Offs LABEL "Posted Write-Offs".

DEFINE SUB-MENU m_View 
       MENU-ITEM m_Posted_Transactions LABEL "Posted Transactions"
       MENU-ITEM m_Production_Files LABEL "Production File"
       RULE
       MENU-ITEM m_Ledger       LABEL "Ledger"        
       MENU-ITEM m_Ledger_Details LABEL "Ledger Detail" .

DEFINE SUB-MENU m_Report 
       MENU-ITEM m_Invoice_Statement LABEL "Generate Agent Invoices"
       MENU-ITEM m_AR_Statement LABEL "Generate Agent Statements"
       MENU-ITEM m_Generate_Production_Activit LABEL "Generate Production Activity"
       RULE
       MENU-ITEM m_AR_Aging     LABEL "Aging"         
       RULE
       MENU-ITEM m_Files_with_unapplied_paymen LABEL "Paid Files with a Positive Balance"
       MENU-ITEM m_Overpayment_Files LABEL "Paid Files with a Negative Balance"
       RULE
       MENU-ITEM m_Period_Processing_GL LABEL "Period Processing GL Transactions"
       MENU-ITEM m_Period_Payments_GL_Report LABEL "Period Cash GL Transactions"
       MENU-ITEM m_Period_GL_Report LABEL "Period Misc GL Transactions"
       MENU-ITEM m_Period_Production_GL_Transa LABEL "Period Production GL Transactions"
       RULE
       MENU-ITEM m_Production_File_Activity LABEL "Production File Activity".

DEFINE SUB-MENU m_Utility 
       MENU-ITEM m_Historic_Transactions LABEL "Add History Transaction"
       MENU-ITEM m_Policy_Audit LABEL "Policy Audit"  .

DEFINE SUB-MENU m_References 
       MENU-ITEM m_Agents       LABEL "All Agents"    
       RULE
       MENU-ITEM m_Revenue      LABEL "Revenue Types" 
       MENU-ITEM m_State_Source LABEL "State Source"  
       MENU-ITEM m_State_Forms  LABEL "State Products"
       RULE
       MENU-ITEM m_Period       LABEL "Periods"       
       MENU-ITEM m_Note_Type    LABEL "Note Types"    
       RULE
       MENU-ITEM m_Agent_File   LABEL "Agent Files"   
       MENU-ITEM m_Destination  LABEL "Destinations"  .

DEFINE MENU MENU-BAR-C-Win MENUBAR
       SUB-MENU  m_Module       LABEL "Module"        
       SUB-MENU  m_Batches      LABEL "Batches"       
       SUB-MENU  m_Deposits     LABEL "Deposits"      
       SUB-MENU  m_Payments     LABEL "Payments"      
       SUB-MENU  m_Credits      LABEL "Credits"       
       SUB-MENU  m_Invoices     LABEL "Invoices"      
       SUB-MENU  m_Adjustments  LABEL "Adjustments"   
       SUB-MENU  m_View         LABEL "View"          
       SUB-MENU  m_Report       LABEL "Reports"       
       SUB-MENU  m_Utility      LABEL "Utility"       
       SUB-MENU  m_References   LABEL "R&eferences"   .

DEFINE MENU POPUP-MENU-brwarAring 
       MENU-ITEM m_Open_Transaction LABEL "Open Transactions"
       MENU-ITEM m_Posted_Transaction LABEL "Posted Transactions"
       RULE
       MENU-ITEM m_Person       LABEL "Contacts"      .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bApplyCredit  NO-FOCUS
     LABEL "ApplyCredit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Apply credit".

DEFINE BUTTON bApplyPayment  NO-FOCUS
     LABEL "ApplyPayment" 
     SIZE 7.2 BY 1.71 TOOLTIP "Apply payment".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON bLedgerGL  NO-FOCUS
     LABEL "Ledger" 
     SIZE 7.2 BY 1.71 TOOLTIP "Ledger".

DEFINE BUTTON bpostedBatch  NO-FOCUS
     LABEL "PostedBatch" 
     SIZE 7.2 BY 1.71 TOOLTIP "Posted batches".

DEFINE BUTTON bPostedCredit  NO-FOCUS
     LABEL "PostedCredits" 
     SIZE 7.2 BY 1.71 TOOLTIP "Posted credits".

DEFINE BUTTON bPostedDeposit  NO-FOCUS
     LABEL "PostedDeposits" 
     SIZE 7.2 BY 1.71 TOOLTIP "Posted deposits".

DEFINE BUTTON bPostedInvoice  NO-FOCUS
     LABEL "PostedInvoices" 
     SIZE 7.2 BY 1.71 TOOLTIP "Posted invoices".

DEFINE BUTTON bPostedPayment  NO-FOCUS
     LABEL "PostedPayments" 
     SIZE 7.2 BY 1.71 TOOLTIP "Posted payments".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh".

DEFINE BUTTON bResetFilter  NO-FOCUS
     LABEL "Reset" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reset filters".

DEFINE BUTTON bUnpostedBatch  NO-FOCUS
     LABEL "UnpostedBatch" 
     SIZE 7.2 BY 1.71 TOOLTIP "Unposted batches".

DEFINE BUTTON bUnpostedCredit  NO-FOCUS
     LABEL "UnpostedCredits" 
     SIZE 7.2 BY 1.71 TOOLTIP "Unposted credits".

DEFINE BUTTON bUnpostedDeposit  NO-FOCUS
     LABEL "UnpostedDeposits" 
     SIZE 7.2 BY 1.71 TOOLTIP "Unposted deposits".

DEFINE BUTTON bUnpostedInvoice  NO-FOCUS
     LABEL "UnpostedInvoices" 
     SIZE 7.2 BY 1.71 TOOLTIP "Unposted invoices".

DEFINE BUTTON bUnpostedPayment  NO-FOCUS
     LABEL "Unposted Payments" 
     SIZE 7.2 BY 1.71 TOOLTIP "Unposted payments".

DEFINE VARIABLE cbManager AS CHARACTER FORMAT "X(256)":U 
     LABEL "Manager" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE cbStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17.2 BY 3.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17.2 BY 3.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 85.4 BY 3.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17.2 BY 3.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 24.2 BY 3.

DEFINE RECTANGLE RECT-78
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 10.6 BY 3.

DEFINE RECTANGLE RECT-87
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17.2 BY 3.

DEFINE RECTANGLE RECT-88
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 24 BY 3.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwarAring FOR 
      ttagent SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwarAring
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwarAring C-Win _FREEFORM
  QUERY brwarAring DISPLAY
      ttagent.stateID     label "State ID"           format "x(10)"     width 10
  ttagent.stat       column-label "Status"           format "x(12)"     width 15
ttagent.agentID           label "Agent ID"           format "x(15)"     width 15
ttagent.name              label "Name"               format "x(70)"     width 58
ttagent.managerDesc       label "Manager"            format "x(35)"     width 25
ttagent.Contactname                                  format "x(70)"     width 25
ttagent.ContactPhone                                 format "x(70)"     width 20
ttagent.ContactEmail                                 format "x(70)"     width 30
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 211 BY 16.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     cbState AT ROW 1.76 COL 119 COLON-ALIGNED WIDGET-ID 288
     bExport AT ROW 1.95 COL 205 WIDGET-ID 436 NO-TAB-STOP 
     cbManager AT ROW 1.76 COL 154.2 COLON-ALIGNED WIDGET-ID 300
     cbStatus AT ROW 2.86 COL 119 COLON-ALIGNED WIDGET-ID 298
     bApplyCredit AT ROW 1.95 COL 76 WIDGET-ID 462 NO-TAB-STOP 
     fSearch AT ROW 2.86 COL 154.2 COLON-ALIGNED WIDGET-ID 464
     brwarAring AT ROW 4.62 COL 3 WIDGET-ID 300
     bApplyPayment AT ROW 1.95 COL 52.2 WIDGET-ID 212 NO-TAB-STOP 
     bLedgerGL AT ROW 1.95 COL 103 WIDGET-ID 440 NO-TAB-STOP 
     bpostedBatch AT ROW 1.95 COL 11.6 WIDGET-ID 460 NO-TAB-STOP 
     bPostedCredit AT ROW 1.95 COL 69 WIDGET-ID 210 NO-TAB-STOP 
     bPostedDeposit AT ROW 1.95 COL 28.4 WIDGET-ID 438 NO-TAB-STOP 
     bPostedInvoice AT ROW 1.95 COL 93 WIDGET-ID 452 NO-TAB-STOP 
     bPostedPayment AT ROW 1.95 COL 45.2 WIDGET-ID 316 NO-TAB-STOP 
     bUnpostedBatch AT ROW 1.95 COL 4.6 WIDGET-ID 408 NO-TAB-STOP 
     bUnpostedCredit AT ROW 1.95 COL 62 WIDGET-ID 426 NO-TAB-STOP 
     bUnpostedDeposit AT ROW 1.95 COL 21.4 WIDGET-ID 428 NO-TAB-STOP 
     bUnpostedInvoice AT ROW 1.95 COL 86 WIDGET-ID 200 NO-TAB-STOP 
     bUnpostedPayment AT ROW 1.95 COL 38.2 WIDGET-ID 206 NO-TAB-STOP 
     bRefresh AT ROW 1.95 COL 198 WIDGET-ID 42 NO-TAB-STOP 
     bResetFilter AT ROW 1.95 COL 188.2 WIDGET-ID 302 NO-TAB-STOP 
     "Invoices" VIEW-AS TEXT
          SIZE 8.8 BY .62 AT ROW 1 COL 86.2 WIDGET-ID 456
     "Ledger" VIEW-AS TEXT
          SIZE 6.8 BY .62 AT ROW 1 COL 102.8 WIDGET-ID 458
     "Batches" VIEW-AS TEXT
          SIZE 8.2 BY .62 AT ROW 1 COL 4.6 WIDGET-ID 324
     "Credits" VIEW-AS TEXT
          SIZE 6.8 BY .62 AT ROW 1 COL 62.2 WIDGET-ID 326
     "Action" VIEW-AS TEXT
          SIZE 6.2 BY .62 AT ROW 1 COL 198.4 WIDGET-ID 432
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1 COL 113.4 WIDGET-ID 434
     "Deposits" VIEW-AS TEXT
          SIZE 8.6 BY .62 AT ROW 1 COL 21.6 WIDGET-ID 446
     "Payments" VIEW-AS TEXT
          SIZE 9.6 BY .62 AT ROW 1 COL 38.4 WIDGET-ID 448
     RECT-1 AT ROW 1.33 COL 196.6 WIDGET-ID 222
     RECT-3 AT ROW 1.33 COL 84.6 WIDGET-ID 232
     RECT-4 AT ROW 1.33 COL 111.6 WIDGET-ID 290
     RECT-87 AT ROW 1.33 COL 20 WIDGET-ID 318
     RECT-88 AT ROW 1.33 COL 60.8 WIDGET-ID 430
     RECT-5 AT ROW 1.33 COL 3.2 WIDGET-ID 442
     RECT-6 AT ROW 1.33 COL 36.8 WIDGET-ID 444
     RECT-78 AT ROW 1.33 COL 101.4 WIDGET-ID 192
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COLUMN 1.2 ROW 1
         SIZE 215 BY 20.95 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Accounts Receivable"
         HEIGHT             = 20.71
         WIDTH              = 215.2
         MAX-HEIGHT         = 37.86
         MAX-WIDTH          = 288
         VIRTUAL-HEIGHT     = 37.86
         VIRTUAL-WIDTH      = 288
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-C-Win:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwarAring fSearch DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bResetFilter IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwarAring:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwarAring:HANDLE
       brwarAring:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwarAring:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

/* SETTINGS FOR RECTANGLE RECT-1 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-3 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-4 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-5 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-6 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-87 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwarAring
/* Query rebuild information for BROWSE brwarAring
     _START_FREEFORM
open query {&SELF-NAME} for each ttagent.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwarAring */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Accounts Receivable */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Accounts Receivable */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Accounts Receivable */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bApplyCredit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bApplyCredit C-Win
ON CHOOSE OF bApplyCredit IN FRAME DEFAULT-FRAME /* ApplyCredit */
DO:
  /* Show records based on the parameter list */
  publish "SetCurrentValue" ("ApplyParams", "|" + {&Credit} + "|").
  publish "OpenWindow" (input "wapply", 
                        input "", 
                        input "wapply.w", 
                        input ?,                                   
                        input this-procedure). 

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bApplyPayment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bApplyPayment C-Win
ON CHOOSE OF bApplyPayment IN FRAME DEFAULT-FRAME /* ApplyPayment */
DO:
  /* Show records based on the parameter list */
  publish "SetCurrentValue" ("ApplyParams", "|" + {&Payment} + "|").
  publish "OpenWindow" (input "wapply", 
                        input "", 
                        input "wapply.w", 
                        input ?,                                   
                        input this-procedure). 

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLedgerGL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLedgerGL C-Win
ON CHOOSE OF bLedgerGL IN FRAME DEFAULT-FRAME /* Ledger */
DO:
  publish "OpenWindow" (input "waccountledger", 
                        input "", 
                        input "waccountledger.w", 
                        input ?,                                   
                        input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bpostedBatch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bpostedBatch C-Win
ON CHOOSE OF bpostedBatch IN FRAME DEFAULT-FRAME /* PostedBatch */
DO:
  publish "OpenWindow" (input "wpostedbatch", 
                        input "", 
                        input "wpostedbatch.w", 
                        input ?,                                   
                        input this-procedure).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPostedCredit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPostedCredit C-Win
ON CHOOSE OF bPostedCredit IN FRAME DEFAULT-FRAME /* PostedCredits */
DO:
  run postedCredits in this-procedure (input "").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPostedDeposit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPostedDeposit C-Win
ON CHOOSE OF bPostedDeposit IN FRAME DEFAULT-FRAME /* PostedDeposits */
DO:
  publish "OpenWindow" (input "wPostedDeposit", 
                        input "", 
                        input "wposteddeposit.w", 
                        input ?,                                   
                        input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPostedInvoice
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPostedInvoice C-Win
ON CHOOSE OF bPostedInvoice IN FRAME DEFAULT-FRAME /* PostedInvoices */
DO:
  run postedInvoices in this-procedure (input "").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPostedPayment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPostedPayment C-Win
ON CHOOSE OF bPostedPayment IN FRAME DEFAULT-FRAME /* PostedPayments */
DO:
  publish "OpenWindow" (input "wpostedpayment", 
                        input "", 
                        input "wpostedpayment.w", 
                        input ?,                                   
                        input this-procedure).
 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME DEFAULT-FRAME /* Refresh */
DO:
  assign
      cbState:screen-value   = cLastState
      cbStatus:screen-value  = cLastStatus
      cbManager:screen-value = cLastManager
      lRefresh               = true
      .
  
  publish "loadAgents".
  
  run getData in this-procedure. 
  
  lRefresh = false.
  fSearch:screen-value = cLastSearchString.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bResetFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bResetFilter C-Win
ON CHOOSE OF bResetFilter IN FRAME DEFAULT-FRAME /* Reset */
DO:
  /* Reset filter widgets */

  assign
      cbState:screen-value    = {&ALL}
      cbStatus:screen-value   = {&ALL}
      cbManager:screen-value  = {&ALL}
      fSearch:screen-value    =  ""
      cLastSearchString       = fSearch  :input-value
      cLastState              = cbState  :input-value
      cLastStatus             = cbStatus :input-value
      cLastManager            = cbManager:input-value
      .
  run filterData      in this-procedure.
  run setFilterButton in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwarAring
&Scoped-define SELF-NAME brwarAring
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarAring C-Win
ON DEFAULT-ACTION OF brwarAring IN FRAME DEFAULT-FRAME
DO:
  if not available ttagent 
   then return.
   
  run openTransaction in this-procedure (input ttagent.agentID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarAring C-Win
ON ROW-DISPLAY OF brwarAring IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarAring C-Win
ON START-SEARCH OF brwarAring IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startSearch.i} 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bUnpostedBatch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bUnpostedBatch C-Win
ON CHOOSE OF bUnpostedBatch IN FRAME DEFAULT-FRAME /* UnpostedBatch */
DO:
  publish "OpenWindow" (input "wunpostedbatch", 
                        input "", 
                        input "wunpostedbatch.w", 
                        input ?,                                   
                        input this-procedure).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bUnpostedCredit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bUnpostedCredit C-Win
ON CHOOSE OF bUnpostedCredit IN FRAME DEFAULT-FRAME /* UnpostedCredits */
DO:
  publish "OpenWindow" (input "wunpostedcredit", 
                        input "", 
                        input "wunpostedcredit.w", 
                        input ?,                                   
                        input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bUnpostedDeposit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bUnpostedDeposit C-Win
ON CHOOSE OF bUnpostedDeposit IN FRAME DEFAULT-FRAME /* UnpostedDeposits */
DO:
  publish "OpenWindow" (input "wUnpostedDeposit", 
                        input "", 
                        input "wunposteddeposit.w", 
                        input ?,                                   
                        input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bUnpostedInvoice
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bUnpostedInvoice C-Win
ON CHOOSE OF bUnpostedInvoice IN FRAME DEFAULT-FRAME /* UnpostedInvoices */
DO:
  publish "OpenWindow" (input "wunpostedinvoice", 
                        input "", 
                        input "wunpostedinvoice.w", 
                        input ?,                                   
                        input this-procedure).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bUnpostedPayment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bUnpostedPayment C-Win
ON CHOOSE OF bUnpostedPayment IN FRAME DEFAULT-FRAME /* Unposted Payments */
DO:
  /* Show records based on the selected deposit ref */
  publish "SetCurrentValue" ("DepositID", "").
  publish "OpenWindow" (input "wunpostedpayment", 
                        input {&ALL}, 
                        input "wunpostedpayment.w", 
                        input ?,                                   
                        input this-procedure).

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbManager
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbManager C-Win
ON VALUE-CHANGED OF cbManager IN FRAME DEFAULT-FRAME /* Manager */
DO:
  run filterdata      in this-procedure.
  cLastManager = cbManager:screen-value.
  
  /* Enable reset filter button when filter applies */
  run setFilterButton in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState C-Win
ON VALUE-CHANGED OF cbState IN FRAME DEFAULT-FRAME /* State */
DO:
  run filterdata      in this-procedure.
  cLastState = cbState:screen-value.
  
  /* Enable reset filter button when filter applies */
  run setFilterButton in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbStatus C-Win
ON VALUE-CHANGED OF cbStatus IN FRAME DEFAULT-FRAME /* Status */
DO:
  run filterdata      in this-procedure.
  cLastStatus = cbStatus:screen-value.
  
  /* Enable reset filter button when filter applies */
  run setFilterButton in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
   /* if search button is clicked or return key is hit, 
     then 'cLastSearchString' stores the last string searched until user again hits the search */
  assign
      lApplySearchString = true  
      cLastSearchString  = fSearch:input-value
      .
  run filterData in this-procedure.
  run setFilterButton in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_About
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_About C-Win
ON CHOOSE OF MENU-ITEM m_About /* About */
DO:
  publish "AboutApplication".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agents C-Win
ON CHOOSE OF MENU-ITEM m_Agents /* All Agents */
DO:
  if valid-handle(hAgent) 
   then 
    run ShowWindow in hAgent no-error.
   else 
    run referenceagent.w persistent set hAgent.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agent_File
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agent_File C-Win
ON CHOOSE OF MENU-ITEM m_Agent_File /* Agent Files */
DO:
  publish "OpenWindow" (input "wagentfile", 
                        input "", 
                        input "wagentfile.w", 
                        input ?,                                   
                        input this-procedure). 

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Apply_Credit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Apply_Credit C-Win
ON CHOOSE OF MENU-ITEM m_Apply_Credit /* Apply Credit */
DO:
  apply "Choose" to bApplyCredit in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Apply_Multiple_Payments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Apply_Multiple_Payments C-Win
ON CHOOSE OF MENU-ITEM m_Apply_Multiple_Payments /* Bulk Apply to Single Invoice */
DO:
  run wbulkapplypayment.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Apply_Payment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Apply_Payment C-Win
ON CHOOSE OF MENU-ITEM m_Apply_Payment /* Apply Payment */
DO:
  apply "Choose" to bApplyPayment in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_AR_Aging
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_AR_Aging C-Win
ON CHOOSE OF MENU-ITEM m_AR_Aging /* Aging */
DO:
  run waging.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_AR_Statement
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_AR_Statement C-Win
ON CHOOSE OF MENU-ITEM m_AR_Statement /* Generate Agent Statements */
DO:
  publish "OpenWindow" (input "wstatement", 
                        input "", 
                        input "wstatement.w", 
                        input ?,                                   
                        input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Configure
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Configure C-Win
ON CHOOSE OF MENU-ITEM m_Configure /* Configure */
DO:
  run actionConfig in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Destination
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Destination C-Win
ON CHOOSE OF MENU-ITEM m_Destination /* Destinations */
DO:
  run ReferenceDestination in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Exit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Exit C-Win
ON CHOOSE OF MENU-ITEM m_Exit /* Exit */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Files_with_unapplied_paymen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Files_with_unapplied_paymen C-Win
ON CHOOSE OF MENU-ITEM m_Files_with_unapplied_paymen /* Paid Files with a Positive Balance */
DO:
  run wunderpaidfile.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_File_Details
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_File_Details C-Win
ON CHOOSE OF MENU-ITEM m_File_Details /* File Detail */
DO:
  publish "SetCurrentValue" ("FileNumber","").
  publish "SetCurrentValue" ("AgentID","").
  publish "OpenWindow" (input "wfile", 
                        input "", 
                        input "wfile.w", 
                        input ?,                                    
                        input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Generate_Production_Activit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Generate_Production_Activit C-Win
ON CHOOSE OF MENU-ITEM m_Generate_Production_Activit /* Generate Production Activity */
DO:
  publish "OpenWindow" (input "wproductionfileactivityreport", 
                        input "", 
                        input "wproductionfileactivityreport.w", 
                        input ?,                                   
                        input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Historic_Transactions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Historic_Transactions C-Win
ON CHOOSE OF MENU-ITEM m_Historic_Transactions /* Add History Transaction */
DO:
  run  whistorictransaction.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Invoice_Statement
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Invoice_Statement C-Win
ON CHOOSE OF MENU-ITEM m_Invoice_Statement /* Generate Agent Invoices */
DO:
  publish "OpenWindow" (input "winvoicestatement", 
                        input "", 
                        input "winvoicestatement.w", 
                        input ?,                                   
                        input this-procedure). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Ledger
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Ledger C-Win
ON CHOOSE OF MENU-ITEM m_Ledger /* Ledger */
DO:
  apply "Choose" to bLedgerGL in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Ledger_Details
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Ledger_Details C-Win
ON CHOOSE OF MENU-ITEM m_Ledger_Details /* Ledger Detail */
DO:
  publish "OpenWindow" (input "wledger", 
                        input "", 
                        input "wledger.w", 
                        input ?,                                    
                        input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Note_Type
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Note_Type C-Win
ON CHOOSE OF MENU-ITEM m_Note_Type /* Note Types */
DO:
  publish "OpenWindow" (input "wnotetype", 
                        input "", 
                        input "wnotetype.w", 
                        input ?,                                   
                        input this-procedure).

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Open_Transaction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Open_Transaction C-Win
ON CHOOSE OF MENU-ITEM m_Open_Transaction /* Open Transactions */
DO:
  if not available ttagent 
   then return.
   
  run openTransaction in this-procedure (input ttagent.agentID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Overpayment_Files
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Overpayment_Files C-Win
ON CHOOSE OF MENU-ITEM m_Overpayment_Files /* Paid Files with a Negative Balance */
DO:
  run woverpaymentfile.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Period
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Period C-Win
ON CHOOSE OF MENU-ITEM m_Period /* Periods */
DO:
  if valid-handle(hPeriod) 
   then
    run ShowWindow in hPeriod no-error.
   else 
    run referenceperiod.w persistent set hPeriod.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Period_GL_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Period_GL_Report C-Win
ON CHOOSE OF MENU-ITEM m_Period_GL_Report /* Period Misc GL Transactions */
DO:
  run wglcreditandinvoice.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Period_Payments_GL_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Period_Payments_GL_Report C-Win
ON CHOOSE OF MENU-ITEM m_Period_Payments_GL_Report /* Period Cash GL Transactions */
DO:
  run wglpayment.w persistent(input "",   /* AgentID     */
                              input "",   /* Name        */
                              input 0,    /* Month       */
                              input 0,    /* Year        */
                              input "",   /* Deposit Ref */
                              input false /* Criteria to choose records passed */
                              ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Period_Processing_GL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Period_Processing_GL C-Win
ON CHOOSE OF MENU-ITEM m_Period_Processing_GL /* Period Processing GL Transactions */
DO:
  run wperiodprocessingglrpt.w persistent("","").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Period_Production_GL_Transa
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Period_Production_GL_Transa C-Win
ON CHOOSE OF MENU-ITEM m_Period_Production_GL_Transa /* Period Production GL Transactions */
DO:
  run wglproductioninvoice.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Person
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Person C-Win
ON CHOOSE OF MENU-ITEM m_Person /* Contacts */
DO:
  run openContacts in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Policy_Audit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Policy_Audit C-Win
ON CHOOSE OF MENU-ITEM m_Policy_Audit /* Policy Audit */
DO:
  run wpolicyaudit.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Policy_Details
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Policy_Details C-Win
ON CHOOSE OF MENU-ITEM m_Policy_Details /* Policy Detail */
DO:
  publish "SetCurrentValue" ("PolicyID","").
  publish "OpenWindow" (input "wpolicy", 
                        input "", 
                        input "wpolicy.w", 
                        input ?,                                   
                        input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Posted_Batches
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Posted_Batches C-Win
ON CHOOSE OF MENU-ITEM m_Posted_Batches /* Posted Batches */
DO:
  apply "Choose" to bpostedBatch in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Posted_Credits
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Posted_Credits C-Win
ON CHOOSE OF MENU-ITEM m_Posted_Credits /* Posted Credits */
DO:
  apply "Choose" to bPostedCredit in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Posted_Deposits
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Posted_Deposits C-Win
ON CHOOSE OF MENU-ITEM m_Posted_Deposits /* Posted Deposits */
DO:
  apply "Choose" to bPostedDeposit in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Posted_Invoices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Posted_Invoices C-Win
ON CHOOSE OF MENU-ITEM m_Posted_Invoices /* Posted Invoices */
DO:
  apply "choose" to bPostedInvoice in frame {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Posted_Payments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Posted_Payments C-Win
ON CHOOSE OF MENU-ITEM m_Posted_Payments /* Posted Payments */
DO:
  apply "Choose" to bPostedPayment in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Posted_Refunds
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Posted_Refunds C-Win
ON CHOOSE OF MENU-ITEM m_Posted_Refunds /* Posted Refunds */
DO:
  publish "OpenWindow" (input "wpostedrefund",                                                                               /*childtype*/
                        input "",                                                             /*childid*/
                        input "wpostedrefund.w",                                                                           /*window*/
                        input "integer|input| " ,                             
                        input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Posted_Transaction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Posted_Transaction C-Win
ON CHOOSE OF MENU-ITEM m_Posted_Transaction /* Posted Transactions */
DO:
  if not available ttagent 
   then return.
   
  run postedTransaction in this-procedure (input ttagent.agentID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Posted_Transactions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Posted_Transactions C-Win
ON CHOOSE OF MENU-ITEM m_Posted_Transactions /* Posted Transactions */
DO:
  run postedTransaction in this-procedure (input "").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Posted_Write-Offs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Posted_Write-Offs C-Win
ON CHOOSE OF MENU-ITEM m_Posted_Write-Offs /* Posted Write-Offs */
DO:
  publish "OpenWindow" (input "wpostedwriteoff",
                        input "",
                        input "wpostedwriteoff.w",
                        input "integer|input| ",                                  
                        input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Production_Apply_Payment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Production_Apply_Payment C-Win
ON CHOOSE OF MENU-ITEM m_Production_Apply_Payment /* Apply Payment to Production Files */
DO:
  publish "OpenWindow" (input "wproductionapply", 
                        input "", 
                        input "wproductionapply.w", 
                        input ?,                                   
                        input this-procedure). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Production_Files
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Production_Files C-Win
ON CHOOSE OF MENU-ITEM m_Production_Files /* Production File */
DO:
  run productionFile in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Production_File_Activity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Production_File_Activity C-Win
ON CHOOSE OF MENU-ITEM m_Production_File_Activity /* Production File Activity */
DO:
  publish "OpenWindow" (input "wproductionfileactivity", 
                        input "", 
                        input "wproductionfileactivity.w", 
                        input ?,                                   
                        input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Refund
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Refund C-Win
ON CHOOSE OF MENU-ITEM m_Refund /* Refund */
DO:
  publish "OpenWindow" (input "wrefund", 
                        input "", 
                        input "wrefund.w", 
                      input "",                                   
                        input this-procedure).

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Revenue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Revenue C-Win
ON CHOOSE OF MENU-ITEM m_Revenue /* Revenue Types */
DO:
  publish "OpenWindow" (input "wrevenuetype", 
                        input "", 
                        input "wrevenuetype.w", 
                        input ?,                                   
                        input this-procedure). 

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_State_Forms
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_State_Forms C-Win
ON CHOOSE OF MENU-ITEM m_State_Forms /* State Products */
DO:
  publish "OpenWindow" (input "wform", 
                        input "", 
                        input "wform.w", 
                        input ?,                                   
                        input this-procedure). 

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_State_Source
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_State_Source C-Win
ON CHOOSE OF MENU-ITEM m_State_Source /* State Source */
DO:
  publish "OpenWindow" (input "wstatesource", 
                        input "", 
                        input "wstatesource.w", 
                        input ?,                                   
                        input this-procedure). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Unapplied_Credits
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Unapplied_Credits C-Win
ON CHOOSE OF MENU-ITEM m_Unapplied_Credits /* Unapplied Credits */
DO:
  run wunappliedtrans.w persistent (input {&Credit}).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Unapplied_Payment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Unapplied_Payment C-Win
ON CHOOSE OF MENU-ITEM m_Unapplied_Payment /* Unapplied Payments */
DO:
  run wunappliedtrans.w persistent (input {&Payment}).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Unposted_Batches
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Unposted_Batches C-Win
ON CHOOSE OF MENU-ITEM m_Unposted_Batches /* Unposted Batches */
DO:
  apply "Choose" to bUnpostedBatch in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Unposted_Credits
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Unposted_Credits C-Win
ON CHOOSE OF MENU-ITEM m_Unposted_Credits /* Unposted Credits */
DO:
  apply "Choose" to bUnpostedCredit in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Unposted_Deposits
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Unposted_Deposits C-Win
ON CHOOSE OF MENU-ITEM m_Unposted_Deposits /* Unposted Deposits */
DO:
  apply "Choose" to bUnpostedDeposit in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Unposted_Invoices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Unposted_Invoices C-Win
ON CHOOSE OF MENU-ITEM m_Unposted_Invoices /* Unposted Invoices */
DO:
  apply "Choose" to bUnpostedInvoice in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Unposted_Payments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Unposted_Payments C-Win
ON CHOOSE OF MENU-ITEM m_Unposted_Payments /* Unposted Payments */
DO:
  apply "Choose" to bUnpostedPayment in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Write-Off
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Write-Off C-Win
ON CHOOSE OF MENU-ITEM m_Write-Off /* Write-Off */
DO:
  publish "OpenWindow" (input "wwriteoff",
                        input "",
                        input "wwriteoff.w",
                        input ?,                                  
                        input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}
      {&window-name}:window-state    = window-minimized  
      .
setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow"              anywhere.
subscribe to "ReferenceDestination"     anywhere.

subscribe to "AgentSelected"            anywhere.

bUnpostedBatch     :load-image("images/report-edit.bmp").
bUnpostedBatch     :load-image-insensitive("images/report-edit-i.bmp").
bPostedBatch       :load-image("images/report.bmp").
bPostedBatch       :load-image-insensitive("images/report-i.bmp").

bUnpostedDeposit   :load-image("images/dollar-edit.bmp").
bUnpostedDeposit   :load-image-insensitive("images/dollar-edit-i.bmp").
bPostedDeposit     :load-image("images/dollar.bmp").
bPostedDeposit     :load-image-insensitive("images/dollar-i.bmp").

bUnpostedPayment   :load-image("images/money-edit.bmp").
bUnpostedPayment   :load-image-insensitive("images/money-edit-i.bmp").
bPostedPayment     :load-image("images/money.bmp").
bPostedPayment     :load-image-insensitive("images/money-i.bmp").
bApplyPayment      :load-image("images/folder_add.bmp").
bApplyPayment      :load-image-insensitive("images/folder_add-i.bmp").

bUnPostedCredit    :load-image("images/money-credit-edit.bmp").
bUnPostedCredit    :load-image-insensitive("images/money-credit-edit-i.bmp").
bPostedCredit      :load-image("images/money-delete.bmp").
bPostedCredit      :load-image-insensitive("images/money-delete-i.bmp").
bApplyCredit       :load-image("images/folder_add.bmp").
bApplyCredit       :load-image-insensitive("images/folder_add-i.bmp").

bUnPostedInvoice   :load-image("images/book_edit.bmp").
bUnPostedInvoice   :load-image-insensitive("images/book_edit-i.bmp").
bPostedInvoice     :load-image("images/book_closed.bmp").
bPostedInvoice     :load-image-insensitive("images/book_closed-i.bmp").

bLedgerGL          :load-image("images/book_key.bmp").
bLedgerGL          :load-image-insensitive("images/book_key-i.bmp").

bRefresh           :load-image-up("images/refresh.bmp").
bRefresh           :load-image-insensitive("images/refresh-i.bmp").

bResetFilter       :load-image("images/filtererase.bmp").
bResetFilter       :load-image-insensitive("images/filtererase-i.bmp").

bExport            :load-image("images/excel.bmp").
bExport            :load-image-insensitive("images/excel-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  run enable_UI.       
  
  {lib/get-column-width.i &col="'Name'"    &var=dColumnWidth} 
  
  /* create state combo */ 
  {lib/get-state-list.i &combo=cbState &addAll=true}

  run getData in this-procedure.

  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure. 
  
  apply 'entry' to brwarAring.
    
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionConfig C-Win 
PROCEDURE actionConfig :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run dialogconfig.w.
  
  if return-value = "no" 
   then
    return.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE agentSelected C-Win 
PROCEDURE agentSelected :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  publish "ActionAgentModify" (pAgentID).  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "GetConfirmExit" (output std-lo).
  
  if std-lo
   then
    do:
      message "Are you sure, you want to exit."
          view-as alert-box question buttons ok-cancel update lChoice as logical.
      if not lChoice 
       then
        return.
    end.
  publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.

  publish "ExitApplication".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbState cbManager cbStatus fSearch 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE cbState bExport cbManager cbStatus bApplyCredit fSearch brwarAring 
         bApplyPayment bLedgerGL bpostedBatch bPostedCredit bPostedDeposit 
         bPostedInvoice bPostedPayment bUnpostedBatch bUnpostedCredit 
         bUnpostedDeposit bUnpostedInvoice bUnpostedPayment bRefresh RECT-88 
         RECT-78 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttagent for ttagent.
  
  if query brwarAring:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
  
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table ttagent:handle.
  run util/exporttable.p (table-handle std-ha,
                          "ttagent",
                          "for each ttagent",
                          "stateID,stat,agentID,name,manager",
                          "StateID,Stat,AgentID,Name,Manager",
                          std-ch,
                          "Agents-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   do with frame {&frame-name}:
   end.
  
   close query brwArAring.
   empty temp-table ttagent.

   if lApplySearchString /* if search filter is also applied */
   then
    for each agent where agent.stateID = (if cbState:input-value   = {&all} then agent.stateID else cbState:input-value)
                     and agent.stat    = (if cbStatus:input-value  = {&all} then agent.stat else cbStatus:input-value)
                     and agent.manager = (if cbManager:input-value = {&all} then agent.manager else cbManager:input-value)
                     and ((if trim(cLastSearchString) <> "" then agent.stateID matches ("*" + trim(cLastSearchString) + "*")
                           else agent.stateID = agent.stateID) or
                          (if trim(cLastSearchString) <> "" then 
                           entry(lookup(agent.stat,cbStatus:list-item-pairs,",") - 1, cbStatus:list-item-pairs,",") matches ("*" + trim(cLastSearchString) + "*")
                           else agent.stat = agent.stat) or
                          (if trim(cLastSearchString) <> "" then agent.agentID matches ("*" + trim(cLastSearchString) + "*")
                           else agent.agentID = agent.agentID) or 
                          (if trim(cLastSearchString) <> "" then lc(agent.name) matches ("*" + lc(trim(cLastSearchString)) + "*")
                           else agent.name = agent.name) or 
                          (if trim(cLastSearchString) <> "" then agent.managerDesc matches ("*" + trim(cLastSearchString) + "*")
                           else agent.managerDesc = agent.managerDesc) /**/ or
                          (if trim(cLastSearchString) <> "" then agent.contactName matches ("*" + trim(cLastSearchString) + "*")
                           else agent.contactName = agent.contactName) or 
                          (if trim(cLastSearchString) <> "" then agent.contactPhone matches ("*" + trim(cLastSearchString) + "*")
                           else agent.contactPhone = agent.contactPhone) or 
                          (if trim(cLastSearchString) <> "" then agent.contactEmail matches ("*" + trim(cLastSearchString) + "*")
                           else agent.contactEmail = agent.contactEmail)
                          ):
      create ttAgent.
      buffer-copy agent to ttAgent.
      ttagent.stat = entry(lookup(ttagent.stat,cbStatus:list-item-pairs,",") - 1, cbStatus:list-item-pairs,",").
    end.
 
   else
    for each agent where agent.stateID = (if cbState:input-value   = {&all} then agent.stateID else cbState:input-value)
                     and agent.stat    = (if cbStatus:input-value  = {&all} then agent.stat else cbStatus:input-value)
                     and agent.manager = (if cbManager:input-value = {&all} then agent.manager else cbManager:input-value):
      create ttAgent.
      buffer-copy agent to ttAgent.
      ttagent.stat = entry(lookup(ttagent.stat,cbStatus:list-item-pairs,",") - 1, cbStatus:list-item-pairs,",").
    end. 
   

  dataSortDesc = not dataSortDesc.
  if dataSortBy = ""
   then 
    dataSortBy = "name".

  run sortData in this-procedure (dataSortBy).
  
  /* Display no. of records on status bar */
  setStatusCount(query brwarAring:num-results).  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer agent for agent.
  define variable lLoadedAgents as logical no-undo.
  close query brwArAring.
  
  empty temp-table agent.
  
  publish "GetPeriods" (output table period).
  publish "getAgents" (output table agent).
  
  if not can-find(first agent)
   then
    return.

  do with frame {&frame-name}:
  end.

  /* remove prospect and withdrawn agents */
  for each agent where agent.stat = 'W' or agent.stat = 'P':
    delete agent.
  end.
  
  /* Manager Combo */
  std-ch = {&ALL} + "," + {&ALL} + ",".
  for each agent
    where agent.manager ne ""
    break by agent.manager:

    if not first-of(agent.manager)
     then
      next.

    std-ch = std-ch + agent.managerdesc + "," + agent.manager + ",".
  end.
  cbManager:list-item-pairs = trim(std-ch,",").

  /* Status Combo */
  std-ch = {&ALL} + "," + {&ALL} + ",".
  for each agent
    break by agent.stat:

    /*Task #79782 do not display agents with status prospect and withdrawn*/
    if not first-of(agent.stat) or (agent.stat = 'P' or agent.stat = 'W')
     then
      next.
    publish "GetSysPropDesc" (input "AMD",
                              input "Agent",
                              input "Status",
                              input agent.stat,
                              output chSysPropDesc).
    std-ch = std-ch + chSysPropDesc + "," + agent.stat + ",".
  end.

  cbStatus:list-item-pairs = trim(std-ch,",").
  
  if lRefresh or lTypeChanged /* retain previous values on refresh */
   then
    assign
        cbState:screen-value   = if cLastState   = "" or cLastState   = ?  then {&ALL} else cLastState
        cbStatus:screen-value  = if cLastStatus  = "" or cLastStatus  = ?  then {&ALL} else cLastStatus
        cbManager:screen-value = if cLastManager = "" or cLastManager = ?  then {&ALL} else cLastManager
        .
   else
    assign
        cbManager:screen-value   = {&ALL}
        cbState:screen-value     = {&ALL}
        cbStatus:screen-value    = {&ALL}
        .
  
  run filterData in this-procedure.   

  /* Display no. of records with date and time on status bar */
  setStatusRecords(query brwarAring:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openContacts C-Win 
PROCEDURE openContacts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available ttagent
   then 
    return.
    
  publish "OpenWindow" (input "wcontacts", 
                        input ttagent.agentID, 
                        input "wcontacts.w", 
                        input "character|input|" + ttagent.stateID + "^character|input|" + ttagent.agentID + 
                              "^character|input|" + ttagent.name + "^character|input|AR" + "^character|input|" + ttagent.orgID,                                   
                        input this-procedure).
                        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openTransaction C-Win 
PROCEDURE openTransaction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcAgentID  as character no-undo.
  
  publish "OpenWindow" (input "wopentransaction", 
                        input ipcAgentID,
                        input "wopentransaction.w", 
                        input "character|input|" + ipcAgentID  + "^character|input|B",                                   
                        input this-procedure).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE postedCredits C-Win 
PROCEDURE postedCredits :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcAgentID as character no-undo.
  
  publish "SetDefaultAgent" (ipcAgentID).
  publish "OpenWindow" (input "wPostedCredit", 
                        input ipcAgentID, 
                        input "wpostedcredit.w", 
                        input ?,                                   
                        input this-procedure).
                            
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE postedInvoices C-Win 
PROCEDURE postedInvoices :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcAgentID as character no-undo.
  
  publish "SetDefaultAgent" (ipcAgentID).
  publish "OpenWindow" (input "wPostedInvoice", 
                        input ipcAgentID, 
                        input "wpostedinvoice.w", 
                        input ?,                                   
                        input this-procedure).
                            
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE postedTransaction C-Win 
PROCEDURE postedTransaction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcAgentID as character no-undo.
  
  publish "SetDefaultAgent" (ipcAgentID).
  publish "OpenWindow" (input "wtransaction", 
                        input ipcAgentID, 
                        input "wtransaction.w", 
                        input ?,                                   
                        input this-procedure).
                            
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE productionFile C-Win 
PROCEDURE productionFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
                       
  publish "OpenWindow" (input "wProductionFileDetails", 
                        input '', 
                        input "wproductionfiledetail.w",
                        input "character|input|" + string('') + 
                              "^character|input|" + string('') + 
                              "^character|input|" + string('')  +
                              "^date|input|" + string('')   +
                              "^date|input|" + string(''),  /*parameters*/                                    
                        input this-procedure).
                            
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ReferenceDestination C-Win 
PROCEDURE ReferenceDestination :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if valid-handle(hDestination) 
   then
    run ShowWindow in hDestination no-error.
   else
    run referencesysdest.w persistent set hDestination. 
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterButton C-Win 
PROCEDURE setFilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
   bResetFilter:sensitive = not (cbState:screen-value  = {&all}
                            and cbStatus:screen-value  = {&all}
                            and cbManager:screen-value = {&all}
                            and fSearch  :screen-value = ""
                            ).
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by ttagent.name by ttagent.agentID".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels       

      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 22
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 24.
      .

  run ShowScrollBars(frame {&frame-name}:handle, no, no).  
  {lib/resize-column.i &col="'Name'"    &var=dColumnWidth} 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getInvoiceStatus C-Win 
FUNCTION getInvoiceStatus RETURNS CHARACTER
  (cStat as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  if cStat = 'O' 
   then
    return 'Open'.   /* Function return value. */
  else if cStat = 'A' 
   then
    return 'Approved'.
  else if cStat = 'P' 
   then
    return 'Posted'.  
  else
    return cStat.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getManagerName C-Win 
FUNCTION getManagerName RETURNS CHARACTER
  ( input ipcUID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cName as CHARACTER   NO-UNDO.
  publish "getManagerName" ("U",ipcUID,output cName).
  return cName.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

