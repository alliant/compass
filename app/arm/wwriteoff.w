&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME eC-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS eC-Win 
/*------------------------------------------------------------------------
  File: wwriteoff.w

  Description: Window for AR Payments

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Shefali
  Version: 1.0
  Created: 05.05.2021
  Modifications:
  Date         Name     Description

  07-26-2021   SB       Task 84781 Remove unnecessary popup menus.
  08-19-2021   SB       Task #84781 Remove unnecessary code on default-action of grid.
  09-21-2021   AG       Task 86863 Changed the format of write-off cut-of amount
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/ar-def.i}
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/winshowscrollbars.i}
{lib/get-column.i}

/* Temp-table Definition */
{tt/artran.i }
{tt/artran.i &tableAlias="ttartran"}
{tt/artran.i &tableAlias="tartran"}
{tt/ledgerreport.i}                                /* Contain info of GL accounts hit by deposit */
{tt/ledgerreport.i &tableAlias="glwriteoffDetail"}  /* contain info of GL accounts hit by each payment of deposit */

/* Variable Definition */
define variable lDefaultAgent      as logical   no-undo.
define variable dtFromDate         as date      no-undo.
define variable dtToDate           as date      no-undo.
define variable dColumnWidth       as decimal   no-undo.
define variable rwRow              as rowid     no-undo.
define variable iMonth             as integer   no-undo.
define variable iYear              as integer   no-undo.                    
define variable dtStartDate        as date      no-undo.
define variable dtEndDate          as date      no-undo.
define variable dewriteoffamt      as decimal   no-undo.
define variable iCount             as integer   no-undo.
define variable lGet               as logical   no-undo.
define variable lGetAgentData      as logical   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwartran

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES artran

/* Definitions for BROWSE brwartran                                     */
&Scoped-define FIELDS-IN-QUERY-brwartran artran.fileId artran.entityID artran.entityname artran.tranDate artran.remainingAmt arTran.selectrecord   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwartran arTran.selectrecord   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwartran arTran
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwartran arTran
&Scoped-define SELF-NAME brwartran
&Scoped-define QUERY-STRING-brwartran for each artran
&Scoped-define OPEN-QUERY-brwartran open query {&SELF-NAME} for each artran.
&Scoped-define TABLES-IN-QUERY-brwartran artran
&Scoped-define FIRST-TABLE-IN-QUERY-brwartran artran


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwartran}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwartran btGo flAmount fCutoffDate ~
flAgentID bAgentLookup flName rbFileBalance fType RECT-79 RECT-85 RECT-77 
&Scoped-Define DISPLAYED-OBJECTS fpostdate flAmount fCutoffDate flAgentID ~
flName rbFileBalance flTotal flCheckAmt fType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFormattedNumber eC-Win 
FUNCTION getFormattedNumber RETURNS CHARACTER
  ( input deTotal as decimal,
    input hWidget as handle )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged eC-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validAgent eC-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR eC-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup 
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Agent lookup".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.67 TOOLTIP "Export data".

DEFINE BUTTON bPrelimRpt  NO-FOCUS
     LABEL "Prelim" 
     SIZE 7.2 BY 1.71 TOOLTIP "Preliminary report".

DEFINE BUTTON btGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.67 TOOLTIP "Get data".

DEFINE BUTTON btpost  NO-FOCUS
     LABEL "Post" 
     SIZE 7.2 BY 1.71 TOOLTIP "Post Write-off".

DEFINE VARIABLE fCutoffDate AS DATE FORMAT "99/99/99":U 
     LABEL "Cut-Off Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE flAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 18.2 BY 1 NO-UNDO.

DEFINE VARIABLE flAmount AS DECIMAL FORMAT "9.99":U INITIAL 0 
     LABEL "Amount" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 7 BY 1 NO-UNDO.

DEFINE VARIABLE flCheckAmt AS DECIMAL FORMAT "(Z,ZZZ,ZZZ)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 14.4 BY 1 NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 46.4 BY 1 NO-UNDO.

DEFINE VARIABLE flTotal AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 38 BY 1 NO-UNDO.

DEFINE VARIABLE fpostdate AS DATE FORMAT "99/99/99":U 
     LABEL "Use Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fType AS CHARACTER FORMAT "X(256)":U INITIAL "File" 
     LABEL "Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 18.2 BY 1 NO-UNDO.

DEFINE VARIABLE rbFileBalance AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Debit", "D",
"Credit", "C"
     SIZE 9.8 BY 2.33 NO-UNDO.

DEFINE RECTANGLE RECT-77
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 10.4 BY 3.05.

DEFINE RECTANGLE RECT-79
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 114.8 BY 3.05.

DEFINE RECTANGLE RECT-85
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 42.2 BY 3.05.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwartran FOR 
      artran SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwartran
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwartran eC-Win _FREEFORM
  QUERY brwartran DISPLAY
      artran.fileId                        label  "File"           format "x(30)" width 35
artran.entityID                      label  "Agent ID"       format "x(15)"
artran.entityname                    label  "Agent Name"     format "x(30)" width 50
artran.tranDate               column-label  "Post!Date"      format "99/99/99  " width 20
artran.remainingAmt           column-label  "Balance"        format "->,>>>,>>9.99" width 17
arTran.selectrecord           column-label  "Select to!Post" view-as toggle-box
enable arTran.selectrecord
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 166.4 BY 17.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     brwartran AT ROW 4.81 COL 3.2 WIDGET-ID 200
     bPrelimRpt AT ROW 2.19 COL 160.6 WIDGET-ID 450 NO-TAB-STOP 
     btpost AT ROW 2.19 COL 153.4 WIDGET-ID 452 NO-TAB-STOP 
     bExport AT ROW 2.19 COL 119.2 WIDGET-ID 460 NO-TAB-STOP 
     btGo AT ROW 2.19 COL 109.2 WIDGET-ID 262 NO-TAB-STOP 
     fpostdate AT ROW 2.52 COL 137.2 COLON-ALIGNED WIDGET-ID 454
     flAmount AT ROW 3.19 COL 45.6 COLON-ALIGNED WIDGET-ID 472
     fCutoffDate AT ROW 3.19 COL 70.2 COLON-ALIGNED WIDGET-ID 474
     flAgentID AT ROW 1.95 COL 13.6 COLON-ALIGNED WIDGET-ID 352
     bAgentLookup AT ROW 1.86 COL 34 WIDGET-ID 350
     flName AT ROW 1.95 COL 37 COLON-ALIGNED NO-LABEL WIDGET-ID 424
     rbFileBalance AT ROW 1.91 COL 96.8 NO-LABEL WIDGET-ID 492
     flTotal AT ROW 22.57 COL 1.4 COLON-ALIGNED NO-LABEL WIDGET-ID 314 NO-TAB-STOP 
     flCheckAmt AT ROW 22.57 COL 134 COLON-ALIGNED NO-LABEL WIDGET-ID 304 NO-TAB-STOP 
     fType AT ROW 3.19 COL 13.6 COLON-ALIGNED WIDGET-ID 496
     "Parameters" VIEW-AS TEXT
          SIZE 11.6 BY .62 AT ROW 1.24 COL 4.8 WIDGET-ID 266
     "Post" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1.24 COL 129.6 WIDGET-ID 458
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.24 COL 119.8 WIDGET-ID 468
     "Balance:" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 2.1 COL 87.2 WIDGET-ID 490
     RECT-79 AT ROW 1.52 COL 3.2 WIDGET-ID 270
     RECT-85 AT ROW 1.52 COL 127.6 WIDGET-ID 456
     RECT-77 AT ROW 1.52 COL 117.6 WIDGET-ID 466
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 225.4 BY 23.19 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW eC-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Write Off"
         HEIGHT             = 22.76
         WIDTH              = 171
         MAX-HEIGHT         = 34.43
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.43
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW eC-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwartran 1 DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bExport IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPrelimRpt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwartran:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwartran:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR BUTTON btpost IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flCheckAmt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flName:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN flTotal IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flTotal:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN fpostdate IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       fType:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(eC-Win)
THEN eC-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwartran
/* Query rebuild information for BROWSE brwartran
     _START_FREEFORM
open query {&SELF-NAME} for each artran.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwartran */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME eC-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eC-Win eC-Win
ON END-ERROR OF eC-Win /* Write Off */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eC-Win eC-Win
ON WINDOW-CLOSE OF eC-Win /* Write Off */
DO:
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eC-Win eC-Win
ON WINDOW-RESIZED OF eC-Win /* Write Off */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup eC-Win
ON CHOOSE OF bAgentLookup IN FRAME DEFAULT-FRAME /* agentlookup */
DO:
  define variable cAgentID  as character no-undo.
  define variable cName     as character no-undo.
    
  run dialogagentlookup.w(input flAgentID:input-value,
                          input "",      /* Selected State ID */
                          input true,    /* Allow 'ALL' */
                          output cAgentID,
                          output std-ch, /* Agent state ID */
                          output cName,
                          output std-lo).
   
  if not std-lo  
   then
     return no-apply.
     
  assign
      flAgentID:screen-value = cAgentID
      flName:screen-value    = cName
      . 
  
  resultsChanged(false).
  
  if lDefaultAgent               and
     flAgentID:input-value <> "" and
     flAgentID:input-value <> {&ALL}
   then
    /* Set default AgentID */
    publish "SetDefaultAgent" (input flAgentID:input-value).
  
  if flAgentID:input-value <> ""
   then
    run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport eC-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrelimRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrelimRpt eC-Win
ON CHOOSE OF bPrelimRpt IN FRAME DEFAULT-FRAME /* Prelim */
do:
   run prelimRpt in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwartran eC-Win
ON ROW-DISPLAY OF brwartran IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwartran eC-Win
ON START-SEARCH OF brwartran IN FRAME DEFAULT-FRAME
do:
  define buffer artran for artran.
  
  std-ha = brwArtran:current-column.
  if std-ha:label = "Select to!Post" 
   then
    do:     
      std-lo = can-find(first artran where not(artran.selectrecord)).   
      for each artran:            
        artran.selectrecord = std-lo.
        /* Retaining selected record */  
        for first ttartran where ttartran.arTranId = arTran.arTranId:
          ttartran.selectrecord = arTran.selectrecord.
        end.
      end.    
      browse brwArtran:refresh(). 
      run enablePosting in this-procedure.
      run displayTotals in this-procedure.
    end.
   else
    do:
      {lib/brw-startsearch.i}
    end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwartran eC-Win
ON VALUE-CHANGED OF brwartran IN FRAME DEFAULT-FRAME
DO:  
  run setWidgetState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGo eC-Win
ON CHOOSE OF btGo IN FRAME DEFAULT-FRAME /* Go */
OR 'RETURN' of flAgentID
DO:
  if not validAgent()
   then
    return no-apply.
  
  btGo:tooltip = if not lGetAgentData then "Clear Data" else "Get Data".
  if not lGetAgentData
   then
    do:
      run getData in this-procedure.
      
      lGetAgentData = lGet.
      
      if not lGetAgentData 
       then
        return no-apply.
        
      btGo:load-image ("images/cancel.bmp").
      btGo:load-image-insensitive("images/cancel-i.bmp").
    end.
   else
    do:
      /* cancel agent */
      run cancelAgent in this-procedure.
    
      if not std-lo 
       then
        return no-apply.
       
      lGetAgentData = false.
      btGo:load-image ("images/completed.bmp").
      btGo:load-image-insensitive("images/completed-i.bmp").
    end.
    run displayTotals in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btpost
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btpost eC-Win
ON CHOOSE OF btpost IN FRAME DEFAULT-FRAME /* Post */
do:
  run postTransaction in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID eC-Win
ON VALUE-CHANGED OF flAgentID IN FRAME DEFAULT-FRAME /* Agent ID */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fType eC-Win
ON VALUE-CHANGED OF fType IN FRAME DEFAULT-FRAME /* Type */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK eC-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow" anywhere.

bAgentLookup:load-image            ("images/s-lookup.bmp").
bAgentLookup:load-image-insensitive("images/s-lookup-i.bmp").

btGo        :load-image             ("images/completed.bmp").
btGo        :load-image-insensitive ("images/completed-i.bmp").

bExport     :load-image             ("images/excel.bmp").
bExport     :load-image-insensitive ("images/excel-i.bmp").

btpost      :load-image            ("images/rejected.bmp").
btpost      :load-image-insensitive("images/rejected-i.bmp").

bPrelimRpt  :load-image            ("images/pdf.bmp").
bPrelimRpt  :load-image-insensitive("images/pdf-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  {&window-name}:window-state = window-minimized.   
 
  run enable_UI.  
  
  {lib/get-column-width.i &col="'Name'" &var=dColumnWidth}  
  
  publish "getDefaultPeriod"(output iMonth,output iYear).
  
  /* Calculate date range of selected period */
  publish "getSelectedPeriodDate" (input iMonth,
                                   input iYear,
                                   output dtStartDate,
                                   output dtEndDate). 
   
  assign 
      fcutoffDate:screen-value = string(dtEndDate)
      flAgentId:screen-value   = {&ALL}
      flName:screen-value      = "N/A"
      flAmount:screen-value    = "5.00".
          
  on 'value-changed':U of  artran.selectrecord in browse  brwartran  
  do:
    do with frame {&frame-name}:
    end.
    if available artran 
     then
      do:
        artran.selectrecord = artran.selectrecord:checked in browse brwartran.
        /* Retaining selected record */  
        for first ttARtran where ttARtran.arTranId = artran.arTranId:
          ttARtran.selectrecord = artran.selectrecord.
        end.
      end.
    run enablePosting in this-procedure.
    run displayTotals in this-procedure.
    flTotal:screen-value in frame {&FRAME-NAME} = "Totals of " + string(iCount) + " selected writeoff(s)".
  end.
  
  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.
  run setWidgetState in this-procedure.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adjustTotals eC-Win 
PROCEDURE adjustTotals :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Setting position of totals widget */
  assign       
      flTotal:y      = brwartran:y + brwartran:height-pixels + 0.01
      flCheckAmt:y   = brwartran:y + brwartran:height-pixels + 0.01
      flTotal:x      = brwartran:x + brwartran:get-browse-column(1):x
      flCheckAmt:x   = brwartran:x + brwartran:get-browse-column(5):x + (brwartran:get-browse-column(5):width-pixels - flCheckAmt:width-pixels) + 10
      no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancelAgent eC-Win 
PROCEDURE cancelAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if can-find (first artran where artran.selectrecord)
   then
    do:
      message "Changes are pending to save. Want to continue?"                      
          view-as alert-box warning buttons yes-no update std-lo.
      
      if not std-lo /* if dont want to change the status */
       then return.
    end.
  
  assign
      browse brwartran:sensitive         = false            
             bAgentLookup:sensitive      = true
             flAgentID:sensitive         = true
             flName:sensitive            = true
             fType:sensitive             = true
             flAmount:sensitive          = true
             fCutoffDate:sensitive       = true
             rbFileBalance:sensitive     = true
             .

  /* Clears all data from screen */
  run clearScreen in this-procedure.
  
  apply 'value-changed' to browse brwartran.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clearScreen eC-Win 
PROCEDURE clearScreen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  empty temp-table artran.
  empty temp-table ttartran.
  empty temp-table tartran.
  
  /* Empties the browser when state is changed */ 
  run emptyBrowser in this-procedure. 
  
  resultsChanged(false).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow eC-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 publish "WindowClosed" (input this-procedure).   
 apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI eC-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(eC-Win)
  THEN DELETE WIDGET eC-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData eC-Win 
PROCEDURE displayData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  close query brwartran.
  empty temp-table artran.
  
  define buffer ttartran for ttartran.

  for each ttartran:
    create artran.
    buffer-copy ttartran to artran.    
  end.
  
  dataSortDesc = not dataSortDesc.
  if dataSortBy = ""
   then dataSortBy = "entityName".
  open query brwartran preselect each artran by artran.tranDate.
                                           
  rwRow = rowid(artran).
  
  apply 'value-changed' to browse brwartran.
  run DisplayTotals in this-procedure.
  run sortData in this-procedure (dataSortBy).
      
  setStatusCount(query brwartran:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayTotals eC-Win 
PROCEDURE displayTotals :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define buffer artran for artran.

  assign
   iCount = 0
   dewriteoffamt = 0.

  for each artran no-lock:
    if artran.selectrecord  
     then
      do:
        dewriteoffamt = dewriteoffamt + artran.remainingamt.
        flCheckAmt:format  = getFormattedNumber(input dewriteoffamt, input flCheckAmt:handle) no-error.
        flCheckAmt:screen-value = string(dewriteoffamt).
        iCount = iCount + 1.
      end.
  end.

  flCheckAmt:format       = getFormattedNumber(input dewriteoffamt, input flCheckAmt:handle) no-error.
  flCheckAmt:screen-value = string(dewriteoffamt).
  flTotal:screen-value    = "Totals of " + string(iCount) + " selected writeoff(s)".
  run adjustTotals in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE emptyBrowser eC-Win 
PROCEDURE emptyBrowser :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwartran.
  empty temp-table artran.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enablePosting eC-Win 
PROCEDURE enablePosting :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
    
  if not can-find(first ttARTran where ttARTran.selectrecord = true)
   then
    assign
        btpost:sensitive     = false
        fpostdate:sensitive  = false
        bPrelimRpt:sensitive = false 
        .
   else
    assign
        btpost:sensitive     = true
        fpostdate:sensitive  = true
        bPrelimRpt:sensitive = true 
        . 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI eC-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fpostdate flAmount fCutoffDate flAgentID flName rbFileBalance flTotal 
          flCheckAmt fType 
      WITH FRAME DEFAULT-FRAME IN WINDOW eC-Win.
  ENABLE brwartran btGo flAmount fCutoffDate flAgentID bAgentLookup flName 
         rbFileBalance fType RECT-79 RECT-85 RECT-77 
      WITH FRAME DEFAULT-FRAME IN WINDOW eC-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW eC-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData eC-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwartran:num-results = 0
   then
    do:
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.

  publish "GetReportDir" (output std-ch).

  empty temp-table tartran.

  for each artran:
    create tartran.
    buffer-copy artran to tartran.
  end.

  std-ha = temp-table tartran:handle.
  
  run util/exporttable.p (table-handle std-ha,
                          "tartran",
                          "for each tartran",
                          "artranID,fileID,entityID,entityName,tranDate,remainingAmt",
                          "Tran ID,File,Entity ID,Entity Name,Post Date,Balance",
                          std-ch,
                          "Write-offs-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
                            
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData eC-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define buffer ttartran for ttartran. 
    
  if fCutoffDate:input-value > today    
   then
    do:
      message "Cut Off Date cannot be in future."
          view-as alert-box.
      return.
    end.

 if flAmount:input-value = ?
   then
    do:
     message "Amount Date cannot be blank."
          view-as alert-box.
      return.
    end.
    
  if fCutoffDate:input-value = ?
   then
    do:
     message "Cut Off Date cannot be blank."
          view-as alert-box.
      return.
    end.
  
  assign
      browse brwartran:sensitive         = true            
             bAgentLookup:sensitive      = false
             flAgentID:sensitive         = false
             flName:sensitive            = false
             fType:sensitive             = false
             flAmount:sensitive          = false
             fCutoffDate:sensitive       = false
             rbFileBalance:sensitive     = false
             .
  close query brwartran.
  
  run server\getwriteoff.p (input {&Agent},                       
                            input flAgentID:screen-value,         
                            input {&File},
                            input flAmount:input-value,
                            input fCutoffDate:input-value,           
                            input rbFileBalance:input-value,                            
                            output table ttartran,                
                            output std-lo,                        
                            output std-ch).
  lGet = std-lo.
  
  if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box info buttons ok.
      return.
    end.
    
  run displayData in this-procedure.
  
  setStatusRecords(query brwartran:num-results). 
  reposition brwartran to rowid rwRow no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openApply eC-Win 
PROCEDURE openApply :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available artran
   then return.
   
  /* Show records based on the parameter list */
  publish "SetCurrentValue" ("ApplyParams", artran.entityID + "|" + {&Payment} + "|" + string(artran.arTranId)).
  
  publish "OpenWindow" (input "wapply", 
                        input {&Payment} + "|" + string(artran.arTranId), 
                        input "wapply.w", 
                        input ?,                                   
                        input this-procedure).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE postTransaction eC-Win 
PROCEDURE postTransaction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lViewPdf   as logical   no-undo.
  
  define buffer artran     for artran.
  define buffer tartran    for tartran.
  define buffer ttartran   for ttartran.

  empty temp-table tARTran.

  do with frame {&frame-name}:
  end.
  
  
 if fpostdate:input-value = ?
  then
   do:
     message "Post date cannot be blank."
         view-as alert-box error buttons ok.
     apply 'entry' to fpostdate.  
     return.      
   end.
 
 if fpostdate:input-value > today
  then
   do:
     message "Post date cannot be in the future."
         view-as alert-box error buttons ok.
     apply 'entry' to fpostdate.     
     return.
   end.
 
 publish "validatePostingDate" (input fpostdate:input-value,
                                output std-lo).
 
 if not std-lo
  then
   do:
     message "Post date must be within an open period."
         view-as alert-box error buttons ok.
     apply 'entry' to fpostdate.     
     return.
   end.
    
  if not can-find(first artran where artran.selectrecord = true)
   then
    do:
      message "Please select at least one AR invoice for posting."
          view-as alert-box error buttons ok.
      return.
    end.
      
  for each artran where artran.selectrecord = true:
    create tartran.
    buffer-copy artran to tartran.
  end.
  
  run dialogviewledger.w (input "Post Writeoff",output  std-lo).  /* ispost*/
   
  if not std-lo
   then
    return.
   
  publish "GetViewPdf" (output lViewPdf). 

  run server/postwriteoff.p (input lViewPdf,
                             input fpostdate:input-value,
                             input table tartran,
                             output table glwriteoffDetail,
                             output std-lo,
                             output std-ch). 
                                
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end. 
   else
     message "Posting was successful."
         view-as alert-box information buttons ok. 

  if lViewPdf
   then
    do:
      if not can-find(first glwriteoffDetail)
       then
        message "Nothing to print."
            view-as alert-box error buttons ok.
       else
        run util/arwriteoffpdf.p (input {&view},
                                      input table glwriteoffDetail,
                                      output std-ch).
    end.
  for each artran where artran.selectrecord = true :
    find first ttartran where ttartran.arTranID = artran.arTranID no-error.
    if available ttartran
     then
      do:
        delete ttartran.
      end.
    delete artran.
  end.
  
  run displayData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE prelimrpt eC-Win 
PROCEDURE prelimrpt :
do with frame {&frame-name}:
  end.

  define buffer artran for artran.
  
  empty temp-table tartran.
  
  for each artran where artran.selectrecord = true:
    create tartran.
    buffer-copy artran to tartran.
  end.

  run server/prelimwriteoffgl.p (input table tartran,
                                 output table glwriteoffDetail,
                                 output std-lo,
                                 output std-ch).
  
  
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
    
  if not can-find(first glwriteoffDetail) 
   then
    do:
      message "Nothing to print."
          view-as alert-box error buttons ok.
      return.
    end.
    /* Generate pdf gl report for the posted write-off's */                             
    run util\arwriteoffpdf.p (input {&view},
                           input table glwriteoffDetail,
                           output std-ch).
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState eC-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  if not available artran
   then
    assign  
        bExport:sensitive    = if (query brwartran:num-results = ?) then false else (query brwartran:num-results > 0)
        bPrelimRpt:sensitive = false
        btpost:sensitive     = false
        fpostdate:sensitive  = false
        .
   else
    assign
        bExport:sensitive    = if (query brwartran:num-results = ?) then false else (query brwartran:num-results > 0)
        bPrelimRpt:sensitive = not artran.void
        btpost:sensitive     = not artran.void
        fpostdate:sensitive  = not artran.void
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow eC-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData eC-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by artran.entityName ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized eC-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 14
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 23
      .
 
  {lib/resize-column.i &col="'Name'" &var=dColumnWidth}
  
  run adjustTotals in this-procedure.
  
  run ShowScrollBars(frame {&frame-name}:handle, no, no).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFormattedNumber eC-Win 
FUNCTION getFormattedNumber RETURNS CHARACTER
  ( input deTotal as decimal,
    input hWidget as handle ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cNumberFormat  as character no-undo.
  define variable cTotalScrValue as character no-undo.
  define variable deWidgetWidth  as decimal   no-undo.
  
  cTotalScrValue = string(deTotal).
     
  /* account for negative numbers */
  if deTotal < 0
   then 
    cNumberFormat = cNumberFormat + "(".
        
  /* loop through the absolute value of the number cast as an int64 */
  do std-in = length(string(int64(absolute(deTotal)))) to 1 by -1:
    if std-in modulo 3 = 0
     then 
      cNumberFormat = cNumberFormat + (if std-in = length(string(int64(absolute(deTotal)))) then ">" else ",>").
     else 
      cNumberFormat = cNumberFormat + (if std-in = 1 then "Z" else ">").
  end.
     
  /* if the number has a non zero value */
  if deTotal > 0 
   then 
    cNumberFormat = cNumberFormat + ".99".
         
  /* account for negative numbers */
  if deTotal < 0
   then 
    cNumberFormat = cNumberFormat + ".99)".

  do std-in = 1 to length(cNumberFormat):
    std-ch = substring(cNumberFormat,std-in,1).
    case std-ch:
      when "(" then deWidgetWidth = deWidgetWidth + 5.75.
      when ")" then deWidgetWidth = deWidgetWidth + 5.75.      
      when "Z" then deWidgetWidth = deWidgetWidth + 12.
      when ">" then deWidgetWidth = deWidgetWidth + 8.
      when "," then deWidgetWidth = deWidgetWidth + 4.
      when "." then deWidgetWidth = deWidgetWidth + 4.
      when "9" then deWidgetWidth = deWidgetWidth + 10.
    end case.
  end.
  
  /* set width of the widget as per format */
  hWidget:width-pixels = deWidgetWidth.      
        
  RETURN cNumberFormat.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged eC-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validAgent eC-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if flAgentID:input-value = {&ALL}
   then
    flName:screen-value = {&NotApplicable}.
       
  else if flAgentID:input-value <> {&ALL} and
          flAgentID:input-value <> ""
   then
    do:
      publish "getAgentName" (input flAgentID:input-value,
                              output std-ch,
                              output std-lo).                                               
      if not std-lo 
       then 
        do:
          assign 
              flAgentID:screen-value = "" 
              flName:screen-value    = ""
              .
          return false. /* Function return value. */
        end.
      flName:screen-value = std-ch.
    end. 
  
  resultsChanged(false).  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

