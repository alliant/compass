&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

File: wperiodprocessingglrpt.w 

Description: 

Input Parameters:
<none>

Output Parameters:
<none>

Author: Rahul Sharma

Created: 01/28/2020

Modified: 
  Date          Name           Description
  01/12/2021    Shefali        Modified to add filter by Batch Id.
  07/07/2021    Shefali        Task 84028 Modified to add posting date in grid.
  07/12/2021    Shefali        Task 84028 Added posting date as separate column 
                               in Export.
------------------------------------------------------------------------*/
/* This .W file was created with the Progress AppBuilder. */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
by this procedure. This is a good default which assures
that this procedure's triggers and internal procedures 
will execute in this procedure's storage, and that proper
cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ************************** Definitions ************************* */
{lib/std-def.i}
{lib/ar-def.i}
{lib/winlaunch.i}
{lib/winshowscrollbars.i}
{lib/getperiodname.i} /* Include function: getPeriodName */

/* Temp-table Definition */
{tt/glperiodprocessing.i}
{tt/glperiodprocessing.i &tableAlias=tglperiodprocessing}

define input parameter ipcPeriod  as character no-undo.
define input parameter ipcBatchId as character no-undo.

define variable dtStartDate    as date      no-undo.
define variable dtEndDate      as date      no-undo.
define variable iMonth         as integer   no-undo.
define variable iYear          as integer   no-undo.
define variable deDebit        as decimal   no-undo.
define variable deCredit       as decimal   no-undo.
define variable lgridChange    as logical   no-undo.
define variable cFilterBatchId as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES glperiodprocessing

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData glperiodprocessing.batchpostingdate glperiodprocessing.description glperiodprocessing.glRef glperiodprocessing.debit glperiodprocessing.credit   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each glperiodprocessing
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each glperiodprocessing.
&Scoped-define TABLES-IN-QUERY-brwData glperiodprocessing
&Scoped-define FIRST-TABLE-IN-QUERY-brwData glperiodprocessing


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btFilter bPeriod rsReportType cbBatchId ~
brwData bGo RECT-2 RECT-3 RECT-79 
&Scoped-Define DISPLAYED-OBJECTS fPeriod rsReportType cbBatchId flTotal ~
fDebit fCredit 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFormattedNumber C-Win 
FUNCTION getFormattedNumber RETURNS CHARACTER
  ( input deTotal as decimal,
    input hWidget as handle)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bGetCSV  NO-FOCUS
     LABEL "CSV" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE BUTTON bPeriod 
     LABEL "Period" 
     SIZE 5 BY 1.19 TOOLTIP "Select period".

DEFINE BUTTON btFilter  NO-FOCUS
     LABEL "Filter" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reset filters".

DEFINE VARIABLE cbBatchId AS CHARACTER FORMAT "X(256)" INITIAL "ALL" 
     LABEL "Batch ID" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 24.4 BY 1 NO-UNDO.

DEFINE VARIABLE fCredit AS DECIMAL FORMAT "(Z,ZZZ,ZZZ)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 19.2 BY 1 NO-UNDO.

DEFINE VARIABLE fDebit AS DECIMAL FORMAT "(Z,ZZZ,ZZZ)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 19.2 BY 1 NO-UNDO.

DEFINE VARIABLE flTotal AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 7.8 BY 1 NO-UNDO.

DEFINE VARIABLE fPeriod AS CHARACTER FORMAT "X(256)":U INITIAL "Select Period" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE rsReportType AS CHARACTER INITIAL "D" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Summary", "S",
"Detailed", "D"
     SIZE 29.4 BY 1.38 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 73 BY 2.62.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 11.4 BY 2.62.

DEFINE RECTANGLE RECT-79
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 46.6 BY 2.62.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      glperiodprocessing SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      glperiodprocessing.batchpostingdate label "Posting Date" format "99/99/99"      
glperiodprocessing.description    label "Description"  format "x(42)" width 35
glperiodprocessing.glRef          label "GL Ref"       format "x(28)" width 35  
glperiodprocessing.debit          label "Debit"        format "->,>>,>>>,>>>,>>9.99"    width 30  
glperiodprocessing.credit         label "Credit"       format "->,>>,>>>,>>>,>>9.99"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 130 BY 14.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     btFilter AT ROW 1.86 COL 112.6 WIDGET-ID 262
     fPeriod AT ROW 2.29 COL 3.4 COLON-ALIGNED NO-LABEL WIDGET-ID 444
     bPeriod AT ROW 2.14 COL 27.4 WIDGET-ID 402
     bGetCSV AT ROW 1.91 COL 123.8 WIDGET-ID 420 NO-TAB-STOP 
     rsReportType AT ROW 2.1 COL 34.8 NO-LABEL WIDGET-ID 456
     cbBatchId AT ROW 2.33 COL 85 COLON-ALIGNED WIDGET-ID 406
     brwData AT ROW 4.48 COL 3 WIDGET-ID 200
     bGo AT ROW 1.91 COL 66.4 WIDGET-ID 4
     flTotal AT ROW 19.52 COL 1 COLON-ALIGNED NO-LABEL WIDGET-ID 314 NO-TAB-STOP 
     fDebit AT ROW 19.52 COL 71.2 COLON-ALIGNED NO-LABEL WIDGET-ID 304 NO-TAB-STOP 
     fCredit AT ROW 19.52 COL 92.8 COLON-ALIGNED NO-LABEL WIDGET-ID 306 NO-TAB-STOP 
     "Parameters" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 1.24 COL 4.4 WIDGET-ID 412
     "Action" VIEW-AS TEXT
          SIZE 6.2 BY .62 AT ROW 1.24 COL 123 WIDGET-ID 438
     "Filters" VIEW-AS TEXT
          SIZE 5.4 BY .62 AT ROW 1.24 COL 77.2 WIDGET-ID 266
     RECT-2 AT ROW 1.48 COL 3 WIDGET-ID 76
     RECT-3 AT ROW 1.48 COL 121.8 WIDGET-ID 416
     RECT-79 AT ROW 1.48 COL 75.4 WIDGET-ID 270
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 134 BY 19.76 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Period Processing GL Transactions"
         HEIGHT             = 19.76
         WIDTH              = 134
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData cbBatchId fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bGetCSV IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR FILL-IN fCredit IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fDebit IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flTotal IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       flTotal:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN fPeriod IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       fPeriod:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each glperiodprocessing.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Period Processing GL Transactions */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Period Processing GL Transactions */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Period Processing GL Transactions */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGetCSV
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGetCSV C-Win
ON CHOOSE OF bGetCSV IN FRAME fMain /* CSV */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:   
  if (iMonth = ? or iMonth = 0) or (iYear = ? or iYear = 0) 
   then
    do:
      message "Please select valid period first."
          view-as alert-box info buttons ok.      
      return no-apply.
    end.
  
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPeriod C-Win
ON CHOOSE OF bPeriod IN FRAME fMain /* Period */
DO:
  run getPeriod in this-procedure.
  apply "choose":U to bGo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
  
  assign
      glperiodprocessing.debit:fgcolor in browse brwData = if (glperiodprocessing.debit < 0)   
                                                            then 12 
                                                            else ?
      glperiodprocessing.credit:fgcolor                  = if (glperiodprocessing.credit < 0)  
                                                            then 12
                                                            else ?       
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btFilter C-Win
ON CHOOSE OF btFilter IN FRAME fMain /* Filter */
do:    
   cbBatchId:screen-value  = {&all}.
   btFilter:sensitive      = false.
   run showData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbBatchId
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbBatchId C-Win
ON VALUE-CHANGED OF cbBatchId IN FRAME fMain /* Batch ID */
DO:
  btFilter:sensitive  = true.
  run showData  in this-procedure.
  if cbBatchId:screen-value = {&all} 
   then
    btFilter:sensitive = false.
  else
    btFilter:sensitive = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rsReportType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rsReportType C-Win
ON VALUE-CHANGED OF rsReportType IN FRAME fMain
DO:
  define variable hColDesc as handle no-undo.
  hColDesc  = brwData:get-browse-column(1).
  if rsReportType:input-value = {&Summary}
   then
    assign
        cbBatchId:screen-value   = {&All}
        cbBatchId:sensitive      = false
        btFilter:sensitive       = false
        cFilterBatchId           = ""
        .
   else if (rsReportType:input-value = {&Detail}) and (query brwData:num-results > 0) and hColDesc:visible = true
    then
     cbBatchId:sensitive     = true.
     
  lgridChange = false.
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */ 
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

assign 
  {&window-name}:min-height-pixels = {&window-name}:height-pixels
  {&window-name}:min-width-pixels  = {&window-name}:width-pixels
  {&window-name}:max-height-pixels = session:height-pixels
  {&window-name}:max-width-pixels  = session:width-pixels
  . 

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME} .

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow" anywhere.

bGo    :load-image            ("images/completed.bmp").
bGo    :load-image-insensitive("images/completed-i.bmp").

bGetCSV:load-image            ("images/excel.bmp").
bGetCSV:load-image-insensitive("images/excel-i.bmp").

bPeriod:load-image            ("images/s-calendar.bmp").
bPeriod:load-image-insensitive("images/s-calendar-i.bmp").

btFilter:load-image            ("images/filtererase.bmp").
btFilter:load-image-insensitive("images/filtererase-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
      
  RUN enable_UI.
  assign  
      btFilter:sensitive  = false
      cbBatchId:sensitive = false.
  
  flTotal:screen-value = "Totals". 
  
  if ipcPeriod ne "" or ipcBatchId ne "" 
   then
    do:
      assign
          iYear                     = integer(substring(string(ipcPeriod), 1 ,4))
          iMonth                    = integer(substring(string(ipcPeriod), 5 ,2))
          rsReportType:screen-value = {&Detail}
          fPeriod:screen-value      = getPeriodName(iMonth,iYear)
          .
      
      /* Calculate date range of selected period */
      publish "getSelectedPeriodDate" (input iMonth,
                                       input iYear,
                                       output dtStartDate,
                                       output dtEndDate).
          
      apply "choose":U to bGo.
      cbBatchId:screen-value    = if can-find(first glperiodprocessing) then ipcBatchId else {&all}.
    end.
   else 
    do:
      publish "getDefaultPeriod"(output iMonth,output iYear).
      fPeriod:screen-value = getPeriodName(iMonth,iYear).
    
      /* Calculate date range of selected period */
      publish "getSelectedPeriodDate" (input iMonth,
                                       input iYear,
                                       output dtStartDate,
                                       output dtEndDate).
    end.
    
  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.
    
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adjustTotals C-Win 
PROCEDURE adjustTotals :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if lgridChange 
   then
    do:
      if rsReportType:input-value = {&Summary}
       then
        assign
            brwData:get-browse-column(4):width-pixels = (27.5 / 100) *  brwData:width-pixels 
            brwData:get-browse-column(3):width-pixels = (40 / 100) *  brwData:width-pixels
            no-error.
       else
        assign
            brwData:get-browse-column(3):width-pixels = (18 / 100) *  brwData:width-pixels 
            brwData:get-browse-column(2):width-pixels = (26 / 100) *  brwData:width-pixels
            brwData:get-browse-column(4):width-pixels = (19 / 100) *  brwData:width-pixels
            no-error.
    end.
    
  assign       
      flTotal:y = brwData:y + brwData:height-pixels + 0.01
      fDebit:y  = brwData:y + brwData:height-pixels + 0.01
      fCredit:y = brwData:y + brwData:height-pixels + 0.01            
      flTotal:x = brwData:x + brwData:get-browse-column(1):x
      fDebit:x  = brwData:x + brwData:get-browse-column(4):x + (brwData:get-browse-column(4):width-pixels - fDebit:width-pixels)  + 6.8 
      fCredit:x = brwData:x + brwData:get-browse-column(5):x + (brwData:get-browse-column(5):width-pixels - fCredit:width-pixels) + 6.8       
      no-error.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE configBrowse C-Win 
PROCEDURE configBrowse :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hColDesc     as handle no-undo.
  define variable hColGlRef    as handle no-undo.
  define variable hColFile     as handle no-undo.
  define variable hColPostDate as handle no-undo.
  
  do with frame {&frame-name} :
  end.
    
  assign
      hColPostDate = brwData:get-browse-column(1)
      hColDesc     = brwData:get-browse-column(2)
      hColGlRef    = brwData:get-browse-column(3) 
      .
    
  if rsReportType:input-value = {&Summary} 
   then
    do: 
      if valid-handle(hColDesc)       
       then     
        hColDesc:visible = false.
        
      if valid-handle(hColPostDate)       
       then     
        hColPostDate:visible = false.
            
      if valid-handle(hColGlRef)     
       then
        hColGlRef:width = 90 .
    end.
   else
    do:
      if valid-handle(hColDesc) 
       then
        hColDesc:visible = true.
                
      if valid-handle(hColPostDate)       
       then     
        hColPostDate:visible = true.
        
      if valid-handle(hColDesc)  and 
         valid-handle(hColGlRef) and
         valid-handle(hColPostDate) 
       then
        assign 
            hColDesc:width     = 45
            hColGlRef:width    = 45
            hColPostDate:width = 15
            .                        
    end.
  lgridChange = true.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fPeriod rsReportType cbBatchId flTotal fDebit fCredit 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE btFilter bPeriod rsReportType cbBatchId brwData bGo RECT-2 RECT-3 
         RECT-79 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cTableField as character no-undo.
  define variable cColName    as character no-undo.
  define variable cFileName   as character no-undo.
  
  do with frame {&frame-name}:
  end.
  
  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
  
  if rsReportType:input-value = {&Summary}
   then
    assign
        cFileName   = "Processing_Summary_GL_Entries_For_Files_" + replace(string(dtStartDate), "/", "") + "_to_" + replace(string(dtEndDate), "/", "") + "_" + string(time) + ".csv"
        cColName    = "GL Ref,Debit,Credit"
        cTableField = "glRef,debit,credit"
        .
   else
    assign
        cFileName   = "Processing_Detailed_GL_Entries_For_Files_" + replace(string(dtStartDate), "/", "") + "_to_" + replace(string(dtEndDate), "/", "")  + "_" + string(time) + ".csv"
        cColName    = "Posting Date,Description,GL Ref,Debit,Credit"
        cTableField = "batchpostingdate,description,glRef,debit,credit"
        .
    
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table glperiodprocessing:handle.
  run util/exporttable.p (table-handle std-ha,
                          "glperiodprocessing",
                          "for each glperiodprocessing",
                          cTableField,
                          cColName,
                          std-ch,
                          cFileName,
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterInitialise C-Win 
PROCEDURE filterInitialise :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  cbBatchId:list-item-pairs = {&ALL} + "," + {&ALL}.

  /* Set the filters based on the data returned, with ALL as the first option */
  for each tglperiodprocessing
    break by tglperiodprocessing.batchid:
    if first-of(tglperiodprocessing.batchid)
     then
      cbBatchId:add-last(replace(string(tglperiodprocessing.batchid),",",""),string(tglperiodprocessing.batchid)).
  end.

  cbBatchId:screen-value = if (cFilterBatchId = "" or lookup(cFilterBatchId,cbBatchId:list-item-pairs) = 0) then {&ALL} else cFilterBatchId. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  close query brwData.
  empty temp-table tglperiodprocessing.
    
  do with frame {&frame-name}:
  end.  
  
  run server/queryglsummary.p (input  if ipcBatchId ne "" then integer(ipcBatchId) else 0, /* Batch ID */
                               input  dtStartDate,                                    /* Period start date */
                               input  dtEndDate,                                      /* Period end date */
                               output table tglperiodprocessing,
                               output std-lo,
                               output std-ch).

  
  if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box info buttons ok.
      return.
    end.
  
  run filterInitialise in this-procedure.
  run showData         in this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getPeriod C-Win 
PROCEDURE getPeriod :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  run dialogperiod.w (input-output iMonth,
                      input-output iYear,
                      output std-ch).
  
  resultsChanged(false).
  
  fPeriod:screen-value = getPeriodName(iMonth,iYear).
  
  /* Calculate date range of selected period */
  publish "getSelectedPeriodDate" (input iMonth,
                                   input iYear,
                                   output dtStartDate,
                                   output dtEndDate).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showData C-Win 
PROCEDURE showData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tglperiodprocessing for tglperiodprocessing.
  define buffer bfglperiodProcessing for glperiodprocessing.
  
  define variable cRef    as character no-undo.
  define variable ddebit  as decimal  no-undo.
  define variable dcredit as decimal  no-undo.
  
  close query brwData.
  empty temp-table glperiodprocessing.
    
  do with frame {&frame-name}:
  end.
  
  assign
      deDebit              = 0
      deCredit             = 0
      fCredit:screen-value = ""
      fDebit:screen-value  = ""
      .
  
  for each tglperiodprocessing 
   where tglperiodprocessing.batchid = (if cbBatchId:input-value = {&all} then tglperiodprocessing.batchid else integer(cbBatchId:input-value))
    :
    assign
        deDebit   = deDebit   +  tglperiodprocessing.debit
        deCredit  = deCredit  +  tglperiodprocessing.credit
        .
    create glperiodprocessing.
        
    buffer-copy tglperiodprocessing except tglperiodprocessing.description to glperiodprocessing.
    glperiodprocessing.description = string(tglperiodprocessing.batchid) + " " + string(date(tglperiodprocessing.batchpostingdate)). 
    
  end.

  if rsReportType:input-value = {&Summary}
   then
    do:
      for each bfglperiodProcessing break by glRef :
        if first-of (bfglperiodProcessing.glRef) 
         then
          do:
            assign
                dDebit  = 0
                dCredit = 0
                .
                
            for each glperiodProcessing where glperiodProcessing.glRef = bfglperiodProcessing.glRef :
                    
              assign
                  dDebit   = dDebit   +  glperiodprocessing.debit
                  dCredit  = dCredit  +  glperiodprocessing.credit
                  .
            end.
             
            assign
                bfglperiodProcessing.debit   = dDebit  
                bfglperiodProcessing.credit  = dCredit 
                .
          end.
        else
         delete bfglperiodProcessing.
      end. /* for each bfglperiodP */
    end. /* if rsReportType:input-value = {&Summary} */
   
     
  cFilterBatchId = cbBatchId:input-value.

  btFilter:sensitive = cbBatchId:input-value ne {&All}.
    
  /* Configure the browse based on the report type */
  run configBrowse in this-procedure.
  
  dataSortDesc = not dataSortDesc.
    
  run sortData in this-procedure ("").
  
  assign       
      fDebit:format  = getFormattedNumber(input deDebit,  input fDebit:handle)
      fCredit:format = getFormattedNumber(input deCredit, input fCredit:handle)  
      no-error.
  
  assign
      fDebit:screen-value  = string(deDebit)
      fCredit:screen-value = string(deCredit)
      .
  
  run adjustTotals in this-procedure.
  
  /* Makes widget enable-disable based on the data */ 
  if query brwData:num-results > 0 
   then
    assign 
        browse brwData:sensitive   = true               
               bGetCSV:sensitive   = true                              
               cbBatchId:sensitive = if rsReportType:input-value ne  {&Summary} then true else false
               .      
   else
    assign 
        browse brwData:sensitive   = false
               bGetCSV:sensitive   = false                               
               cbBatchId:sensitive = false
               .
    
  setStatusRecords(query brwData:num-results).  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  do with frame {&frame-name}:
  end.
  
  tWhereClause = " by glperiodprocessing.description by glperiodprocessing.glref ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end.
    
  assign      
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels          
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 18
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 25.5
      .
  
  run adjustTotals in this-procedure.
  
  run ShowScrollBars(frame {&frame-name}:handle, no, no).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFormattedNumber C-Win 
FUNCTION getFormattedNumber RETURNS CHARACTER
  ( input deTotal as decimal,
    input hWidget as handle) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cNumberFormat  as character no-undo.
  define variable cTotalScrValue as character no-undo.
  define variable deWidgetWidth  as decimal   no-undo.
  
  cTotalScrValue = string(deTotal).
     
  /* account for negative numbers */
  if deTotal < 0
   then 
    cNumberFormat = cNumberFormat + "(".
        
  /* loop through the absolute value of the number cast as an int64 */
  do std-in = length(string(int64(absolute(deTotal)))) to 1 by -1:
    if std-in modulo 3 = 0
     then 
      cNumberFormat = cNumberFormat + (if std-in = length(string(int64(absolute(deTotal)))) then ">" else ",>").
     else 
      cNumberFormat = cNumberFormat + (if std-in = 1 then "Z" else ">").
  end.
     
  /* if the number had a decimal value */
  if index(cTotalScrValue, ".") > 0 
   then 
    cNumberFormat = cNumberFormat + ".99".
         
  /* account for negative numbers */
  if deTotal < 0
   then 
    cNumberFormat = cNumberFormat + ")".
  
  do std-in = 1 to length(cNumberFormat):
    std-ch = substring(cNumberFormat,std-in,1).
    case std-ch:
      when "(" then deWidgetWidth = deWidgetWidth + 5.75.
      when ")" then deWidgetWidth = deWidgetWidth + 5.75.      
      when "Z" then deWidgetWidth = deWidgetWidth + 12.
      when ">" then deWidgetWidth = deWidgetWidth + 8.
      when "," then deWidgetWidth = deWidgetWidth + 4.
      when "." then deWidgetWidth = deWidgetWidth + 4.
      when "9" then deWidgetWidth = deWidgetWidth + 10.
    end case.
  end.
  
  /* set width of the widget as per format */
  hWidget:width-pixels = deWidgetWidth no-error.      
        
  RETURN cNumberFormat.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
  setStatusMessage({&ResultNotMatch}).   
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

