&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wnotetype.w

  Description: Window of system codes

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul Sharma

  Created: 12.06.2019

------------------------------------------------------------------------*/
create widget-pool.

{lib/winshowscrollbars.i}
{lib/std-def.i}
{lib/ar-def.i}

/* Temp-table definitions */
{tt/sysCode.i}
{tt/sysCode.i &tableAlias=tsyscode}
{tt/sysCode.i &tableAlias=ttsyscode}

/* Local Variable Definitions */
define variable cCodeType          as character  no-undo.
define variable cCode              as character  no-undo.
define variable lApplySearchString as logical    no-undo.
define variable cSearchString      as character  no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwCode

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tsyscode

/* Definitions for BROWSE brwCode                                       */
&Scoped-define FIELDS-IN-QUERY-brwCode tSysCode.code tSysCode.description "Template"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwCode   
&Scoped-define SELF-NAME brwCode
&Scoped-define QUERY-STRING-brwCode preselect each tsyscode by tsyscode.codetype by tsyscode.code
&Scoped-define OPEN-QUERY-brwCode open query brwCode preselect each tsyscode by tsyscode.codetype by tsyscode.code.
&Scoped-define TABLES-IN-QUERY-brwCode tsyscode
&Scoped-define FIRST-TABLE-IN-QUERY-brwCode tsyscode


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwCode}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-66 RECT-68 fSearch bSearch brwCode ~
bExport bRefresh bNew bcopy bdelete bEdit 
&Scoped-Define DISPLAYED-OBJECTS fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
    ()  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bcopy  NO-FOCUS
     LABEL "Copy" 
     SIZE 7.2 BY 1.71 TOOLTIP "Copy".

DEFINE BUTTON bdelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete".

DEFINE BUTTON bEdit  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Edit".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to excel".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh data".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search data".

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 44.6 BY 1 TOOLTIP "Search Criteria (Code Type, Code, Type, Description)" NO-UNDO.

DEFINE RECTANGLE RECT-66
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 44 BY 2.19.

DEFINE RECTANGLE RECT-68
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 55 BY 2.19.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwCode FOR 
      tsyscode SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwCode C-Win _FREEFORM
  QUERY brwCode DISPLAY
      tSysCode.code           label  "Type"          format "x(22)"           
tSysCode.description    label        "Template"   format "x(200)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 98.4 BY 16.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     fSearch AT ROW 1.95 COL 44.8 COLON-ALIGNED NO-LABEL WIDGET-ID 66
     bSearch AT ROW 1.62 COL 92.2 WIDGET-ID 308 NO-TAB-STOP 
     brwCode AT ROW 4 COL 2 WIDGET-ID 300
     bExport AT ROW 1.62 COL 9.8 WIDGET-ID 2 NO-TAB-STOP 
     bRefresh AT ROW 1.62 COL 2.8 WIDGET-ID 4 NO-TAB-STOP 
     bNew AT ROW 1.62 COL 16.8 WIDGET-ID 6 NO-TAB-STOP 
     bcopy AT ROW 1.62 COL 37.8 WIDGET-ID 316 NO-TAB-STOP 
     bdelete AT ROW 1.62 COL 30.8 WIDGET-ID 10 NO-TAB-STOP 
     bEdit AT ROW 1.62 COL 23.8 WIDGET-ID 8 NO-TAB-STOP 
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1 COL 3.6 WIDGET-ID 60
     "Search" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1 COL 47.6 WIDGET-ID 320
     RECT-66 AT ROW 1.38 COL 2 WIDGET-ID 58
     RECT-68 AT ROW 1.38 COL 45.6 WIDGET-ID 318
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 178.8 BY 21.52
         DEFAULT-BUTTON bSearch WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Note Types"
         HEIGHT             = 20.05
         WIDTH              = 100.4
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwCode bSearch fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwCode:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwCode:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwCode
/* Query rebuild information for BROWSE brwCode
     _START_FREEFORM
open query brwCode preselect each tsyscode by tsyscode.codetype by tsyscode.code.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwCode */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Note Types */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Note Types */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Note Types */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bcopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bcopy C-Win
ON CHOOSE OF bcopy IN FRAME fMain /* Copy */
do:
  run copyNoteType in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bdelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bdelete C-Win
ON CHOOSE OF bdelete IN FRAME fMain /* Delete */
do:
  run deleteNoteType in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEdit C-Win
ON CHOOSE OF bEdit IN FRAME fMain /* Edit */
do:
  run modifyNoteType in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
do:
  run newNoteType in this-procedure.  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
do:  
  /* refresh the syscode table in sysdatasrv with the databse then call 
     getData to update the browser with updated table syscode */
  run refreshData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwCode
&Scoped-define SELF-NAME brwCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCode C-Win
ON DEFAULT-ACTION OF brwCode IN FRAME fMain
do:
  apply "choose" to bedit.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCode C-Win
ON ROW-DISPLAY OF brwCode IN FRAME fMain
do:
  {lib/brw-rowDisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCode C-Win
ON START-SEARCH OF brwCode IN FRAME fMain
do:    
  {lib/brw-startSearch.i} 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME fMain /* Search */
DO:
  lApplySearchString = true.
  run filterData     in this-procedure.
  run setWidgetState in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON ENTRY OF fSearch IN FRAME fMain
DO:
  /* store the previous value of search string on which search is applied */
  cSearchString = fSearch:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON VALUE-CHANGED OF fSearch IN FRAME fMain
DO:
  /* as soon as we change the search string, we track that string 
  is not applied and change the status in taskbar */
  lApplySearchString = false.
  resultsChanged().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

assign
  {&window-name}:min-height-pixels = C-Win:height-pixels
  {&window-name}:min-width-pixels  = C-Win:width-pixels
  {&window-name}:max-height-pixels = session:height-pixels
  {&window-name}:max-width-pixels  = session:width-pixels
  .

assign current-window                = {&window-name} 
       this-procedure:current-window = {&window-name}.

on close of this-procedure 
  run disable_UI.

pause 0 before-hide.

bExport :load-image("images/excel.bmp").
bExport :load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/refresh.bmp").
bRefresh:load-image-insensitive("images/refresh-i.bmp").
bnew    :load-image("images/add.bmp").
bnew    :load-image-insensitive("images/add-i.bmp").
bEdit   :load-image("images/update.bmp").
bEdit   :load-image-insensitive("images/update-i.bmp").
bDelete :load-image("images/delete.bmp").
bDelete :load-image-insensitive("images/delete-i.bmp").
bSearch :load-image-up("images/magnifier.bmp").
bSearch :load-image-insensitive("images/magnifier-i.bmp").
bcopy   :load-image-up("images/copy.bmp").
bcopy   :load-image-insensitive("images/copy-i.bmp").

/* Initially search string (Blank) is applied */
lApplySearchString = true.

/* Get data from ardatasrv */
run getData in this-procedure.

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
      
  run enable_UI. 

  run setWidgetState in this-procedure.
  
  setStatusRecords(query brwCode:num-results). 

  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure. 
  
  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copyNoteType C-Win 
PROCEDURE copyNoteType :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define buffer ttsyscode for ttsyscode.   
  
  empty temp-table ttsyscode.

  if available tsyscode
   then
    do:
      create ttsyscode.
      buffer-copy tsyscode to ttsyscode.
      run dialognotetype.w (input {&copy},
                            input table ttsyscode,
                            output cCodeType,
                            output cCode,
                            output std-lo).
      if not std-lo 
       then
        return.      
    end.   

  run getData        in this-procedure.
  run setWidgetState in this-procedure.
  
  find first tsysCode where tsysCode.codeType = cCodeType 
                        and tsysCode.code     = cCode no-error.

  if available tsyscode
   then      
    reposition brwCode to rowid rowid(tsysCode) no-error.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteNoteType C-Win 
PROCEDURE deleteNoteType :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  if available tsyscode 
   then
    do:
      if tsyscode.isSecure 
       then
        do:
          message "Secure note type cannot be deleted."
              view-as  alert-box info buttons ok.
          return.
        end.
        
      assign 
          cCodeType = tsyscode.codetype
          cCode     = tsyscode.code
          .
    
      message "Note type will be deleted. Are you sure you want to delete ?" 
          view-as alert-box question buttons yes-no update std-lo.

      if not std-lo 
       then
        return.

      if std-lo 
       then
        do:
          publish "deleteSyscode" (input  cCodeType,
                                   input  cCode,
                                   output std-lo,
                                   output std-ch).
          if not std-lo 
           then
            do:
              message std-ch
                  view-as alert-box info buttons ok.
              return.
            end.                   
        end.
    end.
    
    run getData        in this-procedure.
    run setWidgetState in this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fSearch 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-66 RECT-68 fSearch bSearch brwCode bExport bRefresh bNew bcopy 
         bdelete bEdit 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define variable htableHandle as handle no-undo.    
  
  if query brwCode:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
   
  if lApplySearchString 
   then
    cSearchString = trim(fSearch:input-value).  

  htableHandle = temp-table tsyscode:handle.
  run util/exporttable.p (table-handle htableHandle,
                          "tsyscode",
                          "for each tsyscode",
                          "isSecure,codetype,code,type,description,Comments",
                          "Secure,Code Type,Code,Type,Description,Comments",
                          std-ch,
                          "ARNoteTypes-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer syscode for syscode.
  
  do with frame {&frame-name}:
  end.

  /* if search string is already applied then filter on the basis of what is
     present inside fsearch fill-in */
  if lApplySearchString 
   then
    cSearchString = trim(fSearch:input-value).
  /* if search string is changed but not applied then restrore the fSearch fill-in
     to previous applied serach string and filter on the basis of what is
     present inside fsearch fill-in */
   else
    fSearch:screen-value = cSearchString.

  empty temp-table tSysCode.
  for each syscode where syscode.codetype = "ARNoteType" and
                                            ((if cSearchString <> "" then syscode.codetype matches ("*" + cSearchString + "*")
                                             else syscode.codetype= syscode.codetype) or
                                            (if cSearchString <> "" then syscode.code matches ("*" + cSearchString + "*")
                                             else syscode.code= syscode.code) or
                                            (if cSearchString <> "" then syscode.type matches ("*" + cSearchString + "*")
                                             else syscode.type= syscode.type) or
                                            (if cSearchString <> "" then syscode.description matches ("*" + cSearchString + "*")
                                             else syscode.description = syscode.description)
                                             ) by syscode.codetype by syscode.code:
    
    create tSysCode.
    buffer-copy sysCode to tSysCode.

  end.

  open query brwCode preselect each tsyscode by tsyscode.codetype by tsyscode.code.

  setStatusCount(query brwCode:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  define buffer syscode for syscode.
  
  do with frame {&frame-name}:
  end.
  
  publish "getSysCodes" (input "ARNoteType", /* codeType, if blank means for all codeType */                   
                         output table SysCode).
  
  run filterData in this-procedure.  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyNoteType C-Win 
PROCEDURE modifyNoteType :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end.

  define buffer ttsyscode for ttsyscode.
  
  empty temp-table ttsyscode.
  
  if available tsyscode 
   then
    do:      
      create ttsyscode.
      buffer-copy tsyscode to ttsyscode.
      run dialognotetype.w (input {&modify},
                            input table ttsyscode,
                            output cCodeType,
                            output cCode,
                            output std-lo).
      if not std-lo 
       then
        return.      
    end.

  run getData        in this-procedure.
  run setWidgetState in this-procedure.
  
  find first tsysCode where tsysCode.codeType = cCodeType 
                        and tsysCode.code     = cCode  no-error.

  if available tsyscode
   then      
    reposition brwCode to rowid rowid(sysCode) no-error.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newNoteType C-Win 
PROCEDURE newNoteType :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define buffer ttsyscode for ttsyscode.
  
  empty temp-table ttsyscode.  

  run dialognotetype.w (input {&add},
                        input table ttsyscode,
                        output cCodeType,
                        output cCode,
                        output std-lo).
  if not std-lo 
   then
    return.
  
  run getData        in this-procedure.  
  run setWidgetState in this-procedure.
  
  find first tsysCode where tsysCode.codeType = cCodeType 
                        and tsysCode.code     = cCode  no-error.  

  if available tsyscode
   then      
    reposition brwCode to rowid rowid(tSysCode) no-error.    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData C-Win 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  empty temp-table syscode.

  publish "refreshSysCodes" (input "",                                                             
                             output std-lo,
                             output std-ch).
  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.

  run getData        in this-procedure.
  run setWidgetState in this-procedure.
  
  setStatusRecords(query brwCode:num-results).   

  lApplySearchString = true.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  assign
      bExport:sensitive = available tsyscode
      bedit:sensitive   = available tsyscode
      bcopy:sensitive   = available tsyscode
      bdelete:sensitive = available tsyscode
      .
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized then
     {&window-name}:window-state = window-normal .
  
  {&window-name}:move-to-top().  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame fMain:width-pixels          = {&window-name}:width-pixels
      frame fMain:virtual-width-pixels  = {&window-name}:width-pixels
      frame fMain:height-pixels         = {&window-name}:height-pixels
      frame fMain:virtual-height-pixels = {&window-name}:height-pixels        
      {&browse-name}:width-pixels        = frame {&frame-name}:width-pixels - 10
      {&browse-name}:height-pixels       = frame {&frame-name}:height-pixels - {&browse-name}:y - 3
      .
  run ShowScrollBars(browse brwCode:handle, no, yes).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
    () :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 setStatusMessage({&ResultNotMatchSearchString}). 
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

