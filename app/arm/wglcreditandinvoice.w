&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

File: wglcreditandinvoice.w 

Description: 

Input Parameters:
<none>              

Output Parameters:
<none>

Author: Sachin Chaturvedi

Created: 01/14/2021

Modified: 
Date        Name     Comments 
07/02/2021  Shefali  Modified to add write-off GL report
07/12/2021  Shefali  Task 84028 Added posting date as separate column 
                     in Export.
------------------------------------------------------------------------*/
/* This .W file was created with the Progress AppBuilder. */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
by this procedure. This is a good default which assures
that this procedure's triggers and internal procedures 
will execute in this procedure's storage, and that proper
cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ************************** Definitions ************************* */
{lib/ar-def.i}
{lib/std-def.i}
{lib/winlaunch.i}
{lib/winshowscrollbars.i}
{lib/brw-totalData-def.i}
{lib/getperiodname.i} /* Include function: getPeriodName */
{lib/ar-gettrantype.i}   /* Include function: getTranType */

/* Temp-table Definition */
{tt/glMisc.i &tableAlias=glCreditInv}
{tt/glMisc.i &tableAlias=tglCreditInv}
 
define variable dtStartDate  as date      no-undo.
define variable dtEndDate    as date      no-undo.
define variable tYear        as integer   no-undo.
define variable tMonth       as integer   no-undo.
define variable lgridChange  as logical   no-undo.
define variable cTranID      as character no-undo.
define variable dtFStartDate as date      no-undo.
define variable dtFEndDate   as date      no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES glCreditInv

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData getTranType(glCreditInv.type) @ glCreditInv.type glCreditInv.postingdate glCreditInv.description glCreditInv.arglRef glCreditInv.debit glCreditInv.credit getVoid(glCreditInv.voided) @ glCreditInv.voided glCreditInv.notes   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each glCreditInv by glCreditInv.type by glCreditInv.tranID
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each glCreditInv by glCreditInv.type by glCreditInv.tranID.
&Scoped-define TABLES-IN-QUERY-brwData glCreditInv
&Scoped-define FIRST-TABLE-IN-QUERY-brwData glCreditInv


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bGo bPeriod cbType rsReportType cbTranID ~
tVoided sEarliest brwData sLatest RECT-2 RECT-3 RECT-4 
&Scoped-Define DISPLAYED-OBJECTS fPeriod cbType rsReportType tEarliest ~
tLatest cbTranID tVoided sEarliest sLatest 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getVoid C-Win 
FUNCTION getVoid RETURNS CHARACTER
  ( ipVoid as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bFilterClear  NO-FOCUS
     LABEL "FilterClear" 
     SIZE 7.2 BY 1.71 TOOLTIP "Clear filters".

DEFINE BUTTON bGetCSV  NO-FOCUS
     LABEL "CSV" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get Data".

DEFINE BUTTON bPeriod 
     LABEL "Period" 
     SIZE 5 BY 1.19 TOOLTIP "Select Period".

DEFINE VARIABLE cbTranID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Tran ID" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 18.6 BY 1 NO-UNDO.

DEFINE VARIABLE cbType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 16.8 BY 1 NO-UNDO.

DEFINE VARIABLE fPeriod AS CHARACTER FORMAT "X(256)":U INITIAL "Select Period" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 22.6 BY 1 NO-UNDO.

DEFINE VARIABLE tEarliest AS DATE FORMAT "99/99/99":U 
     LABEL "From" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE tLatest AS DATE FORMAT "99/99/99":U 
     LABEL "To" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE rsReportType AS CHARACTER INITIAL "D" 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Summary", "S",
"Detailed", "D"
     SIZE 13 BY 2.38 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 56.2 BY 3.52.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 10.2 BY 3.52.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 86.2 BY 3.52.

DEFINE VARIABLE sEarliest AS INTEGER INITIAL 1 
     VIEW-AS SLIDER MIN-VALUE 1 MAX-VALUE 31 HORIZONTAL NO-CURRENT-VALUE 
     TIC-MARKS NONE 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE sLatest AS INTEGER INITIAL 31 
     VIEW-AS SLIDER MIN-VALUE 1 MAX-VALUE 31 HORIZONTAL NO-CURRENT-VALUE 
     TIC-MARKS NONE 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE tVoided AS LOGICAL INITIAL no 
     LABEL "Show only voids" 
     VIEW-AS TOGGLE-BOX
     SIZE 20 BY .81 TOOLTIP "Show only voids" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      glCreditInv SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      getTranType(glCreditInv.type) @ glCreditInv.type       label "Type"            format "x(10)"
glCreditInv.postingdate    label "Posting Date" format "99/99/99" 
glCreditInv.description    label "Description"  format "x(42)"    
glCreditInv.arglRef        label "GL Ref"       format "x(28)"   
glCreditInv.debit          label "Debit"        format "->,>>,>>>,>>>,>>9.99"  
glCreditInv.credit         label "Credit"       format "->,>>,>>>,>>>,>>9.99" 
getVoid(glCreditInv.voided) @ glCreditInv.voided label "Void"         format "x(10)"  
glCreditInv.notes          label "Notes"        format "x(320)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 189 BY 14.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bGetCSV AT ROW 2.29 COL 146.4 WIDGET-ID 420 NO-TAB-STOP 
     fPeriod AT ROW 2.19 COL 2.4 COLON-ALIGNED NO-LABEL WIDGET-ID 426
     bGo AT ROW 2.29 COL 50.2 WIDGET-ID 4
     bPeriod AT ROW 2.1 COL 27.4 WIDGET-ID 402
     cbType AT ROW 3.33 COL 8.2 COLON-ALIGNED WIDGET-ID 250
     rsReportType AT ROW 1.95 COL 34.8 NO-LABEL WIDGET-ID 456
     tEarliest AT ROW 2.14 COL 63.8 COLON-ALIGNED WIDGET-ID 2
     tLatest AT ROW 3.33 COL 63.8 COLON-ALIGNED WIDGET-ID 462
     cbTranID AT ROW 2 COL 112.2 COLON-ALIGNED WIDGET-ID 460
     tVoided AT ROW 3.24 COL 114.2 WIDGET-ID 464
     bFilterClear AT ROW 2.33 COL 135.8 WIDGET-ID 262 NO-TAB-STOP 
     sEarliest AT ROW 2.29 COL 79.4 NO-LABEL WIDGET-ID 6 NO-TAB-STOP 
     brwData AT ROW 5.14 COL 3 WIDGET-ID 200
     sLatest AT ROW 3.38 COL 79.4 NO-LABEL WIDGET-ID 8 NO-TAB-STOP 
     "Parameters" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 1.14 COL 4.2 WIDGET-ID 412
     "Action" VIEW-AS TEXT
          SIZE 6.2 BY .62 AT ROW 1.14 COL 146.8 WIDGET-ID 438
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.14 COL 60 WIDGET-ID 442
     RECT-2 AT ROW 1.38 COL 3 WIDGET-ID 76
     RECT-3 AT ROW 1.38 COL 144.8 WIDGET-ID 416
     RECT-4 AT ROW 1.38 COL 58.8 WIDGET-ID 440
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 192.4 BY 19.29 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Period Misc GL Transactions"
         HEIGHT             = 18.91
         WIDTH              = 192.6
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData sEarliest fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bFilterClear IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bGetCSV IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR FILL-IN fPeriod IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       fPeriod:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tEarliest IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tEarliest:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tLatest IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tLatest:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each glCreditInv by glCreditInv.type by glCreditInv.tranID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Period Misc GL Transactions */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Period Misc GL Transactions */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Period Misc GL Transactions */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilterClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilterClear C-Win
ON CHOOSE OF bFilterClear IN FRAME fMain /* FilterClear */
DO:  
  /* Reset filters to initial state */
  assign
      cbTranID:screen-value  = {&ALL}
      tVoided:checked        = false
      sEarliest:screen-value =  string(day(dtStartDate))
      sLatest:screen-value   =  string(day(dtEndDate))
      .
  run comboFilter in this-procedure.
  assign
      tEarliest:screen-value = string(date(month(tEarliest:input-value),
                                       day(dtStartDate),
                                       year(tEarliest:input-value)))
      tLatest:screen-value   = string(date(month(tLatest:input-value),
                                       day(dtEndDate),
                                       year(tLatest:input-value)))
                                       .                                     
                                       

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGetCSV
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGetCSV C-Win
ON CHOOSE OF bGetCSV IN FRAME fMain /* CSV */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:  
  run getData in this-procedure.           
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPeriod C-Win
ON CHOOSE OF bPeriod IN FRAME fMain /* Period */
DO:
  run getPeriod in this-procedure.
  apply "choose":U to bGo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
do:
 {lib/brw-rowdisplay.i}
  
  assign
      glCreditInv.debit:fgcolor in browse brwData = if (glCreditInv.debit < 0)   
                                                   then 12 
                                                   else ?
      glCreditInv.credit:fgcolor                  = if (glCreditInv.credit < 0)  
                                                   then 12
                                                   else ?       
      . 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbTranID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbTranID C-Win
ON VALUE-CHANGED OF cbTranID IN FRAME fMain /* Tran ID */
DO:
  run comboFilter in this-procedure.
  cTranID = cbTranID:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbType C-Win
ON VALUE-CHANGED OF cbType IN FRAME fMain /* Type */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rsReportType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rsReportType C-Win
ON VALUE-CHANGED OF rsReportType IN FRAME fMain
DO:
  define variable hType as handle no-undo.
  
  hType       = brwData:get-browse-column(1).

  if rsReportType:input-value = {&Summary}
   then
    do:
      publish "getSelectedPeriodDate"(input tMonth,input tYear,output dtFStartDate,output dtFEndDate).
      assign
          cbTranID:screen-value  = {&All}
          cbTranID:sensitive     = false
          tVoided:checked        = false
          tVoided:sensitive      = false
          bFilterClear:sensitive = false
          sEarliest:sensitive    = false
          sLatest:sensitive      = false
          sEarliest:screen-value = string(day(dtFStartDate))
          sLatest:screen-value   = string(day(dtFEndDate))  
          tEarliest:screen-value = string(date(month(tEarliest:input-value),
                                           day(dtFStartDate),
                                           year(tEarliest:input-value)))
          tLatest:screen-value = string(date(month(tLatest:input-value),
                                           day(dtFEndDate),
                                           year(tLatest:input-value)))
                                           .                                 
              
    end.    
        
   else if (rsReportType:input-value = {&Detail}) and (query brwData:num-results > 0) and hType:visible = true
    then
     assign  
         cbTranID:sensitive      = true
         tVoided:sensitive       = true
         sEarliest:sensitive     = true
         sLatest:sensitive       = true
         .


  lgridChange = false.
  resultsChanged(false).
  cTranID = cbTranID:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sEarliest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sEarliest C-Win
ON VALUE-CHANGED OF sEarliest IN FRAME fMain
DO:
  def var tMax as int.
  case month(tLatest:input-value):
      when 2 then tMax = 28.
      when 9 or when 4 or when 6 or when 11 then tMax = 30.
      otherwise tMax = 31.
  end.
  if self:input-value > tMax 
   then self:screen-value = string(tMax).
  
  tEarliest:screen-value = string(date(month(tEarliest:input-value),
                                       self:input-value,
                                       year(tEarliest:input-value))).
 
  if self:input-value > sLatest:input-value 
   then 
    do: sLatest:screen-value = string(self:input-value).
        tLatest:screen-value = tEarliest:screen-value.
  end.
  run comboFilter in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sLatest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sLatest C-Win
ON VALUE-CHANGED OF sLatest IN FRAME fMain
DO:
  def var tMax as int.
  case month(tLatest:input-value):
      when 2 then tMax = 28.
      when 9 or when 4 or when 6 or when 11 then tMax = 30.
      otherwise tMax = 31.
  end.
  if self:input-value > tMax 
   then self:screen-value = string(tMax).

  tLatest:screen-value = string(date(month(tLatest:input-value),
                                       self:input-value,
                                       year(tLatest:input-value))).
 
  if self:input-value < sEarliest:input-value 
   then 
    do: sEarliest:screen-value = string(self:input-value).
        tEarliest:screen-value = tLatest:screen-value.
  end.
  run comboFilter in this-procedure.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tVoided
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tVoided C-Win
ON VALUE-CHANGED OF tVoided IN FRAME fMain /* Show only voids */
DO:
  run comboFilter in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */ 
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

assign 
  {&window-name}:min-height-pixels = {&window-name}:height-pixels
  {&window-name}:min-width-pixels  = {&window-name}:width-pixels
  {&window-name}:max-height-pixels = session:height-pixels
  {&window-name}:max-width-pixels  = session:width-pixels
  .

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME} .

setStatusMessage("").  

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow" anywhere.

bGo         :load-image            ("images/completed.bmp").
bGo         :load-image-insensitive("images/completed-i.bmp").

bPeriod     :load-image            ("images/s-calendar.bmp").
bPeriod     :load-image-insensitive("images/s-calendar-i.bmp").

bGetCSV     :load-image            ("images/excel.bmp").
bGetCSV     :load-image-insensitive("images/excel-i.bmp").

bFilterClear  :load-image            ("images/filtererase.bmp").
bFilterClear  :load-image-insensitive("images/filtererase-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
      
  RUN enable_UI.
    
  publish "getDefaultPeriod"(output tMonth,output tYear).
  publish "getSelectedPeriodDate"(input tMonth,input tYear,output dtFStartDate,output dtFEndDate).

  assign
      fPeriod:screen-value   = getPeriodName(tMonth,tYear)
      cbType:list-item-pairs = "ALL,A,Credit,C,Invoice,I,Write-off,W"
      cbType:screen-value    = "A"
      cbTranID:sensitive     = false
      cbTranID:list-items    = {&ALL}
      cbTranID:screen-value  = {&All}
      tVoided:sensitive      = false
      tEarliest:screen-value = string(dtFStartDate)
      tLatest:screen-value   = string(dtFEndDate)
      sEarliest:sensitive    = false
      sLatest:sensitive      = false
      .  
  
  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE comboFilter C-Win 
PROCEDURE comboFilter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Enable/disable filter button based on selected values */
  run setFilterButton in this-procedure.

  /* This will use the screen-value of the filters which is ALL */
  run filterData in this-procedure. 
  run showData   in this-procedure.
  setStatusCount(query brwData:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE configBrowse C-Win 
PROCEDURE configBrowse :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hType      as handle no-undo.
  define variable hColDesc   as handle no-undo.
  define variable hColGlRef  as handle no-undo.
  define variable hColVoid   as handle no-undo.
  define variable hColNotes  as handle no-undo.
  define variable hPostDate  as handle no-undo. 
  define variable hColDebit  as handle no-undo.
  define variable hColCredit as handle no-undo.
  
  do with frame {&frame-name} :
  end.
    
  assign
      hType       = brwData:get-browse-column(1)
      hPostDate   = brwData:get-browse-column(2)
      hColDesc    = brwData:get-browse-column(3)
      hColGlRef   = brwData:get-browse-column(4)
      hColDebit   = brwData:get-browse-column(5)
      hColCredit  = brwData:get-browse-column(6)
      hColVoid    = brwData:get-browse-column(7)
      hColNotes   = brwData:get-browse-column(8)
      .
    
  if rsReportType:input-value = {&Summary} 
   then
    do: 
      if valid-handle(hType)       
       then     
        hType:visible = false.
        
      if valid-handle(hColDesc)       
       then     
        hColDesc:visible = false. 
      
      if valid-handle(hColVoid)       
       then     
        hColVoid:visible = false.
        
      if valid-handle(hColNotes)       
       then     
        hColNotes:visible = false.  
            
      if valid-handle(hColGlRef)     
       then
        assign 
            hColGlRef:width = 90
            .
    end.
   else
    do:
      if valid-handle(hType)       
       then     
        hType:visible = true.
        
      if valid-handle(hColDesc) 
       then
        hColDesc:visible = true.
        
      if valid-handle(hColVoid)       
       then     
        hColVoid:visible = true.
        
      if valid-handle(hColNotes)       
       then     
        hColNotes:visible = true.   
                
      if valid-handle(hColDesc)   and
         valid-handle(hType)      and
         valid-handle(hColGlRef)  and
         valid-handle(hColNotes)  and 
         valid-handle(hPostDate)  and
         valid-handle(hColVoid)   and
         valid-handle(hColDebit)  and 
         valid-handle(hColCredit)
       then
        assign
            hType:width        = 10
            hPostDate:width    = 15
            hColDesc:width     = 27.75
            hColGlRef:width    = 15
            hColNotes:width    = 30     
            hColVoid:width     = 9
            hColDebit:width    = 20
            hColCredit:width   = 20
            .                        
    end.
  lgridChange = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fPeriod cbType rsReportType tEarliest tLatest cbTranID tVoided 
          sEarliest sLatest 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bGo bPeriod cbType rsReportType cbTranID tVoided sEarliest brwData 
         sLatest RECT-2 RECT-3 RECT-4 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
 
  for each glCreditInv:
   assign glCreditInv.type = getTranType(glCreditInv.type).
  end.
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table glCreditInv:handle.
  run util/exporttable.p (table-handle std-ha,
                          "glCreditInv",
                          "for each glCreditInv",
                          if rsReportType:input-value = {&Summary} then "arglref,debit,credit" else "type,postingdate,description,arglref,debit,credit,Voided,notes",
                          if rsReportType:input-value = {&Summary} then "GL Ref,Debit,Credit" else  "Type,Posting Date,Description,GL Ref,Debit,Credit,Void,Notes", 
                          std-ch,
                          "Misc_GL_Trans_" + replace(string(dtStartDate), "/", "") + "_to_" + replace(string(dtEndDate), "/", "") + "_" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tglCreditInv  for tglCreditInv.
  define buffer bfglCreditInv for glCreditInv.
  
  define variable ddebit  as decimal  no-undo.
  define variable dcredit as decimal  no-undo.
    
  close query brwData. 
  empty temp-table glCreditInv.
  
  do with frame {&frame-name}:
  end.
      
  for each tglCreditInv 
    where tglCreditInv.tranID = (if cbtranID:input-value in frame {&frame-name} = {&ALL} then tglCreditInv.tranID else cbtranID:input-value) and 
        ((tglCreditInv.postingdate  ge datetime(tEarliest:screen-value))  and  (tglCreditInv.postingdate le datetime(tLatest:screen-value))) and
          tglCreditInv.voided = if tVoided:checked then "Yes" else tglCreditInv.voided
          :
    create glCreditInv.
    buffer-copy tglCreditInv except tglCreditInv.description to glCreditInv.
    glCreditInv.description = tglCreditInv.agentID + " " + tglCreditInv.tranid + " " + string(date(tglCreditInv.postingdate)).
  end.
  
  if rsReportType:input-value = {&Summary}
    then
     do:
       for each bfglCreditInv break by arglRef :
         if first-of (bfglCreditInv.arglRef) 
          then
           do:
             assign
                 dDebit  = 0
                 dCredit = 0
                 .
                 
             for each glCreditInv where glCreditInv.arglRef = bfglCreditInv.arglRef :
                     
               assign
                    dDebit   = dDebit   +  glCreditInv.debit
                    dCredit  = dCredit  +  glCreditInv.credit
                    .
             end.
              
             assign
                 bfglCreditInv.debit   = dDebit  
                 bfglCreditInv.credit  = dCredit 
                 .
           end.
         else
          delete bfglCreditInv.   
       end.
     end.
     
  /* Configure the browse based on the report type */
  run configBrowse in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/     
  do with frame {&frame-name}:
  end.  
  
  close query brwData.
  empty temp-table tglCreditInv.

  dtStartDate = date(tMonth,1,tYear).
  
  if tMonth = 12 
   then
    dtEndDate = date(1,1,tYear + 1) - 1.
   else
    dtEndDate = date(tMonth + 1, 1 ,tYear) - 1.

  
  run server/queryglcreditandinvoice.p (input  {&All},
                                        input cbType:input-value,
                                        input  dtStartDate,
                                        input  dtEndDate,
                                        output table tglCreditInv,
                                        output std-lo,
                                        output std-ch).
  
  if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box info buttons ok.
      return.
    end.

  run initialiseFilter in this-procedure.
  /* Enable/disable filter button based on selected values */
  run setFilterButton in this-procedure.

  if rsReportType:input-value = {&Summary}
   then
    assign
        cbTranID:screen-value   = {&All}
        cbTranID:sensitive      = false
        tVoided:checked         = false
        tVoided:sensitive       = false
        bFilterClear:sensitive  = false
        .
   else
    assign
       cbTranID:sensitive     = true
       tVoided:sensitive      = true
       sEarliest:sensitive    = true
       sLatest:sensitive      = true
       .
       
  run filterData in this-procedure.  
  run showData   in this-procedure.
  assign
      cbTranID:sensitive = (rsReportType:input-value ne {&Summary}) and (query brwData:num-results > 0)
      tVoided:sensitive  = cbTranID:sensitive
      .
  setStatusRecords(query brwData:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getPeriod C-Win 
PROCEDURE getPeriod :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  run dialogperiod.w (input-output tMonth,
                      input-output tYear,
                      output std-ch).
  
  resultsChanged(false).
  fPeriod:screen-value = getPeriodName(tMonth,tYear).
  
  publish "getSelectedPeriodDate"(input tMonth,input tYear,output dtFStartDate,output dtFEndDate).
  assign
      tEarliest:screen-value = string(dtFStartDate)
      tLatest:screen-value   = string(dtFEndDate)
      sEarliest:screen-value = string(day(dtFStartDate))
      sLatest:screen-value   = string(day(dtFEndDate))
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initialiseFilter C-Win 
PROCEDURE initialiseFilter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define buffer tglCreditInv for tglCreditInv.
  
  assign
      cbTranID:list-items    = ""
      cbTranID:list-items    = {&ALL}
      .

  /* Set the filters based on the data returned, with ALL as the first option */
  for each tglCreditInv
    break by tglCreditInv.tranid:
    if first-of(tglCreditInv.tranid) and tglCreditInv.tranid <> "" and tglCreditInv.tranid <> "?"
     then
      cbTranID:add-last(tglCreditInv.tranid).
  end.
  
  cbTranID:screen-value  = {&ALL}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterButton C-Win 
PROCEDURE setFilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if (cbTranID:input-value  ne {&ALL} and cbTranID:input-value  ne "") or 
     ((sEarliest:screen-value ne  string(day(dtStartDate))) or (sLatest:screen-value ne string(day(dtEndDate)))) or
     tVoided:checked
   then 
    bFilterClear:sensitive = true.
   else   
    bFilterClear:sensitive = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showData C-Win 
PROCEDURE showData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  dataSortDesc = not dataSortDesc.
    
  run sortData in this-procedure ("").
  
  /* Makes filters enable-disable based on the data */  
  cbTranID:sensitive in frame {&frame-name}  = query brwData:num-results > 0.
  
  if query brwData:num-results > 0 
   then
    assign 
        browse brwData:sensitive                        = true               
               bGetCSV:sensitive in frame {&frame-name} = true                              
               .      
   else
    assign 
        browse brwData:sensitive = false
               bGetCSV:sensitive = false                               
               .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by glCreditInv.type by glCreditInv.tranID ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
  {lib/brw-totalData.i &excludeColumn="1,2,3"} 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign      
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 18
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 18
      .
     if rsReportType:input-value = {&Summary}
       then
        assign
            brwData:get-browse-column(3):width-pixels = (23 / 100) *  brwData:width-pixels 
            brwData:get-browse-column(2):width-pixels = (12 / 100) *  brwData:width-pixels
            no-error.
 /* run ShowScrollBars(frame {&frame-name}:handle, no, no).    */
  {lib/brw-totalData.i &excludeColumn="1,2,3"}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getVoid C-Win 
FUNCTION getVoid RETURNS CHARACTER
  ( ipVoid as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if glCreditInv.voided = 'YES'
   then 
    return 'Yes'.
  else
   return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

