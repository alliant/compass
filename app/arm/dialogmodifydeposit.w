&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
  @Modified :
  Date        Name     Description 
  07/22/2020  AG       Modified to use deposit.posted instead of deposit.stat
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{tt/ardeposit.i}

/* Parameters Definitions ---                                           */
define input-output parameter table       for ardeposit.
define input        parameter ipcType     as  character no-undo.
define output       parameter oploSuccess as logical    no-undo.

/* Standard library files */
{lib/std-def.i}
{lib/ar-def.i}

/* Local Variable Definitions ---                                       */
define variable cBankID       as character no-undo.
define variable cDepositRef   as character no-undo.
define variable cDepositType  as character no-undo.
define variable deAmount      as decimal   no-undo.
define variable daDepositdate as date      no-undo.

define temp-table ttAction
   field action      as character
   field createdBy   as character
   field createdDate as character
   field ledgerID    as character.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwAction

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttAction

/* Definitions for BROWSE brwAction                                     */
&Scoped-define FIELDS-IN-QUERY-brwAction ttAction.Action ttAction.createdBy ttAction.CreatedDate ttAction.ledgerID   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAction   
&Scoped-define SELF-NAME brwAction
&Scoped-define QUERY-STRING-brwAction FOR EACH ttAction
&Scoped-define OPEN-QUERY-brwAction OPEN QUERY {&SELF-NAME} FOR EACH ttAction.
&Scoped-define TABLES-IN-QUERY-brwAction ttAction
&Scoped-define FIRST-TABLE-IN-QUERY-brwAction ttAction


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwAction}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS flBankID flDepositRef flDepositType ~
flDepositDate flAmount Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS flBankID flDepositRef flDepositType ~
flDepositDate flAmount fMarkMandatory1 fMarkMandatory2 fMarkMandatory3 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14 TOOLTIP "Save"
     BGCOLOR 8 .

DEFINE VARIABLE flAmount AS DECIMAL FORMAT "Z,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Amount" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE flBankID AS CHARACTER FORMAT "X(50)":U 
     LABEL "Bank ID" 
     VIEW-AS FILL-IN 
     SIZE 33 BY 1 NO-UNDO.

DEFINE VARIABLE flDepositDate AS DATE FORMAT "99/99/99":U 
     LABEL "Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 12.4 BY 1 NO-UNDO.

DEFINE VARIABLE flDepositRef AS CHARACTER FORMAT "X(50)":U 
     LABEL "Deposit Ref" 
     VIEW-AS FILL-IN 
     SIZE 33 BY 1 NO-UNDO.

DEFINE VARIABLE flDepositType AS CHARACTER FORMAT "X(50)":U 
     LABEL "Type" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fMarkMandatory1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory3 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAction FOR 
      ttAction SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAction Dialog-Frame _FREEFORM
  QUERY brwAction DISPLAY
      ttAction.Action               label  "Action"    format "x(15)"     
ttAction.createdBy              label  "By"              format "x(40)" 
ttAction.CreatedDate            label  "Date"            format "x(12)" 
ttAction.ledgerID               label  "Ledger ID"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-AUTO-VALIDATE NO-ROW-MARKERS NO-COLUMN-SCROLLING SEPARATORS NO-SCROLLBAR-VERTICAL SIZE 84.4 BY 3.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     brwAction AT ROW 5.52 COL 4.2 WIDGET-ID 200
     flBankID AT ROW 1.62 COL 15 COLON-ALIGNED WIDGET-ID 2
     flDepositRef AT ROW 2.91 COL 15 COLON-ALIGNED WIDGET-ID 4
     flDepositType AT ROW 2.91 COL 65.2 COLON-ALIGNED WIDGET-ID 6
     flDepositDate AT ROW 4.14 COL 15 COLON-ALIGNED WIDGET-ID 408
     flAmount AT ROW 4.14 COL 84.2 RIGHT-ALIGNED WIDGET-ID 8
     fMarkMandatory1 AT ROW 3.24 COL 48.4 COLON-ALIGNED NO-LABEL WIDGET-ID 52 NO-TAB-STOP 
     Btn_OK AT ROW 9.71 COL 30.6 WIDGET-ID 12
     Btn_Cancel AT ROW 9.71 COL 47.2 WIDGET-ID 10
     fMarkMandatory2 AT ROW 4.33 COL 83.6 COLON-ALIGNED NO-LABEL WIDGET-ID 268 NO-TAB-STOP 
     fMarkMandatory3 AT ROW 4.48 COL 27.8 COLON-ALIGNED NO-LABEL WIDGET-ID 410 NO-TAB-STOP 
     SPACE(58.19) SKIP(6.08)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Edit Deposit" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwAction 1 Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BROWSE brwAction IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flAmount IN FRAME Dialog-Frame
   ALIGN-R                                                              */
ASSIGN 
       flAmount:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flBankID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flDepositDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flDepositRef:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flDepositType:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fMarkMandatory1 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory2 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory3 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAction
/* Query rebuild information for BROWSE brwAction
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH ttAction.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAction */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Edit Deposit */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Save */
DO:
  run savedeposit in this-procedure.
  if not oploSuccess
   then
    return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAmount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAmount Dialog-Frame
ON VALUE-CHANGED OF flAmount IN FRAME Dialog-Frame /* Amount */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flBankID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flBankID Dialog-Frame
ON VALUE-CHANGED OF flBankID IN FRAME Dialog-Frame /* Bank ID */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flDepositDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flDepositDate Dialog-Frame
ON VALUE-CHANGED OF flDepositDate IN FRAME Dialog-Frame /* Date */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flDepositRef
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flDepositRef Dialog-Frame
ON VALUE-CHANGED OF flDepositRef IN FRAME Dialog-Frame /* Deposit Ref */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flDepositType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flDepositType Dialog-Frame
ON VALUE-CHANGED OF flDepositType IN FRAME Dialog-Frame /* Type */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAction
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  Frame Dialog-Frame:Title = (if ipcType = {&Modify} then "Edit Deposit" else "View Deposit").
  run displayData in this-procedure.
  run setWidgetState in this-procedure.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.     
  
  assign
      fMarkMandatory1:screen-value = {&Mandatory}
      fMarkMandatory2:screen-value = {&Mandatory}
      fMarkMandatory3:screen-value = {&Mandatory}
      .
         
  find first ardeposit no-error.
  if available ardeposit 
   then 
    do: 
      assign
          flBankID:screen-value         = ardeposit.bankID
          flDepositRef:screen-value     = ardeposit.depositRef
          flDepositType:screen-value    = ardeposit.depositType
          flDepositDate:screen-value    = string(ardeposit.depositDate)
          flAmount:screen-value         = string(ardeposit.amount)
          . 
       /* ttAction created to display created, posted and voided by data in dialog*/  
       create ttAction.
       assign
           ttAction.action       = "Created"
           ttAction.createdBy    = string(ardeposit.username)
           ttAction.CreatedDate  = if (ardeposit.createDate = ?) then "" else string(ardeposit.createdate)
           ttAction.ledgerID     = ""
           .
       create ttAction.
       assign
           ttAction.action       = "Posted"
           ttAction.createdBy    = ardeposit.postbyname
           ttAction.CreatedDate  = if (ardeposit.transDate = ?) then "" else string(ardeposit.transDate)
           ttAction.ledgerID     = if (ardeposit.ledgerID = 0 ) then "" else string(ardeposit.ledgerID)
           .
       create ttAction.
       assign
           ttAction.action       = "Void"
           ttAction.createdBy    = ardeposit.voidbyname
           ttAction.CreatedDate  = if (ardeposit.voiddate = ?) then "" else string(ardeposit.voiddate)
           ttAction.ledgerID     = if (ardeposit.voidLedgerID = 0) then "" else string(ardeposit.voidLedgerID)
           .
       
       open query brwAction for each ttAction.
       brwAction:Deselect-focused-row().       
    end.
  
  assign
        cBankID       = flBankID:input-value
        cDepositRef   = flDepositRef:input-value
        cDepositType  = flDepositType:input-value
        deAmount      = flAmount:input-value
        daDepositDate = flDepositDate:input-value
        .
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  Btn_OK:sensitive = ((flDepositRef:input-value  <> "" and
                       flAmount:input-value      <> 0  and
                       flDepositDate:input-value <> ?) and
                      (cBankID       <> flBankID:input-value      or
                       cDepositRef   <> flDepositRef:input-value  or
                       cDepositType  <> flDepositType:input-value or
                       daDepositDate <> flDepositDate:input-value or
                       deAmount      <> flAmount:input-value)  
                      ) no-error.
                        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flBankID flDepositRef flDepositType flDepositDate flAmount 
          fMarkMandatory1 fMarkMandatory2 fMarkMandatory3 
      WITH FRAME Dialog-Frame.
  ENABLE flBankID flDepositRef flDepositType flDepositDate flAmount Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveDeposit Dialog-Frame 
PROCEDURE saveDeposit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  do with frame {&frame-name}:
  end.
 
  oploSuccess = false.
    
  if flDepositDate:input-value > today
   then
    do:  
      message "Deposit Date cannot be in future"
        view-as alert-box info button ok.
      return.
    end.
      
  find first ardeposit no-error.
  if not available ardeposit
   then
    create ardeposit.
    
  assign
      ardeposit.bankID      = flBankID:input-value
      ardeposit.depositRef  = flDepositRef:input-value
      ardeposit.depositType = flDepositType:input-value
      ardeposit.amount      = flAmount:input-value
      ardeposit.depositdate = flDepositDate:input-value
      .
  
  run server/modifydeposit.p (input table ardeposit,
                              output oploSuccess,                                                                                       
                              output std-ch).
  
  if not oploSuccess                         
   then
    do:
      message std-ch 
        view-as alert-box.
      return.  
    end.
    
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState Dialog-Frame 
PROCEDURE setWidgetState :
/*------- -----------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if ipcType = {&Modify} 
   then
    assign
        flBankID:read-only      = false 
        flDepositRef:read-only  = false  
        flDepositDate:read-only = false 
        flDepositType:read-only = false 
        flAmount:read-only      = false 
        .
   else
    assign
        flBankID:read-only      = true
        flDepositRef:read-only  = true 
        flDepositDate:read-only = true
        flDepositType:read-only = true
        flAmount:read-only      = true
        .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

