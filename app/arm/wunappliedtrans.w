&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

File: wunappliedtrans.w 

Description:  unapplied cash of agent

Input Parameters:
<none>

Output Parameters:
<none>

Author: Rahul 

Created:

Modified: 
Date         Name        Comments
12/22/2020   Shubham     Added Type Dropdown for Payments(P) and Credits(C)  
12/08/2022   Shefali     Task-#100854 Add status on Unapplied Transactions report

------------------------------------------------------------------------*/
/* This .W file was created with the Progress AppBuilder. */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
by this procedure. This is a good default which assures
that this procedure's triggers and internal procedures 
will execute in this procedure's storage, and that proper
cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ************************** Definitions ************************* */
{tt/state.i}
{tt/agentunappliedCash.i}
{tt/agentunappliedCash.i &tableAlias="ttagentunappliedCash"}

{tt/armisc.i  &tablealias=ttArMisc}    /* used to show header detail */
{tt/arpmt.i   &tablealias=ttArPmt}     /* used to show header detail */

{lib/std-def.i}
{lib/ar-def.i}
{lib/get-column.i}
{lib/winlaunch.i}

define input parameter ipcType as character no-undo.

define variable dColumnWidth       as decimal   no-undo.
define variable opcMsg             as character no-undo.
define variable cAgentID           as character no-undo.
define variable lDefaultAgent      as logical   no-undo.

define variable iSelectedArTranID as integer   no-undo.
define variable hPopupMenu        as handle    no-undo.
define variable hPopupMenuItem    as handle    no-undo.
define variable hPopupMenuItem1   as handle    no-undo.
define variable hPopupMenuItem2   as handle    no-undo.

define variable hColumn           as handle    no-undo.
define variable hColumn1          as handle    no-undo.
define variable hColumn2          as handle    no-undo.
define variable hColumn3          as handle    no-undo.
define variable hColumn4          as handle    no-undo.
define variable hColumn5          as handle    no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttagentunappliedCash

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData ttagentunappliedCash.stat ttagentunappliedCash.agentID ttagentunappliedCash.name ttagentunappliedCash.fileNumber ttagentunappliedCash.checknum ttagentunappliedCash.checkAmt ttagentunappliedCash.appliedAmt ttagentunappliedCash.refundedAmt ttagentunappliedCash.remainingAmt ttagentunappliedCash.checkDate ttagentunappliedCash.receiptDate ttagentunappliedCash.postDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each ttagentunappliedCash
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each ttagentunappliedCash.
&Scoped-define TABLES-IN-QUERY-brwData ttagentunappliedCash
&Scoped-define FIRST-TABLE-IN-QUERY-brwData ttagentunappliedCash


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fAgent bAgentLookup bRefresh cbState cbType ~
brwData RECT-1 RECT-3 
&Scoped-Define DISPLAYED-OBJECTS fAgent cbState cbType fName 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getManager C-Win 
FUNCTION getManager RETURNS CHARACTER
  ( input ipcUID as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup 
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Agent Lookup".

DEFINE BUTTON bCSV  NO-FOCUS
     LABEL "CSV" 
     SIZE 7.2 BY 1.67 TOOLTIP "Export".

DEFINE BUTTON bOpen  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.67 TOOLTIP "Open detail".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.67 TOOLTIP "Get Data".

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 17.6 BY 1 NO-UNDO.

DEFINE VARIABLE cbType AS CHARACTER FORMAT "X(256)":U INITIAL "P" 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Payment","P",
                     "Credit","C"
     DROP-DOWN-LIST
     SIZE 17.6 BY 1 NO-UNDO.

DEFINE VARIABLE fAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 77.8 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 97.8 BY 3.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17.2 BY 3.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      ttagentunappliedCash SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      ttagentunappliedCash.stat               label "Status"              format "x(20)"             width 10
ttagentunappliedCash.agentID            label "Agent ID"            format "x(15)"             width 12
ttagentunappliedCash.name               label "Name"                format "x(100)"            width 40
ttagentunappliedCash.fileNumber         label "File Number"         format "x(30)"             width 18
ttagentunappliedCash.checknum           label "Check/Reference"     format "x(35)"             width 20
ttagentunappliedCash.checkAmt           label "Payment"             format "->,>>>,>>9.99"     width 17
ttagentunappliedCash.appliedAmt         label "Applied"             format "->,>>>,>>9.99"     width 17
ttagentunappliedCash.refundedAmt        label "Refunded"            format "->,>>>,>>9.99"     width 17
ttagentunappliedCash.remainingAmt       label "Remaining"           format "->,>>>,>>9.99"     width 17
ttagentunappliedCash.checkDate          label "Check"               format "99/99/99"          width 12
ttagentunappliedCash.receiptDate        label "Receipt"             format "99/99/99"          width 12
ttagentunappliedCash.postDate           label "Post"                format "99/99/99"          width 12
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 191.8 BY 13.95 ROW-HEIGHT-CHARS .8 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bCSV AT ROW 2 COL 102 WIDGET-ID 442
     fAgent AT ROW 1.76 COL 10 COLON-ALIGNED WIDGET-ID 430
     bAgentLookup AT ROW 1.71 COL 26.2 WIDGET-ID 434
     bRefresh AT ROW 2 COL 91.6 WIDGET-ID 4
     cbState AT ROW 1.76 COL 37 WIDGET-ID 436
     cbType AT ROW 1.76 COL 66.2 WIDGET-ID 452
     fName AT ROW 2.95 COL 10 COLON-ALIGNED NO-LABEL WIDGET-ID 432
     brwData AT ROW 4.76 COL 3.2 WIDGET-ID 200
     bOpen AT ROW 2 COL 109.2 WIDGET-ID 450
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.14 COL 4.4 WIDGET-ID 48
     "Action" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.14 COL 101.6 WIDGET-ID 446
     RECT-1 AT ROW 1.38 COL 3.2 WIDGET-ID 46
     RECT-3 AT ROW 1.38 COL 100.6 WIDGET-ID 444
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 195.2 BY 18 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Unapplied Transactions"
         HEIGHT             = 17.81
         WIDTH              = 196
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData fName fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bCSV IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bOpen IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR COMBO-BOX cbState IN FRAME fMain
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cbType IN FRAME fMain
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN fName IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each ttagentunappliedCash.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Unapplied Transactions */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Unapplied Transactions */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Unapplied Transactions */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup C-Win
ON CHOOSE OF bAgentLookup IN FRAME fMain /* agentlookup */
DO:
  define variable cAgentID  as character no-undo.
  define variable cName     as character no-undo.
    
  run dialogagentlookup.w(input fAgent:input-value,
                          input cbState:input-value,      /* Selected State ID */
                          input true,                     /* Allow 'ALL' */
                          output cAgentID,
                          output std-ch,                  /* Agent state ID */
                          output cName,
                          output std-lo).
   
  if not std-lo or fAgent:input-value = cAgentID  
   then
    return no-apply.
     
  assign
      cbState:screen-value  = (if lookup(std-ch,cbState:list-item-pairs) > 0 then std-ch else {&ALL})
      fAgent:screen-value   = cAgentID
      fName:screen-value    = cName      
      . 
      
  resultsChanged(false).
  
  if lDefaultAgent            and
     fAgent:input-value <> "" and
     fAgent:input-value <> {&ALL}
   then
    /* Set default AgentID */
    publish "SetDefaultAgent" (input fAgent:input-value).
  
  if fAgent:input-value <> "" 
   then
    run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCSV
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCSV C-Win
ON CHOOSE OF bCSV IN FRAME fMain /* CSV */
DO:
  case cbType:screen-value: 
    when {&Payment} /* 'P' type Payment */
     then run exportPaymentData in this-procedure.
    when {&Credit} /* 'C' type Misc Credit */
     then run exportCreditData in this-procedure.
  end case.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOpen C-Win
ON CHOOSE OF bOpen IN FRAME fMain /* Open */
DO:
  run viewDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Go */
or 'RETURN' of fAgent
DO:
  if not validAgent()
   then
    return no-apply.
  
  run getData in this-procedure.           
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  run viewDetail in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  iSelectedArTranID = ttagentunappliedCash.artranid no-error.
  bOpen:sensitive   = (available ttagentunappliedCash).
  run ChangePopupMenu in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState C-Win
ON VALUE-CHANGED OF cbState IN FRAME fMain /* State */
DO:
  resultsChanged(false).
  
  assign
      fAgent:screen-value = {&ALL}
      fName:screen-value  = {&NotApplicable}
      .     
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbType C-Win
ON VALUE-CHANGED OF cbType IN FRAME fMain /* Type */
DO:
  resultsChanged(false). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAgent C-Win
ON VALUE-CHANGED OF fAgent IN FRAME fMain /* Agent */
DO:
  resultsChanged(false).
  assign      
      fName:screen-value   = ""
      bOpen:sensitive      = false
      bCSV:sensitive       = false
      . 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */ 
{&window-name}:window-state = window-minimized. 

{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}
 
assign 
    {&window-name}:min-height-pixels = {&window-name}:height-pixels
    {&window-name}:min-width-pixels  = {&window-name}:width-pixels
    {&window-name}:max-height-pixels = session:height-pixels
    {&window-name}:max-width-pixels  = session:width-pixels
    .

ASSIGN 
    CURRENT-WINDOW                = {&WINDOW-NAME} 
    THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME} .


setStatusMessage("").       
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bCSV     :load-image               ("images/excel.bmp").
bCSV     :load-image-insensitive   ("images/excel-i.bmp").

bopen    :load-image               ("images/open.bmp").
bopen    :load-image-insensitive   ("images/open-i.bmp").

bRefresh:load-image                ("images/completed.bmp").
bRefresh:load-image-insensitive    ("images/completed-i.bmp").

bAgentLookup:load-image            ("images/s-lookup.bmp").
bAgentLookup:load-image-insensitive("images/s-lookup-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
  RUN enable_UI.
  
  {lib/get-column-width.i &col="'Name'"  &var=dColumnWidth} 
  
  /* create state combo */
  {lib/get-state-list.i &combo=cbState &addAll=true}
  
  publish "GetAutoDefaultAgent" (output lDefaultAgent).
  
  if lDefaultAgent
   then
    do:
      publish "GetDefaultAgent"(output cAgentID).
      fAgent:screen-value = cAgentID.         
    end.
  
  cbType:screen-value in frame fmain = ipcType.
  run setBrowse in this-procedure.
  
  run ShowWindow in this-procedure.
  apply "entry" to fAgent.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changePopupMenu C-Win 
PROCEDURE changePopupMenu :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available ttagentunappliedCash
   then return.
     
  if valid-handle(hPopupMenuItem) 
   then
    delete object hPopupMenuItem.

  if valid-handle(hPopupMenuItem1) 
   then
    delete object hPopupMenuItem1.  
  
  if valid-handle(hPopupMenuItem2) 
   then
    delete object hPopupMenuItem2.

  if valid-handle(hPopupMenu) 
   then
    delete object hPopupMenu.
              
  create menu hPopupMenu.
  assign
      hPopupMenu:popup-only = true
      hPopupMenu:title      = "Browser menu"
      .
  
  /* As the popup menu differes depending on the type 
     of header record, so dynamically creating the popup
     menu specific for each artran type. */
 do with frame fMain:
 end.
 
 case cbType:screen-value: 
   when {&Payment} /* 'P' type Payment */
    then
     do:
       create menu-item hPopupMenuItem
       assign
           parent = hPopupMenu
           label  = "Transaction Detail"
           name   = "Transaction Detail"
                  
       triggers:
         on choose persistent run viewDetail in this-procedure.
       end triggers.
    
       create menu-item hPopupMenuItem1
       assign
           parent = hPopupMenu
           label  = "Payment Detail"
           name   = "Payment Detail"
                  
       triggers:
         on choose persistent run showHeaderDetails in this-procedure.
       end triggers.
       
       create menu-item hPopupMenuItem2
       assign
           parent    = hPopupMenu
           label     = "Apply Payment"
           name      = "Apply Payment"
                  
       triggers:
         on choose persistent run openApply in this-procedure.
       end triggers.
       
       self:popup-menu = hPopupMenu.
     end.
       
   when {&Credit} /* 'C' type Misc Credit */
    then
     do:
       create menu-item hPopupMenuItem
       assign
           parent = hPopupMenu
           label  = "Transaction Detail"
           name   = "Transaction Detail"
                  
       triggers:
         on choose persistent run viewDetail in this-procedure.
       end triggers.
      
       create menu-item hPopupMenuItem1
       assign 
           label  = "Credit Detail"
           name   = "Credit Detail"
           parent = hPopupMenu
       triggers:
         on choose persistent run showHeaderDetails in this-procedure.
       end triggers.
       
       create menu-item hPopupMenuItem2
       assign
           parent    = hPopupMenu
           label     = "Apply Credit"
           name      = "Apply Credit"
           
       triggers:
         on choose persistent run openApply in this-procedure.
       end triggers.
       
       self:popup-menu = hPopupMenu.
     end.
    
 end case.
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "WindowClosed" (input this-procedure).   
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fAgent cbState cbType fName 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE fAgent bAgentLookup bRefresh cbState cbType brwData RECT-1 RECT-3 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportCreditData C-Win 
PROCEDURE exportCreditData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttagentunappliedCash for ttagentunappliedCash.
  
  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
    
  empty temp-table agentunappliedCash.
  
  for each ttagentunappliedCash:
    create agentunappliedCash.
    buffer-copy ttagentunappliedCash to agentunappliedCash.                  
    agentunappliedCash.manager = getManager(agentunappliedCash.manager).
  end.
  
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table agentunappliedCash:handle.
  run util/exporttable.p (table-handle std-ha,
                          "agentunappliedCash",
                          "for each agentunappliedCash",
                          "stat,stateID,agentID,name,manager,FileNumber,checknum,checkAmt,appliedAmt,remainingAmt,checkDate,postDate,depositRef,arCashglRef,arglref",
                          "Status,StateID,Agent ID,Name,Manager,File Number,Check/Reference,Credit,Applied,Balance,Check,Post,DepositRef,ArCashglRef,Arglref",
                          std-ch,
                          "AgentUnappliedCash_" + replace(string(today), "/", "") + "_" + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportPaymentData C-Win 
PROCEDURE exportPaymentData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttagentunappliedCash for ttagentunappliedCash.
  
  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
    
  empty temp-table agentunappliedCash.
  
  for each ttagentunappliedCash:
    create agentunappliedCash.
    buffer-copy ttagentunappliedCash to agentunappliedCash.                  
    agentunappliedCash.manager = getManager(agentunappliedCash.manager).
  end.
  
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table agentunappliedCash:handle.
  run util/exporttable.p (table-handle std-ha,
                          "agentunappliedCash",
                          "for each agentunappliedCash",
                          "stat,stateID,agentID,name,manager,FileNumber,checknum,checkAmt,appliedAmt,refundedAmt,remainingAmt,receiptDate,depositRef,arCashglRef,arglref",
                          "Status,StateID,Agent ID,Name,Manager,File Number,Check/Reference,Payment,Applied,Refunded,Remaining,Receipt,DepositRef,ArCashglRef,Arglref",
                          std-ch,
                          "AgentUnappliedCash_" + replace(string(today), "/", "") + "_" + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end.
  
  empty temp-table ttagentunappliedCash.
  empty temp-table agentunappliedCash.

  run server/queryagentunappliedtrans.p (input cbState:screen-value,
                                         input fAgent:screen-value,
                                         input cbType:input-value,
                                         output table agentunappliedCash,
                                         output std-lo,
                                         output std-ch).
  
  for each agentunappliedCash by agentunappliedCash.stateID by agentunappliedCash.agentID:    
    create ttagentunappliedCash.
    buffer-copy agentunappliedCash to ttagentunappliedCash.    
  end.
  
  if can-find(first ttagentunappliedCash)
   then
    assign
        bOpen:sensitive   = true
        bCSV:sensitive    = true
        .
   else
    assign
        bCSV:sensitive    = false
        bOpen:sensitive   = false
        .  
        
  run setBrowse in this-procedure.
  
  open query brwData preselect each ttagentunappliedCash by ttagentunappliedCash.name.  
  
  if can-find(first ttagentunappliedCash) 
   then
    apply 'value-changed' to browse brwData . 
  
  setStatusRecords(query brwData:num-results).    
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openApply C-Win 
PROCEDURE openApply :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available ttagentunappliedCash
   then return.
   
  do with frame fmain:
  end.
  
  case cbtype:screen-value:
    when {&Payment} /* 'P' type Payment */
     then
      do:
        /* Show records based on the parameter list */
        publish "SetCurrentValue" ("ApplyParams", ttagentunappliedCash.agentID + "|" + {&Payment} + "|" + string(ttagentunappliedCash.artranID)).
  
        publish "OpenWindow" (input "wapply", 
                              input {&Payment} + "|" + string(ttagentunappliedCash.artranID), 
                              input "wapply.w", 
                              input ?,                                   
                              input this-procedure).
      end.
      
    when {&Credit} /* 'C' type Misc Credit */
     then
      do:
        /* Show records based on the parameter list */
        publish "SetCurrentValue" ("ApplyParams", ttagentunappliedCash.agentID + "|" + {&Credit} + "|" + string(ttagentunappliedCash.artranID)).
  
        publish "OpenWindow" (input "wapply", 
                              input {&Credit} + "|" + string(ttagentunappliedCash.artranID), 
                              input "wapply.w", 
                              input ?,                                   
                              input this-procedure).            
      end.
       
  end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setBrowse C-Win 
PROCEDURE setBrowse :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame fmain:
  end.
  
  assign
      hColumn  = brwData:get-browse-column(6)
      hColumn1 = brwData:get-browse-column(9)
      hColumn2 = brwData:get-browse-column(8)
      hColumn3 = brwData:get-browse-column(11)
      hColumn4 = brwData:get-browse-column(10)
      hColumn5 = brwData:get-browse-column(12)
      .
  if valid-handle(hColumn)  and
     valid-handle(hColumn1) and
     valid-handle(hColumn2) and
     valid-handle(hColumn3) and
     valid-handle(hColumn4) and
     valid-handle(hColumn5)
   then
    do:
      if cbType:screen-value = "C"
       then
        assign
            hColumn:label     = "Credit"
            hColumn1:label    = "Balance"
            hColumn2:visible  = false
            hColumn3:visible  = false
            hColumn4:visible  = true
            hColumn5:visible  = true
            . 
       else
        assign
            hColumn:label     = "Payment"
            hColumn1:label    = "Remaining"
            hColumn2:visible  = true
            hColumn3:visible  = true
            hColumn4:visible  = false
            hColumn5:visible  = false
            .
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showHeaderDetails C-Win 
PROCEDURE showHeaderDetails :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available ttagentunappliedCash
   then return.
  
  define buffer ttArPmt  for ttArPmt.
  define buffer ttArMisc for ttArMisc.

  empty temp-table ttArPmt.
  empty temp-table ttArMisc.
 
  do with frame fMain:
  end.
 
  case cbType:screen-value:
    when {&Payment} /* 'P' type Payment */
     then
      do:
        run server\getpayment.p (input ttagentunappliedCash.artranID, 
                                 input ttagentunappliedCash.tranID,   
                                 output table ttArPmt,
                                 output std-lo,
                                 output std-ch).
          
        if not std-lo
         then
          do:
            message std-ch
                view-as alert-box error buttons ok.
            return.  
          end.
        
        for first ttArPmt:    
          run dialogmodifypayment.w (input if ttArPmt.void = true then {&view} else {&ModifyPosted},
                                     input-output table ttArPmt,
                                     output std-lo).
        end.
      end.
   
     when {&Credit} /* 'C' type Misc Credit */
      then
       do:
         run server\getinvoices.p (input {&Credit}, 
                                   input ttagentunappliedCash.tranID,   
                                   output table ttArMisc,
                                   output std-lo,
                                   output std-ch).     
        if not std-lo
         then
          do:
            message std-ch
                view-as alert-box error buttons ok.
            return.  
          end.
         
         for first ttArMisc:        
           run dialoginvoice.w (input-output table ttArMisc,
                                input {&Credit},
                                input if ttArMisc.void = true then {&View} else {&ModifyPosted}, /* View */
                                output std-lo).
         end.                    
       end.
  
  end case.
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by ttagentunappliedCash.name ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewDetail C-Win 
PROCEDURE viewDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 if available ttagentunappliedCash 
   then
    publish "OpenWindow" (input "wtransactiondetail",                   /*childtype*/
                          input string(ttagentunappliedCash.ArTranID),  /*childid*/
                          input "wtransactiondetail.w",                 /*window*/
                          input "integer|input|" + string(ttagentunappliedCash.ArTranID)  + "^integer|input|0^character|input|",    /*parameters*/                               
                          input this-procedure).                        /*currentProcedure handle*/ 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign
      frame fMain:width-pixels          = {&window-name}:width-pixels
      frame fMain:virtual-width-pixels  = {&window-name}:width-pixels
      frame fMain:height-pixels         = {&window-name}:height-pixels
      frame fMain:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain components */
      brwData:width-pixels              = frame fmain:width-pixels - 15
      brwData:height-pixels             = frame fMain:height-pixels - 81
      .

  {lib/resize-column.i &col="'Name'"    &var=dColumnWidth} 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getManager C-Win 
FUNCTION getManager RETURNS CHARACTER
  ( input ipcUID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cName as CHARACTER   NO-UNDO.
  publish "getManagerName" ("U",ipcUID,output cName).
  return cName.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if fAgent:input-value = ""
   then return false. /* Function return value. */
  
  else if fAgent:input-value = {&ALL}
   then
    fName:screen-value = {&NotApplicable}.
       
  else if fAgent:input-value <> {&ALL}
   then
    do:
      cbState:screen-value = {&ALL}. 

      publish "getAgentName" (input fAgent:input-value,
                              output std-ch,
                              output std-lo).                                               
      if not std-lo 
       then 
        do:
          assign 
              fAgent:screen-value = "" 
              fName:screen-value  = ""              
              .
          return false. /* Function return value. */
        end.
      fName:screen-value = std-ch.
    end. 
  
  resultsChanged(false).  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

