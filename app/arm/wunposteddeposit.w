&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wUnpostedDeposit.w

  Description: Window for AR Unposted Deposits

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Vikas jain

  Created: 01.30.2020
  
  @Modified :
  Date        Name     Description 
  07/22/2020  AG       Modified to use deposit.posted instead of deposit.stat
  07/31/2020  AC       Added new button "Prelim".
  01/15/2021  Shefali  Added new pop-up menu "View Unposted Payments".
  02/03/2021  Shefali  Modified to display deposit amount totals"
  05/09/2024  SRK      Modified to Production file base related changes
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */

{lib/ar-def.i}
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/winshowscrollbars.i}
{lib/brw-totalData-def.i}
{lib/normalizefileid.i}  /* include file to normalize file number */

/* Temp-table Definition */
{tt/ardeposit.i}                                   /* Contain filtered data */
{tt/ardeposit.i    &tableAlias="tardeposit"}       /* Used for Data Transfer */
{tt/ardeposit.i    &tableAlias="ttardeposit"}      /* Contain data received from server */
{tt/arpmt.i        &tableAlias="tarpmt"}           /* Used for Import Deposit */
{tt/arpmt.i        &tableAlias="ttarpmt"}          /* Used for Import Deposit to retain copy of original data imported*/
{tt/proerr.i       &tableAlias="depositErr"}       /* Used to get validation messages from server */
{tt/importdata.i }                                 /* Used for Import data from file */
{tt/ledgerreport.i &tableAlias="glpaymentdetail"}  /* Contain detail of payemt GL account info */
{tt/ledgerreport.i &tableAlias="gldeposit"}        /* Contail Deposit GL accounts info */

define variable dColumnWidth       as decimal    no-undo.
define variable lApplySearchString as logical    no-undo.
define variable cLastSearchString  as character  no-undo.
define variable cErrFile           as character  no-undo.
define variable iErrSeq            as integer    no-undo.
define variable cCurrentUser       as character  no-undo.
define variable rwRow              as rowid      no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwardeposit

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ardeposit

/* Definitions for BROWSE brwardeposit                                  */
&Scoped-define FIELDS-IN-QUERY-brwardeposit ardeposit.bankID "Bank ID" ardeposit.depositRef "Deposit Ref" ardeposit.depositType "Type" ardeposit.depositdate "Deposit Date" ardeposit.amount "Amount" ardeposit.pmtamount   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwardeposit   
&Scoped-define SELF-NAME brwardeposit
&Scoped-define QUERY-STRING-brwardeposit for each ardeposit
&Scoped-define OPEN-QUERY-brwardeposit open query {&SELF-NAME} for each ardeposit.
&Scoped-define TABLES-IN-QUERY-brwardeposit ardeposit
&Scoped-define FIRST-TABLE-IN-QUERY-brwardeposit ardeposit


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwardeposit}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bEmpty fSearch bRefresh brwardeposit bSearch ~
bDelete bImport bNew bUnpostedPayment RECT-77 RECT-80 RECT-91 
&Scoped-Define DISPLAYED-OBJECTS fSearch flPostDate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwardeposit 
       MENU-ITEM m_View_Unposted_payments LABEL "View Unposted Payments".


/* Definitions of the field level widgets                               */
DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.67 TOOLTIP "Delete deposit".

DEFINE BUTTON bEmpty  NO-FOCUS
     LABEL "Empty" 
     SIZE 7.2 BY 1.67 TOOLTIP "Empty deposit (delete payments)".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.67 TOOLTIP "Export data".

DEFINE BUTTON bImport  NO-FOCUS
     LABEL "Import" 
     SIZE 7.2 BY 1.67 TOOLTIP "Import Payment(s)".

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.67 TOOLTIP "Modify deposit".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.67 TOOLTIP "New deposit".

DEFINE BUTTON bPostDeposit  NO-FOCUS
     LABEL "Postdeposit" 
     SIZE 7.2 BY 1.67 TOOLTIP "Post selected deposit".

DEFINE BUTTON bPrelimRpt  NO-FOCUS
     LABEL "Prelim" 
     SIZE 7.2 BY 1.67 TOOLTIP "Preliminary Report".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.67 TOOLTIP "Reload data".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search".

DEFINE BUTTON bUnpostedPayment  NO-FOCUS
     LABEL "Unposted Payments" 
     SIZE 7.2 BY 1.67 TOOLTIP "Unposted payment(s)".

DEFINE VARIABLE flPostDate AS DATE FORMAT "99/99/99":U 
     LABEL "Use Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 35.6 BY 1 TOOLTIP "Search Criteria (Bank ID,Deposit Ref,Type)" NO-UNDO.

DEFINE RECTANGLE RECT-77
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 59.4 BY 2.67.

DEFINE RECTANGLE RECT-80
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 55.8 BY 2.67.

DEFINE RECTANGLE RECT-91
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 43.8 BY 2.67.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwardeposit FOR 
      ardeposit SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwardeposit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwardeposit C-Win _FREEFORM
  QUERY brwardeposit DISPLAY
      ardeposit.bankID    label   "Bank ID"               format "x(20)"
ardeposit.depositRef      label   "Deposit Ref"           format "x(48)"     width 48
ardeposit.depositType     label   "Type"                  format "x(20)"
ardeposit.depositdate     label   "Deposit Date"          format "99/99/99"  width 15
ardeposit.amount          label   "Amount"                format ">,>>,>>>,>>>,>>9.99"
ardeposit.pmtamount column-label  "Check Payment!Amount"  format ">,>>,>>>,>>>,>>9.99"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 158.6 BY 19.52 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bEmpty AT ROW 2 COL 94.4 WIDGET-ID 448 NO-TAB-STOP 
     bPrelimRpt AT ROW 2 COL 151.6 WIDGET-ID 442
     fSearch AT ROW 2.33 COL 9.6 COLON-ALIGNED WIDGET-ID 310
     bRefresh AT ROW 2 COL 59.4 WIDGET-ID 402 NO-TAB-STOP 
     flPostDate AT ROW 2.33 COL 126.8 COLON-ALIGNED WIDGET-ID 380
     brwardeposit AT ROW 4.57 COL 2.4 WIDGET-ID 200
     bSearch AT ROW 2 COL 48.8 WIDGET-ID 308 NO-TAB-STOP 
     bDelete AT ROW 2 COL 87.4 WIDGET-ID 438 NO-TAB-STOP 
     bExport AT ROW 2 COL 66.4 WIDGET-ID 8
     bImport AT ROW 2 COL 101.4 WIDGET-ID 404 NO-TAB-STOP 
     bModify AT ROW 2 COL 80.4 WIDGET-ID 6 NO-TAB-STOP 
     bNew AT ROW 2 COL 73.4 WIDGET-ID 2 NO-TAB-STOP 
     bPostDeposit AT ROW 2 COL 144.6 WIDGET-ID 196
     bUnpostedPayment AT ROW 2 COL 108.4 WIDGET-ID 440 NO-TAB-STOP 
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.24 COL 59 WIDGET-ID 194
     "Filters" VIEW-AS TEXT
          SIZE 5.6 BY .62 AT ROW 1.24 COL 3.6 WIDGET-ID 436
     "Post" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1.24 COL 118 WIDGET-ID 446
     RECT-77 AT ROW 1.52 COL 57.8 WIDGET-ID 190
     RECT-80 AT ROW 1.52 COL 2.4 WIDGET-ID 428
     RECT-91 AT ROW 1.52 COL 116.8 WIDGET-ID 444
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 163.2 BY 23.81 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Unposted Deposits"
         HEIGHT             = 23.14
         WIDTH              = 162
         MAX-HEIGHT         = 34.43
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.43
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwardeposit flPostDate DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bExport IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bModify IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPostDeposit IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPrelimRpt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwardeposit:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwardeposit:HANDLE
       brwardeposit:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwardeposit:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR FILL-IN flPostDate IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwardeposit
/* Query rebuild information for BROWSE brwardeposit
     _START_FREEFORM
open query {&SELF-NAME} for each ardeposit.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwardeposit */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Unposted Deposits */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Unposted Deposits */
DO:
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Unposted Deposits */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME DEFAULT-FRAME /* Delete */
DO:
  run deleteDeposit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEmpty
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEmpty C-Win
ON CHOOSE OF bEmpty IN FRAME DEFAULT-FRAME /* Empty */
DO:
  run emptyDeposit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bImport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bImport C-Win
ON CHOOSE OF bImport IN FRAME DEFAULT-FRAME /* Import */
DO:
  run importData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME DEFAULT-FRAME /* Modify */
DO:
  run modifyDeposit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME DEFAULT-FRAME /* New */
DO:
  run newDeposit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPostDeposit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPostDeposit C-Win
ON CHOOSE OF bPostDeposit IN FRAME DEFAULT-FRAME /* Postdeposit */
DO:    
  run postDeposit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrelimRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrelimRpt C-Win
ON CHOOSE OF bPrelimRpt IN FRAME DEFAULT-FRAME /* Prelim */
DO:
  run prelimreport in this-procedure.   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME DEFAULT-FRAME /* Refresh */
DO:
  run refreshData    in this-procedure.
  fSearch:screen-value = cLastSearchString.
  run setWidgetState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwardeposit
&Scoped-define SELF-NAME brwardeposit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwardeposit C-Win
ON DEFAULT-ACTION OF brwardeposit IN FRAME DEFAULT-FRAME
DO:
  run viewUnpostedPayment in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwardeposit C-Win
ON ROW-DISPLAY OF brwardeposit IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwardeposit C-Win
ON START-SEARCH OF brwardeposit IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwardeposit C-Win
ON VALUE-CHANGED OF brwardeposit IN FRAME DEFAULT-FRAME
DO:  
  run setWidgetState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  /* if search button is clicked or return key is hit, 
     then 'cLastSearchString' stores the last string searched until user again hits the search */
  assign
      lApplySearchString = true  
      cLastSearchString  = fSearch:input-value
      .
  run filterData in this-procedure.
  run setWidgetState in this-procedure.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bUnpostedPayment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bUnpostedPayment C-Win
ON CHOOSE OF bUnpostedPayment IN FRAME DEFAULT-FRAME /* Unposted Payments */
DO:
  run viewUnpostedPayment in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  apply "CHOOSE" to bSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON VALUE-CHANGED OF fSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  resultsChanged().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Unposted_payments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Unposted_payments C-Win
ON CHOOSE OF MENU-ITEM m_View_Unposted_payments /* View Unposted Payments */
DO:
  run viewUnpostedPayment in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow"        anywhere.
subscribe to "refreshCheckAmount" anywhere.

bSearch         :load-image("images/magnifier.bmp").
bSearch         :load-image-insensitive("images/magnifier-i.bmp").

bExport         :load-image             ("images/excel.bmp").
bExport         :load-image-insensitive ("images/excel-i.bmp").

bRefresh        :load-image            ("images/sync.bmp").
bRefresh        :load-image-insensitive("images/sync-i.bmp").

bNew            :load-image            ("images/add.bmp").
bNew            :load-image-insensitive("images/add-i.bmp").

bModify         :load-image            ("images/update.bmp").
bModify         :load-image-insensitive("images/update-i.bmp").

bDelete         :load-image            ("images/delete.bmp").
bDelete         :load-image-insensitive("images/delete-i.bmp"). 

bEmpty          :load-image            ("images/money-delete.bmp").
bEmpty          :load-image-insensitive("images/money-delete-i.bmp"). 

bImport         :load-image            ("images/import.bmp").
bImport         :load-image-insensitive("images/import-i.bmp").

bPostDeposit    :load-image            ("images/check.bmp").
bPostDeposit    :load-image-insensitive("images/check-i.bmp").

bUnpostedPayment:load-image            ("images/money-edit.bmp").
bUnpostedPayment:load-image-insensitive("images/money-edit-i.bmp").

bPrelimRpt:load-image             ("images/pdf.bmp").
bPrelimRpt:load-image-insensitive ("images/pdf-i.bmp").


/* Get data and set filter based on data, filter data, display data on browse*/
run refreshData in this-procedure.

/* Return curent logged-in user name */
publish "GetCredentialsName" (output cCurrentUser).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:   

  run enable_UI.             
  /* Enable/disable Actions button based on the data */
  run setWidgetState in this-procedure.
  
  {lib/get-column-width.i &col="'DepositRef'" &var=dColumnWidth}
  
  /* When default posting option from config screen is allowed */ 
  publish 'GetDefaultPostingOption' (output std-lo).
  if std-lo
   then
    do:
      /* set default posting date on screen */
      publish 'getDefaultPostingDate' (output std-da).  
      flPostDate:screen-value  = string(std-da).
    end.
        
  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE createErrorRcrd C-Win 
PROCEDURE createErrorRcrd :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pcErrorMsg as character no-undo. 

  create depositErr.
  assign 
      iErrSeq                = iErrSeq + 1
      depositErr.err         = string(iErrSeq)
      depositErr.description = pcErrorMsg
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteDeposit C-Win 
PROCEDURE deleteDeposit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.    

  if not available ardeposit 
   then
    return.   
 
  message "Deposit will be deleted. Are you sure you want to delete the selected deposit?"
      view-as alert-box question buttons yes-no
      title "Delete Deposit"
      update std-lo.
      
  if not std-lo
   then 
    return.
 
  run server/deletedeposit.p(input ardeposit.depositID,
                             output std-lo,
                             output std-ch).

  if not std-lo 
   then
    do:
      message std-ch 
        view-as alert-box.
      return.  
    end.          
   
  for first ttardeposit where ttardeposit.depositID = ardeposit.depositID:
    delete ttardeposit.
  end.  
    
  run filterData in this-procedure.  
  
  apply 'value-changed' to browse brwardeposit.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE emptyDeposit C-Win 
PROCEDURE emptyDeposit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.    

  if not available ardeposit 
   then
    return.   
 
   message "All the payments of the deposit will be deleted. Are you sure you want to continue?"
     view-as alert-box warning buttons yes-no
     title "Delete Payments"
     update std-lo.
  if not std-lo
   then 
    return.
  run server/emptydeposit.p(input ardeposit.depositID,
                             output std-lo,
                             output std-ch).

  if not std-lo 
   then
    do:
      message std-ch 
        view-as alert-box.
      return.  
    end.
    
  for first ttardeposit where ttardeposit.depositID = ardeposit.depositID:
    ttardeposit.pmtamount = 0. 
  end.  
  
  run filterData in this-procedure.  
  
  apply 'value-changed' to browse brwardeposit.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fSearch flPostDate 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bEmpty fSearch bRefresh brwardeposit bSearch bDelete bImport bNew 
         bUnpostedPayment RECT-77 RECT-80 RECT-91 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwardeposit:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table ardeposit:handle.
  run util/exporttable.p (table-handle std-ha,
                          "ardeposit",
                          "for each ardeposit",
                          "depositRef,depositID,bankID,depositType,depositdate,amount,pmtamount,archived,transDate",
                          "Deposit Ref,Deposit ID,Bank,Type,Deposit Date,Amount,Pmt Amount,Archived,Trans Date",
                          std-ch,
                          "Unposteddeposits-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportErrorData C-Win 
PROCEDURE exportErrorData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if can-find(first depositErr)
   then
    do:
      publish "GetReportDir" (output std-ch).
      cErrFile = "ErrorsDeposit(" + ardeposit.depositRef + ")_" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv".
      std-ha = temp-table depositErr:handle.
      run util/exporttable.p (table-handle std-ha,
                              "depositErr",
                              "for each depositErr",
                              "err,description",
                              "Sequence,Error",
                              std-ch,
                              cErrFile,
                              true,
                              output std-ch,
                              output std-in).
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwardeposit.
  empty temp-table ardeposit.
  
  do with frame {&frame-name}:
  end.

  define buffer ttardeposit for ttardeposit.     
  
  if lApplySearchString /* if search filter is also applied */
   then
    for each ttardeposit where (if trim(cLastSearchString) <> "" 
                                 then
                                  ( 
                                   ttardeposit.bankID      matches ("*" + trim(cLastSearchString) + "*") or
                                   ttardeposit.depositRef  matches ("*" + trim(cLastSearchString) + "*") or
                                   ttardeposit.depositType matches ("*" + trim(cLastSearchString) + "*")
                                  )
                                 else 
                                 (ttardeposit.bankID = ttardeposit.bankID            or
                                  ttardeposit.depositRef = ttardeposit.depositRef    or
                                  ttardeposit.depositType = ttardeposit.depositType
                                 )
                               )
                               by ttardeposit.depositDate desc:   
      create ardeposit.
      buffer-copy ttardeposit to ardeposit.
    end.
   else
    for each ttardeposit by ttardeposit.depositDate desc:   
      create ardeposit.
      buffer-copy ttardeposit to ardeposit.
    end.
    
  dataSortDesc = not dataSortDesc.
  if dataSortBy = ""
   then dataSortBy = "depositref".
  open query brwardeposit preselect each ardeposit.
  rwRow = rowid (ardeposit).

  run sortData in this-procedure (dataSortBy).
  setStatusCount(query brwardeposit:num-results).
  reposition brwarDeposit to rowid(rwRow) no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.  
 
  define buffer ttardeposit for ttardeposit.
  {lib/brw-totalData.i &noShow=true}
  
  run server\getdeposits.p (output table tardeposit,
                            output std-lo,
                            output std-ch).
             
  if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box info buttons ok.
      return.
    end.      
  
  empty temp-table ttardeposit.
  for each tardeposit:
    create ttardeposit.
    buffer-copy tardeposit to ttardeposit.
  end. 
          
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE importData C-Win 
PROCEDURE importData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
 
 publish "SetCurrentValue" ("ImportAction",  "ImportDesposit").
 
 run util/importdialognew.w 
  ("RefNumber,AgentID,AgentName,Amount,Check,FileNumber,ProcessingDate",
   "CH,CH,CH,DE,CH,CH,DA",
   "Column 1 is Ref Number: Optional||Column 2 is Agent ID: required for Payment||Column 3 is Agent Name: Optional||Column 4 is Amount: required for Payment||Column 5 is Check: required for Payment||Column 6 is File Number: Optional||Column 7 is Processing Date: required for payment||",
   "ImportDeposits" /* Internal procedure called to import each record */).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ImportDeposits C-Win 
PROCEDURE ImportDeposits :
/*------------------------------------------------------------------------------
  Purpose:     Callback routine used by the generic Import Dialog
  Parameters:  <none>
  Notes:       
               [1]  - Reference Number
               [2]  - Agent ID
               [3]  - Agent Name
               [4]  - Amount
               [5]  - Check
               [6]  - File Number
               [7]  - Processing Date               
------------------------------------------------------------------------------*/
  define input  parameter table     for importdata.
  define output parameter pSuccess  as  logical   init false.
  define output parameter pErrorMsg as  character init "".
  
  define  variable lproduction  as  logical   no-undo.
  define  variable lselected    as  logical   no-undo.
  
  empty temp-table ttarPmt.
  empty temp-table tarPmt.
  
  /* Data will be available if called by the select file dialog */
  find first importdata no-error.
  if not available importdata 
   then return.  
  
  for each importData:
        
    /* These fields are mandatory to create payment record for the deposit */
    if importdata.data[1]  = "" or
       importdata.data[2]  = "" or
       importdata.data[5]  = "" or
       importdata.data[7]  = ?  or
       importdata.data[4]  = "0"
     then  
      next. 
    
    create tarpmt.       
    assign
      tarPmt.entity      = {&Agent}
      tarPmt.depositRef  = importdata.data[1]     
      tarPmt.entityID    = importdata.data[2]
      tarPmt.entityName  = importdata.data[3]
      tarpmt.checkamt    = decimal(importdata.data[4])
      tarpmt.checknum    = importdata.data[5]
      tarpmt.filenumber  = importdata.data[6]
      tarpmt.receiptdate = datetime(importdata.data[7])        
      tarpmt.transType   = if importdata.data[6] <> "" then {&File} else {&None}  
      
      .
  end.
 
  /* Retaining copy of original temp-table */
  for each tarPmt: 
    create ttarPmt.
    buffer-copy tarPmt to ttarPmt.
  end.
  
  run dialogpaymenttype.w(output  lproduction,output lselected).
  
  if not lselected 
   then
    do:
      pSuccess = true.
      leave.
    end.
    
    
    /* client Server Call */            
    run server/importdeposits.p (input lproduction,
                               input-output table tarPmt,
                               input true, /* Warning Msg */
                               output pSuccess,
                               output pErrorMsg).
                          
  if not pSuccess and
     pErrorMsg = {&DuplicateCheck}
   then
    do:
      message "Unposted payment record already exists with the check number same as mentioned in deposit import file . "
              "Do you still want to continue?"
          view-as alert-box  question buttons yes-no title "Duplicate Import"update lupdate as logical.
                  
      if not lUpdate
       then return.
   
      /* Rebuilding temp-table */
      for each ttarPmt: 
        create tarPmt.
        buffer-copy ttarPmt to tarPmt.
      end.
      
      /* client Server Call */            
      run server/importdeposits.p (input-output table tarPmt,
                                   input false, /* Warning Msg */
                                   output pSuccess,
                                   output pErrorMsg).                                                                    
    end.
  
  if not pSuccess
   then
    do:
      message pErrorMsg 
          view-as alert-box error buttons ok.
      return.
    end.
    
  if can-find(first tarpmt)
   then
    do:
      publish "GetReportDir" (output std-ch).
      cErrFile = "PendingDeposits-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv".
      std-ha = temp-table tarpmt:handle.
      run util/exporttable.p (table-handle std-ha,
                              "tarpmt",
                              "for each tarpmt",
                              "depositRef,entityID,entityName,checkamt,checknum,fileNumber,receiptDate,ErrorMsg",
                              "Deposit ID,Agent ID,Agent Name,Amount,Check,File Number,Processing Date, Error",
                              std-ch,
                              cErrFile,
                              true,
                              output std-ch,
                              output std-in).
    end.         
    
  run refreshData    in this-procedure.
  run setWidgetState in this-procedure.    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyDeposit C-Win 
PROCEDURE modifyDeposit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable roDeposit   as rowid no-undo.  

  define buffer ttardeposit for ttardeposit.
  empty temp-table tardeposit.

  if not available ardeposit 
   then
    return.  
  
  roDeposit = rowid(ardeposit).
   
  create tardeposit.
  buffer-copy ardeposit to tardeposit.
    
  run dialogmodifyDeposit.w (input-output table tardeposit,
                             input {&modify}, /*type*/
                             output std-lo).

  if not std-lo 
   then
    return.
    
  find first tardeposit no-error.
  if not available tardeposit 
   then return.
   
  find first ttardeposit where ttardeposit.depositID = tardeposit.depositID no-error.
  if not available ttardeposit
   then return.      
  
  buffer-copy tardeposit to ttardeposit.
  
  run filterData in this-procedure.
       
  reposition {&browse-name} to rowid roDeposit.    
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newDeposit C-Win 
PROCEDURE newDeposit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
     
  define buffer ttardeposit for ttardeposit.
  empty temp-table tardeposit.
   
  run dialogDeposit.w(output table tardeposit,
                      output std-lo).
  if not std-lo
   then 
    return.
  
  find first tardeposit no-error.
  if not available tardeposit 
   then return. 
  
  if can-find(first ttardeposit where ttardeposit.depositID = tardeposit.depositID)
   then return.      
    
  create ttardeposit.
  buffer-copy tardeposit to ttardeposit.
    
  run filterData  in this-procedure.   
       
  find first ardeposit where ardeposit.DepositID = ttardeposit.DepositID no-error.  
  if not available ardeposit
   then
    return.
    
  reposition {&browse-name} to rowid rowid(ardeposit).
  
  apply 'value-changed' to browse brwardeposit.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE postDeposit C-Win 
PROCEDURE postDeposit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iDepositID    as integer   no-undo.
  define variable cMessage      as character no-undo.
  define variable lSuccess      as logical   no-undo.
  define variable cFailMsg      as character no-undo.
  define variable lViewPdf      as logical   no-undo.
  define variable lPost         as logical   no-undo.
  define variable cFileName     as character no-undo.
  
  define buffer ttardeposit for ttardeposit.
  
  do with frame {&frame-name}:
  end.
  
  empty temp-table depositErr.
   
  if flPostDate:input-value = ?
   then
    do:
      message "Post date cannot be blank"
          view-as alert-box error buttons ok.
      apply 'entry' to flPostDate.    
      return.
    end. 

  if flPostDate:input-value > today
   then
    run createErrorRcrd in this-procedure ("Post date " + flPostDate:screen-value + " cannot be in the future").
    
  if available ardeposit and
      flPostDate:input-value < date(ardeposit.depositdate)
   then
    run createErrorRcrd in this-procedure ("Post date cannot be earlier to the deposit date").
     
  publish "validatePostingDate" (input flPostDate:input-value,
                                 output std-lo).
  
  if not std-lo
   then
    run createErrorRcrd in this-procedure ("Post date must be within an open period").
  
  if available ardeposit and (ardeposit.amount <> ardeposit.pmtamount)
   then
    run createErrorRcrd in this-procedure ("Deposit amount does not match with payments total amount").
  
  /* Export error table into csv and open it */
  run exportErrorData in this-procedure.
  
  if can-find(first depositErr)
   then
    do:
      apply 'entry' to flPostDate.  
      return.
    end.
      
  if not available ardeposit or
      arDeposit.posted 
   then
    return.
  
  run dialogviewledger.w (input "Post Deposit",output lPost).

  if not lPost
    then
     return.
     
  publish "GetViewPdf" (output lViewPdf).
  
  empty temp-table  gldeposit.
  empty temp-table  glPaymentDetail.
  
  iDepositID = ardeposit.depositID.
  
  run server/postDeposit.p (input iDepositID,
                            input flPostDate:input-value,
                            input lViewPdf,
                            output table depositErr,
                            output table gldeposit,
                            output table  glPaymentDetail,
                            output cMessage,
                            output lSuccess,
                            output cFailMsg).
  
  /* Export error table into csv and open it */
  run exportErrorData in this-procedure.
      
  if not lSuccess
   then
    do:
      if not can-find(first depositErr) 
       then
        message cFailMsg
          view-as alert-box.
      return.  
    end.
   else
    message "Posting was successful"
        view-as alert-box information buttons ok. 
   
      
  if lViewPdf
   then
    do:
      if not can-find(first gldeposit) or
         not can-find(first glpaymentdetail)
       then
        message "Nothing to print."
         view-as alert-box error buttons ok.
       else
        run util\ardepositpdf.p (input {&view},
                                 input table glPaymentDetail,
                                 input table gldeposit ,
                                 output cFileName). 
    end.                            

  if cMessage > ""
   then
    message cMessage view-as alert-box. 
  
  for first ttarDeposit where ttardeposit.depositID = iDepositID:
    delete ttardeposit.
  end.
 
  run filterData  in this-procedure.
   
  apply 'value-changed' to browse brwardeposit.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE prelimreport C-Win 
PROCEDURE prelimreport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   define variable cFilename as character.
  
   
   run server/prelimdepositgl.p (input ardeposit.depositID,
                                 output table gldeposit,
                                 output table glpaymentdetail,
                                 output std-lo,
                                 output std-ch). 
   if not std-lo 
    then
     do:
         message std-ch
           view-as alert-box error buttons ok.
         return.
     end.
     
   if not can-find(first gldeposit) or
      not can-find(first glpaymentdetail)
    then
     do:
       message "Nothing to print."
           view-as alert-box error buttons ok.
       return.
     end. 
     
   run util\ardepositpdf.p (input {&view},
                            input table glpaymentdetail,
                            input table gldeposit,                     
                            output cFilename).
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshCheckAmount C-Win 
PROCEDURE refreshCheckAmount :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipiDepositID  as integer no-undo.
  define input parameter deCheckAmount as decimal no-undo.  
  
  define variable iSelectedRow as integer no-undo.
  
  do with frame {&frame-name}:
  end.
  
  find first ttardeposit where ttardeposit.depositID = ipiDepositID no-error.
  if not available ttardeposit
   then return.
  
  ttardeposit.pmtamount = deCheckAmount.
  
  
  do iSelectedRow = 1 TO brwardeposit:num-iterations: 
    if brwardeposit:is-row-selected(iSelectedRow) then leave. 
  end.
  
  run filterData in this-procedure.
  
  find first ardeposit where ardeposit.depositID = ipiDepositID no-error.
  if not available ardeposit
   then return.
  
  /* Sets the row index where specified row in the browse viewport was selected */
  {&browse-name}:set-repositioned-row(iSelectedRow) no-error.
  reposition {&browse-name} to rowid rowid(ardeposit) no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData C-Win 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  run getData     in this-procedure.      
  run filterData  in this-procedure.
  
  setStatusRecords(query brwardeposit:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
         
  assign
    bExport:sensitive          = (query brwardeposit:num-results > 0)
    bImport:sensitive          = (available ardeposit and ardeposit.pmtamount <= 0)
    bModify:sensitive          = (available ardeposit and not ardeposit.posted) 
    bUnpostedPayment:sensitive = (query brwardeposit:num-results > 0)
    flPostDate:sensitive       = (available ardeposit and not ardeposit.posted)
    bPostDeposit:sensitive     = (available ardeposit and not ardeposit.posted)
    bPrelimRpt:sensitive       = (available ardeposit and not ardeposit.posted)
    bDelete:sensitive          = (available ardeposit and ardeposit.pmtamount <= 0)
    bEmpty:sensitive           = (available ardeposit and ardeposit.pmtamount > 0)
    bSearch:sensitive          = (query brwardeposit:num-results > 0)
                                  no-error
    .
         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by ardeposit.depositDate desc".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
  {lib/brw-totalData.i &excludeColumn="2,3,4"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewUnpostedPayment C-Win 
PROCEDURE viewUnpostedPayment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available ardeposit
   then
    return.
    
  /* Show records based on the selected deposit ref */
  publish "SetCurrentValue" ("DepositID", string(ardeposit.depositID)).
  publish "SetCurrentValue" ("DepositRef", string(ardeposit.depositRef)).
  publish "SetCurrentValue" ("DepositDate", ardeposit.depositDate).
  publish "OpenWindow" (input "wunpostedpayment", 
                        input string(ardeposit.depositID), 
                        input "wunpostedpayment.w", 
                        input "",                                   
                        input this-procedure).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run sortData in this-procedure (dataSortBy).
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 14
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 22
      .
 
 {lib/resize-column.i &col="'DepositRef'" &var=dColumnWidth}
 run ShowScrollBars(frame {&frame-name}:handle, no, no).  
 {lib/brw-totalData.i &excludeColumn="2,3,4"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  setStatusMessage({&ResultNotMatchSearchString}).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

