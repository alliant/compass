&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

File: winvoicestatement.w 

Description: 

Input Parameters:
<none>

Output Parameters:
<none>

Author: Anjly

Created:

Modified: 
Date                Name            Comments
01/09/2020
07/05/2021          MK              Bug Fix.

------------------------------------------------------------------------*/
/* This .W file was created with the Progress AppBuilder. */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
by this procedure. This is a good default which assures
that this procedure's triggers and internal procedures 
will execute in this procedure's storage, and that proper
cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ************************** Definitions ************************* */
/* Standard Libraries */
{lib/std-def.i}
{lib/ar-def.i}
{lib/get-column.i}
{lib/winlaunch.i}
{lib/rpt-defs.i}
{lib/getperiodname.i} /* Include function: getPeriodName */

/* Temp-table definition */
{tt/invoiceStatement.i}
{tt/arbatch.i &tableAlias=tbatch}          /* Temp-table used to show batch data */
{tt/arinvoices.i &tableAlias=ttarinvoices} /* Temp-table used to show data on browser */
{tt/arinvoices.i &tableAlias=tarinvoices}  /* Temp-table used to show Misc. invoice data; putting batch data also in it */
{tt/miscinvstatement.i}

                      
/* Global variables */
define variable dColumnWidth          as decimal   no-undo.
define variable tYear                 as integer   no-undo.
define variable tMonth                as integer   no-undo.
define variable batchlist             as character.
define variable iBatchChecked         as integer   no-undo.
define variable cInvoiceChecked       as character no-undo.
define variable repRowid              as rowid .
define variable cCurrentUID           as character.

define variable cFormat     as character   no-undo.
define variable cOptions    as character   no-undo.
define variable lViewReport as logical     NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttarinvoices

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData ttarinvoices.selectrecord ttarinvoices.agentid ttarinvoices.agentName ttarinvoices.type ttarinvoices.Reference ttarinvoices.liabilityDelta ttarinvoices.grossPremiumDelta ttarinvoices.retainedPremiumDelta ttarinvoices.netPremiumDelta   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData ttarinvoices.selectrecord   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwData ttarinvoices
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwData ttarinvoices
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each ttarinvoices
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each ttarinvoices.
&Scoped-define TABLES-IN-QUERY-brwData ttarinvoices
&Scoped-define FIRST-TABLE-IN-QUERY-brwData ttarinvoices


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwData bAgentLookup bGo bPeriod tgPosted ~
tSearch cbType cbState btResetFilter cbStatus cbManager tbPrintEmpty RECT-1 ~
RECT-2 RECT-7 RECT-5 
&Scoped-Define DISPLAYED-OBJECTS fPeriod tgPosted tSearch cbType cbState ~
cbStatus cbManager tbPrintEmpty 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getAgentManagerName C-Win 
FUNCTION getAgentManagerName RETURNS CHARACTER
  (input ipcUID as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getStatus C-Win 
FUNCTION getStatus RETURNS CHARACTER
  ( input cAuth as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openFile C-Win 
FUNCTION openFile RETURNS logical
  (input pFilename as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup  NO-FOCUS
     LABEL "agentlookup" 
     SIZE 7.2 BY 1.71 TOOLTIP "Agent specific destinations".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE BUTTON bPeriod 
     LABEL "Period" 
     SIZE 5 BY 1.19 TOOLTIP "Select period".

DEFINE BUTTON bQueue  NO-FOCUS
     LABEL "Queue" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run Report".

DEFINE BUTTON btResetFilter  NO-FOCUS
     LABEL "Reset" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reset filters".

DEFINE VARIABLE cbManager AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Manager" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE cbStatus AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE cbType AS CHARACTER FORMAT "X(256)":U INITIAL "All" 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 12.8 BY 1 NO-UNDO.

DEFINE VARIABLE fPeriod AS CHARACTER FORMAT "X(256)":U INITIAL "Select Period" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE tSearch AS CHARACTER FORMAT "X(10)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 35 BY 1 TOOLTIP "Enter data to be searched" NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 167.2 BY 2.86.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 115.4 BY 3.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 10.6 BY 3.

DEFINE RECTANGLE RECT-7
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 38 BY 3.

DEFINE VARIABLE tbPrintEmpty AS LOGICAL INITIAL no 
     LABEL "Generate invoice with no activity" 
     VIEW-AS TOGGLE-BOX
     SIZE 35.4 BY .81 NO-UNDO.

DEFINE VARIABLE tgPosted AS LOGICAL INITIAL no 
     LABEL "Show only posted" 
     VIEW-AS TOGGLE-BOX
     SIZE 22 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      ttarinvoices SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      ttarinvoices.selectrecord           column-label  "Select"  width 7 view-as toggle-box
ttarinvoices.agentid                label  "Agent ID"           format "x(20)"                  width 10
ttarinvoices.agentName              label  "Name"               format "x(100)"                 width 40
ttarinvoices.type                     label  "Type"               format "x(20)"                  width 7
ttarinvoices.Reference              label  "Reference"          format "x(30)"                  width 25
ttarinvoices.liabilityDelta         label  "Liability"          format "->>>,>>>,>>>"           width 14
ttarinvoices.grossPremiumDelta      label  "Gross Premium"      format "->>>,>>>,>>9.99"        width 14
ttarinvoices.retainedPremiumDelta   label  "Retained Premium"   format "->>>,>>>,>>9.99"        width 14
ttarinvoices.netPremiumDelta        label  "Net Premium"        format "->>>,>>>,>>9.99"        width 14
enable ttarinvoices.selectrecord
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 167 BY 15.48 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwData AT ROW 4.81 COL 3 WIDGET-ID 200
     bQueue AT ROW 21.19 COL 41 WIDGET-ID 74 NO-TAB-STOP 
     bAgentLookup AT ROW 1.95 COL 157.2 WIDGET-ID 492 NO-TAB-STOP 
     fPeriod AT ROW 2 COL 3 COLON-ALIGNED NO-LABEL WIDGET-ID 44
     bGo AT ROW 2.05 COL 31.8 WIDGET-ID 4 NO-TAB-STOP 
     bPeriod AT ROW 1.86 COL 24 WIDGET-ID 402
     tgPosted AT ROW 3.14 COL 5 WIDGET-ID 460
     tSearch AT ROW 1.86 COL 107.8 COLON-ALIGNED WIDGET-ID 354
     cbType AT ROW 2.91 COL 50 COLON-ALIGNED WIDGET-ID 462
     cbState AT ROW 1.86 COL 50 COLON-ALIGNED WIDGET-ID 436
     btResetFilter AT ROW 1.95 COL 147 WIDGET-ID 272 NO-TAB-STOP 
     cbStatus AT ROW 1.86 COL 75 COLON-ALIGNED WIDGET-ID 438
     cbManager AT ROW 2.91 COL 75 COLON-ALIGNED WIDGET-ID 440
     tbPrintEmpty AT ROW 21.62 COL 4.2 WIDGET-ID 448
     "Output" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 20.33 COL 4.2 WIDGET-ID 48
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.14 COL 42 WIDGET-ID 458
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.14 COL 4.4 WIDGET-ID 456
     RECT-1 AT ROW 20.67 COL 3 WIDGET-ID 46
     RECT-2 AT ROW 1.43 COL 40.6 WIDGET-ID 450
     RECT-7 AT ROW 1.43 COL 3 WIDGET-ID 454
     RECT-5 AT ROW 1.43 COL 155.6 WIDGET-ID 490
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 206.4 BY 27.1 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Generate Agent Invoices"
         HEIGHT             = 22.86
         WIDTH              = 171
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bQueue IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR FILL-IN fPeriod IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       fPeriod:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each ttarinvoices.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Generate Agent Invoices */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Generate Agent Invoices */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Generate Agent Invoices */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup C-Win
ON CHOOSE OF bAgentLookup IN FRAME fMain /* agentlookup */
DO:
  publish "SetCurrentValue" ("DestinationAction", "invoiceStatementQuery").
  publish "ReferenceDestination".
  publish "SetCurrentValue" ("DestinationAction", "").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:   
  if (tMonth = 0) or (tYear = 0) 
   then
    do:
      message "Please select valid period first"
          view-as alert-box info buttons ok.      
      return no-apply.
    end.
  
  run getBatchData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPeriod C-Win
ON CHOOSE OF bPeriod IN FRAME fMain /* Period */
DO:
  /* Set selected period */
  run getPeriod in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bQueue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bQueue C-Win
ON CHOOSE OF bQueue IN FRAME fMain /* Queue */
DO:
  run runReport in this-procedure.
  browse brwdata:refresh().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  define buffer ttarinvoices for ttarinvoices.
  
  std-ha = brwData:current-column.
  if std-ha:label = "Select"
   then
    do:     
      std-lo = can-find(first ttarinvoices where not(ttarinvoices.selectrecord)).   
      for each ttarinvoices:            
        ttarinvoices.selectrecord = std-lo.
        /* Retaining selected record */
        for first tarinvoices where tarinvoices.batchID = ttarinvoices.batchID:
          tarinvoices.selectrecord = ttarinvoices.selectrecord.
        end.
      end.
      browse brwData:refresh().
      run setWidgets in this-procedure.
    end.
   else
    do:
      {lib/brw-startsearch.i}
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btResetFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btResetFilter C-Win
ON CHOOSE OF btResetFilter IN FRAME fMain /* Reset */
do:  
  run resetFilters in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSearch C-Win
ON LEAVE OF tSearch IN FRAME fMain /* Search */
OR 'RETURN'        of tSearch
OR 'VALUE-CHANGED' of cbState
OR 'VALUE-CHANGED' of cbStatus
OR 'VALUE-CHANGED' of cbManager
OR 'VALUE-CHANGED' of cbType
DO:
  run filterBatches in this-procedure.
  
  /* Enable reset filter button when filter applies */
  run setFilterButton in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */ 
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

 
assign 
  {&window-name}:min-height-pixels = {&window-name}:height-pixels
  {&window-name}:min-width-pixels  = {&window-name}:width-pixels
  {&window-name}:max-height-pixels = session:height-pixels
  {&window-name}:max-width-pixels  = session:width-pixels
  .

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME} .


setStatusMessage("").       
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow"  anywhere.

bGo          :load-image("images/completed.bmp").
bGo          :load-image-insensitive("images/completed-i.bmp").

bQueue       :load-image("images/completed.bmp").
bQueue       :load-image-insensitive("images/completed-i.bmp").

bPeriod      :load-image("images/s-calendar.bmp").
bPeriod      :load-image-insensitive("images/s-calendar-i.bmp").


btResetFilter:load-image("images/filtererase.bmp").
btResetFilter:load-image-insensitive("images/filtererase-i.bmp").

bAgentLookup :load-image            ("images/arrow_divide_right.bmp").
bAgentLookup :load-image-insensitive("images/arrow_divide_right-i.bmp").

{&window-name}:window-state = 2.

/* Initialise and set filters to ALL */
assign
    cbState:list-items        = {&ALL}
    cbStatus:list-item-pairs  = {&ALL} + "," + {&ALL}
    cbManager:list-item-pairs = {&ALL} + "," + {&ALL}
    cbType:list-item-pairs   = "All,All,Batch,B,Misc,I"
    . 

std-ch = "".
publish "GetSysPropList" ({&printappcode},
                          {&printobjAction},
                          {&printProperty},
                           output std-ch).

/* Get logged-in user Id */
publish "GetCredentialsID" (output cCurrentUID).

on 'value-changed' of ttarinvoices.selectrecord in browse brwData
do:
   if available ttarinvoices 
    then
     do:
       if ttarinvoices.type = 'Batch' 
        then
         iBatchChecked = ttarinvoices.batchid.
        else
         cInvoiceChecked = ttarinvoices.invoiceID.
     end.
     
   if ttarinvoices.type = 'Batch' 
    then
     do:
       find first ttarinvoices where ttarinvoices.batchid = iBatchChecked no-error.
       if available ttarinvoices 
        then
         do:
            ttarinvoices.selectrecord = ttarinvoices.selectrecord:checked in browse brwData.
            /* Retaining selected record */
            for first tarinvoices where tarinvoices.batchID = ttarinvoices.batchID:
              tarinvoices.selectrecord = ttarinvoices.selectrecord.
            end.
         end.
     end.
    else
     do:
       find first ttarinvoices where ttarinvoices.invoiceID = cInvoiceChecked no-error.
       if available ttarinvoices 
        then
         do:
            ttarinvoices.selectrecord = ttarinvoices.selectrecord:checked in browse brwData.
            /* Retaining selected record */
            for first tarinvoices where tarinvoices.invoiceID = ttarinvoices.invoiceID:
              tarinvoices.selectrecord = ttarinvoices.selectrecord.
            end.
         end.
     end.

   run setwidgets in this-procedure.
end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI. 
  {lib/get-column-width.i &col="'Name'"    &var=dColumnWidth} 
  run showWindow in this-procedure.
  run setwidgets in this-procedure.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fPeriod tgPosted tSearch cbType cbState cbStatus cbManager 
          tbPrintEmpty 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwData bAgentLookup bGo bPeriod tgPosted tSearch cbType cbState 
         btResetFilter cbStatus cbManager tbPrintEmpty RECT-1 RECT-2 RECT-7 
         RECT-5 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterBatches C-Win 
PROCEDURE filterBatches :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tarinvoices for tarinvoices.
  
  close query brwdata.
  empty temp-table ttarinvoices.

  do with frame {&frame-name}:
  end.

  for each tarinvoices where tarinvoices.stateID      = (if cbState:screen-value   = {&ALL} then tarinvoices.stateID      else cbState:screen-value)  and
                        tarinvoices.agentStat    = (if cbStatus:screen-value  = {&ALL} then tarinvoices.agentstat    else cbStatus:screen-value) and
                        tarinvoices.type         = (if cbType:screen-value    = {&ALL} then tarinvoices.type         else cbType:screen-value)   and
                        tarinvoices.agentmanager = (if cbManager:screen-value = {&ALL} then tarinvoices.agentmanager else cbManager:screen-value) :
                        
    if tSearch:screen-value <> "" and
       not ((string(tarinvoices.reference)    matches "*" + tSearch:screen-value + "*") or
            (tarinvoices.agentid    matches "*" + tSearch:screen-value + "*") or
            (tarinvoices.agentName  matches "*" + tSearch:screen-value + "*")
           )
     then next.
    create ttarinvoices.
    buffer-copy tarinvoices to ttarinvoices.
    assign 
        ttarinvoices.type = if tarinvoices.type = 'I' then 'Misc' else 'Batch'
        .
  end.

  open query brwdata for each ttarinvoices by ttarinvoices.type by ttarinvoices.batchid by ttarinvoices.agentid.
  run setwidgets in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getBatchData C-Win 
PROCEDURE getBatchData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end.
  
  empty temp-table  tbatch.
  empty temp-table tarinvoices.
  
  /* Client Server Call */
  run server\getcompletedbatches.p (input integer(string(tYear) + string(tMonth,"99")), /* Period ID */
                                    input not tgPosted:checked,                         /* Include All */
                                    input tgPosted:checked,                             /* OnlyPosted */
                                    output table tbatch,
                                    output std-lo,
                                    output std-ch).                                                                                            
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
    
  /* get posted Misc Invoices */  
  run server\queryagentinvoices.p   (input tYear,
                                     input tMonth,
                                     output table tarinvoices,
                                     output std-lo,
                                     output std-ch).
                                                                                             
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
    
  for each tbatch :
    create tarinvoices.
    buffer-copy tbatch to tarinvoices.
    assign
        tarinvoices.reference = string(tarinvoices.batchID)
        tarinvoices.type      = 'B'
        .
  end.
  
  run populateFilters in this-procedure.
  run filterBatches in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getPeriod C-Win 
PROCEDURE getPeriod :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  run dialogperiod.w (input-output tMonth,
                      input-output tYear,
                      output std-ch).
                     
  fPeriod:screen-value = getPeriodName(tMonth,tYear).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE populateFilters C-Win 
PROCEDURE populateFilters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tarinvoices for tarinvoices.
  
  define variable tempstate        as character.
  define variable tempstat         as character.
  define variable tempagentmanager as character.
  
  do with frame {&frame-name}:
  end.
  
  assign
      tempstate        = cbState:screen-value
      tempstat         = cbStatus:screen-value
      tempagentmanager = cbManager:screen-value
      .
 
  do with frame {&frame-name}:
  end.
  
  cbState:list-items = {&ALL}.
  for each tarinvoices where tarinvoices.stateID ne "" break by tarinvoices.stateID:
    if first-of (tarinvoices.stateID) 
     then
      cbState:add-last(tarinvoices.stateID).
  end.
  
  cbState:screen-value = if lookup(tempstate,cbState:list-items, ",") > 0 then tempstate else {&ALL}.
    
  cbStatus:list-item-pairs = {&ALL} + "," + {&ALL}.
  for each tarinvoices where tarinvoices.agentstat ne "" break by tarinvoices.agentstat:
    if first-of (tarinvoices.agentstat) 
     then
      cbStatus:add-last(getstatus(tarinvoices.agentstat),tarinvoices.agentstat). 
  end.

  cbStatus:screen-value  = if lookup(tempstat,cbStatus:list-item-pairs, ",") > 0 then tempstat else {&ALL}.
    
  cbManager:list-item-pairs = {&ALL} + "," + {&ALL}.
  for each tarinvoices where tarinvoices.agentmanager ne "" break by tarinvoices.agentmanager:
    if first-of (tarinvoices.agentmanager)
     then
      cbManager:add-last(getAgentManagerName(tarinvoices.agentmanager),tarinvoices.agentmanager).
  end.
  
  cbManager:screen-value  = if lookup(tempagentmanager,cbManager:list-item-pairs, ",") > 0 then tempagentmanager else {&ALL}.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resetFilters C-Win 
PROCEDURE resetFilters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Reset filters to initial state */
  assign
      btResetFilter:tooltip   = "Reset filters"                   
      cbState:screen-value    = {&ALL}          
      cbStatus:screen-value   = {&ALL}
      cbManager:screen-value  = {&ALL}
      cbType:screen-value     = {&ALL}
      tSearch:screen-value    = ""
      .
       
  run filterBatches in this-procedure.
  
  /* Disable reset filter button when filters reset to ALL */
  run setFilterButton in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE runReport C-Win 
PROCEDURE runReport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end. 

  define variable cBatchIDList   as character   no-undo initial "".
  define variable cMiscInvIDList as character   no-undo initial "".
  define variable chOutFile      as character   no-undo.
  define variable iFileCount     as integer     no-undo.
  define variable iSelected      as integer     no-undo.
  define variable cReportFormat  as character   no-undo.
  define variable cDestination   as character   no-undo.

  assign
      std-ch           = ""
      iSelected        = 0
      cReportFormat    = ""
      cDestination     = ""
      .
      
  for each ttarinvoices where selectrecord = true :
     if ttarinvoices.type = 'Misc'
      then
       cMiscInvIDList = cMiscInvIDList + "," +  ttarinvoices.invoiceID. 
      else 
       cBatchIDList = cBatchIDList + "," +  string(ttarinvoices.batchid).

     iSelected  = iSelected + 1.  
  end.

  /* possible output formats */
  cReportFormat = {&rpt-pdf} + "," + {&rpt-csv}. 
  
  cDestination  = {&rpt-destConfigured} + "," + {&rpt-destEmail} + "," +
                  {&rpt-destPrinter}. 

  /* possible output destinations */
  if (iSelected = 1) then
   cDestination  = cDestination + "," + {&rpt-destView}.
  
  
  {lib/rpt-dialogDestination.i &rpt-format = cReportFormat &rpt-destinationType = cDestination}
  
  if rpt-cancel
   then
    return.
  
  run server\generatearinvoice.p (input  trim(cBatchIDList,","),
                                  input  trim(cMiscInvIDList,","),
                                  input  tbPrintEmpty:checked,
                                  {lib/rpt-setparams.i},
                                  output chOutFile,
                                  output std-lo,
                                  output std-ch).
  if not std-lo
   then
    message std-ch view-as alert-box error buttons ok.
  else if rpt-behavior = "" /* View Report */
   then
    do:
     if chOutFile = "" 
      then
       message "Nothing to print." view-as alert-box warning buttons ok.
     else
      do iFileCount = 1 to num-entries(chOutFile):
       openFile(entry(iFileCount,chOutFile)).
      end. 
    end.
  else if std-ch <> ""
   then
    message std-ch view-as alert-box information buttons ok.
  else
   .                                                                                                                
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterButton C-Win 
PROCEDURE setFilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    btResetFilter:sensitive = not (cbState:screen-value  = {&all}
                              and cbStatus:screen-value  = {&all}
                              and cbManager:screen-value = {&all}
                              and cbType:screen-value    = {&all}
                              and tSearch:screen-value   = "").
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setwidgets C-Win 
PROCEDURE setwidgets :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if not can-find(first ttarinvoices)      and
     cbState:screen-value   = {&ALL}  and
     cbStatus:screen-value  = {&ALL}  and
     cbManager:screen-value = {&ALL}  and
     tSearch:screen-value   = ""  
   then
    assign
        brwData:sensitive            = false
        tSearch:sensitive            = false
        cbState:sensitive            = false
        cbStatus:sensitive           = false
        cbManager:sensitive          = false
        cbType:sensitive             = false
        btResetFilter:sensitive      = false
        tbPrintEmpty:sensitive       = false
        .
   else 
    do:
      assign
          brwData:sensitive            = true
          tbPrintEmpty:sensitive       = true
          tSearch:sensitive            = true
          cbState:sensitive            = true
          cbStatus:sensitive           = true
          cbManager:sensitive          = true
          cbType:sensitive             = true
          .
           
      std-in = 0.
      for each ttarinvoices where ttarinvoices.selectrecord = true:
        std-in = std-in + 1.
      end.
      bQueue:sensitive = (std-in > 0).
      
      if std-in > 0
       then
        setStatusMessage(string(std-in) + (if std-in > 1 then " records selected" else " record selected")).        
      else
       setStatusMessage("").         
      browse brwData:refresh(). 
   end.         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by ttarinvoices.batchid by ttarinvoices.agentid ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  frame fMain:width-pixels = {&window-name}:width-pixels.
  frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
  frame fMain:height-pixels = {&window-name}:height-pixels.
  frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

  /* fMain components */
  brwData:width-pixels = frame fmain:width-pixels - 20.
  brwData:height-pixels = frame fMain:height-pixels - 140.
  
  
  {lib/resize-column.i &col="'Name'"  &var=dColumnWidth} 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getAgentManagerName C-Win 
FUNCTION getAgentManagerName RETURNS CHARACTER
  (input ipcUID as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cManagerName  as character  no-undo.
  
  publish "getManagerName" (input "U",
                            input ipcUID,
                            output cManagerName). 

  return cManagerName.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getStatus C-Win 
FUNCTION getStatus RETURNS CHARACTER
  ( input cAuth as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable chSysPropDesc  as character   no-undo.
  publish "GetSysPropDesc" (input "AMD",
                            input "Agent",
                            input "Status", 
                            input cAuth,
                            output chSysPropDesc). 

  return chSysPropDesc.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openFile C-Win 
FUNCTION openFile RETURNS logical
  (input pFilename as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if search(pFilename) = ? 
   then
    do:
      message "Export file was not created"
          view-as alert-box error buttons ok.
        return false.
    end.

  RUN ShellExecuteA in this-procedure (0,
                                       "open",
                                       pFilename,
                                       "",
                                       "",
                                       1,
                                       OUTPUT std-in).
 
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

