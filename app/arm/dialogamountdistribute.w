&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogamountdistribute.w 

  Description: Distribute file amount among invoices

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul Sharma 

  Created: 09/19/2019 
  @modified
  Date          Name           Description
  
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Parameters Definitions ---      */ 
define input parameter ipiArtranID    as  integer     no-undo.
define input parameter ipcFileNo      as  character   no-undo.
define input parameter ipcFileID      as  character   no-undo.
define input parameter ipcAgentID     as  character   no-undo.
define input parameter ipcName        as  character   no-undo.
define output parameter oplUpdate     as  logical     no-undo.

/* ***************************  Definitions  ************************** */
{tt/artran.i}
{tt/artran.i &tableAlias=ttartran}
{tt/artran.i &tableAlias=tartran}
{lib/ar-def.i}
{lib/std-def.i}

/* Local Variable Definitions ---  */
define variable deDistAmt            as decimal   no-undo.
define variable deOldAppliedAmt      as decimal   no-undo.
define variable deTempOldAppliedAmt  as decimal   no-undo.
define variable lDataChanged         as logical   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwPmtDist

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES artran

/* Definitions for BROWSE brwPmtDist                                    */
&Scoped-define FIELDS-IN-QUERY-brwPmtDist artran.fullypaid artran.tranID artran.revenuetype artran.reference artran.tranamt artran.remainingamt artran.appliedamt artran.selectrecord   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwPmtDist artran.appliedamt   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwPmtDist artran
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwPmtDist artran
&Scoped-define SELF-NAME brwPmtDist
&Scoped-define QUERY-STRING-brwPmtDist for each artran
&Scoped-define OPEN-QUERY-brwPmtDist open query {&SELF-NAME} for each artran.
&Scoped-define TABLES-IN-QUERY-brwPmtDist artran
&Scoped-define FIRST-TABLE-IN-QUERY-brwPmtDist artran


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwPmtDist}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwPmtDist flAgentID flFile flName flDistAmt ~
btCancel RECT-80 RECT-84 
&Scoped-Define DISPLAYED-OBJECTS flAgentID flFile flName flDistAmt 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON btAutoDist  NO-FOCUS
     LABEL "Auto" 
     SIZE 7.2 BY 1.71 TOOLTIP "Auto distribute".

DEFINE BUTTON btCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Cancel"
     BGCOLOR 8 .

DEFINE BUTTON btReset  NO-FOCUS
     LABEL "Reset" 
     SIZE 7.2 BY 1.71 TOOLTIP "Undo".

DEFINE BUTTON btSave AUTO-GO DEFAULT 
     LABEL "Save" 
     SIZE 15 BY 1.14 TOOLTIP "Save"
     BGCOLOR 8 .

DEFINE VARIABLE flAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flDistAmt AS DECIMAL FORMAT ">>>,>>>9.99":U INITIAL 0 
     LABEL "Distribution Amount" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE flFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 44 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-80
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 100.4 BY 2.81.

DEFINE RECTANGLE RECT-84
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 18.4 BY 2.81.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwPmtDist FOR 
      artran SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwPmtDist
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwPmtDist Dialog-Frame _FREEFORM
  QUERY brwPmtDist DISPLAY
      artran.fullypaid           column-label "Fully Paid"                   width 11  view-as toggle-box       
artran.tranID              label  "Invoice ID"                                   format "x(20)"
artran.revenuetype         label  "Revenue Type"                                 format "x(15)"
artran.reference           label  "Policy ID"                                    format "x(12)"
artran.tranamt             label  "Total"                                        format "->>,>>>,>>9.99"
artran.remainingamt        label  "Remaining"                                    format "->>,>>>,>>9.99"
artran.appliedamt          label  "Apply"                                        format ">,>>>,>>9.99"
artran.selectrecord        column-label "Change!Pending"                         view-as toggle-box
enable artran.appliedamt
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 119.8 BY 14.48
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Invoices" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     brwPmtDist AT ROW 4.81 COL 3 WIDGET-ID 200
     flAgentID AT ROW 1.86 COL 12.6 COLON-ALIGNED WIDGET-ID 246
     btAutoDist AT ROW 2.05 COL 105 WIDGET-ID 322 NO-TAB-STOP 
     flFile AT ROW 1.86 COL 79 COLON-ALIGNED WIDGET-ID 248
     flName AT ROW 3 COL 12.6 COLON-ALIGNED WIDGET-ID 242
     btReset AT ROW 2.05 COL 112 WIDGET-ID 268 NO-TAB-STOP 
     flDistAmt AT ROW 3 COL 79 COLON-ALIGNED WIDGET-ID 244
     btSave AT ROW 19.86 COL 46.4 WIDGET-ID 324
     btCancel AT ROW 19.86 COL 63 WIDGET-ID 326
     RECT-80 AT ROW 1.52 COL 3 WIDGET-ID 238
     RECT-84 AT ROW 1.52 COL 103 WIDGET-ID 250
     SPACE(2.39) SKIP(16.80)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Amount Distribution"
         DEFAULT-BUTTON btSave WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwPmtDist 1 Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       brwPmtDist:ALLOW-COLUMN-SEARCHING IN FRAME Dialog-Frame = TRUE
       brwPmtDist:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE.

/* SETTINGS FOR BUTTON btAutoDist IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btReset IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btSave IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flAgentID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flDistAmt:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flFile:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flName:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwPmtDist
/* Query rebuild information for BROWSE brwPmtDist
     _START_FREEFORM
open query {&SELF-NAME} for each artran.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwPmtDist */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Amount Distribution */
do:
  oplUpdate = no.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwPmtDist
&Scoped-define SELF-NAME brwPmtDist
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPmtDist Dialog-Frame
ON ROW-DISPLAY OF brwPmtDist IN FRAME Dialog-Frame /* Invoices */
DO:
  {lib/brw-rowDisplay.i}
  
  assign
      artran.tranamt:fgcolor in browse brwPmtDist = if (artran.tranamt < 0)         then 12 
                                                    else ?
      artran.remainingamt:fgcolor                 = if (artran.remainingamt < 0)    then 12 
                                                    else ?
      artran.appliedamt:fgcolor                   = if (artran.appliedamt < 0)      then 12 
                                                    else ?
      .
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPmtDist Dialog-Frame
ON START-SEARCH OF brwPmtDist IN FRAME Dialog-Frame /* Invoices */
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPmtDist Dialog-Frame
ON VALUE-CHANGED OF brwPmtDist IN FRAME Dialog-Frame /* Invoices */
DO:
  if lDataChanged
   then
    run refreshBrowserData in this-procedure.
    
  lDataChanged = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btAutoDist
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btAutoDist Dialog-Frame
ON CHOOSE OF btAutoDist IN FRAME Dialog-Frame /* Auto */
do:
  run autoDistribute in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btCancel Dialog-Frame
ON CHOOSE OF btCancel IN FRAME Dialog-Frame /* Cancel */
DO:
  if can-find (first artran where artran.selectrecord)
   then
    do:
      message "Changes are pending to save. Want to continue?"                      
          view-as alert-box warning buttons yes-no update std-lo.
      
      if not std-lo /* if dont want to change the status */
       then return no-apply.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btReset
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btReset Dialog-Frame
ON CHOOSE OF btReset IN FRAME Dialog-Frame /* Reset */
do:
  /* Resetting all the unsaved changes to the original state*/
  run undoAllChanges in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btSave Dialog-Frame
ON CHOOSE OF btSave IN FRAME Dialog-Frame /* Save */
do:
  apply 'leave' to artran.appliedamt in browse brwPmtDist.
  
  /* Update applied amount field of the invoices */
  run saveInvoices in this-procedure.   
  
  if not oplUpdate 
   then
     return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */
{lib/brw-main.i}

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? then
  frame {&frame-name}:parent = active-window.

on entry of artran.appliedamt in browse brwPmtDist
do:
  if available artran 
   then
    do: 
      find first ttartran where ttartran.artranID = artran.artranID no-error.
      if available ttartran 
       then
        /* Retain original value of applied amount before any changes */ 
        deOldAppliedAmt = ttartran.appliedamt. 
  
      /* Retain temporary changed value of applied amount */
      deTempOldAppliedAmt = artran.appliedamt.
    end.  
end.  
  
on leave, tab, return of artran.appliedamt in browse brwPmtDist
do:
  run updateAppliedAmt in this-procedure.  
end. 

on value-changed of artran.appliedamt in browse brwPmtDist
do:
  lDataChanged = (deOldAppliedAmt     <> artran.appliedamt:input-value in browse brwPmtDist) or 
                 (deTempOldAppliedAmt <> artran.appliedamt:input-value in browse brwPmtDist).  
end.

btReset    :load-image ("images/undo.bmp").              
btReset    :load-image-insensitive("images/undo-i.bmp").
btAutoDist :load-image ("images/process.bmp").              
btAutoDist :load-image-insensitive("images/process-i.bmp").

/* Get assocaited artran records of a file */
run getArTransactions in this-procedure.

/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.
  
  run displayArTransactions in this-procedure.
  
  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE applyDefaultInvAmt Dialog-Frame 
PROCEDURE applyDefaultInvAmt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available artran
   then return.
  
  do with frame {&frame-name}:
  end.
  
  if artran.remainingamt > 0 and deDistAmt > 0     
   then  
    artran.appliedamt = if artran.remainingamt >= deDistAmt then deDistAmt 
                        else artran.remainingamt. 
   else
    artran.appliedamt = deTempOldAppliedAmt.
  
  assign
      artran.remainingamt     = artran.remainingamt - artran.appliedamt                                   
      artran.fullypaid        = artran.remainingamt = 0 
      artran.selectrecord     = not artran.appliedamt = deOldAppliedAmt
      deDistAmt               = deDistAmt - artran.appliedamt    
      flDistAmt:screen-value  = string(deDistAmt)
      . 
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE autoDistribute Dialog-Frame 
PROCEDURE autoDistribute :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttartran for ttartran.
  
  do with frame {&frame-name}:
  end.
       
  for each artran:
    
    find first ttartran where ttartran.artranID = artran.artranID no-error.
     if available ttartran 
      then
       /* Retain original value of applied amount before any changes */ 
       deOldAppliedAmt = ttartran.appliedamt. 
  
    /* Retain temporary changed value of applied amount */
    deTempOldAppliedAmt = artran.appliedamt.
  
    /* Reset payment values to the initial values when selected invoice is not applied */
    run resetAmount in this-procedure.
        
    run applyDefaultInvAmt in this-procedure.
  end.
  
  run enableDisableWidgets in this-procedure. 
  
  open query brwPmtDist for each artran.
    
  apply 'value-changed' to browse brwPmtDist. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayArTransactions Dialog-Frame 
PROCEDURE displayArTransactions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  close query brwPmtDist.
  empty temp-table artran.
  
  define buffer ttartran for ttartran.
  
  deDistAmt = 0.
  for each ttartran :
    
    if (ttartran.type = {&Invoice} or ttartran.type = {&Reprocess})
     then
      do:        
        find first artran 
          where artran.reference   = ttartran.reference
            and artran.revenuetype = ttartran.revenuetype no-error.
        if not available artran 
         then
          do:
            create artran.
            assign
                artran.artranID    = ttartran.artranID
                artran.revenuetype = ttartran.revenuetype                
                artran.reference   = ttartran.reference
                artran.type        = ttartran.type
                artran.appliedamt  = ttartran.appliedamt
                .                               
          end.
        assign
            artran.tranID       = if artran.tranID > "" then artran.tranID + "," + ttartran.tranID else ttartran.tranID
            artran.tranamt      = artran.tranamt + ttartran.tranamt
            artran.remainingamt = artran.tranamt - artran.appliedamt
            artran.fullypaid    = artran.remainingamt = 0
            .           
      end.
        
    if ttartran.type = {&Apply}               
     then
      deDistAmt = deDistAmt + ttartran.tranamt.
    
  end. /* for each ttartran */
  
  /* Recreating ttartran temp-table with only invoice type artran records with updated tranamt */
  empty temp-table ttartran.
  for each artran:
    create ttartran.
    buffer-copy artran to ttartran.
  end.
  
  assign
      deDistAmt              = absolute(deDistAmt)
      flFile:screen-value    = ipcFileNo
      flAgentID:screen-value = ipcAgentID
      flName:screen-value    = ipcName      
      flDistAmt:screen-value = string(deDistAmt)
      .
  open query brwPmtDist for each artran.
  
  btAutoDist:sensitive = query brwPmtDist:num-results > 0.
  
  run enableDisableWidgets in this-procedure.
  
  apply 'value-changed' to browse brwPmtDist.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableWidgets Dialog-Frame 
PROCEDURE enableDisableWidgets :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if can-find(first artran where artran.selectrecord)
   then
    assign
        btSave:sensitive  = true
        btReset:sensitive = true
        .
   else
    assign
        btSave:sensitive  = false
        btReset:sensitive = false
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flAgentID flFile flName flDistAmt 
      WITH FRAME Dialog-Frame.
  ENABLE brwPmtDist flAgentID flFile flName flDistAmt btCancel RECT-80 RECT-84 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getArTransactions Dialog-Frame 
PROCEDURE getArTransactions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttartran for ttartran.
  
  do with frame {&frame-name}:
  end.
      
  run server/gettransaction.p (input ipiArtranID,  /* Entity */ 
                               input "",           /* TranID */
                               input "",           /* Type */
                               output table ttartran,
                               output std-lo,
                               output std-ch).
    
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box info buttons ok.
      return.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshBrowserData Dialog-Frame 
PROCEDURE refreshBrowserData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  define variable selectedRow as integer no-undo.
  
  do with frame {&frame-name}:
  end.
  
  std-ro = if available artran then rowid(artran) else ?.
  
  do selectedRow = 1 TO brwPmtDist:num-iterations: 
    if brwPmtDist:is-row-selected(selectedRow) then leave. 
  end.
    
  open query brwPmtDist for each artran.
  
  {&browse-name}:set-repositioned-row(selectedRow) no-error.
  reposition {&browse-name} to rowid std-ro no-error.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resetAmount Dialog-Frame 
PROCEDURE resetAmount :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available artran
   then return.
   
  do with frame {&frame-name}:
  end.
  
  assign
      deDistAmt               = deDistAmt + artran.appliedamt
      artran.remainingamt     = artran.remainingamt + artran.appliedamt 
      artran.appliedamt       = 0
      artran.fullypaid        = false
      flDistAmt:screen-value  = string(deDistAmt)
      .
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveInvoices Dialog-Frame 
PROCEDURE saveInvoices :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/    
  define variable cSuccessMsg as character no-undo.
  
  /* Buffer Definition */
  define buffer artran   for artran.
  define buffer tartran  for tartran.
  define buffer ttartran for ttartran.
  
  empty temp-table tartran.
  
  do with frame {&frame-name}:
  end.
  
  for each artran:
    find first ttartran where ttartran.artranID = artran.artranID no-error.
    if available ttartran
     then
      do:
        buffer-compare ttartran using ttartran.selectrecord ttartran.appliedamt to artran
        save result in std-lo.
        if not std-lo
         then
          do:
            create tartran.
            buffer-copy artran to tartran.
          end.
      end.  
  end.
    
  run server/amountdistribute.p (input {&agent},   /* Entity */
                                 input ipcAgentID, /* EntityID */
                                 input ipcFileID,  /* FileID */                                 
                                 input table tartran,
                                 output cSuccessMsg,
                                 output oplUpdate,
                                 output std-ch).
 
  if not oplUpdate
   then
    do:
      message std-ch 
          view-as alert-box info buttons ok.
      return.
    end.
 
  message cSuccessMsg
      view-as alert-box info buttons ok. 
  
  oplUpdate = true.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData Dialog-Frame 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE undoAllChanges Dialog-Frame 
PROCEDURE undoAllChanges :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttartran for ttartran.
  
  do with frame {&frame-name}:
  end.
    
  for each artran:
    find first ttartran where ttartran.artranID = artran.artranID no-error.
    if available ttartran
     then
      do:
        buffer-compare ttartran using ttartran.selectrecord ttartran.appliedamt to artran
        save result in std-lo.
        if not std-lo
         then
          do:
            deDistAmt = deDistAmt + artran.appliedamt.
            buffer-copy ttartran except ttartran.remainingamt to artran. 
            artran.remainingamt = ttartran.tranamt - ttartran.appliedamt.
          end.  
      end.  
  end. 
  
  flDistAmt:screen-value = string(deDistAmt).
  
  run enableDisableWidgets in this-procedure. 
  
  open query brwPmtDist for each artran.
  
  apply 'value-changed' to browse brwPmtDist.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE updateAppliedAmt Dialog-Frame 
PROCEDURE updateAppliedAmt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  if not available artran
   then return.
  
  do with frame {&frame-name}:
  end.
    
  /* Reset payment values to the initial values when selected invoice is not applied */
  run resetAmount in this-procedure.
              
  artran.appliedamt = artran.appliedamt:input-value in browse brwPmtDist.
  
  if artran.appliedamt <= deDistAmt
   then
    do:
      if artran.remainingamt < artran.appliedamt and
         deTempOldAppliedAmt <> artran.appliedamt
       then
        message "Invoice applied amount exceeding invoice remaining amount" 
            view-as alert-box warning buttons ok. 
          
      assign                          
          artran.remainingamt     = artran.remainingamt - artran.appliedamt          
          artran.fullypaid        = artran.remainingamt = 0          
          artran.selectrecord     = not artran.appliedamt = deOldAppliedAmt
          deDistAmt               = deDistAmt - artran.appliedamt          
          flDistAmt:screen-value  = string(deDistAmt)                  
          .                      
    end.  
   else
    do:
      message "Applied amount of the invoice cannot be more than the amount to be distributed" 
          view-as alert-box error buttons ok-cancel.
            
      run applyDefaultInvAmt in this-procedure.    
    end.
  
  run enableDisableWidgets in this-procedure.
  apply 'value-changed' to browse brwPmtDist.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

