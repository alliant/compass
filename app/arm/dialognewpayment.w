&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialognewpayment.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Vikas Jain

  Created:07/23/19
  
  Modified:
  Date        Name     Comments
  05/14/2021  AG       Task# 82118, Mandatory Deposit Ref
  04/04/2022  SC       Task# 92523, prepopulate fields for newpayment.
  05/09/2024  SRK      Modified to related to Production base file
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{tt/arpmt.i &tablealias=ttArPmt}

/* Parameters Definitions                                           */
define input-output parameter table for ttArPmt.
define output parameter oplSuccess  as logical  no-undo.

/* Standard library files */
{lib/ar-def.i}
{lib/std-def.i}
{lib/ar-getentitytype.i} /* Include function: getEntityType */

/* Local variable definitions */
define variable cCurrentUser   as character no-undo.
define variable cCurrentUID    as character no-undo.
define variable cCode          as character no-undo.
define variable cStateID       as character no-undo. 
define variable lDefaultAgent  as logical   no-undo.
define variable cDepositRef    as character no-undo.
define variable cDepositDate   as character no-undo.
define variable cDepositID     as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS rsapplytype cbEntityType flDepositRef ~
flEntity bAgentLookup flRefNum flCheckDate flFileNumber flReceiptDate ~
bSysCode flAmt edDescription Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS rsapplytype cbEntityType flDepositRef ~
flEntity flName flRefNum flCheckDate flFileNumber flReceiptDate flAmt ~
edDescription fEntityTypeMand fRefNumMand fReceipDateMand fAmountMand ~
fEntityMand fDepRefMand fRevenueType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validateReceiptDate Dialog-Frame 
FUNCTION validateReceiptDate RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup 
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bSysCode 
     LABEL "sysCode" 
     SIZE 5 BY 1.14.

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Create" 
     SIZE 15 BY 1.14 TOOLTIP "Create"
     BGCOLOR 8 .

DEFINE VARIABLE cbEntityType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Received From" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Agent","A",
                     "Organization","O"
     DROP-DOWN-LIST
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE edDescription AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 82.8 BY 2.81 NO-UNDO.

DEFINE VARIABLE fAmountMand AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fDepRefMand AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fEntityMand AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fEntityTypeMand AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE flAmt AS DECIMAL FORMAT "Z,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Amount" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE flCheckDate AS DATE FORMAT "99/99/99":U 
     LABEL "Check Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 12.4 BY 1 NO-UNDO.

DEFINE VARIABLE flDepositRef AS CHARACTER FORMAT "X(256)":U 
     LABEL "Deposit Ref" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 30.2 BY 1 NO-UNDO.

DEFINE VARIABLE flEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE flFileNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "File Number" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 30.2 BY 1 NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 53.6 BY 1 NO-UNDO.

DEFINE VARIABLE flReceiptDate AS DATE FORMAT "99/99/99":U 
     LABEL "Receipt Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 12.4 BY 1 NO-UNDO.

DEFINE VARIABLE flRefNum AS CHARACTER FORMAT "X(256)":U 
     LABEL "Check/Reference" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE fReceipDateMand AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fRefNumMand AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fRevenueType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Revenue Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 25.2 BY 1 NO-UNDO.

DEFINE VARIABLE rsapplytype AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Premium", "F",
"Production", "P",
"None", "N"
     SIZE 37.8 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     rsapplytype AT ROW 3.87 COL 68.4 NO-LABEL WIDGET-ID 430
     cbEntityType AT ROW 1.57 COL 20 COLON-ALIGNED WIDGET-ID 86
     flDepositRef AT ROW 1.57 COL 72.6 COLON-ALIGNED WIDGET-ID 104
     flEntity AT ROW 2.71 COL 20 COLON-ALIGNED WIDGET-ID 102
     bAgentLookup AT ROW 2.62 COL 42 WIDGET-ID 350
     flName AT ROW 2.71 COL 49.2 COLON-ALIGNED NO-LABEL WIDGET-ID 424
     flRefNum AT ROW 3.86 COL 20 COLON-ALIGNED WIDGET-ID 42
     flCheckDate AT ROW 5 COL 20 COLON-ALIGNED WIDGET-ID 114
     flFileNumber AT ROW 5 COL 72.6 COLON-ALIGNED WIDGET-ID 70
     flReceiptDate AT ROW 6.14 COL 20 COLON-ALIGNED WIDGET-ID 36
     bSysCode AT ROW 6.05 COL 100 WIDGET-ID 252
     flAmt AT ROW 7.29 COL 40.8 RIGHT-ALIGNED WIDGET-ID 2
     edDescription AT ROW 8.43 COL 22 NO-LABEL WIDGET-ID 26
     Btn_OK AT ROW 11.76 COL 39.8
     Btn_Cancel AT ROW 11.76 COL 56.4
     fEntityTypeMand AT ROW 1.81 COL 40.2 COLON-ALIGNED NO-LABEL WIDGET-ID 268
     fRefNumMand AT ROW 4.1 COL 40.2 COLON-ALIGNED NO-LABEL WIDGET-ID 272
     fReceipDateMand AT ROW 6.38 COL 32.8 COLON-ALIGNED NO-LABEL WIDGET-ID 274
     fAmountMand AT ROW 7.52 COL 40.2 COLON-ALIGNED NO-LABEL WIDGET-ID 276
     fEntityMand AT ROW 2.91 COL 45 COLON-ALIGNED NO-LABEL WIDGET-ID 426
     fDepRefMand AT ROW 1.71 COL 103 COLON-ALIGNED NO-LABEL WIDGET-ID 428
     fRevenueType AT ROW 6.14 COL 72.6 COLON-ALIGNED WIDGET-ID 256 NO-TAB-STOP 
     "Notes:" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 9.29 COL 14.4 WIDGET-ID 28
     "Apply to File for:" VIEW-AS TEXT
          SIZE 16.2 BY 1 AT ROW 3.87 COL 51.8 WIDGET-ID 434
     SPACE(41.39) SKIP(8.36)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "New Payment"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       edDescription:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN fAmountMand IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fDepRefMand IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fEntityMand IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fEntityTypeMand IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flAmt IN FRAME Dialog-Frame
   ALIGN-R                                                              */
/* SETTINGS FOR FILL-IN flName IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flName:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fReceipDateMand IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fRefNumMand IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fRevenueType IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* New Payment */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup Dialog-Frame
ON CHOOSE OF bAgentLookup IN FRAME Dialog-Frame /* agentlookup */
DO:
  define variable cEntityID  as character no-undo.
  define variable cName      as character no-undo.
  
  if cbEntityType:screen-value = {&Agent}
   then
    do:
      run dialogagentlookup.w(input flEntity:input-value,
                              input "",        /* Selected State ID */
                              input false,     /* Allow 'ALL' */
                              output cEntityID,
                              output cStateID, /* Agent state ID */
                              output cName,
                              output std-lo).
   
      if not std-lo or flEntity:input-value = cEntityID  
       then
        return no-apply.
     
      assign
          flEntity:screen-value = cEntityID
          flName:screen-value   = cName
          . 
        
      if flEntity:input-value <> ""
       then
        do:
          if lDefaultAgent
           then
            /* Set default AgentID */
            publish "SetDefaultAgent" (input flEntity:input-value).
        
        end.
    end.
                                     
  run enableDisableSave in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSysCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSysCode Dialog-Frame
ON CHOOSE OF bSysCode IN FRAME Dialog-Frame /* sysCode */
DO:
  run dialogrevenuelookup.w(input fRevenueType:screen-value,
                            input cStateID, 
                            output cCode,
                            output std-lo).
  if not std-lo 
   then
    return no-apply.
     
  fRevenueType:screen-value = cCode.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel Dialog-Frame
ON CHOOSE OF Btn_Cancel IN FRAME Dialog-Frame /* Cancel */
DO:
  oplSuccess = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Create */
DO:
  if not validateReceiptDate()
   then
    return no-apply. 
    
  if flAmt:input-value = 0  or flAmt:input-value = ?
   then
    do:
      message "Amount cannot be zero or ?."
        view-as alert-box error buttons ok.
      return no-apply.
    end.
    
  if rsapplytype:screen-value = "N" and flFileNumber:screen-value <> ""
   then
    do:
       message "Either select Premium or Production in '" + "Apply to File for"  + "' option."
           view-as alert-box information buttons ok.
       apply "entry" to flFileNumber.
       return no-apply.
    end.  
 
  run saveArPayment in this-procedure.
      
   /* If there was any error then do not close the dialog. */
  if not oplSuccess
   then
    return no-apply.      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbEntityType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbEntityType Dialog-Frame
ON VALUE-CHANGED OF cbEntityType IN FRAME Dialog-Frame /* Received From */
DO:
  assign
      flName:hidden          = cbEntityType:input-value = {&Organization}
      bAgentLookup:sensitive = cbEntityType:input-value <> {&Organization}
      flEntity:screen-value  = ""
      flEntity:label         = getEntityType(cbEntityType:input-value)
      .          
        
  run enableDisableSave in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAmt Dialog-Frame
ON VALUE-CHANGED OF flAmt IN FRAME Dialog-Frame /* Amount */
OR 'value-changed' of flDepositRef
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flDepositRef
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flDepositRef Dialog-Frame
ON LEAVE OF flDepositRef IN FRAME Dialog-Frame /* Deposit Ref */
OR 'LEAVE' OF flRefNum 
OR 'LEAVE' OF flFileNumber
OR 'LEAVE' OF flDepositRef
DO:
   self:screen-value = caps(self:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flEntity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flEntity Dialog-Frame
ON LEAVE OF flEntity IN FRAME Dialog-Frame /* Agent */
DO:
  define variable cName  as character no-undo.
  
  if cbEntityType:screen-value = {&Agent}
   then
    do:
      if flEntity:input-value <> ""
       then
        do:
          publish "getAgentName" (input flEntity:input-value,
                                  output cName,
                                  output std-lo).
  
          if not std-lo 
           then 
            do:
              assign 
                  flEntity:screen-value = "" 
                  flName:screen-value    = ""
                  .
              return no-apply.
            end.
      
          publish "getAgentStateID" (input flEntity:input-value,
                                     output cStateID,
                                     output std-lo).
                                 
          /* Set default AgentID */
          if lDefaultAgent 
           then
            publish "SetDefaultAgent" (input self:input-value).
        end.
  
      flName:screen-value = cName.
    end.
  
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flEntity Dialog-Frame
ON VALUE-CHANGED OF flEntity IN FRAME Dialog-Frame /* Agent */
DO:
  flName:screen-value = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flFileNumber
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flFileNumber Dialog-Frame
ON VALUE-CHANGED OF flFileNumber IN FRAME Dialog-Frame /* File Number */
DO:
  rsapplytype:sensitive = (flFileNumber:screen-value <> ""). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flReceiptDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flReceiptDate Dialog-Frame
ON VALUE-CHANGED OF flReceiptDate IN FRAME Dialog-Frame /* Receipt Date */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flRefNum
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flRefNum Dialog-Frame
ON VALUE-CHANGED OF flRefNum IN FRAME Dialog-Frame /* Check/Reference */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rsapplytype
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rsapplytype Dialog-Frame
ON VALUE-CHANGED OF rsapplytype IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Get current logged in username & UID */ 
publish "GetCredentialsName" (output cCurrentUser).
publish "GetCredentialsID"   (output cCurrentUID).

bSysCode    :load-image("images/s-magnifier.bmp").
bSysCode    :load-image-insensitive("images/s-magnifier-i.bmp").

bAgentLookup:load-image("images/s-lookup.bmp").
bAgentLookup:load-image-insensitive("images/s-lookup-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.  
    
  cbEntityType:screen-value = {&Agent}.
  rsapplytype:screen-value = "N".
  rsapplytype:sensitive = false.
  
  publish "GetAutoDefaultAgent" (output lDefaultAgent).
  
  if lDefaultAgent
   then
    do:
      publish "GetDefaultAgent"(output std-ch).
      
      flEntity:screen-value = std-ch.
      
      publish "getAgentName" (input flEntity:input-value,
                              output std-ch,
                              output std-lo).
      
      publish "getAgentStateID" (input flEntity:input-value,
                                 output cStateID,
                                 output std-lo).
      
      flName:screen-value = std-ch. 
    end. 
  publish "GetCurrentValue" ("DepositRef", output cDepositRef).
  publish "GetCurrentValue" ("DepositDate", output cDepositDate).
  publish "GetCurrentValue" ("DepositID", output cDepositID).
  
  find first ttArPmt no-lock no-error. 
  if available ttArPmt
   then
    assign
        flDepositRef:screen-value  = ttArPmt.depositRef
        flReceiptDate:screen-value = string(cDepositDate)
        flRefNum:screen-value      = ttArPmt.checknum
        flDepositRef:read-only     = true
        flReceiptDate:read-only    = true
        .         
   else if cDepositID <> ""
    then
     assign
         flDepositRef:screen-value  = cDepositRef
         flReceiptDate:screen-value = string(cDepositDate)
         flDepositRef:read-only     = true
         flReceiptDate:read-only    = true
         .
  else
   flReceiptDate:screen-value = string(today).
  
  apply 'value-changed' to cbEntityType.
  
  run setMandatoryMark in this-procedure.
  
  apply 'entry' to flEntity.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.    
  Btn_OK:sensitive = not (cbEntityType:input-value = ""  or
                          flEntity:input-value = ""      or 
                          flRefNum:input-value = ""      or
                          flDepositRef:input-value = ""  or
                          flReceiptDate:input-value = ?  or
                          flAmt:input-value = 0             
                          ) no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY rsapplytype cbEntityType flDepositRef flEntity flName flRefNum 
          flCheckDate flFileNumber flReceiptDate flAmt edDescription 
          fEntityTypeMand fRefNumMand fReceipDateMand fAmountMand fEntityMand 
          fDepRefMand fRevenueType 
      WITH FRAME Dialog-Frame.
  ENABLE rsapplytype cbEntityType flDepositRef flEntity bAgentLookup flRefNum 
         flCheckDate flFileNumber flReceiptDate bSysCode flAmt edDescription 
         Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveArPayment Dialog-Frame 
PROCEDURE saveArPayment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/   
  empty temp-table ttArPmt.
  
  define variable iArPmtID           as integer   no-undo.
  define variable dtCreatedDate      as datetime  no-undo.
  define variable lDepositPosted     as logical   no-undo.
  define variable cFileID            as character no-undo.
  define variable cFileExist         as character no-undo.
  define variable lApplyToFile       as logical   no-undo.
  define variable lApplyToProduction as logical   no-undo.
  define variable iDepositID         as integer   no-undo.
  
  create ttArPmt.
  assign
    ttArPmt.entity      = cbEntityType:input-value in frame {&frame-name}    
    ttArPmt.depositRef  = flDepositRef:input-value
    ttArPmt.entityID    = flEntity:input-value
    ttArPmt.entityName  = flName:screen-value
    ttArPmt.checknum    = flRefNum:input-value
    ttArPmt.checkdate   = flCheckDate:input-value        
    ttArPmt.receiptdate = flReceiptDate:input-value        
    ttArPmt.checkamt    = flAmt:input-value          
    ttArPmt.transType   = rsapplytype:screen-value
    ttArPmt.filenumber  = flfileNumber:input-value    
    ttArPmt.revenueType = fRevenueType:input-value            
    ttArPmt.description = edDescription:input-value
    .       
    
  /* client Server Call */            
  run server/newpayment.p (input table ttArPmt,
                           input true, /* Warning Msg */
                           output iarPmtID,
                           output dtCreatedDate,
                           output lDepositPosted,
                           output cFileID,
                           output iDepositID,
                           output lApplyToFile,
                           output lApplyToProduction,
                           output cFileExist,                           
                           output oplSuccess,
                           output std-ch).
  if not oplSuccess and
     std-ch = {&DuplicateCheck}
   then
    do:
      message "Unposted payment record for agent: " flEntity:input-value " and check number: " flRefNum:input-value " already exists. "
              "Do you still want to continue?"
          view-as alert-box question buttons yes-no update lupdate as logical.
                  
      if not lUpdate
       then return.
   
      /* client Server Call */            
      run server/newpayment.p (input table ttArPmt,
                               input false, /* Warning Msg */
                               output iarPmtID,
                               output dtCreatedDate,
                               output lDepositPosted,
                               output cFileID,
                               output iDepositID,
                               output lApplyToFile,  
                               output lApplyToProduction,
                               output cFileExist,                               
                               output oplSuccess,
                               output std-ch).
    end.
  
  if not oplSuccess
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
    
  for first ttArPmt:
    assign
         ttArPmt.ArPmtID       = iarPmtID
         ttArPmt.createdDate   = dtCreatedDate
         ttArPmt.depositPosted = lDepositPosted
         ttArPmt.fileID        = cFileID
         ttArPmt.fileExist     = cFileExist
         ttArPmt.depositID     = iDepositID
         ttArPmt.username      = cCurrentUser
         ttArPmt.createdBy     = cCurrentUID
         ttArPmt.remainingAmt  = ttArPmt.checkAmt   .
         
         if ttArPmt.transType = {&Production}  and lApplyToProduction
          then
           ttArPmt.transType  =  {&Production}  .
          else if  lApplyToFile 
           then
            ttArPmt.transType =  {&File}.  
          else
            ttArPmt.transType  = "N".  
         .
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setMandatoryMark Dialog-Frame 
PROCEDURE setMandatoryMark :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
do with frame {&frame-name}:
end.
assign
  fEntityTypeMand:screen-value = {&Mandatory}
  fRefNumMand:screen-value     = {&Mandatory}
  fReceipDateMand:screen-value = {&Mandatory}
  fAmountMand:screen-value     = {&Mandatory}
  fEntityMand:screen-value     = {&Mandatory}  
  fDepRefMand:screen-value     = {&Mandatory}
  .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validateReceiptDate Dialog-Frame 
FUNCTION validateReceiptDate RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if flReceiptDate:input-value <> ?    and 
     flReceiptDate:input-value > today
   then
    do:
      message "Receipt date cannot be in future."
          view-as alert-box.
      apply 'entry' to flReceiptDate.  
      return false.
    end.
    
  if flCheckDate:input-value   <> ? and 
     flReceiptDate:input-value < flCheckDate:input-value
   then
    do:
      message "Receipt date cannot be prior to the check/reference date."
          view-as alert-box.
      apply 'entry' to flReceiptDate.  
      return false.
    end.
    
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

