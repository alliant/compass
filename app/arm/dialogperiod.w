&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/* dialogperiod.w
   2.6.2013 D.Sinclair
9.3.2015 D.Sinclair - Simplified UI to display all months and a list of years
 */
                                                   
def input-output parameter pMonth as int.
def input-output parameter pYear as int.
def output parameter pSelect as logical init false.

{tt/period.i}

def var tInitYear as int no-undo.
def var tInitMonth as int no-undo.

def var tMinYear as int no-undo.
def var tMaxYear as int no-undo.
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tSelectMonth tSelectYear bSelect 
&Scoped-Define DISPLAYED-OBJECTS tSelectMonth tSelectYear tPeriodStatus 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayPeriod fMain 
FUNCTION displayPeriod RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bSelect AUTO-GO 
     LABEL "Select" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tPeriodStatus AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 18 BY .62
     FGCOLOR 12  NO-UNDO.

DEFINE VARIABLE tSelectMonth AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE NO-DRAG 
     LIST-ITEM-PAIRS "January","1",
                     "February","2",
                     "March","3",
                     "April","4",
                     "May","5",
                     "June","6",
                     "July","7",
                     "August","8",
                     "September","9",
                     "October","10",
                     "November","11",
                     "December","12" 
     SIZE 13.4 BY 7.62 NO-UNDO.

DEFINE VARIABLE tSelectYear AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE NO-DRAG SCROLLBAR-VERTICAL 
     LIST-ITEMS "2015" 
     SIZE 11.6 BY 7.62 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tSelectMonth AT ROW 1.71 COL 5 NO-LABEL WIDGET-ID 18
     tSelectYear AT ROW 1.71 COL 19.4 NO-LABEL WIDGET-ID 20
     bSelect AT ROW 10.29 COL 10.2
     tPeriodStatus AT ROW 9.43 COL 8.4 COLON-ALIGNED NO-LABEL WIDGET-ID 4 NO-TAB-STOP 
     SPACE(6.19) SKIP(1.85)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Select Period"
         DEFAULT-BUTTON bSelect WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN tPeriodStatus IN FRAME fMain
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX fMain
/* Query rebuild information for DIALOG-BOX fMain
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX fMain */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Select Period */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSelectMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSelectMonth fMain
ON MOUSE-SELECT-DBLCLICK OF tSelectMonth IN FRAME fMain
DO:
  apply "GO" to FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSelectMonth fMain
ON VALUE-CHANGED OF tSelectMonth IN FRAME fMain
DO:
  tInitMonth = self:input-value.
  displayPeriod().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSelectYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSelectYear fMain
ON MOUSE-SELECT-DBLCLICK OF tSelectYear IN FRAME fMain
DO:
  apply "GO" to FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSelectYear fMain
ON VALUE-CHANGED OF tSelectYear IN FRAME fMain
DO:
  tInitYear = integer(self:input-value).
  displayPeriod().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


publish "GetPeriods" (output table period).

find first period no-error.
if not available period 
 then tMinYear = year(today).
 else tMinYear = period.periodYear.

find last period no-error.
if not available period 
  or (available period and period.periodYear < year(today))
 then tMaxYear = year(today).
 else tMaxYear = period.periodYear.

std-ch = "".
do std-in = tMaxYear to tMinYear by -1:
 std-ch = std-ch + (if std-ch > "" then "," else "") + string(std-in, "9999").
end.
tSelectYear:list-items in frame fMain = std-ch.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
assign
  tSelectMonth:screen-value = string(pMonth)
  tSelectYear:screen-value = string(pYear)
  tInitMonth = pMonth
  tInitYear = pYear
  no-error.
  displayPeriod().
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  if available period
   then 
    do: if pYear <> tInitYear or pMonth <> tInitMonth
         then assign
                pYear = tInitYear
                pMonth = tInitMonth
                pSelect = true .
    end.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tSelectMonth tSelectYear tPeriodStatus 
      WITH FRAME fMain.
  ENABLE tSelectMonth tSelectYear bSelect 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayPeriod fMain 
FUNCTION displayPeriod RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  def buffer period for period.

  find period
    where period.periodYear = tInitYear
      and period.periodMonth = tInitMonth no-error.

  if not available period 
   then tPeriodStatus:screen-value in frame fMain = "Not Available".
   else
  if period.active = no 
   then tPeriodStatus:screen-value in frame fMain = "Closed".
   else tPeriodStatus:screen-value in frame fMain = "".

  bSelect:sensitive in frame fMain = available period.

  tSelectMonth:screen-value in frame fMain = string(tInitMonth) no-error.
  tSelectYear:screen-value in frame fMain = string(tInitYear, "9999") no-error.
   
  RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

