&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

File: waging.w 

Description: Aging of agent's file.

Input Parameters:
<none>

Output Parameters:
<none>

Author: Rahul

Created:

Modified: 
Date Name Comments
07/23/2019 

------------------------------------------------------------------------*/
/* This .W file was created with the Progress AppBuilder. */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
by this procedure. This is a good default which assures
that this procedure's triggers and internal procedures 
will execute in this procedure's storage, and that proper
cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ************************** Definitions ************************* */
{lib/std-def.i}
{lib/ar-def.i}
{lib/winlaunch.i}
{lib/rpt-defs.i}

define variable cStateList     as character   no-undo.
define variable cSelectedState as character   no-undo.
define variable opcFileName    as character   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fAsOfDate bFirstLookup cbState bGo ~
flStartAgent bLastLookup flEndAgent tgShowAllAgents rbReport rbReportType ~
RECT-132 
&Scoped-Define DISPLAYED-OBJECTS fAsOfDate cbState flStartAgent flEndAgent ~
tgShowAllAgents rbReport rbReportType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bFirstLookup  NO-FOCUS
     LABEL "Agentlookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Lookup for starting agent ID".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Send email".

DEFINE BUTTON bLastLookup  NO-FOCUS
     LABEL "Agentlookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Lookup for ending Agent ID".

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE fAsOfDate AS DATE FORMAT "99/99/99":U 
     LABEL "As Of" 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 TOOLTIP "As of date should be less than or equal to today" NO-UNDO.

DEFINE VARIABLE flEndAgent AS CHARACTER FORMAT "X(6)":U 
     LABEL "Ending Agent ID" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 TOOLTIP "Enter ending agent ID" NO-UNDO.

DEFINE VARIABLE flStartAgent AS CHARACTER FORMAT "X(6)":U 
     LABEL "Starting Agent ID" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 TOOLTIP "Enter starting agent ID" NO-UNDO.

DEFINE VARIABLE rbReport AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Summary", "S",
"Detail", "D"
     SIZE 13 BY 1.71 NO-UNDO.

DEFINE VARIABLE rbReportType AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Files", "F",
"Misc.", "M",
"Both", "B"
     SIZE 12 BY 3.1 NO-UNDO.

DEFINE RECTANGLE RECT-132
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 74.6 BY 10.67.

DEFINE VARIABLE tgShowAllAgents AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 3 BY .91 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     fAsOfDate AT ROW 2.19 COL 19.4 COLON-ALIGNED WIDGET-ID 414
     bFirstLookup AT ROW 5.29 COL 31.6 WIDGET-ID 350 NO-TAB-STOP 
     cbState AT ROW 3.33 COL 19.4 COLON-ALIGNED WIDGET-ID 242
     bGo AT ROW 9.29 COL 36.4 WIDGET-ID 74 NO-TAB-STOP 
     flStartAgent AT ROW 5.38 COL 19.4 COLON-ALIGNED WIDGET-ID 352
     bLastLookup AT ROW 6.43 COL 31.6 WIDGET-ID 480 NO-TAB-STOP 
     flEndAgent AT ROW 6.52 COL 19.4 COLON-ALIGNED WIDGET-ID 482
     tgShowAllAgents AT ROW 7.71 COL 21.4 WIDGET-ID 486
     rbReport AT ROW 2.67 COL 59.8 NO-LABEL WIDGET-ID 442
     rbReportType AT ROW 6 COL 59.8 NO-LABEL WIDGET-ID 492
     "Content" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 5.33 COL 57 WIDGET-ID 490
     "Parameters" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 1.05 COL 3.6 WIDGET-ID 484
     "Show agents with no activity:" VIEW-AS TEXT
          SIZE 27.6 BY 1 AT ROW 7.67 COL 24.2 WIDGET-ID 488
     "Type" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 2 COL 57 WIDGET-ID 476
     RECT-132 AT ROW 1.29 COL 2.4 WIDGET-ID 478
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 127.6 BY 12.86 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Aging (Files)"
         HEIGHT             = 11.24
         WIDTH              = 77.2
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Aging (Files) */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Aging (Files) */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Aging (Files) */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFirstLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFirstLookup C-Win
ON CHOOSE OF bFirstLookup IN FRAME fMain /* Agentlookup */
DO:
  run selectAgent in this-procedure (input flStartAgent:handle). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:
  run checkValidation in this-procedure (output std-ch).
  if std-ch ne ""
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return no-apply.
    end.
              
   run setData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLastLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLastLookup C-Win
ON CHOOSE OF bLastLookup IN FRAME fMain /* Agentlookup */
DO:
  run selectAgent in this-procedure (input flEndAgent:handle).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState C-Win
ON VALUE-CHANGED OF cbState IN FRAME fMain /* State */
DO:
  if cbState:input-value <> cSelectedState
   then
    assign
        flStartAgent:screen-value = ""
        flEndAgent:screen-value   = ""
        .
  cSelectedState = cbState:input-value.    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */ 
{lib/win-main.i}
{lib/win-status.i}

assign 
  {&window-name}:min-height-pixels = {&window-name}:height-pixels
  {&window-name}:min-width-pixels  = {&window-name}:width-pixels
  {&window-name}:max-height-pixels = session:height-pixels
  {&window-name}:max-width-pixels  = session:width-pixels
  .

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME} .


setStatusMessage("").       
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bGo:load-image                     ("images/completed.bmp").
bGo:load-image-insensitive         ("images/completed.bmp").

bLastLookup:load-image             ("images/s-lookup.bmp").
bLastLookup:load-image-insensitive ("images/s-lookup-i.bmp").

bFirstLookup:load-image            ("images/s-lookup.bmp").
bFirstLookup:load-image-insensitive("images/s-lookup-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

/* Return all state ID with description */
publish "getStateList" (output cStateList).

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  RUN enable_UI.

  if cStateList > "" 
   then cStateList = "," + cStateList.

  assign
      cbState:list-item-pairs   = {&ALL} + "," + {&ALL} + cStateList
      cbState:screen-value      = {&ALL} 
      cSelectedState            = {&ALL}
      .

  fAsOfDate:screen-value = string(today).

  run showWindow in this-procedure.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE checkValidation C-Win 
PROCEDURE checkValidation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter opError as character no-undo.
  do with frame {&frame-name}:
  end.
  
  if fAsOfDate:input-value > today 
   then
    do:
      opError = "As of Date cannot be in the future.".
      return.    
    end.
    
  if fAsOfDate:input-value = ?
   then
    do:
      opError = "As of Date cannot be blank.".
      return.
    end.     
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fAsOfDate cbState flStartAgent flEndAgent tgShowAllAgents rbReport 
          rbReportType 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE fAsOfDate bFirstLookup cbState bGo flStartAgent bLastLookup flEndAgent 
         tgShowAllAgents rbReport rbReportType RECT-132 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE selectAgent C-Win 
PROCEDURE selectAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter phaAgentID as handle no-undo.
  
  define variable cAgentID  as character no-undo.
  define variable cStateID  as character no-undo.
  
  do with frame {&frame-name}:
  end.
  
  run dialogagentlookup.w(input phaAgentID:input-value,
                          input cbState:input-value,  /* Selected State ID */
                          input false,                /* Allow 'ALL' */
                          output cAgentID,           
                          output cStateID,            
                          output std-ch,              /* Agent name */
                          output std-lo).
   
  if not std-lo  
   then
     return.
     
  assign
      phaAgentID:screen-value = cAgentID
      cbState:screen-value    = (if cbState:input-value <> cStateID then {&ALL} else cStateID)
      cSelectedState          = cbState:input-value
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData C-Win 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cReportFormat as character no-undo.
  define variable cDestination  as character no-undo.
   
  do with frame {&frame-name}:
  end.
  
  /* possible output formats */
  cReportFormat = {&rpt-pdf} + "," + {&rpt-csv}.  
  
  cDestination  = {&rpt-destEmail} + "," +
                  {&rpt-destPrinter} + "," + {&rpt-destView}. 

  {lib/rpt-dialogDestination.i &rpt-format = cReportFormat &rpt-destinationType = cDestination}

  if rpt-cancel
   then
    return.
  
  run server/generatearaging.p (input flStartAgent:input-value, /* Start AgentID */
                                input flEndAgent:input-value,   /* End AgentID */
                                input cbState:input-value,      /* StateID */
                                input fAsOfDate:input-value,
                                input rbReport:input-value,                               
                                input rbReportType:input-value,
                                input tgShowAllAgents:checked,
                                {lib/rpt-setparams.i},
                                output opcFileName,
                                output std-lo,
                                output std-ch).
                                 
  if not std-lo
   then
    message std-ch
      view-as alert-box error buttons ok. 
   else if rpt-behavior = "" /* code below this will also change AG */
    then
     do:
       if opcFileName <> ""
        then
         run util/openfile.p (opcFileName).
         /* run viewReport in this-procedure. */
        else 
         message "Nothing to print." view-as alert-box warning buttons ok.   
     end.  
   else if std-ch <> ""
    then
     message std-ch
        view-as alert-box information buttons ok.  
        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  frame fMain:width-pixels          = {&window-name}:width-pixels.
  frame fMain:virtual-width-pixels  = {&window-name}:width-pixels.
  frame fMain:height-pixels         = {&window-name}:height-pixels.
  frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

