&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: whistorictransaction.w

  Description: Window for AR posted history transaction from GP

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul

  Created: 27.01.2021

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/ar-def.i}
{lib/winshowscrollbars.i}
{lib/ar-gettrantype.i}   /* Include function: getTranType */
{lib/ar-getentitytype.i} /* Include function: getEntityType */

{tt/artran.i   &tableAlias="ttArTran"}         /* used for data transfer */
{tt/artran.i   &tableAlias="arPostedTran"}     /* contain filtered data */
{tt/artran.i   &tableAlias="ttArPostedTran"}   /* contain data recived from server */
{tt/armisc.i   &tablealias=ttArMisc}           /* used to show header detail */
{tt/arpmt.i    &tablealias=ttArPmt}            /* used to show header detail */
{tt/ledgerreport.i}                            /* Show prelim and posted GL report */

define variable cAgentID          as character no-undo.
define variable cStateID          as character no-undo.
define variable cRevenueType      as character no-undo.
define variable lDefaultAgent     as logical   no-undo.
define variable hPopupMenu        as handle    no-undo.
define variable hPopupMenuItem    as handle    no-undo.
define variable hPopupMenuItem1   as handle    no-undo.
define variable hPopupMenuItem2   as handle    no-undo.
define variable daPostDate        as date      no-undo.
define variable daClosedDate      as date      no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwArPostedTran

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES arPostedTran

/* Definitions for BROWSE brwArPostedTran                               */
&Scoped-define FIELDS-IN-QUERY-brwArPostedTran arPostedTran.entityid "Agent ID" arPostedTran.filenumber "File / ID" getTranType(arPostedTran.type) @ arPostedTran.type "Type" arPostedTran.trandate "Posted" arPostedTran.tranamt "Total" arPostedTran.remainingamt "Balance" arPostedTran.reference   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwArPostedTran   
&Scoped-define SELF-NAME brwArPostedTran
&Scoped-define QUERY-STRING-brwArPostedTran for each arPostedTran
&Scoped-define OPEN-QUERY-brwArPostedTran open query {&SELF-NAME} for each arPostedTran.
&Scoped-define TABLES-IN-QUERY-brwArPostedTran arPostedTran
&Scoped-define FIRST-TABLE-IN-QUERY-brwArPostedTran arPostedTran


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwArPostedTran}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS flAgentID flInvoiceDate flReference flFile ~
flRevType bAgentLookup flReceiptDate flCheck bRevenueType flAmount edNotes ~
flPostDate brwArPostedTran flName bExport RECT-89 RECT-90 RECT-91 
&Scoped-Define DISPLAYED-OBJECTS flAgentID flInvoiceDate flReference flFile ~
flRevType flReceiptDate flCheck flAmount edNotes flPostDate flName ~
fMarkMandatory5 fMarkMandatory1 fMarkMandatory3 fMarkMandatory4 ~
fMarkMandatory2 fMarkMandatory6 fMarkMandatory7 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validateData C-Win 
FUNCTION validateData RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup  NO-FOCUS
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Agent lookup".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bPost  NO-FOCUS
     LABEL "Post" 
     SIZE 7.2 BY 1.71 TOOLTIP "Post data"
     BGCOLOR 8 .

DEFINE BUTTON bPrelimRpt  NO-FOCUS
     LABEL "Preliminary" 
     SIZE 7.2 BY 1.71 TOOLTIP "Preliminary report".

DEFINE BUTTON bRevenueType  NO-FOCUS
     LABEL "RevenueType" 
     SIZE 4.8 BY 1.14 TOOLTIP "Revenue lookup".

DEFINE VARIABLE edNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 62.8 BY 2.81 NO-UNDO.

DEFINE VARIABLE flAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE flAmount AS DECIMAL FORMAT ">>>>>9.99":U INITIAL 0 
     LABEL "Amount" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE flCheck AS CHARACTER FORMAT "X(256)":U 
     LABEL "Check" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE flFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "File Number" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE flInvoiceDate AS DATE FORMAT "99/99/99":U 
     LABEL "Invoice Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 70.6 BY 1 NO-UNDO.

DEFINE VARIABLE flPostDate AS DATE FORMAT "99/99/99":U 
     LABEL "Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE flReceiptDate AS DATE FORMAT "99/99/99":U 
     LABEL "Receipt Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE flReference AS CHARACTER FORMAT "X(256)":U 
     LABEL "Reference" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE flRevType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Revenue Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20.2 BY 1 NO-UNDO.

DEFINE VARIABLE fMarkMandatory1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory3 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory4 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory5 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory6 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory7 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE RECTANGLE RECT-89
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 40.6 BY 2.81.

DEFINE RECTANGLE RECT-90
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 51.8 BY 5.29.

DEFINE RECTANGLE RECT-91
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 51.8 BY 5.29.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwArPostedTran FOR 
      arPostedTran SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwArPostedTran
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwArPostedTran C-Win _FREEFORM
  QUERY brwArPostedTran DISPLAY
      arPostedTran.entityid                              label         "Agent ID"        format "x(8)"    
arPostedTran.filenumber                                  label         "File / ID"       format "x(30)" width 20   
getTranType(arPostedTran.type) @ arPostedTran.type       label         "Type"            format "x(8)"  
arPostedTran.trandate                                    label         "Posted"          format "99/99/99" width 10
arPostedTran.tranamt                                     label         "Total"           format ">>,>>>,>>9.99"  width 15
arPostedTran.remainingamt                                label         "Balance"         format ">>,>>>,>>9.99"  width 15     
arPostedTran.reference                                   column-label  "Check/Ref."      format "x(18)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 104.8 BY 6.86
         TITLE "Transaction Posted During Current Session" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     flAgentID AT ROW 1.57 COL 16.8 COLON-ALIGNED WIDGET-ID 454
     bPost AT ROW 9.71 COL 96 WIDGET-ID 464 NO-TAB-STOP 
     flInvoiceDate AT ROW 3.57 COL 24 COLON-ALIGNED WIDGET-ID 62
     bPrelimRpt AT ROW 9.71 COL 103.2 WIDGET-ID 408 NO-TAB-STOP 
     flReference AT ROW 4.67 COL 24 COLON-ALIGNED WIDGET-ID 244
     flFile AT ROW 5.76 COL 24 COLON-ALIGNED WIDGET-ID 42
     flRevType AT ROW 6.86 COL 24 COLON-ALIGNED WIDGET-ID 64
     bAgentLookup AT ROW 1.48 COL 34 WIDGET-ID 452 NO-TAB-STOP 
     flReceiptDate AT ROW 4.05 COL 76.2 COLON-ALIGNED WIDGET-ID 468
     flCheck AT ROW 5.14 COL 76.2 COLON-ALIGNED WIDGET-ID 460
     bRevenueType AT ROW 6.81 COL 46.4 WIDGET-ID 252 NO-TAB-STOP 
     flAmount AT ROW 6.24 COL 76.2 COLON-ALIGNED WIDGET-ID 456
     edNotes AT ROW 9.19 COL 7.8 NO-LABEL WIDGET-ID 26
     flPostDate AT ROW 10.05 COL 77.8 COLON-ALIGNED WIDGET-ID 472
     brwArPostedTran AT ROW 12.43 COL 7.8 WIDGET-ID 200
     flName AT ROW 1.57 COL 40 COLON-ALIGNED NO-LABEL WIDGET-ID 458 NO-TAB-STOP 
     bExport AT ROW 12.38 COL 2.8 WIDGET-ID 8 NO-TAB-STOP 
     fMarkMandatory5 AT ROW 5.38 COL 101.4 COLON-ALIGNED NO-LABEL WIDGET-ID 272 NO-TAB-STOP 
     fMarkMandatory1 AT ROW 7.14 COL 49.2 COLON-ALIGNED NO-LABEL WIDGET-ID 360 NO-TAB-STOP 
     fMarkMandatory3 AT ROW 1.81 COL 36.8 COLON-ALIGNED NO-LABEL WIDGET-ID 362 NO-TAB-STOP 
     fMarkMandatory4 AT ROW 6.48 COL 91.4 COLON-ALIGNED NO-LABEL WIDGET-ID 364 NO-TAB-STOP 
     fMarkMandatory2 AT ROW 6 COL 49.2 COLON-ALIGNED NO-LABEL WIDGET-ID 466 NO-TAB-STOP 
     fMarkMandatory6 AT ROW 3.81 COL 39.2 COLON-ALIGNED NO-LABEL WIDGET-ID 270 NO-TAB-STOP 
     fMarkMandatory7 AT ROW 4.24 COL 91.4 COLON-ALIGNED NO-LABEL WIDGET-ID 470 NO-TAB-STOP 
     "Notes:" VIEW-AS TEXT
          SIZE 6.4 BY .62 AT ROW 8.57 COL 8 WIDGET-ID 28
     "Post" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 8.91 COL 73.6 WIDGET-ID 412
     "Invoice" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 2.86 COL 9.4 WIDGET-ID 478
     "Payment" VIEW-AS TEXT
          SIZE 8.2 BY .62 AT ROW 2.86 COL 62.4 WIDGET-ID 480
     RECT-89 AT ROW 9.19 COL 72 WIDGET-ID 410
     RECT-90 AT ROW 3.14 COL 7.8 WIDGET-ID 474
     RECT-91 AT ROW 3.14 COL 60.8 WIDGET-ID 476
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 115.2 BY 22.1 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Add History Transaction"
         HEIGHT             = 18.33
         WIDTH              = 114
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwArPostedTran flPostDate DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bPost IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPrelimRpt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwArPostedTran:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwArPostedTran:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwArPostedTran:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

ASSIGN 
       edNotes:RETURN-INSERTED IN FRAME DEFAULT-FRAME  = TRUE.

ASSIGN 
       flName:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       flRevType:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN fMarkMandatory1 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory2 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory3 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory4 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory5 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory6 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory7 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwArPostedTran
/* Query rebuild information for BROWSE brwArPostedTran
     _START_FREEFORM
open query {&SELF-NAME} for each arPostedTran.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwArPostedTran */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Add History Transaction */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Add History Transaction */
DO:  
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Add History Transaction */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup C-Win
ON CHOOSE OF bAgentLookup IN FRAME DEFAULT-FRAME /* agentlookup */
DO:
  define variable cAgentID  as character no-undo.
  define variable cName     as character no-undo.
    
  run dialogagentlookup.w(input flAgentID:input-value,
                          input "",        /* Selected State ID */
                          input false,     /* Allow 'ALL' */
                          output cAgentID,
                          output cStateID, /* Agent state ID */
                          output cName,
                          output std-lo).
   
  if not std-lo or flAgentID:input-value = cAgentID  
   then
     return no-apply.
     
  assign
      flAgentID:screen-value = cAgentID
      flName:screen-value    = cName
      . 
  
  /* Set default AgentID */
  if lDefaultAgent and flAgentID:input-value <> ""
   then
    publish "SetDefaultAgent" (input flAgentID:input-value).                               
  
  run enableDisableSave in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPost
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPost C-Win
ON CHOOSE OF bPost IN FRAME DEFAULT-FRAME /* Post */
DO:
  if validateData()
   then run postHistoricData in this-procedure.    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrelimRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrelimRpt C-Win
ON CHOOSE OF bPrelimRpt IN FRAME DEFAULT-FRAME /* Preliminary */
DO:
  run prelimreport in this-procedure.   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRevenueType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRevenueType C-Win
ON CHOOSE OF bRevenueType IN FRAME DEFAULT-FRAME /* RevenueType */
DO:
  run dialogrevenuelookup.w(input flRevType:screen-value,
                            input cStateID,
                            output cRevenueType,
                            output std-lo).
  if not std-lo 
   then
    return no-apply.

  flRevType:screen-value = cRevenueType.
      
  run enableDisableSave in this-procedure.    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwArPostedTran
&Scoped-define SELF-NAME brwArPostedTran
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArPostedTran C-Win
ON DEFAULT-ACTION OF brwArPostedTran IN FRAME DEFAULT-FRAME /* Transaction Posted During Current Session */
DO:
  case arPostedTran.type :
    when {&Payment} /* 'P' type Payment */
     then
      run showHeaderDetails in this-procedure.
    when {&Invoice} /* 'I' type Misc Invoice */
     then
      run showTransactionDetail in this-procedure.      
  end case.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArPostedTran C-Win
ON ROW-DISPLAY OF brwArPostedTran IN FRAME DEFAULT-FRAME /* Transaction Posted During Current Session */
do:
  {lib/brw-rowdisplay.i}
  
  if arPostedTran.remainingamt < 0 
   then
    arPostedTran.remainingamt   :fgcolor in browse brwArPostedTran   = 12.
   else
    arPostedTran.remainingamt   :fgcolor in browse brwArPostedTran   = 0.
    
  if arPostedTran.tranamt < 0 
   then
    arPostedTran.tranamt   :fgcolor in browse brwArPostedTran   = 12.
   else
    arPostedTran.tranamt   :fgcolor in browse brwArPostedTran   = 0.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArPostedTran C-Win
ON START-SEARCH OF brwArPostedTran IN FRAME DEFAULT-FRAME /* Transaction Posted During Current Session */
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArPostedTran C-Win
ON VALUE-CHANGED OF brwArPostedTran IN FRAME DEFAULT-FRAME /* Transaction Posted During Current Session */
DO:
  run changePopupMenu in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edNotes C-Win
ON VALUE-CHANGED OF edNotes IN FRAME DEFAULT-FRAME
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID C-Win
ON LEAVE OF flAgentID IN FRAME DEFAULT-FRAME /* Agent ID */
DO:
  define variable cName as character no-undo.
    
  if flAgentID:input-value <> ""
   then
    do:
      publish "getAgentName" (input flAgentID:input-value,
                              output cName,
                              output std-lo).
  
      if not std-lo 
       then 
        do:
          assign 
              flAgentID:screen-value = "" 
              flName:screen-value    = ""
              .
          return no-apply.
        end.
      
      publish "getAgentStateID" (input flAgentID:input-value,
                                 output cStateID,
                                 output std-lo).
      
      /* Set default AgentID */
      if lDefaultAgent 
       then
        publish "SetDefaultAgent" (input self:input-value).
    end. 
  
  flName:screen-value = cName.
                                   
  run enableDisableSave in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID C-Win
ON VALUE-CHANGED OF flAgentID IN FRAME DEFAULT-FRAME /* Agent ID */
DO:
  flName:screen-value = "".
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAmount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAmount C-Win
ON VALUE-CHANGED OF flAmount IN FRAME DEFAULT-FRAME /* Amount */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flCheck
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flCheck C-Win
ON VALUE-CHANGED OF flCheck IN FRAME DEFAULT-FRAME /* Check */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flFile C-Win
ON VALUE-CHANGED OF flFile IN FRAME DEFAULT-FRAME /* File Number */
DO:  
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flInvoiceDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flInvoiceDate C-Win
ON VALUE-CHANGED OF flInvoiceDate IN FRAME DEFAULT-FRAME /* Invoice Date */
DO:
  run enableDisableSave in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flPostDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flPostDate C-Win
ON VALUE-CHANGED OF flPostDate IN FRAME DEFAULT-FRAME /* Date */
DO:
  run enableDisableSave in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flReceiptDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flReceiptDate C-Win
ON VALUE-CHANGED OF flReceiptDate IN FRAME DEFAULT-FRAME /* Receipt Date */
DO:
  run enableDisableSave in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flReference
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flReference C-Win
ON VALUE-CHANGED OF flReference IN FRAME DEFAULT-FRAME /* Reference */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

subscribe to "closeWindow" anywhere.
 
/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bPost        :load-image            ("images/check.bmp").              
bPost        :load-image-insensitive("images/check-i.bmp").

bPrelimRpt:load-image             ("images/pdf.bmp").
bPrelimRpt:load-image-insensitive ("images/pdf-i.bmp").

bExport     :load-image            ("images/s-excel.bmp").
bExport     :load-image-insensitive("images/s-excel-i.bmp").

bAgentLookup:load-image            ("images/s-lookup.bmp").
bAgentLookup:load-image-insensitive("images/s-lookup-i.bmp").

bRevenueType:load-image            ("images/s-lookup.bmp").
bRevenueType:load-image-insensitive("images/s-lookup-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  {&window-name}:window-state = window-minimized.  
   
  run enable_UI.  
        
  /* Get default StateID and AgentID */
  run getDefaultValues in this-procedure.
            
  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure. 
  
  apply 'entry' to flAgentID.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changePopupMenu C-Win 
PROCEDURE changePopupMenu :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available arPostedTran
   then return.
     
  if valid-handle(hPopupMenuItem) 
   then
    delete object hPopupMenuItem.

  if valid-handle(hPopupMenuItem1) 
   then
    delete object hPopupMenuItem1.  
  
  if valid-handle(hPopupMenuItem2) 
   then
    delete object hPopupMenuItem2.
    
  if valid-handle(hPopupMenu) 
   then
    delete object hPopupMenu.
              
  create menu hPopupMenu.
  assign
      hPopupMenu:popup-only = true
      hPopupMenu:title      = "Browser menu"
      .
  
  /* As the popup menu differes depending on the type 
     of header record, so dynamically creating the popup
     menu specific for each artran type. */
     
 case arPostedTran.type :
     
   when {&Payment} /* 'P' type Payment */
    then
     do:
       create menu-item hPopupMenuItem
       assign
           parent = hPopupMenu
           label  = "Transaction Detail"
           name   = "Transaction Detail"
                  
       triggers:
         on choose persistent run showTransactionDetail in this-procedure.
       end triggers.
    
       create menu-item hPopupMenuItem1
       assign
           parent = hPopupMenu
           label  = "Payment Detail"
           name   = "Payment Detail"
                  
       triggers:
         on choose persistent run showHeaderDetails in this-procedure.
       end triggers.
       
       create menu-item hPopupMenuItem2
       assign
           parent    = hPopupMenu
           label     = "Apply Payment"
           name      = "Apply Payment"
           sensitive = not arPostedTran.void
                  
       triggers:
         on choose persistent run openApply in this-procedure.
       end triggers.
              
       self:popup-menu = hPopupMenu.
     end.
       
   when {&Invoice} /* 'I' type Misc Invoice */
    then
     do: 
      create menu-item hPopupMenuItem
      assign
          parent = hPopupMenu
          label  = "Transaction Detail"
          name   = "Transaction Detail"
                  
      triggers:
        on choose persistent run showTransactionDetail in this-procedure.
      end triggers.
       
       create menu-item hPopupMenuItem1
       assign 
           label  = "Miscellaneous Invoice Detail"
           name   = "Miscellaneous Invoice Detail"
           parent = hPopupMenu
       triggers:
         on choose persistent run showHeaderDetails in this-procedure.
       end triggers.
          
       self:popup-menu = hPopupMenu.
     end.
          
 end case.
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clearData C-Win 
PROCEDURE clearData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  assign
      cAgentID                   = ""
      cStateID                   = ""
      cRevenueType               = ""
      flAgentID:screen-value     = ""
      flName:screen-value        = ""
      flFile:screen-value        = ""
      flRevType:screen-value     = ""
      flReference:screen-value   = ""
      flCheck:screen-value       = ""
      edNotes:screen-value       = ""
      flAmount:screen-value      = ""
      bPost:sensitive            = false
      bPrelimRpt:sensitive       = false
      flPostDate:screen-value    = string(daPostDate)
      flInvoiceDate:screen-value = string(daClosedDate)
      flReceiptDate:screen-value = string(daClosedDate)      
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if valid-handle(hPopupMenuItem) 
   then
    delete object hPopupMenuItem.

  if valid-handle(hPopupMenuItem1) 
   then
    delete object hPopupMenuItem1.  
   
  if valid-handle(hPopupMenu) 
   then
    delete object hPopupMenu.
  publish "WindowClosed" (input this-procedure).  
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave C-Win 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
 
 bPost:sensitive = not (flAgentID:input-value     = "" or 
                        flCheck:input-value       = "" or
                        flFile:input-value        = "" or
                        flRevType:input-value     = "" or
                        flPostDate:input-value    = ?  or
                        flInvoiceDate:input-value = ?  or
                        flReceiptDate:input-value = ?  or
                        flAmount:input-value      = 0
                        ) no-error.  
 
 bPrelimRpt:sensitive = not (flAgentID:input-value     = "" or 
                             flCheck:input-value       = "" or
                             flFile:input-value        = "" or
                             flRevType:input-value     = "" or
                             flPostDate:input-value    = ?  or
                             flInvoiceDate:input-value = ?  or
                             flReceiptDate:input-value = ?  or
                             flAmount:input-value      = 0
                             ) no-error.                        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flAgentID flInvoiceDate flReference flFile flRevType flReceiptDate 
          flCheck flAmount edNotes flPostDate flName fMarkMandatory5 
          fMarkMandatory1 fMarkMandatory3 fMarkMandatory4 fMarkMandatory2 
          fMarkMandatory6 fMarkMandatory7 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE flAgentID flInvoiceDate flReference flFile flRevType bAgentLookup 
         flReceiptDate flCheck bRevenueType flAmount edNotes flPostDate 
         brwArPostedTran flName bExport RECT-89 RECT-90 RECT-91 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwArPostedTran:num-results = 0 
   then
    do: 
      message "There is nothing to export."
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
  
  define buffer arPostedTran for arPostedTran.
  
  empty temp-table ttArTran.
  
  for each arPostedTran:
     create ttArTran.
     buffer-copy arPostedTran to ttArTran.
     assign
        ttArTran.type       = getTranType(arPostedTran.type)
        ttArTran.transtype  = getTranType(arPostedTran.transtype)
        ttArTran.entity     = getEntityType(arPostedTran.entity)
        .
  end.
  
  std-ha = temp-table ttArTran:handle.
  run util/exporttable.p (table-handle std-ha,
                          "ttArTran",
                          "for each ttArTran",
                          "entity,entityid,entityname,filenumber,fileID,type,revenuetype,sourceID,artranID,trandate,tranamt,remainingamt,appliedamt,reference,void,voiddate,voidby,notes,createddate,createdby,fullypaid,transtype,postdate,postby,accumbalance,username,arnotes",
                          "Entity,Entity ID,Entity Name,File Number,File ID,Type,Revenue Type,Source ID,Artran ID,Transaction Date,Transaction Amount,Remaining Amount,Applied Amount,Reference,Void,Void Date,Void By,Notes,Created Date,Created By,Fullypaid,Trans Type,Post Date,Post By,Accum Balance,Username,Notes",
                          std-ch,
                          "HistoryTransactions-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).                          
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getDefaultValues C-Win 
PROCEDURE getDefaultValues :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Mandatory checks */
  assign      
      fMarkMandatory1:screen-value = {&Mandatory}
      fMarkMandatory2:screen-value = {&Mandatory}
      fMarkMandatory3:screen-value = {&Mandatory}
      fMarkMandatory4:screen-value = {&Mandatory}
      fMarkMandatory5:screen-value = {&Mandatory}
      fMarkMandatory6:screen-value = {&Mandatory} 
      fMarkMandatory7:screen-value = {&Mandatory}
      .
  
  /* Set last closed period date on screen */
  publish "getClosedPeriodDate"(output daClosedDate).
  assign
      flInvoiceDate:screen-value   = string(daClosedDate)
      flReceiptDate:screen-value   = string(daClosedDate)
      .
      
  /* Set default posting date on screen */
  publish "getDefaultPostingDate"(output daPostDate).
  flPostDate:screen-value = string(daPostDate).
  
  publish "GetAutoDefaultAgent" (output lDefaultAgent).
    
  if lDefaultAgent
   then
    do:
      /* Get default AgentID */
      publish "GetDefaultAgent" (output cAgentID).
      flAgentID:screen-value = cAgentID.
      
      publish "getAgentName" (input flAgentID:input-value,
                              output std-ch,
                              output std-lo).
      
      publish "getAgentStateID" (input flAgentID:input-value,
                                 output cStateID,
                                 output std-lo).
      
      flName:screen-value = std-ch. 
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openApply C-Win 
PROCEDURE openApply :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available arpostedtran and arpostedtran.type <> {&Payment} 
   then return.
  
  /* Show records based on the parameter list */
  publish "SetCurrentValue" ("ApplyParams", arpostedtran.entityID + "|" + {&Payment} + "|" + string(arpostedtran.artranID)).
  
  publish "OpenWindow" (input "wapply", 
                        input {&Payment} + "|" + string(arpostedtran.artranID), 
                        input "wapply.w", 
                        input ?,                                   
                        input this-procedure).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE postHistoricData C-Win 
PROCEDURE postHistoricData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lViewPdf as logical no-undo.
  
  define buffer ttArPostedTran for ttArPostedTran. 
  
  do with frame {&frame-name}:
  end.
  
  run dialogviewledger.w (input "Post History Transaction",output  std-lo).  /* ispost*/
   
  if not std-lo
   then
    return.
   
  publish "GetViewPdf" (output lViewPdf). 
  
  run server\posthistorictrans.p(input flAgentID:input-value,
                                 input flFile:input-value,
                                 input flRevType:input-value,
                                 input flReference:input-value,
                                 input flCheck:input-value,
                                 input edNotes:input-value,
                                 input flAmount:input-value,
                                 input flInvoiceDate:input-value,
                                 input flReceiptDate:input-value,
                                 input flPostDate:input-value,
                                 input lViewPdf,
                                 output table ttArPostedTran,
                                 output table ledgerreport,
                                 output std-lo,
                                 output std-ch).                              
  
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
   else
     message "Posting was successful."
         view-as alert-box information buttons ok.
  
  if lViewPdf
   then
    do:
      if not can-find(first ttArPostedTran) 
       then
        message "Nothing to print."
            view-as alert-box error buttons ok.
       else
        run util\arhistorictranspdf.p (input {&view},
                                       input table ledgerreport,
                                       output std-ch).
    end.
  
  empty temp-table arPostedTran.

  for each ttArPostedTran:
    create arPostedTran.
    buffer-copy ttArPostedTran to arPostedTran.    
  end.
    
  open query brwArPostedTran for each arPostedTran.
    
  if can-find(first arPostedTran) 
   then
    apply 'value-changed' to browse brwArPostedTran .
    
  run setwidgets in this-procedure. 
  
  run clearData in this-procedure.   
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE prelimreport C-Win 
PROCEDURE prelimreport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  run server/prelimhistorygl.p (input flAgentID:input-value,
                                input flFile:input-value,                                
                                input flReference:input-value,
                                input flCheck:input-value,
                                input edNotes:input-value,
                                input flAmount:input-value,
                                input flRevType:input-value,
                                output table ledgerreport,
                                output std-lo,
                                output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
    
  if not can-find(first ledgerreport) 
   then
    do:
      message "Nothing to print."
          view-as alert-box error buttons ok.
      return.
    end. 
  
  run util\arhistorictranspdf.p (input {&view},
                                 input table ledgerreport,
                                 output std-ch).                                       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setwidgets C-Win 
PROCEDURE setwidgets :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame default-frame:
  end.
  
  define buffer arpostedtran for arpostedtran.
  
  if not can-find( first arpostedtran ) or
     query brwArPostedTran:num-results = 0
   then 
    bExport:sensitive = false.
   else
    bExport:sensitive = true.     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showHeaderDetails C-Win 
PROCEDURE showHeaderDetails :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available arpostedtran
   then return.
  
  define buffer ttArPmt  for ttArPmt.
  define buffer ttArMisc for ttArMisc.

  empty temp-table ttArPmt.
  empty temp-table ttArMisc.
  
  case arpostedtran.type:
    when {&Payment} /* 'P' type Payment */
     then
      do:
        run server\getpayment.p (input arpostedtran.artranID, 
                                 input arpostedtran.tranID,   
                                 output table ttArPmt,
                                 output std-lo,
                                 output std-ch).
          
        if not std-lo
         then
          do:
            message std-ch
                view-as alert-box error buttons ok.
            return.  
          end.
        
        for first ttArPmt:    
          run dialogmodifypayment.w (input {&ModifyPosted},
                                     input-output table ttArPmt,
                                     output std-lo).
        end.
      end.
   
     when {&Invoice} /* 'I' type Misc Invoice */
      then
       do:
         run server\getinvoices.p (input {&Invoice}, 
                                   input arpostedtran.tranID,   
                                   output table ttArMisc,
                                   output std-lo,
                                   output std-ch).
         if not std-lo
          then
           do:
             message std-ch
                 view-as alert-box error buttons ok.
             return.  
           end.                        
                                  
         for first ttArMisc:   
           run dialoginvoice.w (input-output table ttArMisc,
                                input {&Invoice},
                                input {&ModifyPosted},
                                output std-lo).
         end.                                  
       end.                     
  end case.
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showTransactionDetail C-Win 
PROCEDURE showTransactionDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available arPostedTran
   then return.
   
  publish "OpenWindow" (input "wtransactiondetail",          /*childtype*/
                        input string(arPostedTran.arTranID), /*childid*/
                        input "wtransactiondetail.w",        /*window*/
                        input "integer|input|" + string(arPostedTran.arTranID)  + "^integer|input|0^character|input|",  /*parameters*/                               
                        input this-procedure).               /*currentProcedure handle*/ 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state = window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by arpostedtran.entityid by arpostedtran.type by arpostedtran.tranID ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
  
  if can-find(first arpostedtran) 
   then
    apply 'value-changed' to browse  brwArPostedTran. 
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 47
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 3
      .
   
  run ShowScrollBars(frame {&frame-name}:handle, no, no).  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validateData C-Win 
FUNCTION validateData RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if flAgentID:input-value = "" 
   then
    do:
      message "Agent ID cannot be blank."
        view-as alert-box error buttons ok.
      return false.
    end.
  
  if flFile:input-value = "" 
   then
    do:
      message "File number cannot be blank."
        view-as alert-box error buttons ok.
      return false.
    end.
  
  if flCheck:input-value = "" 
   then
    do:
      message "Check/reference cannot be blank."
        view-as alert-box error buttons ok.
      return false.
    end.
  
  if flRevType:input-value = "" 
   then
    do:
      message "Revenue type cannot be blank."
        view-as alert-box error buttons ok.
      return false.
    end.
 
  if flAmount:input-value = 0 
   then
    do:
      message "Amount cannot be zero."
        view-as alert-box error buttons ok.
      return false.
    end.
  
  if flInvoiceDate:input-value = ? 
   then
    do:
      message "Invoice date cannot be blank."
        view-as alert-box error buttons ok.
      return false.
    end.
  
  if flReceiptDate:input-value = ? 
   then
    do:
      message "Payment receipt date cannot be blank."
        view-as alert-box error buttons ok.
      return false.
    end.
  
  if flPostDate:input-value = ? 
   then
    do:
      message "Post date cannot be blank."
        view-as alert-box error buttons ok.
      return false.
    end.
  
  if flPostDate:input-value < flReceiptDate:input-value
   then
    do:
      message "Post date cannot be prior to the payment receipt date."
        view-as alert-box error buttons ok.
      return false.
    end.
  
  if flPostDate:input-value < flInvoiceDate:input-value
   then
    do:
      message "Post date cannot be prior to the invoice date."
        view-as alert-box error buttons ok.
      return false.
    end.
  
  if flPostDate:input-value > today
   then
    do:
      message "Post date cannot be in the future."
        view-as alert-box error buttons ok.
      return false.
    end.
  
  publish "validatePostingDate" (input flPostDate:input-value,
                                 output std-lo).
  
  if not std-lo
   then
    do:
      message "Post date must be within an open period."
          view-as alert-box error buttons ok.      
      return false.
    end.
  
  publish "validateClosedPeriodDate" (input flInvoiceDate:input-value,
                                      output std-lo).
  
  if std-lo
   then
    do:
      message "Invoice date must be within the closed period."
          view-as alert-box error buttons ok.      
      return false.
    end.
  
  publish "validateClosedPeriodDate" (input flReceiptDate:input-value,
                                      output std-lo).
  
  if std-lo
   then
    do:
      message "Payment receipt date must be within the closed period."
          view-as alert-box error buttons ok.      
      return false.
    end.
  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

