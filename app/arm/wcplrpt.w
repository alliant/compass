&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

File: 

Description: 

Input Parameters:
<none>

Output Parameters:
<none>

Author: 

Created:

Modified: 
Date Name Comments
07/23/2019 

------------------------------------------------------------------------*/
/* This .W file was created with the Progress AppBuilder. */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
by this procedure. This is a good default which assures
that this procedure's triggers and internal procedures 
will execute in this procedure's storage, and that proper
cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

define input parameter cRptType as character no-undo.
/* ************************** Definitions ************************* */
{tt/cpl.i}
{tt/agent.i}
{lib/std-def.i}
{lib/ar-def.i}

{lib/winlaunch.i}

define variable hSelectionAgent       as handle    no-undo.
define variable hTextAgent            as handle    no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES cpl

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData cpl.stat cpl.cplID cpl.issueDate cpl.fileNumber cpl.formID cpl.grossPremium cpl.netPremium cpl.retention   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH cpl
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH cpl.
&Scoped-define TABLES-IN-QUERY-brwData cpl
&Scoped-define FIRST-TABLE-IN-QUERY-brwData cpl


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bExport bRefresh cbState fIssueStartDate ~
fissueEndDate brwData fFileNumber RECT-1 RECT-2 
&Scoped-Define DISPLAYED-OBJECTS cbState fIssueStartDate tStatus ~
fissueEndDate fFileNumber 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get CPL".

DEFINE VARIABLE cbAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 49 BY 1 NO-UNDO.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE fFileNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE fissueEndDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "To" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE fIssueStartDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Issued" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE tStatus AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 142.4 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 92 BY 3.1.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 9.4 BY 3.1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      cpl SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      cpl.stat label "S" format "x(2)"      
 cpl.cplID label "CPL" format "x(15)"
 cpl.issueDate label "Issued"

 cpl.fileNumber label "File" format "x(20)"
 
 cpl.formID label "Form" format "x(25)"
 cpl.grossPremium column-label "Gross!Premium"
 cpl.netPremium column-label "Net!Premium"
 cpl.retention label "Retention"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 141.4 BY 11.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 1.91 COL 94.6 WIDGET-ID 2 NO-TAB-STOP 
     bRefresh AT ROW 1.91 COL 85.6 WIDGET-ID 4 NO-TAB-STOP 
     cbState AT ROW 1.76 COL 5.8 WIDGET-ID 38
     fIssueStartDate AT ROW 1.76 COL 67.6 COLON-ALIGNED WIDGET-ID 22
     tStatus AT ROW 16.33 COL 1.6 NO-LABEL WIDGET-ID 12 NO-TAB-STOP 
     fissueEndDate AT ROW 2.81 COL 67.6 COLON-ALIGNED WIDGET-ID 24
     brwData AT ROW 4.38 COL 2 WIDGET-ID 200
     cbAgent AT ROW 2.86 COL 5.2 WIDGET-ID 40
     fFileNumber AT ROW 1.76 COL 38.6 COLON-ALIGNED WIDGET-ID 44
     "Parameters" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 1 COL 2.6 WIDGET-ID 48
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1 COL 94.2 WIDGET-ID 54
     RECT-1 AT ROW 1.24 COL 2 WIDGET-ID 46
     RECT-2 AT ROW 1.24 COL 93.6 WIDGET-ID 50
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 170.2 BY 16.91 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "CPL Report"
         HEIGHT             = 16.43
         WIDTH              = 143.4
         MAX-HEIGHT         = 16.91
         MAX-WIDTH          = 170.2
         VIRTUAL-HEIGHT     = 16.91
         VIRTUAL-WIDTH      = 170.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData fissueEndDate fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR COMBO-BOX cbAgent IN FRAME fMain
   NO-DISPLAY NO-ENABLE ALIGN-L                                         */
/* SETTINGS FOR COMBO-BOX cbState IN FRAME fMain
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN tStatus IN FRAME fMain
   NO-ENABLE ALIGN-L                                                    */
ASSIGN 
       tStatus:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH cpl.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* CPL Report */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* CPL Report */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* CPL Report */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Go */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState C-Win
ON VALUE-CHANGED OF cbState IN FRAME fMain /* State */
DO:  
   run agentComboHide(true). 
   run agentComboHide(false).   
   run agentComboClear.        
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fFileNumber
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fFileNumber C-Win
ON LEAVE OF fFileNumber IN FRAME fMain /* File */
DO:
  if self:modified 
   then tStatus:screen-value in frame fMain = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fissueEndDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fissueEndDate C-Win
ON LEAVE OF fissueEndDate IN FRAME fMain /* To */
DO:
 if self:modified 
  then tStatus:screen-value in frame fMain = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fIssueStartDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fIssueStartDate C-Win
ON LEAVE OF fIssueStartDate IN FRAME fMain /* Issued */
DO:
 if self:modified 
  then tStatus:screen-value in frame fMain = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

if cRptType = {&UnrepCPLInRepFiles} 
 then
  {&window-name}:title = "Unreported CPL In Reported Files".
else if cRptType = {&CPLForUnrepFiles} 
 then
  {&window-name}:title = "CPL For Unreported Files".  
{lib/win-main.i}
{lib/brw-main.i}

assign 
  {&window-name}:min-height-pixels = {&window-name}:height-pixels
  {&window-name}:min-width-pixels  = {&window-name}:width-pixels
  {&window-name}:max-height-pixels = session:height-pixels
  {&window-name}:max-width-pixels  = session:width-pixels
  .

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bRefresh:load-image("images/completed.bmp").
bExport:load-image("images/excel.bmp").

/* Get the selected states from the config settings. */
publish "GetSearchStates" (output std-ch).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  {&window-name}:window-state = window-minimized.  
   
  RUN enable_UI.
  
  assign
    cbState:list-item-pairs      = {&ALL} + "," + {&ALL} + "," + std-ch
    cbState:screen-value         = {&ALL}    
    fIssueStartDate:screen-value = string(date(1,1,year(today)))
    fIssueEndDate:screen-value   = string(today)
    .  
    
  /* create the agent combo */
  {lib/get-agent-list.i &combo=cbAgent &state=cbState &addAll=false &setEnable=true} 
          
  on entry anywhere
    do:  
      assign
        hSelectionAgent = GetWidgetByName(cbAgent   :frame in frame {&frame-name}, "cbAgentAgentSelection")
        hTextAgent      = GetWidgetByName(cbAgent   :frame in frame {&frame-name}, "cbAgentAgentText")
        .           
      if valid-handle(hSelectionAgent) and valid-handle(hTextAgent)
       then
        if not self:name = "cbAgentAgentText" and 
           not self:name = "cbAgentAgentSelection"
         then
           hSelectionAgent:visible = false.
         else if hSelectionAgent:list-item-pairs > "" and 
                 not hSelectionAgent:visible
          then                        
           hSelectionAgent:visible = true.                       
    end.
      
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE checkValidation C-Win 
PROCEDURE checkValidation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical   no-undo.
  define output parameter std-ch as character no-undo.
  
  do with frame fMain:
  end.
  
  if cbAgent:input-value = "" 
   then
    do:
      std-ch = "Agent ID can not be blank.".        
      return.
    end.
    
  if fIssueStartDate:input-value = ?
   then
    do:
      std-ch = "Issue start date cannot be blank.".        
      return.
    end. 
    
  if fIssueEndDate:input-value = ?
   then
    do:
      std-ch = "Issue end date can not be blank.".        
      return.
    end.  

  if fIssueStartDate:input-value > fIssueEndDate:input-value
   then
    do:
      std-ch = "End date can not be before start date.".        
      return.
    end.  
    
  std-lo = true.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbState fIssueStartDate tStatus fissueEndDate fFileNumber 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bExport bRefresh cbState fIssueStartDate fissueEndDate brwData 
         fFileNumber RECT-1 RECT-2 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def buffer x-cpl for cpl.
  def var exportFilename as char no-undo.
  def var doSave as logical no-undo.

  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
       view-as alert-box.
      return.
    end.

  publish "GetReportDir" (output std-ch).

  system-dialog get-file exportFilename
   filters "CSV Files" "*.csv"
   initial-dir std-ch
   ask-overwrite
   create-test-file
   default-extension ".csv"
   use-filename
   save-as
   update doSave.

  if not doSave 
   then return.

  std-in = 0.
  output to value(exportFilename).
  export delimiter ","
    "CPL"
    "Issued"
    "File"
    "Form"
    "Gross"
    "Net"
    "Retained"
    .

  for each x-cpl:
   export delimiter "," 
     x-cpl.cplID
     x-cpl.issueDate
     x-cpl.fileNumber
     x-cpl.formID 
     x-cpl.grossPremium
     x-cpl.netPremium
     x-cpl.retention
    .
   std-in = std-in + 1.
  end.
  output close.

  RUN ShellExecuteA in this-procedure (0,
                                      "open",
                                      exportFilename,
                                      "",
                                      "",
                                      1,
                                      OUTPUT std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def buffer cpl for cpl.
  def var tFile as char no-undo.
 
  run checkValidation in this-procedure (output std-lo, 
                                         output std-ch).
  if not std-lo
   then
    do:
      message std-ch
        view-as alert-box.
      return.
    end.
 
  close query brwData.
  empty temp-table cpl.
    
  run server/getcplrpt.p (input cRptType,
                          input cbAgent :input-value in frame fMain ,
                          input fFileNumber:input-value,                                               
                          input fIssueStartDate:input-value,
                          input fIssueEndDate:input-value,
                          output table cpl,
                          output std-lo,
                          output std-ch).
        
  run sortData ("cplID").

  tStatus:screen-value in frame fMain = string(query brwData:num-results, ">>,>>>,>>9") 
                                                + " CPLs for agent " 
                                                + cbAgent:input-value in frame fMain.
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  {lib/brw-sortData.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  frame fMain:width-pixels = {&window-name}:width-pixels.
  frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
  frame fMain:height-pixels = {&window-name}:height-pixels.
  frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

  /* fMain components */
  brwData:width-pixels = frame fmain:width-pixels - 10.
  brwData:height-pixels = frame fMain:height-pixels - 75.
  tStatus:Y in frame fMain = frame fMain:height-pixels - 23.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

