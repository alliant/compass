&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wtopicexpense.w

  Description: Window of  TopicExpense

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: S Chandu

  Created: 05.17.2024
  Modified:
  Date         Name         Description

------------------------------------------------------------------------*/
create widget-pool.

{lib/winshowscrollbars.i}
{lib/set-button-def.i}
{lib/std-def.i}
{lib/ar-def.i}


/* Temp-table definitions */
{tt/statesource.i}
{tt/statesource.i &tableAlias="tempstatesource"}


/* Local Variable Definitions */
define variable cTopic             as character  no-undo.
define variable cStateID           as character  no-undo.
define variable lApplySearchString as logical    no-undo.
define variable cSearchString      as character  no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwStateSource

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES statesource

/* Definitions for BROWSE brwStateSource                                */
&Scoped-define FIELDS-IN-QUERY-brwStateSource statesource.stateID statesource.sourceType statesource.revenueType   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwStateSource   
&Scoped-define SELF-NAME brwStateSource
&Scoped-define QUERY-STRING-brwStateSource for each statesource
&Scoped-define OPEN-QUERY-brwStateSource open query {&SELF-NAME} for each statesource.
&Scoped-define TABLES-IN-QUERY-brwStateSource statesource
&Scoped-define FIRST-TABLE-IN-QUERY-brwStateSource statesource


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwStateSource}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bExport RECT-66 brwStateSource bRefresh ~
bDelete bEdit bNew 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete".

DEFINE BUTTON bEdit  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Edit".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh".

DEFINE RECTANGLE RECT-66
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 37 BY 2.19.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwStateSource FOR 
      statesource SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwStateSource
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwStateSource C-Win _FREEFORM
  QUERY brwStateSource DISPLAY
      statesource.stateID label "State ID" width 20 format "x(2)"
statesource.sourceType label "Source Type" width 38 format "x(50)"
statesource.revenueType label "Revenue Type" format "x(50)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 112 BY 10.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 1.62 COL 9.8 WIDGET-ID 2 NO-TAB-STOP 
     brwStateSource AT ROW 3.62 COL 2 WIDGET-ID 300
     bRefresh AT ROW 1.62 COL 2.8 WIDGET-ID 4 NO-TAB-STOP 
     bDelete AT ROW 1.62 COL 30.8 WIDGET-ID 14 NO-TAB-STOP 
     bEdit AT ROW 1.62 COL 23.8 WIDGET-ID 8 NO-TAB-STOP 
     bNew AT ROW 1.62 COL 16.8 WIDGET-ID 6 NO-TAB-STOP 
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1 COL 3.4 WIDGET-ID 60
     RECT-66 AT ROW 1.38 COL 2 WIDGET-ID 58
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 114.2 BY 13.86 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "State Source"
         HEIGHT             = 13.62
         WIDTH              = 113.4
         MAX-HEIGHT         = 37.1
         MAX-WIDTH          = 307.2
         VIRTUAL-HEIGHT     = 37.1
         VIRTUAL-WIDTH      = 307.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwStateSource RECT-66 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwStateSource:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwStateSource:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwStateSource
/* Query rebuild information for BROWSE brwStateSource
     _START_FREEFORM
open query {&SELF-NAME} for each statesource.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwStateSource */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* State Source */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* State Source */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* State Source */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME fMain /* Delete */
DO:
  run deleteStateSource in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEdit C-Win
ON CHOOSE OF bEdit IN FRAME fMain /* Edit */
do:
  run modifystatesource in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
do:
  run newstatesource in this-procedure.  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
do:  
  /* refresh the statesource table in apmdatasrv with the database then call 
     getData to update the browser with updated table statesource */
  run refreshData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwStateSource
&Scoped-define SELF-NAME brwStateSource
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwStateSource C-Win
ON DEFAULT-ACTION OF brwStateSource IN FRAME fMain
do:
  apply "choose" to bedit.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwStateSource C-Win
ON ROW-DISPLAY OF brwStateSource IN FRAME fMain
do:
  {lib/brw-rowDisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwStateSource C-Win
ON START-SEARCH OF brwStateSource IN FRAME fMain
do:    
  {lib/brw-startSearch.i} 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

assign
    {&window-name}:min-height-pixels = C-Win:height-pixels
    {&window-name}:min-width-pixels  = C-Win:width-pixels
    {&window-name}:max-height-pixels = session:height-pixels
    {&window-name}:max-width-pixels  = session:width-pixels
    .

assign current-window                = {&window-name} 
       this-procedure:current-window = {&window-name}.

on close of this-procedure 
  run disable_UI.

pause 0 before-hide.

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
   
  {&window-name}:window-state = window-minimized. 
  
  run enable_UI. 

  /* Get data from ardatasrv */
  run getData in this-procedure.
  
  setStatusRecords(query brwstatesource:num-results). 
  
  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.
  
  {lib/set-button.i &b=bRefresh  &label="Reload data"     &image="images/refresh.bmp"   &inactive="images/refresh-i.bmp"}
  {lib/set-button.i &b=bnew      &label="Export to Excel" &image="images/add.bmp"       &inactive="images/add-i.bmp"}
  {lib/set-button.i &b=bExport   &label="Add"             &image="images/excel.bmp"     &inactive="images/excel-i.bmp"}
  {lib/set-button.i &b=bEdit     &label="Edit"            &image="images/update.bmp"    &inactive="images/update-i.bmp"}
  {lib/set-button.i &b=bDelete    &label="Delete"         &image="images/delete.bmp"    &inactive="images/delete-i.bmp"}
  
setButtons().
  
  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteStateSource C-Win 
PROCEDURE deleteStateSource :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if not available statesource 
  then return.

 define variable cType as character no-undo. 
  
 std-lo = false.
 MESSAGE "State Source will be permanently removed. Continue?"
  VIEW-AS ALERT-BOX question BUTTONS Yes-No update std-lo.
 if not std-lo 
  then return.

 if statesource.sourcetype = "Production"
  then
   cType = "P".
   
  publish "deleteStateSource" (statesource.stateId,      
                               cType,   
                               output std-lo,            
                               output std-ch).           
 if not std-lo 
  then 
   do: MESSAGE std-ch
        VIEW-AS ALERT-BOX error BUTTONS OK.
       return.
   end.

 delete statesource.
 
 browse brwStateSource:delete-selected-rows(). 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE bExport RECT-66 brwStateSource bRefresh bDelete bEdit bNew 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define variable htableHandle as handle no-undo. 
  
  if query brwstatesource:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.

  publish "GetReportDir" (output std-ch).   

  htableHandle = temp-table statesource:handle.
 run util/exporttable.p (table-handle htableHandle,
                         "statesource",
                         "for each statesource ",
                         "stateID,sourceType,revenueType",
                         "StateID,sourceType,revenueType",
                         std-ch,
                         "State Source-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                         true,
                         output std-ch,
                         output std-in). 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  do with frame {&frame-name}:
  end.
  
 empty temp-table statesource. 
 publish "getStateSource" ( output table statesource).
                           
 open query brwStateSource preselect each statesource.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyStateSource C-Win 
PROCEDURE modifyStateSource :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end.

  define buffer tempstatesource for tempstatesource.   
  
  empty temp-table tempstatesource.

  if not available statesource 
   then return.
  
  create tempstatesource.
  buffer-copy statesource to tempstatesource.
  
  run dialogstatesource.w (input "Modify",
                           input table tempstatesource,
                           output cStateID,
                           output std-lo).
  if not std-lo 
   then
    return.      
  
  run getData in this-procedure.
  
  find first statesource where statesource.stateID    = cStateID 
                           and statesource.sourcetype = "Production" no-error.

  if available statesource
   then      
    reposition brwStateSource to rowid rowid(statesource) no-error.  

  setStatusCount(query brwStateSource:num-results).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newStateSource C-Win 
PROCEDURE newStateSource :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  define variable lSuccess as logical no-undo.
  define buffer tempstatesource for tempstatesource.
  
  empty temp-table tempstatesource.  
                  
  run dialogstatesource.w (input "New"  ,
                           input table tempstatesource,
                           output cStateID,
                           output lSuccess).
  if not lSuccess 
   then
    return.      
 
  run getData in this-procedure.

  find first statesource where statesource.stateID    = cStateID 
                           and statesource.sourcetype = "Production" no-error.

  if available statesource
   then      
    reposition brwStateSource to rowid rowid(statesource) no-error.  


  setStatusCount(query brwStateSource:num-results).
   
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData C-Win 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  empty temp-table statesource.

  publish "loadStateSource".

  run getData in this-procedure.

  setStatusRecords(query brwStateSource:num-results).   

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  assign
      bExport:sensitive = available statesource
      bedit:sensitive   = available statesource
      bDelete:sensitive = available statesource
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .
  
  {&window-name}:move-to-top().  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame fMain:width-pixels           = {&window-name}:width-pixels
      frame fMain:virtual-width-pixels   = {&window-name}:width-pixels
      frame fMain:height-pixels          = {&window-name}:height-pixels
      frame fMain:virtual-height-pixels  = {&window-name}:height-pixels        
      {&browse-name}:width-pixels        = frame {&frame-name}:width-pixels - 10
      {&browse-name}:height-pixels       = frame {&frame-name}:height-pixels - {&browse-name}:y - 3
      .
  run ShowScrollBars(browse brwStateSource:handle, no, yes).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

