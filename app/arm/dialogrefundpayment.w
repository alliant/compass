&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogAgentArInfo.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}

/* Parameters Definitions ---                                           */
define input  parameter ipiARPmtID        as integer    no-undo.
define input  parameter ipcEntity         as character  no-undo.
define input  parameter ipcDepositRef     as character  no-undo.
define input  parameter ipcCheck          as character  no-undo.
define input  parameter ipdeUnapplied     as decimal    no-undo.
define input  parameter ipdtPmtPostDate   as date       no-undo.
define output parameter opdeRefund        as decimal    no-undo.
define output parameter loSuccess         as logical    no-undo.   

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS flRefundAmt flPostDate edReason Btn_OK ~
Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS flEntity flDepositRef flRef flUnappliedAmt ~
flRefundAmt flPostDate edReason 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validatePostDate Dialog-Frame 
FUNCTION validatePostDate RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Refund" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE edReason AS CHARACTER 
     VIEW-AS EDITOR NO-WORD-WRAP SCROLLBAR-HORIZONTAL SCROLLBAR-VERTICAL
     SIZE 77 BY 4 NO-UNDO.

DEFINE VARIABLE flDepositRef AS CHARACTER FORMAT "X(256)":U 
     LABEL "Deposit Ref." 
     VIEW-AS FILL-IN NATIVE 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE flEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 77 BY 1 NO-UNDO.

DEFINE VARIABLE flPostDate AS DATE FORMAT "99/99/99":U 
     LABEL "Post Date" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE flRef AS CHARACTER FORMAT "X(256)":U 
     LABEL "Check/Reference" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE flRefundAmt AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "Refund" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE flUnappliedAmt AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "Unappiled" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 21 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     flEntity AT ROW 1.57 COL 21 COLON-ALIGNED WIDGET-ID 28
     flDepositRef AT ROW 2.76 COL 21 COLON-ALIGNED WIDGET-ID 30
     flRef AT ROW 3.95 COL 21 COLON-ALIGNED WIDGET-ID 32
     flUnappliedAmt AT ROW 5.14 COL 21 COLON-ALIGNED WIDGET-ID 10
     flRefundAmt AT ROW 6.38 COL 21 COLON-ALIGNED WIDGET-ID 24
     flPostDate AT ROW 7.57 COL 21 COLON-ALIGNED WIDGET-ID 34
     edReason AT ROW 8.81 COL 23 NO-LABEL WIDGET-ID 36
     Btn_OK AT ROW 13.48 COL 36.2
     Btn_Cancel AT ROW 13.48 COL 52.8
     "Reason:" VIEW-AS TEXT
          SIZE 8.2 BY .71 AT ROW 8.91 COL 14 WIDGET-ID 38
     SPACE(80.99) SKIP(5.42)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Payment Refund"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       edReason:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN flDepositRef IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flEntity IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flRef IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flUnappliedAmt IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Payment Refund */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Refund */
DO: 
  if not validatePostDate()
   then
    return no-apply.
    
  run refundPayment in this-procedure.
  if not loSuccess
   then
    return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flDepositRef
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flDepositRef Dialog-Frame
ON VALUE-CHANGED OF flDepositRef IN FRAME Dialog-Frame /* Deposit Ref. */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flEntity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flEntity Dialog-Frame
ON VALUE-CHANGED OF flEntity IN FRAME Dialog-Frame /* Agent */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flPostDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flPostDate Dialog-Frame
ON VALUE-CHANGED OF flPostDate IN FRAME Dialog-Frame /* Post Date */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flRef
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flRef Dialog-Frame
ON VALUE-CHANGED OF flRef IN FRAME Dialog-Frame /* Check/Reference */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flRefundAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flRefundAmt Dialog-Frame
ON VALUE-CHANGED OF flRefundAmt IN FRAME Dialog-Frame /* Refund */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flUnappliedAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flUnappliedAmt Dialog-Frame
ON VALUE-CHANGED OF flUnappliedAmt IN FRAME Dialog-Frame /* Unappiled */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
    
  assign
    flEntity:screen-value       = ipcEntity
    flDepositRef:screen-value   = ipcDepositRef
    flRef:screen-value          = ipcCheck
    flUnappliedAmt:screen-value = string(ipdeUnapplied)    
    flRefundAmt:screen-value    = string(ipdeUnapplied) 
    flPostDate:screen-value     = string(today)
    no-error.    
  
  apply 'value-changed' to flRefundAmt.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableOK Dialog-Frame 
PROCEDURE enableDisableOK :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  Btn_OK:sensitive =   (flUnappliedAmt:input-value ge flRefundAmt:input-value and
                        flRefundAmt:input-value > 0)  and 
                        flPostDate:input-value <> ?
                        no-error
                        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flEntity flDepositRef flRef flUnappliedAmt flRefundAmt flPostDate 
          edReason 
      WITH FRAME Dialog-Frame.
  ENABLE flRefundAmt flPostDate edReason Btn_OK Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refundPayment Dialog-Frame 
PROCEDURE refundPayment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  run server/refundPayment.p(input ipiArPmtID,
                             input flRefundAmt:input-value,
                             input edReason:input-value,
                             input flPostDate:input-value,
                             output std-lo,
                             output std-ch)
                             .
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
    
  opdeRefund = flRefundAmt:input-value.
  
  loSuccess = std-lo.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validatePostDate Dialog-Frame 
FUNCTION validatePostDate RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if flPostDate:input-value = ?
   then
    do:
      message "Refund post date cannot be blank."
          view-as alert-box info buttons ok.
      apply 'entry' to flPostDate.  
      return false.      
    end.
  
  if flPostDate:input-value > today
   then
    do:
      message "Refund post date cannot be in the future."
          view-as alert-box info buttons ok.
      apply 'entry' to flPostDate.     
      return false.
    end.
    
  publish "validatePostingDate" (input flPostDate:input-value,
                                 output std-lo).
  
  if not std-lo
   then
    do:
      message "Refund post date must be within an open period."
          view-as alert-box info buttons ok.
      apply 'entry' to flPostDate.     
      return false.
    end.
  
   
  if flPostDate:input-value < ipdtPmtPostDate
   then
    do:
      message "Refund post date cannot be prior to the payment post date."
          view-as alert-box info buttons ok.
      apply 'entry' to flPostDate.  
      return false.
    end.
    
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

