&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogmodifypayment.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Vikas Jain

  Created:07/23/19
  
  @Modified :
  Date           Name            Description 
  07/24/2020     AG              Modified to use armpt.tranDate instead of arpmt.postDate
  01/14/2021     Shefali         Modified to validate receipt date while edit the payment.
  05/09/2024     SRK             Modified to related to production file base
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{tt/arpmt.i &tablealias=ttarpmt}
{tt/agent.i}

/* Parameters Definitions                                           */
define input parameter ipcAction as character no-undo.
define input-output  parameter table for ttarpmt. 
define output parameter oplSuccess as logical no-undo.

/* Standard library files */
{lib/ar-def.i}
{lib/std-def.i}

/* Local variable definitions */
define variable cStateID       as character no-undo. 
define variable cFileBased    as character no-undo.
define variable cDepositRef    as character no-undo.
define variable cRevenueType   as character no-undo.
define variable cCheckNum      as character no-undo.
define variable cFileNum       as character no-undo.
define variable deCheckAmt     as decimal   no-undo.
define variable dtCheckDate    as datetime  no-undo.
define variable dtReceiptDate  as datetime  no-undo.
define variable cDescription   as character no-undo.
define variable deRemainingAmt as decimal   no-undo.
define variable cCode          as character no-undo.
define variable ctranstype     as character no-undo.

define temp-table ttAction
  field action      as character
  field createdBy   as character
  field createdDate as character
  field ledgerID    as character.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwAction

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttAction

/* Definitions for BROWSE brwAction                                     */
&Scoped-define FIELDS-IN-QUERY-brwAction ttAction.Action ttAction.createdBy ttAction.CreatedDate ttAction.ledgerID   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAction   
&Scoped-define SELF-NAME brwAction
&Scoped-define QUERY-STRING-brwAction FOR EACH ttAction
&Scoped-define OPEN-QUERY-brwAction OPEN QUERY {&SELF-NAME} FOR EACH ttAction.
&Scoped-define TABLES-IN-QUERY-brwAction ttAction
&Scoped-define FIRST-TABLE-IN-QUERY-brwAction ttAction


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwAction}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS flDepositRef flRefNum flCheckDate ~
flFileNumber flReceiptDate flAmt bSysCode edDescription Btn_Cancel ~
flRemainingAmt flRefundAmt rsapplytype 
&Scoped-Define DISPLAYED-OBJECTS flDepositRef flRefNum flCheckDate ~
flFileNumber flReceiptDate flAmt edDescription fRefNumMand fReceipDateMand ~
fAmountMand flEntity cbEntityType flRemainingAmt tRevType flRefundAmt ~
flEntityID rsapplytype 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validateReceiptDate Dialog-Frame 
FUNCTION validateReceiptDate RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bSysCode 
     LABEL "sysCode" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cbEntityType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Received From" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Agent","A",
                     "Organization","O"
     DROP-DOWN-LIST
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE edDescription AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 101 BY 2.81 NO-UNDO.

DEFINE VARIABLE fAmountMand AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3.6 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE flAmt AS DECIMAL FORMAT "Z,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Amount" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE flCheckDate AS DATE FORMAT "99/99/99":U 
     LABEL "Check Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 12.4 BY 1 NO-UNDO.

DEFINE VARIABLE flDepositRef AS CHARACTER FORMAT "X(256)":U 
     LABEL "Deposit Ref" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23.2 BY 1 NO-UNDO.

DEFINE VARIABLE flEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 55.4 BY 1 NO-UNDO.

DEFINE VARIABLE flEntityID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE flFileNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "File Number" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23.2 BY 1 NO-UNDO.

DEFINE VARIABLE flReceiptDate AS DATE FORMAT "99/99/99":U 
     LABEL "Receipt Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 12.4 BY 1 NO-UNDO.

DEFINE VARIABLE flRefNum AS CHARACTER FORMAT "X(256)":U 
     LABEL "Check/Reference" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE flRefundAmt AS DECIMAL FORMAT "Z,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Refund" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE flRemainingAmt AS DECIMAL FORMAT "Z,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Unapplied" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE fReceipDateMand AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3.6 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fRefNumMand AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3.6 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE tRevType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Revenue Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE rsapplytype AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Premium", "F",
"Production", "P",
"None", "N"
     SIZE 37.8 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAction FOR 
      ttAction SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAction Dialog-Frame _FREEFORM
  QUERY brwAction DISPLAY
      ttAction.Action               label  "Action"    format "x(15)"     
ttAction.createdBy              label  "By"              format "x(50)" 
ttAction.CreatedDate            label  "Date"            format "x(12)" 
ttAction.ledgerID               label  "Ledger ID"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-AUTO-VALIDATE NO-ROW-MARKERS NO-COLUMN-SCROLLING SEPARATORS NO-SCROLLBAR-VERTICAL SIZE 101 BY 3.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     brwAction AT ROW 13.29 COL 5.6 WIDGET-ID 200
     flDepositRef AT ROW 1.33 COL 81.4 COLON-ALIGNED WIDGET-ID 104
     flRefNum AT ROW 3.62 COL 21.8 COLON-ALIGNED WIDGET-ID 42
     flCheckDate AT ROW 4.76 COL 21.8 COLON-ALIGNED WIDGET-ID 36
     flFileNumber AT ROW 4.76 COL 81.4 COLON-ALIGNED WIDGET-ID 70
     flReceiptDate AT ROW 5.91 COL 21.8 COLON-ALIGNED WIDGET-ID 116
     flAmt AT ROW 7.05 COL 41.8 RIGHT-ALIGNED WIDGET-ID 2
     bSysCode AT ROW 8.1 COL 47 WIDGET-ID 252
     edDescription AT ROW 10.1 COL 5.6 NO-LABEL WIDGET-ID 26
     Btn_OK AT ROW 17.62 COL 40.2
     Btn_Cancel AT ROW 17.62 COL 56.8
     fRefNumMand AT ROW 3.95 COL 41.2 COLON-ALIGNED NO-LABEL WIDGET-ID 272
     fReceipDateMand AT ROW 6.24 COL 34.4 COLON-ALIGNED NO-LABEL WIDGET-ID 274
     fAmountMand AT ROW 7.38 COL 41.2 COLON-ALIGNED NO-LABEL WIDGET-ID 276
     flEntity AT ROW 2.48 COL 49.2 COLON-ALIGNED WIDGET-ID 102 NO-TAB-STOP 
     cbEntityType AT ROW 1.33 COL 21.8 COLON-ALIGNED WIDGET-ID 86 NO-TAB-STOP 
     flRemainingAmt AT ROW 7.05 COL 76.4 RIGHT-ALIGNED WIDGET-ID 90 NO-TAB-STOP 
     tRevType AT ROW 8.19 COL 21.8 COLON-ALIGNED WIDGET-ID 64 NO-TAB-STOP 
     flRefundAmt AT ROW 7.05 COL 105.6 RIGHT-ALIGNED WIDGET-ID 298 NO-TAB-STOP 
     flEntityID AT ROW 2.48 COL 21.8 COLON-ALIGNED WIDGET-ID 300
     rsapplytype AT ROW 3.61 COL 70.2 NO-LABEL WIDGET-ID 430
     "Notes:" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 9.48 COL 5.6 WIDGET-ID 28
     "Apply to File for:" VIEW-AS TEXT
          SIZE 16.6 BY 1 AT ROW 3.61 COL 53.6 WIDGET-ID 434
     SPACE(40.20) SKIP(14.38)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Edit Payment"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwAction 1 Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BROWSE brwAction IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbEntityType IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       edDescription:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN fAmountMand IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flAmt IN FRAME Dialog-Frame
   ALIGN-R                                                              */
/* SETTINGS FOR FILL-IN flEntity IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flEntityID IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flRefundAmt IN FRAME Dialog-Frame
   ALIGN-R                                                              */
ASSIGN 
       flRefundAmt:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN flRemainingAmt IN FRAME Dialog-Frame
   ALIGN-R                                                              */
ASSIGN 
       flRemainingAmt:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fReceipDateMand IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fRefNumMand IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tRevType IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       tRevType:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAction
/* Query rebuild information for BROWSE brwAction
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH ttAction.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAction */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Edit Payment */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSysCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSysCode Dialog-Frame
ON CHOOSE OF bSysCode IN FRAME Dialog-Frame /* sysCode */
DO:
  run dialogrevenuelookup.w(input tRevType:screen-value,
                            input cStateID,
                            output cCode,                            
                            output std-lo).
  if not std-lo 
   then
    return no-apply.
  
  tRevType:screen-value = cCode.
      
  run enableDisableSave in this-procedure.    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel Dialog-Frame
ON CHOOSE OF Btn_Cancel IN FRAME Dialog-Frame /* Cancel */
DO:
  oplSuccess = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Save */
DO: 
  if not validateReceiptDate()
   then
    return no-apply.
    
  if rsapplytype:screen-value = "N" and flFileNumber:screen-value <> ""
   then
    do:
       message "Either select Premium or Production in '" + "Apply to File for"  + "' option."
           view-as alert-box information buttons ok.
       apply "entry" to flFileNumber. 
       return no-apply.
    end.    
    
  run saveArPayment in this-procedure.
      
  /* If there was any error then do not close the dialog. */
  if not oplSuccess
   then
    return no-apply.      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbEntityType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbEntityType Dialog-Frame
ON VALUE-CHANGED OF cbEntityType IN FRAME Dialog-Frame /* Received From */
DO:
  if not available ttarpmt
   then
    return.
    
  if ttarpmt.Entity = {&Organization}
   then
    flEntity:screen-value = ttarpmt.Entity.   
   else
    do:
      publish "getAgentName" (input ttarpmt.EntityID, 
                              output std-ch,
                              output std-lo).
      flEntity:screen-value = std-ch.
      
      /* Return StateID to pass stateID into revenue lookup */
      publish "getAgentStateID" (input ttarpmt.EntityID,
                                 output cStateID,
                                 output std-lo).
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edDescription
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edDescription Dialog-Frame
ON VALUE-CHANGED OF edDescription IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAmt Dialog-Frame
ON VALUE-CHANGED OF flAmt IN FRAME Dialog-Frame /* Amount */
DO:
  flRemainingAmt:screen-value = flAmt:input-value.
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flCheckDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flCheckDate Dialog-Frame
ON VALUE-CHANGED OF flCheckDate IN FRAME Dialog-Frame /* Check Date */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flDepositRef
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flDepositRef Dialog-Frame
ON VALUE-CHANGED OF flDepositRef IN FRAME Dialog-Frame /* Deposit Ref */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flEntity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flEntity Dialog-Frame
ON VALUE-CHANGED OF flEntity IN FRAME Dialog-Frame /* Name */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flFileNumber
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flFileNumber Dialog-Frame
ON VALUE-CHANGED OF flFileNumber IN FRAME Dialog-Frame /* File Number */
DO:
  rsapplytype:sensitive = (flFileNumber:screen-value <> ""). 
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flReceiptDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flReceiptDate Dialog-Frame
ON VALUE-CHANGED OF flReceiptDate IN FRAME Dialog-Frame /* Receipt Date */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flRefNum
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flRefNum Dialog-Frame
ON VALUE-CHANGED OF flRefNum IN FRAME Dialog-Frame /* Check/Reference */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rsapplytype
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rsapplytype Dialog-Frame
ON VALUE-CHANGED OF rsapplytype IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tRevType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tRevType Dialog-Frame
ON VALUE-CHANGED OF tRevType IN FRAME Dialog-Frame /* Revenue Type */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAction
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bSysCode  :load-image("images/s-magnifier.bmp").
bSysCode  :load-image-insensitive("images/s-magnifier-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  Frame Dialog-Frame:Title = (if ipcAction = {&Modify} then "Edit Payment" 
                               else if ipcAction = {&ModifyPosted} then "Edit Payment Note" 
                               else "View Payment").
  run displayData in this-procedure.  
  
  run setMandatoryMark in this-procedure.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.   
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
   
  find first ttarpmt no-error.
  if available ttarpmt 
   then
    do:
      assign
          flentityID:screen-value     = ttarpmt.EntityID
          flEntity:screen-value       = ttarpmt.entityname
          cbEntityType:screen-value   = ttarpmt.Entity        
          flDepositRef:screen-value   = ttarpmt.depositRef
          tRevType:screen-value       = ttarpmt.revenueType      
          flCheckDate:screen-value    = string(ttarpmt.checkdate)      
          flRefNum:screen-value       = ttarpmt.checknum
          flAmt:screen-value          = string(ttarpmt.checkamt)  
          flReceiptDate:screen-value  = string(ttarpmt.receiptdate)      
          rsapplytype:screen-value    = if ttarpmt.transType = '' then  "N" else  ttarpmt.transType  
          flFileNumber:screen-value   = ttarpmt.filenumber                
          edDescription:screen-value  = ttarpmt.description      
          flRemainingAmt:screen-value = string(ttarpmt.remainingAmt)
          flRefundAmt:screen-value    = string(abs(ttarpmt.refundAmt))
          no-error.
        
      /* Keep the copy of initial values. Required in enableDisableSave. */ 
      assign                 
          cDepositRef   = ttarpmt.depositRef
          cRevenueType  = ttarpmt.revenueType
          cCheckNum     = ttarpmt.CheckNum
          deCheckAmt    = ttarpmt.CheckAmt    
          dtCheckDate   = ttarpmt.CheckDate    
          dtReceiptDate = ttarpmt.ReceiptDate    
          cFileNum      = ttarpmt.FileNumber
          cDescription  = ttarpmt.Description
          cFileBased    = ttarpmt.transType 
          no-error.    
      /* ttAction table to show details of created, posted and voided in dialog*/
      create ttAction.
      assign
          ttAction.action       = "Created"
          ttAction.createdBy    = ttarpmt.username
          ttAction.CreatedDate  = string(ttarpmt.createdDate)
          ttAction.ledgerID     = "".
      create ttAction.
      assign
          ttAction.action       = "Posted"
          ttAction.createdBy    = ttarpmt.postbyname
          ttAction.CreatedDate  = if (ttarpmt.transDate = ?) then "" else string(ttarpmt.transDate)
          ttAction.ledgerID     = if (ttarpmt.ledgerID = 0 ) then "" else string(ttarpmt.ledgerID).
      create ttAction.
      assign
          ttAction.action       = "Void"
          ttAction.createdBy    = ttarpmt.voidbyusername
          ttAction.CreatedDate  = if (ttarpmt.voidDate = ?) then "" else string(ttarpmt.voidDate)
          ttAction.ledgerID     = if (ttarpmt.voidLedgerID = 0) then "" else string(ttarpmt.voidLedgerID).
       
      open query brwAction for each ttAction.
      brwAction:Deselect-focused-row().
        
    end. 
  
  run setWidgetState in this-procedure.
  apply 'value-changed' to cbEntitytype.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  Btn_OK:sensitive = (((flEntity:input-value <> "")               and                        
                       flRefNum:input-value <> ""                 and
                       flAmt:input-value <> 0                     and
                       flReceiptDate:input-value <> ?)            and                      
                      (cFileBased <> rsapplytype:screen-value    or
                       cDepositRef <> flDepositRef:input-value    or
                       cRevenueType <> tRevType:input-value       or
                       cFileNum <> flFileNumber:input-value       or
                       cCheckNum <> flRefNum:input-value          or
                       deCheckAmt <> flAmt:input-value            or
                       dtCheckDate <> flCheckDate:input-value     or
                       dtReceiptDate <> flReceiptDate:input-value or
                       cDescription  <> edDescription:input-value))
                     no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flDepositRef flRefNum flCheckDate flFileNumber flReceiptDate flAmt 
          edDescription fRefNumMand fReceipDateMand fAmountMand flEntity 
          cbEntityType flRemainingAmt tRevType flRefundAmt flEntityID 
          rsapplytype 
      WITH FRAME Dialog-Frame.
  ENABLE flDepositRef flRefNum flCheckDate flFileNumber flReceiptDate flAmt 
         bSysCode edDescription Btn_Cancel flRemainingAmt flRefundAmt 
         rsapplytype 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveArPayment Dialog-Frame 
PROCEDURE saveArPayment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lupdate            as logical   no-undo.
  define variable lApplyToFile       as logical   no-undo.
  define variable lApplyToProduction as logical   no-undo.
  define variable cFileExist         as character no-undo.
  
  do with frame {&frame-name}:
  end.
  
  find first ttarpmt no-error.
  if not available ttarpmt
   then 
    return.
    
  assign    
    ttarpmt.depositRef   = flDepositRef:input-value
    ttarpmt.revenuetype  = tRevType:input-value
    ttarpmt.checknum     = flRefNum:input-value
    ttarpmt.transType    = rsapplytype:screen-value
    ttarpmt.filenum      = flfileNumber:input-value
    ttarpmt.checkamt     = flAmt:input-value  
    ttarpmt.remainingAmt = flRemainingAmt:input-value
    ttarpmt.checkdate    = flCheckDate:input-value    
    ttarpmt.receiptdate  = flReceiptDate:input-value    
    ttarpmt.description  = edDescription:input-value
    .
  
  /* client Server Call */
  run server/modifypayment.p (input table ttarpmt,
                              input true, /* Warning Msg */
                              output std-in,
                              output std-lo,
                              output lApplyToFile,
                              output lApplyToProduction,
                              output cFileExist,
                              output oplSuccess,
                              output std-ch).   
  if not oplSuccess and
     std-ch = {&DuplicateCheck}
   then
    do:
      if cbEntityType:screen-value = {&Agent}
       then    
        message "Unposted payment record for agent: " flEntityID:screen-value " and check number: " flRefNum:input-value " already exists. "
                "Do you still want to continue?"
            view-as alert-box question buttons yes-no update lupdate.
       else
        message "Unposted payment record for organisation with check number: " flRefNum:input-value " already exists. "
                "Do you still want to continue?"
            view-as alert-box question buttons yes-no update lupdate.
            
      if not lUpdate
       then return.
   
      /* client Server Call */
      run server/modifypayment.p (input table ttarpmt,
                                  input false, /* Warning Msg */
                                  output std-in,
                                  output std-lo,
                                  output lApplyToFile,
                                  output lApplyToProduction,
                                  output cFileExist,
                                  output oplSuccess,
                                  output std-ch).                                                                     
    end.
  
  if not oplSuccess
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
    
  assign
      ttarpmt.depositID     = std-in
      ttarpmt.depositPosted = std-lo
      ttarpmt.fileExist     = cFileExist      .
      
      if ttArPmt.transType = {&Production}  and lApplyToProduction
          then
           ttArPmt.transType  =  {&Production}  .
          else if  lApplyToFile 
           then
            ttArPmt.transType =  {&File}.  
          else
            ttArPmt.transType  = {&None}. 
          

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setMandatoryMark Dialog-Frame 
PROCEDURE setMandatoryMark :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  assign
      fRefNumMand:screen-value     = {&Mandatory}
      fReceipDateMand:screen-value = {&Mandatory}
      fAmountMand:screen-value     = {&Mandatory}  
      .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState Dialog-Frame 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  case ipcAction:
    when {&modify}
     then 
      assign
          flDepositRef:read-only  = false 
          flRefNum:read-only      = false
          flCheckDate:read-only   = false 
          flReceiptDate:read-only = false
          flAmt:read-only         = false   
          rsapplytype:sensitive   = (flFileNumber:screen-value <> "")
          flFileNumber:read-only  = false
          bSysCode:sensitive      = true  
          edDescription:read-only = false 
         .   
    when {&view}
     then
      assign
          flDepositRef:read-only  = true
          flRefNum:read-only      = true
          flCheckDate:read-only   = true
          flReceiptDate:read-only = true
          flAmt:read-only         = true   
          rsapplytype:sensitive   = false
          flFileNumber:read-only  = true  
          bSysCode:sensitive      = false 
          edDescription:read-only = true
         . 
    when {&ModifyPosted}
     then
      assign
          flDepositRef:read-only  = true
          flRefNum:read-only      = true
          flCheckDate:read-only   = true
          flReceiptDate:read-only = true
          flAmt:read-only         = true   
          rsapplytype:sensitive   = false
          flFileNumber:read-only  = true  
          bSysCode:sensitive      = false 
          edDescription:read-only = false
         .
  end case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validateReceiptDate Dialog-Frame 
FUNCTION validateReceiptDate RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if flReceiptDate:input-value <> ?    and 
     flReceiptDate:input-value > today
   then
    do:
      message "Receipt date cannot be in future."
          view-as alert-box.
      apply 'entry' to flReceiptDate.  
      return false.
    end.
    
  if flCheckDate:input-value   <> ? and 
     flReceiptDate:input-value < flCheckDate:input-value
   then
    do:
      message "Receipt date cannot be prior to the check/reference date."
          view-as alert-box.
      apply 'entry' to flReceiptDate.  
      return false.
    end.
    
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

