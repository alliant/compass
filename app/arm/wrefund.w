&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wRefund.w

  Description: Window for AR Payments

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Vikas jain

  Created: 07.11.2019
  Modifications:
  Date         Name     Description
  07/29/2020   RS       To use unappliedtran.transDate instead of unappliedtran.postDate.
  08/16/2021   MK       Added tooltip to the lock button.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/ar-def.i}
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/brw-multi-def.i}
{lib/winshowscrollbars.i}
{lib/brw-totalData-def.i}
{lib/ar-getentitytype.i} /* Include function: getEntityType */
{lib/ar-gettrantype.i}   /* Include function: getTranType */


/* Temp-table Definition */
{tt/unappliedtran.i        &tableAlias="unappliedtran"}     /* table used to show data on browse */
{tt/unappliedtran.i        &tableAlias="ttunappliedtran"}   /* used to keep original data to update change pending field */
{tt/unappliedtran.i        &tableAlias="tunappliedtran"}    /* table used for input output purpose */
{tt/unappliedtran.i        &tableAlias="ttbatchtran"} 
{tt/ledgerreport.i &tableAlias="glpayment"} /* contain refund accounts info for GL report */
{tt/ledgerreport.i &tableAlias="glRefund"}
{tt/arpmt.i &tablealias=ttarpmt}
{tt/armisc.i &tablealias=ttARMisc}

define variable dColumnWidth          as decimal   no-undo.
define variable lDefaultAgent         as logical   no-undo.
define variable haPmtRecord           as handle    no-undo.
define variable haField               as handle    no-undo.
define variable deAmtToRefund         as decimal   no-undo.
define variable iEnunappliedtranID    as integer   no-undo.
define variable iSelectedRow          as integer   no-undo.
define variable dtFromDate            as date      no-undo.
define variable dtToDate              as date      no-undo.
define variable iunappliedtranID      as integer   no-undo.
define variable lockedAgent           as character no-undo.
define variable deTotRefAmt           as decimal   no-undo.
define variable deBatchTot            as decimal   no-undo.
define variable lDataChanged          as logical   no-undo.
define variable deOldAmtToRefund      as decimal   no-undo.
define variable deTempOldAmtToRefund  as decimal initial 0  no-undo.
define variable cType                 as CHARACTER no-undo.
define variable rwRow                 as rowid     no-undo.
define variable dwidth                as decimal   no-undo.

define buffer b-unappliedtran for unappliedtran.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwbatchtran

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttbatchtran unappliedtran

/* Definitions for BROWSE brwbatchtran                                  */
&Scoped-define FIELDS-IN-QUERY-brwbatchtran ttbatchtran.entityID ttbatchtran.reference ttbatchtran.referenceamt ttbatchtran.amttorefund   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwbatchtran   
&Scoped-define SELF-NAME brwbatchtran
&Scoped-define QUERY-STRING-brwbatchtran for each ttbatchtran
&Scoped-define OPEN-QUERY-brwbatchtran open query {&SELF-NAME} for each ttbatchtran.
&Scoped-define TABLES-IN-QUERY-brwbatchtran ttbatchtran
&Scoped-define FIRST-TABLE-IN-QUERY-brwbatchtran ttbatchtran


/* Definitions for BROWSE brwunappliedtran                              */
&Scoped-define FIELDS-IN-QUERY-brwunappliedtran unappliedtran.reference unappliedtran.referencedate unappliedtran.postDate unappliedtran.referenceamt unappliedtran.appliedAmt unappliedtran.remainingAmt abs(unappliedtran.refundAmt) @ unappliedtran.refundamt unappliedtran.amttorefund unappliedtran.selectrecord unappliedtran.stat   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwunappliedtran unappliedtran.selectrecord unappliedtran.amttorefund   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwunappliedtran unappliedtran
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwunappliedtran unappliedtran
&Scoped-define SELF-NAME brwunappliedtran
&Scoped-define QUERY-STRING-brwunappliedtran for each unappliedtran
&Scoped-define OPEN-QUERY-brwunappliedtran open query {&SELF-NAME} for each unappliedtran.
&Scoped-define TABLES-IN-QUERY-brwunappliedtran unappliedtran
&Scoped-define FIRST-TABLE-IN-QUERY-brwunappliedtran unappliedtran


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwbatchtran}~
    ~{&OPEN-QUERY-brwunappliedtran}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bBatchPmt btClear bUndoAll bTranLk bUndo ~
flAgentID bAgentLookup fPostFrom fPostTo cbType flSearch brwunappliedtran ~
brwbatchtran btGo btSetPeriod flReason flTotRefAmt RECT-79 RECT-78 RECT-89 ~
RECT-90 
&Scoped-Define DISPLAYED-OBJECTS flAgentID fPostFrom fPostTo cbType ~
flSearch edReason fPostDate flName flReason flRefTotal flTotRefAmt ~
flBatchTotal flBatchTot 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFormattedNumber C-Win 
FUNCTION getFormattedNumber RETURNS CHARACTER
  ( input deTotal as decimal,
    input hWidget as handle )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup 
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bBatchPmt 
     IMAGE-UP FILE "images/next.bmp":U
     LABEL "Add to Pending" 
     SIZE 7.6 BY 1.71 TOOLTIP "Add to Batch".

DEFINE BUTTON bExport 
     IMAGE-UP FILE "images/excel-i.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 7.2 BY 1.67 TOOLTIP "Export Data".

DEFINE BUTTON bExportBatch 
     IMAGE-UP FILE "images/s-excel-i.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export Data".

DEFINE BUTTON bPrelimRpt  NO-FOCUS
     LABEL "Prelim" 
     SIZE 7.2 BY 1.67 TOOLTIP "Preliminary Report".

DEFINE BUTTON bRefund  NO-FOCUS
     LABEL "Refund" 
     SIZE 7.2 BY 1.67 TOOLTIP "Refund Payment".

DEFINE BUTTON btClear 
     IMAGE-UP FILE "images/s-cancel.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 4.8 BY 1.14 TOOLTIP "Blank out the date range".

DEFINE BUTTON btGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.67 TOOLTIP "Get Data".

DEFINE BUTTON bTranLk  NO-FOCUS
     LABEL "Lock" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON btSetPeriod 
     IMAGE-UP FILE "images/s-calendar.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 4.8 BY 1.14 TOOLTIP "Set current open period as date range".

DEFINE BUTTON btViewPmt 
     IMAGE-UP FILE "images/update-i.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 7.2 BY 1.67 TOOLTIP "Modify Payment Note".

DEFINE BUTTON bUndo 
     IMAGE-UP FILE "images/s-delete-i.bmp":U NO-FOCUS
     LABEL "Undo" 
     SIZE 4.8 BY 1.14 TOOLTIP "Undo".

DEFINE BUTTON bUndoAll 
     IMAGE-UP FILE "images/s-trash-i.bmp":U NO-FOCUS
     LABEL "UndoALL" 
     SIZE 4.8 BY 1.14 TOOLTIP "Undo All".

DEFINE BUTTON bView 
     IMAGE-UP FILE "images/open-i.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 7.2 BY 1.67 TOOLTIP "View Transaction Detail".

DEFINE VARIABLE cbType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Payments","Credits" 
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE edReason AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 139.6 BY 2.29 NO-UNDO.

DEFINE VARIABLE flAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flBatchTot AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
      VIEW-AS TEXT 
     SIZE 10 BY .62 NO-UNDO.

DEFINE VARIABLE flBatchTotal AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 54.4 BY 1 NO-UNDO.

DEFINE VARIABLE flReason AS CHARACTER FORMAT "X(256)":U INITIAL "Reason:" 
      VIEW-AS TEXT 
     SIZE 8 BY .62 NO-UNDO.

DEFINE VARIABLE flRefTotal AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE flSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 TOOLTIP "Enter Check/Reference" NO-UNDO.

DEFINE VARIABLE flTotRefAmt AS DECIMAL FORMAT "(>>,>>9.99)":U INITIAL 0 
      VIEW-AS TEXT 
     SIZE 10 BY .62 NO-UNDO.

DEFINE VARIABLE fPostDate AS DATE FORMAT "99/99/99":U 
     LABEL "Use Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fPostFrom AS DATE FORMAT "99/99/99":U 
     LABEL "Posted" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fPostTo AS DATE FORMAT "99/99/99":U 
     LABEL "To" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-78
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 43 BY 3.1.

DEFINE RECTANGLE RECT-79
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 95 BY 2.91.

DEFINE RECTANGLE RECT-89
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 23.6 BY 2.91.

DEFINE RECTANGLE RECT-90
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 29.6 BY 2.91.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwbatchtran FOR 
      ttbatchtran SCROLLING.

DEFINE QUERY brwunappliedtran FOR 
      unappliedtran SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwbatchtran
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwbatchtran C-Win _FREEFORM
  QUERY brwbatchtran DISPLAY
      ttbatchtran.entityID                         column-label  "Agent ID" format "x(14)"
ttbatchtran.reference                         column-label  "Check/!Reference" format "x(20)"
ttbatchtran.referenceamt                         column-label  "Check/!Reference!Amount"     format ">>>,>>9.99" width 16
ttbatchtran.amttorefund                      column-label  "Amount to!Refund" format "->,>>>,>>9.99" width 17
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 74 BY 10.1
         TITLE "Pending Refunds" ROW-HEIGHT-CHARS .81.

DEFINE BROWSE brwunappliedtran
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwunappliedtran C-Win _FREEFORM
  QUERY brwunappliedtran DISPLAY
      unappliedtran.reference                             column-label  "Check/!Reference" format "x(16)"
unappliedtran.referencedate                        column-label  "Check/!Reference!Date"       format "99/99/99" width 14 
unappliedtran.postDate                        column-label  "Post Date"        format "99/99/99" width 14
unappliedtran.referenceamt                         column-label  "Check/!Reference!Amount"     format ">,>>>,>>9.99"  width 14
unappliedtran.appliedAmt                       column-label  "Applied"          format "->,>>>,>>9.99" width 14
unappliedtran.remainingAmt                     column-label  "Unapplied"        format "->,>>>,>>9.99" width 14
abs(unappliedtran.refundAmt) @ unappliedtran.refundamt column-label  "Refunded!Amount"  format "->,>>>,>>9.99" width 14
unappliedtran.amttorefund                      column-label  "Amount to!Refund" format "->,>>>,>>9.99" width 14
unappliedtran.selectrecord                     column-label  "Select"   width 10 view-as toggle-box
unappliedtran.stat                             column-label  "Status" format "x(12)"
enable unappliedtran.selectrecord unappliedtran.amttorefund
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS NO-COLUMN-SCROLLING SEPARATORS SIZE 147.6 BY 10.1 ROW-HEIGHT-CHARS .81.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bBatchPmt AT ROW 9.43 COL 151.4 WIDGET-ID 498
     btClear AT ROW 2.91 COL 46.2 WIDGET-ID 438 NO-TAB-STOP 
     btViewPmt AT ROW 2.1 COL 134 WIDGET-ID 436
     bUndoAll AT ROW 7.05 COL 234.8 WIDGET-ID 524 NO-TAB-STOP 
     bView AT ROW 2.1 COL 141 WIDGET-ID 4 NO-TAB-STOP 
     bTranLk AT ROW 4.86 COL 234.8 WIDGET-ID 516 NO-TAB-STOP 
     bUndo AT ROW 5.95 COL 234.8 WIDGET-ID 508 NO-TAB-STOP 
     flAgentID AT ROW 1.86 COL 11.4 COLON-ALIGNED WIDGET-ID 352
     bAgentLookup AT ROW 1.76 COL 27.4 WIDGET-ID 350
     bExport AT ROW 2.1 COL 127 WIDGET-ID 8 NO-TAB-STOP 
     fPostFrom AT ROW 2.95 COL 11.4 COLON-ALIGNED WIDGET-ID 360
     bExportBatch AT ROW 8.14 COL 234.8 WIDGET-ID 506 NO-TAB-STOP 
     fPostTo AT ROW 2.95 COL 30.2 COLON-ALIGNED WIDGET-ID 362
     cbType AT ROW 2.95 COL 68.6 COLON-ALIGNED WIDGET-ID 490
     flSearch AT ROW 2.33 COL 103.8 COLON-ALIGNED WIDGET-ID 522
     brwunappliedtran AT ROW 4.86 COL 2 WIDGET-ID 200
     edReason AT ROW 16.43 COL 10.4 NO-LABEL WIDGET-ID 36
     bPrelimRpt AT ROW 1.91 COL 225.6 WIDGET-ID 432 NO-TAB-STOP 
     fPostDate AT ROW 2.24 COL 201.2 COLON-ALIGNED WIDGET-ID 428
     bRefund AT ROW 1.91 COL 218.6 WIDGET-ID 196 NO-TAB-STOP 
     brwbatchtran AT ROW 4.86 COL 160.6 WIDGET-ID 300
     btGo AT ROW 2 COL 88 WIDGET-ID 262 NO-TAB-STOP 
     btSetPeriod AT ROW 2.91 COL 51 WIDGET-ID 440 NO-TAB-STOP 
     flName AT ROW 1.86 COL 30.2 COLON-ALIGNED NO-LABEL WIDGET-ID 424 NO-TAB-STOP 
     flReason AT ROW 16.48 COL 1.8 NO-LABEL WIDGET-ID 430 NO-TAB-STOP 
     flRefTotal AT ROW 15 COL 2.6 COLON-ALIGNED NO-LABEL WIDGET-ID 314 NO-TAB-STOP 
     flTotRefAmt AT ROW 15 COL 115.6 COLON-ALIGNED NO-LABEL WIDGET-ID 500 NO-TAB-STOP 
     flBatchTotal AT ROW 15 COL 162.6 COLON-ALIGNED NO-LABEL WIDGET-ID 502 NO-TAB-STOP 
     flBatchTot AT ROW 15.05 COL 221.2 COLON-ALIGNED NO-LABEL WIDGET-ID 504 NO-TAB-STOP 
     "Parameters" VIEW-AS TEXT
          SIZE 11.2 BY .62 AT ROW 1.05 COL 4.6 WIDGET-ID 266
     "Post" VIEW-AS TEXT
          SIZE 4.8 BY .62 AT ROW 1.05 COL 194.4 WIDGET-ID 434
     "Actions" VIEW-AS TEXT
          SIZE 7.6 BY .62 AT ROW 1.05 COL 127.8 WIDGET-ID 512
     "Filter" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1.1 COL 99.8 WIDGET-ID 520
     RECT-79 AT ROW 1.33 COL 2.4 WIDGET-ID 270
     RECT-78 AT ROW 1.29 COL 191.4 WIDGET-ID 426
     RECT-89 AT ROW 1.33 COL 126 WIDGET-ID 510
     RECT-90 AT ROW 1.33 COL 96.8 WIDGET-ID 518
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 239.4 BY 17.95
         DEFAULT-BUTTON bView WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Refund"
         HEIGHT             = 18.14
         WIDTH              = 239.6
         MAX-HEIGHT         = 34.43
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.43
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwunappliedtran flSearch DEFAULT-FRAME */
/* BROWSE-TAB brwbatchtran bRefund DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bExport IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bExportBatch IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPrelimRpt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bRefund IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwbatchtran:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwbatchtran:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

ASSIGN 
       brwunappliedtran:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR BUTTON btViewPmt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bView IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR edReason IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       edReason:RETURN-INSERTED IN FRAME DEFAULT-FRAME  = TRUE.

/* SETTINGS FOR FILL-IN flBatchTot IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flBatchTot:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN flBatchTotal IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flBatchTotal:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN flName IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flName:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN flReason IN FRAME DEFAULT-FRAME
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN flRefTotal IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flRefTotal:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       flTotRefAmt:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN fPostDate IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwbatchtran
/* Query rebuild information for BROWSE brwbatchtran
     _START_FREEFORM
open query {&SELF-NAME} for each ttbatchtran.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwbatchtran */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwunappliedtran
/* Query rebuild information for BROWSE brwunappliedtran
     _START_FREEFORM
open query {&SELF-NAME} for each unappliedtran.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwunappliedtran */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Refund */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Refund */
DO:

  run validateGetData in this-procedure.
  
  for each ttbatchtran:
     run server\unlocktransaction.p(if ttbatchtran.type = {&Payment} then {&ArPayment} else {&ArCredit},
                                  input ttbatchtran.artranId,
                                  output std-lo,
                                  output std-ch).
    
   if not std-lo
    then
    do:
     message std-ch 
         view-as alert-box error buttons ok.
    end.
  end.
  
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Refund */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup C-Win
ON CHOOSE OF bAgentLookup IN FRAME DEFAULT-FRAME /* agentlookup */
DO:
  define variable cAgentID  as character no-undo.
  define variable cName     as character no-undo.
    
  run dialogagentlookup.w(input flAgentID:input-value,
                          input "",      /* Selected State ID */
                          input false,   /* Allow 'ALL' */
                          output cAgentID,
                          output std-ch, /* Agent state ID */
                          output cName,
                          output std-lo).
   
  if not std-lo 
   then
     return no-apply.
  
  empty temp-table unappliedtran. 
  close query brwunappliedtran.  
  
  run setWidgetState in this-procedure.
  
  resultsChanged(false).
  
  assign
      flAgentID:screen-value = cAgentID
      flName:screen-value    = cName
      . 
  
  if lDefaultAgent and flAgentID:input-value <> ""
   then
    /* Set default AgentID */
    publish "SetDefaultAgent" (input flAgentID:input-value).
  
  if flAgentID:input-value <> ""
   then
    run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBatchPmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBatchPmt C-Win
ON CHOOSE OF bBatchPmt IN FRAME DEFAULT-FRAME /* Add to Pending */
DO:
 run setRefundReason in this-procedure.
 
 run addToBatch in this-procedure.
 
 run enableDisableRefund in this-procedure.
 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExportBatch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExportBatch C-Win
ON CHOOSE OF bExportBatch IN FRAME DEFAULT-FRAME
DO:
  run exportRefundBatch in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrelimRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrelimRpt C-Win
ON CHOOSE OF bPrelimRpt IN FRAME DEFAULT-FRAME /* Prelim */
DO:
    run prelimrpt in this-procedure.   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefund
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefund C-Win
ON CHOOSE OF bRefund IN FRAME DEFAULT-FRAME /* Refund */
DO:
  if can-find(first ttbatchtran)
   then 
    run postRefund in this-procedure. 
    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwbatchtran
&Scoped-define SELF-NAME brwbatchtran
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwbatchtran C-Win
ON DEFAULT-ACTION OF brwbatchtran IN FRAME DEFAULT-FRAME /* Pending Refunds */
DO:
  if not available ttbatchtran
   then
    return.
    
  run deleteFromBatch in this-procedure.
  
  run enableDisableRefund in this-procedure.
/*   run viewPaymentDetail in this-procedure. */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwbatchtran C-Win
ON ROW-DISPLAY OF brwbatchtran IN FRAME DEFAULT-FRAME /* Pending Refunds */
do:
  {lib/brw-rowDisplay-multi.i}  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwbatchtran C-Win
ON START-SEARCH OF brwbatchtran IN FRAME DEFAULT-FRAME /* Pending Refunds */
do:   
   {lib/brw-startSearch-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwbatchtran C-Win
ON VALUE-CHANGED OF brwbatchtran IN FRAME DEFAULT-FRAME /* Pending Refunds */
DO:  
  run setWidgetState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwunappliedtran
&Scoped-define SELF-NAME brwunappliedtran
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwunappliedtran C-Win
ON DEFAULT-ACTION OF brwunappliedtran IN FRAME DEFAULT-FRAME
DO:
  apply "CHOOSE" to bBatchPmt.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwunappliedtran C-Win
ON ROW-DISPLAY OF brwunappliedtran IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowDisplay-multi.i}  
  
  on entry of unappliedtran.selectrecord in browse brwunappliedtran
  do:
      if unappliedtran.selectrecord
       then
       do:
        unappliedtran.amttorefund = ttunappliedtran.remainingAmt.
        open query {&SELF-NAME} for each unappliedtran.
       end.
  end.
  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwunappliedtran C-Win
ON START-SEARCH OF brwunappliedtran IN FRAME DEFAULT-FRAME
do:   
   {lib/brw-startSearch-multi.i} 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwunappliedtran C-Win
ON VALUE-CHANGED OF brwunappliedtran IN FRAME DEFAULT-FRAME
DO:  
  run setWidgetState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClear C-Win
ON CHOOSE OF btClear IN FRAME DEFAULT-FRAME
DO:
 fPostFrom:screen-value = "".
 fPostTo:screen-value = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGo C-Win
ON CHOOSE OF btGo IN FRAME DEFAULT-FRAME /* Go */
or 'return' of flAgentID
do:
  
  run validateGetData in this-procedure.
  
  if not validAgent()
   then
    return no-apply.
        
  run getData in this-procedure.
  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btSetPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btSetPeriod C-Win
ON CHOOSE OF btSetPeriod IN FRAME DEFAULT-FRAME
DO:
  fPostFrom:screen-value = string(dtFromDate).
  fPostTo:screen-value   = string(dtToDate).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btViewPmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btViewPmt C-Win
ON CHOOSE OF btViewPmt IN FRAME DEFAULT-FRAME
DO:
   run viewDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bUndo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bUndo C-Win
ON CHOOSE OF bUndo IN FRAME DEFAULT-FRAME /* Undo */
DO: 
  if not available ttbatchtran
   then
    return.
    
  run deleteFromBatch in this-procedure.
  
  run enableDisableRefund in this-procedure.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bUndoAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bUndoAll C-Win
ON CHOOSE OF bUndoAll IN FRAME DEFAULT-FRAME /* UndoALL */
DO: 
  if not available ttbatchtran
   then
    return.
    
  run deleteAllFromBatch in this-procedure.
  
  run enableDisableRefund in this-procedure.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bView C-Win
ON CHOOSE OF bView IN FRAME DEFAULT-FRAME
DO:
  run viewTransactionDetail in this-procedure.   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbType C-Win
ON VALUE-CHANGED OF cbType IN FRAME DEFAULT-FRAME /* Type */
DO:
  cType = cbType:screen-value.
  
  btViewPmt:tooltip = "Modify " + (if cType = "Payments" then "Payment" else "Credit" ) + " Note".
  
  run setBrowseTitle in this-procedure.
  
  run getData in this-procedure.
    
  open query brwunappliedtran for each unappliedtran.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edReason
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edReason C-Win
ON LEAVE OF edReason IN FRAME DEFAULT-FRAME
DO:
  run setRefundReason in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID C-Win
ON VALUE-CHANGED OF flAgentID IN FRAME DEFAULT-FRAME /* Agent ID */
DO:
  resultsChanged(false).
  flName:screen-value = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fPostFrom
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fPostFrom C-Win
ON VALUE-CHANGED OF fPostFrom IN FRAME DEFAULT-FRAME /* Posted */
DO:  
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fPostTo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fPostTo C-Win
ON VALUE-CHANGED OF fPostTo IN FRAME DEFAULT-FRAME /* To */
DO:  
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwbatchtran
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/win-status.i}
{lib/brw-main-multi.i &browse-list="brwunappliedtran,brwbatchtran"}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

dwidth=  {&window-name}:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").
on 'return':U OF flSearch
do:
  run filterData in this-procedure.
end.
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF this-procedure 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow" anywhere.

btGo            :load-image             ("images/completed.bmp").
btGo            :load-image-insensitive ("images/completed-i.bmp").
    
bExport         :load-image             ("images/excel.bmp").
bExport         :load-image-insensitive ("images/excel-i.bmp").
    
bView           :load-image             ("images/open.bmp").
bView           :load-image-insensitive ("images/open-i.bmp").
    
bRefund         :load-image             ("images/check.bmp").              
bRefund         :load-image-insensitive ("images/check-i.bmp").
    
bAgentLookup    :load-image             ("images/s-lookup.bmp").
bAgentLookup    :load-image-insensitive ("images/s-lookup-i.bmp").
    
bPrelimRpt      :load-image             ("images/pdf.bmp").
bPrelimRpt      :load-image-insensitive ("images/pdf-i.bmp").
    
btViewPmt       :load-image             ("images/update.bmp").
btViewPmt       :load-image-insensitive ("images/update-i.bmp").

bExportBatch    :load-image             ("images/s-excel.bmp").
bExportBatch    :load-image-insensitive ("images/s-excel-i.bmp").

bUndo           :load-image             ("images/s-delete.bmp").
bUndo           :load-image-insensitive ("images/s-delete-i.bmp").

bTranLk         :load-image             ("images/s-lock.bmp").

bBatchPmt       :load-image             ("images/next.bmp").

bUndoAll        :load-image             ("images/s-trash.bmp").
bUndoAll        :load-image-insensitive ("images/s-trash-i.bmp").

cbType:screen-value = "Payments".
cType = cbType:screen-value.
bTranLk:tooltip = "Payment/Credit is locked".

run setBrowseTitle in this-procedure.


on value-changed of unappliedtran.selectrecord in browse brwunappliedtran 
do:
  if query brwunappliedtran:num-results = 0 
   then
    return.
    
  {&browse-name}:select-focused-row() in frame {&frame-name} no-error.
  if unappliedtran.selectrecord:checked in browse brwunappliedtran
   then
   assign
    unappliedtran.Amttorefund:screen-value in browse brwunappliedtran   = string(unappliedtran.remainingAmt)
    unappliedtran.remainingAmt:screen-value in browse brwunappliedtran  = "0"
    unappliedtran.refundAmt:screen-value in browse brwunappliedtran     = string(unappliedtran.refundAmt - unappliedtran.remainingAmt)
    unappliedtran.stat:screen-value in browse brwunappliedtran     = "Pending"
    deTotRefAmt        = (deTotRefAmt + decimal(unappliedtran.Amttorefund:screen-value in browse brwunappliedtran))
    lDataChanged       = true.
   else
    assign
     unappliedtran.Amttorefund:screen-value in browse brwunappliedtran  = "0"
     unappliedtran.remainingAmt:screen-value in browse brwunappliedtran  = string(unappliedtran.remainingAmt)
     unappliedtran.refundAmt:screen-value in browse brwunappliedtran     = string(unappliedtran.refundAmt)
     unappliedtran.stat:screen-value in browse brwunappliedtran     = ""
     deTotRefAmt        = (deTotRefAmt - unappliedtran.Amttorefund)
     lDataChanged       = false.
  
   

   flTotRefAmt:screen-value in frame {&frame-name} = string(deTotRefAmt).
   
   if available unappliedtran
    then                                                                      
     assign
      unappliedtran.selectrecord = unappliedtran.selectrecord:checked
      unappliedtran.Amttorefund  = decimal(unappliedtran.Amttorefund:screen-value).
      
end.

on leave, tab, return, go, recall, value-changed of unappliedtran.Amttorefund in browse brwunappliedtran
do:
  if query brwunappliedtran:num-results = 0 
   then
    return.
  
  {&browse-name}:select-focused-row() in frame {&frame-name} no-error.  
  assign
      haField        = browse brwunappliedtran:query:get-buffer-handle("unappliedtran")
      deAmtToRefund  = decimal(haField:buffer-field("Amttorefund"):buffer-value)
      iEnunappliedtranId     = integer(haField:buffer-field("unappliedtranID"):buffer-value)
      .
  
  find first unappliedtran where unappliedtran.unappliedtranID = iEnunappliedtranId no-error.
  
  lDataChanged = false.
  deTempOldAmtToRefund  = unappliedtran.Amttorefund.
  
  
  if decimal(unappliedtran.Amttorefund:screen-value) > 0
   then
   assign
    unappliedtran.selectrecord:checked = true
    unappliedtran.stat:screen-value in browse brwunappliedtran = "Pending".
  else
  assign
    lDataChanged       = true
    unappliedtran.selectrecord:checked = false
    unappliedtran.stat:screen-value in browse brwunappliedtran = ""
    unappliedtran.Amttorefund:screen-value in browse brwunappliedtran  = "0"
    unappliedtran.remainingAmt:screen-value in browse brwunappliedtran  = string(unappliedtran.remainingAmt)
    unappliedtran.refundAmt:screen-value in browse brwunappliedtran     = string(unappliedtran.refundAmt)
    unappliedtran.stat:screen-value in browse brwunappliedtran     = ""
    unappliedtran.selectrecord:checked = false
    deTotRefAmt        = (deTotRefAmt - deTempOldAmtToRefund) + decimal(unappliedtran.Amttorefund:screen-value in browse brwunappliedtran).
  
  if unappliedtran.selectrecord:checked
   then
   do:
    if decimal(unappliedtran.Amttorefund:screen-value) > unappliedtran.remainingAmt
     then
     do:
      MESSAGE "Amount to refund cannot be more than Unapplied amount!" 
          VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.
      assign
       unappliedtran.Amttorefund:screen-value in browse brwunappliedtran  = "0"
       unappliedtran.remainingAmt:screen-value in browse brwunappliedtran  = string(unappliedtran.remainingAmt)
       unappliedtran.refundAmt:screen-value in browse brwunappliedtran     = string(unappliedtran.refundAmt)
       unappliedtran.stat:screen-value in browse brwunappliedtran     = ""
       unappliedtran.selectrecord:checked = false.
       
      return no-apply.
     end.
     
    assign
     lDataChanged       = true
     unappliedtran.remainingAmt:screen-value in browse brwunappliedtran  = string(unappliedtran.remainingAmt - decimal(unappliedtran.Amttorefund:screen-value))
     unappliedtran.refundAmt:screen-value in browse brwunappliedtran     = string(unappliedtran.refundAmt - decimal(unappliedtran.Amttorefund:screen-value))
     unappliedtran.Amttorefund  = decimal(unappliedtran.Amttorefund:screen-value in browse brwunappliedtran)
     deTotRefAmt        = (deTotRefAmt - deTempOldAmtToRefund) + decimal(unappliedtran.Amttorefund:screen-value in browse brwunappliedtran).
     
   end.
   
   flTotRefAmt:screen-value in frame {&frame-name} = string(deTotRefAmt).
   
end.

on entry of unappliedtran.Amttorefund in browse brwunappliedtran
do:
  edReason:sensitive in frame {&frame-name} = if available unappliedtran then true else edReason:sensitive.
end.
  
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  {&window-name}:window-state = window-minimized.   
 
  run enable_UI.   
  
  flRefTotal:screen-value = "Totals".
  flBatchTotal:screen-value = "Totals".
  bTranLk:hidden    = true.
  bUndo:sensitive   = false.
  bUndoAll:sensitive   = false.
  
  publish "GetAutoDefaultAgent" (output lDefaultAgent).
  
  if lDefaultAgent
   then
    do:
      /* Get default AgentID */
      publish "GetDefaultAgent"(output std-ch).

      flAgentID:screen-value = std-ch.

      publish "getAgentName" (input flAgentID:input-value,
                              output std-ch,
                              output std-lo).

      flName:screen-value = std-ch.
    end.
  
  /* When default posting option from config screen is allowed */ 
  publish 'GetDefaultPostingOption' (output std-lo).
  if std-lo
   then
    do:
      /* Set default posting date on screen */
      publish "getDefaultPostingDate"(output std-da).
      fPostDate:screen-value = string(std-da, "99/99/99").
    end.
  
  /* Getting date range from first and last open active period */   
  publish "getOpenPeriod" (output dtFromDate,output dtToDate).    
 
  fPostFrom:screen-value = "".
  fPostTo:screen-value = "". 
  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.

  apply 'entry' to flAgentID.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addToBatch C-Win 
PROCEDURE addToBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
do with frame {&frame-name}:
end.

define variable cTranType as character no-undo.
define variable cLockedBy as character no-undo.
    if not available unappliedtran
     then
      return.
      
for each unappliedtran where unappliedtran.selectRecord:

  cTranType = if unappliedtran.type = "P" then "Payment" 
              else if unappliedtran.type = "C" then "Credit"
              else "".

  /*----------Lock the payment/Credit-----------*/
  run server\locktransaction.p (if unappliedtran.type = "P" then {&ArPayment} else {&ArCredit},
                                input unappliedtran.artranId,
                                output cLockedBy,
                                output std-lo,   /* isAgentLockedSuccess*/
                                output std-ch).  /*lockedBy*/
                                
  
  if not std-lo       
   then
    do:
        message cTranType + " cannot be locked to Refund."
          view-as alert-box error buttons ok.
        return.
     end.
   else if cLockedBy > "" then
    do:
        message cTranType + " is locked by " + cLockedBy + "."
          view-as alert-box error buttons ok.
        return.
     end.
     
  bTranLk:hidden = false.
  
  assign
    unappliedtran.remainingAmt = unappliedtran.remainingAmt - unappliedtran.Amttorefund
    unappliedtran.refundAmt    = unappliedtran.refundAmt - unappliedtran.Amttorefund.
    
  create ttbatchtran.
  buffer-copy unappliedtran to ttbatchtran.
   
  for first b-unappliedtran where rowid(b-unappliedtran) = rowid(unappliedtran):
   delete b-unappliedtran.
  end.
 end.
 
 run setTotals in this-procedure.
 
 open query brwunappliedtran for each unappliedtran.
 open query brwbatchtran for each ttbatchtran.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adjustTotals C-Win 
PROCEDURE adjustTotals :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Setting position of totals widget */
  assign       
      flRefTotal:y    = brwunappliedtran:y + brwunappliedtran:height-pixels + 0.01
      flTotRefAmt:y   = brwunappliedtran:y + brwunappliedtran:height-pixels + 0.01          
      flRefTotal:x    = brwunappliedtran:x + brwunappliedtran:get-browse-column(1):x
      flTotRefAmt:x   = brwunappliedtran:x + brwunappliedtran:get-browse-column(7):x + (brwunappliedtran:get-browse-column(7):width-pixels - flTotRefAmt:width-pixels)  + 6.8    
      no-error.
      
  /* Setting position of totals widget */
  assign       
      flBatchTotal:y    = brwbatchtran:y + brwbatchtran:height-pixels + 0.01
      flBatchTot:y = brwbatchtran:y + brwbatchtran:height-pixels + 0.01          
      flBatchTotal:x    = brwbatchtran:x + brwbatchtran:get-browse-column(1):x
      flBatchTot:x = brwbatchtran:x + brwbatchtran:get-browse-column(7):x + (brwbatchtran:get-browse-column(7):width-pixels - flBatchTot:width-pixels)  + 6.8    
      no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteAllFromBatch C-Win 
PROCEDURE deleteAllFromBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer b-ttbatchtran for ttbatchtran.
 
  
  for each b-ttbatchtran :
  
   run server\unlocktransaction.p(if b-ttbatchtran.type = "P" then {&ArPayment} else {&ArCredit},
                                  input b-ttbatchtran.artranId,
                                  output std-lo,
                                  output std-ch).
    
   if not std-lo
    then
    do:
     message std-ch 
         view-as alert-box error buttons ok.
     return.
    end.
  
    delete b-ttbatchtran.
   
  end.
  
  run setTotals in this-procedure.
  
  open query brwunappliedtran for each unappliedtran.
  open query brwbatchtran for each ttbatchtran.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteFromBatch C-Win 
PROCEDURE deleteFromBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer b-ttbatchtran for ttbatchtran.
  
  
  for first b-ttbatchtran where rowid(b-ttbatchtran) = rowid(ttbatchtran):
  
   run server\unlocktransaction.p(if b-ttbatchtran.type = "P" then {&ArPayment} else {&ArCredit},
                                  input b-ttbatchtran.artranId,
                                  output std-lo,
                                  output std-ch).
    
   if not std-lo
    then
    do:
     message std-ch 
         view-as alert-box error buttons ok.
     return.
    end.
  
    delete b-ttbatchtran.
   
  end.
  
  run setTotals in this-procedure.
  
  open query brwunappliedtran for each unappliedtran.
  open query brwbatchtran for each ttbatchtran.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData C-Win 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  close query brwunappliedtran.
  empty temp-table unappliedtran.
  
  define buffer ttunappliedtran for ttunappliedtran.

  deTotRefAmt = 0.
  for each ttunappliedtran:
    if not can-find(ttbatchtran where ttbatchtran.unappliedtranId = ttunappliedtran.unappliedtranId) and
       not can-find(tunappliedtran where tunappliedtran.unappliedtranId = ttunappliedtran.unappliedtranId)
     then 
      do:
       create unappliedtran.
       buffer-copy ttunappliedtran to unappliedtran.  
    
       deTotRefAmt = deTotRefAmt + unappliedtran.amttorefund.
      end.
  end.
  
  open query brwunappliedtran for each unappliedtran.
  
  flTotRefAmt:screen-value = string(deTotRefAmt).
  
  setStatusCount(query brwunappliedtran:num-results).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableRefund C-Win 
PROCEDURE enableDisableRefund :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if can-find(first ttbatchtran)
   then
    assign
        bPrelimRpt:sensitive   = true 
        fPostDate:sensitive    = true      
        brefund:sensitive      = true
        bExportBatch:sensitive = true
        bUndo:sensitive        = true
        bUndoAll:sensitive     = true
        bTranLk:hidden         = false
        .
   else
    assign
        bPrelimRpt:sensitive   = false 
        fPostDate:sensitive    = false      
        brefund:sensitive      = false
        bExportBatch:sensitive = false
        bUndo:sensitive        = false
        bUndoAll:sensitive     = false
        bTranLk:hidden         = true
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flAgentID fPostFrom fPostTo cbType flSearch edReason fPostDate flName 
          flReason flRefTotal flTotRefAmt flBatchTotal flBatchTot 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bBatchPmt btClear bUndoAll bTranLk bUndo flAgentID bAgentLookup 
         fPostFrom fPostTo cbType flSearch brwunappliedtran brwbatchtran btGo 
         btSetPeriod flReason flTotRefAmt RECT-79 RECT-78 RECT-89 RECT-90 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwunappliedtran:num-results = 0 
   then
    do: 
      message "There is nothing to export."
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
  
  empty temp-table tunappliedtran.
  for each unappliedtran:
    create tunappliedtran.
    buffer-copy unappliedtran to tunappliedtran.
    assign 
        tunappliedtran.entity    = getEntityType(unappliedtran.entity)
        tunappliedtran.transType = getTranType(unappliedtran.transType)
        .
  end.
 
  std-ha = temp-table tunappliedtran:handle.
  run util/exporttable.p (table-handle std-ha,
                          "tunappliedtran",
                          "for each tunappliedtran",
                          "entity,entityID,entityName,reference,receiptdate,referencedate,referenceamt,appliedamt,refundamt,amtToRefund,remainingamt,depositRef,createddate,username,description,transDate",
                          "Entity,Entity ID,Name,Check Num/Reference,Receipt Date,Check/Reference Date,Check/Reference Amount,Applied Amount,Refund Amount,AmtToRefund,Remaining Amount,Deposit Ref,Created Date,Username,Description,PostDate",
                          std-ch,
                          "PaymentsAndCredits-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
                           
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportRefundBatch C-Win 
PROCEDURE exportRefundBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwbatchtran:num-results = 0 
   then
    do: 
      message "There is nothing to export."
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
  
  empty temp-table tunappliedtran.
  for each ttbatchtran:
    create tunappliedtran.
    buffer-copy ttbatchtran to tunappliedtran.
    assign 
        tunappliedtran.entity    = getEntityType(ttbatchtran.entity)
        tunappliedtran.transType = getTranType(ttbatchtran.transType)
        .
  end.
 
  std-ha = temp-table tunappliedtran:handle.
  run util/exporttable.p (table-handle std-ha,
                          "tunappliedtran",
                          "for each tunappliedtran",
                          "entity,entityID,entityName,reference,receiptdate,referencedate,referenceamt,appliedamt,refundamt,amtToRefund,remainingamt,depositRef,createddate,username,description,transDate",
                          "Entity,Entity ID,Name,Check Num/Reference,Receipt Date,Check/Reference Date,Check/Reference Amount,Applied Amount,Refund Amount,AmtToRefund,Remaining Amount,Deposit Ref,Created Date,Username,Description,PostDate",
                          std-ch,
                          "RefundPaymentsAndCredits-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
                           
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define buffer ttunappliedtran for ttunappliedtran.
  
  close query brwunappliedtran.
  
  empty temp-table unappliedtran.
  
  for each ttUnappliedtran :
    if flSearch:screen-value <> "" and
      not (ttUnappliedtran.reference matches "*" + flSearch:screen-value + "*") 
     then 
      next.
       
    create unappliedtran.
    buffer-copy ttUnappliedtran to Unappliedtran.
    deTotRefAmt = deTotRefAmt + unappliedtran.amttorefund.
  end.
  
  open query brwunappliedtran preselect each Unappliedtran by Unappliedtran.entityid by Unappliedtran.type .
  
  flTotRefAmt:screen-value = string(deTotRefAmt).
  
  rwRow = rowid (Unappliedtran).
 
  setStatusCount(query brwunappliedtran:num-results) no-error.
 
  run setWidgetstate in this-procedure.
  reposition brwunappliedtran to rowid(rwRow) no-error.
  

End PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable cLockedByUser as character no-undo.
 
 do with frame {&frame-name}:
 end.

 define buffer ttunappliedtran for ttunappliedtran.
   
  
 if (flAgentID:input-value = "" or
     flAgentID:input-value = ?) 
   then
    do:
      message "Please select a valid agent."
        view-as alert-box.
      return.
    end.
        
  if fPostTo:input-value < fPostFrom:input-value 
   then
    do:
      message "Post To Date cannot be less than Post From Date."
          view-as alert-box.
      return.  
    end. 
    
  if fPostFrom:input-value > today    
   then
    do:
      message "Post From Date cannot be in future."
          view-as alert-box.
      return.
    end.
    
  if fPostTo:input-value > today    
   then
    do:
      message "Post To Date cannot be in future."
          view-as alert-box.
      return.
    end.
    
  run server\getunappliedtran.p(input {&Agent},                                        /* Entity */
                                input flAgentID:screen-value,                    /* Entity ID */ 
                                input cbType:screen-value = "Payments",          /* Include Payments */
                                input cbType:screen-value = "Credits",           /* Include Credits */
                                input "",                                        /* reference */
                                input fPostFrom:input-value,                     /* from date */
                                input fPostTo:input-value,                       /* To date   */
                                output table ttunappliedtran,    
                                output std-lo,                  
                                output std-ch).
  
 if not std-lo
  then
   do:
     message std-ch 
       view-as alert-box error buttons ok.
     return.
   end.
   
 run displayData in this-procedure.
 
 apply 'value-changed' to browse brwunappliedtran.
 
 /* Display no. of records with date and time on status bar */
 setStatusRecords(query brwunappliedtran:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE postRefund C-Win 
PROCEDURE postRefund :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lViewPdf   as logical   no-undo.
 
  do with frame {&frame-name}:
  end.
  
  empty temp-table tunappliedtran.
  
  /* Validating Refund Posting Date */
  if fPostDate:input-value = ?
   then
    do:
      message "Refund post date cannot be blank."
          view-as alert-box error buttons ok.
      apply 'entry' to fPostDate.  
      return.      
    end.
  
  if fPostDate:input-value > today
   then
    do:
      message "Refund post date cannot be in the future."
          view-as alert-box error buttons ok.
      apply 'entry' to fPostDate.     
      return.
    end.
    
  publish "validatePostingDate" (input fPostDate:input-value,
                                 output std-lo).
  
  if not std-lo
   then
    do:
      message "Refund post date must be within an open period."
          view-as alert-box error buttons ok.
      apply 'entry' to fPostDate.     
      return.
    end.
    
  for each ttbatchtran:
    if fPostDate:input-value < date(ttbatchtran.transdate)
     then
      do:
        message "Refund post date cannot be prior to the payment/credit post date for payment reference: " ttbatchtran.reference
            view-as alert-box error buttons ok.
        apply 'entry' to fPostDate.  
        return.
      end.                                             
    create tunappliedtran.
    buffer-copy ttbatchtran to tunappliedtran.
    tunappliedtran.postdate = fPostDate:input-value.
  end.
  
  run dialogviewledger.w (input "Post Refund",output std-lo).
  
  if not std-lo
   then
    return.
  
  publish "GetViewPdf" (output lViewPdf). 
  
  empty temp-table glRefund.
  
  run server/postrefund.p(input lViewPdf,
                          input table tunappliedtran,
                          output table glpayment,
                          output table glRefund,
                          output std-lo,
                          output std-ch).
                         
  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.  
    end.    
  else
   message "Posting was successful."
       view-as alert-box information buttons ok.  
  
  if lViewPdf
   then
    do:
       if not can-find(first glpayment) 
        then
         message "Nothing to print."
            view-as alert-box error buttons ok.
        else
         run util\arpostrefundpdf.p (input {&view},
                                        input table glpayment,
                                        input table glRefund,
                                        output std-ch).

    end.
                                   
  for each tunappliedtran:
    find first ttbatchtran where 
        ttbatchtran.unappliedtranID = tunappliedtran.unappliedtranID and
        ttbatchtran.type            = tunappliedtran.type
        no-error.
     if available ttbatchtran
      then
       do:         
         run unlockTransaction in this-procedure.
         delete ttbatchtran.
       end.  
  end.
  
  
  empty temp-table ttbatchtran.
  
  run displayData in this-procedure.
  open query brwbatchtran for each ttbatchtran.
  
  run enableDisableRefund in this-procedure.
  fPostDate:screen-value = "".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE prelimrpt C-Win 
PROCEDURE prelimrpt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cFilename        as character no-undo.
  define variable cRefundPmtList   as character no-undo.

  
  do with frame {&frame-name}:
  end.
  
  for each ttbatchtran:
     cRefundPmtList = cRefundPmtList + "," + string(ttbatchtran.unappliedtranID) + "|" + string(ttbatchtran.amttorefund) + "|" + string(ttbatchtran.type).
  end.
  
  cRefundPmtList = trim(cRefundPmtList,",").
  run server\prelimrefundgl.p (input cRefundPmtList,
                               output table glpayment,
                               output table glRefund,
                               output std-lo,
                               output std-ch).
  if not std-lo 
  then
   do:
     message std-ch
         view-as alert-box error buttons ok.
     return.
   end.
  
  if not can-find(first glpayment) 
   then
    do:
       message "Nothing to print."
          view-as alert-box error buttons ok.
       return.
    end.
   
   run util\arpostrefundpdf.p (input {&view},
                                  input table glpayment,
                                  input table glRefund,
                                  output cFilename).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshBrowseData C-Win 
PROCEDURE refreshBrowseData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable selectedRow as integer no-undo.
  
  do with frame {&frame-name}:
  end.
  
  std-ro = if available unappliedtran then rowid(unappliedtran) else ?.
  
  do selectedRow = 1 TO brwunappliedtran:num-iterations: 
    if brwunappliedtran:is-row-selected(selectedRow) then leave. 
  end.
    
  open query brwunappliedtran for each unappliedtran.
  
  {&browse-name}:set-repositioned-row(selectedRow) no-error.
  reposition {&browse-name} to rowid std-ro no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setBrowseTitle C-Win 
PROCEDURE setBrowseTitle :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
do with frame {&frame-name}:
end.

brwunappliedtran:title = "Unapplied " + cType.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setRefundReason C-Win 
PROCEDURE setRefundReason :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/   
  do with frame {&frame-name}:
  end.
  
  if not available unappliedtran
   then return.
          
  unappliedtran.notes = edReason:input-value.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setTotals C-Win 
PROCEDURE setTotals :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  deTotRefAmt = 0.
  deBatchTot  = 0.
  
  for each unappliedtran:
   deTotRefAmt = deTotRefAmt + unappliedtran.amttorefund.
  end.
  
  for each ttbatchtran:
   deBatchTot = deBatchTot + ttbatchtran.amttorefund. 
  end.
  
  flTotRefAmt:screen-value in frame {&frame-name} = string(deTotRefAmt).
  flBatchTot:screen-value in frame {&frame-name} = string(deBatchTot).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  if available unappliedtran 
   then
    assign
        edReason:screen-value = unappliedtran.notes
        edReason:sensitive    = unappliedtran.selectrecord
        bView:sensitive       = true
        btViewPmt:sensitive   = true
        bExport:sensitive     = true
        .
  else
   assign
       edReason:screen-value = ""
       edReason:sensitive    = false
       bView:sensitive       = false
       btViewPmt:sensitive   = false
       bExport:sensitive     = false
       .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if {&window-name}:window-state = window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i } 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE unlockAgent C-Win 
PROCEDURE unlockAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  if lockedAgent ne "" 
   then
    do:
      run server\unlocktransaction.p( {&ArAgent},
                                     input lockedAgent,
                                     output std-lo,
                                     output std-ch).
      if not std-lo
       then
        message std-ch 
            view-as alert-box error buttons ok.
                        
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE unlockTransaction C-Win 
PROCEDURE unlockTransaction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run server\unlocktransaction.p(if ttbatchtran.type = {&Payment} then {&ArPayment} else {&ArCredit},
                               input ttbatchtran.artranId,
                               output std-lo,
                               output std-ch).
  
  if not std-lo
   then
   do:
    message std-ch 
        view-as alert-box error buttons ok.
   end.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE updateAmtToRefund C-Win 
PROCEDURE updateAmtToRefund :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  if not available unappliedtran
   then
    return.
    
  do with frame {&frame-name}:
  end.
  
  if not lDataChanged
   then
    for first ttunappliedtran where ttunappliedtran.unappliedtranID = unappliedtran.unappliedtranID: 
     assign
      unappliedtran.remainingAmt = ttunappliedtran.remainingAmt
      unappliedtran.refundAmt    = ttunappliedtran.refundAmt.
    end.  
    
  open query brwunappliedtran for each unappliedtran.
  open query brwbatchtran for each ttbatchtran.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validateGetData C-Win 
PROCEDURE validateGetData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  if can-find(first unappliedtran where unappliedtran.selectrecord)
   then
    do:
      message "Unsubmitted refunds exists. Do you want to contine?"
                 view-as alert-box warning buttons Yes-No
                 update lResponse as logical.
      if not lResponse
       then
        return.
    end.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validateRefundAmt C-Win 
PROCEDURE validateRefundAmt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter oploValidRefund as logical.  
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewDetail C-Win 
PROCEDURE viewDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  empty temp-table ttarpmt.
  empty temp-table ttarmisc.
      
  if not available unappliedtran 
    then
     return.
     
  if unappliedtran.type = {&Payment}
   then
    do:
      run server\getpayment.p (input unappliedtran.artranID, 
                                 input unappliedtran.unappliedtranID,   
                                 output table ttArPmt,
                                 output std-lo,
                                 output std-ch).
          
        if not std-lo
         then
          do:
            message std-ch
                view-as alert-box error buttons ok.
            return.  
          end.
        
        for first ttArPmt:    
          run dialogmodifypayment.w (input {&ModifyPosted},
                                     input-output table ttArPmt,
                                     output std-lo).
        end.
          
    end.
 
  else
   do:
     run server\getinvoices.p (input {&Credit}, 
                           input unappliedtran.unappliedtranID,   
                           output table ttArMisc,
                           output std-lo,
                           output std-ch).     
     if not std-lo
      then
       do:
         message std-ch
             view-as alert-box error buttons ok.
         return.  
       end.
            
       for first ttArMisc:        
         run dialoginvoice.w (input-output table ttArMisc,
                              input {&Credit},
                              input if ttArMisc.void = true then {&View} else {&ModifyPosted}, /* View */
                              output std-lo).
       end.
     
   end.
  
  if not std-lo 
   then
    return.
    
  find first unappliedtran no-error.
  if (not available unappliedtran)
   then
    return.    
    
  find first ttunappliedtran where ttunappliedtran.unappliedtranID = iunappliedtranID no-error.
  if available ttunappliedtran
   then
    buffer-copy unappliedtran to ttunappliedtran no-error.

  run displayData in this-procedure.  
   
  find first unappliedtran where unappliedtran.unappliedtranID = ttunappliedtran.unappliedtranID no-error.
  
  if not available unappliedtran
   then
    return.
  
  reposition {&browse-name} to rowid rowid(unappliedtran).
  
  apply 'value-changed' to browse brwunappliedtran.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewTransactionDetail C-Win 
PROCEDURE viewTransactionDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if available unappliedtran
    then
     publish "OpenWindow" (input "wtransactiondetail",  /*childtype*/
                           input string(unappliedtran.unappliedtranID), /*childid*/
                           input "wtransactiondetail.w",  /*window*/
                           input "integer|input|0^integer|input|" + string(unappliedtran.unappliedtranID) + "^character|input|" + if unappliedtran.type = {&Payment} then {&Payment} else {&Credit},  /*parameters*/                               
                           input this-procedure).  /*currentProcedure handle*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hReasonLbl as handle no-undo.
  
  
  
  assign 
      {&window-name}:width-pixels               = dwidth
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      hReasonLbl                                = flReason:handle in frame {&frame-name}
      
      /* fMain Components */
      brwunappliedtran:height-pixels             = frame {&frame-name}:height-pixels - brwunappliedtran:y - 70
      brwbatchtran:height-pixels                 = frame {&frame-name}:height-pixels - brwbatchtran:y - 70
      .
      
  run adjustTotals in this-procedure.
  assign
      edReason:y   = frame {&frame-name}:height-pixels - 48
      flReason:y   = frame {&frame-name}:height-pixels - 48
      hReasonLbl:y = frame {&frame-name}:height-pixels - 45
      bBatchPmt:y  = frame {&frame-name}:height-pixels / 2 + 10
      no-error.
  
  run ShowScrollBars(frame {&frame-name}:handle, no, no).  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFormattedNumber C-Win 
FUNCTION getFormattedNumber RETURNS CHARACTER
  ( input deTotal as decimal,
    input hWidget as handle ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cNumberFormat  as character no-undo.
  define variable cTotalScrValue as character no-undo.
  define variable deWidgetWidth  as decimal   no-undo.
  
  cTotalScrValue = string(deTotal).
     
  /* account for negative numbers */
  if deTotal < 0
   then 
    cNumberFormat = cNumberFormat + "(".
        
  /* loop through the absolute value of the number cast as an int64 */
  do std-in = length(string(int64(absolute(deTotal)))) to 1 by -1:
    if std-in modulo 3 = 0
     then 
      cNumberFormat = cNumberFormat + (if std-in = length(string(int64(absolute(deTotal)))) then ">" else ",>").
     else 
      cNumberFormat = cNumberFormat + (if std-in = 1 then "Z" else ">").
  end.
     
  /* if the number had a decimal value */
  if index(cTotalScrValue, ".") > 0 
   then 
    cNumberFormat = cNumberFormat + ".99".
         
  /* account for negative numbers */
  if deTotal < 0
   then 
    cNumberFormat = cNumberFormat + ")".
        
  RETURN cNumberFormat.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage("").
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if flAgentID:input-value = ""
   then return false. /* Function return value. */
        
  else if flAgentID:input-value <> ""
   then
    do:
      publish "getAgentName" (input flAgentID:input-value,
                              output std-ch,
                              output std-lo).                                               
      if not std-lo 
       then 
        do:
          assign 
              flAgentID:screen-value = "" 
              flName:screen-value    = ""
              .
          return false. /* Function return value. */
        end.
        
      flName:screen-value = std-ch.
    end. 
  
  empty temp-table unappliedtran. 
  close query brwunappliedtran.  
  
  run setWidgetState in this-procedure.
  
  resultsChanged(false).  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

