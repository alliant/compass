&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wPostedCredit.w

  Description: Window for AR posted Credits

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Anjly

  Created: 08.09.2019
  Date           Name            Description 
  01/14/2021     Shefali         Modified to make "Apply Credit To File" and 
                                 "Apply Credit To Miscellaneous Invoice" pop-up 
                                 menu to "Apply Credit".
  01/18/2021     Shefali         Modified to label pop-up menu "Credit Detail"
                                 to "Edit Credit".
  02/04/2021     Shefali         Modified to show the total amount of the numeric fields
  02/22/2021     Shefali         Modified to show yes/blank in voided column in grid"
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/ar-def.i}
{lib/winshowscrollbars.i}
{lib/brw-totalData-def.i}
{lib/ar-gettrantype.i}   /* Include function: getTranType */
{lib/ar-getentitytype.i} /* Include function: getEntityType */

{tt/artran.i       &tableAlias="ttArTran"}         /* used for data transfer */
{tt/artran.i       &tableAlias="arPostedTran"}     /* contain filtered data */
{tt/artran.i       &tableAlias="ttArPostedTran"}   /* contain data recived from server */
{tt/armisc.i       &tablealias=ttArMisc}           /* used to show header detail */
{tt/ledgerreport.i}                                /* contain gl account info for prelin\post ledger reporting */
{tt/ledgerreport.i &tableAlias="glPaymentDetail"}  /* contain gl account info for prelin\post ledger reporting (needed for deposit)*/

define variable iSelectedArTranID as integer   no-undo.
define variable cAgentID          as character no-undo.
define variable lDefaultAgent     as logical   no-undo.
define variable hPopupMenu        as handle    no-undo.
define variable hPopupMenuItem    as handle    no-undo.
define variable hPopupMenuItem1   as handle    no-undo.
define variable hPopupMenuItem2   as handle    no-undo.
define variable dtFromDate        as date      no-undo.
define variable dtToDate          as date      no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwArPostedTran

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES arPostedTran

/* Definitions for BROWSE brwArPostedTran                               */
&Scoped-define FIELDS-IN-QUERY-brwArPostedTran arPostedTran.entityid "Agent ID" arPostedTran.filenumber "File / ID" arPostedTran.revenuetype "Revenue Type" arPostedTran.trandate "Posted" arPostedTran.tranamt "Total" arPostedTran.remainingamt "Balance" getVoid(arPostedTran.void) @ arPostedTran.void arPostedTran.reference   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwArPostedTran   
&Scoped-define SELF-NAME brwArPostedTran
&Scoped-define QUERY-STRING-brwArPostedTran for each arPostedTran
&Scoped-define OPEN-QUERY-brwArPostedTran open query {&SELF-NAME} for each arPostedTran.
&Scoped-define TABLES-IN-QUERY-brwArPostedTran arPostedTran
&Scoped-define FIRST-TABLE-IN-QUERY-brwArPostedTran arPostedTran


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwArPostedTran}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btClear btSetPeriod bExport bPrelimRpt ~
bSearch btGo bTranDetail btVoid flAgentID bAgentLookup fPostFrom fPostTo ~
tbFullyAppliedCredit tbCreditVoid tSearch brwArPostedTran RECT-79 RECT-81 ~
RECT-85 
&Scoped-Define DISPLAYED-OBJECTS flAgentID flName fPostFrom fPostTo ~
tbFullyAppliedCredit tbCreditVoid tSearch fVoidDate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getVoid C-Win 
FUNCTION getVoid RETURNS CHARACTER
  ( ipVoid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup 
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Agent lookup".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bPrelimRpt  NO-FOCUS
     LABEL "Prelim" 
     SIZE 7.2 BY 1.71 TOOLTIP "Preliminary Report".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "I" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search".

DEFINE BUTTON btClear 
     IMAGE-UP FILE "images/s-cross.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 4.8 BY 1.14 TOOLTIP "Blank out the date range".

DEFINE BUTTON btGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE BUTTON bTranDetail  NO-FOCUS
     LABEL "TranDetail" 
     SIZE 7.2 BY 1.71 TOOLTIP "Transaction detail".

DEFINE BUTTON btSetPeriod 
     IMAGE-UP FILE "images/s-calendar.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 4.8 BY 1.14 TOOLTIP "Set current open period as date range".

DEFINE BUTTON btVoid  NO-FOCUS
     LABEL "Void" 
     SIZE 7.2 BY 1.71 TOOLTIP "Void transaction".

DEFINE VARIABLE flAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN 
     SIZE 18.2 BY 1 NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 53.2 BY 1 NO-UNDO.

DEFINE VARIABLE fPostFrom AS DATE FORMAT "99/99/99":U 
     LABEL "Posted" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fPostTo AS DATE FORMAT "99/99/99":U 
     LABEL "To" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fVoidDate AS DATE FORMAT "99/99/99":U 
     LABEL "Use Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tSearch AS CHARACTER FORMAT "X(50)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20 BY 1 TOOLTIP "Enter a file number to view it directly" NO-UNDO.

DEFINE RECTANGLE RECT-79
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 98 BY 3.1.

DEFINE RECTANGLE RECT-81
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 53.4 BY 3.1.

DEFINE RECTANGLE RECT-85
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 42.4 BY 3.1.

DEFINE VARIABLE tbCreditVoid AS LOGICAL INITIAL no 
     LABEL "Voided" 
     VIEW-AS TOGGLE-BOX
     SIZE 10 BY .76 TOOLTIP "Select to include void credits also" NO-UNDO.

DEFINE VARIABLE tbFullyAppliedCredit AS LOGICAL INITIAL no 
     LABEL "Fully Applied" 
     VIEW-AS TOGGLE-BOX
     SIZE 15.8 BY .81 TOOLTIP "Select to include fully applied credits also" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwArPostedTran FOR 
      arPostedTran SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwArPostedTran
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwArPostedTran C-Win _FREEFORM
  QUERY brwArPostedTran DISPLAY
      arPostedTran.entityid  label         "Agent ID"        format "x(17)"    
arPostedTran.filenumber                               label         "File / ID"       format "x(30)" width 25    
arPostedTran.revenuetype                              label "Revenue Type" format "x(25)"  
arPostedTran.trandate                                 label "Posted"       format "99/99/99" width 14
arPostedTran.tranamt                                  label "Total"        format "->>>,>>>,>>9.99"  width 22
arPostedTran.remainingamt                             label "Balance"      format "->>>,>>>,>>9.99"  width 22
getVoid(arPostedTran.void) @ arPostedTran.void column-label "Voided"       format "x(8)"     
arPostedTran.reference                         column-label  "Reference"   format "x(25)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 193 BY 17.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     btClear AT ROW 2.95 COL 47.2 WIDGET-ID 448 NO-TAB-STOP 
     btSetPeriod AT ROW 2.95 COL 51.8 WIDGET-ID 450 NO-TAB-STOP 
     bExport AT ROW 2.05 COL 145.2 WIDGET-ID 404 NO-TAB-STOP 
     bPrelimRpt AT ROW 2.05 COL 187.4 WIDGET-ID 430 NO-TAB-STOP 
     bSearch AT ROW 2.05 COL 131.2 WIDGET-ID 356 NO-TAB-STOP 
     btGo AT ROW 2.05 COL 92.2 WIDGET-ID 262 NO-TAB-STOP 
     bTranDetail AT ROW 2.05 COL 138.2 WIDGET-ID 396 NO-TAB-STOP 
     btVoid AT ROW 2.05 COL 180.4 WIDGET-ID 418 NO-TAB-STOP 
     flAgentID AT ROW 1.91 COL 12.8 COLON-ALIGNED WIDGET-ID 352
     bAgentLookup AT ROW 1.81 COL 33 WIDGET-ID 350
     flName AT ROW 1.91 COL 35.8 COLON-ALIGNED NO-LABEL WIDGET-ID 424
     fPostFrom AT ROW 3 COL 12.8 COLON-ALIGNED WIDGET-ID 444
     fPostTo AT ROW 3 COL 31.2 COLON-ALIGNED WIDGET-ID 446
     tbFullyAppliedCredit AT ROW 3.19 COL 63 WIDGET-ID 412
     tbCreditVoid AT ROW 3.19 COL 80.8 WIDGET-ID 388
     tSearch AT ROW 2.38 COL 107.8 COLON-ALIGNED WIDGET-ID 354
     fVoidDate AT ROW 2.38 COL 164 COLON-ALIGNED WIDGET-ID 438
     brwArPostedTran AT ROW 4.81 COL 3 WIDGET-ID 200
     "Parameters" VIEW-AS TEXT
          SIZE 11.2 BY .62 AT ROW 1.1 COL 5 WIDGET-ID 266
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.05 COL 102.8 WIDGET-ID 194
     "Void" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1.1 COL 155.6 WIDGET-ID 440
     RECT-79 AT ROW 1.38 COL 3 WIDGET-ID 270
     RECT-81 AT ROW 1.38 COL 100.6 WIDGET-ID 392
     RECT-85 AT ROW 1.38 COL 153.6 WIDGET-ID 420
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 196.4 BY 22.14 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Posted Credits"
         HEIGHT             = 21.91
         WIDTH              = 197.6
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwArPostedTran fVoidDate DEFAULT-FRAME */
ASSIGN 
       brwArPostedTran:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwArPostedTran:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwArPostedTran:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR FILL-IN flName IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flName:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN fVoidDate IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwArPostedTran
/* Query rebuild information for BROWSE brwArPostedTran
     _START_FREEFORM
open query {&SELF-NAME} for each arPostedTran.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwArPostedTran */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Posted Credits */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Posted Credits */
DO:  
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Posted Credits */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup C-Win
ON CHOOSE OF bAgentLookup IN FRAME DEFAULT-FRAME /* agentlookup */
DO:
  define variable cAgentID  as character no-undo.
  define variable cName     as character no-undo.
    
  run dialogagentlookup.w(input flAgentID:input-value,
                          input "",      /* Selected State ID */
                          input true,    /* Allow 'ALL' */
                          output cAgentID,
                          output std-ch, /* Agent state ID */
                          output cName,
                          output std-lo).
   
  if not std-lo  
   then
     return no-apply.
     
  assign
      flAgentID:screen-value = cAgentID
      flName:screen-value    = cName
      . 
  
  resultsChanged(false).
  
  if lDefaultAgent               and
     flAgentID:input-value <> "" and
     flAgentID:input-value <> {&ALL}
   then
    /* Set default AgentID */
    publish "SetDefaultAgent" (input flAgentID:input-value).
  
  if flAgentID:input-value <> "" 
   then
    run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrelimRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrelimRpt C-Win
ON CHOOSE OF bPrelimRpt IN FRAME DEFAULT-FRAME /* Prelim */
do:
   run prelimRpt in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwArPostedTran
&Scoped-define SELF-NAME brwArPostedTran
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArPostedTran C-Win
ON DEFAULT-ACTION OF brwArPostedTran IN FRAME DEFAULT-FRAME
DO:
  run showHeaderDetails in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArPostedTran C-Win
ON LEFT-MOUSE-UP OF brwArPostedTran IN FRAME DEFAULT-FRAME
DO:
  find current arPostedTran no-error.
  if available arPostedTran 
   then
    brwArPostedTran:tooltip = arPostedTran.notes. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArPostedTran C-Win
ON ROW-DISPLAY OF brwArPostedTran IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}
  
  if arPostedTran.remainingamt < 0 
   then
    arPostedTran.remainingamt   :fgcolor in browse brwArPostedTran   = 12.
   else
    arPostedTran.remainingamt   :fgcolor in browse brwArPostedTran   = 0.
    
  if arPostedTran.tranamt < 0 
   then
    arPostedTran.tranamt   :fgcolor in browse brwArPostedTran   = 12.
   else
    arPostedTran.tranamt   :fgcolor in browse brwArPostedTran   = 0.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArPostedTran C-Win
ON START-SEARCH OF brwArPostedTran IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArPostedTran C-Win
ON VALUE-CHANGED OF brwArPostedTran IN FRAME DEFAULT-FRAME
DO:
  iSelectedArTranID = arPostedTran.artranid no-error.
  run setwidgets in this-procedure. 
  run changePopupMenu in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME DEFAULT-FRAME /* I */
OR 'RETURN' of tSearch
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClear C-Win
ON CHOOSE OF btClear IN FRAME DEFAULT-FRAME
DO:
 fPostFrom:screen-value = "".
 fPostTo:screen-value = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGo C-Win
ON CHOOSE OF btGo IN FRAME DEFAULT-FRAME /* Go */
OR 'RETURN' of flAgentID
DO:
  if not validAgent()
   then
    return no-apply.
        
  run getData in this-procedure.           
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bTranDetail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bTranDetail C-Win
ON CHOOSE OF bTranDetail IN FRAME DEFAULT-FRAME /* TranDetail */
do:
   run showTransactionDetail in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btSetPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btSetPeriod C-Win
ON CHOOSE OF btSetPeriod IN FRAME DEFAULT-FRAME
DO:
  fPostFrom:screen-value = string(dtFromDate).
  fPostTo:screen-value   = string(dtToDate).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btVoid
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btVoid C-Win
ON CHOOSE OF btVoid IN FRAME DEFAULT-FRAME /* Void */
do:
  run voidTransaction in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID C-Win
ON VALUE-CHANGED OF flAgentID IN FRAME DEFAULT-FRAME /* Agent ID */
DO:
  resultsChanged(false).
  assign
      flName:screen-value     = ""
      fVoidDate:screen-value  = "" 
      tSearch:sensitive       = false 
      bSearch:sensitive       = false
      btVoid:sensitive        = false
      bPrelimRpt:sensitive    = false
      fVoidDate:sensitive     = false
      bExport:sensitive       = false
      bTranDetail:sensitive   = false
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tbCreditVoid
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tbCreditVoid C-Win
ON VALUE-CHANGED OF tbCreditVoid IN FRAME DEFAULT-FRAME /* Voided */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tbFullyAppliedCredit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tbFullyAppliedCredit C-Win
ON VALUE-CHANGED OF tbFullyAppliedCredit IN FRAME DEFAULT-FRAME /* Fully Applied */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

subscribe to "closeWindow" anywhere.
subscribe to "RefreshScreensForFileNumModify" anywhere.
subscribe to "CloseScreensForFileNumModify" anywhere.
 
/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.


btGo        :load-image            ("images/completed.bmp").
btGo        :load-image-insensitive("images/completed-i.bmp").

bSearch     :load-image            ("images/find.bmp").
bSearch     :load-image-insensitive("images/find-i.bmp").

bTranDetail :load-image            ("images/open.bmp").
bTranDetail :load-image-insensitive("images/open-i.bmp").

btVoid      :load-image            ("images/rejected.bmp").
btVoid      :load-image-insensitive("images/rejected-i.bmp").

bExport     :load-image            ("images/excel.bmp").
bExport     :load-image-insensitive("images/excel-i.bmp").

bAgentLookup:load-image            ("images/s-lookup.bmp").
bAgentLookup:load-image-insensitive("images/s-lookup-i.bmp").

bPrelimRpt  :load-image            ("images/pdf.bmp").
bPrelimRpt  :load-image-insensitive("images/pdf-i.bmp").
 
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  {&window-name}:window-state = window-minimized.  
   
  run enable_UI.  
        
  publish "GetAutoDefaultAgent" (output lDefaultAgent).
  
  /* override the configration as it is currently disabled as per requirement*/
  lDefaultAgent = true.
  
  if lDefaultAgent
   then
    do:
      /* Get default AgentID */
      publish "GetDefaultAgent" (output cAgentID).
      flAgentID:screen-value = cAgentID.
    end.
    
  /* Getting date range from first and last open active period */   
  publish "getOpenPeriod" (output dtFromDate,output dtToDate).    
  
  fPostFrom:screen-value = string(dtFromDate).                                   
  fPostTo:screen-value   = string(dtToDate).
      
  /* When default posting option from config screen is allowed */ 
  publish 'GetDefaultPostingOption' (output std-lo).
  if std-lo
   then
    do:
      /* Set default posting date on screen */
      publish "getDefaultPostingDate"(output std-da).
      fVoidDate:screen-value = string(std-da, "99/99/99").
    end.
        
  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.  
  run setwidgets in this-procedure. 
  
  apply 'entry' to flAgentID.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changePopupMenu C-Win 
PROCEDURE changePopupMenu :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available arPostedTran
   then return.
     
  if valid-handle(hPopupMenuItem) 
   then
    delete object hPopupMenuItem.

  if valid-handle(hPopupMenuItem1) 
   then
    delete object hPopupMenuItem1.  
  
  if valid-handle(hPopupMenuItem2) 
   then
    delete object hPopupMenuItem2.
    
  if valid-handle(hPopupMenu) 
   then
    delete object hPopupMenu.
              
  create menu hPopupMenu.
  assign
      hPopupMenu:popup-only = true
      hPopupMenu:title      = "Browser menu"
      .

  /* As the popup menu differes depending on the type 
     of header record, so dynamically creating the popup
     menu specific for each artran type. */
     
  create menu-item hPopupMenuItem
  assign
      parent = hPopupMenu
      label  = "Transaction Detail"
      name   = "Transaction Detail"
                  
  triggers:
    on choose persistent run showTransactionDetail in this-procedure.
  end triggers.
      
  create menu-item hPopupMenuItem1
  assign 
      label  = "Edit Credit Note"
      name   = "Edit Credit Note"
      parent = hPopupMenu
  triggers:
    on choose persistent run showHeaderDetails in this-procedure.
  end triggers.
       
  create menu-item hPopupMenuItem2
  assign
      parent    = hPopupMenu
      label     = "Apply Credit"
      name      = "Apply Credit"
      sensitive = not arPostedTran.void
                  
  triggers:
    on choose persistent run openApply in this-procedure.
  end triggers.
       
  self:popup-menu = hPopupMenu.
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseScreensForFileNumModify C-Win 
PROCEDURE CloseScreensForFileNumModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pAgentID as character.
 define input parameter pOldFileID as character.
 define input parameter pNewFileID as character.

 if can-find(first ttArPostedTran where ttArPostedTran.entityID = pAgentID and ttArPostedTran.fileid = pOldFileID) or
    can-find(first ttArPostedTran where ttArPostedTran.entityID = pAgentID and ttArPostedTran.fileid = pNewFileID) 
  then
   run closeWindow.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if valid-handle(hPopupMenuItem) 
   then
    delete object hPopupMenuItem.

  if valid-handle(hPopupMenuItem1) 
   then
    delete object hPopupMenuItem1.  
   
  if valid-handle(hPopupMenu) 
   then
    delete object hPopupMenu.
  publish "WindowClosed" (input this-procedure).  
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flAgentID flName fPostFrom fPostTo tbFullyAppliedCredit tbCreditVoid 
          tSearch fVoidDate 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE btClear btSetPeriod bExport bPrelimRpt bSearch btGo bTranDetail btVoid 
         flAgentID bAgentLookup fPostFrom fPostTo tbFullyAppliedCredit 
         tbCreditVoid tSearch brwArPostedTran RECT-79 RECT-81 RECT-85 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwArPostedTran:num-results = 0 
   then
    do: 
      message "There is nothing to export."
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
  
  define buffer arPostedTran for arPostedTran.
  
  empty temp-table ttArTran.
  
  for each arPostedTran:
     create ttArTran.
     buffer-copy arPostedTran to ttArTran.
     assign
        
        ttArTran.transtype  = getTranType(arPostedTran.transtype)
        ttArTran.entity     = getEntityType(arPostedTran.entity)
        .
  end.
  
  std-ha = temp-table ttArTran:handle.
  run util/exporttable.p (table-handle std-ha,
                          "ttArTran",
                          "for each ttArTran",
                          "entity,entityid,entityname,filenumber,fileID,revenuetype,sourceID,artranID,trandate,tranamt,remainingamt,appliedamt,reference,void,voiddate,voidby,notes,createddate,createdby,fullypaid,transtype,postdate,postby,accumbalance,username,arnotes",
                          "Entity,Entity ID,Entity Name,File Number,File ID,Revenue Type,Source ID,Artran ID,Transaction Date,Transaction Amount,Remaining Amount,Applied Amount,Reference,Void,Void Date,Void By,Notes,Created Date,Created By,Fullypaid,Trans Type,Post Date,Post By,Accum Balance,Username,Notes",
                          std-ch,
                          "Arpostedtrans-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).                          
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  do with frame {&frame-name}:
  end.

  define buffer ttArPostedTran for ttArPostedTran.
  
  close query brwArPostedTran.
  empty temp-table arPostedTran.

  for each ttArPostedTran by ttArPostedTran.createddate:
    if tSearch:screen-value <> "" and
      not ((ttArPostedTran.tranID        matches "*" + tSearch:screen-value + "*") or
           (ttArPostedTran.sourcetype    matches "*" + tSearch:screen-value + "*") or
           (ttArPostedTran.revenuetype   matches "*" + tSearch:screen-value + "*") or
           (ttArPostedTran.type          matches "*" + tSearch:screen-value + "*") or
           (ttArPostedTran.entityname    matches "*" + tSearch:screen-value + "*") or
           (ttArPostedTran.filenumber    matches "*" + tSearch:screen-value + "*")
           )
     then next.

    create arPostedTran.
    buffer-copy ttArPostedTran to arPostedTran.
  end.
  
  open query brwArPostedTran preselect each arPostedTran by arPostedTran.entityid by arPostedTran.type by arPostedTran.tranID.
  
  run sortData in this-procedure (dataSortBy).
  
  run setBrowse in this-procedure.
 
  setStatusCount(query brwArPostedTran:num-results) no-error.
  
  if can-find(first arPostedTran) 
   then
    apply 'value-changed' to browse  brwArPostedTran. 
    
  run setWidgets in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  define buffer ttArPostedTran for ttArPostedTran. 
  
  /*Validations*/
  if fPostTo:input-value < fPostFrom:input-value 
   then
    do:
      message "Post To Date cannot be less than Post From Date."
          view-as alert-box.
      return.  
    end. 
    
  if fPostFrom:input-value > today    
   then
    do:
      message "Post From Date cannot be in future."
          view-as alert-box.
      return.
    end.
    
  if fPostTo:input-value > today    
   then
    do:
      message "Post To Date cannot be in future."
          view-as alert-box.
      return.
    end.
  
  if flAgentID:screen-value = "" or flAgentID:screen-value = "ALL" /* using date filter only when particular agentId is not entered*/
   then
    do:
     if fPostFrom:input-value = ? or fPostTo:input-value = ?
      then
       do:
        message "You must enter date range when searching for all agents."
             view-as alert-box.
         return.
       end.
    end.  
  
  {lib/brw-totalData.i &noShow=true}
  
  close query brwArPostedTran.
  
  run server\gettransactions.p( input 0,                            /* ArTranID */
                                input "",                           /* Entity */
                                input flAgentID:screen-value,       /* Entity ID */                                                                
                                input false,                        /* Include Files */
                                input false,                        /* Include Fully Paid Files */
                                input false,                        /* Include Invoices */
                                input false,                        /* Include Void Invoices */
                                input false,                        /* Include Fully Paid Invoices */                                
                                input true,                         /* Include Credits */
                                input tbCreditVoid:checked,         /* Include Void Credits */
                                input tbFullyAppliedCredit:checked, /* Include Fully Applied Credits */                                 
                                input false,                        /* Include Payments */
                                input false,                        /* Include Void Payments */
                                input false,                        /* Include Fully Applied Payments */                                
                                input false,                        /* Include Refunds */
                                input false,                        /* Include Void Refunds */ 
                                input fPostFrom:input-value,        /* from date */
                                input fPostTo:input-value,          /* To date   */
                                output table ttArPostedTran,                                
                                output std-lo,
                                output std-ch).                              
  
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
    
  empty temp-table arPostedTran.

  for each ttArPostedTran by ttArPostedTran.createddate:
    if tSearch:screen-value <> "" and
     not ( (ttArPostedTran.tranID        matches "*" + tSearch:screen-value + "*") or
           (ttArPostedTran.sourcetype    matches "*" + tSearch:screen-value + "*") or
           (ttArPostedTran.revenuetype   matches "*" + tSearch:screen-value + "*") or
           (ttArPostedTran.type          matches "*" + tSearch:screen-value + "*") or
           (ttArPostedTran.entityname    matches "*" + tSearch:screen-value + "*") or
           (ttArPostedTran.filenumber    matches "*" + tSearch:screen-value + "*")
           )
     then next.

    create arPostedTran.
    buffer-copy ttArPostedTran to arPostedTran.
    
  end.
  
  open query brwArPostedTran preselect each arPostedTran by arPostedTran.entityid by arPostedTran.type by arPostedTran.tranID.
  
  run sortData in this-procedure(dataSortBy).
  
  run setBrowse in this-procedure.
  setStatusRecords(query brwArPostedTran:num-results).
  
  if can-find(first arPostedTran) 
   then
    apply 'value-changed' to browse brwArPostedTran .
  
  run setwidgets in this-procedure.    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Include C-Win 
PROCEDURE Include PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if self:private-data = "1" 
   then
    assign
        self:private-data = "0"
        self:label        = "Deselect All"
        .
   else if self:private-data = "0" 
    then
     assign
         self:private-data = "1"
         self:label        = "Select All"
         .
         
  /* if any one is checked, uncheck them all; otherwise check them all */
  if tbCreditVoid:checked            = true
     or tbFullyAppliedCredit:checked = true
   then
    assign 
        tbCreditVoid:checked         = false
        tbFullyAppliedCredit:checked = false
        .
   else
    assign 
        tbCreditVoid:checked         = true
        tbFullyAppliedCredit:checked = true
        .
        
  run setwidgets in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openApply C-Win 
PROCEDURE openApply :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available arpostedtran
   then return.
  
  /* Show records based on the parameter list */
  publish "SetCurrentValue" ("ApplyParams", arpostedtran.entityID + "|" + {&Credit} + "|" + string(arpostedtran.artranID)).
  
  publish "OpenWindow" (input "wapply", 
                        input {&Credit} + "|" + string(arpostedtran.artranID), 
                        input "wapply.w", 
                        input ?,                                   
                        input this-procedure).            

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE prelimRpt C-Win 
PROCEDURE prelimRpt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available arPostedTran
   then return.
                               
  run server/prelimcreditvoidgl.p(input integer(arPostedTran.tranID),
                                  output table ledgerreport,
                                  output std-lo,
                                  output std-ch).
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
        
  run util\arcreditvoidpdf.p (input {&view},
                              input table ledgerreport,
                              output std-ch).  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshScreensForFileNumModify C-Win 
PROCEDURE RefreshScreensForFileNumModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pAgentID as character.
 define input parameter pOldFileID as character.
 define input parameter pNewFileID as character.

 if can-find(first ttArPostedTran where ttArPostedTran.entityID = pAgentID and ttArPostedTran.fileid = pOldFileID) or
    can-find(first ttArPostedTran where ttArPostedTran.entityID = pAgentID and ttArPostedTran.fileid = pNewFileID) 
  then
   run getdata.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setBrowse C-Win 
PROCEDURE setBrowse :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/    
  do with frame {&frame-name}:
  end.
  
  define buffer arpostedtran for arpostedtran.
  
  find first arpostedtran no-error.

  if not available arpostedtran then return.

  std-lo = can-find(first arpostedtran where dueDate <> ?).   
  std-ha = brwArPostedTran:first-column. 
  
  BLK:
  do while valid-handle(std-ha):     
    if std-ha:name = "duedate" 
     then 
      do:
        if std-lo then view std-ha.                         
        else hide std-ha.                        
            
        leave BLK.
      end.                  
    std-ha = std-ha:next-column.                   
  end. 
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setwidgets C-Win 
PROCEDURE setwidgets :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame default-frame:
  end.
  
  define buffer arpostedtran for arpostedtran.
  
  if not can-find( first arpostedtran ) or
     query brwArPostedTran:num-results = 0
   then
    assign
        tSearch:sensitive       = if tSearch:screen-value = "" then false else true 
        bSearch:sensitive       = if tSearch:screen-value = "" then false else true 
        btVoid:sensitive        = false
        bExport:sensitive       = false
        bTranDetail:sensitive   = false
        bPrelimRpt:sensitive    = false
        fVoidDate:screen-value  = ""
        fVoidDate:sensitive     = false
        .
   else
    assign
        tSearch:sensitive       = true 
        bSearch:sensitive       = true
        btVoid:sensitive        = true
        bExport:sensitive       = true
        bTranDetail:sensitive   = true
        bPrelimRpt:sensitive    = true
        fVoidDate:sensitive     = true
        .
          
   find first arpostedtran where arpostedtran.artranID = iSelectedArTranID  no-error.
   if available arpostedtran
    then
     do:
       if ((arpostedtran.type = {&Invoice} and arpostedtran.transtype = {&None}) or arpostedtran.type = {&Payment} or arpostedtran.type = {&Credit} or arpostedtran.type = {&Refund}) and
          (arpostedtran.void = false)
        then
          assign
             btVoid:sensitive      = true
             bPrelimRpt:sensitive  = true
             fVoidDate:sensitive   = true
             .
        else
         assign
             btVoid:sensitive        = false
             bPrelimRpt:sensitive    = false 
             fVoidDate:screen-value  = ""
             fVoidDate:sensitive     = false
             .
     end.
          
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showHeaderDetails C-Win 
PROCEDURE showHeaderDetails :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available arpostedtran
   then return.
  

  define buffer ttArMisc for ttArMisc.

  empty temp-table ttArMisc.

  run server\getinvoices.p (input {&Credit}, 
                            input arpostedtran.tranID,   
                            output table ttArMisc,
                            output std-lo,
                            output std-ch).     
  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.  
    end.
         
    for first ttArMisc:        
      run dialoginvoice.w (input-output table ttArMisc,
                           input {&Credit},
                           input {&ModifyPosted},
                           output std-lo).
    end.                    
                           
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showTransactionDetail C-Win 
PROCEDURE showTransactionDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "OpenWindow" (input "wtransactiondetail",       /*childtype*/
                        input string(iSelectedArTranID),  /*childid*/
                        input "wtransactiondetail.w",     /*window*/
                        input "integer|input|" + string(iSelectedArTranID)  + "^integer|input|0^character|input|",  /*parameters*/                               
                        input this-procedure).            /*currentProcedure handle*/ 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state = window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by arpostedtran.entityid by arpostedtran.type by arpostedtran.tranID ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
  
  if can-find(first arpostedtran) 
   then
    apply 'value-changed' to browse  brwArPostedTran. 
  
  {lib/brw-totalData.i &excludeColumn="2,3,4,7,8"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE voidTransaction C-Win 
PROCEDURE voidTransaction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lViewPdf   as logical   no-undo.
 
  if not available(arpostedtran) 
   then
    return.
    
  iSelectedArTranID = arpostedtran.arTranID.
 
  do with frame {&frame-name}:
  end.
 
  if fVoidDate:input-value = ?
   then
    do:
      message "Void Date cannot be blank."
        view-as alert-box error buttons ok.
      return.      
    end.
  
  if fVoidDate:input-value > today
   then
    do:
      message "Void date cannot be in future."
          view-as alert-box error buttons ok.    
      return.
    end.
  
  publish "validatePostingDate" (input fVoidDate:input-value,
                                 output std-lo).
  if not std-lo
   then
    do:
      message "Void date must lie in open active period."
          view-as alert-box error buttons ok.     
      return.
    end.
    
  if fVoidDate:input-value < date(arpostedtran.trandate)
   then
    do:
       message  "Void date can not be prior to posting date."
        view-as alert-box error buttons ok.
       return.
   end.
 
  run dialogviewvoidledger.w (input "Void Transaction",output std-lo).
 
  if not std-lo
   then
    return.
    
  /*publish "GetViewPdf" (output lViewPdf). */
  
  run server\voidtransaction.p(input iSelectedArTranID,
                               input 0,
                               input fVoidDate:input-value,
                               input lViewPdf,
                               output table ledgerreport,
                               output table glPaymentDetail, /* needed for deposit void */
                               output table ttArTran,
                               output std-lo,
                               output std-ch).
  if not std-lo 
  then
   do:
     message std-ch
         view-as alert-box error buttons ok.
     return.
   end.
  else
   message "Void was successful."
       view-as alert-box information buttons ok.
           
  /*Viewing Pdf*/
  if lViewPdf
   then
    run util\arcreditvoidpdf.p (input {&view},
                                input table ledgerreport,
                                output std-ch).
                                
  find first arpostedtran where arpostedtran.artranID = iSelectedArTranID no-error.
  if available arpostedtran
   then
    do:
        if tbCreditVoid:checked in frame {&frame-name}
         then
          assign
           arpostedtran.void = true.
         else
          delete arpostedtran.
    end.
  
  open query brwArPostedTran preselect each arpostedtran where tbCreditVoid:checked by arpostedtran.entityid by arpostedtran.type by arpostedtran.tranID.
  
  run sortData in this-procedure (dataSortBy).

  run setBrowse in this-procedure.
  
  run setwidgets in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 21
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 23
      .    
  run ShowScrollBars(frame {&frame-name}:handle, no, no).  
  {lib/brw-totalData.i &excludeColumn="2,3,4,7,8"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getVoid C-Win 
FUNCTION getVoid RETURNS CHARACTER
  ( ipVoid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if arPostedTran.void 
   then 
    return "Yes".
  else
   return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if flAgentID:screen-value = "" 
   then
    flAgentID:screen-value = "ALL".
    
  if flAgentID:input-value = {&ALL}
   then
    flName:screen-value = {&NotApplicable}.
       
  else if flAgentID:input-value <> {&ALL}
   then
    do:
      publish "getAgentName" (input flAgentID:input-value,
                              output std-ch,
                              output std-lo).                                               
      if not std-lo 
       then 
        do:
          assign 
              flAgentID:screen-value = "" 
              flName:screen-value    = ""
              .
          return false. /* Function return value. */
        end.
      flName:screen-value = std-ch.
    end. 
  
  resultsChanged(false). 
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

