&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wpostedwriteoff.w

  Description: Window for AR posted write-off's

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Shefali
  Version: 1.0
  Created: 06/01/2021
  Date           Name            Description 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/ar-def.i}
{lib/winshowscrollbars.i}
{lib/get-column.i}

{tt/arwriteoff.i}
{tt/arwriteoff.i       &tableAlias="tarwriteoff"}
{tt/arwriteoff.i       &tableAlias="ttarwriteoff"}
{tt/artran.i       &tableAlias="ttArTran"}         /* used for data transfer */
{tt/ledgerreport.i}                                /* contain gl account info for prelin\post ledger reporting */
{tt/ledgerreport.i &tableAlias="glPaymentDetail"}  /* contain gl account info for prelin\post ledger reporting (needed for deposit)*/

define input parameter writeoffID  as character  no-undo.

define variable iarwriteoffID     as integer    no-undo.
define variable iSelectedArTranID as integer    no-undo.
define variable cAgentID          as character  no-undo.
define variable lDefaultAgent     as logical    no-undo.
define variable dtFromDate        as date       no-undo.
define variable dtToDate          as date       no-undo.
define variable rwRow             as rowid      no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwArPostedTran

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES arwriteoff

/* Definitions for BROWSE brwArPostedTran                               */
&Scoped-define FIELDS-IN-QUERY-brwArPostedTran arwriteoff.entityid arwriteoff.agentname string(arwriteoff.arwriteoffID) arwriteoff.trandate arwriteoff.tranamount /* arwriteoff.type */ arwriteoff.void   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwArPostedTran   
&Scoped-define SELF-NAME brwArPostedTran
&Scoped-define QUERY-STRING-brwArPostedTran for each arwriteoff by entityID
&Scoped-define OPEN-QUERY-brwArPostedTran open query {&SELF-NAME} for each arwriteoff by entityID.
&Scoped-define TABLES-IN-QUERY-brwArPostedTran arwriteoff
&Scoped-define FIRST-TABLE-IN-QUERY-brwArPostedTran arwriteoff


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwArPostedTran}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bExport btSetPeriod btVoid btClear ~
bPrelimRpt btGo flAgentID bAgentLookup fPostFrom fPostTo tbCreditVoid ~
brwArPostedTran bTranDetail fSearch RECT-79 RECT-81 RECT-85 RECT-83 
&Scoped-Define DISPLAYED-OBJECTS flAgentID flName fPostFrom fPostTo ~
tbCreditVoid fVoidDate fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getVoid C-Win 
FUNCTION getVoid RETURNS CHARACTER
  ( ipVoid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup 
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Agent lookup".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify write-off note".

DEFINE BUTTON bPrelimRpt  NO-FOCUS
     LABEL "Prelim" 
     SIZE 7.2 BY 1.71 TOOLTIP "Preliminary Report".

DEFINE BUTTON btClear 
     IMAGE-UP FILE "images/s-cross.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 4.8 BY 1.14 TOOLTIP "Blank out the date range".

DEFINE BUTTON btGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE BUTTON bTranDetail  NO-FOCUS
     LABEL "TranDetail" 
     SIZE 7.2 BY 1.71 TOOLTIP "Transaction detail".

DEFINE BUTTON btSetPeriod 
     IMAGE-UP FILE "images/s-calendar.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 4.8 BY 1.14 TOOLTIP "Set current open period as date range".

DEFINE BUTTON btVoid  NO-FOCUS
     LABEL "Void" 
     SIZE 7.2 BY 1.71 TOOLTIP "Void transaction".

DEFINE VARIABLE flAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 18.2 BY 1 NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 53.2 BY 1 NO-UNDO.

DEFINE VARIABLE fPostFrom AS DATE FORMAT "99/99/99":U 
     LABEL "Posted" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fPostTo AS DATE FORMAT "99/99/99":U 
     LABEL "To" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 25 BY .95 TOOLTIP "Enter Write-off ID" NO-UNDO.

DEFINE VARIABLE fVoidDate AS DATE FORMAT "99/99/99":U 
     LABEL "Use Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-79
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 120 BY 3.1.

DEFINE RECTANGLE RECT-81
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 24.4 BY 3.1.

DEFINE RECTANGLE RECT-83
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE .6 BY 2.29.

DEFINE RECTANGLE RECT-85
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 42.4 BY 3.1.

DEFINE VARIABLE tbCreditVoid AS LOGICAL INITIAL no 
     LABEL "Include Voided" 
     VIEW-AS TOGGLE-BOX
     SIZE 19 BY .76 TOOLTIP "Select to include void write-offs also" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwArPostedTran FOR 
      arwriteoff SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwArPostedTran
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwArPostedTran C-Win _FREEFORM
  QUERY brwArPostedTran DISPLAY
      arwriteoff.entityid         label "Agent ID"     format "x(17)"
arwriteoff.agentname               label "Agent Name"   format "x(50)"            width 70
string(arwriteoff.arwriteoffID)    label "Write-off ID" format "x(17)"
arwriteoff.trandate                label "Posted"       format "99/99/99"         width 14
arwriteoff.tranamount              label "Total"        format "->>>,>>>,>>9.99"  width 22
arwriteoff.void             column-label "Voided"       format "Yes/No"           width 10
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 186 BY 18 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bExport AT ROW 2.14 COL 124 WIDGET-ID 404 NO-TAB-STOP 
     bModify AT ROW 2.14 COL 131.2 WIDGET-ID 462 NO-TAB-STOP 
     btSetPeriod AT ROW 2.95 COL 52.2 WIDGET-ID 450 NO-TAB-STOP 
     btVoid AT ROW 2.05 COL 173.2 WIDGET-ID 418 NO-TAB-STOP 
     btClear AT ROW 2.95 COL 47.4 WIDGET-ID 448 NO-TAB-STOP 
     bPrelimRpt AT ROW 2.05 COL 180.2 WIDGET-ID 430 NO-TAB-STOP 
     btGo AT ROW 2.05 COL 93.2 WIDGET-ID 262 NO-TAB-STOP 
     flAgentID AT ROW 1.91 COL 12.8 COLON-ALIGNED WIDGET-ID 352
     bAgentLookup AT ROW 1.81 COL 33.2 WIDGET-ID 350
     flName AT ROW 1.91 COL 36.2 COLON-ALIGNED NO-LABEL WIDGET-ID 424
     fPostFrom AT ROW 3 COL 12.8 COLON-ALIGNED WIDGET-ID 444
     fPostTo AT ROW 3 COL 31.2 COLON-ALIGNED WIDGET-ID 446
     tbCreditVoid AT ROW 2.62 COL 103 WIDGET-ID 388
     fVoidDate AT ROW 2.38 COL 156.8 COLON-ALIGNED WIDGET-ID 438
     brwArPostedTran AT ROW 4.81 COL 3 WIDGET-ID 200
     bTranDetail AT ROW 2.14 COL 138.4 WIDGET-ID 396 NO-TAB-STOP 
     fSearch AT ROW 3 COL 64 COLON-ALIGNED WIDGET-ID 426
     "Parameters" VIEW-AS TEXT
          SIZE 11.2 BY .62 AT ROW 1.1 COL 5 WIDGET-ID 266
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.05 COL 125 WIDGET-ID 194
     "Void" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1.05 COL 149 WIDGET-ID 440
     RECT-79 AT ROW 1.38 COL 3 WIDGET-ID 270
     RECT-81 AT ROW 1.38 COL 122.6 WIDGET-ID 392
     RECT-85 AT ROW 1.38 COL 146.6 WIDGET-ID 420
     RECT-83 AT ROW 1.81 COL 101.6 WIDGET-ID 406
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 190.6 BY 22.14 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Posted Write-offs"
         HEIGHT             = 21.91
         WIDTH              = 190.4
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwArPostedTran fVoidDate DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bModify IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwArPostedTran:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwArPostedTran:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwArPostedTran:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR FILL-IN flName IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flName:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN fVoidDate IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwArPostedTran
/* Query rebuild information for BROWSE brwArPostedTran
     _START_FREEFORM
open query {&SELF-NAME} for each arwriteoff by entityID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwArPostedTran */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Posted Write-offs */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Posted Write-offs */
DO:  
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Posted Write-offs */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup C-Win
ON CHOOSE OF bAgentLookup IN FRAME DEFAULT-FRAME /* agentlookup */
DO:
  define variable cAgentID  as character no-undo.
  define variable cName     as character no-undo.
    
  run dialogagentlookup.w(input flAgentID:input-value,
                          input "",      /* Selected State ID */
                          input true,    /* Allow 'ALL' */
                          output cAgentID,
                          output std-ch, /* Agent state ID */
                          output cName,
                          output std-lo).
   
  if not std-lo  
   then
     return no-apply.
     
  assign
      flAgentID:screen-value = cAgentID
      flName:screen-value    = cName
      . 
  
  resultsChanged(false).
  
  if lDefaultAgent               and
     flAgentID:input-value <> "" and
     flAgentID:input-value <> {&ALL}
   then
    /* Set default AgentID */
    publish "SetDefaultAgent" (input flAgentID:input-value).
  
  if flAgentID:input-value <> "" 
   then
    run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME DEFAULT-FRAME /* Modify */
DO:
  run modifywriteoff in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrelimRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrelimRpt C-Win
ON CHOOSE OF bPrelimRpt IN FRAME DEFAULT-FRAME /* Prelim */
do:
   run prelimRpt in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwArPostedTran
&Scoped-define SELF-NAME brwArPostedTran
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArPostedTran C-Win
ON DEFAULT-ACTION OF brwArPostedTran IN FRAME DEFAULT-FRAME
DO:
  run modifywriteoff in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArPostedTran C-Win
ON ROW-DISPLAY OF brwArPostedTran IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}
  assign
      arwriteoff.tranamount :fgcolor in browse brwArPostedtran      = if arwriteoff.tranamount < 0 then 12 else 0.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArPostedTran C-Win
ON START-SEARCH OF brwArPostedTran IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArPostedTran C-Win
ON VALUE-CHANGED OF brwArPostedTran IN FRAME DEFAULT-FRAME
DO:
  iSelectedArTranID = arWriteoff.arwriteoffid no-error.
  run setwidgets in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClear C-Win
ON CHOOSE OF btClear IN FRAME DEFAULT-FRAME
DO:
  fPostFrom:screen-value = "".
  fPostTo:screen-value = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGo C-Win
ON CHOOSE OF btGo IN FRAME DEFAULT-FRAME /* Go */
OR 'RETURN' of flAgentID
DO:
  if not validAgent()
   then
    return no-apply.
        
  run getData in this-procedure.           
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bTranDetail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bTranDetail C-Win
ON CHOOSE OF bTranDetail IN FRAME DEFAULT-FRAME /* TranDetail */
do:
   run showTransactionDetail in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btSetPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btSetPeriod C-Win
ON CHOOSE OF btSetPeriod IN FRAME DEFAULT-FRAME
DO:
  fPostFrom:screen-value = string(dtFromDate).
  fPostTo:screen-value   = string(dtToDate).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btVoid
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btVoid C-Win
ON CHOOSE OF btVoid IN FRAME DEFAULT-FRAME /* Void */
do:
  run voidTransaction in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID C-Win
ON VALUE-CHANGED OF flAgentID IN FRAME DEFAULT-FRAME /* Agent ID */
DO:
  resultsChanged(false).
  assign
      flName:screen-value     = ""
      fVoidDate:screen-value  = "" 
      btVoid:sensitive        = false
      bPrelimRpt:sensitive    = false
      fVoidDate:sensitive     = false
      bExport:sensitive       = false
      bTranDetail:sensitive   = false
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON VALUE-CHANGED OF fSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tbCreditVoid
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tbCreditVoid C-Win
ON VALUE-CHANGED OF tbCreditVoid IN FRAME DEFAULT-FRAME /* Include Voided */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

subscribe to "closeWindow" anywhere.
/*subscribe to "RefreshScreensForFileNumModify" anywhere.
subscribe to "CloseScreensForFileNumModify" anywhere.*/
 
/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.


btGo        :load-image            ("images/completed.bmp").
btGo        :load-image-insensitive("images/completed-i.bmp").

btSetPeriod :load-image-insensitive("images/s-calendar-i.bmp").

bModify     :load-image             ("images/update.bmp").
bModify     :load-image-insensitive ("images/update-i.bmp").

bTranDetail :load-image            ("images/open.bmp").
bTranDetail :load-image-insensitive("images/open-i.bmp").

btVoid      :load-image            ("images/rejected.bmp").
btVoid      :load-image-insensitive("images/rejected-i.bmp").

bExport     :load-image            ("images/excel.bmp").
bExport     :load-image-insensitive("images/excel-i.bmp").

bAgentLookup:load-image            ("images/s-lookup.bmp").
bAgentLookup:load-image-insensitive("images/s-lookup-i.bmp").

bPrelimRpt  :load-image            ("images/pdf.bmp").
bPrelimRpt  :load-image-insensitive("images/pdf-i.bmp").
 
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  {&window-name}:window-state = window-minimized.  
   
  run enable_UI.  
        
  publish "GetAutoDefaultAgent" (output lDefaultAgent).
  
  /* override the configration as it is currently disabled as per requirement*/
  lDefaultAgent = true.
  
  if lDefaultAgent
   then
    do:
      flAgentId:screen-value   = {&ALL}.
      flName:screen-value      = "N/A".
    end.
    
  /* Getting date range from first and last open active period */   
  publish "getOpenPeriod" (output dtFromDate,output dtToDate).    
  
  fPostFrom:screen-value = "".                                   
  fPostTo:screen-value   = "".
      
  /* When default posting option from config screen is allowed */ 
  publish 'GetDefaultPostingOption' (output std-lo).
  if std-lo
   then
    do:
      /* Set default posting date on screen */
      publish "getDefaultPostingDate"(output std-da).
      fVoidDate:screen-value = string(std-da, "99/99/99").
    end.
  
  if writeoffID ne '0'
   then
    do:
      assign
          flAgentID      :sensitive     = false
          fPostFrom      :sensitive     = false
          fPostTo        :sensitive     = false
          fSearch        :sensitive     = false
          bAgentLookup   :sensitive     = false
          btClear        :sensitive     = false
          btSetPeriod    :sensitive     = false
          fSearch        :screen-value  = writeoffID
          tbcreditvoid   :checked       = true
          .
      
      run getData in this-procedure.
    end.
  
  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.  
  run setwidgets in this-procedure. 
  
  apply 'entry' to flAgentID.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "WindowClosed" (input this-procedure).  
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData C-Win 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  close query brwArPostedTran.
  empty temp-table arwriteoff.
  
  define buffer ttarwriteoff for ttarwriteoff.

  for each ttarwriteoff:
    create arwriteoff.
    buffer-copy ttarwriteoff to arwriteoff.
    if ttarwriteoff.type = {&Debit} 
     then
      assign arwriteoff.type = "Debit".
    else 
      assign arwriteoff.type = "Credit".
  end.
  
  dataSortDesc = not dataSortDesc.
  if dataSortBy = ""
   then dataSortBy = "arwriteoffID".
  open query brwArPostedTran preselect each arwriteoff by arwriteoff.arwriteoffID desc.
  rwRow = rowid(arwriteoff).
  
  apply 'value-changed' to browse brwArPostedTran.
  run sortData in this-procedure (dataSortBy).
      
  setStatusCount(query brwArPostedTran:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flAgentID flName fPostFrom fPostTo tbCreditVoid fVoidDate fSearch 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bExport btSetPeriod btVoid btClear bPrelimRpt btGo flAgentID 
         bAgentLookup fPostFrom fPostTo tbCreditVoid brwArPostedTran 
         bTranDetail fSearch RECT-79 RECT-81 RECT-85 RECT-83 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwArPostedTran:num-results = 0
   then
    do:
      message "There is nothing to export."
          view-as alert-box warning buttons ok.
      return.
    end.

  publish "GetReportDir" (output std-ch).
  
  define buffer arwriteoff for arwriteoff.

  empty temp-table ttarwriteoff.

  for each arwriteoff:
    create ttarwriteoff.
    buffer-copy arwriteoff to ttarwriteoff.
    if ttarwriteoff.source = {&File} 
     then
      assign ttarwriteoff.source = "File".
  end.

  std-ha = temp-table ttarwriteoff:handle.
  run util/exporttable.p (table-handle std-ha,
                          "ttarwriteoff",
                          "for each ttarwriteoff",
                          "arwriteoffid,entityid,agentname,source,type,trandate,createdDate,createdbyName,postdate,postbyName,ledgerID,tranamount,void,voiddate,voidbyName,voidledgerId,Notes",
                          "Write-off ID,Agent ID,Name,source,type,Post Date,Created Date,Created By,Posted Date,Posted By,Ledger ID,Total,Void?,Void date,Void By,Void Ledger Id,Notes",                                                  
                          std-ch,
                          "Arpostedwriteoffs-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /*Validations*/
  if fPostTo:input-value < fPostFrom:input-value 
   then
    do:
      message "Post To Date cannot be less than Post From Date."
          view-as alert-box.
      return.  
    end. 
    
  if fPostFrom:input-value > today    
   then
    do:
      message "Post From Date cannot be in future."
          view-as alert-box.
      return.
    end.
    
  if fPostTo:input-value > today    
   then
    do:
      message "Post To Date cannot be in future."
          view-as alert-box.
      return.
    end.
  
  if flAgentID:screen-value = "" or flAgentID:screen-value = "ALL" /* using date filter only when particular agentId is not entered*/
   then
    do:
     if (fPostFrom:input-value = ? or fPostTo:input-value = ?) and fSearch:input-value = "" 
      then
       do:
        message "You must enter date range when searching for all agents."
             view-as alert-box.
         return.
       end.
    end.  
  
  close query brwArPostedTran.
  
  run server\querywriteoffs.p(  input flAgentID:screen-value,
                                input date(fPostFrom:input-value), /* Post from date */
                                input date(fPostTo:input-value),   /* Post to date   */
                                input fSearch:input-value,
                                input tbCreditVoid:checked,
                                output table ttArwriteoff,
                                output std-lo,
                                output std-ch).                             
  
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
   
  run displayData in this-procedure.
  
  setStatusRecords(query brwArPostedTran:num-results).
  
  run setwidgets in this-procedure.   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Include C-Win 
PROCEDURE Include PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if self:private-data = "1" 
   then
    assign
        self:private-data = "0"
        self:label        = "Deselect All"
        .
   else if self:private-data = "0" 
    then
     assign
         self:private-data = "1"
         self:label        = "Select All"
         .
         
  /* if any one is checked, uncheck them all; otherwise check them all */
  if tbCreditVoid:checked            = true
   then
    assign 
        tbCreditVoid:checked         = false.
   else
    assign 
        tbCreditVoid:checked         = true.
        
  run setwidgets in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifywriteoff C-Win 
PROCEDURE modifywriteoff :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define buffer tarwriteoff for tarwriteoff.
  empty temp-table tarwriteoff.

  if not available arwriteoff 
   then
    return.
  
  create tarwriteoff.
  buffer-copy arwriteoff to tarwriteoff.
  
  iArwriteoffID = arwriteoff.arWriteoffID.
  
  run dialogmodifywriteoff.w (input {&ModifyPosted},  /* Only notes modify */
                             input-output table tarwriteoff,
                             output std-lo).
       
  if not std-lo 
   then
    return.
    
  find first tarwriteoff no-error.
  if not available tarwriteoff
   then
    return.    
    
  find first ttarwriteoff where ttarwriteoff.arWriteoffID = iArwriteoffID no-error.
  if available ttarwriteoff
   then
    buffer-copy tarwriteoff to ttarwriteoff no-error.

 run displayData in this-procedure.
   
 find first arwriteoff where arwriteoff.arWriteoffID = ttarwriteoff.arWriteoffID no-error.
  
  if not available arwriteoff
   then
    return.
  
  reposition {&browse-name} to rowid rowid(arwriteoff).
  
  apply 'value-changed' to browse brwArPostedTran.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE prelimRpt C-Win 
PROCEDURE prelimRpt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available arwriteoff
   then 
    return.
  
  run server/prelimwriteoffvoidgl.p(input string(arwriteoff.arwriteoffID),
                                    output table ledgerreport,
                                    output std-lo,
                                    output std-ch).
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
  
  if not can-find(first ledgerreport)
   then
    do:
      message "Nothing to print."
          view-as alert-box error buttons ok.
      return.
    end.
    /* Generate pdf gl report for the posted write-off's */                             
    run util\arwriteoffvoidpdf.p (input {&view},
                                  input table ledgerreport,
                                  output std-ch).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setwidgets C-Win 
PROCEDURE setwidgets :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame default-frame:
  end.
  
  define BUFFER arwriteoff FOR arwriteoff.
  
  if not can-find( first arwriteoff ) or
     query brwArPostedTran:num-results = 0
   then
    assign
        btVoid:sensitive        = false
        bExport:sensitive       = false
        bModify:sensitive       = false
        bTranDetail:sensitive   = false
        bPrelimRpt:sensitive    = false
        fVoidDate:screen-value  = ""
        fVoidDate:sensitive     = false
        .
   else
    assign
        btVoid:sensitive        = true
        bExport:sensitive       = true
        bModify:sensitive       = true
        bTranDetail:sensitive   = true
        bPrelimRpt:sensitive    = true
        fVoidDate:sensitive     = true
        .
          
   find first arwriteoff where arWriteoff.arWriteoffID = iSelectedArTranID  no-error.
   if available arwriteoff
    then
     do:
       if arwriteoff.void = false
        then
          assign
              btVoid:sensitive      = true
              bPrelimRpt:sensitive  = true
              fVoidDate:sensitive   = true
              .
        else
         assign
              btVoid:sensitive        = false
              bPrelimRpt:sensitive    = false
              fVoidDate:screen-value  = ""
              fVoidDate:sensitive     = false
              .
     end.
          
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showTransactionDetail C-Win 
PROCEDURE showTransactionDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "OpenWindow" (input "wtransactiondetail",       /*childtype*/
                        input string(iSelectedArTranID),  /*childid*/
                        input "wtransactiondetail.w",     /*window*/
                        input "integer|input|0^integer|input|" + string(iSelectedArTranID) + "^character|input|" + "W",  /*parameters*/                               
                        input this-procedure).            /*currentProcedure handle*/ 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state = window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by arwriteoff.entityid by arwriteoff.type".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
  
  if can-find(first ttArwriteoff) 
   then
    apply 'value-changed' to browse  brwArPostedTran. 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE voidTransaction C-Win 
PROCEDURE voidTransaction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lViewPdf   as logical   no-undo.

  if not available(arwriteoff)
   then
    return.

  iSelectedArTranID = arwriteoff.arTranID.

  do with frame {&frame-name}:
  end.

  if fVoidDate:input-value = ?
   then
    do:
      message "Void Date cannot be blank."
        view-as alert-box error buttons ok.
      return.
    end.

  if fVoidDate:input-value > today
   then
    do:
      message "Void date cannot be in future."
          view-as alert-box error buttons ok.
      return.
    end.

  publish "validatePostingDate" (input fVoidDate:input-value,
                                 output std-lo).
  if not std-lo
   then
    do:
      message "Void date must lie in open active period."
          view-as alert-box error buttons ok.
      return.
    end.

  if fVoidDate:input-value < date(arwriteoff.trandate)
   then
    do:
      message  "Void date can not be prior to posting date."
       view-as alert-box error buttons ok.
      return.
   end.

  run dialogviewvoidledger.w (input "Void Transaction",output std-lo).

  if not std-lo
   then
    return.

  publish "GetViewPdf" (output lViewPdf).

  run server\voidtransaction.p(input iSelectedArTranID,
                               input 0,
                               input fVoidDate:input-value,
                               input lViewPdf,
                               output table ledgerreport,
                               output table glPaymentDetail,
                               output table ttArTran,
                               output std-lo,
                               output std-ch).
  
  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
   else
    message "Void was successful."
        view-as alert-box information buttons ok.

  /*Viewing Pdf*/
  if lViewPdf
   then
    run util\arwriteoffvoidpdf.p (input {&view},
                                  input table ledgerreport,
                                  output std-ch).   

  for first arwriteoff 
      where arwriteoff.arTranID = iSelectedArTranID:
    for first ttArWriteOff 
        where ttArWriteOff.arTranID = iSelectedArTranID:
      if tbCreditVoid:checked in frame {&frame-name}
       then
        do:
          arwriteoff.void = true.
          ttarwriteoff.void = true.
        end.
       else
        do:
          delete arwriteoff.
          delete ttarwriteoff.
        end.
    end.
  end.
  
  run displayData in this-procedure.
  
  setStatusRecords(query brwArPostedTran:num-results).
  
  run setwidgets in this-procedure.   
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 21
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 4
      .    
  run ShowScrollBars(frame {&frame-name}:handle, no, no).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getVoid C-Win 
FUNCTION getVoid RETURNS CHARACTER
  ( ipVoid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if arwriteoff.void 
   then 
    return "Yes".
  else
   return "No".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if flAgentID:screen-value = "" 
   then
    flAgentID:screen-value = "ALL".
    
  if flAgentID:input-value = {&ALL}
   then
    flName:screen-value = {&NotApplicable}.
       
  else if flAgentID:input-value <> {&ALL}
   then
    do:
      publish "getAgentName" (input flAgentID:input-value,
                              output std-ch,
                              output std-lo).                                               
      if not std-lo 
       then 
        do:
          assign 
              flAgentID:screen-value = "" 
              flName:screen-value    = ""
              .
          return false. /* Function return value. */
        end.
      flName:screen-value = std-ch.
    end. 
  
  resultsChanged(false). 
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

