&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wpostedbatch.w

  Description: Window for showing all batches of a selected period

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul Sharma

  Created: 12-21-2020
  Modified:
  Date          Name           Description
  01/06/2021    Shefali        Modified to show the total amount of the numeric fields.
  01/12/2021    Shefali        Modified to add pop-up menu "View Period Processing GL Report".
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/ar-def.i}
{lib/winlaunch.i} 
{lib/winshowscrollbars.i}
{lib/get-column.i}
{lib/getperiodname.i} /* Include function: getPeriodName */
{lib/brw-totalData-def.i}

/* Temp-table Definition */
{tt/arbatch.i   &tableAlias=batch}           /* Contain filterd data*/
{tt/arbatch.i   &tableAlias=ttbatch}         /* Contain data fetched from server*/
          
/* Variable Definition */
define variable dColumnWidth     as decimal    no-undo.
define variable iYear            as integer    no-undo.
define variable iMonth           as integer    no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwArinv

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES batch

/* Definitions for BROWSE brwArinv                                      */
&Scoped-define FIELDS-IN-QUERY-brwArinv string(batch.batchid) @ batch.batchid batch.agentid batch.agentName batch.stateID string(batch.ledgerID) @ batch.ledgerID batch.receivedDate batch.invoiceDate batch.grossPremiumDelta batch.retainedPremiumDelta batch.netPremiumDelta batch.fileCount batch.policyCount batch.endorsementCount batch.cplCount   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwArinv   
&Scoped-define SELF-NAME brwArinv
&Scoped-define QUERY-STRING-brwArinv for each batch
&Scoped-define OPEN-QUERY-brwArinv open query {&SELF-NAME} for each batch.
&Scoped-define TABLES-IN-QUERY-brwArinv batch
&Scoped-define FIRST-TABLE-IN-QUERY-brwArinv batch


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwArinv}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bExport bGo bPeriod cbState cbAgent brwArinv ~
RECT-78 RECT-79 RECT-7 
&Scoped-Define DISPLAYED-OBJECTS fPeriod cbState cbAgent 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwArinv 
       MENU-ITEM m_View_Detail  LABEL "View Batch Detail"
       MENU-ITEM m_Period_Processing_GL_Report LABEL "View Period Processing GL Transactions".


/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE BUTTON bPeriod  NO-FOCUS
     LABEL "Period" 
     SIZE 5 BY 1.19 TOOLTIP "Select period".

DEFINE BUTTON btResetFilter  NO-FOCUS
     LABEL "Reset" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reset filters".

DEFINE BUTTON bView  NO-FOCUS
     LABEL "View" 
     SIZE 7.2 BY 1.71 TOOLTIP "View batch detail".

DEFINE VARIABLE cbAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 61.2 BY 1 NO-UNDO.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE fPeriod AS CHARACTER FORMAT "X(256)":U INITIAL "Select Period" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-7
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 40 BY 3.1.

DEFINE RECTANGLE RECT-78
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 82.6 BY 3.1.

DEFINE RECTANGLE RECT-79
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 19.2 BY 3.1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwArinv FOR 
      batch SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwArinv
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwArinv C-Win _FREEFORM
  QUERY brwArinv DISPLAY
      string(batch.batchid) @ batch.batchid                  label  "Batch ID"    width 12                      
batch.agentid                label  "Agent ID"               format "x(20)"       width 10          
batch.agentName              label  "Name"                   format "x(200)"      width 48               
batch.stateID                label  "State ID"               format "x(10)"       width 10   
string(batch.ledgerID) @   
batch.ledgerID               label  "Ledger ID"                                   width 12
batch.receivedDate           column-label  "Received"        format "99/99/99"    width 11                  
batch.invoiceDate            column-label  "Completed"       format "99/99/99"    width 12             
batch.grossPremiumDelta      label  "Gross Premium"          format "->>>,>>>,>>9.99"        
batch.retainedPremiumDelta   label  "Retained Premium"       format "->>>,>>>,>>9.99"        
batch.netPremiumDelta        label  "Net Premium"            format "->>>,>>>,>>9.99"        
batch.fileCount              column-label "#File"            format ">,>>9"       width 8
batch.policyCount            column-label "#Policy"          format ">,>>9"       width 10
batch.endorsementCount       column-label "#Endors"          format ">,>>9"       width 10
batch.cplCount               column-label "#CPL"             format ">,>>9"       width 8
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 212 BY 18.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bExport AT ROW 2 COL 127.4 WIDGET-ID 8 NO-TAB-STOP 
     bView AT ROW 2 COL 134.4 WIDGET-ID 464 NO-TAB-STOP 
     bGo AT ROW 2 COL 32.8 WIDGET-ID 466 NO-TAB-STOP 
     bPeriod AT ROW 2.33 COL 25.6 WIDGET-ID 402 NO-TAB-STOP 
     fPeriod AT ROW 2.48 COL 3.6 COLON-ALIGNED NO-LABEL WIDGET-ID 44
     cbState AT ROW 1.86 COL 49.6 COLON-ALIGNED WIDGET-ID 458
     btResetFilter AT ROW 2 COL 115.2 WIDGET-ID 262 NO-TAB-STOP 
     cbAgent AT ROW 2.95 COL 49.6 COLON-ALIGNED WIDGET-ID 246
     brwArinv AT ROW 4.67 COL 3 WIDGET-ID 200
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.1 COL 44 WIDGET-ID 266
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1 COL 126.2 WIDGET-ID 194
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.05 COL 4.4 WIDGET-ID 456
     RECT-78 AT ROW 1.33 COL 42.6 WIDGET-ID 200
     RECT-79 AT ROW 1.33 COL 124.8 WIDGET-ID 268
     RECT-7 AT ROW 1.33 COL 3 WIDGET-ID 454
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 215.8 BY 22.52 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Posted Batches"
         HEIGHT             = 22.38
         WIDTH              = 216.6
         MAX-HEIGHT         = 33.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwArinv cbAgent DEFAULT-FRAME */
ASSIGN 
       brwArinv:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwArinv:HANDLE
       brwArinv:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwArinv:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR BUTTON btResetFilter IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bView IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fPeriod IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       fPeriod:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwArinv
/* Query rebuild information for BROWSE brwArinv
     _START_FREEFORM
open query {&SELF-NAME} for each batch.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwArinv */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Posted Batches */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Posted Batches */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Posted Batches */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME DEFAULT-FRAME /* Go */
DO:   
  if (iMonth = 0) or (iYear = 0) 
   then
    do:
      message "Please select valid period first"
          view-as alert-box info buttons ok.      
      return no-apply.
    end.
  
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPeriod C-Win
ON CHOOSE OF bPeriod IN FRAME DEFAULT-FRAME /* Period */
DO:
  /* Set selected period */
  run getPeriod in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwArinv
&Scoped-define SELF-NAME brwArinv
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArinv C-Win
ON DEFAULT-ACTION OF brwArinv IN FRAME DEFAULT-FRAME
DO:
  run batchDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArinv C-Win
ON ROW-DISPLAY OF brwArinv IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArinv C-Win
ON START-SEARCH OF brwArinv IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btResetFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btResetFilter C-Win
ON CHOOSE OF btResetFilter IN FRAME DEFAULT-FRAME /* Reset */
DO:
  run resetFilters in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bView C-Win
ON CHOOSE OF bView IN FRAME DEFAULT-FRAME /* View */
DO:  
  run batchDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgent C-Win
ON VALUE-CHANGED OF cbAgent IN FRAME DEFAULT-FRAME /* Agent */
OR 'VALUE-CHANGED' of cbState  
DO:
  run displayBatches in this-procedure.
  /* Makes widgets enable-disable based on the data */
  run enableWidgets in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Period_Processing_GL_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Period_Processing_GL_Report C-Win
ON CHOOSE OF MENU-ITEM m_Period_Processing_GL_Report /* View Period Processing GL Transactions */
DO:   
   publish "OpenWindow" (input "wperiodprocessingglrpt", 
                         input string(batch.batchId), 
                         input "wperiodprocessingglrpt.w", 
                         input "character|input|" + string(batch.periodId) + "^character|input|" + string(batch.batchId),                                   
                         input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Detail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Detail C-Win
ON CHOOSE OF MENU-ITEM m_View_Detail /* View Batch Detail */
DO:
  run batchDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow"  anywhere.

bGo          :load-image            ("images/completed.bmp").
bGo          :load-image-insensitive("images/completed-i.bmp").

bPeriod      :load-image            ("images/s-calendar.bmp").
bPeriod      :load-image-insensitive("images/s-calendar-i.bmp").

bExport      :load-image            ("images/excel.bmp").
bExport      :load-image-insensitive("images/excel-i.bmp").

bView       :load-image             ("images/open.bmp").
bView       :load-image-insensitive ("images/open-i.bmp").

btResetFilter:load-image            ("images/filtererase.bmp").
btResetFilter:load-image-insensitive("images/filtererase-i.bmp").

publish "getDefaultPeriod"(output iMonth,output iYear).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
      
  run enable_UI.  

  {lib/get-column-width.i &col="'Name'" &var=dColumnWidth}  
 
  /* Initialise and set filters to ALL */
  assign
      cbState:list-item-pairs  = {&ALL} + "," + {&ALL}
      cbAgent:list-item-pairs  = {&ALL} + "," + {&ALL}
      cbState:screen-value     = {&ALL}
      cbAgent:screen-value     = {&ALL}
      fPeriod:screen-value     = getPeriodName(iMonth,iYear)
      .
     
  run enableWidgets in this-procedure.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE batchDetail C-Win 
PROCEDURE batchDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/                        
  publish "OpenWindow" (input "wBatchDetail", 
                        input string(batch.batchid), 
                        input "wbatchdetail.w", 
                        input "integer|input|" + string(batch.batchid) + "^character|input|" + {&ALL} + "^character|input|" + batch.agentID,                                   
                        input this-procedure).                         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/           
  publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayBatches C-Win 
PROCEDURE displayBatches :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
   
  /* Enable/disable filter button based on selected values */
  run setfilterButton in this-procedure.
  
  /* This will use the screen-value of the filters which is ALL */
  run filterData  in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableFilters C-Win 
PROCEDURE enableFilters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Makes widget enable-disable based on the data */ 
  if query brwArinv:num-results > 0  or
     btResetFilter:sensitive
   then
    assign 
        cbAgent:sensitive   = true            
        cbState:sensitive   = true
        . 
   else
    assign 
        cbAgent:sensitive   = false             
        cbState:sensitive   = false        
        . 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableWidgets C-Win 
PROCEDURE enableWidgets :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Makes widget enable-disable based on the data */         
  if not can-find(first batch)       and
     cbState:screen-value  = {&ALL}  and
     cbAgent:screen-value  = {&ALL} 
   then
    assign
        brwArinv:sensitive  = false
        bExport:sensitive   = false
        bView:sensitive     = false
        cbState:sensitive   = false
        cbAgent:sensitive   = false
        .
   else 
    assign
        brwArinv:sensitive   = can-find(first batch)
        bExport:sensitive    = can-find(first batch) 
        bView:sensitive      = can-find(first batch)
        cbState:sensitive    = true
        cbAgent:sensitive    = true
        .       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fPeriod cbState cbAgent 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bExport bGo bPeriod cbState cbAgent brwArinv RECT-78 RECT-79 RECT-7 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwArinv:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table batch:handle.
  run util/exporttable.p (table-handle std-ha,
                          "batch",
                          "for each batch",                       
                          "batchID,agentID,agentName,periodID,yearID,stateID,ledgerID,createDate,receivedDate,grossPremiumDelta,retainedPremiumDelta,fileCount,cplCount,policyCount,endorsementCount,invoiceDate",
                          "Batch ID,Agent ID, Agent Name,Period ID,Year ID,State ID,Ledger ID,Created Date,Received Date,GrossPremium Delta,RetainedPremium Delta,File Count,Cpl Count,Policy Count,Endorsement Count,Invoice Date",
                          std-ch,
                          "PostedBatches-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable rwRow as rowid no-undo.
  
  do with frame {&frame-name}:
  end.

  define buffer ttbatch for ttbatch.
  
  close query brwArinv.
  empty temp-table batch.

  {lib/brw-totalData.i &noShow=true}
  
  for each ttbatch 
    where ttbatch.agentName = (if cbAgent:input-value  = {&ALL} then ttbatch.agentName else replace(cbAgent:input-value,"#",","))        
      and ttbatch.stateID   = (if cbState:input-value  = {&ALL} then ttbatch.stateID   else cbState:input-value) :
           
    create batch.
    buffer-copy ttbatch to batch.    
  end.

  dataSortDesc = not dataSortDesc.
  if dataSortBy = ""
   then dataSortBy = "agentName".
  
  open query brwArinv preselect each batch by agentname by batchid.  
  rwRow = rowid (batch).

  reposition brwArinv to rowid(rwRow) no-error.   

  run sortData in this-procedure (dataSortBy).
  setStatusCount(query brwArinv:num-results).
  
  reposition brwArinv to rowid(rwRow) no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
    
  empty temp-table ttbatch.
  
  /* Client Server Call */
  run server\getcompletedbatches.p (input integer(string(iYear) + string(iMonth,"99")),     /* Period ID */
                                    input false,    /* Include All */
                                    input true,     /* OnlyPosted */
                                    output table ttbatch,
                                    output std-lo,
                                    output std-ch). 
  
                            
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
        
  run initialiseFilter in this-procedure.
    
  /* Display temp credits record on the screen */
  run displayBatches in this-procedure.
  
  /* Makes widgets enable-disable based on the data */
  run enableWidgets in this-procedure.
  
  /* Makes filters enable-disable based on the data */
  run enableFilters in this-procedure.
    
  /* Set Status count with date and time from the server */
  setStatusRecords(query brwArinv:num-results).      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getPeriod C-Win 
PROCEDURE getPeriod :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  run dialogperiod.w (input-output iMonth,
                      input-output iYear,
                      output std-ch).
                     
  fPeriod:screen-value = getPeriodName(iMonth,iYear).
  apply "choose" to bgo.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initialiseFilter C-Win 
PROCEDURE initialiseFilter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable cstatesSelected as character no-undo. 
  define variable cAgentSelected  as character no-undo.
  
  define buffer ttbatch for ttbatch.
  
  /* Retaining previous selected values of filters */
  assign      
      cstatesSelected = if cbState:screen-value = ? then ""  else  cbState:screen-value      
      cAgentSelected  = if cbAgent:screen-value = ? then ""  else  cbAgent:screen-value 
      .
  
  /* Initialise filters to default values */
  assign
      cbAgent:list-item-pairs   = {&ALL} + "," + {&ALL}
      cbState:list-item-pairs   = {&ALL} + "," + {&ALL}
      cbAgent:screen-value      = {&ALL} 
      cbState:screen-value      = {&ALL}
      .
    
  /* Set the filters based on the data returned, with ALL as the first option */
  for each ttbatch 
    break by ttbatch.agentName:
    if first-of(ttbatch.agentName) and ttbatch.agentName <> ""
     then 
      cbAgent:add-last(replace(ttbatch.agentName,",",""),replace(ttbatch.agentName,",","#")).
  end.
  
  for each ttBatch 
    break by ttBatch.stateID:
    if first-of(ttBatch.stateID) and ttBatch.stateID <> "" 
     then
      cbState:add-last(ttBatch.stateID,ttBatch.stateID).
  end.
    
  /* Setting previous selected values of filters */
  if cstatesSelected <> "" and cstatesSelected <> {&ALL} and lookup(cstatesSelected ,cbState:list-item-pairs,",") > 0  
   then
    cbState:screen-value = cstatesSelected.
      
  if cAgentSelected <> ""and cAgentSelected <> {&ALL} and lookup(cAgentSelected,cbAgent:list-item-pairs,",") > 0  
   then
    cbAgent:screen-value = cAgentSelected.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resetFilters C-Win 
PROCEDURE resetFilters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Reset filters to initial state */
  assign
      btResetFilter:Tooltip      = "Reset filters"
      cbAgent:screen-value       = {&ALL}
      cbState:screen-value       = {&ALL}
      .
 
  run displayBatches in this-procedure.
  
  /* Makes widgets enable-disable based on the data */
  run enableWidgets in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterButton C-Win 
PROCEDURE setFilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
  
 if cbAgent:input-value ne {&ALL} or cbState:input-value ne {&ALL}
  then
    btResetFilter:sensitive = true.
  else   
    btResetFilter:sensitive = false.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by batch.agentname by batch.batchID ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
  {lib/brw-totalData.i &excludeColumn="2,3,4,5,6"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 14
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 20
      .
      
  {lib/resize-column.i &col="'Name'" &var=dColumnWidth} 
  run ShowScrollBars(frame {&frame-name}:handle, no, no).  
  {lib/brw-totalData.i &excludeColumn="2,3,4,5,6"}  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

