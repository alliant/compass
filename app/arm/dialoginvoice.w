&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialoginvoice.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul Sharma

  Created:07/23/19
  Modifications:
  Date        Name       Description
  04/05/2022  SachinC    Task# 92523 prepopulate fields while copying
                         existing invoice.
  05/13/2022  SachinC    Task# 92523 Information message type made to
                         Error Type.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{tt/armisc.i &tablealias=ttARMisc}

/* Parameters Definitions                                           */
define input-output  parameter table       for ttARMisc.
define input         parameter ipcType     as  character  no-undo.
define input         parameter ipcAction   as  character  no-undo.
define output        parameter oplSuccess  as  logical    no-undo.

/* Standard library files */
{lib/std-def.i}
{lib/ar-def.i}

{tt/sysprop.i}

/* include file to normalize file number */
{lib/normalizefileid.i}

/* Local variable definitions */
define variable cCurrentUser   as character no-undo.
define variable cCurrentUID    as character no-undo.
define variable cSource        as character no-undo. 
define variable cStateID       as character no-undo. 
define variable cFileNumber    as character no-undo.
define variable cReferenceNo   as character no-undo.
define variable cAgentID       as character no-undo.
define variable cSourceID      as character no-undo.
define variable cNotes         as character no-undo.
define variable cCode          as character no-undo.
define variable deAmount       as decimal   no-undo.
define variable dtTranDate     as date      no-undo.
define variable dtDueDate      as date      no-undo.
define variable cInvoiceDue    as character no-undo.
define variable iDays          as integer   no-undo.
define variable cDueDays       as character no-undo.
define variable cRevenueType   as character no-undo.
define variable lDefaultAgent  as logical   no-undo.
define variable lFileBased     as logical   no-undo.

define temp-table ttAction
  field action      as character
  field createdBy   as character
  field createdDate as character
  field ledgerID    as character.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwAction

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttAction

/* Definitions for BROWSE brwAction                                     */
&Scoped-define FIELDS-IN-QUERY-brwAction ttAction.Action ttAction.createdBy ttAction.CreatedDate ttAction.ledgerID   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAction   
&Scoped-define SELF-NAME brwAction
&Scoped-define QUERY-STRING-brwAction FOR EACH ttAction
&Scoped-define OPEN-QUERY-brwAction OPEN QUERY {&SELF-NAME} FOR EACH ttAction.
&Scoped-define TABLES-IN-QUERY-brwAction ttAction
&Scoped-define FIRST-TABLE-IN-QUERY-brwAction ttAction


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwAction}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS flAgentID bAgentLookup flInvoiceID flName ~
flType cbSource flFile flSourceID flRevType bSysCode flReference flTranDate ~
flAmount flDueDate edNotes Btn_Cancel RECT-80 
&Scoped-Define DISPLAYED-OBJECTS flAgentID flInvoiceID flName flType ~
tbFileBased cbSource flFile flSourceID flLabel flRevType flReference ~
flTranDate flAmount flDueDate edNotes fMarkMandatory2 fMarkMandatory1 ~
fMarkMandatory3 fMarkMandatory4 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validateArInvoice Dialog-Frame 
FUNCTION validateArInvoice RETURNS LOGICAL
  ( )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup 
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bSysCode 
     LABEL "sysCode" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cbSource AS CHARACTER FORMAT "X(256)":U 
     LABEL "Source" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "--Select Source--","",
                     "Appointment","A",
                     "CPL","CPL",
                     "History Transaction","HT",
                     "Miscellaneous","M",
                     "Search","S",
                     "Sponsorship","N",
                     "Training","T"
     DROP-DOWN-LIST
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE edNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 95.2 BY 2.81 NO-UNDO.

DEFINE VARIABLE flAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE flAmount AS DECIMAL FORMAT ">>>>>9.99":U INITIAL 0 
     LABEL "Amount" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE flDueDate AS DATE FORMAT "99/99/99":U 
     LABEL "Due" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE flFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "File Number" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE flInvoiceID AS CHARACTER FORMAT "X(256)":U INITIAL "0" 
     LABEL "Invoice ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 18.4 BY 1 NO-UNDO.

DEFINE VARIABLE flLabel AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 7.4 BY .62 NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 74.8 BY 1 NO-UNDO.

DEFINE VARIABLE flReference AS CHARACTER FORMAT "X(256)":U 
     LABEL "Reference" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE flRevType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Revenue Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20.2 BY 1 NO-UNDO.

DEFINE VARIABLE flSourceID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Source Number" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE flTranDate AS DATE FORMAT "99/99/99":U 
     LABEL "Invoiced On" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE flType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE fMarkMandatory1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory3 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory4 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE RECTANGLE RECT-80
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 95.4 BY 3.43.

DEFINE VARIABLE tbFileBased AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 3.2 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAction FOR 
      ttAction SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAction Dialog-Frame _FREEFORM
  QUERY brwAction DISPLAY
      ttAction.Action               label  "Action"    format "x(15)"     
ttAction.createdBy              label  "By"              format "x(50)" 
ttAction.CreatedDate            label  "Date"            format "x(12)" 
ttAction.ledgerID               label  "Ledger ID"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-AUTO-VALIDATE NO-ROW-MARKERS NO-COLUMN-SCROLLING SEPARATORS NO-SCROLLBAR-VERTICAL SIZE 95 BY 3.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     flAgentID AT ROW 1.76 COL 23.2 COLON-ALIGNED WIDGET-ID 352
     bAgentLookup AT ROW 1.67 COL 45.4 WIDGET-ID 350
     flInvoiceID AT ROW 1.76 COL 79.6 COLON-ALIGNED WIDGET-ID 258
     flName AT ROW 2.91 COL 23.2 COLON-ALIGNED NO-LABEL WIDGET-ID 354
     flType AT ROW 4.05 COL 23.2 COLON-ALIGNED WIDGET-ID 356
     tbFileBased AT ROW 4.24 COL 94.4 WIDGET-ID 256
     cbSource AT ROW 5.19 COL 23.2 COLON-ALIGNED WIDGET-ID 54
     flFile AT ROW 5.19 COL 70 COLON-ALIGNED WIDGET-ID 42
     flSourceID AT ROW 6.38 COL 23.2 COLON-ALIGNED WIDGET-ID 68
     flLabel AT ROW 8.05 COL 4.2 COLON-ALIGNED NO-LABEL WIDGET-ID 250
     flRevType AT ROW 6.38 COL 70 COLON-ALIGNED WIDGET-ID 64
     bSysCode AT ROW 6.29 COL 92.4 WIDGET-ID 252
     flReference AT ROW 9.05 COL 23.2 COLON-ALIGNED WIDGET-ID 244
     flTranDate AT ROW 9.05 COL 70 COLON-ALIGNED WIDGET-ID 62
     flAmount AT ROW 10.24 COL 23.2 COLON-ALIGNED WIDGET-ID 2
     flDueDate AT ROW 10.24 COL 70 COLON-ALIGNED WIDGET-ID 36
     edNotes AT ROW 12.71 COL 4.8 NO-LABEL WIDGET-ID 26
     brwAction AT ROW 15.81 COL 5 WIDGET-ID 200
     Btn_OK AT ROW 20 COL 36.6
     Btn_Cancel AT ROW 20 COL 53.2
     fMarkMandatory2 AT ROW 9.33 COL 85.2 COLON-ALIGNED NO-LABEL WIDGET-ID 270 NO-TAB-STOP 
     fMarkMandatory1 AT ROW 6.67 COL 95.2 COLON-ALIGNED NO-LABEL WIDGET-ID 360 NO-TAB-STOP 
     fMarkMandatory3 AT ROW 2.05 COL 48.2 COLON-ALIGNED NO-LABEL WIDGET-ID 362 NO-TAB-STOP 
     fMarkMandatory4 AT ROW 10.48 COL 38.4 COLON-ALIGNED NO-LABEL WIDGET-ID 364 NO-TAB-STOP 
     "Notes:" VIEW-AS TEXT
          SIZE 6.4 BY .62 AT ROW 12.1 COL 4.6 WIDGET-ID 28
     "Apply to File" VIEW-AS TEXT
          SIZE 11.6 BY .62 AT ROW 4.33 COL 81.4 WIDGET-ID 358
     RECT-80 AT ROW 8.38 COL 4.8 WIDGET-ID 248
     SPACE(2.79) SKIP(9.80)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE ""
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwAction edNotes Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BROWSE brwAction IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       edNotes:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

ASSIGN 
       flInvoiceID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN flLabel IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flLabel:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flName:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flRevType:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flType:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fMarkMandatory1 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory2 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory3 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory4 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tbFileBased IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAction
/* Query rebuild information for BROWSE brwAction
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH ttAction.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAction */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup Dialog-Frame
ON CHOOSE OF bAgentLookup IN FRAME Dialog-Frame /* agentlookup */
DO:
  define variable cAgentID  as character no-undo.
  define variable cName     as character no-undo.
    
  run dialogagentlookup.w(input flAgentID:input-value,
                          input "",        /* Selected State ID */
                          input false,     /* Allow 'ALL' */
                          output cAgentID,
                          output cStateID, /* Agent state ID */
                          output cName,
                          output std-lo).
   
  if not std-lo or flAgentID:input-value = cAgentID  
   then
     return no-apply.
     
  assign
      flAgentID:screen-value = cAgentID
      flName:screen-value    = cName
      . 
        
  if flAgentID:input-value <> ""
   then
    do:
      if lDefaultAgent
       then
        /* Set default AgentID */
        publish "SetDefaultAgent" (input flAgentID:input-value).
        
      publish "GetAgentInvoiceDue" (input flAgentID:input-value,
                                    output cInvoiceDue,
                                    output iDays).
    end.
  
  apply 'value-changed' to flTranDate.                                
  
  run enableDisableSave in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSysCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSysCode Dialog-Frame
ON CHOOSE OF bSysCode IN FRAME Dialog-Frame /* sysCode */
DO:
  run dialogrevenuelookup.w(input flRevType:screen-value,
                            input cStateID,
                            output cCode,
                            output std-lo).
  if not std-lo 
   then
    return no-apply.

  flRevType:screen-value = cCode.
      
  run enableDisableSave in this-procedure.    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel Dialog-Frame
ON CHOOSE OF Btn_Cancel IN FRAME Dialog-Frame /* Cancel */
DO:
  oplSuccess = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  if validateArInvoice() 
   then     
     run saveArInvoice in this-procedure.
      
   /* If there was any error then do not close the dialog. */
   if not oplSuccess
    then
     return no-apply.   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbSource
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbSource Dialog-Frame
ON VALUE-CHANGED OF cbSource IN FRAME Dialog-Frame /* Source */
DO:
  run enableDisableSave in this-procedure.  
  
  assign
      flSourceID:label        = if      cbSource:input-value = "A"   then "Appointment Number" 
                                else if cbSource:input-value = "T"   then "Training Number"
                                else if cbSource:input-value = "N"   then "Sponsorship Number"
                                else if cbSource:input-value = "CPL" then "CPL Number"
                                else if cbSource:input-value = "S"   then "Search Number"
                                else if cbSource:input-value = "M"   then "Misc. Number"
                                else "Source Number"
      flSourceID:screen-value = if cbSource:input-value = "" then "" else flSourceID:input-value                    
      .   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edNotes Dialog-Frame
ON VALUE-CHANGED OF edNotes IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID Dialog-Frame
ON LEAVE OF flAgentID IN FRAME Dialog-Frame /* Agent ID */
DO:
  define variable cName  as character no-undo.
    
  if flAgentID:input-value <> ""
   then
    do:
      publish "getAgentName" (input flAgentID:input-value,
                              output cName,
                              output std-lo).
  
      if not std-lo 
       then 
        do:
          assign 
              flAgentID:screen-value = "" 
              flName:screen-value    = ""
              .
          return no-apply.
        end.
      
      publish "getAgentStateID" (input flAgentID:input-value,
                                 output cStateID,
                                 output std-lo).
      
      /* Set default AgentID */
      if lDefaultAgent 
       then
        publish "SetDefaultAgent" (input self:input-value).
  
      publish "GetAgentInvoiceDue" (input self:input-value,
                                    output cInvoiceDue,
                                    output iDays).
    end. 
  
  flName:screen-value = cName.
  
  apply 'value-changed' to flTranDate.                                
  
  run enableDisableSave in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID Dialog-Frame
ON VALUE-CHANGED OF flAgentID IN FRAME Dialog-Frame /* Agent ID */
DO:
  flName:screen-value = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAmount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAmount Dialog-Frame
ON VALUE-CHANGED OF flAmount IN FRAME Dialog-Frame /* Amount */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flDueDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flDueDate Dialog-Frame
ON VALUE-CHANGED OF flDueDate IN FRAME Dialog-Frame /* Due */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flFile Dialog-Frame
ON VALUE-CHANGED OF flFile IN FRAME Dialog-Frame /* File Number */
DO:
  tbFileBased:sensitive = (ipcType = {&Credit} and flFile:screen-value <> "").
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flReference
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flReference Dialog-Frame
ON VALUE-CHANGED OF flReference IN FRAME Dialog-Frame /* Reference */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flRevType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flRevType Dialog-Frame
ON VALUE-CHANGED OF flRevType IN FRAME Dialog-Frame /* Revenue Type */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flSourceID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flSourceID Dialog-Frame
ON VALUE-CHANGED OF flSourceID IN FRAME Dialog-Frame /* Source Number */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flTranDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flTranDate Dialog-Frame
ON VALUE-CHANGED OF flTranDate IN FRAME Dialog-Frame /* Invoiced On */
DO:
  if ipcType = {&Invoice}
   then
    do:
      std-da = ?.
      if cInvoiceDue = {&Invdueindays}
       then
        std-da = datetime(date(flTranDate:input-value) + iDays) no-error.
       else if cInvoiceDue = {&Invdueonday}
        then
         do:
           if month(date(flTranDate:input-value)) = 12 
            then
             do:
               std-da = datetime("1/" + string(iDays) + "/" +  string(year(date(flTranDate:input-value)) + 1)) no-error.
               if error-status:error 
                then
                 std-da = datetime("2/1/" + string(year(date(flTranDate:input-value)) + 1)) no-error.
             end.  
            else
             do:
               std-da = datetime(string(month(date(flTranDate:input-value)) + 1 ) + "/" + string(iDays) + "/" +  string(year(date(flTranDate:input-value)))) no-error. 
               if error-status:error 
                then
                 do:
                   std-da = datetime(string(month(date(flTranDate:input-value)) + 2 ) + "/" + "1/" + string(year(date(flTranDate:input-value)))) no-error. 
                   if error-status:error 
                    then std-da = datetime("1/1/" + string(year(date(flTranDate:input-value)) + 1)) no-error. 
                 end. 
             end.    
         end.
    
      if std-da = ? 
       then
        for first sysprop:
          std-da = datetime(date(flTranDate:input-value) + integer(sysprop.objValue)) no-error.
        end.

      flDueDate:screen-value = string(std-da) no-error.      
    end. /* if ipcType = {&Invoice} */
  run enableDisableSave in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tbFileBased
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tbFileBased Dialog-Frame
ON VALUE-CHANGED OF tbFileBased IN FRAME Dialog-Frame
DO:  
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAction
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Get current logged in username & UID */ 
publish "GetCredentialsName" (output cCurrentUser).
publish "GetCredentialsID"   (output cCurrentUID).

publish "GetSysProp" (input "ARM",
                      input "InvoiceDueDays",
                      input "",
                      output table sysProp).

bSysCode     :load-image("images/s-lookup.bmp").
bSysCode     :load-image-insensitive("images/s-lookup-i.bmp").
bAgentLookup :load-image("images/s-lookup.bmp").
bAgentLookup :load-image-insensitive("images/s-lookup-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  run displayData in this-procedure.
  
  if ipcType = {&Invoice} 
   then
     case ipcAction:
      when {&add} then Frame Dialog-Frame:Title = "New Invoice".
      when {&Modify} then Frame Dialog-Frame:Title = "Edit Invoice".
      when {&view} then Frame Dialog-Frame:Title = "View Invoice".
      when {&ModifyPosted} then Frame Dialog-Frame:Title = "Edit Invoice Note".
     end case.
  else if ipcType = {&Credit} then
     case ipcAction:
      when {&add} then Frame Dialog-Frame:Title = "New Credit".
      when {&Modify} then Frame Dialog-Frame:Title = "Edit Credit".
      when {&view} then Frame Dialog-Frame:Title = "View Credit".
      when {&ModifyPosted} then Frame Dialog-Frame:Title = "Edit Credit Note".
     end case.
     
  if ipcAction = {&view} or ipcAction = {&ModifyPosted} 
   then  
    run setWidgetState in this-procedure.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
      
  assign
      flType:screen-value          = if ipcType = {&Invoice} then "Invoice" else "Credit"      
      cbSource:screen-value        = ""
      fMarkMandatory1:screen-value = {&Mandatory}
      fMarkMandatory2:screen-value = {&Mandatory}
      fMarkMandatory3:screen-value = {&Mandatory}
      fMarkMandatory4:screen-value = {&Mandatory}
      .      
     
   find first ttARMisc no-error.
   if available ttARMisc 
    then
     do:
       if ttARMisc.arMiscID = 0    /* Copy */
        then
         assign
             cbSource:screen-value         = ttARMisc.sourcetype
             flSourceID:screen-value       = ttARMisc.sourceID
             flReference:screen-value      = ttARMisc.reference
             flRevType:screen-value        = ttARMisc.revenuetype
             flTranDate:screen-value       = string(ttARMisc.creditInvoiceDt)
             btn_OK:label                  = {&Create}
             btn_OK:tooltip                = {&Create}
             .     
        else                     /* modify */
         do:
           assign
               flInvoiceID:screen-value      = string(ttARMisc.arMiscID)
               cbSource:screen-value         = ttARMisc.sourcetype
               flFile:screen-value           = ttARMisc.filenumber
               flDueDate:screen-value        = string(ttARMisc.duedate)
               flTranDate:screen-value       = string(ttARMisc.creditInvoiceDt)
               flAmount:screen-value         = string(ttARMisc.transamt)
               flReference:screen-value      = ttARMisc.reference          
               flSourceID:screen-value       = ttARMisc.sourceID
               flRevType:screen-value        = ttARMisc.revenuetype
               edNotes:screen-value          = ttARMisc.notes
               flAgentID:screen-value        = ttARMisc.entityID
               flName:screen-value           = ttArMisc.agentName
               flAgentID:sensitive           = false
               bAgentLookup:sensitive        = false
               btn_OK:label                  = {&Save}
               btn_OK:tooltip                = {&Save}          
               . 
         
           publish "getAgentName" (input flAgentID:input-value,
                                   output std-ch,
                                   output std-lo).
        
           publish "getAgentStateID" (input flAgentID:input-value,
                                      output cStateID,
                                      output std-lo).
        
           assign
               flName:screen-value   = std-ch      
               tbFileBased:checked   = (ttARMisc.transtype = {&File})
               tbFileBased:sensitive = (ipcType = {&Credit} and flFile:screen-value <> "")
               .
        
           /* ttAction table to show details of created, posted and voided in dialog*/
           create ttAction.
           assign
               ttAction.action       = "Created"
               ttAction.createdBy    = ttARMisc.username
               ttAction.CreatedDate  = if (ttARMisc.createdate = ?) then "" else string(ttARMisc.createdate)
               ttAction.ledgerID     = "".
           create ttAction.
           assign
               ttAction.action       = "Posted"
               ttAction.createdBy    = ttARMisc.postByName
               ttAction.CreatedDate  = if (ttARMisc.TransDate = ?) then "" else string(ttARMisc.TransDate)
               ttAction.ledgerID     = if (ttARMisc.ledgerID = 0 ) then "" else string(ttARMisc.ledgerID).
           create ttAction.
           assign
               ttAction.action       = "Void"
               ttAction.createdBy    = ttARMisc.voidByName
               ttAction.CreatedDate  = if (ttARMisc.voidDate = ?) then "" else string(ttARMisc.voidDate)
               ttAction.ledgerID     = if (ttARMisc.voidLedgerID = 0) then "" else string(ttARMisc.voidLedgerID).
         
           open query brwAction for each ttAction.
           brwAction:Deselect-focused-row().
       end.
     end.  
   else
      do:                         /* new */
        assign
            btn_OK:label               = {&Create}
            btn_OK:tooltip             = {&Create}        
            .
      
        /* Get default StateID and AgentID */
        run getDefaultValues in this-procedure.
      end. 
    
   
  /* Keep the copy of initial values. Required in enableDisableSave. */ 
  assign                 
      cSource        = cbSource:input-value        
      cSourceID      = flSourceID:input-value      
      cFileNumber    = flFile:input-value   
      deAmount       = flAmount:input-value
      cReferenceNo   = flReference:input-value 
      dtTranDate     = flTranDate:input-value         
      dtDueDate      = flDueDate:input-value         
      cAgentID       = flAgentID:input-value      
      cNotes         = edNotes:input-value
      cRevenueType   = flRevType:input-value
      lFileBased     = tbFileBased:checked
      .
    
  apply 'value-changed' to cbSource.
  apply 'value-changed' to flTranDate.  
      
  /* Modify screen on the basis of Action:New/Modify */
  run resetScreenLayout in this-procedure.
  
  /* Change screen labels based on Type : Invoice/Credit */
  run resetLabels in this-procedure.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  if ipcAction = {&add}
   then
    Btn_OK:sensitive = not (flRevType:input-value = ""
                            or
                            flTranDate:input-value = ?                             
                            or 
                            flAmount:input-value = 0
                           ) no-error.
   else if ipcAction = {&Modify} or ipcAction = {&ModifyPosted} /* when (M)odifyData*/
    then             
    Btn_OK:sensitive = not (cSource       = cbSource:input-value     and
                            cSourceID     = flSourceID:input-value   and
                            cFileNumber   = flFile:input-value       and                            
                            dtTranDate    = flTranDate:input-value   and
                            dtDueDate     = flDueDate:input-value    and                            
                            cAgentID      = flAgentID:input-value    and
                           (deAmount      = flAmount:input-value     or
                            flAmount:input-value = 0)                and
                            cReferenceNo  = flReference:input-value  and
                            cRevenueType  = flRevType:input-value    and
                            cNotes        = edNotes:input-value      and
                            lFileBased    = tbFileBased:checked                            
                            ) no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flAgentID flInvoiceID flName flType tbFileBased cbSource flFile 
          flSourceID flLabel flRevType flReference flTranDate flAmount flDueDate 
          edNotes fMarkMandatory2 fMarkMandatory1 fMarkMandatory3 
          fMarkMandatory4 
      WITH FRAME Dialog-Frame.
  ENABLE flAgentID bAgentLookup flInvoiceID flName flType cbSource flFile 
         flSourceID flRevType bSysCode flReference flTranDate flAmount 
         flDueDate edNotes Btn_Cancel RECT-80 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getDefaultValues Dialog-Frame 
PROCEDURE getDefaultValues :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  publish "getDefaultPostingDate"(output dtTranDate).
  
  flTranDate:screen-value = string(dtTranDate).
  
  publish "GetAutoDefaultAgent" (output lDefaultAgent).
  
  if lDefaultAgent
   then
    do:
      /* Get default AgentID */
      publish "GetDefaultAgent"  (output cAgentID).
         
      flAgentID:screen-value = cAgentID.
      
      publish "getAgentName" (input flAgentID:input-value,
                              output std-ch,
                              output std-lo).
      
      publish "getAgentStateID" (input flAgentID:input-value,
                                 output cStateID,
                                 output std-lo).
      
      flName:screen-value = std-ch.        
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resetLabels Dialog-Frame 
PROCEDURE resetLabels :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
          
  assign
      flLabel:width        = if ipcType  = {&Invoice} then 7.40             else 5.80
      flLabel:screen-value = if ipcType  = {&Invoice} then "Invoice"        else "Credit"
      flTranDate:label     = if ipcType  = {&Invoice} then "Invoiced On"    else "Credited On"
      flInvoiceID:label    = if ipcType  = {&Invoice} then "Invoice ID"     else "Credit ID"
      flDueDate:hidden     = not ipcType = {&Invoice}
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resetScreenLayout Dialog-Frame 
PROCEDURE resetScreenLayout :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
      
  if ipcAction = {&add}
   then
    assign
        Btn_OK:row                    = 16
        Btn_Cancel:row                = 16
        browse brwAction:row          = 1
        frame Dialog-frame:height     = 18
        frame Dialog-frame:scrollable = false
        flInvoiceID:hidden            = true
        brwAction:hidden              = true
        .
   else     
    assign
        browse brwAction:row      = 16
        frame Dialog-frame:height = 22.4
        Btn_OK:row                = 20.4
        Btn_Cancel:row            = 20.4
        flInvoiceID:hidden        = false
        brwAction:hidden          = false
        . 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveArInvoice Dialog-Frame 
PROCEDURE saveArInvoice :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lApplyToFile   as logical   no-undo.
  define variable cTransID       as character no-undo.
  define variable dtCreatedDate  as datetime  no-undo.
 
  find first ttARMisc no-error.
  if available ttARMisc 
   then
    do:
      if ttARMisc.arMiscID = 0    /* Copy Invoice*/
       then
        empty temp-table ttARMisc.
    end.
  if ipcAction = {&add} 
   then
    create ttARMisc.
    
  assign        
      ttARMisc.sourcetype      = cbSource:input-value in frame {&frame-name}
      ttARMisc.sourceID        = flSourceID:input-value
      ttARMisc.filenumber      = flFile:input-value
      ttARMisc.fileID          = normalizeFileID(input flFile:input-value)
      ttARMisc.type            = ipcType
      ttARMisc.creditinvoicedt = flTranDate:input-value      
      ttARMisc.duedate         = flDueDate:input-value
      ttARMisc.entity          = {&Agent}
      ttARMisc.entityID        = flAgentID:input-value            
      ttARMisc.notes           = edNotes:input-value
      ttARMisc.transamt        = flAmount:input-value
      ttARMisc.reference       = flReference:input-value
      ttARMisc.revenuetype     = flRevType:input-value
      ttARMisc.transtype       = if tbFileBased:checked then {&file} else {&None}
      .
                          
  if ipcAction = {&Modify}  or ipcAction = {&ModifyPosted}
   then
    do:
      /* Client Server Call */
      run server/modifyinvoice.p (input table ttARMisc,
                                  output lApplyToFile,
                                  output oplSuccess,
                                  output std-ch).
      ttARMisc.transType = (if lApplyToFile then {&File} else {&None}).                                  
    end.                              
   else /* when (N)ewData */
    do:
      /* client Server Call */
      run server/newinvoice.p (input table ttARMisc,                           
                               output std-in,  /* ArTempID */                              
                               output cTransID,
                               output lApplyToFile,
                               output dtCreatedDate,
                               output oplSuccess,
                               output std-ch).
                              
      assign
          ttARMisc.arMiscID    = std-in
          ttARMisc.transID     = cTransID
          ttARMisc.transType   = (if lApplyToFile then {&File} else {&None})
          ttARMisc.username    = cCurrentUser
          ttARMisc.createdby   = cCurrentUID
          ttARMisc.createDate  = dtCreatedDate
          .                            
    end.                          
    
  if not oplSuccess 
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.  
    end.
    
  publish "getAgentName" (input ttARMisc.entityID,
                          output ttARMisc.agentname,
                          output std-lo).
                          
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState Dialog-Frame 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  assign 
    flAgentID:read-only          = true
    flType:read-only             = true
    cbSource:sensitive           = false
    flSourceID:read-only         = true
    tbFileBased:sensitive        = false
    flFile:read-only             = true
    bAgentLookup:sensitive       = false
    bSysCode:sensitive           = false
    flReference:read-only        = true
    flAmount:read-only           = true
    flTranDate:read-only         = true
    flDueDate:read-only          = true
    edNotes:read-only            = if ipcAction = {&view} then true else false
    flInvoiceID:read-only        = true 
    btn_OK:sensitive             = false
    Btn_Cancel:sensitive         = true 
    no-error. 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validateArInvoice Dialog-Frame 
FUNCTION validateArInvoice RETURNS LOGICAL
  ( ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if flAgentID:input-value = "" 
   then
    do:
      message "Agent ID cannot be blank."
        view-as alert-box error buttons ok.
      return false.
    end.
    
  if flTranDate:input-value = ? 
   then
    do:
      message flType:screen-value  + " date cannot be blank."
        view-as alert-box error buttons ok.
      return false.
    end.  
  
  if flRevType:input-value = "" 
   then
    do:
      message "Revenue type cannot be blank."
        view-as alert-box error buttons ok.
      return false.
    end.
 
  if flAmount:input-value = 0  or flAmount:input-value = ?
   then
    do:
      message "Amount cannot be zero or ?."
        view-as alert-box error buttons ok.
      return false.
    end.
    
 
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

