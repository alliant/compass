&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wPersons.w

  Description:Person Maintainance

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Shubham

  Created: 05.14.2020   
  
  Modified:
    08/16/2021   MK       Added tooltip to the lock button.
------------------------------------------------------------------------*/
create widget-pool.

{tt/artran.i &tableAlias="tartranref"}
{tt/artran.i &tableAlias="artranref"}
{tt/artran.i &tableAlias="ttartranref"}
{tt/artran.i &tableAlias="tartraninv"}
{tt/artran.i &tableAlias="artraninv"}

define variable selectedAgent         as character no-undo.
define variable iOldSelectedInvoice   as integer   no-undo.
define variable iSelectedInvoice      as integer   no-undo.
define variable iSelectedInvoiceRowID as rowid     no-undo.
define variable lLocked               as logical   no-undo.
define variable lGetData              as logical   no-undo.
define variable iSelectedRef          as integer   no-undo.
define variable iSelectedRefRowID     as rowid     no-undo.
define variable tWhereClause          as character no-undo.

{lib/std-def.i}
{lib/ar-def.i}
{lib/winlaunch.i}
{lib/brw-multi-def.i}
{lib/get-column.i}
{lib/winshowscrollbars.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tartranref tartraninv

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData tartranref.selectrecord tartranref.reference "Check Number" tartranref.fileNumber "File Number" tartranref.tranDate tartranref.tranAmt tartranref.remainingAmt   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData tartranref.selectrecord   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwData tartranref
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwData tartranref
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each tartranref by tartranref.reference
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each tartranref by tartranref.reference.
&Scoped-define TABLES-IN-QUERY-brwData tartranref
&Scoped-define FIRST-TABLE-IN-QUERY-brwData tartranref


/* Definitions for BROWSE brwTranData                                   */
&Scoped-define FIELDS-IN-QUERY-brwTranData tartraninv.isRefapplied tartraninv.reference "Reference" tartraninv.filenumber "File Number" tartraninv.tranDate "Post Date" tartraninv.tranamt "Original" tartraninv.appliedamt "Applied" tartraninv.remainingamt "Remaining" tartraninv.appliedamtbyref "Apply"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwTranData tartraninv.isRefapplied   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwTranData tartraninv
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwTranData tartraninv
&Scoped-define SELF-NAME brwTranData
&Scoped-define QUERY-STRING-brwTranData for each tartraninv by tartraninv.reference
&Scoped-define OPEN-QUERY-brwTranData open query {&SELF-NAME} for each tartraninv by tartraninv.reference.
&Scoped-define TABLES-IN-QUERY-brwTranData tartraninv
&Scoped-define FIRST-TABLE-IN-QUERY-brwTranData tartraninv


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}~
    ~{&OPEN-QUERY-brwTranData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-84 bAgentLookup bLock flAgentID flName ~
brwData brwTranData flTran flRemaining flText 
&Scoped-Define DISPLAYED-OBJECTS flAgentID flName flPostingdate flTran ~
flRemaining flText 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup 
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Agent lookup".

DEFINE BUTTON bLock 
     LABEL "Lock" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bRefLookup 
     LABEL "Get" 
     SIZE 4.8 BY 1.14 TOOLTIP "Reload data".

DEFINE BUTTON btPost  NO-FOCUS
     LABEL "Post" 
     SIZE 7.2 BY 1.71 TOOLTIP "Post changes".

DEFINE VARIABLE flAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 10.6 BY 1 NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 77.4 BY 1 NO-UNDO.

DEFINE VARIABLE flPostingdate AS DATE FORMAT "99/99/99":U 
     LABEL "Use Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE flRemaining AS DECIMAL FORMAT "-z,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 19.4 BY 1 NO-UNDO.

DEFINE VARIABLE flText AS CHARACTER FORMAT "X(256)":U INITIAL "Total of 0 payments :" 
      VIEW-AS TEXT 
     SIZE 24.6 BY .62
     FONT 6 NO-UNDO.

DEFINE VARIABLE flTran AS DECIMAL FORMAT "-z,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-84
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 36.8 BY 2.29.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      tartranref SCROLLING.

DEFINE QUERY brwTranData FOR 
      tartraninv SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      tartranref.selectrecord                     column-label  "Select" width 6 view-as toggle-box
tartranref.reference  label     "Check Number"       format "x(25)"           
tartranref.fileNumber       label     "File Number"     format "x(25)" 
tartranref.tranDate         column-label  "Post Date"   format "99/99/99"
tartranref.tranAmt          column-label  "Total"       format ">>>,>>>,>>9.99"  width 20
tartranref.remainingAmt     column-label  "Unapplied"   format "->>,>>>,>>9.99"  width 20
enable tartranref.selectrecord
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE SIZE 112 BY 17.86
         TITLE "Payments" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN TOOLTIP "Select multiple payments to apply to the invoice".

DEFINE BROWSE brwTranData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwTranData C-Win _FREEFORM
  QUERY brwTranData DISPLAY
      tartraninv.isRefapplied                     column-label  "Select" width 6 view-as toggle-box
tartraninv.reference                        label         "Reference"     format "x(15)" 
tartraninv.filenumber                       label         "File Number"   format "x(15)" 
tartraninv.tranDate                         label         "Post Date"     format "99/99/99" width 10
tartraninv.tranamt                          label         "Original"      format "->>>,>>>,>>9.99"
tartraninv.appliedamt                       label         "Applied"       format "->>>,>>>,>>9.99"
tartraninv.remainingamt                     label         "Remaining"     format "->>>,>>>,>>9.99" 
tartraninv.appliedamtbyref                  label         "Apply"         format ">>>,>>>,>>9.99"
enable tartraninv.isRefapplied
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 117 BY 17.86
         TITLE "Miscellaneous Invoices" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN TOOLTIP "Select a single invoice".


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bAgentLookup AT ROW 1.91 COL 25.6 WIDGET-ID 350
     btPost AT ROW 1.67 COL 227.2 WIDGET-ID 304 NO-TAB-STOP 
     bRefLookup AT ROW 1.91 COL 107.8 WIDGET-ID 122 NO-TAB-STOP 
     bLock AT ROW 1.91 COL 112.4 WIDGET-ID 356 NO-TAB-STOP 
     flAgentID AT ROW 2 COL 13 COLON-ALIGNED WIDGET-ID 66 NO-TAB-STOP 
     flName AT ROW 2 COL 28.4 COLON-ALIGNED NO-LABEL WIDGET-ID 88 NO-TAB-STOP 
     flPostingdate AT ROW 2 COL 207.6 COLON-ALIGNED WIDGET-ID 296
     brwData AT ROW 4 COL 116 RIGHT-ALIGNED WIDGET-ID 200
     brwTranData AT ROW 4 COL 234.4 RIGHT-ALIGNED WIDGET-ID 300
     flTran AT ROW 22.14 COL 74.4 NO-LABEL WIDGET-ID 204 NO-TAB-STOP 
     flRemaining AT ROW 22.14 COL 94.4 NO-LABEL WIDGET-ID 208 NO-TAB-STOP 
     flText AT ROW 22.29 COL 49.4 NO-LABEL WIDGET-ID 372 NO-TAB-STOP 
     "Post" VIEW-AS TEXT
          SIZE 4.6 BY .62 AT ROW 1.1 COL 199.4 WIDGET-ID 366
     RECT-84 AT ROW 1.43 COL 198.6 WIDGET-ID 336
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1.05
         SIZE 253 BY 22.76 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Bulk Apply to Single Invoice"
         HEIGHT             = 22.43
         WIDTH              = 237.8
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         MAX-BUTTON         = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData flPostingdate fMain */
/* BROWSE-TAB brwTranData brwData fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bRefLookup IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BROWSE brwData IN FRAME fMain
   ALIGN-R                                                              */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BROWSE brwTranData IN FRAME fMain
   ALIGN-R                                                              */
ASSIGN 
       brwTranData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwTranData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON btPost IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       flName:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN flPostingdate IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flRemaining IN FRAME fMain
   ALIGN-L                                                              */
ASSIGN 
       flRemaining:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN flText IN FRAME fMain
   ALIGN-L                                                              */
ASSIGN 
       flText:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN flTran IN FRAME fMain
   ALIGN-L                                                              */
ASSIGN 
       flTran:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each tartranref by tartranref.reference.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwTranData
/* Query rebuild information for BROWSE brwTranData
     _START_FREEFORM
open query {&SELF-NAME} for each tartraninv by tartraninv.reference.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwTranData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Bulk Apply to Single Invoice */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Bulk Apply to Single Invoice */
do:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup C-Win
ON CHOOSE OF bAgentLookup IN FRAME fMain /* agentlookup */
DO:
  define variable cAgentID  as character no-undo.
  define variable cName     as character no-undo.
  define variable cStateID  as character no-undo.
    
  run dialogagentlookup.w(input flAgentID:input-value,
                          input "",        /* Selected State ID */
                          input false,     /* Allow 'ALL' */
                          output cAgentID,
                          output cStateID, /* Agent state ID */
                          output cName,
                          output std-lo).
   
  if not std-lo
   then
    return no-apply.
     
  assign
      flAgentID:screen-value    = cAgentID
      flName:screen-value       = cName
      bRefLookup:sensitive      = (flAgentID:input-value <> "") 
      . 
   
  if selectedAgent <> cAgentID  
   then
    selectedAgent = flAgentID:input-value.  
    
  pause 0.1. 
  
  bRefLookup:sensitive = true.
  
  apply 'choose' to bRefLookup.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefLookup C-Win
ON CHOOSE OF bRefLookup IN FRAME fMain /* Get */
do:  
  if not lGetData
   then
    do:
      run lockAgent in this-procedure(output lLocked).
      if lLocked
       then
        do:          
          run getRefData      in this-procedure.
          run getTransactions in this-procedure.
          
          if lLocked
           then
            lGetData = true.
           else
            return.
            
          open query brwData for each tartranref by tartranref.reference.
          open query brwTranData for each tartraninv by tartraninv.reference.
          
          bRefLookup:load-image ("images/s-cancel.bmp").
          bRefLookup:load-image-insensitive("images/s-cancel-i.bmp").
        end.
    end.
   else
    do:
      run cancelData in this-procedure.
      
      if not std-lo
       then 
        return.
        
      lGetData = false.
      
      bRefLookup:load-image ("images/s-completed.bmp").
      bRefLookup:load-image-insensitive("images/s-completed-i.bmp").
    end.
  flAgentID   :sensitive = not lGetData.

  bAgentLookup:sensitive = not lGetData.
  run setButtons in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain /* Payments */
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain /* Payments */
do:
  define buffer tartranref for tartranref.
  
  std-ha = brwData:current-column.
  if std-ha:label = "Select"
   then
    do:     
      std-lo = can-find(first tartranref where not(tartranref.selectrecord)).   
      for each tartranref:            
        tartranref.selectrecord = std-lo.
      end. 
      
      run applyAmountOnSelect in this-procedure.
      
      browse brwData:refresh(). 
      browse brwTranData:refresh(). 
      
      run showTotal in this-procedure.   
    end.   
   else /*if std-ha:label = "Post Date"
    then
     do:
       tWhereClause = " by tartranref.tranDate". */
       {lib/brw-startSearch-multi.i}
   /*  end.  */
  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwTranData
&Scoped-define SELF-NAME brwTranData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTranData C-Win
ON ROW-DISPLAY OF brwTranData IN FRAME fMain /* Miscellaneous Invoices */
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTranData C-Win
ON START-SEARCH OF brwTranData IN FRAME fMain /* Miscellaneous Invoices */
do:
  /*std-ha = brwTranData:current-column.
  if std-ha:label = "Reference" 
   then
    do:
      tWhereClause = " by tartraninv.reference".
      {lib/brw-startSearch-multi.i}
    end.
  */
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btPost
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btPost C-Win
ON CHOOSE OF btPost IN FRAME fMain /* Post */
DO:
  /* Applying AR invoices against payment records */
  run applyAmount in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID C-Win
ON LEAVE OF flAgentID IN FRAME fMain /* Agent ID */
DO:
  if flAgentID:input-value <> "" and not validAgent()
   then 
    return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID C-Win
ON RETURN OF flAgentID IN FRAME fMain /* Agent ID */
DO:
  if not validAgent()
   then
    return no-apply.  
    
  pause 0.1. 
  
  bRefLookup:sensitive = true.
  
  apply 'choose' to bRefLookup.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}

{lib/win-status.i }
{lib/brw-main-multi.i &browse-list="brwdata,brwTranData"} 

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

on close of this-procedure 
  run disable_UI.

pause 0 before-hide.
subscribe to "closeWindow"  anywhere.

bAgentLookup :load-image("images/s-lookup.bmp").
bAgentLookup :load-image-insensitive("images/s-lookup-i.bmp").
bRefLookup   :load-image ("images/s-completed.bmp").
bRefLookup   :load-image-insensitive("images/s-completed-i.bmp").
bLock        :load-image("images/s-lock.bmp").
bLock        :load-image-insensitive("images/s-lock-i.bmp").
btPost       :load-image ("images/check.bmp").
btPost       :load-image-insensitive("images/check-i.bmp").

bLock:tooltip = "Agent is locked".

/* Trigger for auto applying amount on the selected row against payment/credit */   
on value-changed of tartraninv.isRefapplied in browse brwTranData
do:
  run selectInvoice in this-procedure.
  run applyAmountOnSelect in this-procedure.
  iOldSelectedInvoice = iSelectedInvoice.
end.

/* Trigger for auto applying amount on the selected row against payment/credit */   
on value-changed of tartranref.selectrecord in browse brwData
do:
  assign
       iSelectedRef      = tartranref.artranID
       iSelectedRefRowID = rowid(tartranref)
       .

  for first tartranref
    where tartranref.artranID = iSelectedRef:
    if rowid(tartranref) = iSelectedRefRowID 
     then
      tartranref.selectrecord = not tartranref.selectrecord.  
     else 
      tartranref.selectrecord = false. 
  end. /* for each tartranref */
  
  run applyAmountOnSelect in this-procedure.

  if can-find (first tartraninv)
   then
    browse brwTranData:refresh(). 
 
  run showTotal in this-procedure.
  apply 'value-changed' to browse brwData.
end.
 
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
 
  run enable_UI.
  
  bLock:hidden = true.
  
  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.
  
  apply 'entry' to flAgentID.
   
  if not this-procedure:persistent 
   then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE applyAmount C-Win 
PROCEDURE applyAmount :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable dcTotalRef as decimal no-undo.
  
  do with frame {&frame-name}:
  end.
      
  empty temp-table ttartranref.
  
  if not can-find (first tartraninv 
                     where tartraninv.isrefapplied 
                       and tartraninv.appliedamtbyref > 0)
   then
    return.
    
  if flPostingDate:input-value = ?
   then
    do:
      message "Post date cannot be blank."
          view-as alert-box error buttons ok.
      apply 'entry' to flPostingDate.  
      return.      
    end.
  
  if flPostingDate:input-value > today
   then
    do:
      message "Post date cannot be in future."
          view-as alert-box error buttons ok.
      apply 'entry' to flPostingDate.     
      return.
    end.
  
  publish "validatePostingDate" (input flPostingDate:input-value,
                                 output std-lo).
  
  if not std-lo
   then
    do:
      message "Apply post date must be within an open period."
          view-as alert-box error buttons ok.
      apply 'entry' to flPostingDate.     
      return.
    end.         
  
  for each tartranref 
    where tartranref.selectrecord = true
      and tartranref.stat = 'P':
    dcTotalRef = dcTotalRef + tartranref.appliedamtbyref.
    create ttartranref.
    buffer-copy tartranref to ttartranref.
  end.
  
  for first artraninv
    where artraninv.artranID = iSelectedInvoice:
    if artraninv.remainingamt < dcTotalRef
     then
      do:
        message "Amount to apply of $" + string(dcTotalRef) + " exceeds the outstanding invoice amount of $" + string(artraninv.remainingamt) + ". Continue?" 
            view-as alert-box question buttons yes-no update std-lo.
        if not std-lo
         then 
          return.
      end.
  end.
  
  for each ttartranref:  
    if flPostingDate:input-value < date(ttartranref.trandate)
     then
      do:
        message "Apply post date cannot be prior to the payment post date"
            view-as alert-box error buttons ok.
        apply 'entry' to flPostingDate.    
        return.    
      end.
  end.
  
  run server/bulkapplypayment.p (input iSelectedInvoice,          /* Misc. Invoice ArtanID */
                                 input flPostingdate:input-value, /* Posting Date */               
                                 input table ttartranref,                             
                                 output std-lo,
                                 output std-ch).
 
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
   else
    do:
      message "Payment(s) are successfuly applied to the invoice" 
          view-as alert-box.
      empty temp-table tartranref.
      empty temp-table tartraninv.
      lLocked = false.
      apply 'choose' to bRefLookup.
    end.
    
  open query brwTranData for each tartraninv by tartraninv.reference.
  open query brwData     for each tartranref by tartranref.reference.
  run showtotal in this-procedure. 
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE applyAmountOnSelect C-Win 
PROCEDURE applyAmountOnSelect :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable dcTotalApply as decimal no-undo.
  
  define buffer ttartranref for tartranref.
  
  for first tartraninv
    where tartraninv.artranId = iOldSelectedInvoice:
    assign
        tartraninv.appliedamt      = tartraninv.appliedamt   - tartraninv.appliedamtbyref           
        tartraninv.remainingamt    = tartraninv.remainingamt + tartraninv.appliedamtbyref
        tartraninv.appliedamtbyref = 0
        tartraninv.fullypaid       = tartraninv.remainingamt  = 0
        .
     
    for each ttartranref by ttartranref.reference:
      assign
          ttartranref.appliedamtbyref = 0
          ttartranref.remainingamt    = ttartranref.remainingamt + ttartranref.appliedamtbyref
          ttartranref.appliedamt      = ttartranref.appliedamt   - ttartranref.appliedamtbyref         
          ttartranref.fullypaid       = ttartranref.remainingamt = 0
          ttartranref.stat            = ""
          .
    end.        
  end.

  for first tartraninv 
    where tartraninv.artranId = iSelectedInvoice:
    if tartraninv.isrefapplied
     then
      do:
        blk-Payment:
        for each ttartranref by ttartranref.reference:
          if ttartranref.selectrecord 
           then
            do:
              if ttartranref.remainingamt > 0
               then
                do:
                  assign
                      tartraninv.appliedamtbyref = tartraninv.appliedamtbyref + ttartranref.remainingamt                                             
                      tartraninv.appliedamt      = tartraninv.appliedamt      + ttartranref.remainingamt                                   
                      tartraninv.remainingamt    = tartraninv.remainingamt    - ttartranref.remainingamt
                      tartraninv.fullypaid       = tartraninv.remainingamt    = 0 
                      ttartranref.fullypaid      = ttartranref.remainingamt   = 0
                      .
                end.
              assign  
                  ttartranref.appliedamt      = ttartranref.appliedamt   + ttartranref.remainingamt
                  ttartranref.appliedamtbyref = ttartranref.remainingamt
                  ttartranref.stat            = 'P'
                  .
            end.        
        end.
      end.
  end.
    
  open query brwTranData for each tartraninv by tartraninv.reference.

  run showtotal in this-procedure.
  run setButtons in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancelData C-Win 
PROCEDURE cancelData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if can-find (first tartranref where tartranref.selectrecord) or 
     can-find (first tartraninv where tartraninv.isrefapplied) 
   then
    do:
      message "Changes are pending to save. Want to continue?"                      
          view-as alert-box warning buttons yes-no update std-lo.
      
      if not std-lo /* if dont want to change the status */
       then 
        return.
    end.
    
  if lLocked
   then
    run UnlockAgent in this-procedure.
   else
    bLock:hidden = not lLocked.
    
  run clearScreen in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clearScreen C-Win 
PROCEDURE clearScreen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end.
  
  empty temp-table tartranref.
  empty temp-table tartraninv.
  
  close query brwdata.
  close query brwtrandata.
  
  flTran:screen-value = "".
  flRemaining:screen-value = "".
  flText:screen-value = "Total of 0 payments :".
  iOldSelectedInvoice = 0.
  apply 'entry' to flAgentID.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if can-find (first tartranref where tartranref.selectrecord) 
   or can-find (first tartraninv where tartraninv.isrefapplied) 
   then
    do:
      message "Changes are pending to save. Want to continue?"                      
          view-as alert-box warning buttons yes-no update std-lo.
      
      if not std-lo /* if dont want to change the status */
       then 
        return.
    end.
    
  if lLocked
   then
    run UnlockAgent in this-procedure.

  apply "CLOSE":U to this-procedure.  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flAgentID flName flPostingdate flTran flRemaining flText 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-84 bAgentLookup bLock flAgentID flName brwData brwTranData flTran 
         flRemaining flText 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getRefData C-Win 
PROCEDURE getRefData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
   
  run server/getpostedtransactions.p (input  0,                        /* arTranID */
                                      input  {&Agent}, 
                                      input  flAgentID:input-value,
                                      input  'P',                      /* (P)ayments */
                                      input  "",
                                      input  false,                    /* IncludeAll */
                                      input  ?,                        /* FromPostDate */
                                      input  ?,                        /* ToPostDate */
                                      output table artranref,
                                      output std-lo,
                                      output std-ch).
  
  if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box error buttons ok.
      return.
    end.
   
  empty temp-table tartranref.
  
  for each artranref:
    create tartranref.
    buffer-copy artranref to tartranref.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getTransactions C-Win 
PROCEDURE getTransactions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  if flAgentID:input-value = ""
   then
    do:
      message "Please select valid agent first" 
          view-as alert-box error buttons ok.   
      return.
    end.

  run server\gettransactions.p( input 0,                            /* ArTranID */
                                input {&agent},                     /* Entity */
                                input flAgentID:input-value,        /* Entity ID */                              
                                input false,                        /* Include Files */
                                input false,                        /* Include Fully Paid Files */
                                input true,                         /* Include Invoices */
                                input false,                        /* Include Void Invoices */
                                input false,                        /* Include Fully Paid Invoices */                                
                                input false,                        /* Include Credits */
                                input false,                        /* Include Void Credits */
                                input false,                        /* Include Fully Applied Credits */                                 
                                input false,                        /* Include Payments */
                                input false,                        /* Include Void Payments */
                                input false,                        /* Include Fully Applied Payments */                                
                                input false,                        /* Include Refunds */
                                input false,                        /* Include Void Refunds */ 
                                input ?,                            /* from date */
                                input ?,                            /* To date   */
                                output table artraninv,                               
                                output std-lo,
                                output std-ch).    
                                
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
  
  /* Populating client side filters */
  for each artraninv:
    assign
        artraninv.appliedamt      = artraninv.tranamt - artraninv.remainingamt        
        artraninv.appliedamtbyref = if artraninv.appliedamtbyref < 0 then absolute(artraninv.appliedamtbyref) else artraninv.appliedamtbyref
        artraninv.isRefapplied    = artraninv.appliedamtbyref > 0        
        . 
    create tartraninv.
    buffer-copy artraninv to tartraninv.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE lockAgent C-Win 
PROCEDURE lockAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter lAgentLock as logical no-undo.
  
  define variable ipcLockedByUser as character.
    
  do with frame {&frame-name}:
  end.
  
  if flAgentID:input-value = "" 
   then 
    return.
   
  run server\locktransaction.p(input  {&ArAgent},
                               input  flAgentID:input-value, 
                               output ipcLockedByUser,
                               output lAgentLock,
                               output std-ch).
  
          
  if lAgentLock = ? 
   then
    lAgentLock = false.
 
  if lAgentLock and ipcLockedByUser > ""
   then
    do:
      message "Agent is locked by " + ipcLockedByUser + "."
        view-as alert-box info buttons ok.
      lAgentLock = false.
    end.
   else if not lAgentLock
    then
     message "Agent is locked by " + ipcLockedByUser + "."
        view-as alert-box info buttons ok.

  bLock:hidden = not lAgentLock.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE selectInvoice C-Win 
PROCEDURE selectInvoice :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign
      iSelectedInvoice      = tartraninv.artranID
      iSelectedInvoiceRowID = rowid(tartraninv)
      .
  
  for each tartraninv:
    if tartraninv.artranID = iSelectedInvoice
     then
      do:
        if rowid(tartraninv) = iSelectedInvoiceRowID
         then
          tartraninv.isRefApplied = not tartraninv.isRefApplied.  
         else 
          tartraninv.isRefApplied = false. 
      end.
     else
      tartraninv.isRefApplied = false. 
  end. /* for each tartraninv */
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setButtons C-Win 
PROCEDURE setButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if can-find (first tartraninv where tartraninv.isrefapplied
                                  and tartraninv.appliedamtbyref > 0)
   then
    do:
      flPostingdate:sensitive = true.
      btPost:sensitive        = true.
    end.
   else
    do:
      flPostingdate:sensitive = false.
      btPost:sensitive        = false.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showTotal C-Win 
PROCEDURE showTotal :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable dcTranTotal  as decimal no-undo.
  define variable dcUnAppTotal as decimal no-undo.
  define variable iCount       as integer no-undo.
  
  for each tartranref 
    where tartranref.selectrecord:
    assign
        dcTranTotal  = dcTranTotal  + tartranref.tranAmt   
        dcUnAppTotal = dcunAppTotal + tartranref.remainingAmt
        iCount = iCount + 1.
        .
  end.
  
  flText:screen-value      = "Total of " + string(iCount) + " payments :".
  flTran:screen-value      = if dcTranTotal  = 0 then "" else string(dcTranTotal).
  flRemaining:screen-value = if dcUnAppTotal = 0 then "" else string(dcUnAppTotal).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i } 
  /* &post-by-clause=" + tWhereClause" */
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE UnlockAgent C-Win 
PROCEDURE UnlockAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lAgentUnLock as logical.
  
  do with frame {&frame-name} :
  end.
  
  if flAgentID:input-value = ""
   then
    return.
    
  run server\unlocktransaction.p({&ArAgent},
                                 input flAgentID:input-value,
                                 output lAgentUnLock,
                                 output std-ch).
                                 
  /* Hide locking images when unlock is successful */
  if lAgentUnLock 
   then
    assign
        lLocked             = false
        bLock:hidden        = if lAgentUnLock then true else bLock:hidden
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cName  as character no-undo.
  
  do with frame {&frame-name}:
  end.
  
  if flAgentID:input-value = ""
   then 
    return false. /* Function return value. */

  else if flAgentID:input-value <> ""
   then
    do:
      publish "getAgentName" (input flAgentID:input-value,
                              output cName,
                              output std-lo).
  
      if not std-lo 
       then 
        do:
          assign 
              flAgentID:screen-value = "" 
              flName:screen-value    = ""
              .
          return false. /* Function return value. */
        end.
        
      flName:screen-value = cName.                                                              
    end.
  
  bRefLookup:sensitive = (flAgentID:input-value <> "").
          
  if flAgentID:input-value <> selectedAgent
   then
    selectedAgent   = flAgentID:input-value.              

  return true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

