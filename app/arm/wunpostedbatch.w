&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wunpostedbatch.w

  Description: Window for AR unposted credit transactions

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Anjly

  Created: 07-24-2020
  Modified:
  Date           Name        Description 
  01/14/2021     Shefali     Modified to add pop-up menu "View Batch Detail"
                             and action "View batch detail".
  02/03/2021     Shefali     Modified to show the total amount of the numeric fields"
  02/15/2021     AG          Enable filters if there is any data in temp-table
  02/17/2021     Shubham     Modified for the batch queue.
  01/21/2022     SC          Assigned value for rpt-destination as 'C' to validate 
                             sysdests before queueing batches.
  06/16/2022     SA          Task:95293 Added logic to display reference field in browser.			     
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/ar-def.i}
{lib/winlaunch.i} 
{lib/winshowscrollbars.i}
{lib/get-column.i}
{lib/rpt-defs.i}

/* Temp-table Definition */
{tt/glbatchrpt.i}                               /* Contain Batch Detail to show in GL Report */ 
{tt/ledgerreport.i &tableAlias="glbatch"}       /* Contain info of GL account hit by Batch */
{tt/ledgerreport.i &tableAlias="glbatchdetail"} /* Contain info of GL account hit by each file of batch */
{tt/arbatch.i      &tableAlias=tbatch}          /* Used for data transfer */
{tt/arbatch.i      &tableAlias=batch}           /* Contain filterd data*/
{tt/arbatch.i      &tableAlias=ttbatch}         /* Contain data fetched from server*/
{tt/proerr.i       &tableAlias="ttBatchErr"}    /* Used to get validation messages from server */

/* Variable Definition */
define variable cDefaultOption          as character  no-undo.
define variable dtPostingDate           as date       no-undo.
define variable cAgentName              as character  no-undo.
define variable cStateID                as character  no-undo.
define variable cPeriodList             as character  no-undo.
define variable iErrSeq                 as integer    no-undo.
define variable dColumnWidth            as decimal    no-undo.
define variable deCheckAmt              as decimal    no-undo.
define variable deCheckRetainedPreAmt   as decimal    no-undo.
define variable deCheckNetPreAmt        as decimal    no-undo.
define variable deCheckFileCount        as decimal    no-undo.
define variable deCheckPolicyCount      as decimal    no-undo.
define variable deCheckEndorsementCount as decimal    no-undo.
define variable deCheckCplCount         as decimal    no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwArinv

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES batch

/* Definitions for BROWSE brwArinv                                      */
&Scoped-define FIELDS-IN-QUERY-brwArinv string(batch.batchid) @ batch.batchid batch.agentid batch.agentName batch.stateID batch.reference batch.receivedDate batch.invoiceDate batch.grossPremiumDelta batch.retainedPremiumDelta batch.netPremiumDelta batch.fileCount batch.policyCount batch.endorsementCount batch.cplCount batch.selectrecord   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwArinv batch.selectrecord   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwArinv batch
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwArinv batch
&Scoped-define SELF-NAME brwArinv
&Scoped-define QUERY-STRING-brwArinv for each batch
&Scoped-define OPEN-QUERY-brwArinv open query {&SELF-NAME} for each batch.
&Scoped-define TABLES-IN-QUERY-brwArinv batch
&Scoped-define FIRST-TABLE-IN-QUERY-brwArinv batch


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwArinv}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbState cbPeriod cbAgent brwArinv bExport ~
bRefresh RECT-78 RECT-79 RECT-87 
&Scoped-Define DISPLAYED-OBJECTS cbState cbPeriod cbAgent rsPost dtPost ~
flTotal flCheckAmt flCheckRetainedPreAmt flCheckNetPreAmt flCheckFileCount ~
flCheckPolicyCount flCheckEndorsementCount flCheckCplCount 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFormattedNumber C-Win 
FUNCTION getFormattedNumber RETURNS CHARACTER
  ( input deTotal as decimal,
    input hWidget as handle )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwArinv 
       MENU-ITEM m_View_Batch_Detail LABEL "View Batch Detail".


/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bPost  NO-FOCUS
     LABEL "Post" 
     SIZE 7.2 BY 1.71 TOOLTIP "Post selected batch(es)".

DEFINE BUTTON bPrelimRpt  NO-FOCUS
     LABEL "Prelim" 
     SIZE 7.2 BY 1.71 TOOLTIP "Preliminary Report".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON btResetFilter  NO-FOCUS
     LABEL "Reset" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reset filters".

DEFINE BUTTON bView  NO-FOCUS
     LABEL "View" 
     SIZE 7.2 BY 1.71 TOOLTIP "View batch detail".

DEFINE VARIABLE cbAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 61.2 BY 1 NO-UNDO.

DEFINE VARIABLE cbPeriod AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Period" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE dtPost AS DATE FORMAT "99/99/99":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flCheckAmt AS DECIMAL FORMAT "(Z,ZZZ,ZZZ)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 19.2 BY 1 NO-UNDO.

DEFINE VARIABLE flCheckCplCount AS DECIMAL FORMAT "(Z,ZZZ,ZZZ)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 9 BY 1 NO-UNDO.

DEFINE VARIABLE flCheckEndorsementCount AS DECIMAL FORMAT "(Z,ZZZ,ZZZ)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 9 BY 1 NO-UNDO.

DEFINE VARIABLE flCheckFileCount AS DECIMAL FORMAT "(Z,ZZZ,ZZZ)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 9 BY 1 NO-UNDO.

DEFINE VARIABLE flCheckNetPreAmt AS DECIMAL FORMAT "(Z,ZZZ,ZZZ)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 19.2 BY 1 NO-UNDO.

DEFINE VARIABLE flCheckPolicyCount AS DECIMAL FORMAT "(Z,ZZZ,ZZZ)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE flCheckRetainedPreAmt AS DECIMAL FORMAT "(Z,ZZZ,ZZZ)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 19.2 BY 1 NO-UNDO.

DEFINE VARIABLE flTotal AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 7.8 BY 1 NO-UNDO.

DEFINE VARIABLE rsPost AS CHARACTER INITIAL "R" 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Use Date:", "P",
"Use Received Date", "R",
"Use Completed Date", "T"
     SIZE 24.8 BY 2.86 NO-UNDO.

DEFINE RECTANGLE RECT-78
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 82.6 BY 3.81.

DEFINE RECTANGLE RECT-79
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 26.4 BY 3.81.

DEFINE RECTANGLE RECT-87
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 48.6 BY 3.81.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwArinv FOR 
      batch SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwArinv
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwArinv C-Win _FREEFORM
  QUERY brwArinv DISPLAY
      string(batch.batchid) @ batch.batchid                  label  "Batch ID"    width 11                      
batch.agentid                label  "Agent ID"               format "x(20)"       width 11          
batch.agentName              label  "Name"                   format "x(200)"      width 30               
batch.stateID                label  "State ID"               format "x(10)"       width 10              
batch.reference              column-label "Reference"        format "x(100)"      width 12
batch.receivedDate           column-label  "Received"        format "99/99/99"    width 11                  
batch.invoiceDate            column-label  "Completed"       format "99/99/99"    width 12             
batch.grossPremiumDelta      label  "Gross Premium"          format "->>>,>>>,>>9.99" width 14       
batch.retainedPremiumDelta   label  "Retained Premium"       format "->>>,>>>,>>9.99" width 14       
batch.netPremiumDelta        label  "Net Premium"            format "->>>,>>>,>>9.99" width 14       
batch.fileCount              column-label "#File"            format ">,>>9"       width 8
batch.policyCount            column-label "#Policy"          format ">,>>9"       width 9
batch.endorsementCount       column-label "#Endors"          format ">,>>9"       width 10
batch.cplCount               column-label "#CPL"             format ">,>>9"       width 8
batch.selectrecord           column-label "Select to!Post"   width 7 view-as toggle-box  
enable batch.selectrecord
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 199.2 BY 19.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bView AT ROW 2.33 COL 101.6 WIDGET-ID 466 NO-TAB-STOP 
     bPrelimRpt AT ROW 2.33 COL 150.2 WIDGET-ID 408
     cbState AT ROW 2.14 COL 10 COLON-ALIGNED WIDGET-ID 458
     bPost AT ROW 2.33 COL 143.2 WIDGET-ID 198 NO-TAB-STOP 
     cbPeriod AT ROW 2.14 COL 49.2 COLON-ALIGNED WIDGET-ID 460
     btResetFilter AT ROW 2.38 COL 75.8 WIDGET-ID 262 NO-TAB-STOP 
     cbAgent AT ROW 3.24 COL 10 COLON-ALIGNED WIDGET-ID 246
     brwArinv AT ROW 5.33 COL 3 WIDGET-ID 200
     rsPost AT ROW 1.81 COL 112.6 NO-LABEL WIDGET-ID 390
     dtPost AT ROW 1.81 COL 124.4 COLON-ALIGNED NO-LABEL WIDGET-ID 272
     bExport AT ROW 2.33 COL 94.6 WIDGET-ID 8 NO-TAB-STOP 
     bRefresh AT ROW 2.33 COL 87.6 WIDGET-ID 4 NO-TAB-STOP 
     flTotal AT ROW 25.05 COL 1 COLON-ALIGNED NO-LABEL WIDGET-ID 314 NO-TAB-STOP 
     flCheckAmt AT ROW 25.05 COL 100 COLON-ALIGNED NO-LABEL WIDGET-ID 480 NO-TAB-STOP 
     flCheckRetainedPreAmt AT ROW 25.05 COL 120.2 COLON-ALIGNED NO-LABEL WIDGET-ID 468 NO-TAB-STOP 
     flCheckNetPreAmt AT ROW 25.05 COL 140.2 COLON-ALIGNED NO-LABEL WIDGET-ID 470 NO-TAB-STOP 
     flCheckFileCount AT ROW 25.05 COL 160.2 COLON-ALIGNED NO-LABEL WIDGET-ID 472 NO-TAB-STOP 
     flCheckPolicyCount AT ROW 25.05 COL 170.2 COLON-ALIGNED NO-LABEL WIDGET-ID 474 NO-TAB-STOP 
     flCheckEndorsementCount AT ROW 25.05 COL 181 COLON-ALIGNED NO-LABEL WIDGET-ID 476 NO-TAB-STOP 
     flCheckCplCount AT ROW 25.05 COL 191 COLON-ALIGNED NO-LABEL WIDGET-ID 478 NO-TAB-STOP 
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.1 COL 4.4 WIDGET-ID 266
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1 COL 86.6 WIDGET-ID 194
     "Post" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1.1 COL 112.4 WIDGET-ID 464
     RECT-78 AT ROW 1.33 COL 3 WIDGET-ID 200
     RECT-79 AT ROW 1.33 COL 85.2 WIDGET-ID 268
     RECT-87 AT ROW 1.33 COL 111 WIDGET-ID 462
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 203.4 BY 25.19
         DEFAULT-BUTTON bRefresh WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Unposted Batches"
         HEIGHT             = 25.19
         WIDTH              = 203.4
         MAX-HEIGHT         = 33.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwArinv cbAgent DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bPost IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPrelimRpt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwArinv:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwArinv:HANDLE
       brwArinv:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwArinv:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR BUTTON btResetFilter IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bView IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dtPost IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flCheckAmt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flCheckCplCount IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flCheckEndorsementCount IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flCheckFileCount IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flCheckNetPreAmt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flCheckPolicyCount IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flCheckRetainedPreAmt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flTotal IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flTotal:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR RADIO-SET rsPost IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwArinv
/* Query rebuild information for BROWSE brwArinv
     _START_FREEFORM
open query {&SELF-NAME} for each batch.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwArinv */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Unposted Batches */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Unposted Batches */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Unposted Batches */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPost
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPost C-Win
ON CHOOSE OF bPost IN FRAME DEFAULT-FRAME /* Post */
DO:
  run postBatch in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrelimRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrelimRpt C-Win
ON CHOOSE OF bPrelimRpt IN FRAME DEFAULT-FRAME /* Prelim */
DO:
  run prelimReport in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME DEFAULT-FRAME /* refresh */
DO:
  run getData in this-procedure.
  
  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActions in this-procedure.
  
  /* Makes filters enable-disable based on the data */
  run enableFilters in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwArinv
&Scoped-define SELF-NAME brwArinv
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArinv C-Win
ON DEFAULT-ACTION OF brwArinv IN FRAME DEFAULT-FRAME
DO:
  run batchDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArinv C-Win
ON ROW-DISPLAY OF brwArinv IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArinv C-Win
ON START-SEARCH OF brwArinv IN FRAME DEFAULT-FRAME
do:
  define buffer batch for batch.
  
  std-ha = brwArinv:current-column.
  if std-ha:label = "Select to!Post"
   then
    do:     
      std-lo = can-find(first batch where not(batch.selectrecord)).   
      for each batch:            
        batch.selectrecord = std-lo.
        /* Retaining selected record */  
        for first ttbatch where ttbatch.batchID = batch.batchID:
          ttbatch.selectrecord = batch.selectrecord.
        end.
      end.    
      browse brwArinv:refresh(). 
      run enablePosting in this-procedure.
    end.
   else
    do:
      {lib/brw-startsearch.i}
    end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btResetFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btResetFilter C-Win
ON CHOOSE OF btResetFilter IN FRAME DEFAULT-FRAME /* Reset */
DO:
  run resetFilters in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bView C-Win
ON CHOOSE OF bView IN FRAME DEFAULT-FRAME /* View */
DO:  
  run batchDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgent C-Win
ON VALUE-CHANGED OF cbAgent IN FRAME DEFAULT-FRAME /* Agent */
OR 'VALUE-CHANGED' of cbState  or 'VALUE-CHANGED' of cbPeriod
DO:
  run displayBatches in this-procedure.
  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActions in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Batch_Detail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Batch_Detail C-Win
ON CHOOSE OF MENU-ITEM m_View_Batch_Detail /* View Batch Detail */
DO:
  run batchDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rsPost
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rsPost C-Win
ON VALUE-CHANGED OF rsPost IN FRAME DEFAULT-FRAME
DO:
   if rsPost:screen-value = "p" and
      can-find(first batch where batch.selectrecord = true)
    then
     dtPost:sensitive = true.
    else
     dtPost:sensitive = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow"  anywhere.

bRefresh     :load-image            ("images/sync.bmp").
bRefresh     :load-image-insensitive("images/sync-i.bmp").

bExport      :load-image            ("images/excel.bmp").
bExport      :load-image-insensitive("images/excel-i.bmp").

bPost        :load-image            ("images/check.bmp").              
bPost        :load-image-insensitive("images/check-i.bmp").

bView        :load-image             ("images/open.bmp").
bView        :load-image-insensitive ("images/open-i.bmp").

btResetFilter:load-image            ("images/filtererase.bmp").
btResetFilter:load-image-insensitive("images/filtererase-i.bmp").

bPrelimRpt:load-image             ("images/pdf.bmp").
bPrelimRpt:load-image-insensitive ("images/pdf-i.bmp").

/* Default Posting option */
publish 'GetPostingConfig' (output cDefaultOption).
rsPost:screen-value = cDefaultOption.

/* Default Posting option to Receive Date*/
/*rsPost:screen-value = "R".*/ /* As per last meeting on 28 aug this is commented */

/* Get list of open active period */
publish 'getOpenPeriodListItemPair' (output cPeriodList). 
cbPeriod:list-item-pairs = {&ALL} + "," + {&ALL} + (if cPeriodList > "" then "," else "") + cPeriodList.

/* Fetch data before window is visible */
run getData.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
      
  run enable_UI.
  
  flTotal:screen-value = "Totals".
  
  {lib/get-column-width.i &col="'Name'" &var=dColumnWidth}    
  
  /* Default to earliest open active period */
  cbPeriod:screen-value = if cPeriodList > "" then entry(4,cbPeriod:list-item-pairs) else {&ALL}.
  
  /* When default posting option from config screen is allowed */ 
  publish 'GetDefaultPostingOption' (output std-lo).
  if std-lo
   then
    do:
      /* Set default posting date on screen */
      publish "getDefaultPostingDate"(output dtPostingDate).
      dtPost:screen-value = string(dtPostingDate, "99/99/99"). 
    end.
    
  ON 'value-changed':U OF  batch.selectrecord in browse  brwArinv  
  DO:
    if available batch 
     then
      do:
        batch.selectrecord = batch.selectrecord:checked in browse brwArinv.
        /* Retaining selected record */  
        for first ttbatch where ttbatch.batchID = batch.batchID:
          ttbatch.selectrecord = batch.selectrecord.
        end.
      end.
    run enablePosting in this-procedure. 
  END.
  
  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActions in this-procedure.
  
  /* Makes filters enable-disable based on the data */
  run enableFilters in this-procedure.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adjustTotals C-Win 
PROCEDURE adjustTotals :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Setting position of totals widget */
 assign       
      flTotal:y                 = brwArinv:y + brwArinv:height-pixels + 0.01
      flCheckAmt:y              = brwArinv:y + brwArinv:height-pixels + 0.01
      flCheckRetainedPreAmt:y   = brwArinv:y + brwArinv:height-pixels + 0.01
      flCheckNetPreAmt:y        = brwArinv:y + brwArinv:height-pixels + 0.01
      flCheckFileCount:y        = brwArinv:y + brwArinv:height-pixels + 0.01
      flCheckPolicyCount:y      = brwArinv:y + brwArinv:height-pixels + 0.01
      flCheckEndorsementCount:y = brwArinv:y + brwArinv:height-pixels + 0.01
      flCheckCplCount:y         = brwArinv:y + brwArinv:height-pixels + 0.01
      flTotal:x                 = brwArinv:x + brwArinv:get-browse-column(1):x
      flCheckAmt:x              = brwArinv:x + brwArinv:get-browse-column(8):x + (brwArinv:get-browse-column(8):width-pixels - flCheckAmt:width-pixels)  + 7.8
      flCheckRetainedPreAmt:x   = brwArinv:x + brwArinv:get-browse-column(9):x + (brwArinv:get-browse-column(9):width-pixels - flCheckRetainedPreAmt:width-pixels)  + 7.8
      flCheckNetPreAmt:x        = brwArinv:x + brwArinv:get-browse-column(10):x + (brwArinv:get-browse-column(10):width-pixels - flCheckNetPreAmt:width-pixels)  + 7.8
      flCheckFileCount:x        = brwArinv:x + brwArinv:get-browse-column(11):x + (brwArinv:get-browse-column(11):width-pixels - flCheckFileCount:width-pixels)  + 6.8
      flCheckPolicyCount:x      = brwArinv:x + brwArinv:get-browse-column(12):x + (brwArinv:get-browse-column(12):width-pixels - flCheckPolicyCount:width-pixels)  + 6.8
      flCheckEndorsementCount:x = brwArinv:x + brwArinv:get-browse-column(13):x + (brwArinv:get-browse-column(13):width-pixels - flCheckEndorsementCount:width-pixels)  + 6.8
      flCheckCplCount:x         = brwArinv:x + brwArinv:get-browse-column(14):x + (brwArinv:get-browse-column(14):width-pixels - flCheckCplCount:width-pixels)  + 6.8
      no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE batchDetail C-Win 
PROCEDURE batchDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/                        
  publish "OpenWindow" (input "wBatchDetail", 
                        input string(batch.batchid), 
                        input "wbatchdetail.w", 
                        input "integer|input|" + string(batch.batchid) + "^character|input|" + {&ALL} + "^character|input|" + batch.agentID,                                   
                        input this-procedure).                         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/           
  publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE createErrorRcrd C-Win 
PROCEDURE createErrorRcrd :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  define input parameter piBatchID  as integer   no-undo.
  define input parameter pcErrorMsg as character no-undo. 

  create ttBatchErr.
  assign 
      iErrSeq                = iErrSeq + 1
      ttBatchErr.err         = string(iErrSeq)
      ttBatchErr.entity      = "Batch"
      ttBatchErr.entityID    = if piBatchID > 0 then string(piBatchID) else ""      
      ttBatchErr.description = pcErrorMsg
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayBatches C-Win 
PROCEDURE displayBatches :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
   
  /* Enable/disable filter button based on selected values */
  run setfilterButton in this-procedure.
  
  /* This will use the screen-value of the filters which is ALL */
  run filterData  in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableActions C-Win 
PROCEDURE enableActions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Makes widget enable-disable based on the data */ 
  if query brwArinv:num-results > 0
   then
    assign 
         browse brwArinv:sensitive  = true
                bExport:sensitive   = true          
                bView:sensitive     = true
         .
   else
    assign 
         browse brwArinv:sensitive  = false
                bExport:sensitive   = false          
                bView:sensitive     = false
         .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableFilters C-Win 
PROCEDURE enableFilters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Makes widget enable-disable based on the data */ 
  /*if query brwArinv:num-results > 0*/  /* Task 79009 */
  if can-find(first ttbatch) 
   then
    assign 
        cbAgent:sensitive   = true            
        cbState:sensitive   = true
        cbPeriod:sensitive  = true
        . 
   else
    assign 
        cbAgent:sensitive   = false             
        cbState:sensitive   = false
        cbPeriod:sensitive  = false
        . 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enablePosting C-Win 
PROCEDURE enablePosting :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
   
  if not can-find(first ttbatch where ttbatch.selectrecord = true)
   then
    assign
        bPost:sensitive      = false
        bPrelimRpt:sensitive = false 
        dtPost:sensitive     = false
        rsPost:sensitive     = false
        .
   else
    assign
        bPost:sensitive      = true
        bPrelimRpt:sensitive = true 
        dtPost:sensitive     = (rsPost:input-value = "P")
        rsPost:sensitive     = true
        . 
        
  apply 'value-changed' to  rsPost in frame {&frame-name}.      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbState cbPeriod cbAgent rsPost dtPost flTotal flCheckAmt 
          flCheckRetainedPreAmt flCheckNetPreAmt flCheckFileCount 
          flCheckPolicyCount flCheckEndorsementCount flCheckCplCount 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE cbState cbPeriod cbAgent brwArinv bExport bRefresh RECT-78 RECT-79 
         RECT-87 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwArinv:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table batch:handle.
  run util/exporttable.p (table-handle std-ha,
                          "batch",
                          "for each batch",                       
                          "batchID,agentID,agentName,reference,periodID,yearID,stateID,createDate,receivedDate,invoiceDate,grossPremiumDelta,retainedPremiumDelta,netPremiumDelta,fileCount,policyCount,endorsementCount,cplCount",
                          "Batch ID,Agent ID, Agent Name, Reference, Period ID,Year ID,State ID,Created Date,Received Date,Completed Date,Gross Premium Delta,Retained Premium Delta,Net Premium Delta,File Count,Policy Count,Endorsement Count,CPL Count",
                          std-ch,
                          "UnpostedBatches-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportErrorData C-Win 
PROCEDURE exportErrorData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if can-find(first ttBatchErr)
   then
    do:
      publish "GetReportDir" (output std-ch).      
      std-ha = temp-table ttBatchErr:handle.
      run util/exporttable.p (table-handle std-ha,
                              "ttBatchErr",
                              "for each ttBatchErr",
                              "err,entityID,description",
                              "Sequence,Batch ID,Error",
                              std-ch,
                              "BatchPostingErrors_" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                              true,
                              output std-ch,
                              output std-in).
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define buffer ttbatch for ttbatch.
  
  assign      
      deCheckAmt                           = 0
      deCheckRetainedPreAmt                = 0
      deCheckNetPreAmt                     = 0
      deCheckFileCount                     = 0
      deCheckPolicyCount                   = 0
      deCheckEndorsementCount              = 0
      deCheckCplCount                      = 0
      flCheckAmt:screen-value              = ""
      flCheckRetainedPreAmt:screen-value   = ""
      flCheckNetPreAmt:screen-value        = ""
      flCheckFileCount:screen-value        = ""
      flCheckPolicyCount:screen-value      = ""
      flCheckEndorsementCount:screen-value = ""
      flCheckCplCount:screen-value         = ""
      .
      
  close query brwArinv.
  empty temp-table batch.

  for each ttbatch 
    where ttbatch.agentName = (if cbAgent:input-value  = {&ALL} then ttbatch.agentName else replace(cbAgent:input-value,"#",","))        
      and ttbatch.stateID   = (if cbState:input-value  = {&ALL} then ttbatch.stateID   else cbState:input-value)
      and ttbatch.periodID  = (if cbPeriod:input-value = {&ALL} then ttbatch.periodID  else cbPeriod:input-value) :
           
    create batch.
    buffer-copy ttbatch to batch.
    assign 
        deCheckAmt              = deCheckAmt + batch.grossPremiumDelta
        deCheckRetainedPreAmt   = deCheckRetainedPreAmt + batch.retainedPremiumDelta
        deCheckNetPreAmt        = deCheckNetPreAmt + batch.netPremiumDelta
        deCheckFileCount        = deCheckFileCount + batch.fileCount
        deCheckPolicyCount      = deCheckPolicyCount + batch.policyCount
        deCheckEndorsementCount = deCheckEndorsementCount + batch.endorsementCount
        deCheckCplCount         = deCheckCplCount + batch.cplCount.
  end.

  open query brwArinv preselect each batch by batch.agentName by batch.batchID.
  
  flCheckAmt:format              = getFormattedNumber(input deCheckAmt, input flCheckAmt:handle) no-error.
  flCheckRetainedPreAmt:format   = getFormattedNumber(input deCheckRetainedPreAmt, input flCheckRetainedPreAmt:handle) no-error.
  flCheckNetPreAmt:format        = getFormattedNumber(input deCheckNetPreAmt, input flCheckNetPreAmt:handle) no-error.
  flCheckFileCount:format        = getFormattedNumber(input deCheckFileCount, input flCheckFileCount:handle) no-error.
  flCheckPolicyCount:format      = getFormattedNumber(input deCheckPolicyCount, input flCheckPolicyCount:handle) no-error.
  flCheckEndorsementCount:format = getFormattedNumber(input deCheckEndorsementCount, input flCheckEndorsementCount:handle) no-error.
  flCheckCplCount:format         = getFormattedNumber(input deCheckCplCount, input flCheckCplCount:handle) no-error.

  assign
      flCheckAmt:screen-value              = string(deCheckAmt)
      flCheckRetainedPreAmt:screen-value   = string(deCheckRetainedPreAmt)
      flCheckNetPreAmt:screen-value        = string(deCheckNetPreAmt)
      flCheckFileCount:screen-value        = string(deCheckFileCount)
      flCheckPolicyCount:screen-value      = string(deCheckPolicyCount)
      flCheckEndorsementCount:screen-value = string(deCheckEndorsementCount)
      flCheckCplCount:screen-value         = string(deCheckCplCount).
  
  run adjustTotals in this-procedure.
  run enablePosting in this-procedure.
  
  setStatusCount(query brwArinv:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
    
  empty temp-table ttbatch.
  
  /* Client Server Call */
  run server\getcompletedbatches.p (input 0,     /* Period ID */
                                    input false, /* Include All */
                                    input false, /* OnlyPosted */
                                    output table ttbatch,
                                    output std-lo,
                                    output std-ch). 
                            
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
   
  run initialiseFilter in this-procedure.
    
  /* Display temp credits record on the screen */
  run displayBatches in this-procedure.
  
  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActions in this-procedure.
  
  /* Makes filters enable-disable based on the data */
  run enableFilters in this-procedure.
    
  /* Set Status count with date and time from the server */
  setStatusRecords(query brwArinv:num-results).      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initialiseFilter C-Win 
PROCEDURE initialiseFilter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable cstatesSelected as character no-undo.
  define variable cPeriodSelected as character no-undo.
  define variable cAgentSelected  as character no-undo.

  define buffer ttbatch for ttbatch.
  
  /* Retaining previous selected values of filters */
  assign 
      cstatesSelected = if cbState:screen-value  = ? then "" else cbState:screen-value
      cPeriodSelected = if cbPeriod:screen-value = ? then "" else cbPeriod:screen-value 
      cAgentSelected  = if cbAgent:screen-value  = ? then "" else cbAgent:screen-value 
      .

  /* Initialise filters to default values */
  assign
      cbAgent:list-item-pairs   = {&ALL} + "," + {&ALL}
      cbState:list-item-pairs   = {&ALL} + "," + {&ALL}
      cbAgent:screen-value      = {&ALL} 
      cbState:screen-value      = {&ALL}
      cbPeriod:screen-value     = if cPeriodList > "" then entry(4,cbPeriod:list-item-pairs) else {&ALL}
      .
    
  /* Set the filters based on the data returned, with ALL as the first option */
  for each ttbatch 
    break by ttbatch.agentName:
    if first-of(ttbatch.agentName) and ttbatch.agentName <> ""
     then 
      cbAgent:add-last(replace(ttbatch.agentName,",",""),replace(ttbatch.agentName,",","#")).
  end.
  
  for each ttBatch 
    break by ttBatch.stateID:
    if first-of(ttBatch.stateID) and ttBatch.stateID <> "" 
     then
      cbState:add-last(ttBatch.stateID,ttBatch.stateID).
  end.
  
  /* Setting previous selected values of filters */
  if cstatesSelected <> "" and cstatesSelected <> {&ALL} and lookup(cstatesSelected ,cbState:list-item-pairs,",") > 0  
   then
    cbState:screen-value = cstatesSelected.
  
  if cPeriodSelected <> ""and cPeriodSelected <> {&ALL} and lookup(cPeriodSelected,cbPeriod:list-item-pairs,",") > 0  
   then
    cbPeriod:screen-value = cPeriodSelected.
    
  if cAgentSelected <> ""and cAgentSelected <> {&ALL} and lookup(cAgentSelected,cbAgent:list-item-pairs,",") > 0  
   then
    cbAgent:screen-value = cAgentSelected.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE postBatch C-Win 
PROCEDURE postBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cFileName  as character no-undo.
  define variable lViewPdf   as logical   no-undo.
  define variable lPost      as logical   no-undo.
  define variable cList      as character no-undo.
  define variable iCount     as integer   no-undo.
  
  define buffer batch   for batch.
  define buffer tbatch  for tbatch.
  define buffer ttbatch for ttbatch.

  do with frame {&frame-name}:
  end.
  
  if not can-find(first batch where batch.selectrecord = true)
   then
    do:
      message "Please select at least one batch for posting"
          view-as alert-box error buttons ok.
      return.
    end.
  
  if rsPost:input-value = "P" and dtPost:input-value = ? 
   then
    do:
      message "Post date cannot be blank"
          view-as alert-box error buttons ok.
      apply 'entry' to dtPost.    
      return.
    end.
  
  /* Validations before posting a batch */
  run validatePost in this-procedure.

  /* Export error table into csv and open it */
  run exportErrorData in this-procedure.
  
  if can-find(first ttBatchErr)
   then
    do:
      apply 'entry' to dtPost.  
      return.
    end.
   
  empty temp-table tbatch.
  
  /* get the data to post*/
  for each batch where batch.selectrecord = true:
    create tbatch.
    buffer-copy batch to tbatch.
    if rsPost:input-value = "P" 
     then
      tbatch.trandate = dtPost:input-value.
    if rsPost:input-value = "T"
     then
      tbatch.trandate = batch.invoicedate.
    if rsPost:input-value = "R"
     then
      tbatch.trandate = batch.receivedDate. 
  end.  
  
  /* update the posting configration as per user action for posting */
    if cDefaultOption <> rsPost:input-value and rsPost:input-value ne "R"
     then
      do:
        publish 'SetPostingConfig' (input rsPost:input-value).
        cDefaultOption = rsPost:input-value.
      end.

  message "Seleted batch(es) will be queued for posting. Continue?"
    view-as alert-box question buttons yes-no title "Post Batch" update lPost.
   
  if not lPost
   then
    return.

  assign
      rpt-behavior = "Q"
      rpt-destination = "C"
      .

  /* server call to post */
  run server/queuepostbatch.p ( input  table tbatch,
                                {lib/rpt-setparams.i},
                                output table ttBatchErr,
                                output cList,
                                output std-lo,
                                output std-ch).        

  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
          
      /* Export error table into csv and open it */
      run exportErrorData in this-procedure.    
      return.
    end.
   else
    message "Batch(es) " + cList + " are successfully queued."
        view-as alert-box information buttons ok. 
    
  /* update the local temp table (remove the records that got posted) after successful post*/    
  do iCount = 1 to num-entries(cList):
    for each batch 
      where batch.selectrecord = true 
        and batch.batchID      = integer(entry(icount,cList)):
      find first ttbatch where ttbatch.batchid = batch.batchid no-error.
      if available ttbatch 
       then
        do:
          assign
              cStateID   = ttBatch.stateID
              cAgentName = ttbatch.agentName            
              .
          delete ttbatch.
          
          /* Deleting unnecessary client-side filters */    
          if not can-find(first ttbatch where ttbatch.agentName = cAgentName)
           then
            cbAgent:delete(replace(cAgentName,",","#")). 
    
          if not can-find(first ttBatch where ttBatch.stateID = cStateID)
           then
            cbState:delete(cStateID).
        end.  
      delete batch.
    end.
  end. 
  
  run displayBatches in this-procedure.

  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActions in this-procedure. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE prelimReport C-Win 
PROCEDURE prelimReport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cBatchList    as character no-undo.
    
  define buffer batch for batch.

  /* get the data to create list*/
  for each batch where batch.selectrecord = true:
    cBatchList = cBatchList + "," + string(batch.batchID).
  end.  
  
  empty temp-table glbatch.
  empty temp-table glbatchdetail.
  empty temp-table glbatchrpt.

  /* server call to post */
  run server/prelimbatchgl.p(input trim(cBatchList,","),
                             output table glbatch,
                             output table glbatchdetail,
                             output table glbatchrpt,
                             output std-lo,
                             output std-ch).        

  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
    
  if not can-find(first glbatch)       and
     not can-find(first glbatchdetail) and
     not can-find(first glbatchrpt)  
   then
    do:
      message "Nothing to print."
          view-as alert-box error buttons ok.
      return.
    end.
    
  run util\arbatchpdf.p (input {&view},
                         input table glbatch,
                         input table glbatchdetail,        
                         input table glbatchrpt,
                         output std-ch).
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resetFilters C-Win 
PROCEDURE resetFilters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Reset filters to initial state */
  assign
      btResetFilter:Tooltip    = "Reset filters"
      cbAgent:sensitive        = true
      cbState:sensitive        = true
      cbPeriod:sensitive       = true
      cbAgent:screen-value     = {&ALL}
      cbState:screen-value     = {&ALL}
      cbPeriod:screen-value    = {&ALL}
      .
 
  run displayBatches in this-procedure.
  
  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActions in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterButton C-Win 
PROCEDURE setFilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
  
 if cbAgent:input-value ne {&ALL} or cbState:input-value ne {&ALL} or cbPeriod:input-value ne {&ALL}
  then
    btResetFilter:sensitive = true.
  else   
    btResetFilter:sensitive = false.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
  run filterData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by batch.agentname by batch.batchID ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validatePost C-Win 
PROCEDURE validatePost :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer batch for batch. 
  
  do with frame {&frame-name}:
  end.
  
  empty temp-table ttBatchErr.
  
  if rsPost:input-value = "P" 
   then
    do:      
      if dtPost:input-value > today
       then
        run createErrorRcrd in this-procedure (input 0, /* Batch ID */
                                               input "Post date cannot be in the future").
  
      if dtPost:input-value <> ?
       then
        do:
          publish "validatePostingDate" (input dtPost:input-value,
                                         output std-lo).
  
          if not std-lo
           then
            run createErrorRcrd in this-procedure (input 0, /* Batch ID */
                                                   input "Period is closed for the post date").
        end.                                           
    end.
    
  for each batch where batch.selectrecord = true:
    if batch.posted
     then
      run createErrorRcrd in this-procedure (input batch.batchID, 
                                             input "Batch is already posted").
                                             
    if rsPost:input-value = "P" and dtPost:input-value < date(Batch.receivedDate)                  
     then
      run createErrorRcrd in this-procedure (input batch.batchID, 
                                             input "Post date cannot be prior to batch received Date").  
     
     else if rsPost:input-value = "R"
      then
       do:
         if batch.receivedDate = ?
          then
           run createErrorRcrd in this-procedure (input batch.batchID, 
                                                  input "Post date cannot be blank").
          
         if date(batch.receivedDate) > today
          then
           run createErrorRcrd in this-procedure (input batch.batchID, 
                                                  input "Post date cannot be in the future").
        
         if batch.receivedDate <> ?
          then
           do:
             publish "validatePostingDate" (input date(batch.receivedDate),
                                            output std-lo).
  
             if not std-lo
              then
               run createErrorRcrd in this-procedure (input batch.batchID, 
                                                      input "Period is closed for the post date").
           end.  
       end.   
     else
      do:
        if batch.invoicedate = ?
         then
          run createErrorRcrd in this-procedure (input batch.batchID, 
                                                 input "Post date cannot be blank").
          
        if date(batch.invoicedate) > today
         then
          run createErrorRcrd in this-procedure (input batch.batchID, 
                                                 input "Post date cannot be in the future").
        
        if batch.invoicedate <> ?
         then
          do:
            publish "validatePostingDate" (input date(batch.invoicedate),
                                           output std-lo).
  
            if not std-lo
             then
              run createErrorRcrd in this-procedure (input batch.batchID, 
                                                     input "Period is closed for the post date").
          end.                                           
      end.
  end.     

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 14
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 23
      .
      
  {lib/resize-column.i &col="'Name'" &var=dColumnWidth} 
  run ShowScrollBars(frame {&frame-name}:handle, no, no).  
  run adjustTotals in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFormattedNumber C-Win 
FUNCTION getFormattedNumber RETURNS CHARACTER
  ( input deTotal as decimal,
    input hWidget as handle ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cNumberFormat  as character no-undo.
  define variable cTotalScrValue as character no-undo.
  define variable deWidgetWidth  as decimal   no-undo.
  
  cTotalScrValue = string(deTotal).
     
  /* account for negative numbers */
  if deTotal < 0
   then 
    cNumberFormat = cNumberFormat + "(".
        
  /* loop through the absolute value of the number cast as an int64 */
  do std-in = length(string(int64(absolute(deTotal)))) to 1 by -1:
    if std-in modulo 3 = 0
     then 
      cNumberFormat = cNumberFormat + (if std-in = length(string(int64(absolute(deTotal)))) then ">" else ",>").
     else 
      cNumberFormat = cNumberFormat + (if std-in = 1 then "Z" else ">").
  end.
     
  /* if the number had a decimal value */
  if index(cTotalScrValue, ".") > 0 
   then 
    cNumberFormat = cNumberFormat + ".99".
         
  /* account for negative numbers */
  if deTotal < 0
   then 
    cNumberFormat = cNumberFormat + ")".
  
  do std-in = 1 to length(cNumberFormat):
    std-ch = substring(cNumberFormat,std-in,1).
    case std-ch:
      when "(" then deWidgetWidth = deWidgetWidth + 5.75.
      when ")" then deWidgetWidth = deWidgetWidth + 5.75.      
      when "Z" then deWidgetWidth = deWidgetWidth + 12.
      when ">" then deWidgetWidth = deWidgetWidth + 8.
      when "," then deWidgetWidth = deWidgetWidth + 4.
      when "." then deWidgetWidth = deWidgetWidth + 4.
      when "9" then deWidgetWidth = deWidgetWidth + 10.
    end case.
  end.
  
  /* set width of the widget as per format */
  hWidget:width-pixels = deWidgetWidth.      
        
  RETURN cNumberFormat.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

