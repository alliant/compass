&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

File: wglpayment.w 

Description: 

Input Parameters:
<none>

Output Parameters:
<none>

Author: Rahul Sharma

Created: 01/29/2020

Modified: 
Date          Name      Description
07/06/2021    SB        Add Posting Date in Payment GL report.  
07/12/2021    SB        Task 84028 Added posting date as separate column 
                        in Export.
------------------------------------------------------------------------*/
/* This .W file was created with the Progress AppBuilder. */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
by this procedure. This is a good default which assures
that this procedure's triggers and internal procedures 
will execute in this procedure's storage, and that proper
cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ************************** Definitions ************************* */
{lib/ar-def.i}
{lib/std-def.i}
{lib/winlaunch.i}
{lib/winshowscrollbars.i}
{lib/getperiodname.i} /* Include function: getPeriodName */

/* Temp-table Definition */
{tt/glPayment.i}
{tt/glPayment.i &tableAlias=tglPayment}

define input parameter ipcAgentID      as character no-undo.
define input parameter ipcName         as character no-undo.
define input parameter ipiMonth        as integer   no-undo.
define input parameter ipiYear         as integer   no-undo.
define input parameter ipcDepRef       as character no-undo.
define input parameter iplParamPassed  as logical   no-undo.

 
define variable dtStartDate  as date      no-undo.
define variable dtEndDate    as date      no-undo.
define variable tYear        as integer   no-undo.
define variable tMonth       as integer   no-undo.
define variable deDebit      as decimal   no-undo.
define variable deCredit     as decimal   no-undo.
define variable lgridChange  as logical   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES glPayment

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData glPayment.depositRef glPayment.postingdate glPayment.description glPayment.arglRef glPayment.debit glPayment.credit getVoid(glPayment.voided) @ glPayment.voided glPayment.notes   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each glPayment
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each glPayment.
&Scoped-define TABLES-IN-QUERY-brwData glPayment
&Scoped-define FIRST-TABLE-IN-QUERY-brwData glPayment


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bPeriod bGo rsReportType cbDepRef tVoided ~
brwData RECT-2 RECT-3 RECT-4 
&Scoped-Define DISPLAYED-OBJECTS fPeriod rsReportType cbDepRef tVoided ~
flTotal fDebit fCredit 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFormattedNumber C-Win 
FUNCTION getFormattedNumber RETURNS CHARACTER
  ( input deTotal as decimal,
    input hWidget as handle )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getVoid C-Win 
FUNCTION getVoid RETURNS CHARACTER
  ( ipVoid as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bFilterClear  NO-FOCUS
     LABEL "FilterClear" 
     SIZE 7.2 BY 1.71 TOOLTIP "Clear filters".

DEFINE BUTTON bGetCSV  NO-FOCUS
     LABEL "CSV" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE BUTTON bPeriod 
     LABEL "Period" 
     SIZE 5 BY 1.19 TOOLTIP "Select Period".

DEFINE VARIABLE cbDepRef AS CHARACTER FORMAT "X(256)":U 
     LABEL "Deposit Ref" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 20.6 BY 1 NO-UNDO.

DEFINE VARIABLE fCredit AS DECIMAL FORMAT "(Z,ZZZ,ZZZ)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 19.2 BY 1 NO-UNDO.

DEFINE VARIABLE fDebit AS DECIMAL FORMAT "(Z,ZZZ,ZZZ)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 19.2 BY 1 NO-UNDO.

DEFINE VARIABLE flTotal AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 7.8 BY 1 NO-UNDO.

DEFINE VARIABLE fPeriod AS CHARACTER FORMAT "X(256)":U INITIAL "Select Period" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 22.6 BY 1 NO-UNDO.

DEFINE VARIABLE rsReportType AS CHARACTER INITIAL "D" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Summary", "S",
"Detailed", "D"
     SIZE 29.4 BY 1.38 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 73 BY 2.81.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 10.6 BY 2.81.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 44 BY 2.81.

DEFINE VARIABLE tVoided AS LOGICAL INITIAL no 
     LABEL "Show only voids" 
     VIEW-AS TOGGLE-BOX
     SIZE 20 BY .81 TOOLTIP "Show only voids" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      glPayment SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      glPayment.depositRef     label "Deposit Ref"  format "x(21)"      
glPayment.postingdate    label "Posting Date" format "99/99/99"
glPayment.description    label "Description"  format "x(42)"
glPayment.arglRef        label "GL Ref"       format "x(28)"   
glPayment.debit          label "Debit"        format "->,>>,>>>,>>>,>>9.99"      
glPayment.credit         label "Credit"       format "->,>>,>>>,>>>,>>9.99"
getVoid(glPayment.voided) @ glPayment.voided column-label "Void"       format "x(10)"
glPayment.notes          label "Notes"        format "x(320)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 190 BY 14.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     fPeriod AT ROW 2.29 COL 2.4 COLON-ALIGNED NO-LABEL WIDGET-ID 426
     bGetCSV AT ROW 1.91 COL 120.8 WIDGET-ID 420 NO-TAB-STOP 
     bPeriod AT ROW 2.19 COL 27.4 WIDGET-ID 402
     bGo AT ROW 1.91 COL 67 WIDGET-ID 4
     rsReportType AT ROW 2.14 COL 35.2 NO-LABEL WIDGET-ID 456
     bFilterClear AT ROW 1.91 COL 110.8 WIDGET-ID 262 NO-TAB-STOP 
     cbDepRef AT ROW 1.81 COL 87.2 COLON-ALIGNED WIDGET-ID 250
     tVoided AT ROW 3 COL 89.4 WIDGET-ID 460
     brwData AT ROW 4.38 COL 3 WIDGET-ID 200
     flTotal AT ROW 19.43 COL 1 COLON-ALIGNED NO-LABEL WIDGET-ID 314 NO-TAB-STOP 
     fDebit AT ROW 19.43 COL 86.4 COLON-ALIGNED NO-LABEL WIDGET-ID 304 NO-TAB-STOP 
     fCredit AT ROW 19.43 COL 108 COLON-ALIGNED NO-LABEL WIDGET-ID 306 NO-TAB-STOP 
     "Parameters" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 1.14 COL 4.2 WIDGET-ID 412
     "Action" VIEW-AS TEXT
          SIZE 6.2 BY .62 AT ROW 1.14 COL 120.4 WIDGET-ID 438
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.14 COL 77 WIDGET-ID 442
     RECT-2 AT ROW 1.38 COL 3 WIDGET-ID 76
     RECT-3 AT ROW 1.38 COL 119.2 WIDGET-ID 416
     RECT-4 AT ROW 1.38 COL 75.6 WIDGET-ID 440
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 192.2 BY 19.86 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Period Cash GL Transactions"
         HEIGHT             = 19.81
         WIDTH              = 194
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData tVoided fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bFilterClear IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bGetCSV IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR FILL-IN fCredit IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fDebit IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flTotal IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       flTotal:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN fPeriod IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       fPeriod:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each glPayment.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Period Cash GL Transactions */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Period Cash GL Transactions */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Period Cash GL Transactions */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilterClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilterClear C-Win
ON CHOOSE OF bFilterClear IN FRAME fMain /* FilterClear */
DO:  
  /* Reset filters to initial state */
  assign
      cbDepRef:screen-value  = {&ALL}      
      tVoided:checked        = false
      .
  run comboFilter in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGetCSV
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGetCSV C-Win
ON CHOOSE OF bGetCSV IN FRAME fMain /* CSV */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:
  run getData in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPeriod C-Win
ON CHOOSE OF bPeriod IN FRAME fMain /* Period */
DO:
  run getPeriod in this-procedure.
  apply "choose":U to bGo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
do:
 {lib/brw-rowdisplay.i}
  
  assign
      glPayment.debit:fgcolor in browse brwData = if (glPayment.debit < 0)   
                                                   then 12 
                                                   else ?
      glPayment.credit:fgcolor                  = if (glPayment.credit < 0)  
                                                   then 12
                                                   else ?       
      . 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbDepRef
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbDepRef C-Win
ON VALUE-CHANGED OF cbDepRef IN FRAME fMain /* Deposit Ref */
DO:
  run comboFilter in this-procedure.
  ipcDepRef = cbDepRef:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rsReportType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rsReportType C-Win
ON VALUE-CHANGED OF rsReportType IN FRAME fMain
DO:
  define variable hColDepRef as handle no-undo.
  hColDepRef  = brwData:get-browse-column(1).
  if rsReportType:input-value = {&Summary}
   then
    assign
        cbDepRef:screen-value   = {&All}
        cbDepRef:sensitive      = false
        tVoided:checked         = false
        tVoided:sensitive       = false
        bFilterClear:sensitive  = false
        .
   else if (rsReportType:input-value = {&Detail}) and (query brwData:num-results > 0) and hColDepRef:visible = true
    then
     assign
         cbDepRef:sensitive     = true
         tVoided:sensitive      = true
         .
         
  lgridChange = false.
  resultsChanged(false).
  ipcDepRef = cbDepRef:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tVoided
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tVoided C-Win
ON VALUE-CHANGED OF tVoided IN FRAME fMain /* Show only voids */
DO:
  run comboFilter in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */ 
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

assign 
  {&window-name}:min-height-pixels = {&window-name}:height-pixels
  {&window-name}:min-width-pixels  = {&window-name}:width-pixels
  {&window-name}:max-height-pixels = session:height-pixels
  {&window-name}:max-width-pixels  = session:width-pixels
  .

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME} .

setStatusMessage("").  

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow" anywhere.

bGo         :load-image            ("images/completed.bmp").
bGo         :load-image-insensitive("images/completed-i.bmp").

bPeriod     :load-image            ("images/s-calendar.bmp").
bPeriod     :load-image-insensitive("images/s-calendar-i.bmp").

bGetCSV     :load-image            ("images/excel.bmp").
bGetCSV     :load-image-insensitive("images/excel-i.bmp").

bFilterClear  :load-image            ("images/filtererase.bmp").
bFilterClear  :load-image-insensitive("images/filtererase-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
      
  RUN enable_UI.
    
  if not iplParamPassed 
   then 
    publish "getDefaultPeriod"(output tMonth,output tYear).
   else
    assign
        tMonth = ipiMonth
        tYear  = ipiYear
        .
  
  assign
      flTotal:screen-value   = "Totals"
      fPeriod:screen-value   = getPeriodName(tMonth,tYear)
      cbDepRef:sensitive     = false
      cbDepRef:list-items    = {&ALL}
      tVoided:sensitive      = false
      .  
  
  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.
  
  if iplParamPassed 
   then
    apply 'choose' to bGo.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adjustTotals C-Win 
PROCEDURE adjustTotals :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  /* Setting width of browse columns when window resizes */

  if lgridChange 
   then
    do:
      if rsReportType:input-value = {&Summary}
       then
        assign
            brwData:get-browse-column(3):width-pixels = (40 / 100) *  brwData:width-pixels
            brwData:get-browse-column(4):width-pixels = (30 / 100) *  brwData:width-pixels
            no-error.
    end.
  
  /* Setting position of totals widget */
  assign       
      flTotal:y = brwData:y + brwData:height-pixels + 0.01
      fDebit:y  = brwData:y + brwData:height-pixels + 0.01
      fCredit:y = brwData:y + brwData:height-pixels + 0.01            
      flTotal:x = brwData:x + brwData:get-browse-column(1):x
      fDebit:x  = brwData:x + brwData:get-browse-column(5):x + (brwData:get-browse-column(5):width-pixels - fDebit:width-pixels)  + 6.8 
      fCredit:x = brwData:x + brwData:get-browse-column(6):x + (brwData:get-browse-column(6):width-pixels - fCredit:width-pixels) + 6.8      
      no-error.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE comboFilter C-Win 
PROCEDURE comboFilter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Enable/disable filter button based on selected values */
  run setFilterButton in this-procedure.

  /* This will use the screen-value of the filters which is ALL */
  run filterData in this-procedure. 
  run showData   in this-procedure.
  setStatusCount(query brwData:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE configBrowse C-Win 
PROCEDURE configBrowse :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hColDepRef as handle no-undo.
  define variable hColDesc   as handle no-undo.
  define variable hColGlRef  as handle no-undo.
  define variable hColDebit  as handle no-undo.
  define variable hColCredit as handle no-undo.
  define variable hColVoid   as handle no-undo.
  define variable hColNotes  as handle no-undo.
  define variable hPostDate  as handle no-undo.
  
  do with frame {&frame-name} :
  end.
    
  assign
      hColDepRef  = brwData:get-browse-column(1)
      hPostDate   = brwData:get-browse-column(2)
      hColDesc    = brwData:get-browse-column(3)
      hColGlRef   = brwData:get-browse-column(4)
      hColDebit   = brwData:get-browse-column(5) 
      hColCredit  = brwData:get-browse-column(6) 
      hColVoid    = brwData:get-browse-column(7) 
      hColNotes   = brwData:get-browse-column(8)
      .
    
  if rsReportType:input-value = {&Summary} 
   then
    do: 
      if valid-handle(hColDepRef)       
       then     
        hColDepRef:visible = false.
        
      if valid-handle(hColDesc)       
       then     
        hColDesc:visible = false.  
            
      if valid-handle(hColVoid)       
       then     
        hColVoid:visible = false.
        
      if valid-handle(hColNotes)       
       then     
        hColNotes:visible = false.  

      if valid-handle(hColGlRef)     
       then
        hColGlRef:width = 90.     
    end.
   else
    do:
      if valid-handle(hColDepRef)       
       then     
        hColDepRef:visible = true.
        
      if valid-handle(hColDesc) 
       then
        hColDesc:visible = true.
      
      if valid-handle(hColVoid)       
       then     
        hColVoid:visible = true. 
        
      if valid-handle(hColNotes)       
       then     
        hColNotes:visible = true.  
      
      if valid-handle(hColDesc)   and
         valid-handle(hColDepRef) and
         valid-handle(hColGlRef)  and
         valid-handle(hColDebit)  and
         valid-handle(hColCredit) and
         valid-handle(hColVoid)   and
         valid-handle(hPostDate)  and
         valid-handle(hColNotes)
       then
        do:
          assign
              hColDepRef:width   = 16.5
              hPostDate:width    = 15
              hColDesc:width     = 35
              hColGlRef:width    = 18
              hColDebit:width    = 20.5
              hColCredit:width   = 20.5
              hColVoid:width     = 10
              hColNotes:width    = 44
              .
        end.                        
    end.
  lgridChange = true.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fPeriod rsReportType cbDepRef tVoided flTotal fDebit fCredit 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bPeriod bGo rsReportType cbDepRef tVoided brwData RECT-2 RECT-3 RECT-4 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table glPayment:handle.

  run util/exporttable.p (table-handle std-ha,
                          "glPayment",
                          "for each glPayment",
                          if rsReportType:input-value = {&Summary} then "arglref,debit,credit" else "depositRef,postingdate,description,arglref,debit,credit,Voided,notes",
                          if rsReportType:input-value = {&Summary} then "GL Ref,Debit,Credit" else  "DepositRef,Posting Date,Description,GL Ref,Debit,Credit,Void,Notes", 
                          std-ch,
                          "Processing_Summary_GL_Entries_For_Files_" + replace(string(dtStartDate), "/", "") + "_to_" + replace(string(dtEndDate), "/", "") + "_" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tglPayment for tglPayment.
  define buffer bfglPayment for glPayment.
  
  define variable ddebit  as decimal  no-undo.
  define variable dcredit as decimal  no-undo.
    
  close query brwData. 
  empty temp-table glPayment.
  
  do with frame {&frame-name}:
  end.
  
  assign
      deDebit              = 0
      deCredit             = 0
      fCredit:screen-value = ""
      fDebit:screen-value  = ""
      .
      
  for each tglPayment where tglPayment.depositRef = (if cbDepRef:input-value in frame {&frame-name} = {&ALL} then tglPayment.depositRef else cbDepRef:input-value) and
                            tglPayment.voided     = if tVoided:checked then "Yes" else tglPayment.voided:
    assign
        deDebit   = deDebit   +  tglPayment.debit
        deCredit  = deCredit  +  tglPayment.credit
        .
    create glPayment.
    buffer-copy tglPayment except tglPayment.description to glPayment.
    glPayment.description = tglPayment.agentID + " " + tglPayment.chknum + " " + tglPayment.pmtid + " " + string(date(tglPayment.postingdate)).
  end.
  
  
  /**/
  if rsReportType:input-value = {&Summary}
    then
     do:
       for each bfglPayment break by arglRef :
         if first-of (bfglPayment.arglRef) 
          then
           do:
             assign
                 dDebit  = 0
                 dCredit = 0
                 .
                 
             for each glPayment where glPayment.arglRef = bfglPayment.arglRef :
                     
               assign
                    dDebit   = dDebit   +  glPayment.debit
                    dCredit  = dCredit  +  glPayment.credit
                    .
             end.
              
             assign
                 bfglPayment.debit   = dDebit  
                 bfglPayment.credit  = dCredit 
                 .
           end.
         else
          delete bfglPayment.   
       end.
     end.
     
  /* Configure the browse based on the report type */
  run configBrowse in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/     
  do with frame {&frame-name}:
  end.  

  close query brwData.

  empty temp-table tglPayment.

  dtStartDate = date(tMonth,1,tYear).
  
  if tMonth = 12 
   then
    dtEndDate = date(1,1,tYear + 1) - 1.
   else
    dtEndDate = date(tMonth + 1, 1 ,tYear) - 1.

  run server/queryglpayment.p (input  {&All},
                               input  dtStartDate,
                               input  dtEndDate,
                               output table tglPayment,
                               output std-lo,
                               output std-ch).
  
  if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box info buttons ok.
      return.
    end.

  run initialiseFilter in this-procedure.
  
  /* Enable/disable filter button based on selected values */
  run setFilterButton in this-procedure.
  if rsReportType:input-value = {&Summary}
   then
    assign
        cbDepRef:screen-value   = {&All}
        cbDepRef:sensitive      = false
        bFilterClear:sensitive  = false
        .
   else
    cbDepRef:sensitive     = true.

  run filterData in this-procedure.  
  run showData   in this-procedure.
  
  assign
      cbDepRef:sensitive = (rsReportType:input-value ne {&Summary}) and (query brwData:num-results > 0)
      tVoided:sensitive  = cbDepRef:sensitive. 
  
  setStatusRecords(query brwData:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getPeriod C-Win 
PROCEDURE getPeriod :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  run dialogperiod.w (input-output tMonth,
                      input-output tYear,
                      output std-ch).
  
  resultsChanged(false).
  fPeriod:screen-value = getPeriodName(tMonth,tYear).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initialiseFilter C-Win 
PROCEDURE initialiseFilter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define buffer tglPayment for tglPayment.
  
  assign
      cbDepRef:list-items    = ""
      cbDepRef:list-items    = {&ALL}
      .

  /* Set the filters based on the data returned, with ALL as the first option */
  for each tglPayment
    break by tglPayment.depositRef:
    if first-of(tglPayment.depositRef) and tglPayment.depositRef <> "" and tglPayment.depositRef <> "?"
     then
      cbDepRef:add-last(tglPayment.depositRef).
  end.
  
  cbDepRef:screen-value  = if (ipcDepRef ne "" and lookup(ipcDepRef,cbDepRef:list-items) > 0) then ipcDepRef else {&ALL}. 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterButton C-Win 
PROCEDURE setFilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if (cbDepRef:input-value  ne {&ALL} and cbDepRef:input-value  ne "") or tVoided:checked
   then
    bFilterClear:sensitive = true.
   else   
    bFilterClear:sensitive = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showData C-Win 
PROCEDURE showData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  dataSortDesc = not dataSortDesc.
    
  run sortData in this-procedure ("").
  
  /* Makes filters enable-disable based on the data */  
  cbDepRef:sensitive in frame {&frame-name}  = query brwData:num-results > 0.

  assign       
      fDebit:format  = getFormattedNumber(input deDebit,  input fDebit:handle)
      fCredit:format = getFormattedNumber(input deCredit, input fCredit:handle)  
      no-error.
  
  assign
      fDebit:screen-value  = string(deDebit)
      fCredit:screen-value = string(deCredit)
      .
  
  run adjustTotals in this-procedure.
  
  /* Makes widget enable-disable based on the data */ 
  if query brwData:num-results > 0 
   then
    assign 
        browse brwData:sensitive = true               
               bGetCSV:sensitive = true                              
               .      
   else
    assign 
        browse brwData:sensitive = false
               bGetCSV:sensitive = false                               
               .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by glPayment.pmtID by glPayment.artranID ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign      
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 22
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 32
      .
  
  run adjustTotals in this-procedure.
  
  run ShowScrollBars(frame {&frame-name}:handle, no, no).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFormattedNumber C-Win 
FUNCTION getFormattedNumber RETURNS CHARACTER
  ( input deTotal as decimal,
    input hWidget as handle ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cNumberFormat  as character no-undo.
  define variable cTotalScrValue as character no-undo.
  define variable deWidgetWidth  as decimal   no-undo.
  
  cTotalScrValue = string(deTotal).
     
  /* account for negative numbers */
  if deTotal < 0
   then 
    cNumberFormat = cNumberFormat + "(".
        
  /* loop through the absolute value of the number cast as an int64 */
  do std-in = length(string(int64(absolute(deTotal)))) to 1 by -1:
    if std-in modulo 3 = 0
     then 
      cNumberFormat = cNumberFormat + (if std-in = length(string(int64(absolute(deTotal)))) then ">" else ",>").
     else 
      cNumberFormat = cNumberFormat + (if std-in = 1 then "Z" else ">").
  end.
     
  /* if the number had a decimal value */
  if index(cTotalScrValue, ".") > 0 
   then 
    cNumberFormat = cNumberFormat + ".99".
         
  /* account for negative numbers */
  if deTotal < 0
   then 
    cNumberFormat = cNumberFormat + ")".
  
  do std-in = 1 to length(cNumberFormat):
    std-ch = substring(cNumberFormat,std-in,1).
    case std-ch:
      when "(" then deWidgetWidth = deWidgetWidth + 5.75.
      when ")" then deWidgetWidth = deWidgetWidth + 5.75.      
      when "Z" then deWidgetWidth = deWidgetWidth + 12.
      when ">" then deWidgetWidth = deWidgetWidth + 8.
      when "," then deWidgetWidth = deWidgetWidth + 4.
      when "." then deWidgetWidth = deWidgetWidth + 4.
      when "9" then deWidgetWidth = deWidgetWidth + 10.
    end case.
  end.
  
  /* set width of the widget as per format */
  hWidget:width-pixels = deWidgetWidth no-error. 
        
  RETURN cNumberFormat.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getVoid C-Win 
FUNCTION getVoid RETURNS CHARACTER
  ( ipVoid as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if glPayment.voided = 'YES'
   then 
    return 'Yes'.
  else
   return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

