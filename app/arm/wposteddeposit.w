&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wposteddeposit.w

  Description: Window for AR Deposits report

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Vikas jain

  Created: 01.30.2020
  
  Modifications:
  Date           Name       Description
  07/28/2020     AG         Changes to search criteria.
  02/04/2021     Shefali    Modified to show the total amount of the numeric fields
  02/22/2021     Shefali    Modified to show yes/blank in void column in grid"
  03/25/2021     Shefali    Removed mandatory check to date range
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/ar-def.i}
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/winshowscrollbars.i}
{lib/get-column.i}
{lib/brw-totalData-def.i}
{lib/getperiodname.i} /* Include function: getPeriodName */

/* Temp-table Definition */
{tt/artran.i       &tableAlias="ttartran"}         /* output of vaid transaction */
{tt/ardeposit.i}                                   /* contain data to show on browse */
{tt/ardeposit.i    &tableAlias="ttArDeposit"}      /* contain data reciced from server */
{tt/ardeposit.i    &tableAlias="tArDeposit"}       /* contain data for view */
{tt/ledgerreport.i}                                /* Contain info of GL accounts hit by deposit */
{tt/ledgerreport.i &tableAlias="glPaymentDetail"}  /* contain info of GL accounts hit by each payment of deposit */

define variable iSelectedDepositID as integer   no-undo.
define variable dColumnWidth       as decimal   no-undo.
define variable lDefaultAgent      as logical   no-undo.
define variable dtFromDate         as date      no-undo.
define variable dtToDate           as date      no-undo.
define variable lcheck             as logical   no-undo.
define variable iDepositID         as integer   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwardeposit

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ardeposit

/* Definitions for BROWSE brwardeposit                                  */
&Scoped-define FIELDS-IN-QUERY-brwardeposit ardeposit.bankID ardeposit.depositRef ardeposit.depositType ardeposit.depositdate ardeposit.transdate ardeposit.amount getVoid(ardeposit.void) @ ardeposit.void ardeposit.pmtamount   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwardeposit   
&Scoped-define SELF-NAME brwardeposit
&Scoped-define QUERY-STRING-brwardeposit for each ardeposit
&Scoped-define OPEN-QUERY-brwardeposit open query {&SELF-NAME} for each ardeposit.
&Scoped-define TABLES-IN-QUERY-brwardeposit ardeposit
&Scoped-define FIRST-TABLE-IN-QUERY-brwardeposit ardeposit


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwardeposit}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btSetPeriod btClear btGo bAgentLookup ~
flAgentID fPostFrom fPostTo fSearch tbIncludeVoid brwardeposit RECT-77 ~
RECT-79 RECT-80 RECT-83 RECT-81 
&Scoped-Define DISPLAYED-OBJECTS flAgentID flName fPostFrom fPostTo fSearch ~
tbIncludeVoid fVoidDate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getVoid C-Win 
FUNCTION getVoid RETURNS CHARACTER
  ( ipVoid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwardeposit 
       MENU-ITEM m_View_Deposit LABEL "View Deposit"  
       MENU-ITEM m_View_Payments LABEL "View Payments" 
       MENU-ITEM m_View_Period_Payments_GL_Rep LABEL "View Period Cash GL Transactions".


/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup  NO-FOCUS
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Agent lookup".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bOpen  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open detail".

DEFINE BUTTON bPrelimRpt  NO-FOCUS
     LABEL "Prelim" 
     SIZE 7.2 BY 1.71 TOOLTIP "Preliminary Report".

DEFINE BUTTON btClear 
     IMAGE-UP FILE "images/s-cross.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 4.8 BY 1.14 TOOLTIP "Blank out the date range".

DEFINE BUTTON btGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE BUTTON btSetPeriod 
     IMAGE-UP FILE "images/s-calendar.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 4.8 BY 1.14 TOOLTIP "Set current open period as date range".

DEFINE BUTTON btVoid  NO-FOCUS
     LABEL "Void" 
     SIZE 7.2 BY 1.71 TOOLTIP "Void Deposit".

DEFINE VARIABLE flAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE fPostFrom AS DATE FORMAT "99/99/99":U 
     LABEL "Posted" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fPostTo AS DATE FORMAT "99/99/99":U 
     LABEL "To" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 25 BY .95 NO-UNDO.

DEFINE VARIABLE fVoidDate AS DATE FORMAT "99/99/99":U 
     LABEL "Use Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-77
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 18.2 BY 3.1.

DEFINE RECTANGLE RECT-79
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 122.6 BY 3.1.

DEFINE RECTANGLE RECT-80
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE .6 BY .05.

DEFINE RECTANGLE RECT-81
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 43.4 BY 3.1.

DEFINE RECTANGLE RECT-83
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE .6 BY 2.29.

DEFINE VARIABLE tbIncludeVoid AS LOGICAL INITIAL no 
     LABEL "Include Voided" 
     VIEW-AS TOGGLE-BOX
     SIZE 18 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwardeposit FOR 
      ardeposit SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwardeposit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwardeposit C-Win _FREEFORM
  QUERY brwardeposit DISPLAY
      ardeposit.bankID                           label  "Bank"            format "x(35)"
ardeposit.depositRef                             label  "Deposit Ref"     format "x(25)"
ardeposit.depositType                            label  "Type"            format "x(20)"
ardeposit.depositdate                     column-label  "Deposit!Date"    format "99/99/9999 "  width 15   
ardeposit.transdate                       column-label  "Post!Date"       format "99/99/9999 "  width 15
ardeposit.amount                          column-label  "Amount"          format ">,>>>,>>9.99" width 15
getVoid(ardeposit.void) @ ardeposit.void  column-label  "Voided"          format "x(12)"        width 15
ardeposit.pmtamount                       column-label  "Payments"        format ">,>>>,>>9.99"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 183.6 BY 15.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     btSetPeriod AT ROW 2.91 COL 51.6 WIDGET-ID 472 NO-TAB-STOP 
     btClear AT ROW 2.91 COL 47 WIDGET-ID 470 NO-TAB-STOP 
     bExport AT ROW 2 COL 126.6 WIDGET-ID 8
     bOpen AT ROW 2 COL 133.6 WIDGET-ID 446
     bPrelimRpt AT ROW 2 COL 176.8 WIDGET-ID 456 NO-TAB-STOP 
     btGo AT ROW 2 COL 95 WIDGET-ID 262
     btVoid AT ROW 2 COL 169.8 WIDGET-ID 458 NO-TAB-STOP 
     bAgentLookup AT ROW 1.76 COL 28 WIDGET-ID 454 NO-TAB-STOP 
     flAgentID AT ROW 1.86 COL 12 COLON-ALIGNED WIDGET-ID 352
     flName AT ROW 1.86 COL 31 COLON-ALIGNED NO-LABEL WIDGET-ID 424
     fPostFrom AT ROW 2.95 COL 12 COLON-ALIGNED WIDGET-ID 360
     fPostTo AT ROW 2.95 COL 31 COLON-ALIGNED WIDGET-ID 362
     fSearch AT ROW 2.95 COL 66 COLON-ALIGNED WIDGET-ID 426
     tbIncludeVoid AT ROW 2.43 COL 105.6 WIDGET-ID 396
     fVoidDate AT ROW 2.33 COL 152.6 COLON-ALIGNED WIDGET-ID 460
     brwardeposit AT ROW 4.62 COL 2.4 WIDGET-ID 200
     "Parameters" VIEW-AS TEXT
          SIZE 11.6 BY .62 AT ROW 1.05 COL 4 WIDGET-ID 266
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.1 COL 126.4 WIDGET-ID 194
     "Void" VIEW-AS TEXT
          SIZE 4.6 BY .62 AT ROW 1.1 COL 144.2 WIDGET-ID 468
     RECT-77 AT ROW 1.33 COL 124.8 WIDGET-ID 190
     RECT-79 AT ROW 1.33 COL 2.4 WIDGET-ID 270
     RECT-80 AT ROW 3.95 COL 72 WIDGET-ID 462
     RECT-83 AT ROW 1.71 COL 103 WIDGET-ID 406
     RECT-81 AT ROW 1.33 COL 142.6 WIDGET-ID 466
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 189 BY 19.86 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Posted Deposits"
         HEIGHT             = 19.33
         WIDTH              = 187
         MAX-HEIGHT         = 34.43
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.43
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwardeposit fVoidDate DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bExport IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bOpen IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPrelimRpt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwardeposit:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwardeposit:HANDLE
       brwardeposit:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwardeposit:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR BUTTON btVoid IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flName IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flName:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN fVoidDate IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwardeposit
/* Query rebuild information for BROWSE brwardeposit
     _START_FREEFORM
open query {&SELF-NAME} for each ardeposit.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwardeposit */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Posted Deposits */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Posted Deposits */
DO:
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Posted Deposits */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup C-Win
ON CHOOSE OF bAgentLookup IN FRAME DEFAULT-FRAME /* agentlookup */
DO:
  define variable cAgentID  as character no-undo.
  define variable cName     as character no-undo.
    
  run dialogagentlookup.w(input flAgentID:input-value,
                          input "",      /* Selected State ID */
                          input true,    /* Allow 'ALL' */
                          output cAgentID,
                          output std-ch, /* Agent state ID */
                          output cName,
                          output std-lo).
   
  if not std-lo 
   then
    return no-apply.
         
  empty temp-table ardeposit.
  close query brwArDeposit.
    
  run setWidgetState in this-procedure.
  
  assign
      flAgentID:screen-value = cAgentID
      flName:screen-value    = cName
      . 
  
  resultsChanged(false).
  
  if lDefaultAgent               and
     flAgentID:input-value <> "" and
     flAgentID:input-value <> {&ALL}
   then
    /* Set default AgentID */
    publish "SetDefaultAgent" (input flAgentID:input-value).
  
  if flAgentID:input-value <> ""   
   then                            
     run getData in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOpen C-Win
ON CHOOSE OF bOpen IN FRAME DEFAULT-FRAME /* Open */
DO:
  run viewPayment in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrelimRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrelimRpt C-Win
ON CHOOSE OF bPrelimRpt IN FRAME DEFAULT-FRAME /* Prelim */
DO:
  run prelimRpt in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwardeposit
&Scoped-define SELF-NAME brwardeposit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwardeposit C-Win
ON DEFAULT-ACTION OF brwardeposit IN FRAME DEFAULT-FRAME
DO:
  run viewPayment in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwardeposit C-Win
ON ROW-DISPLAY OF brwardeposit IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}
  
  /* Change colore of field to RED if value is negative*/
  if ardeposit.amount lt 0
   then
    ardeposit.amount:fgcolor in browse brwArDeposit = 12.

  if ardeposit.pmtamount lt 0
   then
    ardeposit.pmtamount:fgcolor in browse brwArDeposit = 12.
  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwardeposit C-Win
ON START-SEARCH OF brwardeposit IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwardeposit C-Win
ON VALUE-CHANGED OF brwardeposit IN FRAME DEFAULT-FRAME
DO:  
  run setWidgetState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClear C-Win
ON CHOOSE OF btClear IN FRAME DEFAULT-FRAME
DO:
 fPostFrom:screen-value = "".
 fPostTo:screen-value = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGo C-Win
ON CHOOSE OF btGo IN FRAME DEFAULT-FRAME /* Go */
OR 'RETURN' of flAgentID
DO:
  if not validAgent()
   then
    return no-apply.
    
  run getData in this-procedure.           
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btSetPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btSetPeriod C-Win
ON CHOOSE OF btSetPeriod IN FRAME DEFAULT-FRAME
DO:
  fPostFrom:screen-value = string(dtFromDate).
  fPostTo:screen-value   = string(dtToDate).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btVoid
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btVoid C-Win
ON CHOOSE OF btVoid IN FRAME DEFAULT-FRAME /* Void */
DO:
  run voidDeposit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID C-Win
ON VALUE-CHANGED OF flAgentID IN FRAME DEFAULT-FRAME /* Agent ID */
DO:
  resultsChanged(false).
  
  assign
      flName:screen-value  = ""
      bOpen:sensitive      = false
      bExport:sensitive    = false
      btVoid:sensitive     = false
      bPrelimRpt:sensitive = false
      fVoidDate:sensitive  = false.
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fPostFrom
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fPostFrom C-Win
ON VALUE-CHANGED OF fPostFrom IN FRAME DEFAULT-FRAME /* Posted */
DO:  
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fPostTo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fPostTo C-Win
ON VALUE-CHANGED OF fPostTo IN FRAME DEFAULT-FRAME /* To */
DO:  
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON VALUE-CHANGED OF fSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Deposit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Deposit C-Win
ON CHOOSE OF MENU-ITEM m_View_Deposit /* View Deposit */
DO:
  if available ardeposit
   then
    run viewDeposit  in this-procedure (input ardeposit.depositID).
   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Payments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Payments C-Win
ON CHOOSE OF MENU-ITEM m_View_Payments /* View Payments */
DO:
  if available ardeposit
   then
    apply "choose" to bOpen in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Period_Payments_GL_Rep
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Period_Payments_GL_Rep C-Win
ON CHOOSE OF MENU-ITEM m_View_Period_Payments_GL_Rep /* View Period Cash GL Transactions */
DO:
  find current ardeposit no-error.
  if available ardeposit
   then
    do:
      run wglpayment.w persistent(flAgentID:input-value in frame {&frame-name},
                                  flName:input-value,
                                  input month(ardeposit.transdate),
                                  input year(ardeposit.transdate),
                                  input ardeposit.depositRef,
                                  input true /* input params passed or not */
                                  ).
    end.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tbIncludeVoid
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tbIncludeVoid C-Win
ON VALUE-CHANGED OF tbIncludeVoid IN FRAME DEFAULT-FRAME /* Include Voided */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow" anywhere.

btGo        :load-image             ("images/completed.bmp").
btGo        :load-image-insensitive ("images/completed-i.bmp").

bExport     :load-image             ("images/excel.bmp").
bExport     :load-image-insensitive ("images/excel-i.bmp").

bOpen       :load-image             ("images/open.bmp").
bOpen       :load-image-insensitive ("images/open-i.bmp").

bAgentLookup:load-image             ("images/s-lookup.bmp").
bAgentLookup:load-image-insensitive ("images/s-lookup-i.bmp").

bPrelimRpt  :load-image             ("images/pdf.bmp").
bPrelimRpt  :load-image-insensitive ("images/pdf-i.bmp").

btVoid      :load-image             ("images/rejected.bmp").
btVoid      :load-image-insensitive ("images/rejected-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  {&window-name}:window-state = window-minimized.   
 
  run enable_UI.     
  
  {lib/get-column-width.i &col="'Depositref'"    &var=dColumnWidth}
 
  publish "GetAutoDefaultAgent" (output lDefaultAgent).
  
  if lDefaultAgent
   then
    do:
      /* Get default AgentID */
      publish "GetDefaultAgent"(output std-ch).
      
      flAgentID:screen-value = std-ch.
      
      publish "getAgentName" (input flAgentID:input-value,
                              output std-ch,
                              output std-lo).
                              
      flName:screen-value = std-ch.  
    end.
  
  /* Getting date range from first and last open active period */   
  publish "getOpenPeriod" (output dtFromDate,output dtToDate).    
  
  fPostFrom:screen-value = "".                                   
  fPostTo:screen-value   = "". 
  
  /* When default posting option from config screen is allowed */ 
  publish 'GetDefaultPostingOption' (output std-lo).
  if std-lo
   then
    do:
      /* Set default posting date on screen */
      publish "getDefaultPostingDate"(output std-da).
      fVoidDate:screen-value = string(std-da, "99/99/99").
    end.
    
  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.

  apply 'entry' to flAgentID.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData C-Win 
PROCEDURE displayData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  close query brwArDeposit.
  empty temp-table ardeposit.
  
  define buffer ttArDeposit for ttArDeposit.

  for each ttArDeposit:
    create ardeposit.
    buffer-copy ttArDeposit to ardeposit.
  end.

  dataSortDesc = not dataSortDesc.
  if dataSortBy = ""
   then dataSortBy = "depositref".
   
  open query brwArDeposit preselect each ardeposit by ardeposit.depositID.
   
  run sortData in this-procedure (dataSortBy).
  
  setStatusCount(query brwArDeposit:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flAgentID flName fPostFrom fPostTo fSearch tbIncludeVoid fVoidDate 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE btSetPeriod btClear btGo bAgentLookup flAgentID fPostFrom fPostTo 
         fSearch tbIncludeVoid brwardeposit RECT-77 RECT-79 RECT-80 RECT-83 
         RECT-81 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwArDeposit:num-results = 0 
   then
    do: 
      message "There is nothing to export."
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table ardeposit:handle.
  run util/exporttable.p (table-handle std-ha,
                          "ardeposit",
                          "for each ardeposit",
                          "depositRef,depositID,bankID,depositType,depositdate,transDate,postedBy,amount,pmtamount,posted,ledgetID,voidLedgerID,voiddate,voidedby,archived,transDate,createDate,createBy,void",
                          "Deposit Ref,deposit ID,Bank,Type,Deposit Date,Post Date,Post By,Amount,Payment Amount,Posted,ledger ID,Void Ledger ID,Void Date,Voided By,Archived,Trans Date,Create Date,Create By,Void",
                          std-ch,
                          "Deposits_" + replace(string(today), "/", "") + "_" + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define buffer ttArDeposit for ttArDeposit.
    
 if fPostTo:input-value < fPostFrom:input-value 
   then
    do:
      message "Post To Date cannot be less than Post From Date."
          view-as alert-box.
      return.  
    end. 
    
  if fPostFrom:input-value > today    
   then
    do:
      message "Post From Date cannot be in future."
          view-as alert-box.
      return.
    end.
    
  if fPostTo:input-value > today    
   then
    do:
      message "Post To Date cannot be in future."
          view-as alert-box.
      return.
    end.
  
  if flAgentID:screen-value = "" or flAgentID:screen-value = "ALL" /* using date filter only when particular agentId is not entered*/
   then
    do:
     if (fPostFrom:input-value = ? or fPostTo:input-value = ?) and fSearch:input-value = ""
      then
       do:
        message "You must enter date range and/or search string when searching for all agents."
             view-as alert-box.
         return.
       end.
    end.
    
  empty temp-table ttArDeposit.
  
  {lib/brw-totalData.i &noShow=true}
                              
  /* server call */
  run server\querydeposits.p (input flAgentID:screen-value,
                              input fPostFrom:input-value, /* Post from date */
                              input fPostTo:input-value,   /* Post to date   */
                              input fSearch:input-value,
                              input tbIncludeVoid:checked,
                              output table ttArDeposit,
                              output std-lo,
                              output std-ch).
             
  if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box error buttons ok.
      return.
    end.
  
  run displayData in this-procedure.
  
  find first ardeposit no-error. 

  apply 'value-changed' to browse brwArDeposit.
 
  setStatusRecords(query brwArDeposit:num-results). 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE prelimRpt C-Win 
PROCEDURE prelimRpt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   
  run server/prelimdepositvoidgl.p (input ardeposit.depositID,
                                    output table ledgerreport,
                                    output table glPaymentDetail,
                                    output std-lo,
                                    output std-ch). 
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
    
  if not can-find(first ledgerreport) or
     not can-find(first glPaymentDetail)
   then
    do:
      message "Nothing to print."
          view-as alert-box error buttons ok.
      return.
    end.
      
  run util\ardepositvoidpdf.p(input {&view},
                              input table glPaymentDetail,
                              input table ledgerreport,                     
                              output std-ch).  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end. 

  if not available ardeposit
   then
    assign
        bExport:sensitive         = false
        bOpen:sensitive           = false
        bPrelimRpt:sensitive      = false 
        btVoid:sensitive          = false
        fVoidDate:sensitive       = false
        MENU-ITEM m_View_Deposit:sensitive   IN MENU POPUP-MENU-brwardeposit = false
        MENU-ITEM m_View_Payments:sensitive  IN MENU POPUP-MENU-brwardeposit = false
        .                                
   else
    assign
        bExport:sensitive    = true
        bOpen:sensitive      = true
        bPrelimRpt:sensitive = not ardeposit.void 
        btVoid:sensitive     = not ardeposit.void
        fVoidDate:sensitive  = not ardeposit.void
        MENU-ITEM m_View_Deposit:sensitive   IN MENU POPUP-MENU-brwardeposit = true
        MENU-ITEM m_View_Payments:sensitive  IN MENU POPUP-MENU-brwardeposit = true
        .
           
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by ardeposit.depositref ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
  
  {lib/brw-totalData.i &excludeColumn="2,3,4,5"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewDeposit C-Win 
PROCEDURE viewDeposit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipdepositID as integer no-undo.

  empty temp-table tardeposit.
  define buffer ttArDeposit for ttArDeposit.
  
  for first ttArDeposit where ttArDeposit.depositID =  ipdepositID:
    create tardeposit .
    buffer-copy ttArDeposit to tardeposit.
  end.
  
  run dialogmodifydeposit.w ( input-output table tardeposit ,
                              input "View",
                              output lcheck).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewPayment C-Win 
PROCEDURE viewPayment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "OpenWindow" (input "wpaymentreport",             /*childtype*/
                        input string(ardeposit.depositID),  /*childid*/
                        input "wpaymentreport.w",           /*window*/
                        input "integer|input|" + string(ardeposit.depositID),    /*parameters*/                               
                        input this-procedure).              /*currentProcedure handle*/ 
 
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE voidDeposit C-Win 
PROCEDURE voidDeposit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lViewPdf   as logical   no-undo.
 
  do with frame {&frame-name}:
  end.
  
  if not available(ardeposit) 
   then
    return.
    
  iSelectedDepositID = ardeposit.depositID.
  
  if fVoidDate:input-value = ?
   then
    do:
      message "Void date cannot be blank."
        view-as alert-box error buttons ok.
      return.      
    end.
    
  if fVoidDate:input-value > today
   then
    do:
      message "Void date cannot be in future."
          view-as alert-box error buttons ok.    
      return.
    end.
    
  if fVoidDate:input-value < date(ardeposit.transdate)
    then
      do:
         message  "Void date can not be prior to posting date."
          view-as alert-box error buttons ok.
         return.
     end. 
  
  run dialogviewledger.w (input "Void Deposit Ledger",output std-lo).  /*post*/
     
  if not std-lo
   then
    return.
    
  publish "GetViewPdf" (output lViewPdf).  
  
  /*Server Call*/
  run server\voidtransaction.p(input 0,
                               input iSelectedDepositID,
                               input fVoidDate:input-value,
                               input lViewPdf,
                               output table ledgerreport,
                               output table glPaymentDetail,
                               output table ttArTran,
                               output std-lo,
                               output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
   else
    message "Void was successful."
        view-as alert-box information buttons ok.  
  
  /*Viewing Pdf*/
  if lViewPdf
   then
    do:
      if not can-find(first ledgerreport) or
         not can-find(first glPaymentDetail)
       then
        message "Nothing to print."
            view-as alert-box error buttons ok.
       else
        run util\ardepositvoidpdf.p(input {&view},
                                    input table glPaymentDetail ,   /* Payment Details come first */
                                    input table ledgerreport,       /* Summary records come next */              
                                    output std-ch). 
    end.
  find first ardeposit where ardeposit.depositID = iSelectedDepositID no-error.
  if available ardeposit
   then
    do:
        if tbIncludeVoid:checked in frame {&frame-name}
         then
          assign
           ardeposit.void = true.
         else
          delete ardeposit.
    end.
    
  open query brwardeposit preselect each ardeposit by ardeposit.depositID.
  
  run sortData in this-procedure(dataSortBy).
  
  run setWidgetState in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 22
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 26
      .
 
  run ShowScrollBars(frame {&frame-name}:handle, no, no).
  {lib/resize-column.i &col="'depositref'"    &var=dColumnWidth}
  {lib/brw-totalData.i &excludeColumn="2,3,4,5"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getVoid C-Win 
FUNCTION getVoid RETURNS CHARACTER
  ( ipVoid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if ardeposit.void 
   then 
    return "Yes".
  else
   return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if flAgentID:input-value = {&ALL}
   then
    flName:screen-value = {&NotApplicable}.
       
  else if flAgentID:input-value <> {&ALL} and
      flAgentID:input-value <> ""
   then
    do:
      publish "getAgentName" (input flAgentID:input-value,
                              output std-ch,
                              output std-lo).                                               
      if not std-lo 
       then 
        do:
          assign 
              flAgentID:screen-value = "" 
              flName:screen-value    = ""
              .
          return false. /* Function return value. */
        end.
      flName:screen-value = std-ch.
    end. 
  
  resultsChanged(false).  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

