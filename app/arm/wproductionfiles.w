&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wtransaction.w

  Description: Window for AR posted transactions

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Anjly

  Created: 08.09.2019
  Modified:
  Date        Name     Comments
  01/19/2021  Shefali  Added Voided checkbox in UI.

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/ar-def.i}
{lib/winshowscrollbars.i}
{lib/ar-gettrantype.i}   /* Include function: getTranType */
{lib/ar-getentitytype.i} /* Include function: getEntityType */

{tt/prodfile.i       &tableAlias="ttprodFile"}         /* used for data transfer */
{tt/prodfile.i       &tableAlias="arProdFile"}     /* contain filtered data */
{tt/prodfile.i       &tableAlias="ttarProdFile"}   /* contain data recived from server */


define variable iSelectedprodFileID as integer   no-undo.
define variable cAgentID          as character no-undo.
define variable lDefaultAgent     as logical   no-undo.
define variable hPopupMenu        as handle    no-undo.
define variable hPopupMenuItem    as handle    no-undo.
define variable hPopupMenuItem1   as handle    no-undo.
define variable hPopupMenuItem2   as handle    no-undo.
define variable dtFromDate        as date      no-undo.
define variable dtToDate          as date      no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwarProdFile

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES arProdFile

/* Definitions for BROWSE brwarProdFile                                 */
&Scoped-define FIELDS-IN-QUERY-brwarProdFile arProdFile.agentname + " ( " + arProdFile.agentID + " )" "Agent" arProdFile.filenumber "File Number" arProdFile.invoiceDate "Posted" arProdFile.invoicedAmount "Total" arProdFile.appliedAmount "Applied" arProdFile.cancelledAmount "Write-off" arProdFile.balance "Balance"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwarProdFile   
&Scoped-define SELF-NAME brwarProdFile
&Scoped-define QUERY-STRING-brwarProdFile for each arProdFile
&Scoped-define OPEN-QUERY-brwarProdFile open query {&SELF-NAME} for each arProdFile.
&Scoped-define TABLES-IN-QUERY-brwarProdFile arProdFile
&Scoped-define FIRST-TABLE-IN-QUERY-brwarProdFile arProdFile


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwarProdFile}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btClear btSetPeriod flAgentID bAgentLookup ~
fPostFrom fPostTo bSearch tbFullyPaidFile bTranDetail bExport tSearch btGo ~
brwarProdFile RECT-79 RECT-81 RECT-82 
&Scoped-Define DISPLAYED-OBJECTS flAgentID flName fPostFrom fPostTo ~
tbFullyPaidFile tSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VARIABLE C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwArPostedTran 
       MENU-ITEM m_Transaction_Detail LABEL "Transaction Detail".


/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup 
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Agent lookup".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "I" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search".

DEFINE BUTTON btClear 
     IMAGE-UP FILE "images/s-cross.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 4.8 BY 1.14 TOOLTIP "Blank out the date range".

DEFINE BUTTON btGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE BUTTON bTranDetail  NO-FOCUS
     LABEL "TranDetail" 
     SIZE 7.2 BY 1.71 TOOLTIP "Transaction detail".

DEFINE BUTTON btSetPeriod 
     IMAGE-UP FILE "images/s-calendar.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 4.8 BY 1.14 TOOLTIP "Set current open period as date range".

DEFINE VARIABLE flAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN 
     SIZE 18.2 BY 1 NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 46.4 BY 1 NO-UNDO.

DEFINE VARIABLE fPostFrom AS DATE FORMAT "99/99/99":U 
     LABEL "Posted" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fPostTo AS DATE FORMAT "99/99/99":U 
     LABEL "To" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tSearch AS CHARACTER FORMAT "X(50)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 30.2 BY 1 TOOLTIP "Enter a file number to view it directly" NO-UNDO.

DEFINE RECTANGLE RECT-79
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 94 BY 3.33.

DEFINE RECTANGLE RECT-81
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 65.6 BY 3.33.

DEFINE RECTANGLE RECT-82
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 49.4 BY 3.33.

DEFINE VARIABLE tbFullyPaidFile AS LOGICAL INITIAL no 
     LABEL "Include Fully Paid Files" 
     VIEW-AS TOGGLE-BOX
     SIZE 26 BY .81 TOOLTIP "Select to include fully paid files also" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwarProdFile FOR 
      arProdFile SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwarProdFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwarProdFile C-Win _FREEFORM
  QUERY brwarProdFile DISPLAY
      arProdFile.agentname + " ( " + arProdFile.agentID + " )"    label         "Agent"    format "x(60)"  width 55   
arProdFile.filenumber       label         "File Number"       format "x(25)"    
arProdFile.invoiceDate      label         "Posted"      format "99/99/99" width 15
arProdFile.invoicedAmount   label         "Total"       format "->>>,>>9.99"  width 15
arProdFile.appliedAmount    label         "Applied"     format "->>>,>>9.99"  width 15
arProdFile.cancelledAmount  label         "Write-off"   format "->>>,>>9.99"  width 15
arProdFile.balance          label         "Balance"     format "->>>,>>9.99"  width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 159 BY 18.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     btClear AT ROW 3.05 COL 47.4 WIDGET-ID 448 NO-TAB-STOP 
     btSetPeriod AT ROW 3.05 COL 52.2 WIDGET-ID 450 NO-TAB-STOP 
     flAgentID AT ROW 2 COL 12.8 COLON-ALIGNED WIDGET-ID 352
     bAgentLookup AT ROW 1.91 COL 33 WIDGET-ID 350
     flName AT ROW 2 COL 35.8 COLON-ALIGNED NO-LABEL WIDGET-ID 424
     fPostFrom AT ROW 3.1 COL 12.8 COLON-ALIGNED WIDGET-ID 444
     fPostTo AT ROW 3.1 COL 31.2 COLON-ALIGNED WIDGET-ID 446
     bSearch AT ROW 2.05 COL 137.2 WIDGET-ID 356 NO-TAB-STOP 
     tbFullyPaidFile AT ROW 3.19 COL 59.2 WIDGET-ID 398
     bTranDetail AT ROW 2.05 COL 146.6 WIDGET-ID 396 NO-TAB-STOP 
     bExport AT ROW 2.05 COL 153.6 WIDGET-ID 404 NO-TAB-STOP 
     tSearch AT ROW 2.43 COL 104.2 COLON-ALIGNED WIDGET-ID 354
     btGo AT ROW 2.05 COL 85.6 WIDGET-ID 262 NO-TAB-STOP 
     brwarProdFile AT ROW 4.86 COL 3 WIDGET-ID 200
     "Parameters" VIEW-AS TEXT
          SIZE 11.2 BY .62 AT ROW 1.05 COL 4.2 WIDGET-ID 266
     RECT-79 AT ROW 1.24 COL 3 WIDGET-ID 270
     RECT-81 AT ROW 1.24 COL 96.6 WIDGET-ID 392
     RECT-82 AT ROW 1.24 COL 96.6 WIDGET-ID 452
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COLUMN 1.2 ROW 1
         SIZE 250.8 BY 29.24 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Production Files"
         HEIGHT             = 22.91
         WIDTH              = 163
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwarProdFile btGo DEFAULT-FRAME */
ASSIGN 
       brwarProdFile:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwArPostedTran:HANDLE
       brwarProdFile:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwarProdFile:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

/* SETTINGS FOR FILL-IN flName IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flName:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwarProdFile
/* Query rebuild information for BROWSE brwarProdFile
     _START_FREEFORM
open query {&SELF-NAME} for each arProdFile.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwarProdFile */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Production Files */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Production Files */
DO:  
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Production Files */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup C-Win
ON CHOOSE OF bAgentLookup IN FRAME DEFAULT-FRAME /* agentlookup */
DO:
  define variable cAgentID  as character no-undo.
  define variable cName     as character no-undo.
    
  run dialogagentlookup.w(input flAgentID:input-value,
                          input "",      /* Selected State ID */
                          input true,    /* Allow 'ALL' */
                          output cAgentID,
                          output std-ch, /* Agent state ID */
                          output cName,
                          output std-lo).
   
  if not std-lo  
   then
     return no-apply.
     
  assign
      flAgentID:screen-value = cAgentID
      flName:screen-value    = cName
      . 
  
  resultsChanged(false).
  
  if lDefaultAgent               and
     flAgentID:input-value <> "" and
     flAgentID:input-value <> {&ALL}
   then
    /* Set default AgentID */
    publish "SetDefaultAgent" (input flAgentID:input-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwarProdFile
&Scoped-define SELF-NAME brwarProdFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarProdFile C-Win
ON DEFAULT-ACTION OF brwarProdFile IN FRAME DEFAULT-FRAME
DO:
  if not available arProdFile then return.
  run showTransactionDetail in this-procedure.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarProdFile C-Win
ON ROW-DISPLAY OF brwarProdFile IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarProdFile C-Win
ON START-SEARCH OF brwarProdFile IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarProdFile C-Win
ON VALUE-CHANGED OF brwarProdFile IN FRAME DEFAULT-FRAME
DO:
  iSelectedprodFileID = arProdFile.fileARID.
  run setwidgets in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME DEFAULT-FRAME /* I */
OR 'RETURN' of tSearch
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClear C-Win
ON CHOOSE OF btClear IN FRAME DEFAULT-FRAME
DO:
 fPostFrom:screen-value = "".
 fPostTo:screen-value = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGo C-Win
ON CHOOSE OF btGo IN FRAME DEFAULT-FRAME /* Go */
OR 'RETURN' of flAgentID
DO:
  if not validAgent()
   then
    return no-apply.
        
  run getData in this-procedure.           
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bTranDetail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bTranDetail C-Win
ON CHOOSE OF bTranDetail IN FRAME DEFAULT-FRAME /* TranDetail */
do:
   run showTransactionDetail in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btSetPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btSetPeriod C-Win
ON CHOOSE OF btSetPeriod IN FRAME DEFAULT-FRAME
DO:
  fPostFrom:screen-value = string(dtFromDate).
  fPostTo:screen-value   = string(dtToDate).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID C-Win
ON VALUE-CHANGED OF flAgentID IN FRAME DEFAULT-FRAME /* Agent ID */
DO:
  resultsChanged(false).
  assign
      flName:screen-value     = ""
      tSearch:sensitive       = false 
      bSearch:sensitive       = false
      bExport:sensitive       = false
      bTranDetail:sensitive   = false
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Transaction_Detail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Transaction_Detail C-Win
ON CHOOSE OF MENU-ITEM m_Transaction_Detail /* Transaction Detail */
DO:
  run showTransactionDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tbFullyPaidFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tbFullyPaidFile C-Win
ON VALUE-CHANGED OF tbFullyPaidFile IN FRAME DEFAULT-FRAME /* Include Fully Paid Files */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

subscribe to "closeWindow" anywhere.
subscribe to "RefreshScreensForFileNumModify" anywhere.
subscribe to "CloseScreensForFileNumModify" anywhere.
 
/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.


btGo        :load-image            ("images/completed.bmp").
btGo        :load-image-insensitive("images/completed-i.bmp").

bSearch     :load-image            ("images/find.bmp").
bSearch     :load-image-insensitive("images/find-i.bmp").

bTranDetail :load-image            ("images/open.bmp").
bTranDetail :load-image-insensitive("images/open-i.bmp").

bExport     :load-image            ("images/excel.bmp").
bExport     :load-image-insensitive("images/excel-i.bmp").

bAgentLookup:load-image            ("images/s-lookup.bmp").
bAgentLookup:load-image-insensitive("images/s-lookup-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  {&window-name}:window-state = window-minimized.  
   
  run enable_UI.  
        
  publish "GetAutoDefaultAgent" (output lDefaultAgent).
  
  /* override the configration as it is currently disabled as per requirement*/
  lDefaultAgent = false.
  
  if lDefaultAgent
   then
    do:
      /* Get default AgentID */
      publish "GetDefaultAgent" (output cAgentID).
      flAgentID:screen-value = cAgentID.
    end.
    
  /* Getting date range from first and last open active period */   
  publish "getOpenPeriod" (output dtFromDate,output dtToDate).    
  
  fPostFrom:screen-value = string(dtFromDate).                                   
  fPostTo:screen-value   = string(dtToDate).
      
  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.  
  run setwidgets in this-procedure. 
  
  apply 'entry' to flAgentID.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseScreensForFileNumModify C-Win 
PROCEDURE CloseScreensForFileNumModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pAgentID as character.
 define input parameter pOldFileID as character.
 define input parameter pNewFileID as character.

 if can-find(first ttarProdFile where ttarProdFile.agentID = pAgentID and ttarProdFile.fileid = pOldFileID) or
    can-find(first ttarProdFile where ttarProdFile.agentID = pAgentID and ttarProdFile.fileid = pNewFileID) 
  then
   run closeWindow.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "WindowClosed" (input this-procedure).  
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flAgentID flName fPostFrom fPostTo tbFullyPaidFile tSearch 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE btClear btSetPeriod flAgentID bAgentLookup fPostFrom fPostTo bSearch 
         tbFullyPaidFile bTranDetail bExport tSearch btGo brwarProdFile RECT-79 
         RECT-81 RECT-82 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwarProdFile:num-results = 0 
   then
    do: 
      message "There is nothing to export."
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
  
  define buffer arProdFile for arProdFile.
  
  empty temp-table ttprodFile.
  
  for each arProdFile:
     create ttprodFile.
     buffer-copy arProdFile to ttprodFile.
  end.
  
  std-ha = temp-table ttprodFile:handle.
  run util/exporttable.p (table-handle std-ha,
                          "ttprodFile",
                          "for each ttprodFile",
                          "AgentID,Agentname,filenumber,fileID,invoiceDate,invoicedAmount,cancelledAmount,appliedAmount,balance",
                          "Agent ID,Agent Name,File Number,File ID,Invoice Date,Invoice Amount,cancelled Amount,Applied Amount,Balance",
                          std-ch,
                          "ProductionFiles-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).                          
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  do with frame {&frame-name}:
  end.

  define buffer ttarProdFile for ttarProdFile.
  
  close query brwarProdFile.
  empty temp-table arProdFile.

  for each ttarProdFile by ttarProdFile.agentfileid:
    if tSearch:screen-value <> "" and
      not ((ttarProdFile.agentname    matches "*" + tSearch:screen-value + "*") or
           (ttarProdFile.filenumber    matches "*" + tSearch:screen-value + "*")
           )
     then next.

    create arProdFile.
    buffer-copy ttarProdFile to arProdFile.
    assign arProdFile.cancelledAmount =  - 1 *  arProdFile.cancelledAmount
           arProdFile.appliedAmount   = - 1 * arProdFile.appliedAmount
           arProdFile.balance =  arProdFile.invoicedAmount -  arProdFile.cancelledAmount - arProdFile.appliedAmount .
  end.

  open query brwarProdFile preselect each arProdFile by arProdFile.agentID by arProdFile.agentfileid.
  run setBrowse in this-procedure.

  setStatusCount(query brwarProdFile:num-results) no-error.
  
  if can-find(first arProdFile) 
   then
    apply 'value-changed' to browse  brwarProdFile. 
    
   run setWidgets in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  define buffer ttarProdFile for ttarProdFile. 
  
  /*Validations*/
  if fPostTo:input-value < fPostFrom:input-value 
   then
    do:
      message "Post To Date cannot be less than Post From Date."
          view-as alert-box.
      return.  
    end. 
    
  if fPostFrom:input-value > today    
   then
    do:
      message "Post From Date cannot be in future."
          view-as alert-box.
      return.
    end.
    
  if fPostTo:input-value > today    
   then
    do:
      message "Post To Date cannot be in future."
          view-as alert-box.
      return.
    end.
  
  if flAgentID:screen-value = "" or flAgentID:screen-value = "ALL" /* using date filter only when particular agentId is not entered*/
   then
    do:
     if fPostFrom:input-value = ? or fPostTo:input-value = ?
      then
       do:
        message "You must enter date range when searching for all agents."
             view-as alert-box.
         return.
       end.
    end.  
                           
  run server\getproductionfiles.p(input flAgentID:screen-value,       /* Entity ID */                                
                                  input tbFullyPaidFile:checked,      /* Include Fully Paid Files */
                                  input fPostFrom:input-value,        /* from date */
                                  input fPostTo:input-value,          /* To date   */
                                  output table ttarProdFile,                                
                                  output std-lo,
                                  output std-ch).                              
  
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
    
  empty temp-table arProdFile.

  for each ttarProdFile by ttarProdFile.agentfileid:
    if tSearch:screen-value <> "" and
     not ( (ttarProdFile.agentname    matches "*" + tSearch:screen-value + "*") or
           (ttarProdFile.filenumber    matches "*" + tSearch:screen-value + "*")
           )
     then next.

    create arProdFile.
    buffer-copy ttarProdFile to arProdFile.
    assign arProdFile.cancelledAmount =  - 1 *  arProdFile.cancelledAmount
           arProdFile.appliedAmount   = - 1 * arProdFile.appliedAmount
           arProdFile.balance =  arProdFile.invoicedAmount -  arProdFile.cancelledAmount - arProdFile.appliedAmount .
    
  end.
  
  run setwidgets in this-procedure.
  
  open query brwarProdFile preselect each arProdFile by arProdFile.agentID by arProdFile.agentfileID.
  run setBrowse in this-procedure.
  
  setStatusRecords(query brwarProdFile:num-results). 
  
  if can-find(first arProdFile) 
   then
    apply 'value-changed' to browse brwarProdFile .
    
  run setwidgets in this-procedure. 
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshScreensForFileNumModify C-Win 
PROCEDURE RefreshScreensForFileNumModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pAgentID as character.
 define input parameter pOldFileID as character.
 define input parameter pNewFileID as character.

 if can-find(first ttarProdFile where ttarProdFile.agentID = pAgentID and ttarProdFile.fileid = pOldFileID) or
    can-find(first ttarProdFile where ttarProdFile.agentID = pAgentID and ttarProdFile.fileid = pNewFileID) 
  then
   run getdata.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setBrowse C-Win 
PROCEDURE setBrowse :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/    
  do with frame {&frame-name}:
  end.
  /*
  define buffer arProdFile for arProdFile.
  
  find first arProdFile no-error.

  if not available arProdFile then return.

  std-lo = can-find(first arProdFile where dueDate <> ?).   
  std-ha = brwarProdFile:first-column. 
  
  BLK:
  do while valid-handle(std-ha):     
    if std-ha:name = "duedate" 
     then 
      do:
        if std-lo then view std-ha.                         
        else hide std-ha.                        
            
        leave BLK.
      end.                  
    std-ha = std-ha:next-column.                   
  end. */
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setwidgets C-Win 
PROCEDURE setwidgets :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame default-frame:
  end.
  
  define buffer arProdFile for arProdFile.
  
  if not can-find( first arProdFile ) or
     query brwarProdFile:num-results = 0
   then
    assign
        tSearch:sensitive       = if tSearch:screen-value = "" then false else true 
        bSearch:sensitive       = if tSearch:screen-value = "" then false else true 
        bExport:sensitive       = false
        bTranDetail:sensitive   = false
        .
   else
    assign
        tSearch:sensitive       = true 
        bSearch:sensitive       = true
        bExport:sensitive       = true
        bTranDetail:sensitive   = true
        .

  tbFullyPaidFile:checked   = false.
  tbFullyPaidFile:sensitive = true.
          
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showTransactionDetail C-Win 
PROCEDURE showTransactionDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "OpenWindow" (input "wtransactiondetail",       /*childtype*/
                        input string(iSelectedprodFileID),  /*childid*/
                        input "wtransactiondetail.w",     /*window*/
                        input "integer|input|" + string(iSelectedprodFileID) + "^integer|input|0^character|input|" + "Production",    /*parameters*/                                 
                        input this-procedure).            /*currentProcedure handle*/ 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state = window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by arProdFile.agentID by arProdFile.agentfileID ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
  
  if can-find(first arProdFile) 
   then
    apply 'value-changed' to browse  brwarProdFile. 
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 21
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 3
      .
   
  run ShowScrollBars(frame {&frame-name}:handle, no, no).  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if flAgentID:input-value = ""
   then return false. /* Function return value. */
  
  else if flAgentID:input-value = {&ALL}
   then
    flName:screen-value = {&NotApplicable}.
       
  else if flAgentID:input-value <> {&ALL}
   then
    do:
      publish "getAgentName" (input flAgentID:input-value,
                              output std-ch,
                              output std-lo).                                               
      if not std-lo 
       then 
        do:
          assign 
              flAgentID:screen-value = "" 
              flName:screen-value    = ""
              .
          return false. /* Function return value. */
        end.
      flName:screen-value = std-ch.
    end. 
  
  resultsChanged(false). 
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

