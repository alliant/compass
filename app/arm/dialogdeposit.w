&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
  @Modified :
  Date        Name     Description 
  07/22/2020  AG       Modified to use deposit.posted instead of deposit.stat
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{tt/ardeposit.i}

/* Parameters Definitions ---                                           */
define output parameter table        for ardeposit.
define output parameter oploSuccess  as logical no-undo.

/* Standard library files */
{lib/std-def.i}
{lib/ar-def.i}

/* Local Variable Definitions --- */
define variable cCurrentUser  as character no-undo.
define variable cCurrentUID   as character no-undo.
define variable cDepositRef   as character no-undo.
define variable deAmount      as decimal   no-undo.
define variable daDepositdate as date      no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS flBankID flDepositRef flDepositType ~
flDepositDate flAmount Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS flBankID flDepositRef flDepositType ~
flDepositDate flAmount fMarkMandatory1 fMarkMandatory2 fMarkMandatory3 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE flAmount AS DECIMAL FORMAT "Z,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Amount" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE flBankID AS CHARACTER FORMAT "X(50)":U 
     LABEL "Bank ID" 
     VIEW-AS FILL-IN 
     SIZE 33 BY 1 NO-UNDO.

DEFINE VARIABLE flDepositDate AS DATE FORMAT "99/99/99":U 
     LABEL "Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 12.4 BY 1 NO-UNDO.

DEFINE VARIABLE flDepositRef AS CHARACTER FORMAT "X(50)":U 
     LABEL "Deposit Ref." 
     VIEW-AS FILL-IN 
     SIZE 33 BY 1 NO-UNDO.

DEFINE VARIABLE flDepositType AS CHARACTER FORMAT "X(50)":U 
     LABEL "Type" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fMarkMandatory1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory3 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     flBankID AT ROW 1.62 COL 15 COLON-ALIGNED WIDGET-ID 2
     flDepositRef AT ROW 2.81 COL 15 COLON-ALIGNED WIDGET-ID 4
     flDepositType AT ROW 4 COL 15 COLON-ALIGNED WIDGET-ID 6
     flDepositDate AT ROW 5.19 COL 15 COLON-ALIGNED WIDGET-ID 408
     flAmount AT ROW 6.38 COL 34 RIGHT-ALIGNED WIDGET-ID 8
     fMarkMandatory1 AT ROW 3.14 COL 48.4 COLON-ALIGNED NO-LABEL WIDGET-ID 52 NO-TAB-STOP 
     Btn_OK AT ROW 8.29 COL 12.4 WIDGET-ID 12
     Btn_Cancel AT ROW 8.29 COL 29 WIDGET-ID 10
     fMarkMandatory2 AT ROW 6.71 COL 33.4 COLON-ALIGNED NO-LABEL WIDGET-ID 268 NO-TAB-STOP 
     fMarkMandatory3 AT ROW 5.52 COL 27.8 COLON-ALIGNED NO-LABEL WIDGET-ID 410 NO-TAB-STOP 
     SPACE(22.79) SKIP(3.95)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Deposit" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flAmount IN FRAME Dialog-Frame
   ALIGN-R                                                              */
/* SETTINGS FOR FILL-IN fMarkMandatory1 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory2 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory3 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Deposit */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  run saveDeposit in this-procedure.
  if not oploSuccess
   then
    return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAmount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAmount Dialog-Frame
ON VALUE-CHANGED OF flAmount IN FRAME Dialog-Frame /* Amount */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flDepositDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flDepositDate Dialog-Frame
ON VALUE-CHANGED OF flDepositDate IN FRAME Dialog-Frame /* Date */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flDepositRef
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flDepositRef Dialog-Frame
ON VALUE-CHANGED OF flDepositRef IN FRAME Dialog-Frame /* Deposit Ref. */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Get current logged in username & UID */ 
publish "GetCredentialsName" (output cCurrentUser).
publish "GetCredentialsID"   (output cCurrentUID).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  run displayData in this-procedure.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.     
  
  assign
      fMarkMandatory1:screen-value = {&Mandatory}
      fMarkMandatory2:screen-value = {&Mandatory}
      fMarkMandatory3:screen-value = {&Mandatory}
      .
  
  assign
    cDepositRef   = flDepositRef:input-value
    deAmount      = flAmount:input-value
    daDepositDate = flDepositDate:input-value
    .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  Btn_OK:sensitive = (   flDepositRef:input-value  <> cDepositRef  and
                         flAmount:input-value      <> deAmount     and
                         flDepositDate:input-value <> ?                      
                      ) no-error.
                        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flBankID flDepositRef flDepositType flDepositDate flAmount 
          fMarkMandatory1 fMarkMandatory2 fMarkMandatory3 
      WITH FRAME Dialog-Frame.
  ENABLE flBankID flDepositRef flDepositType flDepositDate flAmount Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveDeposit Dialog-Frame 
PROCEDURE saveDeposit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  do with frame {&frame-name}:
  end.
 
  oploSuccess = false.
    
  if flDepositDate:input-value > today
   then
    do:  
      message "Deposit Date cannot be in future"
        view-as alert-box info button ok.
      return.
    end.
      

  create ardeposit.
  assign
      ardeposit.bankID      = flBankID:input-value
      ardeposit.depositRef  = flDepositRef:input-value
      ardeposit.depositType = flDepositType:input-value
      ardeposit.amount      = flAmount:input-value
      ardeposit.depositdate = flDepositDate:input-value
      .

  run server/newdeposit.p(input table ardeposit,
                          output std-in,
                          output std-dt,
                          output oploSuccess,
                          output std-ch).
     
  if not oploSuccess                         
   then
    do:
      empty temp-table ardeposit.     
      message std-ch 
        view-as alert-box.
      return.  
    end.
   
   assign
        ardeposit.depositID  = std-in
        ardeposit.createDate = std-dt
        ardeposit.username   = cCurrentUser 
        ardeposit.createdBy  = cCurrentUID
        .       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

