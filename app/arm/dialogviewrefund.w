&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogviewrefund.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Anjly Chanana

  Created:08/21/20
  
  @Modified :
  Date           Name            Description 
  06/02/21       SA              Modified according to refund
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{tt/artran.i &tablealias=ttartran}

/* Parameters Definitions                                           */
define input parameter ipcAction as character no-undo.
define input-output  parameter table for ttarTran. 
define output parameter oplSuccess as logical no-undo.

/* Standard library files */
{lib/ar-def.i}
{lib/std-def.i}

/* Local variable definitions */
define variable cDescription   as character no-undo.

define temp-table ttAction
   field action      as character
   field createdBy   as character
   field createdDate as character
   field ledgerID    as character.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwAction

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttAction

/* Definitions for BROWSE brwAction                                     */
&Scoped-define FIELDS-IN-QUERY-brwAction ttAction.Action ttAction.createdBy ttAction.CreatedDate ttAction.ledgerID   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAction   
&Scoped-define SELF-NAME brwAction
&Scoped-define QUERY-STRING-brwAction FOR EACH ttAction
&Scoped-define OPEN-QUERY-brwAction OPEN QUERY {&SELF-NAME} FOR EACH ttAction.
&Scoped-define TABLES-IN-QUERY-brwAction ttAction
&Scoped-define FIRST-TABLE-IN-QUERY-brwAction ttAction


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwAction}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS Btn_Cancel edDescription flEntityID ~
flRefundID flEntity flRefNum flCheckDate flRefundAmt tRevType ~
fltotalPymtAmt flAppliedAmountTotal flRefundAmtTotal flRemainingAmt 
&Scoped-Define DISPLAYED-OBJECTS edDescription flEntityID flRefundID ~
flEntity flRefNum flCheckDate flRefundAmt tRevType fltotalPymtAmt ~
flAppliedAmountTotal flRefundAmtTotal flRemainingAmt 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY  NO-FOCUS
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO  NO-FOCUS
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE edDescription AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 98.4 BY 2.81 NO-UNDO.

DEFINE VARIABLE flAppliedAmountTotal AS DECIMAL FORMAT "Z,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL " Applied" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19 BY 1 NO-UNDO.
DEFINE VARIABLE flCheckDate AS DATE FORMAT "99/99/99":U 
     LABEL "Check Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 12.4 BY 1 NO-UNDO.

DEFINE VARIABLE flEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 80.4 BY 1 NO-UNDO.

DEFINE VARIABLE flEntityID AS CHARACTER FORMAT "X(256)":U 
     LABEL "AgentID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE flRefNum AS CHARACTER FORMAT "X(256)":U 
     LABEL "Check/Reference" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE flRefundAmt AS DECIMAL FORMAT "Z,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Refunded" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE flRefundAmtTotal AS DECIMAL FORMAT "Z,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Total Refund" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19 BY 1 NO-UNDO.
DEFINE VARIABLE flRefundID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Refund ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19 BY 1 NO-UNDO.
DEFINE VARIABLE flRemainingAmt AS DECIMAL FORMAT "Z,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Unapplied" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE fltotalPymtAmt AS DECIMAL FORMAT "Z,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Check Amount" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE tRevType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Revenue Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAction FOR 
      ttAction SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAction Dialog-Frame _FREEFORM
  QUERY brwAction DISPLAY
      ttAction.Action               label  "Action"    format "x(15)"     
ttAction.createdBy              label  "By"              format "x(50)" 
ttAction.CreatedDate            label  "Date"            format "x(12)" 
ttAction.ledgerID               label  "Ledger ID"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-AUTO-VALIDATE NO-ROW-MARKERS NO-COLUMN-SCROLLING SEPARATORS NO-SCROLLBAR-VERTICAL SIZE 98.4 BY 3.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     Btn_Cancel AT ROW 16.86 COL 55.6 NO-TAB-STOP 
     Btn_OK AT ROW 16.86 COL 39 NO-TAB-STOP 
     edDescription AT ROW 9.43 COL 4.8 NO-LABEL WIDGET-ID 26
     flEntityID AT ROW 1.33 COL 19.6 COLON-ALIGNED WIDGET-ID 302
     flRefundID AT ROW 1.33 COL 81 COLON-ALIGNED WIDGET-ID 310
     flEntity AT ROW 2.48 COL 19.6 COLON-ALIGNED WIDGET-ID 102
     flRefNum AT ROW 3.62 COL 19.6 COLON-ALIGNED WIDGET-ID 42
     flCheckDate AT ROW 3.62 COL 87.6 COLON-ALIGNED WIDGET-ID 36
     flRefundAmt AT ROW 6.43 COL 39.6 RIGHT-ALIGNED WIDGET-ID 298
     tRevType AT ROW 4.76 COL 81 COLON-ALIGNED WIDGET-ID 64
     fltotalPymtAmt AT ROW 4.76 COL 39.6 RIGHT-ALIGNED WIDGET-ID 304
     flAppliedAmountTotal AT ROW 6.43 COL 101 RIGHT-ALIGNED WIDGET-ID 308
     flRefundAmtTotal AT ROW 7.57 COL 39.6 RIGHT-ALIGNED WIDGET-ID 306
     flRemainingAmt AT ROW 7.57 COL 101 RIGHT-ALIGNED WIDGET-ID 90
     brwAction AT ROW 12.57 COL 4.8 WIDGET-ID 200
     "Reason:" VIEW-AS TEXT
          SIZE 8.2 BY .62 AT ROW 8.81 COL 4.8 WIDGET-ID 28
     SPACE(93.19) SKIP(8.80)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "View Refund"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwAction flRemainingAmt Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BROWSE brwAction IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       edDescription:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.
/* SETTINGS FOR FILL-IN flAppliedAmountTotal IN FRAME Dialog-Frame
   ALIGN-R                                                              */
ASSIGN 
       flAppliedAmountTotal:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flCheckDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flEntity:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flEntityID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flRefNum:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN flRefundAmt IN FRAME Dialog-Frame
   ALIGN-R                                                              */
ASSIGN 
       flRefundAmt:READ-ONLY IN FRAME Dialog-Frame        = TRUE.
/* SETTINGS FOR FILL-IN flRefundAmtTotal IN FRAME Dialog-Frame
   ALIGN-R                                                              */
ASSIGN 
       flRefundAmtTotal:READ-ONLY IN FRAME Dialog-Frame        = TRUE.
ASSIGN 
       flRefundID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN flRemainingAmt IN FRAME Dialog-Frame
   ALIGN-R                                                              */
ASSIGN 
       flRemainingAmt:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fltotalPymtAmt IN FRAME Dialog-Frame
   ALIGN-R                                                              */
ASSIGN 
       fltotalPymtAmt:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tRevType:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAction
/* Query rebuild information for BROWSE brwAction
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH ttAction.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAction */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* View Refund */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel Dialog-Frame
ON CHOOSE OF Btn_Cancel IN FRAME Dialog-Frame /* Cancel */
DO:
  oplSuccess = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Save */
DO: 
  run saveArRefund in this-procedure.     
 /* If there was any error then do not close the dialog.  */
  if not oplSuccess
   then
    return no-apply.      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edDescription
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edDescription Dialog-Frame
ON VALUE-CHANGED OF edDescription IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAction
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.
  for first ttarTran:
    assign
        frame Dialog-frame:title =  if ipcAction ne {&view} then  "Edit Refund Reason - " + (if ttartran.type = "P" then "Payment" else "Credit") else "View Refund"
        flRefNum:label = if ttartran.type = "C" then "Reference" else "Check"
        flCheckDate:label = if ttartran.type = "C" then "Credit Date" else "Check Date"
        fltotalPymtAmt:label = if ttartran.type = "C" then "Credit Amount" else "Check Amount"
        .
  end.
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
     
   run displayData in this-procedure.  
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.   
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
   
  find first ttartran no-error.
  if available ttartran 
   then
    do:
      assign
          flEntity:screen-value       = ttartran.entityname
          flEntityID:screen-value     = ttartran.entityID 
          tRevType:screen-value       = ttartran.revenueType      
          flCheckDate:screen-value    = string(ttartran.tranDate)      
          flRefNum:screen-value       = ttartran.reference                      
          edDescription:screen-value  = ttartran.notes      
          flRemainingAmt:screen-value = string(ttartran.remainingAmt)
          flRefundAmt:screen-value    = string(abs(ttartran.tranamt))
          fltotalPymtAmt:screen-value = string(ttartran.totalPymtAmt)
          flAppliedAmountTotal:screen-value = string(abs(ttartran.appliedamtbyref) )
          flRefundAmtTotal:screen-value = string(abs(ttartran.totalrefundamt) )
          flRefundID:screen-value = string(ttartran.sourceID)  
          no-error.         
     
      create ttAction.
      assign
          ttAction.action       = "Created"
          ttAction.createdBy    = ttARTran.username
          ttAction.CreatedDate  = if (ttARTran.createdDate = ?) then "" else string(ttARTran.createdDate)
          ttAction.ledgerID     = "".
	
      create ttAction.
      assign
          ttAction.action       = "Posted"
          ttAction.createdBy    = ttARTran.postByName
          ttAction.CreatedDate  = if (ttARTran.postDate = ?) then "" else string(ttARTran.postDate)
          ttAction.ledgerID     = if (ttARTran.ledgerID = 0 ) then "" else string(ttARTran.ledgerID).
	
      create ttAction.
      assign
          ttAction.action       = "Void"
          ttAction.createdBy    = ttARTran.voidByName
          ttAction.CreatedDate  = if (ttARTran.voidDate = ?) then "" else string(ttARTran.voidDate)
          ttAction.ledgerID     = if (ttARTran.voidLedgerID = 0) then "" else string(ttARTran.voidLedgerID).
       
      OPEN QUERY brwAction FOR EACH ttAction.
      brwAction:Deselect-focused-row().
     
    end.
  run setWidgetState in this-procedure.
  /* Keep the copy of initial values. Required in enableDisableSave. */
  cDescription  = ttartran.notes.    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  Btn_OK:sensitive = cDescription <> edDescription:input-value
                     no-error.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY edDescription flEntityID flRefundID flEntity flRefNum flCheckDate 
          flRefundAmt tRevType fltotalPymtAmt flAppliedAmountTotal 
          flRefundAmtTotal flRemainingAmt 
      WITH FRAME Dialog-Frame.
  ENABLE Btn_Cancel edDescription flEntityID flRefundID flEntity flRefNum 
         flCheckDate flRefundAmt tRevType fltotalPymtAmt flAppliedAmountTotal 
         flRefundAmtTotal flRemainingAmt 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveArRefund Dialog-Frame 
PROCEDURE saveArRefund :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
 
 ttartran.notes = edDescription:input-value.
 
 /*Server call*/
 run server/modifytrannotes.p(input ttarTran.artranID,
                              input ttartran.notes,
                              output oplSuccess,
                              output std-ch).
 if not oplSuccess 
  then
   do:
     message std-ch
       view-as alert-box error buttons ok.
     return.  
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState Dialog-Frame 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
   
  assign 
    edDescription:read-only  = (if ipcAction = {&view} then true else false) 
    .   

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

