&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
  File: arconfig.p
  Description: Manage XML configuration file.
  Input Parameters:
      <none>
  Output Parameters:
      <none>
  Author: 
  Created: 11.22.2018 
  
  Modified    :
  Date        Name     Comments
  08/11/2020  AC       Added new IP "GetViewPdf" and "SetViewPdf".
  06/09/2021  MK       Added new IP "GetNotifyRequestor" and "SetNotifyRequestor".
  11/21/2024  SR       Modified to set default sys destination and action.
  11/29/2024  SC       Task#117302 Prepopulate the GL Ref numbers in Accounts Receivable screen.
  ----------------------------------------------------------------------*/
/* Standard Libraries */
{lib/std-def.i}
{lib/commonconfig.i}
{lib/configttfile.i &config="arm.xml"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 21.14
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


subscribe to "GetLoadLaunch"               anywhere. 
subscribe to "SetloadLaunch"               anywhere.

subscribe to "GetLockTimeout"              anywhere.
subscribe to "GetUserResponseLockTimeOut"  anywhere.

subscribe to "GetPostingConfig"            anywhere. 
subscribe to "SetPostingConfig"            anywhere.
 
subscribe to "GetDefaultAgent"             anywhere. 
subscribe to "SetDefaultAgent"             anywhere.

subscribe to "GetAutoDefaultAgent"         anywhere. 
subscribe to "SetAutoDefaultAgent"         anywhere.

subscribe to "GetDefaultPostingOption"     anywhere. 
subscribe to "SetDefaultPostingOption"     anywhere.

subscribe to "GetViewPdf"                  anywhere.
subscribe to "SetViewPdf"                  anywhere.

subscribe to "GetNotifyRequestor"          anywhere.
subscribe to "SetNotifyRequestor"          anywhere.

subscribe to "SetSysDestEntity"            anywhere. 
subscribe to "GetSysDestEntity"            anywhere.

subscribe to "SetSysDestAction"            anywhere. 
subscribe to "GetSysDestAction"            anywhere.

subscribe to "GetGLRef"                    anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-GetAutoDefaultAgent) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAutoDefaultAgent Procedure 
PROCEDURE GetAutoDefaultAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical no-undo.
  std-ch = getOption("AutoDefaultAgent").
  if std-ch = "" 
   then std-ch = getSetting("AutoDefaultAgent").
  
  if lookup(std-ch, ",no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetDefaultAgent) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetDefaultAgent Procedure 
PROCEDURE GetDefaultAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define output parameter opcDefaultAgent as character no-undo.
 
 opcDefaultAgent = getOption("DefaultAgent").
 
 if opcDefaultAgent = "" 
  then
   opcDefaultAgent = getSetting("DefaultAgent").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetDefaultPostingOption) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetDefaultPostingOption Procedure 
PROCEDURE GetDefaultPostingOption :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical no-undo.
  std-ch = getOption("DefaultPostingOption").
  if std-ch = "" 
   then std-ch = getSetting("DefaultPostingOption").
  
  if lookup(std-ch, ",no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetGLRef) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetGLRef Procedure 
PROCEDURE GetGLRef :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter opcARGLRef              as character no-undo.
  define output parameter opcPaymentCashGLRef     as character no-undo.
  define output parameter opcRefundCashGLRef      as character no-undo.
  define output parameter opcARGLRefDesc          as character no-undo.
  define output parameter opcPaymentCashGLRefDesc as character no-undo.
  define output parameter opcRefundCashGLRefDesc  as character no-undo.
 
  assign
      opcARGLRef              = getSetting("ARGLRef")
      opcPaymentCashGLRef     = getSetting("PaymentCashGLRef")
      opcRefundCashGLRef      = getSetting("RefundCashGLRef")
      opcARGLRefDesc          = getSetting("ARGLRefDesc")
      opcPaymentCashGLRefDesc = getSetting("PaymentCashGLRefDesc")
      opcRefundCashGLRefDesc  = getSetting("RefundCashGLRefDesc")
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadLaunch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadLaunch Procedure 
PROCEDURE GetLoadLaunch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define output parameter oplstd as logical no-undo.
 
 std-ch = getOption("loadlaunch"). 
 
 if std-ch = "" 
  then 
   std-ch = getSetting("loadlaunch").

 if lookup(std-ch, "no,N,False,F,0") > 0 
  then
   oplstd = false.
  else
   oplstd = true.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLockTimeout) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLockTimeout Procedure 
PROCEDURE GetLockTimeout :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define output parameter opiTimeOut as integer no-undo.
 
 opiTimeOut = integer(getOption("LockTimeout")). 
 
 if opiTimeOut = 0 
  then 
   opiTimeOut = integer(getSetting("LockTimeout")).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetNotifyRequestor) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetNotifyRequestor Procedure 
PROCEDURE GetNotifyRequestor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define output parameter std-lo as logical no-undo.
 
  std-ch = getOption("NotifyRequestor").
  if std-ch = "" 
   then std-ch = getSetting("NotifyRequestor").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetPostingConfig) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPostingConfig Procedure 
PROCEDURE GetPostingConfig :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define output parameter opiPost as character no-undo.
 
 opiPost = getOption("Posting").
 
 if opiPost = ""  
  then 
   opiPost = getSetting("Posting").
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getSysDestAction) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getSysDestAction Procedure 
PROCEDURE getSysDestAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define output parameter opcSysDestAction as character no-undo.
 
 opcSysDestAction = getOption("SysDestAction").
 
 if opcSysDestAction = "" 
  then
   opcSysDestAction = getSetting("SysDestAction").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getSysDestEntity) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getSysDestEntity Procedure 
PROCEDURE getSysDestEntity :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define output parameter opcSysDestEntity as character no-undo.
 
 opcSysDestEntity = getOption("SysDestEntity").
 
 if opcSysDestEntity = "" 
  then
   opcSysDestEntity = getSetting("SysDestEntity").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetUserResponseLockTimeOut) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetUserResponseLockTimeOut Procedure 
PROCEDURE GetUserResponseLockTimeOut :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define output parameter opiTimeOut as integer no-undo.
 
 opiTimeOut = integer(getOption("UserResponseTimeout")). 
 
 if opiTimeOut = 0 
  then 
   opiTimeOut = integer(getSetting("UserResponseTimeout")).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetViewPdf) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetViewPdf Procedure 
PROCEDURE GetViewPdf :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define output parameter std-lo as logical no-undo.
 
  std-ch = getOption("ViewPDF").
  if std-ch = "" 
   then std-ch = getSetting("ViewPDF").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetAutoDefaultAgent) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetAutoDefaultAgent Procedure 
PROCEDURE SetAutoDefaultAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("AutoDefaultAgent", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetDefaultAgent) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetDefaultAgent Procedure 
PROCEDURE SetDefaultAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter ipcDefaultAgent as character no-undo.
 
 setOption("DefaultAgent", ipcDefaultAgent).
 setSetting("DefaultAgent", ipcDefaultAgent).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetDefaultPostingOption) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetDefaultPostingOption Procedure 
PROCEDURE SetDefaultPostingOption :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("DefaultPostingOption", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadLaunch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadLaunch Procedure 
PROCEDURE SetLoadLaunch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter iplloadlaunch as logical no-undo.
 
 setOption("LoadLaunch", string(iplloadlaunch)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetNotifyRequestor) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetNotifyRequestor Procedure 
PROCEDURE SetNotifyRequestor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter std-lo as logical no-undo.
   
 setOption("NotifyRequestor", string(std-lo)).
 setSetting("NotifyRequestor", string(std-lo)).
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetPostingConfig) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetPostingConfig Procedure 
PROCEDURE SetPostingConfig :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter ipcPost as character no-undo.
 
 setOption("Posting", ipcPost).
 setSetting("Posting", ipcPost).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-setSysDestAction) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setSysDestAction Procedure 
PROCEDURE setSysDestAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcSysDestAction as character no-undo.
 
 setOption("SysDestAction", ipcSysDestAction).
 setSetting("SysDestAction", ipcSysDestAction).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-setSysDestEntity) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setSysDestEntity Procedure 
PROCEDURE setSysDestEntity :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter ipcSysDestEntity as character no-undo.
 
 setOption("SysDestEntity", ipcSysDestEntity).
 setSetting("SysDestEntity", ipcSysDestEntity).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetViewPdf) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetViewPdf Procedure 
PROCEDURE SetViewPdf :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter std-lo as logical no-undo.
   
 setOption("ViewPDF", string(std-lo)).
 setSetting("ViewPDF", string(std-lo)).
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

