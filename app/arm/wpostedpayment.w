&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME eC-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS eC-Win 
/*------------------------------------------------------------------------
  File: wpostedpayment.w

  Description: Window for AR Payments

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Vikas jain

  Created: 07.11.2019
  Modifications:
  Date         Name     Description
  07/24/2020   AG       Changes to search using checkNum or depositRef or EntityId
  07/28/2020   AG       Changes to add PostFrom and PostTo date.
  01/14/2021   Shefali  Changes to sort data by deposit ref,post date, check amount.
  01/15/2021   Shefali  Changes to add pop-up menu "Transaction Details" , 
                        "Edit Payment Note" and "Apply Payment".
  02/04/2021   Shefali  Modified to show the total amount of the numeric fields"
  02/22/2021   Shefali  Modified to show yes/blank in void column in grid"
  03/25/2021   Shefali  Removed mandatory check to date range
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/ar-def.i}
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/winshowscrollbars.i}
{lib/brw-totalData-def.i}
{lib/ar-getentitytype.i} /* Include function: getEntityType */
{lib/ar-gettrantype.i}   /* Include function: getTranType */
{lib/get-column.i}

/* Temp-table Definition */
{tt/arpmt.i }
{tt/arpmt.i &tableAlias="ttarpmt"}
{tt/arpmt.i &tableAlias="tarpmt"}
{tt/artran.i       &tableAlias="ttartran"}         /* output of void transaction */
{tt/ledgerreport.i}                                /* Contain info of GL accounts hit by deposit */
{tt/ledgerreport.i &tableAlias="glPaymentDetail"}  /* contain info of GL accounts hit by each payment of deposit */

/* Variable Definition */
define variable iarpmtID           as integer    no-undo.
define variable lDefaultAgent      as logical    no-undo.
define variable dtFromDate         as date       no-undo.
define variable dtToDate           as date       no-undo.
define variable dColumnWidth       as decimal    no-undo.
define variable ctranID            as character  no-undo.
define variable rwRow              as rowid      no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwarpmt

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES arpmt

/* Definitions for BROWSE brwarpmt                                      */
&Scoped-define FIELDS-IN-QUERY-brwarpmt getEntityType(arpmt.entity) @ arpmt.entity arpmt.entityID arpmt.entityName arpmt.checknum arpmt.transDate arpmt.checkamt arpmt.appliedAmt arpmt.remainingAmt abs(arpmt.refundAmt) @ arpmt.refundAmt arpmt.filenumber getVoid(arpmt.void) @ arpmt.void arpmt.depositref   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwarpmt   
&Scoped-define SELF-NAME brwarpmt
&Scoped-define QUERY-STRING-brwarpmt for each arpmt
&Scoped-define OPEN-QUERY-brwarpmt open query {&SELF-NAME} for each arpmt.
&Scoped-define TABLES-IN-QUERY-brwarpmt arpmt
&Scoped-define FIRST-TABLE-IN-QUERY-brwarpmt arpmt


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwarpmt}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btVoid bAgentLookup btClear btGo btSetPeriod ~
bPrelimRpt flAgentID fPostFrom fPostTo fSearch tbIncludeFullyApplied ~
tbIncludeVoid brwarpmt RECT-79 RECT-83 RECT-85 RECT-77 
&Scoped-Define DISPLAYED-OBJECTS flAgentID fPostFrom fPostTo fSearch ~
tbIncludeFullyApplied tbIncludeVoid flName fVoidDate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getVoid eC-Win 
FUNCTION getVoid RETURNS CHARACTER
  ( ipVoid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged eC-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validAgent eC-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR eC-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwarpmt 
       MENU-ITEM m_View_Transaction_Detail LABEL "Transaction Details"
       MENU-ITEM m_View_Payment_Detail LABEL "Edit Payment Note"
       MENU-ITEM m_Apply_Payment LABEL "Apply Payment" .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup  NO-FOCUS
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Agent lookup".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.67 TOOLTIP "Export data".

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.67 TOOLTIP "Modify payment".

DEFINE BUTTON bPrelimRpt  NO-FOCUS
     LABEL "Prelim" 
     SIZE 7.2 BY 1.71 TOOLTIP "Preliminary report".

DEFINE BUTTON btClear 
     IMAGE-UP FILE "images/s-cross.bmp":U NO-FOCUS
     LABEL "Clear" 
     SIZE 4.8 BY 1.14 TOOLTIP "Blank out the date range".

DEFINE BUTTON btGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.67 TOOLTIP "Get data".

DEFINE BUTTON btSetPeriod 
     IMAGE-UP FILE "images/s-calendar.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 4.8 BY 1.14 TOOLTIP "Set current open period as date range".

DEFINE BUTTON btVoid  NO-FOCUS
     LABEL "Void" 
     SIZE 7.2 BY 1.71 TOOLTIP "Void payment".

DEFINE BUTTON bView  NO-FOCUS
     LABEL "View" 
     SIZE 7.2 BY 1.67 TOOLTIP "Transaction detail".

DEFINE VARIABLE flAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 65.2 BY 1 NO-UNDO.

DEFINE VARIABLE fPostFrom AS DATE FORMAT "99/99/99":U 
     LABEL "Posted" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fPostTo AS DATE FORMAT "99/99/99":U 
     LABEL "To" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 TOOLTIP "Enter check# or deposit ref" NO-UNDO.

DEFINE VARIABLE fVoidDate AS DATE FORMAT "99/99/99":U 
     LABEL "Use Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-77
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 26.4 BY 3.05.

DEFINE RECTANGLE RECT-79
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 135.8 BY 3.05.

DEFINE RECTANGLE RECT-83
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE .6 BY 2.19.

DEFINE RECTANGLE RECT-85
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 42.2 BY 3.05.

DEFINE VARIABLE tbIncludeFullyApplied AS LOGICAL INITIAL no 
     LABEL "Fully Applied" 
     VIEW-AS TOGGLE-BOX
     SIZE 15 BY .81 NO-UNDO.

DEFINE VARIABLE tbIncludeVoid AS LOGICAL INITIAL no 
     LABEL "Voided" 
     VIEW-AS TOGGLE-BOX
     SIZE 10 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwarpmt FOR 
      arpmt SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwarpmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwarpmt eC-Win _FREEFORM
  QUERY brwarpmt DISPLAY
      getEntityType(arpmt.entity) @ arpmt.entity  label  "Agent/Org."       format "x(10)"  
arpmt.entityID                               label  "ID"                 format "x(12)"
arpmt.entityName                             label  "Name"               format "x(60)" width 40
arpmt.checknum                               label  "Check /Reference"   format "x(20)"
arpmt.transDate                         column-label  "Post!Date"        format "99/99/99  "
arpmt.checkamt                          column-label  "Check!Amount"     format ">,>>>,>>9.99"  width 17
arpmt.appliedAmt                        column-label  "Applied"          format "->,>>>,>>9.99"
arpmt.remainingAmt                      column-label  "Unapplied"        format "->,>>>,>>9.99" width 17
abs(arpmt.refundAmt) @ arpmt.refundAmt  column-label  "Refund!Amount"    format "->,>>>,>>9.99"
arpmt.filenumber                              label  "File"               format "x(30)" width 15
getVoid(arpmt.void) @ arpmt.void        column-label  "Voided"           format "x(8)"
arpmt.depositref                             label  "Deposit Ref"        format "x(60)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 203.8 BY 17.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     btVoid AT ROW 2.19 COL 190.8 WIDGET-ID 452 NO-TAB-STOP 
     bAgentLookup AT ROW 1.91 COL 30 WIDGET-ID 350 NO-TAB-STOP 
     bExport AT ROW 2.19 COL 141.2 WIDGET-ID 460 NO-TAB-STOP 
     bModify AT ROW 2.19 COL 148.2 WIDGET-ID 462 NO-TAB-STOP 
     btClear AT ROW 3.05 COL 49 WIDGET-ID 444 NO-TAB-STOP 
     btGo AT ROW 2.19 COL 101.8 WIDGET-ID 262 NO-TAB-STOP 
     btSetPeriod AT ROW 3.05 COL 53.6 WIDGET-ID 448 NO-TAB-STOP 
     bView AT ROW 2.19 COL 155.2 WIDGET-ID 464 NO-TAB-STOP 
     bPrelimRpt AT ROW 2.19 COL 197.8 WIDGET-ID 450 NO-TAB-STOP 
     flAgentID AT ROW 2 COL 13.8 COLON-ALIGNED WIDGET-ID 352
     fPostFrom AT ROW 3.1 COL 13.8 COLON-ALIGNED WIDGET-ID 428
     fPostTo AT ROW 3.1 COL 32.8 COLON-ALIGNED WIDGET-ID 430
     fSearch AT ROW 3.1 COL 68 COLON-ALIGNED WIDGET-ID 360
     tbIncludeFullyApplied AT ROW 2.24 COL 121.2 WIDGET-ID 378
     tbIncludeVoid AT ROW 3.05 COL 121.2 WIDGET-ID 396
     flName AT ROW 2 COL 32.8 COLON-ALIGNED NO-LABEL WIDGET-ID 424 NO-TAB-STOP 
     brwarpmt AT ROW 4.91 COL 3.2 WIDGET-ID 200
     fVoidDate AT ROW 2.52 COL 174.4 COLON-ALIGNED WIDGET-ID 454
     "Include:" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 2.38 COL 112.6 WIDGET-ID 400
     "Parameters" VIEW-AS TEXT
          SIZE 11.6 BY .62 AT ROW 1.24 COL 4.8 WIDGET-ID 266
     "Void" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1.24 COL 166.8 WIDGET-ID 458
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.24 COL 140.8 WIDGET-ID 468
     RECT-79 AT ROW 1.52 COL 3.2 WIDGET-ID 270
     RECT-83 AT ROW 1.91 COL 110.6 WIDGET-ID 406
     RECT-85 AT ROW 1.52 COL 164.8 WIDGET-ID 456
     RECT-77 AT ROW 1.52 COL 138.6 WIDGET-ID 466
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 225.4 BY 23.19 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW eC-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Posted Payments"
         HEIGHT             = 21.57
         WIDTH              = 207.6
         MAX-HEIGHT         = 34.43
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.43
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW eC-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwarpmt flName DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bExport IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bModify IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwarpmt:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwarpmt:HANDLE
       brwarpmt:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwarpmt:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR BUTTON bView IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flName IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flName:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN fVoidDate IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(eC-Win)
THEN eC-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwarpmt
/* Query rebuild information for BROWSE brwarpmt
     _START_FREEFORM
open query {&SELF-NAME} for each arpmt.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwarpmt */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME eC-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eC-Win eC-Win
ON END-ERROR OF eC-Win /* Posted Payments */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eC-Win eC-Win
ON WINDOW-CLOSE OF eC-Win /* Posted Payments */
DO:
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eC-Win eC-Win
ON WINDOW-RESIZED OF eC-Win /* Posted Payments */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup eC-Win
ON CHOOSE OF bAgentLookup IN FRAME DEFAULT-FRAME /* agentlookup */
DO:
  define variable cAgentID  as character no-undo.
  define variable cName     as character no-undo.
    
  run dialogagentlookup.w(input flAgentID:input-value,
                          input "",      /* Selected State ID */
                          input true,    /* Allow 'ALL' */
                          output cAgentID,
                          output std-ch, /* Agent state ID */
                          output cName,
                          output std-lo).
   
  if not std-lo 
   then
     return no-apply.
  
  empty temp-table arpmt. 
  close query brwarpmt.  
  
  run setWidgetState in this-procedure.
  
  assign
      flAgentID:screen-value = cAgentID
      flName:screen-value    = cName
      . 
  
  if lDefaultAgent               and
     flAgentID:input-value <> "" and
     flAgentID:input-value <> {&ALL}
   then
    /* Set default AgentID */
    publish "SetDefaultAgent" (input flAgentID:input-value).
  
  if flAgentID:input-value <> "" 
   then
    run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport eC-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify eC-Win
ON CHOOSE OF bModify IN FRAME DEFAULT-FRAME /* Modify */
DO:
  run modifyPayment in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrelimRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrelimRpt eC-Win
ON CHOOSE OF bPrelimRpt IN FRAME DEFAULT-FRAME /* Prelim */
do:
   run prelimRpt in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwarpmt
&Scoped-define SELF-NAME brwarpmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarpmt eC-Win
ON DEFAULT-ACTION OF brwarpmt IN FRAME DEFAULT-FRAME
DO:
  run viewPaymentDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarpmt eC-Win
ON ROW-DISPLAY OF brwarpmt IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarpmt eC-Win
ON START-SEARCH OF brwarpmt IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarpmt eC-Win
ON VALUE-CHANGED OF brwarpmt IN FRAME DEFAULT-FRAME
DO:  
  run setWidgetState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClear eC-Win
ON CHOOSE OF btClear IN FRAME DEFAULT-FRAME /* Clear */
DO:
 fPostFrom:screen-value = "".
 fPostTo:screen-value = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGo eC-Win
ON CHOOSE OF btGo IN FRAME DEFAULT-FRAME /* Go */
OR 'RETURN' of flAgentID
DO:
  if not validAgent()
   then
    return no-apply.
  
  run getData in this-procedure.    
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btSetPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btSetPeriod eC-Win
ON CHOOSE OF btSetPeriod IN FRAME DEFAULT-FRAME
DO:
  fPostFrom:screen-value = string(dtFromDate).
  fPostTo:screen-value   = string(dtToDate).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btVoid
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btVoid eC-Win
ON CHOOSE OF btVoid IN FRAME DEFAULT-FRAME /* Void */
do:
  run voidTransaction in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bView eC-Win
ON CHOOSE OF bView IN FRAME DEFAULT-FRAME /* View */
DO:  
  run viewPaymentDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID eC-Win
ON VALUE-CHANGED OF flAgentID IN FRAME DEFAULT-FRAME /* Agent ID */
DO:
  resultsChanged(false).
  
  assign
      flName:screen-value = ""
      bModify:sensitive   = false
      bView:sensitive     = false
      bExport:sensitive   = false
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch eC-Win
ON RETURN OF fSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch eC-Win
ON VALUE-CHANGED OF fSearch IN FRAME DEFAULT-FRAME /* Search */
DO:  
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Apply_Payment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Apply_Payment eC-Win
ON CHOOSE OF MENU-ITEM m_Apply_Payment /* Apply Payment */
DO:
  run openApply in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Payment_Detail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Payment_Detail eC-Win
ON CHOOSE OF MENU-ITEM m_View_Payment_Detail /* Edit Payment Note */
DO:
  run modifyPayment in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Transaction_Detail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Transaction_Detail eC-Win
ON CHOOSE OF MENU-ITEM m_View_Transaction_Detail /* Transaction Details */
DO:
  run viewPaymentDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tbIncludeFullyApplied
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tbIncludeFullyApplied eC-Win
ON VALUE-CHANGED OF tbIncludeFullyApplied IN FRAME DEFAULT-FRAME /* Fully Applied */
DO:
  resultsChanged(false).
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tbIncludeVoid
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tbIncludeVoid eC-Win
ON VALUE-CHANGED OF tbIncludeVoid IN FRAME DEFAULT-FRAME /* Voided */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK eC-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow" anywhere.

btGo        :load-image             ("images/completed.bmp").
btGo        :load-image-insensitive ("images/completed-i.bmp").

bExport     :load-image             ("images/excel.bmp").
bExport     :load-image-insensitive ("images/excel-i.bmp").

bModify     :load-image             ("images/update.bmp").
bModify     :load-image-insensitive ("images/update-i.bmp").

bView       :load-image             ("images/open.bmp").
bView       :load-image-insensitive ("images/open-i.bmp").

bAgentLookup:load-image             ("images/s-lookup.bmp").
bAgentLookup:load-image-insensitive ("images/s-lookup-i.bmp").

btVoid      :load-image            ("images/rejected.bmp").
btVoid      :load-image-insensitive("images/rejected-i.bmp").

bPrelimRpt  :load-image            ("images/pdf.bmp").
bPrelimRpt  :load-image-insensitive("images/pdf-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  {&window-name}:window-state = window-minimized.   
 
  run enable_UI.  
  
  {lib/get-column-width.i &col="'Name'" &var=dColumnWidth}  
    
  publish "GetAutoDefaultAgent" (output lDefaultAgent).
  
  if lDefaultAgent
   then
    do:
      /* Get default AgentID */
      publish "GetDefaultAgent"(output std-ch).
      
      flAgentID:screen-value = std-ch.
      
      publish "getAgentName" (input flAgentID:input-value,
                              output std-ch,
                              output std-lo).
                              
      flName:screen-value = std-ch.  
    end.
  
  /* Getting date range from first and last open active period */   
  publish "getOpenPeriod" (output dtFromDate,output dtToDate).    
  
  fPostFrom:screen-value = "".
  fPostTo:screen-value   = "".
  
    
  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.
  run setWidgetState in this-procedure.
  
  apply 'entry' to flAgentID.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow eC-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 publish "WindowClosed" (input this-procedure).   
 apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI eC-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(eC-Win)
  THEN DELETE WIDGET eC-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData eC-Win 
PROCEDURE displayData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  close query brwarpmt.
  empty temp-table arpmt.
  
  define buffer ttarpmt for ttarpmt.

  for each ttarpmt:
    create arpmt.
    buffer-copy ttarpmt to arpmt.    
  end.
  
  dataSortDesc = not dataSortDesc.
  if dataSortBy = ""
   then dataSortBy = "entityName".
  open query brwarpmt preselect each arpmt by arpmt.entityName
                                           by arpmt.depositref
                                           by arpmt.transDate
                                           by arpmt.checkamt.
  rwRow = rowid(arpmt).
  
  apply 'value-changed' to browse brwarpmt.
  run sortData in this-procedure (dataSortBy).
      
  setStatusCount(query brwarpmt:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI eC-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flAgentID fPostFrom fPostTo fSearch tbIncludeFullyApplied 
          tbIncludeVoid flName fVoidDate 
      WITH FRAME DEFAULT-FRAME IN WINDOW eC-Win.
  ENABLE btVoid bAgentLookup btClear btGo btSetPeriod bPrelimRpt flAgentID 
         fPostFrom fPostTo fSearch tbIncludeFullyApplied tbIncludeVoid brwarpmt 
         RECT-79 RECT-83 RECT-85 RECT-77 
      WITH FRAME DEFAULT-FRAME IN WINDOW eC-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW eC-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData eC-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwarpmt:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
  
  empty temp-table tarpmt.
  
  for each arpmt:
    create tarpmt.
    buffer-copy arpmt to tarpmt.
    assign 
        tarpmt.entity    = getEntityType(arpmt.entity)
        tarpmt.transType = getTranType(arpmt.transType)
        .
  end.
 
  std-ha = temp-table tarpmt:handle.
  run util/exporttable.p (table-handle std-ha,
                          "tarpmt",
                          "for each tarpmt",
                          "entity,entityID,entityName,arpmtID,depositID,revenuetype,checknum,receiptdate,checkdate,checkamt,appliedamt,remainingamt,fileExist,fileNumber,fileID,depositRef,createddate,createdby,username,description,selectrecord,void,voiddate,voidby,voidbyusername,posted,postdate,postby,postbyname,transType,ledgerID,voidLedgerID,archived,transDate,refundamt,notes,amtToRefund",
                          "Entity,Entity ID,Entity Name,Arpmt ID,Deposit ID,Revenue Type,Check Num,Receipt Date,Check Date,Check Amount,Applied Amount,Remaining Amount,File Exist,File Number,File ID,Deposit Ref,Created Date,Created By,Username,Description,Selectrecord,Void,Void Date,Void By,Voidby Username,Posted,Postdate,Post By,Postby Name,Trans Type,Ledger ID,VoidLedger ID,Archived,TransDate,Refund Amount,Notes,AmtToRefund",
                          std-ch,
                          "Payments-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
                            
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData eC-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define buffer ttarpmt for ttarpmt.
 
  close query brwarpmt.
  
  if fPostTo:input-value < fPostFrom:input-value 
   then
    do:
      message "Post To Date cannot be less than Post From Date."
          view-as alert-box.
      return.  
    end. 
    
  if fPostFrom:input-value > today    
   then
    do:
      message "Post From Date cannot be in future."
          view-as alert-box.
      return.
    end.
    
  if fPostTo:input-value > today    
   then
    do:
      message "Post To Date cannot be in future."
          view-as alert-box.
      return.
    end.
  
  if flAgentID:screen-value = "" or flAgentID:screen-value = "ALL" /* using date filter only when particular agentId is not entered*/
   then
    do:
     if (fPostFrom:input-value = ? or fPostTo:input-value = ?) and fSearch:input-value = ""
      then
       do:
        message "You must enter date range and/or search string when searching for all agents."
             view-as alert-box.
         return.
       end.
    end.  
  
  {lib/brw-totalData.i &noShow=true}
  
  run server\getpayments.p (input yes,       /* posted payments */
                            input {&Agent},
                            input flAgentID:screen-value,
                            input tbIncludeFullyApplied:checked,
                            input tbIncludeVoid:checked,
                            input false, /* archived */
                            input fPostFrom:input-value,
                            input fPostTo:input-value, 
                            input fSearch:input-value,
                            input 0,
                            output table ttarpmt,
                            output std-lo,
                            output std-ch).
             
  if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box info buttons ok.
      return.
    end.
    
  run displayData in this-procedure.
  
  setStatusRecords(query brwarpmt:num-results). 
  reposition brwarpmt to rowid rwRow no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyPayment eC-Win 
PROCEDURE modifyPayment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define buffer tarpmt for tarpmt.
  empty temp-table tarpmt.

  if not available arpmt 
   then
    return.
  
  create tarpmt.
  buffer-copy arpmt to tarpmt.
  
  iArPmtID = arpmt.arpmtID.   
  
  run dialogmodifypayment.w (input {&ModifyPosted},  /* Only notes modify */
                             input-output table tarpmt,                                                        
                             output std-lo).

  if not std-lo 
   then
    return.
    
  find first tarpmt no-error.
  if (not available tarpmt)
   then
    return.    
    
  find first ttarpmt where ttarpmt.arpmtID = iArPmtID no-error.
  if available ttarpmt
   then
    buffer-copy tarpmt to ttarpmt no-error.

  run displayData in this-procedure.  
   
  find first arpmt where arpmt.arPmtID = ttarpmt.arPmtID no-error.
  
  if not available arpmt
   then
    return.
  
  reposition {&browse-name} to rowid rowid(arpmt).
  
  apply 'value-changed' to browse brwarpmt.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openApply eC-Win 
PROCEDURE openApply :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available arpmt
   then return.
   
  /* Show records based on the parameter list */
  publish "SetCurrentValue" ("ApplyParams", arpmt.entityID + "|" + {&Payment} + "|" + string(arpmt.artranID)).
  
  publish "OpenWindow" (input "wapply", 
                        input {&Payment} + "|" + string(arpmt.artranID), 
                        input "wapply.w", 
                        input ?,                                   
                        input this-procedure).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE prelimRpt eC-Win 
PROCEDURE prelimRpt :
run server/prelimpaymentvoidgl.p(input integer(arpmt.arpmtID),
                                  output table ledgerreport,
                                  output std-lo,
                                  output std-ch).
 if not std-lo 
  then
   do:
     message std-ch
        view-as alert-box error buttons ok.
     return.
   end.
   
 run util\arpaymentvoidpdf.p (input {&view},
                              input table ledgerreport,
                              output std-ch).
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refundpayment eC-Win 
PROCEDURE refundpayment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  run dialogRefundPayment.w (input arpmt.arpmtID,                             
                             input arpmt.entityname + " (" + arpmt.entityID + ")",
                             input arpmt.depositref,
                             input arpmt.checknum,
                             input arpmt.remainingAmt,
                             input date(arpmt.postdate),
                             output std-de,             /* refund amount*/
                             output std-lo) .
  
  if not std-lo
   then
    return.
    
  if std-lo
   then
    do:
      find first ttarpmt where ttarpmt.arpmtID = arpmt.arpmtID no-error.
      if available ttarpmt
       then
        assign
          ttarpmt.remainingAmt = ttarpmt.remainingAmt - std-de
          ttarpmt.refundAmt    = ttarpmt.refundAmt - std-de
          .
    end. 
    
  run displayData in this-procedure.
  
  apply 'value-changed' to browse brwarpmt.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState eC-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  if not available arpmt
   then
    assign
        bModify:sensitive    = available arpmt
        bView:sensitive      = available arpmt
        bExport:sensitive    = if (query brwarpmt:num-results = ?) then false else (query brwarpmt:num-results > 0)
        bPrelimRpt:sensitive = false 
        btVoid:sensitive     = false
        fVoidDate:sensitive  = false
        .                                
   else
    assign
        bModify:sensitive    = available arpmt
        bView:sensitive      = available arpmt
        bExport:sensitive    = if (query brwarpmt:num-results = ?) then false else (query brwarpmt:num-results > 0)
        bPrelimRpt:sensitive = not arpmt.void 
        btVoid:sensitive     = not arpmt.void
        fVoidDate:sensitive  = not arpmt.void
        .
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow eC-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData eC-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by arpmt.entityName ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
  {lib/brw-totalData.i &excludeColumn="2,3,4,5,10,11,12"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewPaymentDetail eC-Win 
PROCEDURE viewPaymentDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if available arPmt
    then
     publish "OpenWindow" (input "wtransactiondetailPayment",      /*childtype*/
                           input string(arPmt.arPmtID),  /*childid*/
                           input "wtransactiondetail.w",      /*window*/
                           input "integer|input|0^integer|input|" + string(arPmt.arPmtID) + "^character|input|" + {&Payment},  /*parameters*/                               
                           input this-procedure).       /*currentProcedure handle*/ 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE voidTransaction eC-Win 
PROCEDURE voidTransaction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable lViewPdf   as logical   no-undo.
 
  if not available(arpmt) 
   then
    return.
    
  ctranID = arpmt.arTranID.
    
  do with frame {&frame-name}:
  end.
  
  if fVoidDate:input-value = ?
   then
    do:
      message "Void Date cannot be blank."
        view-as alert-box error buttons ok.
      return.      
    end.
  
  if fVoidDate:input-value > today
   then
    do:
      message "Void date cannot be in future."
          view-as alert-box error buttons ok.    
      return.
    end.
  
  publish "validatePostingDate" (input fVoidDate:input-value,
                                 output std-lo).
  if not std-lo
   then
    do:
      message "Void date must lie in open active period."
          view-as alert-box error buttons ok.     
      return.
    end.
    
  
  if fVoidDate:input-value < date(arpmt.transDate)
   then
    do:
       message  "Void date can not be prior to posting date."
        view-as alert-box error buttons ok.
       return.
   end.
 
  run dialogviewvoidledger.w (input "Void Transaction",output std-lo).
 
  if not std-lo
   then
    return.
    
  publish "GetViewPdf" (output lViewPdf). 
  
  run server\voidtransaction.p(input ctranID,
                               input 0,
                               input fVoidDate:input-value,
                               input lViewPdf,
                               output table ledgerreport,
                               output table glPaymentDetail, /* needed for deposit void */
                               output table ttArTran,
                               output std-lo,
                               output std-ch).
                  
  if not std-lo 
  then
   do:
     message std-ch
         view-as alert-box error buttons ok.
     return.
   end.
  else
   message "Void was successful."
       view-as alert-box information buttons ok.
  
  /*Viewing Pdf*/
  if lViewPdf
   then
    run util\arpaymentvoidpdf.p (input {&view},
                                 input table ledgerreport,
                                 output std-ch). 
    
  find first arpmt where arpmt.artranID = ctranID no-error.
  if available arpmt
   then
    do:
        if tbIncludeVoid:checked in frame {&frame-name}
         then
          assign
           arpmt.void = true.
         else
          delete arpmt.
    end.
 
  open query brwarpmt preselect each arpmt by arpmt.entityName.
  
  run sortData in this-procedure(dataSortBy).
  
  run setWidgetState in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized eC-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 14
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 23
      .
 
  {lib/resize-column.i &col="'Name'" &var=dColumnWidth}
  
  run ShowScrollBars(frame {&frame-name}:handle, no, no).
  {lib/brw-totalData.i &excludeColumn="2,3,4,5,10,11,12"}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getVoid eC-Win 
FUNCTION getVoid RETURNS CHARACTER
  ( ipVoid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if arpmt.void 
   then 
    return "Yes".
  else
   return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged eC-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validAgent eC-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
/*   if flAgentID:input-value = ""                    */
/*    then return false. /* Function return value. */ */
  
/*   else */
  if flAgentID:input-value = {&ALL}
   then
    flName:screen-value = {&NotApplicable}.
       
  else if flAgentID:input-value <> {&ALL} and
      flAgentID:input-value <> ""
   then
    do:
      publish "getAgentName" (input flAgentID:input-value,
                              output std-ch,
                              output std-lo).                                               
      if not std-lo 
       then 
        do:
          assign 
              flAgentID:screen-value = "" 
              flName:screen-value    = ""
              .
          return false. /* Function return value. */
        end.
      flName:screen-value = std-ch.
    end. 
  
  resultsChanged(false).  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

