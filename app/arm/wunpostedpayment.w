&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wunpostedpayment.w

  Description: Window for AR Payments

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Vikas jain

  Created: 07.11.2019
  Modifications:
  Date        Name    Description
  07/24/2020  AG      Changes to input parameters in getPayments.p
  07/30/2020  AC      Added new button "Preliminary".
  01/15/2021  Shefali Added new pop-up menu "Edit Payment".
  04/05/2022  SachinC Task# 92523 Sending field values for new payment.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/ar-def.i}
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/winshowscrollbars.i}
{lib/ar-getentitytype.i} /* Include function: getEntityType */
{lib/ar-gettrantype.i}   /* Include function: getTranType */
{lib/normalizefileid.i}  /* include file to normalize file number */

/* Temp-table Definition */
{tt/arpmt.i}                           /* contains filtered data shown in browse*/
{tt/arpmt.i  &tableAlias="ttarpmt"}    /* contains all data received from server */
{tt/arpmt.i  &tableAlias="tarpmt"}     /* used for passing data to server and to export data */
{tt/proerr.i &tableAlias="ttarpmtErr"} /* Used to get validation messages from server */
{tt/ledgerreport.i &tableAlias="glpayment"}

/* Variable Definition */
define variable iarpmtID           as integer    no-undo.
define variable haPmtRecord        as handle     no-undo.
define variable cRefNumber         as character  no-undo.
define variable deCheckAmt         as decimal    no-undo.
define variable deDepositCheckAmt  as decimal    no-undo.
define variable cDepositID         as character  no-undo.
define variable cCurrentUser       as character  no-undo.
define variable lRefresh           as logical    no-undo.
define variable lApplySearchString as logical    no-undo.
define variable cLastSearchString  as character  no-undo.
define variable cLastEntity        as character  no-undo.
define variable cLastDepositRef    as character  no-undo.
define variable iErrSeq            as integer    no-undo.
define variable lSelect            as logical    no-undo.
define variable cDepositRef        as character  no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwarpmt

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES arpmt

/* Definitions for BROWSE brwarpmt                                      */
&Scoped-define FIELDS-IN-QUERY-brwarpmt getEntityType(arpmt.entity) @ arpmt.entity arpmt.entityID arpmt.entityName arpmt.checknum arpmt.checkdate arpmt.receiptdate arpmt.checkamt arpmt.filenumber arpmt.fileExist arpmt.depositRef arpmt.selectrecord   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwarpmt arpmt.selectrecord   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwarpmt arpmt
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwarpmt arpmt
&Scoped-define SELF-NAME brwarpmt
&Scoped-define QUERY-STRING-brwarpmt for each arpmt
&Scoped-define OPEN-QUERY-brwarpmt open query {&SELF-NAME} for each arpmt.
&Scoped-define TABLES-IN-QUERY-brwarpmt arpmt
&Scoped-define FIRST-TABLE-IN-QUERY-brwarpmt arpmt


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwarpmt}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btFilter cbAgent cbDeposit fSearch bExport ~
flPostDate brwarpmt bNew bRefresh RECT-77 RECT-79 RECT-90 
&Scoped-Define DISPLAYED-OBJECTS cbAgent cbDeposit fSearch rsPost ~
flPostDate flCheckAmt flTotal 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFormattedNumber C-Win 
FUNCTION getFormattedNumber RETURNS CHARACTER
  ( input deTotal as decimal,
    input hWidget as handle )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwarpmt 
       MENU-ITEM m_Edit_Payment LABEL "Edit Payment"  .

/* Definitions of the field level widgets                               */
DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.67 TOOLTIP "Delete payment".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.67 TOOLTIP "Export data".

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.67 TOOLTIP "Modify payment".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.67 TOOLTIP "New payment".

DEFINE BUTTON bPostPayment  NO-FOCUS
     LABEL "Postpayment" 
     SIZE 7.2 BY 1.67 TOOLTIP "Post selected payment(s)".

DEFINE BUTTON bPrelimRpt  NO-FOCUS
     LABEL "Preliminary" 
     SIZE 7.2 BY 1.67 TOOLTIP "Preliminary Report".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.67 TOOLTIP "Reload data".

DEFINE BUTTON btFilter  NO-FOCUS
     LABEL "Filter" 
     SIZE 7.2 BY 1.67 TOOLTIP "Reset filters".

DEFINE VARIABLE cbAgent AS CHARACTER FORMAT "X(256)" 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE cbDeposit AS CHARACTER FORMAT "X(256)" 
     LABEL "Deposit Ref." 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 24.4 BY 1 NO-UNDO.

DEFINE VARIABLE flCheckAmt AS DECIMAL FORMAT "(Z,ZZZ,ZZZ)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 19.2 BY 1 NO-UNDO.

DEFINE VARIABLE flPostDate AS DATE FORMAT "99/99/99":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 12.4 BY 1 NO-UNDO.

DEFINE VARIABLE flTotal AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 7.8 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 60 BY 1 TOOLTIP "Search Criteria (ID,Name,Check/Reference,File Number,File Exists,Deposit Ref.)" NO-UNDO.

DEFINE VARIABLE rsPost AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Use Date:", "P",
"Use Receipt Date", "T"
     SIZE 20.2 BY 2.14 NO-UNDO.

DEFINE RECTANGLE RECT-77
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 40 BY 4.05.

DEFINE RECTANGLE RECT-79
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 91 BY 4.05.

DEFINE RECTANGLE RECT-90
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 45.4 BY 4.05.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwarpmt FOR 
      arpmt SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwarpmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwarpmt C-Win _FREEFORM
  QUERY brwarpmt DISPLAY
      getEntityType(arpmt.entity) @ arpmt.entity         label  "Agent/Org." format "x(10)"  
arpmt.entityID        label  "ID"                        format "x(12)"
arpmt.entityName      label  "Name"                      format "x(48)"
arpmt.checknum        label  "Check/Reference"           format "x(20)"
arpmt.checkdate       column-label  "Check"              format "99/99/99"
arpmt.receiptdate     label  "Receipt"                   format "99/99/99"
arpmt.checkamt        column-label  "Amount"             format ">>>,>>>,>>9.99" width 20
arpmt.filenumber      label  "File Number"               format "x(30)" width 18
arpmt.fileExist       column-label  "File!Exists"        format "x(8)"
arpmt.depositRef      label  "Deposit Ref."              format "x(20)"
arpmt.selectrecord    column-label "Select to!Post"      view-as toggle-box
enable arpmt.selectrecord
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 200.4 BY 17.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bPrelimRpt AT ROW 2.57 COL 168.6 WIDGET-ID 412
     btFilter AT ROW 2.62 COL 82.6 WIDGET-ID 262
     cbAgent AT ROW 1.76 COL 16.8 COLON-ALIGNED WIDGET-ID 246
     bDelete AT ROW 2.62 COL 123.6 WIDGET-ID 4 NO-TAB-STOP 
     cbDeposit AT ROW 2.91 COL 16.8 COLON-ALIGNED WIDGET-ID 406
     fSearch AT ROW 4.05 COL 16.8 COLON-ALIGNED WIDGET-ID 310
     rsPost AT ROW 2.43 COL 134.2 NO-LABEL WIDGET-ID 390
     bExport AT ROW 2.62 COL 102.6 WIDGET-ID 8 NO-TAB-STOP 
     flPostDate AT ROW 2.52 COL 145.4 COLON-ALIGNED NO-LABEL WIDGET-ID 408
     brwarpmt AT ROW 5.81 COL 2.6 WIDGET-ID 200
     bModify AT ROW 2.62 COL 116.6 WIDGET-ID 6 NO-TAB-STOP 
     bNew AT ROW 2.62 COL 109.6 WIDGET-ID 2 NO-TAB-STOP 
     bPostPayment AT ROW 2.57 COL 161.6 WIDGET-ID 196
     bRefresh AT ROW 2.62 COL 95.6 WIDGET-ID 402 NO-TAB-STOP 
     flCheckAmt AT ROW 23.43 COL 118.8 COLON-ALIGNED NO-LABEL WIDGET-ID 304 NO-TAB-STOP 
     flTotal AT ROW 23.43 COL 1 COLON-ALIGNED NO-LABEL WIDGET-ID 314 NO-TAB-STOP 
     "Filters" VIEW-AS TEXT
          SIZE 5.4 BY .62 AT ROW 1.19 COL 4 WIDGET-ID 266
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.14 COL 94.6 WIDGET-ID 194
     "Post" VIEW-AS TEXT
          SIZE 4.8 BY .62 AT ROW 1.19 COL 133.6 WIDGET-ID 416
     RECT-77 AT ROW 1.43 COL 93.2 WIDGET-ID 190
     RECT-79 AT ROW 1.43 COL 2.6 WIDGET-ID 270
     RECT-90 AT ROW 1.43 COL 132.8 WIDGET-ID 414
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 204 BY 23.76
         DEFAULT-BUTTON bDelete WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Unposted Payments"
         HEIGHT             = 23.48
         WIDTH              = 203.6
         MAX-HEIGHT         = 34.43
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.43
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwarpmt flPostDate DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bDelete IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bModify IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPostPayment IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPrelimRpt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwarpmt:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwarpmt:HANDLE
       brwarpmt:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwarpmt:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR FILL-IN flCheckAmt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flTotal IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flTotal:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR RADIO-SET rsPost IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwarpmt
/* Query rebuild information for BROWSE brwarpmt
     _START_FREEFORM
open query {&SELF-NAME} for each arpmt.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwarpmt */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Unposted Payments */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Unposted Payments */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Unposted Payments */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME DEFAULT-FRAME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL DEFAULT-FRAME C-Win
ON GO OF FRAME DEFAULT-FRAME
DO:
  run prelimReport in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME DEFAULT-FRAME /* Delete */
DO:
  run deletePayment in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME DEFAULT-FRAME /* Modify */
DO:
  run modifyPayment in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME DEFAULT-FRAME /* New */
DO:
  run newPayment in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPostPayment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPostPayment C-Win
ON CHOOSE OF bPostPayment IN FRAME DEFAULT-FRAME /* Postpayment */
DO:    
  run postPayment in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrelimRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrelimRpt C-Win
ON CHOOSE OF bPrelimRpt IN FRAME DEFAULT-FRAME /* Preliminary */
DO:
  run prelimReport in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME DEFAULT-FRAME /* Refresh */
DO:
  lRefresh = true.
  run refreshData in this-procedure.
  assign
      fSearch:screen-value   = cLastSearchString
      cbAgent:screen-value   = cLastEntity
      cbDeposit:screen-value = cLastDepositRef
      lRefresh               = false
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwarpmt
&Scoped-define SELF-NAME brwarpmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarpmt C-Win
ON DEFAULT-ACTION OF brwarpmt IN FRAME DEFAULT-FRAME
DO:
  run modifyPayment in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarpmt C-Win
ON ROW-DISPLAY OF brwarpmt IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarpmt C-Win
ON START-SEARCH OF brwarpmt IN FRAME DEFAULT-FRAME
do:
  std-ha = brwarpmt:current-column.
  if std-ha:label = "Select to!Post"
   then
    do:      
      for each arpmt:
        if (not arpmt.posted)
         then
          assign
              arpmt.selectrecord = if arpmt.depositRef = "" then not lSelect else false    
              arpmt.selectrecord:checked in browse brwarpmt = arpmt.selectrecord
              .
        /* Retaining selected record */  
        for first ttarpmt where ttarpmt.arpmtID = arpmt.arpmtID:
          ttarpmt.selectrecord = arpmt.selectrecord.
        end.  
      end.    
      browse brwArPmt:refresh(). 
      run enabledisablepost in this-procedure.
      lSelect = not lSelect.
    end.
  else
    do:
      {lib/brw-startsearch.i}
    end.  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwarpmt C-Win
ON VALUE-CHANGED OF brwarpmt IN FRAME DEFAULT-FRAME
DO:  
  run setWidgetState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btFilter C-Win
ON CHOOSE OF btFilter IN FRAME DEFAULT-FRAME /* Filter */
do:    
  assign
      cbAgent:screen-value       = {&all}        
      cbDeposit:screen-value     = {&all}        
      fSearch:screen-value       = ""
      cLastSearchString          = ""
      cLastEntity                = cbAgent:screen-value
      cLastDepositRef            = cbDeposit:screen-value
      .
      
  run resetFilter in this-procedure.   
  run filterData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgent C-Win
ON VALUE-CHANGED OF cbAgent IN FRAME DEFAULT-FRAME /* Agent */
DO:
  run filterdata in this-procedure.
  cLastEntity = cbAgent:input-value.
  run setFilterButton in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbDeposit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbDeposit C-Win
ON VALUE-CHANGED OF cbDeposit IN FRAME DEFAULT-FRAME /* Deposit Ref. */
DO:
  run filterdata in this-procedure.
  cLastDepositRef = cbDeposit:input-value.
  run setFilterButton in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME DEFAULT-FRAME /* Search */
or 'leave' of fSearch
DO:
  /* if search button is clicked or return key is hit, 
     then 'cLastSearchString' stores the last string searched until user again hits the search */
  assign
      lApplySearchString = true  
      cLastSearchString  = fSearch:input-value
      .
  run filterData in this-procedure.
  
  /* Enable reset filter button when filter applies */
  run setFilterButton in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON VALUE-CHANGED OF fSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  resultsChanged().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Edit_Payment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Edit_Payment C-Win
ON CHOOSE OF MENU-ITEM m_Edit_Payment /* Edit Payment */
DO:
  run modifyPayment in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow" anywhere.

bExport     :load-image            ("images/excel.bmp").
bExport     :load-image-insensitive("images/excel-i.bmp").

bRefresh    :load-image            ("images/sync.bmp").
bRefresh    :load-image-insensitive("images/sync-i.bmp").

bNew        :load-image            ("images/add.bmp").
bNew        :load-image-insensitive("images/add-i.bmp").

bModify     :load-image            ("images/update.bmp").
bModify     :load-image-insensitive("images/update-i.bmp").

bDelete     :load-image            ("images/delete.bmp").
bDelete     :load-image-insensitive("images/delete-i.bmp"). 

btFilter    :load-image            ("images/filtererase.bmp").
btFilter    :load-image-insensitive("images/filtererase-i.bmp").

bPostPayment:load-image            ("images/check.bmp").
bPostPayment:load-image-insensitive("images/check-i.bmp").

bPrelimRpt:load-image             ("images/pdf.bmp").
bPrelimRpt:load-image-insensitive ("images/pdf-i.bmp").

on entry of arpmt.selectrecord in browse brwarpmt
do:
  haPmtRecord = browse brwarpmt:query:get-buffer-handle(1) no-error.
end.

on value-changed of arpmt.selectrecord in browse brwarpmt
do:
  std-in = haPmtRecord:buffer-field("arpmtid"):buffer-value() no-error.      
  for each arpmt where arpmt.arpmtID = std-in:

    assign
        arpmt.selectrecord = if arpmt.depositRef = "" then not arpmt.selectrecord else false    
        lSelect            = if arpmt.depositRef = "" then not arpmt.selectrecord else lSelect
        arpmt.selectrecord:checked in browse brwarpmt = arpmt.selectrecord                                            
        .
    /* Retaining selected record */  
    for first ttarpmt where ttarpmt.arpmtID = arpmt.arpmtID:
      ttarpmt.selectrecord = arpmt.selectrecord.
    end.
  end. 
  browse brwArPmt:refresh().   
  run enableDisablePost in this-procedure.  
end.        

/* Return curent logged-in user name */
publish "GetCredentialsName" (output cCurrentUser).

/* Get all unposted payments from server */
run refreshData in this-procedure.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  {&window-name}:window-state = window-minimized. 
   
  run enable_UI.       
  
  flTotal:screen-value = "Totals".
   
  publish 'GetPostingConfig' (output std-ch).
  rsPost:screen-value = std-ch.
  
  /* When default posting option from config screen is allowed */ 
  publish 'GetDefaultPostingOption' (output std-lo).
  if std-lo
   then
    do:
      publish 'getDefaultPostingDate' (output std-da).
      flPostDate:screen-value = string(std-da).  
    end.
  
  if query brwarpmt:num-results = 0 then
   do:
     run setFilter in this-procedure.
     run setWidgetState in this-procedure.
   end.
   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adjustTotals C-Win 
PROCEDURE adjustTotals :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Setting width of browse columns when window resizes */
  assign
      brwarpmt:get-browse-column(3):width-pixels = (22 / 100) *  brwarpmt:width-pixels /* Name col */
      brwarpmt:get-browse-column(4):width-pixels = (13 / 100) *  brwarpmt:width-pixels /* check num */
      no-error.
  
  /* Setting position of totals widget */
  assign       
      flTotal:y    = brwarpmt:y + brwarpmt:height-pixels + 0.01
      flCheckAmt:y = brwarpmt:y + brwarpmt:height-pixels + 0.01          
      flTotal:x    = brwarpmt:x + brwarpmt:get-browse-column(1):x
      flCheckAmt:x = brwarpmt:x + brwarpmt:get-browse-column(7):x + (brwarpmt:get-browse-column(7):width-pixels - flCheckAmt:width-pixels)  + 6.8    
      no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE createErrorRcrd C-Win 
PROCEDURE createErrorRcrd :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pcErrorMsg as character no-undo. 

  create ttarpmtErr.
  assign 
      iErrSeq                = iErrSeq + 1
      ttarpmtErr.err         = string(iErrSeq)
      ttarpmtErr.description = pcErrorMsg
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deletePayment C-Win 
PROCEDURE deletePayment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define variable iDepositID  as integer   no-undo.
  define variable dePmtAmount as decimal   no-undo.
  
  define buffer ttarpmt for ttarpmt.
  
  if not available arpmt 
   then
    return.

  message "Payment will be deleted. Are you sure you want to delete the selected payment?"
      view-as alert-box question buttons yes-no
      title "Delete Payment"
      update std-lo.
  
  if not std-lo 
   then 
    return.

  /* client Server Call */
  run server/deletepayment.p(input  arpmt.arpmtID,
                             output std-lo,
                             output std-ch).  

  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.

  find first ttarpmt where ttarpmt.arpmtID = arpmt.arPmtID no-error.
  if not available ttarpmt
   then
    return.
  
  assign
    std-ch      = ttarpmt.entityID
    iDepositID  = ttarpmt.depositID
    dePmtAmount = ttarpmt.checkAmt
    .
    
  delete ttarpmt.  
      
  if not can-find(first ttarpmt where ttarpmt.entityID = std-ch)
   then
    cbAgent:delete(std-ch) no-error.
    
  if not can-find(first ttarpmt where ttarpmt.depositID = iDepositID)
   then
    cbDeposit:delete(string(iDepositID)).       
      
  run filterData  in this-procedure.      

  /* Refresh the deposit screen when payment is deleted from the exisitng deposit */
  if cDepositID <> "" and iDepositID = integer(cDepositID) 
   then
    do:
      deDepositCheckAmt = deDepositCheckAmt - dePmtAmount.
      publish "refreshCheckAmount" (input iDepositID, input deDepositCheckAmt).
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData C-Win 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  dataSortDesc = not dataSortDesc.
  if dataSortBy = ""
   then 
    dataSortBy = "entityName".
    
  run sortData in this-procedure (dataSortBy).
         
  flCheckAmt:format = getFormattedNumber(input deCheckAmt, input flCheckAmt:handle) no-error.
  
  flCheckAmt:screen-value = string(deCheckAmt).
  
  run adjustTotals in this-procedure.
  
  run enabledisablepost in this-procedure.
    
  apply 'value-changed' to browse brwarpmt.
  
  setStatusCount(query brwarpmt:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enabledisablepost C-Win 
PROCEDURE enabledisablepost :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
   
  if can-find(first ttarpmt where ttarpmt.selectrecord)
   then
    assign
        flPostDate:sensitive   = true
        rsPost:sensitive       = true
        bpostpayment:sensitive = true
        bPrelimRpt:sensitive   = true 
        .
  else
   assign
       flPostDate:sensitive   = false
       rsPost:sensitive       = false
       bpostpayment:sensitive = false
       bPrelimRpt:sensitive   = false 
       .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbAgent cbDeposit fSearch rsPost flPostDate flCheckAmt flTotal 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE btFilter cbAgent cbDeposit fSearch bExport flPostDate brwarpmt bNew 
         bRefresh RECT-77 RECT-79 RECT-90 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwarpmt:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
  
  empty temp-table tarpmt.
  for each arpmt:
    create tarpmt.
    buffer-copy arpmt to tarpmt.
    tarpmt.entity    = getEntityType(arpmt.entity).
    tarpmt.transType = getTranType(arpmt.transType).
  end.
  std-ha = temp-table tarpmt:handle.
  run util/exporttable.p (table-handle std-ha,
                          "tarpmt",
                          "for each tarpmt",
                          "entity,entityID,entityName,arpmtID,depositID,revenuetype,checknum,receiptdate,checkdate,checkamt,appliedamt,remainingamt,fileExist,fileNumber,fileID,depositRef,createddate,createdby,username,description,transType",
                          "Entity,Entity ID,Entity Name,Arpmt ID,Deposit ID,Revenue Type,Check Num,Receipt Date,Check Date,Check Amount,Applied Amount,Remaining Amount,File Exist,File Number,File ID,Deposit Ref,Created Date,Created By,Username,Description,Trans Type",
                          std-ch,
                          "Unpostedpayments-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).                                                    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportErrorData C-Win 
PROCEDURE exportErrorData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cErrFile as character no-undo.
  
  if can-find(first ttarpmtErr)
   then
    do:
      publish "GetReportDir" (output std-ch).
      cErrFile = "PaymentPostingErrors_" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv".
      std-ha = temp-table ttarpmtErr:handle.
      run util/exporttable.p (table-handle std-ha,
                              "ttarpmtErr",
                              "for each ttarpmtErr",
                              "err,description",
                              "Sequence,Error",
                              std-ch,
                              cErrFile,
                              true,
                              output std-ch,
                              output std-in).
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwarpmt.
  empty temp-table arpmt.
  
  do with frame {&frame-name}:
  end.

  define buffer ttarpmt for ttarpmt.  

  assign      
      deCheckAmt              = 0
      flCheckAmt:screen-value = ""      
      .
  
  for each ttarpmt  
    where ttarpmt.entityID = (if cbAgent:input-value = {&all} then ttarpmt.entityID else cbAgent:input-value)
      and ttarpmt.depositID = (if cbDeposit:input-value = {&all} then ttarpmt.depositID else integer(cbDeposit:input-value))
      and ((if trim(cLastSearchString) <> "" then ttarpmt.entity matches ("*" + trim(cLastSearchString) + "*")
                                             else ttarpmt.entity = ttarpmt.entity) or
           (if trim(cLastSearchString) <> "" then ttarpmt.entityID matches ("*" + trim(cLastSearchString) + "*")
                                             else ttarpmt.entityID = ttarpmt.entityID) or
           (if trim(cLastSearchString) <> "" then ttarpmt.entityName matches ("*" + trim(cLastSearchString) + "*")
                                             else ttarpmt.entityName = ttarpmt.entityName) or
           (if trim(cLastSearchString) <> "" then ttarpmt.checknum matches ("*" + trim(cLastSearchString) + "*")
                                             else ttarpmt.checknum = ttarpmt.checknum) or
           (if trim(cLastSearchString) <> "" then ttarpmt.filenumber matches ("*" + trim(cLastSearchString) + "*")
                                             else ttarpmt.filenumber = ttarpmt.filenumber)or
           (if trim(cLastSearchString) <> "" then ttarpmt.fileExist matches ("*" + trim(cLastSearchString) + "*")
                                             else ttarpmt.fileExist = ttarpmt.fileExist)or
           (if trim(cLastSearchString) <> "" then ttarpmt.depositRef matches ("*" + trim(cLastSearchString) + "*")
                                             else ttarpmt.depositRef = ttarpmt.depositRef)
          )       
      :
    create arpmt.
    buffer-copy ttarpmt to arpmt.
    deCheckAmt = deCheckAmt + ttarpmt.checkAmt.
  end.
  
  run displayData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define buffer ttarpmt for ttarpmt.
  
  /* Show records based on the selected deposit ref */
  publish "GetCurrentValue" ("DepositID", output cDepositID).
  publish "GetCurrentValue" ("DepositRef", output cDepositRef).
  
  run server\getpayments.p (input no,            /* Unposted Payments */
                            input "",            /* entity type */
                            input "",            /* entity ID */
                            input false,         /* include fully applied*/
                            input false,         /* include void */
                            input false,         /* include archived */
                            input ?,             /* post: from date */
                            input ?,             /* post: to date */
                            input "",            /* check# or depositRef*/
                            input cDepositID,
                            output table tarpmt,
                            output std-lo,
                            output std-ch).
 
  if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box info buttons ok.
      return.
    end.

  empty temp-table ttarpmt.
  for each tarpmt:
    create ttarpmt.
    buffer-copy tarpmt to ttarpmt.
  end.
  
  cbAgent:list-item-pairs = {&ALL} + "," + {&ALL}.
  /* Set the filters based on the data returned, with ALL as the first option */
  for each ttarpmt
    break by ttarpmt.entityID:
    if first-of(ttarpmt.entityID) and ttarpmt.entityID <> ""
     then
      cbAgent:add-last(replace(ttarpmt.entityname,",",""),ttarpmt.entityID).
  end.

  cbDeposit:list-item-pairs = {&ALL} + "," + {&ALL}.

  /* Set the filters based on the data returned, with ALL as the first option */
  for each ttarpmt
    break by ttarpmt.depositID:
    if first-of(ttarpmt.depositID) and (ttarpmt.depositID <> -1 and ttarpmt.depositID <> ?)
     then
        cbDeposit:add-last(replace(ttarpmt.depositRef,",",""),string(ttarpmt.depositID)).
  end.
  
  /* Case when despositID is there but no armpt corresponding to it, to display default depositRef in COMBO-BOX */
  if (cDepositID <> "") and  not can-find(first ttarpmt)
   then
    cbDeposit:list-item-pairs = cbDeposit:list-item-pairs + "," + cDepositRef + "," + cDepositID.
       
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyPayment C-Win 
PROCEDURE modifyPayment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable iDepositID    as integer   no-undo.
  define variable deOldPmtAmt   as decimal   no-undo.
  
  define buffer tarpmt for tarpmt.
  empty temp-table tarpmt.

  if not available arpmt 
   then
    return.
  
  create tarpmt.
  buffer-copy arpmt to tarpmt.
  
  assign
    iArPmtID    = arpmt.arpmtID   
    iDepositID  = arpmt.depositID
    deOldPmtAmt = arpmt.checkAmt
    .
  
  run dialogmodifypayment.w (input {&Modify},
                             input-output table tarpmt,                                                        
                             output std-lo).

  if not std-lo 
   then
    return.
    
  find first tarpmt no-error.
  if (not available tarpmt)
   then
    return.    
  
  /* Adding newly added client-side filters */    
  if not can-find(first ttarpmt where ttarpmt.depositID = tarpmt.depositID)
   then
    cbDeposit:add-last(replace(tarpmt.depositRef,",",""),string(tarpmt.depositID)). 
  
  find first ttarpmt where ttarpmt.arpmtID = iArPmtID no-error.
  if available ttarpmt
   then
    buffer-copy tarpmt to ttarpmt no-error.
  
  /* Refresh the deposit screen when payment of a deposit is modified */
  if cDepositID <> ""
   then
    do:
      if iDepositID = integer(cDepositID) and ttarpmt.depositID <> iDepositID
       then 
        do:
          deDepositCheckAmt = deDepositCheckAmt - deOldPmtAmt.
          publish "refreshCheckAmount" (input iDepositID, input deDepositCheckAmt).
        end.
        
      else if ttarpmt.depositID = iDepositID and ttarpmt.depositID = integer(cDepositID) and deOldPmtAmt <> ttarpmt.checkAmt 
       then
        do:
          deDepositCheckAmt = (deDepositCheckAmt - deOldPmtAmt) + ttarpmt.checkAmt.
          publish "refreshCheckAmount" (input ttarpmt.depositID, input deDepositCheckAmt).
        end.
      
      else if (ttarpmt.depositID <> iDepositID) and (ttarpmt.depositID = integer(cDepositID)) 
       then
        do:
          deDepositCheckAmt = deDepositCheckAmt + ttarpmt.checkAmt.
          publish "refreshCheckAmount" (input ttarpmt.depositID, input deDepositCheckAmt).
        end.
    end.
  
  /* Deleting unnecessary client-side filters */    
  if not can-find(first ttarpmt where ttarpmt.depositID = iDepositID)
   then
    cbDeposit:delete(string(iDepositID)). 
        
  run filterData  in this-procedure.  
      
  find first arpmt where arpmt.arPmtID = iArPmtID no-error.
  
  if not available arpmt
   then
    return.
  
  reposition {&browse-name} to rowid rowid(arpmt).
    
  apply 'value-changed' to browse brwarpmt.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newPayment C-Win 
PROCEDURE newPayment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define buffer ttarpmt for ttarpmt.
  define buffer tarpmt for tarpmt.
  /* cDepositID is "" when 'Unposted Payments' is called from Main Window (i.e. for all depositRefs) */
  if cDepositID eq ""
   then
    empty temp-table tarpmt.

  run dialognewpayment.w (input-output table tarpmt,
                          output std-lo).
 
  if not std-lo 
   then
    return.

  find first tarpmt no-error.
  if (not available tarpmt)
   then return.
  
  if not can-find(first ttarpmt where ttarpmt.entityID = tarpmt.entityID)
   then
    cbAgent:add-last(replace(tarpmt.entityname,",",""),tarpmt.entityID).
    
  if not can-find(first ttarpmt where ttarpmt.depositID = tarpmt.depositID)
   then
    cbDeposit:add-last(replace(tarpmt.depositRef,",",""),string(tarpmt.depositID)).  
    
  create ttarpmt.
  buffer-copy tarpmt to ttarpmt no-error.
  
  assign
      ttarpmt.username = cCurrentUser
      iArPmtID         = ttarpmt.arpmtID
      .
  
  /* Refresh the deposit screen when payment is added to the exisitng deposit */
  if cDepositID <> "" and ttarpmt.depositID = integer(cDepositID) 
   then
    do:
      deDepositCheckAmt = deDepositCheckAmt + ttarpmt.checkAmt.
      publish "refreshCheckAmount" (input ttarpmt.depositID, input deDepositCheckAmt).
    end. 
  
  run filterData  in this-procedure.   
     
  find first arpmt where arpmt.arPmtID = iArPmtID no-error.  
  if not available arpmt
   then
    return.
    
  reposition {&browse-name} to rowid rowid(arpmt).
    
  apply 'value-changed' to browse brwarpmt.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE postPayment C-Win 
PROCEDURE postPayment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable iDepositID  as integer   no-undo.
  define variable cMessage    as character no-undo.
  define variable lSuccess    as logical   no-undo.
  define variable cFailMsg    as character no-undo.
  define variable cFileName   as character no-undo.
  define variable lViewPdf    as logical   no-undo.
  define variable lPost       as logical   no-undo.
  
  define buffer ttarpmt for ttarpmt.
  define buffer tarpmt for  tarpmt.
  
  empty temp-table tarpmt.   
  empty temp-table ttarpmtErr.
      
  if not can-find(first arpmt where arpmt.selectRecord)
   then
    do:
      message "Please select at least one payment for posting"
          view-as alert-box error buttons ok.
      return.
    end.
      
  if rsPost:input-value = "P" 
   then
    do:
      if flPostDate:input-value = ?
       then
        do:
          message "Post date cannot be blank"
              view-as alert-box error buttons ok.
          apply 'entry' to flPostDate.    
          return.
        end.        
  
      if flPostDate:input-value > today
       then
        run createErrorRcrd in this-procedure ("Post date cannot be in future").
 
      publish "validatePostingDate" (input flPostDate:input-value,
                                     output std-lo).
  
      if not std-lo
       then
        run createErrorRcrd in this-procedure ("Post date must be within an open period").
    end.    
  
  for each arpmt where arpmt.selectRecord:
    if rsPost:input-value = "P" and flPostDate:input-value < date(arpmt.receiptdate)
     then
      run createErrorRcrd in this-procedure ("Payment post date cannot be prior to the payment receipt date for Payment Ref: " + arpmt.checknum).
  
    if rsPost:input-value = "T"
     then
      do:
        if arpmt.receiptDate = ?
         then
          run createErrorRcrd in this-procedure ("Payment post date cannot be blank for Payment Ref: " + arpmt.checknum). 
               
        if date(arpmt.receiptDate) > today
         then        
          run createErrorRcrd in this-procedure ("Payment post date cannot be in the future for Payment Ref: " + arpmt.checknum). 
          
        publish "validatePostingDate" (input date(arpmt.receiptDate),
                                       output std-lo).
  
        if not std-lo
         then
          run createErrorRcrd in this-procedure ("Post date must be within an open period for Payment Ref: " + arpmt.checknum).
      end.    
    
    /* Validating File Number */
    if arpmt.filenumber ne "" and normalizeFileID(arpmt.filenumber) = ""
     then
      do:
        message "File Number must contain one or more characters (A-Z, a-z) or numbers (0-9)." view-as alert-box error buttons ok.
        return.
      end. 
 
    if arpmt.depositref <> "" and not arpmt.depositPosted
     then
      run createErrorRcrd in this-procedure ("Cannot post a payment with payment ref " + arpmt.checknum + " for an unposted deposit with deposit ref " + arpmt.depositref).
      
    create tarpmt.
    buffer-copy arpmt to tarpmt no-error.    
    tarpmt.transDate = if rsPost:input-value = "P" then flPostDate:input-value
                       else arpmt.receiptDate.                          
  end.  
  
  /* Export error table into csv and open it */
  run exportErrorData in this-procedure.
  
  if can-find(first ttarpmtErr)
   then
    do:
      apply 'entry' to flPostDate.  
      return.
    end.
  
  if can-find(first arpmt where arpmt.selectRecord and arpmt.depositPosted)
   then
    do:
      message "The Deposit is posted. Are you sure you want to post the payment(s)?"
        view-as alert-box question buttons yes-no
        title "" update std-lo as logical.    
          
      if not std-lo
       then
        return.
    end.  
    
  run dialogviewledger.w (input "Post Payment",output lPost).
  
  if not lPost
    then
     return.
  
  /*Get the status for viewing pdf*/
  publish "GetViewPdf" (output lViewPdf). 
   
  run server/postpayments.p(input table tarpmt,
                            input lViewPdf,
                            output table ttarpmtErr,
                            output table glPayment,
                            output cMessage,
                            output lSuccess,
                            output cFailMsg).
  
  /* Export error table into csv and open it */
  run exportErrorData in this-procedure.
  
  if not lSuccess 
   then
    do:
      if not can-find(first ttarpmtErr) 
       then
        message cFailMsg
           view-as alert-box info buttons ok.
      return.
    end.
   else
    message "Posting was successful"
        view-as alert-box information buttons ok.
  
  if lViewPdf 
   then
    do:
      if not can-find(first glpayment) 
       then
         message "Nothing to print."
             view-as alert-box error buttons ok.
       else
        run util\arpaymentpdf.p (input {&view},
                                 input table glpayment,
                                 output cFilename).
    end.
                              
  if cMessage > ""
   then
    message cMessage view-as alert-box.
  
  for each tarpmt:
    find first ttarpmt where ttarpmt.arpmtID = tarpmt.arPmtID no-error.
    if not available ttarpmt
     then
      return.
     
    std-ch = ttarpmt.entityID.
    delete ttarpmt.
    
    if not can-find(first ttarpmt where ttarpmt.entityID = std-ch)
     then
      cbAgent:delete(tarpmt.entityID) no-error.         
      
    if not can-find(first ttarpmt where ttarpmt.depositID = iDepositID)
     then
      cbDeposit:delete(string(tarpmt.depositID)) no-error.           
  end.  
   
  run filterData  in this-procedure.         
            
  publish 'SetPostingConfig' (input rsPost:input-value).

  apply 'value-changed' to browse brwarpmt.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE prelimReport C-Win 
PROCEDURE prelimReport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable cPaymentList as character no-undo.
 define variable cFilename    as character no-undo.
 
  /* get the data to create list*/
  for each arpmt where arpmt.selectrecord:
    cPaymentList = cPaymentList + "," + string(arpmt.arpmtID).
  end. 
  
  /* server call*/
  run server/prelimpaymentgl.p(input trim(cPaymentList,","),
                               output table glpayment,
                               output std-lo,
                               output std-ch).  
                                      
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
    
  if not can-find(first glpayment) 
   then
    do:
       message "Nothing to print."
          view-as alert-box error buttons ok.
       return.
    end.
  
  run util\arpaymentpdf.p (input {&view},
                           input table glpayment,
                           output cFilename).
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData C-Win 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run getData in this-procedure.
  
  run showWindow in this-procedure.
  run setFilterbutton in this-procedure.
  /* Set Status count with date and time from the server */
  setStatusRecords(query brwarpmt:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resetFilter C-Win 
PROCEDURE resetFilter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Reset filter widgets */
  assign
    cbagent:sensitive          = true
    cbDeposit:sensitive        = true
    fSearch:sensitive          = true
    .
  /* Disable reset filter button when filters reset to ALL */
  run setFilterButton in this-procedure. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilter C-Win 
PROCEDURE setFilter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  assign
    cbagent:sensitive      = false
    cbDeposit:sensitive    = false
    fSearch:sensitive      = false
    .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterButton C-Win 
PROCEDURE setFilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    btFilter:sensitive = not (cbAgent:screen-value  = {&all}
                             and cbDeposit:screen-value  = {&all}
                             and fSearch:screen-value   = "").
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
    
  assign
    bModify:sensitive  = available arpmt
    bDelete:sensitive  = available arpmt
    bExport:sensitive  = if (query brwarpmt:num-results) = ? then false else (query brwarpmt:num-results > 0)
                         no-error     
    .
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
  
  /* Show records based on the selected deposit ref */
  publish "GetCurrentValue" ("DepositID", output cDepositID).

  assign
      cbAgent:screen-value      = {&ALL}
      cbDeposit:screen-value    = (if lookup(cDepositID,cbDeposit:list-item-pairs) = 0 then {&ALL} else cDepositID)
      .

  if cbDeposit:input-value = {&all}  or
     lRefresh
   then
    do:
      run resetFilter in this-procedure.
      if lRefresh 
       then
        assign
            cbDeposit:screen-value = cLastDepositRef
            cbAgent  :screen-value = cLastEntity
            .
    end.
   else
    run setFilter in this-procedure.
  
  run filterData  in this-procedure.
  
  /* Total amount of a deposit */
  if cDepositID <> ""
   then
    deDepositCheckAmt = deCheckAmt.
  
  /* Set Status count with date and time from the server */
  setStatusRecords(query brwarpmt:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by arpmt.entityName ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 19 
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 23 
      .
  
  run adjustTotals in this-procedure.
  
  run ShowScrollBars(frame {&frame-name}:handle, no, no). 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFormattedNumber C-Win 
FUNCTION getFormattedNumber RETURNS CHARACTER
  ( input deTotal as decimal,
    input hWidget as handle ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cNumberFormat  as character no-undo.
  define variable cTotalScrValue as character no-undo.
  define variable deWidgetWidth  as decimal   no-undo.
  
  cTotalScrValue = string(deTotal).
     
  /* account for negative numbers */
  if deTotal < 0
   then 
    cNumberFormat = cNumberFormat + "(".
        
  /* loop through the absolute value of the number cast as an int64 */
  do std-in = length(string(int64(absolute(deTotal)))) to 1 by -1:
    if std-in modulo 3 = 0
     then 
      cNumberFormat = cNumberFormat + (if std-in = length(string(int64(absolute(deTotal)))) then ">" else ",>").
     else 
      cNumberFormat = cNumberFormat + (if std-in = 1 then "Z" else ">").
  end.
     
  /* if the number had a decimal value */
  if index(cTotalScrValue, ".") > 0 
   then 
    cNumberFormat = cNumberFormat + ".99".
         
  /* account for negative numbers */
  if deTotal < 0
   then 
    cNumberFormat = cNumberFormat + ")".
  
  do std-in = 1 to length(cNumberFormat):
    std-ch = substring(cNumberFormat,std-in,1).
    case std-ch:
      when "(" then deWidgetWidth = deWidgetWidth + 5.75.
      when ")" then deWidgetWidth = deWidgetWidth + 5.75.      
      when "Z" then deWidgetWidth = deWidgetWidth + 12.
      when ">" then deWidgetWidth = deWidgetWidth + 8.
      when "," then deWidgetWidth = deWidgetWidth + 4.
      when "." then deWidgetWidth = deWidgetWidth + 4.
      when "9" then deWidgetWidth = deWidgetWidth + 10.
    end case.
  end.
  
  /* set width of the widget as per format */
  hWidget:width-pixels = deWidgetWidth.      
        
  RETURN cNumberFormat.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  setStatusMessage({&ResultNotMatchSearchString}).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

