&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogmodifywrite-off.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Shefali
  Version: 1.0
  Created:06/04/21
  
  @Modified :
  Date           Name            Description 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{tt/arwriteoff.i &tablealias=ttarwriteoff}

/* Parameters Definitions                                           */
define input         parameter ipcAction as character no-undo.
define input-output  parameter table for ttarwriteoff. 
define output        parameter oplSuccess as logical no-undo.

/* Standard library files */
{lib/ar-def.i}
{lib/std-def.i}

/* Local variable definitions */
define variable cDescription   as character no-undo.

define temp-table ttAction
  field action      as character
  field createdBy   as character
  field createdDate as character
  field ledgerID    as character.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwAction

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttAction

/* Definitions for BROWSE brwAction                                     */
&Scoped-define FIELDS-IN-QUERY-brwAction ttAction.Action ttAction.createdBy ttAction.CreatedDate ttAction.ledgerID   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAction   
&Scoped-define SELF-NAME brwAction
&Scoped-define QUERY-STRING-brwAction FOR EACH ttAction
&Scoped-define OPEN-QUERY-brwAction OPEN QUERY {&SELF-NAME} FOR EACH ttAction.
&Scoped-define TABLES-IN-QUERY-brwAction ttAction
&Scoped-define FIRST-TABLE-IN-QUERY-brwAction ttAction


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwAction}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS flWriteoffID flEntityID flAmt flWriteoffDate ~
flSource tWriteoffType edDescription flEntity Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS flWriteoffID flEntityID flAmt flWriteoffDate ~
flSource tWriteoffType edDescription flEntity 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE edDescription AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 101 BY 2.81 NO-UNDO.

DEFINE VARIABLE flAmt AS DECIMAL FORMAT "-ZZ9.99":U INITIAL 0 
     LABEL "Amount" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 12.4 BY 1 NO-UNDO.

DEFINE VARIABLE flWriteoffDate AS DATE FORMAT "99/99/99":U 
     LABEL "Write-off Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 12.4 BY 1 NO-UNDO.

DEFINE VARIABLE flEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 55.4 BY 1 NO-UNDO.

DEFINE VARIABLE flEntityID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE flSource AS CHARACTER FORMAT "X(256)":U 
     LABEL "Write-off applied on" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 12.4 BY 1 NO-UNDO.

DEFINE VARIABLE flWriteoffID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Write-off ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tWriteoffType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Write-off Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 12.2 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAction FOR 
      ttAction SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAction Dialog-Frame _FREEFORM
  QUERY brwAction DISPLAY
      ttAction.Action               label  "Action"    format "x(15)"     
ttAction.createdBy              label  "By"              format "x(50)" 
ttAction.CreatedDate            label  "Date"            format "x(12)" 
ttAction.ledgerID               label  "Ledger ID"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-AUTO-VALIDATE NO-ROW-MARKERS NO-COLUMN-SCROLLING SEPARATORS NO-SCROLLBAR-VERTICAL SIZE 101 BY 3.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     flWriteoffID AT ROW 1.24 COL 21.8 COLON-ALIGNED WIDGET-ID 246
     flEntityID AT ROW 2.48 COL 21.8 COLON-ALIGNED WIDGET-ID 300
     flAmt AT ROW 3.62 COL 35.2 RIGHT-ALIGNED WIDGET-ID 2
     flWriteoffDate AT ROW 4.76 COL 21.8 COLON-ALIGNED WIDGET-ID 36
     flSource AT ROW 3.62 COL 69 COLON-ALIGNED WIDGET-ID 116
     tWriteoffType AT ROW 4.76 COL 69 COLON-ALIGNED WIDGET-ID 64
     edDescription AT ROW 6.52 COL 5.6 NO-LABEL WIDGET-ID 26
     flEntity AT ROW 2.48 COL 49.2 COLON-ALIGNED WIDGET-ID 102 NO-TAB-STOP 
     brwAction AT ROW 9.71 COL 5.6 WIDGET-ID 200
     Btn_OK AT ROW 14.05 COL 40.2
     Btn_Cancel AT ROW 14.05 COL 56.8
     "Notes:" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 5.86 COL 5.6 WIDGET-ID 28
     SPACE(98.79) SKIP(9.22)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Edit Wite-off"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwAction flEntity Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BROWSE brwAction IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       edDescription:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN flAmt IN FRAME Dialog-Frame
   ALIGN-R                                                              */
ASSIGN 
       flAmt:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flWriteoffDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flEntity:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flEntityID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flSource:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flWriteoffID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tWriteoffType:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAction
/* Query rebuild information for BROWSE brwAction
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH ttAction.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAction */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Edit Wite-off */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel Dialog-Frame
ON CHOOSE OF Btn_Cancel IN FRAME Dialog-Frame /* Cancel */
DO:
  oplSuccess = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Save */
DO: 
  run saveArPayment in this-procedure.
      
  /* If there was any error then do not close the dialog. */
  if not oplSuccess
   then
    return no-apply.      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edDescription
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edDescription Dialog-Frame
ON VALUE-CHANGED OF edDescription IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAmt Dialog-Frame
ON VALUE-CHANGED OF flAmt IN FRAME Dialog-Frame /* Amount */
DO:
  /*flRemainingAmt:screen-value = flAmt:input-value.*/
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flWriteoffDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flWriteoffDate Dialog-Frame
ON VALUE-CHANGED OF flWriteoffDate IN FRAME Dialog-Frame /* Write-off Date */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flEntity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flEntity Dialog-Frame
ON VALUE-CHANGED OF flEntity IN FRAME Dialog-Frame /* Name */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flSource
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flSource Dialog-Frame
ON VALUE-CHANGED OF flSource IN FRAME Dialog-Frame /* Write-off applied on */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tWriteoffType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tWriteoffType Dialog-Frame
ON VALUE-CHANGED OF tWriteoffType IN FRAME Dialog-Frame /* Write-off Type */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAction
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  Frame Dialog-Frame:Title = (if ipcAction = {&Modify} then "Edit Writeoff" 
                         else if ipcAction = {&ModifyPosted} then "Edit Write-off Note" 
                         else "View Write-off").
  run displayData in this-procedure.  
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.   
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
   
  find first ttarwriteoff no-error.
  if available ttarwriteoff 
   then
    do:
      assign
          flWriteoffID:screen-value   = string(ttarwriteoff.arwriteoffID)
          flentityID:screen-value     = ttarwriteoff.EntityID
          flEntity:screen-value       = ttarwriteoff.agentname
          flWriteoffDate:screen-value = string(ttarwriteoff.trandate)      
          flAmt:screen-value          = string(ttarwriteoff.tranamount)              
          edDescription:screen-value  = ttarwriteoff.notes      
          no-error.
          
      if ttarwriteoff.type = {&Debit} 
       then
        assign ttarwriteoff.type = "Debit".
       else  
        assign ttarwriteoff.type = "Credit". 
        
      if ttarwriteoff.source = {&File} 
       then
        assign ttarwriteoff.source = "File".
    
      assign    
          tWriteoffType:screen-value  = ttarwriteoff.Type
          flSource:screen-value       = ttarwriteoff.source.
        
      /* Keep the copy of initial values. Required in enableDisableSave. */ 
      assign
          cDescription  = ttarwriteoff.notes
          no-error.  
          
      /* ttAction table to show details of created, posted and voided in dialog*/
      create ttAction.
      assign
          ttAction.action       = "Created"
          ttAction.createdBy    = ttarwriteoff.createdbyName
          ttAction.CreatedDate  = string(ttarwriteoff.createdDate)
          ttAction.ledgerID     = "".
      create ttAction.
      assign
          ttAction.action       = "Posted"
          ttAction.createdBy    = ttarwriteoff.postbyName
          ttAction.CreatedDate  = if (ttarwriteoff.tranDate = ?) then "" else string(ttarwriteoff.tranDate)
          ttAction.ledgerID     = if (ttarwriteoff.ledgerID = 0 ) then "" else string(ttarwriteoff.ledgerID).
      create ttAction.
      assign
          ttAction.action       = "Void"
          ttAction.createdBy    = ttarwriteoff.voidbyName
          ttAction.CreatedDate  = if (ttarwriteoff.voidDate = ?) then "" else string(ttarwriteoff.voidDate)
          ttAction.ledgerID     = if (ttarwriteoff.voidLedgerID = 0) then "" else string(ttarwriteoff.voidLedgerID).
       
      open query brwAction for each ttAction.
      brwAction:Deselect-focused-row().
        
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  Btn_OK:sensitive = (cDescription  <> edDescription:INPUT-VALUE)
                     no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flWriteoffID flEntityID flAmt flWriteoffDate flSource tWriteoffType 
          edDescription flEntity 
      WITH FRAME Dialog-Frame.
  ENABLE flWriteoffID flEntityID flAmt flWriteoffDate flSource tWriteoffType 
         edDescription flEntity Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveArPayment Dialog-Frame 
PROCEDURE saveArPayment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lupdate      as logical   no-undo.
  define variable lApplyToFile as logical   no-undo.
  define variable cFileExist   as character no-undo.
  
  do with frame {&frame-name}:
  end.
  
  find first ttarwriteoff no-error.
  if not available ttarwriteoff
   then 
    return.
    
  assign
    ttarwriteoff.notes  = edDescription:input-value.
  
  /* client Server Call */
  /*Server call*/
 run server/modifywriteoffnotes.p(input ttarwriteoff.arwriteoffID,
                                  input ttarwriteoff.notes,
                                  output oplSuccess,
                                  output std-ch).
  
  if not oplSuccess
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

