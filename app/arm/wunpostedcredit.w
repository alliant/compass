&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wunpostedcredit.w

  Description: Window for AR unposted credit transactions

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul Sharma

  Created: 07.10.2019
  
  @Modified:
  Date        Name     Description   
  07/30/2020  AC       Added new button "Preliminary PDF".
  01/18/2021  Shefali  Added new pop-up menu "Edit Credit".
  02/03/2021  Shefali  Modified to display credit amount totals.
  04/05/2021  AG       Default sort on arMiscID Desc, 
                       Always show decimal in total 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/ar-def.i}
{lib/winlaunch.i} 
{lib/get-column.i}
{lib/winshowscrollbars.i}
{lib/ar-getsourcetype.i} /* Include funcion: getSourceType */
{lib/ar-gettrantype.i}   /* Include function: getTranType */
{lib/ar-getentitytype.i} /* Include function: getEntityType */
{lib/normalizefileid.i}  /* include file to normalize file number */

/* Temp-table Definition */
{tt/arMisc.i}                               /* contain filterd data*/
{tt/arMisc.i       &tableAlias=tARMisc}     /* used for data transfer */
{tt/arMisc.i       &tableAlias=ttARMisc}    /* contain data fetched from server*/
{tt/ledgerreport.i &tableAlias="glcredit"}  /* contain info of GL Account for reporting */

define variable cDefaultOption   as character  no-undo.
define variable cAgentID         as character  no-undo.
define variable cRevenueType     as character  no-undo.
define variable cSourceType      as character  no-undo.
define variable deColumnWidth    as decimal    no-undo.
define variable deInvoiceAmt     as decimal    no-undo.
define variable iCount           as integer    no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwArinv

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES arMisc

/* Definitions for BROWSE brwArinv                                      */
&Scoped-define FIELDS-IN-QUERY-brwArinv arMisc.revenuetype arMisc.arMiscID getSourcetype(arMisc.sourcetype) @ arMisc.sourcetype arMisc.sourceID arMisc.entityID arMisc.agentname arMisc.filenumber arMisc.transAmt arMisc.creditinvoiceDt arMisc.notes arMisc.selectrecord   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwArinv arMisc.selectrecord   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwArinv arMisc
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwArinv arMisc
&Scoped-define SELF-NAME brwArinv
&Scoped-define QUERY-STRING-brwArinv for each arMisc by arMisc.armiscID desc
&Scoped-define OPEN-QUERY-brwArinv open query {&SELF-NAME} for each arMisc by arMisc.armiscID desc.
&Scoped-define TABLES-IN-QUERY-brwArinv arMisc
&Scoped-define FIRST-TABLE-IN-QUERY-brwArinv arMisc


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwArinv}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbAgent brwArinv cbRevenueType cbSourceType ~
bExport bRefresh bDelete bModify bNew RECT-78 RECT-79 RECT-88 
&Scoped-Define DISPLAYED-OBJECTS cbAgent cbRevenueType cbSourceType rsPost ~
dtPost flTotal flCheckAmt 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFormattedNumber C-Win 
FUNCTION getFormattedNumber RETURNS CHARACTER
  ( input deTotal as decimal,
    input hWidget as handle )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwArinv 
       MENU-ITEM m_Edit_Credit  LABEL "Edit Credit"   .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete credit".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify credit".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "New credit".

DEFINE BUTTON bPost  NO-FOCUS
     LABEL "Post" 
     SIZE 7.2 BY 1.71 TOOLTIP "Post selected credit(s)".

DEFINE BUTTON bPrelimRpt  NO-FOCUS
     LABEL "Prelim" 
     SIZE 7.2 BY 1.71 TOOLTIP "Preliminary Report".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON btResetFilter  NO-FOCUS
     LABEL "Reset" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reset filters".

DEFINE VARIABLE cbAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 61.8 BY 1 NO-UNDO.

DEFINE VARIABLE cbRevenueType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Revenue Type" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE cbSourceType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Source Type" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE dtPost AS DATE FORMAT "99/99/99":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flCheckAmt AS DECIMAL FORMAT "(Z,ZZZ,ZZZ)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE flTotal AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 38 BY 1 NO-UNDO.

DEFINE VARIABLE rsPost AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Use Date", "P",
"Use Credit Date", "T"
     SIZE 19.8 BY 2 NO-UNDO.

DEFINE RECTANGLE RECT-78
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 91.8 BY 3.33.

DEFINE RECTANGLE RECT-79
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 39.4 BY 3.33.

DEFINE RECTANGLE RECT-88
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 46.4 BY 3.33.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwArinv FOR 
      arMisc SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwArinv
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwArinv C-Win _FREEFORM
  QUERY brwArinv DISPLAY
      arMisc.revenuetype     label  "Revenue Type"         format "x(16)"
arMisc.arMiscID         label  "Credit ID"             format ">>>>>>>>9"
getSourcetype(arMisc.sourcetype)       @ arMisc.sourcetype     label "Source" format "x(14)"
arMisc.sourceID        label  "Source ID"             format "x(14)"
arMisc.entityID        label  "Agent ID"              format "x(10)"
arMisc.agentname       label  "Name"                  format "x(60)"          width 35
arMisc.filenumber      label  "File Number"           format "x(30)"          width 20
arMisc.transAmt        label  "Amount"                format "->,>>>,>>9.99"  width 15
arMisc.creditinvoiceDt label  "Credit"                format "99/99/9999  "
arMisc.notes           label  "Notes"                 format "x(40)"          width 30
arMisc.selectrecord    column-label "Select to!Post"  view-as toggle-box
enable arMisc.selectrecord
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 203.4 BY 19.43 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bPrelimRpt AT ROW 2.24 COL 169.4 WIDGET-ID 408
     btResetFilter AT ROW 2.33 COL 84 WIDGET-ID 262 NO-TAB-STOP 
     cbAgent AT ROW 2.1 COL 18 COLON-ALIGNED WIDGET-ID 246
     brwArinv AT ROW 5 COL 2.2 WIDGET-ID 200
     cbRevenueType AT ROW 3.24 COL 18 COLON-ALIGNED WIDGET-ID 248
     cbSourceType AT ROW 3.24 COL 59.8 COLON-ALIGNED WIDGET-ID 250
     rsPost AT ROW 2.1 COL 134 NO-LABEL WIDGET-ID 390
     dtPost AT ROW 2.14 COL 144.8 COLON-ALIGNED NO-LABEL WIDGET-ID 272
     bExport AT ROW 2.33 COL 102.6 WIDGET-ID 8 NO-TAB-STOP 
     bPost AT ROW 2.24 COL 162.4 WIDGET-ID 198 NO-TAB-STOP 
     bRefresh AT ROW 2.33 COL 95.6 WIDGET-ID 4 NO-TAB-STOP 
     bDelete AT ROW 2.33 COL 123.6 WIDGET-ID 10 NO-TAB-STOP 
     bModify AT ROW 2.33 COL 116.6 WIDGET-ID 6 NO-TAB-STOP 
     bNew AT ROW 2.33 COL 109.6 WIDGET-ID 2 NO-TAB-STOP 
     flTotal AT ROW 24.52 COL 1 COLON-ALIGNED NO-LABEL WIDGET-ID 314 NO-TAB-STOP 
     flCheckAmt AT ROW 24.52 COL 110 COLON-ALIGNED NO-LABEL WIDGET-ID 304 NO-TAB-STOP 
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.14 COL 3.8 WIDGET-ID 266
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.14 COL 94.8 WIDGET-ID 194
     "Post" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1.24 COL 133.8 WIDGET-ID 412
     RECT-78 AT ROW 1.48 COL 2.2 WIDGET-ID 200
     RECT-79 AT ROW 1.48 COL 93.6 WIDGET-ID 268
     RECT-88 AT ROW 1.48 COL 132.4 WIDGET-ID 410
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 216.2 BY 25.14
         DEFAULT-BUTTON bRefresh WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Unposted Credits"
         HEIGHT             = 24.71
         WIDTH              = 206.2
         MAX-HEIGHT         = 33.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwArinv cbAgent DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bPost IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPrelimRpt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwArinv:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwArinv:HANDLE
       brwArinv:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwArinv:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR BUTTON btResetFilter IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dtPost IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flCheckAmt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flTotal IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flTotal:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR RADIO-SET rsPost IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwArinv
/* Query rebuild information for BROWSE brwArinv
     _START_FREEFORM
open query {&SELF-NAME} for each arMisc by arMisc.armiscID desc.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwArinv */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Unposted Credits */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Unposted Credits */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Unposted Credits */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME DEFAULT-FRAME /* Delete */
DO:
  run deleteCredit in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME DEFAULT-FRAME /* Modify */
DO:
  run modifyCredit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME DEFAULT-FRAME /* New */
DO:
  run newCredit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPost
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPost C-Win
ON CHOOSE OF bPost IN FRAME DEFAULT-FRAME /* Post */
DO:
  run postCredit in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrelimRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrelimRpt C-Win
ON CHOOSE OF bPrelimRpt IN FRAME DEFAULT-FRAME /* Prelim */
DO:
   run prelimreport in this-procedure.   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME DEFAULT-FRAME /* refresh */
DO:
  run getData in this-procedure.
  
  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActions in this-procedure.
  
  /* Makes filters enable-disable based on the data */
  run enableFilters in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwArinv
&Scoped-define SELF-NAME brwArinv
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArinv C-Win
ON DEFAULT-ACTION OF brwArinv IN FRAME DEFAULT-FRAME
DO:
  run modifyCredit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArinv C-Win
ON ROW-DISPLAY OF brwArinv IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}
  
  arMisc.transAmt :fgcolor in browse brwArinv   = if arMisc.transAmt < 0 then 12 else 0.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArinv C-Win
ON START-SEARCH OF brwArinv IN FRAME DEFAULT-FRAME
do: 
  define buffer arMisc for arMisc.
  
  std-ha = brwArinv:current-column.
  if std-ha:label = "Select to!Post"
   then
    do:     
      std-lo = can-find(first arMisc where not(arMisc.selectrecord)).   
      for each arMisc:            
        arMisc.selectrecord = std-lo.
        /* Retaining selected record */  
        for first ttARMisc where ttARMisc.arMiscID = arMisc.arMiscID:
          ttARMisc.selectrecord = arMisc.selectrecord.
        end.
      end.    
      browse brwArinv:refresh(). 
      run enablePosting in this-procedure.
      run displayTotals in this-procedure.
    end.
   else
    do:
      {lib/brw-startsearch.i}
    end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btResetFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btResetFilter C-Win
ON CHOOSE OF btResetFilter IN FRAME DEFAULT-FRAME /* Reset */
DO:
  run resetFilters in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgent C-Win
ON VALUE-CHANGED OF cbAgent IN FRAME DEFAULT-FRAME /* Agent */
OR 'VALUE-CHANGED' of cbRevenueType
OR 'VALUE-CHANGED' of cbSourceType
DO:
  run displayCredits in this-procedure.
  run displayTotals in this-procedure.
  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActions in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Edit_Credit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Edit_Credit C-Win
ON CHOOSE OF MENU-ITEM m_Edit_Credit /* Edit Credit */
DO:
  run modifyCredit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rsPost
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rsPost C-Win
ON VALUE-CHANGED OF rsPost IN FRAME DEFAULT-FRAME
DO:
   if rsPost:screen-value = "p" and
      can-find(first arMisc where arMisc.selectrecord = true)
    then
     dtPost:sensitive = true.
    else
     dtPost:sensitive = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow"  anywhere.

bRefresh     :load-image            ("images/sync.bmp").
bRefresh     :load-image-insensitive("images/sync-i.bmp").

bExport      :load-image            ("images/excel.bmp").
bExport      :load-image-insensitive("images/excel-i.bmp").

bNew         :load-image            ("images/add.bmp").
bNew         :load-image-insensitive("images/add-i.bmp").

bModify      :load-image            ("images/update.bmp").
bModify      :load-image-insensitive("images/update-i.bmp").

bPost        :load-image            ("images/check.bmp").              
bPost        :load-image-insensitive("images/check-i.bmp").

bDelete      :load-image            ("images/delete.bmp").
bDelete      :load-image-insensitive("images/delete-i.bmp").

btResetFilter:load-image            ("images/filtererase.bmp").
btResetFilter:load-image-insensitive("images/filtererase-i.bmp").

bPrelimRpt   :load-image            ("images/pdf.bmp").
bPrelimRpt   :load-image-insensitive("images/pdf-i.bmp").

/* Default Posting option */
publish 'GetPostingConfig' (output cDefaultOption).
rsPost:screen-value = cDefaultOption.

/* Fetch data before window is visible */
run getData in this-procedure.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
      
  run enable_UI.  
  
  {lib/get-column-width.i &col="'agentname'" &var=deColumnWidth}
  
  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActions in this-procedure.
  
  /* Makes filters enable-disable based on the data */
  run enableFilters in this-procedure.
  
  /* When default posting option from config screen is allowed */ 
  publish 'GetDefaultPostingOption' (output std-lo).
  if std-lo
   then
    do:
      /* Set default posting date on screen */
      publish "getDefaultPostingDate"(output std-da).
      dtPost:screen-value = string(std-da, "99/99/99"). 
    end.
    
  ON 'value-changed':U OF  arMisc.selectrecord in browse  brwArinv  
  DO:
    if available arMisc 
     then
      do:
        arMisc.selectrecord = arMisc.selectrecord:checked in browse brwArinv.
        /* Retaining selected record */  
        for first ttARMisc where ttARMisc.arMiscID = arMisc.arMiscID:
          ttARMisc.selectrecord = arMisc.selectrecord.
        end.
      end.
    run enablePosting in this-procedure.
    run displayTotals in this-procedure.
    flTotal:screen-value in frame {&FRAME-NAME} = "Totals of " + string(iCount) + " selected invoice(s)".
  END.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adjustTotals C-Win 
PROCEDURE adjustTotals :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Setting position of totals widget */
  assign       
      flTotal:y      = brwArinv:y + brwArinv:height-pixels + 0.01
      flCheckAmt:y   = brwArinv:y + brwArinv:height-pixels + 0.01
      flTotal:x      = brwArinv:x + brwArinv:get-browse-column(1):x
      flCheckAmt:x   = brwArinv:x + brwArinv:get-browse-column(8):x + (brwArinv:get-browse-column(8):width-pixels - flCheckAmt:width-pixels) + 6.8
      no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/           
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteCredit C-Win 
PROCEDURE deleteCredit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttARMisc for ttARMisc.
  
  do with frame {&frame-name}:
  end.
  
  if not available arMisc 
   then
    return.

  message "Credit will be deleted. Are you sure you want to delete the selected credit?"
      view-as alert-box question buttons yes-no
      title "Delete AR Credit"
      update std-lo.
  
  if not std-lo 
   then return.

  /* client Server Call */
  run server/deleteinvoice.p (input  arMisc.arMiscID,
                              output std-lo,
                              output std-ch). 
     
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.

  find first ttARMisc where ttARMisc.arMiscID = arMisc.arMiscID no-error.
  if not available ttARMisc
   then return.
  
  assign
      cAgentID     = ttARMisc.entityID
      cRevenueType = ttARMisc.revenuetype
      cSourceType  = ttARMisc.sourcetype
      .
  
  /* Delete the selected record from the main temp-table at client side */
  delete ttARMisc.
    
  /* Deleting unnecessary client-side filters */    
  if not can-find(first ttARMisc where ttARMisc.entityID = cAgentID)
   then
    do:
      cbAgent:screen-value = if (cbAgent:input-value = cAgentID) then {&ALL} else cbAgent:input-value.
      cbAgent:delete(cAgentID). 
    end.
    
  if not can-find(first ttARMisc where ttARMisc.revenuetype = cRevenueType)
   then
    do:
      cbRevenueType:screen-value = if (cbRevenueType:input-value = cRevenueType) then {&ALL} else cbRevenueType:input-value.
      cbRevenueType:delete(cRevenueType).
    end.
    
  if not can-find(first ttARMisc where ttARMisc.sourcetype = cSourceType)
   then
    do:
      cbSourceType:screen-value = if (cbSourceType:input-value = cSourceType) then {&ALL} else cbSourceType:input-value.
      cbSourceType:delete(cSourceType).  
    end.
  
  run displayCredits in this-procedure.
  run displayTotals in this-procedure.
  
  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActions in this-procedure.
  
  /* Makes filters enable-disable based on the data */
  run enableFilters in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayCredits C-Win 
PROCEDURE displayCredits :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
   
  /* Enable/disable filter button based on selected values */
  run setfilterButton in this-procedure.
  
  /* This will use the screen-value of the filters which is ALL */
  run filterData  in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayTotals C-Win 
PROCEDURE displayTotals :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define buffer ARMisc for ARMisc.

  assign
   iCount = 0
   deInvoiceAmt = 0.

  for each ARMisc no-lock:
    if ARMisc.selectrecord  
     then
      do:
        deInvoiceAmt = deInvoiceAmt + ARMisc.transamt.
        flCheckAmt:format  = getFormattedNumber(input deInvoiceAmt, input flCheckAmt:handle) no-error.
        flCheckAmt:screen-value = string(deInvoiceAmt).
        iCount = iCount + 1.
      end.
  end.

  flCheckAmt:format       = getFormattedNumber(input deInvoiceAmt, input flCheckAmt:handle) no-error.
  flCheckAmt:screen-value = string(deInvoiceAmt).
  flTotal:screen-value    = "Totals of " + string(iCount) + " selected invoice(s)".
  run adjustTotals in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableActions C-Win 
PROCEDURE enableActions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Makes widget enable-disable based on the data */ 
  if query brwArinv:num-results > 0
   then
    assign 
        browse brwArinv:sensitive  = true
               bExport:sensitive   = true
               bModify:sensitive   = true
               bDelete:sensitive   = true          
               .
   else
    assign 
        browse brwArinv:sensitive = false
               bExport:sensitive  = false
               bModify:sensitive  = false
               bDelete:sensitive  = false        
               .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableFilters C-Win 
PROCEDURE enableFilters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* if filter is already applied, then no need to enable/diable filters */
  if btResetFilter:sensitive
   then return.
  
  /* Makes widget enable-disable based on the data */ 
  if query brwArinv:num-results > 0
   then
    assign 
        cbAgent:sensitive       = true
        cbRevenueType:sensitive = true
        cbSourceType:sensitive  = true           
        . 
   else
    assign 
        cbAgent:sensitive       = false
        cbRevenueType:sensitive = false
        cbSourceType:sensitive  = false         
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enablePosting C-Win 
PROCEDURE enablePosting :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if not can-find(first ttARMisc where ttARMisc.selectrecord = true)
   then
    assign
        bPost:sensitive      = false
        dtPost:sensitive     = false
        rsPost:sensitive     = false
        bPrelimRpt:sensitive = false
        .
   else
    assign
        bPost:sensitive      = true
        dtPost:sensitive     = (rsPost:input-value = "P")
        rsPost:sensitive     = true
        bPrelimRpt:sensitive = true 
        . 
        
  apply 'value-changed' to  rsPost in frame {&frame-name}.      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbAgent cbRevenueType cbSourceType rsPost dtPost flTotal flCheckAmt 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE cbAgent brwArinv cbRevenueType cbSourceType bExport bRefresh bDelete 
         bModify bNew RECT-78 RECT-79 RECT-88 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwArinv:num-results = 0 
   then
    do: 
      message "There is nothing to export."
          view-as alert-box warning buttons ok.
      return.
    end.
 
 publish "GetReportDir" (output std-ch).
 
 empty temp-table tARMisc.
 for each arMisc:
      create tARMisc.
      buffer-copy arMisc to tARMisc.
      assign 
        tARMisc.sourcetype = getSourcetype(arMisc.sourcetype) 
        tARMisc.type       = getTranType(arMisc.type)
        tARMisc.transtype  = getTranType(arMisc.transtype)
        tARMisc.entity     = getEntityType(tARMisc.entity).
        .
 end.

  std-ha = temp-table tARMisc:handle.
  run util/exporttable.p (table-handle std-ha,
                          "tARMisc",
                          "for each tARMisc",
                          "revenuetype,arMiscID,sourcetype,sourceID,type,entity,entityID,agentname,filenumber,fileID,transAmt,creditInvoicedt,createdate,notes,reference,transtype,username",
                          "Revenue Type,Credit ID,Source Type,Source ID,Type,Entity,Entity ID,Name,File,File ID,Credit Amount,Credit Date,Created Date,Notes,Reference,Trans Type,User Name",
                          std-ch,
                          "ARCredits-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).                                                  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define buffer ttARMisc for ttARMisc.
  
  close query brwArinv.
  empty temp-table arMisc.

  for each ttARMisc 
    where ttARMisc.arMiscID     > 0 
      and ttARMisc.entityID    = (if cbAgent:input-value       = {&ALL} then ttARMisc.entityID    else cbAgent:input-value)
      and ttARMisc.revenuetype = (if cbRevenueType:input-value = {&ALL} then ttARMisc.revenuetype else cbRevenueType:input-value)      
      and ttARMisc.sourcetype  = (if cbSourceType:input-value  = {&ALL} then ttARMisc.sourcetype  else cbSourceType:input-value):
    create arMisc.
    buffer-copy ttARMisc to arMisc.
  end.

  open query brwArinv preselect each arMisc by arMisc.armiscID desc.

  run enablePosting in this-procedure.
  
  setStatusCount(query brwArinv:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define buffer ttARMisc for ttARMisc.
  empty temp-table ttARMisc.
  
  /* Client Server Call */
  run server/getinvoices.p (input {&Credit}, /* Type */
                            input 0,
                            output table ttARMisc,
                            output std-lo,
                            output std-ch).
                            
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
    
  run initialiseFilter in this-procedure.
    
  /* Display temp credits record on the screen */
  run displayCredits in this-procedure.
  
  /* Set Status count with date and time from the server */
  setStatusRecords(query brwArinv:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initialiseFilter C-Win 
PROCEDURE initialiseFilter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cSelectedRev    as character no-undo.
  define variable cSelectedSource as character no-undo.
  define variable cSelectedAgent  as character no-undo.
  
  define buffer ttARMisc for ttARMisc.
  
  do with frame {&frame-name}:
  end.
  
  /* Retaining previous selected values of filters */
  assign 
      cSelectedRev    = if cbRevenueType:screen-value = ? then "" else  cbRevenueType:screen-value      
      cSelectedAgent  = if cbAgent:screen-value       = ? then "" else  cbAgent:screen-value 
      cSelectedSource = if cbSourceType:screen-value  = ? then "" else  cbSourceType:screen-value 
      .
  
  /* Initialise filters to default values */
  assign
      cbAgent:list-item-pairs        = {&ALL} + "," + {&ALL}
      cbSourceType:list-item-pairs   = {&ALL} + "," + {&ALL}
      cbRevenueType:list-items       = {&ALL}
      cbAgent:screen-value           = {&ALL}
      cbSourceType:screen-value      = {&ALL}
      cbRevenueType:screen-value     = {&ALL} 
      .
    
  /* Set the filters based on the data returned, with ALL as the first option */
  for each ttARMisc 
    break by ttARMisc.entityID:
    if first-of(ttARMisc.entityID) and ttARMisc.entityID <> ""
     then 
      cbAgent:add-last(replace(ttARMisc.agentname,",",""),ttARMisc.entityID).
  end.
  
  for each ttARMisc 
    break by ttARMisc.revenuetype:
    if first-of(ttARMisc.revenuetype) and ttARMisc.revenuetype <> "" 
     then
      cbRevenueType:add-last(ttARMisc.revenuetype).
  end.
  
  for each ttARMisc 
    break by ttARMisc.sourcetype:
    if first-of(ttARMisc.sourcetype) and ttARMisc.sourcetype <> ""
     then 
      cbSourceType:add-last(getSourceType(ttARMisc.sourcetype),ttARMisc.sourcetype).      
  end.
  
  /* Setting previous selected values of filters */
  if cSelectedRev <> "" and cSelectedRev <> {&ALL} and lookup(cSelectedRev,cbRevenueType:list-items) > 0  
   then
    cbRevenueType:screen-value = cSelectedRev.
  
  if cSelectedSource <> "" and cSelectedSource <> {&ALL} and lookup(cSelectedSource,cbSourceType:list-item-pairs) > 0  
   then
    cbSourceType:screen-value = cSelectedSource.
    
  if cSelectedAgent <> "" and cSelectedAgent <> {&ALL} and lookup(cSelectedAgent,cbAgent:list-item-pairs) > 0  
   then
    cbAgent:screen-value = cSelectedAgent.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyCredit C-Win 
PROCEDURE modifyCredit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  define buffer tARMisc  for tARMisc.
  define buffer ttARMisc for ttARMisc.
  
  do with frame {&frame-name}:
  end.
  
  empty temp-table tARMisc.

  if not available arMisc 
   then
    return.

  create tARMisc.
  buffer-copy arMisc to tARMisc.
  
  assign
      cAgentID     = arMisc.entityID
      cRevenueType = arMisc.revenuetype
      cSourceType  = arMisc.sourcetype
      .
  
  run dialoginvoice.w (input-output table tARMisc,
                       input {&Credit},  
                       input {&Modify}, /* Action */
                       output std-lo).

  if not std-lo 
   then
    return.
    
  find first tARMisc no-error.
  if not available tARMisc 
   then return.
  
  /* Adding newly added client-side filters */    
  if not can-find(first ttARMisc where ttARMisc.entityID = tARMisc.entityID)
   then
    cbAgent:add-last(replace(tARMisc.agentname,",",""),tARMisc.entityID).
  
  if not can-find(first ttARMisc where ttARMisc.revenuetype = tARMisc.revenuetype)
   then
    cbRevenueType:add-last(tARMisc.revenuetype).
    
  if not can-find(first ttARMisc where ttARMisc.sourcetype = tARMisc.sourcetype)
   then
    cbSourceType:add-last(getSourceType(tARMisc.sourcetype),tARMisc.sourcetype).
    
  find first ttARMisc where ttARMisc.arMiscID = tARMisc.arMiscID no-error.
  if not available ttARMisc
   then return.
  
  /* Modifying existing record in buffer */
  buffer-copy tARMisc to ttARMisc.
  
  /* Deleting unnecessary client-side filters */    
  if not can-find(first ttARMisc where ttARMisc.entityID = cAgentID)
   then
    do:
      cbAgent:screen-value = if (cbAgent:input-value = cAgentID) then {&ALL} else cbAgent:input-value.
      cbAgent:delete(cAgentID). 
    end.
    
  if not can-find(first ttARMisc where ttARMisc.revenuetype = cRevenueType)
   then
    do:
      cbRevenueType:screen-value = if (cbRevenueType:input-value = cRevenueType) then {&ALL} else cbRevenueType:input-value.
      cbRevenueType:delete(cRevenueType).
    end.
    
  if not can-find(first ttARMisc where ttARMisc.sourcetype = cSourceType)
   then
    do:
      cbSourceType:screen-value = if (cbSourceType:input-value = cSourceType) then {&ALL} else cbSourceType:input-value.
      cbSourceType:delete(cSourceType).  
    end.
  
  /* Display temp credits record on the screen */
  run displayCredits in this-procedure.
  run displayTotals in this-procedure.
       
  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActions in this-procedure.
  
  /* Makes filters enable-disable based on the data */
  run enableFilters in this-procedure.
  
  find first arMisc where arMisc.arMiscID = tARMisc.arMiscID no-error.
  if not available arMisc
   then return.
  
  reposition {&browse-name} to rowid rowid(arMisc).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newCredit C-Win 
PROCEDURE newCredit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  /* Buffer Definition */
  define buffer ttARMisc for ttARMisc.
  define buffer tARMisc  for tARMisc.
  define buffer arMisc   for arMisc.
  
  do with frame {&frame-name}:
  end.
  
  empty temp-table tARMisc.
 
  run dialoginvoice.w (input-output table tARMisc,
                       input {&Credit},  
                       input {&add}, /* Action */
                       output std-lo).
  
  if not std-lo 
   then
    do:
       empty temp-table tARMisc.
       return.
    end.
   
  find first tARMisc no-error.
  if not available tARMisc 
   then return.
    
  if can-find(first ttARMisc where ttARMisc.arMiscID = tARMisc.arMiscID)
   then return.
  
  /* Adding newly added client-side filters */    
  if not can-find(first ttARMisc where ttARMisc.entityID = tARMisc.entityID)
   then
    cbAgent:add-last(replace(tARMisc.agentname,",",""),tARMisc.entityID).
  
  if not can-find(first ttARMisc where ttARMisc.revenuetype = tARMisc.revenuetype)
   then
    cbRevenueType:add-last(tARMisc.revenuetype).
    
  if not can-find(first ttARMisc where ttARMisc.sourcetype = tARMisc.sourcetype)
   then
    cbSourceType:add-last(getSourceType(tARMisc.sourcetype),tARMisc.sourcetype). 
  
  create ttARMisc.
  buffer-copy tARMisc to ttARMisc.
    
  /* Display temp credits record on the screen */
  run displayCredits in this-procedure.
  run displayTotals in this-procedure.
  
  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActions in this-procedure.
  
  /* Makes filters enable-disable based on the data */
  run enableFilters in this-procedure.
  
  find first arMisc where arMisc.arMiscID = tARMisc.arMiscID no-error.
  if not available arMisc
   then return.
  
  reposition {&browse-name} to rowid rowid(arMisc). 
  empty temp-table tARMisc.   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE postCredit C-Win 
PROCEDURE postCredit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lViewPdf   as logical   no-undo.
  
  define buffer arMisc   for arMisc.
  define buffer tARMisc  for tARMisc.
  define buffer ttARMisc for ttARMisc.
  
  do with frame {&frame-name}:
  end.
  
  run validatePost in this-procedure(output std-lo).
  if not std-lo 
   then
    return.
  
  empty temp-table tARMisc.
  
  /* get the data to post*/
  for each arMisc where arMisc.selectrecord = true:
    create tARMisc.
    buffer-copy arMisc to tARMisc.
    if rsPost:input-value = "P" 
     then
      tARMisc.transdate = dtPost:input-value.
     else
      tARMisc.transdate = arMisc.creditinvoiceDt.
  end.  
  
  /* update the posting configration as per user action for posting */
  if cDefaultOption <> rsPost:input-value
   then
    do:
      publish 'SetPostingConfig' (input rsPost:input-value).
      cDefaultOption = rsPost:input-value.
    end.
    
  run dialogviewledger.w (input "Post Credit",output std-lo).
     
  if not std-lo
   then
    return.

  publish "GetViewPdf" (output lViewPdf).  
  
  /* server call to post */
  run server/postcredits.p (input lViewPdf,
                            input table tARMisc,
                            output table glcredit,
                            output std-lo,
                            output std-ch).        

  if not std-lo 
   then
    do:
      empty temp-table tARMisc.
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
   else
    message "Posting was successful."
        view-as alert-box information buttons ok. 
   
    
  if lViewPdf
   then
    do:
      if not can-find(first glcredit) 
       then
        message "Nothing to print."
            view-as alert-box error buttons ok.
       else
        run util\arcreditpdf.p (input {&view},
                                input table glcredit,
                                output std-ch).    
    end.
    
  /* update the local temp table (remove the records that got posted) after successful post*/    
  for each arMisc where arMisc.selectrecord = true :
    find first ttARMisc where ttARMisc.arMiscID = arMisc.arMiscID no-error.
    if available ttARMisc 
     then
      do:
        assign
            cAgentID     = ttARMisc.entityID
            cRevenueType = ttARMisc.revenuetype
            cSourceType  = ttARMisc.sourcetype
            .
        delete ttARMisc.
        
        /* Deleting unnecessary client-side filters */    
        if not can-find(first ttARMisc where ttARMisc.entityID = cAgentID)
         then
          cbAgent:delete(cAgentID). 
  
        if not can-find(first ttARMisc where ttARMisc.revenuetype = cRevenueType)
         then
          cbRevenueType:delete(cRevenueType).
    
        if not can-find(first ttARMisc where ttARMisc.sourcetype = cSourceType)
         then
          cbSourceType:delete(cSourceType).
      end.  
    delete arMisc.
  end.
  empty temp-table tARMisc.
    
  run displayCredits in this-procedure.

  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActions in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE prelimreport C-Win 
PROCEDURE prelimreport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cUnpostedCreditList as character no-undo.
  
  define buffer arMisc for arMisc.

  /*Get the record for creating list*/
  for each arMisc where arMisc.selectrecord:
    if arMisc.revenueType = ""
     then
      do:
         message "Revenue type is blank for Credit ID: " string(arMisc.arMiscID) + "."
             view-as alert-box error buttons ok.
         reposition {&browse-name} to rowid rowid(arMisc).
         return.
      end.
    cUnpostedCreditList = cUnpostedCreditList + "," + string(arMisc.arMiscID).
  end.
  
  cUnpostedCreditList = trim(cUnpostedCreditList,",").
  run server/prelimcreditgl.p(input cUnpostedCreditList,
                              output table glcredit,
                              output std-lo,
                              output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
   
  if not can-find(first glcredit) 
   then
    do:
      message "Nothing to print."
          view-as alert-box error buttons ok.
      return.
    end.
   

   run util\arcreditpdf.p (input {&view},
                           input table glcredit,
                           output std-ch).  /*filename*/
   
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resetFilters C-Win 
PROCEDURE resetFilters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Reset filters to initial state */
  assign
      btResetFilter:Tooltip      = "Reset filters"
      cbAgent:screen-value       = {&ALL}
      cbRevenueType:screen-value = {&ALL}
      cbSourceType:screen-value  = {&ALL}
      .
 
  run displayCredits in this-procedure.
  
  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActions in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterButton C-Win 
PROCEDURE setFilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
  
 if cbAgent:input-value ne {&ALL} or cbRevenueType:input-value ne {&ALL} or cbSourceType:input-value ne {&ALL}
  then
    btResetFilter:sensitive = true.
  else   
    btResetFilter:sensitive = false.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state = window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by arMisc.agentname ".
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validatePost C-Win 
PROCEDURE validatePost :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter pSuccess as logical initial true.

  define buffer arMisc   for arMisc.
  define buffer ttARMisc for ttARMisc. 
  
  do with frame {&frame-name}:
  end.
  
  if rsPost:input-value = "P" 
   then
    do:
       if dtPost:input-value = ?
        then
         do:
            message "Post date cannot be blank."
             view-as alert-box error buttons ok.
            apply 'entry' to dtPost.
            pSuccess = false.
            return.       
         end.
  
       if dtPost:input-value > today
        then
         do:
            message "Post date cannot be in the future."
             view-as alert-box error buttons ok.
            apply 'entry' to dtPost. 
            pSuccess = false.
            return.
         end.
  
       publish "validatePostingDate" (input dtPost:input-value,
                                      output std-lo).
  
       if not std-lo
        then
         do:
            message "Post date must be within an open period."
             view-as alert-box error buttons ok.
           apply 'entry' to dtPost.
           pSuccess = false.
           return.
         end.

   end.
  
  if not can-find(first arMisc where arMisc.selectrecord = true)
   then
    do:
       message "Please select at least one AR credit for posting."
          view-as alert-box error buttons ok.
       pSuccess = false.
       return.
    end.
    
  for each arMisc where arMisc.selectrecord = true:
    if rsPost:input-value = "P" and dtPost:input-value < date(arMisc.creditInvoicedt)
     then
      do:
        message "Post date cannot be prior to credit date for Credit ID: " string(arMisc.arMiscID) + "."
            view-as alert-box error buttons ok.
        reposition {&browse-name} to rowid rowid(arMisc).
        pSuccess = false.
        return.      
      end.
     else 
      do:
        if date(arMisc.creditInvoicedt) > today
         then
          do:
            message "Credit date cannot be in the future for Credit ID: " string(arMisc.arMiscID) + "."
                view-as alert-box error buttons ok.
            reposition {&browse-name} to rowid rowid(arMisc). 
            pSuccess = false.
            return.
          end.
          
        publish "validatePostingDate" (input date(arMisc.creditInvoicedt),
                                       output std-lo).
  
        if not std-lo
         then
          do:
            message "Credit date must be within an open period for Credit ID: " string(arMisc.arMiscID) + "."
                view-as alert-box error buttons ok.
            reposition {&browse-name} to rowid rowid(arMisc).
            pSuccess = false.
            return.
          end.
      end.

    if arMisc.revenueType = ""
     then
      do:
         message "Revenue type is blank for Credit ID: " string(arMisc.arMiscID) + "."
             view-as alert-box error buttons ok.
         reposition {&browse-name} to rowid rowid(arMisc).
         pSuccess = false.
         return.
      end.
       
    /* Validating File Number */
    if arMisc.filenumber ne ""
     then
      do:
        if normalizeFileID(arMisc.filenumber) = ""  
         then
          do:
            message "File Number must contain one or more characters (A-Z, a-z) or numbers (0-9)." view-as alert-box error buttons ok.
            pSuccess = false.
            return.
          end.
      end.
  end.     

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 14
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 20
      .
  
  {lib/resize-column.i &col="'agentname'" &var=deColumnWidth}
  
  run ShowScrollBars(frame {&frame-name}:handle, no, no).  
  run adjustTotals in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFormattedNumber C-Win 
FUNCTION getFormattedNumber RETURNS CHARACTER
  ( input deTotal as decimal,
    input hWidget as handle ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cNumberFormat  as character no-undo.
  define variable cTotalScrValue as character no-undo.
  define variable deWidgetWidth  as decimal   no-undo.
  
  cTotalScrValue = string(deTotal).
     
  /* account for negative numbers */
  if deTotal < 0
   then 
    cNumberFormat = cNumberFormat + "(".
        
  /* loop through the absolute value of the number cast as an int64 */
  do std-in = length(string(int64(absolute(deTotal)))) to 1 by -1:
    if std-in modulo 3 = 0
     then 
      cNumberFormat = cNumberFormat + (if std-in = length(string(int64(absolute(deTotal)))) then ">" else ",>").
     else 
      cNumberFormat = cNumberFormat + (if std-in = 1 then "Z" else ">").
  end.
     
  /* if the number had a decimal value */
  if deTotal > 0 
   then 
    cNumberFormat = cNumberFormat + ".99".
         
  /* account for negative numbers */
  if deTotal < 0
   then 
    cNumberFormat = cNumberFormat + ")".
  
  do std-in = 1 to length(cNumberFormat):
    std-ch = substring(cNumberFormat,std-in,1).
    case std-ch:
      when "(" then deWidgetWidth = deWidgetWidth + 5.75.
      when ")" then deWidgetWidth = deWidgetWidth + 5.75.      
      when "Z" then deWidgetWidth = deWidgetWidth + 12.
      when ">" then deWidgetWidth = deWidgetWidth + 8.
      when "," then deWidgetWidth = deWidgetWidth + 4.
      when "." then deWidgetWidth = deWidgetWidth + 4.
      when "9" then deWidgetWidth = deWidgetWidth + 10.
    end case.
  end.
  
  /* set width of the widget as per format */
  hWidget:width-pixels = deWidgetWidth.      
        
  RETURN cNumberFormat.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

