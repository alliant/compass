&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wUnpostedDeposit.w

  Description: Window for AR Unposted Deposits

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Shefali

  Created: 01.30.2020
  
  @Modified :
  Date        Name     Description 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */

{lib/ar-def.i}
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/winshowscrollbars.i}
{lib/normalizefileid.i}  /* include file to normalize file number */

{tt/policyaudit.i }      /* Used for get data from db */
{tt/importdata.i }      /* Used for Import data from file */

define variable dColumnWidth       as decimal    no-undo.
define variable iCount             as integer    no-undo.
define variable rwRow              as rowid      no-undo.
define temp-table policy field policyId as character.
define temp-table tpolicy  like policy.
define temp-table ttpolicy like policy.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwpolicy

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES policy

/* Definitions for BROWSE brwpolicy                                  */
&Scoped-define FIELDS-IN-QUERY-brwpolicy policy.policyID "Policy ID"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwpolicy   
&Scoped-define SELF-NAME brwpolicy
&Scoped-define QUERY-STRING-brwpolicy for each policy
&Scoped-define OPEN-QUERY-brwpolicy open query {&SELF-NAME} for each policy.
&Scoped-define TABLES-IN-QUERY-brwpolicy policy
&Scoped-define FIRST-TABLE-IN-QUERY-brwpolicy policy


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwpolicy}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwpolicy bImport bDelete bNew RECT-77 ~
RECT-91 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( iCount as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.


/* Definitions of the field level widgets                               */
DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.67 TOOLTIP "Delete policy".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.67 TOOLTIP "Excel Report".

DEFINE BUTTON bSave  NO-FOCUS
     LABEL "Save" 
     SIZE 7.2 BY 1.67 TOOLTIP "Save the policyl list".

DEFINE BUTTON bImport  NO-FOCUS
     LABEL "Import" 
     SIZE 7.2 BY 1.67 TOOLTIP "Import policy(s)".

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.67 TOOLTIP "Modify policy".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.67 TOOLTIP "New policy".

DEFINE BUTTON bPrelimRpt  NO-FOCUS
     LABEL "PDF" 
     SIZE 7.2 BY 1.67 TOOLTIP "PDF Report".

DEFINE RECTANGLE RECT-77
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 40 BY 2.67.

DEFINE RECTANGLE RECT-91
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17 BY 2.67.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwpolicy FOR 
      policy SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwpolicy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwpolicy C-Win _FREEFORM
  QUERY brwpolicy DISPLAY
      policy.policyID    label   "Policy ID"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 56.2 BY 10.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     brwpolicy AT ROW 4.33 COL 2.6 WIDGET-ID 200
     bExport AT ROW 1.95 COL 43.2 WIDGET-ID 448
     bSave AT ROW 1.95 COL 33.6 WIDGET-ID 450
     bImport AT ROW 1.95 COL 26.2 WIDGET-ID 404 NO-TAB-STOP 
     bDelete AT ROW 1.95 COL 18.8 WIDGET-ID 438 NO-TAB-STOP 
     bModify AT ROW 1.95 COL 11.4 WIDGET-ID 6 NO-TAB-STOP 
     bNew AT ROW 1.95 COL 4 WIDGET-ID 2 NO-TAB-STOP 
     bPrelimRpt AT ROW 1.95 COL 50.8 WIDGET-ID 442
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.24 COL 4 WIDGET-ID 194
     "Output" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 1.24 COL 43.6 WIDGET-ID 446
     RECT-77 AT ROW 1.52 COL 2.6 WIDGET-ID 190
     RECT-91 AT ROW 1.52 COL 42.2 WIDGET-ID 444
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 60.2 BY 14.48 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Policy Audit"
         HEIGHT             = 14.52
         WIDTH              = 59.6
         MAX-HEIGHT         = 34.43
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.43
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwpolicy 1 DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bExport IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bSave IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bModify IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPrelimRpt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwpolicy:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwpolicy:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwpolicy
/* Query rebuild information for BROWSE brwpolicy
     _START_FREEFORM
open query {&SELF-NAME} for each policy.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwpolicy */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Policy Audit */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Policy Audit */
DO:
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Policy Audit */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME DEFAULT-FRAME /* Delete */
DO:
  run deletePolicy in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME DEFAULT-FRAME /* Save */
DO:
  run saveData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bImport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bImport C-Win
ON CHOOSE OF bImport IN FRAME DEFAULT-FRAME /* Import */
DO:
  run importData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME DEFAULT-FRAME /* Modify */
DO:
  run modifyPolicy in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME DEFAULT-FRAME /* New */
DO:
  run newPolicy in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&Scoped-define BROWSE-NAME brwpolicy
&Scoped-define SELF-NAME brwpolicy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwpolicy C-Win
ON ROW-DISPLAY OF brwpolicy IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwpolicy C-Win
ON START-SEARCH OF brwpolicy IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwpolicy C-Win
ON VALUE-CHANGED OF brwpolicy IN FRAME DEFAULT-FRAME
DO:  
  run setWidgetState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow"        anywhere.
subscribe to "refreshCheckAmount" anywhere.

bExport         :load-image             ("images/excel.bmp").
bExport         :load-image-insensitive ("images/excel-i.bmp").

bNew            :load-image            ("images/add.bmp").
bNew            :load-image-insensitive("images/add-i.bmp").

bModify         :load-image            ("images/update.bmp").
bModify         :load-image-insensitive("images/update-i.bmp").

bDelete         :load-image            ("images/delete.bmp").
bDelete         :load-image-insensitive("images/delete-i.bmp"). 

bImport         :load-image            ("images/import.bmp").
bImport         :load-image-insensitive("images/import-i.bmp").

bSave           :load-image            ("images/save.bmp").
bSave           :load-image-insensitive("images/save-i.bmp").

bPrelimRpt      :load-image             ("images/pdf.bmp").
bPrelimRpt      :load-image-insensitive ("images/pdf-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:   

  run enable_UI.
  
  resultschanged(query brwpolicy:num-results).
  /* Enable/disable Actions button based on the data */
  run setWidgetState in this-procedure.
  
  {lib/get-column-width.i &col="'policyId'" &var=dColumnWidth}
        
  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deletePolicy C-Win 
PROCEDURE deletePolicy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.    

  if not available policy 
   then
    return.   
 
  message "Policy will be deleted. Are you sure you want to delete the selected policy?"
      view-as alert-box question buttons yes-no
      title "Delete Policy"
      update std-lo.
      
  if not std-lo
   then 
    return.
 
  for first ttpolicy where ttpolicy.policyId = policy.policyId:
    delete ttpolicy.
  end.  
    
  run filterData in this-procedure.  
  
  apply 'value-changed' to browse brwpolicy.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE brwpolicy bImport bDelete bNew RECT-77 RECT-91 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:
  Parameters:  <none>
  Notes:
------------------------------------------------------------------------------*/
  define var policyIdList as character init "" no-undo.
  
  if query brwpolicy:num-results = 0
   then
    do:
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.

  for each policy no-lock:
    if policyIdList = "" 
     then
       assign policyIdList = policy.policyId.  
     else
       assign policyIdList = policyIdList + "," + policy.policyId.
  end.
  run server/querypolicyaudit.p(input policyIdList,
                               output table policyaudit,
                               output std-lo,
                               output std-ch).
  
  for each policyaudit:
  if policyaudit.policyId = "0" then
   assign  policyaudit.policyId = "".
  if policyaudit.action = 'B' then
     
  end.
  publish "GetReportDir" (output std-ch).

  std-ha = temp-table policyaudit:handle.
  run util/exporttable.p (table-handle std-ha,
                          "policyaudit",
                          "for each policyaudit",
                          "policyId,fileNumber,agentID,agentname,amount,postDate,action,reference,createdby,note",
                          "Policy ID,File Number,Agent ID,Agent Name,Amount,Post Date,Action,Check/Reference,User ID,Note",
                          std-ch,
                          "PolicyAudit"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:
  Parameters:  <none>
  Notes:
------------------------------------------------------------------------------*/
  close query brwpolicy.
  empty temp-table policy.

  do with frame {&frame-name}:
  end.

  define buffer ttpolicy for ttpolicy.
   
  for each ttpolicy:
    create policy.
    buffer-copy ttpolicy to policy.
  end.  
  open query brwpolicy preselect each policy.
  rwRow = rowid (policy).

  run sortData in this-procedure (dataSortBy).
  resultschanged(query brwpolicy:num-results).
  reposition brwpolicy to rowid(rwRow) no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE importData C-Win 
PROCEDURE importData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
 
 publish "SetCurrentValue" ("ImportAction",  "ImportPolicy").
 
 run util/importdialognew.w 
  ("PolicyId",
   "CH",
   "Column 1 is Policy ID",
   "ImportPolicy" /* Internal procedure called to import each record */).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ImportPolicy C-Win 
PROCEDURE ImportPolicy :
/*------------------------------------------------------------------------------
  Purpose:     Callback routine used by the generic Import Dialog
  Parameters:  <none>
  Notes:       
               [1]  - Reference Number
               [2]  - Agent ID
               [3]  - Agent Name
               [4]  - Amount
               [5]  - Check
               [6]  - File Number
               [7]  - Processing Date               
------------------------------------------------------------------------------*/
  define input  parameter table     for importdata.
  define output parameter pSuccess  as  logical   init false.
  define output parameter pErrorMsg as  character init "".

  /*empty temp-table ttpolicy.*/
  empty temp-table tpolicy.
  
  /* Data will be available if called by the select file dialog */
  find first importdata no-error.
  if not available importdata
   then 
     return.
  for each importData:
    /* These fields are mandatory to create payment record for the deposit */
    if importdata.data[1]  = ""
     then
      next.

    create tpolicy.
    assign
      tpolicy.policyId      = importdata.data[1]
      .
  end.

  find first tpolicy no-error.
  if not available tpolicy
   then return.

  iCount = 0. 
  for each tpolicy:
    if can-find(first ttpolicy where ttpolicy.policyId = tpolicy.policyId )
     then
      assign iCount = iCount + 1.
     else 
      do:
        create ttpolicy.
        buffer-copy tpolicy to ttpolicy.
     end.
  end.
  
  if iCount > 0 
   then
    message iCount " duplicate records are found"
            view-as alert-box information buttons ok.
            
  run filterData  in this-procedure.

  pSuccess = true.

  run setWidgetState in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyPolicy C-Win 
PROCEDURE modifyPolicy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define variable roDeposit   as rowid no-undo.

  define buffer ttpolicy for ttpolicy.
  empty temp-table tpolicy.

  if not available policy
   then
    return.

  rwRow = rowid(policy).

  create tpolicy.
  buffer-copy policy to tpolicy.

  run dialogpolicyaudit.w(input-output table tpolicy,
                     output std-lo).
      
  if not std-lo
   then
    return.

  find first tpolicy no-error.
  if not available tpolicy
   then return.
 
  for each tpolicy:
    if can-find(first ttpolicy where ttpolicy.policyId = tpolicy.policyId )
     then 
     do:
        message "Policy already exist with this policyID." tpolicy.policyId
            view-as alert-box information buttons ok.
        return.
     end.
  end.
  
  find first ttpolicy where ttpolicy.policyId = policy.policyId no-error.
  if not avail ttpolicy then
    return.
    
  find first tpolicy no-error.
  if avail tpolicy then
     assign ttpolicy.policyID = tpolicy.policyID no-error.

  run filterData in this-procedure.

  reposition {&browse-name} to rowid rwRow.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newPolicy C-Win 
PROCEDURE newPolicy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define buffer ttpolicy for ttpolicy.
  empty temp-table tpolicy.

  run dialogpolicyaudit.w(input-output table tpolicy,
                      output std-lo).
  if not std-lo
   then
    return.

  find first tpolicy no-error.
  if not available tpolicy
   then return.

  iCount = 0.
  for each tpolicy:
    if can-find(first ttpolicy where ttpolicy.policyId = tpolicy.policyId )
     then 
        assign iCount = iCount + 1.
     else 
      do:
        create ttpolicy.
        buffer-copy tpolicy to ttpolicy.
     end.
  end.
  if iCount > 0 
   then
    message iCount " duplicate records are found"
            view-as alert-box information buttons ok.

  run filterData  in this-procedure.

  find first policy where policy.policyId = ttpolicy.policyId no-error.
  if not available policy
   then
    return.

  reposition {&browse-name} to rowid rowid(policy).

  apply 'value-changed' to browse brwpolicy.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveData C-Win 
PROCEDURE saveData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "GetReportDir" (output std-ch).

  std-ha = temp-table policy:handle.
  run util/exporttable.p (table-handle std-ha,
                          "policy",
                          "for each policy by integer(policy.policyId)",
                          "policyID",
                          "Policy ID",
                          std-ch,
                          "PolicyList-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  assign
    bExport:sensitive          = (query brwpolicy:num-results > 0)
    bSave:sensitive            = (query brwpolicy:num-results > 0)
    bModify:sensitive          = (available policy)
    bDelete:sensitive          = (available policy)
                                  no-error.
         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.

/*   tWhereClause = " by integer(policy.policyId)". */

  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run sortData in this-procedure (dataSortBy).
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 14
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 22
      .
 
 {lib/resize-column.i &col="'PolicyId'" &var=dColumnWidth}
 run ShowScrollBars(frame {&frame-name}:handle, no, no).  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( iCount as int ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  setStatusMessage(string(iCount) + " records").

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

