&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogreferencelookup.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul Sharma

  Created:11/09/20
  
  @Modified:
  Date         Name         Description

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/ar-def.i}
{tt/artran.i}
{tt/artran.i &tableAlias=tartran}
{tt/artran.i &tableAlias=ttartran}

/* Include file to normalize file number */
{lib/normalizefileid.i}

/* Parameters Definitions ---                                           */
define input  parameter ipiArtranID     as  integer   no-undo.
define input  parameter ipcEntity       as  character no-undo.
define input  parameter ipcEntityID     as  character no-undo.
define input  parameter ipcType         as  character no-undo.
define output parameter table           for ttartran.
define output parameter oplSuccess      as  logical   no-undo.

/* Local Variable */
define variable lApplySearchString as logical     no-undo.
define variable cSearchString      as character   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwReference

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES artran

/* Definitions for BROWSE brwReference                                  */
&Scoped-define FIELDS-IN-QUERY-brwReference artran.reference "Reference" artran.fileNumber "File Number" artran.tranDate artran.tranAmt artran.remainingAmt   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwReference   
&Scoped-define SELF-NAME brwReference
&Scoped-define QUERY-STRING-brwReference for each artran
&Scoped-define OPEN-QUERY-brwReference open query {&SELF-NAME} for each artran.
&Scoped-define TABLES-IN-QUERY-brwReference artran
&Scoped-define FIRST-TABLE-IN-QUERY-brwReference artran


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwReference}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fSearch bSearch tgfullyAppliedRef ~
brwReference Btn_OK RECT-91 
&Scoped-Define DISPLAYED-OBJECTS fSearch tgfullyAppliedRef 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bSearch 
     LABEL "Search" 
     SIZE 4.8 BY 1.14 TOOLTIP "Search Locks".

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Select" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 41 BY 1 TOOLTIP "Search Criteria (Code Type, Code, Type, Description)" NO-UNDO.

DEFINE RECTANGLE RECT-91
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.6 BY 2.

DEFINE VARIABLE tgfullyAppliedRef AS LOGICAL INITIAL no 
     LABEL "Include fully applied ref." 
     VIEW-AS TOGGLE-BOX
     SIZE 31.8 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwReference FOR 
      artran SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwReference
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwReference Dialog-Frame _FREEFORM
  QUERY brwReference DISPLAY
      artran.reference  label     "Reference"       format "x(20)"           
artran.fileNumber       label     "File Number"     format "x(30)" width 18 
artran.tranDate         column-label  "Post!Date"   format "99/99/99"
artran.tranAmt          column-label  "Total"       format ">>>,>>>,>>9.99" 
artran.remainingAmt     column-label  "Unapplied"   format "->>,>>>,>>9.99"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 87.4 BY 11.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fSearch AT ROW 1.95 COL 3.6 COLON-ALIGNED NO-LABEL WIDGET-ID 66
     bSearch AT ROW 1.86 COL 46.8 WIDGET-ID 314
     tgfullyAppliedRef AT ROW 2.05 COL 55.8 WIDGET-ID 318
     brwReference AT ROW 3.76 COL 3 WIDGET-ID 300
     Btn_OK AT ROW 15.67 COL 38
     "Search" VIEW-AS TEXT
          SIZE 6.8 BY .62 AT ROW 1.14 COL 4.2 WIDGET-ID 322
     RECT-91 AT ROW 1.48 COL 3 WIDGET-ID 320
     SPACE(1.19) SKIP(13.56)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Select Reference" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwReference tgfullyAppliedRef Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       brwReference:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE
       brwReference:COLUMN-MOVABLE IN FRAME Dialog-Frame         = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwReference
/* Query rebuild information for BROWSE brwReference
     _START_FREEFORM
open query {&SELF-NAME} for each artran.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwReference */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Select Reference */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwReference
&Scoped-define SELF-NAME brwReference
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReference Dialog-Frame
ON DEFAULT-ACTION OF brwReference IN FRAME Dialog-Frame
DO:
  apply "Choose" to Btn_OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReference Dialog-Frame
ON ROW-DISPLAY OF brwReference IN FRAME Dialog-Frame
do:
  {lib/brw-rowDisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReference Dialog-Frame
ON START-SEARCH OF brwReference IN FRAME Dialog-Frame
do:    
  {lib/brw-startSearch.i} 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch Dialog-Frame
ON CHOOSE OF bSearch IN FRAME Dialog-Frame /* Search */
DO:
  lApplySearchString = true.
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Select */
DO:
  run returnRefData in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch Dialog-Frame
ON ENTRY OF fSearch IN FRAME Dialog-Frame
DO:
  /* store the previous value of search string on which search is applied */
  cSearchString = fSearch:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch Dialog-Frame
ON RETURN OF fSearch IN FRAME Dialog-Frame
DO:
  apply 'choose' to bSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch Dialog-Frame
ON VALUE-CHANGED OF fSearch IN FRAME Dialog-Frame
DO:
  /* as soon as we change the search string, we track that string 
  is not applied and change the status in taskbar */
  lApplySearchString = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgfullyAppliedRef
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgfullyAppliedRef Dialog-Frame
ON VALUE-CHANGED OF tgfullyAppliedRef IN FRAME Dialog-Frame /* Include fully applied ref. */
DO:
  assign
      cSearchString      = ""
      lApplySearchString = false
      .  
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */
{lib/brw-main.i}

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bSearch:load-image("images/s-magnifier.bmp").
bSearch:load-image-insensitive("images/s-magnifier-i.bmp").

assign
    frame Dialog-Frame:title = (if ipcType = {&Payment} then "Select Payment"                 else "Select Credit")
    tgfullyAppliedRef:label  = (if ipcType = {&Payment} then "Include fully applied payments" else "Include fully applied credits")
    .
    
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
        
  run getData in this-procedure.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fSearch tgfullyAppliedRef 
      WITH FRAME Dialog-Frame.
  ENABLE fSearch bSearch tgfullyAppliedRef brwReference Btn_OK RECT-91 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData Dialog-Frame 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tartran for tartran.
  
  do with frame {&frame-name}:
  end.

  /* Clear the browser */
  close query brwReference.
  empty temp-table artran.
  
  /* if search string is already applied then filter on the basis of what is
     present inside fsearch fill-in */
  if lApplySearchString 
   then
    cSearchString = trim(fSearch:input-value).
  /* if search string is changed but not applied then restrore the fSearch fill-in
     to previous applied serach string and filter on the basis of what is
     present inside fsearch fill-in */
  else
    fSearch:screen-value = cSearchString.
    
  for each tartran where (if cSearchString <> "" then tartran.reference matches ("*" + cSearchString + "*")
                          else tartran.reference = tartran.reference)   or
                         (if cSearchString <> "" then tartran.fileID    matches ("*" + normalizeFileID(cSearchString) + "*")
                          else tartran.fileID = tartran.fileID):
    
    create artran.
    buffer-copy tartran to artran.
  end.

  open query brwReference preselect each artran by artran.tranDate desc.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData Dialog-Frame 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  do with frame {&frame-name}:
  end.
     
  run server/getpostedtransactions.p (input  0,                        /* arTranID */
                                      input  ipcEntity, 
                                      input  ipcEntityID,
                                      input  ipcType,                  /* (C)redit,(D)ebit,(F)ile */
                                      input  "",                       /* Search String */
                                      input tgfullyAppliedRef:checked, /* IncludeAll */
                                      input  ?,                        /* FromPostDate */
                                      input  ?,                        /* ToPostDate */
                                      output table tartran,
                                      output std-lo,
                                      output std-ch).
  
  if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box error buttons ok.
      return.
    end.
    
  run filterData in this-procedure.
  
  find first artran where artran.artranID = ipiArtranID no-error.

  if available artran
   then      
    reposition brwReference to rowid rowid(artran) no-error. 
  
  apply 'value-changed' to brwReference. 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE returnRefData Dialog-Frame 
PROCEDURE returnRefData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table ttartran.
  
  if not available artran
   then return.
  
  create ttartran.
  buffer-copy artran to ttartran.
  oplSuccess = yes.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData Dialog-Frame 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
 
  tWhereClause = " by artran.tranDate ".
  
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

