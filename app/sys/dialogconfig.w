&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fDialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fDialog 
/*------------------------------------------------------------------------
 File: dialogconfig.w

 Description: Create a configuration file to select states at beginning
 
 Input Parameters:
     <none>
 
 Output Parameters:
     <none>
 
 Author: Rahul
 
 Created:03/01/20189


------------------------------------------------------------------------*/
{lib/std-def.i}

&scoped-define  addAllAction     "addAllAction"
&scoped-define  addAction        "addAction"
&scoped-define  removeAllAction  "removeAllAction"
&scoped-define  removeAction     "removeAction"

define variable cTrackReportDir      as character no-undo.
define variable lTrackConfirmExit    as logical   no-undo.
define variable iTrackPeriod         as integer   no-undo.
define variable iServerLines         as integer   no-undo.
define variable lLoadLaunchExit      as logical   no-undo.
define variable cSelectedAgents      as character no-undo.

/* Local Variable Definitions ---                                       */
define variable cAvailableActions    as character no-undo.
define variable cSelectedActions     as character no-undo.
define variable cList                as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fDialog

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS slAvailableActions slSelectedActions bAddAll ~
fReportDir tReportsSearch bRemoveAll fDays tlaunch flServerLines ~
tConfirmExit Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS slAvailableActions slSelectedActions ~
fReportDir fDays tlaunch flServerLines tConfirmExit 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddAction  NO-FOCUS
     LABEL "-->" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a action".

DEFINE BUTTON bAddAll  NO-FOCUS
     LABEL "ALL-->" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add all actions".

DEFINE BUTTON bDeleteAction  NO-FOCUS
     LABEL "<--" 
     SIZE 4.8 BY 1.14 TOOLTIP "Remove a action".

DEFINE BUTTON bRemoveAll  NO-FOCUS
     LABEL "<-- ALL" 
     SIZE 4.8 BY 1.14 TOOLTIP "Remove all action".

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON tReportsSearch 
     LABEL "..." 
     SIZE 4 BY .95.

DEFINE VARIABLE fDays AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Number of days" 
     VIEW-AS FILL-IN 
     SIZE 6.4 BY 1 NO-UNDO.

DEFINE VARIABLE flServerLines AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Server Log Lines" 
     VIEW-AS FILL-IN 
     SIZE 6.4 BY 1 TOOLTIP "Lines to fetch from server logs" NO-UNDO.

DEFINE VARIABLE fReportDir AS CHARACTER FORMAT "X(200)":U 
     LABEL "Temporary Directory" 
     VIEW-AS FILL-IN 
     SIZE 52 BY 1 TOOLTIP "Enter the default directory to save files; blank is session working directory" NO-UNDO.

DEFINE RECTANGLE RECT-31
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 80 BY 16.38.

DEFINE VARIABLE slAvailableActions AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SCROLLBAR-VERTICAL 
     SIZE 34 BY 7.57 NO-UNDO.

DEFINE VARIABLE slSelectedActions AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SCROLLBAR-VERTICAL 
     SIZE 34 BY 7.57 NO-UNDO.

DEFINE VARIABLE tConfirmExit AS LOGICAL INITIAL no 
     LABEL "Confirm Application Exit" 
     VIEW-AS TOGGLE-BOX
     SIZE 26.4 BY .81 NO-UNDO.

DEFINE VARIABLE tlaunch AS LOGICAL INITIAL no 
     LABEL "Load on Launch" 
     VIEW-AS TOGGLE-BOX
     SIZE 19.4 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fDialog
     bAddAction AT ROW 5.57 COL 40.6 WIDGET-ID 98
     bDeleteAction AT ROW 6.76 COL 40.6 WIDGET-ID 100
     bRemoveAll AT ROW 7.95 COL 40.6 WIDGET-ID 102
     bAddAll AT ROW 4.38 COL 40.6 WIDGET-ID 96
     slAvailableActions AT ROW 2.95 COL 5 NO-LABEL WIDGET-ID 92
     slSelectedActions AT ROW 2.95 COL 47 NO-LABEL WIDGET-ID 78
     fReportDir AT ROW 11 COL 22.8 COLON-ALIGNED WIDGET-ID 2
     tReportsSearch AT ROW 11 COL 77 WIDGET-ID 30
     fDays AT ROW 12.38 COL 22.8 COLON-ALIGNED WIDGET-ID 146
     tlaunch AT ROW 12.48 COL 33 WIDGET-ID 140
     flServerLines AT ROW 13.76 COL 22.8 COLON-ALIGNED WIDGET-ID 144
     tConfirmExit AT ROW 15.05 COL 24.8 WIDGET-ID 132
     Btn_OK AT ROW 16.24 COL 26.4 WIDGET-ID 134
     Btn_Cancel AT ROW 16.24 COL 43.8 WIDGET-ID 136
     "Included Actions" VIEW-AS TEXT
          SIZE 19.8 BY .62 AT ROW 2.05 COL 5.2 WIDGET-ID 94
          FONT 6
     "Excluded Actions" VIEW-AS TEXT
          SIZE 19.8 BY .62 AT ROW 2.05 COL 46.8 WIDGET-ID 126
          FONT 6
     "Options" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 1.19 COL 4.6 WIDGET-ID 70
          FONT 6
     RECT-31 AT ROW 1.43 COL 3 WIDGET-ID 68
     SPACE(1.39) SKIP(0.13)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Configuration"
         DEFAULT-BUTTON Btn_OK WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fDialog
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fDialog:SCROLLABLE       = FALSE
       FRAME fDialog:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bAddAction IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDeleteAction IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON Btn_OK IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-31 IN FRAME fDialog
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fDialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDialog fDialog
ON WINDOW-CLOSE OF FRAME fDialog /* Configuration */
DO:
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddAction fDialog
ON CHOOSE OF bAddAction IN FRAME fDialog /* --> */
do:
  run addRemoveAction  in this-procedure(input {&addAction}).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddAll fDialog
ON CHOOSE OF bAddAll IN FRAME fDialog /* ALL--> */
do:
  run addRemoveAction in this-procedure(input {&addAllAction}).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteAction fDialog
ON CHOOSE OF bDeleteAction IN FRAME fDialog /* <-- */
do:
  run addRemoveAction in this-procedure(input {&removeAction}).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRemoveAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRemoveAll fDialog
ON CHOOSE OF bRemoveAll IN FRAME fDialog /* <-- ALL */
do:
  run addRemoveAction  in this-procedure(input {&removeAllAction}).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK fDialog
ON CHOOSE OF Btn_OK IN FRAME fDialog /* OK */
DO:
  if fDays:input-value > 30
   then
    do:
      message "The days are greater than 30 days, the query will be slow. Are you sure you want to continue?" 
        view-as alert-box warning buttons yes-no update std-lo.
  
      if not std-lo
       then
        return no-apply.
   end.
   
  if fDays:input-value > 365
   then
    do:
      message "Days cannot be more than 365 days." 
        view-as alert-box error buttons ok.
  
      return no-apply.
    end.

  run saveConfiguration in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fDays
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDays fDialog
ON VALUE-CHANGED OF fDays IN FRAME fDialog /* Number of days */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flServerLines
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flServerLines fDialog
ON VALUE-CHANGED OF flServerLines IN FRAME fDialog /* Server Log Lines */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fReportDir
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fReportDir fDialog
ON VALUE-CHANGED OF fReportDir IN FRAME fDialog /* Temporary Directory */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME slAvailableActions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL slAvailableActions fDialog
ON VALUE-CHANGED OF slAvailableActions IN FRAME fDialog
DO:
   assign 
     bAddAction      :sensitive = true
     bDeleteAction   :sensitive = false
     bremoveAll      :sensitive = false
     baddall         :sensitive = true
   .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME slSelectedActions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL slSelectedActions fDialog
ON VALUE-CHANGED OF slSelectedActions IN FRAME fDialog
DO:
   assign 
     bDeleteAction   :sensitive = true
     bAddAction      :sensitive = false
     bAddAll         :sensitive = false
     bRemoveAll      :sensitive = true
     .   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tConfirmExit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tConfirmExit fDialog
ON VALUE-CHANGED OF tConfirmExit IN FRAME fDialog /* Confirm Application Exit */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tlaunch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tlaunch fDialog
ON VALUE-CHANGED OF tlaunch IN FRAME fDialog /* Load on Launch */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tReportsSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tReportsSearch fDialog
ON CHOOSE OF tReportsSearch IN FRAME fDialog /* ... */
DO:
  run getReportDirectory in this-procedure.
  run enableDisableSave  in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fDialog 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bAddAction   :load-image("images/s-next.bmp").
bDeleteAction:load-image("images/s-previous.bmp").
bAddAll      :load-image("images/s-nextpg.bmp").
bremoveall   :load-image("images/s-previouspg.bmp").

/* Getting the action list to fill selection list */
publish "getALLSysActionList" (input "," ,
                               output cList,
                               output std-lo,
                               output std-ch).
if not std-lo
 then
  do:
    message std-ch
        view-as alert-box info buttons ok.
    return.
  end.

slAvailableActions:list-items = if cList <> "" and cList <> "?" then cList else ?.

/* Set default configuration data in fields.  */
publish "GetReportDir"       (output fReportDir).
publish "GetConfirmExit"     (output tConfirmExit).
publish "GetLoadLaunch"      (output tlaunch).
publish "GetDefaultDays"     (output fdays).
publish "getDefaultLines"    (output flServerLines). 
publish "getExcludedActions" (output std-ch).

slSelectedActions:list-items = if std-ch <> "" and std-ch <> "?" then std-ch else ?.

assign 
  cTrackReportDir    = fReportDir
  lTrackConfirmExit  = tConfirmExit
  iTrackPeriod       = fdays
  iServerLines       = flServerLines
  lLoadLaunchExit    = tlaunch
  cSelectedAgents    = std-ch
  .

do std-in = 1 to num-entries(std-ch):
  slAvailableActions:delete(entry(std-in, std-ch)).
end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN enable_UI.

  do with frame {&frame-name}:

    if fReportDir:screen-value > "" 
     then
      do:
        file-info:file-name = fReportDir:screen-value.
        if file-info:full-pathname = ?
         or index(file-info:file-type, "D") = 0 
         or index(file-info:file-type, "W") = 0
         then
          do: 
            message "Invalid Directory"
             view-as alert-box error buttons ok.

            undo MAIN-BLOCK, retry MAIN-BLOCK.
          end.
         else 
          fReportDir:screen-value = file-info:full-pathname.
      end.
  end.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
end.

RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addRemoveAction fDialog 
PROCEDURE addRemoveAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcAction as character no-undo.
  
  do with frame {&frame-name}:
  end.

  case ipcAction:

    when {&addAction} then          /* Add a Action to the Search options */
     do:
       if slAvailableActions:input-value = ?
        then 
         return.

       slSelectedActions:add-last(slAvailableActions:input-value).

       slSelectedActions:list-items = trim(slSelectedActions:list-items,",").      
       slAvailableActions:delete(slAvailableActions:input-value).      
     end.
         
    when {&addAllAction} then       /* Add all Actions to the Search options */
     assign  
       cAvailableActions             = if slAvailableActions:list-items = ? or slAvailableActions:list-items = "?" then "" else slAvailableActions:list-items
       cSelectedActions              = if slSelectedActions:list-items  = ? or slSelectedActions:list-items  = "?" then "" else slSelectedActions:list-items
       slSelectedActions:list-items  = trim(cAvailableActions + "," + cSelectedActions,",")
       slAvailableActions:list-items = ?
       cAvailableActions             = ""
       cSelectedActions              = ""
       .

    when {&removeAction} then       /* Remove a Action from the Search options */
     do:
        if slSelectedActions:input-value = ? 
         then
          return.

        slAvailableActions:add-last(slSelectedActions:input-value).
        slAvailableActions:list-items = trim(slAvailableActions:list-items,",").       
        slSelectedActions:delete(slSelectedActions:input-value).
     end.
 
    when {&removeAllAction}         /* Remove all Actions from the Search options */
     then
      do:
         assign  
           cAvailableActions             = if slAvailableActions:list-items = ? or slAvailableActions:list-items = "?" then "" else slAvailableActions:list-items
           cSelectedActions              = if slSelectedActions:list-items  = ? or slSelectedActions:list-items = "?" then "" else slSelectedActions:list-items
           slAvailableActions:list-items = trim(cAvailableActions + "," + cSelectedActions,",")
           slSelectedActions:list-items  = ?
           cAvailableActions             = ""
           cSelectedActions              = ""
           .
      end.
    otherwise.
  end case.

  run setWidgetState    in this-procedure.
  run enableDisableSave in this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fDialog  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fDialog.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave fDialog 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* Enabling the save button only when user made any modification on this dialog after its loading. */
  Btn_OK:sensitive = not (lTrackConfirmExit     =    tConfirmExit:checked      and
                          cTrackReportDir       =    fReportDir:input-value    and
                          iTrackPeriod          =    fdays:input-value         and
                          flServerLines         =    flServerLines:input-value and
                          lLoadLaunchExit       =    tlaunch:checked           and 
                          cSelectedAgents       =    slSelectedActions:list-items) no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fDialog  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY slAvailableActions slSelectedActions fReportDir fDays tlaunch 
          flServerLines tConfirmExit 
      WITH FRAME fDialog.
  ENABLE slAvailableActions slSelectedActions bAddAll fReportDir tReportsSearch 
         bRemoveAll fDays tlaunch flServerLines tConfirmExit Btn_Cancel 
      WITH FRAME fDialog.
  VIEW FRAME fDialog.
  {&OPEN-BROWSERS-IN-QUERY-fDialog}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getReportDirectory fDialog 
PROCEDURE getReportDirectory :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name} :
  end.

  std-ch = fReportDir:input-value.
  
  if fReportDir:input-value > "" 
   then
    system-dialog get-dir std-ch initial-dir std-ch. 
   else
    system-dialog get-dir std-ch.
  
  if std-ch > "" and std-ch <> ? 
   then
    fReportDir:screen-value = std-ch.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveConfiguration fDialog 
PROCEDURE saveConfiguration :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* Set fields value in configuration. */
  publish "SetReportDir"   (fReportDir:input-value).
  publish "SetConfirmExit" (tConfirmExit:checked).  
  publish "SetLoadLaunch"  (tlaunch:checked).
  publish "SetDefaultDays" (fdays:input-value).

  publish "setExcludedActions" (slSelectedActions:list-items).
  
  /* Sets default value to the no. of lines fill-in in the server log window */
  publish "setDefaultLines" (flServerLines:screen-value).

  /* Refresh main Sys screen if there is any change in days. */
  if iTrackPeriod         ne fdays:input-value or
     cSelectedAgents      ne slSelectedActions:list-items
   then
    publish "periodModified" (fdays:input-value).  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData fDialog 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  cSelectedAgents =  std-ch.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState fDialog 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* Enable/Disable the Add/Add All/Remove/Remove All buttons based on 
     the list items in their respective selection lists.*/
  assign
    bAddAction:sensitive    = slAvailableActions:input-value <> ?
    bRemoveAll:sensitive    = slSelectedActions:num-items > 1
    bAddAll:sensitive       = slAvailableActions:num-items > 0
    bDeleteAction:sensitive = slSelectedActions:num-items > 1 and slSelectedActions:input-value <> ? 
    .
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

