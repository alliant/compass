&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogsysnotification.p

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gurvindar Singh

  Created:10/05/18
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{tt/sysnotification.i}
{lib/std-def.i}
{lib/sys-def.i}

/* Parameters Definitions ---                                           */
define input parameter table for sysnotification.

/* Local Variable Definitions ---                                       */

define variable cStatus                as char      no-undo.
define variable cNotificationStatus    as char      no-undo.

define variable c                      as handle    no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS flUID flEntity flEntityID flStatus ~
flSourceType flNotificationStatus flSourceID edNotification flCreateDate ~
flDismissedDate 
&Scoped-Define DISPLAYED-OBJECTS flUID flEntity flEntityID flStatus ~
flSourceType flNotificationStatus flSourceID edNotification flCreateDate ~
flDismissedDate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD updateNotificationStatus Dialog-Frame 
FUNCTION updateNotificationStatus RETURNS CHARACTER
  (cNotificationStatus as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD updateStatus Dialog-Frame 
FUNCTION updateStatus RETURNS CHARACTER
  (cStatus as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE VARIABLE edNotification AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 58 BY 3.38 NO-UNDO.

DEFINE VARIABLE flCreateDate AS CHARACTER FORMAT "X(256)":U 
     LABEL "Created" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE flDismissedDate AS CHARACTER FORMAT "X(256)":U 
     LABEL "Dismissed" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE flEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity Type" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE flEntityID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity ID" 
     VIEW-AS FILL-IN 
     SIZE 58 BY 1 NO-UNDO.

DEFINE VARIABLE flNotificationStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Notification Status" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE flSourceID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Source ID" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE flSourceType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Source Type" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE flStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE flUID AS CHARACTER FORMAT "X(256)":U 
     LABEL "User ID" 
     VIEW-AS FILL-IN 
     SIZE 58 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     flUID AT ROW 1.71 COL 20 COLON-ALIGNED WIDGET-ID 2 NO-TAB-STOP 
     flEntity AT ROW 2.91 COL 20 COLON-ALIGNED WIDGET-ID 20 NO-TAB-STOP 
     flEntityID AT ROW 4.1 COL 20 COLON-ALIGNED WIDGET-ID 42 NO-TAB-STOP 
     flStatus AT ROW 5.29 COL 20 COLON-ALIGNED WIDGET-ID 38 NO-TAB-STOP 
     flSourceType AT ROW 5.29 COL 62 COLON-ALIGNED WIDGET-ID 16 NO-TAB-STOP 
     flNotificationStatus AT ROW 6.48 COL 20 COLON-ALIGNED WIDGET-ID 6 NO-TAB-STOP 
     flSourceID AT ROW 6.48 COL 62 COLON-ALIGNED WIDGET-ID 32 NO-TAB-STOP 
     edNotification AT ROW 7.71 COL 22 NO-LABEL WIDGET-ID 40 NO-TAB-STOP 
     flCreateDate AT ROW 11.38 COL 20 COLON-ALIGNED WIDGET-ID 36 NO-TAB-STOP 
     flDismissedDate AT ROW 11.38 COL 56 COLON-ALIGNED WIDGET-ID 34 NO-TAB-STOP 
     "Notification:" VIEW-AS TEXT
          SIZE 11.8 BY .62 AT ROW 7.67 COL 10 WIDGET-ID 30
     SPACE(77.39) SKIP(4.80)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "System Notification" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       edNotification:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flCreateDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flDismissedDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flEntity:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flEntityID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flNotificationStatus:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flSourceID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flSourceType:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flStatus:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flUID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* System Notification */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  run enable_UI.
  
  /* sets default values to the widgets */
  run setData.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.

RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flUID flEntity flEntityID flStatus flSourceType flNotificationStatus 
          flSourceID edNotification flCreateDate flDismissedDate 
      WITH FRAME Dialog-Frame.
  ENABLE flUID flEntity flEntityID flStatus flSourceType flNotificationStatus 
         flSourceID edNotification flCreateDate flDismissedDate 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData Dialog-Frame 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  frame dialog-frame:title = "View System Notification".

  find first sysnotification no-error.
  if not available sysnotification 
   then
    return.

  assign 
      
      flEntity:screen-value                 = sysnotification.entityType
      flEntityID:screen-value               = sysnotification.entityID
      flUID:screen-value                    = sysnotification.uid
      flStatus:screen-value                 = updateStatus(sysnotification.stat)
      flSourceType:screen-value             = sysnotification.sourceType
      edNotification:screen-value           = sysnotification.notification 
      flNotificationStatus:screen-value     = updateNotificationStatus(sysnotification.notificationStat)
      flSourceID:screen-value               = sysnotification.sourceID 
      flCreateDate:screen-value             = if sysnotification.createDate    <> ? then string(datetime(sysnotification.createDate))    else ""
      flDismissedDate:screen-value          = if sysnotification.dismissedDate <> ? then string(datetime(sysnotification.dismissedDate)) else ""
      .     
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION updateNotificationStatus Dialog-Frame 
FUNCTION updateNotificationStatus RETURNS CHARACTER
  (cNotificationStatus as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  c = flNotificationStatus:handle.
  
  if cNotificationStatus = {&Yellow}
   then 
    do:
        c:bgcolor = 14. 
        return {&YellowValue}.
    end.
   else if cNotificationStatus = {&Red}
   then 
    do:
         c:bgcolor = 12.
        return {&RedValue}.
    end.
   else if cNotificationStatus = {&Green}
   then 
    do:
        c:bgcolor = 10.
        return {&GreenValue}.
    end.
  RETURN "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION updateStatus Dialog-Frame 
FUNCTION updateStatus RETURNS CHARACTER
  (cStatus as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

   if cStatus = {&New}
   then 
    do:
        return {&NewValue}.
    end.
   else if cStatus = {&Supress}
   then 
    do:
        return {&SupressValue}.
    end.
   else if cStatus = {&Dismiss}
   then 
    do:
        return {&DismissValue}.
    end.

  RETURN "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

