&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogsysaction.w

  Description: Create and Edit SystemAction

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gurvindar singh

  Created: 08.22.2018
  @modified
  09/02/21 - MK - updated for framework changes.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/winlaunch.i}
{lib/sys-def.i}

{tt/sysaction.i}
{tt/sysuser.i}
{tt/sysrole.i}

/* Parameters Definitions ---                                           */
 define input  parameter  ipcActionType  as  character no-undo.
 define input  parameter  table          for sysaction.
 define output parameter  opAction       as  character no-undo.
 define output parameter  lSuccess       as  logical   no-undo.

/* Local Variable Definitions ---                                       */
 define variable availableCards as character no-undo.
 define variable selectedCards  as character no-undo.

 /* Tracking the change */
 define variable cTrackAction         as character no-undo.
 define variable cTrackDesc           as character no-undo.
 define variable cTrackProgex         as character no-undo.
 define variable cTrackActive         as logical   no-undo.
 define variable cTrackAny            as logical   no-undo.
 define variable cTrackSecure         as logical   no-undo.
 define variable cTracklog            as logical   no-undo.
 define variable cTrackAudit          as logical   no-undo.
 define variable cTrackEvent          as logical   no-undo.
 define variable cTrackSend           as character no-undo.
 
 define variable cTrackEmailSuccess   as character no-undo.
 define variable cTrackEmailFail      as character no-undo.
 define variable cTrackComments       as character no-undo.
 define variable cTrackSelectedRoles  as character no-undo.
 define variable cTrackSelectedUser   as character no-undo.
 define variable cTrackIP             as character no-undo.
 
 define variable cTrackQueueable      as logical   no-undo.
 define variable cTrackDestination    as logical   no-undo.
 define variable cTrackName           as character no-undo.
 define variable cTrackSubject        as character no-undo.
 define variable cTrackProgQueue      as character no-undo.
 define variable cTrackHtmlEmail      as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bAddAll fAction fDesc fprogexec tActive ~
tAnonymous tsecure tLog tAudit tevent radioEmails femailSuccess ~
femailFailure tQueueable fprogQueue tDestination fName fSubject fhtmlEmail ~
slAvailableRoles bRemoveAll slSelectedRoles slavailbleUsers slSelectedUsers ~
faddr createDate eComments Btn_OK Btn_Cancel RECT-68 RECT-69 RECT-70 ~
RECT-71 
&Scoped-Define DISPLAYED-OBJECTS fAction fDesc fprogexec tActive tAnonymous ~
tsecure tLog tAudit tevent radioEmails femailSuccess femailFailure ~
tQueueable fprogQueue tDestination fName fSubject fhtmlEmail ~
slAvailableRoles slSelectedRoles slavailbleUsers slSelectedUsers faddr ~
createDate eComments 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddAll  NO-FOCUS
     LABEL "ALL-->" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add all Roles to the Action".

DEFINE BUTTON bAddRole  NO-FOCUS
     LABEL "-->" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a Role".

DEFINE BUTTON bAdduser  NO-FOCUS
     LABEL "-->" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a User".

DEFINE BUTTON bDeleteRole  NO-FOCUS
     LABEL "<--" 
     SIZE 4.8 BY 1.14 TOOLTIP "Remove a Role".

DEFINE BUTTON bDeleteUser  NO-FOCUS
     LABEL "<--" 
     SIZE 4.8 BY 1.14 TOOLTIP "Remove a User".

DEFINE BUTTON bEditEmailBody  NO-FOCUS
     LABEL "Edit" 
     SIZE 4.8 BY 1.1 TOOLTIP "Modify Email Body".

DEFINE BUTTON bRemoveAll  NO-FOCUS
     LABEL "<-- ALL" 
     SIZE 4.8 BY 1.14 TOOLTIP "Remove All Roles from the Action".

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE eComments AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 70.8 BY 2.48 TOOLTIP "Comments" NO-UNDO.

DEFINE VARIABLE faddr AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 70.8 BY 1.91 TOOLTIP "Comma delimited list of client IPs addresses" NO-UNDO.

DEFINE VARIABLE fDesc AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 59.8 BY 2.14 TOOLTIP "Description" NO-UNDO.

DEFINE VARIABLE createDate AS DATE FORMAT "99/99/99":U 
     LABEL "Create Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fAction AS CHARACTER FORMAT "X(256)":U 
     LABEL "Action" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 31.8 BY 1 NO-UNDO.

DEFINE VARIABLE femailFailure AS CHARACTER FORMAT "X(256)":U 
     LABEL "Failure Address" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 57 BY 1 TOOLTIP "Comma delimited list of failure email address" DROP-TARGET NO-UNDO.

DEFINE VARIABLE femailSuccess AS CHARACTER FORMAT "X(256)":U 
     LABEL "Success Address" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 57 BY 1 TOOLTIP "Comma delimited list of success email addresses" NO-UNDO.

DEFINE VARIABLE fhtmlEmail AS CHARACTER FORMAT "X(256)":U 
     LABEL "Email Body" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 52 BY 1 TOOLTIP "Email Body" NO-UNDO.

DEFINE VARIABLE fName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Display Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 57 BY 1 TOOLTIP "Action Name" NO-UNDO.

DEFINE VARIABLE fprogexec AS CHARACTER FORMAT "X(256)":U 
     LABEL "Program Execution" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 59.8 BY 1 TOOLTIP "Program Execution" NO-UNDO.

DEFINE VARIABLE fprogQueue AS CHARACTER FORMAT "X(256)":U 
     LABEL "Program Queue" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 54 BY 1 TOOLTIP "Program Queue" NO-UNDO.

DEFINE VARIABLE fSubject AS CHARACTER FORMAT "X(256)":U 
     LABEL "Email Subject" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 57 BY 1 TOOLTIP "Email Subject" NO-UNDO.

DEFINE VARIABLE radioEmails AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Success", "S",
"Failure", "F",
"Both", "B",
"None", "N"
     SIZE 46.2 BY .95 NO-UNDO.

DEFINE RECTANGLE RECT-68
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 85 BY 15.62.

DEFINE RECTANGLE RECT-69
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 78 BY 4.14.

DEFINE RECTANGLE RECT-70
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 78 BY 5.29.

DEFINE RECTANGLE RECT-71
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 78 BY 2.62.

DEFINE VARIABLE slAvailableRoles AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SCROLLBAR-VERTICAL 
     SIZE 29.8 BY 5.19 NO-UNDO.

DEFINE VARIABLE slavailbleUsers AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SCROLLBAR-VERTICAL 
     SIZE 29.8 BY 5.19 NO-UNDO.

DEFINE VARIABLE slSelectedRoles AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SCROLLBAR-VERTICAL 
     SIZE 29.8 BY 5.19 NO-UNDO.

DEFINE VARIABLE slSelectedUsers AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SCROLLBAR-VERTICAL 
     SIZE 29.8 BY 5.19 NO-UNDO.

DEFINE VARIABLE tActive AS LOGICAL INITIAL no 
     LABEL "Active" 
     VIEW-AS TOGGLE-BOX
     SIZE 10 BY .81 NO-UNDO.

DEFINE VARIABLE tAnonymous AS LOGICAL INITIAL no 
     LABEL "Anonymous" 
     VIEW-AS TOGGLE-BOX
     SIZE 14.2 BY .81 NO-UNDO.

DEFINE VARIABLE tAudit AS LOGICAL INITIAL no 
     LABEL "Audit" 
     VIEW-AS TOGGLE-BOX
     SIZE 9.8 BY .81 NO-UNDO.

DEFINE VARIABLE tDestination AS LOGICAL INITIAL no 
     LABEL "Has Output" 
     VIEW-AS TOGGLE-BOX
     SIZE 15 BY .81 NO-UNDO.

DEFINE VARIABLE tevent AS LOGICAL INITIAL no 
     LABEL "Event Enabled" 
     VIEW-AS TOGGLE-BOX
     SIZE 19 BY .81 NO-UNDO.

DEFINE VARIABLE tLog AS LOGICAL INITIAL no 
     LABEL "Log" 
     VIEW-AS TOGGLE-BOX
     SIZE 7.8 BY .81 NO-UNDO.

DEFINE VARIABLE tQueueable AS LOGICAL INITIAL no 
     LABEL "Queueable" 
     VIEW-AS TOGGLE-BOX
     SIZE 15 BY .81 NO-UNDO.

DEFINE VARIABLE tsecure AS LOGICAL INITIAL no 
     LABEL "Secure" 
     VIEW-AS TOGGLE-BOX
     SIZE 11.2 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bEditEmailBody AT ROW 19.43 COL 74.8 WIDGET-ID 172 NO-TAB-STOP 
     bAddAll AT ROW 2.86 COL 127.8 WIDGET-ID 96
     fAction AT ROW 1.33 COL 19.2 COLON-ALIGNED WIDGET-ID 2
     fDesc AT ROW 2.52 COL 21.2 NO-LABEL WIDGET-ID 72
     fprogexec AT ROW 4.86 COL 19.2 COLON-ALIGNED WIDGET-ID 24
     tActive AT ROW 5.91 COL 21.2 WIDGET-ID 10
     bAddRole AT ROW 4.05 COL 127.8 WIDGET-ID 98
     tAnonymous AT ROW 5.91 COL 37.2 WIDGET-ID 56
     tsecure AT ROW 5.91 COL 55 WIDGET-ID 62
     tLog AT ROW 6.81 COL 21.2 WIDGET-ID 64
     tAudit AT ROW 6.81 COL 37.2 WIDGET-ID 66
     tevent AT ROW 6.81 COL 55 WIDGET-ID 68
     radioEmails AT ROW 8.38 COL 22.4 NO-LABEL WIDGET-ID 112
     bAdduser AT ROW 10.38 COL 127.8 WIDGET-ID 106
     femailSuccess AT ROW 9.52 COL 20.4 COLON-ALIGNED WIDGET-ID 48
     femailFailure AT ROW 10.71 COL 20.4 COLON-ALIGNED WIDGET-ID 46
     tQueueable AT ROW 12.86 COL 22.6 WIDGET-ID 162
     bDeleteRole AT ROW 5.24 COL 127.8 WIDGET-ID 100
     fprogQueue AT ROW 13.86 COL 20.6 COLON-ALIGNED WIDGET-ID 160
     tDestination AT ROW 16.05 COL 23 WIDGET-ID 154
     fName AT ROW 17.1 COL 20.4 COLON-ALIGNED WIDGET-ID 148
     fSubject AT ROW 18.29 COL 20.4 COLON-ALIGNED WIDGET-ID 150
     fhtmlEmail AT ROW 19.48 COL 20.4 COLON-ALIGNED WIDGET-ID 146
     bDeleteUser AT ROW 11.57 COL 127.8 WIDGET-ID 108
     slAvailableRoles AT ROW 2.62 COL 94.8 NO-LABEL WIDGET-ID 78
     bRemoveAll AT ROW 6.43 COL 127.8 WIDGET-ID 102
     slSelectedRoles AT ROW 2.62 COL 135.8 NO-LABEL WIDGET-ID 92
     slavailbleUsers AT ROW 9 COL 94.8 NO-LABEL WIDGET-ID 82
     slSelectedUsers AT ROW 8.95 COL 135.8 NO-LABEL WIDGET-ID 86
     faddr AT ROW 14.67 COL 95 NO-LABEL WIDGET-ID 70
     createDate AT ROW 17.24 COL 93 COLON-ALIGNED WIDGET-ID 30
     eComments AT ROW 18.52 COL 95 NO-LABEL WIDGET-ID 58
     Btn_OK AT ROW 21.43 COL 67
     Btn_Cancel AT ROW 21.43 COL 83.4
     "Comments:" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 18.48 COL 84 WIDGET-ID 60
     "Users:" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 8.95 COL 87.8 WIDGET-ID 84
     "Roles:" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 2.67 COL 87.8 WIDGET-ID 90
     "Output" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 15.43 COL 4 WIDGET-ID 158
     "Available" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 1.81 COL 95 WIDGET-ID 94
          FONT 6
     "Send When:" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 8.43 COL 9.4 WIDGET-ID 116
     "Security" VIEW-AS TEXT
          SIZE 8.6 BY .62 AT ROW 1.1 COL 85.4 WIDGET-ID 120
     "Selected" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 1.81 COL 135.6 WIDGET-ID 126
          FONT 6
     "Available" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 8.24 COL 95 WIDGET-ID 128
          FONT 6
     "Selected" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 8.24 COL 136.4 WIDGET-ID 130
          FONT 6
     "Email Notifications" VIEW-AS TEXT
          SIZE 18 BY .62 AT ROW 7.71 COL 4 WIDGET-ID 124
     "Description:" VIEW-AS TEXT
          SIZE 11.2 BY .62 AT ROW 2.71 COL 10 WIDGET-ID 74
     "System Queue" VIEW-AS TEXT
          SIZE 15 BY .62 AT ROW 12.33 COL 4 WIDGET-ID 168
     "Client IPs:" VIEW-AS TEXT
          SIZE 9.4 BY .62 AT ROW 14.57 COL 85.2 WIDGET-ID 76
     RECT-68 AT ROW 1.33 COL 84.2 WIDGET-ID 118
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME Dialog-Frame
     RECT-69 AT ROW 8.05 COL 3 WIDGET-ID 122
     RECT-70 AT ROW 15.76 COL 3 WIDGET-ID 152
     RECT-71 AT ROW 12.67 COL 3 WIDGET-ID 166
     SPACE(89.19) SKIP(7.56)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Sys Action"
         CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bAddRole IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bAdduser IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDeleteRole IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDeleteUser IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bEditEmailBody IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       createDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Sys Action */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddAll Dialog-Frame
ON CHOOSE OF bAddAll IN FRAME Dialog-Frame /* ALL--> */
do:
  assign 
      availableCards                = if slAvailableRoles:list-items = ? or slAvailableRoles:list-items = "?" then "" else slAvailableRoles:list-items
      selectedCards                 = if slSelectedRoles:list-items  = ? or slSelectedRoles:list-items  = "?" then "" else slSelectedRoles:list-items
      slSelectedRoles:list-items    = trim(availableCards + "," + selectedCards,",")
      slAvailableRoles:list-items   = ?
      bAddAll     :sensitive        = false
      bRemoveAll  :sensitive        = true
      bAddRole    :sensitive        = false
      availableCards                = ""
      selectedCards                 = ""
      .

  run enableDisableSave in this-procedure.

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddRole Dialog-Frame
ON CHOOSE OF bAddRole IN FRAME Dialog-Frame /* --> */
do:
  if slAvailableRoles:input-value = ?
   then
    return.
 
  slSelectedRoles :add-last(slAvailableRoles:input-value).
  slSelectedRoles :list-items = trim(slSelectedRoles:list-items,",").
  slAvailableRoles:delete(slAvailableRoles:input-value).
  bAddRole:sensitive   = false.
  bRemoveAll:sensitive = true .

  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAdduser
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAdduser Dialog-Frame
ON CHOOSE OF bAdduser IN FRAME Dialog-Frame /* --> */
do:
  if slavailbleUsers:input-value = ?
   then 
    return.

  slSelectedusers:add-last(slavailbleUsers:input-value).
  slSelectedusers:list-items = trim(slSelectedusers:list-items,",").
  slavailbleUsers:delete(slavailbleUsers:input-value).
  bAdduser:sensitive = false.

  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteRole Dialog-Frame
ON CHOOSE OF bDeleteRole IN FRAME Dialog-Frame /* <-- */
do:
  if slSelectedRoles:input-value = ? 
   then
    return.

  slAvailableRoles:list-items = trim(slAvailableRoles:list-items).

  if slSelectedRoles:input-value = "!*" 
   then
    slAvailableRoles:add-first(slSelectedRoles:input-value).
   else
    slAvailableRoles:add-last(slSelectedRoles:input-value).

  slAvailableRoles:list-items = trim(slAvailableRoles:list-items,",").
  slSelectedRoles :delete(slSelectedRoles:input-value).
  bDeleteRole     :sensitive  = false.
  bAddAll         :sensitive  = true.
  
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteUser
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteUser Dialog-Frame
ON CHOOSE OF bDeleteUser IN FRAME Dialog-Frame /* <-- */
do:
  if slSelectedUsers:input-value = ?
   then
    return.

  slavailbleUsers:list-items = trim(slavailbleUsers:list-items).

  if slSelectedUsers:input-value = "!*" 
   then
    slavailbleUsers:add-first(slSelectedUsers:input-value).
  else
    slavailbleUsers:add-last(slSelectedUsers:input-value).

  slavailbleUsers:list-items = trim(slavailbleUsers:list-items,",").
  slSelectedUsers:delete(slSelectedUsers:input-value).
  bDeleteuser:sensitive = false.

  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRemoveAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRemoveAll Dialog-Frame
ON CHOOSE OF bRemoveAll IN FRAME Dialog-Frame /* <-- ALL */
do:
  assign 
      availableCards               = if slAvailableRoles:list-items = ? or slAvailableRoles:list-items = "?" then "" else slAvailableRoles:list-items
      selectedCards                = if slSelectedRoles:list-items  = ? or slSelectedRoles:list-items  = "?" then "" else slSelectedRoles:list-items
      slAvailableRoles:list-items  = trim(availableCards + "," + selectedCards,",")
      slSelectedRoles :list-items  = ?
      bRemoveAll  :sensitive       = false
      bAddAll     :sensitive       = true
      bDeleteRole:sensitive        = false
      availableCards               = ""
      selectedCards                = ""
      .
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  run saveSysAction in this-procedure.
  
  if not lSuccess /* on failure of server call dialog should not be closed */
   then 
    return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eComments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eComments Dialog-Frame
ON VALUE-CHANGED OF eComments IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAction Dialog-Frame
ON VALUE-CHANGED OF fAction IN FRAME Dialog-Frame /* Action */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME faddr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL faddr Dialog-Frame
ON VALUE-CHANGED OF faddr IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fDesc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDesc Dialog-Frame
ON VALUE-CHANGED OF fDesc IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME femailFailure
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL femailFailure Dialog-Frame
ON VALUE-CHANGED OF femailFailure IN FRAME Dialog-Frame /* Failure Address */
DO:
   run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME femailSuccess
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL femailSuccess Dialog-Frame
ON VALUE-CHANGED OF femailSuccess IN FRAME Dialog-Frame /* Success Address */
DO:
   run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fhtmlEmail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fhtmlEmail Dialog-Frame
ON VALUE-CHANGED OF fhtmlEmail IN FRAME Dialog-Frame /* Email Body */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fName Dialog-Frame
ON VALUE-CHANGED OF fName IN FRAME Dialog-Frame /* Display Name */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fprogexec
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fprogexec Dialog-Frame
ON VALUE-CHANGED OF fprogexec IN FRAME Dialog-Frame /* Program Execution */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fprogQueue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fprogQueue Dialog-Frame
ON VALUE-CHANGED OF fprogQueue IN FRAME Dialog-Frame /* Program Queue */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSubject
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSubject Dialog-Frame
ON VALUE-CHANGED OF fSubject IN FRAME Dialog-Frame /* Email Subject */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME radioEmails
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL radioEmails Dialog-Frame
ON VALUE-CHANGED OF radioEmails IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME slAvailableRoles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL slAvailableRoles Dialog-Frame
ON VALUE-CHANGED OF slAvailableRoles IN FRAME Dialog-Frame
DO:
   assign 
       bAddRole   :sensitive = true
       bDeleteRole:sensitive = false
       bAddAll    :sensitive = true
       bRemoveAll :sensitive = false
       .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME slavailbleUsers
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL slavailbleUsers Dialog-Frame
ON VALUE-CHANGED OF slavailbleUsers IN FRAME Dialog-Frame
DO:
  assign 
      bAddUser   :sensitive = true
      bDeleteUser:sensitive = false
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME slSelectedRoles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL slSelectedRoles Dialog-Frame
ON VALUE-CHANGED OF slSelectedRoles IN FRAME Dialog-Frame
DO:
   assign 
       bDeleteRole:sensitive = true
       bAddRole   :sensitive = false
       bremoveAll :sensitive = true
       baddall    :sensitive = false
       .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME slSelectedUsers
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL slSelectedUsers Dialog-Frame
ON VALUE-CHANGED OF slSelectedUsers IN FRAME Dialog-Frame
DO:
    assign 
        bDeleteUser:sensitive = true
        bAddUser   :sensitive = false
        .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tActive
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tActive Dialog-Frame
ON VALUE-CHANGED OF tActive IN FRAME Dialog-Frame /* Active */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAnonymous
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAnonymous Dialog-Frame
ON VALUE-CHANGED OF tAnonymous IN FRAME Dialog-Frame /* Anonymous */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAudit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAudit Dialog-Frame
ON VALUE-CHANGED OF tAudit IN FRAME Dialog-Frame /* Audit */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDestination
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDestination Dialog-Frame
ON VALUE-CHANGED OF tDestination IN FRAME Dialog-Frame /* Has Output */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tevent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tevent Dialog-Frame
ON VALUE-CHANGED OF tevent IN FRAME Dialog-Frame /* Event Enabled */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLog Dialog-Frame
ON VALUE-CHANGED OF tLog IN FRAME Dialog-Frame /* Log */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tQueueable
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tQueueable Dialog-Frame
ON VALUE-CHANGED OF tQueueable IN FRAME Dialog-Frame /* Queueable */
DO:
  fprogQueue:sensitive = tQueueable:checked.
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tsecure
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tsecure Dialog-Frame
ON VALUE-CHANGED OF tsecure IN FRAME Dialog-Frame /* Secure */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

 bAddRole      :load-image("images/s-next.bmp").
 bDeleteRole   :load-image("images/s-previous.bmp").
 bAddAll       :load-image("images/s-nextpg.bmp").
 bremoveall    :load-image("images/s-previouspg.bmp").
 bAddUser      :load-image("images/s-next.bmp").
 bDeleteuser   :load-image("images/s-previous.bmp").
 bEditEmailBody:load-image("images/s-update.bmp").
 bAddRole      :load-image-insensitive("images/s-next-i.bmp").
 bDeleteRole   :load-image-insensitive("images/s-previous-i.bmp").
 bAddAll       :load-image-insensitive("images/s-nextpg-i.bmp").
 bremoveall    :load-image-insensitive("images/s-previouspg-i.bmp").
 bAddUser      :load-image-insensitive("images/s-next-i.bmp").
 bDeleteuser   :load-image-insensitive("images/s-previous-i.bmp").
 bEditEmailBody:load-image-insensitive("images/s-update-i.bmp").
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  /* getting the data */
  run getData in this-procedure.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enabledisableSave Dialog-Frame 
PROCEDURE enabledisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  case ipcActionType:
   when {&New} 
    then  
     Btn_OK:sensitive = (faction:input-value <> "" ).

   when {&Modify}
    then
     Btn_OK:sensitive = not (cTrackDesc          = fdesc:input-value           and
                            cTrackProgex         = fprogexec:input-value       and
                            cTrackActive         = tActive:checked             and
                            cTrackAny            = tAnonymous:checked          and
                            cTrackSecure         = tsecure:checked             and
                            cTracklog            = tlog:checked                and
                            cTrackAudit          = tAudit:checked              and
                            cTrackEvent          = tevent:checked              and
                            cTrackSend           = radioemails:input-value     and
                            cTrackEmailSuccess   = femailsuccess:input-value   and
                            cTrackEmailFail      = femailFailure:input-value   and
                            cTrackComments       = eComments:input-value       and
                            cTrackIP             = faddr:input-value           and
                            cTrackQueueable      = tQueueable:checked          and
                            cTrackDestination    = tDestination:checked        and
                            cTrackName           = fName:input-value           and
                            cTrackSubject        = fSubject:input-value        and
                            cTrackProgQueue      = fprogQueue:input-value      and
                            cTrackHtmlEmail      = fhtmlEmail:input-value      and
                            (cTrackSelectedRoles = slSelectedRoles:list-items) and 
                            (cTrackSelectedUser  = slSelectedusers:list-items)
                            ).
   when {&copy}
    then
     Btn_OK:sensitive = (cTrackAction <> faction:input-value).

  end case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fAction fDesc fprogexec tActive tAnonymous tsecure tLog tAudit tevent 
          radioEmails femailSuccess femailFailure tQueueable fprogQueue 
          tDestination fName fSubject fhtmlEmail slAvailableRoles 
          slSelectedRoles slavailbleUsers slSelectedUsers faddr createDate 
          eComments 
      WITH FRAME Dialog-Frame.
  ENABLE bAddAll fAction fDesc fprogexec tActive tAnonymous tsecure tLog tAudit 
         tevent radioEmails femailSuccess femailFailure tQueueable fprogQueue 
         tDestination fName fSubject fhtmlEmail slAvailableRoles bRemoveAll 
         slSelectedRoles slavailbleUsers slSelectedUsers faddr createDate 
         eComments Btn_OK Btn_Cancel RECT-68 RECT-69 RECT-70 RECT-71 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData Dialog-Frame 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  define variable chRoleList as character no-undo.
  define variable chUserList as character no-undo.
 
  /* Getting user and roles list */
  publish "getSysRoleList" (",",
                            output chRoleList,
                            output std-lo,
                            output std-ch ).
 
  publish "getSysUserList" (",",
                            output chUserList,
                            output std-lo,
                            output std-ch ).
 
  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.
 
  slavailbleUsers :list-items in frame {&frame-name} =  "!*" + "," +  trim(chUserList,",").
  slAvailableRoles:list-items in frame {&frame-name} =  "!*" + "," +  trim(chRoleList,",").
 
  find first sysaction no-error.
  if available sysaction 
   then
    do:
      if ipcActionType = {&copy}
       then
        do:
          assign
              frame dialog-frame:title = "Copy System Action"
              faction:sensitive        = true
              createDate:screen-value  = string(now)
              .
        end.
      else
      do:
        assign
            faction:sensitive        = false
            frame dialog-frame:title = "Edit System Action"
            createDate:screen-value  = string(sysaction.createDate)
            .
      end.
 
      slavailbleUsers:delete(trim(sysaction.userids,",")).
      slAvailableRoles:delete( trim(sysaction.roles,",")).
      
     
      assign
          faction:screen-value           = sysaction.action
          fDesc:screen-value             = sysaction.description
          fprogexec:screen-value         = sysaction.progExec
          tActive:checked                = sysaction.isActive
          tAnonymous:checked             = sysaction.isAnonymous                     
          tSecure:checked                = sysaction.isSecure                
          tlog:checked                   = sysaction.isLog           
          tAudit:checked                 = sysaction.isAudit         
          slSelectedRoles:list-items     = trim(sysaction.roles,",")
          slSelectedusers:list-items     = trim(sysaction.userids,",")
          faddr:screen-value             = sysaction.addrs           
          radioemails:screen-value       = sysaction.emails          
          femailSuccess:screen-value     = sysaction.emailSuccess    
          femailFailure:screen-value     = sysaction.emailFailure    
          tevent:checked                 = logical(sysaction.isEventsEnabled )
          ecomments:screen-value         = sysaction.comments
          tQueueable:checked             = sysaction.isQueueable
          tDestination:checked           = sysaction.isDestination          
          fname:screen-value             = sysaction.name    
          fhtmlEmail:screen-value        = sysaction.htmlEmail    
          fSubject:screen-value          = sysaction.subject
          fprogQueue:screen-value        = sysaction.progQueue
          .
    end.
   else
   do:
     frame dialog-frame:title = "New System Action".
     faction:sensitive = true.
     createDate:screen-value = string(now).
   end.   
   
   fprogQueue:sensitive = tQueueable:checked.
   
   
   /* EnableDisablesave */
   assign
       cTrackAction        =  faction:input-value         
       cTrackDesc          =  fDesc:input-value         
       cTrackProgex        =  fprogexec:input-value     
       cTrackActive        =  tActive:input-value            
       cTrackAny           =  tAnonymous:input-value         
       cTrackSecure        =  tSecure:input-value            
       cTracklog           =  tlog:input-value               
       cTrackAudit         =  tAudit:input-value             
       cTrackEvent         =  tevent:input-value   
       cTrackSend          =  radioemails:input-value
       cTrackEmailSuccess  =  femailSuccess:input-value  
       cTrackEmailFail     =  femailFailure:input-value    
       cTrackComments      =  ecomments:input-value 
       cTrackSelectedRoles =  slSelectedRoles:list-items 
       cTrackSelectedUser  =  slSelectedusers:list-items
       cTrackIP            =  faddr:input-value  
       cTrackQueueable     =  tQueueable:input-value             
       cTrackDestination   =  tDestination:input-value    
       cTrackName          =  fname:input-value 
       cTrackSubject       =  fsubject:input-value 
       cTrackProgQueue     =  fprogQueue:input-value
       cTrackHtmlEmail     =  fhtmlEmail:input-value
       .
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveSysAction Dialog-Frame 
PROCEDURE saveSysAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  if ipcActionType = {&new} or ipcActionType = {&copy}
   then
    do:
      empty temp-table sysaction.
      create sysaction.
      sysaction.action = faction:input-value in frame {&frame-name}.
    end.
    

  assign   
      sysaction.description      = fDesc:input-value                            
      sysaction.progExec         = fprogexec:input-value                          
      sysaction.isActive         = tActive:input-value
      sysaction.isAnonymous      = tAnonymous:input-value                        
      sysaction.isSecure         = tSecure:input-value                   
      sysaction.isLog            = tlog:input-value    
      sysaction.isAudit          = tAudit:input-value 
      sysaction.roles            = slSelectedRoles:list-items
      sysaction.userids          = slSelectedUsers:list-items
      sysaction.addrs            = faddr:input-value
      sysaction.createDate       = today 
      sysaction.emails           = radioemails:input-value        
      sysaction.emailSuccess     = femailSuccess:input-value       
      sysaction.emailFailure     = femailFailure:input-value      
      sysaction.isEventsEnabled  = tevent:input-value      
      sysaction.comments         = ecomments:input-value
      sysaction.isQueueable      = tQueueable:input-value 
      sysaction.isDestination    = tDestination:input-value        
      sysaction.name             = fName:input-value       
      sysaction.htmlEmail        = fhtmlEmail:input-value      
      sysaction.subject          = fSubject:input-value      
      sysaction.progQueue        = fprogQueue:input-value
      opAction                   = faction:input-value
      .

  if ipcActionType = {&modify}  
   then
    publish "modifySysAction" (input table sysaction,
                               output lSuccess,
                               output std-ch).
   else
     publish "newSysAction" (input table sysaction,  /* for new Action or Copy */
                             output lSuccess,
                             output std-ch).

  if not lSuccess
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

