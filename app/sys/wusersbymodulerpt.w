&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wusersbymodulerpt.w

  Description: Screen for system users and their call count
  
  Modified:
 
  Date       Name      Description
 
  08/22/2019 Gurvindar Removed progress error while populating combo-box.
----------------------------------------------------------------------*/
create widget-pool.

{lib/std-def.i}
{lib/sys-def.i}
{lib/get-column.i}
{lib/winshowscrollbars.i}

{tt/syslog.i}
{tt/syslog.i &tableAlias=ttsyslog}

define variable dColumnWidth   as decimal   no-undo.
define variable chAction       as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwSysLogRpt

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttsyslog

/* Definitions for BROWSE brwSysLogRpt                                  */
&Scoped-define FIELDS-IN-QUERY-brwSysLogRpt ttsyslog.applicationID ttsyslog.name ttsyslog.role ttsyslog.totalcount   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwSysLogRpt   
&Scoped-define SELF-NAME brwSysLogRpt
&Scoped-define QUERY-STRING-brwSysLogRpt for each ttsyslog
&Scoped-define OPEN-QUERY-brwSysLogRpt open query {&SELF-NAME} for each ttsyslog.
&Scoped-define TABLES-IN-QUERY-brwSysLogRpt ttsyslog
&Scoped-define FIRST-TABLE-IN-QUERY-brwSysLogRpt ttsyslog


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btGet flStartDate cbModule flEndDate RECT-65 
&Scoped-Define DISPLAYED-OBJECTS flStartDate cbModule flEndDate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getModuleDesc C-Win 
FUNCTION getModuleDesc RETURNS CHARACTER
  ( pApplicationID as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL PRIVATE
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setData C-Win 
FUNCTION setData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validateData C-Win 
FUNCTION validateData RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON btGet  NO-FOCUS
     LABEL "get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get Data".

DEFINE VARIABLE cbModule AS CHARACTER FORMAT "X(256)":U 
     LABEL "Module" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 44.2 BY 1 TOOLTIP "Select Module" NO-UNDO.

DEFINE VARIABLE flEndDate AS DATE FORMAT "99/99/99":U 
     LABEL "End" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 TOOLTIP "Display the status as of this date" NO-UNDO.

DEFINE VARIABLE flStartDate AS DATE FORMAT "99/99/99":U 
     LABEL "Start" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 108.4 BY 2.33.

DEFINE RECTANGLE RECT-65
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 9.8 BY 2.33.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwSysLogRpt FOR 
      ttsyslog SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwSysLogRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwSysLogRpt C-Win _FREEFORM
  QUERY brwSysLogRpt DISPLAY
      ttsyslog.applicationID           label  "Module"    format "X(50)"             
ttsyslog.name                    label  "Name"      format "x(45)"    width 32
ttsyslog.role                    label  "Roles"     format "x(50)"    width 44
ttsyslog.totalcount              label  "Calls"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 117.6 BY 18.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     btGet AT ROW 1.57 COL 102 WIDGET-ID 266 NO-TAB-STOP 
     btExport AT ROW 1.57 COL 111.4 WIDGET-ID 262 NO-TAB-STOP 
     flStartDate AT ROW 1.91 COL 60.8 COLON-ALIGNED WIDGET-ID 280
     cbModule AT ROW 1.91 COL 9.6 COLON-ALIGNED WIDGET-ID 242
     flEndDate AT ROW 1.91 COL 83 COLON-ALIGNED WIDGET-ID 252
     brwSysLogRpt AT ROW 4.05 COL 2.2 WIDGET-ID 200
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1 COL 3.2 WIDGET-ID 28
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1 COL 111.6 WIDGET-ID 274
     RECT-2 AT ROW 1.29 COL 2.2 WIDGET-ID 8
     RECT-65 AT ROW 1.29 COL 110.2 WIDGET-ID 272
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 273.2 BY 24.76
         DEFAULT-BUTTON btGet WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Users By Module"
         HEIGHT             = 22
         WIDTH              = 120
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwSysLogRpt flEndDate fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BROWSE brwSysLogRpt IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwSysLogRpt:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwSysLogRpt:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON btExport IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwSysLogRpt
/* Query rebuild information for BROWSE brwSysLogRpt
     _START_FREEFORM
open query {&SELF-NAME} for each ttsyslog.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwSysLogRpt */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Users By Module */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Users By Module */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Users By Module */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwSysLogRpt
&Scoped-define SELF-NAME brwSysLogRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSysLogRpt C-Win
ON ROW-DISPLAY OF brwSysLogRpt IN FRAME fMain
do:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSysLogRpt C-Win
ON START-SEARCH OF brwSysLogRpt IN FRAME fMain
do:
  assign
      hSortColumn = browse {&browse-name}:current-column
      chAction    = hSortColumn:name 
      .
   
  {lib/brw-startSearch.i}    
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btExport C-Win
ON CHOOSE OF btExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGet C-Win
ON CHOOSE OF btGet IN FRAME fMain /* get */
do:
  if not validateData() 
   then
    return no-apply.

  run getdata in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbModule
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbModule C-Win
ON VALUE-CHANGED OF cbModule IN FRAME fMain /* Module */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flEndDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flEndDate C-Win
ON VALUE-CHANGED OF flEndDate IN FRAME fMain /* End */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flStartDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flStartDate C-Win
ON VALUE-CHANGED OF flStartDate IN FRAME fMain /* Start */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

/* The CLOSE event can be used from inside or outside the procedure to terminate it. */
on close of this-procedure 
  run disable_UI.

/* Best default for GUI applications is...                              */
pause 0 before-hide.
subscribe to "closeWindow" anywhere.

btGet:load-image ("images/Completed.bmp").              
btGet:load-image-insensitive("images/Completed-i.bmp").
btExport:load-image("images/excel.bmp").
btExport:load-image-insensitive("images/excel-i.bmp").

/* Get Application module list from syscode */
run getModuleCombo in this-procedure.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

  run enable_UI. 
   
  /* Function to set default values */
  setData().

  {lib/get-column-width.i &col="'name'" &var=dColumnWidth}
  
  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.
  
  run windowResized in this-procedure.
  
  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flStartDate cbModule flEndDate 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE btGet flStartDate cbModule flEndDate RECT-65 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if query brwSysLogRpt:num-results = 0 
   then
    do: 
      message "There is nothing to export"
       view-as alert-box warning buttons ok.
      return.
    end.
  
  publish "GetReportDir" (output std-ch).
  
  std-ha = temp-table ttsyslog:handle.
  run util/exporttable.p (table-handle std-ha,
                          "ttsyslog",
                          "for each ttsyslog ",
                          "applicationID,name,role,totalcount",
                          "From " + flStartDate:screen-value + " To " + flEndDate:screen-value + chr(10) + "Module,Name,Role,Calls",
                          std-ch,
                          "UsersByModule" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define buffer syslog for syslog.
 
 close query brwSysLogRpt.
 empty temp-table ttsyslog.

 do with frame {&frame-name}:
 end.
 
 /* Calling client side script to call server file */
 run server/queryusersbymodule.p (input cbModule:input-value,                              
                                  input flStartDate:input-value,
                                  input flEndDate:input-value,
                                  output table syslog,
                                  output std-lo,
                                  output std-ch).
  
 if not std-lo
  then
   do:
     message std-ch
       view-as alert-box info buttons ok.
     return.
   end.
 
 for each syslog:
   create ttsyslog.
   buffer-copy syslog using syslog.name syslog.role syslog.totalcount to ttsyslog.
   ttsyslog.applicationID = getModuleDesc(syslog.applicationID).
 end.

 open query brwSysLogRpt preselect each ttsyslog.

 /* Makes widget enable-disable based on the data */ 
 if query brwSysLogRpt:num-results > 0 
  then
   assign 
       browse brwSysLogRpt:sensitive = true
              btExport:sensitive     = true
              .  
 else
   assign 
       browse brwSysLogRpt:sensitive = false
              btExport:sensitive     = false
              .

  /* Display no. of records with date and time on status bar */
  setStatusRecords(query brwSysLogRpt:num-results).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getModuleCombo C-Win 
PROCEDURE getModuleCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Local variable */
  define variable cModuleList as character no-undo.
   
   do with frame {&frame-name}:
   end.
 
  publish "getSysCodeListItems" (input  "Module",
                                 output cModuleList,
                                 output std-lo,
                                 output std-ch).
 
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.
 
  cbModule:list-item-pairs = "ALL,ALL".
  if cModuleList = ""  
   then
    return.
    
  cbModule:list-item-pairs = cbModule:list-item-pairs + "," + cModuleList + "," + "Unknown,Unknown".      
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  if chAction = "applicationID" 
   then
    tWhereClause = tWhereClause + " by ttsyslog.name by ttsyslog.role by ttsyslog.totalcount ".
  else if chAction = "name" 
   then
    tWhereClause = tWhereClause + " by ttsyslog.applicationID by ttsyslog.role by ttsyslog.totalcount ".
  else if chAction = "totalcount"
   then
    tWhereClause = tWhereClause + " by ttsyslog.applicationID by ttsyslog.name by ttsyslog.role ".
  else
   tWhereClause = tWhereClause + " by ttsyslog.applicationID by ttsyslog.name by ttsyslog.totalcount ".
 
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels         
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 12        
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y
      .

  {lib/resize-column.i &col="'applicationID,name,role'" &var=dColumnWidth}

  run ShowScrollBars(browse brwSysLogRpt:handle, no, yes).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getModuleDesc C-Win 
FUNCTION getModuleDesc RETURNS CHARACTER
  ( pApplicationID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cModuleName as character no-undo.

  publish "getSysCodeDesc" (input "Module", 
                            input pApplicationID, 
                            output cModuleName, 
                            output std-lo, 
                            output std-ch).

  if not std-lo 
   then
    message std-ch
        view-as alert-box info buttons ok.
  
  return cModuleName.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL PRIVATE
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 setStatusMessage({&ResultNotMatch}).
 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setData C-Win 
FUNCTION setData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end.
  
  /* Get default no. of days from dialog config to initialise start date */
  publish "getDefaultdays" (output std-in).

  assign
      cbModule:screen-value    = "ALL"
      flStartDate:screen-value = string(today - std-in)
      flEndDate:screen-value   = if cbModule:input-value <> "" then string(today) else ""
      btExport:sensitive       = false
      .
  setStatusMessage("").

  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validateData C-Win 
FUNCTION validateData RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if interval(flEndDate:input-value, flStartDate:input-value, "days") > 30 
   then
    do:
      message "The date range is greater than 30 days the report will be slow. Are you sure you want to continue?" 
        view-as alert-box warning buttons yes-no update std-lo.
     
      if not std-lo 
       then
        return false.
    end.
 
  if interval(flEndDate:input-value, flStartDate:input-value, "years") > 0
  then
    do:
      message "Date range cannot be more than a year." 
        view-as alert-box error buttons ok.
     
      return false.
    end.

  return true.   /* function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

