&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File:dialogsyskey.w

  Description: Dialog to add or edit a systemkey .

  Input Parameters:
 
  Author:Gurvindar 

  Created:02.18.2019 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{tt/syskey.i}
{lib/sys-def.i}
{lib/std-def.i}

/* Parameters Definitions ---                                           */
define input  parameter ipcActionType as character no-undo.
define input-output parameter table for syskey.
define output parameter lSuccess      as logical   no-undo.

/* Local Variable Definitions ---                                       */
define variable cCurrUser   as character no-undo.
define variable cCurrentUID as character no-undo.
define variable iOldSeq     as integer   no-undo.

define variable cTrackseq    as integer   no-undo.
define variable cTracknotes  as character no-undo.
define variable cTrackType   as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS flType flSeq edNotes bCancel 
&Scoped-Define DISPLAYED-OBJECTS flType flSeq edNotes flUser flChangeDate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Cancel"
     BGCOLOR 8 .

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14 TOOLTIP "Save"
     BGCOLOR 8 .

DEFINE VARIABLE edNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 54.8 BY 2.48 NO-UNDO.

DEFINE VARIABLE flChangeDate AS DATE FORMAT "99/99/99":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14.6 BY 1 NO-UNDO.

DEFINE VARIABLE flSeq AS INTEGER FORMAT ">>>>>>>9":U INITIAL 0 
     LABEL "Sequence" 
     VIEW-AS FILL-IN 
     SIZE 21.6 BY 1 NO-UNDO.

DEFINE VARIABLE flType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 21.6 BY 1 NO-UNDO.

DEFINE VARIABLE flUser AS CHARACTER FORMAT "X(256)":U 
     LABEL "Last Modified" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 26 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     flType AT ROW 1.48 COL 15.6 COLON-ALIGNED WIDGET-ID 40
     flSeq AT ROW 2.71 COL 15.6 COLON-ALIGNED WIDGET-ID 16
     edNotes AT ROW 3.95 COL 17.6 NO-LABEL WIDGET-ID 12
     flUser AT ROW 6.67 COL 15.6 COLON-ALIGNED WIDGET-ID 44
     flChangeDate AT ROW 6.67 COL 43.4 COLON-ALIGNED NO-LABEL WIDGET-ID 42
     bSave AT ROW 8.29 COL 22.2
     bCancel AT ROW 8.29 COL 40.8
     "Notes:" VIEW-AS TEXT
          SIZE 6.8 BY .62 AT ROW 3.91 COL 10.6 WIDGET-ID 14
     SPACE(56.59) SKIP(5.41)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "New Syskey"
         CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bSave IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flChangeDate IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flChangeDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN flUser IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flUser:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* New Syskey */
do:
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave Dialog-Frame
ON CHOOSE OF bSave IN FRAME Dialog-Frame /* Save */
do:
  run validationcheck in this-procedure (output std-ch).

  if std-ch <> "" 
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return no-apply.
    end.

  run saveSystemKey in this-procedure.
  
  if not lSuccess
   then
     return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edNotes Dialog-Frame
ON VALUE-CHANGED OF edNotes IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flSeq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flSeq Dialog-Frame
ON VALUE-CHANGED OF flSeq IN FRAME Dialog-Frame /* Sequence */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flType Dialog-Frame
ON VALUE-CHANGED OF flType IN FRAME Dialog-Frame /* Type */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? THEN
  frame {&frame-name}:parent = active-window.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

  run enable_UI.

  /* current user logged in details */
  publish "GetCredentialsName" (output cCurrUser).
  publish "GetCredentialsID"   (output cCurrentUID).

  /* display data on screen */
  run displayData in this-procedure.
  
  /* sensitivity of save button */
  run enableDisableSave in this-procedure.

  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* showing data on widgets */
  find first syskey no-error.
  if available syskey
   then
    do:
      case ipcActionType:
       when {&New}
        then
         do:
           assign
               flChangeDate:screen-value = ""
               flUser:screen-value       = ""
               flType:sensitive          = true
               frame Dialog-Frame:title  = "New Key"
               .
         end.
       when {&modify}
        then
         do:
           assign
               flChangeDate:screen-value = string(syskey.changedate)
               flUser:screen-value       = syskey.username
               frame Dialog-Frame:title  = "Edit Key"
               flType:sensitive          = false
               .
         end.
       when {&Copy}
        then
         do:
           assign
               flChangeDate:screen-value = ""
               flUser:screen-value       = ""
               frame Dialog-Frame:title  = "Copy Key"
               flType:sensitive          = true
               .
         end.
      end case.

      assign 
          flSeq :screen-value       = string(syskey.seq)
          iOldSeq                   = syskey.seq
          flType:screen-value       = syskey.type
          .
    end.

  /* Keep the initial Values, required in enableDisableSave. */
  assign
      cTrackseq   = flSeq :input-value
      cTracknotes = edNotes:input-value 
      cTrackType  = fltype:input-value
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  case ipcActionType:
   when {&New} 
    then
     bSave:sensitive = (flseq:input-value   <> 0    or
                        edNotes:input-value <> " "  or
                        fltype:input-value  <> " " )
                      .
   when {&modify} 
    then
     bSave:sensitive = not(cTrackSeq   = (flSeq:input-value) and
                           cTrackNotes = trim(edNotes:input-value)
                       ).
   when {&Copy} 
    then
      bSave:sensitive = not(fltype:input-value  = cTrackType).
 
  end case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flType flSeq edNotes flUser flChangeDate 
      WITH FRAME Dialog-Frame.
  ENABLE flType flSeq edNotes bCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveSystemKey Dialog-Frame 
PROCEDURE saveSystemKey :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable changeDate as datetime no-undo.

  do with frame {&frame-name}:
  end.

  if ipcActionType = {&New} 
   then
    do:
      empty temp-table syskey.
      create syskey.
    end.
   
  /* Modify as well as for new and copy filling the data */
  assign                                       
      syskey.type       = flType:input-value  
      syskey.username   = cCurrUser
      syskey.seq        = flSeq:input-value
      syskey.notes      = edNotes:input-value
      .      

  /* client Server Call */

  if ipcActionType = {&Modify}
   then
    run server/modifysyskey.p (input table syskey,
                               output changeDate,
                               output lSuccess,
                               output std-ch).
   else
    run server/newsyskey.p (input table syskey,
                            output changeDate,
                            output lSuccess,
                            output std-ch).

  if not lSuccess
   then
    do:
      message std-ch
        view-as alert-box info buttons ok.
      return no-apply.
    end.


  /* adding change date in temp-table */
  find first syskey no-error.
  if available syskey
   then
    assign 
        syskey.changedate = changedate
        .
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validationCheck Dialog-Frame 
PROCEDURE validationCheck :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define output parameter valMsg as character no-undo.
 
  case ipcActionType:
   when {&New} 
    then
     do:
       if flType:screen-value = " "
        then
         do:
           valMsg = "Type cannot be blank".
           return valMsg.
         end.
      end.
   
   when {&modify} 
    then
     do:
       if iOldSeq > integer(flSeq:screen-value)
        then
         do:
           valMsg = "You cannot set the value lower than the existing value".
           return valMsg.
         end.
     end.

   when {&Copy} 
    then
     do:
       if flType:screen-value = " " 
        then
         do:
           valMsg = "Type cannot be blank".
           return valMsg. 
         end.
     end.
  end case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

