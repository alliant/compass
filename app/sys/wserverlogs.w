&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* sys/wserverlogs.w
   View the Server log file content on the browser.
   
   Author: Rahul Sharma
   Created: 04/01/2019
 */


CREATE WIDGET-POOL.

{lib/std-def.i}
{lib/sys-def.i}
{tt/serverlog.i}
{tt/serverlog.i &tableAlias=tserverlog}
{tt/serverlog.i &tableAlias=ttserverlog}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwServerLog

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttserverlog

/* Definitions for BROWSE brwServerLog                                  */
&Scoped-define FIELDS-IN-QUERY-brwServerLog ttserverlog.logdate ttserverlog.processID ttserverlog.threadID ttserverlog.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwServerLog   
&Scoped-define SELF-NAME brwServerLog
&Scoped-define QUERY-STRING-brwServerLog for each ttserverlog
&Scoped-define OPEN-QUERY-brwServerLog open query {&SELF-NAME} for each ttserverlog.
&Scoped-define TABLES-IN-QUERY-brwServerLog ttserverlog
&Scoped-define FIRST-TABLE-IN-QUERY-brwServerLog ttserverlog


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS flNoLines btGet RECT-78 RECT-79 RECT-80 
&Scoped-Define DISPLAYED-OBJECTS flNoLines fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search Data".

DEFINE BUTTON btGet  NO-FOCUS
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get Data".

DEFINE BUTTON bView  NO-FOCUS
     LABEL "View" 
     SIZE 7.2 BY 1.71 TOOLTIP "View Entry".

DEFINE VARIABLE flNoLines AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "No. of Lines" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 56.2 BY 1 TOOLTIP "Enter a search string (from the current cursor position)" NO-UNDO.

DEFINE RECTANGLE RECT-78
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 29.2 BY 2.24.

DEFINE RECTANGLE RECT-79
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 67 BY 2.24.

DEFINE RECTANGLE RECT-80
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 9.4 BY 2.24.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwServerLog FOR 
      ttserverlog SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwServerLog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwServerLog C-Win _FREEFORM
  QUERY brwServerLog DISPLAY
      ttserverlog.logdate        label  "Log Timestamp"            format "99/99/9999 HH:MM:SS"  width 24
ttserverlog.processID      label  "Process ID"           format "x(15)"   
ttserverlog.threadID       label  "Thread ID"            format "x(15)"
ttserverlog.description    label  "Description"          format "x(300)"    width 60
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 126 BY 17.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     flNoLines AT ROW 1.95 COL 14 COLON-ALIGNED WIDGET-ID 118
     btGet AT ROW 1.62 COL 22.8 WIDGET-ID 326 NO-TAB-STOP 
     fSearch AT ROW 1.95 COL 30.4 COLON-ALIGNED NO-LABEL WIDGET-ID 112
     brwServerLog AT ROW 3.95 COL 2 WIDGET-ID 200
     bSearch AT ROW 1.62 COL 89.4 WIDGET-ID 308 NO-TAB-STOP 
     bView AT ROW 1.62 COL 98.4 WIDGET-ID 4 NO-TAB-STOP 
     "Parameter" VIEW-AS TEXT
          SIZE 10 BY .62 AT ROW 1 COL 3.2 WIDGET-ID 124
     "Search" VIEW-AS TEXT
          SIZE 6.8 BY .62 AT ROW 1 COL 32.2 WIDGET-ID 126
     "Action" VIEW-AS TEXT
          SIZE 6.4 BY .62 AT ROW 1 COL 98.6 WIDGET-ID 330
     RECT-78 AT ROW 1.38 COL 2 WIDGET-ID 120
     RECT-79 AT ROW 1.38 COL 30.8 WIDGET-ID 122
     RECT-80 AT ROW 1.38 COL 97.4 WIDGET-ID 328
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 127.8 BY 21
         DEFAULT-BUTTON btGet WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Server Logs"
         HEIGHT             = 21
         WIDTH              = 127.8
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwServerLog fSearch fMain */
/* SETTINGS FOR BROWSE brwServerLog IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwServerLog:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwServerLog:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON bSearch IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bView IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fSearch IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwServerLog
/* Query rebuild information for BROWSE brwServerLog
     _START_FREEFORM
open query {&SELF-NAME} for each ttserverlog.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwServerLog */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Server Logs */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Server Logs */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Server Logs */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwServerLog
&Scoped-define SELF-NAME brwServerLog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwServerLog C-Win
ON DEFAULT-ACTION OF brwServerLog IN FRAME fMain
do:
  run openAction in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwServerLog C-Win
ON ROW-DISPLAY OF brwServerLog IN FRAME fMain
do:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwServerLog C-Win
ON START-SEARCH OF brwServerLog IN FRAME fMain
do:
  {lib/brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME fMain /* Search */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGet C-Win
ON CHOOSE OF btGet IN FRAME fMain /* Get */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bView C-Win
ON CHOOSE OF bView IN FRAME fMain /* View */
DO:
  run openAction in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flNoLines
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flNoLines C-Win
ON VALUE-CHANGED OF flNoLines IN FRAME fMain /* No. of Lines */
DO:
  resultsChanged(false).
  btGet:sensitive = (self:screen-value <> "0").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fMain
DO:
  apply 'choose' to bSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow" anywhere.

bView   :load-image            ("images/open.bmp").
bView   :load-image-insensitive("images/open-i.bmp").
btGet   :load-image            ("images/completed.bmp").              
btGet   :load-image-insensitive("images/completed-i.bmp").
bSearch :load-image-up         ("images/magnifier.bmp").
bSearch :load-image-insensitive("images/magnifier-i.bmp").

/* Sets default value to flNoLines fill-in */
publish "getDefaultLines" (output std-ch).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
    
  flNoLines:screen-value = std-ch.

  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.
   
  run windowResized in this-procedure.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  APPLY "CLOSE":U TO THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flNoLines fSearch 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE flNoLines btGet RECT-78 RECT-79 RECT-80 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer serverlog for serverlog.
 
  close query brwServerLog.
  empty temp-table ttserverlog.
  
  do with frame {&frame-name}:
  end.
  
  for each serverlog:
    /* test if the record contains the search text */
    if fSearch:screen-value <> "" and 
      not (serverlog.description  matches "*" + fSearch:screen-value + "*")
     then next.
    
    create ttserverlog.
    buffer-copy serverlog to ttserverlog.
  end.
  
  open query brwServerLog preselect each ttserverlog.
  
  /* Display no. of records on status bar */
  setStatusCount(query brwServerLog:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  define variable cOldDateFormat  as character  no-undo.
  
  define buffer tserverlog  for tserverlog.
  
  empty temp-table tserverlog.

  do with frame {&frame-name}:
  end.
  
  if flNoLines:input-value = 0
   then
    do:
      message "Please enter number of lines"
          view-as alert-box info buttons ok.
      return.
    end.

  /* client Server Call */
  run server/getserverlogs.p (input flNoLines:input-value,
                              output table tserverlog,
                              output std-lo,
                              output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch
        view-as alert-box info buttons ok.
      return.
    end.
     
  assign
      cOldDateFormat      = session:date-format
      session:date-format = "ymd"
      . 
  
  empty temp-table serverlog.

  for each tserverlog:
    if tserverlog.description = ""
     then next.

    create serverlog.
    assign
        serverlog.logdate     = datetime(replace(substring(tserverlog.description,2,21),"@"," "))
        serverlog.processID   = substring(tserverlog.description,32,6)
        serverlog.threadID    = substring(tserverlog.description,41,6)
        serverlog.loglevel    = substring(tserverlog.description,48,1)
        serverlog.description = substring(tserverlog.description,50)
        .
  end.
  
  session:date-format = cOldDateFormat.

  /* This will use the screen-value of search textbox */
  run filterData in this-procedure. 

  /* Makes widget enable-disable based on the data */ 
  if query brwServerLog:num-results > 0 
   then
    assign 
        browse brwServerLog:sensitive = true
               bView:sensitive        = true
               fSearch:sensitive      = true
               bSearch:sensitive      = true               
               .  
  else
    assign 
        browse brwServerLog:sensitive = false
               bView:sensitive        = false
               fSearch:sensitive      = false
               bSearch:sensitive      = false
               .
    
  /* Set Status count with date and time from the server on status bar */
  setStatusRecords(query brwServerLog:num-results).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openAction C-Win 
PROCEDURE openAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available ttserverlog
   then return.
  
  empty temp-table tserverlog.

  create tserverlog.
  buffer-copy ttserverlog to tserverlog.

  run dialogserverlog.w (input table tserverlog).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .
  
  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
assign 
      frame fMain:width-pixels          = {&window-name}:width-pixels
      frame fMain:virtual-width-pixels  = {&window-name}:width-pixels
      frame fMain:height-pixels         = {&window-name}:height-pixels
      frame fMain:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels       = frame fmain:width-pixels - 10
      {&browse-name}:height-pixels      = frame fMain:height-pixels - {&browse-name}:y - 3
      .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

