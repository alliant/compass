&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wsysNotification.w

  Description: Window of system notifications

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gurvindar

  Created: 09.25.2018

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
   by this procedure. This is a good default which assures
   that this procedure's triggers and internal procedures 
   will execute in this procedure's storage, and that proper
   cleanup will occur on deletion of the procedure. */

create widget-pool.

{lib/std-def.i}
{lib/sys-def.i}
{lib/winshowscrollbars.i}
{lib/get-column.i} 
/*------------------------Temp table Definitions--------------------*/
{tt/sysnotification.i}
{tt/sysnotification.i &tableAlias=ttsysnotification}
{tt/sysnotification.i &tableAlias=tsysnotification}

{tt/agent.i}
{tt/person.i}
{tt/sysuser.i}

/* Variable Definition */
define variable dColumnWidth   as decimal   no-undo.

define variable hSelectionAgent       as handle    no-undo.
define variable hTextAgent            as handle    no-undo.
define variable hSelectionPerson      as handle    no-undo.
define variable hTextPerson           as handle    no-undo.

define variable UID                   as character no-undo.
define variable cEntityID             as character no-undo.

define variable cStatus               as character no-undo.
define variable cNotificationStatus   as character no-undo.
define variable cLastSearchString     as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttsysnotification

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData updateEntity(ttsysnotification.entityType) @ ttsysnotification.entityType "Entity Type" ttsysnotification.UID "User ID" updateStatus(ttsysnotification.stat) @ ttsysnotification.stat "Status" updateNotificationStatus(ttsysnotification.notificationStat) @ ttsysnotification.notificationStat "Notification Status" ttsysnotification.sourceID "Source ID" ttsysnotification.createDate "Create Date" ttsysnotification.notification "Notification"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each ttsysnotification
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each ttsysnotification.
&Scoped-define TABLES-IN-QUERY-brwData ttsysnotification
&Scoped-define FIRST-TABLE-IN-QUERY-brwData ttsysnotification


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbSysUser bGet cbstatus cbentity fStartDate ~
fEndDate brwData bRefresh RECT-62 RECT-63 RECT-61 
&Scoped-Define DISPLAYED-OBJECTS cbSysUser cbstatus cbentity cbAgent ~
cbEntityID cbPerson fStartDate fEndDate fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD updateEntity C-Win 
FUNCTION updateEntity RETURNS CHARACTER
  (cEntity as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD updateNotificationStatus C-Win 
FUNCTION updateNotificationStatus RETURNS CHARACTER
  (cNotificationStatus as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD updateStatus C-Win 
FUNCTION updateStatus RETURNS CHARACTER
  (cStatus as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validateData C-Win 
FUNCTION validateData RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON bGet  NO-FOCUS
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get Data".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search".

DEFINE BUTTON bView  NO-FOCUS
     LABEL "View" 
     SIZE 7.2 BY 1.71 TOOLTIP "View Entry".

DEFINE VARIABLE cbAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity ID" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 52.4 BY 1 TOOLTIP "Entity ID" NO-UNDO.

DEFINE VARIABLE cbentity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL",
                     "Agent","A",
                     "Employee","E",
                     "Person","P"
     DROP-DOWN-LIST
     SIZE 16.4 BY 1 TOOLTIP "Entity Type" NO-UNDO.

DEFINE VARIABLE cbEntityID AS CHARACTER 
     LABEL "Entity ID" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN AUTO-COMPLETION
     SIZE 52.4 BY 1 TOOLTIP "Entity ID" NO-UNDO.

DEFINE VARIABLE cbPerson AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity ID" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 52.4 BY 1 TOOLTIP "Entity ID" NO-UNDO.

DEFINE VARIABLE cbstatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 5
     LIST-ITEM-PAIRS "New","N",
                     "Supress","S",
                     "Dismiss","D"
     DROP-DOWN-LIST
     SIZE 19 BY 1 TOOLTIP "Status" NO-UNDO.

DEFINE VARIABLE cbSysUser AS CHARACTER 
     LABEL "User ID" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN
     SIZE 31 BY 1 TOOLTIP "User ID" NO-UNDO.

DEFINE VARIABLE fEndDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "End Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Create Date To" NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 33.6 BY 1 TOOLTIP "Search Criteria (User ID,Source ID,Notification)" NO-UNDO.

DEFINE VARIABLE fStartDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Start Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Create Date From" NO-UNDO.

DEFINE RECTANGLE RECT-61
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 25.6 BY 2.62.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 144.4 BY 2.62.

DEFINE RECTANGLE RECT-63
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 46 BY 2.62.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      ttsysnotification SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      updateEntity(ttsysnotification.entityType) @ ttsysnotification.entityType                         label       "Entity Type"                   format "x(25)"    width 15
 ttsysnotification.UID                                                                                  label       "User ID"                       format "x(30)" width 30
 updateStatus(ttsysnotification.stat)                         @ ttsysnotification.stat                  label       "Status"                        format "x(25)" width 20
 updateNotificationStatus(ttsysnotification.notificationStat) @ ttsysnotification.notificationStat      label       "Notification Status"           format "x(25)" width 20
 ttsysnotification.sourceID                                                                             label       "Source ID"                     format "x(25)" width 20
 ttsysnotification.createDate                                                                           label       "Create Date"                   format "99/99/9999" width 15
 ttsysnotification.notification                                                                         label       "Notification"                  format "x(80)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 215.2 BY 16.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     cbSysUser AT ROW 1.76 COL 37.2 COLON-ALIGNED WIDGET-ID 326
     bGet AT ROW 1.95 COL 163.4 WIDGET-ID 64 NO-TAB-STOP 
     cbstatus AT ROW 2.91 COL 37.2 COLON-ALIGNED WIDGET-ID 316
     cbentity AT ROW 1.76 COL 80.6 COLON-ALIGNED WIDGET-ID 320
     bSearch AT ROW 1.95 COL 209 WIDGET-ID 308 NO-TAB-STOP 
     cbAgent AT ROW 2.91 COL 80.6 COLON-ALIGNED WIDGET-ID 322
     cbEntityID AT ROW 2.91 COL 80.6 COLON-ALIGNED WIDGET-ID 318
     cbPerson AT ROW 2.91 COL 80.6 COLON-ALIGNED WIDGET-ID 324
     fStartDate AT ROW 1.76 COL 146 COLON-ALIGNED WIDGET-ID 54
     bExport AT ROW 2 COL 3.8 WIDGET-ID 2 NO-TAB-STOP 
     fEndDate AT ROW 2.86 COL 146 COLON-ALIGNED WIDGET-ID 58
     fSearch AT ROW 2.38 COL 172 COLON-ALIGNED NO-LABEL WIDGET-ID 310
     brwData AT ROW 4.38 COL 2.8 WIDGET-ID 200
     bView AT ROW 2 COL 19.6 WIDGET-ID 4 NO-TAB-STOP 
     bRefresh AT ROW 2 COL 11.6 WIDGET-ID 158 NO-TAB-STOP 
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.24 COL 4 WIDGET-ID 52
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.24 COL 29 WIDGET-ID 62
     "Search" VIEW-AS TEXT
          SIZE 7.6 BY .62 AT ROW 1.24 COL 173.4 WIDGET-ID 314
     RECT-62 AT ROW 1.52 COL 28 WIDGET-ID 60
     RECT-63 AT ROW 1.52 COL 172 WIDGET-ID 312
     RECT-61 AT ROW 1.52 COL 2.6 WIDGET-ID 50
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 218.8 BY 21.14
         DEFAULT-BUTTON bGet WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Notifications"
         HEIGHT             = 20.62
         WIDTH              = 219.2
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData fSearch fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON bSearch IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bView IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbAgent IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbEntityID IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbPerson IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fSearch IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each ttsysnotification.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Notifications */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Notifications */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Notifications */
DO:
   run windowResized in this-procedure.   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGet C-Win
ON CHOOSE OF bGet IN FRAME fMain /* Get */
DO:
  if not validateData() 
   then
    return no-apply.

  run getData in this-procedure.
  fSearch:screen-value = cLastSearchString.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
do:
  run getData in this-procedure.
  fSearch:screen-value = cLastSearchString.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  run showSysNotification in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME fMain /* Search */
DO:
  /* if search button is clicked or return key is hit, 
     then 'cLastSearchString' stores the last string searched until user again hits the search */
  cLastSearchString  = fSearch:input-value.
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bView C-Win
ON CHOOSE OF bView IN FRAME fMain /* View */
DO:
  run showSysNotification in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgent C-Win
ON VALUE-CHANGED OF cbAgent IN FRAME fMain /* Entity ID */
DO:
   cEntityID = cbAgent:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbentity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbentity C-Win
ON VALUE-CHANGED OF cbentity IN FRAME fMain /* Entity */
DO:
  run actionValueChanged in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbEntityID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbEntityID C-Win
ON VALUE-CHANGED OF cbEntityID IN FRAME fMain /* Entity ID */
DO:
  cEntityID = cbEntityID:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbPerson
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbPerson C-Win
ON VALUE-CHANGED OF cbPerson IN FRAME fMain /* Entity ID */
DO:
  cEntityID = cbPerson:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbSysUser
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbSysUser C-Win
ON VALUE-CHANGED OF cbSysUser IN FRAME fMain /* User ID */
DO:
  UID = cbSysUser:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fEndDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fEndDate C-Win
ON VALUE-CHANGED OF fEndDate IN FRAME fMain /* End Date */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON ENTRY OF fSearch IN FRAME fMain
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fMain
DO:
  apply 'choose':U to bSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStartDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStartDate C-Win
ON VALUE-CHANGED OF fStartDate IN FRAME fMain /* Start Date */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/****************************  Main Block  ****************************/

{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

assign 
  current-window                = {&window-name} 
  this-procedure:current-window = {&window-name}
  .

{lib/win-main.i}
{lib/win-status.i}

on close of this-procedure 
  run disable_UI.

setStatusMessage("").

pause 0 before-hide.

bGet:load-image               ("images/completed.bmp").
bGet:load-image-insensitive   ("images/completed-i.bmp").

bSearch:load-image            ("images/find.bmp").
bSearch:load-image-insensitive("images/find-i.bmp").

bExport:load-image            ("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").

bView:load-image              ("images/open.bmp").
bView:load-image-insensitive  ("images/open-i.bmp").

bRefresh   :load-image ("images/refresh.bmp").
bRefresh   :load-image-insensitive("images/refresh-i.bmp").

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

  run enable_UI.  

  /* get the column width */
  {lib/get-column-width.i &col="'createDate'"  &var=dColumnWidth} 
 
  /* procedure to set default start and end date */
  run setData in this-procedure.

  /* create the agent combo */
  {lib/get-agent-list.i &combo=cbAgent &addAll=true &setEnable=std-lo} 
  
  {lib/get-person-list.i &combo=cbPerson &addAll=true &setEnable=std-lo}
 
  run showWindow in this-procedure.
  
  on entry anywhere
    do:
      hSelectionAgent    = GetWidgetByName(cbAgent   :frame in frame {&frame-name}, "cbAgentAgentSelection").
      hTextAgent         = GetWidgetByName(cbAgent   :frame in frame {&frame-name}, "cbAgentAgentText").
      hSelectionPerson   = GetWidgetByName(cbPerson  :frame in frame {&frame-name}, "cbPersonPersonSelection").
      hTextPerson        = GetWidgetByName(cbPerson  :frame in frame {&frame-name}, "cbPersonPersonText").
      hSelectionPerson   = GetWidgetByName(cbPerson  :frame in frame {&frame-name}, "cbPersonPersonSelection").
      hTextPerson        = GetWidgetByName(cbPerson  :frame in frame {&frame-name}, "cbPersonPersonText").

      if valid-handle(hSelectionAgent) and valid-handle(hTextAgent)
       then
        if not self:name = "cbAgentAgentText" and not self:name = "cbAgentAgentSelection"
         then hSelectionAgent:visible = false.
        else
        do:
          if hSelectionAgent:list-item-pairs > "" and not hSelectionAgent:visible
           then hSelectionAgent:visible = true.
        end.

      if valid-handle(hSelectionPerson) and valid-handle(hTextPerson)
       then
        if not self:name = "cbPersonPersonText" and not self:name = "cbPersonPersonSelection"
         then hSelectionPerson:visible = false.
         else if hSelectionPerson:list-item-pairs > "" and not hSelectionPerson:visible
         then hSelectionPerson:visible = true.
    end.

  cbEntity:screen-value = {&All}.
  cbStatus:screen-value = {&new}.
  
  run getSysUser in this-procedure.
  
  apply "value-changed" to cbEntity.


  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionValueChanged C-Win 
PROCEDURE actionValueChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* Hide both the combo-boxes. Depending on the 
     value of cbEntity combo-box, the label and 
     combo-boxes will be shown. */
  if cbEntity:input-value = {&All} then
  do:
    assign 
      cbEntityID:visible   = true
      cbEntityID:sensitive = false
      cbEntityId:label     = "Entity ID"
      cbAgent:label        = ""      
      cbPerson:label       = ""
      .
     
    run agentComboHide(true).
    run agentComboEnable(false).
    run personComboHide(true).
    run personComboEnable(false).
  
    run PersonComboClear in this-procedure.
    run AgentComboClear in this-procedure.
  end.
  
  
  /* Populating the combo box here for what the new requirement going to be made
     its for Agent, Person, User*/
     
  /* Populating for Agent */
  else if cbEntity:input-value = {&Agent}
   then
    do:
     assign
         cbEntityID:visible       = false                      
         cbEntityID:sensitive     = false
         cbEntityId:label         = ""
         cbAgent:label            = "Agent ID"    
         cbPerson:label           = ""
         .
         
     run AgentComboHide(false).
     run AgentComboEnable(true).
     run PersonComboHide(true).
     run PersonComboEnable(false).
     
     run PersonComboClear in this-procedure.
    end.
   /* Populating for Person */
   else if cbEntity:input-value = {&Person}
   then
    do:
     assign
         cbEntityID:visible   = false                     
         cbEntityID:sensitive = false 
         cbEntityId:label     = "" 
         cbAgent:label        = ""   
         cbPerson:label       = "Person ID" 
         .
         
     run PersonComboHide(false).
     run PersonComboEnable(true).
     run agentComboHide(true).
     run agentComboEnable(false).
     
     run AgentComboClear in this-procedure.

    end.
    /* Populating for Employee */
   else if cbEntity:input-value = {&Employee}
    then
     do:
      assign
          cbEntityID:visible    = true                      
          cbEntityID:sensitive  = true
          cbEntityId:label      = "Emp ID"
          cbAgent:label         = ""    
          cbPerson:label        = ""
          .
          
      run agentComboHide(true).
      run agentComboEnable(false).
      run PersonComboHide(true).
      run PersonComboEnable(false).
      
      cbEntityId:move-to-top().    
         
      run AgentComboClear in this-procedure.
      run PersonComboClear in this-procedure.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbSysUser cbstatus cbentity cbAgent cbEntityID cbPerson fStartDate 
          fEndDate fSearch 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE cbSysUser bGet cbstatus cbentity fStartDate fEndDate brwData bRefresh 
         RECT-62 RECT-63 RECT-61 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if query brwData:num-results = 0 
   then
    do:
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.

  publish "GetReportDir" (output std-ch).

  
  std-ha = temp-table ttsysnotification:handle.
   run util/exporttable.p (table-handle std-ha,
                          "ttsysnotification",
                          "for each ttsysnotification",
                          "uid,entityType,entityID,stat,notificationStat,createDate,sourceType,sourceID,notification,dismissedDate",
                          "From " + fStartDate:screen-value + " To " + fEndDate:screen-value + chr(10) + "User ID,Entity,Entity ID,Status,Notification Status,Create Date,Source,Source ID,Notification,Dismissed Date",
                          std-ch,
                          "SystemNotification-" + replace(string(now,"99-99-99"),"-","") + "-" + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-lo).      

  if std-ch <> ""
   then
    do:
      message std-ch
          view-as alert-box warning buttons ok.
      return.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount  as integer   no-undo.
  define variable cStatus as character no-undo.
  
  define buffer sysnotification for sysnotification.

  close query brwData.
  empty temp-table ttsysnotification.
  
  do with frame {&frame-name}:
  end.
 
  for each sysnotification:
    /* test if the record contains the search text */
    if cLastSearchString <> "" and 
      not ( (sysnotification.entityType         matches "*" + cLastSearchString + "*")  or 
            (sysnotification.UID                matches "*" + cLastSearchString + "*")  or 
            (sysnotification.stat               matches "*" + cLastSearchString + "*")  or
            (sysnotification.notificationstat   matches "*" + cLastSearchString + "*")  or 
            (sysnotification.sourceID           matches "*" + cLastSearchString + "*")  or 
            (sysnotification.notification       matches "*" + cLastSearchString + "*")
             )
     then 
      next.
    create ttsysnotification. 
    buffer-copy sysnotification to ttsysnotification.
  end.

  open query brwData preselect each ttsysnotification by ttsysnotification.createDate desc.

  if (query brwData:num-results) > 0
   then
    assign
      bExport:sensitive = true
      bView:sensitive   = true.
   else
    assign
      bExport:sensitive = false
      bView:sensitive   = false.

  setStatusCount(query brwData:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  run server/getsysnotifications.p(input date(fStartDate:input-value),
                                  input date(fEndDate:input-value),
                                  input cbEntity:input-value,     /*Entity*/
                                  input (if cbEntity:input-value eq {&Agent} then cbAgent:input-value 
                                         else if cbEntity:input-value eq {&Person} then cbPerson:input-value 
                                         else cbEntityID:input-value), /*cbEntityID*/
                                  input cbSysUser:input-value,     /*UID*/
                                  input cbStatus:input-value,
                                  output table sysNotification,
                                  output std-lo,
                                  output std-ch).
  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.
   
  run filterData.

  if (query brwData:num-results) > 0
   then
    assign
      fSearch:sensitive = true
      bSearch:sensitive = true
      .
   else
    assign
      fSearch:sensitive = false
      bSearch:sensitive = false
      .

  setStatusRecords(query brwData:num-results).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getSysUser C-Win 
PROCEDURE getSysUser :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
  end.

  define variable cUserList  as character  no-undo.

  /* returns the list of sys user */
  publish "getSysUserList" (",",
                            output cUserList,
                            output std-lo,
                            output std-ch ).
  if not std-lo
   then
    do:
      message std-ch
           view-as alert-box error buttons ok.
      return.
    end.

  assign
      cbEntityID:list-items   = "ALL" + "," + trim(cUserList,",")
      cbSysUser:list-items    = "ALL" + "," + trim(cUserList,",")
      cbSysUser:screen-value  = "ALL"
      .    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData C-Win 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Get default no. of days from dialog config to initialise start date */
  publish "getDefaultdays" (output std-in).

  assign
    fStartDate:screen-value = string(today - std-in)
    fEndDate:screen-value   = string(today)
    .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showSysNotification C-Win 
PROCEDURE showSysNotification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  define buffer tsysnotification for tsysnotification.
  
  empty temp-table tsysnotification.
  
  if available ttsysnotification
   then
    do:
      create tsysnotification.
      buffer-copy ttsysnotification to tsysnotification.
      run dialogsysnotification.w (input table tsysnotification).
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized
   then
    {&window-name}:window-state = window-normal .
  
  c-Win:move-to-top(). 

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i}   

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
    frame fMain:width-pixels          = {&window-name}:width-pixels
    frame fMain:virtual-width-pixels  = {&window-name}:width-pixels
    frame fMain:height-pixels         = {&window-name}:height-pixels
    frame fMain:virtual-height-pixels = {&window-name}:height-pixels

    /* fMain Components */
    brwData:width-pixels              = frame fmain:width-pixels  - 13
    brwData:height-pixels             = frame fMain:height-pixels - {&browse-name}:y - 7
    .

  {lib/resize-column.i &col="'createDate'" &var=dColumnWidth}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION updateEntity C-Win 
FUNCTION updateEntity RETURNS CHARACTER
  (cEntity as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  if cEntity = {&Agent}
   then 
    return "Agent".
   else if cEntity = {&Person}
    then 
     return "Person".
   else if cEntity = {&Employee}
    then 
     return "Employee".
   else if cEntity <> "" or cEntity = "" 
    then
     return "".  

  return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION updateNotificationStatus C-Win 
FUNCTION updateNotificationStatus RETURNS CHARACTER
  (cNotificationStatus as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cNotificationStatus = {&Yellow}
   then 
    do:
        return {&YellowValue}.
    end.
   else if cNotificationStatus = {&Red}
   then 
    do:
        return {&RedValue}.
    end.
   else if cNotificationStatus = {&Green}
   then 
    do:
        return {&GreenValue}.
    end.
  RETURN "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION updateStatus C-Win 
FUNCTION updateStatus RETURNS CHARACTER
  (cStatus as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  if cStatus = {&New}
   then 
    do:
        return {&NewValue}.
    end.
   else if cStatus = {&Supress}
   then 
    do:
        return {&SupressValue}.
    end.
   else if cStatus = {&Dismiss}
   then 
    do:
        return {&DismissValue}.
    end.

  RETURN "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validateData C-Win 
FUNCTION validateData RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if interval(fEndDate:input-value, fStartDate:input-value, "days") > 30 
   then
    do:
      message "The date range is greater than 30 days, the report will be slow. Are you sure you want to continue?" 
        view-as alert-box warning buttons yes-no update std-lo.
     
      if not std-lo 
       then
        return false.
    end.
 
  if interval(fEndDate:input-value, fStartDate:input-value, "years") > 0
  then
    do:
      message "Date range cannot be more than a year." 
        view-as alert-box error buttons ok.
     
      return false.
    end.

  return true.   /* function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

