&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogsyscode.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gurvindar Singh

  Created:09/26/18
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/sys-def.i}
{tt/syscode.i}
{tt/syscode.i &tableAlias=ttsyscode}


/* Parameters Definitions ---                                           */
define input  parameter ipcAction     as  character no-undo.
define input  parameter table         for ttsyscode.
define output parameter opcCodeType   as  character   no-undo.
define output parameter opcCode       as  character   no-undo.
define output parameter oplSuccess    as  logical     no-undo.


/* Local Variable Definitions ---                                       */
define variable cTrackCodeType as character no-undo.
define variable cTrackCode     as character no-undo.
define variable lTrackSecure   as logical   no-undo.
define variable cTrackType     as character no-undo.
define variable cTrackDesc     as character no-undo.
define variable cTrackComment  as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbCodeType tsecure fType fdesc eComment ~
Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS cbCodeType fCode tsecure fType fdesc ~
eComment 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 12 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 12 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cbCodeType AS CHARACTER 
     LABEL "CodeType" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN AUTO-COMPLETION
     SIZE 62.6 BY 1 TOOLTIP "Select code or type new code" NO-UNDO.

DEFINE VARIABLE eComment AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 62.6 BY 2.81 NO-UNDO.

DEFINE VARIABLE fdesc AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 62.6 BY 2.81 NO-UNDO.

DEFINE VARIABLE fCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Code" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 30.6 BY 1 NO-UNDO.

DEFINE VARIABLE fType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 62.6 BY 1 NO-UNDO.

DEFINE VARIABLE tsecure AS LOGICAL INITIAL no 
     LABEL "Secure" 
     VIEW-AS TOGGLE-BOX
     SIZE 12 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     cbCodeType AT ROW 1.48 COL 12.2 COLON-ALIGNED WIDGET-ID 30
     fCode AT ROW 2.62 COL 12.2 COLON-ALIGNED WIDGET-ID 2
     tsecure AT ROW 2.71 COL 46.6 WIDGET-ID 24
     fType AT ROW 3.76 COL 12.2 COLON-ALIGNED WIDGET-ID 32
     fdesc AT ROW 4.91 COL 14.2 NO-LABEL WIDGET-ID 34
     eComment AT ROW 7.86 COL 14.2 NO-LABEL WIDGET-ID 26
     Btn_OK AT ROW 11 COL 26.8
     Btn_Cancel AT ROW 11 COL 41
     "Description:" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 4.91 COL 2.8 WIDGET-ID 36
     "Comment:" VIEW-AS TEXT
          SIZE 9.4 BY .62 AT ROW 7.86 COL 4.6 WIDGET-ID 28
     SPACE(64.99) SKIP(4.13)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "System Code"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       eComment:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN fCode IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fdesc:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* System Code */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  run saveSystemCode in this-procedure.
  if not oplSuccess 
   then
    return no-apply.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbCodeType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbCodeType Dialog-Frame
ON VALUE-CHANGED OF cbCodeType IN FRAME Dialog-Frame /* CodeType */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eComment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eComment Dialog-Frame
ON VALUE-CHANGED OF eComment IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fCode Dialog-Frame
ON VALUE-CHANGED OF fCode IN FRAME Dialog-Frame /* Code */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fdesc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fdesc Dialog-Frame
ON VALUE-CHANGED OF fdesc IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fType Dialog-Frame
ON VALUE-CHANGED OF fType IN FRAME Dialog-Frame /* Type */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tsecure
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tsecure Dialog-Frame
ON VALUE-CHANGED OF tsecure IN FRAME Dialog-Frame /* Secure */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  run displayData in this-procedure.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cCodeTypeList as character   no-undo.

  do with frame {&frame-name}:
  end.
  
  /* Get the list of all codeType present in sysdatasrv */
  publish "getSysCodeTypeListItems" (output cCodeTypeList,
                                     output std-lo,
                                     output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.

  cbCodeType:list-items = cCodeTypeList.

  case ipcAction:
    when {&New}
     then
      assign 
        frame dialog-frame:title = "New Code"
        Btn_OK :label            = "Create" 
        Btn_OK :tooltip          = "Create"
        fcode:sensitive          = true
        cbCodeType:sensitive     = true
        .
    when {&Modify} 
     then
      do:
        find first ttsyscode no-error.
        if available ttsysCode 
         then            
           assign
             frame dialog-frame:title   = "Edit Code"                                                           
             fcode:sensitive            = false
             cbCodeType:sensitive       = false
             Btn_OK :label              = "Save" 
             Btn_OK :tooltip            = "Save"  
             cbCodeType:screen-value    = ttsyscode.codeType
             fcode:screen-value         = ttsyscode.code
             tsecure:checked            = if ttsyscode.issecure = ? then no else ttsyscode.issecure
             fType:screen-value         = ttsyscode.type
             fdesc:screen-value         = ttsyscode.description            
             ecomment:screen-value      = ttsyscode.comments
             .            
      end.       

    when {&copy} 
     then
      do:
        find first ttsyscode no-error.
        if available ttsysCode 
         then            
           assign
             frame dialog-frame:title   = "Copy Code"
             fcode:sensitive            = true
             cbCodeType:sensitive       = true
             Btn_OK :label              = "Create" 
             Btn_OK :tooltip            = "Create" 
             cbCodeType:screen-value    = ttsyscode.codeType
             fcode:screen-value         = ttsyscode.code
             tsecure:checked            = if ttsyscode.issecure = ? then no else ttsyscode.issecure
             fType:screen-value         = ttsyscode.type
             fdesc:screen-value         = ttsyscode.description            
             ecomment:screen-value      = ttsyscode.comments
             .            
      end.
  end case.  

  assign
    cTrackCodeType = trim(cbCodeType:input-value)
    cTrackCode     = trim(fcode:input-value)
    lTrackSecure   = tsecure:checked
    cTrackType     = trim(fType:input-value)
    cTrackDesc     = trim(fdesc:input-value)
    cTrackComment  = trim(ecomment:input-value)
    .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  if ipcAction = {&New}
   then
    Btn_OK:sensitive = (cbCodeType:input-value <> "" and  
                        cbCodeType:input-value <> ?  and
                        fcode:input-value      <> "")
                        .
  else if ipcAction = {&Modify}                     
   then
    Btn_OK:sensitive = not(lTrackSecure   = tsecure:checked           and    
                           cTrackType     = trim(fType:input-value)   and    
                           cTrackDesc     = trim(fdesc:input-value)   and    
                           cTrackComment  = trim(ecomment:input-value)
                           ).
  else   
    Btn_OK:sensitive = ((cbCodeType:input-value <> "" and 
                         cbCodeType:input-value <> ?  and 
                         cTrackCodeType <> cbCodeType:input-value) or
                        (fcode:input-value <> ""  and
                         cTrackCode <> fcode:input-value)
                        ). 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbCodeType fCode tsecure fType fdesc eComment 
      WITH FRAME Dialog-Frame.
  ENABLE cbCodeType tsecure fType fdesc eComment Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveSystemCode Dialog-Frame 
PROCEDURE saveSystemCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.   
  empty temp-table ttsyscode.
  create ttsyscode.
  assign
    ttsyscode.codetype      = cbCodeType:input-value
    ttsyscode.type          = fType     :input-value
    ttsyscode.code          = fcode     :input-value   
    ttsyscode.description   = fdesc     :input-value
    ttsyscode.isSecure      = tsecure   :input-value
    ttsyscode.comments      = ecomment  :input-value
    .
  assign
    opcCodeType = cbCodeType:input-value
    opcCode     = fcode:input-value
    .

  case ipcAction:
    when {&Modify} 
     then
      publish "modifySysCode" (input table ttsyscode,
                               output oplSuccess,
                               output std-ch).

    when {&New} 
     then
      publish "newSysCode" (input table ttsyscode,
                            output oplSuccess,
                            output std-ch).
    otherwise /* Copy */    
      publish "newSysCode" (input table ttsyscode,
                            output oplSuccess,
                            output std-ch).
  end case.  

  if not oplSuccess
   then 
    message std-ch
      view-as alert-box info buttons ok.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

