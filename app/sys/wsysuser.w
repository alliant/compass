&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wsysuser.w

  Description: User Interface for system Users

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gurvindar

  Created: 09.25.2018
  
  Modified:
 
  Date       Name      Description
 
  08/22/2019 Gurvindar Removed progress error while populating combo-box.
  12/17/2019 Rahul     Records sorted by sysUser.uid

------------------------------------------------------------------------*/
create widget-pool.

/* Standard Library Files */
{lib/std-def.i}
{lib/sys-def.i}
{lib/set-filter-def.i &tableName="sysuser"}

/* Temp-table Definition */
{tt/sysuser.i}
{tt/sysuser.i &tableAlias=ttsysuser}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES sysuser

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData sysUser.isActive sysuser.uid sysuser.name sysuser.initials sysuser.department sysuser.email sysuser.role  
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each sysuser by sysuser.UID
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each sysuser by sysuser.UID.
&Scoped-define TABLES-IN-QUERY-brwData sysuser
&Scoped-define FIRST-TABLE-IN-QUERY-brwData sysuser


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-61 RECT-62 fDept fSearch brwData bNew ~
bSearch bExport bRefresh 
&Scoped-Define DISPLAYED-OBJECTS fDept fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bActiveUsr  NO-FOCUS
     LABEL "Active User" 
     SIZE 7.2 BY 1.71 TOOLTIP "Activate".

DEFINE BUTTON bChangePwd  NO-FOCUS
     LABEL "Change Pwd" 
     SIZE 7.2 BY 1.71 TOOLTIP "Change Password".

DEFINE BUTTON bcopy  NO-FOCUS
     LABEL "Copy" 
     SIZE 7.2 BY 1.71 TOOLTIP "Copy".

DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete".

DEFINE BUTTON bEdit  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Edit".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "New".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search Data".

DEFINE VARIABLE fDept AS CHARACTER FORMAT "X(256)":U 
     LABEL "Department" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 44 BY 1 TOOLTIP "Search Criteria (Uid,Name,Role)" NO-UNDO.

DEFINE RECTANGLE RECT-61
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 60.8 BY 2.14.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 102.6 BY 2.14.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      sysuser SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      sysUser.isActive column-label "Active"    view-as toggle-box 
sysuser.uid label "UID" width 37            
sysuser.name width 26      
sysuser.initials width 6
sysuser.department width 20
sysuser.email width 40    
sysuser.role width 20
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 163 BY 17.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bActiveUsr AT ROW 1.62 COL 54.8 WIDGET-ID 56 NO-TAB-STOP 
     fDept AT ROW 2 COL 74.6 COLON-ALIGNED WIDGET-ID 322
     fSearch AT ROW 2 COL 110.4 COLON-ALIGNED WIDGET-ID 310
     brwData AT ROW 4.1 COL 2 WIDGET-ID 200
     bChangePwd AT ROW 1.62 COL 47.4 WIDGET-ID 54 NO-TAB-STOP 
     bcopy AT ROW 1.62 COL 40 WIDGET-ID 316 NO-TAB-STOP 
     bDelete AT ROW 1.62 COL 32.6 WIDGET-ID 206 NO-TAB-STOP 
     bEdit AT ROW 1.62 COL 25.2 WIDGET-ID 8 NO-TAB-STOP 
     bNew AT ROW 1.62 COL 17.8 WIDGET-ID 6 NO-TAB-STOP 
     bSearch AT ROW 1.62 COL 157 WIDGET-ID 308 NO-TAB-STOP 
     bExport AT ROW 1.62 COL 3 WIDGET-ID 2 NO-TAB-STOP 
     bRefresh AT ROW 1.62 COL 10.4 WIDGET-ID 4 NO-TAB-STOP 
     "Filter" VIEW-AS TEXT
          SIZE 4.6 BY .62 AT ROW 1.05 COL 63.4 WIDGET-ID 320
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1 COL 2.8 WIDGET-ID 52
     RECT-61 AT ROW 1.43 COL 2.2 WIDGET-ID 50
     RECT-62 AT ROW 1.43 COL 62.6 WIDGET-ID 312
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 178.8 BY 21.52
         DEFAULT-BUTTON bSearch WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Users"
         HEIGHT             = 21.05
         WIDTH              = 165
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData fSearch fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bActiveUsr IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bChangePwd IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bcopy IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDelete IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bDelete:PRIVATE-DATA IN FRAME fMain     = 
                "A".

/* SETTINGS FOR BUTTON bEdit IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each sysuser by sysuser.UID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Users */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Users */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Users */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bActiveUsr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActiveUsr C-Win
ON CHOOSE OF bActiveUsr IN FRAME fMain /* Active User */
do:
  if not available sysuser 
   then return.

  if sysuser.isActive 
   then
    run deactivateUser in this-procedure.
   else
    run activateUser in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bChangePwd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bChangePwd C-Win
ON CHOOSE OF bChangePwd IN FRAME fMain /* Change Pwd */
DO:
  run changePassword in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bcopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bcopy C-Win
ON CHOOSE OF bcopy IN FRAME fMain /* Copy */
do:
  run copyUser in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME fMain /* Delete */
do:
  run deleteUser in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEdit C-Win
ON CHOOSE OF bEdit IN FRAME fMain /* Edit */
do:
  run modifyUser in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
do:
  run newUser in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
do:
  run refreshData in this-procedure.                                    
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
do:
 run modifyUser in this-procedure.  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  run changeFlagStatus in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME fMain /* Search */
DO:
  dataSortDesc = not dataSortDesc.
  run sortData (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fDept
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDept C-Win
ON VALUE-CHANGED OF fDept IN FRAME fMain /* Department */
DO:
  dataSortDesc = not dataSortDesc.
  run sortData (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fMain /* Search */
DO:
  apply "CHOOSE" to bSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

on close of this-procedure 
  run disable_UI.

pause 0 before-hide.
subscribe to "closeWindow" anywhere.

setStatusMessage("").

/* Setting button images */
bRefresh  :load-image("images/refresh.bmp").

bExport   :load-image            ("images/excel.bmp").
bExport   :load-image-insensitive("images/excel-i.bmp").

bnew      :load-image            ("images/add.bmp").
bnew      :load-image-insensitive("images/add-i.bmp").

bEdit     :load-image            ("images/update.bmp").
bEdit     :load-image-insensitive("images/update-i.bmp").

bdelete   :load-image            ("images/delete.bmp").
bdelete   :load-image-insensitive("images/delete-i.bmp").

bcopy     :load-image-up         ("images/copy.bmp").
bcopy     :load-image-insensitive("images/copy-i.bmp").

bChangePwd:load-image            ("images/lock.bmp").
bChangePwd:load-image-insensitive("images/lock-i.bmp").

bActiveUsr:load-image            ("images/flag_red.bmp").
bActiveUsr:load-image-insensitive("images/flag-i.bmp").

bSearch   :load-image-up         ("images/magnifier.bmp").
bSearch   :load-image-insensitive("images/magnifier-i.bmp").

{lib/set-filter.i &label="Dept"   &id="department"    &useSingle=true}
{lib/set-filter.i &label="Search" &id="name:role:uid" &populate=false}

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.

  run getData in this-procedure.
  fDept:screen-value = "ALL".
  
  publish "GetCurrentValue" ("RefreshUsers", output std-ch).

  if logical(std-ch) 
   then
    /* Set Status count with date and time from the server */
    setStatusRecords(query brwData:num-results).

  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.
   
  run windowResized in this-procedure.

  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE activateUser C-Win 
PROCEDURE activateUser :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cUid as character no-undo.
  
  if not available sysuser 
   then return.
 
  cUid = sysuser.uid.

  publish "activateSysUser" (input sysuser.uid,
                             output std-lo,
                             output std-ch).
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.
    
  run getdata in this-procedure.

  find first sysuser where sysuser.uid = cUid no-error.
  if not available sysuser 
   then return.
       
  reposition {&browse-name} to rowid rowid(sysuser).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changeFlagStatus C-Win 
PROCEDURE changeFlagStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available sysuser 
   then return.
  
  do with frame {&frame-name}:
  end.

  if sysuser.isActive 
   then
    do:
      bActiveUsr:load-image("images/flag_red.bmp").
      bActiveUsr:tooltip = "Deactivate".
    end.
   else
    do:
      bActiveUsr:load-image("images/flag_green.bmp").
      bActiveUsr:tooltip = "Activate".
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changePassword C-Win 
PROCEDURE changePassword :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available sysuser 
   then return.
    
  assign 
      std-ch = sysuser.password
      std-ro = rowid(sysuser)
      .

  run dialogchangepwd.w (input sysuser.uid).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  APPLY "CLOSE":U TO THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copyUser C-Win 
PROCEDURE copyUser :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cUid as character  no-undo.

  define buffer ttsysuser for ttsysuser.
  empty temp-table ttsysuser.

  if not available sysuser 
   then
    return.

  create ttsysuser.
  buffer-copy sysuser to ttsysuser.

  run dialognewsysuser.w (input table ttsysuser,
                          output cUid, /* UID */
                          output std-lo).

  if not std-lo 
   then
    return.
  
  run getData in this-procedure.

  find first sysuser where sysuser.uid = cUid no-error.
  if not available sysuser
   then return.

  reposition {&browse-name} to rowid rowid(sysuser). 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deactivateUser C-Win 
PROCEDURE deactivateUser :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  define variable cUid as character no-undo.

  if not available sysuser 
   then return.
 
  cUid = sysuser.uid.

  publish "deactivateSysUser" (input sysuser.uid,
                               output std-lo,
                               output std-ch).
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.

  run getdata in this-procedure.

  find first sysuser where sysuser.uid = cUid no-error.
  if not available sysuser 
   then return.
       
  reposition {&browse-name} to rowid rowid(sysuser). 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteUser C-Win 
PROCEDURE deleteUser :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available sysuser 
   then
    return.

  message "User will be deleted. Are you sure you want to delete the selected user?"
      view-as alert-box question buttons yes-no
      title "Delete sysuser"
      update std-lo.
  
  if not std-lo 
   then return.

  publish "deleteSysUser" (input sysuser.uid,                                
                           output std-lo,
                           output std-ch).

  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.

  run getData in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fDept fSearch 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-61 RECT-62 fDept fSearch brwData bNew bSearch bExport bRefresh 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/                                                                                         
  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table sysuser:handle.
  run util/exporttable.p (table-handle std-ha,
                          "sysuser",
                          "for each sysuser ",
                          "uid,name,initials,email,role,isActive,createDate,comments,passwordSetDate,passwordExpired",
                          "UID,Name,Initials,Email,Role,Is Active,Create Date,Comments,Password Set Date,Password Expired",
                          std-ch,
                          "System User.csv-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cCurrentDeptId as character  no-undo.

  do with frame {&frame-name}:
  end.
    
  /* Retain previous combo-box value */
  cCurrentDeptId = fDept:input-value.

  publish "getSysUsers" (output table sysuser,
                         output std-lo,
                         output std-ch).

  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box info buttons ok.
      return.
    end.
  
  dataSortBy = "".
  run sortData ("").
  run setWidgetState in this-procedure.
  setFilterCombos("ALL").
  enableFilters(can-find(first sysuser)).
  
  apply 'value-changed' to browse brwData.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyUser C-Win 
PROCEDURE modifyUser :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cUid as character no-undo.

  define buffer ttsysuser for ttsysuser.
  empty temp-table ttsysuser.

  if not available sysuser 
   then
    return.

  create ttsysuser.
  buffer-copy sysuser to ttsysuser.
  
  cUid = sysuser.uid.

  run dialogmodifysysuser.w (input table ttsysuser,                               
                             output std-lo).

  if not std-lo 
   then
    return.

  run getData in this-procedure.

  find first sysuser where sysuser.uid = cUid no-error.
  if not available sysuser 
   then return.
    
  reposition {&browse-name} to rowid rowid(sysuser).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newUser C-Win 
PROCEDURE newUser :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cUid as character no-undo.

  define buffer ttsysuser for ttsysuser.
  empty temp-table ttsysuser.
 
  run dialognewsysuser.w (input table ttsysuser,
                          output cUid, /* UID */
                          output std-lo).
 
  if not std-lo 
   then
    return.
 
  run getData in this-procedure.
 
  find first sysuser where sysuser.uid = cUid no-error.
  if not available sysuser 
   then return.
  
  reposition {&browse-name} to rowid rowid(sysuser).   

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData C-Win 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "refreshSysUsers" (output std-lo,
                             output std-ch). 
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.

  run getData in this-procedure.
  
  /* Set Status count with date and time from the server */
  setStatusRecords(query brwData:num-results).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.

 if query brwData:num-results > 0 
  then
   assign
       bEdit:sensitive      = true
       bcopy:sensitive      = true
       bExport:sensitive    = true
       bDelete:sensitive    = true
       bChangePwd:sensitive = true
       bActiveUsr:sensitive = true
       .  
  else
   assign
       bEdit:sensitive      = false
       bcopy:sensitive      = false
       bExport:sensitive    = false
       bDelete:sensitive    = false
       bChangePwd:sensitive = false
       bActiveUsr:sensitive = false
       .  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .
  
  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i &pre-by-clause="getWhereClause() +"}
  setStatusCount(query brwData:num-results).   

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame fMain:width-pixels          = {&window-name}:width-pixels
      frame fMain:virtual-width-pixels  = {&window-name}:width-pixels
      frame fMain:height-pixels         = {&window-name}:height-pixels
      frame fMain:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels       = frame fmain:width-pixels - 10
      {&browse-name}:height-pixels      = frame fMain:height-pixels - {&browse-name}:y - 2
      .
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

