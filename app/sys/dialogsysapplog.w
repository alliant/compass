&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogsysapplog.p

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Sachin Chaturvedi

  Created:01/22/2020
  @Modification:
  Date          Name          Description
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{tt/sysapplog.i}
{lib/std-def.i}

/* Parameters Definitions ---                                           */
define input parameter table for sysapplog.

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS flAppCode fSeq flEntityType flEntityID ~
flEntityName flObjAction flObjID flObjName edobjDesc flObjRef flLogDate ~
flUserName 
&Scoped-Define DISPLAYED-OBJECTS flAppCode fSeq flEntityType flEntityID ~
flEntityName flObjAction flObjID flObjName edobjDesc flObjRef flLogDate ~
flUserName 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE VARIABLE edobjDesc AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 60.4 BY 3.38 NO-UNDO.

DEFINE VARIABLE flAppCode AS CHARACTER FORMAT "X(20)":U 
     LABEL "Code" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE flEntityID AS CHARACTER FORMAT "X(50)":U 
     LABEL "ID" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE flEntityName AS CHARACTER FORMAT "X(50)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 60.4 BY 1 NO-UNDO.

DEFINE VARIABLE flEntityType AS CHARACTER FORMAT "X(50)":U 
     LABEL "Type" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE flLogDate AS DATETIME FORMAT "99/99/99 HH:MM:SS":U 
     LABEL "Log Date" 
     VIEW-AS FILL-IN 
     SIZE 60.4 BY 1 NO-UNDO.

DEFINE VARIABLE flObjAction AS CHARACTER FORMAT "X(100)":U 
     LABEL "Action" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE flObjID AS CHARACTER FORMAT "X(100)":U 
     LABEL "Object ID" 
     VIEW-AS FILL-IN 
     SIZE 60.4 BY 1 NO-UNDO.

DEFINE VARIABLE flObjName AS CHARACTER FORMAT "X(200)":U 
     LABEL "Object" 
     VIEW-AS FILL-IN 
     SIZE 60.4 BY 1 NO-UNDO.

DEFINE VARIABLE flObjRef AS CHARACTER FORMAT "X(200)":U 
     LABEL "Reference" 
     VIEW-AS FILL-IN 
     SIZE 60.4 BY 1 NO-UNDO.

DEFINE VARIABLE flUserName AS CHARACTER FORMAT "X(50)":U 
     LABEL "User" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE fSeq AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     LABEL "Sequence" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     flAppCode AT ROW 1.52 COL 16.6 COLON-ALIGNED WIDGET-ID 26
     fSeq AT ROW 1.48 COL 57 COLON-ALIGNED WIDGET-ID 44
     flEntityType AT ROW 2.71 COL 16.6 COLON-ALIGNED WIDGET-ID 20
     flEntityID AT ROW 2.67 COL 57 COLON-ALIGNED WIDGET-ID 2
     flEntityName AT ROW 3.91 COL 16.6 COLON-ALIGNED WIDGET-ID 32
     flObjAction AT ROW 5.1 COL 16.6 COLON-ALIGNED WIDGET-ID 42
     flObjID AT ROW 6.29 COL 16.6 COLON-ALIGNED WIDGET-ID 22
     flObjName AT ROW 7.48 COL 16.6 COLON-ALIGNED WIDGET-ID 6
     edobjDesc AT ROW 8.67 COL 18.6 NO-LABEL WIDGET-ID 40
     flObjRef AT ROW 12.24 COL 16.6 COLON-ALIGNED WIDGET-ID 16
     flLogDate AT ROW 13.43 COL 16.6 COLON-ALIGNED WIDGET-ID 38
     flUserName AT ROW 14.57 COL 16.6 COLON-ALIGNED WIDGET-ID 34
     "Description:" VIEW-AS TEXT
          SIZE 11.8 BY .62 AT ROW 8.71 COL 6.6 WIDGET-ID 30
     SPACE(62.79) SKIP(6.57)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Application Log" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       edobjDesc:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flAppCode:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flEntityID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flEntityName:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flEntityType:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flLogDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flObjAction:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flObjID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flObjName:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flObjRef:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flUserName:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fSeq:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Application Log */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  run enable_UI.
  
  /* sets default values to the widgets */
  run setData.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.

RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flAppCode fSeq flEntityType flEntityID flEntityName flObjAction 
          flObjID flObjName edobjDesc flObjRef flLogDate flUserName 
      WITH FRAME Dialog-Frame.
  ENABLE flAppCode fSeq flEntityType flEntityID flEntityName flObjAction 
         flObjID flObjName edobjDesc flObjRef flLogDate flUserName 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData Dialog-Frame 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  frame dialog-frame:title = "View Application Log".

  find first sysapplog no-error.
  if not available sysapplog 
   then
    return.

  assign 
      flAppCode:screen-value     = sysapplog.appCode
      fSeq:screen-value          = string(sysapplog.entitySeq)
      flEntityType:screen-value  = sysapplog.entityType
      flEntityID:screen-value    = sysapplog.entityID
      flEntityName:screen-value  = sysapplog.entityname 
      flObjAction:screen-value   = sysapplog.objAction 
      flObjID:screen-value       = sysapplog.objID 
      flObjName:screen-value     = sysapplog.ObjName
      edobjDesc:screen-value     = sysapplog.objDesc  
      flObjRef:screen-value      = sysapplog.objRef
      flLogDate:screen-value     = string(sysapplog.logDate)      
      flUserName:screen-value    = sysapplog.username 
      .     
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

