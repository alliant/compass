&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File:wsysrole.w 

  Description:U 

  Input Parameters:
     .

  Output Parameters:
      .

  Author: Gurvindar 

  Created:09.28.2018 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool. 
/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/sys-def.i}
{lib/winlaunch.i}
{lib/brw-multi-def.i}
{tt/sysrole.i}
{tt/sysrole.i &tableAlias=tsysrole}
{tt/sysrole.i &tableAlias=ttsysrole}
{tt/sysuser.i}
{tt/sysuser.i &tableAlias=tsysuser} 

/* Local Variable Definitions ---                                       */
define variable cRole as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwsysrole

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES sysrole sysuser

/* Definitions for BROWSE brwsysrole                                    */
&Scoped-define FIELDS-IN-QUERY-brwsysrole sysrole.roleid sysrole.Description sysrole.comments   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwsysrole   
&Scoped-define SELF-NAME brwsysrole
&Scoped-define QUERY-STRING-brwsysrole for each sysrole
&Scoped-define OPEN-QUERY-brwsysrole open query {&SELF-NAME} for each sysrole.
&Scoped-define TABLES-IN-QUERY-brwsysrole sysrole
&Scoped-define FIRST-TABLE-IN-QUERY-brwsysrole sysrole


/* Definitions for BROWSE brwSysUsers                                   */
&Scoped-define FIELDS-IN-QUERY-brwSysUsers sysUser.isActive sysUser.uid "UID" sysUser.name "Name" sysUser.initials "Initial" sysUser.email "Email" sysUser.role "Role"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwSysUsers   
&Scoped-define SELF-NAME brwSysUsers
&Scoped-define QUERY-STRING-brwSysUsers for each sysuser where lookup("*", ~
      sysuser.role) > 0 or lookup(cRole, ~
      sysuser.role) > 0
&Scoped-define OPEN-QUERY-brwSysUsers open query {&SELF-NAME} for each sysuser where lookup("*", ~
      sysuser.role) > 0 or lookup(cRole, ~
      sysuser.role) > 0.
&Scoped-define TABLES-IN-QUERY-brwSysUsers sysuser
&Scoped-define FIRST-TABLE-IN-QUERY-brwSysUsers sysuser


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwsysrole}~
    ~{&OPEN-QUERY-brwSysUsers}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-62 RECT-63 fSearch brwsysrole ~
brwSysUsers bNew bRefresh bCopy bEdit bSearch 
&Scoped-Define DISPLAYED-OBJECTS fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCopy  NO-FOCUS
     LABEL "Copy" 
     SIZE 7.2 BY 1.71 TOOLTIP "Copy".

DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete Sysrole".

DEFINE BUTTON bEdit  NO-FOCUS
     LABEL "Edit" 
     SIZE 4.8 BY 1.14 TOOLTIP "Edit User".

DEFINE BUTTON bEditsysrole  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Edit Sysrole".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bGrpAssignAct  NO-FOCUS
     LABEL "Assign Action" 
     SIZE 7.2 BY 1.71 TOOLTIP "Assign Role to Action".

DEFINE BUTTON bGrpAssignUsr  NO-FOCUS
     LABEL "Assign User" 
     SIZE 7.2 BY 1.71 TOOLTIP "Assign Role to User".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add Sysrole".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload Data".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search".

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 46.8 BY 1 TOOLTIP "Search Criteria (Role, Comments, Description)" NO-UNDO.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 60.8 BY 2.19.

DEFINE RECTANGLE RECT-63
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 56.8 BY 2.19.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwsysrole FOR 
      sysrole SCROLLING.

DEFINE QUERY brwSysUsers FOR 
      sysuser SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwsysrole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwsysrole C-Win _FREEFORM
  QUERY brwsysrole DISPLAY
      sysrole.roleid   label "Role ID"        format "x(20)"     width 20
sysrole.Description    label "Description"   format "x(150)"    width 75
sysrole.comments       label "Comment"       format "x(150)"    width 20
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SEPARATORS NO-TAB-STOP SIZE 158 BY 13.81
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Roles" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwSysUsers
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwSysUsers C-Win _FREEFORM
  QUERY brwSysUsers DISPLAY
      sysUser.isActive column-label "Active"      view-as toggle-box 
 sysUser.uid       label    "UID"                format "x(30)"              
sysUser.name       label    "Name"      width 30 format "x(50)"         
sysUser.initials   label    "Initial"   width 6  format "x(25)"          
sysUser.email      label    "Email"              format "x(25)"       
sysUser.role       label    "Role"               format "x(50)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 153 BY 7.86
         TITLE "Users" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bEdit AT ROW 17.9 COL 1.8 WIDGET-ID 224 NO-TAB-STOP 
     bDelete AT ROW 1.57 COL 33.2 WIDGET-ID 206 NO-TAB-STOP 
     fSearch AT ROW 1.95 COL 62.4 COLON-ALIGNED NO-LABEL WIDGET-ID 310
     bEditsysrole AT ROW 1.57 COL 25.8 WIDGET-ID 298 NO-TAB-STOP 
     brwsysrole AT ROW 3.76 COL 2 WIDGET-ID 500
     brwSysUsers AT ROW 17.95 COL 7 WIDGET-ID 300
     bExport AT ROW 1.57 COL 3.6 WIDGET-ID 2 NO-TAB-STOP 
     bNew AT ROW 1.57 COL 18.4 WIDGET-ID 204 NO-TAB-STOP 
     bRefresh AT ROW 1.57 COL 11 WIDGET-ID 158 NO-TAB-STOP 
     bCopy AT ROW 1.57 COL 40.6 WIDGET-ID 316 NO-TAB-STOP 
     bGrpAssignAct AT ROW 1.57 COL 48 WIDGET-ID 312 NO-TAB-STOP 
     bSearch AT ROW 1.57 COL 111.8 WIDGET-ID 320 NO-TAB-STOP 
     bGrpAssignUsr AT ROW 1.57 COL 55.4 WIDGET-ID 314 NO-TAB-STOP 
     "Search" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.05 COL 64 WIDGET-ID 324
     "Actions" VIEW-AS TEXT
          SIZE 7.6 BY .62 AT ROW 1 COL 3.8 WIDGET-ID 318
     RECT-62 AT ROW 1.33 COL 2.8 WIDGET-ID 308
     RECT-63 AT ROW 1.33 COL 63.2 WIDGET-ID 322
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 160.2 BY 27.43 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Roles"
         HEIGHT             = 24.91
         WIDTH              = 159.8
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwsysrole bEditsysrole DEFAULT-FRAME */
/* BROWSE-TAB brwSysUsers brwsysrole DEFAULT-FRAME */
ASSIGN 
       bCopy:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "A".

/* SETTINGS FOR BUTTON bDelete IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bDelete:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "A".

/* SETTINGS FOR BUTTON bEditsysrole IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bExport IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bGrpAssignAct IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bGrpAssignAct:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "A".

/* SETTINGS FOR BUTTON bGrpAssignUsr IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bGrpAssignUsr:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "A".

ASSIGN 
       bNew:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "A".

ASSIGN 
       brwsysrole:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwsysrole:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

ASSIGN 
       brwSysUsers:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwSysUsers:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwsysrole
/* Query rebuild information for BROWSE brwsysrole
     _START_FREEFORM
open query {&SELF-NAME} for each sysrole.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwsysrole */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwSysUsers
/* Query rebuild information for BROWSE brwSysUsers
     _START_FREEFORM
open query {&SELF-NAME} for each sysuser where lookup("*",sysuser.role) > 0 or lookup(cRole,sysuser.role) > 0.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwSysUsers */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Roles */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Roles */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Roles */
DO:
   run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCopy C-Win
ON CHOOSE OF bCopy IN FRAME DEFAULT-FRAME /* Copy */
do:
  run copySysRole in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME DEFAULT-FRAME /* Delete */
do:
  run deleteSysRole in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEdit C-Win
ON CHOOSE OF bEdit IN FRAME DEFAULT-FRAME /* Edit */
do:
  run modifyUser in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEditsysrole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditsysrole C-Win
ON CHOOSE OF bEditsysrole IN FRAME DEFAULT-FRAME /* Edit */
do:
  run modifySysRole in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGrpAssignAct
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGrpAssignAct C-Win
ON CHOOSE OF bGrpAssignAct IN FRAME DEFAULT-FRAME /* Assign Action */
do:
  run assignActions in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGrpAssignUsr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGrpAssignUsr C-Win
ON CHOOSE OF bGrpAssignUsr IN FRAME DEFAULT-FRAME /* Assign User */
do:
  run assignUsers in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME DEFAULT-FRAME /* New */
do:
  run newSysRole in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME DEFAULT-FRAME /* Refresh */
do:
  /* refresh the sysrole and sysuser table in sysdatasrv with the databse then call 
     getData to update the browser with updated table sysuser and sysrole */
  run refreshData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwsysrole
&Scoped-define SELF-NAME brwsysrole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwsysrole C-Win
ON DEFAULT-ACTION OF brwsysrole IN FRAME DEFAULT-FRAME /* Roles */
do:
  apply "choose" to bEditsysrole.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwsysrole C-Win
ON ROW-DISPLAY OF brwsysrole IN FRAME DEFAULT-FRAME /* Roles */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwsysrole C-Win
ON START-SEARCH OF brwsysrole IN FRAME DEFAULT-FRAME /* Roles */
do:
  {lib/brw-startSearch-multi.i &browseName ="brwsysrole"}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwsysrole C-Win
ON VALUE-CHANGED OF brwsysrole IN FRAME DEFAULT-FRAME /* Roles */
do:
  if available sysrole
   then
    do:      
      cRole = sysrole.roleID.
      open query brwSysUsers preselect each sysuser where lookup("*",sysuser.role) > 0 or lookup(cRole,sysuser.role) > 0.
    end.
    
  if num-results("brwSysUsers") > 0 
   then   
    bEdit:sensitive = true.                
   else                                     
    bEdit:sensitive = false.               
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwSysUsers
&Scoped-define SELF-NAME brwSysUsers
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSysUsers C-Win
ON DEFAULT-ACTION OF brwSysUsers IN FRAME DEFAULT-FRAME /* Users */
DO:
  apply "choose" to bedit.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSysUsers C-Win
ON ROW-DISPLAY OF brwSysUsers IN FRAME DEFAULT-FRAME /* Users */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSysUsers C-Win
ON START-SEARCH OF brwSysUsers IN FRAME DEFAULT-FRAME /* Users */
DO:
  {lib/brw-startSearch-multi.i &browseName ="brwSysUsers"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME DEFAULT-FRAME
DO:
  apply "CHOOSE" to bSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwsysrole
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main-multi.i &browse-list="brwSysRole,brwsysUsers"}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

assign current-window                = {&window-name} 
       this-procedure:current-window = {&window-name}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
on close of this-procedure 
  run disable_UI.

subscribe to "closeWindow" in this-procedure.

/* Best default for GUI applications is...                              */
pause 0 before-hide.

bExport       :load-image("images/excel.bmp").
bRefresh      :load-image ("images/refresh.bmp").
bRefresh      :load-image-insensitive("images/refresh-i.bmp").
bNew          :load-image("images/add.bmp").
bNew          :load-image-insensitive("images/add-i.bmp").
bEditsysrole  :load-image("images/update.bmp").
bEditsysrole  :load-image-insensitive("images/update-i.bmp").
bdelete       :load-image("images/delete.bmp").
bdelete       :load-image-insensitive("images/delete-i.bmp").
bEdit         :load-image("images/s-update.bmp").
bEdit         :load-image-insensitive("images/s-update-i.bmp").
bGrpAssignUsr :load-image("images/users.bmp").
bGrpAssignUsr :load-image-insensitive("images/users-i.bmp").
bGrpAssignAct :load-image("images/connect.bmp").
bGrpAssignAct :load-image-insensitive("images/connect-i.bmp").
bcopy         :load-image-up("images/copy.bmp").
bcopy         :load-image-insensitive("images/copy-i.bmp").
bSearch       :load-image-up("images/magnifier.bmp").
bSearch       :load-image-insensitive("images/magnifier-i.bmp").
          
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.
  
  run getdata in this-procedure.

  run showWindow in this-procedure.

  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assignActions C-Win 
PROCEDURE assignActions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available sysrole 
   then return.

  run dialogassignaction.w (input sysrole.roleID,
                            output std-lo).
  if not std-lo
   then
    return.
  
  /* Refresh the browser and get the updated data from the datasrv */
  run getData in this-procedure.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assignUsers C-Win 
PROCEDURE assignUsers :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available sysrole 
   then return.
  
  run dialogassignuser.w (input sysrole.roleID,
                          output std-lo).
  if not std-lo
   then
    return.
  
  /* Refresh the browser and get the updated data from the datasrv */ 
  run getData in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "close":u to this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copySysRole C-Win 
PROCEDURE copySysRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable opRoleID as character no-undo.
  define buffer tsysrole   for tsysrole.

  do with frame {&frame-name}:
  end.

  empty temp-table tsysrole.
 
  if not available sysrole 
   then return.

  create tsysrole.
  buffer-copy sysrole to tsysrole.
 
  run dialogsysrole.w (input {&copy},
                       input table tsysrole,
                       output opRoleID,
                       output std-lo).
 
  if not std-lo /* if fails or no call made */
   then
    return.
  
  /* Refresh the browser and get the updated data from the datasrv */
  run getData in this-procedure.

  do std-in = 1 to {&browse-name}:num-iterations:
   if {&browse-name}:is-row-selected(std-in)
    then
     leave.
  end.

  /* updating the temptable */
  find first sysrole where sysrole.roleid = opRoleID no-error.
  if not available sysrole   
   then return.
  
  std-ro = rowid(sysrole).
  reposition {&browse-name} to rowid std-ro. 
  {&browse-name}:set-repositioned-row(std-in,"ALWAYS").
  setStatusCount(query brwsysrole:num-results).
  
  apply "value-changed" to brwsysrole in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteSysRole C-Win 
PROCEDURE deleteSysRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  message "The role will be unassigned from all the associated users and actions. 
           Are you sure you want to delete the selected sysrole?"
    view-as alert-box warning buttons yes-no
    title "Delete sysrole"
    update std-lo.
  
  if not std-lo or not available sysrole 
   then return.
   
  
  publish "deleteSysRole" (input sysrole.roleID,                                
                           output std-lo,
                           output std-ch).
  
  if not std-lo 
   then 
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.
   
  /* Refresh the browser and get the updated data from the datasrv */ 
  run getData in this-procedure.
  open query brwSysUsers preselect each sysuser.

  setStatusCount(query brwsysrole:num-results).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fSearch 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-62 RECT-63 fSearch brwsysrole brwSysUsers bNew bRefresh bCopy 
         bEdit bSearch 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwsysrole:num-results = 0 
   then
    do:
      message "There is nothing to export"
        view-as alert-box warning buttons ok.
      return.
    end.

  publish "GetReportDir" (output std-ch).
  std-ha = temp-table sysrole:handle.
  run util/exporttable.p (table-handle std-ha,
                          "sysrole",
                          "for each sysrole",
                          "roleID,description,comments",
                          "RoleID,Description,Comments",
                          std-ch,
                          "Sysrole" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwsysrole.
  close query brwsysusers.
 
  empty temp-table sysrole.
  
  define buffer ttsysrole for ttsysrole.

  do with frame {&frame-name}:
  end.
  
  for each ttsysrole:
    /* test if the record contains the search text */
    if fSearch:screen-value <> "" and 
      not ( (ttsysrole.roleid      matches "*" + fSearch:screen-value + "*")  or 
            (ttsysrole.comments    matches "*" + fSearch:screen-value + "*")  or 
            (ttsysrole.description matches "*" + fSearch:screen-value + "*") )
     then next.
 
    create sysrole.
    buffer-copy ttsysrole to sysrole.
  end.
   
  open query brwsysrole preselect each sysrole.
  
  find first sysrole where sysrole.roleID = cRole no-error.
  if available sysrole
   then
    reposition brwsysrole to rowid rowid(sysrole).
  
  setStatusCount(query brwsysrole:num-results).   
  run setButtons in this-procedure.
  apply "value-changed" to brwsysrole in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttsysrole for ttsysrole.
  define buffer sysuser   for sysuser.

  empty temp-table sysuser.
  empty temp-table ttsysrole.
  
  /* getting sysrole */
  publish "getSysroles" (output table ttsysrole,
                         output table sysuser,
                         output std-lo,
                         output std-ch).
  if not std-lo
   then
    do:
      message std-ch
            view-as alert-box info buttons ok.
      return.
    end.

  /* filtering the data */
  run filterData in this-procedure.

  /* for record count */
  publish "GetCurrentValue" ("Refreshroles", output std-ch).

  if logical(std-ch)
   then
    setStatusRecords(query brwsysrole:num-results).

  apply "value-changed" to brwsysrole in frame {&frame-name}.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifySysRole C-Win 
PROCEDURE modifySysRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable opRoleID as character no-undo.
  define buffer tsysrole   for tsysrole.

  empty temp-table tsysrole.

  do with frame {&frame-name}:
  end.

  if not available sysrole 
   then return.
  
  create tsysrole.
  buffer-copy sysrole to tsysrole.
  
  run dialogsysrole.w(input {&modify},
                      input table tsysrole,
                      output opRoleID,
                      output std-lo).
  if not std-lo
   then
    return.
  
  /* Refresh the browser and get the updated data from the datasrv */
  run getData in this-procedure.

  do std-in = 1 to {&browse-name}:num-iterations:
    if {&browse-name}:is-row-selected(std-in)
     then
      leave.
  end.

  find first sysrole where sysrole.roleid = oproleID no-error.
  if not available sysrole 
   then return.
  
  std-ro = rowid(sysrole).
  open query {&browse-name} for each sysrole.
  reposition {&browse-name} to rowid std-ro.
  {&browse-name}:set-repositioned-row(std-in,"ALWAYS").
  
  apply "value-changed" to brwsysrole.
  setStatusCount(query brwsysrole:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyUser C-Win 
PROCEDURE modifyUser :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cUid   as character no-undo.
  define buffer tsysuser for tsysuser.

  empty temp-table tsysuser.

  if not available sysuser 
   then
    return.

  create tsysuser.
  buffer-copy sysuser to tsysuser.
  
  cUid = sysuser.uid.

  run dialogmodifysysuser.w (input table tsysuser,                               
                             output std-lo).

  if not std-lo 
   then
    return.
  
  /* Refresh the browser and get the updated data from the datasrv */
  run getData in this-procedure.
  setStatusCount(query brwsysrole:num-results).

  find first sysuser where sysuser.uid = cUid  
     and (lookup("*",sysuser.role) > 0 or lookup(cRole,sysuser.role) > 0) 
      no-error.
  if available sysuser 
   then 
    reposition brwsysusers to rowid rowid(sysuser). 
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newSysRole C-Win 
PROCEDURE newSysRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable opRoleID as character no-undo.
  define buffer tsysrole   for tsysrole.

  empty temp-table tsysrole.  

  do with frame {&frame-name}:
  end.

  run dialogsysrole.w (input {&new},
                       input table tsysrole,
                       output opRoleID,
                       output std-lo).

  if not std-lo
   then
    return.
  
  /* Refresh the browser and get the updated data from the datasrv */
  run getdata in this-procedure.

  do std-in = 1 to {&browse-name}:num-iterations:
   if {&browse-name}:is-row-selected(std-in)
    then
     leave.
  end.

  find first sysrole where sysrole.roleID = opRoleID no-error.
  if not available sysrole
   then return.
   
  std-ro = rowid(sysrole).    
  open query {&browse-name} preselect each sysrole.
  reposition {&browse-name} to rowid std-ro.
  {&browse-name}:set-repositioned-row(std-in,"ALWAYS").
  apply "value-changed" to brwsysrole in frame {&frame-name}.
   
  setStatusCount(query brwsysrole:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData C-Win 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  publish "refreshSysRoles" (output std-lo,
                             output std-ch).
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.
  
  /* Refresh the browser and get the updated data from the datasrv */
  run getdata in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setButtons C-Win 
PROCEDURE setButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name} :
  end.

  if query brwsysrole:num-results > 0 
   then
    assign 
        bExport      :sensitive = true
        bEditsysrole :sensitive = true
        bDelete      :sensitive = true
        bGrpAssignAct:sensitive = true
        bGrpAssignUsr:sensitive = true
        bCopy        :sensitive = true
        .
   else
    assign
        bExport      :sensitive = false
        bEditsysrole :sensitive = false
        bDelete      :sensitive = false
        bGrpAssignAct:sensitive = false
        bGrpAssignUsr:sensitive = false
        bCopy        :sensitive = false
        .
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .
  
  c-Win:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/brw-sortData-multi.i}
 apply "value-changed" to brwSysrole in frame {&frame-name}.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      brwsysrole:width-pixels                   = frame {&frame-name}:width-pixels - 10
      brwSysUsers:width-pixels                  = frame {&frame-name}:width-pixels - 35 
      brwSysUsers:height-pixels                 = frame {&frame-name}:height-pixels - brwSysUsers:y - 3
      . 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

