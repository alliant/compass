&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogsysrole.w

  Description: Dialog for modifying/creating new sysrole.

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gurvindar Singh

  Created:09/26/18
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{tt/sysrole.i}

/* Parameters Definitions ---                                           */
define input parameter ipcAction   as character no-undo.
define input parameter table       for  sysrole.
define output parameter opRoleID   as character no-undo.
define output parameter oplSuccess as logical   no-undo.

/* Standard Libraries */
{lib/std-def.i}
{lib/sys-def.i}

/* Local Variable Definitions ---                                       */
define variable cTrackRole   as character no-undo.
define variable cTrackDesc   as character no-undo.
define variable cTrackComm   as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fRole edDecsc edComment Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS fRole edDecsc edComment 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE edComment AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 63.6 BY 2 NO-UNDO.

DEFINE VARIABLE edDecsc AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 63.6 BY 2 NO-UNDO.

DEFINE VARIABLE fRole AS CHARACTER FORMAT "X(256)":U 
     LABEL "Role" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 63.6 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fRole AT ROW 1.48 COL 13.6 COLON-ALIGNED WIDGET-ID 14
     edDecsc AT ROW 2.67 COL 15.6 NO-LABEL WIDGET-ID 6
     edComment AT ROW 4.91 COL 15.6 NO-LABEL WIDGET-ID 8
     Btn_OK AT ROW 7.19 COL 26.4
     Btn_Cancel AT ROW 7.19 COL 43.4
     "Description:" VIEW-AS TEXT
          SIZE 11.6 BY .62 AT ROW 2.71 COL 3.4 WIDGET-ID 10
     "Comments:" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 4.91 COL 4.2 WIDGET-ID 12
     SPACE(66.79) SKIP(3.13)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "System Role"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* System Role */
DO:
  APPLY "END-ERROR":U TO SELF.
  oplSuccess = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  if fRole:screen-value = ""
   then
    do:
      message "Role can not be blank"
          view-as alert-box info buttons ok.
      return no-apply.
    end.

  run saveSysRole in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edComment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edComment Dialog-Frame
ON VALUE-CHANGED OF edComment IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edDecsc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edDecsc Dialog-Frame
ON VALUE-CHANGED OF edDecsc IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fRole Dialog-Frame
ON VALUE-CHANGED OF fRole IN FRAME Dialog-Frame /* Role */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.
                                                          
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  run setData in this-procedure.
   /* sensitivity of save button */
  run enableDisableSave in this-procedure.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  case ipcAction:
    when {&New} 
     then
      Btn_OK:sensitive = (fRole:input-value <> "").
    when {&modify} 
     then
      Btn_OK:sensitive = not(cTrackDesc = (edDecsc:input-value) and
                             cTrackComm = trim(edComment:input-value)).
    when {&Copy} 
     then
      Btn_OK:sensitive = not(fRole:input-value = cTrackRole).
  end case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fRole edDecsc edComment 
      WITH FRAME Dialog-Frame.
  ENABLE fRole edDecsc edComment Btn_OK Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveSysRole Dialog-Frame 
PROCEDURE saveSysRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  find first sysrole no-error.
  if not available sysrole 
   then
    create sysrole.
  
  assign 
      sysrole.roleID      = fRole:input-value    
      sysrole.description = edDecsc:input-value   
      sysrole.comments    = edComment:input-value
      oproleID            = fRole:input-value 
      .

  if ipcAction = {&Modify}
   then
    publish "ModifySysRole" (input table sysrole,
                             output oplSuccess,
                             output std-ch).
   else
    publish "newSysRole" (input table sysrole,
                          output oplSuccess,
                          output std-ch).
  if not oplSuccess 
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData Dialog-Frame 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if ipcAction = {&New}                      
   then                                      
    frame dialog-frame:title = "New Role".   
  else if ipcAction = {&Copy}               
   then                                     
    frame dialog-frame:title = "Copy Role". 
   else                                     
    do:                                      
      fRole:sensitive = false.               
      frame dialog-frame:title = "Edit Role".
    end.                                     
                                             
  find first sysrole no-error.
  if available sysrole 
   then
    do:
      assign 
          fRole:screen-value     = sysrole.roleId
          edDecsc:screen-value   = sysrole.description
          edComment:screen-value = sysrole.comments
          .
    end.
  
  /* Keep the initial Values, required in enableDisableSave. */
  assign
      cTrackRole  = fRole :input-value
      cTrackDesc  = edDecsc:input-value 
      cTrackComm  = edComment:input-value
      .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

