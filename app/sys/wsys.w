&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wsys.w

  Description: Main window for SYS application

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul

  Created: 09.25.2018
  @Modification:
 
  Date             Name            Description 
  07/18/2019       Gurvindar       Fixed sorting issue 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/winlaunch.i}
{lib/brw-multi-def.i} 
{lib/sys-def.i}
{lib/get-column.i}

{tt/ttstatistics.i}

define temp-table ttUserStat like ttstatistics
  field intCalls     as integer.
  
define temp-table ttActionStat like ttstatistics
  field cavgDuration as character
  field intCalls     as integer.
  
define variable hSort           as handle  no-undo.
define variable lLoadOnLaunch   as logical    no-undo.
define variable iDays           as integer    no-undo.
define variable dColumnWidth    as decimal    no-undo. 
define variable dtFromDate      as datetime   no-undo.
define variable dtToDate        as datetime   no-undo.

define variable hSysAction      as handle     no-undo.
define variable hSysUser        as handle     no-undo.
define variable hSysRole        as handle     no-undo.
define variable hSysDocs        as handle     no-undo.
define variable hSysLock        as handle     no-undo.
define variable hSysCode        as handle     no-undo.
define variable hSysProp        as handle     no-undo.
define variable hSysKey         as handle     no-undo.
define variable hSysIndex       as handle     no-undo.
define variable hSysEnvs        as handle     no-undo.
define variable hSysLogs        as handle     no-undo.
define variable hServerLogs     as handle     no-undo.
define variable hComLogDelete   as handle     no-undo.
define variable hSysLogDelete   as handle     no-undo.
define variable hsysAppLogs     as handle     no-undo.
define variable hSysQueue       as handle     no-undo.
define variable hSysDest        as handle     no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwActions

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttActionStat ttUserStat

/* Definitions for BROWSE brwActions                                    */
&Scoped-define FIELDS-IN-QUERY-brwActions ttActionStat.action "Action Name" ttActionStat.calls "Calls" ttActionStat.error "Errors" ttActionStat.cavgDuration "Average Duration (sec)" ttActionStat.description "Description"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwActions   
&Scoped-define SELF-NAME brwActions
&Scoped-define QUERY-STRING-brwActions for each ttActionStat
&Scoped-define OPEN-QUERY-brwActions open query {&SELF-NAME} for each ttActionStat.
&Scoped-define TABLES-IN-QUERY-brwActions ttActionStat
&Scoped-define FIRST-TABLE-IN-QUERY-brwActions ttActionStat


/* Definitions for BROWSE brwUsers                                      */
&Scoped-define FIELDS-IN-QUERY-brwUsers ttUserStat.uName "UserName" ttUserStat.calls "Calls" ttUserStat.error "Errors" ttUserStat.roles "Roles"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwUsers   
&Scoped-define SELF-NAME brwUsers
&Scoped-define QUERY-STRING-brwUsers for each ttUserStat
&Scoped-define OPEN-QUERY-brwUsers open query {&SELF-NAME} for each ttUserStat.
&Scoped-define TABLES-IN-QUERY-brwUsers ttUserStat
&Scoped-define FIRST-TABLE-IN-QUERY-brwUsers ttUserStat


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwActions}~
    ~{&OPEN-QUERY-brwUsers}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bActive bSysIndex bSysLog bSysProp bAction ~
bActiveUser RECT-73 RECT-75 RECT-76 binActiveUsr RECT-77 RECT-78 flPeriod ~
brwActions bInactive bNocalls brwUsers bRefresh bCode bDoc berrors bSysenv ~
bSysKey bSysLock bSysRole bUser ffromdate ftodate 
&Scoped-Define DISPLAYED-OBJECTS flPeriod funiqueAction finactiveActions ~
fcalls ferrors fActiveUsers fInActiveUsers ffromdate ftodate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_Module 
       MENU-ITEM m_Configure    LABEL "Configure"     
       MENU-ITEM m_About        LABEL "About"         
       MENU-ITEM m_Exit         LABEL "Exit"          .

DEFINE SUB-MENU m_View 
       MENU-ITEM m_Sys_Action   LABEL "Actions"       
       MENU-ITEM m_Sys_User     LABEL "Users"         
       MENU-ITEM m_Sys_Role     LABEL "Roles"         
       MENU-ITEM m_Sys_Doc      LABEL "Documents"     
       MENU-ITEM m_Sys_Lock     LABEL "Locks"         
       MENU-ITEM m_Sys_Code     LABEL "Codes"         
       MENU-ITEM m_Sys_Prop     LABEL "Properties"    
       MENU-ITEM m_Sys_Key      LABEL "Keys"          
       MENU-ITEM m_System_Environment LABEL "Environment"   
       RULE
       MENU-ITEM m_Logs         LABEL "Logs"          
       MENU-ITEM m_Indexes      LABEL "Indexes"       .

DEFINE SUB-MENU m_Tools 
       MENU-ITEM m_Delete_System_Logs LABEL "Delete System Logs"
       MENU-ITEM m_Delete_Compliance_Logs LABEL "Delete Compliance Logs"
       RULE
       MENU-ITEM m_View_Server_Logs LABEL "View Server Logs"
       MENU-ITEM m_View_Broker_Logs LABEL "View Broker Logs"
       MENU-ITEM m_View_Sys_Dest LABEL "View System Destinations"
       MENU-ITEM m_View_System_Queue LABEL "View System Queue".
       MENU-ITEM m_View_Application_Logs LABEL "View Application Logs".

DEFINE SUB-MENU m_Report 
       MENU-ITEM m_Users_By_Module LABEL "Users By Module"
       MENU-ITEM m_Actions_By_Module LABEL "Actions By Module".

DEFINE MENU MENU-BAR-C-Win MENUBAR
       SUB-MENU  m_Module       LABEL "Module"        
       SUB-MENU  m_View         LABEL "View"          
       SUB-MENU  m_Tools        LABEL "Tools"         
       SUB-MENU  m_Report       LABEL "Reports"       .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bAction  NO-FOCUS
     LABEL "Action" 
     SIZE 7.2 BY 1.71 TOOLTIP "Actions".

DEFINE BUTTON bActive  NO-FOCUS
     LABEL "Active" 
     SIZE 4.8 BY 1.14 TOOLTIP "View unique actions".

DEFINE BUTTON bActiveUser  NO-FOCUS
     LABEL "Active" 
     SIZE 4.8 BY 1.14 TOOLTIP "View active users".

DEFINE BUTTON bCode  NO-FOCUS
     LABEL "Sys Code" 
     SIZE 7.2 BY 1.71 TOOLTIP "Codes".

DEFINE BUTTON bDoc  NO-FOCUS
     LABEL "Sys Doc" 
     SIZE 7.2 BY 1.71 TOOLTIP "Documents".

DEFINE BUTTON berrors  NO-FOCUS
     LABEL "Errors" 
     SIZE 4.8 BY 1.14 TOOLTIP "View number of errors".

DEFINE BUTTON bInactive  NO-FOCUS
     LABEL "InActive" 
     SIZE 4.8 BY 1.14 TOOLTIP "View Inactive actions".

DEFINE BUTTON binActiveUsr  NO-FOCUS
     LABEL "InactiveUser" 
     SIZE 4.8 BY 1.14 TOOLTIP "View Inactive users".

DEFINE BUTTON bNocalls  NO-FOCUS
     LABEL "Calls" 
     SIZE 4.8 BY 1.14 TOOLTIP "View number of calls".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON bSysenv  NO-FOCUS
     LABEL "Sys Env" 
     SIZE 7.2 BY 1.71 TOOLTIP "Environment".

DEFINE BUTTON bSysIndex  NO-FOCUS
     LABEL "Sys Index" 
     SIZE 7.2 BY 1.71 TOOLTIP "Indexes".

DEFINE BUTTON bSysKey  NO-FOCUS
     LABEL "Sys Key" 
     SIZE 7.2 BY 1.71 TOOLTIP "Keys".

DEFINE BUTTON bSysLock  NO-FOCUS
     LABEL "Sys Lock" 
     SIZE 7.2 BY 1.71 TOOLTIP "Locks".

DEFINE BUTTON bSysLog  NO-FOCUS
     LABEL "Sys Log" 
     SIZE 7.2 BY 1.71 TOOLTIP "Logs".

DEFINE BUTTON bSysProp  NO-FOCUS
     LABEL "Sys Prop" 
     SIZE 7.2 BY 1.71 TOOLTIP "Properties".

DEFINE BUTTON bSysRole  NO-FOCUS
     LABEL "Sys Role" 
     SIZE 7.2 BY 1.71 TOOLTIP "Roles".

DEFINE BUTTON bUser  NO-FOCUS
     LABEL "User" 
     SIZE 7.2 BY 1.71 TOOLTIP "Users".

DEFINE VARIABLE fActiveUsers AS INTEGER FORMAT ">>>>>":U INITIAL 0 
     LABEL "Active" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 9.4 BY 1 NO-UNDO.

DEFINE VARIABLE fcalls AS INTEGER FORMAT ">>>>>>":U INITIAL 0 
     LABEL "Number of calls" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 9.4 BY 1 NO-UNDO.

DEFINE VARIABLE ferrors AS INTEGER FORMAT ">>>>>":U INITIAL 0 
     LABEL "Number of Errors" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 9.4 BY 1 NO-UNDO.

DEFINE VARIABLE ffromdate AS DATETIME FORMAT "99/99/99 HH:MM:SS":U INITIAL "10/31/2018 00:00:01.000" 
      VIEW-AS TEXT 
     SIZE 18.8 BY .62 NO-UNDO.

DEFINE VARIABLE finactiveActions AS INTEGER FORMAT ">>>>>":U INITIAL 0 
     LABEL "Inactive" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 9.4 BY 1 NO-UNDO.

DEFINE VARIABLE fInActiveUsers AS INTEGER FORMAT ">>>>>":U INITIAL 0 
     LABEL "Inactive" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 9.4 BY 1 NO-UNDO.

DEFINE VARIABLE flPeriod AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Period" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 5.4 BY 1 NO-UNDO.

DEFINE VARIABLE ftodate AS DATETIME FORMAT "99/99/99 HH:MM:SS":U INITIAL "11/02/2018 12:22:22.000" 
      VIEW-AS TEXT 
     SIZE 19.4 BY .62 NO-UNDO.

DEFINE VARIABLE funiqueAction AS INTEGER FORMAT ">>>>>":U INITIAL 0 
     LABEL "Active" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 9.4 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-73
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 138.2 BY 9.14.

DEFINE RECTANGLE RECT-75
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE-PIXELS 691 BY 191.

DEFINE RECTANGLE RECT-76
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 29 BY 2.52.

DEFINE RECTANGLE RECT-77
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 66.6 BY 2.52.

DEFINE RECTANGLE RECT-78
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17.6 BY 2.52.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwActions FOR 
      ttActionStat SCROLLING.

DEFINE QUERY brwUsers FOR 
      ttUserStat SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwActions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwActions C-Win _FREEFORM
  QUERY brwActions DISPLAY
      ttActionStat.action  label   "Action Name"              width 40    format "x(70)" 
ttActionStat.calls         label   "Calls"                    width 8     format "x(15)"              
ttActionStat.error         label   "Errors"                   width 6     format "x(20)"         
ttActionStat.cavgDuration  label   "Average Duration (sec)"   width 25    format "x(20)" 
ttActionStat.description   label   "Description"              width 50    format "x(60)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-SCROLLBAR-VERTICAL NO-TAB-STOP SIZE 135 BY 6.81
         TITLE "Top 5" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwUsers
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwUsers C-Win _FREEFORM
  QUERY brwUsers DISPLAY
      ttUserStat.uName      label      "UserName"  width 30    format "x(25)"              
ttUserStat.calls         label      "Calls"     width 8     format "x(10)"         
ttUserStat.error         label      "Errors"    width 6     format "x(25)"
ttUserStat.roles         label      "Roles"     width 40    format "x(60)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-SCROLLBAR-VERTICAL NO-TAB-STOP SIZE 135 BY 6.81
         TITLE "Top 5" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bActive AT ROW 13.33 COL 25 WIDGET-ID 172 NO-TAB-STOP 
     bSysIndex AT ROW 1.67 COL 106.6 WIDGET-ID 8 NO-TAB-STOP 
     bSysLog AT ROW 1.67 COL 99.6 WIDGET-ID 26 NO-TAB-STOP 
     bSysProp AT ROW 1.67 COL 75.4 WIDGET-ID 16 NO-TAB-STOP 
     bAction AT ROW 1.67 COL 33.4 WIDGET-ID 2 NO-TAB-STOP 
     bActiveUser AT ROW 23.05 COL 25 WIDGET-ID 166 NO-TAB-STOP 
     binActiveUsr AT ROW 23.1 COL 56.4 WIDGET-ID 168 NO-TAB-STOP 
     flPeriod AT ROW 2.05 COL 10 COLON-ALIGNED WIDGET-ID 180
     brwActions AT ROW 6.14 COL 4.8 WIDGET-ID 200
     funiqueAction AT ROW 13.38 COL 13.4 COLON-ALIGNED WIDGET-ID 92
     bInactive AT ROW 13.33 COL 56.4 WIDGET-ID 178 NO-TAB-STOP 
     finactiveActions AT ROW 13.38 COL 44.8 COLON-ALIGNED WIDGET-ID 152
     fcalls AT ROW 13.38 COL 94 RIGHT-ALIGNED WIDGET-ID 94
     ferrors AT ROW 13.38 COL 123.6 COLON-ALIGNED WIDGET-ID 150
     bNocalls AT ROW 13.33 COL 95.2 WIDGET-ID 174 NO-TAB-STOP 
     brwUsers AT ROW 15.91 COL 4.8 WIDGET-ID 300
     bRefresh AT ROW 1.67 COL 23.2 WIDGET-ID 4 NO-TAB-STOP 
     fActiveUsers AT ROW 23.1 COL 13.4 COLON-ALIGNED WIDGET-ID 100
     fInActiveUsers AT ROW 23.14 COL 44.8 COLON-ALIGNED WIDGET-ID 102
     bCode AT ROW 1.67 COL 68.4 WIDGET-ID 12 NO-TAB-STOP 
     bDoc AT ROW 1.67 COL 54.4 WIDGET-ID 10 NO-TAB-STOP 
     berrors AT ROW 13.29 COL 135.2 WIDGET-ID 176 NO-TAB-STOP 
     bSysenv AT ROW 1.67 COL 89.4 WIDGET-ID 58 NO-TAB-STOP 
     bSysKey AT ROW 1.67 COL 82.4 WIDGET-ID 14 NO-TAB-STOP 
     bSysLock AT ROW 1.67 COL 61.4 WIDGET-ID 18 NO-TAB-STOP 
     bSysRole AT ROW 1.67 COL 47.4 WIDGET-ID 20 NO-TAB-STOP 
     bUser AT ROW 1.67 COL 40.4 WIDGET-ID 6 NO-TAB-STOP 
     ffromdate AT ROW 4.48 COL 23.8 COLON-ALIGNED NO-LABEL WIDGET-ID 158
     ftodate AT ROW 4.48 COL 48.4 COLON-ALIGNED NO-LABEL WIDGET-ID 162
     "days" VIEW-AS TEXT
          SIZE 4.4 BY .62 AT ROW 2.24 COL 18 WIDGET-ID 182
     "Actions" VIEW-AS TEXT
          SIZE 8.8 BY .62 AT ROW 5.43 COL 4.8 WIDGET-ID 184
          FONT 6
     "Users" VIEW-AS TEXT
          SIZE 6.8 BY .62 AT ROW 15.14 COL 4.6 WIDGET-ID 186
          FONT 6
     "Statistics between:" VIEW-AS TEXT
          SIZE 21 BY .62 AT ROW 4.48 COL 4.6 WIDGET-ID 156
          FONT 6
     "and" VIEW-AS TEXT
          SIZE 4.2 BY .62 AT ROW 4.43 COL 45 WIDGET-ID 160
          FONT 6
     RECT-73 AT ROW 15.48 COL 3.2 WIDGET-ID 110
     RECT-75 AT Y 101 X 11 WIDGET-ID 154
     RECT-76 AT ROW 1.29 COL 3.2 WIDGET-ID 188
     RECT-77 AT ROW 1.29 COL 31.8 WIDGET-ID 190
     RECT-78 AT ROW 1.29 COL 98 WIDGET-ID 192
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 142.4 BY 23.76
         DEFAULT-BUTTON bRefresh WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = ""
         HEIGHT             = 23.76
         WIDTH              = 142.6
         MAX-HEIGHT         = 33.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.52
         VIRTUAL-WIDTH      = 273.2
         MAX-BUTTON         = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-C-Win:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwActions flPeriod DEFAULT-FRAME */
/* BROWSE-TAB brwUsers bNocalls DEFAULT-FRAME */
ASSIGN 
       brwActions:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwActions:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

ASSIGN 
       brwUsers:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwUsers:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR FILL-IN fActiveUsers IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       fActiveUsers:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN fcalls IN FRAME DEFAULT-FRAME
   NO-ENABLE ALIGN-R                                                    */
ASSIGN 
       fcalls:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN ferrors IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       ferrors:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       ffromdate:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN finactiveActions IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       finactiveActions:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN fInActiveUsers IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       fInActiveUsers:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       ftodate:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN funiqueAction IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       funiqueAction:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwActions
/* Query rebuild information for BROWSE brwActions
     _START_FREEFORM
open query {&SELF-NAME} for each ttActionStat.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwActions */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwUsers
/* Query rebuild information for BROWSE brwUsers
     _START_FREEFORM
open query {&SELF-NAME} for each ttUserStat.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwUsers */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win
DO:
  /* This event will close the window and terminate the procedure.  */
  publish "GetConfirmExit" (output std-lo).
  
  if std-lo
   then
    do:
      message "Are you sure, you want to exit."
          view-as alert-box question buttons ok-cancel update lChoice as logical.
      if not lChoice 
       then
        return no-apply.
    end.

  apply "CLOSE":U to this-procedure.

  publish "ExitApplication".
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAction C-Win
ON CHOOSE OF bAction IN FRAME DEFAULT-FRAME /* Action */
DO:
  run openSysAction in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bActive
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActive C-Win
ON CHOOSE OF bActive IN FRAME DEFAULT-FRAME /* Active */
DO:
  run openActionRpt in this-procedure (input "A") .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bActiveUser
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActiveUser C-Win
ON CHOOSE OF bActiveUser IN FRAME DEFAULT-FRAME /* Active */
DO:
  run openUserRpt in this-procedure (input "A") .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCode C-Win
ON CHOOSE OF bCode IN FRAME DEFAULT-FRAME /* Sys Code */
DO:
  run openSysCode in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDoc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDoc C-Win
ON CHOOSE OF bDoc IN FRAME DEFAULT-FRAME /* Sys Doc */
DO:
  run openSysDoc in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME berrors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL berrors C-Win
ON CHOOSE OF berrors IN FRAME DEFAULT-FRAME /* Errors */
DO:
  run openErrorActionRpt in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bInactive
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bInactive C-Win
ON CHOOSE OF bInactive IN FRAME DEFAULT-FRAME /* InActive */
DO:
  run openActionRpt in this-procedure (input "I") .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME binActiveUsr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL binActiveUsr C-Win
ON CHOOSE OF binActiveUsr IN FRAME DEFAULT-FRAME /* InactiveUser */
DO:
  run openUserRpt in this-procedure (input "I").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNocalls
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNocalls C-Win
ON CHOOSE OF bNocalls IN FRAME DEFAULT-FRAME /* Calls */
DO:
  run openActionCalledRpt in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME DEFAULT-FRAME /* refresh */
DO:
  run validateSearch in this-procedure (output std-lo).
  if not std-lo
   then
    return no-apply.

  iDays = flPeriod:input-value.
  run setStatisticsDates in this-procedure.
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwActions
&Scoped-define SELF-NAME brwActions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwActions C-Win
ON ROW-DISPLAY OF brwActions IN FRAME DEFAULT-FRAME /* Top 5 */
do:
  {lib/brw-rowDisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwActions C-Win
ON START-SEARCH OF brwActions IN FRAME DEFAULT-FRAME /* Top 5 */
do:
  hSort = browse brwActions:current-column.
  
  if hSort:name  = "Calls" 
   then
    hSort:name = "intCalls".
     
  {lib/brw-startSearch-multi.i &browseName ="brwActions"} 
  
  if hSort:name = "intCalls" 
   then
    hSort:name = "Calls".
   
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwUsers
&Scoped-define SELF-NAME brwUsers
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwUsers C-Win
ON ROW-DISPLAY OF brwUsers IN FRAME DEFAULT-FRAME /* Top 5 */
do:
 {lib/brw-rowDisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwUsers C-Win
ON START-SEARCH OF brwUsers IN FRAME DEFAULT-FRAME /* Top 5 */
do:
  hSort = browse brwUsers:current-column.
  
  if hSort:name  = "Calls" 
   then
    hSort:name = "intCalls".
     
  {lib/brw-startSearch-multi.i &browseName ="brwUsers"}
 
  if hSort:name = "intCalls" 
   then
    hSort:name = "Calls".
      
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSysenv
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSysenv C-Win
ON CHOOSE OF bSysenv IN FRAME DEFAULT-FRAME /* Sys Env */
DO: 
  run openSysEnv in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSysIndex
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSysIndex C-Win
ON CHOOSE OF bSysIndex IN FRAME DEFAULT-FRAME /* Sys Index */
DO:
  run openSysIndex in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSysKey
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSysKey C-Win
ON CHOOSE OF bSysKey IN FRAME DEFAULT-FRAME /* Sys Key */
DO:
  run openSysKey in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSysLock
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSysLock C-Win
ON CHOOSE OF bSysLock IN FRAME DEFAULT-FRAME /* Sys Lock */
DO:
  run openSysLock in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSysLog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSysLog C-Win
ON CHOOSE OF bSysLog IN FRAME DEFAULT-FRAME /* Sys Log */
DO: 
  run openSysLog in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSysProp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSysProp C-Win
ON CHOOSE OF bSysProp IN FRAME DEFAULT-FRAME /* Sys Prop */
DO:
  run openSysProp in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSysRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSysRole C-Win
ON CHOOSE OF bSysRole IN FRAME DEFAULT-FRAME /* Sys Role */
DO:
  run openSysRole in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bUser
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bUser C-Win
ON CHOOSE OF bUser IN FRAME DEFAULT-FRAME /* User */
DO:
  run openSysUser in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_About
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_About C-Win
ON CHOOSE OF MENU-ITEM m_About /* About */
DO:
  publish "AboutApplication".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Actions_By_Module
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Actions_By_Module C-Win
ON CHOOSE OF MENU-ITEM m_Actions_By_Module /* Actions By Module */
DO:
  run wactionsbymodulerpt.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Configure
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Configure C-Win
ON CHOOSE OF MENU-ITEM m_Configure /* Configure */
DO:
  run actionConfig in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Delete_Compliance_Logs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Delete_Compliance_Logs C-Win
ON CHOOSE OF MENU-ITEM m_Delete_Compliance_Logs /* Delete Compliance Logs */
DO:
  run deleteComLog in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Delete_System_Logs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Delete_System_Logs C-Win
ON CHOOSE OF MENU-ITEM m_Delete_System_Logs /* Delete System Logs */
DO:
  run deleteSysLog in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Exit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Exit C-Win
ON CHOOSE OF MENU-ITEM m_Exit /* Exit */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Indexes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Indexes C-Win
ON CHOOSE OF MENU-ITEM m_Indexes /* Indexes */
DO:
  apply "Choose" to bSysIndex in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Logs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Logs C-Win
ON CHOOSE OF MENU-ITEM m_Logs /* Logs */
DO:
  apply "Choose" to bSysLog in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_System_Environment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_System_Environment C-Win
ON CHOOSE OF MENU-ITEM m_System_Environment /* Environment */
DO:
  apply "choose" to bsysenv in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Sys_Action
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Sys_Action C-Win
ON CHOOSE OF MENU-ITEM m_Sys_Action /* Actions */
DO:
  apply "Choose" to bAction in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Sys_Code
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Sys_Code C-Win
ON CHOOSE OF MENU-ITEM m_Sys_Code /* Codes */
DO:
  apply "Choose" to bCode in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Sys_Doc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Sys_Doc C-Win
ON CHOOSE OF MENU-ITEM m_Sys_Doc /* Documents */
DO:
  apply "Choose" to bDoc in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Sys_Key
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Sys_Key C-Win
ON CHOOSE OF MENU-ITEM m_Sys_Key /* Keys */
DO:
  apply "Choose" to bSysKey in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Sys_Lock
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Sys_Lock C-Win
ON CHOOSE OF MENU-ITEM m_Sys_Lock /* Locks */
DO:
  apply "Choose" to bSysLock in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Sys_Prop
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Sys_Prop C-Win
ON CHOOSE OF MENU-ITEM m_Sys_Prop /* Properties */
DO:
  apply "Choose" to bsysprop in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Sys_Role
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Sys_Role C-Win
ON CHOOSE OF MENU-ITEM m_Sys_Role /* Roles */
DO:
  apply "Choose" to bSysRole in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Sys_User
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Sys_User C-Win
ON CHOOSE OF MENU-ITEM m_Sys_User /* Users */
DO:
  apply "Choose" to bUser in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Users_By_Module
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Users_By_Module C-Win
ON CHOOSE OF MENU-ITEM m_Users_By_Module /* Users By Module */
DO:
  run wusersbymodulerpt.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Application_Logs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Application_Logs C-Win
ON CHOOSE OF MENU-ITEM m_View_Application_Logs /* View Application Logs */
DO:
  run openSysAppLog in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Broker_Logs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Broker_Logs C-Win
ON CHOOSE OF MENU-ITEM m_View_Broker_Logs /* View Broker Logs */
DO:
  run wbrokerlogs.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Server_Logs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Server_Logs C-Win
ON CHOOSE OF MENU-ITEM m_View_Server_Logs /* View Server Logs */
DO:
  run wserverlogs.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_System_Queue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_System_Queue C-Win
ON CHOOSE OF MENU-ITEM m_View_System_Queue /* View System Queue */
DO:
  if valid-handle(hSysQueue)
   then run ShowWindow in hSysQueue.
   else run referencequeue.w persistent set hSysQueue.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Sys_Dest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Sys_Dest C-Win
ON CHOOSE OF MENU-ITEM m_View_Sys_Dest /* View System Destinations */
DO:
  if valid-handle(hSysDest)
   then run ShowWindow in hSysDest.
   else run referencesysdest.w persistent set hSysDest.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwActions
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-status.i}
{lib/brw-main-multi.i &browse-list="brwActions,brwUsers"}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Refresh Data for changring search Period in configuration */
subscribe to "periodModified" anywhere.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bAction     :load-image("images/sysaction.bmp").
bAction     :load-image-insensitive("images/sysaction-i.bmp").

bUser       :load-image("images/users.bmp").
bUser       :load-image-insensitive("images/users-i.bmp").

bSysRole    :load-image("images/agent.bmp").
bSysRole    :load-image-insensitive("images/agent-i.bmp").

bSysLog     :load-image("images/log.bmp").
bSysLog     :load-image-insensitive("images/log-i.bmp").

bDoc        :load-image("images/docs.bmp").
bDoc        :load-image-insensitive("images/docs-i.bmp").

bSysLock    :load-image("images/lock.bmp").
bSysLock    :load-image-insensitive("images/lock-i.bmp").

bCode       :load-image("images/syscode.bmp").
bCode       :load-image-insensitive("images/syscode-i.bmp").

bsysprop    :load-image("images/sysprop.bmp").
bsysprop    :load-image-insensitive("images/sysprop-i.bmp").

bSysIndex   :load-image("images/sysindex.bmp").
bSysIndex   :load-image-insensitive("images/sysindex-i.bmp").

bSysKey     :load-image("images/syskey.bmp").
bSysKey     :load-image-insensitive("images/syskey-i.bmp").

bSysenv     :load-image("images/config.bmp").
bSysenv     :load-image-insensitive("images/config-i.bmp").

bRefresh    :load-image("images/sync.bmp").
bRefresh    :load-image-insensitive("images/sync-i.bmp").

bActive     :load-image("images/s-magnifier.bmp").
bActive     :load-image-insensitive("images/s-magnifier-i.bmp").

bInactive   :load-image("images/s-magnifier.bmp").
bInactive   :load-image-insensitive("images/s-magnifier-i.bmp").

bNocalls    :load-image("images/s-magnifier.bmp").
bNocalls    :load-image-insensitive("images/s-magnifier-i.bmp").

berrors     :load-image("images/s-magnifier.bmp").
berrors     :load-image-insensitive("images/s-magnifier-i.bmp").

bActiveUser :load-image("images/s-magnifier.bmp").
bActiveUser :load-image-insensitive("images/s-magnifier-i.bmp").

bInActiveUsr:load-image("images/s-magnifier.bmp").
bInActiveUsr:load-image-insensitive("images/s-magnifier-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  run enable_UI.                           
  
  /* Get default period from configuration. */
  publish "getDefaultDays" (output iDays).

  assign   
    dtToDate    = now        
    dtFromDate  = datetime(today - iDays)
    .  

  {lib/get-column-width.i &col="'action'" &var=dColumnWidth &browse-name=brwActions}

  /* Get default loadOnLaunch status from configuration. */
  publish "getLoadLaunch"  (output lLoadOnLaunch).

  if lLoadOnLaunch
   then
    run getData in this-procedure.

  /* Restore the current window after fetching data */
  {&window-name}:window-state = WINDOW-NORMAL. 

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionConfig C-Win 
PROCEDURE actionConfig :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run dialogconfig.w.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteComLog C-Win 
PROCEDURE deleteComLog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not valid-handle(hComLogDelete) 
   then
    run wdeletecompliancelogs.w persistent set hComLogDelete.
   else
    run showWindow in hComLogDelete.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteSysLog C-Win 
PROCEDURE deleteSysLog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not valid-handle(hSysLogDelete) 
   then
    run wdeletesyslogs.w persistent set hSysLogDelete.
   else
    run showWindow in hSysLogDelete.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData C-Win 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define buffer ttstatistics for ttstatistics.

  empty temp-table ttActionStat.
  empty temp-table ttUserStat.

  for each ttstatistics:

    case ttstatistics.type:
      when {&Action} 
       then
        do:
          create ttActionStat.
          buffer-copy ttstatistics to ttActionStat.
          assign
              ttActionStat.cavgDuration = trim(string(ttActionStat.avgDuration / 1000,"zz9.999")) 
              ttactionStat.intCalls     = integer(ttstatistics.calls)
          .
        end.
      when {&Users}
       then
        do:
          create ttUserStat.
          buffer-copy ttstatistics to ttUserStat .
          ttUserStat.intCalls = integer(ttstatistics.calls).
          
        end.
      when {&ActionType} 
       then
         assign
           funiqueAction:screen-value    =  string(ttstatistics.noAction)
           finactiveActions:screen-value =  string(ttstatistics.inactive)
           fcalls:screen-value           =  ttstatistics.calls
           ferrors:screen-value          =  ttstatistics.error
           .
    
      when {&UserType} 
       then
         assign 
           fActiveUsers:screen-value    = string(ttstatistics.active)    
           fInActiveUsers:screen-value  = string(ttstatistics.inactive)
           .       
    end case.
  end.

  open query brwActions for each ttActionStat.
  open query brwUsers   for each ttUserStat.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flPeriod funiqueAction finactiveActions fcalls ferrors fActiveUsers 
          fInActiveUsers ffromdate ftodate 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bActive bSysIndex bSysLog bSysProp bAction bActiveUser RECT-73 RECT-75 
         RECT-76 binActiveUsr RECT-77 RECT-78 flPeriod brwActions bInactive 
         bNocalls brwUsers bRefresh bCode bDoc berrors bSysenv bSysKey bSysLock 
         bSysRole bUser ffromdate ftodate 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  define buffer ttstatistics for ttstatistics.

  empty temp-table ttstatistics.
  /* Get default period from configuration. */
  publish "getExcludedActions" (output std-ch).
  
  run server/getstatistics.p(input  trim(std-ch,","),
                             input  dtFromDate,
                             input  dtToDate,
                             output table ttstatistics,
                             output std-lo,
                             output std-ch).
  
  
  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.

  run setStatisticsDates in this-procedure.
  run displayData        in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openActionCalledRpt C-Win 
PROCEDURE openActionCalledRpt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  run wactioncalledrpt.w persistent (input ffromdate:input-value,
                                     input ftodate:input-value).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openActionRpt C-Win 
PROCEDURE openActionRpt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipAction as character no-undo.

  do with frame {&frame-name}:
  end.

  run wsysactionrpt.w persistent (input ipAction,
                                  input ffromdate:input-value,
                                  input ftodate:input-value).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openErrorActionRpt C-Win 
PROCEDURE openErrorActionRpt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  run wactionerrorrpt.w persistent (input ffromdate:input-value,
                                    input ftodate:input-value).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openSysAction C-Win 
PROCEDURE openSysAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not valid-handle(hSysAction) 
   then
    run wsysaction.w persistent set hSysAction.
   else
    run showWindow in hSysAction.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openSysAppLog C-Win 
PROCEDURE openSysAppLog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if not valid-handle(hsysAppLogs) 
   then
    run wsysapplog.w persistent set hsysAppLogs.
   else 
    run showWindow in hsysAppLogs.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openSysCode C-Win 
PROCEDURE openSysCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not valid-handle(hSysCode) 
   then
    run wsyscode.w persistent set hSysCode.
   else
    run showWindow in hSysCode.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openSysDoc C-Win 
PROCEDURE openSysDoc :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not valid-handle(hsysDocs) 
   then
    run wsysdoc.w persistent set hsysDocs.
   else
    run showWindow in hsysDocs.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openSysEnv C-Win 
PROCEDURE openSysEnv :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not valid-handle(hsysenvs) 
   then
    run wsysenv.w persistent set hsysenvs.
   else
    run showWindow in hsysenvs.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openSysIndex C-Win 
PROCEDURE openSysIndex :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not valid-handle(hsysIndex) 
   then
    run wsysindex.w persistent set hsysIndex.
   else
    run showWindow in hsysIndex.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openSysKey C-Win 
PROCEDURE openSysKey :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not valid-handle(hsysKey) 
   then
    run wsyskey.w persistent set hsysKey.
   else
    run showWindow in hsysKey.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openSysLock C-Win 
PROCEDURE openSysLock :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not valid-handle(hsysLock) 
   then
    run wsyslock.w persistent set hsysLock.
   else
    run showWindow in hsysLock.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openSysLog C-Win 
PROCEDURE openSysLog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not valid-handle(hsysLogs) 
   then
    run wsyslog.w persistent set hsysLogs.
   else
    run showWindow in hsysLogs.
                                
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openSysProp C-Win 
PROCEDURE openSysProp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not valid-handle(hsysProp) 
   then
    run wsysprop.w persistent set hsysProp.
   else
    run showWindow in hsysProp.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openSysRole C-Win 
PROCEDURE openSysRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not valid-handle(hSysrole) 
   then
    run wsysrole.w persistent set hSysrole.
   else
    run showWindow in hSysrole.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openSysUser C-Win 
PROCEDURE openSysUser :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not valid-handle(hSysUser) 
   then
    run wsysuser.w persistent set hSysUser.
   else
    run showWindow in hSysUser.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openUserRpt C-Win 
PROCEDURE openUserRpt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipAction as character no-undo.

  do with frame {&frame-name}:
  end.

  run wsysuserrpt.w persistent (input ipAction,
                                ffromdate:input-value,
                                ftodate:input-value).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE periodModified C-Win 
PROCEDURE periodModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipiDays as integer no-undo.

  do with frame {&frame-name}:
  end.

  flPeriod:screen-value = string(ipiDays).
  iDays = ipiDays.

  run setStatisticsDates in this-procedure.
  run getData            in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setStatisticsDates C-Win 
PROCEDURE setStatisticsDates :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  flPeriod:screen-value = string(iDays).
  assign   
    ftodate:screen-value    = string(now)         
    ffromdate:screen-value  = string(today - integer(flPeriod:screen-value))
    dtToDate                = now
    dtFromDate              = datetime(today - integer(flPeriod:screen-value))
    .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validateSearch C-Win 
PROCEDURE validateSearch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter oplError as logical.
 
  do with frame {&frame-name}:
  end.

  if flPeriod:input-value > 30
   then
    do:
      message "The days are greater than 30 days, the query will be slow. Are you sure you want to continue?" 
        view-as alert-box warning buttons yes-no update std-lo.
  
      if not std-lo
       then
        do:
          flPeriod:screen-value = string(iDays).
          return.
        end.        
   end.
   
  if flPeriod:input-value > 365
   then
    do:
      message "Days cannot be more than 365 days." 
        view-as alert-box error buttons ok.

      flPeriod:screen-value = string(iDays).
      return.
    end.

  oplError = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hLabel   as handle no-undo.
  define variable hLabel2  as handle no-undo.

  assign
    frame {&frame-name}:width-pixels          = {&window-name}:width-pixels - 10
    frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels - 10
    frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
    frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
    
    hlabel                                    = fActiveUsers:side-label-handle
    hlabel2                                   = fInactiveusers:side-label-handle

    RECT-75:width-pixels                      = frame {&frame-name}:width-pixels  - 13
    RECT-73:width-pixels                      = frame {&frame-name}:width-pixels  - 13

    brwActions:width-pixels                   = frame {&frame-name}:width-pixels  - 30
    brwUsers:width-pixels                     = frame {&frame-name}:width-pixels  - 30

    brwUsers:height-pixels                    = frame {&frame-name}:height-pixels - brwUsers:y - 40
    RECT-73:height-pixels                     = frame {&frame-name}:height-pixels - brwUsers:y + 5
     
    /* Resize fill in*/
    fActiveUsers:y                            = brwUsers:y + brwUsers:height-pixels + 6
    fInactiveusers:y                          = brwUsers:y + brwUsers:height-pixels + 6
    bActiveUser:y                             = brwUsers:y + brwUsers:height-pixels + 4
    binactiveusr:y                            = brwUsers:y + brwUsers:height-pixels + 4
    .

    /* Resize Browse */
    if valid-handle(hlabel) and valid-handle (hlabel2) 
     then 
      assign 
        hlabel:y  = brwUsers:y + brwUsers:height-pixels + 6
        hlabel2:y = brwUsers:y + brwUsers:height-pixels + 6
        . 
  {lib/resize-column.i &col="'uName,calls'" &var=dColumnWidth &browse-name=brwUsers}    
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

