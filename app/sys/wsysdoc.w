&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wsysdoc.w

  Description: Window of system documents

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gurvindar

  Created: 09.25.2018

------------------------------------------------------------------------*/
create widget-pool.

{lib/sys-def.i}
{lib/std-def.i}
{lib/winshowscrollbars.i}

/* Temp-table definitions */
{tt/sysdoc.i}
{tt/sysdoc.i &tableAlias=ttsysdoc}

define variable pList  as character no-undo.
define variable fdays  as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES sysdoc

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData sysdoc.entityType "Entity Type" sysdoc.entityID "Entity ID" sysdoc.entitySeq "Entity Sequence" sysdoc.docDate sysdoc.uid "UID"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each sysdoc
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each sysdoc.
&Scoped-define TABLES-IN-QUERY-brwData sysdoc
&Scoped-define FIRST-TABLE-IN-QUERY-brwData sysdoc


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbentity bdelete fStartDate fEndDate brwData ~
bView bExport bSearch RECT-61 RECT-68 
&Scoped-Define DISPLAYED-OBJECTS cbentity fStartDate fEndDate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bdelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get".

DEFINE BUTTON bView  NO-FOCUS
     LABEL "View" 
     SIZE 7.2 BY 1.71 TOOLTIP "View Entry".

DEFINE VARIABLE cbentity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity Type" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE fEndDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "End Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fStartDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Start Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-61
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 25 BY 2.81.

DEFINE RECTANGLE RECT-68
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 82.8 BY 2.81.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      sysdoc SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      sysdoc.entityType  label        "Entity Type"           format "x(20)"              
 sysdoc.entityID    label        "Entity ID"                  format "x(15)"         
 sysdoc.entitySeq   label        "Entity Sequence"                   
 sysdoc.docDate     column-label "Document! Date"    width 12 format 99/99/99      
 sysdoc.uid         label        "UID"                        format "x(50)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 120.8 BY 16.91 ROW-HEIGHT-CHARS .71 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     cbentity AT ROW 2.33 COL 14 COLON-ALIGNED WIDGET-ID 58
     bdelete AT ROW 1.95 COL 93.4 WIDGET-ID 10 NO-TAB-STOP 
     fStartDate AT ROW 1.76 COL 58.2 COLON-ALIGNED WIDGET-ID 68
     fEndDate AT ROW 2.86 COL 58.2 COLON-ALIGNED WIDGET-ID 66
     brwData AT ROW 4.81 COL 2.2 WIDGET-ID 200
     bView AT ROW 1.95 COL 100.8 WIDGET-ID 70 NO-TAB-STOP 
     bExport AT ROW 1.95 COL 86 WIDGET-ID 2 NO-TAB-STOP 
     bSearch AT ROW 1.91 COL 76.2 WIDGET-ID 64 NO-TAB-STOP 
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.14 COL 86.4 WIDGET-ID 52
     "Parameters" VIEW-AS TEXT
          SIZE 11.6 BY .62 AT ROW 1.1 COL 3.4 WIDGET-ID 56
     RECT-61 AT ROW 1.43 COL 84.6 WIDGET-ID 50
     RECT-68 AT ROW 1.43 COL 2.2 WIDGET-ID 54
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 178.8 BY 22.19 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "System Documents"
         HEIGHT             = 20.86
         WIDTH              = 123.2
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData fEndDate fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each sysdoc.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* System Documents */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* System Documents */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* System Documents */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bdelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bdelete C-Win
ON CHOOSE OF bdelete IN FRAME fMain /* Delete */
do:
  run deleteSysDoc in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
do:
  run showSysDoc in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME fMain /* Get */
do: 
  run getData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bView C-Win
ON CHOOSE OF bView IN FRAME fMain /* View */
DO:
  run showSysDoc in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbentity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbentity C-Win
ON VALUE-CHANGED OF cbentity IN FRAME fMain /* Entity Type */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fEndDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fEndDate C-Win
ON LEAVE OF fEndDate IN FRAME fMain /* End Date */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStartDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStartDate C-Win
ON LEAVE OF fStartDate IN FRAME fMain /* Start Date */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

assign current-window                = {&window-name} 
       this-procedure:current-window = {&window-name}.

on close of this-procedure 
  run disable_UI.

setStatusMessage("").

pause 0 before-hide.
subscribe to "closeWindow" anywhere.

bSearch :load-image            ("images/completed.bmp").
bSearch :load-image-insensitive("images/completed-i.bmp").

bExport :load-image            ("images/excel.bmp").
bExport :load-image-insensitive("images/excel-i.bmp").

bDelete :load-image            ("images/delete.bmp").
bDelete :load-image-insensitive("images/delete-i.bmp").

bView:load-image               ("images/open.bmp").
bView:load-image-insensitive   ("images/open-i.bmp").

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.
  
  /* Populate combo-box cbentity with sysdoc types */
  run getSysDocTypes in this-procedure.

  run setDefaultDays in this-procedure.
  run setWidgetState in this-procedure.

  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteSysDoc C-Win 
PROCEDURE deleteSysDoc :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available sysdoc
   then
    return.

  message "Highlighted Record will be deleted." skip "Do you want to continue?"
      view-as alert-box question buttons yes-no title "Delete Record" update lContinue as logical.

  if not lContinue 
   then
    return.
                                                                                                                                                                                    
  /* server call to delete the syslock records */
  run server/unlinkdocument.p(input  sysdoc.entityType,                                                                                                                             
                              input  sysdoc.entityID,                                                                                                                               
                              input  sysdoc.entitySeq,                                                                                                                              
                              output std-lo,
                              output std-ch).

  if not std-lo
   then
    do:
      message std-ch
        view-as alert-box info buttons ok.
      return.
    end.
  
  delete sysdoc.
  
  /* Refresh data on the screen */
  run displayData in this-procedure.  

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData C-Win 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  open query brwData preselect each sysdoc  by sysdoc.docDate desc.

  run setWidgetState in this-procedure.

  setStatusRecords(query brwData:num-results).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbentity fStartDate fEndDate 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE cbentity bdelete fStartDate fEndDate brwData bView bExport bSearch 
         RECT-61 RECT-68 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
  
  publish "GetReportDir" (output std-ch).
  
  std-ha = temp-table sysdoc:handle.
  
  run util/exporttable.p (table-handle std-ha,
                          "sysdoc",
                          "for each sysdoc ",
                          "entitytype,entityid,entityseq,uid,docDate",
                          "Entity Type,Entity ID,Sequence,Username,Document Date",
                          std-ch,
                          "sysdoc-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer sysdoc for sysdoc.

  do with frame {&frame-name}:
  end.
      
  run server/getsysdocs.p(input cbentity:input-value, 
                          datetime(fStartDate:input-value),
                          datetime(fEndDate:input-value),
                          output table sysdoc,
                          output std-lo,
                          output std-ch).
   
  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
  
  /* Refresh data on the screen */
  run displayData in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getSysDocTypes C-Win 
PROCEDURE getSysDocTypes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* return list of unqiue document entity type*/
  publish "getSysDocTypes" (output pList,
                            output std-lo,
                            output std-ch).
  if not std-lo
   then
    do:
      message std-ch
           view-as alert-box error buttons ok.
      return.
    end.
 
  assign
      cbEntity:list-items   = "ALL" + "," + trim(pList,",")
      cbEntity:screen-value = "ALL"
      .
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setDefaultDays C-Win 
PROCEDURE setDefaultDays :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Return default number of days from dialog config screen */ 
  publish "GetDefaultDays" (output fdays).

  assign
      fEndDate:screen-value   = string(now)
      fStartDate:screen-value = string(today - integer(fdays))
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if (query brwData:num-results) > 0
   then
    assign
        bExport:sensitive    = true
        bdelete:sensitive    = true
        bView:sensitive      = true
        .
   else
    assign
        bExport:sensitive    = false
        bdelete:sensitive    = false
        bView:sensitive      = false      
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showSysDoc C-Win 
PROCEDURE showSysDoc :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttsysdoc for ttsysdoc.

  empty temp-table ttsysdoc.
  
  if not available sysdoc 
   then
    return.

  create ttsysdoc.
  buffer-copy sysdoc to ttsysdoc.

  run dialogsysdoc.w (input table ttsysdoc).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .
  
  c-Win:move-to-top().
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame fMain:width-pixels          = {&window-name}:width-pixels
      frame fMain:virtual-width-pixels  = {&window-name}:width-pixels
      frame fMain:height-pixels         = {&window-name}:height-pixels
      frame fMain:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      brwData:width-pixels              = frame fmain:width-pixels - 13
      brwData:height-pixels             = frame {&frame-name}:height-pixels - {&browse-name}:y - 2
      .
 
  run ShowScrollBars(frame fMain:handle, no, no).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage("Results may not match current parameters.").
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

