&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wsysapplog.w

  Description: Window for logs of non critical error messages

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Sachin Chaturvedi

  Created: 01.16.2020

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
   by this procedure. This is a good default which assures
   that this procedure's triggers and internal procedures 
   will execute in this procedure's storage, and that proper
   cleanup will occur on deletion of the procedure. */

create widget-pool.

{lib/std-def.i}
{lib/sys-def.i}
{lib/winshowscrollbars.i}
{lib/get-column.i} 
/*------------------------Temp table Definitions--------------------*/
{tt/sysapplog.i}
{tt/sysapplog.i &tableAlias="tempsysapplog"}

define temp-table ttsysapplog no-undo like sysapplog
  field clogID as character
  .

/* Variable Definition */
define variable dColumnWidth   as decimal   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttsysapplog

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData ttsysapplog.logDate "Date" ttsysapplog.appCode "Code" ttsysapplog.entityType "Type" ttsysapplog.objAction "Action" ttsysapplog.objName "Object" ttsysapplog.userName "User" ttsysapplog.objDesc "Description"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each ttsysapplog by ttsysapplog.logdate desc
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each ttsysapplog by ttsysapplog.logdate desc.
&Scoped-define TABLES-IN-QUERY-brwData ttsysapplog
&Scoped-define FIRST-TABLE-IN-QUERY-brwData ttsysapplog


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bGet fStartDate fEndDate brwData RECT-62 ~
RECT-63 RECT-61 
&Scoped-Define DISPLAYED-OBJECTS fStartDate fEndDate fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validateData C-Win 
FUNCTION validateData RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON bGet  NO-FOCUS
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search".

DEFINE BUTTON bView  NO-FOCUS
     LABEL "View" 
     SIZE 7.2 BY 1.71 TOOLTIP "View Entry".

DEFINE VARIABLE fEndDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "End Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 54 BY 1 TOOLTIP "Search Criteria (Code,Type,Action,Object,User,Description)" NO-UNDO.

DEFINE VARIABLE fStartDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Start Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-61
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17.8 BY 2.62.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 36.4 BY 2.62.

DEFINE RECTANGLE RECT-63
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 65 BY 2.62.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      ttsysapplog SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      ttsysapplog.logDate      column-label   "Date"     width 28  
 ttsysapplog.appCode      label          "Code"          width 10        format "x(20)" 
 ttsysapplog.entityType   label          "Type"          width 14        format "x(50)"
 ttsysapplog.objAction    label          "Action"        width 30        format "x(100)"
 ttsysapplog.objName      label          "Object"        width 27        format "x(200)"
  ttsysapplog.userName    label          "User"          width 18        format "x(50)"
 ttsysapplog.objDesc      label          "Description"   width 50        format "x(200)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 193.8 BY 16.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bSearch AT ROW 1.91 COL 94.6 WIDGET-ID 308 NO-TAB-STOP 
     bGet AT ROW 1.91 COL 30 WIDGET-ID 64 NO-TAB-STOP 
     fStartDate AT ROW 1.71 COL 13 COLON-ALIGNED WIDGET-ID 54
     fEndDate AT ROW 2.81 COL 13 COLON-ALIGNED WIDGET-ID 58
     bExport AT ROW 1.91 COL 104 WIDGET-ID 2 NO-TAB-STOP 
     fSearch AT ROW 2.29 COL 37.6 COLON-ALIGNED NO-LABEL WIDGET-ID 310
     brwData AT ROW 4.33 COL 2.2 WIDGET-ID 200
     bView AT ROW 1.91 COL 112 WIDGET-ID 4 NO-TAB-STOP 
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.19 COL 104 WIDGET-ID 52
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.19 COL 3.4 WIDGET-ID 62
     "Search" VIEW-AS TEXT
          SIZE 7.6 BY .62 AT ROW 1.19 COL 39.4 WIDGET-ID 314
     RECT-62 AT ROW 1.48 COL 2.2 WIDGET-ID 60
     RECT-63 AT ROW 1.48 COL 38.2 WIDGET-ID 312
     RECT-61 AT ROW 1.48 COL 102.8 WIDGET-ID 50
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 196.4 BY 21.14
         DEFAULT-BUTTON bGet WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Application Logs"
         HEIGHT             = 20.24
         WIDTH              = 196.4
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData fSearch fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON bSearch IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bView IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fSearch IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each ttsysapplog by ttsysapplog.logdate desc.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Application Logs */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Application Logs */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Application Logs */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGet C-Win
ON CHOOSE OF bGet IN FRAME fMain /* Get */
DO:
  if not validateData() 
   then
    return no-apply. 

  run getData in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  run showsysapplog in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME fMain /* Search */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bView C-Win
ON CHOOSE OF bView IN FRAME fMain /* View */
DO:
  run showsysapplog in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fEndDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fEndDate C-Win
ON VALUE-CHANGED OF fEndDate IN FRAME fMain /* End Date */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fMain
DO:
  apply 'choose' to bSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStartDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStartDate C-Win
ON VALUE-CHANGED OF fStartDate IN FRAME fMain /* Start Date */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/****************************  Main Block  ****************************/

{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

assign 
  current-window                = {&window-name} 
  this-procedure:current-window = {&window-name}
  .

{lib/win-main.i}
{lib/win-status.i}

on close of this-procedure 
  run disable_UI.

setStatusMessage("").

pause 0 before-hide.

bGet:load-image               ("images/completed.bmp").
bGet:load-image-insensitive   ("images/completed-i.bmp").

bSearch:load-image            ("images/find.bmp").
bSearch:load-image-insensitive("images/find-i.bmp").

bExport:load-image            ("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").

bView:load-image              ("images/open.bmp").
bView:load-image-insensitive  ("images/open-i.bmp").

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

  run enable_UI.  

  /* get the column width */
  {lib/get-column-width.i &col="'Action'"  &var=dColumnWidth} 
  
  /* procedure to set default start and end date */
  run setData in this-procedure.

  run showWindow in this-procedure.
  
  run windowResized in this-procedure.

  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fStartDate fEndDate fSearch 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bGet fStartDate fEndDate brwData RECT-62 RECT-63 RECT-61 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if query brwData:num-results = 0 
   then
    do:
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.

  publish "GetReportDir" (output std-ch).
  
  std-ha = temp-table ttsysapplog:handle.
  run util/exporttable.p (table-handle std-ha,
                          "ttsysapplog",
                          "for each ttsysapplog ",
                          "logDate,appCode,entityType,entityID,entitySeq,entityName,objAction,objID,objName,objRef,uid,userName,objDesc",
                          "From " + fStartDate:screen-value + " To " + fEndDate:screen-value + chr(10) + "Date,Code,Type,ID,Seq,Name,Action,Object ID,Object,Reference,User ID,User,Description",
                          std-ch,
                          "ApplicationLogs-" + replace(string(now,"99-99-99"),"-","") + "-" + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).

  if std-ch <> ""
   then
    do:
      message std-ch
          view-as alert-box warning buttons ok.
      return.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount  as integer   no-undo.
  define variable cStatus as character no-undo.
  
  define buffer sysapplog for sysapplog.

  close query brwData.
  empty temp-table ttsysapplog.
  
  do with frame {&frame-name}:
  end.

  for each sysapplog:
    /* test if the record contains the search text */
    if fSearch:screen-value <> "" and 
      not ((sysapplog.appCode       matches "*" + fSearch:input-value + "*") or 
           (sysapplog.entityType    matches "*" + fSearch:input-value + "*") or 
           (sysapplog.objAction    matches "*" + fSearch:input-value + "*")  or
           (string(sysapplog.entitySeq)     matches "*" + fSearch:input-value + "*") or
           (sysapplog.objName       matches "*" + fSearch:input-value + "*") or 
           (sysapplog.objDesc       matches "*" + fSearch:input-value + "*") or 
           (sysapplog.userName      matches "*" + fSearch:input-value + "*") or
           (sysapplog.entityName    matches "*" + fSearch:input-value + "*")
          )
     then
      next.
    
    create ttsysapplog.
    buffer-copy sysapplog to ttsysapplog.
    
  end.
  
  open query brwData preselect each ttsysapplog by ttsysapplog.logDate desc.
  
  if (query brwData:num-results) > 0
   then
    assign
      bExport:sensitive = true
      bView:sensitive   = true.
   else
    assign
      bExport:sensitive = false
      bView:sensitive   = false.
   
  

  setStatusCount(query brwData:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  run server/getsysapplogs.p(input "All",   /* appcode */
                             fStartDate:input-value,
                             fendDate:input-value,
                             output table sysapplog,
                             output std-lo,
                             output std-ch).
  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.
    
  for each sysapplog:
    sysapplog.entityType = if sysapplog.entityType = "A" or sysapplog.entityType = "G" then "Agent"
                           else if sysapplog.entityType = "C" then "Company"
                           else sysapplog.entityType.
                           
  end.

  run filterData.

  if (query brwData:num-results) > 0
   then
    assign
      fSearch:sensitive = true
      bSearch:sensitive = true
      .
   else
    assign
      fSearch:sensitive = false
      bSearch:sensitive = false
      .

  setStatusRecords(query brwData:num-results).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData C-Win 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Get default no. of days from dialog config to initialise start date */
  publish "getDefaultdays" (output std-in).

  assign
    fStartDate:screen-value = string(today - std-in)
    fEndDate:screen-value   = string(today)
    .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showSysappLog C-Win 
PROCEDURE showSysappLog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  if not available ttsysapplog
   then return.
 /* tempsysapplog
  empty temp-table sysapplog.
  
      create sysapplog.
      buffer-copy ttsysapplog to sysapplog.  */
      
      
  empty temp-table tempsysapplog.
  
      create tempsysapplog.
      buffer-copy ttsysapplog to tempsysapplog. 
   
  run dialogsysapplog.w (input table tempsysapplog).
    

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized
   then
    {&window-name}:window-state = window-normal .
  
  c-Win:move-to-top(). 

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i}   

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
    frame fMain:width-pixels          = {&window-name}:width-pixels
    frame fMain:virtual-width-pixels  = {&window-name}:width-pixels
    frame fMain:height-pixels         = {&window-name}:height-pixels
    frame fMain:virtual-height-pixels = {&window-name}:height-pixels

    /* fMain Components */
    brwData:width-pixels              = frame fmain:width-pixels  - 13
    brwData:height-pixels             = frame fMain:height-pixels - 70
    .

  {lib/resize-column.i &col="'createDate,Action,UID'" &var=dColumnWidth}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validateData C-Win 
FUNCTION validateData RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if interval(fEndDate:input-value, fStartDate:input-value, "years") > 0
  then
    do:
      message "Date range cannot be more than a year." 
        view-as alert-box error buttons ok.
     
      return false.
    end.

  if interval(fEndDate:input-value, fStartDate:input-value, "days") > 30 
   then
    do:
      message "The date range is greater than 30 days, the report will be slow. Are you sure you want to continue?" 
        view-as alert-box warning buttons yes-no update std-lo.
     
      if not std-lo 
       then
        return false.
    end.

  return true.   /* function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

