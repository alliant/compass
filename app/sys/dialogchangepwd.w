&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File:dialogchangepwd.w

  Description:Dialog to change password of sysuser. 

  Input Parameters:ipcUserID(uid)
  
  Input/output Parameters:
                        
  Author:Rahul Sharma

  Created:10.03.2018 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{lib/std-def.i}

/* Parameters Definitions ---                                           */
define input parameter  ipcUserID  as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bEye tNewPassword bCancel bOk 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validateNewPassword Dialog-Frame 
FUNCTION validateNewPassword RETURNS LOGICAL PRIVATE
  (ipcPassword as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bEye  NO-FOCUS
     LABEL "View" 
     SIZE 4.8 BY 1.14 TOOLTIP "View/Hide password".

DEFINE BUTTON bOk 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tNewPassword AS CHARACTER FORMAT "X(20)":U 
     LABEL "New" 
     VIEW-AS FILL-IN 
     SIZE 32 BY 1 TOOLTIP "Enter your new password" NO-UNDO.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 58 BY 5.48.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bEye AT ROW 1.29 COL 46.2 WIDGET-ID 24 NO-TAB-STOP 
     tNewPassword AT ROW 1.38 COL 11.8 COLON-ALIGNED WIDGET-ID 4 PASSWORD-FIELD 
     bCancel AT ROW 9.29 COL 15.4
     bOk AT ROW 9.29 COL 31.4
     "- Must contain at least one special character" VIEW-AS TEXT
          SIZE 44.2 BY .62 AT ROW 6.95 COL 2.8 WIDGET-ID 18
     "! @ $ % # & ^ * ( ) _ + - =" VIEW-AS TEXT
          SIZE 24 BY .62 AT ROW 7.71 COL 6 WIDGET-ID 36
     "Password Restrictions" VIEW-AS TEXT
          SIZE 26 BY .62 AT ROW 3.1 COL 3 WIDGET-ID 12
          FONT 6
     "- Must be different from your last four passwords" VIEW-AS TEXT
          SIZE 47.2 BY .62 AT ROW 6.19 COL 2.8 WIDGET-ID 22
     "- Must be between 8 and 20 characters in length" VIEW-AS TEXT
          SIZE 48.2 BY .62 AT ROW 5.48 COL 2.8 WIDGET-ID 14
     "- Must contain at least one number" VIEW-AS TEXT
          SIZE 45 BY .62 AT ROW 4.76 COL 2.8 WIDGET-ID 16
     "- Must contain a capital letter" VIEW-AS TEXT
          SIZE 35.2 BY .62 AT ROW 4.05 COL 2.8 WIDGET-ID 34
     RECT-6 AT ROW 3.33 COL 2 WIDGET-ID 20
     SPACE(0.39) SKIP(1.80)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Set Password"
         DEFAULT-BUTTON bOk CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR RECTANGLE RECT-6 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tNewPassword IN FRAME Dialog-Frame
   NO-DISPLAY                                                           */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX Dialog-Frame
/* Query rebuild information for DIALOG-BOX Dialog-Frame
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX Dialog-Frame */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Set Password */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEye
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEye Dialog-Frame
ON MOUSE-SELECT-DOWN OF bEye IN FRAME Dialog-Frame /* View */
DO:
  assign
      tNewPassword:password-field = false
      tNewPassword:read-only      = true
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEye Dialog-Frame
ON MOUSE-SELECT-UP OF bEye IN FRAME Dialog-Frame /* View */
DO:
  assign
      tNewPassword:password-field = true
      tNewPassword:read-only      = false
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOk
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOk Dialog-Frame
ON CHOOSE OF bOk IN FRAME Dialog-Frame /* Save */
DO:
  run savePassword in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNewPassword
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNewPassword Dialog-Frame
ON LEAVE OF tNewPassword IN FRAME Dialog-Frame /* New */
DO:
  std-lo = validateNewPassword(self:screen-value).

  if not std-lo
   then
    assign
        self:bgcolor = 14
        self:tooltip = "Does not meet restrictions"
        .
   else 
    assign
        self:bgcolor = ?
        self:tooltip = "Enter your new password"
        .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* centers the window */
{lib/win-main.i}

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bEye:load-image("images/s-eye.bmp").
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
REPEAT ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  WAIT-FOR GO OF FRAME {&frame-name}.
  
  leave MAIN-BLOCK.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE bEye tNewPassword bCancel bOk 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE savePassword Dialog-Frame 
PROCEDURE savePassword :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  std-lo = validateNewPassword(tNewPassword:input-value).
  
  if not std-lo 
   then
    do:
      message "The new password does not meet the restrictions."
       view-as alert-box warning buttons ok.
      return.
    end.
   
  run server/changepassword.p (input ipcUserID,
                               input tNewPassword:input-value,
                               output std-lo,
                               output std-ch). 
  
  if not std-lo 
   then
    do:
      message std-ch view-as alert-box warning buttons ok.
      tNewPassword:screen-value = "".
      apply "ENTRY" to tNewPassword.
      return.
    end.
   else
    apply "WINDOW-CLOSE" to frame {&frame-name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validateNewPassword Dialog-Frame 
FUNCTION validateNewPassword RETURNS LOGICAL PRIVATE
  (ipcPassword as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
  Notes:  
------------------------------------------------------------------------------*/
 define variable hasNumber  as logical initial false.
 define variable hasSpecial as logical initial false.
 define variable hasCapital as logical initial false.

 if length(ipcPassword) < 8 or length(ipcPassword) > 20 
  then return false.
  
 if index(ipcPassword, ":") > 0
  then return false.

 do std-in = 1 to length(ipcPassword):
   std-ch = substring(ipcPassword, std-in, 1).
   if index("0123456789", std-ch) > 0 
    then hasNumber = true.
   if index("!@$%#&^*()_+-=", std-ch) > 0 
    then hasSpecial = true.
   /* check for capital letter */
   if asc(upper(std-ch)) >= 65 and asc(upper(std-ch)) <= 90 and asc(std-ch) = asc(upper(std-ch))
    then hasCapital = true.
 end.

 return (hasNumber and hasSpecial and hasCapital).
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

