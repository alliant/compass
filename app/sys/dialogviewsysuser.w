&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME log-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS log-Frame 
/*------------------------------------------------------------------------

  File: dialogViewSysUser.w

  Description: View the user only 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gurvindar Singh

  Created:03/14/19
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{tt/sysuser.i}

define input parameter table for sysuser.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME log-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fUid fName finitial femail eRoles ~
flCreatedate flpasswordSetDate eComments 
&Scoped-Define DISPLAYED-OBJECTS fUid fName finitial tactive femail eRoles ~
flCreatedate flpasswordSetDate eComments tpasswordExpired 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE VARIABLE eComments AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 51.6 BY 2.24 NO-UNDO.

DEFINE VARIABLE eRoles AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 51.6 BY 2.24 NO-UNDO.

DEFINE VARIABLE femail AS CHARACTER FORMAT "X(256)":U 
     LABEL "Email" 
     VIEW-AS FILL-IN 
     SIZE 51.6 BY 1 NO-UNDO.

DEFINE VARIABLE finitial AS CHARACTER FORMAT "X(256)":U 
     LABEL "Initials" 
     VIEW-AS FILL-IN 
     SIZE 12.8 BY 1 NO-UNDO.

DEFINE VARIABLE flCreatedate AS DATE FORMAT "99/99/9999":U 
     LABEL "Create" 
     VIEW-AS FILL-IN 
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE flpasswordSetDate AS DATE FORMAT "99/99/9999":U 
     LABEL "Password Set On" 
     VIEW-AS FILL-IN 
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE fName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 39.8 BY 1 NO-UNDO.

DEFINE VARIABLE fUid AS CHARACTER FORMAT "X(256)":U 
     LABEL "UID" 
     VIEW-AS FILL-IN 
     SIZE 39.8 BY 1 NO-UNDO.

DEFINE VARIABLE tactive AS LOGICAL INITIAL no 
     LABEL "Active" 
     VIEW-AS TOGGLE-BOX
     SIZE 10.8 BY .81 NO-UNDO.

DEFINE VARIABLE tpasswordExpired AS LOGICAL INITIAL no 
     LABEL "Password Expired" 
     VIEW-AS TOGGLE-BOX
     SIZE 20.4 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME log-Frame
     fUid AT ROW 1.33 COL 19.6 COLON-ALIGNED WIDGET-ID 20
     fName AT ROW 2.52 COL 19.6 COLON-ALIGNED WIDGET-ID 2
     finitial AT ROW 3.71 COL 19.6 COLON-ALIGNED WIDGET-ID 6
     tactive AT ROW 2.67 COL 62.6 WIDGET-ID 14 NO-TAB-STOP 
     femail AT ROW 4.91 COL 19.6 COLON-ALIGNED WIDGET-ID 16
     eRoles AT ROW 6.14 COL 21.6 NO-LABEL WIDGET-ID 92
     flCreatedate AT ROW 8.67 COL 19.6 COLON-ALIGNED WIDGET-ID 40
     flpasswordSetDate AT ROW 9.86 COL 19.6 COLON-ALIGNED WIDGET-ID 42
     eComments AT ROW 11.05 COL 21.6 NO-LABEL WIDGET-ID 8
     tpasswordExpired AT ROW 9.95 COL 43 WIDGET-ID 46 NO-TAB-STOP 
     "Selected Roles:" VIEW-AS TEXT
          SIZE 15 BY .62 AT ROW 6 COL 6 WIDGET-ID 94
     "Comment:" VIEW-AS TEXT
          SIZE 9.2 BY .62 AT ROW 11 COL 11.8 WIDGET-ID 38
     SPACE(53.79) SKIP(1.94)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "View System User" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX log-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME log-Frame:SCROLLABLE       = FALSE
       FRAME log-Frame:HIDDEN           = TRUE.

ASSIGN 
       eComments:READ-ONLY IN FRAME log-Frame        = TRUE.

ASSIGN 
       eRoles:READ-ONLY IN FRAME log-Frame        = TRUE.

ASSIGN 
       femail:READ-ONLY IN FRAME log-Frame        = TRUE.

ASSIGN 
       finitial:READ-ONLY IN FRAME log-Frame        = TRUE.

ASSIGN 
       flCreatedate:READ-ONLY IN FRAME log-Frame        = TRUE.

ASSIGN 
       flpasswordSetDate:READ-ONLY IN FRAME log-Frame        = TRUE.

ASSIGN 
       fName:READ-ONLY IN FRAME log-Frame        = TRUE.

ASSIGN 
       fUid:READ-ONLY IN FRAME log-Frame        = TRUE.

/* SETTINGS FOR TOGGLE-BOX tactive IN FRAME log-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tpasswordExpired IN FRAME log-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME log-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL log-Frame log-Frame
ON WINDOW-CLOSE OF FRAME log-Frame /* View System User */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK log-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  /* displaying the data */
  run setData in this-procedure.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI log-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME log-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI log-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fUid fName finitial tactive femail eRoles flCreatedate 
          flpasswordSetDate eComments tpasswordExpired 
      WITH FRAME log-Frame.
  ENABLE fUid fName finitial femail eRoles flCreatedate flpasswordSetDate 
         eComments 
      WITH FRAME log-Frame.
  VIEW FRAME log-Frame.
  {&OPEN-BROWSERS-IN-QUERY-log-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData log-Frame 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* Filling data in widgets */
  find first sysuser no-error.
  if not available sysuser
   then return.
  
  assign
      fUid:screen-value              = sysuser.uid
      fName:screen-value             = sysUser.name 
      finitial:screen-value          = sysUser.initials 
      femail:screen-value            = sysUser.email 
      tactive:checked                = sysUser.isActive
      tpasswordExpired:checked       = sysUser.passwordExpired
      flCreatedate:screen-value      = string(sysuser.createDate)
      flpasswordSetDate:screen-value = string(sysuser.passwordSetDate)
      ecomments:screen-value         = sysUser.comments
      eRoles:screen-value            = sysUser.role
      .
  
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

