&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File:dialogassignaction.w 

  Description:Dialog to select multiple actions for assigning a role. 

  Input Parameters:
      ipcRoleID  :<char> Role ID

  Output Parameters:
      oplSuccess :<logical> Success/Failure

  Author: Rahul Sharma

  Created:10.17.2018 
  Modified:
 Date       Name      Description
 08/22/2019 Gurvindar Removed progress error while populating combo-box.
  
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/
/* ***************************  Definitions  ************************** */

 /* Parameters Definitions ---    */
define input  parameter ipcRoleID  as character no-undo.
define output parameter oplSuccess as logical no-undo.

/* Standard Libraries */
{lib/std-def.i}
{lib/sys-def.i}
{lib/winlaunch.i}
{lib/brw-multi-def.i} 

/* Temp-table Definitions ---                                           */
{tt/sysaction.i &tableAlias=ttsysaction}
{tt/sysaction.i &tableAlias=ttSelectedAction}

/* --- Local Variable Definitions --- */
define variable chRoleList  as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwAvailableAct

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttsysaction ttSelectedAction

/* Definitions for BROWSE brwAvailableAct                               */
&Scoped-define FIELDS-IN-QUERY-brwAvailableAct ttsysaction.action ttsysaction.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAvailableAct   
&Scoped-define SELF-NAME brwAvailableAct
&Scoped-define QUERY-STRING-brwAvailableAct for each ttsysaction by ttsysaction.action
&Scoped-define OPEN-QUERY-brwAvailableAct open query {&SELF-NAME} for each ttsysaction by ttsysaction.action.
&Scoped-define TABLES-IN-QUERY-brwAvailableAct ttsysaction
&Scoped-define FIRST-TABLE-IN-QUERY-brwAvailableAct ttsysaction


/* Definitions for BROWSE brwSelectedActions                            */
&Scoped-define FIELDS-IN-QUERY-brwSelectedActions ttSelectedAction.action ttSelectedAction.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwSelectedActions   
&Scoped-define SELF-NAME brwSelectedActions
&Scoped-define QUERY-STRING-brwSelectedActions for each ttSelectedAction by ttSelectedAction.action
&Scoped-define OPEN-QUERY-brwSelectedActions open query {&SELF-NAME} for each ttSelectedAction by ttSelectedAction.action.
&Scoped-define TABLES-IN-QUERY-brwSelectedActions ttSelectedAction
&Scoped-define FIRST-TABLE-IN-QUERY-brwSelectedActions ttSelectedAction


/* Definitions for DIALOG-BOX Dialog-Frame                              */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbRole brwAvailableAct brwSelectedActions ~
BtnOK BtnCancel 
&Scoped-Define DISPLAYED-OBJECTS cbRole 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON baddAction 
     LABEL "Add Action >" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add Action".

DEFINE BUTTON bAddAllAction 
     LABEL "Add All Actions >>" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add All Actions".

DEFINE BUTTON bRemoveAction 
     LABEL "< Remove Action" 
     SIZE 4.8 BY 1.14 TOOLTIP "Remove Action".

DEFINE BUTTON bRemoveAll 
     LABEL "<< Remove All Actions" 
     SIZE 4.8 BY 1.14 TOOLTIP "Remove All Actions".

DEFINE BUTTON BtnCancel AUTO-END-KEY DEFAULT 
     LABEL "Cancel" 
     SIZE 12 BY 1.14 TOOLTIP "Cancel".

DEFINE BUTTON BtnOK AUTO-GO DEFAULT 
     LABEL "Save" 
     SIZE 12 BY 1.14 TOOLTIP "Save".

DEFINE VARIABLE cbRole AS CHARACTER FORMAT "X(256)":U 
     LABEL "Role" 
     VIEW-AS COMBO-BOX INNER-LINES 11
     LIST-ITEMS "Select Role" 
     DROP-DOWN-LIST
     SIZE 32 BY 1 TOOLTIP "Select Role" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAvailableAct FOR 
      ttsysaction SCROLLING.

DEFINE QUERY brwSelectedActions FOR 
      ttSelectedAction SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAvailableAct
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAvailableAct Dialog-Frame _FREEFORM
  QUERY brwAvailableAct DISPLAY
      ttsysaction.action         column-label "Name"         format "x(30)"         width 28
ttsysaction.description    column-label "Description"  format "x(70)"   width 40
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 94.4 BY 15.81
         TITLE "Available Actions" ROW-HEIGHT-CHARS .81 NO-EMPTY-SPACE.

DEFINE BROWSE brwSelectedActions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwSelectedActions Dialog-Frame _FREEFORM
  QUERY brwSelectedActions DISPLAY
      ttSelectedAction.action   column-label "Name"         format "x(30)"     width 28
ttSelectedAction.description    column-label "Description"  format "x(70)"     width 40
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 99.6 BY 15.81
         TITLE "Selected Actions" ROW-HEIGHT-CHARS .81 NO-EMPTY-SPACE.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     cbRole AT ROW 2.24 COL 6.4 COLON-ALIGNED WIDGET-ID 320
     brwAvailableAct AT ROW 4.1 COL 2.6 WIDGET-ID 300
     brwSelectedActions AT ROW 4.1 COL 111 WIDGET-ID 400
     bAddAllAction AT ROW 9.62 COL 101.6 WIDGET-ID 310
     baddAction AT ROW 10.81 COL 101.6 WIDGET-ID 322
     bRemoveAction AT ROW 12 COL 101.6 WIDGET-ID 312
     bRemoveAll AT ROW 13.19 COL 101.6 WIDGET-ID 324
     BtnOK AT ROW 20.48 COL 91.4 WIDGET-ID 208
     BtnCancel AT ROW 20.48 COL 105.4 WIDGET-ID 204
     SPACE(93.79) SKIP(0.28)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Assign Role To Action"
         DEFAULT-BUTTON BtnOK CANCEL-BUTTON BtnCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
/* BROWSE-TAB brwAvailableAct cbRole Dialog-Frame */
/* BROWSE-TAB brwSelectedActions brwAvailableAct Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON baddAction IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bAddAllAction IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bRemoveAction IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bRemoveAll IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       brwAvailableAct:ALLOW-COLUMN-SEARCHING IN FRAME Dialog-Frame = TRUE
       brwAvailableAct:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE.

ASSIGN 
       brwSelectedActions:ALLOW-COLUMN-SEARCHING IN FRAME Dialog-Frame = TRUE
       brwSelectedActions:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAvailableAct
/* Query rebuild information for BROWSE brwAvailableAct
     _START_FREEFORM
open query {&SELF-NAME} for each ttsysaction by ttsysaction.action.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwAvailableAct */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwSelectedActions
/* Query rebuild information for BROWSE brwSelectedActions
     _START_FREEFORM
open query {&SELF-NAME} for each ttSelectedAction by ttSelectedAction.action.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwSelectedActions */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Assign Role To Action */
DO:
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME baddAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL baddAction Dialog-Frame
ON CHOOSE OF baddAction IN FRAME Dialog-Frame /* Add Action > */
do:
  run AddAction in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddAllAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddAllAction Dialog-Frame
ON CHOOSE OF bAddAllAction IN FRAME Dialog-Frame /* Add All Actions >> */
do:
  run addAllAction in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRemoveAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRemoveAction Dialog-Frame
ON CHOOSE OF bRemoveAction IN FRAME Dialog-Frame /* < Remove Action */
do:
  run removeAction in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRemoveAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRemoveAll Dialog-Frame
ON CHOOSE OF bRemoveAll IN FRAME Dialog-Frame /* << Remove All Actions */
do:
  run removeAllAction in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAvailableAct
&Scoped-define SELF-NAME brwAvailableAct
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAvailableAct Dialog-Frame
ON DEFAULT-ACTION OF brwAvailableAct IN FRAME Dialog-Frame /* Available Actions */
do:
  apply "choose" to bAddAction.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAvailableAct Dialog-Frame
ON ROW-DISPLAY OF brwAvailableAct IN FRAME Dialog-Frame /* Available Actions */
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAvailableAct Dialog-Frame
ON START-SEARCH OF brwAvailableAct IN FRAME Dialog-Frame /* Available Actions */
DO:
  define variable tWhereClause as character no-undo.
  {lib/brw-startSearch-multi.i &browseName="brwAvailableAct" &whereClause="tWhereClause"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAvailableAct Dialog-Frame
ON VALUE-CHANGED OF brwAvailableAct IN FRAME Dialog-Frame /* Available Actions */
DO:
  if available ttsysaction
   then
    assign
        bAddAllAction:sensitive = true 
        baddAction :sensitive   = true 
        .
   else
    assign
        bAddAllAction:sensitive = false 
        baddAction :sensitive   = false 
        .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwSelectedActions
&Scoped-define SELF-NAME brwSelectedActions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSelectedActions Dialog-Frame
ON DEFAULT-ACTION OF brwSelectedActions IN FRAME Dialog-Frame /* Selected Actions */
DO:
  apply "choose" to bRemoveAction.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSelectedActions Dialog-Frame
ON ROW-DISPLAY OF brwSelectedActions IN FRAME Dialog-Frame /* Selected Actions */
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSelectedActions Dialog-Frame
ON START-SEARCH OF brwSelectedActions IN FRAME Dialog-Frame /* Selected Actions */
do:
  define variable tWhereClause as character no-undo.
  {lib/brw-startSearch-multi.i &browseName="brwSelectedActions" &whereClause="tWhereClause"}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnCancel Dialog-Frame
ON CHOOSE OF BtnCancel IN FRAME Dialog-Frame /* Cancel */
do:
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnOK Dialog-Frame
ON CHOOSE OF BtnOK IN FRAME Dialog-Frame /* Save */
do:
  run assignAction in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRole Dialog-Frame
ON VALUE-CHANGED OF cbRole IN FRAME Dialog-Frame /* Role */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAvailableAct
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? then
  frame {&frame-name}:parent = active-window.


/* Now enable the interface and wait for the exit condition.*/

/* getting the roles list */
publish "getSysRoleList" (input ",",
                          output chRoleList,
                          output std-lo,
                          output std-ch).
if not std-lo
 then
  do:
    message std-ch
        view-as alert-box info buttons ok.
    return.
  end.
  
cbRole:list-items = {&SelectRole}.
if chRoleList ne ""
 then
  cbRole:list-items   = cbRole:list-items + ","  + trim(chRoleList,",") no-error .
 
bAddAllAction:load-image("images/s-nextpg.bmp").
bremoveall   :load-image("images/s-previouspg.bmp").
baddAction   :load-image("images/s-next.bmp").
bRemoveAction:load-image("images/s-previous.bmp").

/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.

  if lookup(ipcRoleID,cbRole:list-items) = 0
   then
    cbRole:add-last(ipcRoleID).
  
  cbRole:screen-value = ipcRoleID.
  
  /* Getting the data */
  apply 'value-changed' to cbRole.

  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AddAction Dialog-Frame 
PROCEDURE AddAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if not available ttsysaction 
   then return.

  if can-find(first ttSelectedAction where ttSelectedAction.action = ttsysaction.action) 
   then return.
        
  create ttSelectedAction.
  buffer-copy ttsysaction to ttSelectedAction.
  std-ro = rowid(ttSelectedAction).
  delete ttsysaction.
  
  open query brwAvailableAct    for each ttsysaction      by ttsysaction.action.
  open query brwSelectedActions for each ttSelectedAction by ttSelectedAction.action. 
  
  reposition brwSelectedActions to rowid std-ro.
  
  if num-results("brwAvailableAct") = 0 
   then
    assign
        baddAction :sensitive   = false
        bAddAllAction:sensitive = false
        .
   else
    assign
        baddAction :sensitive   = false
        bAddAllAction:sensitive = false
        .
  assign
      BtnOK :sensitive        = true
      bRemoveAction:sensitive = true
      bRemoveAll:sensitive    = true
      .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addAllAction Dialog-Frame 
PROCEDURE addAllAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  for each ttsysaction:                                                                              
    if not can-find(first ttSelectedAction where ttSelectedAction.action = ttsysaction.action) 
     then  
      do:                                                                                              
        create ttSelectedAction.                                                                       
        buffer-copy ttsysaction to ttSelectedAction.                                                   
        delete ttsysaction.                                                                            
      end.                                                                                             
  end.                                                                                               
                                                                                                     
  open query brwAvailableAct    for each ttsysaction      by ttsysaction.action.                             
  open query brwSelectedActions for each ttSelectedAction by ttSelectedAction.action.                
                                                                                                     
  if num-results("brwAvailableAct") = 0 
   then   
    assign
        bAddAllAction:sensitive = false
        baddAction:sensitive    = false
        .
   else                                                                                               
    assign
        bAddAllAction:sensitive = true
        baddAction:sensitive    = true
        .                                       
  assign
      BtnOK:sensitive         = true
      bRemoveAction:sensitive = true
      bRemoveAll:sensitive    = true
      .                                                                                               
                                                                                                    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assignAction Dialog-Frame 
PROCEDURE assignAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cSelectedActionList as character no-undo.

  for each ttSelectedAction:
    cSelectedActionList = cSelectedActionList + "," + ttSelectedAction.action.
  end.

  cSelectedActionList = trim(cSelectedActionList,",").

  publish "assignAction" (input cbRole:screen-value in frame {&frame-name},
                          input cSelectedActionList,
                          output oplSuccess,
                          output std-ch).

  if not oplSuccess 
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return no-apply.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbRole 
      WITH FRAME Dialog-Frame.
  ENABLE cbRole brwAvailableAct brwSelectedActions BtnOK BtnCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData Dialog-Frame 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  empty temp-table ttsysaction.

  BtnOK:sensitive = false .

  if cbRole:screen-value <> {&SelectRole}
   then
    do:
      /* getting all the actions */
      publish "GetSysActions" ( output table ttsysaction,
                                output std-lo,
                                output std-ch).

      if not std-lo  /* if fails return */
       then
        do:
          message std-ch
              view-as alert-box info buttons ok.
          return.
        end.

      for each ttsysaction:
        if lookup(cbRole:screen-value,ttsysaction.roles) > 0 
         then
          delete ttsysaction.
      end.
  
      open query brwAvailableAct for each ttsysaction.
      
      if num-results("brwSelectedActions") <> 0 
       then
        assign
            bRemoveAction:sensitive = true
            bRemoveAll :sensitive   = true
            .
       else
        assign
            bRemoveAction:sensitive = false
            bRemoveAll :sensitive   = false
            .
  
      apply "value-changed" to brwAvailableAct.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE removeAction Dialog-Frame 
PROCEDURE removeAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  if not available ttSelectedAction
   then return.
    
  if can-find(first ttsysaction where ttsysaction.action = ttSelectedAction.action) 
   then return.

  create ttsysaction.
  buffer-copy ttSelectedAction to ttsysaction.
  std-ro = rowid(ttsysaction).
  delete ttSelectedAction.
  
  open query brwAvailableAct    for each ttsysaction      by ttsysaction.action.
  open query brwSelectedActions for each ttSelectedAction by ttSelectedAction.action. 
  
  reposition brwAvailableAct to rowid std-ro.
  
  if num-results("brwSelectedActions") = 0 
   then
    assign
        bRemoveAction :sensitive = false
        bRemoveAll    :sensitive = false
        .
   else
    assign
        bRemoveAction :sensitive = true
        bRemoveAll    :sensitive = true
        .
  assign
      BtnOK :sensitive        = true
      bAddAllAction:sensitive = true
      baddAction:sensitive    = true
      .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE removeAllAction Dialog-Frame 
PROCEDURE removeAllAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  for each ttSelectedAction:
    if not can-find(first ttsysaction where ttsysaction.action = ttSelectedAction.action) 
     then
      do:
        create ttsysaction.
        buffer-copy ttSelectedAction to ttsysaction.
        delete ttSelectedAction.
      end.
  end.
  
  open query brwAvailableAct    for each ttsysaction      by ttsysaction.action.
  open query brwSelectedActions for each ttSelectedAction by ttSelectedAction.action. 
  
  if num-results("brwSelectedActions") = 0 
   then
    assign
        bRemoveAction:sensitive = false
        bRemoveAll:sensitive    = false
        .
  else
   assign
       bRemoveAction:sensitive = true
       bRemoveAll:sensitive    = true
       .

  assign
      BtnOK:sensitive         = true
      bAddAllAction:sensitive = true
      baddAction:sensitive    = true
      .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData Dialog-Frame 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

