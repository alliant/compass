&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wsysfavorite.w 

  Description:User Interface for system favorite records. 

  Input Parameters:
       <none>
       
  Output Parameters:
        <none>
        
  Author: Rahul 

  Created:07.02.19 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

  create widget-pool.
  
/* ***************************  Definitions  ************************** */
  {lib/sys-def.i}
  {lib/std-def.i}
  {lib/get-column.i}
  {lib/winshowscrollbars.i}
  
  /* Temp table definitions */
  {tt/sysfavorite.i}
  {tt/sysfavorite.i &tableAlias =ttsysfavorite}
  {tt/sysfavorite.i &tableAlias =tsysfavorite}
  {tt/person.i}
  {tt/agent.i}


 /* Variable Definition */
  define variable hTextPerson        as handle    no-undo.
  define variable hTextAgent         as handle    no-undo.
  define variable hSelectionPerson   as handle    no-undo.
  define variable hSelectionAgent    as handle    no-undo.

  define variable dColumnWidth       as decimal   no-undo.
  
  define variable UID                as character no-undo.
  define variable cEntity            as character no-undo.
  define variable cbEntityIdSelected as character no-undo.
  define variable cLastSearchString  as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwsysfavorite

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttsysfavorite

/* Definitions for BROWSE brwsysfavorite                                */
&Scoped-define FIELDS-IN-QUERY-brwsysfavorite ttsysfavorite.uid ttsysfavorite.entity if (ttsysfavorite.entityID eq '""' or ttsysfavorite.entityID eq "") then "" else ttsysfavorite.entityID ttsysfavorite.entityName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwsysfavorite   
&Scoped-define SELF-NAME brwsysfavorite
&Scoped-define QUERY-STRING-brwsysfavorite for each ttsysfavorite
&Scoped-define OPEN-QUERY-brwsysfavorite open query {&SELF-NAME} for each ttsysfavorite.
&Scoped-define TABLES-IN-QUERY-brwsysfavorite ttsysfavorite
&Scoped-define FIRST-TABLE-IN-QUERY-brwsysfavorite ttsysfavorite


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwsysfavorite}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-62 RECT-63 RECT-84 cbUID cbEntity ~
bRefresh bGet brwsysfavorite 
&Scoped-Define DISPLAYED-OBJECTS cbUID cbEntity cbPerson cbAgent cbEntityId ~
fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getEntity C-Win 
FUNCTION getEntity RETURNS CHARACTER
  ( cEntity as character  /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getEntityIdValue C-Win 
FUNCTION getEntityIdValue RETURNS CHARACTER
  ( cEntity as character /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON bGet  NO-FOCUS
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get Data".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON bSearch 
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search Data".

DEFINE VARIABLE cbAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 41.4 BY 1 NO-UNDO.

DEFINE VARIABLE cbEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL",
                     "Agent","A",
                     "Employee","E",
                     "Person","P"
     DROP-DOWN-LIST
     SIZE 15.4 BY 1 TOOLTIP "Entity" NO-UNDO.

DEFINE VARIABLE cbEntityId AS CHARACTER 
     LABEL "" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN AUTO-COMPLETION
     SIZE 41.4 BY 1 NO-UNDO.

DEFINE VARIABLE cbPerson AS CHARACTER FORMAT "X(256)":U 
     LABEL "" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 41.4 BY 1 TOOLTIP "Entity ID" NO-UNDO.

DEFINE VARIABLE cbUID AS CHARACTER 
     LABEL "UID" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN
     SIZE 33.2 BY 1 TOOLTIP "User ID" NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 27.6 BY 1 TOOLTIP "Search Criteria (A=Agent;P=Person;E=Employee;UID,Entity ID,Entity Name)" NO-UNDO.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17.4 BY 2.14.

DEFINE RECTANGLE RECT-63
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 37.8 BY 2.14.

DEFINE RECTANGLE RECT-84
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 126.6 BY 2.14.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwsysfavorite FOR 
      ttsysfavorite SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwsysfavorite
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwsysfavorite C-Win _FREEFORM
  QUERY brwsysfavorite DISPLAY
      ttsysfavorite.uid           label "UID"                 format "x(40)"   
ttsysfavorite.entity        label "Entity"              format "x(20)"
if (ttsysfavorite.entityID eq '""' or  ttsysfavorite.entityID eq "") then "" else ttsysfavorite.entityID      label "Entity ID"           format "x(20)"
ttsysfavorite.entityName    label "Entity Name "        format "x(80)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SEPARATORS NO-TAB-STOP SIZE 181 BY 18.1
         BGCOLOR 15  ROW-HEIGHT-CHARS .71 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 1.57 COL 3 WIDGET-ID 344 NO-TAB-STOP 
     bSearch AT ROW 1.57 COL 174.6 WIDGET-ID 314 NO-TAB-STOP 
     cbUID AT ROW 1.95 COL 22.8 COLON-ALIGNED WIDGET-ID 332
     cbEntity AT ROW 1.95 COL 64.2 COLON-ALIGNED WIDGET-ID 346
     bRefresh AT ROW 1.57 COL 10.8 WIDGET-ID 158 NO-TAB-STOP 
     cbPerson AT ROW 1.95 COL 92.8 COLON-ALIGNED WIDGET-ID 354
     cbAgent AT ROW 1.95 COL 92.8 COLON-ALIGNED WIDGET-ID 352
     cbEntityId AT ROW 1.95 COL 92.8 COLON-ALIGNED WIDGET-ID 350
     fSearch AT ROW 1.95 COL 144.2 COLON-ALIGNED NO-LABEL WIDGET-ID 312
     bGet AT ROW 1.57 COL 137 WIDGET-ID 336 NO-TAB-STOP 
     brwsysfavorite AT ROW 3.76 COL 2 WIDGET-ID 500
     "Search" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.05 COL 146 WIDGET-ID 316
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1 COL 3.2 WIDGET-ID 52
     "Parameters" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 1.05 COL 19.8 WIDGET-ID 338
     RECT-62 AT ROW 1.38 COL 2 WIDGET-ID 308
     RECT-63 AT ROW 1.38 COL 145.2 WIDGET-ID 310
     RECT-84 AT ROW 1.38 COL 18.8 WIDGET-ID 330
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 239.2 BY 27.33
         DEFAULT-BUTTON bGet WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Favorites"
         HEIGHT             = 21.1
         WIDTH              = 183
         MAX-HEIGHT         = 32.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwsysfavorite bGet fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwsysfavorite:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwsysfavorite:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON bSearch IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbAgent IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbEntityId IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbPerson IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fSearch IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwsysfavorite
/* Query rebuild information for BROWSE brwsysfavorite
     _START_FREEFORM
open query {&SELF-NAME} for each ttsysfavorite.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwsysfavorite */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Favorites */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Favorites */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Favorites */
DO:
   run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGet C-Win
ON CHOOSE OF bGet IN FRAME fMain /* Get */
DO:
  run getData in this-procedure.
  fSearch:screen-value = cLastSearchString.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
do:
  run getData in this-procedure.
  fSearch:screen-value = cLastSearchString.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwsysfavorite
&Scoped-define SELF-NAME brwsysfavorite
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwsysfavorite C-Win
ON ROW-DISPLAY OF brwsysfavorite IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwsysfavorite C-Win
ON START-SEARCH OF brwsysfavorite IN FRAME fMain
do:
  {lib/brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME fMain /* Search */
DO:
  /* if search button is clicked or return key is hit, 
     then 'cLastSearchString' stores the last string searched until user again hits the search */
  cLastSearchString  = fSearch:input-value.
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgent C-Win
ON VALUE-CHANGED OF cbAgent IN FRAME fMain
DO:
   resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbEntity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbEntity C-Win
ON VALUE-CHANGED OF cbEntity IN FRAME fMain /* Entity */
DO:
  resultsChanged(false).
  run actionValueChanged in this-procedure.
  run setEntityId     in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbEntityId
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbEntityId C-Win
ON VALUE-CHANGED OF cbEntityId IN FRAME fMain
DO:
   resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbPerson
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbPerson C-Win
ON VALUE-CHANGED OF cbPerson IN FRAME fMain
DO:
   resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbUID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbUID C-Win
ON VALUE-CHANGED OF cbUID IN FRAME fMain /* UID */
DO:
  resultsChanged(false).   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fMain
DO:
  apply 'choose':U to bSearch.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON VALUE-CHANGED OF fSearch IN FRAME fMain
DO:
   resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i }

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
assign current-window                = {&window-name} 
       this-procedure:current-window = {&window-name}.
       

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
on close of this-procedure 
  run disable_UI.

/* Best default for GUI applications is...                              */
pause 0 before-hide.
subscribe to "closeWindow" anywhere.

bExport    :load-image            ("images/excel.bmp").
bExport    :load-image-insensitive("images/excel-i.bmp").
bRefresh   :load-image            ("images/refresh.bmp").
bRefresh   :load-image-insensitive("images/refresh-i.bmp").
bGet       :load-image            ("images/completed.bmp").
bGet       :load-image-insensitive("images/completed-i.bmp").
bSearch    :load-image            ("images/magnifier.bmp").
bSearch    :load-image-insensitive("images/magnifier-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.

   /* create the agent combo */
  {lib/get-agent-list.i &combo=cbAgent &addAll=true &setEnable=std-lo}
  {lib/get-column-width.i &col="'UID'"  &var=dColumnWidth}
  /* create the person combo */
  {lib/get-person-list.i &combo=cbPerson &addAll=true &setEnable=std-lo}

  /* get-agent-list.i and get-person-list.i defines the "on entry anywhere" trigger, the later 
  definition overrides the initial definition. So we have to redefine the "on entry anywhere" trigger
  including code for all widgets of agent and person lists and fill-ins.  */      
  on entry anywhere
  do:
    hSelectionAgent    = GetWidgetByName(cbAgent   :frame in frame {&frame-name},  "cbAgentAgentSelection").
    hTextAgent         = GetWidgetByName(cbAgent   :frame in frame {&frame-name},  "cbAgentAgentText").
    hSelectionPerson   = GetWidgetByName(cbPerson  :frame in frame {&frame-name}, "cbPersonPersonSelection").
    hTextPerson        = GetWidgetByName(cbPerson  :frame in frame {&frame-name}, "cbPersonPersonText").
    
    if valid-handle(hSelectionAgent) and valid-handle(hTextAgent)
     then
      if not self:name = "cbAgentAgentText" and not self:name = "cbAgentAgentSelection"
       then hSelectionAgent:visible = false.
      else if hSelectionAgent:list-item-pairs > "" and not hSelectionAgent:visible
       then hSelectionAgent:visible = true.
      
    if valid-handle(hSelectionPerson) and valid-handle(hTextPerson)
     then
      if not self:name = "cbPersonPersonText" and not self:name = "cbPersonPersonSelection"
       then hSelectionPerson:visible = false.
      else if hSelectionPerson:list-item-pairs > "" and not hSelectionPerson:visible
       then hSelectionPerson:visible = true.
      
  end.

  /* Set the default values. */
  cbAgent:screen-value = "".
  cbEntity:screen-value = cbEntity:entry(1).
  
  /* Populate combo-box cbUID with sysuser */  
  run getSysUserList in this-procedure.
  run actionValueChanged   in this-procedure.
  
  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionValueChanged C-Win 
PROCEDURE actionValueChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  cEntity = cbEntity:input-value.
  /* Populating the combo box here for what the new requirement going to be made
     its for Agent, Person, User */
  /* Populating for All */
  if cEntity = {&All}
   then
    do:
      assign
          cbEntityId:visible   = true
          cbEntityId:sensitive = false
          cbEntityId:label     = "Entity ID"
          cbAgent:label        = ""      
          cbPerson:label       = ""
          .
     
      run agentComboHide(true).
      run agentComboEnable(false).
      run PersonComboHide(true).
      run PersonComboEnable(false).
      
      run PersonComboClear in this-procedure.
      run AgentComboClear in this-procedure.
    end.

  /* Populating for Agent */
  else if cEntity = {&Agent}
   then
    do:
      assign
          cbEntityId:visible   = false                      
          cbEntityId:sensitive = false
          cbEntityId:label     = ""
          cbAgent:label        = "Agent ID"    
          cbPerson:label       = ""
          .

      run AgentComboHide(false).
      run AgentComboEnable(true).
      run PersonComboHide(true).
      run PersonComboEnable(false).
      
      run PersonComboClear in this-procedure.
    end.
    
   /* Populating for Person */
   else if cEntity = {&Person}
   then
    do:
     assign
         cbEntityId:visible   = false                      
         cbEntityId:sensitive = false
         cbEntityId:label     = ""
         cbAgent:label        = ""    
         cbPerson:label       = "Person ID"
           .
     
     run PersonComboHide(false).
     run PersonComboEnable(true).
     run AgentComboHide(true).
     run AgentComboEnable(false).
     
     run AgentComboClear in this-procedure.
    end.
   /* Populating for Employee */
   else if cEntity = {&Employee}
   then
    do:
      assign
          cbEntityId:visible    = true                      
          cbEntityId:sensitive  = true
          cbEntityId:label      = "Emp ID"
          cbAgent:label         =  ""    
          cbPerson:label        = ""
          .
         
      run agentComboHide(true).
      run agentComboEnable(false).
      run PersonComboHide(true).
      run PersonComboEnable(false).
      
      cbEntityId:move-to-top().
      
      run AgentComboClear in this-procedure.
      run PersonComboClear in this-procedure.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 APPLY "CLOSE":U TO THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbUID cbEntity cbPerson cbAgent cbEntityId fSearch 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-62 RECT-63 RECT-84 cbUID cbEntity bRefresh bGet brwsysfavorite 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable htable as handle no-undo.

  if query brwsysfavorite:num-results = 0 
   then
    do: 
      message "There is nothing to export"
         view-as alert-box warning buttons ok.
      return.
    end.
  
  publish "GetReportDir" (output std-ch).

  htable = temp-table ttsysfavorite:handle.

  run util/exporttable.p (table-handle htable,
                          "ttsysfavorite",
                          "for each ttsysfavorite",
                          "sysFavoriteID,uid,entity,entityid,entityName",
                          "Sys Favorite ID,UID,Entity,Entity Id,Entity Name",
                          std-ch,
                          "SysFavorite-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in). 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwsysfavorite.
  empty temp-table ttsysfavorite.
  
  define variable cEntityName as character no-undo.
  
  do with frame {&frame-name}: 
  end.
  
  if cLastSearchString = {&Agent} or cLastSearchString = {&Person} or cLastSearchString = {&Employee}
   then
    do:
      for each sysfavorite no-lock where sysfavorite.entity = cLastSearchString:
        publish "getEntityName" (input sysfavorite.entity,
                                 input sysfavorite.entityId,
                                 output cEntityName).                        
        sysfavorite.entityName   = cEntityName.
      
        create ttsysfavorite.
        buffer-copy sysfavorite except entityName to ttsysfavorite.
      
        assign
            ttsysfavorite.entity       = getEntity(sysfavorite.entity)
             ttsysfavorite.entityName   = sysfavorite.entityName
             .
    end.
  end.

  else 
   for each sysfavorite:
    publish "getEntityName" (input sysfavorite.entity,
                             input sysfavorite.entityId,
                             output cEntityName).
                             
    sysfavorite.entityName   = cEntityName.
    /* test if the record contains the search text */
    if cLastSearchString <> "" and
      not ((sysfavorite.UID               matches "*" + cLastSearchString + "*") or
           (sysfavorite.entityID          matches "*" + cLastSearchString + "*") or
           (sysfavorite.entityName        matches "*" + cLastSearchString + "*"))
      then
       next.

    create ttsysfavorite.
    buffer-copy sysfavorite except entityName to ttsysfavorite.

    assign
        ttsysfavorite.entity       = getEntity(sysfavorite.entity)
        ttsysfavorite.entityName   = sysfavorite.entityName
        .
  end.
 
  open query brwsysfavorite preselect each ttsysfavorite.
  setStatusCount(query brwsysfavorite:num-results).
  run setwidgetstate in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getAllEntity C-Win 
PROCEDURE getAllEntity :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  assign                                  
      cbEntityId:visible           = false
      cbEntityId:sensitive         = false
      .          
  run agentComboHide(true).
  run agentComboEnable(false).
  run PersonComboHide(true).
  run PersonComboEnable(false).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  empty temp-table sysfavorite.
   
  /* server call to get the sysfavorite records */
  run server/getsysfavorite.p  (input cbUID:input-value,
                                input cbEntity:input-value,
                                input getEntityIdValue(cbEntity:input-value),
                                output table sysfavorite,
                                output       std-lo,
                                output       std-ch).

  if not std-lo
   then
    do:
      message std-ch
        view-as alert-box info buttons ok.
    end.

  run filterdata in this-procedure.
  setStatusRecords(query brwsysfavorite:num-results).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getSysUserList C-Win 
PROCEDURE getSysUserList :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define variable cUserList  as character  no-undo.

  /* return list of sys user*/
  publish "getSysUserList" (",",
                            output cUserList,
                            output std-lo,
                            output std-ch ).
  if not std-lo
   then
    do:
      message std-ch
           view-as alert-box error buttons ok.
      return.
    end.
 
  assign
      cbUID:list-items        = "ALL" + "," + trim(cUserList,",")
      cbUID:screen-value      = "ALL"
      cbEntityId:list-items   = "ALL" + "," + trim(cUserList,",")
      cbEntityId:screen-value = "ALL"
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setEntityId C-Win 
PROCEDURE setEntityId :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.  
  
  if cbEntity:input-value <> ""
   then
    assign
        hTextAgent:screen-value  = ""
        hTextPerson:screen-value = ""
        cbEntityId:screen-value  = ""
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if query brwsysfavorite:num-results > 0
   then
    assign 
        bExport:sensitive   = true
        fSearch:sensitive   = true 
        bSearch:sensitive   = true
        .
  else
   bExport:sensitive    = false.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized then
     {&window-name}:window-state = window-normal .
  
  c-Win:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  
  assign frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
         frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
         frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
         frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
         {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 8.8
         {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 3 .

  {lib/resize-column.i &col="'UID,entityID,note'" &var=dColumnWidth}       
         
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getEntity C-Win 
FUNCTION getEntity RETURNS CHARACTER
  ( cEntity as character  /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 define variable rcEntity as character  no-undo.

 if cEntity = "A"
  then 
   do:
     rcEntity = "Agent" .
     return rcEntity.  /* Function return value. */
   end.
 else if cEntity = "E"
  then
   do:
     rcEntity = "Employee" .
     return rcEntity. /* Function return value. */
   end.
 else if cEntity = "P"
  then 
   do:
     rcEntity = "Person" .
     return rcEntity. /* Function return value. */
   end.
 else if cEntity <> "" or cEntity = "" 
  then
   do:
     cEntity = "". 
     return cEntity .                /* Function return value. */
   end.


END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getEntityIdValue C-Win 
FUNCTION getEntityIdValue RETURNS CHARACTER
  ( cEntity as character /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
 
 if cEntity = {&Person}
  then
   do:
     if cbPerson:input-value = ""
      then 
       return hTextPerson:input-value. /* Function return value. */
     else
      return cbPerson:input-value.
   end.   
      
 else if cEntity = {&Agent}
  then
   do:
     if cbAgent:input-value = ""
      then
       return hTextAgent:input-value. /* Function return value. */
     else
      return cbAgent:input-value.       
  end.
       
 else if cEntity = {&Employee}
  then
   return cbEntityId:input-value.   /* Function return value. */
  
 else if cEntity = ""
  then
   return cEntity.                  /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage("Results may not match current parameters.").
  return true.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

