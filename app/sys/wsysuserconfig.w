&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-WinConfig
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-WinConfig 
/*------------------------------------------------------------------------

  File: wsysuserconfig.w 

  Description:User Interface for system config records. 

  Input Parameters:

  Output Parameters:

  Author: Rahul Sharma

  Created:07.02.19 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.
{lib/sys-def.i}
{lib/std-def.i}
{lib/get-column.i}
{lib/winshowscrollbars.i}
/* ***************************  Definitions  ************************** */
{tt/sysuserconfig.i}
{tt/sysuserconfig.i &tableAlias =ttsysuserconfig}
{tt/sysuserconfig.i &tableAlias =tsysuserconfig}

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
define variable cLastSearchString  as character no-undo.
define variable lApplySearchString as logical   no-undo.
define variable dColumnWidth   as decimal   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwsysuserconfig

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttsysuserconfig

/* Definitions for BROWSE brwsysuserconfig                              */
&Scoped-define FIELDS-IN-QUERY-brwsysuserconfig ttsysuserconfig.UID ttsysuserconfig.username ttsysuserconfig.configType ttsysuserconfig.configuration   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwsysuserconfig   
&Scoped-define SELF-NAME brwsysuserconfig
&Scoped-define QUERY-STRING-brwsysuserconfig for each ttsysuserconfig
&Scoped-define OPEN-QUERY-brwsysuserconfig open query {&SELF-NAME} for each ttsysuserconfig.
&Scoped-define TABLES-IN-QUERY-brwsysuserconfig ttsysuserconfig
&Scoped-define FIRST-TABLE-IN-QUERY-brwsysuserconfig ttsysuserconfig


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwsysuserconfig}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbUID fSearch brwsysuserconfig bRefresh ~
RECT-62 RECT-63 RECT-84 
&Scoped-Define DISPLAYED-OBJECTS cbUID fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-WinConfig 
FUNCTION resultsChanged RETURNS LOGICAL
  ( )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-WinConfig AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON bSearch 
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search User Configuration".

DEFINE BUTTON bView  NO-FOCUS
     LABEL "View" 
     SIZE 7.2 BY 1.71 TOOLTIP "View Entry".

DEFINE VARIABLE cbUID AS CHARACTER 
     LABEL "UID" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN
     SIZE 39.8 BY 1 TOOLTIP "User ID" NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 36.2 BY 1 TOOLTIP "Search Criteria (UID, Username,ConfigurationType,Configuration)" NO-UNDO.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 24.2 BY 2.14.

DEFINE RECTANGLE RECT-63
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 45.8 BY 2.14.

DEFINE RECTANGLE RECT-84
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 47 BY 2.14.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwsysuserconfig FOR 
      ttsysuserconfig SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwsysuserconfig
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwsysuserconfig C-WinConfig _FREEFORM
  QUERY brwsysuserconfig DISPLAY
      ttsysuserconfig.UID              label "UID"                      format "x(40)"
ttsysuserconfig.username         label "User Name "               format "x(30)"
ttsysuserconfig.configType       label "Configuration Type"       format "x(30)"      
ttsysuserconfig.configuration    label "Configuration"            format "x(80)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SEPARATORS NO-TAB-STOP SIZE 149 BY 18.1
         BGCOLOR 15  ROW-HEIGHT-CHARS .71 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bView AT ROW 1.57 COL 18.2 WIDGET-ID 318 NO-TAB-STOP 
     cbUID AT ROW 2 COL 30 COLON-ALIGNED WIDGET-ID 332
     bExport AT ROW 1.57 COL 10.6 WIDGET-ID 2 NO-TAB-STOP 
     fSearch AT ROW 2 COL 71.6 COLON-ALIGNED NO-LABEL WIDGET-ID 312
     bSearch AT ROW 1.57 COL 110.4 WIDGET-ID 314 NO-TAB-STOP 
     brwsysuserconfig AT ROW 3.81 COL 2 WIDGET-ID 500
     bRefresh AT ROW 1.57 COL 3 WIDGET-ID 158 NO-TAB-STOP 
     "Filters" VIEW-AS TEXT
          SIZE 5.8 BY .62 AT ROW 1.05 COL 26.8 WIDGET-ID 338
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1 COL 3 WIDGET-ID 52
     "Search" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.05 COL 73.4 WIDGET-ID 316
     RECT-62 AT ROW 1.38 COL 2.2 WIDGET-ID 308
     RECT-63 AT ROW 1.38 COL 72.8 WIDGET-ID 310
     RECT-84 AT ROW 1.38 COL 26 WIDGET-ID 330
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 265.6 BY 31.71 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-WinConfig ASSIGN
         HIDDEN             = YES
         TITLE              = "User Configurations"
         HEIGHT             = 21.19
         WIDTH              = 151.2
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwsysuserconfig bSearch fMain */
/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwsysuserconfig:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwsysuserconfig:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

/* SETTINGS FOR BUTTON bSearch IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bView IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-WinConfig)
THEN C-WinConfig:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwsysuserconfig
/* Query rebuild information for BROWSE brwsysuserconfig
     _START_FREEFORM
open query {&SELF-NAME} for each ttsysuserconfig.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwsysuserconfig */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-WinConfig
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-WinConfig C-WinConfig
ON END-ERROR OF C-WinConfig /* User Configurations */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  if this-procedure:persistent then return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-WinConfig C-WinConfig
ON WINDOW-CLOSE OF C-WinConfig /* User Configurations */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-WinConfig C-WinConfig
ON WINDOW-RESIZED OF C-WinConfig /* User Configurations */
DO:
   run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-WinConfig
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-WinConfig
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
do:
  run refreshData in this-procedure.
  fSearch:screen-value = cLastSearchString.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwsysuserconfig
&Scoped-define SELF-NAME brwsysuserconfig
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwsysuserconfig C-WinConfig
ON DEFAULT-ACTION OF brwsysuserconfig IN FRAME fMain
DO:
   run showSysUserConfig in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwsysuserconfig C-WinConfig
ON ROW-DISPLAY OF brwsysuserconfig IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwsysuserconfig C-WinConfig
ON START-SEARCH OF brwsysuserconfig IN FRAME fMain
do:
  {lib/brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-WinConfig
ON CHOOSE OF bSearch IN FRAME fMain /* Search */
DO:
  /* if search button is clicked or return key is hit, 
     then 'cLastSearchString' stores the last string searched until user again hits the search */
  assign
      lApplySearchString = true  
      cLastSearchString  = fSearch:input-value
      .
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bView C-WinConfig
ON CHOOSE OF bView IN FRAME fMain /* View */
do:
  run showSysUserConfig in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbUID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbUID C-WinConfig
ON VALUE-CHANGED OF cbUID IN FRAME fMain /* UID */
DO:
   run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-WinConfig
ON RETURN OF fSearch IN FRAME fMain
DO:
  apply "CHOOSE" to bSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-WinConfig
ON VALUE-CHANGED OF fSearch IN FRAME fMain
DO:
  resultsChanged().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-WinConfig 


/* ***************************  Main Block  *************************** */

{lib/brw-main.i}


{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
assign current-window                = {&window-name} 
       this-procedure:current-window = {&window-name} .

       
{lib/win-main.i}
{lib/win-status.i}       

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
on close of this-procedure 
  run disable_UI.

/* Best default for GUI applications is...                              */
pause 0 before-hide.


bExport    :load-image            ("images/excel.bmp").
bExport    :load-image-insensitive("images/excel-i.bmp").
bRefresh   :load-image            ("images/refresh.bmp").
bRefresh   :load-image-insensitive("images/refresh-i.bmp").
bView      :load-image            ("images/open.bmp").
bView      :load-image-insensitive("images/open-i.bmp").
bSearch    :load-image            ("images/magnifier.bmp").
bSearch    :load-image-insensitive("images/magnifier-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.

  {lib/get-column-width.i &col="'UID'"  &var=dColumnWidth}
  
  run getSysUserList in this-procedure.
   
  apply "value-changed" to cbUID.
  
  cbUID:screen-value      = "ALL".
  
  run showWindow in this-procedure.

  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-WinConfig 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 APPLY "CLOSE":U TO THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-WinConfig  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-WinConfig)
  THEN DELETE WIDGET C-WinConfig.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-WinConfig  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbUID fSearch 
      WITH FRAME fMain IN WINDOW C-WinConfig.
  ENABLE cbUID fSearch brwsysuserconfig bRefresh RECT-62 RECT-63 RECT-84 
      WITH FRAME fMain IN WINDOW C-WinConfig.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-WinConfig.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-WinConfig 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable htable as handle no-undo.

  if query brwsysuserconfig:num-results = 0 
   then
    do: 
      message "There is nothing to export"
         view-as alert-box warning buttons ok.
      return.
    end.
  
  publish "GetReportDir" (output std-ch).

  htable = temp-table ttsysuserconfig:handle.

  run util/exporttable.p (table-handle htable,
                          "ttsysuserconfig",
                          "for each ttsysuserconfig",
                          "UID,username,configType,configuration",
                          "UID,User Name,Configuration Type,Configuration",
                          std-ch,
                          "SysUserConfiguration-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in). 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-WinConfig 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwsysuserconfig.
  empty temp-table ttsysuserconfig.

  do with frame {&frame-name}: 
  end.
 
  /* if search string is already applied then filter on the basis of what is
     present inside fsearch fill-in */

  if lApplySearchString 
   then  
    for each sysuserconfig where sysuserconfig.UID = (if cbUID:input-value = "ALL" then sysuserconfig.UID else cbUID:input-value)
                                                and ((if cLastSearchString <> "" then sysuserconfig.UID matches ("*" + cLastSearchString + "*")
                                                      else sysuserconfig.UID = sysuserconfig.UID) or
                                                     (if cLastSearchString <> "" then sysuserconfig.username matches ("*" + cLastSearchString + "*")
                                                      else sysuserconfig.username = sysuserconfig.username) or
                                                     (if cLastSearchString <> "" then sysuserconfig.configType matches ("*" + cLastSearchString + "*")
                                                      else sysuserconfig.configType = sysuserconfig.configType) or
                                                     (if cLastSearchString <> "" then sysuserconfig.configuration matches ("*" + cLastSearchString + "*")
                                                      else sysuserconfig.configuration = sysuserconfig.configuration)
                                                    ) by sysuserconfig.UID:
    
      create ttsysuserconfig. 
      buffer-copy sysuserconfig to ttsysuserconfig.
    end.
   else
    for each sysuserconfig where sysuserconfig.UID = (if cbUID:input-value = "ALL" then sysuserconfig.UID else cbUID:input-value):
      create ttsysuserconfig. 
      buffer-copy sysuserconfig to ttsysuserconfig.
    end.
    
  open query brwsysuserconfig preselect each ttsysuserconfig.
  setStatusCount(query brwsysuserconfig:num-results).
  run setwidgetstate in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-WinConfig 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cUID as character no-undo.
  
  do with frame {&frame-name}:
  end.
  
  empty temp-table sysuserconfig.

  publish "GetSysUserConfig" (output table sysuserconfig, 
                              output std-lo,
                              output std-ch)
                              .
                              
  if not std-lo /* fails return */
   then
    do:
      message std-ch
            view-as alert-box info buttons ok.
      return.
    end.
    
  run filterdata in this-procedure.
  
  publish "GetCurrentValue" ("RefreshUserConfig", output std-lo).
  
  if std-lo 
   then       
    setStatusRecords(query brwsysuserconfig:num-results).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getSysUserList C-WinConfig 
PROCEDURE getSysUserList :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define variable cUserList  as character  no-undo.

  /* return list of sys user*/
  publish "getSysUserList" (",",
                            output cUserList,
                            output std-lo,
                            output std-ch ).
  if not std-lo
   then
    do:
      message std-ch
           view-as alert-box error buttons ok.
      return.
    end.
 
  cbUID:list-items        = "ALL" + "," + trim(cUserList,",").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData C-WinConfig 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* Refreshing the screen  */
  publish "refreshSysUserConfig" (output std-lo,
                                  output std-ch)
                                  .

  if not std-lo /* not success return */
   then 
    do:
      message std-ch
          view-as alert-box info buttons ok.
    end.
  
  run getData in this-procedure.

  setStatusRecords(query brwsysuserconfig:num-results).   
  lApplySearchString = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-WinConfig 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if query brwsysuserconfig:num-results > 0
   then
    assign 
        bExport:sensitive = true
        bView:sensitive   = true
        bSearch:sensitive = true
        fSearch:sensitive = true
        .
  else
   assign
       bExport:sensitive = false
       bView:sensitive   = false
       .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showSysUserConfig C-WinConfig 
PROCEDURE showSysUserConfig :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table tsysuserconfig.
  
  if available ttsysuserconfig
   then
    do:
      create tsysuserconfig.
      buffer-copy ttsysuserconfig to tsysuserconfig.
      run dialogsysuserconfig.w (input table tsysuserconfig).
      
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-WinConfig 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized then
     {&window-name}:window-state = window-normal .
  
   {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-WinConfig 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-WinConfig 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
         frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
         frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
         frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
         {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 10
         {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y .

  {lib/resize-column.i &col="'UID,configType,configuration,username'" &var=dColumnWidth}
         
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-WinConfig 
FUNCTION resultsChanged RETURNS LOGICAL
  ( ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatchSearchString}).
/*   return true. */
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

