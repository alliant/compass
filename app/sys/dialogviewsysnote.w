&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS dialog-Frame 
/*------------------------------------------------------------------------

  File:dialogviewsysnote.w

  Description: Dialog to view a sysnote record.

  Input Parameters:
      Input Temp-table : sysnote
  Author:Rahul Sharma 

  Created:07.10.2019 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
  {lib/sys-def.i}
  
/* Temp-table definitions */
  {tt/sysnote.i}
  
/* Parameters Definitions  */
  define input  parameter table       for sysnote.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fUID fcdate fmdate fentity fEntityId edNotes 
&Scoped-Define DISPLAYED-OBJECTS fUID fcdate fmdate fentity fEntityId ~
edNotes 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE VARIABLE edNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 52.4 BY 5.05 NO-UNDO.

DEFINE VARIABLE fcdate AS DATE FORMAT "99/99/99":U 
     LABEL "Created" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE fentity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fEntityId AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity Name" 
     VIEW-AS FILL-IN 
     SIZE 52.4 BY 1 NO-UNDO.

DEFINE VARIABLE fmdate AS DATE FORMAT "99/99/99":U 
     LABEL " Modified" 
     VIEW-AS FILL-IN 
     SIZE 20.4 BY 1 NO-UNDO.

DEFINE VARIABLE fUID AS CHARACTER FORMAT "X(256)":U 
     LABEL "UID" 
     VIEW-AS FILL-IN 
     SIZE 36 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME dialog-Frame
     fUID AT ROW 1.43 COL 11.8 COLON-ALIGNED WIDGET-ID 332 NO-TAB-STOP 
     fcdate AT ROW 5.14 COL 11.8 COLON-ALIGNED WIDGET-ID 10 NO-TAB-STOP 
     fmdate AT ROW 5.14 COL 43.8 COLON-ALIGNED WIDGET-ID 14 NO-TAB-STOP 
     fentity AT ROW 2.67 COL 11.8 COLON-ALIGNED WIDGET-ID 334 NO-TAB-STOP 
     fEntityId AT ROW 3.91 COL 1.4 WIDGET-ID 336 NO-TAB-STOP 
     edNotes AT ROW 6.43 COL 13.8 NO-LABEL WIDGET-ID 12 NO-TAB-STOP 
     "Notes:" VIEW-AS TEXT
          SIZE 7 BY 1 AT ROW 6.33 COL 6.6 WIDGET-ID 342
     SPACE(53.19) SKIP(4.47)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "View Note" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME dialog-Frame:SCROLLABLE       = FALSE
       FRAME dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       edNotes:READ-ONLY IN FRAME dialog-Frame        = TRUE.

ASSIGN 
       fcdate:READ-ONLY IN FRAME dialog-Frame        = TRUE.

ASSIGN 
       fentity:READ-ONLY IN FRAME dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fEntityId IN FRAME dialog-Frame
   ALIGN-L                                                              */
ASSIGN 
       fEntityId:READ-ONLY IN FRAME dialog-Frame        = TRUE.

ASSIGN 
       fmdate:READ-ONLY IN FRAME dialog-Frame        = TRUE.

ASSIGN 
       fUID:READ-ONLY IN FRAME dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dialog-Frame dialog-Frame
ON WINDOW-CLOSE OF FRAME dialog-Frame /* View Note */
do:
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? THEN
  frame {&frame-name}:parent = active-window.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.
  
  /* Set default values to the widgets */
  run setData in this-procedure.
    
  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fUID fcdate fmdate fentity fEntityId edNotes 
      WITH FRAME dialog-Frame.
  ENABLE fUID fcdate fmdate fentity fEntityId edNotes 
      WITH FRAME dialog-Frame.
  VIEW FRAME dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData dialog-Frame 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  define variable cEntityName as character no-undo.

  /* Filling data in widgets */
  find first sysnote no-error.
  if available sysnote
   then 
    do:
      assign 
         fUID:screen-value          = sysnote.UID    
         fEntityId:screen-value     = sysnote.entityId
         fmdate:screen-value        = string(sysnote.DateModified )     
         fcdate:screen-value        = string(sysnote.DateCreate )
         edNotes:screen-value       = sysnote.notes 
         .
         if sysnote.entity = {&Agent} 
          then
           fEntity:screen-value   = "Agent".
               
          else if sysnote.entity = {&Attorney} 
           then
            fEntity:screen-value   = "Attorney". 
           
          else if sysnote.entity = {&Employee} 
           then
            fEntity:screen-value   = "Employee".  
           
          else if sysnote.entity = {&Person} 
           then
            fEntity:screen-value   = "Person".           
            
   
         publish "getEntityName" (input sysnote.entity,
                                  input sysnote.entityId,
                                  output cEntityName).
         assign                         
             fEntityId:screen-value = cEntityName + "(" + sysnote.entityId + ")"
             fEntityId:tooltip      = cEntityName + "(" + sysnote.entityId + ")"
             .
                                       
   end.     

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

