&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wsysaction.w

  Description: Window of system actions

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gurvindar

  Created: 09.25.2018

------------------------------------------------------------------------*/

create widget-pool.

{lib/winshowscrollbars.i}
{lib/std-def.i}
{lib/sys-def.i}

/* temp table definitions */
{tt/sysaction.i}

define temp-table ttsysaction  like sysaction.
define temp-table tsysaction   like sysaction.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttsysaction

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData ttsysaction.isActive ttsysaction.isSecure "Secure" ttsysaction.isLog "Log" ttsysaction.action "Action" ttsysaction.progExec "Program Execution" ttsysaction.description "Description"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each ttsysaction
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each ttsysaction.
&Scoped-define TABLES-IN-QUERY-brwData ttsysaction
&Scoped-define FIRST-TABLE-IN-QUERY-brwData ttsysaction


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-61 RECT-62 fSearch bSearch brwData ~
bcopy bExport bNew bRefresh bdelete bEdit 
&Scoped-Define DISPLAYED-OBJECTS fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bcopy  NO-FOCUS
     LABEL "Copy" 
     SIZE 7.2 BY 1.71 TOOLTIP "Copy".

DEFINE BUTTON bdelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete".

DEFINE BUTTON bEdit  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Edit".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "New".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search".

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 46.8 BY 1 TOOLTIP "Search Criteria (Action, Program Execution, Description)" NO-UNDO.

DEFINE RECTANGLE RECT-61
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 46 BY 2.14.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 56.8 BY 2.14.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      ttsysaction SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      ttsysaction.isActive  column-label  "Active"      view-as toggle-box       
ttsysaction.isSecure column-label    "Secure"      view-as toggle-box        
ttsysaction.isLog column-label     "Log"      view-as toggle-box                                                  
 ttsysaction.action label        "Action"        format "x(35)"                
ttsysaction.progExec label      "Program Execution"   width 30      format "x(45)"
ttsysaction.description label    "Description"   width 40     format "x(70)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 159 BY 18.1 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     fSearch AT ROW 2 COL 47 COLON-ALIGNED NO-LABEL WIDGET-ID 310
     bSearch AT ROW 1.62 COL 96.4 WIDGET-ID 308 NO-TAB-STOP 
     brwData AT ROW 4.1 COL 2 WIDGET-ID 200
     bcopy AT ROW 1.62 COL 40 WIDGET-ID 316 NO-TAB-STOP 
     bExport AT ROW 1.62 COL 3 WIDGET-ID 2 NO-TAB-STOP 
     bNew AT ROW 1.62 COL 17.8 WIDGET-ID 6 NO-TAB-STOP 
     bRefresh AT ROW 1.62 COL 10.4 WIDGET-ID 4 NO-TAB-STOP 
     bdelete AT ROW 1.62 COL 32.6 WIDGET-ID 10 NO-TAB-STOP 
     bEdit AT ROW 1.62 COL 25.2 WIDGET-ID 8 NO-TAB-STOP 
     "Search" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.1 COL 48.6 WIDGET-ID 314
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1 COL 2.8 WIDGET-ID 52
     RECT-61 AT ROW 1.43 COL 2.2 WIDGET-ID 50
     RECT-62 AT ROW 1.43 COL 47.8 WIDGET-ID 312
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 178.8 BY 21.52 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Actions"
         HEIGHT             = 21
         WIDTH              = 161.6
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData bSearch fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each ttsysaction.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Actions */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Actions */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Actions */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bcopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bcopy C-Win
ON CHOOSE OF bcopy IN FRAME fMain /* Copy */
do:
  run ActionCopy in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bdelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bdelete C-Win
ON CHOOSE OF bdelete IN FRAME fMain /* Delete */
do:
  run ActionDelete in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEdit C-Win
ON CHOOSE OF bEdit IN FRAME fMain /* Edit */
do:
  run ActionModify in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
do:
  run ActionNew in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
do:
  /* refresh the sysaction table in sysdatasrv with the databse then call 
     getData to update the browser with updated table sysaction */
  run refreshData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
do:
  run ActionModify in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME fMain /* Search */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fMain
DO:
  apply "CHOOSE" to bSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

assign current-window                = {&window-name} 
       this-procedure:current-window = {&window-name}.

on close of this-procedure 
  run disable_UI.

pause 0 before-hide.

subscribe to "closeWindow" anywhere. 

bExport :load-image("images/excel.bmp").
bExport :load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/sync.bmp").
bRefresh:load-image-insensitive("images/sync-i.bmp").
bnew    :load-image("images/add.bmp").
bnew    :load-image-insensitive("images/add-i.bmp").
bEdit   :load-image("images/update.bmp").
bEdit   :load-image-insensitive("images/update-i.bmp").
bDelete :load-image("images/delete.bmp").
bDelete :load-image-insensitive("images/delete-i.bmp").
bSearch :load-image-up("images/magnifier.bmp").
bSearch :load-image-insensitive("images/magnifier-i.bmp").
bcopy   :load-image-up("images/copy.bmp").
bcopy   :load-image-insensitive("images/copy-i.bmp").

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

  run enable_UI.

 /* Getting the data */
  run getData in this-procedure.

  /* maintaing window size back to normal and on top */
  run showWindow in this-procedure.

  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionCopy C-Win 
PROCEDURE ActionCopy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  do with frame {&frame-name}:
  end.

  define variable cAction  as character no-undo.
  define variable iCount   as integer   no-undo.

  define buffer tsysaction for tsysaction.
  empty temp-table tsysaction.
 
  if available ttsysaction 
   then
    do:
      create tsysaction.
      buffer-copy ttsysaction to tsysaction.
 
      run dialogsysaction.w (input {&copy},
                             input table tsysaction,
                             output cAction,
                             output std-lo).
    end.
 
  if not std-lo /* if fails or no call made */
   then
    return.
  
  run getData in this-procedure.
 
  do iCount = 1 to {&browse-name}:num-iterations:
   if {&browse-name}:is-row-selected(iCount)
    then
     leave.
  end.
 
  /* updating the temptable */
  find first ttsysaction where ttsysaction.action = cAction no-error.
  if available ttsysaction   
   then
    do:
      std-ro = rowid(ttsysaction).
      reposition {&browse-name} to rowid std-ro. 
      {&browse-name}:set-repositioned-row(iCount,"ALWAYS").
      setStatusCount(query brwData:num-results).
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDelete C-Win 
PROCEDURE ActionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available ttsysaction 
   then
    return.

  std-lo = false.
  

  message "Highlighted System Action record will be deleted." skip "Do you want to continue?"                                                                                                                        
   view-as alert-box question buttons yes-no                                                                                                                                                 
     title "Delete System Action"                                                                                                                                                                 
       update std-lo as logical.  
 
  if not std-lo 
   then
    return.
  
  publish "deleteSysAction" (ttsysaction.action,
                             output std-lo,
                             output std-ch).
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.

  run getdata in this-procedure.

  setStatusCount(query brwData:num-results).  

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionModify C-Win 
PROCEDURE ActionModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define variable cAction  as character no-undo.
  define variable iCount   as integer   no-undo.

  define buffer tsysaction for tsysaction.
  empty temp-table tsysaction.
 
  if available ttsysaction 
   then
    do:
      create tsysaction.
      buffer-copy ttsysaction to tsysaction.
   
      run dialogsysaction.w (input {&modify},
                             input table tsysaction,
                             output cAction,
                             output std-lo).
 
      if not std-lo
       then
         return.
 
      run getdata in this-procedure.

      do iCount = 1 to {&browse-name}:num-iterations:
        if {&browse-name}:is-row-selected(iCount)
         then
          leave.
      end.

      /* updating the temptable */
      find first ttsysaction where ttsysaction.action = cAction no-error.
      if available ttsysaction 
       then
        do: 
          std-ro = rowid(ttsysaction).
          reposition {&browse-name} to rowid std-ro.
          {&browse-name}:set-repositioned-row(iCount,"ALWAYS").
          setStatusCount(query brwData:num-results). 
        end.
    end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNew C-Win 
PROCEDURE ActionNew :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define variable cAction as character no-undo.
  define variable iCount   as integer   no-undo.
  
  define buffer tsysaction for tsysaction.
  empty temp-table tsysaction.
  
  run dialogsysaction.w (input {&new},
                         input table tsysaction,
                         output cAction,
                         output std-lo).
  
  if not std-lo
   then 
     return.
  
  run getdata in this-procedure.
  
  do iCount = 1 to {&browse-name}:num-iterations:
    if {&browse-name}:is-row-selected(iCount)
     then
      leave.
   end.
  
  /* updating the temptable */
  find first ttsysaction where ttsysaction.action = cAction no-error.
  if available ttsysaction   
   then
    do:
      std-ro = rowid(ttsysaction).
      {&browse-name}:set-repositioned-row(iCount,"ALWAYS").
      reposition {&browse-name} to rowid std-ro. 
    end.
   
  setStatusCount(query brwData:num-results).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 apply "close":u to this-procedure.
 return no-apply.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fSearch 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-61 RECT-62 fSearch bSearch brwData bcopy bExport bNew bRefresh 
         bdelete bEdit 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable htableHandle as handle no-undo.
                                                                                           
  
  if query brwData:num-results = 0 then
  do: 
    message "There is nothing to export"
        view-as alert-box warning buttons ok.
    return.
  end.
 
  publish "GetReportDir" (output std-ch).
 
  htableHandle = temp-table ttsysaction:handle.
  run util/exporttable.p (table-handle htableHandle,
                          "ttsysaction",
                          "for each ttsysaction ",
                          "isActive,isSecure,isLog,action,progexec,description",
                          "Active,Secure,Log,Action,Program Execution,Description",
                          std-ch,
                          "SysAction.csv-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 close query brwData.

 empty temp-table ttsysaction.
 
 do with frame {&frame-name}:
 end.

 for each sysaction:
   /* test if the record contains the search text */
   if fSearch:screen-value <> "" and 
     not ( (sysaction.action      matches "*" + fSearch:screen-value + "*")  or 
           (sysaction.progExec    matches "*" + fSearch:screen-value + "*")  or 
           (sysaction.description matches "*" + fSearch:screen-value + "*") )
     then next.
  
   create ttsysaction.
   buffer-copy sysaction to ttsysaction.
 end.
 
 open query brwData preselect each ttsysaction.
 setStatusCount(query brwData:num-results).   
 run setButtons in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table sysaction.
 
  publish "GetSysActions" ( output table sysaction,  
                            output std-lo,           
                            output std-ch).          

  if not std-lo /* fails return */
   then
    do:
      message std-ch
            view-as alert-box info buttons ok.
      return.
    end.
 
  run filterData in this-procedure.

  publish "GetCurrentValue" ("RefreshActions", output std-ch).

  if logical(std-ch) 
   then 
    setStatusRecords(query brwData:num-results).   
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData C-Win 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* Refreshing the screen  */
  publish "refreshSysActions" (input {&Both},
                               input "ALL", /*  all */ 
                               input  ?,     /* startdate */
                               input  ?,     /* enddate */                                                           
                               output std-lo,
                               output std-ch). 

  if not std-lo /* not sucess return */
   then 
    do:
      message std-ch
          view-as alert-box info buttons ok.
    end.
  
  run getData in this-procedure.

  setStatusRecords(query brwData:num-results).   

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setButtons C-Win 
PROCEDURE setButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if query brwData:num-results > 0 
   then
    assign 
      bExport    :sensitive  = true
      bEdit      :sensitive  = true
      bDelete    :sensitive  = true
      bcopy      :sensitive  = true
      .
   else
    assign
      bExport    :sensitive = false
      bEdit      :sensitive = false
      bDelete    :sensitive = false
      bcopy      :sensitive = false
      .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if {&window-name}:window-state eq window-minimized 
  then
   {&window-name}:window-state = window-normal .

 c-Win:move-to-top().
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign frame fMain:width-pixels          = {&window-name}:width-pixels
         frame fMain:virtual-width-pixels  = {&window-name}:width-pixels
         frame fMain:height-pixels         = {&window-name}:height-pixels
         frame fMain:virtual-height-pixels = {&window-name}:height-pixels
        /* fMain Components */
         brwData:width-pixels              = frame fmain:width-pixels - 8
         brwData:height-pixels             = frame fMain:height-pixels - 65.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

