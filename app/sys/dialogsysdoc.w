&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogsysdoc.w

  Description: Display system document 

  Input Parameters:
      sysdoc table

  Output Parameters:
      <none>

  Author: Gurvindar Singh

  Created:09/26/18
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{tt/sysdoc.i}

/* Parameters Definitions ---                                           */
define input parameter table for sysdoc.

/* Standard Library */
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fEntityType fEntityID fEntityseq fobjType ~
fobjattr fdocdate fUid 
&Scoped-Define DISPLAYED-OBJECTS fEntityType fEntityID fEntityseq fobjType ~
fobjattr fdocdate fUid 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE VARIABLE fdocdate AS DATE FORMAT "99/99/99":U 
     LABEL "Document Date" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fEntityID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity ID" 
     VIEW-AS FILL-IN 
     SIZE 38 BY 1 NO-UNDO.

DEFINE VARIABLE fEntityseq AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Entity Seq" 
     VIEW-AS FILL-IN 
     SIZE 38 BY 1 NO-UNDO.

DEFINE VARIABLE fEntityType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity Type" 
     VIEW-AS FILL-IN 
     SIZE 38 BY 1 NO-UNDO.

DEFINE VARIABLE fobjattr AS CHARACTER FORMAT "X(256)":U 
     LABEL "Object Attribute" 
     VIEW-AS FILL-IN 
     SIZE 38 BY 1 NO-UNDO.

DEFINE VARIABLE fobjType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Object Type" 
     VIEW-AS FILL-IN 
     SIZE 38 BY 1 NO-UNDO.

DEFINE VARIABLE fUid AS CHARACTER FORMAT "X(256)":U 
     LABEL "User Id" 
     VIEW-AS FILL-IN 
     SIZE 38 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fEntityType AT ROW 1.57 COL 18.6 COLON-ALIGNED WIDGET-ID 60
     fEntityID AT ROW 2.76 COL 18.6 COLON-ALIGNED WIDGET-ID 4
     fEntityseq AT ROW 3.91 COL 18.6 COLON-ALIGNED WIDGET-ID 14
     fobjType AT ROW 5.05 COL 18.6 COLON-ALIGNED WIDGET-ID 6
     fobjattr AT ROW 6.19 COL 18.6 COLON-ALIGNED WIDGET-ID 8
     fdocdate AT ROW 7.33 COL 18.6 COLON-ALIGNED WIDGET-ID 10
     fUid AT ROW 8.52 COL 18.6 COLON-ALIGNED WIDGET-ID 12
     SPACE(2.99) SKIP(0.76)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "System Document" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       fdocdate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fEntityID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fEntityseq:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fEntityType:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fobjattr:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fobjType:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fUid:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* System Document */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  /* Display data on the widgets */
  run setData in this-procedure.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fEntityType fEntityID fEntityseq fobjType fobjattr fdocdate fUid 
      WITH FRAME Dialog-Frame.
  ENABLE fEntityType fEntityID fEntityseq fobjType fobjattr fdocdate fUid 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData Dialog-Frame 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  find first sysdoc no-error.
  if not available sysdoc 
   then
    return.

  assign
      fEntityType:screen-value = sysdoc.entityType
      fEntityID:screen-value   = sysdoc.entityid
      fEntityseq:screen-value  = string(sysdoc.entityseq)
      fobjType:screen-value    = sysdoc.objType
      fobjAttr:screen-value    = sysdoc.objAttr
      fdocdate:screen-value    = string(sysdoc.docdate)
      fuid:screen-value        = sysdoc.uid
      .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

