&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogsysprop.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gurvindar Singh

  Created:09/26/18
  
  @Modified:
  Date        Name        Description
  08/06/2020  AG          Copy Action - Enable save button if any of the
                          fields is changed 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{tt/sysprop.i &tablealias=ttsysprop}

/* Parameters Definitions                                           */
 define input  parameter table for ttsysprop.
 define input  parameter ipcActionMode  as character no-undo. /* (N)ew, (M)odify or (C)opy*/
 define output parameter opcAppCode     as character no-undo.
 define output parameter opcObjAction   as character no-undo.
 define output parameter opcObjID       as character no-undo.
 define output parameter opcObjProperty as character no-undo.
 define output parameter opcObjValue    as character no-undo.
 define output parameter oplSuccess     as logical   no-undo.

 /* library files */
 {lib/std-def.i}
 {lib/sys-def.i}

 /* local variable definitions */
 define variable cAppCode   as character no-undo.
 define variable cObjAction as character no-undo. 
 define variable cObjID     as character no-undo.
 define variable cObjProp   as character no-undo.
 define variable cObjValue  as character no-undo.
 define variable cObjName   as character no-undo.
 define variable cObjdesc   as character no-undo.
 define variable cObjref    as character no-undo.
 define variable cComment   as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fappCode fobjAction fobjID fobjProp ~
fobjValue fobjName fobjdesc fobjref eComment Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS fappCode fobjAction fobjID fobjProp ~
fobjValue fobjName fobjdesc fobjref fmodified lastModified eComment 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validateSysProp Dialog-Frame 
FUNCTION validateSysProp RETURNS LOGICAL
  ( )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO  NO-FOCUS
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE eComment AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 42.2 BY 2.81 NO-UNDO.

DEFINE VARIABLE fappCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Application Code" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 42.2 BY 1 NO-UNDO.

DEFINE VARIABLE fmodified AS CHARACTER FORMAT "X(256)":U 
     LABEL "Modified by" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 42.2 BY 1 NO-UNDO.

DEFINE VARIABLE fobjAction AS CHARACTER FORMAT "X(256)":U 
     LABEL "Action" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 42.2 BY 1 NO-UNDO.

DEFINE VARIABLE fobjdesc AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 42.2 BY 1 NO-UNDO.

DEFINE VARIABLE fobjID AS CHARACTER FORMAT "X(256)":U 
     LABEL " ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 42.2 BY 1 NO-UNDO.

DEFINE VARIABLE fobjName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 42.2 BY 1 NO-UNDO.

DEFINE VARIABLE fobjProp AS CHARACTER FORMAT "X(256)":U 
     LABEL "Property" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 42.2 BY 1 NO-UNDO.

DEFINE VARIABLE fobjref AS CHARACTER FORMAT "X(256)":U 
     LABEL "Reference" 
     VIEW-AS FILL-IN 
     SIZE 42.2 BY 1 NO-UNDO.

DEFINE VARIABLE fobjValue AS CHARACTER FORMAT "X(256)":U 
     LABEL "Value" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 42.2 BY 1 NO-UNDO.

DEFINE VARIABLE lastModified AS DATE FORMAT "99/99/99":U 
     LABEL "Last Modified" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 13.2 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     Btn_OK AT ROW 16.38 COL 22.6 NO-TAB-STOP 
     fappCode AT ROW 1.62 COL 25.8 COLON-ALIGNED WIDGET-ID 38
     fobjAction AT ROW 2.76 COL 25.8 COLON-ALIGNED WIDGET-ID 4
     fobjID AT ROW 3.91 COL 25.8 COLON-ALIGNED WIDGET-ID 30
     fobjProp AT ROW 5.05 COL 25.8 COLON-ALIGNED WIDGET-ID 40
     fobjValue AT ROW 6.19 COL 25.8 COLON-ALIGNED WIDGET-ID 42
     fobjName AT ROW 7.33 COL 25.8 COLON-ALIGNED WIDGET-ID 2
     fobjdesc AT ROW 8.48 COL 25.8 COLON-ALIGNED WIDGET-ID 48
     fobjref AT ROW 9.62 COL 25.8 COLON-ALIGNED WIDGET-ID 50
     fmodified AT ROW 13.71 COL 25.8 COLON-ALIGNED WIDGET-ID 34
     lastModified AT ROW 14.91 COL 25.8 COLON-ALIGNED WIDGET-ID 36
     eComment AT ROW 10.76 COL 27.8 NO-LABEL WIDGET-ID 26
     Btn_Cancel AT ROW 16.38 COL 40.6
     "Comments:" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 10.76 COL 17 WIDGET-ID 28
     SPACE(49.59) SKIP(6.37)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Properties"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       eComment:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN fmodified IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fmodified:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN lastModified IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       lastModified:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Properties */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel Dialog-Frame
ON CHOOSE OF Btn_Cancel IN FRAME Dialog-Frame /* Cancel */
DO:
  oplSuccess = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  if validateSysProp() 
   then 
    do:
      run saveSysProp in this-procedure.
      /* If there was any error then do not close the dialog. */
      if not oplSuccess
       then
        return no-apply.
      assign
          opcAppCode     = ttsysProp.appCode    
          opcObjAction   = ttsysProp.objAction  
          opcObjID       = ttsysProp.objID      
          opcObjProperty = ttsysProp.objProperty
          opcObjValue    = ttsysProp.objValue   
          no-error
          . 
    end.   
   else
    return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eComment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eComment Dialog-Frame
ON VALUE-CHANGED OF eComment IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fappCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fappCode Dialog-Frame
ON VALUE-CHANGED OF fappCode IN FRAME Dialog-Frame /* Application Code */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fobjAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fobjAction Dialog-Frame
ON VALUE-CHANGED OF fobjAction IN FRAME Dialog-Frame /* Action */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fobjdesc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fobjdesc Dialog-Frame
ON VALUE-CHANGED OF fobjdesc IN FRAME Dialog-Frame /* Description */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fobjID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fobjID Dialog-Frame
ON VALUE-CHANGED OF fobjID IN FRAME Dialog-Frame /*  ID */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fobjName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fobjName Dialog-Frame
ON VALUE-CHANGED OF fobjName IN FRAME Dialog-Frame /* Name */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fobjProp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fobjProp Dialog-Frame
ON VALUE-CHANGED OF fobjProp IN FRAME Dialog-Frame /* Property */
DO:
  run enableDisableSave in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fobjref
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fobjref Dialog-Frame
ON VALUE-CHANGED OF fobjref IN FRAME Dialog-Frame /* Reference */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fobjValue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fobjValue Dialog-Frame
ON VALUE-CHANGED OF fobjValue IN FRAME Dialog-Frame /* Value */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  run displayData.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cErrMsg as character no-undo.

  do with frame {&frame-name}:
  end.
  
  find first ttsysprop  no-error.
  if available ttsysprop 
   then
    do:
      if ipcActionMode = {&Modify}            
       then
        do:
          /* displaying username instead of userID */      
          publish "GetSysUserName" (input ttsysprop.modifiedby, 
                                    output std-ch,
                                    output std-lo,
                                    output cErrMsg).
          if not std-lo 
           then
            message cErrMsg
                view-as alert-box info buttons ok.

          assign
              fappCode    :sensitive    = false
              fobjProp    :sensitive    = false
              fobjAction  :sensitive    = false
              fobjvalue   :sensitive    = false
              fobjID      :sensitive    = false
              fmodified   :screen-value = if std-ch ne ? then std-ch else ""
              lastmodified:screen-value = string(ttsysprop.lastModified)
              .
        end.
        
      assign
          frame dialog-frame:title  = if ipcActionMode = {&Modify} then "Edit Property" else "Copy Property"
          fappcode:screen-value     = ttsysprop.appcode
          fobjvalue:screen-value    = ttsysprop.objvalue
          fobjref:screen-value      = ttsysprop.objref
          fobjprop:screen-value     = ttsysprop.objproperty
          fobjdesc:screen-value     = ttsysprop.objdesc
          fobjName:screen-value     = ttsysprop.objName
          fobjAction:screen-value   = ttsysprop.objAction
          fobjID:screen-value       = ttsysprop.objID
          ecomment:screen-value     = ttsysprop.comments          
          btn_OK:label              = if ipcActionMode = {&Modify} then "Save" else "Create"
          btn_OK:tooltip            = if ipcActionMode = {&Modify} then "Save" else "Create"
          .
    end.
    
   else
    do:
      assign 
          fappCode   :sensitive = true
          fobjProp   :sensitive = true
          fobjAction :sensitive = true
          fobjvalue  :sensitive = true
          fobjID     :sensitive = true
          .
      
      assign
          frame dialog-frame:title  = "New Property"
          btn_OK:label              = "Create"
          btn_OK:tooltip            = "Create"
          .
    end.

  /* Keep the copy of initial values. Required in enableDisableSave. */ 
  assign
      cAppCode    = fAppCode:input-value     
      cObjAction  = fObjAction:input-value        
      cObjID      = fObjID:input-value       
      cObjProp    = fObjProp:input-value          
      cObjValue   = fObjValue:input-value        
      cObjName    = fobjName:input-value       
      cObjdesc    = fobjdesc:input-value   
      cObjref     = fobjref:input-value      
      cComment    = eComment:input-value
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if ipcActionMode = {&New} 
   then
    Btn_OK:sensitive = not ((fAppCode:input-value   = "" or
                             fObjAction:input-value = "" 
                            ) 
                            or
                            (
                             cAppCode          = fAppCode:input-value     and
                             cObjAction        = fObjAction:input-value   
                            )
                           )no-error.
						   
   else if ipcActionMode = {&Copy}
    then 
     Btn_OK:sensitive = not ((fAppCode:input-value   = "" or
                              fObjAction:input-value = "" 
                             ) 
                             or
                             (cAppCode         = fAppCode:input-value     and
                             cObjAction        = fObjAction:input-value   and
                             cObjID            = fObjID:input-value       and
                             cObjProp          = fObjProp:input-value     and
                             cObjValue         = fObjValue:input-value
                             )
                            ) no-error.

	   
   else  /* when (M)odifyData*/
    Btn_OK:sensitive = not (cAppCode          = fAppCode:input-value     and
                            cObjAction        = fObjAction:input-value   and
                            cObjID            = fObjID:input-value       and
                            cObjProp          = fObjProp:input-value     and
                            cObjValue         = fObjValue:input-value    and
                            cObjName          = fobjName:input-value     and
                            cObjdesc          = fobjdesc:input-value     and
                            cObjref           = fobjref:input-value      and
                            cComment          = eComment:input-value
                            ) no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fappCode fobjAction fobjID fobjProp fobjValue fobjName fobjdesc 
          fobjref fmodified lastModified eComment 
      WITH FRAME Dialog-Frame.
  ENABLE fappCode fobjAction fobjID fobjProp fobjValue fobjName fobjdesc 
         fobjref eComment Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveSysProp Dialog-Frame 
PROCEDURE saveSysProp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if ipcActionMode = {&New} 
   then
    create ttsysprop.
     
  assign 
      ttsysprop.appcode       = fappcode:input-value in frame {&frame-name}
      ttsysprop.objAction     = fobjAction:input-value
      ttsysprop.objId         = fobjID:input-value
      ttsysprop.objproperty   = fobjProp:input-value
      ttsysprop.objValue      = fobjValue:input-value
      ttsysprop.objName       = fobjName:input-value
      ttsysprop.objdesc       = fobjdesc:input-value
      ttsysprop.objref        = fobjref:input-value
      ttsysprop.comments      = eComment:input-value
      .

  if ipcActionMode = {&Modify}
   then 
    publish "modifySysProp" (input table ttsysprop,
                             output oplSuccess,
                             output std-ch ).
   else /* when (N)ewData or (C)opyData */ 
    publish "NewSysProp" (input table ttsysprop,
                          output oplSuccess,
                          output std-ch).

  if not oplSuccess 
   then 
    message std-ch
      view-as alert-box info buttons ok.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validateSysProp Dialog-Frame 
FUNCTION validateSysProp RETURNS LOGICAL
  ( ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* Validations */
  if fAppCode:input-value in frame {&frame-name} = "" 
   then
    do:
      message "Application Code cannot be blank."
        view-as alert-box info buttons ok.
      return false.
    end.

  if fobjAction:input-value in frame {&frame-name} = "" 
   then
    do:
      message "Action cannot be blank."
        view-as alert-box info buttons ok.
      return false.
    end.

 /* if fobjID:input-value in frame {&frame-name} = "" 
   then
    do:
      message "ID cannot be blank."
        view-as alert-box info buttons ok.
      return false.
    end.

  if fobjProp:input-value in frame {&frame-name} = "" 
   then
    do:
      message "Property cannot be blank."
        view-as alert-box info buttons ok.
      return false.
    end.

  if fobjValue:input-value in frame {&frame-name} = "" 
   then
    do:
      message "Value cannot be blank."
        view-as alert-box info buttons ok.
      return false.
    end.*/

  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

