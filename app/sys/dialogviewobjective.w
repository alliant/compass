&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogviewobjective.p

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Sachin Chaturvedi

  Created:01/29/2020
  @Modification:
  Date          Name          Description
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{tt/objective.i}
{lib/sys-def.i}

/* Parameters Definitions ---                                           */
define input parameter table for objective.

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS flObjectiveID tPrivate flEntity flEntityID ~
flEntityName flStat flUID flDueDate edDescription flType flCreateDate 
&Scoped-Define DISPLAYED-OBJECTS flObjectiveID tPrivate flEntity flEntityID ~
flEntityName flStat flUID flDueDate edDescription flType flCreateDate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE VARIABLE edDescription AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 40 BY 4 NO-UNDO.

DEFINE VARIABLE flCreateDate AS DATETIME FORMAT "99/99/99 HH:MM:SS":U 
     LABEL "Create" 
     VIEW-AS FILL-IN 
     SIZE 31 BY 1 NO-UNDO.

DEFINE VARIABLE flDueDate AS DATETIME FORMAT "99/99/99 HH:MM:SS":U 
     LABEL "Due" 
     VIEW-AS FILL-IN 
     SIZE 31 BY 1 NO-UNDO.

DEFINE VARIABLE flEntity AS CHARACTER FORMAT "X(50)":U 
     LABEL "Entity" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE flEntityID AS CHARACTER FORMAT "X(50)":U 
     LABEL "Entity ID" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE flEntityName AS CHARACTER FORMAT "X(200)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 40.4 BY 1 NO-UNDO.

DEFINE VARIABLE flObjectiveID AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "ID" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE flStat AS CHARACTER FORMAT "X(50)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE flType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flUID AS CHARACTER FORMAT "X(100)":U 
     LABEL "UID" 
     VIEW-AS FILL-IN 
     SIZE 40.4 BY 1 NO-UNDO.

DEFINE VARIABLE tPrivate AS LOGICAL INITIAL no 
     LABEL "Is Private" 
     VIEW-AS TOGGLE-BOX
     SIZE 13 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     flObjectiveID AT ROW 1.52 COL 14.2 COLON-ALIGNED WIDGET-ID 26
     tPrivate AT ROW 1.52 COL 44.8 WIDGET-ID 46
     flEntity AT ROW 2.71 COL 14.2 COLON-ALIGNED WIDGET-ID 20
     flEntityID AT ROW 3.91 COL 14.2 COLON-ALIGNED WIDGET-ID 2
     flEntityName AT ROW 5.1 COL 14.2 COLON-ALIGNED WIDGET-ID 16
     flStat AT ROW 6.29 COL 14.2 COLON-ALIGNED WIDGET-ID 32
     flUID AT ROW 7.48 COL 14.2 COLON-ALIGNED WIDGET-ID 22
     flDueDate AT ROW 8.71 COL 14 COLON-ALIGNED WIDGET-ID 48
     edDescription AT ROW 9.95 COL 16 NO-LABEL WIDGET-ID 50
     flType AT ROW 14.19 COL 14.2 COLON-ALIGNED WIDGET-ID 54
     flCreateDate AT ROW 15.43 COL 14.2 COLON-ALIGNED WIDGET-ID 56
     "Description:" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 10.05 COL 4.2 WIDGET-ID 52
     SPACE(45.79) SKIP(6.13)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Objective" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       flEntity:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flEntityID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flEntityName:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flObjectiveID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flStat:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flUID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Objective */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  run enable_UI.
  
  /* sets default values to the widgets */
  run setData.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.

RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flObjectiveID tPrivate flEntity flEntityID flEntityName flStat flUID 
          flDueDate edDescription flType flCreateDate 
      WITH FRAME Dialog-Frame.
  ENABLE flObjectiveID tPrivate flEntity flEntityID flEntityName flStat flUID 
         flDueDate edDescription flType flCreateDate 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData Dialog-Frame 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  frame dialog-frame:title = "View objective".

  find first objective no-error.
  if not available objective 
   then
    return.
    
  assign 
      flobjectiveID:screen-value   = string(objective.objectiveID)
      flEntity:screen-value        = objective.entity
      flEntityID:screen-value      = objective.entityID
      flStat:screen-value          = objective.stat
      flUID:screen-value           = objective.UID
      flDueDate:screen-value       = string(objective.dueDate) 
      tPrivate:screen-value        = string(objective.private) 
      flEntityName:screen-value    = objective.entityName
      edDescription:screen-value   = objective.description
      flType:screen-value          = objective.type
      flCreateDate:screen-value    = string(objective.createDate)
      .
      
   /* Entity */
   if objective.entity = {&Agent} 
    then
     flEntity:screen-value   = "Agent".       
    else if objective.entity = {&Person} 
     then
      flEntity:screen-value   = "Person".
    
   /* Status */ 
   if objective.stat = {&Active} 
    then
     flStat:screen-value = {&ActiveValue}.       
    else if objective.stat = {&Closed} 
     then
      flStat:screen-value = {&ClosedValue}.
    else if objective.stat = {&Cancelled} 
     then
      flStat:screen-value = {&CancelledValue}.
   
   /* Type*/
   if objective.type = {&Our} 
    then
     flType:screen-value   = {&OurValue}.       
    else if objective.type = {&Their} 
     then
      flType:screen-value   = {&TheirValue}.   
      
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

