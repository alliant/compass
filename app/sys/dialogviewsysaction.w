&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogviewsysaction.w

  Description: View SystemAction

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul

  Created: 03.22.2019
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/sys-def.i}

{tt/sysaction.i}

/* Parameters Definitions ---                                           */
 define input  parameter table for sysaction.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fAction fDesc fprogexec femailSuccess ~
femailFailure edRoles edUsers edAddr createDate eComments RECT-69 RECT-70 
&Scoped-Define DISPLAYED-OBJECTS fAction fDesc fprogexec tActive tAnonymous ~
tLog tAudit tsecure tevent radioEmails femailSuccess femailFailure edRoles ~
edUsers edAddr createDate eComments 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE VARIABLE eComments AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 68.2 BY 2.14 TOOLTIP "Comments" NO-UNDO.

DEFINE VARIABLE edAddr AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 66.2 BY 2.14 TOOLTIP "Comments" NO-UNDO.

DEFINE VARIABLE edRoles AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 66.2 BY 2.14 TOOLTIP "Comments" NO-UNDO.

DEFINE VARIABLE edUsers AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 66.2 BY 2.14 TOOLTIP "Comments" NO-UNDO.

DEFINE VARIABLE fDesc AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 57.8 BY 2.14 TOOLTIP "Description" NO-UNDO.

DEFINE VARIABLE createDate AS DATE FORMAT "99/99/99":U 
     LABEL "Create" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fAction AS CHARACTER FORMAT "X(256)":U 
     LABEL "Action" 
     VIEW-AS FILL-IN 
     SIZE 31.8 BY 1 NO-UNDO.

DEFINE VARIABLE femailFailure AS CHARACTER FORMAT "X(256)":U 
     LABEL "Failure Address" 
     VIEW-AS FILL-IN 
     SIZE 57.8 BY 1 TOOLTIP "Comma delimited list of failure email address" DROP-TARGET NO-UNDO.

DEFINE VARIABLE femailSuccess AS CHARACTER FORMAT "X(256)":U 
     LABEL "Success Address" 
     VIEW-AS FILL-IN 
     SIZE 57.8 BY 1 TOOLTIP "Comma delimited list of success email addresses" NO-UNDO.

DEFINE VARIABLE fprogexec AS CHARACTER FORMAT "X(256)":U 
     LABEL "Program Execution" 
     VIEW-AS FILL-IN 
     SIZE 57.8 BY 1 TOOLTIP "Program Execution" NO-UNDO.

DEFINE VARIABLE radioEmails AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Success", "S",
"Failure", "F",
"Both", "B",
"None", "N"
     SIZE 46.2 BY .95 NO-UNDO.

DEFINE RECTANGLE RECT-69
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 79 BY 4.67.

DEFINE RECTANGLE RECT-70
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 79 BY 8.29.

DEFINE VARIABLE tActive AS LOGICAL INITIAL no 
     LABEL "Active" 
     VIEW-AS TOGGLE-BOX
     SIZE 10 BY .81 NO-UNDO.

DEFINE VARIABLE tAnonymous AS LOGICAL INITIAL no 
     LABEL "Anonymous" 
     VIEW-AS TOGGLE-BOX
     SIZE 14.2 BY .81 NO-UNDO.

DEFINE VARIABLE tAudit AS LOGICAL INITIAL no 
     LABEL "Audit" 
     VIEW-AS TOGGLE-BOX
     SIZE 9.8 BY .81 NO-UNDO.

DEFINE VARIABLE tevent AS LOGICAL INITIAL no 
     LABEL "Event Enabled" 
     VIEW-AS TOGGLE-BOX
     SIZE 19 BY .81 NO-UNDO.

DEFINE VARIABLE tLog AS LOGICAL INITIAL no 
     LABEL "Log" 
     VIEW-AS TOGGLE-BOX
     SIZE 7.8 BY .81 NO-UNDO.

DEFINE VARIABLE tsecure AS LOGICAL INITIAL no 
     LABEL "Secure" 
     VIEW-AS TOGGLE-BOX
     SIZE 11.2 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fAction AT ROW 1.43 COL 20.2 COLON-ALIGNED WIDGET-ID 2
     fDesc AT ROW 2.67 COL 22.2 NO-LABEL WIDGET-ID 72
     fprogexec AT ROW 5.05 COL 20.2 COLON-ALIGNED WIDGET-ID 24
     tActive AT ROW 6.29 COL 22.2 WIDGET-ID 10
     tAnonymous AT ROW 6.29 COL 40.4 WIDGET-ID 56
     tLog AT ROW 7.29 COL 22.2 WIDGET-ID 64
     tAudit AT ROW 7.29 COL 40.4 WIDGET-ID 66
     tsecure AT ROW 8.24 COL 22.2 WIDGET-ID 62
     tevent AT ROW 8.24 COL 40.2 WIDGET-ID 68
     radioEmails AT ROW 10.1 COL 22.8 NO-LABEL WIDGET-ID 112
     femailSuccess AT ROW 11.29 COL 20.8 COLON-ALIGNED WIDGET-ID 48
     femailFailure AT ROW 12.57 COL 20.8 COLON-ALIGNED WIDGET-ID 46
     edRoles AT ROW 15.43 COL 14.4 NO-LABEL WIDGET-ID 132
     edUsers AT ROW 17.86 COL 14.4 NO-LABEL WIDGET-ID 138
     edAddr AT ROW 20.29 COL 14.4 NO-LABEL WIDGET-ID 136
     createDate AT ROW 23.33 COL 12 COLON-ALIGNED WIDGET-ID 30
     eComments AT ROW 24.62 COL 14 NO-LABEL WIDGET-ID 58
     "Comments:" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 24.67 COL 3.2 WIDGET-ID 60
     "Send When:" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 10.24 COL 9.8 WIDGET-ID 116
     "Client IPs:" VIEW-AS TEXT
          SIZE 8.8 BY .62 AT ROW 20.38 COL 5 WIDGET-ID 140
     "Users:" VIEW-AS TEXT
          SIZE 6.4 BY .62 AT ROW 17.91 COL 8 WIDGET-ID 142
     "Roles:" VIEW-AS TEXT
          SIZE 6.4 BY .62 AT ROW 15.48 COL 8 WIDGET-ID 144
     "Security" VIEW-AS TEXT
          SIZE 8.6 BY .62 AT ROW 14.43 COL 4.4 WIDGET-ID 146
     "Email Notifications" VIEW-AS TEXT
          SIZE 18 BY .62 AT ROW 9.24 COL 4.4 WIDGET-ID 124
     "Description:" VIEW-AS TEXT
          SIZE 11.2 BY .62 AT ROW 2.81 COL 11 WIDGET-ID 74
     RECT-69 AT ROW 9.57 COL 3.4 WIDGET-ID 122
     RECT-70 AT ROW 14.81 COL 3.4 WIDGET-ID 134
     SPACE(1.39) SKIP(3.84)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Sys Action" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       createDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       eComments:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       edAddr:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       edRoles:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       edUsers:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fAction:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fDesc:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       femailFailure:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       femailSuccess:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fprogexec:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR RADIO-SET radioEmails IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tActive IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tAnonymous IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tAudit IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tevent IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tLog IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tsecure IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Sys Action */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  /* getting the data */
  run getData in this-procedure.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fAction fDesc fprogexec tActive tAnonymous tLog tAudit tsecure tevent 
          radioEmails femailSuccess femailFailure edRoles edUsers edAddr 
          createDate eComments 
      WITH FRAME Dialog-Frame.
  ENABLE fAction fDesc fprogexec femailSuccess femailFailure edRoles edUsers 
         edAddr createDate eComments RECT-69 RECT-70 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData Dialog-Frame 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame Dialog-Frame:
  end.

  define buffer sysaction for sysaction.

  find first sysaction no-error.
  if not available sysaction 
   then
    return.

     
  assign
      faction:screen-value           = sysaction.action
      fDesc:screen-value             = sysaction.description
      fprogexec:screen-value         = sysaction.progExec
      tActive:checked                = sysaction.isActive
      tAnonymous:checked             = sysaction.isAnonymous                     
      tSecure:checked                = sysaction.isSecure                
      tlog:checked                   = sysaction.isLog           
      tAudit:checked                 = sysaction.isAudit         
      edRoles:screen-value           = trim(sysaction.roles,",")
      edUsers:screen-value           = trim(sysaction.userids,",")
      edAddr:screen-value            = sysaction.addrs           
      createDate:screen-value        = string(sysaction.createDate)      
      radioemails:screen-value       = sysaction.emails          
      femailSuccess:screen-value     = sysaction.emailSuccess    
      femailFailure:screen-value     = sysaction.emailFailure    
      tevent:checked                 = logical(sysaction.isEventsEnabled )
      ecomments:screen-value         = sysaction.comments
      .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

