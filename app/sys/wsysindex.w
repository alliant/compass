&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File:wsysindex.w 

  Description:User Interface for System Indexes. 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul Sharma 

  Created:03.13.2019 
  
  Modified:
 
  Date       Name      Description
 
  08/22/2019 Gurvindar Removed progress error while populating combo-box.

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

{lib/std-def.i}

/* ***************************  Definitions  ************************** */
{tt/sysindex.i}
{tt/sysindex.i &tableAlias=tsysindex}
{tt/sysindex.i &tableAlias=ttsysindex}

&scoped-define SelectType "--Select Type--"
&scoped-define SelectID   "--Select ID--"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwsysindex

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttsysindex

/* Definitions for BROWSE brwsysindex                                   */
&Scoped-define FIELDS-IN-QUERY-brwsysindex ttsysindex.objType "Object Type" ttsysindex.objID "Object ID" ttsysindex.objAttribute "Object Attribute" ttsysindex.objKey "Object Key"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwsysindex   
&Scoped-define SELF-NAME brwsysindex
&Scoped-define QUERY-STRING-brwsysindex for each ttsysindex
&Scoped-define OPEN-QUERY-brwsysindex open query {&SELF-NAME} for each ttsysindex.
&Scoped-define TABLES-IN-QUERY-brwsysindex ttsysindex
&Scoped-define FIRST-TABLE-IN-QUERY-brwsysindex ttsysindex


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwsysindex}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-62 RECT-63 RECT-61 bSearch cbObjType ~
fSearch brwsysindex 
&Scoped-Define DISPLAYED-OBJECTS cbObjType cbObjId fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resetScreen C-Win 
FUNCTION resetScreen RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search Data".

DEFINE BUTTON btGet  NO-FOCUS
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get Data".

DEFINE BUTTON bView  NO-FOCUS
     LABEL "View" 
     SIZE 7.2 BY 1.71 TOOLTIP "View Entry".

DEFINE VARIABLE cbObjId AS CHARACTER 
     LABEL "Object ID" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN AUTO-COMPLETION
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE cbObjType AS CHARACTER FORMAT "X(256)" 
     LABEL "Object Type" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 47.6 BY 1 TOOLTIP "Search Criteria (objectkey,attribute)" NO-UNDO.

DEFINE RECTANGLE RECT-61
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17.4 BY 2.43.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 58.6 BY 2.43.

DEFINE RECTANGLE RECT-63
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 73.4 BY 2.43.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwsysindex FOR 
      ttsysindex SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwsysindex
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwsysindex C-Win _FREEFORM
  QUERY brwsysindex DISPLAY
      ttsysindex.objType       label        "Object Type"        format "x(25)"    width 20
ttsysindex.objID         label        "Object ID"          format "x(20)"    width 20
ttsysindex.objAttribute  label        "Object Attribute"   format "x(25)"    width 30
ttsysindex.objKey        label        "Object Key"         format "x(60)"    width 20
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SEPARATORS SIZE 148.4 BY 17.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bExport AT ROW 1.67 COL 134.4 WIDGET-ID 2 NO-TAB-STOP 
     bSearch AT ROW 1.67 COL 125 WIDGET-ID 314 NO-TAB-STOP 
     cbObjType AT ROW 2 COL 14 COLON-ALIGNED WIDGET-ID 322
     btGet AT ROW 1.67 COL 66.8 WIDGET-ID 326 NO-TAB-STOP 
     cbObjId AT ROW 2 COL 45.8 COLON-ALIGNED WIDGET-ID 324
     bView AT ROW 1.67 COL 142 WIDGET-ID 4 NO-TAB-STOP 
     fSearch AT ROW 2 COL 74.4 COLON-ALIGNED NO-LABEL WIDGET-ID 310
     brwsysindex AT ROW 4.1 COL 2 WIDGET-ID 500
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.05 COL 134.4 WIDGET-ID 52
     "Parameters" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 1.1 COL 3.2 WIDGET-ID 320
     "Search" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.1 COL 76.2 WIDGET-ID 316
     RECT-62 AT ROW 1.33 COL 75 WIDGET-ID 312
     RECT-63 AT ROW 1.33 COL 2 WIDGET-ID 318
     RECT-61 AT ROW 1.33 COL 133.2 WIDGET-ID 50
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 158.6 BY 25.67
         DEFAULT-BUTTON btGet WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Indexes"
         HEIGHT             = 21
         WIDTH              = 150.6
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwsysindex fSearch DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bExport IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwsysindex:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwsysindex:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

/* SETTINGS FOR BUTTON btGet IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bView IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbObjId IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwsysindex
/* Query rebuild information for BROWSE brwsysindex
     _START_FREEFORM
open query {&SELF-NAME} for each ttsysindex.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwsysindex */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Indexes */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Indexes */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Indexes */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwsysindex
&Scoped-define SELF-NAME brwsysindex
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwsysindex C-Win
ON DEFAULT-ACTION OF brwsysindex IN FRAME DEFAULT-FRAME
DO:
  run showSysIndex in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwsysindex C-Win
ON ROW-DISPLAY OF brwsysindex IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwsysindex C-Win
ON START-SEARCH OF brwsysindex IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGet C-Win
ON CHOOSE OF btGet IN FRAME DEFAULT-FRAME /* Get */
DO:
  resetScreen().
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bView C-Win
ON CHOOSE OF bView IN FRAME DEFAULT-FRAME /* View */
DO:
  run showSysIndex in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbObjId
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbObjId C-Win
ON VALUE-CHANGED OF cbObjId IN FRAME DEFAULT-FRAME /* Object ID */
DO:
  resultsChanged(false).

  btGet:sensitive = (self:screen-value <> {&SelectID} and self:screen-value <> ?).
  
  if self:screen-value <> {&SelectID} 
   then
    /* Delete Select Type from Object Type combo-box */
    {lib/modifylist.i &cbhandle=cbObjId:handle &list=list-items &delimiter = "," &item = '{&SelectID}'}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbObjType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbObjType C-Win
ON VALUE-CHANGED OF cbObjType IN FRAME DEFAULT-FRAME /* Object Type */
DO:
  resultsChanged(false).
  
  /* Procedure to populate sysindex object Ids in cbObjId cmbo-box */
  run getSysIndexIds in this-procedure (input cbObjType:input-value).

  assign
      cbObjId:sensitive = (self:screen-value    <> {&SelectType} and self:screen-value    <> ?)
      btGet:sensitive   = (cbObjId:screen-value <> {&SelectID} and cbObjId:screen-value <> ?)
      .
  
  if self:screen-value <> {&SelectType} 
   then
    /* Delete Select Type from Object Type combo-box */
    {lib/modifylist.i &cbhandle=cbObjType:handle &list=list-items &delimiter = "," &item = '{&SelectType}'} 
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME DEFAULT-FRAME
DO:
  apply "CHOOSE" to bSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
on close of this-procedure 
  run disable_UI.

/* Best default for GUI applications is...                              */
pause 0 before-hide.
subscribe to "closeWindow" anywhere.

btGet  :load-image            ("images/completed.bmp").              
btGet  :load-image-insensitive("images/completed-i.bmp").

bView  :load-image            ("images/open.bmp").
bView  :load-image-insensitive("images/open-i.bmp").

bSearch:load-image-up         ("images/magnifier.bmp").
bSearch:load-image-insensitive("images/magnifier-i.bmp").

bExport:load-image            ("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.
  
  /* Function to reset default values and clear filters */
  resetScreen().
  
  /* Populate cbObjType combo-box with sysindex object type list */
  run getSysIndexTypes in this-procedure.

  setStatusMessage("").

  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.
   
  run windowResized in this-procedure.
  
  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbObjType cbObjId fSearch 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-62 RECT-63 RECT-61 bSearch cbObjType fSearch brwsysindex 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable htable as handle no-undo.

  if query brwsysindex:num-results = 0 
   then
    do: 
      message "There is nothing to export"
        view-as alert-box warning buttons ok.
      return.
    end.
  
  publish "GetReportDir" (output std-ch).
  htable = temp-table ttsysindex:handle.
  run util/exporttable.p (table-handle htable,
                          "ttsysindex",
                          "for each ttsysindex",
                          "objID,objType,objAttribute,objKey",
                          "ObjectID,Object Type,Object Attribute,Object Key",
                          std-ch,
                          "System Indexes-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in). 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterdata C-Win 
PROCEDURE filterdata :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer sysindex for sysindex.
 
  close query brwsysindex.
  empty temp-table ttsysindex.
  
  do with frame {&frame-name}:
  end.
  
  for each sysindex:
    /* test if the record contains the search text */
    if fSearch:screen-value <> "" and 
      not ( (sysindex.objType      matches "*" + fSearch:screen-value + "*")  or 
            (sysindex.objAttribute matches "*" + fSearch:screen-value + "*")  or 
            (sysindex.objKey       matches "*" + fSearch:screen-value + "*") )
     then next.
   
    create ttsysindex.
    buffer-copy sysindex to ttsysindex.
  end.
  
  open query brwsysindex preselect each ttsysindex.
  
  if query brwsysindex:num-results > 0 
   then
    assign 
        bExport:sensitive = true
        bView:sensitive   = true
        .
   else
    assign 
         bExport:sensitive = false
         bView:sensitive   = false
         .

  /* Display no. of records on status bar */
  setStatusCount(query brwsysindex:num-results).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer sysindex for sysindex. 
   
  do with frame {&frame-name}:
  end.
  
  /* client Server Call */
  run server/getsysindexes.p (input cbObjType:input-value, /* ObjType */
                              input cbObjId:input-value,   /* ObjId   */
                              output table sysindex,
                              output std-lo,
                              output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch
        view-as alert-box info buttons ok.
      return.
    end. 
                                                                       
  /* This will use the screen-value of the filter search box */
  run filterData in this-procedure.
  
  assign 
      browse brwsysindex:sensitive = true
      fSearch:sensitive            = true
      bSearch:sensitive            = true
      .  

  /* Display no. of records with date and time on status bar */
  setStatusRecords(query brwsysindex:num-results).
              
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getSysIndexIds C-Win 
PROCEDURE getSysIndexIds :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: Populate combo-box cbObjId with sysindex object Ids based on object type      
------------------------------------------------------------------------------*/
  /* Input Parameter */
  define input parameter cObjType as character no-undo.
  
  /* Local variable */
  define variable cObjIdList as character no-undo.
  
  do with frame {&frame-name}:
  end.

  if cObjType = {&SelectType} 
   then
    return.

  publish "getSysIndexIds" (input  cObjType,
                            input  ",",   /* Delimiter */
                            output cObjIdList,
                            output std-lo,
                            output std-ch).

  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.

  cbObjId:list-items   = {&SelectID}.  
   if cObjIdList = "" 
    then
     return.

  assign
      cbObjId:list-items = cbObjId:list-items + "," + cObjIdList
      cbObjId:screen-value = {&SelectID}
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getSysIndexTypes C-Win 
PROCEDURE getSysIndexTypes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: Populate combo-box cbObjType with sysindex object types
------------------------------------------------------------------------------*/
  /* Local variable */
  define variable cObjTypeList as character no-undo.
  
  do with frame {&frame-name}:
  end.

  publish "getSysIndexTypes" (output cObjTypeList,
                              output std-lo,
                              output std-ch).

  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.

  cbObjType:list-items   = {&SelectType}.
   if cObjTypeList = "" 
    then
     return.

  assign 
    cbObjType:list-items   = cbObjType:list-items + "," + cObjTypeList
    cbObjType:screen-value = {&SelectType} 
    .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showSysIndex C-Win 
PROCEDURE showSysIndex :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  define buffer tsysindex for tsysindex.
  empty temp-table tsysindex.

  if not available ttsysindex
   then return.
  
  create tsysindex.
  buffer-copy ttsysindex to tsysindex.

  run dialogsysindex.w (input table tsysindex).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
         frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
         frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
         frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
         {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 11
         {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - 70.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resetScreen C-Win 
FUNCTION resetScreen RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:  
    /* Set default values for the client side filters */
    assign
        fSearch:screen-value = ""
        fSearch:sensitive    = false
        bSearch:sensitive    = false
        bExport:sensitive    = false
        bView:sensitive      = false
        .
  end.

  setStatusMessage("").
  return true. /* Function return value */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage("Results may not match current parameters.").
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

