&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File:wsysnotes.w 

  Description: SYSTEM NOTES

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gurvindar

  Created: 07/10/19

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

  create widget-pool.

  /* ***************************  Definitions  ************************** */
  {lib/std-def.i}
  {lib/sys-def.i}
  {lib/get-column.i}
  {lib/winshowscrollbars.i}

  /* Temp table definations */
  {tt/sysnote.i}
  {tt/sysnote.i &tableAlias="ttSysNote"}
  {tt/sysnote.i &tableAlias="tSysNote"}
  {tt/person.i}
  {tt/agent.i}
  {tt/attorney.i}
 
  /* Variable Definition */
  define variable hTextPerson         as handle    no-undo.
  define variable hTextAttorney       as handle    no-undo.
  define variable hSelectionPerson    as handle    no-undo.
  define variable hSelectionAttorney  as handle    no-undo.
  define variable hSelectionAgent     as handle    no-undo.
  define variable hTextAgent          as handle    no-undo.
  define variable cLastSearchString   as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES sysnote

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData sysnote.entityID "Entity ID" sysnote.entityName "Entity Name" sysnote.UID "UID" sysnote.notes "Notes"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each sysnote
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each sysnote.
&Scoped-define TABLES-IN-QUERY-brwData sysnote
&Scoped-define FIRST-TABLE-IN-QUERY-brwData sysnote


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btrefresh cbUID cbEntity brwData RECT-64 ~
RECT-69 RECT-71 
&Scoped-Define DISPLAYED-OBJECTS cbUID cbEntity fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getEntityId C-Win 
FUNCTION getEntityId RETURNS CHARACTER
  (cEntity as character /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btexport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON btget 
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get Data".

DEFINE BUTTON btrefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload Data".

DEFINE BUTTON btsearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search Data".

DEFINE BUTTON btview  NO-FOCUS
     LABEL "View" 
     SIZE 7.2 BY 1.71 TOOLTIP "View".

DEFINE VARIABLE cbAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity ID" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 47.2 BY 1 NO-UNDO.

DEFINE VARIABLE cbAttorney AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity ID" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 47.2 BY 1 NO-UNDO.

DEFINE VARIABLE cbEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 5
     LIST-ITEM-PAIRS "Agent","A",
                     "Attorney","T",
                     "Employee","E",
                     "Person","P"
     DROP-DOWN-LIST
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE cbEntityId AS CHARACTER 
     LABEL "Entity ID" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEMS "Item 1" 
     DROP-DOWN AUTO-COMPLETION
     SIZE 47.2 BY 1 NO-UNDO.

DEFINE VARIABLE cbPerson AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity ID" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 47.2 BY 1 NO-UNDO.

DEFINE VARIABLE cbUID AS CHARACTER 
     LABEL "UID" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN
     SIZE 34 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 32 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-64
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 24.2 BY 3.38.

DEFINE RECTANGLE RECT-69
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 109 BY 3.38.

DEFINE RECTANGLE RECT-71
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 41.8 BY 3.38.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      sysnote SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      sysnote.entityID       label      "Entity ID"                format "x(30)" width 15
 sysnote.entityName     label      "Entity Name"              format "x(40)" width 40     
 sysnote.UID            label      "UID"                      format "x(50)" width 40
 sysnote.notes          label      "Notes"                    format "x(200)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE NO-TAB-STOP SIZE 174 BY 16.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     btexport AT ROW 2.29 COL 3 WIDGET-ID 290 NO-TAB-STOP 
     btrefresh AT ROW 2.29 COL 10.6 WIDGET-ID 288 NO-TAB-STOP 
     btsearch AT ROW 2.29 COL 168.2 WIDGET-ID 320 NO-TAB-STOP 
     btview AT ROW 2.29 COL 18.2 WIDGET-ID 300 NO-TAB-STOP 
     cbUID AT ROW 2.67 COL 30 COLON-ALIGNED WIDGET-ID 306
     cbEntity AT ROW 2 COL 76 COLON-ALIGNED WIDGET-ID 310
     cbEntityId AT ROW 3.29 COL 76 COLON-ALIGNED WIDGET-ID 308
     cbPerson AT ROW 3.29 COL 76 COLON-ALIGNED WIDGET-ID 326
     cbAttorney AT ROW 3.29 COL 76 COLON-ALIGNED WIDGET-ID 324
     cbAgent AT ROW 3.29 COL 76 COLON-ALIGNED WIDGET-ID 330
     fSearch AT ROW 2.67 COL 133.6 COLON-ALIGNED NO-LABEL WIDGET-ID 314
     brwData AT ROW 5.29 COL 2.2 WIDGET-ID 200
     btget AT ROW 2.29 COL 126.8 WIDGET-ID 312 NO-TAB-STOP 
     "Actions" VIEW-AS TEXT
          SIZE 7.2 BY .62 AT ROW 1.1 COL 3.2 WIDGET-ID 286
     "Search" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 1.14 COL 135.8 WIDGET-ID 322
     "Parameters" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 1.14 COL 27 WIDGET-ID 304
     RECT-64 AT ROW 1.48 COL 2.2 WIDGET-ID 284
     RECT-69 AT ROW 1.48 COL 26 WIDGET-ID 302
     RECT-71 AT ROW 1.48 COL 134.6 WIDGET-ID 318
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 176.4 BY 21.76 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Notes"
         HEIGHT             = 21.71
         WIDTH              = 176.4
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData fSearch DEFAULT-FRAME */
ASSIGN 
       FRAME DEFAULT-FRAME:RESIZABLE        = TRUE.

ASSIGN 
       brwData:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwData:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

/* SETTINGS FOR BUTTON btexport IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btget IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btsearch IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btview IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbAgent IN FRAME DEFAULT-FRAME
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR COMBO-BOX cbAttorney IN FRAME DEFAULT-FRAME
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR COMBO-BOX cbEntityId IN FRAME DEFAULT-FRAME
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR COMBO-BOX cbPerson IN FRAME DEFAULT-FRAME
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN fSearch IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each sysnote.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Notes */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the note presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Notes */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Notes */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME DEFAULT-FRAME
do:
  apply 'choose':U to btView.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btexport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btexport C-Win
ON CHOOSE OF btexport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btget
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btget C-Win
ON CHOOSE OF btget IN FRAME DEFAULT-FRAME /* Get */
DO:
   run getData in this-procedure.
   fSearch:screen-value = cLastSearchString.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btrefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btrefresh C-Win
ON CHOOSE OF btrefresh IN FRAME DEFAULT-FRAME /* Refresh */
DO:
  run getData in this-procedure.
  fSearch:screen-value = cLastSearchString.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btsearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btsearch C-Win
ON CHOOSE OF btsearch IN FRAME DEFAULT-FRAME /* Search */
DO:
   /* if search button is clicked or return key is hit, 
     then 'cLastSearchString' stores the last string searched until user again hits the search */
   cLastSearchString  = fSearch:input-value.
   run filterData in this-procedure.    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btview
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btview C-Win
ON CHOOSE OF btview IN FRAME DEFAULT-FRAME /* View */
DO:
  run showSysNote in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgent C-Win
ON VALUE-CHANGED OF cbAgent IN FRAME DEFAULT-FRAME /* Entity ID */
DO:
  resultsChanged(false). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAttorney
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAttorney C-Win
ON VALUE-CHANGED OF cbAttorney IN FRAME DEFAULT-FRAME /* Entity ID */
DO:
  resultsChanged(false). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbEntity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbEntity C-Win
ON VALUE-CHANGED OF cbEntity IN FRAME DEFAULT-FRAME /* Entity */
DO:
   resultsChanged(false).                                                                                            
   /* Procedure to populate sysnote entityId in cbEntityID cmbo-box */
   run getNoteEntityIds    in this-procedure.    
   run getEntityIdScrnValue in this-procedure.  
   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbEntityId
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbEntityId C-Win
ON VALUE-CHANGED OF cbEntityId IN FRAME DEFAULT-FRAME /* Entity ID */
DO:
   resultsChanged(false). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbPerson
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbPerson C-Win
ON VALUE-CHANGED OF cbPerson IN FRAME DEFAULT-FRAME /* Entity ID */
DO:
  resultsChanged(false). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbUID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbUID C-Win
ON VALUE-CHANGED OF cbUID IN FRAME DEFAULT-FRAME /* UID */
DO:
    resultsChanged(false).
    cbEntity:sensitive = true.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME DEFAULT-FRAME
DO: 
   apply 'choose':U to btSearch.
   return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/brw-main.i}
{lib/win-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
assign current-window                = {&window-name} 
       this-procedure:current-window = {&window-name}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
on close of this-procedure 
  run disable_UI.


setStatusMessage("").

/* Best default for GUI applications is...                              */
pause 0 before-hide.

btexport    :load-image            ("images/excel.bmp").
btexport    :load-image-insensitive("images/excel-i.bmp").

btrefresh   :load-image            ("images/refresh.bmp").
btrefresh   :load-image-insensitive("images/refresh-i.bmp").

btget       :load-image            ("images/completed.bmp").
btget       :load-image-insensitive("images/completed-i.bmp").

btsearch    :load-image("images/magnifier.bmp").
btsearch    :load-image-insensitive("images/magnifier-i.bmp").

btview      :load-image            ("images/open.bmp").
btview      :load-image-insensitive("images/open-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
    
  run enable_UI.
      
  std-lo = not can-find(first sysnote ).

  /* create the agent combo */
  {lib/get-agent-list.i &combo=cbAgent &addAll=true &setEnable=std-lo}

  /* create the person combo */
  {lib/get-person-list.i &combo=cbPerson &addAll=true &setEnable=std-lo}

  /* create the attorney combo */
  {lib/get-attorney-list.i &combo=cbAttorney &addAll=true &setEnable=std-lo}
   
  /* get-agent-list.i,get-person-list.i and get-attorney-list.i defines the "on entry anywhere" trigger, the later 
  definition overrides the initial definition. So we have to redefine the "on entry anywhere" trigger
  including code for all widgets of agent, person and attorney lists and fill-ins.  */      
  on entry anywhere
  do:
    hSelectionAgent    = GetWidgetByName(cbAgent   :frame in frame {&frame-name}, "cbAgentAgentSelection").
    hTextAgent         = GetWidgetByName(cbAgent   :frame in frame {&frame-name}, "cbAgentAgentText").
    hSelectionPerson   = GetWidgetByName(cbPerson   :frame in frame {&frame-name}, "cbPersonPersonSelection").
    hTextPerson        = GetWidgetByName(cbPerson   :frame in frame {&frame-name}, "cbPersonPersonText").
    hSelectionAttorney = GetWidgetByName(cbAttorney :frame in frame {&frame-name}, "cbAttorneyAttorneySelection").
    hTextAttorney      = GetWidgetByName(cbAttorney :frame in frame {&frame-name}, "cbAttorneyAttorneyText").
    
    if valid-handle(hSelectionAgent) and valid-handle(hTextAgent)
     then
      if not self:name = "cbAgentAgentText" and not self:name = "cbAgentAgentSelection"
       then hSelectionAgent:visible = false.
      else if hSelectionAgent:list-item-pairs > "" and not hSelectionAgent:visible
       then hSelectionAgent:visible = true.
    
      
    if valid-handle(hSelectionPerson) and valid-handle(hTextPerson)
     then
      if not self:name = "cbPersonPersonText" and not self:name = "cbPersonPersonSelection"
       then hSelectionPerson:visible = false.
      else if hSelectionPerson:list-item-pairs > "" and not hSelectionPerson:visible
       then hSelectionPerson:visible = true.
      
    
    if valid-handle(hSelectionAttorney) and valid-handle(hTextAttorney)
     then
      if not self:name = "cbAttorneyAttorneyText" and not self:name = "cbAttorneyAttorneySelection"
       then hSelectionAttorney:visible = false.
      else if hSelectionAttorney:list-item-pairs > "" and not hSelectionAttorney:visible
       then hSelectionAttorney:visible = true.
  end.
  
  /* Set the default values. */
  cbAgent:screen-value = "".
  cbEntity:screen-value = {&agent}.
    
  /* Populate combo-box cbUID with sysuser */
  run getSysUserList      in this-procedure. 
  run getNoteEntityIds    in this-procedure.

  run setWidgetsState     in this-procedure.
  
  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.
  

  if not this-procedure:persistent then
    wait-for close of this-procedure.

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteSysNote C-Win 
PROCEDURE deleteSysNote :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name} :
  end.
 
  if not available sysnote
   then 
    return.
 
  message "System note will be permanently deleted. Do you want to continue?"
       view-as alert-box question buttons yes-no update lchoice as logical.

  if not lchoice
   then
    return.

  run server/deletesysnote.p(input sysnote.sysNoteID,
                             output std-lo,
                             output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.

  run getData in this-procedure.
  setStatusCount(query brwData:num-results).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbUID cbEntity fSearch 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE btrefresh cbUID cbEntity brwData RECT-64 RECT-69 RECT-71 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:                                                                                                                                                                                                                                      
  end.                                                                                                                                                                                                                                                              
                                                                                                                                                                                                                                                                    
  if query brwData:num-results = 0                                                                                                                                                                                                                                  
   then                                                                                                                                                                                                                                                             
    do:                                                                                                                                                                                                                                                             
      message "There is nothing to export"                                                                                                                                                                                                                          
          view-as alert-box warning buttons ok.                                                                                                                                                                                                                     
      return.                                                                                                                                                                                                                                                       
    end.                                                                                                                                                                                                                                                            
                                                                                                                                                                                                                                                                    
  publish "GetReportDir" (output std-ch).                                                                                                                                                                                                                           
                                                                                                                                                                                                                                                        
  std-ha = temp-table sysnote:handle.                                                                                                                                                                                                                     
  run util/exporttable.p (table-handle std-ha,                                                                                                                                                                                                                      
                          "sysnote",                                                                                                                                                                                                                      
                          "for each sysnote ",       
                          "sysNoteID,UID,entity,entityID,entityName,notes,dateCreated,dateModified",                                                                                                                                                                                                           
                          "Sys Note ID,UID,Entity,Entity ID,Entity Name,Notes,Created Date,Modified Date",  
                          std-ch,                                                                                                                                                                                                                                   
                          "SystemNote-" + replace(string(now,"99-99-99"),"-","") + "-" + replace(string(time,"HH:MM:SS"),":","") + ".csv",                                                                                                                           
                          true,                                                                                                                                                                                                                                     
                          output std-ch,                                                                                                                                                                                                                            
                          output std-in).   
  if std-ch <> ""
   then
    do:
      message std-ch
          view-as alert-box warning buttons ok.
      return.
    end.                        
                                                                                                                                                                                                                                                          

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwData.
  empty temp-table sysnote.
  
  define variable cEntityName as character no-undo.
  
  do with frame {&frame-name}:
  end.
  
  for each ttsysnote:
    publish "getEntityName" (input ttsysnote.entity,
                             input ttsysnote.entityId,
                             output cEntityName).
      
    ttsysnote.entityName = cEntityName.
    
    /* test if the record contains the search text */
    if cLastSearchString <> "" and 
      not ((ttsysnote.notes      matches "*" + cLastSearchString + "*") or
          (ttsysnote.UID         matches "*" + cLastSearchString + "*") or
          (ttsysnote.entityId    matches "*" + cLastSearchString + "*") or
          (ttsysnote.entityName  matches "*" + cLastSearchString + "*"))
     then next.
   
    create sysnote.
    buffer-copy ttsysnote to sysnote.
    
    
  end.
  
  open query brwData preselect each sysnote.   

  setStatusCount(query brwData:num-results).  
    
  run setWidgetsState in this-procedure.
  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/   
  do with frame {&frame-name}:
  end.
  define variable entity as character no-undo.
  
  empty temp-table ttsysnote.
  run server/getsysnote.p(input cbUID:input-value,       /* UID */
                          input cbEntity:input-value,    /* Entity */
                          input getEntityId(cbEntity:input-value),   /* EntityID */
                          output table ttsysnote,
                          output std-lo,
                          output std-ch).

  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.

  /* This will use the screen-value of the filter search box */
  run filterData in this-procedure.
  
  run setWidgetsState     in this-procedure.  
  setStatusRecords(query brwData:num-results). 
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getEntityIdScrnValue C-Win 
PROCEDURE getEntityIdScrnValue :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.  
  
  if cbEntity:input-value <> ""
   then
    assign
       hTextAgent:screen-value = ""
       hTextAttorney:screen-value = ""
       hTextPerson:screen-value = ""
       cbEntityId:screen-value= ""
       .
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getNoteEntityIds C-Win 
PROCEDURE getNoteEntityIds :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* Hide the combo-boxes. Depending on the 
   value of cbEntity combo-box, the label and 
   combo-boxes will be shown. */
  assign                                  
      cbEntityId:visible   = false
      cbEntityId:sensitive = false
/*       cbEntityId:label     = "Entity ID" */
/*       cbAgent:label        = ""          */
/*       cbPerson:label       = ""          */
      .   
  run agentComboHide(true).
  run agentComboEnable(false).
  run AttorneyComboHide(true).
  run AttorneyComboEnable(false).
  run PersonComboHide(true).
  run PersonComboEnable(false).
  
/* Populating for Agent */
 if cbEntity:input-value = {&Agent}
  then
   do:
     assign
         cbEntityId:visible      =  false
         cbEntityId:sensitive    =  false
         btget      :sensitive   =  true
         cbEntityId:label        = ""
         cbAgent:label           = "Agent ID"    
         cbPerson:label          = ""
         cbAttorney:label        = ""
         .
     run AgentComboHide(false).
     run AgentComboEnable(true).
     run PersonComboHide(true).   
     run PersonComboEnable(false). 
     run AttorneyComboHide(true).  
     run AttorneyComboEnable(false).
   end.

/* Populating for Person */
 else if cbEntity:input-value = {&person}
  then
   do:
     assign
         cbEntityId:visible      =  false
         cbEntityId:sensitive    =  false
         btget      :sensitive   =  true
         cbEntityId:label        = ""
         cbAgent:label           = ""    
         cbPerson:label          = "Person ID"
         cbAttorney:label        = ""
         .
     run AgentComboHide(true).
     run AgentComboEnable(false).
     run PersonComboHide(false).   
     run PersonComboEnable(true). 
     run AttorneyComboHide(true).  
     run AttorneyComboEnable(false).
   end.

 /* Populating for Attorney */
  else if cbEntity:input-value = {&Attorney}
   then
    do:
      assign
          cbEntityId:visible      = false
          cbEntityId:sensitive    = false
          btget     :sensitive    = true 
          cbEntityId:label        = ""
          cbAgent:label           = ""    
          cbPerson:label          = ""
          cbAttorney:label        = "Attorney ID"
          .
      run AgentComboHide(true).
      run AgentComboEnable(false).          
      run PersonComboHide(true).
      run PersonComboEnable(false).
      run AttorneyComboHide(false).
      run AttorneyComboEnable(true).
    end.

  /* Populating for Employee */
   else if cbEntity:input-value = {&Employee}
    then
     do:
       assign
           cbEntityId:visible     = true
           cbEntityId:sensitive   = true
           btget     :sensitive   = true
           cbEntityId:label      = "Emp ID"
           cbAgent:label         =  ""    
           cbPerson:label        = ""
           cbAttorney:label        = ""
           .
       run AgentComboHide(true).
       run AgentComboEnable(false).           
       run AttorneyComboHide(true).
       run AttorneyComboEnable(false).
       run PersonComboHide(true).
       run PersonComboEnable(false).
       cbEntityId:move-to-top().
     end.

  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getSysUserList C-Win 
PROCEDURE getSysUserList :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define variable cUserList  as character  no-undo.

  /* return list of sys user*/
  publish "getSysUserList" (",",
                            output cUserList,
                            output std-lo,
                            output std-ch ).
  if not std-lo
   then
    do:
      message std-ch
           view-as alert-box error buttons ok.
      return.
    end.
 
  assign
      cbUID:list-items        = "ALL" + "," + trim(cUserList,",")
      cbUID:screen-value      = "ALL"
      cbEntityId:list-items   = "ALL" + "," + trim(cUserList,",")
      cbEntityId:screen-value = "ALL"
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetsState C-Win 
PROCEDURE setWidgetsState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if query brwData:num-results >0
   then
    assign
        btexport:sensitive = true
        btview:sensitive   = true
        fSearch:sensitive  = true
        btSearch:sensitive  = true
        . 
   else
    assign  
      btexport:sensitive   = false
      btview:sensitive     = false
     .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showSysNote C-Win 
PROCEDURE showSysNote :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tSysNote for tSysNote.

  empty temp-table tSysNote.

  if not available sysnote
   then
    return.

  create tSysNote.
  buffer-copy sysnote to tSysNote.

  run dialogviewsysnote.w (input table tSysNote).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized
   then
    {&window-name}:window-state = window-normal .
  
  c-Win:move-to-top(). 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
    frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
    frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
    frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
    frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels

    /* {&frame-name} Components */
    brwData:width-pixels              = frame {&frame-name}:width-pixels  - 11
    brwData:height-pixels             = frame {&frame-name}:height-pixels - {&browse-name}:y - 11
    .



  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getEntityId C-Win 
FUNCTION getEntityId RETURNS CHARACTER
  (cEntity as character /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
 
 if cEntity = {&Person}
  then
   do:
     if cbPerson:input-value = ""
      then 
        return hTextPerson:input-value. /* Function return value. */
     else
      return cbPerson:input-value.
   end.   
      
 else if cEntity = {&Agent}
  then
   do:
     if cbAgent:input-value = ""
      then
        return hTextAgent:input-value. /* Function return value. */
     else
      return cbAgent:input-value.       
  end.
      
 else if cEntity = {&Attorney}
  then
    do:
     if cbAttorney:input-value = ""
      then
        return hTextAttorney:input-value. /* Function return value. */
     else
      return cbAttorney:input-value.       
    end.  
    
 else if cEntity = {&Employee}
  then
   return cbEntityId:input-value.   /* Function return value. */
 
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage("Results may not match current parameters.").
  return true.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

