&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File:wobjective.w 

  Description: UI to get Objectives

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Sachin Chaturvedi

  Created: 01/27/2020

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

  create widget-pool.

  /* ***************************  Definitions  ************************** */
  {lib/std-def.i}
  {lib/sys-def.i}
  {lib/get-column.i}
  {lib\winlaunch.i}
  {lib/winshowscrollbars.i}
  {lib/brw-multi-def.i}
  /* Temp table definitions */
  {tt/objective.i}
  {tt/sentiment.i}
  {tt/objective.i &tableAlias="ttObjective"}
  {tt/objective.i &tableAlias="tObjective"}
  {tt/objectivesentiment.i}
  {tt/person.i}
  {tt/agent.i}
 
  /* Variable Definition */
  define variable hTextPerson         as handle    no-undo.
  define variable hSelectionPerson    as handle    no-undo.
  define variable hSelectionAgent     as handle    no-undo.
  define variable hTextAgent          as handle    no-undo.
  define variable cLastSearchString   as character no-undo.
  /* for resize*/
  define variable drsH           as decimal  no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES objective sentiment

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData updateEntity(objective.entity) @ objective.entity "Entity" objective.entityID "ID" objective.entityName "Name" objective.UID "UID" /* updateStatus(objective.stat) @ objective.stat */ objective.description "Description" objective.dueDate "Due" /* objective.private */   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each objective
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each objective.
&Scoped-define TABLES-IN-QUERY-brwData objective
&Scoped-define FIRST-TABLE-IN-QUERY-brwData objective


/* Definitions for BROWSE brwSentiment                                  */
&Scoped-define FIELDS-IN-QUERY-brwSentiment sentiment.type "Type" sentiment.UID "UID" sentiment.notes "Description" sentiment.createDate "Due"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwSentiment   
&Scoped-define SELF-NAME brwSentiment
&Scoped-define QUERY-STRING-brwSentiment for each sentiment
&Scoped-define OPEN-QUERY-brwSentiment open query {&SELF-NAME} for each sentiment.
&Scoped-define TABLES-IN-QUERY-brwSentiment sentiment
&Scoped-define FIRST-TABLE-IN-QUERY-brwSentiment sentiment


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwData}~
    ~{&OPEN-QUERY-brwSentiment}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fText cbEntity btrefresh cbStatus tType ~
brwData RECT-64 RECT-69 RECT-71 brwSentiment 
&Scoped-Define DISPLAYED-OBJECTS fText cbEntity cbStatus tType fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getEntityId C-Win 
FUNCTION getEntityId RETURNS CHARACTER
  (cEntity as character /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD updateEntity C-Win 
FUNCTION updateEntity RETURNS CHARACTER
  ( cEntity as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD updateStatus C-Win 
FUNCTION updateStatus RETURNS CHARACTER
  ( cStatus as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btexport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON btget 
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get Data".

DEFINE BUTTON btrefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload Data".

DEFINE BUTTON btsearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search Data".

DEFINE BUTTON btview  NO-FOCUS
     LABEL "View" 
     SIZE 7.2 BY 1.71 TOOLTIP "View".

DEFINE VARIABLE cbAgent AS CHARACTER FORMAT "X(256)":U INITIAL "All" 
     LABEL "Entity ID" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 42.4 BY 1 NO-UNDO.

DEFINE VARIABLE cbEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 5
     LIST-ITEM-PAIRS "Agent","A",
                     "Person","P"
     DROP-DOWN-LIST
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE cbEntityId AS CHARACTER 
     LABEL "Entity ID" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEMS "Item 1" 
     DROP-DOWN AUTO-COMPLETION
     SIZE 42.4 BY 1 NO-UNDO.

DEFINE VARIABLE cbPerson AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity ID" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 42.4 BY 1 NO-UNDO.

DEFINE VARIABLE cbStatus AS CHARACTER FORMAT "X(256)":U INITIAL "A" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Active","A",
                     "Closed","C",
                     "Cancelled","X"
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 33.2 BY 1 NO-UNDO.

DEFINE VARIABLE fText AS CHARACTER FORMAT "X(256)":U INITIAL "Type:" 
      VIEW-AS TEXT 
     SIZE 5.8 BY .62 NO-UNDO.

DEFINE VARIABLE tType AS CHARACTER INITIAL "T" 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Our", "O",
"Their", "T"
     SIZE 9.8 BY 2.38 NO-UNDO.

DEFINE RECTANGLE RECT-64
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 24.2 BY 3.38.

DEFINE RECTANGLE RECT-69
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 86.2 BY 3.38.

DEFINE RECTANGLE RECT-71
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 43.2 BY 3.38.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      objective SCROLLING.

DEFINE QUERY brwSentiment FOR 
      sentiment SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      updateEntity(objective.entity) @ objective.entity   label      "Entity"     format "x(50)" width 10     
 objective.entityID        label        "ID"          format "x(50)" width 10
 objective.entityName      label        "Name"        format "x(50)" width 30
 objective.UID             label        "UID"         format "x(100)" width 32
/*  updateStatus(objective.stat) @ objective.stat  label "Status" format "x(20)" width 15 */
 objective.description     label        "Description" format "x(60)" width 37
 objective.dueDate         label        "Due"         width 25
/*  objective.private         column-label "Is Private":C  view-as toggle-box */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 152.8 BY 10.81
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Objectives" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwSentiment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwSentiment C-Win _FREEFORM
  QUERY brwSentiment DISPLAY
      sentiment.type        label        "Type"        format "->9" width 20
 sentiment.UID         label        "UID"         format "x(100)" width 40
 sentiment.notes       label        "Description" format "x(60)" width 50
 sentiment.createDate  label        "Due"         width 25
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 152.8 BY 7.81
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Sentiments" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     fText AT ROW 2.24 COL 82.6 COLON-ALIGNED NO-LABEL WIDGET-ID 342
     btexport AT ROW 2.29 COL 3 WIDGET-ID 290 NO-TAB-STOP 
     cbEntity AT ROW 2 COL 36.8 COLON-ALIGNED WIDGET-ID 310
     btrefresh AT ROW 2.29 COL 10.6 WIDGET-ID 288 NO-TAB-STOP 
     cbEntityId AT ROW 3.29 COL 36.8 COLON-ALIGNED WIDGET-ID 308
     cbPerson AT ROW 3.29 COL 36.8 COLON-ALIGNED WIDGET-ID 326
     btsearch AT ROW 2.29 COL 146.6 WIDGET-ID 320 NO-TAB-STOP 
     btview AT ROW 2.29 COL 18.2 WIDGET-ID 300 NO-TAB-STOP 
     cbAgent AT ROW 3.29 COL 36.8 COLON-ALIGNED WIDGET-ID 330
     cbStatus AT ROW 2 COL 63.2 COLON-ALIGNED WIDGET-ID 338
     tType AT ROW 1.91 COL 91.2 NO-LABEL WIDGET-ID 332
     fSearch AT ROW 2.71 COL 110.8 COLON-ALIGNED NO-LABEL WIDGET-ID 314
     brwData AT ROW 5.29 COL 2.2 WIDGET-ID 200
     btget AT ROW 2.29 COL 103.8 WIDGET-ID 312 NO-TAB-STOP 
     brwSentiment AT ROW 16.57 COL 2.2 WIDGET-ID 300
     "Actions" VIEW-AS TEXT
          SIZE 7.2 BY .62 AT ROW 1.1 COL 3.2 WIDGET-ID 286
     "Search" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.1 COL 112.8 WIDGET-ID 322
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.1 COL 27 WIDGET-ID 304
     RECT-64 AT ROW 1.48 COL 2.2 WIDGET-ID 284
     RECT-69 AT ROW 1.48 COL 26 WIDGET-ID 302
     RECT-71 AT ROW 1.48 COL 111.8 WIDGET-ID 318
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 155.2 BY 23.9 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Objectives"
         HEIGHT             = 23.86
         WIDTH              = 155.2
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData fSearch DEFAULT-FRAME */
/* BROWSE-TAB brwSentiment RECT-71 DEFAULT-FRAME */
ASSIGN 
       FRAME DEFAULT-FRAME:RESIZABLE        = TRUE.

ASSIGN 
       brwData:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwData:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

ASSIGN 
       brwSentiment:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwSentiment:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

/* SETTINGS FOR BUTTON btexport IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btget IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btsearch IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btview IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbAgent IN FRAME DEFAULT-FRAME
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR COMBO-BOX cbEntityId IN FRAME DEFAULT-FRAME
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR COMBO-BOX cbPerson IN FRAME DEFAULT-FRAME
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN fSearch IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each objective.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwSentiment
/* Query rebuild information for BROWSE brwSentiment
     _START_FREEFORM
open query {&SELF-NAME} for each sentiment.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwSentiment */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Objectives */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the note presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Objectives */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Objectives */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME DEFAULT-FRAME /* Objectives */
do:
  apply 'choose':U to btView.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME DEFAULT-FRAME /* Objectives */
do:
  {lib/brw-rowDisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME DEFAULT-FRAME /* Objectives */
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME DEFAULT-FRAME /* Objectives */
DO:
  find current objective no-error.
      
  close query brwSentiment.
  open  query brwSentiment for each sentiment where sentiment.objectiveID = objective.objectiveID.
    
  if can-find(first sentiment)
   then
    brwSentiment:set-repositioned-row(1,"ALWAYS").
   else
    brwSentiment:sensitive = false.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwSentiment
&Scoped-define SELF-NAME brwSentiment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSentiment C-Win
ON ROW-DISPLAY OF brwSentiment IN FRAME DEFAULT-FRAME /* Sentiments */
do:
  {lib/brw-rowDisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSentiment C-Win
ON START-SEARCH OF brwSentiment IN FRAME DEFAULT-FRAME /* Sentiments */
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btexport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btexport C-Win
ON CHOOSE OF btexport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btget
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btget C-Win
ON CHOOSE OF btget IN FRAME DEFAULT-FRAME /* Get */
DO:
   run getData in this-procedure.
   fSearch:screen-value = cLastSearchString.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btrefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btrefresh C-Win
ON CHOOSE OF btrefresh IN FRAME DEFAULT-FRAME /* Refresh */
DO:
  run getData in this-procedure.
  fSearch:screen-value = cLastSearchString.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btsearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btsearch C-Win
ON CHOOSE OF btsearch IN FRAME DEFAULT-FRAME /* Search */
DO:
   /* if search button is clicked or return key is hit, 
     then 'cLastSearchString' stores the last string searched until user again hits the search */
   cLastSearchString  = fSearch:input-value.
   run filterData in this-procedure.    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btview
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btview C-Win
ON CHOOSE OF btview IN FRAME DEFAULT-FRAME /* View */
DO:
  run showObjective in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgent C-Win
ON VALUE-CHANGED OF cbAgent IN FRAME DEFAULT-FRAME /* Entity ID */
DO:
  resultsChanged(false). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbEntity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbEntity C-Win
ON VALUE-CHANGED OF cbEntity IN FRAME DEFAULT-FRAME /* Entity */
DO:
   resultsChanged(false).                                                                                            
  /* Procedure to populate entityId in cbEntityID cmbo-box */
   run setEntityCombos    in this-procedure.    
   run getEntityIdScrnValue in this-procedure.  
   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbEntityId
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbEntityId C-Win
ON VALUE-CHANGED OF cbEntityId IN FRAME DEFAULT-FRAME /* Entity ID */
DO:
   resultsChanged(false). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbPerson
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbPerson C-Win
ON VALUE-CHANGED OF cbPerson IN FRAME DEFAULT-FRAME /* Entity ID */
DO:
  resultsChanged(false). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME DEFAULT-FRAME
DO: 
   apply 'choose':U to btSearch.
   return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/brw-main-multi.i &browse-list="brwData,brwSentiment"}
{lib/win-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
assign current-window                = {&window-name} 
       this-procedure:current-window = {&window-name}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
on close of this-procedure 
  run disable_UI.


setStatusMessage("").

/* Best default for GUI applications is...                              */
pause 0 before-hide.

btexport    :load-image            ("images/excel.bmp").
btexport    :load-image-insensitive("images/excel-i.bmp").

btrefresh   :load-image            ("images/refresh.bmp").
btrefresh   :load-image-insensitive("images/refresh-i.bmp").

btget       :load-image            ("images/completed.bmp").
btget       :load-image-insensitive("images/completed-i.bmp").

btsearch    :load-image("images/magnifier.bmp").
btsearch    :load-image-insensitive("images/magnifier-i.bmp").

btview      :load-image            ("images/open.bmp").
btview      :load-image-insensitive("images/open-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
    
  run enable_UI.
      
  std-lo = not can-find(first objective ).

  /* create the agent combo */
  {lib/get-agent-list.i &combo=cbAgent &addAll=true &setEnable=std-lo}

  /* create the person combo */
  {lib/get-person-list.i &combo=cbPerson &addAll=true &setEnable=std-lo}
   
  /* get-agent-list.i,get-person-list.i and get-attorney-list.i defines the "on entry anywhere" trigger, the later 
  definition overrides the initial definition. So we have to redefine the "on entry anywhere" trigger
  including code for all widgets of agent, person and attorney lists and fill-ins.  */      
  on entry anywhere
  do:
    hSelectionAgent    = GetWidgetByName(cbAgent   :frame in frame {&frame-name}, "cbAgentAgentSelection").
    hTextAgent         = GetWidgetByName(cbAgent   :frame in frame {&frame-name}, "cbAgentAgentText").
    hSelectionPerson   = GetWidgetByName(cbPerson   :frame in frame {&frame-name}, "cbPersonPersonSelection").
    hTextPerson        = GetWidgetByName(cbPerson   :frame in frame {&frame-name}, "cbPersonPersonText").
    
    if valid-handle(hSelectionAgent) and valid-handle(hTextAgent)
     then
      if not self:name = "cbAgentAgentText" and not self:name = "cbAgentAgentSelection"
       then hSelectionAgent:visible = false.
      else if hSelectionAgent:list-item-pairs > "" and not hSelectionAgent:visible
       then hSelectionAgent:visible = true.
    
      
    if valid-handle(hSelectionPerson) and valid-handle(hTextPerson)
     then
      if not self:name = "cbPersonPersonText" and not self:name = "cbPersonPersonSelection"
       then hSelectionPerson:visible = false.
      else if hSelectionPerson:list-item-pairs > "" and not hSelectionPerson:visible
       then hSelectionPerson:visible = true.

  end.
  
  /* Set the default values. */
  cbAgent:screen-value = "".
  cbEntity:screen-value = {&agent}.
    
  /* Populate combo-box cbUID with sysuser */
  run setEntityCombos    in this-procedure.
  run setWidgetsState     in this-procedure.
  
  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.
  
  /* Store dimensions of various components for resizing. */
  drsH  = brwData:height.
  if not this-procedure:persistent then
    wait-for close of this-procedure.

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fText cbEntity cbStatus tType fSearch 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE fText cbEntity btrefresh cbStatus tType brwData RECT-64 RECT-69 
         RECT-71 brwSentiment 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:                                                                                                                                                                                                                                      
  end.                                                                                                                                                                                                                                                              
                                                                                                                                                                                                                                                                    
  if query brwData:num-results = 0                                                                                                                                                                                                                                  
   then                                                                                                                                                                                                                                                             
    do:                                                                                                                                                                                                                                                             
      message "There is nothing to export"                                                                                                                                                                                                                          
          view-as alert-box warning buttons ok.                                                                                                                                                                                                                     
      return.                                                                                                                                                                                                                                                       
    end.                                                                                                                                                                                                                                                            
                                                                                                                                                                                                                                                               
  publish "GetReportDir" (output std-ch).                                                                                                                                                                                                                           

  empty temp-table objectivesentiment.
  
  for each objective:
    if not can-find(first sentiment where sentiment.objectiveID = objective.objectiveID) 
     then
      do:
        create objectivesentiment.
        assign 
            objectivesentiment.objectiveID    = objective.objectiveID
            objectivesentiment.entity         = objective.entity
            objectivesentiment.entityID       = objective.entityID
            objectivesentiment.description    = objective.description
            objectivesentiment.dueDate        = objective.dueDate
            objectivesentiment.objType        = objective.type
            .
      end.
    
     else do:
       for each sentiment where sentiment.objectiveID = objective.objectiveID:
         create objectivesentiment.
         assign
             objectivesentiment.objectiveID    = objective.objectiveID
             objectivesentiment.entity         = objective.entity
             objectivesentiment.entityID       = objective.entityID
             objectivesentiment.description    = objective.description
             objectivesentiment.dueDate        = objective.dueDate
             objectivesentiment.objType        = objective.type
             objectivesentiment.sentimentID    = sentiment.sentimentID
             objectivesentiment.sentimentType  = sentiment.type
             objectivesentiment.createDate     = sentiment.createDate
             objectivesentiment.notes          = sentiment.notes
             objectivesentiment.uid            = sentiment.uid
             .
       end.
     end.
    
  end.

  std-ha = temp-table objectivesentiment:handle.                                                                                                                                                                                                                     
  run util/exporttable.p (table-handle std-ha,                                                                                                                                                                                                                      
                          "objectivesentiment",                                                                                                                                                                                                                      
                          "for each objectivesentiment ",       
                          "objectiveID,sentimentID,entity,entityID,description,dueDate,objType,sentimentType,createDate,notes,UID",                                                                                                                                                                                                           
                          "Objective ID,Sentiment ID,Entity,Entity ID,Description,Obj Due,Obj Type,Sentiment Type,create,Notes,UID",  
                          std-ch,                                                                                                                                                                                                                                   
                          "Objectivesentiment" + replace(string(now,"99-99-99"),"-","") + "-" + replace(string(time,"HH:MM:SS"),":","") + ".csv",                                                                                                                           
                          true,                                                                                                                                                                                                                                     
                          output std-ch,                                                                                                                                                                                                                            
                          output std-in).   
  if std-ch <> ""
   then
    do:
      message std-ch
          view-as alert-box warning buttons ok.
      return.
    end.  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwData.
  empty temp-table Objective.
  
  define variable cEntityName as character no-undo.
  
  do with frame {&frame-name}:
  end.
  
  for each ttObjective:
    publish "getEntityName" (input ttObjective.entity,
                             input ttObjective.entityId,
                             output cEntityName).
      
    ttObjective.entityName = cEntityName.

    /* test if the record contains the search text */
    if cLastSearchString <> "" and 
      not ((ttObjective.entity          matches "*" + cLastSearchString + "*") or
           (ttObjective.entityId        matches "*" + cLastSearchString + "*") or
           (ttObjective.entityName      matches "*" + cLastSearchString + "*") or
           (ttObjective.UID             matches "*" + cLastSearchString + "*") or
           (ttObjective.stat            matches "*" + cLastSearchString + "*") or
           (ttObjective.type            matches "*" + cLastSearchString + "*") or
           (string(ttObjective.dueDate) matches "*" + cLastSearchString + "*") or
           (ttObjective.description     matches "*" + cLastSearchString + "*")
          )
     then next.
   
    create objective.
    buffer-copy ttObjective to objective.    
  end.
  
  open query brwData for each objective.

  setStatusCount(query brwData:num-results).  
    
  run setWidgetsState in this-procedure.
  apply 'value-changed':U to brwData.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/   
  do with frame {&frame-name}:
  end.
  define variable entity as character no-undo.
  
  empty temp-table ttObjective.

  run server/getObjectives.p(input cbEntity:input-value,    /* Entity */
                             input getEntityId(cbEntity:input-value),   /* EntityID */
                             input cbStatus:input-value, /* (A)ctive/(Closed)/(X)Cancelled */
                             input tType:input-value,  /* (O)ur/(T)heir */ 
                             output table ttObjective,
                             output table sentiment,
                             output std-lo,
                             output std-ch).

  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.

  /* This will use the screen-value of the filter search box */
  run filterData in this-procedure.
  
  run setWidgetsState     in this-procedure.  
  setStatusRecords(query brwData:num-results). 
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getEntityIdScrnValue C-Win 
PROCEDURE getEntityIdScrnValue :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.  
  
  if cbEntity:input-value <> ""
   then
    assign
       hTextAgent:screen-value  = ""
       hTextPerson:screen-value = ""
       cbEntityId:screen-value  = ""
       .
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setEntityCombos C-Win 
PROCEDURE setEntityCombos :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* Depending on the 
   value of cbEntity combo-box, the label and 
   combo-boxes will be shown. */
  
/* Populating for Agent */
 if cbEntity:input-value = {&Agent}
  then
   do:
     assign
         cbEntityId:visible      =  false
         cbEntityId:sensitive    =  false
         btget      :sensitive   =  true 
         tType:sensitive         =  true
         cbEntityId:label        = ""
         cbAgent:label           = "Agent ID"    
         cbPerson:label          = ""
         .
         
     run AgentComboHide(false).
     run AgentComboEnable(true).
     run PersonComboHide(true).   
     run PersonComboEnable(false).
     
     run PersonComboClear in this-procedure.
   end.

/* Populating for Person */
 else if cbEntity:input-value = {&person}
  then
   do:
     assign
         cbEntityId:visible      =  false
         cbEntityId:sensitive    =  false
         btget      :sensitive   =  true
         tType:sensitive         =  false
         tType:screen-value      =  "T"     /* Only (T)heir for Person Objective*/
         cbEntityId:label        = "" 
         cbAgent:label           = ""   
         cbPerson:label          = "Person ID" 
         .

     run AgentComboHide(true).
     run AgentComboEnable(false).
     run PersonComboHide(false).   
     run PersonComboEnable(true).
     
     run AgentComboClear in this-procedure.
   end.

  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetsState C-Win 
PROCEDURE setWidgetsState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if query brwData:num-results >0
   then
    assign
        btexport:sensitive = true
        btview:sensitive   = true
        fSearch:sensitive  = true
        btSearch:sensitive  = true
        . 
   else
    assign  
      btexport:sensitive   = false
      btview:sensitive     = false
     .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showObjective C-Win 
PROCEDURE showObjective :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tObjective for tObjective.

  empty temp-table tObjective.

  if not available Objective
   then
    return.

  create tObjective.
  buffer-copy Objective to tObjective.

  run dialogviewObjective.w (input table tObjective).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized
   then
    {&window-name}:window-state = window-normal .
  
  c-Win:move-to-top(). 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
    frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
    frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
    frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
    frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels

    /* {&frame-name} Components */
    brwData:width-pixels        = frame {&frame-name}:width-pixels  - 11
    brwSentiment:width-pixels   = frame {&frame-name}:width-pixels  - 11
    brwData:height              = drsH
    brwSentiment:height         = c-win:height - (drsH + 5).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getEntityId C-Win 
FUNCTION getEntityId RETURNS CHARACTER
  (cEntity as character /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
 
 if cEntity = {&Person}
  then
   do:
     if cbPerson:input-value = ""
      then 
        return hTextPerson:input-value. /* Function return value. */
     else
      return cbPerson:input-value.
   end.   
      
  else  
   do:
     if cbAgent:input-value = ""
      then
        return hTextAgent:input-value. /* Function return value. */
     else
      return cbAgent:input-value.       
   end.
 
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage("Results may not match current parameters.").
  return true.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION updateEntity C-Win 
FUNCTION updateEntity RETURNS CHARACTER
  ( cEntity as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
 
 if cEntity = {&Agent}
   then 
    return "Agent".
   else if cEntity = {&Person}
    then 
     return "Person".
   else if cEntity <> "" or cEntity = "" 
    then
     return "".  

  return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION updateStatus C-Win 
FUNCTION updateStatus RETURNS CHARACTER
  ( cStatus as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cStatus = {&Active}
   then 
    return {&ActiveValue}.
   else if cStatus = {&Closed}
    then 
     return {&ClosedValue}.
   else if cStatus = {&Cancelled}
    then 
     return {&CancelledValue}.

  RETURN "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

