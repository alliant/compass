&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wSysprop.w

  Description: Window of system properties

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gurvindar

  Created: 09.25.2018

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
create widget-pool.

/* ***************************  Temp-Table Definitions  ************************** */
{tt/sysprop.i}                         /* temp-table having filtered data */
{tt/sysprop.i &tableAlias=tempSysProp} /* temp-table having all data */
{tt/sysprop.i &tableAlias=ttSysProp}   /* temp-table to send record for new or modify */

/*  Library files */
{lib/std-def.i}
{lib/sys-def.i}
{lib/get-column.i}
{lib/winshowscrollbars.i}

/*  Variable  definitions */
define variable cList              as character no-undo. 
define variable cLastSearchString  as character no-undo.
define variable lApplySearchString as logical   no-undo.
define variable dColumnWidth       as decimal   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwProp

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES sysProp

/* Definitions for BROWSE brwProp                                       */
&Scoped-define FIELDS-IN-QUERY-brwProp sysProp.appcode sysProp.objAction sysProp.objID sysProp.objProperty sysProp.objValue sysProp.objName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwProp   
&Scoped-define SELF-NAME brwProp
&Scoped-define QUERY-STRING-brwProp for each sysProp
&Scoped-define OPEN-QUERY-brwProp open query {&SELF-NAME} for each sysProp.
&Scoped-define TABLES-IN-QUERY-brwProp sysProp
&Scoped-define FIRST-TABLE-IN-QUERY-brwProp sysProp


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwProp}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bCopy RECT-66 RECT-62 bSearch cbappCode ~
fSearch brwProp bdelete bEdit bExport bNew bRefresh 
&Scoped-Define DISPLAYED-OBJECTS cbappCode fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ()  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCopy  NO-FOCUS
     LABEL "Copy" 
     SIZE 7.2 BY 1.71 TOOLTIP "Copy".

DEFINE BUTTON bdelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete".

DEFINE BUTTON bEdit  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Edit".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search".

DEFINE VARIABLE cbappCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Application Code" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 22.2 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 43.8 BY 1 TOOLTIP "Search Criteria (Action,Property,Name,ID,Value)" NO-UNDO.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 103.2 BY 2.19.

DEFINE RECTANGLE RECT-66
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 48.4 BY 2.19.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwProp FOR 
      sysProp SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwProp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwProp C-Win _FREEFORM
  QUERY brwProp DISPLAY
      sysProp.appcode   label "Application Code"     format "x(18)"   
 sysProp.objAction      label "Action"               format "x(20)"  
 sysProp.objID          label "ID"       width 30    format "x(45)"   
 sysProp.objProperty    label "Property" width 18    format "x(45)"
 sysProp.objValue       label "Value"    width 28    format "x(45)"   
 sysProp.objName        label "Name"     width 45    format "x(50)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 161.4 BY 18 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bCopy AT ROW 1.67 COL 42.6 WIDGET-ID 316 NO-TAB-STOP 
     bSearch AT ROW 1.67 COL 145.4 WIDGET-ID 308 NO-TAB-STOP 
     cbappCode AT ROW 2 COL 67.4 COLON-ALIGNED WIDGET-ID 64
     fSearch AT ROW 2 COL 98.8 COLON-ALIGNED WIDGET-ID 310
     brwProp AT ROW 4.24 COL 2.6 WIDGET-ID 300
     bdelete AT ROW 1.67 COL 34.8 WIDGET-ID 10 NO-TAB-STOP 
     bEdit AT ROW 1.67 COL 27 WIDGET-ID 8 NO-TAB-STOP 
     bExport AT ROW 1.67 COL 11.4 WIDGET-ID 2 NO-TAB-STOP 
     bNew AT ROW 1.67 COL 19.2 WIDGET-ID 6 NO-TAB-STOP 
     bRefresh AT ROW 1.67 COL 3.6 WIDGET-ID 4 NO-TAB-STOP 
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.05 COL 3.6 WIDGET-ID 60
     "Filter" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1.14 COL 51.6 WIDGET-ID 314
     RECT-66 AT ROW 1.43 COL 2.6 WIDGET-ID 58
     RECT-62 AT ROW 1.43 COL 50.6 WIDGET-ID 312
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 178.8 BY 24.1 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Properties"
         HEIGHT             = 21.24
         WIDTH              = 164.2
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwProp fSearch fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwProp:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwProp:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwProp
/* Query rebuild information for BROWSE brwProp
     _START_FREEFORM
open query {&SELF-NAME} for each sysProp.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwProp */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Properties */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Properties */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Properties */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCopy C-Win
ON CHOOSE OF bCopy IN FRAME fMain /* Copy */
do:
  run actionCopy in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bdelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bdelete C-Win
ON CHOOSE OF bdelete IN FRAME fMain /* Delete */
do:
  run actionDelete in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEdit C-Win
ON CHOOSE OF bEdit IN FRAME fMain /* Edit */
do:
  run actionModify in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
do:
  run actionNew in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
do:  
  run refreshData in this-procedure.
  fSearch:screen-value = cLastSearchString.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwProp
&Scoped-define SELF-NAME brwProp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwProp C-Win
ON DEFAULT-ACTION OF brwProp IN FRAME fMain
do:
  apply "choose" to bedit.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwProp C-Win
ON ROW-DISPLAY OF brwProp IN FRAME fMain
do:
  {lib/brw-rowDisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwProp C-Win
ON START-SEARCH OF brwProp IN FRAME fMain
do:
  {lib/brw-startSearch.i} 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwProp C-Win
ON VALUE-CHANGED OF brwProp IN FRAME fMain
DO:
  run setWidgetState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME fMain /* Search */
DO:
  /* if search button is clicked or return key is hit, 
     then 'cLastSearchString' stores the last string searched until user again hits the search */
  assign
      lApplySearchString = true  
      cLastSearchString  = fSearch:input-value
      .
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbappCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbappCode C-Win
ON VALUE-CHANGED OF cbappCode IN FRAME fMain /* Application Code */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fMain /* Search */
DO:
  apply "CHOOSE" to bSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON VALUE-CHANGED OF fSearch IN FRAME fMain /* Search */
DO:
  resultsChanged().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

on close of this-procedure 
  run disable_UI.

pause 0 before-hide.

bExport :load-image("images/excel.bmp").
bExport :load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/refresh.bmp").
bRefresh:load-image-insensitive("images/excel-i.bmp").
bnew    :load-image("images/add.bmp").
bnew    :load-image-insensitive("images/excel-i.bmp").
bEdit   :load-image("images/update.bmp").
bEdit   :load-image-insensitive("images/excel-i.bmp").
bDelete :load-image("images/delete.bmp").
bDelete :load-image-insensitive("images/excel-i.bmp").
bSearch :load-image("images/magnifier.bmp").
bSearch :load-image-insensitive("images/magnifier-i.bmp").
bCopy   :load-image("images/copy.bmp").
bCopy   :load-image-insensitive("images/copy-i.bmp").

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.
  
  {lib/get-column-width.i &col="'objValue'"  &var=dColumnWidth}

  run getData in this-procedure.

  publish "GetCurrentValue" ("RefreshProps", output std-ch).

  if logical(std-ch) 
   then
    /* Set Status count with date and time from the server */
    setStatusRecords(query brwProp:num-results).

  /* This procedure restores the window and move it to top */

  run showWindow in this-procedure.

  run windowResized in this-procedure.

  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionCopy C-Win 
PROCEDURE actionCopy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cAppCode     as character   no-undo.
  define variable cObjAction   as character   no-undo.
  define variable cObjID       as character   no-undo.
  define variable cObjProperty as character   no-undo.
  define variable cObjValue    as character   no-undo.

  define buffer ttSysProp for ttSysProp.
  empty temp-table ttSysProp.

  if not available sysProp 
   then
    return.
  
  create ttSysProp.
  buffer-copy sysProp to ttSysProp.
  
  /* All 5 character output parameters are used to uniquely identify a record */
  run dialogsysprop.w (input table ttSysProp,
                       input {&Copy},             
                       output cAppCode,
                       output cObjAction,
                       output cObjID,
                       output cObjProperty,
                       output cObjValue,
                       output std-lo)
                      .
  /*  if nothing is being updated then return. */
  if not std-lo 
   then
    return.

  run getData in this-procedure.

  find first sysProp where 
     sysProp.appCode     = cAppCode     and 
     sysProp.objAction   = cObjAction   and
     sysProp.objID       = cObjID       and  
     sysProp.objProperty = cObjProperty and 
     sysProp.objValue    = cObjValue 
      no-error.
  
  if available sysProp 
   then
    reposition brwProp to rowid rowid(sysProp) no-error. 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionDelete C-Win 
PROCEDURE actionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lSuccess as logical   no-undo.
  define variable cMsg     as character no-undo.
  
  if not available sysProp 
   then
    return.
  
  message "System Property will be deleted. Are you sure you want to delete ?"
      view-as alert-box question buttons yes-no update std-lo.
  if std-lo 
   then
    do:
      publish "DeleteSysProp" (input  sysProp.appcode,
                               input  sysProp.objaction,
                               input  sysProp.objID,
                               input  sysProp.objproperty,
                               input  sysProp.objvalue,
                               output lSuccess,
                               output cMsg).
      if not lSuccess 
       then
        do:
          message cMsg
              view-as alert-box info buttons ok.
          return.
        end.

      run getdata in this-procedure.
    end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionModify C-Win 
PROCEDURE actionModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cAppCode     as character   no-undo.
  define variable cObjAction   as character   no-undo.
  define variable cObjID       as character   no-undo.
  define variable cObjProperty as character   no-undo.
  define variable cObjValue    as character   no-undo.

  define buffer ttSysProp for ttSysProp.
  empty temp-table ttSysProp.

  if not available sysProp 
   then
    return.

  create ttSysProp.
  buffer-copy sysProp to ttSysProp.
  
  /* All 5 character output parameters are used to uniquely identify a record */
  run dialogsysprop.w (input table ttSysProp,
                       input {&Modify},             
                       output cAppCode,
                       output cObjAction,
                       output cObjID,
                       output cObjProperty,
                       output cObjValue,
                       output std-lo).
  if not std-lo 
   then
    return.

  run getData in this-procedure.

  find first sysProp where 
     sysProp.appCode     = cAppCode     and 
     sysProp.objAction   = cObjAction   and
     sysProp.objID       = cObjID       and  
     sysProp.objProperty = cObjProperty and 
     sysProp.objValue    = cObjValue 
      no-error.
  if available sysProp 
   then
    reposition brwProp to rowid rowid(sysProp) no-error. 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionNew C-Win 
PROCEDURE actionNew :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cAppCode     as character   no-undo.
  define variable cObjAction   as character   no-undo.
  define variable cObjID       as character   no-undo.
  define variable cObjProperty as character   no-undo.
  define variable cObjValue    as character   no-undo.

  define buffer ttSysProp for ttSysProp.
  empty temp-table ttSysProp.

  /* All 5 character output parameters are used to uniquely identify a record */
  run dialogsysprop.w (input table ttSysProp,
                       input {&New},             
                       output cAppCode,
                       output cObjAction,
                       output cObjID,
                       output cObjProperty,
                       output cObjValue,
                       output std-lo).
  if not std-lo 
   then
    return.

  run getData in this-procedure.

  find first sysProp where 
     sysProp.appCode     = cAppCode     and 
     sysProp.objAction   = cObjAction   and
     sysProp.objID       = cObjID       and  
     sysProp.objProperty = cObjProperty and 
     sysProp.objValue    = cObjValue 
      no-error.

  if available sysProp
   then
    reposition brwProp to rowid rowid(sysProp) no-error.
     
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbappCode fSearch 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bCopy RECT-66 RECT-62 bSearch cbappCode fSearch brwProp bdelete bEdit 
         bExport bNew bRefresh 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable htableHandle as handle no-undo.

  if query brwProp:num-results = 0 
   then
    do:
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.

  publish "GetReportDir" (output std-ch).

  do with frame {&frame-name}:
  end.

  htableHandle = temp-table sysProp:handle.
 
  run util/exporttable.p (table-handle htableHandle,
                          "sysprop",
                          "for each sysProp ",
                          "appcode,objAction,objID,objProperty,objValue,objName",
                          "Application Code,Action,ID,Property,Value,Name",
                          std-ch,
                          "SystemProperties-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  close query brwProp.
  empty temp-table sysProp.

  if lApplySearchString /* if search filter is also applied */
   then
    for each tempSysProp where tempSysProp.appcode = (if cbAppCode:input-value = "ALL" then tempSysProp.appcode else cbAppCode:input-value)
                                        and ((if trim(cLastSearchString) <> "" then tempSysProp.appcode matches ("*" + trim(cLastSearchString) + "*")
                                              else tempSysProp.appcode= tempSysProp.appcode) or
                                             (if trim(cLastSearchString) <> "" then tempSysProp.objAction matches ("*" + trim(cLastSearchString) + "*")
                                              else tempSysProp.objAction= tempSysProp.objAction) or
                                             (if trim(cLastSearchString) <> "" then tempSysProp.objProperty matches ("*" + trim(cLastSearchString) + "*")
                                              else tempSysProp.objProperty= tempSysProp.objProperty) or
                                             (if trim(cLastSearchString) <> "" then tempSysProp.objValue matches ("*" + trim(cLastSearchString) + "*")
                                              else tempSysProp.objValue= tempSysProp.objValue) or
                                             (if trim(cLastSearchString) <> "" then tempSysProp.objName matches ("*" + trim(cLastSearchString) + "*")
                                              else tempSysProp.objName = tempSysProp.objName)or
                                             (if trim(cLastSearchString) <> "" then tempSysProp.objID matches ("*" + trim(cLastSearchString) + "*")
                                              else tempSysProp.objID = tempSysProp.objID)
                                            ):
      create sysProp.
      buffer-copy tempSysProp to sysProp.
    end.
 
   else
    for each tempSysProp where tempSysProp.appcode = (if cbAppCode:input-value = "ALL" then tempSysProp.appcode else cbAppCode:input-value):
      create sysProp.
      buffer-copy tempSysProp to sysProp.
    end.

  open query brwProp preselect each sysProp by sysProp.appcode by sysProp.objAction by sysProp.objID by sysProp.objProperty by sysProp.objValue.

  setStatusCount(query brwProp:num-results).
  run setWidgetState in this-procedure. 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cCurrentAppCode as character no-undo.

  cCurrentAppCode = cbAppCode:input-value in frame {&frame-name}.
  
  empty temp-table tempSysProp.

  publish "GetSysProps" (output table tempSysProp, 
                         output std-lo,
                         output std-ch).
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box info buttons ok.
      return.
    end.

  /* get the list of all AppCodes present in sysdatasrv */

  publish "getAppCodeList"(output cList,
                           output std-lo,
                           output std-ch)
                           .                       
  if not std-lo
   then
    do:
      message std-ch
        view-as alert-box info buttons ok.
      return.
    end.

  cbappCode:list-items   = "ALL"  + (if cList ne "" then "," else "") + cList.
  cbappCode:screen-value = (if (cCurrentAppCode = "" or lookup(cCurrentAppCode,cbappCode:list-items) = 0)
                              then "ALL" 
                             else cCurrentAppCode).

  run filterData in this-procedure. 

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData C-Win 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table ttSysProp.
  publish "refreshSysProps" (output std-lo,
                             output std-ch). 
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.

  run getData in this-procedure.
  setStatusRecords(query brwProp:num-results).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if available sysProp
   then
    assign
        bExport:sensitive   = true
        bedit:sensitive     = true
        bcopy:sensitive     = true
        bDelete:sensitive   = true
        .
   else
    assign
        bExport:sensitive = false
        bedit:sensitive   = false
        bcopy:sensitive   = false
        bDelete:sensitive = false
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized then
     {&window-name}:window-state = window-normal.

  {&window-name}:move-to-top().
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame fMain:width-pixels          = {&window-name}:width-pixels
      frame fMain:virtual-width-pixels  = {&window-name}:width-pixels
      frame fMain:height-pixels         = {&window-name}:height-pixels
      frame fMain:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      brwprop:width-pixels              = frame fmain:width-pixels - 14
      brwprop:height-pixels             = frame fMain:height-pixels - {&browse-name}:y - 1
      .

  {lib/resize-column.i &col="'objAction,objProperty,objID,objValue'" &var=dColumnWidth} 
  run ShowScrollBars(browse brwProp:handle, no, yes).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  () :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatchSearchString}).
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

