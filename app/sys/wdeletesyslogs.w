&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File:wDeletesyslogs.w 

  Description: Delete Sysyem Logs

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul

  Created: 03.19.2018 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

  create widget-pool.
  
  /* ***************************  Definitions  ************************** */
  
  {lib/std-def.i}
  {lib/sys-def.i}
  {lib/get-column.i}                
  
  /* Temp table definations */
  {tt/sysLog.i &tableAlias="ttSysLog"}
  
  define temp-table sysLog like ttsysLog
    field cLogId as character.

  /* Variable Definition */
  define variable dColumnWidth   as decimal   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwDeletelogs

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES sysLog

/* Definitions for BROWSE brwDeletelogs                                 */
&Scoped-define FIELDS-IN-QUERY-brwDeletelogs sysLog.createDate "Create" sysLog.Action "Action" sysLog.UID "User ID" sysLog.progexec "Program Execution" sysLog.iserror "Error" sysLog.clogID "Log ID"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDeletelogs   
&Scoped-define SELF-NAME brwDeletelogs
&Scoped-define QUERY-STRING-brwDeletelogs for each sysLog
&Scoped-define OPEN-QUERY-brwDeletelogs open query {&SELF-NAME} for each sysLog.
&Scoped-define TABLES-IN-QUERY-brwDeletelogs sysLog
&Scoped-define FIRST-TABLE-IN-QUERY-brwDeletelogs sysLog


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwDeletelogs}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btGet fi-TillDate brwDeletelogs RECT-63 ~
RECT-64 
&Scoped-Define DISPLAYED-OBJECTS fi-TillDate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btdelete 
     LABEL "Delete" 
     SIZE 7 BY 1.67 TOOLTIP "Delete Compliance logs".

DEFINE BUTTON btexport 
     LABEL "Export" 
     SIZE 7 BY 1.67 TOOLTIP "Export to csv".

DEFINE BUTTON btGet  NO-FOCUS
     LABEL "Get" 
     SIZE 7.2 BY 1.67 TOOLTIP "Get Syslog Data".

DEFINE BUTTON btView 
     LABEL "View" 
     SIZE 7 BY 1.67 TOOLTIP "View ComLog".

DEFINE VARIABLE fi-TillDate AS DATE FORMAT "99/99/99":U 
     LABEL "Till Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Delete Compliance log till date" NO-UNDO.

DEFINE RECTANGLE RECT-63
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 34.6 BY 2.67.

DEFINE RECTANGLE RECT-64
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 24.8 BY 2.67.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwDeletelogs FOR 
      sysLog SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwDeletelogs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDeletelogs C-Win _FREEFORM
  QUERY brwDeletelogs DISPLAY
      sysLog.createDate label     "Create"   
 sysLog.Action     label          "Action"             format "x(30)"  
 sysLog.UID        label          "User ID"            format "x(40)"     
 sysLog.progexec   label          "Program Execution"  format "x(30)"    
 sysLog.iserror    column-label   "Error"              width 5 view-as toggle-box 
 sysLog.clogID     label          "Log ID"             format "x(15)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE NO-TAB-STOP SIZE 157.2 BY 18.81
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     btGet AT ROW 2.05 COL 28.6 WIDGET-ID 42 NO-TAB-STOP 
     fi-TillDate AT ROW 2.38 COL 11.6 COLON-ALIGNED WIDGET-ID 280
     brwDeletelogs AT ROW 4.57 COL 3 WIDGET-ID 200
     btdelete AT ROW 2.05 COL 38.8 WIDGET-ID 288 NO-TAB-STOP 
     btexport AT ROW 2.05 COL 46 WIDGET-ID 290 NO-TAB-STOP 
     btView AT ROW 2.05 COL 53.2 WIDGET-ID 292 NO-TAB-STOP 
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .81 AT ROW 1.14 COL 4.4 WIDGET-ID 266
     "Action" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.29 COL 38.8 WIDGET-ID 286
     RECT-63 AT ROW 1.57 COL 3 WIDGET-ID 236
     RECT-64 AT ROW 1.57 COL 37.2 WIDGET-ID 284
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 169 BY 26.24
         DEFAULT-BUTTON btGet WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Delete System Logs"
         HEIGHT             = 22.62
         WIDTH              = 162.4
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwDeletelogs fi-TillDate DEFAULT-FRAME */
ASSIGN 
       brwDeletelogs:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwDeletelogs:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

/* SETTINGS FOR BUTTON btdelete IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btexport IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btView IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDeletelogs
/* Query rebuild information for BROWSE brwDeletelogs
     _START_FREEFORM
open query {&SELF-NAME} for each sysLog.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwDeletelogs */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Delete System Logs */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Delete System Logs */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Delete System Logs */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDeletelogs
&Scoped-define SELF-NAME brwDeletelogs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDeletelogs C-Win
ON DEFAULT-ACTION OF brwDeletelogs IN FRAME DEFAULT-FRAME
do:
  run showsysLog in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDeletelogs C-Win
ON ROW-DISPLAY OF brwDeletelogs IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDeletelogs C-Win
ON START-SEARCH OF brwDeletelogs IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btdelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btdelete C-Win
ON CHOOSE OF btdelete IN FRAME DEFAULT-FRAME /* Delete */
DO:
  run deleteSysLog in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btexport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btexport C-Win
ON CHOOSE OF btexport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGet C-Win
ON CHOOSE OF btGet IN FRAME DEFAULT-FRAME /* Get */
do:
  run getData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btView C-Win
ON CHOOSE OF btView IN FRAME DEFAULT-FRAME /* View */
DO:
  run showSysLog in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
assign current-window                = {&window-name} 
       this-procedure:current-window = {&window-name}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
on close of this-procedure 
  run disable_UI.

setStatusMessage("").

/* Best default for GUI applications is...                              */
pause 0 before-hide.
subscribe to "closeWindow" anywhere.

btGet:load-image               ("images/Completed.bmp").              
btGet:load-image-insensitive   ("images/Completed-i.bmp").

btexport:load-image            ("images/excel.bmp").
btexport:load-image-insensitive("images/excel-i.bmp").

btdelete:load-image            ("images/delete.bmp").
btdelete:load-image-insensitive("images/delete-i.bmp").

btView:load-image              ("images/open.bmp").
btView:load-image-insensitive  ("images/open-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
    
  run enable_UI.
  
  {lib/get-column-width.i &col="'action'" &var=dColumnWidth} 
  
  run showWindow in this-procedure.

  run windowResized in this-procedure.

  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteSysLog C-Win 
PROCEDURE deleteSysLog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable csuccessmsg as character no-undo.

  do with frame {&frame-name}:
  end.

  message "System logs will be permanently deleted. Do you want to continue?"
       view-as alert-box question buttons yes-no update std-lo.

  if not std-lo
   then
    return.

  run server/deletesyslogs.p(input fi-tilldate:input-value,
                             output cSuccessMsg,
                             output std-lo,
                             output std-ch). 
  
  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.
     
  empty temp-table sysLog.
  close query brwDeletelogs.
  
  /* Sets buttons enable/disable when data is emptied */
  run setWidgetsState in this-procedure.
  
  setStatusMessage("").

  message cSuccessMsg
      view-as alert-box info buttons ok.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData C-Win 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  open query brwdeletelogs preselect each sysLog.

  setStatusRecords(query brwdeletelogs:num-results). 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fi-TillDate 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE btGet fi-TillDate brwDeletelogs RECT-63 RECT-64 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if query brwDeletelogs:num-results = 0 
   then
    do:
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.

  publish "GetReportDir" (output std-ch).

  std-ha = temp-table ttsyslog:handle.
  run util/exporttable.p (table-handle std-ha,
                          "ttsyslog",
                          "for each ttsyslog ",
                          "clogID,createDate,action,uid,addr,role,progExec,duration,isError,msg,faultCode,refType,refNum,sysMsg",
                          "Till " + fi-TillDate:screen-value + chr(10) + "LogID,Create Date,Action,UID,Address,Role,Program execution,Duration,Error,Message,Fault Code,Reference Type,Reference Number,System Message",
                          std-ch,
                          "DeleteSystemLogs-" + replace(string(now,"99-99-99"),"-","") + "-" + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttSysLog for ttSysLog.

  empty temp-table ttSysLog.
  empty temp-table sysLog.    
  
  do with frame {&frame-name}:
  end.
  
  if fi-tilldate:input-value = ? 
   then
    do:
      message "Input date cannot be blank."
          view-as alert-box info buttons ok.
      return.
    end.

  run server/getsyslogs.p(input "A", /* For all actions */
                          input "ALL",
                          input "ALL",
                          input ?,
                          input fi-tilldate:input-value,
                          output table ttSysLog,
                          output std-lo,
                          output std-ch).

  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.
  
  for each ttSysLog:
    create sysLog.
    buffer-copy ttSysLog to sysLog.
    sysLog.cLogID = string(sysLog.logID).         
  end.

  run displayData     in this-procedure.

  run setWidgetsState in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetsState C-Win 
PROCEDURE setWidgetsState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if query brwDeleteLogs:num-results > 0
   then
    assign
        btdelete:sensitive = true
        btExport:sensitive = true
        btView:sensitive   = true
        .
   else
    assign
        btdelete:sensitive = false
        btExport:sensitive = false
        btView:sensitive   = false
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showSysLog C-Win 
PROCEDURE showSysLog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttSyslog for ttSyslog.
  empty temp-table ttsyslog.
  
  if not available syslog
   then return.

  create ttSyslog.
  buffer-copy syslog to ttSyslog.

  run dialogsyslog.w (input table ttSyslog).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .
  
  c-Win:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i} 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      .

  /* {&frame-name} Components */
  assign 
      {&browse-name}:width-pixels  = frame {&frame-name}:width-pixels - 19
      {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 2
      .

  {lib/resize-column.i &col="'Action,UID,progexec'" &var=dColumnWidth}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

