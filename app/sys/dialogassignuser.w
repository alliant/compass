&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File:dialogassignuser.w 

  Description:Dialog to select multiple users for assigning a role. 

  Input Parameters:
      ipcRoleID  :<char> Role ID

  Output Parameters:
      oplSuccess :<char> Success/Failure 

  Author: Rahul Sharma

  Created:10.17.2018 

  Modified:

  Date       Name      Description

  08/22/2019 Gurvindar Removed progress error while populating combo-box.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/
/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---    */
define input  parameter ipcRoleID  as character no-undo.
define output parameter oplSuccess as logical   no-undo.

{lib/std-def.i}
{lib/sys-def.i}
{lib/brw-multi-def.i}

/* Temp-table Definitions ---                                           */
{tt/sysuser.i &tableAlias=ttsysuser}
{tt/sysuser.i &tableAlias=ttSelectedUser}

/* --- Local Variable Definitions --- */
define variable chRoleList as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwAvailableUsrs

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttsysuser ttSelectedUser

/* Definitions for BROWSE brwAvailableUsrs                              */
&Scoped-define FIELDS-IN-QUERY-brwAvailableUsrs ttsysuser.name ttsysuser.uid   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAvailableUsrs   
&Scoped-define SELF-NAME brwAvailableUsrs
&Scoped-define QUERY-STRING-brwAvailableUsrs for each ttsysuser by ttsysuser.name
&Scoped-define OPEN-QUERY-brwAvailableUsrs open query {&SELF-NAME} for each ttsysuser by ttsysuser.name.
&Scoped-define TABLES-IN-QUERY-brwAvailableUsrs ttsysuser
&Scoped-define FIRST-TABLE-IN-QUERY-brwAvailableUsrs ttsysuser


/* Definitions for BROWSE brwSelectedUsers                              */
&Scoped-define FIELDS-IN-QUERY-brwSelectedUsers ttSelectedUser.name ttSelectedUser.uid   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwSelectedUsers   
&Scoped-define SELF-NAME brwSelectedUsers
&Scoped-define QUERY-STRING-brwSelectedUsers for each ttSelectedUser by ttSelectedUser.name
&Scoped-define OPEN-QUERY-brwSelectedUsers open query {&SELF-NAME} for each ttSelectedUser by ttSelectedUser.name.
&Scoped-define TABLES-IN-QUERY-brwSelectedUsers ttSelectedUser
&Scoped-define FIRST-TABLE-IN-QUERY-brwSelectedUsers ttSelectedUser


/* Definitions for DIALOG-BOX Dialog-Frame                              */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbRole brwAvailableUsrs brwSelectedUsers ~
BtnOK BtnCancel 
&Scoped-Define DISPLAYED-OBJECTS cbRole 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddAllUsers 
     LABEL "Add All Users >>" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add All Users".

DEFINE BUTTON bAddUser 
     LABEL "Add User >" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add User".

DEFINE BUTTON bRemoveAll 
     LABEL "<< Remove All Users" 
     SIZE 4.8 BY 1.14 TOOLTIP "Remove All Users".

DEFINE BUTTON bRemoveUser 
     LABEL "< Remove User" 
     SIZE 4.8 BY 1.14 TOOLTIP "Remove User".

DEFINE BUTTON BtnCancel AUTO-END-KEY DEFAULT 
     LABEL "Cancel" 
     SIZE 12 BY 1.14 TOOLTIP "Cancel".

DEFINE BUTTON BtnOK AUTO-GO DEFAULT 
     LABEL "Save" 
     SIZE 12 BY 1.14 TOOLTIP "Save".

DEFINE VARIABLE cbRole AS CHARACTER FORMAT "X(256)":U 
     LABEL "Role" 
     VIEW-AS COMBO-BOX INNER-LINES 11
     DROP-DOWN-LIST
     SIZE 32 BY 1 TOOLTIP "Select Role" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAvailableUsrs FOR 
      ttsysuser SCROLLING.

DEFINE QUERY brwSelectedUsers FOR 
      ttSelectedUser SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAvailableUsrs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAvailableUsrs Dialog-Frame _FREEFORM
  QUERY brwAvailableUsrs DISPLAY
      ttsysuser.name         column-label "Name"         format "x(30)"         width 28
ttsysuser.uid    column-label "UID"  format "x(70)"   width 40
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 95.4 BY 15.81
         TITLE "Available Users" ROW-HEIGHT-CHARS .81 NO-EMPTY-SPACE.

DEFINE BROWSE brwSelectedUsers
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwSelectedUsers Dialog-Frame _FREEFORM
  QUERY brwSelectedUsers DISPLAY
      ttSelectedUser.name   column-label "Name"         format "x(30)"     width 28
ttSelectedUser.uid    column-label "UID"  format "x(70)"     width 40
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 100.6 BY 15.81
         TITLE "Selected Users" ROW-HEIGHT-CHARS .81 NO-EMPTY-SPACE.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     cbRole AT ROW 2.48 COL 6.2 COLON-ALIGNED WIDGET-ID 320
     brwAvailableUsrs AT ROW 4.1 COL 2.6 WIDGET-ID 300
     brwSelectedUsers AT ROW 4.1 COL 110 WIDGET-ID 400
     bAddAllUsers AT ROW 9.62 COL 101.6 WIDGET-ID 310
     bAddUser AT ROW 10.81 COL 101.6 WIDGET-ID 322
     bRemoveUser AT ROW 12 COL 101.6 WIDGET-ID 312
     bRemoveAll AT ROW 13.19 COL 101.6 WIDGET-ID 324
     BtnOK AT ROW 20.48 COL 91.4 WIDGET-ID 208
     BtnCancel AT ROW 20.48 COL 105.4 WIDGET-ID 204
     SPACE(93.79) SKIP(0.28)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Assign Role To User"
         DEFAULT-BUTTON BtnOK CANCEL-BUTTON BtnCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
/* BROWSE-TAB brwAvailableUsrs cbRole Dialog-Frame */
/* BROWSE-TAB brwSelectedUsers brwAvailableUsrs Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bAddAllUsers IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bAddUser IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bRemoveAll IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bRemoveUser IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       brwAvailableUsrs:ALLOW-COLUMN-SEARCHING IN FRAME Dialog-Frame = TRUE
       brwAvailableUsrs:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE.

ASSIGN 
       brwSelectedUsers:ALLOW-COLUMN-SEARCHING IN FRAME Dialog-Frame = TRUE
       brwSelectedUsers:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAvailableUsrs
/* Query rebuild information for BROWSE brwAvailableUsrs
     _START_FREEFORM
open query {&SELF-NAME} for each ttsysuser by ttsysuser.name.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwAvailableUsrs */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwSelectedUsers
/* Query rebuild information for BROWSE brwSelectedUsers
     _START_FREEFORM
open query {&SELF-NAME} for each ttSelectedUser by ttSelectedUser.name.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwSelectedUsers */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Assign Role To User */
DO:
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddAllUsers
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddAllUsers Dialog-Frame
ON CHOOSE OF bAddAllUsers IN FRAME Dialog-Frame /* Add All Users >> */
do:
  run addAllUsers in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddUser
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddUser Dialog-Frame
ON CHOOSE OF bAddUser IN FRAME Dialog-Frame /* Add User > */
do:
  run addUser in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRemoveAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRemoveAll Dialog-Frame
ON CHOOSE OF bRemoveAll IN FRAME Dialog-Frame /* << Remove All Users */
do:
  run removeAllUsers in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRemoveUser
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRemoveUser Dialog-Frame
ON CHOOSE OF bRemoveUser IN FRAME Dialog-Frame /* < Remove User */
do:
  run removeUser in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAvailableUsrs
&Scoped-define SELF-NAME brwAvailableUsrs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAvailableUsrs Dialog-Frame
ON DEFAULT-ACTION OF brwAvailableUsrs IN FRAME Dialog-Frame /* Available Users */
do:
  apply "choose" to bAddUser.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAvailableUsrs Dialog-Frame
ON ROW-DISPLAY OF brwAvailableUsrs IN FRAME Dialog-Frame /* Available Users */
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAvailableUsrs Dialog-Frame
ON START-SEARCH OF brwAvailableUsrs IN FRAME Dialog-Frame /* Available Users */
do:
  define variable tWhereClause as character no-undo.
  {lib/brw-startSearch-multi.i &browseName="brwAvailableUsrs" &whereClause="tWhereClause"}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAvailableUsrs Dialog-Frame
ON VALUE-CHANGED OF brwAvailableUsrs IN FRAME Dialog-Frame /* Available Users */
DO:
  if available ttsysuser 
   then
    assign
        bAddAllUsers:sensitive = true
        bAddUser:sensitive     = true
        .
   else
    assign
        bAddAllUsers:sensitive = false
        bAddUser:sensitive     = false
        .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwSelectedUsers
&Scoped-define SELF-NAME brwSelectedUsers
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSelectedUsers Dialog-Frame
ON DEFAULT-ACTION OF brwSelectedUsers IN FRAME Dialog-Frame /* Selected Users */
DO:
  apply "choose" to bRemoveUser.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSelectedUsers Dialog-Frame
ON ROW-DISPLAY OF brwSelectedUsers IN FRAME Dialog-Frame /* Selected Users */
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSelectedUsers Dialog-Frame
ON START-SEARCH OF brwSelectedUsers IN FRAME Dialog-Frame /* Selected Users */
do:
  define variable tWhereClause as character no-undo.
  {lib/brw-startSearch-multi.i &browseName="brwSelectedUsers" &whereClause="tWhereClause"}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnCancel Dialog-Frame
ON CHOOSE OF BtnCancel IN FRAME Dialog-Frame /* Cancel */
do:
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnOK Dialog-Frame
ON CHOOSE OF BtnOK IN FRAME Dialog-Frame /* Save */
do:
  run assignUser in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRole Dialog-Frame
ON VALUE-CHANGED OF cbRole IN FRAME Dialog-Frame /* Role */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAvailableUsrs
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
 if valid-handle(active-window) and frame {&frame-name}:parent eq ? then
  frame {&frame-name}:parent = active-window.

/* Now enable the interface and wait for the exit condition.            */

publish "getSysRoleList" (input ",",
                          output chRoleList,
                          output std-lo,
                          output std-ch).
if not std-lo
 then
  do:
    message std-ch
       view-as alert-box info buttons ok.
    return.
  end.

bAddAllUsers:load-image("images/s-nextpg.bmp").
bremoveall :load-image("images/s-previouspg.bmp").
bAddUser   :load-image("images/s-next.bmp").
bremoveuser:load-image("images/s-previous.bmp").

cbRole:list-items = {&SelectRole}.
if chRoleList ne ""
 then
  cbRole:list-items = cbRole:list-items + ","  + trim(chRoleList,",") no-error.

/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.
  
  if lookup(ipcRoleID,cbRole:list-items) = 0
   then
    cbRole:add-last(ipcRoleID).
    
  cbRole:screen-value = ipcRoleID.
  
  apply 'value-changed' to cbRole.

  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addAllUsers Dialog-Frame 
PROCEDURE addAllUsers :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  for each ttsysuser:
    if not can-find(first ttSelectedUser where ttSelectedUser.uid = ttsysuser.uid) 
     then
      do:
        create ttSelectedUser.
        buffer-copy ttsysuser to ttSelectedUser.
        delete ttsysuser.
      end.
  end.
  
  open query brwAvailableUsrs for each ttsysuser      by ttsysuser.name.
  open query brwSelectedUsers for each ttSelectedUser by ttSelectedUser.name.  
  
  if num-results("brwAvailableUsrs") = 0 
   then
    assign
        bAddAllUsers:sensitive = false
        bAddUser    :sensitive = false
        . 
   else
    assign
        bAddAllUsers:sensitive = true
        bAddUser    :sensitive = true
        .

  assign
      BtnOK:sensitive       = true
      bRemoveUser:sensitive = true
      bRemoveAll:sensitive  = true
      . 
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addUser Dialog-Frame 
PROCEDURE addUser :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if not available ttsysuser 
   then return.

  if can-find(first ttSelectedUser where ttSelectedUser.uid = ttsysuser.uid) 
   then return.
  
  create ttSelectedUser.
  buffer-copy ttsysuser to ttSelectedUser.
  std-ro = rowid(ttSelectedUser).
  delete ttsysuser.
  
  open query brwAvailableUsrs for each ttsysuser      by ttsysuser.name.
  open query brwSelectedUsers for each ttSelectedUser by ttSelectedUser.name. 
  
  reposition brwSelectedUsers to rowid std-ro.
  
  if num-results("brwAvailableUsrs") = 0 
   then
    assign
        bAddUser:sensitive     = false
        bAddAllUsers:sensitive = false
        . 
   else
    assign
        bAddUser:sensitive     = true
        bAddAllUsers:sensitive = true
        . 
  assign
      BtnOK:sensitive       = true
      bRemoveUser:sensitive = true
      bRemoveAll:sensitive  = true
      . 
  apply "value-changed" to brwAvailableUsrs.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assignUser Dialog-Frame 
PROCEDURE assignUser :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cSelectedUserList as character no-undo.

  for each ttSelectedUser:
    cSelectedUserList = cSelectedUserList + "," + ttSelectedUser.uid.
  end.

  assign cSelectedUserList = trim(cSelectedUserList,",").

  /* Server call */
  publish "assignUser" (input cbRole:input-value in frame {&frame-name},
                        input cSelectedUserList,
                        output oplSuccess,
                        output std-ch).

  if not oplSuccess /* if not success */ 
   then
    do:
      message std-ch
        view-as alert-box info buttons ok.

      return no-apply.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbRole 
      WITH FRAME Dialog-Frame.
  ENABLE cbRole brwAvailableUsrs brwSelectedUsers BtnOK BtnCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData Dialog-Frame 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  empty temp-table ttsysuser.

  if cbRole:screen-value  <> {&SelectRole}
   then
    do:  
      /* Getting the sysuser detials */
      publish "GetSysUsers" (output table ttsysuser,
                             output std-lo,
                             output std-ch).
   
      if not std-lo  /* returns if fails */
       then
        do:
          MESSAGE std-ch
              VIEW-AS ALERT-BOX INFO BUTTONS OK.
          return.
        end.

      BtnOK:sensitive = false .
  
      open query brwAvailableUsrs for each ttsysuser.
      
      if num-results("brwSelectedUsers") <> 0 
       then
        assign
            bRemoveUser:sensitive = true 
            bRemoveAll:sensitive  = true 
            .
      else
       assign                              
           bRemoveUser:sensitive = false     
           bRemoveAll:sensitive  = false      
           .                                
      apply "value-changed" to brwAvailableUsrs.
    end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE removeAllUsers Dialog-Frame 
PROCEDURE removeAllUsers :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  for each ttSelectedUser:
    if not can-find(first ttsysuser where ttsysuser.uid = ttSelectedUser.uid)
     then
      do:
        create ttsysuser.
        buffer-copy ttSelectedUser to ttsysuser.
        delete ttSelectedUser.
      end.
  end.
  
  open query brwAvailableUsrs for each ttsysuser      by ttsysuser.name.
  open query brwSelectedUsers for each ttSelectedUser by ttSelectedUser.name. 
  
  if num-results("brwSelectedUsers") = 0 
   then
    assign
        bRemoveUser:sensitive = false
        bRemoveAll:sensitive  = false
        .
   else
    assign
        bRemoveUser:sensitive = true
        bRemoveAll:sensitive  = true
        .
   
  assign
      BtnOK:sensitive        = true   
      bAddAllUsers:sensitive = true   
      bAddUser:sensitive     = true
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE removeUser Dialog-Frame 
PROCEDURE removeUser :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if not available ttSelectedUser 
   then return.

  if can-find(first ttsysuser where ttsysuser.uid = ttSelectedUser.uid) 
   then return.
       
  create ttsysuser.
  buffer-copy ttSelectedUser to ttsysuser.
  std-ro = rowid(ttsysuser).
  delete ttSelectedUser.
  
  open query brwAvailableUsrs for each ttsysuser      by ttsysuser.name.
  open query brwSelectedUsers for each ttSelectedUser by ttSelectedUser.name. 
  
  reposition brwAvailableUsrs to rowid std-ro.
  
  if num-results("brwSelectedUsers") = 0 
   then
    assign
        bRemoveUser:sensitive = false
        bRemoveAll :sensitive = false
        .
      
   else
    assign
        bRemoveUser:sensitive = true
        bRemoveAll :sensitive = true
        .
  
  assign
      bAddAllUsers:sensitive = true
      BtnOK :sensitive       = true
      bAddUser:sensitive     = true
      .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData Dialog-Frame 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

