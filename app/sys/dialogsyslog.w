&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogsyslog.p

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gurvindar Singh

  Created:10/05/18
  @Modification:
  Date          Name          Description
  07/18/2019    Gurvindar     Added duration in UI
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{tt/syslog.i}
{lib/std-def.i}

/* Parameters Definitions ---                                           */
define input parameter table for syslog.

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS flLogID flAction flModuleId flUID edAddress ~
edRole flCreateDate flProgExec fduration flMsg flSysMsg flFaultCode ~
flReftype flRefNum 
&Scoped-Define DISPLAYED-OBJECTS flLogID tError flAction flModuleId flUID ~
edAddress edRole flCreateDate flProgExec fduration flMsg flSysMsg ~
flFaultCode flReftype flRefNum 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE VARIABLE edAddress AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 58 BY 2.24 NO-UNDO.

DEFINE VARIABLE edRole AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 58 BY 3.38 NO-UNDO.

DEFINE VARIABLE fduration AS DECIMAL FORMAT ">>>>9.99":U INITIAL 0 
     LABEL "Duration (sec)" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE flAction AS CHARACTER FORMAT "X(256)":U 
     LABEL "Action" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE flCreateDate AS DATETIME FORMAT "99/99/99 HH:MM:SS":U 
     LABEL "Create Date" 
     VIEW-AS FILL-IN 
     SIZE 58 BY 1 NO-UNDO.

DEFINE VARIABLE flFaultCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Fault Code" 
     VIEW-AS FILL-IN 
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE flLogID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Log ID" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE flModuleId AS CHARACTER FORMAT "X(256)":U 
     LABEL "Module ID" 
     VIEW-AS FILL-IN 
     SIZE 15.8 BY 1 NO-UNDO.

DEFINE VARIABLE flMsg AS CHARACTER FORMAT "X(256)":U 
     LABEL "Message" 
     VIEW-AS FILL-IN 
     SIZE 58 BY 1 NO-UNDO.

DEFINE VARIABLE flProgExec AS CHARACTER FORMAT "X(256)":U 
     LABEL "Program Execution" 
     VIEW-AS FILL-IN 
     SIZE 58 BY 1 NO-UNDO.

DEFINE VARIABLE flRefNum AS CHARACTER FORMAT "X(256)":U 
     LABEL "Reference Number" 
     VIEW-AS FILL-IN 
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE flReftype AS CHARACTER FORMAT "X(256)":U 
     LABEL "Reference Type" 
     VIEW-AS FILL-IN 
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE flSysMsg AS CHARACTER FORMAT "X(256)":U 
     LABEL "System Message" 
     VIEW-AS FILL-IN 
     SIZE 58 BY 1 NO-UNDO.

DEFINE VARIABLE flUID AS CHARACTER FORMAT "X(256)":U 
     LABEL "UID" 
     VIEW-AS FILL-IN 
     SIZE 58 BY 1 NO-UNDO.

DEFINE VARIABLE tError AS LOGICAL INITIAL no 
     LABEL "Error" 
     VIEW-AS TOGGLE-BOX
     SIZE 9.6 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     flLogID AT ROW 1.71 COL 20 COLON-ALIGNED WIDGET-ID 26
     tError AT ROW 1.81 COL 46.6 WIDGET-ID 14
     flAction AT ROW 2.91 COL 20 COLON-ALIGNED WIDGET-ID 20
     flModuleId AT ROW 2.91 COL 62.2 COLON-ALIGNED WIDGET-ID 42
     flUID AT ROW 4.1 COL 20 COLON-ALIGNED WIDGET-ID 2
     edAddress AT ROW 5.29 COL 22 NO-LABEL WIDGET-ID 8
     edRole AT ROW 7.76 COL 22 NO-LABEL WIDGET-ID 40
     flCreateDate AT ROW 11.38 COL 20 COLON-ALIGNED WIDGET-ID 38
     flProgExec AT ROW 12.57 COL 20 COLON-ALIGNED WIDGET-ID 22
     fduration AT ROW 13.71 COL 20 COLON-ALIGNED WIDGET-ID 44
     flMsg AT ROW 14.86 COL 20 COLON-ALIGNED WIDGET-ID 6
     flSysMsg AT ROW 16.05 COL 20 COLON-ALIGNED WIDGET-ID 16
     flFaultCode AT ROW 17.24 COL 20 COLON-ALIGNED WIDGET-ID 32
     flReftype AT ROW 18.43 COL 20 COLON-ALIGNED WIDGET-ID 34
     flRefNum AT ROW 19.67 COL 20 COLON-ALIGNED WIDGET-ID 36
     "Address:" VIEW-AS TEXT
          SIZE 7.8 BY .62 AT ROW 5.38 COL 13 WIDGET-ID 18
     "Role:" VIEW-AS TEXT
          SIZE 5.4 BY .62 AT ROW 7.81 COL 16.4 WIDGET-ID 30
     SPACE(59.39) SKIP(12.42)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "System  Log" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       edAddress:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       edRole:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fduration:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flAction:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flCreateDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flFaultCode:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flLogID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flModuleId:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flMsg:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flProgExec:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flRefNum:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flReftype:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flSysMsg:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flUID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR TOGGLE-BOX tError IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* System  Log */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  run enable_UI.
  
  /* sets default values to the widgets */
  run setData.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.

RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flLogID tError flAction flModuleId flUID edAddress edRole flCreateDate 
          flProgExec fduration flMsg flSysMsg flFaultCode flReftype flRefNum 
      WITH FRAME Dialog-Frame.
  ENABLE flLogID flAction flModuleId flUID edAddress edRole flCreateDate 
         flProgExec fduration flMsg flSysMsg flFaultCode flReftype flRefNum 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData Dialog-Frame 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  frame dialog-frame:title = "View System Log".

  find first syslog no-error.
  if not available syslog 
   then
    return.

  assign 
      flLogID:screen-value          = string(syslog.logID)
      flCreateDate:screen-value     = string(syslog.createDate)
      flAction:screen-value         = syslog.action
      flUID:screen-value            = syslog.uid
      edAddress:screen-value        = syslog.addr 
      edRole:screen-value           = syslog.role 
      tError:checked                = logical(syslog.iserror)
      flProgExec:screen-value       = syslog.progExec 
      flMsg:screen-value            = syslog.msg
      flFaultCode:screen-value      = syslog.faultCode 
      flReftype:screen-value        = syslog.refType 
      flRefNum:screen-value         = syslog.refNum
      flSysmsg:screen-value         = syslog.sysMsg      
      flModuleId:screen-value       = syslog.applicationID 
      fduration:screen-value        = string(syslog.duration / 1000).
      .     
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

