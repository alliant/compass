&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogmodifysysuser.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gurvindar Singh

  Created:09/26/18
 
 Modified:
 
 Date        Name     Description
 
 08/22/2019 Gurvindar Removed progress error while populating combo-box and selection list.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{tt/sysuser.i}
{tt/sysrole.i}

/* Parameters Definitions ---                                           */
define input  parameter table       for sysuser.
define output parameter oplSuccess  as logical   no-undo.

/* Local Variable Definitions ---                                       */
define variable cName           as character no-undo.
define variable cEmail          as character no-undo.
define variable cDeptID         as character no-undo.
define variable cInitials       as character no-undo.
define variable cComments       as character no-undo.
define variable cAvailableCards as character no-undo.
define variable cSelectedCards  as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bAddAll fName fInitial bRemoveAll cbDept ~
fEmail slAvailableRoles slSelectedRoles eComments BtnCancel 
&Scoped-Define DISPLAYED-OBJECTS fUid fName fInitial cbDept fEmail ~
slAvailableRoles slSelectedRoles tActive flCreatedate flPasswordSetDate ~
eComments tPasswordExpired 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddAll  NO-FOCUS
     LABEL "ALL-->" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add all Roles".

DEFINE BUTTON bAddRole  NO-FOCUS
     LABEL "-->" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a Role".

DEFINE BUTTON bDeleteRole  NO-FOCUS
     LABEL "<--" 
     SIZE 4.6 BY 1 TOOLTIP "Remove a Role".

DEFINE BUTTON bRemoveAll  NO-FOCUS
     LABEL "<-- ALL" 
     SIZE 4.6 BY 1 TOOLTIP "Remove all Roles".

DEFINE BUTTON BtnCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Cancel"
     BGCOLOR 8 .

DEFINE BUTTON BtnOK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14 TOOLTIP "Save"
     BGCOLOR 8 .

DEFINE VARIABLE cbDept AS CHARACTER FORMAT "X(256)":U 
     LABEL "Department" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "System","Technology","Agency","Executive","Accounting","Production","Underwriting","Risk","Operations","Claims","Communications","Marketing","HR" 
     DROP-DOWN-LIST
     SIZE 39.8 BY 1 NO-UNDO.

DEFINE VARIABLE eComments AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 61 BY 2.24 NO-UNDO.

DEFINE VARIABLE fEmail AS CHARACTER FORMAT "X(256)":U 
     LABEL "Email" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 61 BY 1 NO-UNDO.

DEFINE VARIABLE fInitial AS CHARACTER FORMAT "X(256)":U 
     LABEL "Initials" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 12.8 BY 1 NO-UNDO.

DEFINE VARIABLE flCreatedate AS DATE FORMAT "99/99/9999":U 
     LABEL "Create" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE flPasswordSetDate AS DATE FORMAT "99/99/9999":U 
     LABEL "Password Set On" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE fName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 39.8 BY 1 NO-UNDO.

DEFINE VARIABLE fUid AS CHARACTER FORMAT "X(256)":U 
     LABEL "UID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 39.8 BY 1 NO-UNDO.

DEFINE VARIABLE slAvailableRoles AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SCROLLBAR-VERTICAL 
     SIZE 21.8 BY 5.76 NO-UNDO.

DEFINE VARIABLE slSelectedRoles AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SCROLLBAR-VERTICAL 
     SIZE 21.8 BY 5.76 NO-UNDO.

DEFINE VARIABLE tActive AS LOGICAL INITIAL no 
     LABEL "Active" 
     VIEW-AS TOGGLE-BOX
     SIZE 10.8 BY .81 NO-UNDO.

DEFINE VARIABLE tPasswordExpired AS LOGICAL INITIAL no 
     LABEL "Password Expired" 
     VIEW-AS TOGGLE-BOX
     SIZE 20.4 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bAddAll AT ROW 7.91 COL 49.29 WIDGET-ID 90 NO-TAB-STOP 
     bAddRole AT ROW 9.1 COL 49.29 WIDGET-ID 78 NO-TAB-STOP 
     bDeleteRole AT ROW 10.29 COL 49.29 WIDGET-ID 80 NO-TAB-STOP 
     bRemoveAll AT ROW 11.33 COL 49.29 WIDGET-ID 88 NO-TAB-STOP 
     fUid AT ROW 1.33 COL 19.2 COLON-ALIGNED WIDGET-ID 20
     fName AT ROW 2.57 COL 19.2 COLON-ALIGNED WIDGET-ID 2
     fInitial AT ROW 2.57 COL 67.4 COLON-ALIGNED WIDGET-ID 6
     cbDept AT ROW 3.81 COL 19.2 COLON-ALIGNED WIDGET-ID 114
     fEmail AT ROW 5.05 COL 19.2 COLON-ALIGNED WIDGET-ID 16
     slAvailableRoles AT ROW 7.24 COL 21.2 NO-LABEL WIDGET-ID 82
     slSelectedRoles AT ROW 7.24 COL 60.4 NO-LABEL WIDGET-ID 84
     tActive AT ROW 1.48 COL 69.6 WIDGET-ID 14 NO-TAB-STOP 
     flCreatedate AT ROW 14.76 COL 19.2 COLON-ALIGNED WIDGET-ID 40
     flPasswordSetDate AT ROW 13.43 COL 19.2 COLON-ALIGNED WIDGET-ID 42
     eComments AT ROW 16.1 COL 21.2 NO-LABEL WIDGET-ID 8
     BtnOK AT ROW 18.81 COL 28 WIDGET-ID 112
     BtnCancel AT ROW 18.81 COL 46.2 WIDGET-ID 110
     tPasswordExpired AT ROW 13.57 COL 62.2 WIDGET-ID 46 NO-TAB-STOP 
     "Available Roles" VIEW-AS TEXT
          SIZE 18 BY .62 AT ROW 6.48 COL 22 WIDGET-ID 86
          FONT 6
     "Selected Roles" VIEW-AS TEXT
          SIZE 18.8 BY .62 AT ROW 6.48 COL 61 WIDGET-ID 44
          FONT 6
     "Comment:" VIEW-AS TEXT
          SIZE 9.2 BY .62 AT ROW 16.24 COL 11.2 WIDGET-ID 38
     SPACE(65.19) SKIP(3.18)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Edit User" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bAddRole IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDeleteRole IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BtnOK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flCreatedate IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flPasswordSetDate IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fUid IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fUid:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR TOGGLE-BOX tActive IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tPasswordExpired IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Edit User */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddAll Dialog-Frame
ON CHOOSE OF bAddAll IN FRAME Dialog-Frame /* ALL--> */
do:
  assign 
      cAvailableCards               = if slAvailableRoles:list-items = ? or slAvailableRoles:list-items = "?" then "" else slAvailableRoles:list-items
      cSelectedCards                = if slSelectedRoles:list-items  = ? or slSelectedRoles:list-items  = "?" then "" else slSelectedRoles:list-items
      slSelectedRoles:list-items    = trim(cAvailableCards + "," + cSelectedCards,",")
      slAvailableRoles:list-items   = ?
      bAddAll:sensitive             = false
      bRemoveAll:sensitive          = true
      bAddRole:sensitive            = false
      cAvailableCards               = ""
      cSelectedCards                = ""
      .

  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddRole Dialog-Frame
ON CHOOSE OF bAddRole IN FRAME Dialog-Frame /* --> */
do:
  if slAvailableRoles:input-value = ?
   then return.

  slSelectedRoles:add-last(slAvailableRoles:input-value).
  slSelectedRoles:list-items = trim(slSelectedRoles:list-items,",").
  slAvailableRoles:delete(slAvailableRoles:input-value).
  bAddRole  :sensitive = false.
  bRemoveAll:sensitive = true.

  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteRole Dialog-Frame
ON CHOOSE OF bDeleteRole IN FRAME Dialog-Frame /* <-- */
do:
  if slSelectedRoles:input-value = ? 
   then return.

  slAvailableRoles:list-items = trim(slAvailableRoles:list-items).
  slAvailableRoles:add-last(slSelectedRoles:input-value).
  slAvailableRoles:list-items = trim(slAvailableRoles:list-items,",").
  slSelectedRoles:delete(slSelectedRoles:input-value).
  bDeleteRole:sensitive = false.
  bAddAll    :sensitive = true.

  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRemoveAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRemoveAll Dialog-Frame
ON CHOOSE OF bRemoveAll IN FRAME Dialog-Frame /* <-- ALL */
do:
  assign 
      cAvailableCards              = if slAvailableRoles:list-items = ? or slAvailableRoles:list-items = "?" then "" else slAvailableRoles:list-items
      cSelectedCards               = if slSelectedRoles:list-items  = ? or slSelectedRoles:list-items  = "?" then "" else slSelectedRoles:list-items
      slAvailableRoles:list-items  = trim(cAvailableCards + "," + cSelectedCards,",")
      slSelectedRoles :list-items  = ?
      bRemoveAll:sensitive         = false
      bAddAll:sensitive            = true
      bDeleteRole:sensitive        = false
      cAvailableCards              = ""
      cSelectedCards               = ""
      .

  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnCancel Dialog-Frame
ON CHOOSE OF BtnCancel IN FRAME Dialog-Frame /* Cancel */
DO:
  oplSuccess = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnOK Dialog-Frame
ON CHOOSE OF BtnOK IN FRAME Dialog-Frame /* Save */
DO:
  if fName:input-value in frame {&frame-name} = "" 
   then
    do:
      message "Name cannot be blank."
        view-as alert-box info buttons ok.
      return no-apply.
    end.

  if fEmail:input-value = "" 
   then
    do:
      message "Email cannot be blank."
        view-as alert-box info buttons ok.
      return no-apply.
    end.

  /* Save sysuser to the server */
  run saveSysUser in this-procedure.
   
  /* If there was any error then do not close the dialog. */
  if not oplSuccess
   then
    return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbDept
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbDept Dialog-Frame
ON VALUE-CHANGED OF cbDept IN FRAME Dialog-Frame /* Department */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eComments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eComments Dialog-Frame
ON VALUE-CHANGED OF eComments IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fEmail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fEmail Dialog-Frame
ON VALUE-CHANGED OF fEmail IN FRAME Dialog-Frame /* Email */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fInitial
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fInitial Dialog-Frame
ON VALUE-CHANGED OF fInitial IN FRAME Dialog-Frame /* Initials */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fName Dialog-Frame
ON VALUE-CHANGED OF fName IN FRAME Dialog-Frame /* Name */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME slAvailableRoles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL slAvailableRoles Dialog-Frame
ON DEFAULT-ACTION OF slAvailableRoles IN FRAME Dialog-Frame
DO:
  apply "CHOOSE" to bAddRole.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL slAvailableRoles Dialog-Frame
ON VALUE-CHANGED OF slAvailableRoles IN FRAME Dialog-Frame
DO:
  assign
      bAddRole:sensitive    = true
      bDeleteRole:sensitive = false
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME slSelectedRoles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL slSelectedRoles Dialog-Frame
ON DEFAULT-ACTION OF slSelectedRoles IN FRAME Dialog-Frame
DO:
  apply "CHOOSE" to bDeleteRole.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL slSelectedRoles Dialog-Frame
ON VALUE-CHANGED OF slSelectedRoles IN FRAME Dialog-Frame
DO:
  assign 
      bDeleteRole:sensitive = true
      bAddRole:sensitive    = false
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bAddRole   :load-image("images/s-next.bmp").
bDeleteRole:load-image("images/s-previous.bmp").
bAddAll    :load-image("images/s-nextpg.bmp").
bremoveall :load-image("images/s-previouspg.bmp").

/* Get sysroles and populate slAvailableRoles selection list */
run getSysRoles in this-procedure.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  /* Display data on the widgets */
  run displayData.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  find first sysuser no-error.
  if available sysuser
   then
    do:
      slAvailableRoles:delete(trim(sysUser.role,",")) no-error.
      slSelectedRoles:list-items = trim(sysUser.role,",") no-error.
   
      assign
          fUid:screen-value              = sysuser.uid                    
          fName:screen-value             = sysuser.name           
          fInitial:screen-value          = sysuser.initials 
          fEmail:screen-value            = sysuser.email
          cbDept:screen-value            = if lookup(sysuser.department,cbDept:list-items) = 0 then "" else sysuser.department
          tActive:checked                = sysuser.isActive
          tPasswordExpired:checked       = sysuser.passwordExpired
          flCreatedate:screen-value      = string(sysuser.createDate)
          flPasswordSetDate:screen-value = string(sysuser.passwordSetDate)
          eComments:screen-value         = sysuser.comments          
          .
    end.
   
  /* Keep the copy of initial values. Required in enableDisableSave. */ 
  assign
      cName           = fName:input-value
      cInitials       = fInitial:input-value
      cEmail          = fEmail:input-value
      cDeptID         = cbDept:input-value
      cAvailableCards = slAvailableRoles:list-items
      cSelectedCards  = slSelectedRoles:list-items
      cComments       = eComments:input-value
      . 
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
             
  BtnOK:sensitive = not (cName           = fName:input-value            and
                         cInitials       = fInitial:input-value         and
                         cEmail          = fEmail:input-value           and
                         cDeptID         = cbDept:input-value           and
                         cAvailableCards = slAvailableRoles:list-items  and
                         cSelectedCards  = slSelectedRoles:list-items   and
                         cComments       = eComments:input-value) no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fUid fName fInitial cbDept fEmail slAvailableRoles slSelectedRoles 
          tActive flCreatedate flPasswordSetDate eComments tPasswordExpired 
      WITH FRAME Dialog-Frame.
  ENABLE bAddAll fName fInitial bRemoveAll cbDept fEmail slAvailableRoles 
         slSelectedRoles eComments BtnCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getSysRoles Dialog-Frame 
PROCEDURE getSysRoles :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Local variable */
  define variable cRoleList as character no-undo.
  
  do with frame {&frame-name}:
  end.

  publish "getSysRoleList" (input  ",",  /* Delimiter */
                            output cRoleList,
                            output std-lo,
                            output std-ch).

  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.

  slAvailableRoles:list-items = "*".
  if cRoleList = "" 
   then
    return.

  slAvailableRoles:list-items = slAvailableRoles:list-items + "," + cRoleList.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveSysUser Dialog-Frame 
PROCEDURE saveSysUser :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end.
    
  if not available sysuser
   then return.
  /* Assigning new values in the existing temp table record */
  assign 
      sysuser.name          = fName:input-value
      sysuser.initials      = fInitial:input-value
      sysuser.email         = fEmail:input-value
      sysuser.role          = slSelectedRoles:list-items      
      sysuser.department    = cbDept:input-value
      sysuser.comments      = eComments:input-value
      .
   
  publish "modifySysUser" (input table sysuser,
                           output oplSuccess,
                           output std-ch ).

  if not oplSuccess
   then
    message std-ch
      view-as alert-box info buttons ok.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

