&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File:wsyskey.w 

  Description:User Interface for system keys. 

  Input Parameters:

  Output Parameters:

  Author: Rahul 

  Created:04.11.2018 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */

{lib/winshowscrollbars.i}
{lib/get-column.i}

{lib/std-def.i}
{lib/sys-def.i}

{tt/syskey.i}
{tt/syskey.i &tableAlias="tsyskey"}
{tt/syskey.i &tableAlias="ttsyskey"}

/* Local Variable Definitions ---                                       */
define variable dColumnWidth as decimal no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwsyskey

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES sysKey

/* Definitions for BROWSE brwsyskey                                     */
&Scoped-define FIELDS-IN-QUERY-brwsyskey syskey.type sysKey.Sequence sysKey.username sysKey.changedate sysKey.notes   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwsyskey   
&Scoped-define SELF-NAME brwsyskey
&Scoped-define QUERY-STRING-brwsyskey for each sysKey
&Scoped-define OPEN-QUERY-brwsyskey open query {&SELF-NAME} for each sysKey.
&Scoped-define TABLES-IN-QUERY-brwsyskey sysKey
&Scoped-define FIRST-TABLE-IN-QUERY-brwsyskey sysKey


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwsyskey}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bCopy RECT-62 RECT-63 bSearch fSearch bNew ~
brwsyskey bRefresh 
&Scoped-Define DISPLAYED-OBJECTS fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCopy  NO-FOCUS
     LABEL "Copy" 
     SIZE 7.2 BY 1.71 TOOLTIP "Copy".

DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete".

DEFINE BUTTON bEditsyskey  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Edit".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "New".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search".

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 46.8 BY 1 TOOLTIP "Search Criteria (Type,Username)" NO-UNDO.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 46.6 BY 2.24.

DEFINE RECTANGLE RECT-63
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 57.6 BY 2.24.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwsyskey FOR 
      sysKey SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwsyskey
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwsyskey C-Win _FREEFORM
  QUERY brwsyskey DISPLAY
      syskey.type         label "Type"           format "x(20)"    width 20
sysKey.Sequence     label "Sequence"             format "x(20)"    width 20
sysKey.username     label "Last Modified by"     format "x(28)"    width 27.6
sysKey.changedate   label "Last Modified"        format "99/99/99" width 14
sysKey.notes        label "Notes"                format "x(20)"    width 20
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SEPARATORS SIZE 125 BY 13.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bCopy AT ROW 1.67 COL 40 WIDGET-ID 320 NO-TAB-STOP 
     bDelete AT ROW 1.67 COL 32.6 WIDGET-ID 206 NO-TAB-STOP 
     bSearch AT ROW 1.67 COL 97.2 WIDGET-ID 316 NO-TAB-STOP 
     fSearch AT ROW 2.05 COL 47.6 COLON-ALIGNED NO-LABEL WIDGET-ID 310
     bNew AT ROW 1.67 COL 18 WIDGET-ID 204 NO-TAB-STOP 
     brwsyskey AT ROW 4 COL 2 WIDGET-ID 500
     bEditsyskey AT ROW 1.67 COL 25.4 WIDGET-ID 298 NO-TAB-STOP 
     bExport AT ROW 1.67 COL 3.2 WIDGET-ID 2 NO-TAB-STOP 
     bRefresh AT ROW 1.67 COL 10.6 WIDGET-ID 158 NO-TAB-STOP 
     "Search" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.05 COL 49.2 WIDGET-ID 314
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.05 COL 2.8 WIDGET-ID 318
     RECT-62 AT ROW 1.43 COL 2 WIDGET-ID 308
     RECT-63 AT ROW 1.43 COL 48.2 WIDGET-ID 312
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 127.2 BY 20.19 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Key"
         HEIGHT             = 17
         WIDTH              = 126.6
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwsyskey bNew DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bDelete IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bDelete:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "A".

/* SETTINGS FOR BUTTON bEditsyskey IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bExport IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bNew:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "A".

ASSIGN 
       brwsyskey:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwsyskey:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwsyskey
/* Query rebuild information for BROWSE brwsyskey
     _START_FREEFORM
open query {&SELF-NAME} for each sysKey.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwsyskey */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Key */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Key */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closewindow in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Key */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCopy C-Win
ON CHOOSE OF bCopy IN FRAME DEFAULT-FRAME /* Copy */
do:
  run copySysKey in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME DEFAULT-FRAME /* Delete */
do:
  run deleteSysKey in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEditsyskey
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditsyskey C-Win
ON CHOOSE OF bEditsyskey IN FRAME DEFAULT-FRAME /* Edit */
do:
  run modifySysKey in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME DEFAULT-FRAME /* New */
do:
  run newSysKey in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME DEFAULT-FRAME /* Refresh */
do:
  run getData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwsyskey
&Scoped-define SELF-NAME brwsyskey
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwsyskey C-Win
ON DEFAULT-ACTION OF brwsyskey IN FRAME DEFAULT-FRAME
do:
  apply "choose" to bEditsyskey.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwsyskey C-Win
ON ROW-DISPLAY OF brwsyskey IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwsyskey C-Win
ON START-SEARCH OF brwsyskey IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwsyskey C-Win
ON VALUE-CHANGED OF brwsyskey IN FRAME DEFAULT-FRAME
do:
  run setButtons in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME DEFAULT-FRAME
DO:
  apply "CHOOSE" to bSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

assign current-window                = {&window-name} 
       this-procedure:current-window = {&window-name}.

{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

subscribe to "closeWindow" anywhere.
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
on close of this-procedure 
  run disable_UI.

/* Best default for GUI applications is...                              */
pause 0 before-hide.

bExport    :load-image("images/excel.bmp").
bExport    :load-image-insensitive("images/excel-i.bmp").
bRefresh   :load-image ("images/refresh.bmp").
bRefresh   :load-image-insensitive("images/refresh-i.bmp").
bNew       :load-image("images/add.bmp").
bNew       :load-image-insensitive("images/add-i.bmp").
bEditsysKey:load-image("images/update.bmp").
bEditsysKey:load-image-insensitive("images/update-i.bmp").
bdelete    :load-image("images/delete.bmp").
bdelete    :load-image-insensitive("images/delete-i.bmp").
bSearch    :load-image("images/magnifier.bmp").
bSearch    :load-image-insensitive("images/magnifier-i.bmp").
bcopy      :load-image-up("images/copy.bmp").
bcopy      :load-image-insensitive("images/copy-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.

  {lib/get-column-width.i &col="'username'" &var=dColumnWidth}

  run getData in this-procedure.
  
  /* disbale/enable buttons */
  run setButtons in this-procedure.

  /* Maximize the current window while fetching data */
  run showwindow in this-procedure.

  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 apply "close":u to this-procedure.
 return no-apply.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copySysKey C-Win 
PROCEDURE copySysKey :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount       as integer no-undo.
  
  define buffer ttsysKey for ttsysKey.
  empty temp-table ttsysKey.

  do with frame {&frame-name}:
  end.

  if not available sysKey
   then
    return.
 
  create ttsysKey.
  buffer-copy sysKey to ttsysKey.
  std-ro = rowid(sysKey).
   
  run dialogsyskey.w (input {&Copy},  /* for copying the data */
                      input-output table ttsysKey,
                      output std-lo).

  if not std-lo /* if no server call made then return */
   then
    return.

  do iCount = 1 to brwSyskey:num-iterations:
    if brwSyskey:is-row-selected(iCount)
     then
      leave.
  end.

  /* adding new sysKey in the local temp table */
  find first ttsysKey no-error.
  if available ttsysKey and 
   not can-find(first sysKey where sysKey.type = ttsysKey.type) 
   then
    do:
      create sysKey.
      buffer-copy ttsysKey to sysKey
      assign 
          sysKey.sequence = string(ttsysKey.seq)
          std-ro = rowid(sysKey)
           .

      /* display the data on browse */
      run displaySysKey in this-procedure.
      brwSyskey:set-repositioned-row(iCount,"ALWAYS").
      reposition {&browse-name} to rowid std-ro.
      setStatusCount (query brwSyskey:num-results).

    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteSysKey C-Win 
PROCEDURE deleteSysKey :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cType    as character no-undo.
 
  if not available sysKey
   then
    return.
  
  cType = sysKey.type.
 
  message "Highlighted System Key will be deleted." skip "Do you want to continue?"                                                                                                                        
    view-as alert-box info buttons yes-no                                                                                                                                                 
      title "Delete System Key"                                                                                                                                                                 
        update lContinue as logical.  
 
  if not lContinue
   then
    return.
                                                                                                                                                                                   
   /* deleting the sysKey */
   run server/deletesyskey.p (input  sysKey.type, /* sysKey type */
                              output std-lo,
                              output std-ch).                                                                                                                                                                            
  
   if not std-lo /* If not success */
    then 
     do:
       message std-ch
         view-as alert-box info buttons ok.
       return.
     end.
    
   /* deleting the sysKey from temptable */
   find first sysKey where sysKey.type = cType no-error.
   if available sysKey
    then
     delete sysKey. 
 
   /* display the records on browse */
   run displaySysKey in this-procedure.
   setStatusCount (query brwSyskey:num-results).
   apply 'value-changed' to browse brwsyskey.                                                                                                                                                             

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displaySysKey C-Win 
PROCEDURE displaySysKey :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  open query brwsyskey preselect each sysKey by sysKey.type.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fSearch 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bCopy RECT-62 RECT-63 bSearch fSearch bNew brwsyskey bRefresh 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable htable as handle no-undo.
  if query brwsyskey:num-results = 0 then
  do: 
    message "There is nothing to export"
      view-as alert-box warning buttons ok.
    return.
  end.
  
  publish "GetReportDir" (output std-ch).
  htable = temp-table sysKey:handle.
  run util/exporttable.p (table-handle htable,
                          "Syskey",
                          "for each sysKey",
                          "type,seq,Username,changedate,notes",
                          "Type,Sequence,Last Modified by,Last Modified,Notes",
                          std-ch,
                          "SystemKey-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in). 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 close query brwSyskey.

 empty temp-table sysKey.

 do with frame {&frame-name}:
 end.

 for each tsysKey:
   /* test if the record contains the search text */
   if trim(fSearch:screen-value) <> " " and
     not ( (tsysKey.type        matches "*" + trim(fSearch:screen-value) + "*")  or
           (tsysKey.username    matches "*" + trim(fSearch:screen-value) + "*")  or
           (string(tsysKey.seq) matches "*" + trim(fSearch:screen-value) + "*")  or
           (tsysKey.notes       matches "*" + trim(fSearch:screen-value) + "*") )
     then next.

   create sysKey.
   buffer-copy tsysKey to sysKey.
   assign
       sysKey.sequence = string(tsysKey.seq)
       .
 end.

 /* display the data on browse */
 run displaysyskey in this-procedure.

 setStatusCount (query brwSyskey:num-results).
 run setButtons in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table tsysKey.
  empty temp-table sysKey.

  /* server call to get the sysKey records */
  run server/getsyskeys.p (output table tsysKey,
                           output std-lo,
                           output std-ch).
 
  if not std-lo   /* not success return */
   then
    do:
      message std-ch
        view-as alert-box info buttons ok.
      return.
    end.
   
  run filterData in this-procedure.
  setStatusRecords(query brwSyskey:num-results).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifySysKey C-Win 
PROCEDURE modifySysKey :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount       as integer no-undo.
  
  define buffer ttSysKey for ttSysKey.
  empty temp-table ttsysKey.
  
  do with frame {&frame-name}:
  end.

  if not available sysKey
   then
    return.

  create ttsysKey.
  buffer-copy sysKey to ttsysKey.

  run dialogsyskey.w (input {&Modify},  /* Modifydata */
                      input-output table ttsysKey,
                      output std-lo). 

  if not std-lo /* if no server call */
   then
    return.
  
  
  do iCount = 1 to brwSyskey:num-iterations:
    if brwSyskey:is-row-selected(iCount)
     then
      leave.
  end.

  /* Updating the existing sysKey */

  find first ttsysKey no-error.
  if available ttsysKey and 
    can-find (first sysKey where sysKey.type = ttsysKey.type) 
   then
    do:
      buffer-copy ttsysKey except ttsysKey.type to sysKey.
      assign
          sysKey.sequence = string(ttsysKey.seq)
          std-ro          = rowid(sysKey)
          .
      /* displaying the data on browse */
      run displaySysKey in this-procedure.
      brwSyskey:set-repositioned-row(iCount,"ALWAYS").
      reposition brwsyskey to rowid std-ro no-error.
      apply 'value-changed' to browse brwsyskey.
      setStatusCount (query brwSyskey:num-results).
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newSysKey C-Win 
PROCEDURE newSysKey :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount       as integer no-undo.
  
  define buffer ttSysKey for ttSysKey.
  empty temp-table ttsysKey.

  do with frame {&frame-name}:
  end.

  run dialogsyskey.w (input {&new},
                      input-output table ttsysKey,
                      output std-lo). 

  if not std-lo /* if not success then return */
   then
    return.
  
  do iCount = 1 to brwSyskey:num-iterations:
    if brwSyskey:is-row-selected(iCount)
     then
      leave.
  end.

  /* adding new sysKey in the local temp table */
  find first ttsysKey no-error.
  if available ttsysKey and 
    not can-find(first sysKey where sysKey.type = ttsysKey.type) 
   then
    do:
      create sysKey.
      buffer-copy ttsysKey to sysKey.
      assign 
         sysKey.sequence = string(ttsysKey.seq)
         std-ro          = rowid(sysKey)
         .
      /* displaying the data on browse */
      run displaySysKey in this-procedure.
      brwSyskey:set-repositioned-row(iCount,"ALWAYS").
      reposition {&browse-name} to rowid std-ro.   
      setStatusCount (query brwSyskey:num-results).

    end.
  apply 'value-changed' to browse brwsyskey.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setButtons C-Win 
PROCEDURE setButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if query brwsyskey:num-results > 0 
   then
    assign 
      bExport    :sensitive  = true
      bEditsyskey:sensitive  = true
      bDelete    :sensitive  = true
      bcopy      :sensitive  = true
      .
   else
    assign
      bExport    :sensitive = false
      bEditsyskey:sensitive = false
      bDelete    :sensitive = false
      bcopy      :sensitive = false
      .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized then
     {&window-name}:window-state = window-normal .
  
   c-Win:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/brw-sortData.i}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 assign frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
        frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
        frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
        frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
        {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 10
        {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 1.

  {lib/resize-column.i &col="'type,username,notes'" &var=dColumnWidth}
  run ShowScrollBars(browse brwSyskey:handle, no, yes).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

