&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
    File        : compilegui.w
    Purpose     : Compiles all client GUI programs under a given folder.

    Author(s)   : Bryan Johnson
    Created     : 08.03.2014

    Description : Program uses an initialization file that is loaded
                  via util/ini.p.  The initialization file contains 
                  the following parameters:
                  
                  SourceFolder - folder containing source code
                  SaveInto - destination folder for r-code
                  LogFile - full pathname of log file
                  ErrorFile - full pathname of error file
                  
  ----------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

&global-define debugFile C:\ANTIC\Compass\scripts\compiler_client_debug.txt

def var debugMode       as log  no-undo init no.
def var i               as int  no-undo.
def var c               as char no-undo.
def var TotalCount      as int  no-undo.
def var SuccessCount    as int  no-undo.
def var ErrorCount      as int  no-undo.

def var iParameterList  as char no-undo.
def var iSourceFolder   as char no-undo.
def var iSaveInto       as char no-undo.
def var iLogFile        as char no-undo.
def var iErrorFile      as char no-undo.
def var iDebugMode      as char no-undo.

/* For testing */
/* def var tSourceFolder   as char no-undo init "C:\ANTIC\CompassRC\ops_rc".                             */
/* def var tSaveInto       as char no-undo init "C:\ANTIC\CompassRC\ops_rx".                             */
/* def var tLogFile        as char no-undo init "C:\ANTIC\Compass\scripts\compiler_ops_client_log.txt".  */
/* def var tErrorFile      as char no-undo init "C:\ANTIC\Compass\scripts\compiler_ops_client_err.txt".  */
/* def var tDebugMode      as char no-undo init "yes".                                                   */

def var tFatalFault     as log  no-undo init no.

def temp-table ttFileList
    field basename   as char
    field pathname   as char
    field attributes as char
    field level      as int.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tSourceFolder tSaveInto tLogFile tErrorFile ~
tDebugMode tMessages bStart 
&Scoped-Define DISPLAYED-OBJECTS tSourceFolder tSaveInto tLogFile ~
tErrorFile tDebugMode tMessages 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD closeLog C-Win 
FUNCTION closeLog RETURNS LOGICAL
  ( pFilename as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD debug C-Win 
FUNCTION debug RETURNS LOGICAL
  ( pMsg as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD fault C-Win 
FUNCTION fault RETURNS LOGICAL
  ( pMsg as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD log C-Win 
FUNCTION log RETURNS LOGICAL
  ( pMsg as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD msg C-Win 
FUNCTION msg RETURNS LOGICAL
  ( pMsg as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openLog C-Win 
FUNCTION openLog RETURNS LOGICAL
  (pFilename as char)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bStart 
     LABEL "Start Compile" 
     SIZE 20 BY 1.14.

DEFINE VARIABLE tMessages AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL LARGE
     SIZE 125.4 BY 16.05 NO-UNDO.

DEFINE VARIABLE tErrorFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "Error File" 
     VIEW-AS FILL-IN 
     SIZE 111 BY 1 NO-UNDO.

DEFINE VARIABLE tLogFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "Log File" 
     VIEW-AS FILL-IN 
     SIZE 111 BY 1 NO-UNDO.

DEFINE VARIABLE tSaveInto AS CHARACTER FORMAT "X(256)":U 
     LABEL "Save Into" 
     VIEW-AS FILL-IN 
     SIZE 111 BY 1 NO-UNDO.

DEFINE VARIABLE tSourceFolder AS CHARACTER FORMAT "X(256)":U 
     LABEL "Source Folder" 
     VIEW-AS FILL-IN 
     SIZE 111 BY 1 NO-UNDO.

DEFINE VARIABLE tDebugMode AS LOGICAL INITIAL no 
     LABEL "Debug Mode" 
     VIEW-AS TOGGLE-BOX
     SIZE 25 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     tSourceFolder AT ROW 1.71 COL 15 COLON-ALIGNED WIDGET-ID 6
     tSaveInto AT ROW 2.91 COL 15 COLON-ALIGNED WIDGET-ID 8
     tLogFile AT ROW 4.1 COL 15 COLON-ALIGNED WIDGET-ID 10
     tErrorFile AT ROW 5.29 COL 15 COLON-ALIGNED WIDGET-ID 12
     tDebugMode AT ROW 6.71 COL 17 WIDGET-ID 14
     tMessages AT ROW 8.14 COL 2.6 NO-LABEL WIDGET-ID 2
     bStart AT ROW 24.67 COL 55.4 WIDGET-ID 4
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 128.6 BY 25.48 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Compass Client Compiler"
         HEIGHT             = 25.48
         WIDTH              = 128.6
         MAX-HEIGHT         = 25.48
         MAX-WIDTH          = 128.6
         VIRTUAL-HEIGHT     = 25.48
         VIRTUAL-WIDTH      = 128.6
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
ASSIGN 
       tErrorFile:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tLogFile:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tMessages:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tSaveInto:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tSourceFolder:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Compass Client Compiler */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Compass Client Compiler */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  /*RETURN NO-APPLY.*/
  quit.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bStart
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bStart C-Win
ON CHOOSE OF bStart IN FRAME DEFAULT-FRAME /* Start Compile */
DO:
  debugMode = tDebugMode:checked.
  run compileApp.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

session:immediate-display = yes.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
    run util/ini.p persistent.
    
    publish "GetSystemParametersList" (output iParameterList).
    
    if iParameterList = ? or iParameterList = "" then
    do:
        message
            "Could not retrieve parameters from INI file or INI file not specified."
            view-as alert-box error.
        
        bStart:sensitive = no.
    end.
    
    publish "GetSystemParameter" ("SourceFolder", output iSourceFolder).
    publish "GetSystemParameter" ("SaveInto", output iSaveInto).
    publish "GetSystemParameter" ("LogFile", output iLogFile).
    publish "GetSystemParameter" ("ErrorFile", output iErrorFile).
    publish "GetSystemParameter" ("DebugMode", output iDebugMode).
    
    if iDebugMode <> ? and iDebugMode <> "" then
    debugMode = logical(iDebugMode).  
  
    tSourceFolder:screen-value = iSourceFolder.
    tSaveInto:screen-value = iSaveInto.
    tLogFile:screen-value = iLogFile.
    tErrorFile:screen-value = iErrorFile.
    tDebugMode:checked = debugMode.
    
    apply 'entry' to bStart.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE compileApp C-Win 
PROCEDURE compileApp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

do with frame {&frame-name}:

tFatalFault = no.
TotalCount = 0.
SuccessCount = 0.
ErrorCount = 0.
tMessages:screen-value = "".
bStart:sensitive = no.
disable bStart.
bStart:label = "Compiling...".

openLog(iLogFile).
openLog(iErrorFile).

if debugMode then
do:
    openLog("{&debugFile}").
    do i = 1 to num-entries(iParameterList):
        publish "GetSystemParameter" (entry(i,iParameterList), output c).
        debug(entry(i,iParameterList) + "=" + c).
    end.
    debug("").
end.


file-info:file-name = iSourceFolder.
if index(file-info:file-type, "D") = 0 then
do: 
    fault("Source folder: " + iSourceFolder + " not found.").
    tFatalFault = yes.
end.

file-info:file-name = iSaveInto.
if index(file-info:file-type, "D") = 0 then
do: 
    fault("Save into folder: " + iSaveInto + " not found.").
    tFatalFault = yes.
end.
if index(file-info:file-type, "W") = 0 then
do: 
    fault("Save into folder: " + iSaveInto + " is not writable.").
    tFatalFault = yes.
end.

if debugMode then
do:
    debug("tFatalFault=" + string(tFatalFault)).
    debug("").
end.

if not tFatalFault then
do:
    /* Remove all r-code in destination folder */
    os-command silent value("del /s /q " + iSaveInto + "\*.r").    
    
    if debugMode then
    debug("Begin root compileFolder, level 0").
    
    run compileFolder(iSourceFolder, iSaveInto, 0).
    
    if debugMode then
    debug("End root compileFolder, level 0").
    
    log("").
    log("Total Program Count  : " + string(TotalCount)).
    log("Compiled Successfully: " + string(SuccessCount)).
    log("Failed to Compile    : " + string(ErrorCount)).
    log("").
    
    fault("").
    fault("Total Program Count  : " + string(TotalCount)).
    fault("Compiled Successfully: " + string(SuccessCount)).
    fault("Failed to Compile    : " + string(ErrorCount)).
    fault("").
    
    msg("").
    msg("Total Program Count: " + string(TotalCount)).
    msg("Compiled Successfully: " + string(SuccessCount)).
    msg("Failed to Compile: " + string(ErrorCount)).
    msg("").
end.

if debugMode then
do:
    closeLog("{&debugFile}").
end.

closeLog(iErrorFile).
closeLog(iLogFile).

if tFatalFault = yes then
message
    "A fatal error has occurred.  Compile failed." skip
    "Check the error log for details:" skip
    iErrorFile
    view-as alert-box error.

bStart:label = "Start Compile".
bStart:sensitive = yes.
enable bStart.

end. /* do with frame... */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE compileFile C-Win 
PROCEDURE compileFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

def input param pFile     as char.
def input param pSaveInto as char.

def var tSaveInto as char.

msg("Compiling File: " + pFile).
TotalCount = TotalCount + 1.

/* When Progress compiles class files (.cls) using SAVE INTO, it saves the
   resulting .r files in a folder structure that preserves the hierarchy of the
   class, relative to the PROPATH.  It will create the necessary folders
   in order to do this if they don't already exist.
   
   So, for classes I have to adjust the SAVE INTO path to account for
   the compiler's creation of the class file folder structure.
*/
if index(pFile,".cls") <> 0 then
do i = 1 to num-entries(propath):
    if pFile begins entry(i,propath) then
    do:
        assign tSaveInto = replace(entry(i,propath), iSourceFolder, iSaveInto).
        leave.
    end.
end.
else
assign tSaveInto = pSaveInto.

if debugMode then
do:
    debug("CompileFile: pFile=" + pFile + ", pSaveInto=" + pSaveInto + ", tSaveInto=" + tSaveInto).
end.

compile value(pFile) save into value(tSaveInto) min-size = true no-error. 
       
if compiler:error = false then
do:
    log(pFile + " compiled successfully.").
    SuccessCount = SuccessCount + 1.
end.
else 
do:
    fault(pFile + " failed at " + string(compiler:error-row) + " " + compiler:get-message(1)).
    ErrorCount = ErrorCount + 1.
end.

return "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE compileFolder C-Win 
PROCEDURE compileFolder :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

def input param pFolder   as char.
def input param pSaveInto as char.
def input param pLevel    as int.

def buffer ttFileList for ttFileList.

def var tError as int no-undo.
def var fileExt as char no-undo.

msg("Compiling Folder: " + pFolder).

if debugMode then
do:
    debug("CompileFolder: pFolder=" + pFolder + ", pSaveInto=" + pSaveInto + ", pLevel=" + string(pLevel)).
end.

file-info:file-name = pSaveInto.
if file-info:full-pathname = ? then
do:
    os-create-dir value(pSaveInto).
    tError = os-error.
    if tError <> 0 then 
    do:
        if debugMode then
        do:
            debug("CompileFolder - error creating folder: " + pSaveInto + " - Error=" + string(tError)).
        end.
        
        fault("CompileFolder - error creating folder: " + pSaveInto + " - Error=" + string(tError)).
        return "ERROR".
    end.
    else 
    do:
        if debugMode then
        do:
            debug("CompileFolder - created folder: " + pSaveInto).
        end.
    
        log("CompileFolder - created folder: " + pSaveInto).
    end.
end.

                  
input from os-dir(pFolder).
repeat:
    create ttFileList.
    import ttFileList.
    assign ttFileList.level = pLevel.
end.
input close.


/* Filter file list to only folders and progress source code. */
repeat preselect each ttFileList where ttFileList.level = pLevel:

    find next ttFileList.
    
    if ttFileList.basename = ?
    or trim(ttFileList.basename) = ""
    or trim(ttFileList.basename) = "?"
    or trim(ttFileList.basename) = "."
    or trim(ttFileList.basename) = ".." then
    do:
        delete ttFileList.
        next.
    end.
    
    /* Keep in list if it's a folder/directory */
    if index(ttFileList.attributes,"D") <> 0 then
    next.
    
    /* Keep in list if it's a progress source file. */
    fileExt = trim(substring(ttFileList.basename, r-index(ttFileList.basename,"."))).
    if lookup(fileExt, ".w,.p,.cls") <> 0 then
    next.

    /* Otherwise get rid of it. */
    delete ttFileList.
end.

/* if debugMode then                                           */
/* do:                                                       */
/*     debug("File List (ttFileList):").                     */
/*     for each ttFileList where ttFileList.level = pLevel:  */
/*         debug(ttFileList.basename + "," +                 */
/*               ttFileList.pathname + "," +                 */
/*               ttFileList.attributes + "," +               */
/*               string(ttFileList.level)).                  */
/*     end.                                                  */
/*     debug("End File List").                               */
/* end.                                                      */
    
repeat preselect each ttFileList where ttFileList.level = pLevel:

    find next ttFileList.

    /* For some reason there seems to be extra blank records in the temp-table
       at this point even though they should have been removed above.  I think
       this may be a result of the recursive calls.
    */
    if ttFileList.basename = "" then
    do:
        delete ttFileList.
        next.
    end.

/*     if debugMode then                                                            */
/*     do:                                                                        */
/*         debug("CompileFolder - ttFileList: basename=" + ttFileList.basename +  */
/*               ", pathname=" + ttFileList.pathname +                            */
/*               ", attributes=" + ttFileList.attributes +                        */
/*               ", level=" + string(ttFileList.level)).                          */
/*     end.                                                                       */
    
    if index(ttFileList.attributes,"D") <> 0 then
    do:
        run compileFolder(trim(ttFileList.pathname), pSaveInto + "\" + trim(ttFileList.basename), pLevel + 1).
        if return-value = "ERROR" then
        do:
            if debugMode then
            debug("Error in CompileFolder").
            
            fault("Error in CompileFolder").
        end.
    end.
    else
    do:
        if index(ttFileList.attributes,"F") <> 0 then
        do:
            run compileFile(trim(ttFileList.pathname), pSaveInto).
            if return-value = "ERROR" then
            do:
                if debugMode then
                debug("Error in CompileFile").
                
                fault("Error in CompileFile").
            end.
        end.
    end.
    
    delete ttFileList.
end.

return "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tSourceFolder tSaveInto tLogFile tErrorFile tDebugMode tMessages 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE tSourceFolder tSaveInto tLogFile tErrorFile tDebugMode tMessages 
         bStart 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION closeLog C-Win 
FUNCTION closeLog RETURNS LOGICAL
  ( pFilename as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 output to value(pFilename) append.
 put unformatted "End of compile session: " + string(now) skip.
 output close.
 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION debug C-Win 
FUNCTION debug RETURNS LOGICAL
  ( pMsg as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 output to value("{&debugFile}") page-size 0 append.
 put unformatted now.
 put unformatted " " pMsg skip.
 output close.

 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION fault C-Win 
FUNCTION fault RETURNS LOGICAL
  ( pMsg as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 output to value(iErrorFile) page-size 0 append.
 put unformatted now.
 put unformatted " " pMsg skip.
 output close.

 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION log C-Win 
FUNCTION log RETURNS LOGICAL
  ( pMsg as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 output to value(iLogFile) page-size 0 append.
 put unformatted now.
 put unformatted " " pMsg skip.
 output close.

 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION msg C-Win 
FUNCTION msg RETURNS LOGICAL
  ( pMsg as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  do with frame {&frame-name}:
    tMessages:insert-string(pMsg + chr(10)).
  end.

  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openLog C-Win 
FUNCTION openLog RETURNS LOGICAL
  (pFilename as char) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 output to value(pFilename) page-size 0.
 put unformatted "Start of compile session: " + string(now) skip.
 output close.
 RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

