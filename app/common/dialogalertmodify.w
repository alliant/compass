&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Alert Modify
@description View the details of the alert

@author John Oliver
@version 1.0
@created 2017/10/09
@notes 
---------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
/* Parameters Definitions ---                                           */
define input parameter hFileDataSrv as handle no-undo.

/* Local Variable Definitions ---                                       */
define variable hNoteAdd as handle no-undo.
define variable iAlertID as integer no-undo.
define variable isClosed as logical no-undo.

{lib/std-def.i}
{lib/add-delimiter.i}
{tt/alert.i}
{tt/alertnote.i}
{tt/agent.i}
{tt/state.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fAlert

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-38 RECT-39 RECT-40 bClose tStat ~
tAgentID tCodeText tThreshold tScore tDescription bSave tEffDate tCreated ~
tCreatedBy tClosed tClosedBy tNoteCategory tNoteOrder tNoteDesc 
&Scoped-Define DISPLAYED-OBJECTS tStateID tStat tAgentID tOwner tCodeText ~
tCode tThreshold tScore tSeverity tDescription tEffDate tCreated tCreatedBy ~
tClosed tClosedBy tNoteCategory tNoteOrder tNoteDesc 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doModify C-Win 
FUNCTION doModify RETURNS LOGICAL
  ( input pModify as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bClose 
     LABEL "Close" 
     SIZE 6 BY 1.14 TOOLTIP "Close the Alert".

DEFINE BUTTON bNoteNew AUTO-END-KEY 
     LABEL "New" 
     SIZE 4.8 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Alert Type" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "None","None"
     DROP-DOWN-LIST
     SIZE 69 BY 1 NO-UNDO.

DEFINE VARIABLE tNoteCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 26.4 BY 1 NO-UNDO.

DEFINE VARIABLE tOwner AS CHARACTER FORMAT "X(256)":U 
     LABEL "Owner" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "None","None"
     DROP-DOWN-LIST
     SIZE 69 BY 1 NO-UNDO.

DEFINE VARIABLE tSeverity AS CHARACTER FORMAT "x(10)":U 
     LABEL "Severity" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "None","0"
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "None","None"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tDescription AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 69 BY 1.67 NO-UNDO.

DEFINE VARIABLE tNoteDesc AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 69.4 BY 8.81 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 71 BY 1 NO-UNDO.

DEFINE VARIABLE tClosed AS DATETIME FORMAT "99/99/9999":U 
     LABEL "Closed" 
     VIEW-AS FILL-IN 
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE tClosedBy AS CHARACTER FORMAT "X(256)":U 
     LABEL "Closed By" 
     VIEW-AS FILL-IN 
     SIZE 26.4 BY 1 NO-UNDO.

DEFINE VARIABLE tCodeText AS CHARACTER FORMAT "X(256)":U 
     LABEL "Alert Type" 
     VIEW-AS FILL-IN 
     SIZE 69 BY 1 NO-UNDO.

DEFINE VARIABLE tCreated AS DATETIME FORMAT "99/99/9999":U 
     LABEL "Created" 
     VIEW-AS FILL-IN 
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE tCreatedBy AS CHARACTER FORMAT "X(256)":U 
     LABEL "Created By" 
     VIEW-AS FILL-IN 
     SIZE 26.4 BY 1 NO-UNDO.

DEFINE VARIABLE tEffDate AS DATETIME FORMAT "99/99/9999":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN 
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE tScore AS DECIMAL FORMAT "->,>>>,>>9.99":U INITIAL 0 
     LABEL "Measure" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tStat AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tThreshold AS DECIMAL FORMAT "->,>>>,>>9.99":U INITIAL 0 
     LABEL "Threshold" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 85 BY 7.62.

DEFINE RECTANGLE RECT-39
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 85 BY 4.76.

DEFINE RECTANGLE RECT-40
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 85 BY 11.43.

DEFINE VARIABLE tNoteOrder AS LOGICAL INITIAL no 
     LABEL "Descending Order" 
     VIEW-AS TOGGLE-BOX
     SIZE 21.4 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fAlert
     bClose AT ROW 1.38 COL 82.2 WIDGET-ID 270
     tStateID AT ROW 1.48 COL 15 COLON-ALIGNED WIDGET-ID 166
     tStat AT ROW 1.48 COL 59.6 COLON-ALIGNED WIDGET-ID 268
     tAgentID AT ROW 2.67 COL 15 COLON-ALIGNED WIDGET-ID 266
     tOwner AT ROW 4.81 COL 15 COLON-ALIGNED WIDGET-ID 210
     tCodeText AT ROW 6 COL 15 COLON-ALIGNED WIDGET-ID 264
     tCode AT ROW 6 COL 15 COLON-ALIGNED WIDGET-ID 208
     tThreshold AT ROW 7.19 COL 15 COLON-ALIGNED WIDGET-ID 212
     tScore AT ROW 7.19 COL 42 COLON-ALIGNED WIDGET-ID 214
     tSeverity AT ROW 7.19 COL 68 COLON-ALIGNED WIDGET-ID 248
     tDescription AT ROW 8.38 COL 16.8 NO-LABEL WIDGET-ID 254
     bSave AT ROW 10.29 COL 38 WIDGET-ID 134
     tEffDate AT ROW 13.38 COL 14 COLON-ALIGNED WIDGET-ID 224
     tCreated AT ROW 14.57 COL 14 COLON-ALIGNED WIDGET-ID 226
     tCreatedBy AT ROW 14.57 COL 57 COLON-ALIGNED WIDGET-ID 244
     tClosed AT ROW 15.76 COL 14 COLON-ALIGNED WIDGET-ID 228
     tClosedBy AT ROW 15.76 COL 57 COLON-ALIGNED WIDGET-ID 242
     bNoteNew AT ROW 18.62 COL 80.6 WIDGET-ID 240
     tNoteCategory AT ROW 18.67 COL 13.8 COLON-ALIGNED WIDGET-ID 236
     tNoteOrder AT ROW 18.76 COL 44.6 WIDGET-ID 262
     tNoteDesc AT ROW 19.86 COL 15.8 NO-LABEL WIDGET-ID 234
     "Notes:" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 20 COL 9 WIDGET-ID 238
     "Description:" VIEW-AS TEXT
          SIZE 11.8 BY .62 AT ROW 8.48 COL 5 WIDGET-ID 256
     "Alert" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 4.1 COL 4 WIDGET-ID 206
          FONT 6
     "Dates" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 12.43 COL 4 WIDGET-ID 222
          FONT 6
     "Notes" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 17.67 COL 4 WIDGET-ID 232
          FONT 6
     RECT-38 AT ROW 4.33 COL 3 WIDGET-ID 204
     RECT-39 AT ROW 12.67 COL 3 WIDGET-ID 220
     RECT-40 AT ROW 17.91 COL 3 WIDGET-ID 230
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 88.8 BY 29.81 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Alert"
         HEIGHT             = 29.81
         WIDTH              = 88.8
         MAX-HEIGHT         = 30.95
         MAX-WIDTH          = 106.2
         VIRTUAL-HEIGHT     = 30.95
         VIRTUAL-WIDTH      = 106.2
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fAlert
   FRAME-NAME                                                           */
/* SETTINGS FOR BUTTON bNoteNew IN FRAME fAlert
   NO-ENABLE                                                            */
ASSIGN 
       tAgentID:READ-ONLY IN FRAME fAlert        = TRUE.

ASSIGN 
       tClosed:READ-ONLY IN FRAME fAlert        = TRUE.

ASSIGN 
       tClosedBy:READ-ONLY IN FRAME fAlert        = TRUE.

/* SETTINGS FOR COMBO-BOX tCode IN FRAME fAlert
   NO-ENABLE                                                            */
ASSIGN 
       tCodeText:HIDDEN IN FRAME fAlert           = TRUE.

ASSIGN 
       tCreated:READ-ONLY IN FRAME fAlert        = TRUE.

ASSIGN 
       tCreatedBy:READ-ONLY IN FRAME fAlert        = TRUE.

ASSIGN 
       tDescription:READ-ONLY IN FRAME fAlert        = TRUE.

ASSIGN 
       tEffDate:READ-ONLY IN FRAME fAlert        = TRUE.

ASSIGN 
       tNoteDesc:READ-ONLY IN FRAME fAlert        = TRUE.

/* SETTINGS FOR COMBO-BOX tOwner IN FRAME fAlert
   NO-ENABLE                                                            */
ASSIGN 
       tScore:READ-ONLY IN FRAME fAlert        = TRUE.

/* SETTINGS FOR COMBO-BOX tSeverity IN FRAME fAlert
   NO-ENABLE                                                            */
ASSIGN 
       tStat:READ-ONLY IN FRAME fAlert        = TRUE.

/* SETTINGS FOR COMBO-BOX tStateID IN FRAME fAlert
   NO-ENABLE                                                            */
ASSIGN 
       tThreshold:READ-ONLY IN FRAME fAlert        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Alert */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Alert */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClose
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClose C-Win
ON CHOOSE OF bClose IN FRAME fAlert /* Close */
DO:
  define variable cStatus as character no-undo.
  publish "GetSysPropDesc" ("AMD", "Alert", "Status", "C", output cStatus).
  {lib/confirm-status-change.i "Alert" cStatus}

  run CloseAlert in hFileDataSrv (output std-lo).
  if std-lo
   then
    do:
      run GetAlert in hFileDataSrv (output table alert).
      run LoadAlertNotes in hFileDataSrv.
      run SetFields in this-procedure.
      doModify(false).
    end. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNoteNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNoteNew C-Win
ON CHOOSE OF bNoteNew IN FRAME fAlert /* New */
DO:
  if valid-handle(hFileDataSrv)
   then
    if valid-handle(hNoteAdd)
     then run ShowWindow in hNoteAdd.
     else run dialogalertnoteadd.w persistent set hNoteAdd (hFileDataSrv).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME fAlert /* Save */
DO:
  run SaveAlert in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNoteCategory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNoteCategory C-Win
ON VALUE-CHANGED OF tNoteCategory IN FRAME fAlert /* Category */
DO:
  run SetNote in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNoteOrder
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNoteOrder C-Win
ON VALUE-CHANGED OF tNoteOrder IN FRAME fAlert /* Descending Order */
DO:
  run SetNote in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-icon.i}
subscribe to "AlertNoteChanged" anywhere.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bClose:load-image("images/s-close.bmp").
bClose:load-image-insensitive("images/s-close-i.bmp").
bNoteNew:load-image("images/s-add.bmp").
bNoteNew:load-image-insensitive("images/s-add-i.bmp").

/* get the agents */
publish "GetAgents" (output table agent).

/* set the alert type (codes) */
{lib/get-syscode-list.i &combo=tCode &codeType="'Alert'"}

/* set the alert owner */
{lib/get-sysprop-list.i &combo=tOwner &appCode="'AMD'" &objAction="'Alert'" &objProperty="'Owner'"}

/* set the alert severity */
{lib/get-sysprop-list.i &combo=tSeverity &appCode="'AMD'" &objAction="'Alert'" &objProperty="'Severity'"}

/* set the alert category */
{lib/get-sysprop-list.i &combo=tNoteCategory &appCode="'AMD'" &objAction="'AlertNote'" &objProperty="'Category'"}

/* get the agent */
if valid-handle(hFileDataSrv)
 then 
  do:
    run GetAlert in hFileDataSrv (output table alert).
    run LoadAlertNotes in hFileDataSrv.
  end.
 
for first alert no-lock:
  publish "GetSysCodeDesc" ("Alert", alert.processCode, output std-ch).
  for first agent no-lock
      where agent.agentID = alert.agentID:
    
    {&window-name}:title = std-ch + " for " + agent.name.
  end.
  isClosed = (alert.stat = "C").
end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
   
  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-state-list.i &combo=tStateID}
    /* set the values */
    {lib/set-current-value.i &state=tStateID}
  end.
  
  /* set the fields */
  doModify(not isClosed).
  run SetFields in this-procedure.
  run SetNote in this-procedure.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AlertNoteChanged C-Win 
PROCEDURE AlertNoteChanged :
/*------------------------------------------------------------------------------
@description Closes the window
------------------------------------------------------------------------------*/
  run SetNote in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseWindow C-Win 
PROCEDURE CloseWindow :
/*------------------------------------------------------------------------------
@description Closes the window
------------------------------------------------------------------------------*/
  if valid-handle(hNoteAdd)
   then run CloseWindow in hNoteAdd.
  APPLY "WINDOW-CLOSE":U TO {&window-name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tStateID tStat tAgentID tOwner tCodeText tCode tThreshold tScore 
          tSeverity tDescription tEffDate tCreated tCreatedBy tClosed tClosedBy 
          tNoteCategory tNoteOrder tNoteDesc 
      WITH FRAME fAlert IN WINDOW C-Win.
  ENABLE RECT-38 RECT-39 RECT-40 bClose tStat tAgentID tCodeText tThreshold 
         tScore tDescription bSave tEffDate tCreated tCreatedBy tClosed 
         tClosedBy tNoteCategory tNoteOrder tNoteDesc 
      WITH FRAME fAlert IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fAlert}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveAlert C-Win 
PROCEDURE SaveAlert :
/*------------------------------------------------------------------------------
@description Save the alert
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    for first alert exclusive-lock:
      assign
        /* scores */
        alert.owner = tOwner:input-value
        alert.severity = tSeverity:input-value
        alert.description = tDescription:input-value
        .
    end.
    run ModifyAlert in hFileDataSrv (table alert, output std-lo).
    if std-lo
     then
      do:
        doModify(false).
        run SetFields in this-procedure.
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetFields C-Win 
PROCEDURE SetFields :
/*------------------------------------------------------------------------------
@description Sets the fields
------------------------------------------------------------------------------*/
  define variable hQuery as handle no-undo.
  define variable hBuffer as handle no-undo.
  
  do with frame {&frame-name}:
    /* used to turn null values into blanks */
    create buffer hBuffer for table "alert".
    create query hQuery.
    hQuery:set-buffers(hBuffer).
    hQuery:query-prepare("for each alert").
    hQuery:query-open().
    hQuery:get-first().
    repeat while not hQuery:query-off-end:
      do std-in = 1 to hBuffer:num-fields:
        std-ha = hBuffer:buffer-field(std-in).
        if std-ha:buffer-value() = "?" or std-ha:buffer-value() = ?
         then 
          case std-ha:data-type:
           when "character" then std-ha:buffer-value() = "".
           when "decimal" then std-ha:buffer-value() = 0.
          end.
      end.
      hQuery:get-next().
    end.
    hQuery:query-close().
    delete object hQuery no-error.
    /* used to turn null values into blanks */
    
    /* set the fields */
    for first alert no-lock:
      std-ch = "".
      publish "GetSysPropDesc" ("AMD", "Alert", "Status", alert.stat, output std-ch).
      assign
        iAlertID = alert.alertID
        /* scores */
        tStat:screen-value = std-ch
        tOwner:screen-value = alert.owner
        tThreshold:screen-value = string(alert.threshold)
        tScore:screen-value = string(alert.score)
        tSeverity:screen-value = string(alert.severity)
        tDescription:screen-value = alert.description
        /* dates */
        tEffDate:screen-value = string(alert.effDate)
        tCreated:screen-value = string(alert.dateCreated)
        tClosed:screen-value = string(alert.dateClosed)
        .
      /* determine if the alert is a user-made alert or not */
      if lookup(alert.processCode, tCode:list-item-pairs) > 0
       then tCode:screen-value = alert.processCode.
       else
        assign
          tCode:visible = false
          tCodeText:visible = true
          tCodeText:read-only = true
          tCodeText:screen-value = alert.processCode
          .
      /* agent */
      tStateID:screen-value = alert.stateID.
      for first agent no-lock
          where agent.agentID = alert.agentID:

        tAgentID:screen-value = agent.name + " (" + agent.agentID + ")".
      end.
      /* set the created by name */
      std-ch = "".
      publish "GetSysUserName" (alert.createdBy, output std-ch).
      tCreatedBy:screen-value = std-ch.
      /* set the closed by name */
      if alert.closedBy > ""
       then
        do:
          std-ch = "".
          publish "GetSysUserName" (alert.closedBy, output std-ch).
          tCreatedBy:screen-value = std-ch.
        end.
    end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetNote C-Win 
PROCEDURE SetNote :
/*------------------------------------------------------------------------------
@description Sets the fields
------------------------------------------------------------------------------*/
  define variable hQuery as handle no-undo.
  define variable hBuffer as handle no-undo.
  define variable cCategoryDesc as character no-undo.
  define variable cUsername as character no-undo.
  define variable tNote as character no-undo.
  define variable tNotes as longchar no-undo.
  
  if valid-handle(hFileDataSrv)
   then run GetAlertNotes in hFileDataSrv (output table alertnote).
  
  /* get the category */
  do with frame {&frame-name}:
    std-ch = (if tNoteOrder:checked then " desc" else "").
    create buffer hBuffer for table "alertnote".
    create query hQuery.
    hQuery:set-buffers(hBuffer).
    hQuery:query-prepare("for each alertnote by dateCreated" + std-ch).
    hQuery:query-open().
    hQuery:get-first().
    repeat while not hQuery:query-off-end:
      /* test if the record is the category */
      std-ch = hBuffer:buffer-field("category"):buffer-value().
      if tNoteCategory:screen-value = "ALL" or std-ch = tNoteCategory:screen-value
       then
        do:
          publish "GetSysPropDesc" ("AMD", "AlertNote", "Category", std-ch, output cCategoryDesc).
          /* get the user's name */
          std-ch = hBuffer:buffer-field("createdBy"):buffer-value().
          publish "GetSysUserName" (std-ch, output cUsername).
          assign
            /* build the note */
            tNote = "[  " + string(hBuffer:buffer-field("dateCreated"):buffer-value(), "99/99/9999 HH:MM AM")
            tNote = tNote + "  |  " + caps(cUsername) + "  |  " + caps(cCategoryDesc) + "  ]" + chr(10) + chr(10)
            tNote = tNote + hBuffer:buffer-field("description"):buffer-value() + chr(10) + chr(10)
            tNotes = tNotes + tNote
            .
        end.
        hQuery:get-next().
    end.
    hQuery:query-close().
    delete object hQuery no-error.
    
    
    tNoteDesc:screen-value = tNotes.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
@description Shows the window
------------------------------------------------------------------------------*/
  {&window-name}:move-to-top().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doModify C-Win 
FUNCTION doModify RETURNS LOGICAL
  ( input pModify as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      bNoteNew:sensitive = pModify
      bClose:sensitive = pModify
      /* scores */
      tOwner:sensitive = pModify
      tSeverity:sensitive = (lookup(alert.processCode, tCode:list-item-pairs) = 0 and pModify)
      /* if user-made alert */
      std-lo = lookup(alert.processCode, tCode:list-item-pairs) > 0
      tCodeText:read-only = (std-lo or not pModify)
      tDescription:read-only = (std-lo or not pModify)
      tScore:read-only = (std-lo or not pModify)
      .
  end.
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

