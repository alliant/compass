&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/

define input parameter pApp          as character no-undo. /* Application ID */
define input parameter pServerPath   as character no-undo.
define input parameter pDesc         as character no-undo. /* UI Transaction description */
define input parameter pValidActions as character no-undo. /* new,delete,open,modify,send,request */

/* Used to call Compass server to hold reference that docs exist */
define input parameter pEntityType   as character no-undo.
define input parameter pEntityID     as character no-undo.
define input parameter pEntitySeq    as integer   no-undo.


CREATE WIDGET-POOL.

{lib/std-def.i}
{lib/brw-multi-def.i}
{lib/set-button-def.i}

{tt/doc.i}
{tt/doc.i &tableAlias="folder"}
{tt/doc.i &tableAlias="root"}
{tt/docuser.i}

{lib/add-delimiter.i}
{lib/urlencode.i}
{lib/winlaunch.i}
{lib/repository-procedures.i}

define variable tRootFolder    as character no-undo.
define variable tCurrentFolder as character no-undo initial ".". /* Set to "." to initialize first load of folder list */
define variable tDisable       as logical   no-undo initial false.

/* variables used to store original values for resize */
define variable dColumnWidth   as decimal no-undo.

DEFINE MENU folderpopmenu TITLE "Folder Actions"
  menu-item m_PopNew label "New..."
  menu-item m_PopRename label "Rename..."
  rule
  menu-item m_PopShare label "Share Folder..."
  menu-item m_PopSend label "Send Folder..."
  menu-item m_PopRequest label "Request Files..."
  rule
  menu-item m_PopDelete label "Delete..."
  .

ON 'choose':U OF menu-item m_PopNew in menu folderpopmenu
DO:
  run ActionNewFolder in this-procedure.
  RETURN.
END.

ON 'choose':U OF menu-item m_PopRename in menu folderpopmenu
DO:
  run ActionRenameFolder in this-procedure.
  RETURN.
END.

ON 'choose':U OF menu-item m_PopShare in menu folderpopmenu
DO:
  run ActionShareFolder in this-procedure.
  RETURN.
END.

ON 'choose':U OF menu-item m_PopSend in menu folderpopmenu
DO:
  run ActionSendFolder in this-procedure.
  RETURN.
END.

ON 'choose':U OF menu-item m_PopRequest in menu folderpopmenu
DO:
  run ActionRequestFiles in this-procedure.
  RETURN.
END.

ON 'choose':U OF menu-item m_PopDelete in menu folderpopmenu
DO:
  run ActionDeleteFolder in this-procedure.
  RETURN.
END.

DEFINE MENU docpopmenu TITLE "File Actions"
  menu-item m_PopOpen LABEL "Open"
  rule 
  menu-item m_PopNew label "Upload..."
  menu-item m_PopNotes label "Edit Notes..."
  rule
  menu-item m_PopSend label "Send File(s)..."
  rule
  menu-item m_PopDelete label "Delete..."
  .

ON 'choose':U OF menu-item m_PopOpen in menu docpopmenu
DO:
  run ActionOpenFile in this-procedure (output std-lo).
  RETURN.
END.

ON 'choose':U OF menu-item m_PopNew in menu docpopmenu
DO:
  run ActionNewFile in this-procedure.
  RETURN.
END.

ON 'choose':U OF menu-item m_PopNotes in menu docpopmenu
DO:
  run ActionEditNotes in this-procedure.
  RETURN.
END.

ON 'choose':U OF menu-item m_PopSend in menu docpopmenu
DO:
  run ActionSendFile in this-procedure.
  RETURN.
END.

ON 'choose':U OF menu-item m_PopDelete in menu docpopmenu
DO:
  run ActionDeleteFile in this-procedure.
  RETURN.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwDocs

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES document folder

/* Definitions for BROWSE brwDocs                                       */
&Scoped-define FIELDS-IN-QUERY-brwDocs document.displayName document.details document.createdDate document.filetype document.fileSizeDesc document.createdBy   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDocs   
&Scoped-define SELF-NAME brwDocs
&Scoped-define QUERY-STRING-brwDocs FOR EACH document by document.displayName
&Scoped-define OPEN-QUERY-brwDocs OPEN QUERY {&SELF-NAME} FOR EACH document by document.displayName.
&Scoped-define TABLES-IN-QUERY-brwDocs document
&Scoped-define FIRST-TABLE-IN-QUERY-brwDocs document


/* Definitions for BROWSE brwFolders                                    */
&Scoped-define FIELDS-IN-QUERY-brwFolders folder.displayName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFolders   
&Scoped-define SELF-NAME brwFolders
&Scoped-define QUERY-STRING-brwFolders FOR EACH folder by folder.displayName
&Scoped-define OPEN-QUERY-brwFolders OPEN QUERY {&SELF-NAME} FOR EACH folder by folder.displayName.
&Scoped-define TABLES-IN-QUERY-brwFolders folder
&Scoped-define FIRST-TABLE-IN-QUERY-brwFolders folder


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwFolders brwDocs bRefresh bFileRefresh 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFileType C-Win 
FUNCTION getFileType RETURNS CHARACTER
  ( input pFileName as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshDocs C-Win 
FUNCTION refreshDocs RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshFolders C-Win 
FUNCTION refreshFolders RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD uploadFile C-Win 
FUNCTION uploadFile RETURNS LOGICAL PRIVATE
  ( input pFile as character,
    output pMsg as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bFileDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete selected document(s)".

DEFINE BUTTON bFileDownload  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open the selected document(s)".

DEFINE BUTTON bFileModify  NO-FOCUS
     LABEL "Edit File" 
     SIZE 7.2 BY 1.71 TOOLTIP "Edit document filename and notes".

DEFINE BUTTON bFileRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh the list of documents".

DEFINE BUTTON bFileRequest  NO-FOCUS
     LABEL "Request" 
     SIZE 7.2 BY 1.71 TOOLTIP "Request files for selected folder via email".

DEFINE BUTTON bFileSend  NO-FOCUS
     LABEL "Send" 
     SIZE 7.2 BY 1.71 TOOLTIP "Send selected file(s) via email".

DEFINE BUTTON bFileUpload  NO-FOCUS
     LABEL "Upload" 
     SIZE 7.2 BY 1.71 TOOLTIP "Upload a new document".

DEFINE BUTTON bFolderDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete selected folder(s)".

DEFINE BUTTON bFolderNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Create a new folder".

DEFINE BUTTON bFolderRename  NO-FOCUS
     LABEL "Rename" 
     SIZE 7.2 BY 1.71 TOOLTIP "Rename selected folder".

DEFINE BUTTON bFolderSend  NO-FOCUS
     LABEL "Send" 
     SIZE 7.2 BY 1.71 TOOLTIP "Send selected folder via email".

DEFINE BUTTON bFolderShare  NO-FOCUS
     LABEL "Share" 
     SIZE 7.2 BY 1.71 TOOLTIP "Share selected folder via email".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh the list of folders".

DEFINE RECTANGLE rButtons
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 45.6 BY 2.05.

DEFINE RECTANGLE rButtons-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 53 BY 2.05.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwDocs FOR 
      document SCROLLING.

DEFINE QUERY brwFolders FOR 
      folder SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwDocs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDocs C-Win _FREEFORM
  QUERY brwDocs DISPLAY
      document.displayName format "x(256)" width 50 label "File Name"
document.details format "x(200)" width 45 label "Notes"
document.createdDate format "x(24)" label "Date Saved"
document.filetype format "x(12)" label "Type"
document.fileSizeDesc format "x(15)" width 10 label "Size"
document.createdBy format "x(20)" label "Saved By"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE DROP-TARGET SIZE 163 BY 18.86
         TITLE "Documents" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwFolders
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFolders C-Win _FREEFORM
  QUERY brwFolders DISPLAY
      folder.displayName format "x(256)" width 50 label "Folder Name"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET SIZE 53 BY 18.86
         TITLE "Folders" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bFolderNew AT ROW 1.38 COL 2.6 WIDGET-ID 44
     brwFolders AT ROW 3.48 COL 2 WIDGET-ID 300
     brwDocs AT ROW 3.48 COL 56 WIDGET-ID 200
     bFileModify AT ROW 1.38 COL 71.4 WIDGET-ID 36
     bFileRequest AT ROW 1.38 COL 32.2 WIDGET-ID 58
     bFolderShare AT ROW 1.38 COL 17.4 WIDGET-ID 62
     bFolderSend AT ROW 1.38 COL 24.8 WIDGET-ID 50
     bFileSend AT ROW 1.38 COL 78.8 WIDGET-ID 56
     bRefresh AT ROW 1.38 COL 47 WIDGET-ID 54
     bFileDelete AT ROW 1.38 COL 86.2 WIDGET-ID 6
     bFolderRename AT ROW 1.38 COL 10 WIDGET-ID 52
     bFolderDelete AT ROW 1.38 COL 39.6 WIDGET-ID 46
     bFileDownload AT ROW 1.38 COL 56.6 WIDGET-ID 24
     bFileRefresh AT ROW 1.38 COL 93.6 WIDGET-ID 32
     bFileUpload AT ROW 1.38 COL 64 WIDGET-ID 4
     rButtons AT ROW 1.24 COL 56 WIDGET-ID 22
     rButtons-2 AT ROW 1.24 COL 2 WIDGET-ID 48
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 218.8 BY 21.52 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Documents"
         HEIGHT             = 21.52
         WIDTH              = 218.8
         MAX-HEIGHT         = 31.33
         MAX-WIDTH          = 232.4
         VIRTUAL-HEIGHT     = 31.33
         VIRTUAL-WIDTH      = 232.4
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwFolders bFolderNew fMain */
/* BROWSE-TAB brwDocs brwFolders fMain */
/* SETTINGS FOR BUTTON bFileDelete IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bFileDelete:PRIVATE-DATA IN FRAME fMain     = 
                "Delete".

/* SETTINGS FOR BUTTON bFileDownload IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bFileDownload:PRIVATE-DATA IN FRAME fMain     = 
                "Open".

/* SETTINGS FOR BUTTON bFileModify IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bFileModify:PRIVATE-DATA IN FRAME fMain     = 
                "Email".

ASSIGN 
       bFileRefresh:PRIVATE-DATA IN FRAME fMain     = 
                "Email".

/* SETTINGS FOR BUTTON bFileRequest IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bFileRequest:PRIVATE-DATA IN FRAME fMain     = 
                "Delete".

/* SETTINGS FOR BUTTON bFileSend IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bFileSend:PRIVATE-DATA IN FRAME fMain     = 
                "Delete".

/* SETTINGS FOR BUTTON bFileUpload IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bFileUpload:PRIVATE-DATA IN FRAME fMain     = 
                "New".

/* SETTINGS FOR BUTTON bFolderDelete IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bFolderDelete:PRIVATE-DATA IN FRAME fMain     = 
                "Delete".

/* SETTINGS FOR BUTTON bFolderNew IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bFolderNew:PRIVATE-DATA IN FRAME fMain     = 
                "New".

/* SETTINGS FOR BUTTON bFolderRename IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bFolderRename:PRIVATE-DATA IN FRAME fMain     = 
                "Email".

/* SETTINGS FOR BUTTON bFolderSend IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bFolderSend:PRIVATE-DATA IN FRAME fMain     = 
                "Delete".

/* SETTINGS FOR BUTTON bFolderShare IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bFolderShare:PRIVATE-DATA IN FRAME fMain     = 
                "Delete".

ASSIGN 
       bRefresh:PRIVATE-DATA IN FRAME fMain     = 
                "Email".

ASSIGN 
       brwDocs:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwDocs:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

ASSIGN 
       brwFolders:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwFolders:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR RECTANGLE rButtons IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE rButtons-2 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDocs
/* Query rebuild information for BROWSE brwDocs
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH document by document.displayName.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwDocs */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFolders
/* Query rebuild information for BROWSE brwFolders
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH folder by folder.displayName.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwFolders */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Documents */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Documents */
DO:
  publish "WindowClosed" (input this-procedure ).
 
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Documents */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileDelete C-Win
ON CHOOSE OF bFileDelete IN FRAME fMain /* Del */
DO:
  run ActionDeleteFile in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileDownload
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileDownload C-Win
ON CHOOSE OF bFileDownload IN FRAME fMain /* Open */
DO:
   run ActionOpenFile in this-procedure (output std-lo).
   if std-lo 
    then apply "WINDOW-CLOSE" to frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileModify C-Win
ON CHOOSE OF bFileModify IN FRAME fMain /* Edit File */
DO:
  run ActionEditNotes in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileRefresh C-Win
ON CHOOSE OF bFileRefresh IN FRAME fMain /* Refresh */
DO:
  refreshDocs().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileRequest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileRequest C-Win
ON CHOOSE OF bFileRequest IN FRAME fMain /* Request */
DO:
  run ActionRequestFiles in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileSend
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileSend C-Win
ON CHOOSE OF bFileSend IN FRAME fMain /* Send */
DO:
  run ActionSendFile in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileUpload
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileUpload C-Win
ON CHOOSE OF bFileUpload IN FRAME fMain /* Upload */
DO:
  run ActionNewFile in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFolderDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFolderDelete C-Win
ON CHOOSE OF bFolderDelete IN FRAME fMain /* Del */
DO:
  run ActionDeleteFolder in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFolderNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFolderNew C-Win
ON CHOOSE OF bFolderNew IN FRAME fMain /* New */
DO:
  run ActionNewFolder in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFolderRename
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFolderRename C-Win
ON CHOOSE OF bFolderRename IN FRAME fMain /* Rename */
DO:
  run ActionRenameFolder in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFolderSend
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFolderSend C-Win
ON CHOOSE OF bFolderSend IN FRAME fMain /* Send */
DO:
  run ActionSendFolder in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFolderShare
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFolderShare C-Win
ON CHOOSE OF bFolderShare IN FRAME fMain /* Share */
DO:
  run ActionShareFolder in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  refreshFolders().
  refreshDocs().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDocs
&Scoped-define SELF-NAME brwDocs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON DEFAULT-ACTION OF brwDocs IN FRAME fMain /* Documents */
DO:
   run ActionOpenFile in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON DROP-FILE-NOTIFY OF brwDocs IN FRAME fMain /* Documents */
DO:
  run ActionFilesDropped in this-procedure (self:handle).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON ROW-DISPLAY OF brwDocs IN FRAME fMain /* Documents */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON START-SEARCH OF brwDocs IN FRAME fMain /* Documents */
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON VALUE-CHANGED OF brwDocs IN FRAME fMain /* Documents */
DO:
  if brwDocs:num-selected-rows > 0 
   then assign
          bFileDownload:sensitive = lookup("open", pValidActions) > 0
          bFileDelete:sensitive = lookup("delete", pValidActions) > 0
          bFileSend:sensitive = lookup("send", pValidActions) > 0
          bFileModify:sensitive = lookup("modify", pValidActions) > 0 
          menu-item m_PopOpen:sensitive in menu docpopmenu = bFileDownload:sensitive
          menu-item m_PopDelete:sensitive in menu docpopmenu = bFileDelete:sensitive
          menu-item m_PopSend:sensitive in menu docpopmenu = bFileSend:sensitive
          menu-item m_PopNotes:sensitive in menu docpopmenu = bFileModify:sensitive
          .
   else assign
          bFileDownload:sensitive = false
          bFileDelete:sensitive = false
          bFileSend:sensitive = false
          bFileModify:sensitive = false
          menu-item m_PopOpen:sensitive in menu docpopmenu = false
          menu-item m_PopDelete:sensitive in menu docpopmenu = false
          menu-item m_PopSend:sensitive in menu docpopmenu = false
          menu-item m_PopNotes:sensitive in menu docpopmenu = false
          .
  bFileUpload:sensitive in frame {&frame-name} = lookup("new", pValidActions) > 0.
  menu-item m_PopNew:sensitive in menu docpopmenu = bFileUpload:sensitive.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFolders
&Scoped-define SELF-NAME brwFolders
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFolders C-Win
ON DEFAULT-ACTION OF brwFolders IN FRAME fMain /* Folders */
DO:
   /*run ActionOpen in this-procedure (output std-lo).*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFolders C-Win
ON DROP-FILE-NOTIFY OF brwFolders IN FRAME fMain /* Folders */
DO:
  run ActionFoldersDropped in this-procedure (self:handle).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFolders C-Win
ON ROW-DISPLAY OF brwFolders IN FRAME fMain /* Folders */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFolders C-Win
ON START-SEARCH OF brwFolders IN FRAME fMain /* Folders */
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFolders C-Win
ON VALUE-CHANGED OF brwFolders IN FRAME fMain /* Folders */
DO:
  if brwFolders:num-selected-rows > 0 
   then assign
          bFolderNew:sensitive = lookup("new", pValidActions) > 0
          bFolderRename:sensitive = lookup("modify", pValidActions) > 0 and folder.name <> ""
          bFolderShare:sensitive = lookup("share", pValidActions) > 0
          bFolderSend:sensitive = lookup("send", pValidActions) > 0
          bFileRequest:sensitive = lookup("request", pValidActions) > 0
          bFolderDelete:sensitive = lookup("delete", pValidActions) > 0 and folder.name <> ""
          menu-item m_PopNew:sensitive in menu folderpopmenu = bFolderNew:sensitive
          menu-item m_PopRename:sensitive in menu folderpopmenu = bFolderRename:sensitive
          menu-item m_PopShare:sensitive in menu folderpopmenu = bFolderShare:sensitive
          menu-item m_PopSend:sensitive in menu folderpopmenu = bFolderSend:sensitive
          menu-item m_PopRequest:sensitive in menu folderpopmenu = bFileRequest:sensitive
          menu-item m_PopDelete:sensitive in menu folderpopmenu = bFolderDelete:sensitive
          .
   else assign
          bFolderNew:sensitive = lookup("new", pValidActions) > 0
          bFolderRename:sensitive = false 
          bFolderShare:sensitive = false
          bFolderSend:sensitive = false
          bFileRequest:sensitive = lookup("request", pValidActions) > 0
          bFolderDelete:sensitive = false
          menu-item m_PopNew:sensitive in menu folderpopmenu = false
          menu-item m_PopRename:sensitive in menu folderpopmenu = false
          menu-item m_PopShare:sensitive in menu folderpopmenu = false
          menu-item m_PopSend:sensitive in menu folderpopmenu = false
          menu-item m_PopRequest:sensitive in menu folderpopmenu = bFileRequest:sensitive
          menu-item m_PopDelete:sensitive in menu folderpopmenu = false
          .
  
  if tCurrentFolder <> folder.name
   then
    do:
      tCurrentFolder = "/" + folder.name.
      refreshDocs().
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDocs
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

subscribe to "CloseWindow" anywhere.

{lib/win-main.i}
{lib/win-show.i}
{lib/win-close.i}
{lib/brw-main-multi.i &browse-list="brwDocs,brwFolders"}

if pDesc > "" 
 then {&window-name}:title = {&window-name}:title + " for " + pDesc.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

{lib/set-button.i &label="FileDownload" &image="images/download.bmp"   &inactive="images/download-i.bmp"}
{lib/set-button.i &label="FileUpload"   &image="images/upload.bmp"     &inactive="images/upload-i.bmp"}
{lib/set-button.i &label="FileDelete"   &image="images/erase.bmp"      &inactive="images/erase-i.bmp"}
{lib/set-button.i &label="FileRefresh"  &image="images/sync.bmp"       &inactive="images/sync-i.bmp"}
{lib/set-button.i &label="FileModify"   &image="images/update.bmp"     &inactive="images/update-i.bmp"}
{lib/set-button.i &label="FileSend"     &image="images/send.bmp"       &inactive="images/send-i.bmp"}
{lib/set-button.i &label="FileRequest"  &image="images/request.bmp"    &inactive="images/request-i.bmp"}
{lib/set-button.i &label="FolderNew"    &image="images/folder-add.bmp" &inactive="images/folder-add-i.bmp"}
{lib/set-button.i &label="FolderDelete" &image="images/erase.bmp"      &inactive="images/erase-i.bmp"}
{lib/set-button.i &label="FolderSend"   &image="images/send.bmp"       &inactive="images/send-i.bmp"}
{lib/set-button.i &label="Refresh"      &image="images/sync.bmp"       &inactive="images/sync-i.bmp"}
{lib/set-button.i &label="FolderShare"  &image="images/users.bmp"      &inactive="images/users-i.bmp"}
{lib/set-button.i &label="FolderRename" &image="images/update.bmp"     &inactive="images/update-i.bmp"}
setButtons().

browse brwFolders:POPUP-MENU = MENU folderpopmenu:HANDLE.
browse brwDocs:POPUP-MENU = MENU docpopmenu:HANDLE.

/* Tell the parent window we are open even though we haven't pulled 
   directory data yet */
publish "WindowOpened" ("DOCUMENTS").

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
       
{&window-name}:window-state = 2.

/* remove the last / or \ */
pServerPath = right-trim(pServerPath, "~\").
pServerPath = right-trim(pServerPath, "~/").

/* Grab the root folder name for display in the browse */
if pServerPath begins "~/" and num-entries(pServerPath, "~/") > 1
 then tRootFolder = entry(num-entries(pServerPath, "~/"), pServerPath, "~/").
 else 
  if pServerPath begins "~\"  and num-entries(pServerPath, "~\") > 1 
   then tRootFolder = entry(num-entries(pServerPath,"~\"), pServerPath, "~\").
   else tRootFolder = "Root".

/* Prepend root folder based on config setting (dev versus prod) */
std-ch = "".
publish "GetRepositoryRoot" (output std-ch).
pServerPath = std-ch + pServerPath.

{lib/pbshow.i "'Initializing window, please wait...'"}
{lib/pbupdate.i "'Initializing window'" 10}

/* login */
/* The reason for no uid or password is because the publish "SetRepositoryToken" will */
/* make a server call to log the user in */
setUserToken("", "", output std-ch). 

/* create the new folder in case it's not created */
run NewRepositoryFolder (pServerPath, output std-lo, output std-ch).
if not std-lo
 then
  do:
    message "Folder not found and could not create folder: " + std-ch view-as alert-box information buttons ok.
    assign
      tDisable = true
      std-ch   = pServerPath 
      .
  end.
 else  std-ch = pServerPath 
              + "  (" 
              + (if lookup("new", pValidActions) > 0 then "C" else "")
              + (if lookup("open", pValidActions) > 0 then "R" else "")
              + (if lookup("modify", pValidActions) > 0 then "U" else "")
              + (if lookup("delete", pValidActions) > 0 then "D" else "")
              + ")".
status input std-ch in window {&window-name}.
status default std-ch in window {&window-name}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
  RUN enable_UI.
  if not tDisable
   then 
    do:
      {lib/pbupdate.i "'Initializing window'" 80}
      refreshFolders().
      apply "value-changed" to brwFolders.
    end.
   else
    assign
      bFileDownload:sensitive  = false
      bFileUpload:sensitive    = false
      bFileDelete:sensitive    = false
      bFileRefresh:sensitive   = false
      bFileModify:sensitive    = false
      bFolderNew:sensitive     = false
      bFolderDelete:sensitive  = false
      bFolderSend:sensitive    = false
      bFolderRename:sensitive  = false
      bRefresh:sensitive       = false
      bFileSend:sensitive      = false
      bFileRequest:sensitive   = false
      bFolderShare:sensitive   = false
      .
   
  {lib/get-column-width.i &browse-name=brwDocs &col="'details'" &var=dColumnWidth}
  run windowResized in this-procedure.
  
  {lib/pbhide.i}
  {&window-name}:window-state = 3.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDeleteFile C-Win 
PROCEDURE ActionDeleteFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tFileCnt as integer   no-undo.
  define variable tMsg     as character no-undo.
  define variable tGoodCnt as integer   no-undo.
  
  if lookup("delete", pValidActions) = 0
   then return.
  
  if browse brwDocs:num-selected-rows = 0
   then 
    do: bFileDelete:sensitive in frame {&frame-name} = false.
        menu-item m_PopDelete:sensitive in menu docpopmenu = false.
        return.
    end.
  
  {lib/confirm-delete.i "Document(s)"}
  
  do tFileCnt = 1 to browse brwDocs:num-selected-rows:
    browse brwDocs:fetch-selected-row(tFileCnt).
    
    if not available document 
     then next.
    
    if document.identifier = ""
     then
      do: 
        tMsg = tMsg + (if tMsg > "" then chr(19) else "") + document.fullpath + " is invalid.".
        next.
      end.
    
    run DeleteRepositoryItem (document.fullpath, output std-lo, output std-ch).
                                  
    if not std-lo 
     then tMsg = tMsg + (if tMsg > "" then chr(19) else "") + "Unable to remove " + document.name + " .".
     else tGoodCnt = tGoodCnt + 1.
  end.
  
  if tMsg > "" or tGoodCnt <> browse brwDocs:num-selected-rows
   then MESSAGE "Failed to remove " (browse brwDocs:num-selected-rows - tGoodCnt) " files." skip(1) tMsg skip(2) "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
   else refreshFolders().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDeleteFolder C-Win 
PROCEDURE ActionDeleteFolder :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tFileCnt as integer   no-undo.
  define variable tMsg     as character no-undo.
  define variable tGoodCnt as integer   no-undo.
  
  if lookup("delete", pValidActions) = 0
   then return.
  
  if browse brwFolders:num-selected-rows = 0
   then 
    do: bFileDelete:sensitive in frame {&frame-name} = false.
        menu-item m_PopDelete:sensitive in menu docpopmenu = false.
        return.
    end.
  
  {lib/confirm-delete.i "Folder and its contents"}
  
  do tFileCnt = 1 to browse brwFolders:num-selected-rows:
    browse brwFolders:fetch-selected-row(tFileCnt).
    
    if not available folder 
     then next.
    
    if folder.identifier = ""
     then
      do: 
        tMsg = tMsg + (if tMsg > "" then chr(19) else "") + folder.fullpath + " is invalid.".
          next.
      end.
    
    run DeleteRepositoryItem (folder.fullpath, output std-lo, output std-ch).
    
    if not std-lo 
     then tMsg = tMsg + (if tMsg > "" then chr(19) else "") + "Unable to remove " + folder.name + " .".
     else tGoodCnt = tGoodCnt + 1.
  end.
  
  if tMsg > "" or tGoodCnt <> browse brwFolders:num-selected-rows
   then MESSAGE "Failed to remove " (browse brwFolders:num-selected-rows - tGoodCnt) " files." skip(1) tMsg skip(2) "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
   else refreshFolders().
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionEditNotes C-Win 
PROCEDURE ActionEditNotes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tName  as character no-undo.
  define variable tNotes as character no-undo.
  
  if lookup("modify", pValidActions) = 0
   then return.
  
  if browse brwDocs:num-selected-rows = 0
   then return.
  
  if not available document
   then return.
  
  if document.identifier = ""
   then
    do: 
      message document.fullpath + " is invalid." view-as alert-box error.
      return.
    end.
  
  tName  = document.displayName.
  tNotes = document.details.

  run dialogdocumentfileedit.w (input-output tName, input-output tNotes, output std-lo).
  if not std-lo
   then return.

  run ModifyRepositoryItem (document.fullpath, tName, tNotes, output std-lo, output std-ch).
  
  if not std-lo
   then
    do: 
      MESSAGE "Failed to edit " + document.displayName + "." skip std-ch skip "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
      return.
    end.
 
  refreshDocs().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionEmail C-Win 
PROCEDURE ActionEmail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tTo         as character no-undo.
  define variable tSubject    as character no-undo.
  define variable tMsg        as character no-undo.
  define variable tExpiration as integer   no-undo.
  define variable tNotify     as logical   no-undo.
  define variable tDownloads  as integer   no-undo initial 2.
  
  if lookup("email", pValidActions) = 0
   then return.
  
  if browse brwDocs:num-selected-rows = 0
   then return.
  
  if document.identifier = ""
   then
    do: 
      MESSAGE "Document record is corrupted.  Please contact the System Administrator." VIEW-AS ALERT-BOX INFO BUTTONS OK.
      return.
    end.
  
  MAIL-BLOCK:
  repeat:
    /* Prompt for email attributes */
    run dialogdocumentemail.w (input document.displayName,
                               input-output tTo,
                               input-output tSubject,
                               input-output tMsg,
                               input-output tExpiration,
                               input-output tNotify,
                               input-output tDownloads,
                               output std-lo).
    if not std-lo 
     then leave MAIL-BLOCK.
    
    run RequestRepositoryDownload 
        (document.fullpath,
         tTo,
         tSubject,
         tMsg,
         "",
         false,
         true,
         tExpiration,
         tNotify,
         false,
         tDownloads,
         false,
         output std-lo,
         output std-ch).
    
    if std-lo 
     then 
      do: 
        publish "AddContact" (tTo).
        leave MAIL-BLOCK.
      end.
    
    MESSAGE std-ch VIEW-AS ALERT-BOX warning BUTTONS OK.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionFilesDropped C-Win 
PROCEDURE ActionFilesDropped :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pWidget as handle.

 def var tFileCnt as int no-undo.
 def var tMsg as character no-undo.

 if not valid-handle(pWidget) 
  then return.

 if lookup("new", pValidActions) = 0
  then
   do:  pWidget:end-file-drop().
        return.
   end.

 FILE-LOOP:
 do tFileCnt = 1 to pWidget:num-dropped-files:
  std-ch = pWidget:get-dropped-file(tFileCnt).
  std-lo = uploadFile(std-ch, output tMsg).
  if not std-lo 
   then
    do:
        MESSAGE tMsg skip(2) "File uploads interrupted."
         VIEW-AS ALERT-BOX error BUTTONS OK.
        leave FILE-LOOP.
    end.
 end.
 pWidget:end-file-drop().

 refreshDocs().
 if can-find(first document) 
  then apply "value-changed" to brwDocs in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionFoldersDropped C-Win 
PROCEDURE ActionFoldersDropped :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

/*
 def input parameter pWidget as handle.

 def var tFileCnt as int no-undo.
 def var tMsg as character no-undo.

 if not valid-handle(pWidget) 
  then return.

 if lookup("new", pValidActions) = 0
  then
   do:  pWidget:end-file-drop().
        return.
   end.

 FILE-LOOP:
 do tFileCnt = 1 to pWidget:num-dropped-files:
  std-ch = pWidget:get-dropped-file(tFileCnt).
  std-lo = uploadFile(std-ch, output tMsg).
  if not std-lo 
   then
    do:
        MESSAGE tMsg skip(2)
                "File uploads interrupted."
         VIEW-AS ALERT-BOX error BUTTONS OK.
        leave FILE-LOOP.
    end.
 end.
 pWidget:end-file-drop().

 refreshDocs().
 if can-find(first document) 
  then apply "value-changed" to brwDocs in frame {&frame-name}.
*/  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNewFile C-Win 
PROCEDURE ActionNewFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tFile as character no-undo.

 if lookup("new", pValidActions) = 0
  then return.

 system-dialog get-file tFile
  must-exist 
  title "Document to Upload"
  update std-lo.
 if not std-lo 
  then return.
 
 publish "GetConfirmFileUpload" (output std-lo).
 if std-lo 
  then
   do:
       MESSAGE tFile " will be uploaded.  Continue?"
        VIEW-AS ALERT-BOX question BUTTONS YES-NO title "Confirmation" update std-lo.
       if not std-lo 
        then return.
   end.

 std-lo = uploadFile(tFile, output std-ch).
 if not std-lo 
  then
   do:
       MESSAGE std-ch
        VIEW-AS ALERT-BOX warning BUTTONS OK.
       return.
   end.

 refreshDocs().
 if can-find(first document) 
  then apply "value-changed" to brwDocs in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNewFolder C-Win 
PROCEDURE ActionNewFolder :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tFolder as character no-undo.
  
  if lookup("new", pValidActions) = 0
  then return.
  
  run dialogdocumentnewfolder.w (output tFolder, output std-lo).
  if not std-lo
  then return.
  
  if tFolder = "" or tFolder = ? 
  then return.

  run NewRepositoryFolder (pServerPath + "/" + tFolder, output std-lo, output std-ch).
  
  if not std-lo
   then
    do:
      MESSAGE "Failed to create folder '" + tFolder + "'." skip std-ch skip "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
      return.
    end.
 
  refreshFolders().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionOpenFile C-Win 
PROCEDURE ActionOpenFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter pSuccess as logical initial false no-undo.
  
  define variable tLocalPath as character no-undo.
  define variable tMsg       as character no-undo.
  define variable tFile      as character no-undo.
  define variable tFileCnt   as integer   no-undo.
  define variable tGoodCnt   as integer   no-undo.
  
  if lookup("open", pValidActions) = 0
   then return.
  
  if browse brwDocs:num-selected-rows = 0
   then return.
  
  /* das:ActionOpen - handle multiple */
  
  publish "GetTempDir" (output tLocalPath).
  
  do tFileCnt = 1 to browse brwDocs:num-selected-rows:
    browse brwDocs:fetch-selected-row(tFileCnt).
    
    if not available document 
     then next.
    
    if document.identifier = ""
     then
      do: 
        tMsg = addDelimiter(tMsg, chr(19)) + document.fullpath + " is invalid.".
        next.
      end.
    
    tFile = tLocalPath + document.name.
    if search(tFile) <> ? 
     then os-delete value(tFile).
    
    run DownloadRepositoryFile (document.fullpath, tFile, output std-lo, output std-ch).
    
    if not std-lo 
     then
      do: 
        tMsg = addDelimiter(tMsg, chr(19)) + "Unable to download " + document.name + " .".
        next.
      end.
    
    if search(tFile) = ? 
     then
      do: 
        tMsg = addDelimiter(tMsg, chr(19)) + document.name + " failed to download.".
        next.
      end.
    
    publish "AddTempFile" (document.name, tFile).
    run util/openfile.p (tFile).
    tGoodCnt = tGoodCnt + 1.
  end.
  if tMsg > "" or tGoodCnt <> browse brwDocs:num-selected-rows
   then MESSAGE "Failed to download " (browse brwDocs:num-selected-rows - tGoodCnt) " files." skip tMsg skip "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
   else pSuccess = true.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionRenameFolder C-Win 
PROCEDURE ActionRenameFolder :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tFolder as character no-undo.
  
  if lookup("modify", pValidActions) = 0
   then return.

  if browse brwFolders:num-selected-rows = 0
   then return.
  
  if not available folder
   then return.
  
  if folder.identifier = ""
   then
    do: 
      message folder.fullpath + " is invalid." view-as alert-box error.
      return.
    end.
  
  tFolder = folder.displayName.
  run dialogdocumentrenamefolder.w (input-output tFolder, output std-lo).
  
  if not std-lo
   then return.
  
  if tFolder = "" or tFolder = ? 
   then return.

  run ModifyRepositoryItem (folder.fullpath, tFolder, "", output std-lo, output std-ch).
  
  if not std-lo
   then
    do:
      MESSAGE "Failed to rename folder '" + folder.displayName + "'." skip(2) std-ch skip "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
      return.
  end.
 
  refreshFolders().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionRequestFiles C-Win 
PROCEDURE ActionRequestFiles :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tTo             as character no-undo.
  define variable tSubject        as character no-undo.
  define variable tBody           as character no-undo.
  define variable tCCSender       as logical   no-undo.
  define variable tNotifyOnUpload as logical   no-undo.
  define variable tCounter        as integer   no-undo.

  if lookup("send", pValidActions) = 0
   then return.

  if browse brwFolders:num-selected-rows = 0
   then return.
  
  if not available folder
   then return.
  
  if folder.identifier = ""
   then
    do: 
      message folder.fullpath + " is invalid." view-as alert-box error.
      return.
    end.
  
  run dialogdocumentsendrequest.w (input  "Request",
                                   input  "File",
                                   input  folder.displayName,
                                   output tTo,
                                   output tSubject,
                                   output tBody,
                                   output tCCSender,
                                   output tNotifyOnUpload,
                                   output std-lo).
  if not std-lo
  then return.

  do tCounter = 1 to num-entries(tTo):
    run RequestRepositoryUpload 
        (folder.fullpath,
         entry(tCounter, tTo),
         tSubject,
         tBody,
         tCCSender,
         true,
         0,
         tNotifyOnUpload,
         output std-lo,
         output std-ch).
    
    if not std-lo
     then
      do:
        MESSAGE "Failed to request file(s) for folder '" + folder.displayName + "'." skip(2) std-ch skip(2) "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
        return.
      end.
  end.
 
  /*refreshFolders().*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionSendFile C-Win 
PROCEDURE ActionSendFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tFileCnt          as integer   no-undo.
  define variable tFileList         as character no-undo.
  define variable tMsg              as character no-undo.
  define variable tGoodCnt          as integer   no-undo.
  define variable tCounter          as integer   no-undo.
  
  define variable tTo               as character no-undo.
  define variable tSubject          as character no-undo.
  define variable tBody             as character no-undo.
  define variable tCCSender         as logical   no-undo.
  define variable tNotifyOnDownload as logical   no-undo.

  if lookup("send", pValidActions) = 0
   then return.

  if browse brwDocs:num-selected-rows = 0
   then return.

  do tFileCnt = 1 to browse brwDocs:num-selected-rows:
    browse brwDocs:fetch-selected-row(tFileCnt).
  
    if not available document 
     then next.
  
    if document.identifier = ""
     then
      do: 
        tMsg = tMsg + (if tMsg > "" then chr(19) else "") + document.fullpath + " is invalid.".
        next.
      end.
      
    tFileList = tFileList + chr(10) + document.displayName.      
  end.
  
  if tMsg <> "" then
  do:
    message tMsg view-as alert-box error.
    return.
  end.
  
  tMsg = "".

  run dialogdocumentsendrequest.w (input  "Send",
                                   input  "File",
                                   input  tFileList,
                                   output tTo,
                                   output tSubject,
                                   output tBody,
                                   output tCCSender,
                                   output tNotifyOnDownload,
                                   output std-lo).
  if not std-lo
  then return.
  
  do tFileCnt = 1 to browse brwDocs:num-selected-rows:
    browse brwDocs:fetch-selected-row(tFileCnt).
  
    if not available document 
     then next.
  
    do tCounter = 1 to num-entries(tTo):
      run RequestRepositoryDownload 
          (document.fullpath,
           entry(tCounter, tTo),
           tSubject,
           tBody,
           tCCSender,
           true,
           false,
           -1,
           tNotifyOnDownload,
           false,
           -1,
           yes,
           output std-lo,
           output std-ch).
    
      if not std-lo 
       then tMsg = tMsg + (if tMsg > "" then chr(19) else "") + "Unable to send " + document.name + " to " + entry(tCounter, tTo) + ".".
    end.
    
    if std-lo
     then tGoodCnt = tGoodCnt + 1.
  end.

  if tMsg > "" or tGoodCnt <> browse brwDocs:num-selected-rows
   then MESSAGE "Failed to send " (browse brwDocs:num-selected-rows - tGoodCnt) " files." skip tMsg skip "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionSendFolder C-Win 
PROCEDURE ActionSendFolder :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tTo               as character no-undo.
  define variable tSubject          as character no-undo.
  define variable tBody             as character no-undo.
  define variable tCCSender         as logical   no-undo.
  define variable tNotifyOnDownload as logical   no-undo.
  define variable tCounter          as integer   no-undo.

  if lookup("send", pValidActions) = 0
   then return.

  if browse brwFolders:num-selected-rows = 0
   then return.
  
  if not available folder
   then return.
  
  if folder.identifier = ""
   then
    do: 
      message folder.fullpath + " is invalid." view-as alert-box error.
      return.
    end.
  
  run dialogdocumentsendrequest.w (input  "Send",
                                   input  "Folder",
                                   input  folder.displayName,
                                   output tTo,
                                   output tSubject,
                                   output tBody,
                                   output tCCSender,
                                   output tNotifyOnDownload,
                                   output std-lo).
  if not std-lo
  then return.
  
  do tCounter = 1 to num-entries(tTo):
    run RequestRepositoryDownload 
        (folder.fullpath,
         entry(tCounter, tTo),
         tSubject,
         tBody,
         tCCSender,
         true,
         false,
         -1,
         tNotifyOnDownload,
         false,
         -1,
         false,
         output std-lo,
         output std-ch).
    
    if not std-lo
     then 
      do:
        MESSAGE "Failed to send folder '" + folder.displayName + "'." skip(2) std-ch skip "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
        return.
      end.
  end.
  
  /*refreshFolders().*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionShareFolder C-Win 
PROCEDURE ActionShareFolder :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tTo               as character no-undo.
  define variable tSubject          as character no-undo.
  define variable tBody             as character no-undo.
  define variable tCCSender         as logical   no-undo.
  define variable tNotifyOnDownload as logical   no-undo.
  define variable tCounter          as integer   no-undo.

  if lookup("send", pValidActions) = 0
  then return.

  if browse brwFolders:num-selected-rows = 0
  then return.
  
  if not available folder
  then return.
  
  if folder.identifier = ""
   then
    do: 
      message folder.fullpath + " is invalid." view-as alert-box error.
      return.
    end.
  
  run dialogdocumentsendrequest.w (input  "Share",
                                   input  "Folder",
                                   input  folder.displayName,
                                   output tTo,
                                   output tSubject,
                                   output tBody,
                                   output tCCSender,
                                   output tNotifyOnDownload,
                                   output std-lo).
  if not std-lo
  then return.
  
  do tCounter = 1 to num-entries(tTo):
    empty temp-table docuser no-error.
    run GetRepositoryUser (entry(tCounter, tTo), output table docuser, output std-lo, output std-ch).
    
    if not std-lo
     then 
      do:
        MESSAGE "Failed to get info for user '" + entry(tCounter, tTo) + "'." skip(2) std-ch skip(2) "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
        return.
      end.
     else
      for first docuser no-lock:
        run server/grantrepositoryuser.p (folder.fullpath, entry(tCounter, tTo), output std-lo, output std-ch).
        if not std-lo
         then
          do:
            message "Failed to grant user '" + docuser.Name + "' access to the folder." skip(2) std-ch skip(2) "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
            return.
          end.
         else run SendRepositoryWelcome (entry(tCounter, tTo), tBody, output std-lo, output std-ch).
      end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE brwFolders brwDocs bRefresh bFileRefresh 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  
  /* {&frame-name} components */
  brwFolders:height-pixels = frame {&frame-name}:height-pixels - brwFolders:y - 5.
  
  brwDocs:width-pixels = frame {&frame-name}:width-pixels - brwDocs:x - 5.
  brwDocs:height-pixels = frame {&frame-name}:height-pixels - brwDocs:y - 5.
    
  {lib/resize-column.i &browse-name=brwDocs &col="'details'" &var=dColumnWidth}
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFileType C-Win 
FUNCTION getFileType RETURNS CHARACTER
  ( input pFileName as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  def var tFileExt as character init "".
  def var tFileType as character no-undo.

  if num-entries(pFileName,".") > 1 then
  tFileExt = trim(entry(num-entries(pFileName,"."), pFileName, ".")).

  if tFileEXt <> "" then
  do:
    case tFileExt:
      when "doc" or when "docx" then
      tFileType = "Word".
      when "xls" or when "xlsx" then
      tFileType = "Excel".
      when "ppt" or when "pptx" then
      tFileType = "PowerPoint".
      when "msg" then
      tFileType = "Email".
      otherwise
      tFileType = caps(tFileExt).
    end case.
  end.

  RETURN tFileType.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshDocs C-Win 
FUNCTION refreshDocs RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define buffer document for document.
  
  close query brwDocs.
  empty temp-table document.
  
  run GetRepositoryFolder (input pServerPath + tCurrentFolder, output table document, output std-lo, output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch view-as alert-box warning.
      return false.
    end.
  
  for each document where document.type <> "file":
    delete document.
  end.
  
  for each document:
    document.filetype = getFileType(document.displayName).
  end.
   
  {lib/brw-startSearch-multi.i &browseName="brwDocs" &sortClause="'by displayName'"}
  apply "value-changed" to brwDocs in frame {&frame-name}.
  
  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshFolders C-Win 
FUNCTION refreshFolders RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
/* def buffer folder for folder.  */

  close query brwFolders.
  empty temp-table folder.
  
  run GetRepositoryFolder (pServerPath, output table folder, output std-lo, output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch view-as alert-box warning.
      return false.
    end.
  
  /* Get 'root' folder */
  run GetRepositoryItem (pServerPath, output table root, output std-lo, output std-ch).
  for first root exclusive-lock:
    assign
      root.name        = ""
      root.displayName = ".<" + tRootFolder + ">"
      .
    create folder.
    buffer-copy root to folder.
        
    if root.fileCount > 0
     then run server/linkdocument.p (pEntityType, pEntityID, pEntitySeq, output std-lo, output std-ch).
     else run server/unlinkdocument.p (pEntityType, pEntityID, pEntitySeq, output std-lo, output std-ch).
  end.
   
  for each folder where folder.type <> "folder":
    delete folder.
  end.
  
  open query brwFolders for each folder by folder.displayName.
  apply "value-changed" to brwFolders in frame {&frame-name}.
  apply "entry" to brwFolders in frame {&frame-name}.

RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION uploadFile C-Win 
FUNCTION uploadFile RETURNS LOGICAL PRIVATE
  ( input pFile as character,
    output pMsg as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tFileID as character no-undo.
  
  if not available folder
   then
    do:
      pMsg = "No folder is selected.".
      return false.
    end.
   
  if folder.fullPath = "" or folder.fullPath = ?
   then
    do:
      pMsg = "Folder path is blank or unknown.".
      return false.
    end.
  
  file-info:file-name = pFile.
  if file-info:full-pathname = ?
    or index(file-info:file-type, "F") = 0
    or index(file-info:file-type, "R") = 0
   then 
    do:
      pMsg = pFile + " is not accessible".
      return false.
    end.
  
  {lib/pbshow.i "'Uploading document, please wait...'"}
  {lib/pbupdate.i "'Uploading document...'" 10}
  
  setUserToken("", "", output std-ch).
  run UploadRepositoryFile (pFile, folder.fullPath, output std-lo, output pMsg).
  
  if not std-lo 
   then
    do:
      if pMsg = ""
       then pMsg = "Could not upload file".
      {lib/pbhide.i}
      return false.
    end.
  
  {lib/pbupdate.i "'Uploading document...'" 80}
  
  if pEntityType > ""
   then run server/linkdocument.p (pEntityType, pEntityID, pEntitySeq, output std-lo, output std-ch).
   else std-lo = true.
  {&window-name}:move-to-top().
  
  {lib/pbupdate.i "'Upload complete'" 100 &delay=1}
  {lib/pbhide.i}
  
  RETURN std-lo.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

