&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Notes (whistoricalobjectives.w)
@description To show historical objectives and their sentiments for an agent
             
@param iAgentID;integer;

@author Sachin Chaturvedi
@version 1.0
@created 2020/07/02
@notes 
---------------------------------------------------------------------*/


/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

{lib/std-def.i}
{lib/amd-def.i}
{lib/brw-multi-def.i}
 
CREATE WIDGET-POOL.
define input parameter ipcAgentID     as character no-undo.
define input parameter ipcName        as character no-undo.
define input parameter hFileDataSrv   as handle    no-undo.

/* ***************************  Definitions  ************************** */

{tt/agent.i}
{tt/objective.i}
{tt/objective.i &tableAlias="ttObjective"}
{tt/sentiment.i}
{tt/sentiment.i &tableAlias="ttSentiment"}
{tt/sentiment.i &tableAlias="tempSentiment"}
{lib/add-delimiter.i}

define variable hModifyWindow      as handle no-undo.
define variable cAgentID           as character no-undo.
define variable cLastSearchString  as character no-undo.
define variable lApplySearchString as logical   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwObj

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES objective sentiment

/* Definitions for BROWSE brwObj                                        */
&Scoped-define FIELDS-IN-QUERY-brwObj objective.private getObjStatus(objective.stat) @ objective.stat objective.dueDate objective.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwObj   
&Scoped-define SELF-NAME brwObj
&Scoped-define QUERY-STRING-brwObj for each objective no-lock
&Scoped-define OPEN-QUERY-brwObj open query {&SELF-NAME} for each objective no-lock.
&Scoped-define TABLES-IN-QUERY-brwObj objective
&Scoped-define FIRST-TABLE-IN-QUERY-brwObj objective


/* Definitions for BROWSE brwSentiment                                  */
&Scoped-define FIELDS-IN-QUERY-brwSentiment getSentimentDef(Sentiment.type) @ Sentiment.typeDesc Sentiment.createDate Sentiment.personName Sentiment.notes   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwSentiment   
&Scoped-define SELF-NAME brwSentiment
&Scoped-define QUERY-STRING-brwSentiment for each sentiment
&Scoped-define OPEN-QUERY-brwSentiment open query {&SELF-NAME} for each sentiment.
&Scoped-define TABLES-IN-QUERY-brwSentiment sentiment
&Scoped-define FIRST-TABLE-IN-QUERY-brwSentiment sentiment


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwObj}~
    ~{&OPEN-QUERY-brwSentiment}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bSearch RECT-1 RECT-2 bRefresh rsType ~
cbStatus fSearch brwObj brwSentiment bNewStmt 
&Scoped-Define DISPLAYED-OBJECTS rsType cbStatus fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getObjStatus C-Win 
FUNCTION getObjStatus RETURNS CHARACTER
  ( input cObjStat as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getSentimentDef C-Win 
FUNCTION getSentimentDef RETURNS CHARACTER
  ( input iSentmtType as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD SearchObjective C-Win 
FUNCTION SearchObjective RETURNS LOGICAL
  ( input hBuffer as handle,
    input pSearch as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwQAR 
       MENU-ITEM m_Mail_auditor LABEL "Mail auditor"  .

DEFINE MENU POPUP-MENU-brwQAR-2 
       MENU-ITEM m_Mail_auditor-2 LABEL "Mail auditor"  .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bDelStmt  NO-FOCUS
     LABEL "Delete" 
     SIZE 4.6 BY 1.14 TOOLTIP "Delete the sentiment".

DEFINE BUTTON bEditStmt  NO-FOCUS
     LABEL "Edit" 
     SIZE 4.6 BY 1.14 TOOLTIP "Update Sentiment".

DEFINE BUTTON bNewStmt  NO-FOCUS
     LABEL "New" 
     SIZE 4.6 BY 1.14 TOOLTIP "Add a sentiment".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh data".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search".

DEFINE VARIABLE cbStatus AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20 BY 1 TOOLTIP "Select the staus of objectives to view" NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 47.2 BY 1 TOOLTIP "Search" NO-UNDO.

DEFINE VARIABLE rsType AS CHARACTER INITIAL "T" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Our", "O",
"Their", "T"
     SIZE 18 BY 1.33 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 129.2 BY 2.48.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 9.8 BY 2.48.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwObj FOR 
      objective SCROLLING.

DEFINE QUERY brwSentiment FOR 
      sentiment SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwObj
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwObj C-Win _FREEFORM
  QUERY brwObj DISPLAY
      objective.private                                  column-label "IsPrivate"  width 10 view-as toggle-box
getObjStatus(objective.stat) @ objective.stat column-label "Status" format "x(10)"     width 15
objective.dueDate                                  label "Due"              format "99/99/99"  width 15
objective.description                              label "Description"      format "x(100)"  width 115
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 158 BY 11.81
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Objectives" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwSentiment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwSentiment C-Win _FREEFORM
  QUERY brwSentiment DISPLAY
      getSentimentDef(Sentiment.type) @ Sentiment.typeDesc column-label "Type" format "x(10)"     width 11
Sentiment.createDate label "Created" format "99/99/99"  width 11
Sentiment.personName   label "Person"      format "x(30)" width 30     
Sentiment.notes   label "Notes"      format "x(100)" width 99
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 152 BY 9.81
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Sentiments" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bSearch AT ROW 1.91 COL 123.6 WIDGET-ID 308 NO-TAB-STOP 
     bRefresh AT ROW 1.91 COL 133 WIDGET-ID 4 NO-TAB-STOP 
     rsType AT ROW 2.1 COL 12.8 NO-LABEL WIDGET-ID 316
     cbStatus AT ROW 2.24 COL 40 COLON-ALIGNED WIDGET-ID 312
     fSearch AT ROW 2.24 COL 74 COLON-ALIGNED WIDGET-ID 314
     brwObj AT ROW 4.57 COL 3 WIDGET-ID 400
     brwSentiment AT ROW 17.05 COL 9 WIDGET-ID 500
     bDelStmt AT ROW 19.19 COL 3 WIDGET-ID 208 NO-TAB-STOP 
     bEditStmt AT ROW 18.1 COL 3 WIDGET-ID 272 NO-TAB-STOP 
     bNewStmt AT ROW 17 COL 3 WIDGET-ID 274 NO-TAB-STOP 
     "Action" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.14 COL 132.8 WIDGET-ID 328
     "Type:" VIEW-AS TEXT
          SIZE 5.6 BY .62 AT ROW 2.38 COL 7 WIDGET-ID 320
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.14 COL 4 WIDGET-ID 326
     RECT-1 AT ROW 1.52 COL 3 WIDGET-ID 322
     RECT-2 AT ROW 1.52 COL 131.8 WIDGET-ID 324
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 162 BY 26.81 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Historical Objectives"
         HEIGHT             = 26.86
         WIDTH              = 162
         MAX-HEIGHT         = 48.43
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 48.43
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwObj fSearch DEFAULT-FRAME */
/* BROWSE-TAB brwSentiment brwObj DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bDelStmt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bEditStmt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwObj:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwQAR:HANDLE
       brwObj:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwObj:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

ASSIGN 
       brwSentiment:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwQAR-2:HANDLE
       brwSentiment:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwSentiment:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwObj
/* Query rebuild information for BROWSE brwObj
     _START_FREEFORM
open query {&SELF-NAME} for each objective no-lock.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwObj */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwSentiment
/* Query rebuild information for BROWSE brwSentiment
     _START_FREEFORM
open query {&SELF-NAME} for each sentiment.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwSentiment */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Historical Objectives */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Historical Objectives */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Historical Objectives */
DO:
  run WindowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelStmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelStmt C-Win
ON CHOOSE OF bDelStmt IN FRAME DEFAULT-FRAME /* Delete */
DO:
  run deleteSentiment in this-procedure.    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEditStmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditStmt C-Win
ON CHOOSE OF bEditStmt IN FRAME DEFAULT-FRAME /* Edit */
DO:
      define variable iSentimentID as integer no-undo.
      
      find current sentiment no-error.
      if not available sentiment
       then
        return.
        
      find current objective no-error.
      if not available objective
       then
        return.  

      empty temp-table tempSentiment.
      create tempSentiment.
      buffer-copy sentiment to tempSentiment.

      run dialogsentiment.w(input hFileDataSrv,
                            input objective.description,
                            input table tempSentiment,
                            output iSentimentID,
                            output std-lo).
      if not std-lo
       then
        return no-apply.
      
      empty temp-table tempSentiment. 
      run getSentiment in hFileDataSrv(input iSentimentID,
                                       output table tempSentiment).
      for first tempSentiment:
        for first ttSentiment where ttSentiment.sentimentID = tempSentiment.sentimentID:
           buffer-copy tempSentiment to ttSentiment. 
        end.
      end.
      
      apply 'value-changed' to brwObj.
      
      find first sentiment where sentiment.sentimentID = tempSentiment.sentimentID no-error.
      if available sentiment
       then std-ro = rowid(sentiment).

      reposition brwSentiment to rowid std-ro no-error.
      
      if objective.stat = {&ActiveStat} /* As timeline tab in agent window shows active objectives only */
       then
        publish "refreshAgentTimeline" (input ipcAgentID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNewStmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewStmt C-Win
ON CHOOSE OF bNewStmt IN FRAME DEFAULT-FRAME /* New */
DO: 
      define variable iSentimentID as integer no-undo.
      
      find current objective no-error.

      if not available objective
       then
        return.

      empty temp-table tempSentiment.
      create tempSentiment.

      tempSentiment.objectiveID  = objective.objectiveID.

      run dialogsentiment.w(input hFileDataSrv,
                            input objective.description,
                            input table tempSentiment,
                            output iSentimentID,
                            output std-lo).
      if not std-lo
       then
        return no-apply.
         
      run getSentiment in hFileDataSrv(input iSentimentID,
                                       output table tempSentiment).
      for first tempSentiment:
        create ttSentiment.
        buffer-copy tempSentiment to ttSentiment. 
      end.
      
      apply 'value-changed' to brwObj.
      
      find first sentiment where sentiment.sentimentID = iSentimentID no-error.
      if available sentiment
       then std-ro = rowid(sentiment).

      reposition brwSentiment to rowid std-ro no-error.
      
      if objective.stat = {&ActiveStat} /* As timeline tab in agent window shows active objectives only */
       then
        publish "refreshAgentTimeline" (input ipcAgentID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME DEFAULT-FRAME /* Go */
DO:
  run loadObjectives in hFileDataSrv (input ''              /* All */ 
                                     ).
  run getdata    in this-procedure.
  run filterData in this-procedure.
  fSearch:screen-value = cLastSearchString.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwObj
&Scoped-define SELF-NAME brwObj
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwObj C-Win
ON ROW-DISPLAY OF brwObj IN FRAME DEFAULT-FRAME /* Objectives */
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwObj C-Win
ON START-SEARCH OF brwObj IN FRAME DEFAULT-FRAME /* Objectives */
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwObj}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwObj C-Win
ON VALUE-CHANGED OF brwObj IN FRAME DEFAULT-FRAME /* Objectives */
DO:
  
  define buffer ttSentiment for ttSentiment.
  empty temp-table sentiment.
  find current objective no-error.

  if available objective
   then
    do:
      
      for each ttsentiment where ttsentiment.objectiveID = objective.objectiveID:
        create sentiment.
        buffer-copy ttSentiment to sentiment.
      end.
    
      close query brwsentiment.  
      open query brwsentiment for each sentiment by sentiment.createDate desc.
      
      run displaywidgets in this-procedure.
    end.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwSentiment
&Scoped-define SELF-NAME brwSentiment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSentiment C-Win
ON DEFAULT-ACTION OF brwSentiment IN FRAME DEFAULT-FRAME /* Sentiments */
DO:
  apply "choose" to bEditStmt.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSentiment C-Win
ON ROW-DISPLAY OF brwSentiment IN FRAME DEFAULT-FRAME /* Sentiments */
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSentiment C-Win
ON START-SEARCH OF brwSentiment IN FRAME DEFAULT-FRAME /* Sentiments */
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwSentiment}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSentiment C-Win
ON VALUE-CHANGED OF brwSentiment IN FRAME DEFAULT-FRAME /* Sentiments */
DO:
  run displaywidgets in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  assign
      lApplySearchString = true  
      cLastSearchString  = fSearch:input-value
      .
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbStatus C-Win
ON VALUE-CHANGED OF cbStatus IN FRAME DEFAULT-FRAME /* Status */
DO:
  run filterdata in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  apply 'choose':U to bSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rsType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rsType C-Win
ON VALUE-CHANGED OF rsType IN FRAME DEFAULT-FRAME
DO:
  run filterdata in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwObj
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{&window-name}:window-state = 2.
{lib/win-main.i}
{lib/brw-main-multi.i &browse-list="brwObj,brwSentiment"}

subscribe to "CloseWindow" anywhere.
subscribe to "refreshHistoricalObjs" anywhere.

/* if valid-handle(hFileDataSrv)                            */
/*  then run GetAgent in hFileDataSrv (output table agent). */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
       

assign
  {&window-name}:min-height-pixels = {&window-name}:height-pixels
  {&window-name}:min-width-pixels = {&window-name}:width-pixels
  {&window-name}:max-height-pixels = session:height-pixels
  {&window-name}:max-width-pixels = session:width-pixels
  .
  
bSearch:load-image-up("images/magnifier.bmp").
bSearch:load-image-insensitive("images/magnifier-i.bmp").
bRefresh:load-image("images/sync.bmp").
bRefresh:load-image-insensitive("images/sync-i.bmp").

bNewStmt:load-image("images/s-add.bmp").
bEditStmt:load-image("images/s-update.bmp").
bEditStmt:load-image-insensitive("images/s-update-i.bmp").
bDelStmt:load-image("images/s-delete.bmp").
bDelStmt:load-image-insensitive("images/s-delete-i.bmp").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Status type */
{lib/get-sysprop-list.i &combo=cbStatus &appCode="'CRM'" &objAction="'Objective'" &objProperty="'Status'" &addAll=true}

{&window-name}:title = {&window-name}:title  + " - " + ipcName + " [" + ipcAgentID + "]".

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  run getData       in this-procedure. 
  run filterData    in this-procedure.
  run WindowResized in this-procedure.
  run ShowWindow    in this-procedure.
  {&window-name}:window-state = 3.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseWindow C-Win 
PROCEDURE CloseWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "WINDOW-CLOSE" to {&window-name}.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteSentiment C-Win 
PROCEDURE deleteSentiment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  find current sentiment no-error.
  if available sentiment 
   then
    do:
      run deletesentiment in hFileDataSrv (input sentiment.sentimentID,
                                           output std-lo,
                                           output std-ch).
      if not std-lo
       then
        do:
          message std-ch
          view-as alert-box information button ok.
          return no-apply.
        end.
        

      for first ttSentiment where ttSentiment.sentimentID = sentiment.sentimentID:
        delete ttSentiment.
      end.
      
      apply 'value-changed' to brwObj in frame {&frame-name}.
      
      find current objective no-error.
      if available objective and 
          objective.stat = {&ActiveStat} /* As timeline tab in agent window shows active objectives only */
       then   
        publish "refreshAgentTimeline" (input ipcAgentID).

    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayWidgets C-Win 
PROCEDURE displayWidgets :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  publish "GetCredentialsID" (output std-ch). 
  
  find current objective no-error.
  find current sentiment no-error.
        
  if available objective            and 
     available sentiment            and
     objective.stat = {&ActiveStat} and 
     sentiment.uid = std-ch
   then
    assign
        bEditStmt:sensitive = true
        bDelStmt:sensitive  = true
        .
   else
    assign
        bEditStmt:sensitive = false
        bDelStmt:sensitive  = false
        .
  if not available objective 
   then
    bNewStmt:sensitive = false.
   else
    bNewStmt:sensitive = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY rsType cbStatus fSearch 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bSearch RECT-1 RECT-2 bRefresh rsType cbStatus fSearch brwObj 
         brwSentiment bNewStmt 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwObj.
  close query brwSentiment.
  empty temp-table objective.
  
  do with frame {&frame-name}:
  end.

  define buffer ttObjective for ttObjective.
  
  if lApplySearchString /* if search filter is also applied */
   then
    for each ttObjective  
        where ttObjective.type = rsType:input-value
          and ttObjective.stat = (if cbStatus:input-value = {&all} then ttObjective.stat else cbStatus:input-value)
          and ((if trim(cLastSearchString) <> "" then ttObjective.description matches ("*" + trim(cLastSearchString) + "*")
                                                 else ttObjective.description = ttObjective.description) or
               (if trim(cLastSearchString) <> "" then ttObjective.stat matches ("*" + trim(cLastSearchString) + "*")
                                                 else ttObjective.stat = ttObjective.stat)
              ):
        create objective.
        buffer-copy ttObjective to objective.
    end.
  
   else
    for each ttObjective  
     where ttObjective.type = rsType:input-value
       and ttObjective.stat = (if cbStatus:input-value = {&all} then ttObjective.stat else cbStatus:input-value):
     create objective.
     buffer-copy ttObjective to objective.
    end.
  
  open query brwObj for each objective by objective.stat.
  apply 'value-changed' to brwObj.
  
  run displayWidgets in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run getObjectives in hFileDataSrv (input "",              /* All */ 
                                     output table ttObjective,
                                     output table ttSentiment
                                     ).
                                   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshHistoricalObjs C-Win 
PROCEDURE refreshHistoricalObjs :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcharAgentID as character no-undo.
  define input parameter ipcharType    as character no-undo.

  if ipcharAgentID ne ipcAgentID 
   then
    return.
    
  find current objective no-error.
  if not available objective
   then
    return.
    
  run getData    in this-procedure.
  
  if (objective.stat ne {&ActiveStat}) or
     (objective.type ne ipcharType)
   then
    return.
    
  run filterData in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE WindowResized C-Win 
PROCEDURE WindowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      frame {&frame-name}:width-pixels = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels


    browse brwObj:width-pixels = frame default-frame:width-pixels -  21. 
    browse brwSentiment:width-pixels = frame default-frame:width-pixels - 51.

    if {&window-name}:width-pixels > frame default-frame:width-pixels 
     then
      do: 
        frame default-frame:width-pixels = {&window-name}:width-pixels.
        frame default-frame:virtual-width-pixels = {&window-name}:width-pixels.                
      end.
     else
      do:
        frame default-frame:virtual-width-pixels = {&window-name}:width-pixels.
        frame default-frame:width-pixels = {&window-name}:width-pixels.
      end.

    browse brwSentiment:height-pixels = frame default-frame:height-pixels - 358. 
      .
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getObjStatus C-Win 
FUNCTION getObjStatus RETURNS CHARACTER
  ( input cObjStat as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  std-ch = "".
  publish "GetSysPropDesc" ("CRM", "Objective", "Status", cObjStat, output std-ch).
  return std-ch.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getSentimentDef C-Win 
FUNCTION getSentimentDef RETURNS CHARACTER
  ( input iSentmtType as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  std-ch = "".
  publish "GetSysPropDesc" ("CRM", "Sentiment", "Description", string(iSentmtType), output std-ch).
  return std-ch.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION SearchObjective C-Win 
FUNCTION SearchObjective RETURNS LOGICAL
  ( input hBuffer as handle,
    input pSearch as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  return hBuffer:buffer-field("notes"):buffer-value() matches "*" + pSearch + "*" or
         hBuffer:buffer-field("subject"):buffer-value() matches "*" + pSearch + "*".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

