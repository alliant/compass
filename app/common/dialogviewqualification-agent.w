&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogqualification.w

  Description: User Interface to add or edit a qualification.

  Input Parameters:
  
  Output Parameters:

  Author: Naresh Chopra 

  Created: 03.11.2018
  Modification:
  Date          Name      Description
  12/21/2018    Sachin    Modified to remove review temptable and use 
                          qualification temptable to create review record
  04/11/19      Rahul     Add mandatory flag '*' in stateId,qualification fillIn 
                          and add validations.
  08/09/2019    Gurvindar Removed progress error while populating combo-box                       
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{tt/qualification.i &tableAlias="tqualification"}


{lib/getstatename.i}


/* Parameters Definitions ---                                           */


define input  parameter ipcEntity       as character no-undo.
define input  parameter ipcEntityID     as character no-undo.
define input  parameter ipcStateID      as character no-undo.
define input  parameter ipcQualID       as integer   no-undo.


/* Library Files   */
{lib/std-def.i}
{lib/com-def.i} 

/* Local Variables */
define variable cCurrUser as character no-undo.
define variable cList     as character no-undo.
define variable cQualList as character no-undo.
define variable lIsNew    as logical   no-undo.

/* Variables defined to be used in IP enableDisableSave. */
define variable chstate          as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fLicensenum feffDate fProvider fexpDate ~
fcoverageAmt RetroDate fdeductAmt fAggregateAmt InsuranceBroker Enotes ~
reviewDate reviewby validDays nxtReviewDate RECT-1 RECT-61 
&Scoped-Define DISPLAYED-OBJECTS CbState fLicensenum fMarkMandatory1 ~
feffDate cbQual fProvider fexpDate fMarkMandatory2 tactive fcoverageAmt ~
RetroDate fdeductAmt cbStatus fAggregateAmt cbAuth InsuranceBroker Enotes ~
reviewDate reviewby validDays nxtReviewDate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validateQual Dialog-Frame 
FUNCTION validateQual returns logic
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE VARIABLE cbAuth AS CHARACTER FORMAT "X(256)":U 
     LABEL "Authorized by" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "First-Party","Third-Party" 
     DROP-DOWN-LIST
     SIZE 15.4 BY 1 NO-UNDO.

DEFINE VARIABLE cbQual AS CHARACTER FORMAT "X(256)":U 
     LABEL "Qualification" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 57.6 BY 1 NO-UNDO.

DEFINE VARIABLE CbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE cbStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE Enotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 85.6 BY 3.95 NO-UNDO.

DEFINE VARIABLE fAggregateAmt AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Aggregate Amount $" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fcoverageAmt AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Coverage Amount $" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fdeductAmt AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Deductible Amount $" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE feffDate AS DATE FORMAT "99/99/99":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fexpDate AS DATE FORMAT "99/99/99":U 
     LABEL "Expiration" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fLicensenum AS CHARACTER FORMAT "X(256)":U 
     LABEL "Number/ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 31.4 BY 1 NO-UNDO.

DEFINE VARIABLE fMarkMandatory1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fProvider AS CHARACTER FORMAT "X(256)":U 
     LABEL "Issuer" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 31.4 BY 1 NO-UNDO.

DEFINE VARIABLE InsuranceBroker AS CHARACTER FORMAT "X(256)":U 
     LABEL "Broker" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 61 BY 1 NO-UNDO.

DEFINE VARIABLE nxtReviewDate AS DATE FORMAT "99/99/99":U 
     LABEL "Next Review Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE RetroDate AS DATE FORMAT "99/99/99":U 
     LABEL "Retro Respective" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE reviewby AS CHARACTER FORMAT "X(256)":U 
     LABEL "Reviewed By" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 28.2 BY 1 NO-UNDO.

DEFINE VARIABLE reviewDate AS DATE FORMAT "99/99/99":U 
     LABEL "Review date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE validDays AS INTEGER FORMAT "->>>>":U INITIAL 0 
     LABEL "Valid for Days" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 85.6 BY 4.67.

DEFINE RECTANGLE RECT-61
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 85.8 BY 3.38.

DEFINE VARIABLE tactive AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 3.4 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     CbState AT ROW 1.86 COL 17.6 COLON-ALIGNED WIDGET-ID 314 NO-TAB-STOP 
     fLicensenum AT ROW 4.24 COL 17.6 COLON-ALIGNED WIDGET-ID 42
     fMarkMandatory1 AT ROW 2.1 COL 41.6 COLON-ALIGNED NO-LABEL WIDGET-ID 52 NO-TAB-STOP 
     feffDate AT ROW 5.43 COL 66 COLON-ALIGNED WIDGET-ID 6
     cbQual AT ROW 3.05 COL 17.6 COLON-ALIGNED WIDGET-ID 218 NO-TAB-STOP 
     fProvider AT ROW 6.62 COL 17.6 COLON-ALIGNED WIDGET-ID 236
     fexpDate AT ROW 6.62 COL 66 COLON-ALIGNED WIDGET-ID 22
     fMarkMandatory2 AT ROW 3.29 COL 75.2 COLON-ALIGNED NO-LABEL WIDGET-ID 330 NO-TAB-STOP 
     tactive AT ROW 3.14 COL 89.4 WIDGET-ID 244 NO-TAB-STOP 
     fcoverageAmt AT ROW 9.57 COL 26.6 COLON-ALIGNED WIDGET-ID 222
     RetroDate AT ROW 9.57 COL 71.6 COLON-ALIGNED WIDGET-ID 226
     fdeductAmt AT ROW 10.76 COL 26.6 COLON-ALIGNED WIDGET-ID 224
     cbStatus AT ROW 4.24 COL 66 COLON-ALIGNED WIDGET-ID 30 NO-TAB-STOP 
     fAggregateAmt AT ROW 10.76 COL 71.6 COLON-ALIGNED WIDGET-ID 328
     cbAuth AT ROW 5.43 COL 17.6 COLON-ALIGNED WIDGET-ID 220 NO-TAB-STOP 
     InsuranceBroker AT ROW 11.95 COL 26.6 COLON-ALIGNED WIDGET-ID 234
     Enotes AT ROW 14.86 COL 6.4 NO-LABEL WIDGET-ID 34
     reviewDate AT ROW 20.14 COL 20.6 COLON-ALIGNED WIDGET-ID 320
     reviewby AT ROW 20.14 COL 59.4 COLON-ALIGNED WIDGET-ID 326
     validDays AT ROW 21.33 COL 20.6 COLON-ALIGNED WIDGET-ID 324
     nxtReviewDate AT ROW 21.33 COL 59.4 COLON-ALIGNED WIDGET-ID 322
     "Insurance (If Applicable)" VIEW-AS TEXT
          SIZE 23.8 BY .62 AT ROW 8.62 COL 7.6 WIDGET-ID 240
     "Review" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 19.29 COL 7.6 WIDGET-ID 318
     "Notes:" VIEW-AS TEXT
          SIZE 6.8 BY .62 AT ROW 14.1 COL 6.4 WIDGET-ID 242
     "Active:" VIEW-AS TEXT
          SIZE 6.8 BY .62 AT ROW 3.29 COL 81.8 WIDGET-ID 246
     RECT-1 AT ROW 8.95 COL 6.6 WIDGET-ID 238
     RECT-61 AT ROW 19.57 COL 6.4 WIDGET-ID 316
     SPACE(4.99) SKIP(0.33)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "View qualifications" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR COMBO-BOX cbAuth IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbQual IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX CbState IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbStatus IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       Enotes:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE
       Enotes:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fAggregateAmt:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fcoverageAmt:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fdeductAmt:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       feffDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fexpDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fLicensenum:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fMarkMandatory1 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory2 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fProvider:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       InsuranceBroker:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       nxtReviewDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       RetroDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       reviewby:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       reviewDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR TOGGLE-BOX tactive IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       validDays:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* View qualifications */
do:
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? 
 then
  frame {&frame-name}:parent = active-window.
  run getdata in this-procedure.
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.
 
  

  if std-ch ne ""
   then
    cbStatus:list-item-pairs = replace(std-ch,"^",",").
  else
   cbStatus:list-item-pairs = ",".
  
  run displayData in this-procedure.
   
  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  find first tQualification no-error.
        if available tQualification 
         then
          do:
            if tQualification.stat ne "" and
               not can-do(cbStatus:list-item-pairs,tQualification.stat) 
             then
              run updateStatusCombo (input tQualification.stat).
           
            if lookup(tQualification.Qualification,cQualList) = 0
              then
               cbQual:add-last(tQualification.Qualification).
               
            chstate = getStateName (tQualification.stateid).
            
            cbstate:add-last(chstate).
            
            assign
                reviewDate     :screen-value = string( tQualification.reviewDate)
                reviewby       :screen-value = tQualification.reviewBy 
                nxtreviewDate  :screen-value = string(tQualification.nextReviewDueDate)
                validdays      :screen-value = string(tQualification.numvaliddays)
                cbQual         :screen-value = string(tQualification.Qualification)
                cbStatus       :screen-value = tQualification.stat
                cbAuth         :screen-value = if string(tQualification.authorizedBy) = {&CompanyCode} then {&First-Party} else {&Third-Party}
                feffDate       :screen-value = string(tQualification.EffectiveDate)
                fexpDate       :screen-value = string(tQualification.ExpirationDate)
                Enotes         :screen-value = string(tQualification.notes)
                fLicensenum    :screen-value = tQualification.QualificationNum
                insurancebroker:screen-value = tQualification.insurancebroker
                cbState        :screen-value = chstate
                fProvider      :screen-value = tQualification.provider
                fcoverageamt   :screen-value = string(tQualification.coverageamt)
                fdeductAmt     :screen-value = string(tQualification.deductibleamt)
                retrodate      :screen-value = string(tQualification.retrodate)
                tactive        :screen-value = string(tQualification.activ)
                fAggregateAmt  :screen-value = string(tQualification.aggregateamt)
                .
          end. 
          
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY CbState fLicensenum fMarkMandatory1 feffDate cbQual fProvider fexpDate 
          fMarkMandatory2 tactive fcoverageAmt RetroDate fdeductAmt cbStatus 
          fAggregateAmt cbAuth InsuranceBroker Enotes reviewDate reviewby 
          validDays nxtReviewDate 
      WITH FRAME Dialog-Frame.
  ENABLE fLicensenum feffDate fProvider fexpDate fcoverageAmt RetroDate 
         fdeductAmt fAggregateAmt InsuranceBroker Enotes reviewDate reviewby 
         validDays nxtReviewDate RECT-1 RECT-61 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE expDtLeave Dialog-Frame 
PROCEDURE expDtLeave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if (nxtreviewDate:input-value in frame {&frame-name}) gt (fexpDate:input-value) 
   then
    do:
      if (date(fexpDate:input-value) - 14) gt today 
       then
        nxtreviewDate:screen-value = string(date(fexpDate:input-value) - 14).
      else
        nxtreviewDate:screen-value = string(today + 1).
    end.
  
  apply "value-changed" to nxtreviewDate.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData Dialog-Frame 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable oplSuccess as logical   no-undo.
 define variable opcMsg     as character no-undo.
 
/* client Server Call */
 run server/getqualifications.p (input ipcStateID,
                                 input ipcEntity,
                                 input ipcEntityID,
                                 input ipcQualID,
                                 output table tqualification,
                                 output oplSuccess,
                                 output opcMsg).

 if not oplSuccess 
  then
   return.                                                                         
 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState Dialog-Frame 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable cAuthByVal as character no-undo.

 if cbstatus:input-value in frame {&frame-name} = {&Waived} 
  then
   do:
      assign 
          fLicenseNum    :sensitive      = false
          fProvider      :sensitive      = false
          cbAuth         :sensitive      = false
          fCoverageAmt   :sensitive      = false
          fDeductAmt     :sensitive      = false
          fAggregateAmt  :sensitive      = false
          insuranceBroker:sensitive      = false
          retroDate      :sensitive      = false
          fLicenseNum    :screen-value   = ""
          fProvider      :screen-value   = ""
          cbAuth         :list-items     = ""
          fCoverageAmt   :screen-value   = ""
          fDeductAmt     :screen-value   = ""
          InsuranceBroker:screen-value   = ""
          retroDate      :screen-value   = ""
          fAggregateAmt  :screen-value   = ""
          .
     if lIsNew 
      then
       cbQual:sensitive  = false.
   end.
  else 
   do:
     assign  
         cAuthByVal                     = cbAuth:input-value 
         cbAuth         :list-items     = {&first-party} + "," + {&Third-Party} 
         cbAuth         :screen-value   = cAuthByVal 
         fLicenseNum    :sensitive      = true 
         fProvider      :sensitive      = true 
         cbAuth         :sensitive      = true 
         fCoverageAmt   :sensitive      = true 
         fDeductAmt     :sensitive      = true 
         insuranceBroker:sensitive      = true 
         retroDate      :sensitive      = true  
         fAggregateAmt  :sensitive      = true 
         . 
     if lIsNew  
      then 
       cbQual:sensitive  = true. 
   end.

  apply "value-changed" to cbQual.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE updateStatusCombo Dialog-Frame 
PROCEDURE updateStatusCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>                                                     rre
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcQualStat as character  no-undo.
  
  define variable hcmdhandle  as handle     no-undo.

  do with frame {&frame-name}:
  end.

  hcmdhandle = cbStatus:handle.
  std-ch = "".
  publish "GetSysPropDesc" ("COM", "Qualification", "Status", ipcQualStat, output std-ch).

  hcmdhandle:add-last(std-ch,ipcQualStat).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE valChgNxtRevDt Dialog-Frame 
PROCEDURE valChgNxtRevDt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable myDate as date no-undo.

  myDate = date(nxtReviewDate:input-value in frame {&frame-name}) no-error.

  if not error-status:error and not myDate = ? 
   then
    Validdays:screen-value = string(date(nxtReviewDate:input-value) - today).
  else 
   Validdays:screen-value = "0".
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE valChgQual Dialog-Frame 
PROCEDURE valChgQual :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iReviewPeriod as integer   no-undo.
  
  if lIsNew 
   then
    do:
      if cbStatus:input-value in frame {&frame-name} = {&GoodStanding} 
       then
        do:
          publish "getQualReviewPeriod" (input {&Qualification},
                                         input cbQual:input-value, 
                                         output iReviewPeriod,
                                         output std-lo,
                                         output std-ch).
          
          if std-lo and iReviewPeriod ne 0 
           then
            nxtreviewDate:screen-value = string(today + iReviewPeriod).      
        
          if (fexpDate:input-value ne ?) and
             (nxtreviewDate:input-value gt fexpDate:input-value) 
           then
            do:
              if (date(fexpDate:screen-value) - 14) lt today 
               then
                nxtreviewDate:screen-value = string(today + 1).
              else
               nxtreviewDate:screen-value = string(date(fexpDate:input-value) - 14 ).
            end.
        end.
      else
       nxtreviewDate:screen-value = "".
    end.
  
  apply "value-changed" to nxtreviewDate.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE valChgState Dialog-Frame 
PROCEDURE valChgState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* removing select state once other state is selected */
  do with frame {&frame-name}:
  end.

  {lib/modifylist.i &cbhandle=cbState:handle &list=list-item-pairs &delimiter = "," &item = '{&SelectState}'}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validateQual Dialog-Frame 
FUNCTION validateQual returns logic
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable iReviewPeriod as integer  no-undo.

  /* Validations */
  if cbState:input-value in frame {&frame-name} = "" 
   then
    do:
      message "State cannot be blank."
        view-as alert-box info buttons ok.
      return false.
    end.
  
  if cbqual:input-value = "" 
   then
    do:
      message "Qualification cannot be blank"
        view-as alert-box info buttons ok.
      return false.
    end.
    
  if fexpDate:input-value < feffDate:input-value 
   then
    do:
      message "Expiration date cannot be before qualification effective date."
        view-as alert-box info buttons ok.
      return false.
    end.
  
  publish "getQualReviewPeriod" (input {&Qualification},
                                 input  cbQual:input-value,
                                 output iReviewPeriod,
                                 output std-lo,
                                 output std-ch).

  if cbStatus:input-value = {&GoodStanding}  and 
     iReviewPeriod        <> 0               and 
     nxtreviewDate:input-value = ? 
   then
    do:  
      message "Next Review date cannot be blank."
        view-as alert-box info buttons ok.
      assign nxtreviewDate:screen-value = string(today + iReviewPeriod).
      apply "value-changed" to nxtreviewDate. 
      return false.
    end. /* if nxtreviewDate:input-value       = "" or ...*/


  if tActive:checked = true    and 
      (feffDate:input-value = ?)
   then
    do:
      message "Please enter Effective Date as qualification is active."
          view-as alert-box info buttons ok.
      return false.
    end.

  if date(feffDate:input-value) gt today
   then
    do:
      message "Effective Date is in future. Continue?" 
          view-as alert-box warning buttons yes-no update std-lo.
      if not std-lo 
       then
        return false.
    end.

  if date(fexpDate:input-value) lt today and
     cbStatus:screen-value      eq {&GoodStanding}
   then
    do:
      message "The Expiration Date is in past and the status is Good Standing. Continue?" 
          view-as alert-box warning buttons yes-no update std-lo.
      if not std-lo 
       then
        return false.
    end.

  if not (date(fexpDate:input-value) = ?) and (nxtreviewDate:input-value > fexpDate:input-value) 
   then
    do:
      message "New Review Due Date cannot be after the qualification expiration date."
        view-as alert-box info buttons ok.
                
      if (date(fexpDate:input-value) - 14) < today 
       then
        nxtreviewDate:screen-value = string(today + 1). 
       else
        nxtreviewDate:screen-value = string(date(fexpDate:input-value) - 14). 
      apply "value-changed" to nxtreviewDate.
      return false.
    end. /* if not (date(fexpDate:input-value) = ?) and...*/      
  
  if not (date(feffDate:input-value) = ?) and (nxtreviewDate:input-value < feffDate:input-value) 
   then
    do:
      message "Next Review date cannot be less than effective date"
        view-as alert-box info buttons ok.
      return false.
    end. /* if not (date(feffDate:input-value) = ?) and... */


  return true.
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

