&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File:dialognewpersoncontact.w

  Description:Dialog to create relation between Person and personcontact. 

  Input Parameters:
      <none>

  Output Parameters:
      <none>
      
  Author:Sachin Anthwal

  Created:08.18.2022 
  @Modified
  Date         Name            Description
  12/21/22  Sachin Chaturvedi  Task #100776 Validation and Format on Phone number
  04/19/24  S Chandu           Task #111689 Modified to save international(+) numbers
                                and double click on contact changed.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* --Temp-Table Definitions ---                                         */
{tt/personcontact.i}

/* --Parameters Definitions ---                                         */
define input  parameter ipcActionType       as character no-undo.
define input  parameter iphPersondata       as handle    no-undo.
define input  parameter ipcPersonID         as character no-undo.
define input  parameter ipcPersonName       as character no-undo.
define input  parameter table               for personcontact   .
define output parameter oplSuccess          as logical   no-undo.

{lib/std-def.i}
{lib/com-def.i}
{lib/validEmailList.i}
{lib/formatcontactnumber.i}
{lib/isnumeric.i}

/* --Local Variable Definitions ---                                     */
define variable cList         as character no-undo.
define variable cValuePair    as character no-undo. 
define variable cNewList      as character no-undo.
define variable iCount        as integer no-undo.
define variable chType        as character no-undo.
define variable chSubType     as character no-undo.
define variable chContact     as character no-undo.
define variable chExt         as character no-undo.
define variable lIsNew        as logical   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RdAgentPerson cbSubType fiId fExt bCancel 
&Scoped-Define DISPLAYED-OBJECTS RdAgentPerson cbSubType fiId ~
fMarkMandatory2 fExt 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Cancel".

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Create" 
     SIZE 15 BY 1.14 TOOLTIP "Create".

DEFINE VARIABLE cbSubType AS CHARACTER 
     LABEL "Sub Type" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN
     SIZE 48 BY 1 NO-UNDO.

DEFINE VARIABLE fExt AS INTEGER FORMAT ">>>>>":U INITIAL 0 
     LABEL "Ext." 
     VIEW-AS FILL-IN 
     SIZE 8.2 BY 1 NO-UNDO.

DEFINE VARIABLE fiId AS CHARACTER FORMAT "X(256)":U 
     LABEL "Contact" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE fMarkMandatory2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3.2 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE RdAgentPerson AS CHARACTER INITIAL "P" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Phone", "P":U,
"Email", "E":U
     SIZE 25 BY .95 TOOLTIP "Type" NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     RdAgentPerson AT ROW 1.24 COL 12.8 NO-LABEL WIDGET-ID 316
     cbSubType AT ROW 2.33 COL 2.2 WIDGET-ID 30
     fiId AT ROW 3.48 COL 10.6 COLON-ALIGNED WIDGET-ID 50
     fMarkMandatory2 AT ROW 3.71 COL 40.6 COLON-ALIGNED NO-LABEL WIDGET-ID 314
     fExt AT ROW 3.48 COL 50.4 COLON-ALIGNED WIDGET-ID 322
     bSave AT ROW 4.95 COL 15.8
     bCancel AT ROW 4.95 COL 32.4
     "Type:" VIEW-AS TEXT
          SIZE 5.8 BY .62 TOOLTIP "Type" AT ROW 1.38 COL 6.6 WIDGET-ID 320
     SPACE(49.79) SKIP(4.32)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "New Contact"
         DEFAULT-BUTTON bSave CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bSave IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbSubType IN FRAME Dialog-Frame
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN fMarkMandatory2 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* New Contact */
do:
  oplSuccess = false.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel Dialog-Frame
ON CHOOSE OF bCancel IN FRAME Dialog-Frame /* Cancel */
do:
  oplSuccess = false.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave Dialog-Frame
ON CHOOSE OF bSave IN FRAME Dialog-Frame /* Create */
do:
   define  variable  cformatcontact as  character    no-undo .
   define  variable  lerror         as  logical      no-undo .
  if fiId:input-value <> ""  and RdAgentPerson:input-value = 'E'
   then
    do:
      if not validEmailAddr(fiId:screen-value)
       then
        do:
         message "Invalid Email Address" view-as alert-box warning buttons ok.
         apply  "Entry" to  fiId.
         return no-apply.
        end.
     end.
     
   if RdAgentPerson:input-value = 'P'
   then
    do: 
      apply  "Entry" to  fiId.

      Validatenumber(input fiId:screen-value,output cformatcontact ,output lerror).
      if lerror 
       then
        do:
          MESSAGE "Invalid Contact Number"
            VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.
          apply  "Entry" to  fiId.
          return no-apply.
        end. 
       fiId:screen-value = cformatcontact.

    end.

  run savePersonContact in this-procedure.

  /* Variable oplSuccess gets updated in savepersoncontact procedure. */
  if not oplSuccess 
   then
    return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbSubType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbSubType Dialog-Frame
ON VALUE-CHANGED OF cbSubType IN FRAME Dialog-Frame /* Sub Type */
do:
  if self:screen-value = "Mobile" or rdAgentPerson:input-value = 'E' then
   assign 
       fExt:screen-value = '0'
       fExt:hidden =  true.
   else 
   assign
       fExt:screen-value = if avail personcontact and num-entries(personcontact.contactID) > 1 then entry(2,personcontact.contactID) else fExt:screen-value
       fExt:hidden = false.
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fExt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fExt Dialog-Frame
ON VALUE-CHANGED OF fExt IN FRAME Dialog-Frame /* Ext. */
DO:
  run enableDisableSave in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fiId
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiId Dialog-Frame
ON ENTRY OF fiId IN FRAME Dialog-Frame /* Contact */
DO:
 /* self:format = 'x(256)'. */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiId Dialog-Frame
ON LEAVE OF fiId IN FRAME Dialog-Frame /* Contact */
DO:
  if fiId:input-value <> ""  and RdAgentPerson:input-value = 'E'
   then
    fExt:hidden            = true.   
    
  if RdAgentPerson:input-value = 'P' and cbSubType:input-value ne "Mobile"
   then
    fExt:hidden            = false.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiId Dialog-Frame
ON VALUE-CHANGED OF fiId IN FRAME Dialog-Frame /* Contact */
DO:
   run enableDisableSave in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RdAgentPerson
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RdAgentPerson Dialog-Frame
ON VALUE-CHANGED OF RdAgentPerson IN FRAME Dialog-Frame
DO:
   cbSubType:list-items = "".
   fiId:format = 'x(256)'.
   if rdAgentPerson:input-value = 'P'
    then
     assign
         cbSubType:list-items   = "Select Sub Type" + ',' + {&ComPhone}
         cbSubType:screen-value = "Select Sub Type"
         fiId:screen-value      = ""
         fExt:hidden            = false
         .
    else
      assign
          cbSubType:list-items   = "Select Sub Type" + ',' + {&ComEmail}
          cbSubType:screen-value = "Select Sub Type"
          fiId:screen-value      = ""
          fExt:hidden            = true
          fExt:screen-value      = ""
          .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? 
 then
  frame {&frame-name}:parent = active-window.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

   cbSubType:list-items = "".
   if rdAgentPerson = "P" 
    then
     do:
       cbSubType:list-items = "Select Sub Type" + ',' + {&ComPhone}.
       cbSubType:screen-value = "Select Sub Type".
     end.
   else
    do:
      cbSubType:list-items = "Select Sub Type" + ',' + {&ComEmail}.
      cbSubType:screen-value = "Select Sub Type".
    end.
    
  run enable_UI.
  
  run displayData       in this-procedure.
  run enableDisableSave in this-procedure.
  
  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cFormatNumber as char no-undo.
  
  do with frame {&frame-name}:
  end.
  
  if ipcActionType = 'Modify' 
   then
    do:
      RdAgentPerson:sensitive = false.
      
      find first personcontact no-error.
      
      if available personcontact 
       then
        do:
          
          assign 
              frame Dialog-Frame:title          = "Modify Contact"
              bSave             :label          = "Save"
              bSave             :tooltip        = "Save"
              fMarkMandatory2   :screen-value   = {&mandatory}
              fMarkMandatory2   :hidden         = false
              RdAgentPerson:screen-value        = personcontact.contactType
              .
              apply "value-changed" to RdAgentPerson.
              cbSubType         :screen-value   = if personcontact.contactSubType = '' then "Select Sub Type" else personcontact.contactSubType .
              fiId              :screen-value   = entry(1,personcontact.contactID).
              fExt              :screen-value   = if num-entries(personcontact.contactID) > 1 then  entry(2,personcontact.contactID) else ''.
              fExt              :hidden         = if cbSubType:screen-value = "Mobile" or rdAgentPerson:input-value = 'E' then true else false
              no-error.
        end.  /* if available personcontact */
    end.
  else if ipcActionType = 'New' 
   then
   do:
     RdAgentPerson:sensitive = true.
     
     assign
         frame Dialog-Frame:title          = "New Contact"
         bSave             :label          = "Create"  
         bSave             :tooltip        = "Create" 
         fMarkMandatory2   :screen-value   = {&mandatory}
         fMarkMandatory2   :hidden         = false
         cbSubType         :screen-value   = "Select Sub Type"
         .  
   end. /* else */
 
  Validatenumber(input fiId:input-value, output cFormatNumber,output std-lo).
  if not std-lo 
   then
    fiId:screen-value= cFormatNumber.
  
  assign
      chType    = RdAgentPerson :input-value
      chSubType = cbSubType:input-value
      chContact = fiId:input-value
      chExt     = fExt:input-value
      .
      
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* New: enable save, mandatory fields (state and role) is filled. */
  if ipcActionType = 'New'
   then
    do:
      bSave:sensitive =  (fiId:input-value ne "" and fiId:input-value ne ? ) no-error.
    end.    
  else 
   bSave:sensitive =   not ( chType    = RdAgentPerson :input-value and 
                             chSubType = cbSubType:input-value      and
                             chContact = fiId:input-value           and
                             chExt     = fExt:input-value
                            ) no-error.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY RdAgentPerson cbSubType fiId fMarkMandatory2 fExt 
      WITH FRAME Dialog-Frame.
  ENABLE RdAgentPerson cbSubType fiId fExt bCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE savePersonContact Dialog-Frame 
PROCEDURE savePersonContact :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable ipersoncontactID as int no-undo.
  define variable cContactID       as char no-undo.
  
  if fiId:screen-value = "" then
  do:
     message "ContactID cannot be blank"
          view-as alert-box error buttons ok.
      return. 
  end.
  
  if RdAgentPerson:input-value = "P" and substring(fiID:input-value,1,1) <> "+"
   then
    cContactID = substring(fiId:input-value,2,3) + substring(fiId:input-value,7,3) + substring(fiId:input-value,11,4). 
   else
    cContactID = fiId:input-value.
 
  find first personcontact no-error.                  
  if not available personcontact
   then
    do:
      create personcontact.
      publish "GetCredentialsID" (output std-ch).
      assign
          personcontact.uid = std-ch 
          personcontact.createDate = today
          .
    end.
     
  assign
      personcontact.personID        = ipcPersonID
      personcontact.contactType     = RdAgentPerson:input-value
      personcontact.contactSubType  = if cbSubType:input-value = 'Select Sub Type' then "" else cbSubType:input-value
      personcontact.contactID       = cContactID
      personcontact.extension       = if fExt:input-value = '0' then '' else fExt:input-value
      no-error .

  if not valid-handle(iphPersondata)
   then
    do:
      message "Data Model not found."
          view-as alert-box error buttons ok.
      return.
    end.

  if ipcActionType = 'New'
   then
    run newpersoncontact in iphPersondata (input table personcontact,
                                           output ipersoncontactID,
                                           output oplSuccess,
                                           output std-ch).
  else if ipcActionType = 'Modify'
   then
    run modifypersoncontact in iphPersondata (input table personcontact,
                                              output oplSuccess,
                                              output std-ch).
  else
   do:
     run deactivatepersoncontact in iphPersondata (input personcontact.personcontactID,
                                                   input today,
                                                   output oplSuccess,
                                                   output std-ch).
   end. 
   
  if not oplSuccess
   then
    message std-ch
      view-as alert-box error buttons ok.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

