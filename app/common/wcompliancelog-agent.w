&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wcompliancelog.w

  Description: .

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Sachin Chaturvedi

  Created:05.17.2018 
  Modified: 06.12.2020  Archana Gupta  Modified code according to new organization structure
------------------------------------------------------------------------*/
/* ***************************  definitions  ************************** */

create widget-pool.

/* Temp-table definition*/
{tt/comLog.i}
{tt/comlog.i &tableAlias="tcomlog"}
{tt/comlog.i &tableAlias="ttcomlog"}

/* Parameters definitions ---    */
define input parameter pentity   as character no-undo.
define input parameter pEntityID as character no-undo.
define input parameter pStateID  as character no-undo.
define input parameter pRole     as character no-undo.
define input parameter pRoleID   as character no-undo.
define input parameter pName     as character no-undo.

define variable        cRoles as character no-undo.

{lib/std-def.i}
{lib/com-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwResult

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttcomLog

/* Definitions for BROWSE brwResult                                     */
&Scoped-define FIELDS-IN-QUERY-brwResult getComStat(ttcomLog.sequence) @ ttcomLog.sequence ttcomLog.objRef ttcomLog.logdate ttcomLog.username ttcomLog.notes   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwResult   
&Scoped-define SELF-NAME brwResult
&Scoped-define QUERY-STRING-brwResult for each ttcomLog by ttcomLog.logdate descending
&Scoped-define OPEN-QUERY-brwResult open query {&SELF-NAME} for each ttcomLog by ttcomLog.logdate descending.
&Scoped-define TABLES-IN-QUERY-brwResult ttcomLog
&Scoped-define FIRST-TABLE-IN-QUERY-brwResult ttcomLog


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwResult}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bExport bOpen dtStartDate bGet dtEndDate ~
brwResult flEntityName flEntityID RECT-54 RECT-55 RECT-68 RECT-56 
&Scoped-Define DISPLAYED-OBJECTS cbRole dtStartDate dtEndDate flEntityName ~
flEntityID 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getComStat C-Win 
FUNCTION getComStat returns character
 ( input cStat as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getcomstatus C-Win 
FUNCTION getcomstatus RETURNS CHARACTER
  ( input cStat as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL PRIVATE
  ( pValid as logical  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bGet  NO-FOCUS
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE BUTTON bOpen  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open log".

DEFINE VARIABLE cbRole AS CHARACTER FORMAT "X(256)":U 
     LABEL "Role" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 23.8 BY 1 NO-UNDO.

DEFINE VARIABLE dtEndDate AS DATE FORMAT "99/99/99":U 
     LABEL "End Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE dtStartDate AS DATE FORMAT "99/99/99":U 
     LABEL "Start Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flEntityID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity ID" 
     VIEW-AS FILL-IN 
     SIZE 18.6 BY 1 NO-UNDO.

DEFINE VARIABLE flEntityName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity Name" 
     VIEW-AS FILL-IN 
     SIZE 48.2 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-54
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 38 BY 2.48.

DEFINE RECTANGLE RECT-55
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 69.4 BY 2.48.

DEFINE RECTANGLE RECT-56
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17.2 BY 2.48.

DEFINE RECTANGLE RECT-68
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 71.4 BY 2.48.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwResult FOR 
      ttcomLog SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwResult
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwResult C-Win _FREEFORM
  QUERY brwResult DISPLAY
      getComStat(ttcomLog.sequence)  @ ttcomLog.sequence label ""        format "x(2)"                 width 2   
ttcomLog.objRef                                  label "Role"    format "x(25)"                width 18
ttcomLog.logdate                                 label "Update"  format "99/99/9999 HH:MM:SS"  width 23
ttcomLog.username                                label "User ID" format "x(33)"                width 25
ttcomLog.notes                                   label "Notes"   format "x(200)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 194.6 BY 18
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 1.81 COL 181 WIDGET-ID 2 NO-TAB-STOP 
     bOpen AT ROW 1.81 COL 188 WIDGET-ID 266 NO-TAB-STOP 
     cbRole AT ROW 2.14 COL 149.6 COLON-ALIGNED WIDGET-ID 270
     dtStartDate AT ROW 2.14 COL 83.6 COLON-ALIGNED WIDGET-ID 110
     bGet AT ROW 1.81 COL 132 WIDGET-ID 262 NO-TAB-STOP 
     dtEndDate AT ROW 2.14 COL 113.2 COLON-ALIGNED WIDGET-ID 112
     brwResult AT ROW 4.19 COL 2 WIDGET-ID 200
     flEntityName AT ROW 2.67 COL 20.8 COLON-ALIGNED WIDGET-ID 102 NO-TAB-STOP 
     flEntityID AT ROW 1.62 COL 20.8 COLON-ALIGNED WIDGET-ID 104 NO-TAB-STOP 
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.1 COL 180.6 WIDGET-ID 100
     "Parameters" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 1.1 COL 74 WIDGET-ID 108
     "Filters" VIEW-AS TEXT
          SIZE 5.4 BY .62 AT ROW 1.1 COL 143 WIDGET-ID 272
     RECT-54 AT ROW 1.43 COL 142 WIDGET-ID 92
     RECT-55 AT ROW 1.43 COL 73 WIDGET-ID 106
     RECT-68 AT ROW 1.43 COL 2 WIDGET-ID 264
     RECT-56 AT ROW 1.43 COL 179.6 WIDGET-ID 268
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 203 BY 21.67
         DEFAULT-BUTTON bGet WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Compliance Log Details"
         HEIGHT             = 21.43
         WIDTH              = 196.4
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwResult dtEndDate fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwResult:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwResult:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwResult:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR COMBO-BOX cbRole IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       flEntityID:AUTO-RESIZE IN FRAME fMain      = TRUE
       flEntityID:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       flEntityName:AUTO-RESIZE IN FRAME fMain      = TRUE
       flEntityName:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwResult
/* Query rebuild information for BROWSE brwResult
     _START_FREEFORM
open query {&SELF-NAME} for each ttcomLog by ttcomLog.logdate descending.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwResult */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Compliance Log Details */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Compliance Log Details */
do:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Compliance Log Details */
do:
  run windowResized in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGet C-Win
ON CHOOSE OF bGet IN FRAME fMain /* Get */
do:
  /* Validation for start and end date. */
  run validateDate in this-procedure (output std-lo).  
  
  /* if error is returned from the validate procedure, then return. */
  if std-lo 
   then
    return no-apply.

  run getData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOpen C-Win
ON CHOOSE OF bOpen IN FRAME fMain /* Open */
do:
  run openLog in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwResult
&Scoped-define SELF-NAME brwResult
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwResult C-Win
ON DEFAULT-ACTION OF brwResult IN FRAME fMain
DO:
  run openLog in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwResult C-Win
ON ROW-DISPLAY OF brwResult IN FRAME fMain
do:
  {lib/brw-rowDisplay.i}

  /* "3" = Red, 2 = "Yellow", 1 = "Green" */
  case ttcomLog.sequence:
    when "3" 
     then  
      ttcomLog.sequence:bgcolor in browse brwResult = 12.

    when "2" 
     then 
      ttcomLog.sequence:bgcolor in browse brwResult = 14.

    when "1" 
     then
      ttcomLog.sequence:bgcolor in browse brwResult =  2.
  end case.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwResult C-Win
ON START-SEARCH OF brwResult IN FRAME fMain
do:
  define variable hsort as handle no-undo.

  hsort = browse {&browse-name}:current-column.

  /* Standard browse sorting does not work if the column label is blank.
     Therefore, set the label of first column,
     sort the browse, then reset the label. */

  if hsort:label = "" 
   then
    hsort:label = "Comstat".
  
  {lib/brw-startSearch.i}
  
  if hsort:label = "Comstat" 
   then
    hsort:label = "".
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRole C-Win
ON VALUE-CHANGED OF cbRole IN FRAME fMain /* Role */
do:
  run filterData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dtEndDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dtEndDate C-Win
ON LEAVE OF dtEndDate IN FRAME fMain /* End Date */
do:
  if self:modified
   then
    resultsChanged(false).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dtStartDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dtStartDate C-Win
ON LEAVE OF dtStartDate IN FRAME fMain /* Start Date */
do:
  if self:modified 
   then
    resultsChanged(false).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

{lib/win-main.i}
{lib/win-status.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .
    
/* The CLOSE event can be used from inside or outside the procedure to  */     
/* terminate it.                                                        */
on close of this-procedure
  run disable_UI.
  

/* Best default for GUI applications is...                              */
pause 0 before-hide.
subscribe to "closeWindow" anywhere.

bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bget:load-image("images/Completed.bmp").
bget:load-image-insensitive("images/Completed-i.bmp").
bOpen:load-image("images/open.bmp").
bOpen:load-image-insensitive("images/open-i.bmp").

run windowResized in this-procedure.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  
  /* Set the widget labels according to the entity. */
  case pentity:  
    when {&PersonCode} 
     then
      assign
          flEntityID:label     = "Person ID"
          flEntityName:label   = "Person Name"
          .
    when {&OrganizationCode} 
     then
      assign
          flEntityID:label   = "Organization ID"
          flEntityName:label = "Organization Name"
          .    
  end case.

  /* The role column is visible only on case of Person entity.
     Otherwise hide the column. */
  if (pentity = {&PersonCode} or pentity = {&OrganizationCode}) and pRole ne {&ALL}
   then 
    do:
      std-ha = brwResult:get-browse-column(2).
      if valid-handle(std-ha) 
       then
        std-ha:visible = false.
    end.
  std-ha = ?.

  run enable_UI.
  
  /* Set the static items of the screen. */
  assign
      flEntityName:screen-value = pName
      flEntityID:screen-value   = pEntityID
      dtStartDate:screen-value  = string(today)
      dtEndDate:screen-value    = string(today)
      .
  
  setStatusMessage("").
  run setButton in this-procedure.
  if not this-procedure:persistent
   then
    wait-for close of this-procedure.

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayComplianceLog C-Win 
PROCEDURE displayComplianceLog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  open query brwResult for each ttcomLog by ttcomLog.logdate descending.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbRole dtStartDate dtEndDate flEntityName flEntityID 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bExport bOpen dtStartDate bGet dtEndDate brwResult flEntityName 
         flEntityID RECT-54 RECT-55 RECT-68 RECT-56 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hTable as handle no-undo.
 
  define buffer ttcomLog for ttcomLog.

  if query brwResult:num-results = 0 
   then
    do:
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.

  /* Updating the master table itself as we will get the data again from server. */
  for each ttcomlog:
    ttcomlog.comstat = getcomstatus(ttcomlog.comstat).
  end.
 
  publish "GetReportDir" (output std-ch).

  hTable = temp-table ttcomLog:handle.
  run util/exporttable.p (table-handle hTable,
                          "ttcomLog",
                          "for each ttcomLog",
                          "comStat,objRef,refID,entityName,action,username,logdate,notes",
                          "Status,Role,Entity ID,Entity Name,Action,User ID,Update Date,Notes",
                           std-ch,
                          "comLog.csv-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwResult.
  empty temp-table ttComLog.

  for each comLog:
    if cbRole:input-value in frame {&frame-name} <> {&ALL} and comLog.objRef <> cbRole:input-value 
     then 
      next.
    create ttComLog.
    buffer-copy comLog to ttComLog.
  end.

  run displayCompliancelog in this-procedure.

  /* Display the record count after applying the filter. */
  setStatusCount(query brwResult:num-results).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hbrowse as handle no-undo.
  define variable hColumn as handle no-undo.
  empty temp-table comLog.
 
  do with frame fMain:
  end.
  
  /* Get comlog record from server */
  run server/getcompliancelogs.p (input pentity,                  /* P/O */
                                  input pEntityID,                /* Person/Organization ID   */
                                  input pStateID,                 /* State ID   */
                                  input pRole,                    /* Role   */
                                  input pRoleID,                  /* Role ID   */
                                  input dtStartDate:input-value,  /* Start Date */
                                  input dtEndDate:input-value,    /* End Date   */                                  
                                  output table comLog,
                                  output std-lo ,                 /* Success or not */           
                                  output std-ch).
 
  if not std-lo  /* Return if not success*/
    then
     do:
       message std-ch
         view-as alert-box error buttons ok.
       return.
     end.
  cRoles = "".
  for each comLog break by comlog.objRef:
    if first-of(comlog.objRef) 
     then
      cRoles = if cRoles = "" then comlog.objRef else cRoles + "," + comlog.objRef. 
  end.
  
  cbRole:list-items   = if cRoles = "" then {&ALL} else {&ALL} + "," + cRoles no-error.
  cbRole:screen-value = if pRole  = "" then {&ALL} else pRole no-error.
  
  run filterData in this-procedure.
  
  setStatusrecords(query brwResult:num-results). 
  run setButton in this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openLog C-Win 
PROCEDURE openLog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table tcomlog. 

  if not available ttcomlog
   then 
    return.
  
  create tcomlog.
  buffer-copy ttcomlog to tcomlog.   
  tcomlog.comstat = getcomstatus(tcomlog.comstat).
  
  run dialogcomlog-agent.w (input table tcomlog).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetButton C-Win 
PROCEDURE SetButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if query brwResult:num-results = 0 
   then
    do:
      cbRole:sensitive  = false.
      bExport:sensitive = false.
      bOpen  :sensitive = false.
    end.
  else
   do:
     
     cbRole:sensitive  = true.
     bexport:sensitive = true.
     bOpen  :sensitive = true.
   end.
   

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .
  
  c-Win:move-to-top().
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validateDate C-Win 
PROCEDURE validateDate :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter pError as logical no-undo.
  
  do with frame fMain:
  end.
  
  if (dtStartDate:input-value ne "" and dtEndDate:input-value   ne "") and 
     (date(dtStartDate:input-value) gt date(dtEndDate:input-value))    
   then
    do:
      message "Start Date should be greater than End Date."
         view-as alert-box info buttons ok.
      pError = yes.
    end. 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* {&frame-name} Components */
      {&browse-name}:width-pixels  = frame {&frame-name}:width-pixels - 10
      {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5
      .
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getComStat C-Win 
FUNCTION getComStat returns character
 ( input cStat as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:          
------------------------------------------------------------------------------*/
  case cStat:
    when "1" 
     then
      return {&Gcode}.

    when "2" 
     then
      return {&Ycode}.
     
    when "3" 
     then 
      return {&Rcode}.
   
  end case.  

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getcomstatus C-Win 
FUNCTION getcomstatus RETURNS CHARACTER
  ( input cStat as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cstat = {&Gcode}
   then 
    return {&Green}.
  else if cstat = {&Ycode}
   then
    return {&Yellow}.
  else
   return {&Red}.
    
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL PRIVATE
  ( pValid as logical  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

