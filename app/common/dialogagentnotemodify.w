&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name dialogagentnoteadd.w
@description Add a note 

@param FileDataSrv;handle;A handle to the agent's data procedure

@author John Oliver
@version 1.0
@created 04/06/2017
@notes 
---------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input parameter hFileDataSrv as handle no-undo.

/* Local Variable Definitions ---                                       */
{tt/agent.i}
{tt/agentnote.i}
{lib/std-def.i}

define variable cAgentID as character no-undo.
define variable iNoteSeq as integer no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwNotes

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES agentnote

/* Definitions for BROWSE brwNotes                                      */
&Scoped-define FIELDS-IN-QUERY-brwNotes agentnote.edited agentnote.categoryDesc agentnote.typeDesc agentnote.subject agentnote.noteDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwNotes   
&Scoped-define SELF-NAME brwNotes
&Scoped-define QUERY-STRING-brwNotes FOR EACH agentnote where agentnote.noteType <> "0" no-lock
&Scoped-define OPEN-QUERY-brwNotes OPEN QUERY {&SELF-NAME} FOR EACH agentnote where agentnote.noteType <> "0" no-lock.
&Scoped-define TABLES-IN-QUERY-brwNotes agentnote
&Scoped-define FIRST-TABLE-IN-QUERY-brwNotes agentnote


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwNotes}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bSpellCheck RECT-42 brwNotes cmbCategory ~
cmbType tSubject tNote bSave bCancel 
&Scoped-Define DISPLAYED-OBJECTS cmbCategory cmbType tSubject tNote 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSpellCheck  NO-FOCUS
     LABEL "Spell Check" 
     SIZE 7.2 BY 1.71 TOOLTIP "Check spelling".

DEFINE VARIABLE cmbCategory AS CHARACTER FORMAT "X(256)":U
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 23 BY 1 TOOLTIP "Select the type of notes to view" NO-UNDO.

DEFINE VARIABLE cmbType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tNote AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL LARGE
     SIZE 170 BY 14.95
     BGCOLOR 15 FONT 5 NO-UNDO.

DEFINE VARIABLE tSubject AS CHARACTER FORMAT "X(256)":U 
     LABEL "Summary" 
     VIEW-AS FILL-IN 
     SIZE 152 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-42
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 170 BY .1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwNotes FOR 
      agentnote SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwNotes C-Win _FREEFORM
  QUERY brwNotes NO-LOCK DISPLAY
      agentnote.edited column-label "Edited" view-as toggle-box
      agentnote.categoryDesc column-label "Category" format "x(100)" width 20
      agentnote.typeDesc column-label "Type" format "x(100)" width 20
      agentnote.subject column-label "Subject" format "x(200)" width 93
      agentnote.noteDate column-label "Date" format "99/99/9999 HH:MM:SS" width 30
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 170 BY 6.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bSpellCheck AT ROW 9.33 COL 165 WIDGET-ID 308 NO-TAB-STOP 
     brwNotes AT ROW 1.29 COL 2 WIDGET-ID 400
     cmbCategory AT ROW 9.1 COL 10 COLON-ALIGNED WIDGET-ID 240 NO-TAB-STOP 
     cmbType AT ROW 9.1 COL 42 COLON-ALIGNED WIDGET-ID 314
     tSubject AT ROW 10.29 COL 10 COLON-ALIGNED WIDGET-ID 302
     tNote AT ROW 11.48 COL 2 NO-LABEL WIDGET-ID 6
     bSave AT ROW 26.95 COL 74 WIDGET-ID 318
     bCancel AT ROW 26.95 COL 90 WIDGET-ID 316
     RECT-42 AT ROW 8.62 COL 2 WIDGET-ID 320
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 172 BY 27.62 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Modify Notes"
         HEIGHT             = 27.62
         WIDTH              = 172
         MAX-HEIGHT         = 31.76
         MAX-WIDTH          = 185.6
         VIRTUAL-HEIGHT     = 31.76
         VIRTUAL-WIDTH      = 185.6
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwNotes RECT-42 DEFAULT-FRAME */
ASSIGN 
       brwNotes:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

ASSIGN 
       tNote:RETURN-INSERTED IN FRAME DEFAULT-FRAME  = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwNotes
/* Query rebuild information for BROWSE brwNotes
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH agentnote where agentnote.noteType <> "0" no-lock
     _END_FREEFORM
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _Query            is OPENED
*/  /* BROWSE brwNotes */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Modify Notes */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Modify Notes */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME DEFAULT-FRAME /* Cancel */
DO:
  APPLY "WINDOW-CLOSE":U TO {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwNotes
&Scoped-define SELF-NAME brwNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotes C-Win
ON ROW-DISPLAY OF brwNotes IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotes C-Win
ON START-SEARCH OF brwNotes IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-startSearch.i}
  apply 'value-changed' to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotes C-Win
ON VALUE-CHANGED OF brwNotes IN FRAME DEFAULT-FRAME
DO:
  if not available agentnote
   then return.
   
  assign
    tNote:screen-value = agentnote.notes
    tSubject:screen-value = agentnote.subject
    cmbCategory:screen-value = agentnote.category
    cmbType:screen-value = agentnote.noteType
    iNoteSeq = agentnote.seq
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME DEFAULT-FRAME /* Save */
DO:
  run ModifyAgentNote in hFileDataSrv (table agentnote).
  APPLY "WINDOW-CLOSE":U TO {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSpellCheck
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSpellCheck C-Win
ON CHOOSE OF bSpellCheck IN FRAME DEFAULT-FRAME /* Spell Check */
DO:
  ASSIGN tNote.
  run util/spellcheck.p ({&window-name}:handle, input-output tNote, output std-lo).
  display tNote with frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbCategory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbCategory C-Win
ON VALUE-CHANGED OF cmbCategory IN FRAME DEFAULT-FRAME /* Category */
DO:
  run EditNote in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbType C-Win
ON VALUE-CHANGED OF cmbType IN FRAME DEFAULT-FRAME /* Status */
DO:
  run EditNote in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNote C-Win
ON VALUE-CHANGED OF tNote IN FRAME DEFAULT-FRAME
DO:
  run EditNote in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSubject
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSubject C-Win
ON VALUE-CHANGED OF tSubject IN FRAME DEFAULT-FRAME /* Summary */
DO:
  run EditNote in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bSpellCheck:load-image-up("images/spellcheck.bmp").
bSpellCheck:load-image-insensitive("images/spellcheck-i.bmp").

{lib/brw-main.i}

/* note category */
{lib/get-sysprop-list.i &combo=cmbCategory &appCode="'AMD'" &objAction="'AgentNote'" &objProperty="'Category'"}

/* note type */
{lib/get-sysprop-list.i &combo=cmbType &appCode="'AMD'" &objAction="'AgentNote'" &objProperty="'Type'"}

if valid-handle(hFileDataSrv)
 then
  do:
    run GetAgent in hFileDataSrv (output table agent).
    run GetAgentNotes in hFileDataSrv (output table agentnote).
  end.
 
for first agent no-lock:
  cAgentID = agent.agentID.
  {&window-name}:title = "Modify Notes for " + agent.name.
end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  apply "VALUE-CHANGED" to {&browse-name}.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE EditNote C-Win 
PROCEDURE EditNote :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    for first agentnote exclusive-lock
        where agentnote.agentID = cAgentID
          and agentnote.seq = iNoteSeq:
      
      assign
        agentnote.category = cmbCategory:screen-value
        agentnote.subject = tSubject:screen-value
        agentnote.notes = tNote:screen-value
        agentnote.noteType = cmbType:screen-value
        agentnote.edited = true
        .
      publish "GetNoteCategoryDesc" (agentnote.category, output agentnote.categoryDesc).
      publish "GetNoteTypeDesc" (agentnote.noteType, output agentnote.typeDesc).
    end.
    browse {&browse-name}:refresh().
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cmbCategory cmbType tSubject tNote 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bSpellCheck RECT-42 brwNotes cmbCategory cmbType tSubject tNote bSave 
         bCancel 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SortData C-Win 
PROCEDURE SortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo initial "where noteType <> '0'".
  {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

