&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEFINE VARIABLE tMonth as integer no-undo.
DEFINE VARIABLE tYear as integer no-undo.
define variable tDelim as character no-undo initial ",".
define variable tAppCode as character no-undo.
{lib/std-def.i}

/* handles to button and toggle-box */
define variable hButton as handle no-undo.
define variable hToggle as handle no-undo.

/* Functions ---                                                        */
{lib/find-widget.i}
{lib/add-delimiter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fConfig

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-34 RECT-35 tConfirmClose tConfirmDelete ~
tConfirmExit tTempDir tReportDir tConfirmFileUpload tConfirmStatusChange ~
tAutoView tNoteView bSave tExportType bCancel bTempDir bReportDir ~
tLoadAccounts tLoadAttorneys tLoadEndorsements tLoadPeriods tLoadStatCodes ~
tLoadStateForms tLoadSysProps tLoadALL bLoadAccounts bLoadAttorneys ~
bLoadEndorsements bLoadPeriods bLoadStatCodes bLoadStateForms bLoadSysProps ~
bLoadALL tLoadAgents tLoadCounties tLoadOffices tLoadRegions tLoadStates ~
tLoadSysCodes tLoadVendors bLoadAgents bLoadCounties bLoadOffices ~
bLoadRegions bLoadStates bLoadSysCodes bLoadVendors 
&Scoped-Define DISPLAYED-OBJECTS tConfirmClose tConfirmDelete tConfirmExit ~
tTempDir tReportDir tConfirmFileUpload tConfirmStatusChange tAutoView ~
tNoteView tExportType tLoadAccounts tLoadAttorneys tLoadEndorsements ~
tLoadPeriods tLoadStatCodes tLoadStateForms tLoadSysProps tLoadALL ~
tLoadAgents tLoadCounties tLoadOffices tLoadRegions tLoadStates ~
tLoadSysCodes tLoadVendors 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setLoadALL C-Win 
FUNCTION setLoadALL RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE VARIABLE tAMDClick AS CHARACTER FORMAT "X(256)":U INITIAL "S" 
     LABEL "Double Click" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Modify Agent","M",
                     "Agent Summary","S"
     DROP-DOWN-LIST
     SIZE 31 BY 1 NO-UNDO.

DEFINE VARIABLE tDefaultStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Default Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 31 BY 1 NO-UNDO.

DEFINE VARIABLE tDefaultView AS CHARACTER FORMAT "X(256)":U 
     LABEL "Default View" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 31 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentPDF AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Email", "E",
"View", "V"
     SIZE 22 BY .71 NO-UNDO.

DEFINE VARIABLE tDefaultCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Note Category" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 20 BY 1 TOOLTIP "Select the default Category when adding a new note" NO-UNDO.

DEFINE VARIABLE tDormant AS INTEGER FORMAT "zz9":U INITIAL 0 
     LABEL "Dormant" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 TOOLTIP "Enter the default for the dormant file report" NO-UNDO.

DEFINE VARIABLE tAutoViewRecent AS LOGICAL INITIAL no 
     LABEL "Auto-view last file viewed on startup" 
     VIEW-AS TOGGLE-BOX
     SIZE 39 BY .81 TOOLTIP "Automatically open the last claim that was viewed" NO-UNDO.

DEFINE VARIABLE tAutoViewSearch AS LOGICAL INITIAL no 
     LABEL "Auto-view the file when only one result" 
     VIEW-AS TOGGLE-BOX
     SIZE 41 BY .81 TOOLTIP "Automatically view the file if there is only a single result when searching" NO-UNDO.

DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 17 BY 1.19
     BGCOLOR 8 .

DEFINE BUTTON bLoadAccounts 
     LABEL "Load" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bLoadAgents 
     LABEL "Load" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bLoadALL 
     LABEL "Load" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bLoadAttorneys 
     LABEL "Load" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bLoadCounties 
     LABEL "Load" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bLoadEndorsements 
     LABEL "Load" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bLoadOffices 
     LABEL "Load" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bLoadPeriods 
     LABEL "Load" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bLoadRegions 
     LABEL "Load" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bLoadStatCodes 
     LABEL "Load" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bLoadStateForms 
     LABEL "Load" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bLoadStates 
     LABEL "Load" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bLoadSysCodes 
     LABEL "Load" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bLoadSysProps 
     LABEL "Load" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bLoadVendors 
     LABEL "Load" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bReportDir 
     LABEL "..." 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 17 BY 1.19
     BGCOLOR 8 .

DEFINE BUTTON bTempDir 
     LABEL "..." 
     SIZE 4.6 BY 1.1.

DEFINE VARIABLE tReportDir AS CHARACTER FORMAT "X(256)":U 
     LABEL "Report Directory" 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1 NO-UNDO.

DEFINE VARIABLE tTempDir AS CHARACTER FORMAT "X(200)":U 
     LABEL "Temporary Directory" 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1 TOOLTIP "Enter the default directory to save files; blank is session working directory" NO-UNDO.

DEFINE VARIABLE tExportType AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "XLSX", "X",
"CSV", "C"
     SIZE 11 BY 1.43 TOOLTIP "Select the type of Excel export to use" NO-UNDO.

DEFINE VARIABLE tNoteView AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Page", "P",
"Browse", "B"
     SIZE 12 BY 1.43 TOOLTIP "Select the style of notes window to use" NO-UNDO.

DEFINE RECTANGLE RECT-34
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 63 BY 12.62.

DEFINE RECTANGLE RECT-35
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 83 BY 12.62.

DEFINE VARIABLE tAutoView AS LOGICAL INITIAL no 
     LABEL "Auto-View Data" 
     VIEW-AS TOGGLE-BOX
     SIZE 19 BY .81 TOOLTIP "Automatically retrieve data from server when opening a new window" NO-UNDO.

DEFINE VARIABLE tConfirmClose AS LOGICAL INITIAL no 
     LABEL "Confirm Close" 
     VIEW-AS TOGGLE-BOX
     SIZE 18 BY .81 TOOLTIP "Ask before closing a window if currently being modified" NO-UNDO.

DEFINE VARIABLE tConfirmDelete AS LOGICAL INITIAL no 
     LABEL "Confirm Delete" 
     VIEW-AS TOGGLE-BOX
     SIZE 19 BY .81 TOOLTIP "Ask before deleting" NO-UNDO.

DEFINE VARIABLE tConfirmExit AS LOGICAL INITIAL no 
     LABEL "Confirm Exit" 
     VIEW-AS TOGGLE-BOX
     SIZE 20 BY .81 TOOLTIP "Ask before exiting the application" NO-UNDO.

DEFINE VARIABLE tConfirmFileUpload AS LOGICAL INITIAL no 
     LABEL "Confirm File Upload" 
     VIEW-AS TOGGLE-BOX
     SIZE 22 BY .81 TOOLTIP "Ask before deleting" NO-UNDO.

DEFINE VARIABLE tConfirmStatusChange AS LOGICAL INITIAL no 
     LABEL "Confirm Status Change" 
     VIEW-AS TOGGLE-BOX
     SIZE 25 BY .81 NO-UNDO.

DEFINE VARIABLE tLoadAccounts AS LOGICAL INITIAL no 
     LABEL "Accounts" 
     VIEW-AS TOGGLE-BOX
     SIZE 21 BY .81 NO-UNDO.

DEFINE VARIABLE tLoadAgents AS LOGICAL INITIAL no 
     LABEL "Agents" 
     VIEW-AS TOGGLE-BOX
     SIZE 16 BY .81 NO-UNDO.

DEFINE VARIABLE tLoadALL AS LOGICAL INITIAL no 
     LABEL "ALL" 
     VIEW-AS TOGGLE-BOX
     SIZE 13 BY .81 NO-UNDO.

DEFINE VARIABLE tLoadAttorneys AS LOGICAL INITIAL no 
     LABEL "Attorneys" 
     VIEW-AS TOGGLE-BOX
     SIZE 17 BY .81 NO-UNDO.

DEFINE VARIABLE tLoadCounties AS LOGICAL INITIAL no 
     LABEL "Counties" 
     VIEW-AS TOGGLE-BOX
     SIZE 17 BY .81 NO-UNDO.

DEFINE VARIABLE tLoadEndorsements AS LOGICAL INITIAL no 
     LABEL "Endorsements" 
     VIEW-AS TOGGLE-BOX
     SIZE 18 BY .81 NO-UNDO.

DEFINE VARIABLE tLoadOffices AS LOGICAL INITIAL no 
     LABEL "Offices" 
     VIEW-AS TOGGLE-BOX
     SIZE 17 BY .81 NO-UNDO.

DEFINE VARIABLE tLoadPeriods AS LOGICAL INITIAL no 
     LABEL "Periods" 
     VIEW-AS TOGGLE-BOX
     SIZE 17 BY .81 NO-UNDO.

DEFINE VARIABLE tLoadRegions AS LOGICAL INITIAL no 
     LABEL "Regions" 
     VIEW-AS TOGGLE-BOX
     SIZE 17 BY .81 NO-UNDO.

DEFINE VARIABLE tLoadStatCodes AS LOGICAL INITIAL no 
     LABEL "STAT Codes" 
     VIEW-AS TOGGLE-BOX
     SIZE 17 BY .81 NO-UNDO.

DEFINE VARIABLE tLoadStateForms AS LOGICAL INITIAL no 
     LABEL "State Forms" 
     VIEW-AS TOGGLE-BOX
     SIZE 18 BY .81 NO-UNDO.

DEFINE VARIABLE tLoadStates AS LOGICAL INITIAL no 
     LABEL "States" 
     VIEW-AS TOGGLE-BOX
     SIZE 17 BY .81 NO-UNDO.

DEFINE VARIABLE tLoadSysCodes AS LOGICAL INITIAL no 
     LABEL "System Codes" 
     VIEW-AS TOGGLE-BOX
     SIZE 21 BY .81 NO-UNDO.

DEFINE VARIABLE tLoadSysProps AS LOGICAL INITIAL no 
     LABEL "System Properties" 
     VIEW-AS TOGGLE-BOX
     SIZE 21 BY .81 NO-UNDO.

DEFINE VARIABLE tLoadVendors AS LOGICAL INITIAL no 
     LABEL "Vendors" 
     VIEW-AS TOGGLE-BOX
     SIZE 21 BY .81 NO-UNDO.

DEFINE VARIABLE tOPSClick AS CHARACTER FORMAT "X(256)":U INITIAL "S" 
     LABEL "Double Click" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Batch Details","S",
                     "Modify Batch","M"
     DROP-DOWN-LIST
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE tLastPeriod AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 11.2 BY .62 NO-UNDO.

DEFINE VARIABLE tStartPeriod AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Open in Current Period", "C",
"Open in Last Period Viewed", "U"
     SIZE 30.4 BY 1.81 NO-UNDO.

DEFINE VARIABLE tConfirmInvoice AS LOGICAL INITIAL no 
     LABEL "Confim Invoice" 
     VIEW-AS TOGGLE-BOX
     SIZE 19 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fConfig
     tConfirmClose AT ROW 5.29 COL 5 WIDGET-ID 194
     tConfirmDelete AT ROW 6.24 COL 5 WIDGET-ID 192
     tConfirmExit AT ROW 7.19 COL 5 WIDGET-ID 196
     tTempDir AT ROW 2.19 COL 23 COLON-ALIGNED WIDGET-ID 186
     tReportDir AT ROW 3.43 COL 23 COLON-ALIGNED WIDGET-ID 188
     tConfirmFileUpload AT ROW 5.29 COL 27 WIDGET-ID 226
     tConfirmStatusChange AT ROW 6.24 COL 27 WIDGET-ID 232
     tAutoView AT ROW 7.19 COL 27 WIDGET-ID 198
     tNoteView AT ROW 5.81 COL 55 NO-LABEL WIDGET-ID 216
     bSave AT ROW 14.33 COL 55 WIDGET-ID 222
     tExportType AT ROW 5.86 COL 71 NO-LABEL WIDGET-ID 208
     bCancel AT ROW 14.33 COL 73 WIDGET-ID 220
     bTempDir AT ROW 2.14 COL 77 WIDGET-ID 30
     bReportDir AT ROW 3.38 COL 77 WIDGET-ID 190
     tLoadAccounts AT ROW 2.57 COL 89 WIDGET-ID 206
     tLoadAttorneys AT ROW 4 COL 89 WIDGET-ID 236
     tLoadEndorsements AT ROW 5.43 COL 89 WIDGET-ID 166
     tLoadPeriods AT ROW 6.86 COL 89 WIDGET-ID 144
     tLoadStatCodes AT ROW 8.29 COL 89 WIDGET-ID 148
     tLoadStateForms AT ROW 9.71 COL 89 WIDGET-ID 162
     tLoadSysProps AT ROW 11.14 COL 89 WIDGET-ID 170
     tLoadALL AT ROW 12.57 COL 89 WIDGET-ID 180
     bLoadAccounts AT ROW 2.43 COL 111 WIDGET-ID 204
     bLoadAttorneys AT ROW 3.86 COL 111 WIDGET-ID 234
     bLoadEndorsements AT ROW 5.29 COL 111 WIDGET-ID 164
     bLoadPeriods AT ROW 6.71 COL 111 WIDGET-ID 146
     bLoadStatCodes AT ROW 8.14 COL 111 WIDGET-ID 150
     bLoadStateForms AT ROW 9.57 COL 111 WIDGET-ID 160
     bLoadSysProps AT ROW 11 COL 111 WIDGET-ID 168
     bLoadALL AT ROW 12.43 COL 111 WIDGET-ID 178
     tLoadAgents AT ROW 2.57 COL 119 WIDGET-ID 140
     tLoadCounties AT ROW 4 COL 119 WIDGET-ID 158
     tLoadOffices AT ROW 5.43 COL 119 WIDGET-ID 240
     tLoadRegions AT ROW 6.86 COL 119 WIDGET-ID 230
     tLoadStates AT ROW 8.29 COL 119 WIDGET-ID 154
     tLoadSysCodes AT ROW 9.71 COL 119 WIDGET-ID 176
     tLoadVendors AT ROW 11.14 COL 119 WIDGET-ID 202
     bLoadAgents AT ROW 2.43 COL 141 WIDGET-ID 142
     bLoadCounties AT ROW 3.86 COL 141 WIDGET-ID 156
     bLoadOffices AT ROW 5.29 COL 141 WIDGET-ID 238
     bLoadRegions AT ROW 6.71 COL 141 WIDGET-ID 228
     bLoadStates AT ROW 8.14 COL 141 WIDGET-ID 152
     bLoadSysCodes AT ROW 9.57 COL 141 WIDGET-ID 172
     bLoadVendors AT ROW 11 COL 141 WIDGET-ID 200
     "Notes View" VIEW-AS TEXT
          SIZE 13 BY .62 AT ROW 5.14 COL 55 WIDGET-ID 242
     "Options" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 184
     "Load on Startup" VIEW-AS TEXT
          SIZE 16 BY .62 AT ROW 1.24 COL 87 WIDGET-ID 4
     "Export To" VIEW-AS TEXT
          SIZE 11 BY .62 TOOLTIP "Export to Excel as Spreadsheet or CSV file" AT ROW 5.14 COL 71 WIDGET-ID 214
     RECT-34 AT ROW 1.48 COL 86 WIDGET-ID 2
     RECT-35 AT ROW 1.48 COL 2 WIDGET-ID 182
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 148.8 BY 14.95 WIDGET-ID 100.

DEFINE FRAME fAMD
     tDefaultView AT ROW 1.24 COL 17.8 WIDGET-ID 194
     tDefaultStatus AT ROW 2.43 COL 16.4 WIDGET-ID 186
     tAMDClick AT ROW 3.62 COL 17.8 WIDGET-ID 196
     tAgentPDF AT ROW 4.81 COL 31 NO-LABEL WIDGET-ID 244
     "Agent PDF:" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 4.81 COL 19.4 WIDGET-ID 248
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 4 ROW 8.38
         SIZE 79 BY 5.24 WIDGET-ID 300.

DEFINE FRAME fOPS
     tConfirmInvoice AT ROW 1.24 COL 2 WIDGET-ID 100
     tStartPeriod AT ROW 1.24 COL 28 NO-LABEL WIDGET-ID 94
     tOPSClick AT ROW 3.38 COL 14.8 WIDGET-ID 102
     tLastPeriod AT ROW 2.29 COL 56.6 COLON-ALIGNED NO-LABEL WIDGET-ID 98 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 4 ROW 8.38
         SIZE 79 BY 5.24 WIDGET-ID 400.

DEFINE FRAME fCLM
     tDefaultCategory AT ROW 1.24 COL 57 COLON-ALIGNED WIDGET-ID 178
     tAutoViewRecent AT ROW 1.48 COL 2 WIDGET-ID 134
     tAutoViewSearch AT ROW 2.43 COL 2 WIDGET-ID 164
     tDormant AT ROW 2.43 COL 57 COLON-ALIGNED WIDGET-ID 174
     "days" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 2.62 COL 65.4 WIDGET-ID 176
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 4 ROW 8.38
         SIZE 79 BY 5.24 WIDGET-ID 200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Configuration"
         HEIGHT             = 14.95
         WIDTH              = 148.8
         MAX-HEIGHT         = 24.05
         MAX-WIDTH          = 152.8
         VIRTUAL-HEIGHT     = 24.05
         VIRTUAL-WIDTH      = 152.8
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fAMD:FRAME = FRAME fConfig:HANDLE
       FRAME fCLM:FRAME = FRAME fConfig:HANDLE
       FRAME fOPS:FRAME = FRAME fConfig:HANDLE.

/* SETTINGS FOR FRAME fAMD
                                                                        */
ASSIGN 
       FRAME fAMD:HIDDEN           = TRUE.

/* SETTINGS FOR COMBO-BOX tAMDClick IN FRAME fAMD
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX tDefaultStatus IN FRAME fAMD
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX tDefaultView IN FRAME fAMD
   ALIGN-L                                                              */
/* SETTINGS FOR FRAME fCLM
                                                                        */
ASSIGN 
       FRAME fCLM:HIDDEN           = TRUE.

/* SETTINGS FOR FRAME fConfig
   FRAME-NAME L-To-R,COLUMNS                                            */
/* SETTINGS FOR FRAME fOPS
                                                                        */
ASSIGN 
       FRAME fOPS:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN tLastPeriod IN FRAME fOPS
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR COMBO-BOX tOPSClick IN FRAME fOPS
   ALIGN-L                                                              */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Configuration */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Configuration */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAMD
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAMD C-Win
ON GO OF FRAME fAMD
DO:
  define variable cView as character no-undo.
  define variable cStat as character no-undo.

  /* agency specific */
  DO WITH FRAME fAMD:
    /* default view */
    publish "GetDefaultView" (output cView).
    {lib/get-sysprop-list.i &combo=tDefaultView &appCode="'AMD'" &objAction="'Main'" &objProperty="'View'" &d=cView}
    
    /* default status */
    publish "GetDefaultStatus" (output cStat).
    {lib/get-sysprop-list.i &combo=tDefaultStatus &appCode="'AMD'" &objAction="'Agent'" &objProperty="'Status'" &addAll=true &d=cStat}
    
    /* double click */
    std-ch = "".
    publish "GetDoubleClick" (output std-ch).
    if std-ch = "" or lookup(std-ch, tAMDClick:list-item-pairs) = 0
     then std-ch = entry(2, tAMDClick:list-item-pairs).
    tAMDClick:screen-value = std-ch.

    std-ch = "".
    publish "GetAgentPdf" (output std-ch).
    tAgentPdf:screen-value = if std-ch = "" then "E" else std-ch.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fCLM
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fCLM C-Win
ON GO OF FRAME fCLM
DO:
  DO WITH FRAME fCLM:
    std-lo = false.
    publish "GetAutoViewRecent" (output std-lo).
    tAutoViewRecent:checked = std-lo.
    
    std-lo = false.
    publish "GetAutoViewSearch" (output std-lo).
    tAutoViewSearch:checked = std-lo.
    
    std-lo = false.
    publish "GetDormantDays" (output std-in).
    tDormant:screen-value = string(std-in).
    
    std-ch = "".
    publish "GetNoteCategoryUserList" (output std-ch).
    tDefaultCategory:list-item-pairs = std-ch.

    std-ch = "".
    publish "GetDefaultNoteCategory" (output std-ch).
    if std-ch = "" or lookup(std-ch, tDefaultCategory:list-item-pairs) = 0
     then std-ch = entry(2, tDefaultCategory:list-item-pairs).
    tDefaultCategory:screen-value = std-ch.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fOPS
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fOPS C-Win
ON GO OF FRAME fOPS
DO:
  DO WITH FRAME fOPS:
    std-lo = false.
    publish "GetConfirmInvoice" (output std-lo).
    tConfirmInvoice:checked = std-lo.
    
    std-ch = "".
    publish "GetStartPeriod" (output std-ch).
    if lookup(std-ch, "C,U") = 0 
     then tStartPeriod:screen-value = "C".
     else tStartPeriod:screen-value = std-ch.
     
    publish "GetLastPeriod" (output tMonth, output tYear).
    tLastPeriod:screen-value = string(tMonth) + "/" + string(tYear).
    
    /* double click */
    std-ch = "".
    publish "GetDoubleClick" (output std-ch).
    if std-ch = "" or lookup(std-ch, tOPSClick:list-item-pairs) = 0
     then std-ch = entry(2, tOPSClick:list-item-pairs).
    tOPSClick:screen-value = std-ch.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME fConfig /* Cancel */
DO:
  apply "WINDOW-CLOSE" to {&WINDOW-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLoadAccounts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLoadAccounts C-Win
ON CHOOSE OF bLoadAccounts IN FRAME fConfig /* Load */
DO:
  PUBLISH "LoadAccounts".
  MESSAGE "Accounts Updated" VIEW-AS ALERT-BOX.
  publish "AccountDataChanged".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLoadAgents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLoadAgents C-Win
ON CHOOSE OF bLoadAgents IN FRAME fConfig /* Load */
DO:
  PUBLISH "LoadAgents".
  MESSAGE "Agents Updated" VIEW-AS ALERT-BOX.
  publish "AgentDataChanged".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLoadALL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLoadALL C-Win
ON CHOOSE OF bLoadALL IN FRAME fConfig /* Load */
DO:
  std-ha = GetWidgetBeginName(FRAME {&FRAME-NAME}:HANDLE, ?, "bLoad").
  REPEAT WHILE VALID-HANDLE(std-ha):
    IF std-ha:TYPE = "BUTTON" and std-ha:name <> "bLoadALL" and std-ha:sensitive
     THEN apply "CHOOSE" to std-ha.
    std-ha = GetWidgetBeginName(FRAME {&FRAME-NAME}:HANDLE, std-ha, "bLoad").
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLoadAttorneys
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLoadAttorneys C-Win
ON CHOOSE OF bLoadAttorneys IN FRAME fConfig /* Load */
DO:
  PUBLISH "LoadCounties".
  MESSAGE "Counties Updated" VIEW-AS ALERT-BOX.
  publish "CountyDataChanged".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLoadCounties
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLoadCounties C-Win
ON CHOOSE OF bLoadCounties IN FRAME fConfig /* Load */
DO:
  PUBLISH "LoadCounties".
  MESSAGE "Counties Updated" VIEW-AS ALERT-BOX.
  publish "CountyDataChanged".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLoadEndorsements
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLoadEndorsements C-Win
ON CHOOSE OF bLoadEndorsements IN FRAME fConfig /* Load */
DO:
  publish "LoadDefaultEndorsements".
  MESSAGE "Default Endorsements Updated" VIEW-AS ALERT-BOX.
  publish "DefaultEndorsementDataChanged".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLoadOffices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLoadOffices C-Win
ON CHOOSE OF bLoadOffices IN FRAME fConfig /* Load */
DO:
  PUBLISH "LoadCounties".
  MESSAGE "Counties Updated" VIEW-AS ALERT-BOX.
  publish "CountyDataChanged".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLoadPeriods
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLoadPeriods C-Win
ON CHOOSE OF bLoadPeriods IN FRAME fConfig /* Load */
DO:
  publish "LoadPeriods".
  MESSAGE "Periods Updated" VIEW-AS ALERT-BOX.
  publish "PeriodDataChanged".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLoadRegions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLoadRegions C-Win
ON CHOOSE OF bLoadRegions IN FRAME fConfig /* Load */
DO:
  publish "LoadRegions".
  MESSAGE "Regions Updated" VIEW-AS ALERT-BOX.
  publish "RegionDataChanged".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLoadStatCodes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLoadStatCodes C-Win
ON CHOOSE OF bLoadStatCodes IN FRAME fConfig /* Load */
DO:
  publish "LoadStatCodes".
  MESSAGE "STAT Codes Updated" VIEW-AS ALERT-BOX.
  publish "StatCodeDataChanged".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLoadStateForms
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLoadStateForms C-Win
ON CHOOSE OF bLoadStateForms IN FRAME fConfig /* Load */
DO:
  publish "LoadStateForms".
  MESSAGE "State Forms Updated" VIEW-AS ALERT-BOX.
  publish "StateFormDataChanged".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLoadStates
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLoadStates C-Win
ON CHOOSE OF bLoadStates IN FRAME fConfig /* Load */
DO:
  publish "LoadStates".
  MESSAGE "States Updated" VIEW-AS ALERT-BOX.
  publish "StateDataChanged".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLoadSysCodes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLoadSysCodes C-Win
ON CHOOSE OF bLoadSysCodes IN FRAME fConfig /* Load */
DO:
  publish "LoadSysCodes".
  MESSAGE "System Codes Updated" VIEW-AS ALERT-BOX.
  publish "SysCodeDataChanged".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLoadSysProps
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLoadSysProps C-Win
ON CHOOSE OF bLoadSysProps IN FRAME fConfig /* Load */
DO:
  publish "LoadSysProps".
  MESSAGE "System Properties Updated" VIEW-AS ALERT-BOX.
  publish "SysPropDataChanged".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLoadVendors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLoadVendors C-Win
ON CHOOSE OF bLoadVendors IN FRAME fConfig /* Load */
DO:
  publish "LoadVendors".
  MESSAGE "Vendors Updated" VIEW-AS ALERT-BOX.
  publish "VendorDataChanged".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReportDir
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReportDir C-Win
ON CHOOSE OF bReportDir IN FRAME fConfig /* ... */
DO:
  std-ch = tReportDir:screen-value.
  if tReportDir:screen-value > ""
    then system-dialog get-dir std-ch
           initial-dir std-ch.
    else system-dialog get-dir std-ch.
  if std-ch > "" and std-ch <> ?
   then tReportDir:screen-value = std-ch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME fConfig /* Save */
DO:
  do with frame {&frame-name}:
    /* set common values */
    publish "SetTempDir" (tTempDir:input-value).
    publish "SetReportDir" (tReportDir:input-value).
    publish "SetConfirmClose" (tConfirmClose:checked).
    publish "SetConfirmDelete" (tConfirmDelete:checked).
    publish "SetConfirmFileUpload" (tConfirmFileUpload:checked).
    publish "SetConfirmExit" (tConfirmExit:checked).
    publish "SetConfirmStatusChange" (tConfirmStatusChange:checked).
    publish "SetAutoView" (tAutoView:checked).
    publish "SetNoteWindow" (tNoteView:input-value).
    publish "SetExportType" (tExportType:input-value).
    
    publish "SetAgentPdf" (tAgentPdf:input-value in frame fAMD).
    
    /* set the double click */
    std-ch = "".
    case tAppCode:
     when "AMD" then std-ch = tAMDClick:screen-value in frame fAMD.
     when "OPS" then std-ch = tOPSClick:screen-value in frame fOPS.
    end case.
    publish "SetDoubleClick" (std-ch).

    /* set what to load */
    publish "SetLoadAccounts" (tLoadAccounts:checked).
    publish "SetLoadAgents" (tLoadAgents:checked).
    publish "SetLoadAttorneys" (tLoadAttorneys:checked).
    publish "SetLoadCounties" (tLoadCounties:checked).
    publish "SetLoadDefaultEndorsements" (tLoadEndorsements:checked).
    publish "SetLoadOffices" (tLoadOffices:checked).
    publish "SetLoadPeriods" (tLoadPeriods:checked).
    publish "SetLoadRegions" (tLoadRegions:checked).
    publish "SetLoadStatCodes" (tLoadStatCodes:checked).
    publish "SetLoadStates" (tLoadStates:checked).
    publish "SetLoadStateForms" (tLoadStateForms:checked).
    publish "SetLoadSysCodes" (tLoadSysCodes:checked).
    publish "SetLoadSysProps" (tLoadSysProps:checked).
    publish "SetLoadVendors" (tLoadVendors:checked).
  end.
  /* Agency specific */
  do with frame fAMD:
    publish "SetDefaultStatus" (tDefaultStatus:input-value).
    publish "SetDefaultView" (tDefaultView:input-value).
  end.
  /* claim specific */
  DO WITH FRAME fCLM:
    publish "SetAutoViewRecent" (tAutoViewRecent:checked).
    publish "SetAutoViewSearch" (tAutoViewSearch:checked).
    publish "SetDormantDays" (tDormant:input-value).
    publish "SetDefaultNoteCategory" (tDefaultCategory:screen-value).
  END.
  /* policy processing specific */
  DO WITH FRAME fOPS:
    publish "SetStartPeriod" (tStartPeriod:input-value).
    publish "SetConfirmInvoice" (tConfirmInvoice:checked).
  END.
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bTempDir
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bTempDir C-Win
ON CHOOSE OF bTempDir IN FRAME fConfig /* ... */
DO:
  std-ch = tTempDir:screen-value.
  if tTempDir:screen-value > ""
    then system-dialog get-dir std-ch
           initial-dir std-ch.
    else system-dialog get-dir std-ch.
  if std-ch > "" and std-ch <> ?
   then tTempDir:screen-value = std-ch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLoadAccounts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLoadAccounts C-Win
ON VALUE-CHANGED OF tLoadAccounts IN FRAME fConfig /* Accounts */
DO:
  setLoadALL().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLoadAgents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLoadAgents C-Win
ON VALUE-CHANGED OF tLoadAgents IN FRAME fConfig /* Agents */
DO:
  setLoadALL().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLoadALL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLoadALL C-Win
ON VALUE-CHANGED OF tLoadALL IN FRAME fConfig /* ALL */
DO:
  std-ha = GetWidgetBeginName(FRAME {&FRAME-NAME}:HANDLE, ?, "tLoad").
  REPEAT WHILE VALID-HANDLE(std-ha):
    IF std-ha:TYPE = "TOGGLE-BOX" and std-ha:name <> "tLoadALL" and std-ha:sensitive
     THEN std-ha:CHECKED = SELF:CHECKED.
    std-ha = GetWidgetBeginName(FRAME {&FRAME-NAME}:HANDLE, std-ha, "tLoad").
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLoadAttorneys
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLoadAttorneys C-Win
ON VALUE-CHANGED OF tLoadAttorneys IN FRAME fConfig /* Attorneys */
DO:
  setLoadALL().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLoadCounties
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLoadCounties C-Win
ON VALUE-CHANGED OF tLoadCounties IN FRAME fConfig /* Counties */
DO:
  setLoadALL().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLoadEndorsements
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLoadEndorsements C-Win
ON VALUE-CHANGED OF tLoadEndorsements IN FRAME fConfig /* Endorsements */
DO:
  setLoadALL().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLoadOffices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLoadOffices C-Win
ON VALUE-CHANGED OF tLoadOffices IN FRAME fConfig /* Offices */
DO:
  setLoadALL().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLoadPeriods
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLoadPeriods C-Win
ON VALUE-CHANGED OF tLoadPeriods IN FRAME fConfig /* Periods */
DO:
  setLoadALL().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLoadRegions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLoadRegions C-Win
ON VALUE-CHANGED OF tLoadRegions IN FRAME fConfig /* Regions */
DO:
  setLoadALL().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLoadStatCodes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLoadStatCodes C-Win
ON VALUE-CHANGED OF tLoadStatCodes IN FRAME fConfig /* STAT Codes */
DO:
  setLoadALL().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLoadStateForms
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLoadStateForms C-Win
ON VALUE-CHANGED OF tLoadStateForms IN FRAME fConfig /* State Forms */
DO:
  setLoadALL().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLoadStates
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLoadStates C-Win
ON VALUE-CHANGED OF tLoadStates IN FRAME fConfig /* States */
DO:
  setLoadALL().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLoadSysCodes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLoadSysCodes C-Win
ON VALUE-CHANGED OF tLoadSysCodes IN FRAME fConfig /* System Codes */
DO:
  setLoadALL().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLoadSysProps
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLoadSysProps C-Win
ON VALUE-CHANGED OF tLoadSysProps IN FRAME fConfig /* System Properties */
DO:
  setLoadALL().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLoadVendors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLoadVendors C-Win
ON VALUE-CHANGED OF tLoadVendors IN FRAME fConfig /* Vendors */
DO:
  setLoadALL().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* set the refresh buttons */
std-ha = GetWidgetBeginName(FRAME {&FRAME-NAME}:HANDLE, ?, "bLoad").
REPEAT WHILE VALID-HANDLE(std-ha):
  IF std-ha:TYPE = "BUTTON"
   THEN
    DO:
      std-ha:LOAD-IMAGE("images/s-sync.bmp").
      std-ha:LOAD-IMAGE-INSENSITIVE("images/s-sync-i.bmp").
    END.
  std-ha = GetWidgetBeginName(FRAME {&FRAME-NAME}:HANDLE, std-ha, "bLoad").
END.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  /* general config */
  DO WITH FRAME {&FRAME-NAME}:
    std-ch = "".
    publish "GetTempDir" (output std-ch).
    tTempDir:screen-value = std-ch.
    
    std-ch = "".
    publish "GetReportDir" (output std-ch).
    tReportDir:screen-value = std-ch.
    
    std-lo = false.
    publish "GetConfirmClose" (output std-lo).
    tConfirmClose:checked = std-lo.
    
    std-lo = false.
    publish "GetConfirmStatusChange" (output std-lo).
    tConfirmStatusChange:checked = std-lo.
    
    std-lo = false.
    publish "GetConfirmFileUpload" (output std-lo).
    tConfirmFileUpload:checked = std-lo.
    
    std-lo = false.
    publish "GetConfirmDelete" (output std-lo).
    tConfirmDelete:checked = std-lo.
    
    std-lo = false.
    publish "GetConfirmExit" (output std-lo).
    tConfirmExit:checked = std-lo.
    
    std-lo = false.
    publish "GetAutoView" (output std-lo).
    tAutoView:checked = std-lo.
    
    std-ch = "".
    publish "GetNoteWindow" (output std-ch).
    tNoteView:screen-value = std-ch.
    
    std-ch = "".
    publish "GetExportType" (output std-ch).
    tExportType:screen-value = std-ch.
    /* loads */
    std-lo = false.
    publish "GetLoadAccounts" (output std-lo).
    tLoadAccounts:checked = std-lo.

    std-lo = false.
    publish "GetLoadAgents" (output std-lo).
    tLoadAgents:checked = std-lo.

    std-lo = false.
    publish "GetLoadAttorneys" (output std-lo).
    tLoadAttorneys:checked = std-lo.

    std-lo = false.
    publish "GetLoadCounties" (output std-lo).
    tLoadCounties:checked = std-lo.

    std-lo = false.
    publish "GetLoadDefaultEndorsements" (output std-lo).
    tLoadEndorsements:checked = std-lo.

    std-lo = false.
    publish "GetLoadOffices" (output std-lo).
    tLoadOffices:checked = std-lo.

    std-lo = false.
    publish "GetLoadPeriods" (output std-lo).
    tLoadPeriods:checked = std-lo.

    std-lo = false.
    publish "GetLoadRegions" (output std-lo).
    tLoadRegions:checked = std-lo.

    std-lo = false.
    publish "GetLoadStatCodes" (output std-lo).
    tLoadStatCodes:checked = std-lo.

    std-lo = false.
    publish "GetLoadStates" (output std-lo).
    tLoadStates:checked = std-lo.

    std-lo = false.
    publish "GetLoadStateForms" (output std-lo).
    tLoadStateForms:checked = std-lo.

    std-lo = false.
    publish "GetLoadSysCodes" (output std-lo).
    tLoadSysCodes:checked = std-lo.

    std-lo = false.
    publish "GetLoadSysProps" (output std-lo).
    tLoadSysProps:checked = std-lo.

    std-lo = false.
    publish "GetLoadVendors" (output std-lo).
    tLoadVendors:checked = std-lo.
    
    
    
  END.
  /* hide the frames */
  std-ha = GetWidgetBeginName(FRAME {&FRAME-NAME}:HANDLE, ?, "f").
  REPEAT WHILE VALID-HANDLE(std-ha):
    IF std-ha:TYPE = "FRAME"
     then std-ha:hidden = true.
    std-ha = GetWidgetBeginName(FRAME {&FRAME-NAME}:HANDLE, std-ha, "f").
  END.
  /* show the correct frame */
  publish "GetAppCode" (output tAppCode).
  std-ha = GetWidgetByName(frame {&frame-name}:handle, "f" + tAppCode).
  if valid-handle(std-ha) and std-ha:type = "FRAME"
   then
    do:
      std-ha:hidden = false.
      apply "GO" to std-ha.
    end.
  /* get the necessary loads */
  std-ch = "".
  publish "GetSysPropList" (tAppCode, "Configuration", "Loads", output std-ch).
  if index(std-ch, tDelim) = 0
   then tDelim = {&msg-dlm}.
  do std-in = 1 to num-entries(std-ch, tDelim) / 2:
    std-ha = GetWidgetByName(frame {&FRAME-NAME}:handle, "tLoad" + entry(std-in * 2, std-ch, tDelim)).
    if valid-handle(std-ha)
     then
      assign
        std-lo = (entry(std-in * 2 - 1, std-ch, tDelim) = "TRUE")
        std-ha:checked = std-lo
        std-ha:sensitive = not std-lo
        .
  end.
  SetLoadALL().
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tConfirmClose tConfirmDelete tConfirmExit tTempDir tReportDir 
          tConfirmFileUpload tConfirmStatusChange tAutoView tNoteView 
          tExportType tLoadAccounts tLoadAttorneys tLoadEndorsements 
          tLoadPeriods tLoadStatCodes tLoadStateForms tLoadSysProps tLoadALL 
          tLoadAgents tLoadCounties tLoadOffices tLoadRegions tLoadStates 
          tLoadSysCodes tLoadVendors 
      WITH FRAME fConfig IN WINDOW C-Win.
  ENABLE RECT-34 RECT-35 tConfirmClose tConfirmDelete tConfirmExit tTempDir 
         tReportDir tConfirmFileUpload tConfirmStatusChange tAutoView tNoteView 
         bSave tExportType bCancel bTempDir bReportDir tLoadAccounts 
         tLoadAttorneys tLoadEndorsements tLoadPeriods tLoadStatCodes 
         tLoadStateForms tLoadSysProps tLoadALL bLoadAccounts bLoadAttorneys 
         bLoadEndorsements bLoadPeriods bLoadStatCodes bLoadStateForms 
         bLoadSysProps bLoadALL tLoadAgents tLoadCounties tLoadOffices 
         tLoadRegions tLoadStates tLoadSysCodes tLoadVendors bLoadAgents 
         bLoadCounties bLoadOffices bLoadRegions bLoadStates bLoadSysCodes 
         bLoadVendors 
      WITH FRAME fConfig IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fConfig}
  DISPLAY tDefaultView tDefaultStatus tAMDClick tAgentPDF 
      WITH FRAME fAMD IN WINDOW C-Win.
  ENABLE tDefaultView tDefaultStatus tAMDClick tAgentPDF 
      WITH FRAME fAMD IN WINDOW C-Win.
  VIEW FRAME fAMD IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fAMD}
  DISPLAY tDefaultCategory tAutoViewRecent tAutoViewSearch tDormant 
      WITH FRAME fCLM IN WINDOW C-Win.
  ENABLE tDefaultCategory tAutoViewRecent tAutoViewSearch tDormant 
      WITH FRAME fCLM IN WINDOW C-Win.
  VIEW FRAME fCLM IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fCLM}
  DISPLAY tConfirmInvoice tStartPeriod tOPSClick 
      WITH FRAME fOPS IN WINDOW C-Win.
  ENABLE tConfirmInvoice tStartPeriod tOPSClick 
      WITH FRAME fOPS IN WINDOW C-Win.
  VIEW FRAME fOPS IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fOPS}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setLoadALL C-Win 
FUNCTION setLoadALL RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
    DEFINE VARIABLE hToggle AS HANDLE NO-UNDO.
    DEFINE VARIABLE lChecked AS LOGICAL NO-UNDO INITIAL TRUE.
    hToggle = GetWidgetBeginName(FRAME {&FRAME-NAME}:HANDLE, ?, "tLoad").
    REPEAT WHILE VALID-HANDLE(hToggle):
      IF hToggle:TYPE = "TOGGLE-BOX" AND hToggle:NAME <> "tLoadALL" AND lChecked
       THEN lChecked = hToggle:CHECKED.
      hToggle = GetWidgetBeginName(FRAME {&FRAME-NAME}:HANDLE, hToggle, "tLoad").
    END.
    tLoadALL:SCREEN-VALUE IN FRAME {&FRAME-NAME} = STRING(lChecked).
    RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

