&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File:dialogattorney.w 

  Description:To create a new Attorney record or modify an existing one. 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul sharma

  Created: 
  Modification:
  Date            Name               Description
  11/14/2018      Gurvindar          Modified to fetch state from config
  04/11/2019      Rahul              Add mandatory flag '*' in person and state fillIn
  07/26/2019      Gurvindar          Fixed combo-box issues 
  04/02/2020      Shubham            Modified code according to new organization structure
  03/17/2022      Shefali            Task# 86699 set "none" the default operational state
  05/20/2022      Shefali            Task# 93864 Get a attroney record from server when input temp-table is empty. 
  04/04/2024      S Chandu           Task #111689 Commented the reference of person.phone,person.mail and person.mobile.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
/* Temp table Definitions ---                                           */
{tt/person.i}
{tt/person.i   &tableAlias=tperson}
{tt/attorney.i} 
{tt/state.i}

/* Parameters Definitions ---                                           */
define input  parameter table         for attorney.
define input  parameter ipcAction     as character no-undo.
define input  parameter ipcAttorneyID as character no-undo.
define input  parameter ipcPersonId   as character no-undo.
define output parameter opcAttorneyID as character no-undo.
define output parameter oplSave       as logical   no-undo.

/* Library Files  */
{lib/com-def.i}
{lib/std-def.i}
{lib/winlaunch.i}
{lib/set-button-def.i}

/* local variable definition */
define variable cValue        as character no-undo.
define variable cAction       as character no-undo.
define variable cOldPersonID  as character no-undo.

/* Variables defined to be used in IP enableDisableSave. */
define variable cTrackState    as character no-undo.
define variable cTrackPerson   as character no-undo.
define variable cTrackName     as character no-undo.  
define variable cTrackName2    as character no-undo.
define variable cTrackStatus   as character no-undo.
define variable cTrackAddr     as character no-undo.
define variable cTrackAddr2    as character no-undo.
define variable cTrackCity     as character no-undo.
define variable cTrackSt       as character no-undo.
define variable cTrackZip      as character no-undo.
define variable cTrackMobile   as character no-undo.
define variable cTrackPhone    as character no-undo.
define variable cTrackEmail    as character no-undo.

define variable cTrackWebsite  as character no-undo.
define variable cTrackID       as character no-undo.
define variable cTrackContact  as character no-undo.
define variable cTrackSigned   as character no-undo.
define variable cTrackSubmit   as character no-undo.
define variable cTrackNote     as character no-undo.

/* Preprocessors define to identify which action to perform */
&scoped-define EditFromPerson "EFP" 
&scoped-define NewFromPerson  "NFP" 
&scoped-define NewFromMain    "NFM"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bPersonLookup fPerson cbState fName fName2 ~
cbStatus fWebsite fAddr fAddr2 fCity cbSt fZip fMobile fPhone fEmail ~
fContractID fContact fContractDate fSubmissionDate flCreateDate ~
flProspectDate flWithdrawnDate flActiveDate flClosedDate flCancelDate ~
edNotes BtnUrl BtnCancel RECT-5 RECT-6 RECT-7 
&Scoped-Define DISPLAYED-OBJECTS fPerson cbState fName fName2 cbStatus ~
fWebsite fAddr fAddr2 fCity cbSt fZip fMobile fPhone fEmail fContractID ~
fContact fContractDate fSubmissionDate flCreateDate flProspectDate ~
flWithdrawnDate flActiveDate flClosedDate flCancelDate edNotes ~
fMarkMandatory1 fMarkMandatory2 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doModify Dialog-Frame 
FUNCTION doModify RETURNS LOGICAL
  ( input pModify as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getPersonName Dialog-Frame 
FUNCTION getPersonName RETURNS CHARACTER
  ( ipcPersonID as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validPerson Dialog-Frame 
FUNCTION validPerson RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bPersonLookup  NO-FOCUS
     LABEL "Lookup" 
     SIZE 4.6 BY 1.14 TOOLTIP "Person lookup".

DEFINE BUTTON BtnCancel AUTO-END-KEY DEFAULT 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON BtnOK AUTO-GO DEFAULT 
     LABEL "Create" 
     SIZE 15 BY 1.14 TOOLTIP "Create".

DEFINE BUTTON BtnUrl  NO-FOCUS
     LABEL "URL" 
     SIZE 4.6 BY 1.14 TOOLTIP "Open URL".

DEFINE VARIABLE cbSt AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(50)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "itme1","item1"
     DROP-DOWN-LIST
     SIZE 24.8 BY 1 NO-UNDO.

DEFINE VARIABLE cbStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 24.8 BY 1 NO-UNDO.

DEFINE VARIABLE edNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 135 BY 2.81 NO-UNDO.

DEFINE VARIABLE fAddr AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 47.6 BY 1 NO-UNDO.

DEFINE VARIABLE fAddr2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 47.6 BY 1 NO-UNDO.

DEFINE VARIABLE fCity AS CHARACTER FORMAT "x(40)":U 
     VIEW-AS FILL-IN 
     SIZE 20.8 BY 1 TOOLTIP "Enter the Person city" NO-UNDO.

DEFINE VARIABLE fContact AS CHARACTER FORMAT "X(256)":U 
     LABEL "Contact" 
     VIEW-AS FILL-IN 
     SIZE 47.6 BY 1 NO-UNDO.

DEFINE VARIABLE fContractDate AS DATE FORMAT "99/99/99":U 
     LABEL "Signed" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fContractID AS CHARACTER FORMAT "X(256)":U 
     LABEL "ID" 
     VIEW-AS FILL-IN 
     SIZE 47.6 BY 1 NO-UNDO.

DEFINE VARIABLE fEmail AS CHARACTER FORMAT "X(256)":U 
     LABEL "Email" 
     VIEW-AS FILL-IN 
     SIZE 47.6 BY 1 NO-UNDO.

DEFINE VARIABLE flActiveDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Active" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flCancelDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Cancelled" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flClosedDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Closed" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flCreateDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Created" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flProspectDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Prospect" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flWithdrawnDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Withdrawn" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fMarkMandatory1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMobile AS CHARACTER FORMAT "X(256)":U 
     LABEL "Mobile" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE fName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 47.6 BY 1 NO-UNDO.

DEFINE VARIABLE fName2 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Firm name" 
     VIEW-AS FILL-IN 
     SIZE 47.6 BY 1 NO-UNDO.

DEFINE VARIABLE fPerson AS CHARACTER FORMAT "X(256)":U 
     LABEL "Person" 
     VIEW-AS FILL-IN 
     SIZE 47.6 BY 1 NO-UNDO.

DEFINE VARIABLE fPhone AS CHARACTER FORMAT "X(256)":U 
     LABEL "Phone" 
     VIEW-AS FILL-IN 
     SIZE 18.2 BY 1 NO-UNDO.

DEFINE VARIABLE fSubmissionDate AS DATE FORMAT "99/99/99":U 
     LABEL "Submitted" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fWebsite AS CHARACTER FORMAT "X(256)":U 
     LABEL "Website" 
     VIEW-AS FILL-IN 
     SIZE 47.6 BY 1 NO-UNDO.

DEFINE VARIABLE fZip AS CHARACTER FORMAT "x(10)":U 
     VIEW-AS FILL-IN 
     SIZE 13.6 BY 1 TOOLTIP "Enter the Zipcode" NO-UNDO.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 65.2 BY 5.43.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 64 BY 5.38.

DEFINE RECTANGLE RECT-7
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 65.2 BY 7.29.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bPersonLookup AT ROW 1.38 COL 63.2 WIDGET-ID 292 NO-TAB-STOP 
     fPerson AT ROW 1.43 COL 13.2 COLON-ALIGNED WIDGET-ID 290
     cbState AT ROW 2.62 COL 13.2 COLON-ALIGNED WIDGET-ID 282
     fName AT ROW 3.81 COL 13.2 COLON-ALIGNED WIDGET-ID 274
     fName2 AT ROW 5 COL 13.2 COLON-ALIGNED WIDGET-ID 294
     cbStatus AT ROW 6.19 COL 13.2 COLON-ALIGNED WIDGET-ID 286
     fWebsite AT ROW 7.38 COL 13.2 COLON-ALIGNED WIDGET-ID 22
     fAddr AT ROW 2.62 COL 85.6 COLON-ALIGNED WIDGET-ID 54
     fAddr2 AT ROW 3.81 COL 85.6 COLON-ALIGNED NO-LABEL WIDGET-ID 264
     fCity AT ROW 5 COL 85.6 COLON-ALIGNED NO-LABEL WIDGET-ID 56
     cbSt AT ROW 5 COL 108 COLON-ALIGNED NO-LABEL WIDGET-ID 288
     fZip AT ROW 5 COL 119.6 COLON-ALIGNED NO-LABEL WIDGET-ID 60
     fMobile AT ROW 6.19 COL 85.6 COLON-ALIGNED WIDGET-ID 260
     fPhone AT ROW 6.19 COL 115 COLON-ALIGNED WIDGET-ID 262
     fEmail AT ROW 7.38 COL 85.6 COLON-ALIGNED WIDGET-ID 266
     fContractID AT ROW 10.86 COL 13.2 COLON-ALIGNED WIDGET-ID 302
     fContact AT ROW 12 COL 13.2 COLON-ALIGNED WIDGET-ID 314
     fContractDate AT ROW 13.14 COL 13.2 COLON-ALIGNED WIDGET-ID 300
     fSubmissionDate AT ROW 13.14 COL 46.8 COLON-ALIGNED WIDGET-ID 308
     flCreateDate AT ROW 10.86 COL 85.6 COLON-ALIGNED WIDGET-ID 304
     flProspectDate AT ROW 12 COL 85.6 COLON-ALIGNED WIDGET-ID 48
     flWithdrawnDate AT ROW 13.14 COL 85.6 COLON-ALIGNED WIDGET-ID 246
     flActiveDate AT ROW 10.86 COL 119.2 COLON-ALIGNED WIDGET-ID 50
     flClosedDate AT ROW 12 COL 119.2 COLON-ALIGNED WIDGET-ID 298
     flCancelDate AT ROW 13.14 COL 119.2 COLON-ALIGNED WIDGET-ID 296
     edNotes AT ROW 16.48 COL 4 NO-LABEL WIDGET-ID 252
     BtnUrl AT ROW 7.33 COL 63.2 WIDGET-ID 92 NO-TAB-STOP 
     BtnOK AT ROW 20.67 COL 55.6 WIDGET-ID 14
     BtnCancel AT ROW 20.67 COL 72.2 WIDGET-ID 16
     fMarkMandatory1 AT ROW 2.86 COL 38.4 COLON-ALIGNED NO-LABEL WIDGET-ID 52
     fMarkMandatory2 AT ROW 4.05 COL 61.4 COLON-ALIGNED NO-LABEL WIDGET-ID 322
     "Activity" VIEW-AS TEXT
          SIZE 8.8 BY .62 AT ROW 9.48 COL 75.4 WIDGET-ID 138
          FONT 6
     "Notes" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 15.76 COL 5 WIDGET-ID 254
     "Contract" VIEW-AS TEXT
          SIZE 9.8 BY .62 AT ROW 9.52 COL 5.6 WIDGET-ID 320
          FONT 6
     "Contact" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 1.24 COL 75.4 WIDGET-ID 318
          FONT 6
     RECT-5 AT ROW 9.81 COL 73.8 WIDGET-ID 136
     RECT-6 AT ROW 9.81 COL 4 WIDGET-ID 310
     RECT-7 AT ROW 1.57 COL 73.8 WIDGET-ID 316
     SPACE(2.39) SKIP(13.56)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "New Attorney"
         DEFAULT-BUTTON BtnOK CANCEL-BUTTON BtnCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON BtnOK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       edNotes:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

ASSIGN 
       flActiveDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flCancelDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flClosedDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flCreateDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flProspectDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flWithdrawnDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fMarkMandatory1 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory2 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* New Attorney */
do:
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPersonLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPersonLookup Dialog-Frame
ON CHOOSE OF bPersonLookup IN FRAME Dialog-Frame /* Lookup */
do:
  run openDialog in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnCancel Dialog-Frame
ON CHOOSE OF BtnCancel IN FRAME Dialog-Frame /* Cancel */
do:
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnOK Dialog-Frame
ON CHOOSE OF BtnOK IN FRAME Dialog-Frame /* Create */
do:
  if fPerson:input-value <> "" and fPerson:private-data <> "-99" 
   then
    do:
      if not validPerson()
       then
        return no-apply.
     end. 
     
  run saveAttorney in this-procedure.
  
  if not oplSave 
   then
    return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnUrl
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnUrl Dialog-Frame
ON CHOOSE OF BtnUrl IN FRAME Dialog-Frame /* URL */
do:
  if fWebsite:screen-value <> "" 
   then
    run openURL in this-procedure (fWebsite:screen-value).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbSt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbSt Dialog-Frame
ON VALUE-CHANGED OF cbSt IN FRAME Dialog-Frame
or "value-changed":u of cbstate 
or "value-changed":u of cbstatus
or "value-changed":u of fAddr 
or "value-changed":u of fAddr2 
or "value-changed":u of fCity 
or "value-changed":u of fEmail 
or "value-changed":u of fMobile
or "value-changed":u of fName 
or "value-changed":u of fName2 
or "value-changed":u of fPhone 
or "value-changed":u of fZip 
or "value-changed":u of fPerson
or "value-changed":u of fWebsite 
or "value-changed":u of fContractID
or "value-changed":u of fContact 
or "value-changed":u of fContractDate 
or "value-changed":u of fSubmissionDate 
or "value-changed":u of edNotes 
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? 
 then
  frame {&frame-name}:parent = active-window.

{lib/set-button.i &label="PersonLookup"  &image="images/s-lookup.bmp" &inactive="images/s-lookup-i.bmp"}
{lib/set-button.i &label="URL" &b=btnURL &image="images/s-url.bmp"    &inactive="images/s-url-i.bmp"}
setButtons().

if ipcAction = {&ActionEdit}
 then
  cAction = {&EditFromPerson}.
else if (ipcPersonID <> ""   and 
         ipcPersonID <> "0") 
      then
       cAction = {&NewFromPerson}.
     else
      cAction = {&NewFromMain}.  
 
 if ipcAction = {&ActionEdit} 
  then
   do:
     if not can-find(first attorney) 
      then
       run server/getAttorneys.p(ipcAttorneyID,
                                 "P",
                                 output table attorney,
                                 output std-lo,
                                 output std-ch).
   end.
  
  if not std-lo 
   then 
    do:
      if std-ch <> "" 
       then
        message std-ch view-as alert-box error buttons ok.
    end.

  /* Populate the cbState combo-box with states 
  for which we can craete a new attorney or modify the existing one */
{lib/get-state-list.i &combo=cbState &useFilter=false}
{lib/get-state-list.i &combo=cbSt    &useFilter=false &useSingle=true &onlyActive=false}

/* Populte the cbStatus combo-box with all the status an attorney might have */
{lib/get-sysprop-list.i &combo=cbStatus &appCode="'COM'" &objAction="'Attorney'" &objProperty="'Status'" &d="'{&ProspectStat}'"}
    
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.
  
  /* display data on UI */
  run displayData       in this-procedure.
  run enableDisableSave in this-procedure.

  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE createPerson Dialog-Frame 
PROCEDURE createPerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcPersonID as character no-undo.
  
  create tPerson.
  assign 
      tPerson.personID   = ipcPersonID
      tPerson.firstName  = Attorney.name1
      tPerson.dispName   = Attorney.name1
      tPerson.address1   = Attorney.addr1
      tPerson.address2   = Attorney.addr2
      tPerson.city       = Attorney.city
      tPerson.state      = Attorney.state
      tPerson.zip        = Attorney.zip
     /* tPerson.phone      = Attorney.phone         
      tPerson.mobile     = Attorney.contact
      tPerson.email      = Attorney.email   */      
      .
        
  create person.
  buffer-copy tPerson to person. 
  
  publish "newPersonAttorney" (input table tPerson).
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* This is the case of Modify from Person Detail */
  
  if cAction = {&EditFromPerson}
   then
    do:      
      assign
          frame dialog-frame:title     = "Edit Attorney"
          BtnOK             :label     = "Save"
          BtnOK             :tooltip   = "Save"
          cbState           :sensitive = false 
          cbStatus          :sensitive = false 
          .
     
      for first Attorney where Attorney.AttorneyID = ipcAttorneyID:
        assign
            cbState        :screen-value  = Attorney.stateID  
            fPerson        :screen-value  = if getPersonName(Attorney.personid) = ? or getPersonName(Attorney.personid) = "" then "" else getPersonName(Attorney.personid) 
            fPerson        :private-data  = Attorney.personid
            fname          :screen-value  = Attorney.name1
            fname2         :screen-value  = Attorney.firmName
            cbstatus       :screen-value  = Attorney.stat
            fAddr          :screen-value  = Attorney.addr1 
            fAddr2         :screen-value  = Attorney.addr2 
            fCity          :screen-value  = Attorney.city
            cbSt           :screen-value  = Attorney.state
            fZip           :screen-value  = Attorney.zip
            fMobile        :screen-value  = Attorney.contact
            fPhone         :screen-value  = Attorney.phone       
            fEmail         :screen-value  = Attorney.email                
            fWebsite       :screen-value  = Attorney.website  
            fContractID    :screen-value  = Attorney.contractID  
            fContact       :screen-value  = Attorney.contact 
            fContractDate  :screen-value  = string(Attorney.contractDate)  
            fSubmissionDate:screen-value  = string(Attorney.submissionDate)
            edNotes        :screen-value  = Attorney.comments
            flCreateDate   :screen-value  = string(Attorney.createDate)
            flProspectDate :screen-value  = string(Attorney.prospectDate)
            flWithdrawnDate:screen-value  = string(Attorney.withdrawnDate)
            flActiveDate   :screen-value  = string(Attorney.activeDate)
            flClosedDate   :screen-value  = string(Attorney.closedDate)
            flCancelDate   :screen-value  = string(Attorney.cancelDate)
            fMarkMandatory1:hidden        = true
            fMarkMandatory2:hidden        = false
            fMarkMandatory2:screen-value  = {&mandatory}
            opcAttorneyId                 = Attorney.AttorneyId no-error
           .
        if Attorney.personID <> "" 
         then
          do:
            cOldPersonID         = Attorney.personID.
          end.
      end.
    end.
    /* in case of new Attorney*/
  else
   do:
     cbState:add-first("None","None").
     assign
         frame dialog-frame:title    = "New Attorney"
         BtnOK             :label    = "Create"
         BtnOK             :tooltip  = "Create"
         cbState:screen-value        = "None"
         .

     /* when call from Person Detail */
     if cAction = {&NewFromPerson}
      then
       do:
         find first person no-error.
         assign
             fMarkMandatory1:hidden         = false
             fMarkMandatory1:screen-value   = {&mandatory}
             .
        end.
                          
       /* when call from Com Main Screen */
     else
      do:      
        find first person 
          where person.personID = fPerson:input-value no-error.
          assign
              fMarkMandatory1:hidden         = false
              fMarkMandatory1:screen-value   = {&mandatory}
              .
      end.

     assign
              fMarkMandatory2:hidden         = false
              fMarkMandatory2:screen-value   = {&mandatory}.

     if available person 
      then
       assign
           cbState :screen-value = if can-do(cbState:list-item-pairs ,person.state) 
                                   then person.state
                                   else cbState:input-value
           fPerson :screen-value = person.personID   
           fname   :screen-value = person.dispName
           fname2  :screen-value = person.dispName
           fAddr   :screen-value = person.address1
           fAddr2  :screen-value = person.address2          
           fCity   :screen-value = person.city
           cbSt    :screen-value = person.state
           fZip    :screen-value = person.zip
          /* fMobile :screen-value = person.mobile
           fPhone  :screen-value = person.phone
           fEmail  :screen-value = person.email */ no-error
           .
          
   end.
 
  assign
      cTrackName   = fname          :input-value   
      cTrackName2  = fname2         :input-value   
      cTrackStatus = cbstatus       :input-value
      cTrackAddr   = fAddr          :input-value   
      cTrackAddr2  = fAddr2         :input-value  
      cTrackCity   = fCity          :input-value   
      cTrackSt     = cbSt           :input-value    
      cTrackZip    = fZip           :input-value    
      cTrackMobile = fMobile        :input-value 
      cTrackPhone  = fPhone         :input-value  
      cTrackEmail  = fEmail         :input-value
      cTrackPerson = fPerson        :input-value 
      cTrackWebsite= fWebsite       :input-value
      cTrackID     = fContractID    :input-value
      cTrackContact= fContact       :input-value
      cTrackSigned = fContractDate  :input-value
      cTrackSubmit = fSubmissionDate:input-value
      cTrackNote   = edNotes        :input-value
      no-error
      .
 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* in case of edit an attorney */
  if cAction = {&EditFromPerson}
   then
    BtnOK:sensitive = not(cTrackName   = fname          :input-value      and
                          cTrackName2  = fname2          :input-value     and
                          cTrackStatus = cbstatus        :input-value     and
                          cTrackAddr   = fAddr           :input-value     and
                          cTrackAddr2  = fAddr2          :input-value     and
                          cTrackCity   = fCity           :input-value     and
                          cTrackSt     = cbSt            :input-value     and
                          cTrackZip    = fZip            :input-value     and
                          cTrackMobile = fMobile         :input-value     and
                          cTrackPhone  = fPhone          :input-value     and
                          cTrackPerson = fPerson         :input-value     and
                          cTrackEmail  = fEmail          :input-value     and
                          cTrackWebsite= fWebsite        :input-value     and
                          cTrackID     = fContractID     :input-value     and
                          cTrackContact= fContact        :input-value     and
                          cTrackSigned = fContractDate   :input-value     and
                          cTrackSubmit = fSubmissionDate :input-value     and
                          cTrackNote   = edNotes         :input-value)    and
                          fName:input-value  <> ""   
                          no-error.
 
  /* in case of creating a new attorney */                       
  else
    BtnOK:sensitive = not ((cbState :input-value = ""      or 
                            cbState :input-value = ?       or
                            cbState :input-value = "None"  or
                            cbState :input-value = 0)  or
                           (cbstatus:input-value = ""  or 
                            cbstatus:input-value = ?)  or
                           (fName   :input-value = ""  or 
                            fName   :input-value = ?)).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fPerson cbState fName fName2 cbStatus fWebsite fAddr fAddr2 fCity cbSt 
          fZip fMobile fPhone fEmail fContractID fContact fContractDate 
          fSubmissionDate flCreateDate flProspectDate flWithdrawnDate 
          flActiveDate flClosedDate flCancelDate edNotes fMarkMandatory1 
          fMarkMandatory2 
      WITH FRAME Dialog-Frame.
  ENABLE bPersonLookup fPerson cbState fName fName2 cbStatus fWebsite fAddr 
         fAddr2 fCity cbSt fZip fMobile fPhone fEmail fContractID fContact 
         fContractDate fSubmissionDate flCreateDate flProspectDate 
         flWithdrawnDate flActiveDate flClosedDate flCancelDate edNotes BtnUrl 
         BtnCancel RECT-5 RECT-6 RECT-7 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openDialog Dialog-Frame 
PROCEDURE openDialog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
  define variable cPersonID         as character no-undo.
  define variable cName             as character no-undo.
  define variable cAttorneyPersonID as character no-undo.
  
  if available attorney
   then
    cAttorneyPersonID = attorney.personID.
    
  run dialogpersonlookup.w(input  cAttorneyPersonID,
                           input  true,        /* True if add "New" */
                           input  true,        /* True if add "Blank" */
                           input  false,       /* True if add "ALL" */
                           output cPersonID,
                           output cName,
                           output std-lo).
   
  if not std-lo 
   then
    return no-apply.
    
  if  cAttorneyPersonID = cPersonID 
   then
    doModify(false).
  if cPersonID = "-99"
   then
    doModify(true).
  
  fPerson:screen-value = if cPersonID = "" then "" else cName . 
  fPerson:private-data = if cPersonID = "" then "" else cPersonID.
  
  if cAttorneyPersonID ne cPersonID
   then
    run enableDisableSave in this-procedure.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openURL Dialog-Frame 
PROCEDURE openURL :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pURL as character no-undo.
 
  run ShellExecuteA in this-procedure (0,
                                       "open",
                                       pURL,
                                       "",
                                       "",
                                       1,
                                       output std-in).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveAttorney Dialog-Frame 
PROCEDURE saveAttorney :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.  

  define variable opcPersonID  as character no-undo.
  define variable opcOrgID     as character no-undo.
   /* Although below validation will not execute as the save button will not be enabled
     unless person is selected, yet keeping it here for safer side. */

  if fname:input-value = "" or
     fname:input-value = ?
   then
    do:
      message "Name cannot be blank."
          view-as alert-box info buttons ok.
      run enableDisableSave in this-procedure.
      return.
    end.

  if cbState:input-value = "None" or 
     cbState:input-value = "" or
     cbState:input-value = ?
   then
    do:
      message "Attorney state is not valid."
          view-as alert-box info buttons ok.
      run enableDisableSave in this-procedure.
      return.
    end.
    
  if fPerson:private-data = "-99" 
   then
    message "A new person will also be created." view-as alert-box information button ok.  
    
  empty temp-table attorney.      
  create Attorney.
  assign  
      Attorney.stateID        = cbState        :input-value
      Attorney.personID       = fPerson        :private-data
      Attorney.name1          = fname          :input-value
      Attorney.firmName       = fname2         :input-value
      Attorney.stat           = cbStatus       :input-value        
      Attorney.addr1          = fAddr          :input-value
      Attorney.addr2          = fAddr2         :input-value
      Attorney.city           = fCity          :input-value
      Attorney.state          = cbSt           :input-value
      Attorney.zip            = fZip           :input-value
      Attorney.contact        = fMobile        :input-value        
      Attorney.phone          = fPhone         :input-value
      Attorney.email          = fEmail         :input-value
      Attorney.website        = fWebsite       :input-value       
      Attorney.contractID     = fContractID    :input-value       
      Attorney.contact        = fContact       :input-value       
      Attorney.contractDate   = fContractDate  :input-value
      Attorney.submissionDate = fSubmissionDate:input-value
      Attorney.comments       = edNotes        :input-value     
      Attorney.createDate     = flCreateDate   :input-value 
      Attorney.prospectDate   = flProspectDate :input-value
      Attorney.withdrawnDate  = flWithdrawnDate:input-value
      Attorney.activeDate     = flActiveDate   :input-value   
      Attorney.closedDate     = flClosedDate   :input-value  
      Attorney.cancelDate     = flCancelDate   :input-value   
      .

  if Attorney.personID = "New Person" 
   then
    Attorney.personID = "-99".
  
  /* in case of editing an attorney */
  if cAction = {&EditFromPerson}
   then
    do:
      Attorney.attorneyID = opcAttorneyId.
      
      publish "modifyAttorney" (input table Attorney,
                                output opcOrgID,
                                output opcPersonID,
                                output std-lo,
                                output std-ch).  
    end.   
  /* in case of creating new attorney */
  else
   do:
     publish "newAttorney" (input table Attorney,
                            output opcAttorneyID,
                            output opcOrgID,
                            output opcPersonID,
                            output std-lo,
                            output std-ch).    
   end.
    
  
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
  else
   do:
     oplSave = true.     
     if Attorney.personID = "-99" and (opcPersonID <> "" or opcPersonID <> ?)
      then
       run createPerson in this-procedure (input opcPersonID).
       
     publish "roleComplianceModified" (input cOldPersonID).    
     publish "roleComplianceModified" (input Attorney.personID).
   end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doModify Dialog-Frame 
FUNCTION doModify RETURNS LOGICAL
  ( input pModify as logical ) :
/*------------------------------------------------------------------------------
@description Disables/Enables the save buttons
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign 
      BtnOK:sensitive     = pModify
      BtnCancel:sensitive = pModify.
  end.
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getPersonName Dialog-Frame 
FUNCTION getPersonName RETURNS CHARACTER
  ( ipcPersonID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable opcName as character no-undo.
  
  publish "getPersonName" (input  ipcPersonID,
                           output opcName,
                           output std-lo).     

  if std-lo 
   then 
    return opcName.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validPerson Dialog-Frame 
FUNCTION validPerson RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  publish "validPerson" (input  fPerson:private-data,
                         output std-lo).                                               
  if not std-lo 
   then 
    do:
      fPerson:screen-value = "".    
      return false. /* Function return value. */
    end.
  return true.   /* Function return value. */
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

