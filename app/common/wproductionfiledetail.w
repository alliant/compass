&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wproductionfiledetail.w

  Description: Window for showing child records of transaction file detail records.

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: S Chandu

  Created: 06.05.2024
  Modified: 
  Date        Name     Comments
  07/22/2024  S Chandu On click of enter execute get data and sort by trandate.
  11/13/2024  SRK      Added note for legacy payment
  11/26/2024  Chandu   Added 'referenceID' instead of 'arTranID'.
  12/11/2024  Chandu   Modified to check 'artranID'.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */

define input parameter ipcAgentID     as character.
define input parameter ipcAgentName   as character.
define input parameter ipcFileNumber  as character.
define input parameter ipdtFromDate   as date.
define input parameter ipdtToDate     as date.

{lib/std-def.i}  
{lib/ar-def.i}
{lib/winlaunch.i} 
{lib/winshowscrollbars.i}

/* Temp-table Definition */
{tt/filedetail.i}
{tt/filedetail.i &tablealias=ttfiledetail}


define variable cAppCode as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwFileDetail

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES filedetail

/* Definitions for BROWSE brwFileDetail                                 */
&Scoped-define FIELDS-IN-QUERY-brwFileDetail filedetail.trxdetail filedetail.trandate filedetail.amount filedetail.accumbalance filedetail.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFileDetail   
&Scoped-define SELF-NAME brwFileDetail
&Scoped-define QUERY-STRING-brwFileDetail for each filedetail
&Scoped-define OPEN-QUERY-brwFileDetail open query {&SELF-NAME} for each filedetail.
&Scoped-define TABLES-IN-QUERY-brwFileDetail filedetail
&Scoped-define FIRST-TABLE-IN-QUERY-brwFileDetail filedetail


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwFileDetail}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bAgentLookup btGo flfileNumber flAgentID ~
brwFileDetail 
&Scoped-Define DISPLAYED-OBJECTS flfileNumber flAgentID flName 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VARIABLE C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup 
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Agent lookup".

DEFINE BUTTON btGo  NO-FOCUS
     LABEL "Go" 
     SIZE 4.8 BY 1.14 TOOLTIP "Get data".

DEFINE VARIABLE flAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN 
     SIZE 18.2 BY 1 NO-UNDO.

DEFINE VARIABLE flfileNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "File Number" 
     VIEW-AS FILL-IN 
     SIZE 27.2 BY 1 NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 48 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwFileDetail FOR 
      filedetail SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwFileDetail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFileDetail C-Win _FREEFORM
  QUERY brwFileDetail DISPLAY
      filedetail.trxdetail      label "Transaction"               format "x(40)" width 35
filedetail.trandate         label  "Date"                     format "99/99/99" width 11
filedetail.amount           label  "Amount"                   format "->>>,>>>,>>9.99" width 11
filedetail.accumbalance     label  "Balance"                  format "->>>,>>>,>>9.99" width 11
filedetail.description      label  "Notes"                    format "x(200)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 139.2 BY 12.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bAgentLookup AT ROW 1.48 COL 31 WIDGET-ID 350
     btGo AT ROW 1.48 COL 124.8 WIDGET-ID 262 NO-TAB-STOP 
     flfileNumber AT ROW 1.52 COL 95.4 COLON-ALIGNED WIDGET-ID 426
     flAgentID AT ROW 1.57 COL 10.8 COLON-ALIGNED WIDGET-ID 352
     flName AT ROW 1.57 COL 34 COLON-ALIGNED NO-LABEL WIDGET-ID 424
     brwFileDetail AT ROW 3.1 COL 2.8 WIDGET-ID 200
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COLUMN 1.2 ROW 1
         SIZE 235 BY 24.67 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Production File Detail"
         HEIGHT             = 15.05
         WIDTH              = 143
         MAX-HEIGHT         = 37.1
         MAX-WIDTH          = 307.2
         VIRTUAL-HEIGHT     = 37.1
         VIRTUAL-WIDTH      = 307.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwFileDetail flName DEFAULT-FRAME */
ASSIGN 
       brwFileDetail:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwFileDetail:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR FILL-IN flName IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flName:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFileDetail
/* Query rebuild information for BROWSE brwFileDetail
     _START_FREEFORM
open query {&SELF-NAME} for each filedetail.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwFileDetail */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Production File Detail */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Production File Detail */
DO:  
  /* This event will close the window and terminate the procedure.  */ 
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Production File Detail */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup C-Win
ON CHOOSE OF bAgentLookup IN FRAME DEFAULT-FRAME /* agentlookup */
DO:
  define variable cAgentID  as character no-undo.
  define variable cName     as character no-undo.
    
  run dialogagentlookup.w(input flAgentID:input-value,
                          input "",      /* Selected State ID */
                          input true,    /* Allow 'ALL' */
                          output cAgentID,
                          output std-ch, /* Agent state ID */
                          output cName,
                          output std-lo).
   
  if not std-lo  
   then
     return no-apply.
     
  assign
      flAgentID:screen-value = cAgentID
      flName:screen-value    = cName
      . 
 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFileDetail
&Scoped-define SELF-NAME brwFileDetail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFileDetail C-Win
ON ROW-DISPLAY OF brwFileDetail IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i} 
 
   /* background color change for date ranged */
   if (filedetail.trandate >= ipdtFromDate and filedetail.trandate <= ipdtToDate)
    then
     do:
        assign
            filedetail.trxdetail :fgcolor in browse  brwFileDetail        = 9
            filedetail.trandate :fgcolor in browse brwFileDetail          = 9
            filedetail.amount :fgcolor in browse  brwFileDetail           = 9
            filedetail.accumbalance :fgcolor in browse brwFileDetail      = 9
            filedetail.description :fgcolor in browse  brwFileDetail      = 9
            .
     end.
     
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGo C-Win
ON CHOOSE OF btGo IN FRAME DEFAULT-FRAME /* Go */
DO:
  if not validAgent()
   then
    return no-apply.
        
  run getData in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID C-Win
ON VALUE-CHANGED OF flAgentID IN FRAME DEFAULT-FRAME /* Agent ID */
DO:
  assign
      flName:screen-value     = ""
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flfileNumber
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flfileNumber C-Win
ON return OF flfileNumber IN FRAME DEFAULT-FRAME /* File Number */
DO:
  if not flfileNumber:read-only = true
  then
   apply 'choose':U to btGo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.


/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow" anywhere.

setStatusMessage("").

/* Fetch data before window is visible */
if ipcAgentID ne '' 
 then   
  run getData in this-procedure.

bAgentLookup:load-image            ("images/s-lookup.bmp").
bAgentLookup:load-image-insensitive("images/s-lookup-i.bmp").

btGo        :load-image            ("images/s-completed.bmp").
btGo        :load-image-insensitive("images/s-completed-i.bmp").
/*bExport    :load-image             ("images/s-excel.bmp").
bExport    :load-image-insensitive ("images/s-excel-i.bmp").   */
   
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
  run enable_UI.             

  assign
      flAgentID:screen-value    =  ipcAgentID  
      flName:screen-value       =  ipcAgentName
      flfileNumber:screen-value =  ipcFileNumber
      .
      
  if ipcAgentID ne "" 
   then
    do:
      bAgentLookup:sensitive = false.
      flAgentID:read-only = true.
      flfileNumber:read-only = true.
      btGo:hidden = true.
    end.
   else
     do:
       flfileNumber:read-only = false.
     end.
      
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flfileNumber flAgentID flName 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bAgentLookup btGo flfileNumber flAgentID brwFileDetail 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwFileDetail:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
  
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table filedetail:handle.
  run util/exporttable.p (table-handle std-ha,
                          "filedetail",
                          "for each filedetail",
                          "trxdetail,trandate,amount,description",
                          "Transaction,Date,Amount,Remarks" ,
                          std-ch,
                          "Production_File_Detail_" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable deBalance   as decimal   no-undo.
  define variable cAgentID    as character no-undo.
  define variable cFileNumber as character no-undo.
   
  assign
      cAgentID    = if flAgentID:input-value = ""    then ipcAgentID else flAgentID:input-value.
      cFileNumber = if flfileNumber:input-value = "" then ipcFileNumber else flfileNumber:input-value.
      .
      
 if cAgentID  = "" or cFileNumber  = ""
  then
   return.
  
  empty temp-table ttfiledetail.
  empty temp-table filedetail.
      
  run server\getproductionfiledetail.p (input cAgentID,
                                        input cFileNumber,
                                        output table ttfiledetail,
                                        output std-lo,
                                        output std-ch).
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
 
  for each ttfiledetail by ttfiledetail.trandate:
    create filedetail.
    buffer-copy ttfiledetail to filedetail.
    if filedetail.type = "A" and  filedetail.artranID > 0
     then
      filedetail.description = substitute("Applied by &1 on &2. Check Number: &3", string(filedetail.appliedBy), string(filedetail.applieddate), string(filedetail.checkNumber) ).
     else if filedetail.type = "A" and  (filedetail.referenceID <> 0 or filedetail.referenceID <> ? ) and ( filedetail.artranID = 0 or filedetail.artranID = ?)
      then
       filedetail.description = filedetail.description. 
      
      /* Rolling Balance */
    filedetail.accumbalance = filedetail.amount + deBalance.
    deBalance = filedetail.accumbalance.  
  end.
  
  apply 'value-changed' to browse brwFileDetail.
  
  open query brwFileDetail preselect each filedetail.
  
   /* Set Status count with date and time from the server */
  setStatusRecords(query brwFileDetail:num-results).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 42
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 3
      .
   
  run ShowScrollBars(frame {&frame-name}:handle, no, no).  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
   do with frame {&frame-name}:
  end.
  
  if flAgentID:input-value = ""
   then return false. /* Function return value. */
  
  else if flAgentID:input-value = {&ALL}
   then
    flName:screen-value = {&NotApplicable}.
       
  else if flAgentID:input-value <> {&ALL}
   then
    do:
      publish "getAgentName" (input flAgentID:input-value,
                              output std-ch,
                              output std-lo).                                               
      if not std-lo 
       then 
        do:
          assign 
              flAgentID:screen-value = "" 
              flName:screen-value    = ""
              .
          return false. /* Function return value. */
        end.
      flName:screen-value = std-ch.
    end. 
  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

