&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/* dialogliabrange.w
   display the LIABility RANGE for the liability distribution window
   D.Sinclair 6.20.2013
   */
   

def input parameter pOldList as char.
def output parameter pNewList as char.

/* def var pOldList as char no-undo.  */
/* def var pNewList as char no-undo.  */

{lib/std-def.i}

def temp-table liability
 field seq as int
 field amount as deci format "zzz,zzz,zz9.99"
 field openend as logical
 .

def buffer x-liability for liability.
def var tCnt as int no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES liability

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData liability.amount   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData amount   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH liability
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH liability.
&Scoped-define TABLES-IN-QUERY-brwData liability
&Scoped-define FIRST-TABLE-IN-QUERY-brwData liability


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwData bSort Btn_OK 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bSort  NO-FOCUS
     LABEL "Sort" 
     SIZE 11.4 BY 1.1 TOOLTIP "Sequence the liabilities from low to high value".

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Apply" 
     SIZE 15 BY 1.14 TOOLTIP "Save the liability ranges"
     BGCOLOR 8 .

DEFINE VARIABLE tChanged AS CHARACTER FORMAT "X(256)":U INITIAL "Changed" 
      VIEW-AS TEXT 
     SIZE 10 BY .62 TOOLTIP "Generate the results to see the changed liability ranges"
     FGCOLOR 12  NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      liability SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData Dialog-Frame _FREEFORM
  QUERY brwData DISPLAY
      liability.amount label "Maximum"

 enable amount
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-SCROLLBAR-VERTICAL SIZE 32.2 BY 28.38 ROW-HEIGHT-CHARS .67 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     brwData AT ROW 1.33 COL 2.2 WIDGET-ID 200
     bSort AT ROW 29.86 COL 22.6 WIDGET-ID 2
     Btn_OK AT ROW 31.48 COL 11.4
     tChanged AT ROW 30.14 COL 2.4 NO-LABEL WIDGET-ID 6
     SPACE(23.19) SKIP(2.33)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Liability Ranges" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData 1 Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN tChanged IN FRAME Dialog-Frame
   NO-DISPLAY NO-ENABLE ALIGN-L                                         */
ASSIGN 
       tChanged:HIDDEN IN FRAME Dialog-Frame           = TRUE
       tChanged:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH liability.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Liability Ranges */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData Dialog-Frame
ON VALUE-CHANGED OF brwData IN FRAME Dialog-Frame
DO:
  tChanged:visible in frame {&frame-name} = true.
  tChanged:screen-value = "Changed".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSort
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSort Dialog-Frame
ON CHOOSE OF bSort IN FRAME Dialog-Frame /* Sort */
DO:
  close query brwData.
  tCnt = 0.
  for each liability 
    where liability.amount > 0
     by liability.amount:
   tCnt = tCnt + 1.
   liability.seq = tCnt.
  end.
  for each liability
    where liability.amount = 0:
   tCnt = tCnt + 1.
   liability.seq = tCnt.
  end.
  open query brwData
    for each liability by liability.seq.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

if num-entries(pOldList) > 0 
 then
  do: OLD-LOOP:
      do std-in = 1 to num-entries(pOldList):
       if std-in > 32 
        then leave OLD-LOOP.
       std-de = decimal(entry(std-in, pOldList)) no-error.
       if error-status:error 
        then leave OLD-LOOP.
       create liability.
       liability.amount = std-de.
      end.
  end.

tCnt = 0.
for each liability
  by liability.amount:
 tCnt = tCnt + 1.
 liability.seq = tCnt.
end.

do std-in = tCnt + 1 to 32:
 create liability.
 liability.seq = std-in.
 liability.amount = 0.
end.


pNewList = pOldList.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  
  pNewList = "".
  for each x-liability
    where x-liability.amount > 0
    by x-liability.amount:
   pNewList = pNewList 
      + (if pNewList > "" then "," else "")
      + string(x-liability.amount).
  end.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE brwData bSort Btn_OK 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

