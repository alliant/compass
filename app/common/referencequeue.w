&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Temp Table Definitions ---                                           */
{tt/sysqueue.i &tableAlias="data"}
{tt/ini.i &tableAlias="paramitem"}
{tt/ini.i &tableAlias="destitem"}

/* Local Variable Definitions ---                                       */
define variable dColWidth as decimal   no-undo.
define variable tItem     as character no-undo.
define variable tItemDesc as character no-undo.
define variable cActions  as character no-undo.
define variable cSearch   as character no-undo.

{lib/std-def.i}
{lib/set-button-def.i}
{lib/set-filter-def.i &tableName=data}
{lib/brw-multi-def.i}
&scoped-define defaultSort "'by queueID descending by seq'"
&global-define ResultNotMatch "Results may not match current parameters."
&global-define All              "ALL"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fQueue
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data destitem paramitem

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData data.queueID data.seq data.actionID data.UID data.notifyRequestor data.appCode data.queueDate data.StartDate data.EndDate data.queueStatDesc data.itemStatDesc data.itemFault   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each data
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each data.
&Scoped-define TABLES-IN-QUERY-brwData data
&Scoped-define FIRST-TABLE-IN-QUERY-brwData data


/* Definitions for BROWSE brwDests                                      */
&Scoped-define FIELDS-IN-QUERY-brwDests destitem.id destitem.val   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDests   
&Scoped-define SELF-NAME brwDests
&Scoped-define QUERY-STRING-brwDests for each destitem
&Scoped-define OPEN-QUERY-brwDests open query {&SELF-NAME} for each destitem.
&Scoped-define TABLES-IN-QUERY-brwDests destitem
&Scoped-define FIRST-TABLE-IN-QUERY-brwDests destitem


/* Definitions for BROWSE brwParams                                     */
&Scoped-define FIELDS-IN-QUERY-brwParams paramitem.id paramitem.val   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwParams   
&Scoped-define SELF-NAME brwParams
&Scoped-define QUERY-STRING-brwParams for each paramitem
&Scoped-define OPEN-QUERY-brwParams open query {&SELF-NAME} for each paramitem.
&Scoped-define TABLES-IN-QUERY-brwParams paramitem
&Scoped-define FIRST-TABLE-IN-QUERY-brwParams paramitem


/* Definitions for FRAME fQueue                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fQueue ~
    ~{&OPEN-QUERY-brwData}~
    ~{&OPEN-QUERY-brwDests}~
    ~{&OPEN-QUERY-brwParams}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btSetPeriod bGet btClear fActionParam ~
fUidParam fAppCodeParam fQueueStatusParam fQueuedFromParam fQueuedToParam ~
fAction fSearch brwData brwParams brwDests rFilter RECT-2 
&Scoped-Define DISPLAYED-OBJECTS fActionParam fUidParam fAppCodeParam ~
fQueueStatusParam fQueuedFromParam fQueuedToParam fAction fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bFilterClear 
     LABEL "FilterClear" 
     SIZE 7 BY 1.67.

DEFINE BUTTON bGet  NO-FOCUS
     LABEL "Get Data" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get Data".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON btClear 
     IMAGE-UP FILE "images/s-cross.bmp":U NO-FOCUS
     LABEL "Clear" 
     SIZE 5 BY 1.14 TOOLTIP "Blank out the date range".

DEFINE BUTTON btSetPeriod 
     IMAGE-UP FILE "images/s-calendar.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 5 BY 1.14 TOOLTIP "Set 30 days range to current date".

DEFINE VARIABLE fAction AS CHARACTER FORMAT "X(256)":U 
     LABEL "Action" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 33 BY 1 NO-UNDO.

DEFINE VARIABLE fActionParam AS CHARACTER FORMAT "X(256)":U 
     LABEL "Action" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 50.8 BY 1 NO-UNDO.

DEFINE VARIABLE fAppCodeParam AS CHARACTER FORMAT "X(256)":U 
     LABEL "App Code" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 17.2 BY 1 NO-UNDO.

DEFINE VARIABLE fQueueStatusParam AS CHARACTER FORMAT "X(256)":U 
     LABEL "Queue Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fUidParam AS CHARACTER FORMAT "X(256)":U 
     LABEL "UID" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 43 BY 1 NO-UNDO.

DEFINE VARIABLE fQueuedFromParam AS DATETIME FORMAT "99/99/99":U 
     LABEL "Queued" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fQueuedToParam AS DATETIME FORMAT "99/99/99":U 
     LABEL "To" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 33 BY 1 NO-UNDO.

DEFINE RECTANGLE rActions
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17.4 BY 3.33.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 125 BY 3.33.

DEFINE RECTANGLE rFilter
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 50.4 BY 3.33.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      data SCROLLING.

DEFINE QUERY brwDests FOR 
      destitem SCROLLING.

DEFINE QUERY brwParams FOR 
      paramitem SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      data.queueID      width 10
data.seq                width 6
data.actionID           width 28     format "x(50)"  label "Action"
data.UID                width 32     format "x(50)"
data.notifyRequestor    width 8      format "Yes/No"
data.appCode            width 10
data.queueDate          width 13
data.StartDate          width 13
data.EndDate            width 13
data.queueStatDesc      width 13
data.itemStatDesc       width 13
data.itemFault
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 192 BY 14.86
         TITLE "Queue Items" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwDests
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDests C-Win _FREEFORM
  QUERY brwDests DISPLAY
      destitem.id  column-label "Field" format "x(100)" width 20
destitem.val column-label "Value" format "x(1000)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 94.6 BY 7.86
         TITLE "Destinations" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwParams
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwParams C-Win _FREEFORM
  QUERY brwParams DISPLAY
      paramitem.id column-label "Field" format "x(100)" width 20
paramitem.val column-label "Value" format "x(1000)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 97 BY 7.86
         TITLE "Parameters" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fQueue
     btSetPeriod AT ROW 2.91 COL 112.8 WIDGET-ID 62 NO-TAB-STOP 
     bExport AT ROW 2.05 COL 185.4 WIDGET-ID 16 NO-TAB-STOP 
     bGet AT ROW 2.05 COL 118.6 WIDGET-ID 36 NO-TAB-STOP 
     bRefresh AT ROW 2.05 COL 178.2 WIDGET-ID 18 NO-TAB-STOP 
     btClear AT ROW 2.91 COL 107.6 WIDGET-ID 60 NO-TAB-STOP 
     fActionParam AT ROW 1.76 COL 11.8 COLON-ALIGNED WIDGET-ID 46
     fUidParam AT ROW 1.76 COL 72.6 COLON-ALIGNED WIDGET-ID 48
     fAppCodeParam AT ROW 2.95 COL 11.8 COLON-ALIGNED WIDGET-ID 50
     fQueueStatusParam AT ROW 2.95 COL 46.6 COLON-ALIGNED WIDGET-ID 52
     fQueuedFromParam AT ROW 2.95 COL 72.6 COLON-ALIGNED WIDGET-ID 56
     fQueuedToParam AT ROW 2.95 COL 91 COLON-ALIGNED WIDGET-ID 58
     fAction AT ROW 1.76 COL 133.6 COLON-ALIGNED WIDGET-ID 26
     fSearch AT ROW 2.95 COL 133.6 COLON-ALIGNED WIDGET-ID 22
     brwData AT ROW 4.76 COL 2 WIDGET-ID 200
     brwParams AT ROW 19.81 COL 2 WIDGET-ID 400
     brwDests AT ROW 19.81 COL 99.4 WIDGET-ID 300
     bFilterClear AT ROW 2.05 COL 169.2 WIDGET-ID 24 NO-TAB-STOP 
     "Filters" VIEW-AS TEXT
          SIZE 5.6 BY .71 AT ROW 1 COL 127.8 WIDGET-ID 42
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .71 AT ROW 1 COL 178 WIDGET-ID 44
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .48 AT ROW 1 COL 3 WIDGET-ID 40
     rActions AT ROW 1.24 COL 176.8 WIDGET-ID 8
     rFilter AT ROW 1.24 COL 126.8 WIDGET-ID 20
     RECT-2 AT ROW 1.24 COL 2 WIDGET-ID 30
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 194 BY 26.71 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "System Queue"
         HEIGHT             = 26.71
         WIDTH              = 194
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fQueue
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData fSearch fQueue */
/* BROWSE-TAB brwParams brwData fQueue */
/* BROWSE-TAB brwDests brwParams fQueue */
/* SETTINGS FOR BUTTON bExport IN FRAME fQueue
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bFilterClear IN FRAME fQueue
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bRefresh IN FRAME fQueue
   NO-ENABLE                                                            */
ASSIGN 
       brwData:ALLOW-COLUMN-SEARCHING IN FRAME fQueue = TRUE
       brwData:COLUMN-RESIZABLE IN FRAME fQueue       = TRUE.

ASSIGN 
       brwDests:ALLOW-COLUMN-SEARCHING IN FRAME fQueue = TRUE
       brwDests:COLUMN-RESIZABLE IN FRAME fQueue       = TRUE.

ASSIGN 
       brwParams:ALLOW-COLUMN-SEARCHING IN FRAME fQueue = TRUE
       brwParams:COLUMN-RESIZABLE IN FRAME fQueue       = TRUE.

/* SETTINGS FOR RECTANGLE rActions IN FRAME fQueue
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each data.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDests
/* Query rebuild information for BROWSE brwDests
     _START_FREEFORM
open query {&SELF-NAME} for each destitem.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwDests */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwParams
/* Query rebuild information for BROWSE brwParams
     _START_FREEFORM
open query {&SELF-NAME} for each paramitem.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwParams */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* System Queue */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* System Queue */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* System Queue */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fQueue /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilterClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilterClear C-Win
ON CHOOSE OF bFilterClear IN FRAME fQueue /* FilterClear */
DO:  
  assign
     fSearch:screen-value = ""
     fAction:screen-value = {&ALL}
     .
    
  apply "VALUE-CHANGED" to fAction.
   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGet C-Win
ON CHOOSE OF bGet IN FRAME fQueue /* Get Data */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fQueue /* Refresh */
DO:
  publish "LoadSysDests".
  
  assign
      cActions = fAction:input-value
      cSearch  = fSearch:input-value
      .
  
  run getData in this-procedure.
  
  assign
      fAction:screen-value      = cActions
      fSearch:screen-value      = cSearch
      .
 
   apply "VALUE-CHANGED" to fAction.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fQueue /* Queue Items */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fQueue /* Queue Items */
DO:
  {lib/brw-startSearch-multi.i &sortClause={&defaultSort}}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fQueue /* Queue Items */
DO:
  assign
    bExport:sensitive  = available data
    bRefresh:sensitive = true
    .
  
  close query brwParams.
  close query brwDests.
  
  if not available data
   then return.

  /* add the parameters */
  empty temp-table paramitem.
  do std-in = 1 to num-entries(data.parameters, {&msg-dlm}):
    tItem = entry(std-in, data.parameters, {&msg-dlm}).
    create paramitem.
    assign
      paramitem.id  = entry(1, tItem, "=")
      paramitem.val = entry(2, tItem, "=")
      .
  end.
  {&OPEN-QUERY-brwParams}

  /* add the destinations */
  empty temp-table destitem.
  do std-in = 1 to num-entries(data.destinations, {&msg-dlm}):
    tItem = entry(std-in, data.destinations, {&msg-dlm}).
    tItemDesc = "".
    publish "GetSysPropDesc" ("SYS", "Destination", "Type", entry(1, tItem, "="), output tItemDesc, output std-lo, output std-ch).
    create destitem.
    assign
      destitem.id  = tItemDesc
      destitem.val = entry(2, tItem, "=")
      .
  end.
  {&OPEN-QUERY-brwDests}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDests
&Scoped-define SELF-NAME brwDests
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDests C-Win
ON ROW-DISPLAY OF brwDests IN FRAME fQueue /* Destinations */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDests C-Win
ON START-SEARCH OF brwDests IN FRAME fQueue /* Destinations */
DO:
  {lib/brw-startSearch-multi.i &sortClause={&defaultSort}}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwParams
&Scoped-define SELF-NAME brwParams
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwParams C-Win
ON ROW-DISPLAY OF brwParams IN FRAME fQueue /* Parameters */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwParams C-Win
ON START-SEARCH OF brwParams IN FRAME fQueue /* Parameters */
DO:
  {lib/brw-startSearch-multi.i &sortClause={&defaultSort}}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClear C-Win
ON CHOOSE OF btClear IN FRAME fQueue /* Clear */
DO:
  assign
      fQueuedFromParam:screen-value = ""
      fQueuedToParam:screen-value   = ""
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btSetPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btSetPeriod C-Win
ON CHOOSE OF btSetPeriod IN FRAME fQueue
DO:
   assign
       fQueuedFromParam:screen-value = string(today - 30)
       fQueuedToParam:screen-value   = string(today)
       .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAction C-Win
ON VALUE-CHANGED OF fAction IN FRAME fQueue /* Action */
DO:
  define variable tWhereClause as character no-undo.
  tWhereClause = getWhereClause().
  
  setFilterCombos(self:label).
  {lib/brw-startSearch-multi.i &whereClause="tWhereClause" &browse-name=brwData &sortClause={&defaultSort}}
  apply "VALUE-CHANGED" to browse brwData.
  run setFilterButton in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fActionParam
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fActionParam C-Win
ON VALUE-CHANGED OF fActionParam IN FRAME fQueue /* Action */
OR 'VALUE-CHANGED' of fUidParam
OR 'VALUE-CHANGED' of fAppCodeParam
OR 'VALUE-CHANGED' of fQueueStatusParam
DO:
   resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON LEAVE OF fSearch IN FRAME fQueue /* Search */
DO:
  define variable tWhereClause as character no-undo.
  tWhereClause = getWhereClause().
  
  {lib/brw-startSearch-multi.i &whereClause="tWhereClause" &browse-name=brwData &sortClause={&defaultSort}}
  apply "VALUE-CHANGED" to browse brwData.
   
  run setFilterButton in this-procedure.
END.


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fQueue /* Search */
DO:
  apply "LEAVE" to fSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-close.i}
{lib/win-status.i}
{lib/brw-main-multi.i &browse-list="brwData,brwParams,brwDests"}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

{&window-name}:window-state = 2.

{&window-name}:max-width-pixels = session:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:min-height-pixels = {&window-name}:height-pixels.

{lib/set-filter.i &label="Search" &id="uid:destinations:parameters" &populate=false}
{lib/set-filter.i &label="Action" &id="actionID" &value="actionID"}
bGet:load-image               ("images/completed.bmp").
bGet:load-image-insensitive   ("images/completed-i.bmp").
{lib/set-button.i &label="Refresh"}
{lib/set-button.i &label="Export"}
{lib/set-button.i &label="FilterClear"}
setButtons().



/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  assign
      fAction:list-items   = "ALL"
      fAction:screen-value = {&ALL}
      .

  run getParametersData in this-procedure.
  

  {lib/get-column-width.i &var=dColWidth &col="'itemFault'"}
  {&window-name}:window-state = 3.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableActionButtons C-Win 
PROCEDURE enableActionButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 std-lo = can-find(first data).
 do with frame {&frame-name}:
  assign
      bExport:sensitive      = std-lo
      bRefresh:sensitive     = true
      .
 end.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fActionParam fUidParam fAppCodeParam fQueueStatusParam 
          fQueuedFromParam fQueuedToParam fAction fSearch 
      WITH FRAME fQueue IN WINDOW C-Win.
  ENABLE btSetPeriod bGet btClear fActionParam fUidParam fAppCodeParam 
         fQueueStatusParam fQueuedFromParam fQueuedToParam fAction fSearch 
         brwData brwParams brwDests rFilter RECT-2 
      WITH FRAME fQueue IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fQueue}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table data:handle.
  run util/exporttable.p (table-handle std-ha,
                          "data",
                          "for each data",
                          "queueID,seq,actionID,uid,notifyRequestor,appCode,queueDate,startDate,endDate,queueStatDesc,itemStatDesc,itemFault",
                          "Queue ID,Seq,Action ID,UID,Notify Requester,App Code,Queue Date,Start Date,End Date,Queue Status,Item Status,Item Fault",
                          std-ch,
                          "SystemQueue-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  empty temp-table data.
  
  run server/getsysqueue.p (input  0,
                            input fActionParam:input-value,
                            input fUidParam:input-value,                      
                            input fAppCodeParam:input-value,
                            input fQueueStatusParam:input-value,  
                            input fQueuedFromParam:input-value, 
                            input fQueuedToParam:input-value,
                            output table data,
                            output std-lo,
                            output std-ch
                            ).
                            
          
  if not std-lo
   then
    do:
      message std-ch view-as alert-box error buttons ok.
      return.
    end.
    
  setFilterCombos("ALL").  
  {lib/brw-startSearch-multi.i &sortClause={&defaultSort}}

    run setFilterButton in this-procedure.
    
    run enableActionButtons in this-procedure.
    
    apply "VALUE-CHANGED" to {&browse-name}. 
    
    setStatusRecords(num-results("{&browse-name}")).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getParametersData C-Win 
PROCEDURE getParametersData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
define variable cAction      as character no-undo.
define variable cUid         as character no-undo.
define variable cAppcode     as character no-undo.
define variable cQueueStatus as character no-undo.
define variable lSuccess     as logical   no-undo.
define variable cMsg         as character no-undo.
  

run server/getsysqueueparamsdata.p(output cAction,
                                   output cUid,
                                   output cAppcode,
                                   output cQueueStatus,
                                   output lSuccess,
                                   output cMsg).


assign
    fActionParam:list-items           = "ALL" + (if cAction <> ""  then ","   else "" ) +  cAction
    fQueueStatusParam:list-item-pairs = "ALL,ALL" + (if cQueueStatus <> ""  then ","  else "" ) + cQueueStatus
    fUidParam:list-items              = "ALL" +  (if cUid <> ""  then ","  else "" ) + cUid                           
    fAppCodeParam:list-items          = "ALL" +  (if cAppCode <> ""  then ","  else "" ) + cAppCode
    . 
assign
    fActionParam:screen-value       = {&ALL}
    fUidParam:screen-value          = {&ALL}
    fAppCodePAram:screen-value      = {&ALL}
    fQueueStatusParam:screen-value  = {&ALL}
    .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterButton C-Win 
PROCEDURE setFilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  if fAction:screen-value      ne {&ALL} or     
     fSearch:screen-value      ne ""
   then
    bFilterClear:sensitive = true.
   else   
    bFilterClear:sensitive = false.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  {lib/brw-sortData-multi.i}
  setStatusCount(num-results("{&browse-name}")).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  
  /* {&frame-name} components */                                                           
  assign
    /* main browse */
    {&browse-name}:width-pixels   = frame {&frame-name}:width-pixels - 10
    {&browse-name}:height-pixels  = frame {&frame-name}:height-pixels - {&browse-name}:y - 175
    /* parameters browse */
    browse brwParams:y            = {&browse-name}:y + {&browse-name}:height-pixels + 5
    browse brwParams:width-pixels = {&browse-name}:width-pixels / 2 - 3
    /* destinations browse */
    browse brwDests:y             = {&browse-name}:y + {&browse-name}:height-pixels + 5
    browse brwDests:x             = browse brwParams:x + browse brwParams:width-pixels + 5
    browse brwDests:width-pixels  = {&browse-name}:width-pixels / 2 - 3
    .
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

  {lib/resize-column.i &var=dColWidth &col="'itemFault'"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

