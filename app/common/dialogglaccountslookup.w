&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogglaccountlookup.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul

  Created:12/31/19
  
  @Modified:
  Date         Name         Description
  08/06/2020   AG           Change Title and removed Cancel button. 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/ar-def.i}

{tt/state.i}
{tt/glaccount.i}
{tt/glaccount.i &tableAlias=ttglaccount}

/* Parameters Definitions ---                                           */
define input  parameter ipcID    as  character no-undo.
define output parameter opcID    as  character no-undo.
define output parameter opcDesc           as  character no-undo.
define output parameter oplSuccess        as  logical   no-undo.

/* Local Variable */
define variable lApplySearchString as logical     no-undo.
define variable cSearchString      as character   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwglaccount

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES glaccount

/* Definitions for BROWSE brwglaccount                                  */
&Scoped-define FIELDS-IN-QUERY-brwglaccount glaccount.type "Type" glaccount.ID "ID" glaccount.description "Description"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwglaccount   
&Scoped-define SELF-NAME brwglaccount
&Scoped-define QUERY-STRING-brwglaccount preselect each glaccount
&Scoped-define OPEN-QUERY-brwglaccount open query brwglaccount preselect each glaccount.
&Scoped-define TABLES-IN-QUERY-brwglaccount glaccount
&Scoped-define FIRST-TABLE-IN-QUERY-brwglaccount glaccount


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwglaccount}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fSearch bSearch brwglaccount Btn_OK 
&Scoped-Define DISPLAYED-OBJECTS fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bSearch 
     LABEL "Search" 
     SIZE 4.8 BY 1.14 TOOLTIP "Search Locks".

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Select" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 57 BY 1 TOOLTIP "Search Criteria (Code Type, Code, Type, Description)" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwglaccount FOR 
      glaccount SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwglaccount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwglaccount Dialog-Frame _FREEFORM
  QUERY brwglaccount DISPLAY
      glaccount.type     label        "Type"            format "x(12)"           
glaccount.ID             label        "ID"              format "x(16)" 
glaccount.description    label        "Description"     format "x(200)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 71 BY 11.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fSearch AT ROW 1.86 COL 9 COLON-ALIGNED WIDGET-ID 66
     bSearch AT ROW 1.76 COL 69 WIDGET-ID 314
     brwglaccount AT ROW 3.71 COL 3 WIDGET-ID 300
     Btn_OK AT ROW 16.19 COL 31
     SPACE(29.99) SKIP(0.47)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Select Account" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwglaccount bSearch Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       brwglaccount:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE
       brwglaccount:COLUMN-MOVABLE IN FRAME Dialog-Frame         = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwglaccount
/* Query rebuild information for BROWSE brwglaccount
     _START_FREEFORM
open query brwglaccount preselect each glaccount.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwglaccount */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Select Account */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwglaccount
&Scoped-define SELF-NAME brwglaccount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwglaccount Dialog-Frame
ON DEFAULT-ACTION OF brwglaccount IN FRAME Dialog-Frame
DO:
  apply "Choose" to Btn_OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwglaccount Dialog-Frame
ON ROW-DISPLAY OF brwglaccount IN FRAME Dialog-Frame
do:
  {lib/brw-rowDisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwglaccount Dialog-Frame
ON START-SEARCH OF brwglaccount IN FRAME Dialog-Frame
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch Dialog-Frame
ON CHOOSE OF bSearch IN FRAME Dialog-Frame /* Search */
DO:
  lApplySearchString = true.
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Select */
DO:
  run setAgentID in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch Dialog-Frame
ON ENTRY OF fSearch IN FRAME Dialog-Frame /* Search */
DO:
  /* store the previous value of search string on which search is applied */
  cSearchString = fSearch:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch Dialog-Frame
ON RETURN OF fSearch IN FRAME Dialog-Frame /* Search */
DO:
  apply "Choose" to bSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch Dialog-Frame
ON VALUE-CHANGED OF fSearch IN FRAME Dialog-Frame /* Search */
DO:
  /* as soon as we change the search string, we track that string 
  is not applied and change the status in taskbar */
  lApplySearchString = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */
{lib/brw-main.i}

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bSearch:load-image("images/s-magnifier.bmp").
bSearch:load-image-insensitive("images/s-magnifier-i.bmp").
 
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.


  
  run getData in this-procedure.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fSearch 
      WITH FRAME Dialog-Frame.
  ENABLE fSearch bSearch brwglaccount Btn_OK 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData Dialog-Frame 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define buffer ttglaccount for ttglaccount.
  
  close query brwglaccount.
  empty temp-table glaccount.
  
  /* if search string is already applied then filter on the basis of what is
     present inside fsearch fill-in */
  if lApplySearchString 
   then
    cSearchString = trim(fSearch:input-value).
  /* if search string is changed but not applied then restrore the fSearch fill-in
     to previous applied serach string and filter on the basis of what is
     present inside fsearch fill-in */
  else
    fSearch:screen-value = cSearchString.

  create glaccount.
  assign
      glaccount.type           = {&NotApplicable}
      glaccount.ID             = {&ALL}
      glaccount.description    = {&NotApplicable}
      .
    
  for each ttglaccount where (if cSearchString <> "" then ttglaccount.ID matches ("*" + cSearchString + "*")
                                             else ttglaccount.ID = ttglaccount.ID) or
                                            (if cSearchString <> "" then ttglaccount.description matches ("*" + cSearchString + "*")
                                             else ttglaccount.description = ttglaccount.description) or
                                            (if cSearchString <> "" then ttglaccount.type matches ("*" + cSearchString + "*")
                                             else ttglaccount.type = ttglaccount.type):
    
    create glaccount.
    buffer-copy ttglaccount to glaccount.
  end.

  open query brwglaccount for each glaccount by glaccount.ID desc.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData Dialog-Frame 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  do with frame {&frame-name}:
  end.
  
  publish "getAccounts"(output table ttglaccount).

  run filterData in this-procedure.
 
  find first glaccount where glaccount.ID = ipcID no-error.
  if available glaccount
   then      
    reposition brwglaccount to rowid rowid(glaccount) no-error.   
  
  apply 'entry' to fSearch.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setAgentID Dialog-Frame 
PROCEDURE setAgentID :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if available glaccount
   then
    assign
        opcID      = glaccount.ID
        opcDesc    = glaccount.description
        oplSuccess = yes
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData Dialog-Frame 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by glaccount.ID ". 
  
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

