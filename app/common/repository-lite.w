&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input parameter pEntity   as character no-undo.
define input parameter pEntityID as character no-undo.

/* Local Variable Definitions ---                                       */

{lib/std-def.i}
{lib/set-button-def.i}

/* Temp Table ---                                                       */
{tt/repository.i &tableAlias="orderfile"}
{tt/repository.i &tableAlias="temporderfile"}
{tt/repository.i &tableAlias="temporderfile2"}

/* Functions and Parameters ---                                         */
{lib/add-delimiter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwDocs

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES orderfile

/* Definitions for BROWSE brwDocs                                       */
&Scoped-define FIELDS-IN-QUERY-brwDocs orderfile.displayName orderfile.category orderfile.createdDate orderfile.fileSizeDesc orderfile.createdByDesc   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDocs   
&Scoped-define SELF-NAME brwDocs
&Scoped-define QUERY-STRING-brwDocs FOR EACH orderfile by orderfile.displayName
&Scoped-define OPEN-QUERY-brwDocs OPEN QUERY {&SELF-NAME} FOR EACH orderfile by orderfile.displayName.
&Scoped-define TABLES-IN-QUERY-brwDocs orderfile
&Scoped-define FIRST-TABLE-IN-QUERY-brwDocs orderfile


/* Definitions for FRAME DEFAULT-FRAME                                  */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bFileDelete bFileDownload bFileModify ~
bFileRefresh bFileUpload tCategory brwDocs 
&Scoped-Define DISPLAYED-OBJECTS tCategory 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD checkDisplayNameModify C-Win 
FUNCTION checkDisplayNameModify RETURNS LOGICAL
  ( input pDisplayName as character,
    input pCategory as character,
    input pID as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD checkDisplayNameNew C-Win 
FUNCTION checkDisplayNameNew RETURNS LOGICAL
  ( input pDisplayName as character,
    input pCategory as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doModifyDocuments C-Win 
FUNCTION doModifyDocuments RETURNS LOGICAL
  ( input pEnable as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setupDocuments C-Win 
FUNCTION setupDocuments RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bFileDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 7.2 BY 1.71 TOOLTIP "Remove the previously saved document(s)".

DEFINE BUTTON bFileDownload  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open the selected document(s)".

DEFINE BUTTON bFileModify  NO-FOCUS
     LABEL "Categorize" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify the category of the selected document(s)".

DEFINE BUTTON bFileRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh the list of documents".

DEFINE BUTTON bFileUpload  NO-FOCUS
     LABEL "Upload" 
     SIZE 7.2 BY 1.71 TOOLTIP "Upload a new document".

DEFINE VARIABLE tCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Default Category" 
     VIEW-AS FILL-IN 
     SIZE 34 BY 1 TOOLTIP "Category associated to newly added documents" NO-UNDO.

DEFINE RECTANGLE rButtons
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 38.2 BY 2.05.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwDocs FOR 
      orderfile SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwDocs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDocs C-Win _FREEFORM
  QUERY brwDocs DISPLAY
      orderfile.displayName column-label "File Name" width 40
 orderfile.category format "x(200)" width 25 label "Category"
 orderfile.createdDate width 25
 orderfile.fileSizeDesc width 10
       orderfile.createdByDesc label "Saved By" width 20
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE DROP-TARGET SIZE 147.6 BY 13.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bFileDelete AT ROW 1.38 COL 17.4 WIDGET-ID 6
     bFileDownload AT ROW 1.38 COL 2.6 WIDGET-ID 24
     bFileModify AT ROW 1.38 COL 24.8 WIDGET-ID 36
     bFileRefresh AT ROW 1.38 COL 32.2 WIDGET-ID 32
     bFileUpload AT ROW 1.38 COL 10 WIDGET-ID 4
     tCategory AT ROW 1.76 COL 59 COLON-ALIGNED WIDGET-ID 40
     brwDocs AT ROW 3.38 COL 2 WIDGET-ID 200
     rButtons AT ROW 1.24 COL 2 WIDGET-ID 22
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 149.6 BY 16.48 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Repository"
         HEIGHT             = 16.48
         WIDTH              = 149.6
         MAX-HEIGHT         = 17.62
         MAX-WIDTH          = 149.6
         VIRTUAL-HEIGHT     = 17.62
         VIRTUAL-WIDTH      = 149.6
         MAX-BUTTON         = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwDocs tCategory DEFAULT-FRAME */
ASSIGN 
       bFileDelete:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "Delete".

ASSIGN 
       bFileDownload:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "Open".

ASSIGN 
       bFileModify:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "Email".

ASSIGN 
       bFileRefresh:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "Email".

ASSIGN 
       bFileUpload:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "New".

ASSIGN 
       brwDocs:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwDocs:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR RECTANGLE rButtons IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDocs
/* Query rebuild information for BROWSE brwDocs
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH orderfile by orderfile.displayName.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwDocs */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Repository */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Repository */
DO:
  publish "WindowClosed" ("DOCUMENTS").

  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileDelete C-Win
ON CHOOSE OF bFileDelete IN FRAME DEFAULT-FRAME /* Del */
DO:
  if not available orderfile
   then return.
   
  std-lo = true.
  publish "GetConfirmDelete" (output std-lo).
  if std-lo
   then
    do:
      std-lo = false.
      message "File '" + orderfile.displayName + "' will be permanently removed. Continue?" view-as alert-box question buttons yes-no update std-lo.
      if not std-lo 
       then return.
    end.
    
  run server/deletearcrepositoryfile.p (orderfile.identifier, output std-lo, output std-ch).
  if not std-lo
   then message std-ch view-as alert-box error buttons ok.
   else apply "CHOOSE" to bFileRefresh.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileDownload
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileDownload C-Win
ON CHOOSE OF bFileDownload IN FRAME DEFAULT-FRAME /* Open */
DO:
  if not available orderfile
   then return.
  
  define variable tFile as character no-undo.
  tFile = orderfile.name.
  run server/downloadarcrepositoryfile.p (orderfile.identifier, input-output tFile, output std-lo, output std-ch).
  if not std-lo
   then message std-ch view-as alert-box error buttons ok.
   else run util/openfile.p (tFile).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileModify C-Win
ON CHOOSE OF bFileModify IN FRAME DEFAULT-FRAME /* Categorize */
DO:
  if not available orderfile
   then return.

  define variable tNewCategory as character no-undo.
  
  run dialogdocumentcategory.w (output tNewCategory, output std-lo).
  if not std-lo
   then return.
   
  /* we need to rename all the category for the files */
  empty temp-table temporderfile.
  empty temp-table temporderfile2.
  buffer-copy orderfile to temporderfile.
  assign
    temporderfile.displayName = orderfile.displayName
    temporderfile.category    = tNewCategory
    temporderfile.isPrivate   = false
    orderfile.category        = tNewCategory
    .
  run server/modifyarcrepositoryfile.p (table temporderfile, output table temporderfile2, output std-lo, output std-ch).
  if not std-lo
   then message std-ch view-as alert-box error buttons ok.
   else apply "CHOOSE" to bFileRefresh.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileRefresh C-Win
ON CHOOSE OF bFileRefresh IN FRAME DEFAULT-FRAME /* Refresh */
DO:
  empty temp-table orderfile.
  run server/getarcrepositoryfiles.p (pEntity, pEntityId, false, output table orderfile, output std-lo, output std-ch).
  if not std-lo
   then
    do:
      doModifyDocuments(false).
      message std-ch view-as alert-box error buttons ok.
    end.
   else setupDocuments().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileUpload
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileUpload C-Win
ON CHOOSE OF bFileUpload IN FRAME DEFAULT-FRAME /* Upload */
DO:
  define variable pFile as character no-undo.
  define variable pSave as logical   no-undo initial false.

  run dialogrepositoryupload-lite.w (table orderfile, tCategory:screen-value, output pFile, output pSave).
  if not pSave
   then return.

  run uploadRepositoryFile in this-procedure (tCategory:screen-value, pFile, output std-lo).
  if std-lo
   then setupDocuments().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDocs
&Scoped-define SELF-NAME brwDocs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON DEFAULT-ACTION OF brwDocs IN FRAME DEFAULT-FRAME
DO:
  apply "CHOOSE" to bFileDownload.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON DROP-FILE-NOTIFY OF brwDocs IN FRAME DEFAULT-FRAME
DO:  
  define variable tPercent as integer no-undo.
  
  {lib/pbshow.i "''"}
  FILE-LOOP:
  do std-in = 1 to self:num-dropped-files:
    std-ch = self:get-dropped-file(std-in).
    tPercent = (std-in / self:num-dropped-files) * 100.
    {lib/pbupdate.i "'Uploading file ' + std-ch" tPercent}
  
    if not checkDisplayNameNew(entry(1, std-ch, "."), tCategory:screen-value)
     then next.
    
    run uploadRepositoryFile in this-procedure (tCategory:screen-value, std-ch, output std-lo).
    if not std-lo 
     then leave FILE-LOOP.
  end.
  self:end-file-drop().
  setupDocuments().
  {lib/pbhide.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON ROW-DISPLAY OF brwDocs IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON START-SEARCH OF brwDocs IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON VALUE-CHANGED OF brwDocs IN FRAME DEFAULT-FRAME
DO:
  assign
    std-lo                  = available orderfile
    bFileDelete:sensitive   = std-lo
    bFileModify:sensitive   = std-lo
    bFileDownload:sensitive = std-lo
    tCategory:screen-value  = if std-lo then orderfile.category else ""
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/brw-main.i}
{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}
{lib/win-status.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.
   
{lib/set-button.i &label="FileDownload"  &image="images/download.bmp"    &inactive="images/download-i.bmp"}
{lib/set-button.i &label="FileUpload"    &image="images/upload.bmp"      &inactive="images/upload-i.bmp" &toggle=false}
{lib/set-button.i &label="FileModify"    &image="images/update.bmp"      &inactive="images/update-i.bmp"}
{lib/set-button.i &label="FileDelete"    &image="images/delete.bmp"      &inactive="images/delete-i.bmp"}
{lib/set-button.i &label="FileRefresh"   &image="images/sync.bmp"        &inactive="images/sync-i.bmp"   &toggle=false}
setButtons().

subscribe to "CheckDisplayName" anywhere.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  apply "CHOOSE" to bFileRefresh.
  apply "VALUE-CHANGED" to brwDocs.
  RUN enable_UI.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CheckDisplayName C-Win 
PROCEDURE CheckDisplayName :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pDisplayName as character no-undo.
  define input  parameter pCategory    as character no-undo.
  define input  parameter pID          as character no-undo.
  define output parameter pCheck       as logical   no-undo.

  if pID = ""
   then pCheck = checkDisplayNameNew(pDisplayName, pCategory).
   else pCheck = checkDisplayNameModify(pDisplayName, pCategory, pID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tCategory 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bFileDelete bFileDownload bFileModify bFileRefresh bFileUpload 
         tCategory brwDocs 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i} 
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE uploadRepositoryFile C-Win 
PROCEDURE uploadRepositoryFile PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pCategory as character no-undo.
  define input  parameter pFile     as character no-undo.
  define output parameter pSuccess  as logical   no-undo.
  
  define variable tUserID as character no-undo.
  publish "GetCredentialsID" (output tUserID).
  
  empty temp-table temporderfile.
  run server/uploadarcrepositoryfile.p (tUserID, false, pEntity, pEntityId, pCategory, pFile, output table temporderfile, output pSuccess, output std-ch).
  if not pSuccess
   then message std-ch view-as alert-box error buttons ok.
   else
    for first temporderfile no-lock:
      create orderfile.
      buffer-copy temporderfile to orderfile.
      run server/linkdocument.p (pEntity, pEntityID, 0, output std-lo, output std-ch).
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION checkDisplayNameModify C-Win 
FUNCTION checkDisplayNameModify RETURNS LOGICAL
  ( input pDisplayName as character,
    input pCategory as character,
    input pID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tCheck as logical no-undo initial yes.
  if can-find(first orderfile where displayName = pDisplayName and category = pCategory and not (identifier = pID))
   then message "There is already a file in category '" + pCategory + "' with the name '" + pDisplayName + "'. Do you want to override the file?" view-as alert-box question buttons yes-no update tCheck.

  RETURN tCheck.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION checkDisplayNameNew C-Win 
FUNCTION checkDisplayNameNew RETURNS LOGICAL
  ( input pDisplayName as character,
    input pCategory as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tCheck as logical no-undo initial yes.
  if can-find(first orderfile where displayName = pDisplayName and category = pCategory)
   then message "There is already a file in category '" + pCategory + "' with the name '" + pDisplayName + "'. Do you want to override the file?" view-as alert-box question buttons yes-no update tCheck.

  RETURN tCheck.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doModifyDocuments C-Win 
FUNCTION doModifyDocuments RETURNS LOGICAL
  ( input pEnable as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      bFileRefresh:sensitive   = pEnable
      bFileModify:sensitive    = pEnable
      bFileDelete:sensitive    = pEnable
      bFileDownload:sensitive  = pEnable
      bFileUpload:sensitive    = pEnable
      tCategory:read-only      = not pEnable
      .
  end.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setupDocuments C-Win 
FUNCTION setupDocuments RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  for each orderfile exclusive-lock:
    publish "GetSysUserName" (orderfile.createdBy, output orderfile.createdByDesc).
  end.
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

