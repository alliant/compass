&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/* common/dialogmovepolicy.w
   @date 1.12.2020
   @author Shubham
   @purpose Simple UI to allow user to change a file number.
*/

def input parameter pCurrent as char no-undo.
def input-output parameter pNew as char no-undo.
def output parameter pSave as logical no-undo init false.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tCurrent tNew Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tCurrent tNew tCurrent-2 tCurrent-3 ~
tCurrent-4 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Move" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tCurrent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Current File Number" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 36.2 BY 1 NO-UNDO.

DEFINE VARIABLE tCurrent-2 AS CHARACTER FORMAT "X(256)":U INITIAL "This utility moves the current policy from one file to another file and only updates records related" 
      VIEW-AS TEXT 
     SIZE 91 BY .62 NO-UNDO.

DEFINE VARIABLE tCurrent-3 AS CHARACTER FORMAT "X(256)":U INITIAL "to the policy. Examples of records that are updated include the batch and accounting records" 
      VIEW-AS TEXT 
     SIZE 91 BY .62 NO-UNDO.

DEFINE VARIABLE tCurrent-4 AS CHARACTER FORMAT "X(256)":U INITIAL "for the one policy. The old and new files are both assigned to the same agent." 
      VIEW-AS TEXT 
     SIZE 91 BY .62 NO-UNDO.

DEFINE VARIABLE tNew AS CHARACTER FORMAT "X(256)":U 
     LABEL "New File Number" 
     VIEW-AS FILL-IN 
     SIZE 36.2 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     tCurrent AT ROW 4.05 COL 32.6 COLON-ALIGNED WIDGET-ID 2 NO-TAB-STOP 
     tNew AT ROW 5.24 COL 32.6 COLON-ALIGNED WIDGET-ID 4
     Btn_OK AT ROW 7.14 COL 32.8
     Btn_Cancel AT ROW 7.14 COL 49.4
     tCurrent-2 AT ROW 1.33 COL 1.8 COLON-ALIGNED NO-LABEL WIDGET-ID 8 NO-TAB-STOP 
     tCurrent-3 AT ROW 2 COL 1.8 COLON-ALIGNED NO-LABEL WIDGET-ID 10 NO-TAB-STOP 
     tCurrent-4 AT ROW 2.67 COL 1.8 COLON-ALIGNED NO-LABEL WIDGET-ID 12 NO-TAB-STOP 
     SPACE(1.39) SKIP(5.84)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Move Policy"
         DEFAULT-BUTTON Btn_Cancel CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       tCurrent:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN tCurrent-2 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       tCurrent-2:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN tCurrent-3 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       tCurrent-3:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN tCurrent-4 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       tCurrent-4:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Move Policy */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNew Dialog-Frame
ON VALUE-CHANGED OF tNew IN FRAME Dialog-Frame /* New File Number */
DO:

 if tCurrent:screen-value <> tNew:screen-value 
  then
   enable Btn_OK with frame Dialog-Frame. 
  else 
   disable  Btn_OK with frame Dialog-Frame. 

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

tCurrent = pCurrent.
tNew = pNew.
pSave = false.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  pNew = tNew:screen-value .
  pSave = true.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tCurrent tNew tCurrent-2 tCurrent-3 tCurrent-4 
      WITH FRAME Dialog-Frame.
  ENABLE tCurrent tNew Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

