&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogagentsgroup.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Sachin Chaturvedi

  Created:09/15/2020
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/get-column.i}
{tt/agent.i &tablealias = "tAgent"}

{tt/consolidate.i &tableAlias=tConsolidate}

/* Parameter Definitions ---                                           */
define input  parameter table for tConsolidate.
define input parameter lEditable    as logical   no-undo.

/* Local Variable Definitions ---                                       */
define variable haRecord     as handle    no-undo.
define variable cKey         as character no-undo.
define variable cValue       as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwConsolidate

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tConsolidate

/* Definitions for BROWSE brwConsolidate                                */
&Scoped-define FIELDS-IN-QUERY-brwConsolidate tConsolidate.fldKey tConsolidate.fldDesc tConsolidate.isChecked   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwConsolidate tConsolidate.isChecked   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwConsolidate tConsolidate
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwConsolidate tConsolidate
&Scoped-define SELF-NAME brwConsolidate
&Scoped-define QUERY-STRING-brwConsolidate for each tConsolidate
&Scoped-define OPEN-QUERY-brwConsolidate open query {&SELF-NAME} for each tConsolidate.
&Scoped-define TABLES-IN-QUERY-brwConsolidate tConsolidate
&Scoped-define FIRST-TABLE-IN-QUERY-brwConsolidate tConsolidate


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwConsolidate}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS Btn_Cancel brwConsolidate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwQAR-2 
       MENU-ITEM m_Mail_auditor-2 LABEL "Mail auditor"  .


/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 14 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK 
     LABEL "Consolidate" 
     SIZE 14 BY 1.14
     BGCOLOR 8 .

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwConsolidate FOR 
      tConsolidate SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwConsolidate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwConsolidate Dialog-Frame _FREEFORM
  QUERY brwConsolidate DISPLAY
      tConsolidate.fldKey column-label "Field" format "x(20)"     width 16
tConsolidate.fldDesc   label "Value"      format "x(80)" width 35
tConsolidate.isChecked  column-label  "Select to!Consolidate"      view-as toggle-box
enable tConsolidate.isChecked
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 69 BY 10.52
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     Btn_OK AT ROW 12.52 COL 22.8
     Btn_Cancel AT ROW 12.52 COL 38.2
     brwConsolidate AT ROW 1.52 COL 3 WIDGET-ID 500
     SPACE(1.19) SKIP(1.95)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Consolidation Parameters"
         CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwConsolidate Btn_Cancel Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       brwConsolidate:POPUP-MENU IN FRAME Dialog-Frame             = MENU POPUP-MENU-brwQAR-2:HANDLE
       brwConsolidate:ALLOW-COLUMN-SEARCHING IN FRAME Dialog-Frame = TRUE
       brwConsolidate:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwConsolidate
/* Query rebuild information for BROWSE brwConsolidate
     _START_FREEFORM
open query {&SELF-NAME} for each tConsolidate.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwConsolidate */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Consolidation Parameters */
DO:
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwConsolidate
&Scoped-define SELF-NAME brwConsolidate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwConsolidate Dialog-Frame
ON ROW-DISPLAY OF brwConsolidate IN FRAME Dialog-Frame
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwConsolidate Dialog-Frame
ON START-SEARCH OF brwConsolidate IN FRAME Dialog-Frame
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Consolidate */
DO:
  
  run server/getConsolidatedAgents.p(input table tConsolidate,
                                     output std-in,
                                     output table tagent,
                                     output std-lo,
                                     output std-ch
                                     ).
  if std-in eq 1
   then
    message "Only one agent matches the selected criteria. No consolidation will occur."
        view-as alert-box information buttons ok.
   else
    do:
      run wagentsgroup.w persistent(input table tConsolidate,
                                    input table tagent).
      apply "END-ERROR":U to self.                                           
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */
 {lib/brw-main.i}
/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

on entry of tConsolidate.isChecked in browse brwConsolidate
do:
  haRecord = browse brwConsolidate:query:get-buffer-handle(1).
end.

on value-changed of tConsolidate.isChecked in browse brwConsolidate
do:
    
  cKey = haRecord:buffer-field("fldKey"):buffer-value().
  cValue = haRecord:buffer-field("fldValue"):buffer-value().
  for each tConsolidate where tConsolidate.fldKey = cKey and tConsolidate.fldValue = cValue:
    assign
        tConsolidate.isChecked = if cValue = "" then false else (if lEditable then (not tConsolidate.isChecked) else tConsolidate.isChecked)
        tConsolidate.isChecked:checked in browse brwConsolidate =  tConsolidate.isChecked
        .
  end.
  browse brwConsolidate:refresh().
  run enableDisableSave in this-procedure. 
end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  run enableDisableSave in this-procedure.
  if not lEditable 
   then
    do:
      assign
        Btn_OK:hidden = true
        Btn_Cancel:hidden = true
        .
      run changeColLabel in this-procedure.
    end.    
        
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changeColLabel Dialog-Frame 
PROCEDURE changeColLabel :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hBuffer as handle    no-undo.
  define variable hField  as handle    no-undo.
  define variable hColumn as handle    no-undo.
  define variable cLabel  as character no-undo.
  
  create buffer hBuffer for table "tConsolidate".

  cLabel = "".
  hField = hBuffer:buffer-field(4).
  if valid-handle(hField)
   then
    do:
      if hField:name = "isChecked" then cLabel = "Selected to!Consolidate".
      /* set the column */
      hColumn = getColumn(browse {&browse-name}:handle, hField:name).
      if valid-handle(hColumn) and cLabel > ""
       then hColumn:label = cLabel.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if lEditable and can-find(first tConsolidate where tConsolidate.isChecked) 
   then
    Btn_OK:sensitive = true.
   else 
    Btn_OK:sensitive = false.
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE Btn_Cancel brwConsolidate 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

