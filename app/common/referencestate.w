&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* referencestate.w
   Window of STATEs
   @author D.Sinclair
   @created 7.14.2014 based on wstatcode.w
   @modification
     date         Name           Description
     06/15/2022   SA             Task 93485 - Added RegionDesc field displaying  
                                 from browse(grid).
 */


CREATE WIDGET-POOL.

{tt/state.i}
{tt/stateevent.i}
{tt/state.i      &tableAlias="tempstate"}
{tt/stateevent.i &tableAlias="tempstateevent"}

{lib/std-def.i}
{lib/set-button-def.i}
{lib/brw-multi-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES state stateevent

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData state.stateID state.seq state.description state.regionDesc state.active state.appDate state.licEffDate state.website   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each state
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each state.
&Scoped-define TABLES-IN-QUERY-brwData state
&Scoped-define FIRST-TABLE-IN-QUERY-brwData state


/* Definitions for BROWSE brwEvent                                      */
&Scoped-define FIELDS-IN-QUERY-brwEvent stateevent.eventdate stateevent.Description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwEvent   
&Scoped-define SELF-NAME brwEvent
&Scoped-define QUERY-STRING-brwEvent for each stateevent
&Scoped-define OPEN-QUERY-brwEvent open query {&SELF-NAME} for each stateevent.
&Scoped-define TABLES-IN-QUERY-brwEvent stateevent
&Scoped-define FIRST-TABLE-IN-QUERY-brwEvent stateevent


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}~
    ~{&OPEN-QUERY-brwEvent}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwData bActivate bEventAdd bEventDelete ~
bEventModify bExport bModify bRefresh brwEvent 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bActivate  NO-FOCUS
     LABEL "Act" 
     SIZE 7.2 BY 1.71 TOOLTIP "Activate".

DEFINE BUTTON bEventAdd  NO-FOCUS
     LABEL "addAQ" 
     SIZE 4.6 BY 1.14 TOOLTIP "Add Event".

DEFINE BUTTON bEventDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 4.6 BY 1.14 TOOLTIP "Delete Event".

DEFINE BUTTON bEventModify  NO-FOCUS
     LABEL "Edit" 
     SIZE 4.6 BY 1.14 TOOLTIP "Edit Event".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Edit".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE RECTANGLE rActions
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 30.8 BY 2.05.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      state SCROLLING.

DEFINE QUERY brwEvent FOR 
      stateevent SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      state.stateID label "StateID" width 10
 state.seq label "Seq" format "99" width 6
 state.description label "Description" format "x(40)"
 state.regionDesc label "Region" format "x(23)" 
 state.active label "Active" format "Yes/No" width 8
 state.appDate column-label "Application!Date" format "99/99/9999"
 state.licEffDate column-label "Licensed!Date" format "99/99/9999"
 state.website label "Website" format "x(100)" width 30
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 176 BY 13.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwEvent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwEvent C-Win _FREEFORM
  QUERY brwEvent DISPLAY
      stateevent.eventdate column-label "Event Date" format "99/99/9999"
 stateevent.Description label "Description" format "x(70)"width 40
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 171 BY 5.86
         TITLE "Key Events" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwData AT ROW 3.38 COL 2 WIDGET-ID 200
     bActivate AT ROW 1.38 COL 24.8 WIDGET-ID 14 NO-TAB-STOP 
     bEventAdd AT ROW 17.43 COL 1.8 WIDGET-ID 204 NO-TAB-STOP 
     bEventDelete AT ROW 19.62 COL 1.8 WIDGET-ID 206 NO-TAB-STOP 
     bEventModify AT ROW 18.52 COL 1.8 WIDGET-ID 224 NO-TAB-STOP 
     bExport AT ROW 1.38 COL 10 WIDGET-ID 2 NO-TAB-STOP 
     bModify AT ROW 1.38 COL 17.4 WIDGET-ID 16 NO-TAB-STOP 
     bRefresh AT ROW 1.38 COL 2.6 WIDGET-ID 4 NO-TAB-STOP 
     brwEvent AT ROW 17.48 COL 7 WIDGET-ID 300
     rActions AT ROW 1.24 COL 2 WIDGET-ID 8
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 178 BY 22.52 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "States"
         HEIGHT             = 22.52
         WIDTH              = 178
         MAX-HEIGHT         = 23.71
         MAX-WIDTH          = 178
         VIRTUAL-HEIGHT     = 23.71
         VIRTUAL-WIDTH      = 178
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData 1 fMain */
/* BROWSE-TAB brwEvent rActions fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

ASSIGN 
       brwEvent:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwEvent:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

/* SETTINGS FOR RECTANGLE rActions IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each state.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwEvent
/* Query rebuild information for BROWSE brwEvent
     _START_FREEFORM
open query {&SELF-NAME} for each stateevent.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwEvent */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* States */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* States */
DO:
  /* This event will close the window and terminate the procedure.  */
  apply "CLOSE":U to this-procedure.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* States */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bActivate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActivate C-Win
ON CHOOSE OF bActivate IN FRAME fMain /* Act */
DO:
  define variable lSuccess    as logical   no-undo.
  define variable activeRowID as rowid     no-undo.
  define variable cStateID    as character no-undo.
  
  if not available state
   then return.
  
  cStateID = state.stateID.

  if state.active 
   then publish "DeactivateState" (input cStateID, output lSuccess).
   else publish "ActivateState" (input cStateID, output lSuccess).
     
  if lSuccess 
   then
    do:
      run getData.  
  
      activeRowID = rowID(state).
      reposition brwData to rowID activeRowID no-error.
      apply "VALUE-CHANGED" to brwData.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEventAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEventAdd C-Win
ON CHOOSE OF bEventAdd IN FRAME fMain /* addAQ */
DO:
  define variable activeRowID as rowid     no-undo.
  define variable cStateID    as character no-undo.
  
  empty temp-table tempstateevent.
  if available stateevent 
   then 
    assign
      cStateID    = stateevent.stateID
      activeRowID = rowid(stateevent)
      .

  run dialogaddstateevent.w (input              cStateID,
                             input-output table tempstateevent).
  
  find first tempstateevent no-error.
  if available tempstateevent and not can-find (first stateevent where stateevent.stateID = tempstateevent.stateID
                                                                 and stateevent.seq     = tempstateevent.seq) then
  do:
    create stateevent.
    buffer-copy tempstateevent to stateevent.
    assign activeRowID = rowid(stateevent).
  end.
 
  open query brwEvent for each stateevent where stateevent.stateID = cStateID.
  reposition brwEvent to rowid activeRowID no-error.
  apply "value-changed" to brwData in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEventDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEventDelete C-Win
ON CHOOSE OF bEventDelete IN FRAME fMain /* Delete */
do:
  define variable cStateID as character no-undo.
  define variable lSuccess as logical   no-undo.
  define variable cMsg     as character no-undo.
  
  {lib/confirm-delete.i "State Event"}
  
  find current stateevent no-error.
  if available stateevent 
   then
    do:
      cStateID = stateevent.stateID.
      publish "deleteStateEvent" (input  stateevent.stateId,
                                  input  stateevent.seq,
                                  output lSuccess,
                                  output cMsg).
      if lSuccess 
       then
        do:
          delete stateevent.
          open query brwEvent for each stateevent where stateevent.stateID = cStateID.
          apply "value-changed" to brwData in frame {&frame-name}.
        end. /* if lSuccess then */

    end. /* if avail stateevent then */
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEventModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEventModify C-Win
ON CHOOSE OF bEventModify IN FRAME fMain /* Edit */
do:
  define variable cStateID    as character no-undo.
  define variable activeRowID as rowid     no-undo.
  empty temp-table tempstateevent.

  find current stateevent no-error.
  if available stateevent 
   then
    do:
      cStateID = stateevent.stateID.
      create tempstateevent.
      buffer-copy stateevent to tempstateevent.
    end.
      
  run dialogaddstateevent.w (input stateevent.stateID,
                             input-output table tempstateevent).
  
  find first tempstateevent no-error.
  if available tempstateevent and can-find (first stateevent where stateevent.stateID = tempstateevent.stateID
                                                             and stateevent.seq     = tempstateevent.seq)
   then
    do:
      buffer-copy tempstateevent except tempstateevent.seq to stateevent.
      assign activeRowID = rowid(stateevent).
    end.
 
  open query brwEvent for each stateevent where stateevent.stateID = cStateID.
  reposition brwEvent to rowid activeRowID no-error.
  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME fMain /* Modify */
DO:
  if not available state
   then return.
   
  run actionModify in this-procedure (state.stateID).

  /*empty temp-table ttstate.
  if available state
   then
    do:
      cStateID = state.stateID.
      create ttstate.
      buffer-copy state to ttstate.
    end.

  run dialogmodifystate.w (input cStateID,
                           input-output table ttstate).

  find first ttstate no-error.
  if available ttstate
   then
    do:
      cCurrentStateID = ttstate.stateId.
      run getData.
  
      find first state 
        where state.stateID = cCurrentStateID 
        no-error.
      reposition brwData to rowid rowid(state) no-error.
      apply "value-changed" to brwData in frame {&frame-name}.
    end.*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  publish "GetStateDetails" (input "",
                             output table state,
                             output table stateEvent).
  run getData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  apply "choose" to bModify .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  if not available state
   then return.
   
  if state.active 
   then
    do:
      bactivate:load-image("images/flag_red.bmp").
      bactivate:tooltip = "Deactivate".
    end.
   else
    do:
      bactivate:load-image("images/flag_green.bmp").
      bactivate:tooltip = "Activate".
    end.
    
  open query brwEvent for each stateevent where stateevent.stateID = state.stateID.
  
  if num-results("brwEvent") > 0
   then enable bEventModify bEventDelete with frame {&frame-name}.
   else disable bEventModify bEventDelete with frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwEvent
&Scoped-define SELF-NAME brwEvent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwEvent C-Win
ON DEFAULT-ACTION OF brwEvent IN FRAME fMain /* Key Events */
DO:
  apply "choose" to bEventModify.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwEvent C-Win
ON ROW-DISPLAY OF brwEvent IN FRAME fMain /* Key Events */
DO:
  {lib/brw-rowDisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwEvent C-Win
ON START-SEARCH OF brwEvent IN FRAME fMain /* Key Events */
DO:
  if not available stateevent
   then return.

  define variable tWhereClause as character no-undo.
  tWhereClause = "where stateevent.stateID ='" + stateevent.stateID + "'".

  {lib/brw-startSearch-multi.i &whereClause="tWhereClause"}

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}
{lib/brw-main-multi.i &browse-list="brwData,brwEvent"}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

subscribe to "StateDataChanged" anywhere.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/set-button.i &label="Refresh"     &toggle=false}
{lib/set-button.i &label="Export"}
{lib/set-button.i &label="Modify"}
{lib/set-button.i &label="Activate"    &image="images/flag_red.bmp" &inactive="images/flag-i.bmp"}
{lib/set-button.i &label="EventAdd"    &image="images/s-add.bmp"    &inactive="images/s-add-i.bmp" &toggle=false}
{lib/set-button.i &label="EventDelete" &image="images/s-delete.bmp" &inactive="images/s-delete-i.bmp"}
{lib/set-button.i &label="EventModify" &image="images/s-update.bmp" &inactive="images/s-update-i.bmp"}
setButtons().

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  run enable_UI.
  
  enableButtons(false).

  run getData.

  status default "" in window {&window-name}.
  status input   "" in window {&window-name}.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionModify C-Win 
PROCEDURE actionModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pStateID as character no-undo.
  
  define variable pRegionID as character no-undo.

  empty temp-table tempstate.
  for first state no-lock
      where state.stateID = pStateID:
      
    create tempstate.
    buffer-copy state to tempstate.
  end.

  UPDATE-BLOCK:
  repeat:
    run dialogmodifystate.w (input-output table tempstate, output std-lo).
    if std-lo 
     then leave UPDATE-BLOCK.
    
    publish "ModifyState" (table tempstate, output std-lo).
    if not std-lo 
     then next UPDATE-BLOCK.
     
    for first tempstate no-lock:
     pRegionID = tempstate.region.
    end.
    publish "RegionChanged"(pRegionID).
    
    leave.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE brwData bActivate bEventAdd bEventDelete bEventModify bExport bModify 
         bRefresh brwEvent 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwData:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "states"

  std-ch = "C".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query {&browse-name}.
  close query brwEvent.

  empty temp-table state.
  empty temp-table stateEvent.

  publish "GetStates" (output table state).
  publish "GetStateEvents" (output table stateevent).
  
   /*client-side use only to get region description*/
  for each state no-lock: 
    publish "GetSysCodeDesc"("Region", state.region, output state.regionDesc). 
  end.
  
  open query brwevent for each stateevent.
  open query brwData for each state.

  enableButtons(can-find(first state)).
  apply "value-changed" to brwData in frame {&frame-name}.


end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
  if pHandle:name = "brwData" then
    apply "value-changed" to brwData in frame {&frame-name}.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateDataChanged C-Win 
PROCEDURE StateDataChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run getData in this-procedure.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels          = {&window-name}:width-pixels.
 frame fMain:virtual-width-pixels  = {&window-name}:width-pixels.
 frame fMain:height-pixels         = {&window-name}:height-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

/* fMain components */
 brwData:width-pixels  = frame fmain:width-pixels - 10.
 brwEvent:width-pixels  = frame fmain:width-pixels - 35.
 brwEvent:height-pixels = frame {&frame-name}:height-pixels - brwEvent:y - 5. 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

