&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogobjective.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Sachin Chaturvedi

  Created:05/08/2020
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{tt/objective.i}
{tt/objective.i &tableAlias=ttobjective}


/* Parameters Definitions ---                                           */
define input  parameter iphfileDataSrv  as  handle.
define input  parameter ipcAction       as  character  no-undo.
define input  parameter table         for objective.
define output parameter opiObjectiveID  as  integer    no-undo.
define output parameter oplSuccess      as  logical    no-undo.


/* Local Variable Definitions ---                                       */
define variable cTrackDesc     as character no-undo.
define variable cTrackDue      as datetime      no-undo.
define variable lTrackPrivate  as logical   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS edDesc flDueDt tPrivate Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS edDesc flDueDt tPrivate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 12 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 12 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE edDesc AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 71 BY 3.62 NO-UNDO.

DEFINE VARIABLE flDueDt AS DATETIME FORMAT "99/99/99":U 
     LABEL "Due" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tPrivate AS LOGICAL INITIAL no 
     LABEL "Private" 
     VIEW-AS TOGGLE-BOX
     SIZE 11 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     edDesc AT ROW 1.76 COL 17 NO-LABEL WIDGET-ID 52
     flDueDt AT ROW 5.81 COL 15.2 COLON-ALIGNED WIDGET-ID 42
     tPrivate AT ROW 5.86 COL 77.8 WIDGET-ID 294
     Btn_OK AT ROW 7.91 COL 33
     Btn_Cancel AT ROW 7.91 COL 47.2
     "Objective:" VIEW-AS TEXT
          SIZE 10 BY .62 AT ROW 1.71 COL 6.8 WIDGET-ID 54
     SPACE(74.99) SKIP(7.09)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Objective"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Objective */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  run saveObjective in this-procedure.
  if not oplSuccess 
   then
    return no-apply.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edDesc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edDesc Dialog-Frame
ON VALUE-CHANGED OF edDesc IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flDueDt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flDueDt Dialog-Frame
ON VALUE-CHANGED OF flDueDt IN FRAME Dialog-Frame /* Due */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tPrivate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tPrivate Dialog-Frame
ON VALUE-CHANGED OF tPrivate IN FRAME Dialog-Frame /* Private */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  run displayData in this-procedure.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  do with frame {&frame-name}:
  end.
  
  find first objective no-error.
  if not available objective
   then 
    return.

  assign
    edDesc:screen-value       = objective.description
    flDueDt:screen-value      = string(objective.dueDate)
    tPrivate:checked          = objective.private
    opiObjectiveID            = objective.objectiveID
    .
  
  if ipcAction = "N"
   then
    assign
      frame dialog-frame:title = "New objective"
      Btn_OK :label            = "Create" 
      Btn_OK :tooltip          = "Create"
      .
   else       
     assign
       frame dialog-frame:title = "Edit objective"
       Btn_OK :label            = "Save" 
       Btn_OK :tooltip          = "Save"
       .

  assign
      cTrackDesc       = edDesc:input-value
      cTrackDue        = flDueDt:input-value
      lTrackPrivate    = tPrivate:checked
      .
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if ipcAction = "N"
   then
    Btn_OK:sensitive = (edDesc:input-value <> "") and
                       (flDueDt:input-value <> ?) no-error.
  else
   Btn_OK:sensitive =  (edDesc:input-value <> "") and
                       (flDueDt:input-value <> ?) and
                       (edDesc:input-value  <> cTrackDesc  or
                        flDueDt:input-value <> cTrackDue   or
                        tPrivate:checked <> lTrackPrivate
                        ) no-error
                        .   
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY edDesc flDueDt tPrivate 
      WITH FRAME Dialog-Frame.
  ENABLE edDesc flDueDt tPrivate Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveObjective Dialog-Frame 
PROCEDURE saveObjective :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  empty temp-table ttobjective.  
  for first objective:
    create ttobjective.
    buffer-copy objective to ttobjective.
  end.

  assign
    ttobjective.description = edDesc:input-value
    ttobjective.dueDate     = flDueDt:input-value
    ttobjective.private     = tPrivate:checked
    .

  if ipcAction = "M"
   then
    run modifyObjective in iphFileDataSrv (input table ttobjective,
                                           output oplSuccess).

   else
    run newObjective in iphFileDataSrv (input table ttobjective,
                                        output opiObjectiveID,
                                        output oplSuccess).      

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

