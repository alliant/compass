&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogmodifystate.w

  Description: ModifyState

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gurvindar singh

  Created: 08.22.2018
  @modification
    date         Name           Description
    06/10/2022   SA             Task #93485 - Added new Field "Region" 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{tt/state.i}
{tt/region.i}
{lib/std-def.i}
{lib/winlaunch.i}
{lib/set-button-def.i}

/* Parameters Definitions ---                                           */
define input-output parameter table for state.
define       output parameter pCancel as logical no-undo initial false.

/* Local Variable Definitions ---                                       */
define variable lValidationFailed as logical no-undo.
define variable iseq              as integer no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS Btn_Cancel texternal fLicense AppDate ~
LicEffDate cancelDate LicExpDate burl lastAuditDate fName faddr1 faddr2 ~
faddr-3 faddr-4 fstate fCity fzip cbRegion fcounty fphone fcontact femail ~
website eComments 
&Scoped-Define DISPLAYED-OBJECTS fStateID tActive fDesc texternal fLicense ~
AppDate LicEffDate cancelDate LicExpDate lastAuditDate fName faddr1 faddr2 ~
faddr-3 faddr-4 fstate fCity fzip cbRegion fcounty fphone fcontact femail ~
website eComments 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY  NO-FOCUS
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO  NO-FOCUS
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON burl  NO-FOCUS
     LABEL "url" 
     SIZE 4.6 BY 1.14.

DEFINE VARIABLE cbRegion AS CHARACTER FORMAT "X(256)":U 
     LABEL "Region" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 55.8 BY 1 NO-UNDO.

DEFINE VARIABLE eComments AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 60.6 BY 2.14 NO-UNDO.

DEFINE VARIABLE AppDate AS DATE FORMAT "99/99/99":U 
     LABEL "Application Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE cancelDate AS DATE FORMAT "99/99/99":U 
     LABEL "Cancel Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE faddr-3 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 55.8 BY 1 NO-UNDO.

DEFINE VARIABLE faddr-4 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 55.8 BY 1 NO-UNDO.

DEFINE VARIABLE faddr1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 55.8 BY 1 NO-UNDO.

DEFINE VARIABLE faddr2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 55.8 BY 1 NO-UNDO.

DEFINE VARIABLE fCity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20 BY 1 TOOLTIP "City" NO-UNDO.

DEFINE VARIABLE fcontact AS CHARACTER FORMAT "X(256)":U 
     LABEL "Contact" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14.4 BY 1 NO-UNDO.

DEFINE VARIABLE fcounty AS CHARACTER FORMAT "X(256)":U 
     LABEL "County" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14.6 BY 1 NO-UNDO.

DEFINE VARIABLE fDesc AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE femail AS CHARACTER FORMAT "X(256)":U 
     LABEL "Email" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 32 BY 1.

DEFINE VARIABLE fLicense AS CHARACTER FORMAT "X(256)":U 
     LABEL "License" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 13.8 BY 1 NO-UNDO.

DEFINE VARIABLE fName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 55.8 BY 1 NO-UNDO.

DEFINE VARIABLE fphone AS CHARACTER FORMAT "X(256)":U 
     LABEL "Phone" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE fstate AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14.8 BY 1 NO-UNDO.

DEFINE VARIABLE fStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fzip AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 TOOLTIP "Zip" NO-UNDO.

DEFINE VARIABLE lastAuditDate AS DATE FORMAT "99/99/99":U 
     LABEL "Last Audit Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE LicEffDate AS DATE FORMAT "99/99/99":U 
     LABEL "License Effective Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE LicExpDate AS DATE FORMAT "99/99/99":U 
     LABEL "License Expiry Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE website AS CHARACTER FORMAT "X(256)":U 
     LABEL "WebSite" 
     VIEW-AS FILL-IN 
     SIZE 55.8 BY 1 NO-UNDO.

DEFINE VARIABLE tActive AS LOGICAL INITIAL no 
     LABEL "Active" 
     VIEW-AS TOGGLE-BOX
     SIZE 10 BY .81 NO-UNDO.

DEFINE VARIABLE texternal AS LOGICAL INITIAL no 
     LABEL "External Approval" 
     VIEW-AS TOGGLE-BOX
     SIZE 21.2 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fStateID AT ROW 1.67 COL 19.2 COLON-ALIGNED WIDGET-ID 2
     Btn_OK AT ROW 23.29 COL 27.2 NO-TAB-STOP 
     tActive AT ROW 1.76 COL 37 WIDGET-ID 10
     fDesc AT ROW 2.95 COL 19.2 COLON-ALIGNED WIDGET-ID 12
     Btn_Cancel AT ROW 23.29 COL 44.6 NO-TAB-STOP 
     texternal AT ROW 3.05 COL 54.8 WIDGET-ID 56
     fLicense AT ROW 4.14 COL 19.2 COLON-ALIGNED WIDGET-ID 24
     AppDate AT ROW 5.33 COL 19.2 COLON-ALIGNED WIDGET-ID 14
     LicEffDate AT ROW 5.33 COL 60.8 COLON-ALIGNED WIDGET-ID 20
     cancelDate AT ROW 6.52 COL 19.2 COLON-ALIGNED WIDGET-ID 28
     LicExpDate AT ROW 6.52 COL 60.8 COLON-ALIGNED WIDGET-ID 26
     burl AT ROW 19.43 COL 77.4 WIDGET-ID 22 NO-TAB-STOP 
     lastAuditDate AT ROW 7.71 COL 19.2 COLON-ALIGNED WIDGET-ID 30
     fName AT ROW 8.91 COL 19.2 COLON-ALIGNED WIDGET-ID 32
     faddr1 AT ROW 10.05 COL 19.2 COLON-ALIGNED WIDGET-ID 34
     faddr2 AT ROW 11.19 COL 19.2 COLON-ALIGNED NO-LABEL WIDGET-ID 36
     faddr-3 AT ROW 12.38 COL 19.2 COLON-ALIGNED NO-LABEL WIDGET-ID 38
     faddr-4 AT ROW 13.52 COL 19.2 COLON-ALIGNED NO-LABEL WIDGET-ID 40
     fstate AT ROW 14.71 COL 19.2 COLON-ALIGNED WIDGET-ID 44
     fCity AT ROW 14.71 COL 36.6 COLON-ALIGNED NO-LABEL WIDGET-ID 42
     fzip AT ROW 14.71 COL 59 COLON-ALIGNED NO-LABEL WIDGET-ID 48
     cbRegion AT ROW 15.91 COL 19.2 COLON-ALIGNED WIDGET-ID 64
     fcounty AT ROW 17.1 COL 19.2 COLON-ALIGNED WIDGET-ID 46
     fphone AT ROW 17.1 COL 43 COLON-ALIGNED WIDGET-ID 52
     fcontact AT ROW 18.24 COL 19.2 COLON-ALIGNED WIDGET-ID 50
     femail AT ROW 18.29 COL 43 COLON-ALIGNED WIDGET-ID 54
     website AT ROW 19.48 COL 19.2 COLON-ALIGNED WIDGET-ID 18
     eComments AT ROW 20.76 COL 21.2 NO-LABEL WIDGET-ID 58
     "Comments:" VIEW-AS TEXT
          SIZE 11.2 BY .62 AT ROW 20.81 COL 9.8 WIDGET-ID 60
     SPACE(64.79) SKIP(3.56)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Modify State"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       eComments:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN fDesc IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fStateID IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tActive IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Modify State */
DO:
  pCancel = true.
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME AppDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL AppDate Dialog-Frame
ON VALUE-CHANGED OF AppDate IN FRAME Dialog-Frame /* Application Date */
or value-changed of fLicense
or value-changed of LicEffDate
or value-changed of cancelDate
or value-changed of LicExpDate
or value-changed of lastAuditDate
or value-changed of fName
or value-changed of faddr1
or value-changed of faddr2
or value-changed of faddr-3
or value-changed of faddr-4
or value-changed of fstate
or value-changed of fCity
or value-changed of fzip
or value-changed of fcounty
or value-changed of fphone
or value-changed of fcontact
or value-changed of femail
or value-changed of website
or value-changed of eComments
do:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel Dialog-Frame
ON CHOOSE OF Btn_Cancel IN FRAME Dialog-Frame /* Cancel */
DO:
  pCancel = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  run validationCheck in this-procedure.
  if lValidationFailed
   then return no-apply.

  for first state exclusive-lock:
    assign
      state.stateID          = fStateId:screen-value
      state.active           = logical(tActive:screen-value)                            
      state.description      = fDesc:screen-value                          
      state.license          = fLicense:screen-value                       
      state.appdate          = datetime(appdate:screen-value)                        
      state.licEffDate       = datetime(liceffdate:screen-value)                    
      state.website          = website:screen-value    
      state.seq              = iseq 
      state.externalApproval = texternal:input-value         
      state.licExpDate       = datetime(LicExpDate:screen-value)   
      state.cancelDate       = datetime(cancelDate:screen-value )
      state.lastAuditDate    = datetime(lastAuditDate:screen-value)
      state.name             = fName:screen-value        
      state.addr1            = faddr1:screen-value       
      state.addr2            = faddr2:screen-value       
      state.addr3            = faddr-3:screen-value      
      state.addr4            = faddr-4:screen-value      
      state.state            = fState:screen-value       
      state.city             = fCity:screen-value 
      state.region           = cbRegion:input-value
      state.zip              = fzip:screen-value         
      state.county           = fcounty:screen-value      
      state.contact          = fcontact:screen-value     
      state.phone            = fphone:screen-value       
      state.email            = femail:screen-value       
      state.comments         = ecomments:screen-value
      /*client-side specific*/
      state.regionDesc       = ""
      .
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME burl
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL burl Dialog-Frame
ON CHOOSE OF burl IN FRAME Dialog-Frame /* url */
DO:
  if Website:screen-value ne "" then
    run ShellExecuteA in this-procedure (0,
                                         "open",
                                         Website:screen-value,
                                         "",
                                         "",
                                         1,
                                         output std-in).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRegion
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRegion Dialog-Frame
ON VALUE-CHANGED OF cbRegion IN FRAME Dialog-Frame /* Region */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME texternal
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL texternal Dialog-Frame
ON VALUE-CHANGED OF texternal IN FRAME Dialog-Frame /* External Approval */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

/* Populate "cbRegion" combo-box*/
{lib/get-region-list.i &combo=cbRegion}

{lib/set-button.i &label="Url" &image="images/s-url.bmp" &inactive="images/s-url-i.bmp"}
setButtons().

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  run getData.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  btn_ok:sensitive = not(fLicense:screen-value                        = state.license                     and           
                         appdate:input-value                          = state.appdate                     and
                         liceffdate:input-value                       = state.licEffDate                  and 
                         website:screen-value                         = state.website                     and 
                         texternal:checked                            = logical(state.externalApproval)   and 
                         LicExpDate:input-value                       = state.licExpDate                  and 
                         cancelDate:input-value                       = state.cancelDate                  and 
                         lastAuditDate:input-value                    = state.lastAuditDate               and 
                         fName:screen-value                           = state.name                        and 
                         faddr1:screen-value                          = state.addr1                       and 
                         faddr2:screen-value                          = state.addr2                       and 
                         faddr-3:screen-value                         = state.addr3                       and 
                         faddr-4:screen-value                         = state.addr4                       and 
                         fState:screen-value                          = state.state                       and 
                         fCity:screen-value                           = state.city                        and 
                         fzip:screen-value                            = state.zip                         and
                         cbRegion:screen-value                        = state.region                      and
                         fcounty:screen-value                         = state.county                      and 
                         fcontact:screen-value                        = state.contact                     and 
                         fphone:screen-value                          = state.phone                       and 
                         femail:screen-value                          = state.email                       and 
                         ecomments:screen-value                       = state.comments) no-error.                   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fStateID tActive fDesc texternal fLicense AppDate LicEffDate 
          cancelDate LicExpDate lastAuditDate fName faddr1 faddr2 faddr-3 
          faddr-4 fstate fCity fzip cbRegion fcounty fphone fcontact femail 
          website eComments 
      WITH FRAME Dialog-Frame.
  ENABLE Btn_Cancel texternal fLicense AppDate LicEffDate cancelDate LicExpDate 
         burl lastAuditDate fName faddr1 faddr2 faddr-3 faddr-4 fstate fCity 
         fzip cbRegion fcounty fphone fcontact femail website eComments 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData Dialog-Frame 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  for first state no-lock:
    assign
      fStateId:screen-value in frame {&frame-name} = state.stateID
      tActive:checked                              = logical(state.active)
      fDesc:screen-value                           = state.description
      fLicense:screen-value                        = state.license
      appdate:screen-value                         = string(state.appdate)
      liceffdate:screen-value                      = string(state.licEffDate)
      website:screen-value                         = string(state.website)
      iseq                                         = integer (state.seq)
      texternal:checked                            = logical(state.externalApproval)
      LicExpDate:screen-value                      = string (state.licExpDate)
      cancelDate:screen-value                      = string (state.cancelDate)
      lastAuditDate:screen-value                   = string (state.lastAuditDate)
      fName:screen-value                           = state.name
      faddr1:screen-value                          = state.addr1
      faddr2:screen-value                          = state.addr2
      faddr-3:screen-value                         = state.addr3
      faddr-4:screen-value                         = state.addr4
      fState:screen-value                          = state.state
      cbRegion:screen-value                        = state.region
      fCity:screen-value                           = state.city
      fzip:screen-value                            = state.zip
      fcounty:screen-value                         = state.county
      fcontact:screen-value                        = state.contact
      fphone:screen-value                          = state.phone
      femail:screen-value                          = state.email
      ecomments:screen-value                       = state.comments
      .
end.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validationCheck Dialog-Frame 
PROCEDURE validationCheck :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
 if LicEffDate:input-value > LicExpDate:input-value
  then
   do:
     message "Licence Effective Date should be less than License Expiry Date."
        view-as alert-box info buttons ok.
     apply "entry" to LicEffDate.
     lValidationFailed = yes.
   end.
  else 
   lValidationFailed = no.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

