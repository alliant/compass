&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wattorneys.w

  Description:Attorney Maintainance

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Shubham

  Created: 12.04.2019
  Modification:
  Date            Name               Description
  03/17/2022      Shefali            Task# 86699 Manage Attorney Firm from the same screen
  04/26/2022      Shefali            Task #93684 Modified to display attorney name with attorneyID in bracket
                                     in status change dialog.

------------------------------------------------------------------------*/
create widget-pool.

{tt/attorney.i &tableAlias="ttAttorney"}

{tt/person.i}
{tt/person.i &tableAlias="tPerson"}
{tt/state.i}

define variable cStateId          as character  no-undo.
define variable dColumnWidth      as decimal    no-undo.
define variable cAttorneyID       as character  no-undo.
define variable cAttorneyStatus   as character  no-undo. 

/* track status change to set status in filter*/
define variable cStatusDateTime  as logical   no-undo. 
define temp-table attorney like ttAttorney
 fields entity     as character
 fields entityID   as character
 fields entityName as character.
 
define temp-table tattorney like attorney.

{lib/std-def.i}
{lib/com-def.i}

{lib/get-column.i}
{lib/winshowscrollbars.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES attorney

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData attorney.stateID attorney.attorneyID attorney.entityname attorney.entityID getStatDesc(attorney.stat) @ attorney.stat attorney.city attorney.state attorney.zip   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData preselect each attorney
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} preselect each attorney.
&Scoped-define TABLES-IN-QUERY-brwData attorney
&Scoped-define FIRST-TABLE-IN-QUERY-brwData attorney


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bEdit RECT-61 RECT-62 attorneyType cbState ~
fSearch brwData bSearch bStatusChange bRefresh bExport bNew bdelete 
&Scoped-Define DISPLAYED-OBJECTS attorneyType cbState fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD attorneyStat C-Win 
FUNCTION attorneyStat RETURNS CHARACTER
  ( input cStat as character /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getStatDesc C-Win 
FUNCTION getStatDesc RETURNS CHARACTER
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bdelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete attorney".

DEFINE BUTTON bEdit  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify attorney".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "New attorney".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search".

DEFINE BUTTON bStatusChange  NO-FOCUS
     LABEL "Status Change" 
     SIZE 7.2 BY 1.71 TOOLTIP "Change Status".

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 22.6 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 32.4 BY 1 NO-UNDO.

DEFINE VARIABLE attorneyType AS CHARACTER INITIAL "B" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Organization", "O":U,
          "Person", "P":U,
          "Both", "B":U
     SIZE 36.8 BY 1.38 NO-UNDO.

DEFINE RECTANGLE RECT-61
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 43.8 BY 2.14.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 117.6 BY 2.14.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      attorney SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      attorney.stateID    format "x(5)" column-label "State ID"  width 11
attorney.attorneyID format "x(15)"       label "Attorney ID"
attorney.entityname format "x(50)"       label "Name"  width 51
attorney.entityID   format "x(10)"       label "Org ID/Person ID"    width 20
getStatDesc(attorney.stat)
 @ attorney.stat    format "x(15)"       label "Status"
attorney.city       format "x(40)"       label "City"         width 18
attorney.state      format "x(8)"        label "State"        width 10
attorney.zip        format "x(12)"       label "Zip Code"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 161 BY 13.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bEdit AT ROW 1.62 COL 141 WIDGET-ID 8 NO-TAB-STOP 
     attorneyType AT ROW 1.81 COL 3.2 NO-LABEL WIDGET-ID 358
     cbState AT ROW 2 COL 45.4 COLON-ALIGNED WIDGET-ID 48
     fSearch AT ROW 2 COL 77 COLON-ALIGNED WIDGET-ID 62
     brwData AT ROW 3.86 COL 2 WIDGET-ID 200
     bSearch AT ROW 1.62 COL 111.8 WIDGET-ID 60 NO-TAB-STOP 
     bStatusChange AT ROW 1.62 COL 155 WIDGET-ID 64 NO-TAB-STOP 
     bRefresh AT ROW 1.62 COL 120 WIDGET-ID 4 NO-TAB-STOP 
     bExport AT ROW 1.62 COL 127 WIDGET-ID 2 NO-TAB-STOP 
     bNew AT ROW 1.62 COL 134 WIDGET-ID 6 NO-TAB-STOP 
     bdelete AT ROW 1.62 COL 148 WIDGET-ID 10 NO-TAB-STOP 
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1 COL 120 WIDGET-ID 52
     "Filter" VIEW-AS TEXT
          SIZE 5.2 BY .62 AT ROW 1.14 COL 2.8 WIDGET-ID 56
     RECT-61 AT ROW 1.43 COL 119.4 WIDGET-ID 50
     RECT-62 AT ROW 1.43 COL 2 WIDGET-ID 54
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 163.4 BY 17.1
         DEFAULT-BUTTON bSearch WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Attorney Management"
         HEIGHT             = 16.91
         WIDTH              = 162.8
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData fSearch fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} preselect each attorney.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Attorney Management */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Attorney Management */
do:
  /* This event will close the window and terminate the procedure.  */
  apply "CLOSE":U to this-procedure.
  return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Attorney Management */
do:
  run windowResized in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME attorneyType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL attorneyType C-Win
ON VALUE-CHANGED OF attorneyType IN FRAME fMain
DO:
  if attorneyType:input-value = "B" then
  do:
    assign bNew:sensitive  = false
           bEdit:sensitive = false.
  end.
  else 
  do:
    assign bNew:sensitive  = true
           bEdit:sensitive = true.
  end.
      
  run filterData in this-procedure.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bdelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bdelete C-Win
ON CHOOSE OF bdelete IN FRAME fMain /* Delete */
do:
  run actionDelete in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEdit C-Win
ON CHOOSE OF bEdit IN FRAME fMain /* Edit */
do:
  run actionModify in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
do:
  run actionNew in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
do:
  run refreshData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
do:
  if attorneyType:input-value ne "B" 
   then
    run actionModify in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
do:
  {lib/brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME fMain /* Search */
do:
  run filterData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bStatusChange
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bStatusChange C-Win
ON CHOOSE OF bStatusChange IN FRAME fMain /* Status Change */
do: 
  define variable cToStatus as character  no-undo.
    
  run getChangedStatus in this-procedure (output cToStatus,
                                          output std-lo). 
                                          
  if not std-lo
   then
    return no-apply.

  run setAttorneyStatus in this-procedure (input cToStatus).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState C-Win
ON VALUE-CHANGED OF cbState IN FRAME fMain /* State */
do:
  resultsChanged(false).
  run filterData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON ENTRY OF fSearch IN FRAME fMain /* Search */
do:
  if self:input-value <> "" 
   then
    run filterData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON VALUE-CHANGED OF fSearch IN FRAME fMain /* Search */
do:
  resultsChanged(false).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i }

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

on close of this-procedure 
  run disable_UI.

pause 0 before-hide.

bExport :load-image("images/excel.bmp").
bExport :load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/sync.bmp").
bRefresh:load-image-insensitive("images/sync-i.bmp").
bnew    :load-image("images/add.bmp").
bnew    :load-image-insensitive("images/add-i.bmp").
bEdit   :load-image("images/update.bmp").
bEdit   :load-image-insensitive("images/update-i.bmp").
bDelete :load-image("images/delete.bmp").
bDelete :load-image-insensitive("images/delete-i.bmp").
bSearch :load-image("images/magnifier.bmp").
bStatusChange:load-image("images/swap.bmp").
bStatusChange:load-image-insensitive("images/swap-i.bmp").

subscribe to "AttorneyDataChanged"     anywhere.
subscribe to "DisplayChangedAttorney"  anywhere.

{lib/get-state-list.i &combo=cbState &useFilter=false}

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

  run getData in this-procedure.
  
  run enable_UI.
  
  cbState:add-first("ALL","ALL").
    assign 
        cbState:screen-value = "ALL".
        
  if attorneyType:input-value = "B" then
    assign bNew:sensitive  = false
           bEdit:sensitive = false.
  else 
    assign bNew:sensitive  = true
           bEdit:sensitive = true.
    
  {lib/get-column-width.i &col="'Name'" &var=dColumnWidth}

  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.

  {&window-name}:visible = true.

  if not this-procedure:persistent 
   then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionDelete C-Win 
PROCEDURE actionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lChoice as logical no-undo.
  
  if not available attorney 
   then
    return.
 
  lchoice = false.

  message "Highlighted Attorney will be deleted." skip "Do you want to continue?"
      view-as alert-box question buttons yes-no title "Delete Attorney" update lChoice.
 
  if not lChoice 
   then
    return.

  cAttorneyID = attorney.attorneyID.
 
  run server/deleteattorney.p (input cAttorneyID,
                               output std-lo,
                               output std-ch).

  if not std-lo 
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.

  publish "refreshAttorneys" (output std-lo,
                               output std-ch).
                               
  run getData in this-procedure. 

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionModify C-Win 
PROCEDURE actionModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount  as integer no-undo.
  
  if not available attorney 
   then
    return.
 
  do with frame {&frame-name}:
  end.
 
  do iCount = 1 to brwdata:num-iterations:
    if brwdata:is-row-selected(iCount) 
     then  
      leave.
  end.

  empty temp-table ttattorney.
  buffer-copy attorney to ttattorney.
  
  if attorneyType:input-value = {&OrganizationCode}  
   then
    run dialogattorneyfirm.w (input table ttattorney,
                              input {&ActionEdit},
                              input ttattorney.attorneyID,
                              input string(ttattorney.orgID),                     
                              output cAttorneyID,                          
                              output std-lo ).
   else if attorneyType:input-value = {&PersonCode}
    then
     run dialogattorney.w (input table ttattorney,
                           input {&ActionEdit},
                           input ttattorney.attorneyID,
                           input string(ttattorney.personID),                     
                           output cAttorneyID,                          
                           output std-lo ).
  
  if not std-lo 
   then
    return.
 
  /* Update the data from the data model. */
  empty temp-table tAttorney.
  publish "getAttorney" (output table tAttorney).
  
  run filterData in this-procedure.
                
  for first attorney 
    where attorney.attorneyID = cAttorneyId:
    std-ro = rowid(attorney).
  end.
       
  brwdata:set-repositioned-row(iCount,"ALWAYS").
  if std-ro <> ? then
   reposition brwdata to rowid std-ro.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionNew C-Win 
PROCEDURE actionNew :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable iCount  as integer no-undo.

  empty temp-table ttattorney.
  if attorneyType:input-value = {&OrganizationCode}
   then
     run dialogattorneyfirm.w(input table ttattorney,
                              input {&ActionAdd},
                              input "",
                              input "",
                              output cAttorneyId,
                              output std-lo).
   else 
    run dialogattorney.w(input table ttattorney,
                         input {&ActionAdd},
                         input "",
                         input "",
                         output cAttorneyId,
                         output std-lo).
                       
  if not std-lo  
   then
    return.
          
  /* Update the data from the data model. */
  
  publish "getAttorney" (output table tAttorney).

  run filterData in this-procedure.

  find first attorney 
    where attorney.attorneyID = cAttorneyId 
    no-error.

  if available attorney and (cbState:input-value = attorney.stateID or cbState:input-value = "ALL" )
   then
    std-ro = rowid(attorney).
  else 
   std-ro = ?.
                                                     
  if std-ro <> ? 
   then
    do:
      do iCount = 1 TO brwdata:num-iterations:
        if brwdata:is-row-selected(iCount)
         then
          leave.
      end.   
      brwdata:set-repositioned-row(iCount,"ALWAYS").
      reposition brwdata TO rowid std-ro.
      brwdata:get-repositioned-row().

      message "New Attorney is created with ID: " + cAttorneyId
        view-as alert-box info buttons ok.       
    end.   
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AttorneyDataChanged C-Win 
PROCEDURE AttorneyDataChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run refreshData in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DisplayChangedAttorney C-Win 
PROCEDURE DisplayChangedAttorney :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  publish "getAttorney" (output table tAttorney).
  run filterData in this-procedure.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY attorneyType cbState fSearch 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bEdit RECT-61 RECT-62 attorneyType cbState fSearch brwData bSearch 
         bStatusChange bRefresh bExport bNew bdelete 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable htableHandle as handle no-undo.
  
  empty temp-table tAttorney.
 
  for each attorney where attorney.stateID = (if cbState:input-value in frame {&frame-name} = "ALL"
                                               then
                                                attorney.stateID 
                                              else 
                                               cbState:input-value):
    create tAttorney.
    buffer-copy attorney to tAttorney.
    assign 
        tattorney.stat   =  getStatDesc(tattorney.stat)
        tattorney.entity = if tattorney.entity = {&OrganizationCode} then {&Organization} else if tattorney.entity = {&PersonCode} then {&Person} else ""
        .
    
  end.
  
  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
         view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
 
  htableHandle = temp-table tAttorney:handle.
  run util/exporttable.p (table-handle htableHandle,
                          "tAttorney",
                          "for each tAttorney ",
                          "stateID,attorneyID,entityname,entity,entityID,stat,activeDate,contact,addr1,addr2,city,state,zip,phone,email,website,comments,createDate,createdBy,contractDate,contractID",
                          "State ID,Attorney ID,Name,Entity,Org ID/Person ID,Status,Activation Date,contact,Address 1,Address 2,City,State,Zip Code,Phone,Email,Website,Notes,Create Date, Created By,Contract Date,Contract ID",
                          std-ch,
                          "Attorney.csv-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).   
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  close query brwData.
  
  empty temp-table attorney.
  
  for each tAttorney:
   if attorneyType:input-value = "B" then
   do:
     if not tAttorney.stateID = (if cbState:input-value = "All" then tAttorney.stateID else cbState:input-value) or
      ( fsearch:input-value <> "" and 
       not ((string(tAttorney.attorneyID) matches "*" + string(fSearch:input-value) + "*") or 
            (tAttorney.personID   matches "*" + fSearch:input-value + "*") or 
            (tAttorney.orgID      matches "*" + fSearch:input-value + "*") or
            (tAttorney.name1      matches "*" + fSearch:input-value + "*") or 
            (tAttorney.firmName   matches "*" + fSearch:input-value + "*") or 
            (tAttorney.addr1      matches "*" + fSearch:input-value + "*") or   
            (tAttorney.addr2      matches "*" + fSearch:input-value + "*") or 
            (tAttorney.city       matches "*" + fSearch:input-value + "*") or 
            (tAttorney.email      matches "*" + fSearch:input-value + "*") or 
            (tAttorney.stat       matches "*" + fSearch:input-value + "*")))
     then 
      next.  
   end.
   else if attorneyType:input-value = {&OrganizationCode} then
   do:
     if not tAttorney.stateID = (if cbState:input-value = "All" then tAttorney.stateID else cbState:input-value) or
      (tAttorney.personID ne "" and tAttorney.personID ne ?) or
      ( fsearch:input-value <> "" and 
       not ((string(tAttorney.attorneyID) matches "*" + string(fSearch:input-value) + "*") or 
            (tAttorney.orgID      matches "*" + fSearch:input-value + "*") or
            (tAttorney.name1      matches "*" + fSearch:input-value + "*") or
            (tAttorney.firmName   matches "*" + fSearch:input-value + "*") or 
            (tAttorney.addr1      matches "*" + fSearch:input-value + "*") or   
            (tAttorney.addr2      matches "*" + fSearch:input-value + "*") or 
            (tAttorney.city       matches "*" + fSearch:input-value + "*") or 
            (tAttorney.email      matches "*" + fSearch:input-value + "*") or 
            (tAttorney.stat       matches "*" + fSearch:input-value + "*")))
     then 
      next.  
   end.
   else if attorneyType:input-value = {&PersonCode} then
   do:
     if not tAttorney.stateID = (if cbState:input-value = "All" then tAttorney.stateID else cbState:input-value) or
      (tAttorney.orgID ne "" and tAttorney.orgID ne ?) or
      ( fsearch:input-value <> "" and 
       not ((string(tAttorney.attorneyID) matches "*" + string(fSearch:input-value) + "*") or 
            (tAttorney.personID   matches "*" + fSearch:input-value + "*") or 
            (tAttorney.name1      matches "*" + fSearch:input-value + "*") or
            (tAttorney.firmName   matches "*" + fSearch:input-value + "*") or
            (tAttorney.addr1      matches "*" + fSearch:input-value + "*") or   
            (tAttorney.addr2      matches "*" + fSearch:input-value + "*") or 
            (tAttorney.city       matches "*" + fSearch:input-value + "*") or 
            (tAttorney.email      matches "*" + fSearch:input-value + "*") or 
            (tAttorney.stat       matches "*" + fSearch:input-value + "*")))
     then 
      next.  
   end.

    create attorney.
    buffer-copy tAttorney to attorney.
  end.
  
  open query brwData preselect each attorney.
  
  /* Display the record count after applying the filter. */
  setStatusCount(query brwData:num-results).  
  run setButtons in this-procedure. 

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getChangedStatus C-Win 
PROCEDURE getChangedStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter ocToStatus      as character no-undo.
  define output parameter olStatusChange  as logical   no-undo.

  do with frame {&frame-name}:
  end.
  
  if available attorney 
   then
    do:
      assign 
            std-ch = attorney.entityname + "(" + attorney.attorneyID + ")"
            cAttorneyStatus = attorney.stat
            .
    end. 
    
  run dialogchangestatus.w (input  std-ch,
                            input  cAttorneyStatus,
                            input  {&Attorney},
                            output ocToStatus,
                            output olStatusChange).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table tAttorney.
  
  do with frame {&Frame-name}:
  end.
  
  publish "getAttorneys" (output table tAttorney).
      
  run filterData in this-procedure.
  
  /* Display no. of records with date and time on status bar */
   
  setStatusRecords(query brwData:num-results).
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData C-Win 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  publish "refreshAttorneys"(output std-lo,
                             output std-ch).
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
 
  run getData in this-procedure.
end  procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setAttorneyStatus C-Win 
PROCEDURE setAttorneyStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pStat as character no-undo.
  
  define variable lSuccess    as logical   no-undo.
  define variable cStatus     as character no-undo.
  define variable cAttorneyId as character no-undo.
  define variable iCount      as integer   no-undo.

  cAttorneyId = attorney.attorneyId.
  
  do with frame {&frame-name}:
  end.

  do iCount = 1 to brwdata:num-iterations :
    if brwdata:is-row-selected(iCount) 
     then  
      leave.
  end.

  publish "setAttorneyStatus" (attorney.attorneyId , pStat , output lSuccess).

  if lSuccess
   then
    do:
      if attorney.entityID ne "" 
       then
        publish "roleComplianceModified" (input attorney.entityID).
        
      if cAttorneyStatus eq {&ActiveStat} and pStat ne {&ActiveStat} 
       then
        message "The status of the attorney is changed from Active to " +  attorneyStat(pStat) + ". Compliance will not be tracked."
          view-as alert-box information buttons ok.

      else if cAttorneyStatus ne {&ActiveStat} and pStat = {&ActiveStat} 
       then
        message "The status of the attorney is changed from " + attorneyStat(cAttorneyStatus) + " to Active. Compliance will be tracked."
          view-as alert-box information buttons ok.
         
      cAttorneyStatus = pStat.                
      
      publish "getAttorney" (output table tAttorney).

      run filterData in this-procedure.
      
      for first attorney 
        where attorney.attorneyID = cAttorneyId:
        std-ro = rowid(attorney).
      end.
       
      brwdata:set-repositioned-row(iCount,"ALWAYS").
      reposition brwdata to rowid std-ro.
      
      run ShowWindow in this-procedure.
    end.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setButtons C-Win 
PROCEDURE setButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if query brwData:num-results > 0 
   then
    assign 
        bExport       :sensitive  = true
        bEdit         :sensitive  = if attorneyType:input-value = "B" then false else true
        bDelete       :sensitive  = true
        bStatusChange :sensitive  = true 
        .
   else
    assign
        bExport        :sensitive = false
        bEdit          :sensitive = false
        bDelete        :sensitive = false
        bStatusChange  :sensitive = false
        .

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}                                                          
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  assign 
      frame fMain:width-pixels          = {&window-name}:width-pixels
      frame fMain:virtual-width-pixels  = {&window-name}:width-pixels
      frame fMain:height-pixels         = {&window-name}:height-pixels
      frame fMain:virtual-height-pixels = {&window-name}:height-pixel
      /* fMain Components */
      brwData:width-pixels              = frame fmain:width-pixels - 12
      
      brwData:height-pixels             = frame fmain:height-pixels - brwData:y - 4
      .
  {lib/resize-column.i &col="'Name'" &var=dColumnWidth}
  run ShowScrollBars(browse brwData:handle, no, yes).
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION attorneyStat C-Win 
FUNCTION attorneyStat RETURNS CHARACTER
  ( input cStat as character /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable pReturn as character no-undo.
  
  publish "GetSysPropDesc" ("COM", "Attorney", "Status", cStat, output pReturn).
  return pReturn.
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getStatDesc C-Win 
FUNCTION getStatDesc RETURNS CHARACTER
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cStat = {&ActiveStat} 
   then  
    cStat = {&Active}.
  else if cStat = {&ClosedStat}
   then  
    cStat = {&Closed}.
  else if cStat = {&ProspectStat}
   then  
    cStat = {&Prospect}.
  else if cStat = {&WithdrawnStat}
   then  
    cStat = {&Withdrawn}.
  else if cStat = {&CancelledStat}
   then  
    cStat = {&Cancelled}.
  else if cStat = {&InactiveStat}
   then  
    cStat = {&Inactive}.  
  return cStat.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 setStatusMessage({&ResultNotMatch}).
 cStatusDateTime = false.
 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

