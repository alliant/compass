&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI ADM2
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWin
{adecomm/appserv.i}
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/*------------------------------------------------------------------------

  File: worganizationdetail.w

  Description: Show Organization and its linking with others.

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Shubham

  Created:09.23.2019 
  
  Modified:
  Date          Name         Description
  03/17/2022   Shefali       Task# 86699 Implement copy feature, and Cancelled 
                             the attorney while deletion of attorney firm orgrole
  03/25/2022   Sachin C      Task #92363 Bug Fix edited name displaying 
                             in the fill-in(fname).   
  04/26/2022    Shefali      Task #93684 Modified to refresh the fulfillment data properly.   
  28/07/2022    Vignesh R    Modified to support clob field changes
  10/28/2022    SA        Task #96812 Modified to add logic according to personagent.
  11/08/2022    AG          Task #96812 Modified to add person level document feature
  03/08/2023    S Chandu    Task #102073 Added Renew Qualification changes.
  06/26/2023    SB          Task #104008 Modified to implement tags for person
  07/21/2023    SK          Add Agents tab in the person detail screen and make it default from the AMD APP.
  02/06/2024    SB          Task 110389 Disable the edit and delete role button when there is no record in roles grid.
  04/04/2024    SB          Task# 111689 Modified to remove the reference of title
  04/04/2024    SR          Task #111689 Removed the reference of person.phone,person.email and person.mobile.
  04/11/2024    SRK         Task #111689 Personcontact refresh after all actions.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AB.              */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* Temp-table Definitions ---                                           */
{tt/reqfulfill.i}
{tt/reqfulfill.i &tableAlias="tReqfulfill"}

{tt/qualification.i}
{tt/qualification.i &tableAlias="tqualification"}

{tt/affiliation.i &tableAlias="taffiliation"}

{tt/personrole.i &tableAlias="tpersonRole"}

{tt/person.i} 
{tt/person.i &tableAlias="tperson"} 
{tt/personcontact.i} 
{tt/personcontact.i &tableAlias="tpersoncontact"} 
{tt/personcontact.i &tableAlias="ttpersoncontact"}
{tt/personcontact.i &tableAlias="texppersoncontact"}

{tt/personagent.i} 
{tt/personagent.i &tableAlias="tpersonagent"} 
{tt/imageinfo.i}

{tt/state.i}

{tt/tag.i}
{tt/tag.i &tableAlias=temptag}
{tt/tag.i &tableAlias=ttag}

{tt/personagentdetails.i}
{tt/personagentdetails.i &tableAlias="ttpersonagentdetails"}
{tt/personagentdetails.i &tableAlias="tpersonagentdetails"}

/* We need sequence field for sorting the compliance status column. 
   R,G,Y can not be sorted using char sorting. */

define temp-table affiliation    no-undo like taffiliation
  field sequence as character.

define temp-table personRole     no-undo like tPersonRole
  field sequence as character
  field totfulfillreq as integer.

/* Parameters definitions ---    */

define input parameter ipcPersonId as character no-undo. 
define input parameter ipiPageNo   as integer   no-undo.
define input parameter ipcEntityID as character no-undo.

{src/adm2/widgetprto.i}

{lib/winlaunch.i}
{lib/std-def.i}
{lib/winshowscrollbars.i}
{lib/com-def.i}
{lib/brw-multi-def.i}

/* Local Variable Definitions ---      */
 
define variable hPersonDataSrv    as handle    no-undo.
define variable hComLog           as handle    no-undo.
define variable hDocument         as handle    no-undo.
define variable IMAGE-2           as handle    no-undo.
define variable chStateIDList     as character no-undo.
define variable cFirstName        as character no-undo.
define variable cMiddleName       as character no-undo.
define variable cLastName         as character no-undo.
define variable procname          as character no-undo.
define variable chimage           as longchar  no-undo.
define variable chPath            as character no-undo.
define variable cstat             as character no-undo.
define variable decdmptr          as memptr    no-undo.
define variable lImage            as longchar  no-undo.
define variable lPictureChange    as logical   no-undo.
define variable outimgloc         as character no-undo.
define variable dAffiliation      as decimal   no-undo.
define variable dRole             as decimal   no-undo.
define variable dColumnWidth      as decimal   no-undo.
define variable dColumnWidthq     as decimal   no-undo.
define variable dColumnWidthc     as decimal   no-undo. 
define variable dColumnWidthu     as decimal   no-undo.
define variable inooffulfillreq   as integer   no-undo.
define variable cDisplayName      as character no-undo.
define variable hComAgents        as handle    no-undo.
define variable iTagID            as integer   no-undo.
define variable cCurrUser         as character no-undo.
define variable cCurrentUID       as character no-undo.
define variable cAgentID          as character no-undo.

/* Create data model for agent Detail */ 
run persondatasrv.p persistent set hPersonDataSrv (input ipcPersonId).

publish "GetCredentialsName" (output cCurrUser).
publish "GetCredentialsID"   (output cCurrentUID).

run getData in this-procedure.

if not can-find(first person where person.personId = ipcPersonID) 
 then
  do:
    message "The Person with ID: " + ipcPersonID + " does not exist."  
      view-as alert-box error buttons ok.
     
    delete procedure hPersonDataSrv.
    delete procedure this-procedure.
    return.
  end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

&Scoped-define ADM-SUPPORTED-LINKS Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwAffiliations

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES affiliation personagentdetails reqfulfill ~
personContact personRole Qualification tag

/* Definitions for BROWSE brwAffiliations                               */
&Scoped-define FIELDS-IN-QUERY-brwAffiliations affiliation.isCtrlPerson getEntityName(affiliation.partnerA, affiliation.partnerAType, affiliation.partnerAName, affiliation.partnerBName) @ affiliation.partnerAName affiliation.jobTitle getEntityID(affiliation.partnerAType, affiliation.partnerA, affiliation.partnerB) @ affiliation.partnerA getEntity(affiliation.partnerAType) @ affiliation.partnerAType   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAffiliations   
&Scoped-define SELF-NAME brwAffiliations
&Scoped-define QUERY-STRING-brwAffiliations for each affiliation by affiliation.partnerAName
&Scoped-define OPEN-QUERY-brwAffiliations open query {&SELF-NAME} for each affiliation by affiliation.partnerAName.
&Scoped-define TABLES-IN-QUERY-brwAffiliations affiliation
&Scoped-define FIRST-TABLE-IN-QUERY-brwAffiliations affiliation


/* Definitions for BROWSE brwAgents                                     */
&Scoped-define FIELDS-IN-QUERY-brwAgents personagentdetails.Agent personagentdetails.statedesc personagentdetails.statdesc personagentdetails.jobTitle   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAgents   
&Scoped-define SELF-NAME brwAgents
&Scoped-define QUERY-STRING-brwAgents for each personagentdetails where personagentdetails.stat = "A"  by personagentdetails.stat desc
&Scoped-define OPEN-QUERY-brwAgents open query brwAgents for each personagentdetails where personagentdetails.stat = "A"  by personagentdetails.stat desc .
&Scoped-define TABLES-IN-QUERY-brwAgents personagentdetails
&Scoped-define FIRST-TABLE-IN-QUERY-brwAgents personagentdetails


/* Definitions for BROWSE brwFulfillment                                */
&Scoped-define FIELDS-IN-QUERY-brwFulfillment reqfulfill.reqMet reqfulfill.reqdesc "Requirement" getQualificationDesc(reqfulfill.stat) @ reqfulfill.stat "Status"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFulfillment   
&Scoped-define SELF-NAME brwFulfillment
&Scoped-define QUERY-STRING-brwFulfillment for each reqfulfill
&Scoped-define OPEN-QUERY-brwFulfillment open query {&SELF-NAME} for each reqfulfill.
&Scoped-define TABLES-IN-QUERY-brwFulfillment reqfulfill
&Scoped-define FIRST-TABLE-IN-QUERY-brwFulfillment reqfulfill


/* Definitions for BROWSE brwpersoncontact                              */
&Scoped-define FIELDS-IN-QUERY-brwpersoncontact personContact.active getContactType(personContact.contactType) @ personContact.contactType personContact.contactSubType personContact.contactID getPersonName(personContact.uid) @ personContact.uid personContact.createDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwpersoncontact   
&Scoped-define SELF-NAME brwpersoncontact
&Scoped-define QUERY-STRING-brwpersoncontact for each personContact by personContact.active desc
&Scoped-define OPEN-QUERY-brwpersoncontact open query {&SELF-NAME} for each personContact by personContact.active desc .
&Scoped-define TABLES-IN-QUERY-brwpersoncontact personContact
&Scoped-define FIRST-TABLE-IN-QUERY-brwpersoncontact personContact


/* Definitions for BROWSE brwPersonRoles                                */
&Scoped-define FIELDS-IN-QUERY-brwPersonRoles personRole.comstat personRole.stateID personRole.role personRole.roleID getRoleStatus(personrole.active) @ personrole.active personRole.totfulfillreq   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwPersonRoles   
&Scoped-define SELF-NAME brwPersonRoles
&Scoped-define QUERY-STRING-brwPersonRoles for each personRole
&Scoped-define OPEN-QUERY-brwPersonRoles open query {&SELF-NAME} for each personRole.
&Scoped-define TABLES-IN-QUERY-brwPersonRoles personRole
&Scoped-define FIRST-TABLE-IN-QUERY-brwPersonRoles personRole


/* Definitions for BROWSE brwQualifications                             */
&Scoped-define FIELDS-IN-QUERY-brwQualifications qualification.activ qualification.stateID qualification.Qualification qualification.QualificationNumber getQualificationDesc(qualification.stat) @ qualification.stat qualification.effectiveDate qualification.expirationDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQualifications   
&Scoped-define SELF-NAME brwQualifications
&Scoped-define QUERY-STRING-brwQualifications for each Qualification where Qualification.entityId = ipcPersonId by Qualification.QualificationID
&Scoped-define OPEN-QUERY-brwQualifications open query {&SELF-NAME} for each Qualification where Qualification.entityId = ipcPersonId by Qualification.QualificationID.
&Scoped-define TABLES-IN-QUERY-brwQualifications Qualification
&Scoped-define FIRST-TABLE-IN-QUERY-brwQualifications Qualification


/* Definitions for BROWSE brwTags                                       */
&Scoped-define FIELDS-IN-QUERY-brwTags tag.name tag.createddate tag.userName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwTags   
&Scoped-define SELF-NAME brwTags
&Scoped-define QUERY-STRING-brwTags FOR EACH tag
&Scoped-define OPEN-QUERY-brwTags OPEN QUERY {&SELF-NAME} FOR EACH tag.
&Scoped-define TABLES-IN-QUERY-brwTags tag
&Scoped-define FIRST-TABLE-IN-QUERY-brwTags tag


/* Definitions for FRAME FRAME-C                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FRAME-C ~
    ~{&OPEN-QUERY-brwAffiliations}

/* Definitions for FRAME FRAME-D                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FRAME-D ~
    ~{&OPEN-QUERY-brwQualifications}

/* Definitions for FRAME FRAME-E                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FRAME-E ~
    ~{&OPEN-QUERY-brwFulfillment}~
    ~{&OPEN-QUERY-brwPersonRoles}

/* Definitions for FRAME FRAME-G                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FRAME-G ~
    ~{&OPEN-QUERY-brwpersoncontact}

/* Definitions for FRAME FRAME-H                                        */

/* Definitions for FRAME FRAME-I                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FRAME-I ~
    ~{&OPEN-QUERY-brwAgents}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bPersonDocuments bAddImg bDeleteImg fname ~
fAddr fAddr2 fCity cbState fZip tgldnc tglEmp cbNPN cbAltaUid bEditname ~
bContSave edContNotes bPersonDelete bRefresh bViewLog bContCanc RECT-64 
&Scoped-Define DISPLAYED-OBJECTS fentityId fname fAddr fAddr2 fCity cbState ~
fZip tgldnc tglEmp cbNPN cbAltaUid edContNotes 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doModify wWin 
FUNCTION doModify RETURNS LOGICAL
  ( input pModify as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getComSeq wWin 
FUNCTION getComSeq RETURNS CHARACTER
  ( input cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getComStat wWin 
FUNCTION getComStat RETURNS CHARACTER
  ( input cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getContactType wWin 
FUNCTION getContactType RETURNS CHARACTER
  (ctype as char /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getEntity wWin 
FUNCTION getEntity RETURNS CHARACTER
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getEntityID wWin 
FUNCTION getEntityID RETURNS CHARACTER
  ( cPartnerAType as character,
    cPartnerA     as character,
    cPartnerB     as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getEntityName wWin 
FUNCTION getEntityName RETURNS CHARACTER
  ( cPartnerA     as character,
    cPartnerAType as character,
    cPartnerAName as character,
    cPartnerBName as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getPersonName wWin 
FUNCTION getPersonName RETURNS CHARACTER
  (cUserID as char /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getQualificationDesc wWin 
FUNCTION getQualificationDesc RETURNS CHARACTER
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getRoleStatus wWin 
FUNCTION getRoleStatus RETURNS CHARACTER
  ( cStat as logical /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openTags wWin 
FUNCTION openTags RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwAffiliations 
       MENU-ITEM m_View_affiliation LABEL "View affiliation".

DEFINE MENU POPUP-MENU-brwPersonRoles 
       MENU-ITEM m_View_role_fulfillments LABEL "View fulfillments".


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_folder AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddImg  NO-FOCUS
     LABEL "Add picture" 
     SIZE 4.6 BY 1.14 TOOLTIP "Add person picture".

DEFINE BUTTON bContCanc  NO-FOCUS
     LABEL "Cancel" 
     SIZE 7.2 BY 1.71 TOOLTIP "Cancel".

DEFINE BUTTON bContSave  NO-FOCUS
     LABEL "Save" 
     SIZE 7.2 BY 1.71 TOOLTIP "Save".

DEFINE BUTTON bDeleteImg  NO-FOCUS
     LABEL "Delete picture" 
     SIZE 4.6 BY 1.14 TOOLTIP "Delete person picture".

DEFINE BUTTON bEditname  NO-FOCUS
     LABEL "Edit" 
     SIZE 4.8 BY 1.14 TOOLTIP "Edit name".

DEFINE BUTTON bPersonDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete person".

DEFINE BUTTON bPersonDocuments  NO-FOCUS
     LABEL "Documents" 
     SIZE 7.2 BY 1.71 TOOLTIP "Documents".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON bViewLog  NO-FOCUS
     LABEL "ViewLogs" 
     SIZE 7.2 BY 1.71 TOOLTIP "View Logs".

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX SORT INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 8.6 BY 1 NO-UNDO.

DEFINE VARIABLE edContNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 159.6 BY 2 NO-UNDO.

DEFINE VARIABLE cbAltaUid AS CHARACTER FORMAT "X(256)":U 
     LABEL "ALTA UID" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE cbNPN AS CHARACTER FORMAT "X(256)":U 
     LABEL "NPN" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE fAddr AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 40 BY 1 NO-UNDO.

DEFINE VARIABLE fAddr2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 40 BY 1 NO-UNDO.

DEFINE VARIABLE fCity AS CHARACTER FORMAT "x(40)":U 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 TOOLTIP "City" NO-UNDO.

DEFINE VARIABLE fentityId AS CHARACTER FORMAT "X(256)":U 
     LABEL "Person ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE fname AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE fZip AS CHARACTER FORMAT "x(10)":U 
     VIEW-AS FILL-IN 
     SIZE 10.6 BY 1 TOOLTIP "Zipcode" NO-UNDO.

DEFINE RECTANGLE RECT-64
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 44 BY 2.14.

DEFINE VARIABLE tgldnc AS LOGICAL INITIAL no 
     LABEL "Do Not Contact" 
     VIEW-AS TOGGLE-BOX
     SIZE 18 BY .81 NO-UNDO.

DEFINE VARIABLE tglEmp AS LOGICAL INITIAL no 
     LABEL "Our Employee" 
     VIEW-AS TOGGLE-BOX
     SIZE 22 BY .81 NO-UNDO.

DEFINE BUTTON bAffiliatePerson  NO-FOCUS
     LABEL "Affiliate Person" 
     SIZE 4.8 BY 1.14 TOOLTIP "Affiliate person".

DEFINE BUTTON bDeleteAff  NO-FOCUS
     LABEL "Delete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete affiliation".

DEFINE BUTTON bEditAff  NO-FOCUS
     LABEL "Modify" 
     SIZE 4.8 BY 1.14 TOOLTIP "Modify affiliation".

DEFINE BUTTON bExportAff  NO-FOCUS
     LABEL "Export" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bNewAff  NO-FOCUS
     LABEL "New" 
     SIZE 4.8 BY 1.14 TOOLTIP "Affiliate organization".

DEFINE BUTTON bOpenAff  NO-FOCUS
     LABEL "Open" 
     SIZE 4.8 BY 1.14 TOOLTIP "Open affiliation".

DEFINE BUTTON bActive  NO-FOCUS
     LABEL "Active" 
     SIZE 4.8 BY 1.14 TOOLTIP "Activate/Deactivate qualification".

DEFINE BUTTON bDeleteQual  NO-FOCUS
     LABEL "Delete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete qualification".

DEFINE BUTTON bDocuments  NO-FOCUS
     LABEL "Documents" 
     SIZE 4.8 BY 1.14 TOOLTIP "Documents".

DEFINE BUTTON bEditQual  NO-FOCUS
     LABEL "Modify" 
     SIZE 4.8 BY 1.14 TOOLTIP "Modify qualification".

DEFINE BUTTON bExportQual  NO-FOCUS
     LABEL "Export" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bNewQual  NO-FOCUS
     LABEL "New" 
     SIZE 4.8 BY 1.14 TOOLTIP "New qualification".

DEFINE BUTTON bQualificationCopy  NO-FOCUS
     LABEL "Copy" 
     SIZE 4.8 BY 1.14 TOOLTIP "Copy qualification".

DEFINE BUTTON bQualificationRenew  NO-FOCUS
     LABEL "Renew" 
     SIZE 4.8 BY 1.14 TOOLTIP "Renew qualification".

DEFINE BUTTON bVerify  NO-FOCUS
     LABEL "Verify" 
     SIZE 4.8 BY 1.14 TOOLTIP "Verify qualification".

DEFINE VARIABLE QualType AS CHARACTER INITIAL "B" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Active", "A":U,
"Inactive", "I":U,
"Both", "B":U
     SIZE 35 BY 1.38 NO-UNDO.

DEFINE BUTTON bDeleteRole  NO-FOCUS
     LABEL "Delete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete role".

DEFINE BUTTON bEditRole  NO-FOCUS
     LABEL "Modify" 
     SIZE 4.8 BY 1.14 TOOLTIP "Modify role".

DEFINE BUTTON bNewRole  NO-FOCUS
     LABEL "New" 
     SIZE 4.8 BY 1.14 TOOLTIP "New role".

DEFINE BUTTON bOpenRole  NO-FOCUS
     LABEL "Fullfill" 
     SIZE 4.8 BY 1.14 TOOLTIP "Open fulfillments".

DEFINE BUTTON bRoleLog  NO-FOCUS
     LABEL "ViewRoleLog" 
     SIZE 4.8 BY 1.14 TOOLTIP "View logs".

DEFINE VARIABLE roleType AS CHARACTER INITIAL "B" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Active", "A":U,
"Inactive", "I":U,
"Both", "B":U
     SIZE 35 BY 1.38 NO-UNDO.

DEFINE BUTTON bEditPersonContact  NO-FOCUS
     LABEL "Modify" 
     SIZE 4.8 BY 1.14 TOOLTIP "Modify contact".

DEFINE BUTTON bExportPersonContact  NO-FOCUS
     LABEL "Export" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bExpPersonContact  NO-FOCUS
     LABEL "Expire" 
     SIZE 4.8 BY 1.14 TOOLTIP "Deactivate contact".

DEFINE BUTTON bMailPersonContact  NO-FOCUS
     LABEL "Mail" 
     SIZE 4.8 BY 1.14 TOOLTIP "Mail".

DEFINE BUTTON bNewPersonContact  NO-FOCUS
     LABEL "New" 
     SIZE 4.8 BY 1.14 TOOLTIP "New contact".

DEFINE VARIABLE rdContactType AS CHARACTER INITIAL "B" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Active", "A":U,
"Inactive", "I":U,
"Both", "B":U
     SIZE 35 BY 1.38 NO-UNDO.

DEFINE BUTTON bDelTag  NO-FOCUS
     LABEL "Delete tag" 
     SIZE 4.6 BY 1.14 TOOLTIP "Delete tag".

DEFINE BUTTON bEditTag  NO-FOCUS
     LABEL "Edit" 
     SIZE 4.6 BY 1.14 TOOLTIP "Update tag".

DEFINE BUTTON bExportTags  NO-FOCUS
     LABEL "Export" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bNewTag  NO-FOCUS
     LABEL "New" 
     SIZE 4.6 BY 1.14 TOOLTIP "Add a tag".

DEFINE BUTTON bEditPersonagent  NO-FOCUS
     LABEL "Modify" 
     SIZE 4.8 BY 1.13 TOOLTIP "Modify Association".

DEFINE BUTTON bExportAgents  NO-FOCUS
     LABEL "Export" 
     SIZE 4.8 BY 1.13 TOOLTIP "Export data".

DEFINE BUTTON bNewAgentLink  NO-FOCUS
     LABEL "New" 
     SIZE 4.8 BY 1.13 TOOLTIP "Link  Agent".

DEFINE BUTTON bPersonAgentDeactivate  NO-FOCUS
     LABEL "PersonAgentDeactivate" 
     SIZE 4.6 BY 1.13 TOOLTIP "Deactivate personagent".

DEFINE VARIABLE rdAgentType AS CHARACTER INITIAL "B" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Active", "A":U,
"Inactive", "I":U,
"Both", "B":U
     SIZE 35 BY 1.39 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAffiliations FOR 
      affiliation SCROLLING.

DEFINE QUERY brwAgents FOR 
      personagentdetails SCROLLING.

DEFINE QUERY brwFulfillment FOR 
      reqfulfill SCROLLING.

DEFINE QUERY brwpersoncontact FOR 
      personContact SCROLLING.

DEFINE QUERY brwPersonRoles FOR 
      personRole SCROLLING.

DEFINE QUERY brwQualifications FOR 
      Qualification SCROLLING.

DEFINE QUERY brwTags FOR 
      tag SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAffiliations
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAffiliations wWin _FREEFORM
  QUERY brwAffiliations DISPLAY
      affiliation.isCtrlPerson            column-label "Control Person"  width 16      view-as toggle-box   
getEntityName(affiliation.partnerA,
              affiliation.partnerAType,
              affiliation.partnerAName,
              affiliation.partnerBName) @ affiliation.partnerAName         label "Name"           format "x(30)"    width 60          
            
affiliation.jobTitle                                                       label "Job Title"          format "x(22)"    width 24              

getEntityID(affiliation.partnerAType,
            affiliation.partnerA,
            affiliation.partnerB)       @ affiliation.partnerA             label "ID"             format "999999"  width 15
            
getEntity(affiliation.partnerAType)     @ affiliation.partnerAType         label "Type"           format "x(15)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SEPARATORS SIZE 158 BY 12.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwAgents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAgents wWin _FREEFORM
  QUERY brwAgents DISPLAY
      personagentdetails.Agent      label "Agent" format "x(250)" width 50
personagentdetails.statedesc    label "State "   format "x(30)" width 25        
personagentdetails.statdesc     label "Association"        format "x(30)"      width 20
personagentdetails.jobTitle     label "Job Title"        format "x(50)"      width 65
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SEPARATORS SIZE 158 BY 11.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwFulfillment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFulfillment wWin _FREEFORM
  QUERY brwFulfillment DISPLAY
      reqfulfill.reqMet                                          column-label "Met"         width 6    view-as toggle-box
reqfulfill.reqdesc                                           label        "Requirement"   format "x(100)"      width 53
getQualificationDesc(reqfulfill.stat) @ reqfulfill.stat      label        "Status"        format "x(16)"      width 19
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 88.6 BY 11.81
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Requirements of Selected Role" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN TOOLTIP "Requirements and Fulfillments".

DEFINE BROWSE brwpersoncontact
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwpersoncontact wWin _FREEFORM
  QUERY brwpersoncontact DISPLAY
      personContact.active       column-label "Active"  width 10      view-as toggle-box  
getContactType(personContact.contactType) @ personContact.contactType     label "Contact Type"    format "x(50)"    width 20  
personContact.contactSubType     label "Sub Type"       format "x(50)"      width 25 
personContact.contactID          label "Contact"        format "x(50)"      width 50 
getPersonName(personContact.uid) @ personContact.uid    label "Created By"     format "x(30)"      width 28 
personContact.createDate         label "Created Date"   format "99/99/9999" width 30
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 158 BY 11.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwPersonRoles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwPersonRoles wWin _FREEFORM
  QUERY brwPersonRoles DISPLAY
      personRole.comstat  label ""              format "x(2)"     width 2   
personRole.stateID        label "State"         format "x(5)"     width 6
personRole.role           label "Role"          format "x(15)"    width 17
personRole.roleID         label "Role ID"       format "x(10)"    width 11
getRoleStatus(personrole.active) @ personrole.active    label "Status"    format "x(8)"    width 8
personRole.totfulfillreq   column-label "# Fulfillable ! Requirements "   format ">>>"     width 5
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SEPARATORS SIZE 67.6 BY 11.86
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Roles" ROW-HEIGHT-CHARS .85 FIT-LAST-COLUMN.

DEFINE BROWSE brwQualifications
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQualifications wWin _FREEFORM
  QUERY brwQualifications DISPLAY
      qualification.activ                 column-label "Active"                             width 10      view-as toggle-box   
qualification.stateID                     column-label "State ID"       format "x(5)"          width 12
qualification.Qualification               label "Qualification"         format "x(50)"         width 45
qualification.QualificationNumber         label "Number"                format "x(20)"         width 22
getQualificationDesc(qualification.stat)  
@ qualification.stat                      label "Status"                format "x(15)"         width 25
qualification.effectiveDate               label "Effective"             format "99/99/9999"    width 20
qualification.expirationDate              label "Expiration"            format "99/99/9999"    width 25
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 158 BY 11.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwTags
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwTags wWin _FREEFORM
  QUERY brwTags DISPLAY
      tag.name label "Tag Name"    format "x(30)"
tag.createddate label "Tag Date" format "99/99/9999" width 30
tag.userName label "User Name"    format "x(30)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 156.8 BY 12.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bPersonDocuments AT ROW 1.62 COL 38.6 WIDGET-ID 386 NO-TAB-STOP 
     bAddImg AT ROW 1.57 COL 131.6 WIDGET-ID 234 NO-TAB-STOP 
     bDeleteImg AT ROW 2.71 COL 131.6 WIDGET-ID 346 NO-TAB-STOP 
     fentityId AT ROW 4 COL 15 COLON-ALIGNED WIDGET-ID 372
     fname AT ROW 5.1 COL 15 COLON-ALIGNED WIDGET-ID 6
     fAddr AT ROW 6.19 COL 15 COLON-ALIGNED WIDGET-ID 54
     fAddr2 AT ROW 7.29 COL 15 COLON-ALIGNED NO-LABEL WIDGET-ID 370
     fCity AT ROW 8.38 COL 15 COLON-ALIGNED NO-LABEL WIDGET-ID 56
     cbState AT ROW 8.38 COL 35.4 COLON-ALIGNED NO-LABEL WIDGET-ID 314
     fZip AT ROW 8.38 COL 44.4 COLON-ALIGNED NO-LABEL WIDGET-ID 60
     tgldnc AT ROW 4 COL 81 WIDGET-ID 384
     tglEmp AT ROW 4.71 COL 81 WIDGET-ID 380
     cbNPN AT ROW 5.52 COL 79 COLON-ALIGNED WIDGET-ID 22
     cbAltaUid AT ROW 6.62 COL 79 COLON-ALIGNED WIDGET-ID 24
     bEditname AT ROW 5 COL 47.2 WIDGET-ID 316 NO-TAB-STOP 
     bContSave AT ROW 1.62 COL 10.6 WIDGET-ID 360 NO-TAB-STOP 
     edContNotes AT ROW 9.76 COL 6.6 NO-LABEL WIDGET-ID 250
     bPersonDelete AT ROW 1.62 COL 24.6 WIDGET-ID 362 NO-TAB-STOP 
     bRefresh AT ROW 1.62 COL 3.6 WIDGET-ID 322 NO-TAB-STOP 
     bViewLog AT ROW 1.62 COL 31.6 WIDGET-ID 364 NO-TAB-STOP 
     bContCanc AT ROW 1.62 COL 17.6 WIDGET-ID 210 NO-TAB-STOP 
     RECT-64 AT ROW 1.43 COL 2.8 WIDGET-ID 366
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 167.4 BY 27 WIDGET-ID 100.

DEFINE FRAME FRAME-E
     roleType AT ROW 1.1 COL 22.2 NO-LABEL WIDGET-ID 358
     brwPersonRoles AT ROW 2.86 COL 6.6 WIDGET-ID 400
     brwFulfillment AT ROW 2.86 COL 75.4 WIDGET-ID 1300
     bOpenRole AT ROW 7.24 COL 1.4 WIDGET-ID 228 NO-TAB-STOP 
     bRoleLog AT ROW 6.14 COL 1.4 WIDGET-ID 356 NO-TAB-STOP 
     bNewRole AT ROW 2.86 COL 1.4 WIDGET-ID 276 NO-TAB-STOP 
     bDeleteRole AT ROW 5.05 COL 1.4 WIDGET-ID 324 NO-TAB-STOP 
     bEditRole AT ROW 3.95 COL 1.4 WIDGET-ID 328 NO-TAB-STOP 
     "Show Status :" VIEW-AS TEXT
          SIZE 14 BY .86 AT ROW 1.38 COL 8.2 WIDGET-ID 362
    WITH 1 DOWN NO-BOX OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2 ROW 13.62
         SIZE 164 BY 14 WIDGET-ID 1000.

DEFINE FRAME FRAME-D
     QualType AT ROW 1.1 COL 22.2 NO-LABEL WIDGET-ID 358
     bQualificationRenew AT ROW 7.19 COL 1.4 WIDGET-ID 364 NO-TAB-STOP 
     brwQualifications AT ROW 2.86 COL 6.6 WIDGET-ID 200
     bQualificationCopy AT ROW 8.29 COL 1.4 WIDGET-ID 348 NO-TAB-STOP 
     bVerify AT ROW 10.48 COL 1.4 WIDGET-ID 354 NO-TAB-STOP 
     bDeleteQual AT ROW 6.1 COL 1.4 WIDGET-ID 206 NO-TAB-STOP 
     bEditQual AT ROW 5 COL 1.4 WIDGET-ID 298 NO-TAB-STOP 
     bExportQual AT ROW 2.81 COL 1.4 WIDGET-ID 340 NO-TAB-STOP 
     bNewQual AT ROW 3.91 COL 1.4 WIDGET-ID 204 NO-TAB-STOP 
     bActive AT ROW 11.57 COL 1.4 WIDGET-ID 350 NO-TAB-STOP 
     bDocuments AT ROW 9.38 COL 1.4 WIDGET-ID 352 NO-TAB-STOP
     "Show Status :" VIEW-AS TEXT
          SIZE 14 BY .86 AT ROW 1.38 COL 8.2 WIDGET-ID 362
    WITH 1 DOWN NO-BOX OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2 ROW 13.62
         SIZE 164 BY 14 WIDGET-ID 600.

DEFINE FRAME FRAME-I
     bEditPersonagent AT ROW 4.83 COL 1.8 WIDGET-ID 298 NO-TAB-STOP 
     rdAgentType AT ROW 1.09 COL 27.6 NO-LABEL WIDGET-ID 358
     brwAgents AT ROW 2.65 COL 7 WIDGET-ID 2200
     bPersonAgentDeactivate AT ROW 5.96 COL 2 WIDGET-ID 408 NO-TAB-STOP 
     bExportAgents AT ROW 2.61 COL 1.8 WIDGET-ID 354 NO-TAB-STOP 
     bNewAgentLink AT ROW 3.7 COL 1.8 WIDGET-ID 204 NO-TAB-STOP 
     "Show Association :" VIEW-AS TEXT
          SIZE 19 BY .87 AT ROW 1.39 COL 7.4 WIDGET-ID 362
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2 ROW 13.62
         SIZE 164 BY 14 WIDGET-ID 2100.
DEFINE FRAME FRAME-A
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 136.4 ROW 1.62
         SIZE 29.8 BY 7.76 WIDGET-ID 1200.

DEFINE FRAME FRAME-C
     brwAffiliations AT ROW 1.57 COL 6.6 WIDGET-ID 1100
     bAffiliatePerson AT ROW 2.62 COL 1.4 WIDGET-ID 352 NO-TAB-STOP 
     bNewAff AT ROW 3.71 COL 1.4 WIDGET-ID 290 NO-TAB-STOP 
     bDeleteAff AT ROW 5.91 COL 1.4 WIDGET-ID 292 NO-TAB-STOP 
     bEditAff AT ROW 4.81 COL 1.4 WIDGET-ID 288 NO-TAB-STOP 
     bExportAff AT ROW 1.52 COL 1.4 WIDGET-ID 354 NO-TAB-STOP 
     bOpenAff AT ROW 7 COL 1.4 WIDGET-ID 300 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2 ROW 13.62
         SIZE 164 BY 14 WIDGET-ID 300.

DEFINE FRAME FRAME-G
     bMailPersonContact AT ROW 6.95 COL 1.8 WIDGET-ID 364 NO-TAB-STOP 
     rdContactType AT ROW 1.1 COL 22.2 NO-LABEL WIDGET-ID 358
     brwpersoncontact AT ROW 2.67 COL 7 WIDGET-ID 1700
     bEditPersonContact AT ROW 4.81 COL 1.8 WIDGET-ID 298 NO-TAB-STOP 
     bExpPersonContact AT ROW 5.91 COL 1.8 WIDGET-ID 206 NO-TAB-STOP 
     bNewPersonContact AT ROW 3.71 COL 1.8 WIDGET-ID 204 NO-TAB-STOP 
     bExportPersonContact AT ROW 2.62 COL 1.8 WIDGET-ID 354 NO-TAB-STOP 
     "Show Status :" VIEW-AS TEXT
          SIZE 14 BY .86 AT ROW 1.38 COL 8.2 WIDGET-ID 362
    WITH 1 DOWN NO-BOX OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2 ROW 13.62
         SIZE 164 BY 14 WIDGET-ID 1400.

DEFINE FRAME FRAME-H
     brwTags AT ROW 1.71 COL 7.2 WIDGET-ID 2000
     bExportTags AT ROW 1.67 COL 2 WIDGET-ID 354 NO-TAB-STOP 
     bDelTag AT ROW 4.95 COL 2 WIDGET-ID 208 NO-TAB-STOP 
     bEditTag AT ROW 3.86 COL 2 WIDGET-ID 272 NO-TAB-STOP 
     bNewTag AT ROW 2.76 COL 2 WIDGET-ID 274 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2 ROW 13.62
         SIZE 164 BY 14 WIDGET-ID 1800.

/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Container Links: Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source
   Other Settings: APPSERVER
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = "Person"
         HEIGHT             = 27.19
         WIDTH              = 167.4
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB wWin 
/* ************************* Included-Libraries *********************** */

{src/adm2/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME FRAME-A:FRAME = FRAME fMain:HANDLE
       FRAME FRAME-C:FRAME = FRAME fMain:HANDLE
       FRAME FRAME-D:FRAME = FRAME fMain:HANDLE
       FRAME FRAME-E:FRAME = FRAME fMain:HANDLE
       FRAME FRAME-G:FRAME = FRAME fMain:HANDLE
       FRAME FRAME-H:FRAME = FRAME fMain:HANDLE
       FRAME FRAME-I:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */

DEFINE VARIABLE XXTABVALXX AS LOGICAL NO-UNDO.

ASSIGN XXTABVALXX = FRAME FRAME-C:MOVE-BEFORE-TAB-ITEM (fentityId:HANDLE IN FRAME fMain)
       XXTABVALXX = FRAME FRAME-A:MOVE-AFTER-TAB-ITEM (cbAltaUid:HANDLE IN FRAME fMain)
       XXTABVALXX = FRAME FRAME-A:MOVE-BEFORE-TAB-ITEM (edContNotes:HANDLE IN FRAME fMain)
       XXTABVALXX = FRAME FRAME-G:MOVE-BEFORE-TAB-ITEM (FRAME FRAME-C:HANDLE)
       XXTABVALXX = FRAME FRAME-E:MOVE-BEFORE-TAB-ITEM (FRAME FRAME-G:HANDLE)
       XXTABVALXX = FRAME FRAME-D:MOVE-BEFORE-TAB-ITEM (FRAME FRAME-E:HANDLE)
/* END-ASSIGN-TABS */.

ASSIGN 
       FRAME fMain:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN fentityId IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       fentityId:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FRAME FRAME-A
                                                                        */
/* SETTINGS FOR FRAME FRAME-C
                                                                        */
/* BROWSE-TAB brwAffiliations 1 FRAME-C */
ASSIGN 
       bAffiliatePerson:PRIVATE-DATA IN FRAME FRAME-C     = 
                "A".

ASSIGN 
       bDeleteAff:PRIVATE-DATA IN FRAME FRAME-C     = 
                "A".

ASSIGN 
       bExportAff:PRIVATE-DATA IN FRAME FRAME-C     = 
                "A".

ASSIGN 
       bNewAff:PRIVATE-DATA IN FRAME FRAME-C     = 
                "A".

ASSIGN 
       bOpenAff:PRIVATE-DATA IN FRAME FRAME-C     = 
                "A".

ASSIGN 
       brwAffiliations:POPUP-MENU IN FRAME FRAME-C             = MENU POPUP-MENU-brwAffiliations:HANDLE
       brwAffiliations:ALLOW-COLUMN-SEARCHING IN FRAME FRAME-C = TRUE
       brwAffiliations:COLUMN-RESIZABLE IN FRAME FRAME-C       = TRUE.

/* SETTINGS FOR FRAME FRAME-D
                                                                        */
/* BROWSE-TAB brwQualifications bQualificationRenew FRAME-D */
ASSIGN 
       bDeleteQual:PRIVATE-DATA IN FRAME FRAME-D     = 
                "A".

ASSIGN 
       bExportQual:PRIVATE-DATA IN FRAME FRAME-D     = 
                "A".

ASSIGN 
       bNewQual:PRIVATE-DATA IN FRAME FRAME-D     = 
                "A".

ASSIGN 
       brwQualifications:ALLOW-COLUMN-SEARCHING IN FRAME FRAME-D = TRUE
       brwQualifications:COLUMN-RESIZABLE IN FRAME FRAME-D       = TRUE.

/* SETTINGS FOR FRAME FRAME-E
                                                                        */
/* BROWSE-TAB brwPersonRoles roleType FRAME-E */
/* BROWSE-TAB brwFulfillment brwPersonRoles FRAME-E */
ASSIGN 
       bDeleteRole:PRIVATE-DATA IN FRAME FRAME-E     = 
                "A".

ASSIGN 
       bEditRole:PRIVATE-DATA IN FRAME FRAME-E     = 
                "A".

ASSIGN 
       brwFulfillment:ALLOW-COLUMN-SEARCHING IN FRAME FRAME-E = TRUE
       brwFulfillment:COLUMN-RESIZABLE IN FRAME FRAME-E       = TRUE
       brwFulfillment:COLUMN-MOVABLE IN FRAME FRAME-E         = TRUE.

ASSIGN 
       brwPersonRoles:POPUP-MENU IN FRAME FRAME-E             = MENU POPUP-MENU-brwPersonRoles:HANDLE
       brwPersonRoles:ALLOW-COLUMN-SEARCHING IN FRAME FRAME-E = TRUE
       brwPersonRoles:COLUMN-RESIZABLE IN FRAME FRAME-E       = TRUE.

/* SETTINGS FOR FRAME FRAME-G
                                                                        */
/* BROWSE-TAB brwpersoncontact rdContactType FRAME-G */
ASSIGN 
       bExportPersonContact:PRIVATE-DATA IN FRAME FRAME-G     = 
                "A".

ASSIGN 
       bExpPersonContact:PRIVATE-DATA IN FRAME FRAME-G     = 
                "A".

ASSIGN 
       bNewPersonContact:PRIVATE-DATA IN FRAME FRAME-G     = 
                "A".

ASSIGN 
       brwpersoncontact:ALLOW-COLUMN-SEARCHING IN FRAME FRAME-G = TRUE
       brwpersoncontact:COLUMN-RESIZABLE IN FRAME FRAME-G       = TRUE.

/* SETTINGS FOR FRAME FRAME-H
                                                                        */
/* BROWSE-TAB brwTags 1 FRAME-H */
/* SETTINGS FOR BUTTON bDelTag IN FRAME FRAME-H
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bEditTag IN FRAME FRAME-H
   NO-ENABLE                                                            */
ASSIGN 
       bExportTags:PRIVATE-DATA IN FRAME FRAME-H     = 
                "A".

ASSIGN 
       brwTags:ALLOW-COLUMN-SEARCHING IN FRAME FRAME-H = TRUE
       brwTags:COLUMN-RESIZABLE IN FRAME FRAME-H       = TRUE
       brwTags:COLUMN-MOVABLE IN FRAME FRAME-H         = TRUE.

/* SETTINGS FOR FRAME FRAME-I
                                                                        */
/* BROWSE-TAB brwAgents rdAgentType FRAME-I */
ASSIGN 
       bExportAgents:PRIVATE-DATA IN FRAME FRAME-I     = 
                "A".

ASSIGN 
       bNewAgentLink:PRIVATE-DATA IN FRAME FRAME-I     = 
                "A".

/* SETTINGS FOR BUTTON bPersonAgentDeactivate IN FRAME FRAME-I
   NO-ENABLE                                                            */
ASSIGN 
       brwAgents:ALLOW-COLUMN-SEARCHING IN FRAME FRAME-I = TRUE
       brwAgents:COLUMN-RESIZABLE IN FRAME FRAME-I       = TRUE
       brwAgents:COLUMN-MOVABLE IN FRAME FRAME-I         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAffiliations
/* Query rebuild information for BROWSE brwAffiliations
     _START_FREEFORM
open query {&SELF-NAME} for each affiliation by affiliation.partnerAName
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAffiliations */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAgents
/* Query rebuild information for BROWSE brwAgents
     _START_FREEFORM
open query brwAgents for each personagentdetails where personagentdetails.stat = "A"  by personagentdetails.stat desc .
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAgents */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFulfillment
/* Query rebuild information for BROWSE brwFulfillment
     _START_FREEFORM
open query {&SELF-NAME} for each reqfulfill.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwFulfillment */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwpersoncontact
/* Query rebuild information for BROWSE brwpersoncontact
     _START_FREEFORM
open query {&SELF-NAME} for each personContact by personContact.active desc .
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwpersoncontact */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwPersonRoles
/* Query rebuild information for BROWSE brwPersonRoles
     _START_FREEFORM
open query {&SELF-NAME} for each personRole.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwPersonRoles */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQualifications
/* Query rebuild information for BROWSE brwQualifications
     _START_FREEFORM
open query {&SELF-NAME} for each Qualification where Qualification.entityId = ipcPersonId by Qualification.QualificationID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQualifications */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwTags
/* Query rebuild information for BROWSE brwTags
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tag.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwTags */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin /* Person */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  if this-procedure:persistent 
   then 
    return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin /* Person */
do:
  /* This event will close the window and terminate the procedure.  */  
  run closeWindow in this-procedure.
  return no-apply.          
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-RESIZED OF wWin /* Person */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-D
&Scoped-define SELF-NAME bActive
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActive wWin
ON CHOOSE OF bActive IN FRAME FRAME-D /* Active */
do:
  run changeStatus in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bAddImg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddImg wWin
ON CHOOSE OF bAddImg IN FRAME fMain /* Add picture */
do:
  run addImage in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-C
&Scoped-define SELF-NAME bAffiliatePerson
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAffiliatePerson wWin
ON CHOOSE OF bAffiliatePerson IN FRAME FRAME-C /* Affiliate Person */
do:
  run affiliatePerson in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bContCanc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bContCanc wWin
ON CHOOSE OF bContCanc IN FRAME fMain /* Cancel */
do:
  run cancelPerson in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bContSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bContSave wWin
ON CHOOSE OF bContSave IN FRAME fMain /* Save */
do:
  run savePerson in this-procedure.
  run setButtons in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-C
&Scoped-define SELF-NAME bDeleteAff
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteAff wWin
ON CHOOSE OF bDeleteAff IN FRAME FRAME-C /* Delete */
do:
  run deleteAffiliation in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bDeleteImg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteImg wWin
ON CHOOSE OF bDeleteImg IN FRAME fMain /* Delete picture */
do:
  run deleteImage in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-D
&Scoped-define SELF-NAME bDeleteQual
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteQual wWin
ON CHOOSE OF bDeleteQual IN FRAME FRAME-D /* Delete */
do:
  run deleteQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-E
&Scoped-define SELF-NAME bDeleteRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteRole wWin
ON CHOOSE OF bDeleteRole IN FRAME FRAME-E /* Delete */
do:
  run deletePersonRole in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-H
&Scoped-define SELF-NAME bDelTag
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelTag wWin
ON CHOOSE OF bDelTag IN FRAME FRAME-H /* Delete tag */
DO:
  if not available tag
   then return.
  
  empty temp-table temptag.
  create temptag.
  assign
      temptag.tagID      = tag.tagID
      temptag.entity     = "P"
      temptag.entityID   = tag.entityID
      temptag.uid        = tag.uid
      temptag.name       = tag.name
      . 
  
  message "Highlighted tag will be deleted." skip "Do you want to continue?"
    view-as alert-box question buttons yes-no
    title "Delete Tag"
    update lContinue as logical.
  
  if not lContinue
   then
    return.
  
  run untagentity in hPersonDataSrv (input table temptag,
                                     output std-lo).
  if not std-lo
   then
    return no-apply.
    
  openTags().
  
  apply 'value-changed' to brwTags in frame frame-h.
  
  empty temp-table temptag.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-D
&Scoped-define SELF-NAME bDocuments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDocuments wWin
ON CHOOSE OF bDocuments IN FRAME FRAME-D /* Documents */
do:
 run openDocument in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-C
&Scoped-define SELF-NAME bEditAff
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditAff wWin
ON CHOOSE OF bEditAff IN FRAME FRAME-C /* Modify */
do:
  run modifyAffiliation in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bEditname
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditname wWin
ON CHOOSE OF bEditname IN FRAME fMain /* Edit */
do:
  run editPersonName in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-I
&Scoped-define SELF-NAME bEditPersonagent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditPersonagent wWin
ON CHOOSE OF bEditPersonagent IN FRAME FRAME-I /* Modify */
do:
  run modifyPersonagent in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-G
&Scoped-define SELF-NAME bEditPersonContact
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditPersonContact wWin
ON CHOOSE OF bEditPersonContact IN FRAME FRAME-G /* Modify */
do:
  run modifyPersonContact in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-D
&Scoped-define SELF-NAME bEditQual
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditQual wWin
ON CHOOSE OF bEditQual IN FRAME FRAME-D /* Modify */
do:
  run modifyQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-E
&Scoped-define SELF-NAME bEditRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditRole wWin
ON CHOOSE OF bEditRole IN FRAME FRAME-E /* Modify */
do:
  run modifyPersonRole in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-H
&Scoped-define SELF-NAME bEditTag
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditTag wWin
ON CHOOSE OF bEditTag IN FRAME FRAME-H /* Edit */
DO:
  if available person and available tag
   then
    do:
      empty temp-table temptag.
      create temptag.
      buffer-copy tag to temptag.
      assign temptag.entityname = person.dispName.
      
      run dialogtag.w(input hPersonDataSrv,
                      input "M",
                      input-output table temptag,
                      output std-lo).
      if not std-lo
       then
        return no-apply.
      
      openTags().
      
      /* Reposition to the record. */
      for first temptag:
        find first tag where tag.tagID = temptag.tagID no-error.
        if available tag 
         then std-ro = rowid(tag).
      end.
  
      reposition brwTags to rowid std-ro no-error.
      apply 'value-changed' to brwTags in frame frame-h.
    end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-C
&Scoped-define SELF-NAME bExportAff
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExportAff wWin
ON CHOOSE OF bExportAff IN FRAME FRAME-C /* Export */
do:
  run exportData in this-procedure ({&AffiliationCode}).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-I
&Scoped-define SELF-NAME bExportAgents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExportAgents wWin
ON CHOOSE OF bExportAgents IN FRAME FRAME-I /* Export */
do:
  run exportData in this-procedure ('I').
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-G
&Scoped-define SELF-NAME bExportPersonContact
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExportPersonContact wWin
ON CHOOSE OF bExportPersonContact IN FRAME FRAME-G /* Export */
do:
  run exportData in this-procedure ('C').
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-D
&Scoped-define SELF-NAME bExportQual
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExportQual wWin
ON CHOOSE OF bExportQual IN FRAME FRAME-D /* Export */
do:
  run exportData in this-procedure ({&QualificationCode}).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-H
&Scoped-define SELF-NAME bExportTags
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExportTags wWin
ON CHOOSE OF bExportTags IN FRAME FRAME-H /* Export */
do:
  run exportData in this-procedure ('T').
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-G
&Scoped-define SELF-NAME bExpPersonContact
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExpPersonContact wWin
ON CHOOSE OF bExpPersonContact IN FRAME FRAME-G /* Expire */
do:
  run expirePersonContact in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bMailPersonContact
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bMailPersonContact wWin
ON CHOOSE OF bMailPersonContact IN FRAME FRAME-G /* Mail */
DO:
  if not available personcontact
  then
   return.

 run openURL("mailto:" + personContact.contactID).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-C
&Scoped-define SELF-NAME bNewAff
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewAff wWin
ON CHOOSE OF bNewAff IN FRAME FRAME-C /* New */
do:
  run newAffiliation in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-I
&Scoped-define SELF-NAME bNewAgentLink
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewAgentLink wWin
ON CHOOSE OF bNewAgentLink IN FRAME FRAME-I /* New */
do:
  run newAgentLink in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-G
&Scoped-define SELF-NAME bNewPersonContact
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewPersonContact wWin
ON CHOOSE OF bNewPersonContact IN FRAME FRAME-G /* New */
do:
  run newPersonContact in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-D
&Scoped-define SELF-NAME bNewQual
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewQual wWin
ON CHOOSE OF bNewQual IN FRAME FRAME-D /* New */
do:
  run newQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-E
&Scoped-define SELF-NAME bNewRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewRole wWin
ON CHOOSE OF bNewRole IN FRAME FRAME-E /* New */
do:                                                                               
  run newPersonRole in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-H
&Scoped-define SELF-NAME bNewTag
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewTag wWin
ON CHOOSE OF bNewTag IN FRAME FRAME-H /* New */
DO: 
  if available person
   then
    do:
      empty temp-table temptag.
      create temptag.
      assign
        temptag.entity     = "P"
        temptag.entityID   = personID
        temptag.uid        = cCurrentUID
        temptag.username   = cCurrUser        
        temptag.entityname = person.dispName
        .
      run dialogtag.w(input hPersonDataSrv,
                      input "N",
                      input-output table temptag,
                      output std-lo).
      if not std-lo
       then
        return no-apply.
      
      openTags().
      
      for first temptag:
        find first tag where tag.tagID = temptag.tagID no-error.
        if available tag 
         then std-ro = rowid(tag).
      end.
      
      reposition brwTags to rowid std-ro no-error.
      apply 'value-changed' to brwTags in frame frame-h.
    end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-C
&Scoped-define SELF-NAME bOpenAff
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOpenAff wWin
ON CHOOSE OF bOpenAff IN FRAME FRAME-C /* Open */
do:
  run openAffiliation in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-E
&Scoped-define SELF-NAME bOpenRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOpenRole wWin
ON CHOOSE OF bOpenRole IN FRAME FRAME-E /* Fullfill */
do:
  run openRoleFulfillment in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-I
&Scoped-define SELF-NAME bPersonAgentDeactivate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPersonAgentDeactivate wWin
ON CHOOSE OF bPersonAgentDeactivate IN FRAME FRAME-I /* PersonAgentDeactivate */
do:
  if not available personagentdetails
   then
    return.
    
  run deactivatepersonagent in this-procedure.
  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bPersonDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPersonDelete wWin
ON CHOOSE OF bPersonDelete IN FRAME fMain /* Delete */
do:
  run deletePerson in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPersonDocuments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPersonDocuments wWin
ON CHOOSE OF bPersonDocuments IN FRAME fMain /* Documents */
do:
  publish "OpenDocument" (input {&Person},
                          input fentityId:input-value,
                          input this-procedure).

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-D
&Scoped-define SELF-NAME bQualificationCopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bQualificationCopy wWin
ON CHOOSE OF bQualificationCopy IN FRAME FRAME-D /* Copy */
do:
  run copyQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bQualificationRenew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bQualificationRenew wWin
ON CHOOSE OF bQualificationRenew IN FRAME FRAME-D /* Renew */
do:
  run renewQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh wWin
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
do:
  run refreshData in this-procedure (yes,  /*  NeedQualification  */  
                                     yes,  /* iplNeedAffiliation  */ 
                                     yes,  /* iplNeedpersonagent   */
                                     yes,  /*  NeedPersonrole     */
                                     yes,  /*  NeedReqFulfill     */
                                     yes , /*  NeedPersonContact  */  
                                     yes ). /* NeedPersonAgentDetails */
  run refreshTags in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-E
&Scoped-define SELF-NAME bRoleLog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRoleLog wWin
ON CHOOSE OF bRoleLog IN FRAME FRAME-E /* ViewRoleLog */
do:
  run openRoleLog in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAffiliations
&Scoped-define FRAME-NAME FRAME-C
&Scoped-define SELF-NAME brwAffiliations
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAffiliations wWin
ON DEFAULT-ACTION OF brwAffiliations IN FRAME FRAME-C
DO:
  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  if std-ch = 'COM' 
   then
    do:
      if affiliation.partnerAType <> {&AgentCode} 
       then
          run openAffiliation in this-procedure.  
     end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAffiliations wWin
ON ROW-DISPLAY OF brwAffiliations IN FRAME FRAME-C
do:
  {lib/brw-rowdisplay-multi.i} 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAffiliations wWin
ON START-SEARCH OF brwAffiliations IN FRAME FRAME-C
do:                                                                                         
  {lib/brw-startSearch-multi.i &browse-name = brwAffiliations &sortClause = "'by partnerAName'"}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAffiliations wWin
ON VALUE-CHANGED OF brwAffiliations IN FRAME FRAME-C
do:
  brwAffiliations:tooltip = (if affiliation.partnerAType = {&PersonCode} then "PersonID:" else (if affiliation.partnerAType = {&AgentCode} then "AgentID:" else "OrgID: ")) + string(affiliation.partnerA) .
  
  run setButtons in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAgents
&Scoped-define FRAME-NAME FRAME-I
&Scoped-define SELF-NAME brwAgents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAgents wWin
ON ROW-DISPLAY OF brwAgents IN FRAME FRAME-I
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAgents wWin
ON START-SEARCH OF brwAgents IN FRAME FRAME-I
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwAgents}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAgents wWin
ON VALUE-CHANGED OF brwAgents IN FRAME FRAME-I
DO:
  if not available personagentdetails
   then
    return. 
     
  if  personagentdetails.stat <> "A"
   then
    do:                                                            
      bPersonAgentDeactivate    :load-image("images/s-flag_green.bmp").
      bPersonAgentDeactivate    :tooltip   = "Activate association".
    end.
  else 
   do:
      bPersonAgentDeactivate    :load-image("images/s-flag_red.bmp").
      bPersonAgentDeactivate    :tooltip   = "Deactivate association".
   end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFulfillment
&Scoped-define FRAME-NAME FRAME-E
&Scoped-define SELF-NAME brwFulfillment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFulfillment wWin
ON ROW-DISPLAY OF brwFulfillment IN FRAME FRAME-E /* Requirements of Selected Role */
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFulfillment wWin
ON START-SEARCH OF brwFulfillment IN FRAME FRAME-E /* Requirements of Selected Role */
do:
  {lib/brw-startSearch-multi.i &browse-name = brwFulfillment}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwpersoncontact
&Scoped-define FRAME-NAME FRAME-G
&Scoped-define SELF-NAME brwpersoncontact
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwpersoncontact wWin
ON DEFAULT-ACTION OF brwpersoncontact IN FRAME FRAME-G
DO:
  run modifyPersonContact in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwpersoncontact wWin
ON ROW-DISPLAY OF brwpersoncontact IN FRAME FRAME-G
do:
  {lib/brw-rowdisplay-multi.i} 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwpersoncontact wWin
ON START-SEARCH OF brwpersoncontact IN FRAME FRAME-G
do:
  {lib/brw-startSearch-multi.i &browse-name = brwPersonContact}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwpersoncontact wWin
ON VALUE-CHANGED OF brwpersoncontact IN FRAME FRAME-G
DO:
  if not available personcontact
   then
    return.
     
 if personcontact.contactID ne '' and personcontact.contacttype = 'E' and tgldnc:checked in frame fmain = false
  then
   bMailPersonContact:sensitive = true. 
 else
   bMailPersonContact:sensitive = false. 
    
  run setButtons in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwPersonRoles
&Scoped-define FRAME-NAME FRAME-E
&Scoped-define SELF-NAME brwPersonRoles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPersonRoles wWin
ON DEFAULT-ACTION OF brwPersonRoles IN FRAME FRAME-E /* Roles */
do:
  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  if std-ch = 'COM' 
   then
    run openRoleFulfillment in this-procedure. 
  apply "value-changed" to brwPersonRoles. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPersonRoles wWin
ON ROW-DISPLAY OF brwPersonRoles IN FRAME FRAME-E /* Roles */
do:
  {lib/brw-rowdisplay-multi.i}
   cstat = "".
   if personrole.role = {&Attorney}
    then
     do:
       publish "getAttorneyStatus" (input personrole.roleID,output cstat).
       if cstat ne {&ActiveStat} 
        then
         personrole.comstat = "".     
     end.
    
  case personrole.comstat:
    when "R" 
     then  
      personrole.comstat:bgcolor in browse brwPersonRoles = 12.

    when "Y" 
     then 
      personrole.comstat:bgcolor in browse brwPersonRoles = 14.

    when "G" 
     then
      personrole.comstat:bgcolor in browse brwPersonRoles =  2.
  end case.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPersonRoles wWin
ON START-SEARCH OF brwPersonRoles IN FRAME FRAME-E /* Roles */
do:
  {lib/brw-startSearch-multi.i &browse-name = brwPersonRoles}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPersonRoles wWin
ON VALUE-CHANGED OF brwPersonRoles IN FRAME FRAME-E /* Roles */
do:
  do with frame {&frame-name}:
  end.
  
  if can-find(first personrole) 
   then
    do:
      run refreshfulfillmentData in this-procedure.  
    end.
   
  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  if std-ch = 'COM' 
   then
    do:

      /* We cannot edit or delete "Control Person(personRole.stateId = "00" ) "*/
      if can-find(first affiliation where affiliation.isCtrlPerson)
       then
        do:
          if personRole.stateId = "00"         and
             personRole.role    = {&ControlPerson} 
           then
            do:
              assign
                  bDeleteRole:sensitive = false
                  bEditRole:sensitive   = false
                  .
              return.
            end.
        end.
      
      assign
          bDeleteRole:sensitive = if query brwPersonRoles:num-results = 0 or query brwPersonRoles:num-results = ? then false else true
          bEditRole:sensitive   = if query brwPersonRoles:num-results = 0 or query brwPersonRoles:num-results = ? then false else true
          .
          
    end.      
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQualifications
&Scoped-define FRAME-NAME FRAME-D
&Scoped-define SELF-NAME brwQualifications
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualifications wWin
ON DEFAULT-ACTION OF brwQualifications IN FRAME FRAME-D
do:
  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  if std-ch = 'COM' 
   then
    run modifyQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualifications wWin
ON ROW-DISPLAY OF brwQualifications IN FRAME FRAME-D
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualifications wWin
ON START-SEARCH OF brwQualifications IN FRAME FRAME-D
do:
  {lib/brw-startSearch-multi.i &browse-name = brwQualifications}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualifications wWin
ON VALUE-CHANGED OF brwQualifications IN FRAME FRAME-D
do:
  run setButtons in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwTags
&Scoped-define FRAME-NAME FRAME-H
&Scoped-define SELF-NAME brwTags
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTags wWin
ON ROW-DISPLAY OF brwTags IN FRAME FRAME-H
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTags wWin
ON START-SEARCH OF brwTags IN FRAME FRAME-H
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwTags}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTags wWin
ON VALUE-CHANGED OF brwTags IN FRAME FRAME-H
DO:
  assign
    bEditTag:sensitive = (available tag and tag.uid = cCurrentUID)
    bDelTag:sensitive  = (available tag and tag.uid = cCurrentUID)
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-D
&Scoped-define SELF-NAME bVerify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bVerify wWin
ON CHOOSE OF bVerify IN FRAME FRAME-D /* Verify */
do:
  run openReview in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bViewLog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewLog wWin
ON CHOOSE OF bViewLog IN FRAME fMain /* ViewLogs */
do:
  run openLog in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAltaUid
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAltaUid wWin
ON VALUE-CHANGED OF cbAltaUid IN FRAME fMain /* ALTA UID */
or 'value-changed' of cbNPN
or 'value-changed' of cbState
or 'value-changed' of edContNotes
or 'value-changed' of fAddr
or 'value-changed' of fAddr2
or 'value-changed' of fCity
or 'value-changed' of fname
or 'value-changed' of fZip
or 'value-changed' of tglEmp
or 'value-changed' of tgldnc
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_affiliation
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_affiliation wWin
ON CHOOSE OF MENU-ITEM m_View_affiliation /* View affiliation */
DO:
  run openAffiliation in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_role_fulfillments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_role_fulfillments wWin
ON CHOOSE OF MENU-ITEM m_View_role_fulfillments /* View fulfillments */
DO:
  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  if std-ch = 'COM' 
   then
    run openRoleFulfillment in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-D
&Scoped-define SELF-NAME QualType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL QualType wWin
ON VALUE-CHANGED OF QualType IN FRAME FRAME-D
DO:
  empty temp-table qualification. 
  if qualType:input-value = "A" 
   then
    do:
      for each tqualification where tqualification.activ = true :
        create qualification. 
        buffer-copy tqualification to qualification.
      end.
     end.
   else if qualType:input-value = "I" 
    then
     do:
       for each tqualification where tqualification.activ = false :
         create qualification. 
         buffer-copy tqualification to qualification.
       end.
     end.
   else
    do:
      for each tqualification:
        create qualification. 
        buffer-copy tqualification to qualification.
      end.
    end.
  open query brwQualifications for each qualification.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-I
&Scoped-define SELF-NAME rdAgentType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rdAgentType wWin
ON VALUE-CHANGED OF rdAgentType IN FRAME FRAME-I
DO:
  empty temp-table personagentdetails. 
  if rdAgentType:input-value = "A" 
   then
    for each ttpersonagentdetails where ttpersonagentdetails.stat = "A" :
      create personagentdetails. 
      buffer-copy ttpersonagentdetails to personagentdetails.
    end.
   else if rdAgentType:input-value = "I" 
    then
     for each ttpersonagentdetails where ttpersonagentdetails.stat <> "A" :
       create personagentdetails. 
       buffer-copy ttpersonagentdetails to personagentdetails.
     end.
   else
    for each ttpersonagentdetails:
      create personagentdetails. 
      buffer-copy ttpersonagentdetails to personagentdetails.
    end.
  open query brwAgents for each personagentdetails by personagentdetails.agentId desc .
  apply 'value-changed' to brwAgents.
  run setButtons in this-procedure.     
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-G
&Scoped-define SELF-NAME rdContactType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rdContactType wWin
ON VALUE-CHANGED OF rdContactType IN FRAME FRAME-G
DO:

  empty temp-table personContact. 
  if rdContactType:input-value = "A" 
   then
    do:
      for each tpersonContact where tpersonContact.active = true :
        create personContact. 
        buffer-copy tpersonContact to personContact.
      end.
     end.
   else if rdContactType:input-value = "I" 
    then
     do:
       for each tpersonContact where tpersonContact.active = false :
         create personContact. 
         buffer-copy tpersonContact to personContact.
       end.
     end.
   else
    do:
      for each tpersonContact:
        create personContact. 
        buffer-copy tpersonContact to personContact.
      end.
    end.
    
  open query brwpersonContact for each personContact by personContact.active desc .
  run setButtons in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-E
&Scoped-define SELF-NAME roleType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL roleType wWin
ON VALUE-CHANGED OF roleType IN FRAME FRAME-E
DO:
  empty temp-table personrole. 
  if roleType:input-value = "A" 
   then
    do:
      for each tpersonrole where tpersonrole.active = true :
        create personrole. 
        buffer-copy tpersonrole to personrole.
        inooffulfillreq  = 0.
        for each treqfulfill where treqfulfill.personroleID = tpersonrole.personroleID:
          inooffulfillreq = inooffulfillreq + 1.
        end.
        assign personrole.totfulfillreq = inooffulfillreq.
      end.
    end.
  else if roleType:input-value = "I" 
   then
    do:
      for each tpersonrole where tpersonrole.active = false :
        create personrole. 
        buffer-copy tpersonrole to personrole.
        inooffulfillreq  = 0.
        for each treqfulfill where treqfulfill.personroleID = tpersonrole.personroleID:
          inooffulfillreq = inooffulfillreq + 1.
        end.
        assign personrole.totfulfillreq = inooffulfillreq.
      end.
    end.
  else
   do:
     for each tpersonrole:
       create personrole. 
       buffer-copy tpersonrole to personrole.
       inooffulfillreq  = 0.
        for each treqfulfill where treqfulfill.personroleID = tpersonrole.personroleID:
          inooffulfillreq = inooffulfillreq + 1.
        end.
        assign personrole.totfulfillreq = inooffulfillreq.
      end.
   end.
  open query brwpersonroles for each personrole.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwAffiliations
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm2/windowmn.i}

{lib/win-main.i}
{lib/win-status.i}
      
bContSave        :load-image ("images/Save.bmp").
bContSave        :load-image-insensitive("images/Save-i.bmp").
bContCanc        :load-image ("images/Cancel.bmp").
bContCanc        :load-image-insensitive("images/Cancel-i.bmp").
bRefresh         :load-image("images/Refresh.bmp").
bRefresh         :load-image-insensitive("images/Refresh-i.bmp").
bViewLog         :load-image("images/log.bmp").
bViewLog         :load-image-insensitive("images/log-i.bmp").
bPersonDelete    :load-image("images/delete.bmp").
bPersonDelete    :load-image-insensitive("images/delete-i.bmp").
bEditname        :load-image ("images/s-update.bmp").
bEditname        :load-image-insensitive("images/s-update-i.bmp").

bExportQual      :load-image ("images/s-excel.bmp").
bExportQual      :load-image-insensitive("images/s-excel-i.bmp").
bNewQual         :load-image ("images/s-add.bmp").
bNewQual         :load-image-insensitive("images/s-add-i.bmp").
bEditQual        :load-image ("images/s-update.bmp").
bEditQual        :load-image-insensitive("images/s-update-i.bmp").
bDeleteQual      :load-image ("images/s-delete.bmp").
bDeleteQual      :load-image-insensitive("images/s-delete-i.bmp").
bQualificationCopy:load-image("images/s-copy.bmp").
bQualificationCopy:load-image-insensitive("images/s-copy-i.bmp").
bDocuments       :load-image ("images/s-attach.bmp").
bDocuments       :load-image-insensitive("images/s-attach-i.bmp").
bVerify          :load-image("images/s-star.bmp").
bVerify          :load-image-insensitive("images/s-star-i.bmp").
bActive          :load-image("images/s-flag_red.bmp").
bActive          :load-image-insensitive("images/s-flag-i.bmp").
bQualificationRenew:load-image("images/s-award.bmp").
bQualificationRenew:load-image-insensitive("images/s-award-i.bmp").

bExportAff       :load-image ("images/s-excel.bmp").
bExportAff       :load-image-insensitive("images/s-excel-i.bmp").
bNewAff          :load-image("images/s-building-link.bmp").
bNewAff          :load-image-insensitive("images/s-building-link-i.bmp").
bEditAff         :load-image("images/s-update.bmp").
bEditAff         :load-image-insensitive("images/s-update-i.bmp").
bDeleteAff       :load-image ("images/s-delete.bmp").
bDeleteAff       :load-image-insensitive("images/s-delete-i.bmp").
bOpenAff         :load-image("images/s-magnifier.bmp").
bOpenAff         :load-image-insensitive("images/s-magnifier-i.bmp").

bNewRole         :load-image ("images/s-users.bmp").
bNewRole         :load-image-insensitive("images/s-users-i.bmp").
bEditRole        :load-image ("images/s-update.bmp").
bEditRole        :load-image-insensitive("images/s-update-i.bmp").
bDeleteRole      :load-image ("images/s-delete.bmp").
bDeleteRole      :load-image-insensitive("images/s-delete-i.bmp").
bRoleLog         :load-image ("images/s-log.bmp").
bRoleLog         :load-image-insensitive("images/s-log-i.bmp").
bOpenRole        :load-image("images/s-magnifier.bmp").
bOpenRole        :load-image-insensitive("images/s-magnifier-i.bmp").

bAddImg          :load-image("images/s-photo.bmp").
bDeleteImg       :load-image("images/s-delete.bmp").

bAffiliatePerson:load-image("images/s-group-link.bmp").
bAffiliatePerson:load-image-insensitive("images/s-group-link-i.bmp").

bExportPersonContact :load-image ("images/s-excel.bmp").
bExportPersonContact :load-image-insensitive("images/s-excel-i.bmp").
bNewPersonContact    :load-image ("images/s-add.bmp").
bNewPersonContact    :load-image-insensitive("images/s-add-i.bmp").
bEditPersonContact   :load-image ("images/s-update.bmp").
bEditPersonContact   :load-image-insensitive("images/s-update-i.bmp").
bExpPersonContact :load-image ("images/s-flag_red.bmp").
bExpPersonContact :load-image-insensitive("images/s-flag-i.bmp").
bMailPersonContact :load-image("images/s-email.bmp").
bMailPersonContact :load-image-insensitive("images/s-email-i.bmp").
bPersonDocuments   :load-image("images/attach.bmp").
bPersonDocuments   :load-image-insensitive("images/attach-i.bmp").

bExportTags        :load-image ("images/s-excel.bmp").
bExportTags        :load-image-insensitive("images/s-excel-i.bmp").
bNewTag            :load-image ("images/s-add.bmp").
bNewTag            :load-image-insensitive("images/s-add-i.bmp").
bEditTag           :load-image-insensitive("images/s-update-i.bmp").
bEditTag           :load-image ("images/s-update.bmp").
bDelTag            :load-image ("images/s-delete.bmp").
bDelTag            :load-image-insensitive("images/s-delete-i.bmp").

bExportAgents          :load-image ("images/s-excel.bmp").
bExportAgents          :load-image-insensitive("images/s-excel-i.bmp").
bNewAgentLink          :load-image("images/s-building-link.bmp").
bNewAgentLink          :load-image-insensitive("images/s-building-link-i.bmp").
bEditPersonagent       :load-image ("images/s-update.bmp").
bEditPersonagent       :load-image-insensitive("images/s-update-i.bmp").
bPersonAgentDeactivate :load-image ("images/s-flag_red.bmp").
bPersonAgentDeactivate :load-image-insensitive("images/s-flag-i.bmp").

{lib/brw-main-multi.i &browse-list="brwQualifications,brwAffiliations,brwPersonRoles,brwFulfillment,brwPersonContact,brwTags,brwTagDetail,brwAgents"}  

/* Note: All above events needs to named different as unique set of code 
   needs to be executed in all above scenarios. */
subscribe to "modifiedAffiliation"              anywhere.

/* Subscribe to the events, so that this screen can update itself for the
    changes executed in the documents screen. For example if a new document
    is uploaded, then the "hasDoc" flag should be checked. */
subscribe to "documentModified"                 anywhere run-procedure "refreshQualifications".

/* Subscribe to the events, so that qualification screen can refresh 
    in case review is modified  are modified. */
subscribe to "qualificationReviewModified"      anywhere run-procedure "refreshQualifications".

/* Subscribe to the events, so that qualification can refresh 
   in case fulfillments are modified. */
subscribe to "fulFillmentModified"              anywhere.

/* Subscribe to the events, so that person can chnge status on UI. */
subscribe to "personStatusModified"             anywhere.

/* Subscribe to the event in case state requirement qualification record
  is activated or deactivated. This also results in change of fulfillments
  grid. */
subscribe to "requirementModified"              anywhere.

subscribe to "roleComplianceModified"           anywhere.

/* if fulfillment are modified from first party fulfillment utility of
agent/attorneyirole using person qualification then new qualification for person will be created*/ 
subscribe to "bulkFulfillmentsModified"         anywhere run-procedure "fulFillmentModified".

subscribe to "closeWindow" anywhere.

subscribe to "updateAffiliationAgent" anywhere.

setStatusMessage("").
run setTabLabels          in this-procedure.      
run initializeObject      in this-procedure.
run displayPerson         in this-procedure.
run displayQualification  in this-procedure.
run displayAffiliation    in this-procedure.
run displayPersonRole     in this-procedure.
run displayPersonContact  in this-procedure.


openTags().

/* Select the role when called from main window Attorney tab */
if ipcEntityID <> ""
 then.
  do: 
    find first personRole where personRole.roleID = ipcEntityID no-error.
    if available personRole
     then
      do:
        reposition brwPersonRoles to rowid rowid(personRole).
        apply "value-changed" to brwPersonRoles.
      end.
  end.

{lib/get-column-width.i &col="'partnerAName'"  &var=dAffiliation   &browse-name=brwAffiliations }
{lib/get-column-width.i &col="'source'"        &var=dRole          &browse-name=brwPersonRoles    }
{lib/get-column-width.i &col="'reqdesc'"        &var=dColumnWidth  &browse-name=brwFulfillment} 
{lib/get-column-width.i &col="'qualification'"  &var=dColumnWidthq &browse-name=brwQualifications} 
{lib/get-column-width.i &col="'Contact'"  &var=dColumnWidthc &browse-name=brwpersoncontact}  
{lib/get-column-width.i &col="'UserName'"  &var=dColumnWidthu &browse-name=brwTags}
run setButtons  in this-procedure.
apply "value-changed" to brwPersonRoles.

/* doModify(false). */

/* This procedure restores the window and move it to top */
run showWindow            in this-procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addImage wWin 
PROCEDURE addImage :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Initially false */ 
  std-lo = false.
   do with frame fMain:
   
   end.
  system-dialog get-file procname title "Select the Picture File"  
    filters "PNG (*.PNG)"   "*.PNG",
            "JPG (*.JPG)"   "*.JPG",
            "GIF (*.GIF)"   "*.GIF",
            "ICO (*.ICO)"   "*.ICO",
            "JPEG (*.JPEG)"   "*.JPEG",
            "BITMAP FILES (*.BMP)"  "*.BMP"
            
    must-exist   
    use-filename 
    update std-lo.

  if not std-lo 
   then
    return.

        
  run loadImage in hPersonDataSrv(input procname,
                                  input "P",
                                  input fentityId:screen-value,
                                  output std-lo,
                                  output std-ch).
 
  if not std-lo 
   then
    do:
      message std-ch
        view-as alert-box information buttons ok.
      return no-apply.
    end.

  run GetPersonPicture in hPersonDataSrv (output chimage). 
  
  if chImage > '' then
  do:
    decdmptr = base64-decode(chImage).
  
    copy-lob from decdmptr to file chPath.

    run setPicture in this-procedure.

    os-delete value(chPath) no-error.
      
  end.                              

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects wWin  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE currentPage  AS INTEGER NO-UNDO.

  ASSIGN currentPage = getCurrentPage().

  CASE currentPage: 

    WHEN 0 THEN DO:
       RUN constructObject (
             INPUT  'adm2/folder.w':U ,
             INPUT  FRAME fMain:HANDLE ,
             INPUT  'FolderLabels':U + 'Affiliations|Qualifications|Roles|Contacts|Tags' + 'FolderTabWidth25FolderFont-1HideOnInitnoDisableOnInitnoObjectLayout':U ,
             OUTPUT h_folder ).
       RUN repositionObject IN h_folder ( 12.67 , 2.00 ) NO-ERROR.
       RUN resizeObject IN h_folder ( 15.14 , 165.00 ) NO-ERROR.

       /* Links to SmartFolder h_folder. */
       RUN addLink ( h_folder , 'Page':U , THIS-PROCEDURE ).

       /* Adjust the tab order of the smart objects. */
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE affiliatePerson wWin 
PROCEDURE affiliatePerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iAffiliationID as  integer  no-undo.

  empty temp-table taffiliation.
 
  do with frame fMain:
  end.
  
  run dialogaffiliation-ptop.w(input table tAffiliation,
                               input ipcPersonId,
                               input fname:input-value,
                               input hPersonDataSrv,
                               output std-lo). 
  if not std-lo 
   then
    return.

  run getAffiliations in hPersonDataSrv( output table taffiliation).

  empty temp-table affiliation.

  for each taffiliation:
    create affiliation.
    buffer-copy taffiliation to affiliation.
  end.

  run displayAffiliation in this-procedure.

  run setButtons in this-procedure.

  if affiliation.isctrlPerson
   then
    run refreshPersonRoles in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancelPerson wWin 
PROCEDURE cancelPerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer person for person.
  
  do with frame {&frame-name}:
  end.

  /* Undo all changes of person table */
  for first person 
    where person.personID = ipcPersonId:
    assign
        fentityID:screen-value     = person.personID
        cbAltaUid:screen-value     = person.altaUID
        cbNPN:screen-value         = person.npn
        edContNotes:screen-value   = person.notes                                                                             
        fname:screen-value         = person.dispname
        fAddr:screen-value         = person.address1
        fAddr2:screen-value        = person.address2
        fCity:screen-value         = person.city
        fZip:screen-value          = person.zip
        cFirstName                 = person.firstName
        cMiddleName                = person.middleName
        cLastName                  = person.lastName
        bContCanc:sensitive        = false 
        bContSave:sensitive        = false
        tglemp:checked             = person.internal
        tgldnc:checked             = person.doNotCall
        .
    
  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  if std-ch = 'COM' 
   then
    cbstate:screen-value = if lookup(person.state,chStateIDList) = 0 then "" else person.state.
  else
   cbstate:screen-value = person.state.
   
  end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changeStatus wWin 
PROCEDURE changeStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount   as integer     no-undo.
  define variable iQualID  as integer     no-undo.
  define variable lCheck   as logical     no-undo.
 
  do with frame {&frame-name}:
  end.

  if not available qualification
   then
    return.

  iQualID = qualification.qualificationID.

  do iCount = 1 to brwQualifications:num-iterations in frame FRAME-D:
    if brwQualifications:is-row-selected(iCount)
     then
      leave.
  end.

  if qualification.active
   then
    do:
      run deactivateQualification in hPersonDataSrv (input iQualID,
                                                     input  yes, /* Yes - validate if the qualification is in use */
                                                     output std-lo,
                                                     output std-ch).  
     
      if std-ch <> "" 
       then
        do:
          if std-ch = {&InUse} 
           then
            message "Fulfillment record exists for this qualification. Do you still want to deactivate?"
                view-as alert-box question buttons yes-no update lupdate as logical.
          else 
           return.
          
          if not lUpdate
           then 
            return.
          
          run deActivateQualification in hPersonDataSrv (input  iQualID,
                                                         input  no, /* No - do not validate if the qualification is in use */
                                                         output std-lo,
                                                         output std-ch).
          
          if not std-lo 
           then
            do:
              message std-ch 
                  view-as alert-box error buttons ok.
              return.
            end.
        end. /* if std-ch <> ""... */

    end. /* if qualification.active */  
  else
   do:
     if qualification.effectiveDate = ?
      then
       do:
         message "Effective Date must be set to activate the Qualification." 
             view-as alert-box error buttons ok.
         return.
       end.
     
     if qualification.stat ne {&GoodStanding} and qualification.stat ne {&waived}
      then
       do:
         message "Only a qualification in good standing or waved status can fully meet a requirement. Are you sure you want to continue?"
             view-as alert-box question buttons yes-no update lCheck.
          
         if not lCheck
          then
           return.
       end.
     
     run activateQualification in hPersonDataSrv(input iQualID,
                                                 output std-lo,
                                                 output std-ch).
     
     if not std-lo
      then
       do:
         message std-ch
             view-as alert-box error buttons ok.
         return.
       end.
   end.

  run getQualifications in hPersonDataSrv(output table tQualification).
  
  empty temp-table qualification.

  for each tQualification:
    create qualification.
    buffer-copy tqualification to qualification.
  end.

  run displayQualification in this-procedure.

  run roleComplianceModified (input ipcpersonID ) .
  run setButtons in this-procedure.

  publish "fulfillmentModifiedRole" .  
  
  for first qualification
    where qualification.qualificationID = iQualID:
    std-ro = rowid(qualification).
  end.

  brwQualifications:set-repositioned-row(iCount,"ALWAYS") no-error.
  reposition brwQualifications to rowid std-ro no-error.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow wWin 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if valid-handle(hPersonDataSrv)
   then
    delete procedure hPersonDataSrv.
   
  hPersonDataSrv = ?. 
  
  if valid-handle(IMAGE-2)
   then
    delete object IMAGE-2.
   
  IMAGE-2 = ?.
       
  if valid-handle(hComLog)
   then
    run closeWindow in hComLog.
  hComLog = ?.
     

  if valid-handle(hDocument)
   then
    run closeWindow in hDocument.
  hDocument = ?. 

  /* Close parent and child windows.
     Also clear the temp-table in comstart.w  */
  publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copyQualification wWin 
PROCEDURE copyQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tQualification for tQualification.

  define variable iQualID   as integer  no-undo.
  define variable iReviewID as integer  no-undo.
  
  do with frame {&frame-name}:
  end.
  
  if not available qualification 
   then
    return.
 
  empty temp-table tQualification.
  create tQualification.
  buffer-copy qualification to tQualification.
  tQualification.qualificationID = 0.
  
  run dialogqualification.w (input hPersonDataSrv,
                             input {&PersonCopy},
                             input {&PersonCode},
                             input person.personID,
                             input person.state,
                             input table tQualification,
                             output std-lo,
                             output iQualID,
                             output iReviewID).
  
  if not std-lo 
   then
    return.

  run getQualifications in hPersonDataSrv(output table tQualification).
  
  empty temp-table qualification.

  for each tQualification:
    create qualification.
    buffer-copy tqualification to qualification.
  end.
  
  run displayQualification in this-procedure.

  for first qualification
    where qualification.qualificationID = iQualID:
    std-ro = rowid(qualification).
  end.

  reposition brwqualifications to rowid std-ro.
  brwqualifications:get-repositioned-row() in frame FRAME-D.
  
  run setButtons in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deactivatepersonagent wWin 
PROCEDURE deactivatepersonagent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var pipersonagentID as integer   no-undo.
 def var cstat            as character no-undo.
 def var olsuccess       as logical   no-undo.

 if not available personagentdetails
  then
   return.
 do with frame FRAME-I:
 end.
 pipersonagentID = personagentdetails.personagentID .

    
 if personagentdetails.stat = "A"  
  then
   do:
     run deactivatepersonagent in hPersonDataSrv(input pipersonagentID,
                                                 input today,
                                                 output olsuccess,
                                                 output std-ch).
                                            
   end.
  else
   do:
     run deactivatepersonagent in hPersonDataSrv(input pipersonagentID,
                                                 input ?,
                                                 output olsuccess,
                                                 output std-ch).
   end.

 if not olsuccess
  then
   return.
  publish "refreshpersonnames" (input olsuccess).
  
  publish "refreshAgentPerson".  /*refresh the personagent screens*/
 
  run updatedpersonagentstatus in hPersonDataSrv(input pipersonagentID, input cstat)   .
  
  empty temp-table ttpersonagentdetails.
  run getPersonagentDetails in hPersonDataSrv(output table ttpersonagentdetails).
  
  for first ttpersonagentdetails where ttpersonagentdetails.personagentID = pipersonagentID :
    if ttpersonagentdetails.stat = "A"
     then
      rdAgentType:screen-value = "A".
     else
      rdAgentType:screen-value = "I".
  end.
 
  apply 'value-changed' to rdAgentType.
  apply 'value-changed' to brwAgents. 
  
  for first personagentdetails where personagentdetails.personagentID = pipersonagentID:
    std-ro = rowid(personagentdetails).
  end.

  reposition brwAgents to rowid std-ro.
  run setButtons in this-procedure.
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteAffiliation wWin 
PROCEDURE deleteAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable lCtrlPerson as logical no-undo.
  define variable dExpDate as date no-undo.
  
  define buffer taffiliation  for taffiliation.

  if not available affiliation
   then
    return.

  /* adding data temporarily as it's required after deleting affiliation record */
  find current affiliation no-lock no-error.
  if available affiliation 
   then
    do:
      create  taffiliation.
      buffer-copy  affiliation to taffiliation.
    end.

  if affiliation.partnerAType = {&AgentCode}
   then
    do:
      if affiliation.expdate = ? 
       then
        dExpDate = today.
      else
       dExpDate = ?.
       
       
      run deactivatepersonagent in hPersonDataSrv (input affiliation.affiliationID,
                                                   input dExpDate,
                                                   output std-lo,
                                                   output std-ch).
                                             
      
      if not std-lo
       then
        return.
        
       publish "refreshAgentPerson".  /*refresh the personagent screens*/
      
    end.
  else
   do:
     
     message "Highlighted Affiliation will be deleted." skip "Do you want to continue?" 
         view-as alert-box question buttons yes-no title "Delete Affiliation" update std-lo.
       
     if not std-lo 
      then
       return.
     
     lCtrlPerson = affiliation.isctrlPerson.
     
     run deleteAffiliation in hPersonDataSrv(input affiliation.affiliationID,
                                             input yes,  /* Yes - validate if the Person is in use */
                                             output std-lo,                      
                                             output std-ch).
     if not std-lo 
      then
       do:
         if std-ch = {&InUse} 
          then
           do:
             message "The Qualification of this person are used to fulfill the requirement of the Agent. Do you still want to delete?"
                      view-as alert-box question buttons yes-no update lupdate as logical.
             
             if not lUpdate
              then
               return.
               
             run deleteAffiliation in hPersonDataSrv(input affiliation.affiliationID,
                                                     input no, 
                                                     output std-lo,                      
                                                     output std-ch).
           end.
         else
          do:
            message std-ch 
              view-as alert-box error buttons ok.
            return.
          end.
       end.

     for first tAffiliation:
       if std-lo
        then
         if taffiliation.partnerAType = 'P'
          then
           do:
             if taffiliation.partnerA = ipcPersonID
              then
               publish "modifiedAffiliation" (input taffiliation.partnerB).
             else
              publish "modifiedAffiliation" (input taffiliation.partnerA).
           end.
          else
           publish "modifiedAffiliation" (input taffiliation.partnerA).

       if taffiliation.partnerAType = 'O' and taffiliation.partnerBType = 'P'
        then
         do:
           run modifiedAffiliation in this-procedure (input ipcPersonId) .
           publish "refreshAgentPerson".  /*refresh the personagent screens*/
         end.
     end.

     if lCtrlPerson
      then
       do:
         run refreshPersonRoles in this-procedure.
          /* refreshPersonRoles procedure will refresh all the grids, so no need to 
             do it again. So return statement is written. */
         return.
       end. 
   end. /* else*/  
   
   
  empty temp-table taffiliation.
  run getAffiliations in hPersonDataSrv(output table taffiliation). 
  
  empty temp-table affiliation.

  for each taffiliation:
    create affiliation.
    buffer-copy taffiliation to affiliation.
  end.
  
  run displayAffiliation in this-procedure.

  run setButtons in this-procedure.
  
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteImage wWin 
PROCEDURE deleteImage :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   run server\deletepersonpicture.p( input fentityId:screen-value in frame fMain,
                                     output std-lo,
                                     output std-ch).
  if not std-lo 
   then
    do:
       return no-apply.
    end. 
  IMAGE-2:load-image("") no-error.   
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deletePerson wWin 
PROCEDURE deletePerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  message "Are you sure you want to delete the selected record?"
    view-as alert-box question buttons yes-no update std-lo.
  
  if not std-lo 
   then
    return.

  run "deletePerson" in hPersonDataSrv (output std-lo,
                                        output std-ch).
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.

   publish "refreshAgentPerson". /*refresh the personagent screens*/
   
  /* Once person is deleted, close the window. */
  run closeWindow in this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deletePersonRole wWin 
PROCEDURE deletePersonRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   define variable lSuccess     as logical  no-undo.
  do with frame {&frame-name}:
  end.
 
  if not available personRole
   then
    return.

  message "Highlighted Role will be deleted." skip "Do you want to continue?" 
      view-as alert-box question buttons yes-no title "Delete PersonRole" update std-lo.
    
  if not std-lo 
   then
    return.

  run deletepersonRole in hPersonDataSrv(input personRole.personRoleID,
                                         output std-lo,                      
                                         output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.

  publish "deactivateAttorney" (personrole.roleID , output lSuccess).
  run getpersonRole in hPersonDataSrv(output table tpersonRole). 
  
  empty temp-table personRole.

  for each tpersonrole:
    create personrole.
    buffer-copy tpersonrole to personrole.

    personRole.sequence = getComSeq(personRole.ComStat).
  end.
  
  run displaypersonRole in this-procedure.
  run setButtons in this-procedure.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteQualification wWin 
PROCEDURE deleteQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  if available qualification
   then
    message "Highlighted Person Qualification will be deleted." skip "Do you want to continue?" 
        view-as alert-box question buttons yes-no title "Delete Person Qualification" update std-lo.
    
  if not std-lo 
   then
    return.

  run deleteQualification in hPersonDataSrv(input qualification.qualificationID,
                                            output std-lo,                      
                                            output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.

  run getQualifications in hPersonDataSrv(output table tQualification). 
  
  empty temp-table qualification.

  for each tQualification:
    create qualification.
    buffer-copy tQualification to qualification.
  end.
  
  run displayQualification in this-procedure.
  run setButtons           in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayAffiliation wWin 
PROCEDURE displayAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  open query brwAffiliations for each affiliation by affiliation.partnerAName.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayPerson wWin 
PROCEDURE displayPerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer person for person.
 
  do with frame {&frame-name}:
  end.

  find first Person where Person.PersonID = ipcPersonId no-error.
  if not available Person 
   then
    do:
      message "Internal Error of missing data."
          view-as alert-box error buttons ok.
      return.
    end.

  assign 
      fentityID:screen-value     =  person.personID
      cbAltaUid:screen-value     =  person.altaUID
      cbNPN:screen-value         =  person.npn
      edContNotes:screen-value   =  person.notes                                                                             
      fname:screen-value         =  person.dispname
      fAddr:screen-value         =  person.address1
      fAddr2:screen-value        =  person.address2
      fCity:screen-value         =  person.city
      fZip:screen-value          =  person.zip
      tglemp:checked             =  person.internal   
      tgldnc:checked             =  person.doNotCall  
      .
  
  if can-find(first personagentdetails where personagentdetails.statdesc = "Active")
     and not tglemp:checked
   then
    assign tglEmp:sensitive = false.
   else
    assign tglEmp:sensitive = true.
    
  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  if std-ch = 'COM' 
   then
    cbstate:screen-value = if lookup(person.state,chStateIDList) = 0 then "" else person.state no-error.
  else
   cbstate:screen-value = person.state no-error.
   
  /* Handling all the errors that might come while storing the image in lImage variable. */
  if lImage <> ""  and 
     lImage <> "?" and 
     lImage <> ?   and 
     lImage <> "!ISO8859-1!?" 
   then
    do:
      decdmptr = base64-decode(lImage).
      copy-lob from decdmptr to file chPath.
      run setPicture in this-procedure.
      os-delete value(chPath).
    end.    

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayPersonContact wWin 
PROCEDURE displayPersonContact :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frame-G:
  end.
  apply 'value-changed' to rdContactType. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayPersonRole wWin 
PROCEDURE displayPersonRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frame-E:
  end.
  open query brwPersonRoles for each personRole by stateId.
  apply "value-changed" to brwPersonRoles.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayQualification wWin 
PROCEDURE displayQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  open query brwQualifications for each qualification.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE documentModified wWin 
PROCEDURE documentModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameter Definitions */
  define input parameter ipiQualificationID as integer no-undo.
  
  /* Local Variable */
  define variable iCount  as integer  no-undo.

  if not can-find(first qualification where qualification.qualificationID = ipiQualificationID)
   then
    return.  
  
  do with frame {&frame-name}:
  end.
  
  do iCount = 1 to brwQualifications:num-iterations in frame FRAME-D:
    if brwQualifications:is-row-selected(iCount)
     then
      leave.
  end.

  run refreshData in this-procedure (yes, /*  NeedQualification  */  
                                     no,  /*  NeedAffiliation  */    
                                     no, /*  NeedPersonAgent*/
                                     no,  /*  NeedPersonrole   */
                                     no, /*  NeedReqFulfill  */
                                     no , /*  NeedPersonContact  */ 
                                     no ). /* NeedPersonAgentDetails */
  for first qualification 
    where qualification.qualificationID = ipiQualificationID:
    std-ro = rowid(qualification).
  end.

  brwQualifications:set-repositioned-row(iCount,"ALWAYS") no-error.
  reposition brwQualifications to rowid std-ro no-error.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE editPersonName wWin 
PROCEDURE editPersonName :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  find first Person where Person.PersonID = ipcPersonId no-error.
  if not available Person 
   then
    return.
      
  if cFirstName = "" 
   then
    assign 
        cFirstName   = Person.firstName
        cMiddleName  = Person.middleName
        cLastName    = Person.lastName
        cDisplayName = Person.dispname
        .
     
  run dialogeditname.w (input-output ipcPersonId,
                        input-output cFirstName,
                        input-output cMiddleName,
                        input-output cLastName,
                        input-output cDisplayName,
                        output       std-lo).
    
  if std-lo then
   do:
     if not Person.dispname = cDisplayName
      then
       do:
         fname:screen-value = cDisplayName.
       end.
        
     if fname:input-value = "" 
      then
       do:
         if cMiddleName <> "":U 
          then
           fname:screen-value = cFirstName  + " ":U  +
                                cMiddleName + " ":U  + 
                                cLastName.
          else 
           fname:screen-value = cFirstName  + " ":U  +
                                cLastName.
       end.
     assign
        Person.firstName  = cFirstName
        Person.middleName = cMiddleName
        Person.lastName   = cLastName
        Person.dispname   = cDisplayName   .
     
     run displayPerson in this-procedure. 
     publish "refreshAgentPerson".  /*refresh the personagent screens*/
   end.
 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave wWin 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  find first person where person.personID = ipcPersonId no-error.
 
  if not available person 
   then
    return.

    
  /* Enable the save button as soon as any person related information is changed. */
  bContSave:sensitive = not (person.NPN      = cbNPN:input-value       and
                            person.altaUID   = cbAltaUid:input-value   and
                            person.dispname  = fname:input-value       and
                            person.address1  = fAddr:input-value       and
                            person.address2  = fAddr2:input-value      and
                            person.city      = fcity:input-value       and
                            person.state     = cbState:input-value     and
                            person.zip       = fZip:input-value        and
                            person.notes     = edContNotes:input-value and
                            person.internal  = tglemp:input-value      and
                            person.doNotCall = tgldnc:input-value )
                            no-error.  

  bContCanc:sensitive = bContSave:sensitive.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fentityId fname fAddr fAddr2 fCity cbState fZip tgldnc tglEmp cbNPN 
          cbAltaUid edContNotes 
      WITH FRAME fMain IN WINDOW wWin.
  ENABLE bPersonDocuments bAddImg bDeleteImg fname fAddr fAddr2 fCity cbState 
         fZip tgldnc tglEmp cbNPN cbAltaUid bEditname bContSave edContNotes 
         bPersonDelete bRefresh bViewLog bContCanc RECT-64 
      WITH FRAME fMain IN WINDOW wWin.
  VIEW FRAME fMain IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW FRAME FRAME-A IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-A}
  ENABLE brwAffiliations bAffiliatePerson bNewAff bDeleteAff bEditAff 
         bExportAff bOpenAff 
      WITH FRAME FRAME-C IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-C}
  DISPLAY QualType 
      WITH FRAME FRAME-D IN WINDOW wWin.
  ENABLE QualType bQualificationRenew brwQualifications bQualificationCopy 
         bVerify bDeleteQual bEditQual bExportQual bNewQual bActive bDocuments 
      WITH FRAME FRAME-D IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-D}
  DISPLAY roleType 
      WITH FRAME FRAME-E IN WINDOW wWin.
  ENABLE roleType brwPersonRoles brwFulfillment bOpenRole bRoleLog bNewRole 
         bDeleteRole bEditRole 
      WITH FRAME FRAME-E IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-E}
  DISPLAY rdContactType 
      WITH FRAME FRAME-G IN WINDOW wWin.
  ENABLE bMailPersonContact rdContactType brwpersoncontact bEditPersonContact 
         bExpPersonContact bNewPersonContact bExportPersonContact 
      WITH FRAME FRAME-G IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-G}
  ENABLE brwTags bExportTags bNewTag 
      WITH FRAME FRAME-H IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-H}
  DISPLAY rdAgentType 
      WITH FRAME FRAME-I IN WINDOW wWin.
  ENABLE bEditPersonagent rdAgentType brwAgents bExportAgents bNewAgentLink 
      WITH FRAME FRAME-I IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-I}
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exitObject wWin 
PROCEDURE exitObject :
/*------------------------------------------------------------------------------
  Purpose:  Window-specific override of this procedure which destroys 
            its contents and itself.
    Notes:  
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.
  return.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE expirePersonContact wWin 
PROCEDURE expirePersonContact :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var iPersoncontactID as int no-undo.
  define variable iCount as integer     no-undo.
  define variable lIsExpire as logical no-undo.
  
  if not available personcontact 
   then
    return.

  do with frame FRAME-G:
  end.
   
  if personcontact.expirationdate = ?  
   then
    do:
      run deactivatepersoncontact in hPersonDataSrv (input personcontact.personcontactID,
                                                     input today,
                                                     output std-lo,
                                                     output std-ch).
    end.
  else
   do:
     run deactivatepersoncontact in hPersonDataSrv (input personcontact.personcontactID,
                                                    input ?,
                                                    output std-lo,
                                                    output std-ch).
   end.
   
 if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box error buttons ok.
      return.
    end.
   
   publish "refreshAgentPerson".  /*refresh the personagent screens*/
   
 empty temp-table tpersoncontact.
 run refreshData in this-procedure (no,       /* iplNeedQualification */
                                     no,      /* iplNeedAffiliation   */
                                     no,      /* iplNeedpersonagents   */
                                     no,       /* iplNeedPersonRole    */
                                     no,      /* iplNeedReqFulfill    */
                                     yes , /*  NeedPersonContact  */ 
                                     no ). /* NeedPersonAgentDetails */
  apply 'value-changed' to rdContactType.
  run setButtons in this-procedure.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData wWin 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter cType as character no-undo.

  define variable hTableHandle as handle no-undo.
 
  define buffer affiliation    for affiliation.
  define buffer tAffiliation   for tAffiliation.
  define buffer qualification  for qualification.
  define buffer tQualification for tQualification.
  define buffer personcontact  for personcontact.
  define buffer texppersoncontact for texppersoncontact.
  define buffer tag             for tag.
  define buffer tTag            for tTag.
  define buffer personagentdetails            for personagentdetails.
  define buffer ttpersonagentdetails            for ttpersonagentdetails.
  
  empty temp-table tAffiliation.
  empty temp-table tQualification.  
  empty temp-table texppersoncontact.
  empty temp-table tTag.
  
  if cType = "A" 
   then
    do:
      if query brwAffiliations:num-results = 0 
       then
        do: 
          message "There is nothing to export"
            view-as alert-box warning buttons ok.
          return.
        end.

      for each affiliation no-lock:
        create taffiliation.
        buffer-copy affiliation to taffiliation.
        taffiliation.partnerAName = getEntityName(affiliation.partnerA,
                                                  affiliation.partnerAType,
                                                  affiliation.partnerAName,
                                                  affiliation.partnerBName).
        taffiliation.partnerA     = getEntityID(affiliation.partnerAType,
                                                affiliation.partnerA,
                                                affiliation.partnerB).
        taffiliation.partnerAType  = getEntity(affiliation.partnerAType).                                                
      end.
      
      publish "GetReportDir" (output std-ch).
      
      hTableHandle = temp-table taffiliation:handle.
      
      run util/exporttable.p (table-handle hTableHandle,
                              "taffiliation",
                              "for each taffiliation ",
                              "isCtrlPerson,partnerAName,partnerA,partnerAType",
                              "Control Person,Name,ID,Type",
                              std-ch,
                              "affiliations-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                              true,
                              output std-ch,
                              output std-in).
    end.
  else if cType = "Q"
   then
    do:
      if query brwQualifications:num-results = 0 
       then
        do: 
          message "There is nothing to export"
            view-as alert-box warning buttons ok.
          return.
        end.
      
      for each qualification no-lock:
        create tQualification.
        buffer-copy qualification to tQualification.
        tQualification.stat =  getQualificationDesc(qualification.stat).
      end.
      
      publish "GetReportDir" (output std-ch).
      
      hTableHandle = temp-table tQualification:handle.
      
      run util/exporttable.p (table-handle hTableHandle,
                              "tQualification",
                              "for each tQualification ",
                              "active,stateID,Qualification,QualificationNumber,stat,effectiveDate,expirationDate",
                              "Active,State ID,Qualification,Number,Status,Effective,Expiration",
                              std-ch,
                              "qualifications-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                              true,
                              output std-ch,
                              output std-in).
                              
    end.
  else if cType = "C"
   then
    do:
      if query brwpersoncontact:num-results = 0 
       then
        do: 
          message "There is nothing to export"
            view-as alert-box warning buttons ok.
          return.
        end.
      
      for each personcontact no-lock:
        create texppersoncontact.
        buffer-copy personcontact to texppersoncontact.
        texppersoncontact.contactType = getContactType(personcontact.contactType).
        texppersoncontact.uid = getPersonName(personContact.uid).
      end.
      
      publish "GetReportDir" (output std-ch).
      
      hTableHandle = temp-table texppersoncontact:handle.
      
      run util/exporttable.p (table-handle hTableHandle,
                              "texppersoncontact",
                              "for each texppersoncontact ",
                              "active,contacttype,contactSubType,contactID,uid,createDate",
                              "Active,Contact Type,Sub Type,Contact,Created By,Created Date",
                              std-ch,
                              "personcontact-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                              true,
                              output std-ch,
                              output std-in).
                              
    end.
  else if cType = "T"
   then
    do:
      if query brwTags:num-results = 0 
       then
        do: 
          message "There is nothing to export"
            view-as alert-box warning buttons ok.
          return.
        end.
      
      for each tag no-lock:
        create tTag.
        buffer-copy tag to tTag.
      end.
      
      publish "GetReportDir" (output std-ch).
      
      hTableHandle = temp-table tTag:handle.
      
      run util/exporttable.p (table-handle hTableHandle,
                              "tTag",
                              "for each tTag ",
                              "name,createddate,userName",
                              "Tag Name,Tag Date,User Name",
                              std-ch,
                              "personcontact-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                              true,
                              output std-ch,
                              output std-in).
                              
    end.
  else if cType = "I"
   then
    do:
      if query brwAgents:num-results = 0 
       then
        do: 
          message "There is nothing to export"
            view-as alert-box warning buttons ok.
          return.
        end.
      
      publish "GetReportDir" (output std-ch).
      
      hTableHandle = temp-table personagentdetails:handle.
      
      run util/exporttable.p (table-handle hTableHandle,
                              "personagentdetails",
                              "for each personagentdetails",
                              "statedesc,agentID,name,statdesc,jobTitle",
                              "State,Agent ID,Name,Status,Job Title",
                              std-ch,
                              "Agents-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                              true,
                              output std-ch,
                              output std-in).
                              
    end.  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fulFillmentModified wWin 
PROCEDURE fulFillmentModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:   if fullfilment are modified then qualification will also get modified 
           fullfilment can be modified by agent,company or attorney
           so person need to refresh if any of then update fullfilments
------------------------------------------------------------------------------*/
  define input parameter ipcEntityID     as character no-undo.
      
  if (ipcEntityID ne "0") and  
     (ipcEntityID ne fentityId:input-value in frame fMain) and                    /* do not reresh if not concerned person*/
     (not can-find(first qualification where qualification.inUseCompany and qualification.entityID eq ipcEntityID))   /*  do not refresh if company update the fulfilment but person does not have any qualification used by compny */
   then
    return.

  if fentityId:input-value in frame fMain = ipcEntityID 
   then
    run refreshData in this-procedure (yes,    /* iplNeedQualification */
                                       no,     /* iplNeedAffiliation   */
                                       no,     /*  iplneedAgentPerson*/
                                       yes,    /* iplNeedPersonRole    */ 
                                       yes,   /* iplNeedReqFulfill    */
                                       no , /*  NeedPersonContact  */ 
                                       no ). /* NeedPersonAgentDetails */
     
  apply "value-changed" to brwQualifications in frame FRAME-D.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData wWin 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwQualifications.  
  close query brwAffiliations.
  close query brwPersonRoles.
  close query brwPersonContact.

  empty temp-table tperson.
  empty temp-table tqualification.
  empty temp-table tpersonrole.
  empty temp-table taffiliation.
  empty temp-table tpersoncontact.
  empty temp-table ttpersonagentdetails.
  
    
  /* Get person details information in data modal from server. */
  run getPersonDetails in hPersonDataSrv(output table tperson,
                                         output table tqualification,
                                         output table taffiliation,
                                         output table tpersonrole,
                                         output table tReqfulfill,
                                         output table tpersoncontact,
                                         output table ttpersonagentdetails,
                                         output std-lo,
                                         output std-ch). 
  if not std-lo 
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.

  /* Remove client data. */
  empty temp-table person.
  empty temp-table qualification.
  empty temp-table personrole.
  empty temp-table affiliation.
  empty temp-table reqfulfill.
  empty temp-table personcontact.
  empty temp-table personagentdetails.

  /* Update the local copy of data. */
  for each tperson:
    create person.
    buffer-copy tperson to person.
  end.
 
  for each taffiliation:
    create affiliation.
    buffer-copy taffiliation to affiliation.
  end.

  for each tqualification:
    create qualification.
    buffer-copy tqualification to qualification.
  end.

  for each tpersonrole:
    create personrole.
    buffer-copy tpersonrole to personrole.
    inooffulfillreq  = 0.
    for each treqfulfill where treqfulfill.personroleID = tpersonrole.personroleID:
       inooffulfillreq = inooffulfillreq + 1.
    end.
    assign personrole.totfulfillreq = inooffulfillreq.

    personRole.sequence = getComSeq(personRole.ComStat).
  end.
  
  find first personrole no-error.
  if avail personrole then
  do:
    for each treqfulfill where treqfulfill.personroleID = personrole.personroleID:
      create reqfulfill.
      buffer-copy treqfulfill to reqfulfill.
    end.
  end.
  
  for each tpersoncontact:
    create personcontact.
    buffer-copy tpersoncontact to personcontact.
  end.

  for each ttpersonagentdetails:
    create personagentdetails.
    buffer-copy ttpersonagentdetails to personagentdetails.
  end.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initializeObject wWin 
PROCEDURE initializeObject :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
  /* Code placed here will execute PRIOR to standard behavior. */
  {&window-name}:window-state = 2. /* Minimized */

  run super.
  
  {&window-name}:max-width-pixels  = session:width-pixels.
  {&window-name}:max-height-pixels = session:height-pixels.
  {&window-name}:min-width-pixels  = {&window-name}:width-pixels.
  {&window-name}:min-height-pixels = {&window-name}:height-pixels.
   
  run GetPersonPicture in hPersonDataSrv (output chimage).
  if chimage > ""
   then lImage = chimage.

  std-ch = "".
  publish "GetTempDir" (output std-ch).
  chPath = std-ch + "\personlogo".

  std-ch = "".
  
  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  if std-ch = 'COM' 
   then
    do:
    
      publish "GetStateIDList"(input ",",
                               output chStateIDList,
                               output std-lo,
                               output std-ch).
      
      if not std-lo 
       then
        do:
          message std-ch
            view-as alert-box error buttons ok.
          return.
        end.
        cbState:list-items in frame {&frame-name} = chStateIDList.
    end.
  else
   do:
     {lib/get-full-state-list.i &combo=cbState &useSingle=true &ALL=true}
     cbState:delete("ALL").
   end.
 
  run getEntityName in hPersonDataSrv (output std-ch).
  wWin:title = wWin:title  + " - " + std-ch. 
  
  run selectPage in this-procedure(if ipiPageNo > 0 then ipiPageNo else 1).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifiedAffiliation wWin 
PROCEDURE modifiedAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcPartnerB as character no-undo.

  if ipcPartnerB = ipcPersonId 
   then
    do:
      run refreshData in this-procedure (no,       /* iplNeedQualification */
                                         yes,      /* iplNeedAffiliation   */
                                         yes,       /*  NeedPersonAgent  */ 
                                         yes,      /* iplNeedPersonRole    */
                                         yes,     /* iplNeedReqFulfill    */
                                         no , /*  NeedPersonContact  */ 
                                         no ). /* NeedPersonAgentDetails */
      run setButtons in this-procedure.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyAffiliation wWin 
PROCEDURE modifyAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount             as integer     no-undo.
  define variable iAffiliationID     as integer     no-undo.

  
  empty temp-table taffiliation.
 
  if not available affiliation 
   then
    return.

  do with frame {&frame-name}:
  end.
   
  do iCount = 1 to brwAffiliations:num-iterations in frame FRAME-C:
    if brwAffiliations:is-row-selected(iCount)
     then
      leave.
  end.

  create taffiliation.
  buffer-copy affiliation to taffiliation.
  iAffiliationID = affiliation.affiliationID.
  
  if affiliation.partnerAType = {&OrganizationCode} 
   then
    run dialogaffiliation-ptoo.w(input table tAffiliation,
                                 input ipcPersonId,
                                 input fname:input-value,
                                 input hPersonDataSrv,
                                 output std-lo).
  else
   run dialogaffiliation-ptop.w(input table tAffiliation,
                                input ipcPersonId,
                                input fname:input-value,
                                input hPersonDataSrv,
                                output std-lo).
                                
  if not std-lo 
   then
    return.

  run getAffiliations in hPersonDataSrv(output table taffiliation).
  
  empty temp-table affiliation.

  for each taffiliation:
    create affiliation.
    buffer-copy taffiliation to affiliation.
  end.
  
  run displayAffiliation in this-procedure.

  run setButtons in this-procedure.

  if affiliation.isctrlPerson
   then
    run refreshPersonRoles in this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyPersonAgent wWin 
PROCEDURE modifyPersonAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount           as integer       no-undo.
 
  empty temp-table tpersonagentdetails.
 
  if not available personagentdetails 
   then
    return.

  do with frame FRAME-I:
  end.
   
  do iCount = 1 to brwAgents:num-iterations in frame FRAME-I:
    if brwAgents:is-row-selected(iCount)
     then
      leave.
  end.

  create tpersonagentdetails.
  buffer-copy personagentdetails to tpersonagentdetails.
  cAgentID =  tpersonagentdetails.agentID.
    
  run dialoglinkagent.w (input 'Modify',
                         input fentityID:input-value in frame {&frame-name},
                         input table tpersonagentdetails,
                         output cAgentID,
                         output std-lo).
                         
  if not std-lo 
   then
    return.
  empty temp-table ttpersonagentdetails.
  run getPersonagentDetails in hPersonDataSrv(output table ttpersonagentdetails).
  
  for first ttpersonagentdetails where ttpersonagentdetails.agentID = cAgentID :
    if ttpersonagentdetails.stat = "A"
     then
      rdAgentType:screen-value = "A".
     else
      rdAgentType:screen-value = "I".
  end.
 
  apply 'value-changed' to rdAgentType.
  apply 'value-changed' to brwAgents.
  
  for first personagentdetails where personagentdetails.agentID = cAgentID:
    std-ro = rowid(personagentdetails).
  end.

  reposition brwAgents to rowid std-ro.
  run setButtons in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyPersonContact wWin 
PROCEDURE modifyPersonContact :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var iPersoncontactID as int no-undo.
  define variable iCount           as integer     no-undo.
 
  empty temp-table ttpersoncontact.
 
  if not available personcontact 
   then
    return.

  do with frame FRAME-G:
  end.
   
  do iCount = 1 to brwpersoncontact:num-iterations in frame FRAME-G:
    if brwpersoncontact:is-row-selected(iCount)
     then
      leave.
  end.

  create ttpersoncontact.
  buffer-copy personcontact to ttpersoncontact.
  
  iPersoncontactID = personcontact.personcontactID.
    
  run dialognewpersoncontact.w  ( input 'Modify',
                                  input hPersonDataSrv,
                                  input fentityID:input-value in frame {&frame-name},
                                  input fname:input-value in frame {&frame-name},
                                  input table ttpersoncontact,
                                  output std-lo).
                                  
                                    
  if not std-lo 
   then
    return.
   publish "refreshAgentPerson".  /*refresh the personagent screens*/  
   
  empty temp-table tpersoncontact.
  run refreshData in this-procedure (no,       /* iplNeedQualification */
                                     no,      /* iplNeedAffiliation   */
                                     no,      /* iplNeedpersonagents   */
                                     no,       /* iplNeedPersonRole    */
                                     no,      /* iplNeedReqFulfill    */
                                     yes , /*  NeedPersonContact  */ 
                                     no ). /* NeedPersonAgentDetails */
  
  apply 'value-changed' to rdContactType.
  run setButtons in this-procedure.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyPersonRole wWin 
PROCEDURE modifyPersonRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount           as integer     no-undo.
  define variable opiPersonRoleID  as integer     no-undo.
  define variable iPersonRoleID    as integer     no-undo.

  empty temp-table tpersonRole.
 
  if not available personRole 
   then
    return.

  do with frame {&frame-name}:
  end.
   
  do iCount = 1 to brwPersonRoles:num-iterations in frame FRAME-E:
    if brwPersonRoles:is-row-selected(iCount)
     then
      leave.
  end.

  create tpersonRole.
  buffer-copy personRole to tpersonRole.
  iPersonRoleID = personRole.personRoleID.

  run dialognewpersonrole.w  ( input hPersonDataSrv,
                               input fentityID:input-value,
                               input fname:input-value,
                               input table tpersonrole,
                               output opiPersonRoleID,
                               output std-lo). 
  
  if not std-lo 
   then
    return.

  run getpersonRole in hPersonDataSrv(output table tpersonRole).
 
  empty temp-table personRole.

  for each tpersonrole:
    create personrole.
    buffer-copy tpersonrole to personrole.

    personRole.sequence = getComSeq(personRole.ComStat).
  end.
  
 
  for first personRole
    where personRole.personRoleID = iPersonRoleID:
    std-ro = rowid(personRole).
  end.

  brwPersonRoles:set-repositioned-row(iCount,"ALWAYS").
  reposition brwPersonRoles to rowid std-ro.  

  run setButtons in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyQualification wWin 
PROCEDURE modifyQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount           as integer     no-undo.
  define variable iQualID          as integer     no-undo.
  define variable iReviewID        as integer     no-undo.
  define variable iQualificationID as integer     no-undo.

  empty temp-table tQualification.
 
  if not available qualification 
   then
    return.

  do with frame {&frame-name}:
  end.
   
  do iCount = 1 to brwQualifications:num-iterations in frame FRAME-D:
    if brwQualifications:is-row-selected(iCount)
     then
      leave.
  end.

  create tQualification.
  buffer-copy qualification to tQualification.
  iQualificationID = qualification.qualificationID.
  
  run dialogqualification.w (input hPersonDataSrv,
                             input {&PersonModify},
                             input {&PersonCode},             
                             input fentityId:input-value, 
                             input cbstate:input-value, 
                             input table tQualification,
                             output std-lo,  
                             output iQualID,
                             output iReviewID).
  
  if not std-lo 
   then
    return.

  run getQualifications in hPersonDataSrv(output table tQualification).
 
  empty temp-table qualification.

  for each tQualification:
    create qualification.
    buffer-copy tqualification to qualification.
  end.

  run displayQualification in this-procedure.

  for first qualification
    where qualification.qualificationID = iQualificationID:
    std-ro = rowid(qualification).
  end.

  brwQualifications:set-repositioned-row(iCount,"ALWAYS").
  reposition brwQualifications to rowid std-ro.  
  

  run roleComplianceModified (input ipcPersonID ) .  
  run setButtons in this-procedure.
  
  publish "fulfillmentModifiedRole" .
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newAffiliation wWin 
PROCEDURE newAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iAffiliationID as  integer  no-undo.

  empty temp-table taffiliation.
 
  do with frame fMain:
  end.
  
  run dialogaffiliation-ptoo.w(input table tAffiliation,
                               input ipcPersonId,
                               input fname:input-value,
                               input hPersonDataSrv,
                               output std-lo). 
  if not std-lo 
   then
    return.

  run getAffiliations in hPersonDataSrv( output table taffiliation).

  empty temp-table affiliation.

  for each taffiliation:
    create affiliation.
    buffer-copy taffiliation to affiliation.
  end.

  run displayAffiliation in this-procedure.
 
  run setButtons in this-procedure.

  if affiliation.isctrlPerson
   then
    run refreshPersonRoles in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newAgentLink wWin 
PROCEDURE newAgentLink :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   do with frame FRAME-I:
   end.
   empty temp-table tpersonagentdetails.
   run dialoglinkagent.w (input 'New',
                         input fentityID:input-value in frame {&frame-name},
                         input table tpersonagentdetails,
                         output cAgentID,
                         output std-lo).
   if not std-lo 
   then
    return.
   empty temp-table ttpersonagentdetails.
   run getPersonagentDetails in hPersonDataSrv(output table ttpersonagentdetails).
    
   for first ttpersonagentdetails where ttpersonagentdetails.agentID = cAgentID :
     if ttpersonagentdetails.stat = "A"
      then
       rdAgentType:screen-value = "A".
      else
       rdAgentType:screen-value = "I".
   end.
 
   apply 'value-changed' to rdAgentType.
   apply 'value-changed' to brwAgents.
  
   for first personagentdetails where personagentdetails.agentID = cAgentID:
     std-ro = rowid(personagentdetails).
   end.

   reposition brwAgents to rowid std-ro.
   
   run setButtons in this-procedure.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newPersonContact wWin 
PROCEDURE newPersonContact :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   do with frame {&frame-name}:
   end
   .
   empty temp-table ttpersoncontact.
   
   run dialognewpersoncontact.w  ( input 'New',
                                   input hPersonDataSrv,
                                   input fentityID:input-value,
                                   input fname:input-value,
                                   input table ttpersoncontact,
                                   output std-lo).

  if not std-lo 
   then
    return.
  
   publish "refreshAgentPerson".  /*refresh the personagent screens*/
  
  empty temp-table tpersoncontact.
  run refreshData in this-procedure (no,       /* iplNeedQualification */
                                     no,      /* iplNeedAffiliation   */
                                     no,      /* iplNeedpersonagents   */
                                     no,       /* iplNeedPersonRole    */
                                     no,      /* iplNeedReqFulfill    */
                                     yes , /*  NeedPersonContact  */ 
                                     no ). /* NeedPersonAgentDetails */
  
 apply 'value-changed' to rdContactType in frame FRAME-G.
 run setButtons in this-procedure.                                   
                                   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newPersonRole wWin 
PROCEDURE newPersonRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iPersonroleID as integer  no-undo.

  define buffer tpersonrole for tpersonrole.
  define buffer personrole  for personrole.

  do with frame fMain:
  end.

  empty temp-table tpersonrole.

  run dialognewpersonrole.w ( input hPersonDataSrv,
                              input fentityID:input-value,
                              input fname:input-value,
                              input table tpersonrole,
                              output iPersonroleID,
                              output std-lo). 
  if not std-lo 
   then
    return.

  run getPersonRole in hPersonDataSrv( output table tPersonRole).

  empty temp-table personrole.

  for each tpersonrole:
    create personrole.
    buffer-copy tpersonrole to personrole.

    personRole.sequence = getComSeq(personRole.ComStat).
  end.

  run displayPersonrole in this-procedure.

  for first personrole
    where personrole.personroleID = iPersonroleID:
    std-ro = rowid(personrole).
  end.

  reposition brwPersonRoles to rowid std-ro.
  brwPersonRoles:get-repositioned-row() in frame FRAME-E.

  apply "value-changed" to brwPersonRoles.
  run setButtons in this-procedure.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newQualification wWin 
PROCEDURE newQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tQualification for tQualification.
  define buffer qualification  for qualification.

  define variable iQualID   as integer  no-undo.
  define variable iReviewID as integer  no-undo.
  
  do with frame {&frame-name}:
  end.
 
  empty temp-table tQualification.
  
  run dialogqualification.w (input hPersonDataSrv,
                             input {&PersonNew},
                             input {&PersonCode},
                             input fentityId:input-value,
                             input cbstate:input-value,
                             input table tQualification,
                             output std-lo,
                             output iQualID,
                             output iReviewID).
  
  if not std-lo 
   then
    return.

  run getQualifications in hPersonDataSrv(output table tQualification).
  
  empty temp-table qualification.

  for each tQualification:
    create qualification.
    buffer-copy tqualification to qualification.
  end.
  
  run displayQualification in this-procedure.

  for first qualification
    where qualification.qualificationID = iQualID:
    std-ro = rowid(qualification).
  end.

  reposition brwQualifications to rowid std-ro.
  brwQualifications:get-repositioned-row() in frame FRAME-D.
  
  run setButtons in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openAffiliation wWin 
PROCEDURE openAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available affiliation 
   then
    return.
    
  if affiliation.PartnerAType = {&OrganizationCode} 
   then
    publish "OpenWindow" (input {&organization},                                /*childtype*/ 
                          input affiliation.partnerA,                           /*childid*/
                          input "worganizationdetail.w",                        /*window*/
                          input "character|input|" + affiliation.partnerA + "^integer|input|1" + "^character|input|",                                   
                          input this-procedure).
  else
   do:
     if affiliation.partnerA = ipcPersonId 
      then
       publish "OpenWindow" (input {&person},                                      /*childtype*/
                             input affiliation.partnerB,                           /*childid*/
                             input "wpersondetail.w",                              /*window*/
                             input "character|input|" + affiliation.partnerB + "^integer|input|1" + "^character|input|",                                   
                             input this-procedure).
     else
      publish "OpenWindow" (input {&person},                                      /*childtype*/
                            input affiliation.partnerA,                           /*childid*/
                            input "wpersondetail.w",                              /*window*/
                            input "character|input|" + affiliation.partnerA + "^integer|input|1" + "^character|input|",                                   
                            input this-procedure).                                /*currentProcedure handle*/ 
   end.
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openAgents wWin 
PROCEDURE openAgents :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame fMain:
  end.

  if valid-handle(hComAgents)  
   then
    run showWindow in hComAgents.
  else
   run wpersonagentrpt.w persistent set hComAgents          
        (input fentityId:input-value, 
         input fname:input-value, /* Entity Name */
         input hPersonDataSrv).       

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openDocument wWin 
PROCEDURE openDocument :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available qualification 
   then
    return.
  
 /* Open child window and maintain its instance */
  publish "OpenDocument" (input "Compliance",
                          input qualification.qualificationId,
                          input this-procedure).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openLog wWin 
PROCEDURE openLog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame fMain:
  end.

  if valid-handle(hComLog)  
   then
    run showWindow in hComLog.
  else
   run wcompliancelog.w persistent set hComLog          
        (input {&PersonCode},            /* Entity = "P" */
         input fentityId:input-value ,   /* Person Id */   
         input {&All},                   /* State ID */           
         input {&All},                   /* role */
         input {&All},                   /* roleID */
         input fname:input-value).       /* Entity Name */

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openReview wWin 
PROCEDURE openReview :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available qualification 
   then
    return.
    
 /* Open Child window and maintain its instance */ 
  publish "OpenWindow" (input "review", 
                        input string(qualification.qualificationID), 
                        input "wqualificationreviews.w", 
                        input "handle|input|" + string(hPersonDataSrv) + "^integer|input|" + string(qualification.qualificationID),                                   
                        input this-procedure).                                
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openRoleFulfillment wWin 
PROCEDURE openRoleFulfillment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame fMain:
  end. 
  
  define variable iCount         as integer     no-undo.
  define variable iPersonRoleID  as integer     no-undo.
  
  empty temp-table tpersonRole.
  
  if not available personRole
   then
    return.
 
  do iCount = 1 to brwpersonRoles:num-iterations in frame FRAME-E:
    if brwPersonRoles:is-row-selected(iCount)
     then
      leave.
  end.
  
  create tpersonRole.
  buffer-copy personRole to tpersonRole.
  iPersonRoleID = personRole.personRoleID.

  publish "OpenWindow" (input "personrole",            /*childtype*/
                        input string(iPersonRoleID),   /*childid*/
                        input "wpersonrole.w" ,        /*window*/
                        input "handle|input|" + string(hPersonDataSrv) + "^integer|input|" + string(iPersonRoleID),
                        input this-procedure).         /*currentProcedure handle*/ 


   
  run getPersonRole in hPersonDataSrv(output table tpersonRole).
 
  empty temp-table personRole.

  for each tpersonrole:
    create personrole.
    buffer-copy tpersonrole to personrole.
    inooffulfillreq  = 0.
    for each treqfulfill where treqfulfill.personRoleID = tpersonRole.personRoleID:
      inooffulfillreq = inooffulfillreq + 1.
    end.
    assign personrole.totfulfillreq = inooffulfillreq.
  end.

  apply 'value-changed' to roleType.
   
  for first personRole
    where personRole.personRoleID = ipersonRoleID:
    std-ro = rowid(personRole).
  end.

  brwpersonRoles:set-repositioned-row(iCount,"ALWAYS").
  reposition brwpersonRoles to rowid std-ro.  

  run setButtons in this-procedure.   
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openRoleLog wWin 
PROCEDURE openRoleLog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available personRole
   then return.
  
  publish "OpenWindow" (input "wcompliancelog",                           /*childtype*/
                        input personRole.roleID,                          /*childid*/
                        input "wcompliancelog.w",                         /*window*/
                        input "character|input|" + {&PersonCode} + "^character|input|" + personRole.personID + "^character|input|" + personRole.stateID + "^character|input|" + personRole.role +
                              "^character|input|" + (if personRole.role = {&Attorney} then personRole.roleID else personRole.personID) + "^character|input|" + person.dispname,                                   
                        input this-procedure).                            /* parent window handle */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openURL wWin 
PROCEDURE openURL :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter ipcURL as character no-undo.

 run ShellExecuteA in this-procedure (0,
                                      "open",
                                      ipcURL,
                                      "",
                                      "",
                                      1,
                                      output std-in).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE personStatusModified wWin 
PROCEDURE personStatusModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcPersonIDLocal as character.

  if ipcPersonIDLocal ne ipcPersonId 
   then
    return.

  run getData         in this-procedure.
  run displayPerson   in this-procedure.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshAffiliation wWin 
PROCEDURE refreshAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: if Affiliations are modified by person or agent than person and its
         role gets affected so need to refresh    
------------------------------------------------------------------------------*/
  define input parameter ipcOrgID   as character no-undo.
  
  define variable iCountAffiliated  as integer no-undo.
  define variable iAffiliationID    as integer no-undo.

  /* Match if this is concerned agent. */
  if not can-find( first affiliation where affiliation.partnerA = ipcOrgID ) 
   then
    return.

  do with frame {&frame-name}:
  end.

  find current affiliation no-error.
  if available affiliation 
   then
    iAffiliationID = affiliation.affiliationID.

  do iCountAffiliated = 1 to brwAffiliations:num-iterations in frame FRAME-C:
    if brwAffiliations:is-row-selected(iCountAffiliated)
     then
      leave.
  end.

  run refreshData in this-procedure (no,       /* iplNeedQualification */
                                     yes,      /* iplNeedAffiliation   */
                                     yes,      /* iplNeedpersonagents   */
                                     no,       /* iplNeedPersonRole    */
                                     no,      /* iplNeedReqFulfill    */
                                     no , /*  NeedPersonContact  */ 
                                     no ). /* NeedPersonAgentDetails */

  for first affiliation 
    where affiliation.affiliationID = iAffiliationID:
    std-ro = rowid(affiliation).
  end.

  brwAffiliations:set-repositioned-row(iCountAffiliated,"ALWAYS").
  reposition brwAffiliations to rowid std-ro no-error.

  run setButtons in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData wWin 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter iplNeedQualification as logical.
  define input parameter iplNeedAffiliation   as logical.
  define input parameter iplNeedPersonAgent as logical.  
  define input parameter iplNeedPersonRole    as logical.
  define input parameter iplNeedReqFulfill    as logical.
  define input parameter iplNeedPersonContact as logical.
  define input parameter iplNeedPADetails     as logical.
  
  run refreshPersonDetails in hPersonDataSrv (input  iplNeedQualification, /*  NeedQualification  */ 
                                              input  iplNeedAffiliation,   /*  NeedPerson  */  
                                              input  iplNeedPersonAgent, /*  NeedPersonContact*/
                                              input  iplNeedPersonRole,    /*  NeedPersonrole */
                                              input  iplNeedReqFulfill,    /*  NeedReqFulfill */
                                              input  iplNeedPersonContact, /*  NeedPersonContact */
                                              input  iplNeedPADetails,     /*NeedpersonAgentDetails */
                                              output std-lo,
                                              output std-ch).
  if not std-lo 
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end. 
    
  run refreshPersonPicture in hPersonDataSrv (output chimage).
  if chimage > ""
   then 
    lImage = chimage.
    
  /* Delete all local data and get updated data from server */
  run getData              in this-procedure.
  run displayPerson        in this-procedure.
  run displayPersonrole    in this-procedure.
  run displayQualification in this-procedure.
  run displayAffiliation   in this-procedure.
  run displayPersonContact in this-procedure.
  run setButtons           in this-procedure.
  apply "value-changed" to brwPersonRoles in frame frame-e.
  
  do with frame frame-I:
  end.
  rdAgentType:screen-value = "A".
  apply 'value-changed' to rdAgentType in frame frame-I.
  apply 'value-changed' to brwAgents.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshFulfillmentData wWin 
PROCEDURE refreshFulfillmentData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iPersonRoleID  as integer no-undo.
  
  assign inooffulfillreq = 0.
  
  empty temp-table reqfulfill.
  
  for each treqfulfill where treqfulfill.personRoleID = personrole.personRoleID:
    create reqfulfill. 
    buffer-copy treqfulfill to reqfulfill.
    inooffulfillreq = inooffulfillreq + 1.
  end.
  
  assign personrole.totfulfillreq = inooffulfillreq
         iPersonRoleID = personrole.personRoleID.
         
  for first personrole
    where personrole.personRoleID = iPersonRoleID :
    std-ro = rowid(personrole).
  end.

  open query brwFulfillment for each reqfulfill.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshPersonRoles wWin 
PROCEDURE refreshPersonRoles :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: if Affiliations are modified by person or agent than person and its
         role gets affected so need to refresh    
------------------------------------------------------------------------------*/

  run refreshData in this-procedure (no,       /* iplNeedQualification */    
                                     no,       /* iplNeedAffiliation   */    
                                     no,       /* iplNeedpersonagent   */    
                                     yes,      /* iplNeedPersonRole    */    
                                     no,     /* iplNeedReqFulfill    */      
                                     no , /*  NeedPersonContact  */
                                     no ). /*NeedPersomAgentDetails */
  run setButtons  in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshQualifications wWin 
PROCEDURE refreshQualifications :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameter Definition */
  define input parameter ipiQualificationID as integer no-undo.
  
  /* Local Variable */
  define variable iCount  as integer no-undo.

  if not can-find(first qualification where qualification.qualificationID = ipiQualificationID)
   then
    return.  
  
  do with frame {&frame-name}:
  end.
  
  do iCount = 1 to brwQualifications:num-iterations in frame FRAME-D:
    if brwQualifications:is-row-selected(iCount)
     then
      leave.
  end.

  run refreshData in this-procedure (yes, /*  NeedQualification */  
                                     no,  /*  NeedAffiliation   */ 
                                     no,  /* iplNeedpersonagent   */
                                     no,  /*  NeedPersonrole    */
                                     no, /*  NeedReqFulfill    */
                                     no, /*  NeedPersonContact  */ 
                                     no ). /* NeedPersonAgentDetails */
  for first qualification 
    where qualification.qualificationID = ipiQualificationID:
    std-ro = rowid(qualification).
  end.

  brwQualifications:set-repositioned-row(iCount,"ALWAYS") no-error.
  reposition brwQualifications to rowid std-ro no-error.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshTags wWin 
PROCEDURE refreshTags :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  find first person no-error.
  if not available person
   then
    return.
    
  run LoadUserTags in hPersonDataSrv(input "P", input person.personID).
  
  openTags().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE renewQualification wWin 
PROCEDURE renewQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tQualification for tQualification.

  define variable iQualID   as integer  no-undo.
  define variable iReviewID as integer  no-undo.
  define variable iPrevQualID as integer no-undo.
  
  do with frame {&frame-name}:
  end.
  
  if not available qualification 
   then
    return.
 
  empty temp-table tQualification.
  create tQualification.
  buffer-copy qualification to tQualification.
  iPrevQualID = tQualification.qualificationID.
  
  run dialogqualification.w (input hPersonDataSrv,
                             input {&PersonRenew},
                             input {&PersonCode},
                             input person.personID,
                             input person.state,
                             input table tQualification,
                             output std-lo,
                             output iQualID,
                             output iReviewID).
  
  if not std-lo 
   then
    return.

  run getQualifications in hPersonDataSrv(output table tQualification).
  
  empty temp-table qualification.

  for each tQualification:
    create qualification.
    buffer-copy tqualification to qualification.
  end.
  
  run refreshQualifications in this-procedure (input iPrevQualID).
  run displayQualification in this-procedure.

  for first qualification
    where qualification.qualificationID = iQualID:
    std-ro = rowid(qualification).
  end.

  reposition brwqualifications to rowid std-ro.
  brwqualifications:get-repositioned-row() in frame FRAME-D.
  
  run setButtons in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE roleComplianceModified wWin 
PROCEDURE roleComplianceModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: 
------------------------------------------------------------------------------*/
  define input parameter ipcEntityID as character no-undo.
  
  if ipcEntityID ne ipcPersonID  
   then
    return.

  run refreshData in this-procedure(no,    /* iplNeedQualification */     
                                    no,    /* iplNeedAffiliation   */     
                                    no,    /* iplNeedpersonagent   */     
                                    yes,   /* iplNeedPersonRole    */     
                                    no,  /* iplNeedReqFulfill    */       
                                    no ,  /*  NeedPersonContact  */ 
                                    no ). /* NeedPersonAgentDetails */
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE savePerson wWin 
PROCEDURE savePerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tperson for tperson.
  define buffer person  for person.

  empty temp-table tPerson.

  find first person  where person.personID = fentityId:input-value in frame {&frame-name} no-error.
  if not available person 
   then
    return.

  create tPerson.
  assign 
      tPerson.NPN        = cbNPN:input-value in frame {&frame-name}
      tPerson.AltaUID    = cbAltaUid:input-value
      tPerson.firstName  = person.firstName    
      tPerson.middleName = person.middleName
      tPerson.lastName   = person.lastName
      tperson.dispName   = fname:input-value
      tPerson.address1   = fAddr:input-value
      tPerson.address2   = fAddr2:input-value
      tPerson.city       = fCity:input-value
      tPerson.state      = cbstate:input-value
      tPerson.zip        = fZip:input-value
      tPerson.notes      = edContNotes:input-value
      tperson.personID   = fentityId:input-value
      tPerson.firstName  = cFirstName
      tPerson.middleName = cMiddleName
      tPerson.lastName   = cLastName
      tperson.active     = person.active
      tperson.internal   = tglEmp:checked
      tperson.doNotCall  = tgldnc:checked
      .

  run modifyPerson in hPersonDataSrv (input table tPerson,
                                      output std-lo,
                                      output std-ch).
  if not std-lo
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.
    
  empty temp-table person.

  run getperson in hPersonDataSrv (output table tperson).

  for each tperson:
    create person.
    buffer-copy tperson to person.
  end.
    
  run displayPerson in this-procedure. 
  publish "refreshAgentPerson".  /*refresh the personagent screens*/
  
  if avail person and person.internal then
  do:
    empty temp-table tpersoncontact.
    run refreshData in this-procedure (no,       /* iplNeedQualification */
                                       no,      /* iplNeedAffiliation   */
                                       no,      /* iplNeedpersonagents   */
                                       no,       /* iplNeedPersonRole    */
                                       no,      /* iplNeedReqFulfill    */
                                       yes , /*  NeedPersonContact  */ 
                                       no ). /* NeedPersonAgentDetails */
  
    apply 'value-changed' to rdContactType in frame FRAME-G.
  end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE selectPage wWin 
PROCEDURE selectPage :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/

  define input parameter piPageNum as integer no-undo.

  /* Code placed here will execute PRIOR to standard behavior. */
  run super(input piPageNum).
  
  if piPageNum = 1 
   then
    do:
      assign 
          frame FRAME-D:visible   = false
          frame FRAME-D:sensitive = false
          frame FRAME-E:visible   = false
          frame FRAME-E:sensitive = false
          frame FRAME-C:visible   = true
          frame FRAME-C:sensitive = true
          frame FRAME-G:visible   = false
          frame FRAME-G:sensitive = false
          frame FRAME-H:visible   = false
          frame FRAME-H:sensitive = false
          frame FRAME-I:visible   = false
          frame FRAME-I:sensitive = false
          .
    end.
   else if piPageNum = 2 
    then
     do:
       assign 
           frame FRAME-C:visible   = false
           frame FRAME-C:sensitive = false
           frame FRAME-E:visible   = false
           frame FRAME-E:sensitive = false
           frame FRAME-D:visible   = true
           frame FRAME-D:sensitive = true
           frame FRAME-G:visible   = false
           frame FRAME-G:sensitive = false
           frame FRAME-H:visible   = false
           frame FRAME-H:sensitive = false
           frame FRAME-I:visible   = false
           frame FRAME-I:sensitive = false
           .
     end.
   else if piPageNum = 3 
    then
     do:
       assign 
          frame FRAME-C:visible   = false
          frame FRAME-C:sensitive = false
          frame FRAME-D:visible   = false
          frame FRAME-D:sensitive = false
          frame FRAME-E:visible   = true
          frame FRAME-E:sensitive = true
          frame FRAME-G:visible   = false
          frame FRAME-G:sensitive = false
          frame FRAME-H:visible   = false
          frame FRAME-H:sensitive = false
          frame FRAME-I:visible   = false
          frame FRAME-I:sensitive = false
          .
     end.
   else if piPageNum = 4 THEN
    do:
      rdContactType:screen-value = "A".
      apply 'value-changed' to rdContactType.
      assign 
          frame FRAME-C:visible   = false
          frame FRAME-C:sensitive = false
          frame FRAME-E:visible   = false
          frame FRAME-E:sensitive = false
          frame FRAME-D:visible   = false
          frame FRAME-D:sensitive = false
          frame FRAME-G:visible   = true
          frame FRAME-G:sensitive = true
          frame FRAME-H:visible   = false
          frame FRAME-H:sensitive = false
          frame FRAME-I:visible   = false
          frame FRAME-I:sensitive = false
          .
    end.
   else if piPageNum = 5 then
    do:
      assign 
          frame FRAME-C:visible   = false
          frame FRAME-C:sensitive = false
          frame FRAME-E:visible   = false
          frame FRAME-E:sensitive = false
          frame FRAME-D:visible   = false
          frame FRAME-D:sensitive = false
          frame FRAME-G:visible   = false
          frame FRAME-G:sensitive = false
          frame FRAME-H:visible   = true
          frame FRAME-H:sensitive = true
          frame FRAME-I:visible   = false
          frame FRAME-I:sensitive = false
          .
    end.
    else
     do:
       rdAgentType:screen-value = "A".
       apply 'value-changed' to rdAgentType.
       apply 'value-changed' to brwAgents.
       assign 
          frame FRAME-C:visible   = false
          frame FRAME-C:sensitive = false
          frame FRAME-E:visible   = false
          frame FRAME-E:sensitive = false
          frame FRAME-D:visible   = false
          frame FRAME-D:sensitive = false
          frame FRAME-G:visible   = false
          frame FRAME-G:sensitive = false
          frame FRAME-H:visible   = false
          frame FRAME-H:sensitive = false
          frame FRAME-I:visible   = true
          frame FRAME-I:sensitive = true
          .
     end.
   
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setButtons wWin 
PROCEDURE setButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  if std-ch = 'COM' 
   then
    do:
      do with frame FRAME-D:
      end.
     
      if query brwQualifications:num-results = 0 or query brwQualifications:num-results = ?
       then
        assign 
            bEditqual:sensitive           = false
            bDeleteQual:sensitive         = false
            bDocuments:sensitive          = false
            bVerify:sensitive             = false
            bActive:sensitive             = false
            bQualificationCopy:sensitive  = false
            bQualificationRenew:sensitive = false
            .
      else
       do:
         assign 
             bEditqual:sensitive           = true    
             bDocuments:sensitive          = true
             bVerify:sensitive             = true
             bActive:sensitive             = true
             bQualificationCopy:sensitive  = true
             bQualificationRenew:sensitive = true
            .
       
         if available qualification
          then
           do:
             if Qualification.activ 
              then
               do:
                 bActive    :load-image("images/s-flag_red.bmp").
                 bActive    :tooltip   = "Deactivate qualification".
                 bDeleteQual:sensitive = false.
               end.
             else
              do:
                bActive    :load-image("images/s-flag_green.bmp").
                bActive    :tooltip   = "Activate qualification".
                bDeleteQual:sensitive = true.
              end.
           end.   
       end.
     
      do with frame FRAME-C:
      end.
     
      if query brwAffiliations:num-results = 0 or query brwAffiliations:num-results = ?  
       then
        assign  
            bEditaff:sensitive    = false
            bDeleteaff:sensitive  = false
            bOpenAff:sensitive  = false
            .   
      else    
       assign  
           bEditaff:sensitive   = true
           bDeleteaff:sensitive = true
           bOpenAff:sensitive = true
           .
     if available affiliation
      then
       do:
         if affiliation.partnerAType = {&AgentCode} 
          then
           do:
           
             if affiliation.expdate <> ?
               then
                do:                                                            
                  bDeleteAff    :load-image("images/s-flag_green.bmp").
                  bDeleteAff    :tooltip   = "Activate association".
                end.
              else 
               do:
                  bDeleteAff    :load-image("images/s-flag_red.bmp").
                  bDeleteAff    :tooltip   = "Deactivate association".
               end.
               
             assign  
                  bOpenAff:sensitive         = false
                  bEditaff:sensitive         = false.
                  
           end.   
         else
          do:
             bDeleteAff    :load-image("images/s-delete.bmp"). 
             bDeleteAff    :load-image-insensitive("images/s-delete-i.bmp").
             bDeleteAff    :tooltip   = "Delete affiliation".        /* load-image-insensitive("images/s-delete-i.bmp").*/
             
             assign  
                  bAffiliatePerson:sensitive = true
                  bNewAff:sensitive          = true
                  bOpenAff:sensitive         = true
                  .
          end.
       end.
       
       
      do with frame FRAME-E:
      end.
      
      if query brwPersonRoles:num-results = 0 or query brwPersonRoles:num-results = ? 
       then
        assign
            bDeleteRole:sensitive = false
            bOpenRole:sensitive   = false
            bRoleLog:sensitive    = false
            bEditRole:sensitive   = false
            .
      else
       assign
           bDeleteRole:sensitive = true
           bOpenRole:sensitive   = true
           bRoleLog:sensitive    = true
           bEditRole:sensitive   = true
           .
           
      do with frame FRAME-G:
      end.
      
      if query brwpersoncontact:num-results = 0 or query brwPersonRoles:num-results = ? 
       then
        assign
    /*         bNewPersonContact:sensitive    = false */
            bEditPersonContact:sensitive   = false
            bExpPersonContact:sensitive    = false
            bMailPersonContact:sensitive   = false
            .
      else
       assign
    /*        bNewPersonContact:sensitive    = true */
           bEditPersonContact:sensitive   = true
           bExpPersonContact:sensitive    = true
           .
          
      if available personcontact
      then
       do:
         if personcontact.active  
          then
           do:                                                            
             bExpPersonContact    :load-image("images/s-flag_red.bmp").
             bExpPersonContact    :tooltip   = "Expire contact".
           end.
         else
          do:
            bExpPersonContact    :load-image("images/s-flag_green.bmp").
            bExpPersonContact    :tooltip   = "Activate contact". 
          end.
         if personcontact.contactID ne '' and personcontact.contacttype = 'E' and tgldnc:checked in frame fmain = false
           then
             bMailPersonContact:sensitive = true.
         else
             bMailPersonContact:sensitive = false.
       end.   
     
    end.
  else /* person detail is launched from other than com */
   do:
   
      do with frame FRAME-D:
      end.
     
      assign 
          bNewQual:sensitive     = false
          bEditqual:sensitive    = false
          bDeleteQual:sensitive  = false
          bDocuments:sensitive   = false
          bVerify:sensitive      = false
          bActive:sensitive      = false
          bQualificationCopy:sensitive = false
          bQualificationRenew:sensitive = false
          .
       
      
     
      do with frame FRAME-C:
      end.
     
     menu-item m_View_affiliation:sensitive IN MENU POPUP-MENU-brwAffiliations = false.
      
      
     assign  
         bAffiliatePerson:sensitive    = false
         bNewAff:sensitive    = false       
         bEditaff:sensitive    = false
         bDeleteaff:sensitive  = false
         bOpenAff:sensitive  = false
         .
         
     if available affiliation
      then
       do:
         if affiliation.partnerAType = {&AgentCode} 
          then
           do:
               bDeleteaff:sensitive  = true.
               
             if affiliation.expdate <> ?
               then
                do:                                                            
                  bDeleteAff    :load-image("images/s-flag_green.bmp").
                  bDeleteAff    :tooltip   = "Activate affiliation".
                end.
              else 
               do:
                  bDeleteAff    :load-image("images/s-flag_red.bmp").
                  bDeleteAff    :tooltip   = "Expire affiliation".
               end.
           end.   
         else
          do:
             bDeleteAff    :load-image("images/s-delete.bmp"). 
             bDeleteAff    :load-image-insensitive("images/s-delete-i.bmp").
             bDeleteAff    :tooltip   = "Delete affiliation".        /* load-image-insensitive("images/s-delete-i.bmp").*/
          end.
       end.
       
       
      do with frame FRAME-E:
      end.
      
     menu-item m_View_role_fulfillments:sensitive IN MENU POPUP-MENU-brwPersonRoles = false.
      
     assign
         bNewRole:sensitive = false
         bDeleteRole:sensitive = false
         bOpenRole:sensitive   = false
         bRoleLog:sensitive    = false
         bEditRole:sensitive   = false
         .
           
      do with frame FRAME-G:
      end.
      
      if query brwpersoncontact:num-results = 0 or query brwPersonRoles:num-results = ? 
       then
        assign
    /*         bNewPersonContact:sensitive    = false */
            bEditPersonContact:sensitive   = false
            bExpPersonContact:sensitive    = false
            bMailPersonContact:sensitive   = false
            .
      else
       assign
    /*        bNewPersonContact:sensitive    = true */
           bEditPersonContact:sensitive   = true
           bExpPersonContact:sensitive    = true
           .
          
      if available personcontact
      then
       do:
         if personcontact.active  
          then
           do:                                                            
             bExpPersonContact    :load-image("images/s-flag_red.bmp").
             bExpPersonContact    :tooltip   = "Expire contact".
           end.
         else
          do:
            bExpPersonContact    :load-image("images/s-flag_green.bmp").
            bExpPersonContact    :tooltip   = "Activate contact". 
          end.

          if personcontact.contactID ne '' and personcontact.contacttype = 'E' and tgldnc:checked in frame fmain = false
            then
              bMailPersonContact:sensitive = true.
          else
            bMailPersonContact:sensitive = false.
       end.
      do with frame FRAME-I:
      end.
      
      if query brwAgents:num-results = 0 or query brwAgents:num-results = ? 
       then
        assign
            bExportAgents:sensitive          = false
            bEditPersonagent:sensitive       = false
            bPersonAgentDeactivate:sensitive = false
            .
      else
       assign
           bExportAgents:sensitive          = true
           bNewAgentLink:sensitive          = not tglEmp:checked
           bEditPersonagent:sensitive       = not tglEmp:checked
           bPersonAgentDeactivate:sensitive = not tglEmp:checked.
      
   end.
  run enableDisableSave in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setPicture wWin 
PROCEDURE setPicture :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iImgheight as integer no-undo.
  define variable iImgwidth  as integer no-undo.

  if valid-handle(IMAGE-2) 
   then
    delete object IMAGE-2.

  create image IMAGE-2.
  IMAGE-2:frame = frame FRAME-A:handle.
  IMAGE-2:load-image(chPath) no-error.
  IMAGE-2:transparent = true.

  assign 
      iImgheight = IMAGE-2:width-pixel
      iImgwidth  = IMAGE-2:height-pixel
      .
  
  IMAGE-2:load-image("") no-error.

  if iImgheight > frame FRAME-A:width-pixel 
   then
    IMAGE-2:width-pixel = frame FRAME-A:width-pixel - 2.
  else
   image-2:x = image-2:frame-x + ( frame FRAME-A:width-pixels / 2 ) - ( image-2:width-pixels / 2 ) - 2 no-error.
   
  if iImgwidth > frame FRAME-A:height-pixel 
   then
    IMAGE-2:height-pixel = frame FRAME-A:height-pixel - 2.
  else
   image-2:y = image-2:frame-y + ( frame FRAME-A:height-pixels / 2 ) - ( image-2:height-pixels / 2 ) - 2 no-error.

  IMAGE-2:load-image(chPath) no-error.

  assign 
      IMAGE-2:stretch-to-fit = true
      IMAGE-2:resizable      = true
      IMAGE-2:hidden         = false 
      no-error
      .
        
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setTabLabels wWin 
PROCEDURE setTabLabels :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE VARIABLE clabel AS CHARACTER   no-undo initial "Affiliations|Qualifications|Roles|Contacts|Tags|Agents".
  DEFINE VARIABLE i AS INTEGER     NO-UNDO.
  
  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  if std-ch = 'AMD' 
   then
     do:
       do i = 1 to num-entries(clabel, "|"):
         run create-folder-page in h_folder(i, entry(i,clabel,'|')).
       end.
     end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow wWin 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData wWin 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
   
 /* apply 'value-changed' to rdAgentType in frame frame-I.  */
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE updateAffiliationAgent wWin 
PROCEDURE updateAffiliationAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwAffiliations.
  
  empty temp-table taffiliation.
  
  run getAffiliations in hPersonDataSrv(output table taffiliation). 
  
  empty temp-table affiliation.
  for each taffiliation:
    create affiliation.
    buffer-copy taffiliation to affiliation.
  end.
  
  run displayAffiliation in this-procedure.
  run setButtons in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized wWin 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign
      frame fmain:width-pixels                              = {&window-name}:width-pixels
      frame fmain:virtual-width-pixels                      = {&window-name}:width-pixels
      frame fmain:height-pixels                             = {&window-name}:height-pixels
      frame fmain:virtual-height-pixels                     = {&window-name}:height-pixels
      .
      
  run resizeObject in h_folder (frame fMain:height - 3, frame fMain:width - 2). 

  assign
      frame frame-C:width-pixels                        = {&window-name}:width-pixels  - 20 
      frame frame-C:virtual-width-pixels                = {&window-name}:width-pixels  - 20  
      frame frame-C:height-pixels                       = {&window-name}:height-pixels - 103.90     
      frame frame-C:virtual-height-pixels               = {&window-name}:height-pixels - 103.90     
      
      
      frame frame-E:width-pixels                              = frame fmain:width-pixels    - 20 
      frame frame-E:virtual-width-pixels                      = frame fmain:width-pixels    - 20  
      frame frame-E:height-pixels                             = frame fmain:height-pixels   - 103.90 
      frame frame-E:virtual-height-pixels                     = frame fmain:height-pixels   - 103.90 
      
      
      frame frame-D:width-pixels                              = frame fmain:width-pixels  - 20 
      frame frame-D:virtual-width-pixels                      = frame fmain:width-pixels  - 20  
      frame frame-D:height-pixels                             = frame fmain:height-pixels - 103.90 
      frame frame-D:virtual-height-pixels                     = frame fmain:height-pixels - 103.90 
      
      
      
      frame FRAME-G:width-pixels                              = frame fmain:width-pixels  - 20 
      frame FRAME-G:virtual-width-pixels                      = frame fmain:width-pixels  - 20  
      frame FRAME-G:height-pixels                             = frame fmain:height-pixels - 103.90 
      frame FRAME-G:virtual-height-pixels                     = frame fmain:height-pixels - 103.90 
      
      frame FRAME-H:width-pixels                              = frame fmain:width-pixels  - 20 
      frame FRAME-H:virtual-width-pixels                      = frame fmain:width-pixels  - 20  
      frame FRAME-H:height-pixels                             = frame fmain:height-pixels - 103.90 
      frame FRAME-H:virtual-height-pixels                     = frame fmain:height-pixels - 103.90
      
      brwAffiliations:width-pixels    in frame frame-C    = frame frame-C:width-pixels - 32
      brwAffiliations:height-pixels   in frame frame-C    = frame frame-C:height-pixels - brwAffiliations:y - 10
      brwPersonRoles:height-pixels    in frame frame-E    = frame frame-E:height-pixels - brwPersonRoles:y - 10
      brwFulfillment:width-pixels     in frame frame-E    = frame frame-E:width-pixels - brwPersonRoles:width-pixels - 35
      brwFulfillment:height-pixels    in frame frame-E    = frame frame-E:height-pixels - brwFulfillment:y - 10
      brwQualifications:width-pixels  in frame frame-D    = frame frame-D:width-pixels - 32
      brwQualifications:height-pixels in frame frame-D    = frame frame-D:height-pixels - brwQualifications:y - 10
      brwpersoncontact:width-pixels  in frame  frame-G    = frame frame-G:width-pixels - 32
      brwpersoncontact:height-pixels in frame  frame-G    = frame frame-G:height-pixels - brwpersoncontact:y - 10
      brwTags:width-pixels           in frame  frame-H    = frame frame-H:width-pixels - 32
      brwTags:height-pixels          in frame  frame-H    = frame frame-H:height-pixels - brwTags:y - 10
      .

 /* resize the description column in the claims browse */ 
  {lib/resize-column.i &col="'Name,Type'" &var=dAffiliation &browse-name=brwAffiliations}
  {lib/resize-column.i &col="'Qualification'" &var=dColumnWidthq &browse-name=brwQualifications}
  {lib/resize-column.i &col="'reqdesc,qualification'" &var=dColumnWidth &browse-name=brwFulfillment} 
  {lib/resize-column.i &col="'contactID,contactSubType'" &var=dColumnWidthc &browse-name=brwpersoncontact}
  {lib/resize-column.i &col="'userName'" &var=dColumnWidthu &browse-name=brwTags}
  
  run ShowScrollBars(frame fmain:handle, no, no).
  run ShowScrollBars(frame frame-C:handle, no, no).
  run ShowScrollBars(frame frame-E:handle, no, no).
  run ShowScrollBars(frame frame-D:handle, no, no).
  run ShowScrollBars(frame frame-G:handle, no, no).
  run ShowScrollBars(frame frame-H:handle, no, no).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doModify wWin 
FUNCTION doModify RETURNS LOGICAL
  ( input pModify as logical ) :
/*------------------------------------------------------------------------------
@description Disables/Enables the save buttons
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign 
        bContSave:sensitive = pModify
        bContCanc:sensitive = pModify
        .
  end.
  return true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getComSeq wWin 
FUNCTION getComSeq RETURNS CHARACTER
  ( input cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cStat = {&Rcode} 
   then 
    return "3".
  else if cStat = {&Gcode} 
   then
    return "1".
  else 
   return "2".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getComStat wWin 
FUNCTION getComStat RETURNS CHARACTER
  ( input cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cStat = "2" 
   then 
    return {&Ycode}. 
  else if cStat = "1" 
   then
    return {&Gcode}.  
  else
   return {&Rcode}.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getContactType wWin 
FUNCTION getContactType RETURNS CHARACTER
  (ctype as char /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 if ctype = "P" 
   then 
    return "Phone". 
  else if ctype = "E" 
   then
    return "Email".  
  else
   return ctype.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getEntity wWin 
FUNCTION getEntity RETURNS CHARACTER
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cStat = {&OrganizationCode}
   then
    cStat = {&Organization}.
  else if cStat = {&AgentCode}
   then
    cStat = {&Agent}.
  else
   cStat = {&Person}.
  
  return  cStat.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getEntityID wWin 
FUNCTION getEntityID RETURNS CHARACTER
  ( cPartnerAType as character,
    cPartnerA     as character,
    cPartnerB     as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cPartnerAType = {&OrganizationCode} 
   then
    return cPartnerA.
  else if cPartnerAType = {&AgentCode} 
   then
    return cPartnerA.
  else
   do:
      if cPartnerA = ipcPersonId 
       then
        return cPartnerB.
      else
       return cPartnerA.
    end.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getEntityName wWin 
FUNCTION getEntityName RETURNS CHARACTER
  ( cPartnerA     as character,
    cPartnerAType as character,
    cPartnerAName as character,
    cPartnerBName as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  if cPartnerAType = {&PersonCode} 
   then
    do:
      if cPartnerA = ipcPersonId 
       then
        return cPartnerBName.
      else
       return cPartnerAName.
    end.
  else if cPartnerAType = {&AgentCode}  then
   return cPartnerAName.
  else
   return cPartnerAName.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getPersonName wWin 
FUNCTION getPersonName RETURNS CHARACTER
  (cUserID as char /* parameter-definitions */ ) :
/*---------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  def var cDesc as char no-undo.
  
  publish "GetSysUserName" (cUserID, output cDesc).
  
  return cDesc.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getQualificationDesc wWin 
FUNCTION getQualificationDesc RETURNS CHARACTER
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:   
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("COM", "Qualification", "Status", cStat, output std-ch).
  return std-ch.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getRoleStatus wWin 
FUNCTION getRoleStatus RETURNS CHARACTER
  ( cStat as logical /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cVar as character no-undo.
  
  if cStat
   then
    cVar = {&Active}.
  else
   cVar = {&InActive}.
  
  return  cVar.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openTags wWin 
FUNCTION openTags RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 find first person no-error.
  if not available person
   then
    return false.
    
  run getuserTags in hPersonDataSrv (input "P", input person.personID, output table tag). 
      
  open query brwTags for each tag where tag.entity = "P".
  
  apply 'value-changed' to brwTags in frame frame-h.
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

