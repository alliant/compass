&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

  Modified:
     Date       Name                  Description
     01/13/22   Sachin Chaturvedi     implemented paramaters for Entity,Destination
                                      and actions according to parameters we get 
                                      sysdest table.
     05/05/22   Sachin Chaturvedi     Task# 80191 Added the parameters while editing,deleting, 
                                      copying and while creating new system destination.
     09/13/23   Sagar K               Modified to show error message
     07/16/24   SB                    Modified to set default sys destination entity and sys destination action
     11/21/24   S Chandu              Modified to set default sys destination and action for 'productionfileactivity'.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Temp Table Definitions ---                                           */
{tt/sysdest.i}
{tt/sysdest.i &tableAlias="origdata"}
{tt/sysdest.i &tableAlias="tempsysdest"}
{tt/sysaction.i &tableAlias="ttSysAction"}
{tt/sysaction.i}

define temp-table sysDestEntityList
  field sysDestItemName as character
  field sysDestItemValue as character.

/* Local Variable Definitions ---                                       */
{lib/std-def.i}
{lib/set-button-def.i}

/* Can�t use lib/sys-def.i as preprocessor �Inactive� defined in this coincides with �Inactive� in set-button.i*/
&global-define ResultNotMatch "Results may not match current parameters." 
&global-define All              "ALL"

define variable iDestID       as integer   no-undo.
define variable iSelectedRow  as integer   no-undo.
define variable cDestinations as character no-undo.
define variable cActions      as character no-undo.
define variable cEntitys      as character no-undo.
define variable cSearch       as character no-undo.
define variable cAppCode      as character no-undo.
define variable cSysDestEntity as character no-undo.
define variable cSysDestAction as character no-undo.
define variable cDestAction   as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fDest
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES sysdest

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData sysdest.entityDesc sysdest.entityID sysdest.entityName sysdest.actionDesc sysdest.destTypeDesc sysdest.destNameDesc   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each sysdest
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each sysdest.
&Scoped-define TABLES-IN-QUERY-brwData sysdest
&Scoped-define FIRST-TABLE-IN-QUERY-brwData sysdest


/* Definitions for FRAME fDest                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fDest ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bGetdata fEntityParam fDestinationParam ~
fActionParam fAction fDestination fEntity fSearch brwData bRefresh bCopy ~
bDelete bExport bModify bNew RECT-3 RECT-4 
&Scoped-Define DISPLAYED-OBJECTS fEntityParam fDestinationParam ~
fActionParam fAction fDestination fEntity fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VARIABLE C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCopy  NO-FOCUS
     LABEL "Copy" 
     SIZE 7.2 BY 1.71 TOOLTIP "Copy the selected Destination".

DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Remove the selected Destination".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bFilterClear  NO-FOCUS
     LABEL "FilterClear" 
     SIZE 7.2 BY 1.71 TOOLTIP "Clear filters".

DEFINE BUTTON bGetdata  NO-FOCUS
     LABEL "getData" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get Data".

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify the selected Destination".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add a new Destination".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE VARIABLE fAction AS CHARACTER FORMAT "X(256)":U 
     LABEL "Action" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE fActionParam AS CHARACTER FORMAT "X(256)":U 
     LABEL "Action" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 59 BY 1 NO-UNDO.

DEFINE VARIABLE fDestination AS CHARACTER FORMAT "X(256)":U 
     LABEL "Destination" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE fDestinationParam AS CHARACTER FORMAT "X(256)":U 
     LABEL "Destination" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE fEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE fEntityParam AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 25.2 BY 1 NO-UNDO.

DEFINE RECTANGLE rActions
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 46 BY 3.1.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 84.4 BY 3.1.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 79 BY 3.1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      sysdest SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      sysdest.entityDesc width 15
sysdest.entityID width 35
sysdest.entityName width 40
sysdest.actionDesc width 30
  sysdest.destTypeDesc width 15
  sysdest.destNameDesc  format "x(50)" width 40
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 208.8 BY 20.81 ROW-HEIGHT-CHARS .71 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fDest
     bGetdata AT ROW 1.95 COL 72 WIDGET-ID 314 NO-TAB-STOP 
     fEntityParam AT ROW 1.71 COL 9 COLON-ALIGNED WIDGET-ID 308
     fDestinationParam AT ROW 1.71 COL 42 COLON-ALIGNED WIDGET-ID 310
     fActionParam AT ROW 2.91 COL 9 COLON-ALIGNED WIDGET-ID 312
     fAction AT ROW 2.91 COL 87 COLON-ALIGNED WIDGET-ID 22
     fDestination AT ROW 1.71 COL 128 COLON-ALIGNED WIDGET-ID 24
     fEntity AT ROW 1.71 COL 87 COLON-ALIGNED WIDGET-ID 30
     fSearch AT ROW 2.91 COL 128 COLON-ALIGNED WIDGET-ID 266
     brwData AT ROW 4.62 COL 2 WIDGET-ID 200
     bFilterClear AT ROW 1.95 COL 157 WIDGET-ID 264 NO-TAB-STOP 
     bRefresh AT ROW 1.95 COL 166.6 WIDGET-ID 18 NO-TAB-STOP 
     bCopy AT ROW 1.95 COL 201.6 WIDGET-ID 26 NO-TAB-STOP 
     bDelete AT ROW 1.95 COL 194.6 WIDGET-ID 14 NO-TAB-STOP 
     bExport AT ROW 1.95 COL 173.6 WIDGET-ID 16 NO-TAB-STOP 
     bModify AT ROW 1.95 COL 187.6 WIDGET-ID 12 NO-TAB-STOP 
     bNew AT ROW 1.95 COL 180.6 WIDGET-ID 10 NO-TAB-STOP 
     "Parameters" VIEW-AS TEXT
          SIZE 13 BY .62 AT ROW 1 COL 3 WIDGET-ID 274
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1 COL 166 WIDGET-ID 194
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1 COL 82 WIDGET-ID 268
     rActions AT ROW 1.29 COL 164.8 WIDGET-ID 8
     RECT-3 AT ROW 1.29 COL 80.8 WIDGET-ID 20
     RECT-4 AT ROW 1.29 COL 2 WIDGET-ID 272
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COLUMN 1 ROW 1.14
         SIZE 211.4 BY 25.24 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Destinations"
         HEIGHT             = 24.76
         WIDTH              = 211
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fDest
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData fSearch fDest */
/* SETTINGS FOR BUTTON bFilterClear IN FRAME fDest
   NO-ENABLE                                                            */
ASSIGN 
       brwData:ALLOW-COLUMN-SEARCHING IN FRAME fDest = TRUE
       brwData:COLUMN-RESIZABLE IN FRAME fDest       = TRUE.

/* SETTINGS FOR RECTANGLE rActions IN FRAME fDest
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each sysdest.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Destinations */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Destinations */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Destinations */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCopy C-Win
ON CHOOSE OF bCopy IN FRAME fDest /* Copy */
DO:
  run copySysDest in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME fDest /* Delete */
DO:
  run deleteSysDest in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fDest /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilterClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilterClear C-Win
ON CHOOSE OF bFilterClear IN FRAME fDest /* FilterClear */
DO:
  run clearFilters in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGetdata
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGetdata C-Win
ON CHOOSE OF bGetdata IN FRAME fDest /* getData */
DO:
  run getData in this-procedure.     
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME fDest /* Modify */
DO:
  run modifySysDest in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fDest /* New */
DO:
  run newSysDest in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fDest /* Refresh */
DO:
  cDestinations = fDestination:input-value.
  cActions      = fAction:input-value.
  cEntitys      = fEntity:input-value.
  cSearch       = fSearch:input-value.
  run refreshData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fDest
DO:
  apply "CHOOSE" to bModify.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fDest
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fDest
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fDest
DO:
  assign
    bCopy:sensitive    = available sysdest
    bModify:sensitive  = available sysdest
    bDelete:sensitive  = available sysdest
    bExport:sensitive  = available sysdest
    bRefresh:sensitive = true
    bNew:sensitive     = true
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fEntity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fEntity C-Win
ON VALUE-CHANGED OF fEntity IN FRAME fDest /* Entity */
OR 'VALUE-CHANGED' of fAction
OR 'VALUE-CHANGED' of fDestination
DO:
  /* Enable/disable filter button based on selected values */
  run setFilterButton in this-procedure. 
  
  /* This will use the screen-value of the filters */
  run filterData in this-procedure.
  
  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActionButtons in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fEntityParam
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fEntityParam C-Win
ON VALUE-CHANGED OF fEntityParam IN FRAME fDest /* Entity */
OR 'VALUE-CHANGED' of fActionParam
OR 'VALUE-CHANGED' of fDestinationParam
DO:
   resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON LEAVE OF fSearch IN FRAME fDest /* Search */
OR 'leave' of fSearch
DO:
  /* Enable/disable filter button based on selected values */
  run setFilterButton in this-procedure. 
  
  /* This will use the screen-value of the filters */
  run filterData in this-procedure.
  
  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActionButtons in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/brw-main.i}
{lib/win-main.i}
{lib/win-status.i &entity="'Destination'"}
{lib/win-close.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/set-button.i &label="getData"  &image="images/completed.bmp" &inactive="images/completed-i.bmp" }
{lib/set-button.i &label="Refresh"}
{lib/set-button.i &label="Export"}
{lib/set-button.i &label="New"}
{lib/set-button.i &label="Modify"}
{lib/set-button.i &label="Delete"}
{lib/set-button.i &label="Copy"}
{lib/set-button.i &label="FilterClear"}


setButtons().
clearStatus().
/* Initialise filters to default values */
assign
    fAction:list-item-pairs      = "ALL,ALL"
    fEntity:list-item-pairs      = "ALL,ALL"
    fDestination:list-item-pairs = "ALL,ALL"
    fAction:screen-value         = {&ALL}
    fEntity:screen-value         = {&ALL}
    fDestination:screen-value    = {&ALL} 
    .

assign
    fActionParam:list-item-pairs      = "ALL,ALL"
    fEntityParam:list-item-pairs      = "ALL,ALL"
    fDestinationParam:list-item-pairs = "ALL,ALL"
    fActionParam:screen-value         = {&ALL} 
    fEntityParam:screen-value         = {&ALL}
    fDestinationParam:screen-value    = {&ALL}
    . 
    

run getParametersData in this-procedure.
  
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  RUN enable_UI.
  
  publish "GetCurrentValue" ("DestinationAction", output cDestAction).
  publish "GetAppCode" (output cAppCode).
 
  if cAppCode = "ARM" and cDestAction = "productionFileActivity" 
   then
   do:
     publish "GetSysDestEntity"(output cSysDestEntity).
     publish "GetSysDestAction"(output cSysDestAction).
    
     assign
         fEntityParam:screen-value = cSysDestEntity
         fActionParam:screen-value = cSysDestAction.
     run getData in this-procedure.
   end.

  /* Enable/disable action buttons */
  run enableActionButtons in this-procedure.
        
  run showWindow in this-procedure.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clearFilters C-Win 
PROCEDURE clearFilters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Reset filters to initial state */
  assign
      fEntity:screen-value       = {&ALL}
      fAction:screen-value       = {&ALL}
      fSearch:screen-value       = ""
      fDestination:screen-value  = {&ALL}
      .
 
  /* Enable/disable filter button based on selected values */
  run setFilterButton in this-procedure.
  
  /* This will use the screen-value of the filters which is ALL */
  run filterData  in this-procedure.
   
  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActionButtons in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copySysDest C-Win 
PROCEDURE copySysDest :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/   
 define variable cMsg as character no-undo.
 define buffer tempsysdest for tempsysdest.
  
  if not available sysdest
   then return.
  
  do with frame {&frame-name}:
  end.
  
  empty temp-table tempsysdest.
  
  create tempsysdest.
  buffer-copy sysdest to tempsysdest.
  
  do iSelectedRow = 1 TO {&browse-name}:num-iterations:
    if {&browse-name}:is-row-selected(iSelectedRow)
     then leave.
  end.
  
  run dialogdestination.w (input        true,
                           input        "",
                           input        "",
                           input        "",
                           input-output table tempsysdest,
                                 output std-lo).
  if not std-lo
   then return.

  publish "NewSysDest" (table tempsysdest,output iDestID, output std-lo,output cMsg).
  if not std-lo 
   then 
     do:
      message  cMsg
          view-as alert-box information buttons ok.
      return. 
     end.
   
  run getData in this-procedure.
  
  /* Enable/disable action buttons */
  run enableActionButtons in this-procedure.
  
  find first sysdest where sysdest.destID = iDestID no-error.
  if not available sysdest
   then return.
  
  {&browse-name}:set-repositioned-row(iSelectedRow,"ALWAYS").
  reposition {&browse-name} to rowid rowid(sysdest). 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteSysDest C-Win 
PROCEDURE deleteSysDest :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cMsg as character no-undo.
  if not available sysdest
   then return.

  {lib/confirm-delete.i "Destination"}
  
  if not std-lo 
   then return. 
   
  publish "DeleteSysDest" (input sysdest.destID, output std-lo, output cMsg).
  if not std-lo 
   then return.

  run getData in this-procedure.
  
  /* Enable/disable action buttons */
  run enableActionButtons in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableActionButtons C-Win 
PROCEDURE enableActionButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  std-lo = can-find(first sysdest).
  do with frame {&frame-name}:
    assign
        bCopy:sensitive    = std-lo
        bModify:sensitive  = std-lo
        bDelete:sensitive  = std-lo
        bExport:sensitive  = std-lo
        bRefresh:sensitive = true
        bNew:sensitive     = true
        .
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fEntityParam fDestinationParam fActionParam fAction fDestination 
          fEntity fSearch 
      WITH FRAME fDest IN WINDOW C-Win.
  ENABLE bGetdata fEntityParam fDestinationParam fActionParam fAction 
         fDestination fEntity fSearch brwData bRefresh bCopy bDelete bExport 
         bModify bNew RECT-3 RECT-4 
      WITH FRAME fDest IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fDest}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query {&browse-name}:num-results = 0 
   then
    do: 
      MESSAGE "There is nothing to export" VIEW-AS ALERT-BOX warning BUTTONS OK.
      return.
    end.


  &scoped-define ReportName "SystemDestinations"

  std-ch = "C".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwData.
  empty temp-table sysdest.
  
  do with frame {&frame-name}:
  end.

  define buffer origdata for origdata.
  define variable cDestAction as character no-undo.

  /* action specific data */
  publish "GetCurrentValue" ("DestinationAction", output cDestAction).
  if cDestAction <> "" 
   then
   for each origdata 
    break by origdata.action:
    if first-of(origdata.action) and origdata.action = cDestAction
     then
       fAction:screen-value         = cDestAction.
       
   end.
  for each origdata 
    where origdata.entityType = (if fEntity:input-value       = {&ALL} then origdata.entityType  else fEntity:input-value)
      and origdata.action     = (if fAction:input-value       = {&ALL} then origdata.action      else fAction:input-value)      
      and origdata.destType   = (if fDestination:input-value  = {&ALL} then origdata.destType    else fDestination:input-value):
    
    origdata.entityName = if origdata.entityName = "" then "Default" else origdata.entityName.
    
    /* test if the record contains the search text */
    if fSearch:input-value <> "" and 
      not ((origdata.entityDesc   matches "*" + fSearch:input-value + "*")  or 
           (origdata.entityID     matches "*" + fSearch:input-value + "*")  or
           (origdata.entityName   matches "*" + fSearch:input-value + "*")  or
           (origdata.actionDesc   matches "*" + fSearch:input-value + "*")  or
           (origdata.destTypeDesc matches "*" + fSearch:input-value + "*")  or
           (origdata.destName     matches "*" + fSearch:input-value + "*"))
     then next.
      
    create sysdest.
    buffer-copy origdata to sysdest.
  end.   
  
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define buffer origdata for origdata.
   
  empty temp-table origdata.
  
  publish "GetSysDests" (input fEntityParam:input-value,
                         input fDestinationParam:input-value,
                         input fActionParam:input-value,
                         output table origdata).
                         
  run initialiseFilter in this-procedure.
  
  /* Enable/disable filter button based on selected values */
  run setFilterButton in this-procedure. 
  
  /* This will use the screen-value of the filters */
  run filterData in this-procedure.
  
   /* Enable/disable action buttons */
  run enableActionButtons in this-procedure.
        
  /* Set Status count with date and time from the server */
  setStatusRecords(num-results("{&browse-name}")).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getParametersData C-Win 
PROCEDURE getParametersData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable listCount as integer no-undo.
  
  do with frame {&frame-name}:
  end.
  
  define buffer ttSysAction for ttSysAction.
  define variable cEntity      as character no-undo.
  define variable cDestination as character no-undo.
    
  empty temp-table ttSysAction.
  empty temp-table sysaction.
  publish "GetSysActionEntity" (output table ttSysAction,
                                output cEntity ,
                                output cDestination).
  
  fDestinationParam:list-item-pairs = "ALL,ALL" + (if cDestination <> ""  then ","  else "" ) + cDestination.   
    
  do listCount = 1 to num-entries(cEntity,","):
   
    create sysDestEntityList.
    assign 
        sysDestEntityList.sysDestItemName = entry(listCount,cEntity)
        sysDestEntityList.sysDestItemValue = entry(listCount + 1,cEntity) 
        .
    listCount = listCount + 1.
  end.
  
  for each sysDestEntityList by sysDestEntityList.sysDestItemName:
    fEntityParam:list-item-pairs =  fEntityParam:list-item-pairs + "," + sysDestEntityList.sysDestItemName + "," + sysDestEntityList.sysDestItemValue.
  end.
   
  for each ttSysAction 
   break by ttSysAction.action:
    if first-of(ttSysAction.action) and ttSysAction.action <> ""
     then 
      do:
        create sysAction.
        buffer-copy ttSysAction to sysAction.
      end.
  end.
  
  for each sysAction by sysAction.name:
      fActionParam:add-last(sysAction.name, sysAction.action).
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initialiseFilter C-Win 
PROCEDURE initialiseFilter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define buffer origdata for origdata.

  assign
    fAction:list-item-pairs      = "ALL,ALL"
    fEntity:list-item-pairs      = "ALL,ALL"
    fDestination:list-item-pairs = "ALL,ALL"
    fAction:screen-value         = {&ALL}
    fEntity:screen-value         = {&ALL}
    fDestination:screen-value    = {&ALL}  
    .
 
  /* Set the filters based on the data returned, with ALL as the first option */
  for each origdata 
    break by origdata.entityType:
    if first-of(origdata.entityType) and origdata.entityType <> "" 
     then
      fEntity:add-last(origdata.entityDesc, origdata.entityType).
      
  end.
    
  for each origdata 
    break by origdata.action:
    if first-of(origdata.action) and origdata.action <> ""
     then 
      fAction:add-last(origdata.actionDesc, origdata.action).
  end.
                             
  for each origdata 
    break by origdata.destType:
    if first-of(origdata.destType) and origdata.destType <> ""
     then 
      fDestination:add-last(origdata.destTypeDesc, origdata.destType).
  end.
     
  assign
      fDestination:screen-value = cDestinations
      fAction:screen-value      = cActions
      fEntity:screen-value      = cEntitys
      fSearch:screen-value      = cSearch
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifySysDest C-Win 
PROCEDURE modifySysDest :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
 define variable cMsg as character no-undo.
 define buffer tempsysdest for tempsysdest.
  
  if not available sysdest
   then return.
  
  do with frame {&frame-name}:
  end.
  
  empty temp-table tempsysdest.
  
  create tempsysdest.
  buffer-copy sysdest to tempsysdest.
  iDestID = sysdest.destID.
  
  do iSelectedRow = 1 TO {&browse-name}:num-iterations:
    if {&browse-name}:is-row-selected(iSelectedRow)
     then leave.
  end.
  
  run dialogdestination.w (input        false,
                           input        "",
                           input        "",
                           input        "",
                           input-output table tempsysdest,
                                 output std-lo).
  if not std-lo
   then return.
   
   
  publish "ModifySysDest" (table tempsysdest, output std-lo, output cMsg).
  if not std-lo 
   then 
    do:
      message   cMsg
          view-as alert-box information  buttons ok.
      return. 
    end.

  run getData in this-procedure.
  
  /* Enable/disable action buttons */
  run enableActionButtons in this-procedure.
  
  find first sysdest where sysdest.destID = iDestID no-error.
  if not available sysdest
   then return.
  
  {&browse-name}:set-repositioned-row(iSelectedRow,"ALWAYS").
  reposition {&browse-name} to rowid rowid(sysdest).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newSysDest C-Win 
PROCEDURE newSysDest :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iDest as integer   no-undo.
  define variable cMsg  as character no-undo.

  define buffer tempsysdest for tempsysdest.
  empty temp-table tempsysdest.
  
  do with frame {&frame-name}:
  end.
  
  do iSelectedRow = 1 TO {&browse-name}:num-iterations:
    if {&browse-name}:is-row-selected(iSelectedRow)
     then leave.
  end.
  
  run dialogdestination.w (input        true,
                           input        "",
                           input        "",
                           input        "",
                           input-output table tempsysdest,
                                 output std-lo).
    
  if not std-lo 
   then return.
  
  publish "NewSysDest" (table tempsysdest,output iDest,output std-lo,output cMsg).
  if not std-lo 
   then 
     do:
      message  cMsg
          view-as alert-box information buttons ok.
      return. 
     end.
   
  run getData in this-procedure.
  
  /* Enable/disable action buttons */
  run enableActionButtons in this-procedure.
  
  find first sysdest where sysdest.destID = iDestID no-error.
  if not available sysdest
   then return.
  
  {&browse-name}:set-repositioned-row(iSelectedRow,"ALWAYS").
  reposition {&browse-name} to rowid rowid(sysdest).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData C-Win 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "LoadSysDests".
  
  run getData in this-procedure.
  
  /* Enable/disable action buttons */
  run enableActionButtons in this-procedure.
  
  /* Set Status count with date and time from the server */
  setStatusRecords(num-results("{&browse-name}")).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterButton C-Win 
PROCEDURE setFilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if fEntity:input-value      ne {&ALL} or 
     fAction:input-value      ne {&ALL} or 
     fDestination:input-value ne {&ALL} or
     fSearch:input-value      ne ""
   then
    bFilterClear:sensitive = true.
   else   
    bFilterClear:sensitive = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  {lib/brw-sortData.i &pre-by-clause="tWhereClause +" &post-by-clause="+ ' by entityType by entityID by action by destType'"}
  setStatusCount(num-results("{&browse-name}")).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  /* {&frame-name} components */
  {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 4.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
    setStatusMessage({&ResultNotMatch}).
    return true.
   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

