&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wPersons.w

  Description:Person Maintainance

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Shubham

  Created: 01.28.2020     
   Modified    :                   
   Date        Name        Description                
   07/12/2021  Shefali     Task 79221 Modified to reflect the persons updated data in 
                                      Person Management.
   03.29.2022   Shefali    Task# 86699  Modified in exportData IP.
   10/28/2022    SA        Task #96812 Modified to add logic according to personagent.
   02/24/2023  Shefali     Task# 102649 Parameterize the Person Screen.
   06/26/2023  SB          Task #104008 Modified to implement tags for person
   07/21/2023  SK          Add Agents tab in the person detail screen and make it default from the AMD APP.
   04/04/2024  SB          Task# 111689 Modified to remove the reference of title
   04/04/2024  SR          Task #111689 Modified fields to contactPhone,contactEmail and contactMobile.
------------------------------------------------------------------------*/
create widget-pool.
{tt/person.i &tableAlias="tPerson"}
{tt/person.i &tableAlias="ttPerson"}
define temp-table person like tperson
  field icount as integer.
{tt/person.i &tableAlias="tagperson"}
{tt/state.i}
{tt/tag.i}
{tt/tag.i &tableAlias=temptag}
{tt/tag.i &tableAlias=removedtag}

define variable cStateId              as character  no-undo.
define variable dColumnWidth          as decimal    no-undo.
define variable cPersonID             as character  no-undo.
define variable cNameBegins           as character  no-undo.
define variable cNameType             as character  no-undo.
/* track status change to set status in filter*/
define variable cStatusDateTime       as logical    no-undo.
define variable cCurrUser             as character  no-undo.
define variable cCurrentUID           as character  no-undo.
define variable ivarcount             as integer    no-undo.
define variable iPersonSelected       as integer    no-undo.
define variable personIDList          as character  no-undo.
define variable hFileDataSrv          as handle     no-undo.

{lib/std-def.i}
{lib/com-def.i}

{lib/winlaunch.i}
{lib/get-column.i}
{lib/winshowscrollbars.i}
{lib/find-widget.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES person

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData person.selectrecord person.dispname person.contactEmail person.contactPhone person.fulladdress person.personID   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData person.selectrecord   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwData person
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwData person
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData preselect each person by person.dispname
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} preselect each person by person.dispname.
&Scoped-define TABLES-IN-QUERY-brwData person
&Scoped-define FIRST-TABLE-IN-QUERY-brwData person


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bGo RECT-61 RECT-63 fSearch cbState ~
personType brwData btn1 btn10 btn11 btn12 btn13 btn14 btn15 btn16 btn17 ~
btn18 btn19 btn2 btn20 btn21 btn22 btn23 btn24 btn25 btn26 btn3 btn4 btn5 ~
btn6 btn7 btn8 btn9 btnA btnB btnC btnD btnE btnF btnG btnH btnI btnJ btnK ~
btnL btnM btnN btnO btnP btnQ btnR btnS btnT btnU btnV btnW btnX btnY btnZ ~
bdelete bEdit bExport bNew 
&Scoped-Define DISPLAYED-OBJECTS fSearch cbState personType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getStatDesc C-Win 
FUNCTION getStatDesc RETURNS CHARACTER
  ( cStat as LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwData 
       MENU-ITEM m_Select       LABEL "Select"        
       RULE
       MENU-ITEM m_Add_Tag      LABEL "Tag"           
       MENU-ITEM m_Un-Tag       LABEL "Un-Tag"        
       MENU-ITEM m_Email        LABEL "Email"         .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bdelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete person".

DEFINE BUTTON bEdit  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify person".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "New person".

DEFINE BUTTON btn1  NO-FOCUS NO-CONVERT-3D-COLORS
     LABEL "A" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn10  NO-FOCUS
     LABEL "J" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn11  NO-FOCUS
     LABEL "K" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn12  NO-FOCUS
     LABEL "L" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn13  NO-FOCUS
     LABEL "M" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn14  NO-FOCUS
     LABEL "N" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn15  NO-FOCUS
     LABEL "O" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn16  NO-FOCUS
     LABEL "P" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn17  NO-FOCUS
     LABEL "Q" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn18  NO-FOCUS
     LABEL "R" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn19  NO-FOCUS
     LABEL "S" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn2  NO-FOCUS
     LABEL "B" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn20  NO-FOCUS
     LABEL "T" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn21  NO-FOCUS
     LABEL "U" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn22  NO-FOCUS
     LABEL "V" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn23  NO-FOCUS
     LABEL "W" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn24  NO-FOCUS
     LABEL "X" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn25  NO-FOCUS
     LABEL "Y" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn26  NO-FOCUS
     LABEL "Z" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn3  NO-FOCUS
     LABEL "C" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn4  NO-FOCUS
     LABEL "D" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn5  NO-FOCUS
     LABEL "E" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn6  NO-FOCUS
     LABEL "F" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn7  NO-FOCUS
     LABEL "G" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btn8  NO-FOCUS
     LABEL "H" 
     SIZE 3.6 BY .81 TOOLTIP "Last Name".

DEFINE BUTTON btn9  NO-FOCUS
     LABEL "I" 
     SIZE 3.6 BY .86 TOOLTIP "Last Name".

DEFINE BUTTON btnA  NO-FOCUS NO-CONVERT-3D-COLORS
     LABEL "A" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnB  NO-FOCUS
     LABEL "B" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnC  NO-FOCUS
     LABEL "C" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnD  NO-FOCUS
     LABEL "D" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnE  NO-FOCUS
     LABEL "E" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnF  NO-FOCUS
     LABEL "F" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnG  NO-FOCUS
     LABEL "G" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnH  NO-FOCUS
     LABEL "H" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnI  NO-FOCUS
     LABEL "I" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnJ  NO-FOCUS
     LABEL "J" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnK  NO-FOCUS
     LABEL "K" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnL  NO-FOCUS
     LABEL "L" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnM  NO-FOCUS
     LABEL "M" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnN  NO-FOCUS
     LABEL "N" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnO  NO-FOCUS
     LABEL "O" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnP  NO-FOCUS
     LABEL "P" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnQ  NO-FOCUS
     LABEL "Q" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnR  NO-FOCUS
     LABEL "R" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnS  NO-FOCUS
     LABEL "S" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnT  NO-FOCUS
     LABEL "T" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnU  NO-FOCUS
     LABEL "U" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnV  NO-FOCUS
     LABEL "V" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnW  NO-FOCUS
     LABEL "W" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnX  NO-FOCUS
     LABEL "X" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnY  NO-FOCUS
     LABEL "Y" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON btnZ  NO-FOCUS
     LABEL "Z" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 21.4 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 71.6 BY 1 TOOLTIP "Enter keywords or #tags separated by spaces" NO-UNDO.

DEFINE VARIABLE personType AS CHARACTER INITIAL "A" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Active", "A":U,
"Inactive", "I":U,
"Both", "B":U
     SIZE 35 BY 1.38 NO-UNDO.

DEFINE RECTANGLE RECT-61
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 30.4 BY 2.38.

DEFINE RECTANGLE RECT-63
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 119.2 BY 2.38.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      person SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      person.selectrecord                      column-label  "Select" view-as toggle-box
person.dispname           format "x(100)"        label "Name"           width 30
person.contactEmail              format "x(50)"        label "EmailID"         width 30
person.contactPhone             format "x(13)"        label "Phone"           width 20
person.fulladdress      format "x(250)"        label "Address"            width 30 
person.personID           format "x(20)"        label "Person ID"       width 8
enable person.selectrecord
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE NO-TAB-STOP SIZE 142.2 BY 20.86 ROW-HEIGHT-CHARS .82 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bGo AT ROW 1.62 COL 113 WIDGET-ID 74 NO-TAB-STOP 
     fSearch AT ROW 1.95 COL 9.4 COLON-ALIGNED WIDGET-ID 370
     cbState AT ROW 1.95 COL 88.6 COLON-ALIGNED WIDGET-ID 372
     personType AT ROW 3.67 COL 20.2 NO-LABEL WIDGET-ID 358
     brwData AT ROW 5.1 COL 5.8 WIDGET-ID 200
     btn1 AT ROW 5.05 COL 148 WIDGET-ID 432 NO-TAB-STOP 
     btn10 AT ROW 12.33 COL 148 WIDGET-ID 450 NO-TAB-STOP 
     btn11 AT ROW 13.14 COL 148 WIDGET-ID 452 NO-TAB-STOP 
     btn12 AT ROW 13.95 COL 148 WIDGET-ID 454 NO-TAB-STOP 
     btn13 AT ROW 14.76 COL 148 WIDGET-ID 456 NO-TAB-STOP 
     btn14 AT ROW 15.48 COL 148 WIDGET-ID 458 NO-TAB-STOP 
     btn15 AT ROW 16.19 COL 148 WIDGET-ID 460 NO-TAB-STOP 
     btn16 AT ROW 17 COL 148 WIDGET-ID 462 NO-TAB-STOP 
     btn17 AT ROW 17.81 COL 148 WIDGET-ID 464 NO-TAB-STOP 
     btn18 AT ROW 18.62 COL 148 WIDGET-ID 466 NO-TAB-STOP 
     btn19 AT ROW 19.43 COL 148 WIDGET-ID 468 NO-TAB-STOP 
     btn2 AT ROW 5.86 COL 148 WIDGET-ID 434 NO-TAB-STOP 
     btn20 AT ROW 20.24 COL 148 WIDGET-ID 470 NO-TAB-STOP 
     btn21 AT ROW 21.05 COL 148 WIDGET-ID 472 NO-TAB-STOP 
     btn22 AT ROW 21.86 COL 148 WIDGET-ID 474 NO-TAB-STOP 
     btn23 AT ROW 22.67 COL 148 WIDGET-ID 476 NO-TAB-STOP 
     btn24 AT ROW 23.48 COL 148 WIDGET-ID 478 NO-TAB-STOP 
     btn25 AT ROW 24.29 COL 148 WIDGET-ID 480 NO-TAB-STOP 
     btn26 AT ROW 25.1 COL 148 WIDGET-ID 482 NO-TAB-STOP 
     btn3 AT ROW 6.67 COL 148 WIDGET-ID 436 NO-TAB-STOP 
     btn4 AT ROW 7.48 COL 148 WIDGET-ID 438 NO-TAB-STOP 
     btn5 AT ROW 8.29 COL 148 WIDGET-ID 440 NO-TAB-STOP 
     btn6 AT ROW 9.1 COL 148 WIDGET-ID 442 NO-TAB-STOP 
     btn7 AT ROW 9.91 COL 148 WIDGET-ID 444 NO-TAB-STOP 
     btn8 AT ROW 10.76 COL 148 WIDGET-ID 446 NO-TAB-STOP 
     btn9 AT ROW 11.52 COL 148 WIDGET-ID 448 NO-TAB-STOP 
     btnA AT ROW 5.05 COL 2.2 WIDGET-ID 208 NO-TAB-STOP 
     btnB AT ROW 5.86 COL 2.2 WIDGET-ID 382 NO-TAB-STOP 
     btnC AT ROW 6.67 COL 2.2 WIDGET-ID 384 NO-TAB-STOP 
     btnD AT ROW 7.48 COL 2.2 WIDGET-ID 386 NO-TAB-STOP 
     btnE AT ROW 8.29 COL 2.2 WIDGET-ID 388 NO-TAB-STOP 
     btnF AT ROW 9.1 COL 2.2 WIDGET-ID 390 NO-TAB-STOP 
     btnG AT ROW 9.91 COL 2.2 WIDGET-ID 392 NO-TAB-STOP 
     btnH AT ROW 10.71 COL 2.2 WIDGET-ID 394 NO-TAB-STOP 
     btnI AT ROW 11.52 COL 2.2 WIDGET-ID 396 NO-TAB-STOP 
     btnJ AT ROW 12.33 COL 2.2 WIDGET-ID 398 NO-TAB-STOP 
     btnK AT ROW 13.14 COL 2.2 WIDGET-ID 400 NO-TAB-STOP 
     btnL AT ROW 13.95 COL 2.2 WIDGET-ID 430 NO-TAB-STOP 
     btnM AT ROW 14.76 COL 2.2 WIDGET-ID 428 NO-TAB-STOP 
     btnN AT ROW 15.48 COL 2.2 WIDGET-ID 426 NO-TAB-STOP 
     btnO AT ROW 16.19 COL 2.2 WIDGET-ID 424 NO-TAB-STOP 
     btnP AT ROW 17 COL 2.2 WIDGET-ID 422 NO-TAB-STOP 
     btnQ AT ROW 17.81 COL 2.2 WIDGET-ID 420 NO-TAB-STOP 
     btnR AT ROW 18.62 COL 2.2 WIDGET-ID 418 NO-TAB-STOP 
     btnS AT ROW 19.43 COL 2.2 WIDGET-ID 416 NO-TAB-STOP 
     btnT AT ROW 20.24 COL 2.2 WIDGET-ID 414 NO-TAB-STOP 
     btnU AT ROW 21.05 COL 2.2 WIDGET-ID 412 NO-TAB-STOP 
     btnV AT ROW 21.86 COL 2.2 WIDGET-ID 410 NO-TAB-STOP 
     btnW AT ROW 22.67 COL 2.2 WIDGET-ID 408 NO-TAB-STOP 
     btnX AT ROW 23.48 COL 2.2 WIDGET-ID 406 NO-TAB-STOP 
     btnY AT ROW 24.29 COL 2.2 WIDGET-ID 404 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 190.2 BY 25.52 WIDGET-ID 100.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME fMain
     btnZ AT ROW 25.1 COL 2.2 WIDGET-ID 402 NO-TAB-STOP 
     bdelete AT ROW 1.62 COL 142.8 WIDGET-ID 10 NO-TAB-STOP 
     bEdit AT ROW 1.62 COL 135.8 WIDGET-ID 8 NO-TAB-STOP 
     bExport AT ROW 1.62 COL 121.8 WIDGET-ID 2 NO-TAB-STOP 
     bNew AT ROW 1.62 COL 128.8 WIDGET-ID 6 NO-TAB-STOP 
     "Show Status :" VIEW-AS TEXT
          SIZE 14 BY .86 AT ROW 3.95 COL 6.2 WIDGET-ID 362
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.05 COL 122.6 WIDGET-ID 52
     "Parameter" VIEW-AS TEXT
          SIZE 10 BY .62 AT ROW 1.05 COL 3.6 WIDGET-ID 72
     RECT-61 AT ROW 1.29 COL 120.8 WIDGET-ID 50
     RECT-63 AT ROW 1.29 COL 2 WIDGET-ID 70
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 190.2 BY 25.52 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "People Management"
         HEIGHT             = 25.05
         WIDTH              = 151.2
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData personType fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:POPUP-MENU IN FRAME fMain             = MENU POPUP-MENU-brwData:HANDLE
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} preselect each person by person.dispname
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* People Management */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* People Management */
do:
  publish "WindowClosed" (input this-procedure).
  
  /* This event will close the window and terminate the procedure.  */
  apply "CLOSE":U to this-procedure.
  return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* People Management */
do:
  run windowResized in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bdelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bdelete C-Win
ON CHOOSE OF bdelete IN FRAME fMain /* Delete */
do:
  if not available person
   then 
    return.
  
  run actionDelete in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEdit C-Win
ON CHOOSE OF bEdit IN FRAME fMain /* Edit */
do:
  run actionModify in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:
  run searchPerson in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
do:
  run actionNew in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
do:
  run actionModify in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
do:
  define variable iSelectedPerson as integer no-undo. 
  define buffer person for person.
  
  std-ha = brwData:current-column.
  if std-ha:label = "Select" 
   then
    do:     
      std-lo = can-find(first person where not(person.selectrecord)).   
      iSelectedPerson = 0.
      for each person:            
        person.selectrecord = std-lo.
        if std-lo then
         assign iSelectedPerson = iSelectedPerson + 1.  
        else
         assign iSelectedPerson = iSelectedPerson - 1
                iPersonselected = 0.
         
        /* Retaining selected record */  
        for first tperson where tperson.personID = person.personID:
          tperson.selectrecord = person.selectrecord.
        end.
      end.    
      browse brwData:refresh(). 
      if iSelectedPerson > 1 then
      assign 
          bEdit:sensitive   = false
          bDelete:sensitive = false
          .     
     else
       assign 
           bEdit:sensitive   = true
           bDelete:sensitive = true
           .
    end.
   else
    do:
      {lib/brw-startsearch.i}
    end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  if brwData:num-selected-rows > 1 or iPersonselected > 1 then
      assign
          bEdit:sensitive   = false
          bDelete:sensitive = false.
     else
       assign
           bEdit:sensitive   = true
           bDelete:sensitive = true.
           
   run enableDisablePopupMenu in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn1 C-Win
ON CHOOSE OF btn1 IN FRAME fMain /* A */
,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn10,btn11,btn12,btn13,btn14,btn15,btn16,btn17,btn18,btn19,btn20,btn21,btn22,btn23,btn24,btn25,btn26 
DO:      
   assign
       cbState:screen-value = {&ALL}
       fSearch:screen-value = ""
       cNameType            = "LastName"
       cNameBegins          = self:label
       iPersonSelected      = 0
       .
   
   run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnA
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnA C-Win
ON CHOOSE OF btnA IN FRAME fMain /* A */
,btnB,btnC,btnD,btnE,btnF,btnG,btnH,btnI,btnJ,btnK,btnL,btnM,btnN,btnO,btnP,btnQ,btnR,btnS,btnT,btnU,btnV,btnW,btnX,btnY,btnZ 
DO:      
   assign
       cbState:screen-value = {&ALL}
       fSearch:screen-value = ""
       cNameType            = "FirstName"
       cNameBegins          = self:label
       iPersonSelected      = 0
       .
   
   run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState C-Win
ON VALUE-CHANGED OF cbState IN FRAME fMain /* State */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fMain /* Search */
DO:
  run searchPerson in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON VALUE-CHANGED OF fSearch IN FRAME fMain /* Search */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Add_Tag
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Add_Tag C-Win
ON CHOOSE OF MENU-ITEM m_Add_Tag /* Tag */
DO:
  run addtag in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Email
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Email C-Win
ON CHOOSE OF MENU-ITEM m_Email /* Email */
DO:
  define buffer person for person.
  
  if not can-find(first person)
   then
    return.

  assign personIDList = "".
  for each person where person.selectrecord = true:
    if person.doNotCall = true then
     next.
    assign 
        personIDList = if personIDList = "" then person.contactEmail else personIDList + ";" + person.contactEmail
        personIDList = trim(personIDList,";")
        .
  end.  
  run util/openURL.p ("mailto:?subject=%20" + "&bcc=" + personIDList).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Select
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Select C-Win
ON CHOOSE OF MENU-ITEM m_Select /* Select */
DO:
 define variable i as integer no-undo.
 
  do i = 1 to brwData:num-selected-rows in frame {&frame-name}:
    if brwData:FETCH-SELECTED-ROW(i) then 
      do:
        assign person.selectrecord = true.
        /* Retaining selected record */  
        for first tPerson where tPerson.personId = person.personId:
          tPerson.selectrecord = person.selectrecord.
        end.
      end.
  end.
  
  iPersonSelected = 0.
  for each person:
    if person.selectrecord then
      assign iPersonSelected = iPersonSelected + 1.
  end.
   if iPersonSelected > 1 then
      assign 
          bEdit:sensitive   = false
          bDelete:sensitive = false.     
     else
       assign 
           bEdit:sensitive   = true
           bDelete:sensitive = true. 
  
  browse brwData:refresh().
  run enableDisablePopupMenu in this-procedure.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Un-Tag
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Un-Tag C-Win
ON CHOOSE OF MENU-ITEM m_Un-Tag /* Un-Tag */
DO:
  run untagentity in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME personType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL personType C-Win
ON VALUE-CHANGED OF personType IN FRAME fMain
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i }

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

on close of this-procedure 
  run disable_UI.

pause 0 before-hide.

bExport :load-image("images/excel.bmp").
bExport :load-image-insensitive("images/excel-i.bmp").
bnew    :load-image("images/add.bmp").
bEdit   :load-image("images/update.bmp").
bEdit   :load-image-insensitive("images/update-i.bmp").
bDelete :load-image("images/delete.bmp").
bDelete :load-image-insensitive("images/delete-i.bmp").
bGo     :load-image("images/completed.bmp").
bGo     :load-image-insensitive("images/completed.bmp").

personType    :tooltip   = "Active = Person has at least one role or an affiliation or association to an agent ; Inactive = Person does not have any role, affiliation or association".

subscribe to "personModifiedData" anywhere. 
subscribe to "refreshpersonnames" anywhere.

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  publish "GetCredentialsName" (output cCurrUser).
  publish "GetCredentialsID" (output cCurrentUID).
  
  if std-ch = 'COM' 
   then 
    do:
      /* Get the selected states from the config settings. */
      publish "GetSearchStates" (output std-ch).
      
      /* Retain the current value of the state selected in the drop down box. */
      cStateId = cbState:input-value.
      
      if std-ch > "" 
       then
        std-ch = "," + std-ch.
        
      assign 
          cbState:list-item-pairs  = "ALL,ALL" + std-ch
          /* In case the current state is not available in the combo, then set "ALL", 
             otherwise, show the current value. */
          cbState:screen-value = if lookup(cStateId,cbState:list-item-pairs) > 0 then cStateId else {&ALL} 
          .
    end.
  else
   do: 
      {lib/get-state-list.i &combo=cbState &useFullName=true } 
      cbState:add-first(entry(1, "ALL"), entry(1, "ALL")).
      cbState:screen-value = if lookup(cStateId,cbState:list-item-pairs) > 0 then cStateId else {&ALL}. 
   end.
  
  run enable_UI.
  
  iPersonSelected = 0.
  on 'value-changed':U of  person.selectrecord in browse  brwData  
  do:
    do with frame {&frame-name}:
    end.
    if available person 
     then
      do:
        person.selectrecord = person.selectrecord:checked in browse brwData.
        if person.selectrecord then
         assign iPersonSelected = iPersonSelected + 1.  
        else 
         assign iPersonSelected = iPersonSelected - 1.
       
        /* Retaining selected record */  
        for first tPerson where tPerson.personId = person.personId:
          tPerson.selectrecord = person.selectrecord.
        end.
      end.
    browse brwData:refresh().  
    if iPersonSelected > 1 then
      assign 
          bEdit:sensitive   = false
          bDelete:sensitive = false
          .     
     else
       assign 
           bEdit:sensitive   = true
           bDelete:sensitive = true
           .  
     run enableDisablePopupMenu in this-procedure.
  end.
  
  run enableDisablePopupMenu in this-procedure.
         
  {lib/get-column-width.i &col="'dispname'" &var=dColumnWidth}
  
  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.
 
  if not this-procedure:persistent 
   then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionDelete C-Win 
PROCEDURE actionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  define variable lChoice as logical no-undo.
  
  if not available person 
   then
    return.
 
  lchoice = false.

  message "Highlighted Person will be deleted." skip "Do you want to continue?"
      view-as alert-box question buttons yes-no title "Delete Person" update lChoice.
 
  if not lChoice 
   then
    return.

  cPersonID = person.personID .
 
  publish "deletePerson" (input cPersonID,
                          output std-lo,
                          output std-ch).

  if not std-lo 
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
                               
  find first person
    where person.personID = cPersonID
    no-error.

  if available person
   then
     delete person.
   
  open query brwData preselect each person.
  
  run setButtons in this-procedure.

  setStatusRecords(query brwData:num-results).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionModify C-Win 
PROCEDURE actionModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  
 if std-ch = "AMD" 
  then
     do:
       publish "OpenWindow" (input {&person}, 
                             input person.personID, 
                             input "wpersondetail.w", 
                             input "character|input|" + person.personID + "^integer|input|6" + "^character|input|",                                   
                             input this-procedure).                                          
     end. 
 else
   do:
     publish "OpenWindow" (input {&person}, 
                           input person.personID, 
                           input "wpersondetail.w", 
                           input "character|input|" + person.personID + "^integer|input|2" + "^character|input|",                                   
                           input this-procedure).
      
   end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionNew C-Win 
PROCEDURE actionNew :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
  
  define variable cPersonID as character no-undo.
  define variable iCount    as integer   no-undo.

  run dialognewperson.w(input '',
                        output cPersonID,
                        output std-lo).   
                                                        
  if not std-lo 
   then
    return.
     
  /* Update the data from the data model. */
  publish "getperson" (input cPersonID, output table tPerson).

  for first tperson where tperson.personID = cPersonID:
    /* if mobile is not empty showing mobile on UI, otherwise phone */
    if tPerson.contactMobile ne "" and tPerson.contactMobile ne ?
     then
      tPerson.contactPhone = tPerson.contactMobile. 
      
    find first person where person.personID = tperson.personID no-error.
    if not avail person 
     then
      do:
        create person.
        buffer-copy tPerson to person.          
      end.  
  end.
      
  open query brwData preselect each person.
  
  run setButtons in this-procedure.

  find first person 
    where person.personID = cPersonID 
    no-error.

  if available person
   then
    std-ro = rowid(person).
  else 
   std-ro = ?.
                                                     
  if std-ro <> ? 
   then
    do:
      do iCount = 1 TO brwdata:num-iterations:
        if brwdata:is-row-selected(iCount)
         then
          leave.
      end.   
      brwdata:set-repositioned-row(iCount,"ALWAYS").
      reposition brwdata to rowid std-ro.
      brwdata:get-repositioned-row().

    end. 
    
  setStatusRecords(query brwData:num-results).
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addtag C-Win 
PROCEDURE addtag :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  empty temp-table temptag.
  
  define buffer person for person.
 
  for each person where person.selectrecord = true:
    create temptag.
    assign
        temptag.entity     = "P"
        temptag.entityID   = person.personID
        temptag.uid        = cCurrentUID
        temptag.username   = cCurrUser
        temptag.entityname = person.dispName
        .
  end.
  
  if can-find(first temptag) then
   do:
      run dialogtag.w(input hFileDataSrv,
                      input "N",
                      input-output table temptag,
                      output std-lo).
     if not std-lo
      then
       return no-apply. 
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisablePopupMenu C-Win 
PROCEDURE enableDisablePopupMenu :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable i                as integer no-undo.
  define variable lUnTagSensitive  as logical no-undo.
  
  do with frame {&frame-name}:
  end.
  
  do-blk:
  do i = 1 to num-entries(fSearch:screen-value," "):
    if (entry(i,fSearch:screen-value," ") begins "#" and num-entries(fSearch:screen-value," ") = 1)  THEN
    do:
      assign lUnTagSensitive = true.
      leave do-blk.
    end.
  end.

  if avail person and person.selectrecord = true and lUnTagSensitive 
   then
    assign menu-item m_Un-Tag:sensitive in menu POPUP-MENU-brwData = true.
  else 
    assign menu-item m_Un-Tag:sensitive in menu POPUP-MENU-brwData = false.
  
  if avail person 
   then
    do:
      if brwData:num-selected-rows ne 0 then 
        assign menu-item m_Select:sensitive in menu POPUP-MENU-brwData = true.
       else 
        assign menu-item m_Select:sensitive in menu POPUP-MENU-brwData = false.
      if person.selectrecord = true 
       then
        assign
            menu-item m_Add_Tag:sensitive in menu POPUP-MENU-brwData = true
            menu-item m_Email:sensitive in menu POPUP-MENU-brwData   = true.
       else
         assign
             menu-item m_Add_Tag:sensitive in menu POPUP-MENU-brwData = false
             menu-item m_Email:sensitive in menu POPUP-MENU-brwData   = false.
    end.
   else
    assign
        menu-item m_Select:sensitive in menu POPUP-MENU-brwData  = false
        menu-item m_Email:sensitive in menu POPUP-MENU-brwData   = false
        menu-item m_Add_Tag:sensitive in menu POPUP-MENU-brwData = false
        menu-item m_Email:sensitive in menu POPUP-MENU-brwData   = false.
         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fSearch cbState personType 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bGo RECT-61 RECT-63 fSearch cbState personType brwData btn1 btn10 
         btn11 btn12 btn13 btn14 btn15 btn16 btn17 btn18 btn19 btn2 btn20 btn21 
         btn22 btn23 btn24 btn25 btn26 btn3 btn4 btn5 btn6 btn7 btn8 btn9 btnA 
         btnB btnC btnD btnE btnF btnG btnH btnI btnJ btnK btnL btnM btnN btnO 
         btnP btnQ btnR btnS btnT btnU btnV btnW btnX btnY btnZ bdelete bEdit 
         bExport bNew 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
  
  define variable htableHandle as handle no-undo.
  
  empty temp-table tPerson.
 
  for each Person:
    create tPerson.
    buffer-copy Person to tPerson.
    tPerson.tempStat = getStatDesc(person.active).
  end.
  
  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
         view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
 
  htableHandle = temp-table tPerson:handle.
  run util/exporttable.p (table-handle htableHandle,
                          "tPerson",
                          "for each tPerson ",
                          "personID,dispname,tempStat,city,state,zip",
                          "Person ID,Name,Status,City,State,Zip Code",
                          std-ch,
                          "Person.csv-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).   
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  close query brwData.
  assign
      ivarcount       = 0
      iPersonSelected = 0.
  empty temp-table person.
  for each tPerson where tPerson.tagby = cCurrentUID and
                         tperson.active = (if personType:input-value = "A" then true 
                                      else if personType:input-value = "B" then tperson.active
                                      else false):
      create person.
      buffer-copy tPerson to person.
      assign 
          ivarcount     = ivarcount + 1.
          person.icount = ivarcount.
  end.
  for each tPerson where tPerson.tagby <> cCurrentUID and
                         tperson.active = (if personType:input-value = "A" then true 
                                      else if personType:input-value = "B" then tperson.active
                                      else false):
      create person.
      buffer-copy tPerson to person.
      assign
          ivarcount     = ivarcount + 1.
          person.icount = ivarcount.
  end.
  
  open query brwData preselect each person by person.icount.
      
  run enableDisablePopupMenu in this-procedure.
  run setButtons in this-procedure.
  
  setStatusRecords(query brwData:num-results).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table tPerson.
  
  do with frame {&Frame-name}:
  end.
  
  publish "searchpersons" (input cbState:screen-value,
                           input fSearch:input-value,
                           input if fSearch:input-value <> "" then "" else cNameType,
                           input if fSearch:input-value <> "" then "" else cNameBegins,
                           output table tPerson,
                           output std-lo,
                           output std-ch).     
                           
   /* if mobile is not empty showing mobile on UI, otherwise phone */
  for each tPerson:
    if tPerson.contactMobile ne "" and tPerson.contactMobile ne ?
     then
      tPerson.contactPhone = tPerson.contactMobile.      
  end.
  run filterData in this-procedure.
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE personModifiedData C-Win 
PROCEDURE personModifiedData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter cPersonID  as character  no-undo.
  define input parameter table for tPerson.
  
  define variable iCount            as integer    no-undo.
  
  do with frame {&FRAME-NAME}:
  end.

  /* if mobile is not empty showing mobile on UI, otherwise phone */
  for each tPerson:
    if tPerson.contactMobile ne "" and tPerson.contactMobile ne ?
     then
      tPerson.contactPhone = tPerson.contactMobile.      
  end.
  
  /* Update the data from the data model. */
  
  for first tPerson where tperson.personID = cPersonID:
    find first person where person.personID = tperson.personID no-error.
    if avail person then
      buffer-copy tPerson to person.
  end.  
    
  open query brwData preselect each person.
  
  run setButtons in this-procedure.

  find first person
    where person.personID = cPersonID
    no-error.

  if available person
   then
    std-ro = rowid(person).
  else
    std-ro = ?.
  
  if std-ro <> ?
   then
    do:
      do iCount = 1 TO brwdata:num-iterations:
        if brwdata:is-row-selected(iCount)
         then
          leave.
      end.
      brwdata:set-repositioned-row(iCount,"ALWAYS").
      reposition brwdata to rowid std-ro.
      brwdata:get-repositioned-row().
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshpersonnames C-Win 
PROCEDURE refreshpersonnames :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ilsuccess as logical no-undo.
  if ilsuccess
   then
    do:
      publish  "getPerson"(output table ttPerson).
      
      for first ttPerson no-lock:
        for first tPerson where tPerson.personID = ttPerson.personID:
          assign  
              tPerson.firstName     = ttPerson.firstName   
              tPerson.middleName    = ttPerson.middleName
              tPerson.lastName      = ttPerson.lastName   
              tPerson.dispName      = ttPerson.dispName
              tperson.linkedToAgent = ttperson.linkedToAgent
              tperson.active        = ttperson.active
              
              .         
        end.
      end.
      
      run filterData in this-procedure.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE searchPerson C-Win 
PROCEDURE searchPerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
 assign 
      cNameType       = ""
      cNameBegins     = ""
      iPersonSelected = 0.
      
  if fSearch:screen-value = "" and cbState:screen-value = {&ALL} then
  do:
    message "The query will fetch all the records." skip "Do you want to continue?"
      view-as alert-box question buttons yes-no
      title "Confirmation"
      update lContinue as logical.
  
    if not lContinue
     then
      return.   
  end.
  run getData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setButtons C-Win 
PROCEDURE setButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if query brwData:num-results > 0 
   then
    assign 
        bExport       :sensitive  = true
        bEdit         :sensitive  = true
        bDelete       :sensitive  = true
        .
   else
    assign
        bExport        :sensitive = false
        bEdit          :sensitive = false
        bDelete        :sensitive = false
        .

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i  &post-by-clause=" + ' by dispname ' "}

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE untagentity C-Win 
PROCEDURE untagentity :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  define buffer person for person.
  
  empty temp-table temptag.
  empty temp-table removedtag.

  for each person where person.selectrecord = true:
    create temptag.
    assign
        temptag.entity     = "P"
        temptag.entityID   = person.personID
        temptag.uid        = person.tagBy
        temptag.entityname = person.dispName
        temptag.name       = left-trim(fSearch:screen-value ,"#")
        .
  end.
  if can-find(first temptag) then
   do: 
     message "Tag " left-trim(fSearch:screen-value ,"#") "for selected people will be deleted." skip "Do you want to continue?"
       view-as alert-box question buttons yes-no
       title "Delete Tag"
       update lContinue as logical.
  
     if not lContinue
      then
       return.
  
     publish "untagentity" (input table temptag,
                            output table removedtag,
                            output std-lo,
                            output std-ch).

     if not std-lo
      then
       return no-apply.
   end.  
   if std-ch ne "" then
     message std-ch
         view-as alert-box information button ok.
 
   for each removedtag:
     for each tPerson where tPerson.personID = removedtag.entityID:
       delete tPerson.
     end.
   end.
  
   run filterData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount     as integer init 1 no-undo.
  define variable hBtnHandle as handle         no-undo.
  
  assign 
      frame fMain:width-pixels          = {&window-name}:width-pixels
      frame fMain:virtual-width-pixels  = {&window-name}:width-pixels
      frame fMain:height-pixels         = {&window-name}:height-pixels
      frame fMain:virtual-height-pixels = {&window-name}:height-pixel
      /* fMain Components */
      brwData:width-pixels              = frame fmain:width-pixels - 47
      
      brwData:height-pixels             = frame fmain:height-pixels - brwData:y - 2.8
      .
  
  repeat iCount = 1 to 26:
    hBtnHandle = GetWidgetByName(frame fMain:handle, "btn" + string(iCount)). 
    hBtnHandle:x = brwData:width-pixels + 24.
  end.
      
  {lib/resize-column.i &col="'fulladdress,dispname'" &var=dColumnWidth}
  run ShowScrollBars(browse brwData:handle, no, yes).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getStatDesc C-Win 
FUNCTION getStatDesc RETURNS CHARACTER
  ( cStat as LOGICAL ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cStatReturn as character no-undo.
  if cStat 
   then 
    cStatReturn = {&Active}.
  else
   cStatReturn  = {&InActive}.
    
  return cStatReturn.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 setStatusMessage({&ResultNotMatch}).
 cStatusDateTime = false.
 return true.
 
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

