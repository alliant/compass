&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wagentfile.w

  Description: User Interface for agent file

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul Sharma

  Created: 10.22.2019
  
  Modified:
 
  Date        Name      Description
  06-03-2024  Sachin    Task 112908- Changes in Agentfile related to Notes, Source and CPLAddr1, etc.
------------------------------------------------------------------------*/
create widget-pool.

/* Standard Library Files */
{lib/std-def.i}
{lib/ar-def.i}
{lib/winshowscrollbars.i}

/* Temp-table Definition */
{tt/agentfile.i}
{tt/agentfile.i &tableAlias=ttagentfile}
{tt/agentfile.i &tableAlias=tagentfile}


define variable hFiledetail as handle.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES agentfile

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData agentfile.fileID "File ID" agentfile.fileNumber "File Number" getFileStatus(agentfile.stat) @ agentfile.stat agentfile.stage "Stage" agentfile.reportingDate "Reporting" agentfile.fileType "File Type" agentfile.source "Source" agentfile.transactionType agentfile.liabilityAmt "Liability"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each agentfile
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each agentfile.
&Scoped-define TABLES-IN-QUERY-brwData agentfile
&Scoped-define FIRST-TABLE-IN-QUERY-brwData agentfile


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btGet bSearch flAgentID bAgentLookup flFile ~
tgClosedFiles fSearch brwData RECT-61 RECT-62 RECT-83 
&Scoped-Define DISPLAYED-OBJECTS flAgentID flName flFile tgClosedFiles ~
fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFileStatus C-Win 
FUNCTION getFileStatus RETURNS CHARACTER
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VARIABLE C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup 
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete record".

DEFINE BUTTON bEdit  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Edit".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON bHistory  NO-FOCUS
     LABEL "History" 
     SIZE 7.2 BY 1.71 TOOLTIP "View File History".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "New".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search Data".

DEFINE BUTTON btGet  NO-FOCUS
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get Data".

DEFINE BUTTON bViewFile  NO-FOCUS
     LABEL "ViewFile" 
     SIZE 7.2 BY 1.71 TOOLTIP "View Agent File".

DEFINE VARIABLE flAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE flFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 19 BY 1 TOOLTIP "Begins" NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 57 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 41.4 BY 1 TOOLTIP "Search Criteria (fileID,fileNumber,stage,notes)" NO-UNDO.

DEFINE RECTANGLE RECT-61
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 46.4 BY 4.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 62.2 BY 4.

DEFINE RECTANGLE RECT-83
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 79.2 BY 4.

DEFINE VARIABLE tgClosedFiles AS LOGICAL INITIAL no 
     LABEL "Include Closed Files" 
     VIEW-AS TOGGLE-BOX
     SIZE 23.2 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      agentfile SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      agentfile.fileID               label      "File ID"      format "x(24)"
agentfile.fileNumber           label      "File Number"        format "X(30)" width 25
getFileStatus(agentfile.stat) @ agentfile.stat  label "Status"  format "x(10)"
agentfile.stage                label      "Stage"              format "x(20)"
agentfile.reportingDate        label      "Reporting"          format "99/99/9999"  width 16
agentfile.fileType              label      "File Type"      format "x(24)" width 17
agentfile.source           label      "Source"        format "x(20)"
agentfile.transactionType  label "Transaction Type"  format "x(20)"  width 25
agentfile.liabilityAmt                label      "Liability"              format "->>>,>>>,>>9"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 186.8 BY 17.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     btGet AT ROW 2.62 COL 72.2 WIDGET-ID 266 NO-TAB-STOP 
     bDelete AT ROW 2.62 COL 166 WIDGET-ID 4 NO-TAB-STOP 
     bSearch AT ROW 2.62 COL 134 WIDGET-ID 308 NO-TAB-STOP 
     flAgentID AT ROW 1.81 COL 11.4 COLON-ALIGNED WIDGET-ID 352
     bAgentLookup AT ROW 1.71 COL 33.6 WIDGET-ID 350
     flName AT ROW 2.95 COL 11.4 COLON-ALIGNED NO-LABEL WIDGET-ID 354
     flFile AT ROW 4.1 COL 11.4 COLON-ALIGNED WIDGET-ID 330
     tgClosedFiles AT ROW 4.19 COL 33.4 WIDGET-ID 334
     fSearch AT ROW 2.95 COL 88.6 COLON-ALIGNED WIDGET-ID 310
     brwData AT ROW 5.86 COL 2.4 WIDGET-ID 200
     bHistory AT ROW 2.62 COL 180 WIDGET-ID 332 NO-TAB-STOP 
     bEdit AT ROW 2.62 COL 159 WIDGET-ID 8 NO-TAB-STOP 
     bExport AT ROW 2.62 COL 145 WIDGET-ID 2 NO-TAB-STOP 
     bNew AT ROW 2.62 COL 152 WIDGET-ID 6 NO-TAB-STOP 
     bViewFile AT ROW 2.62 COL 173 WIDGET-ID 20 NO-TAB-STOP 
     "Filters" VIEW-AS TEXT
          SIZE 5.4 BY .62 AT ROW 1.14 COL 82 WIDGET-ID 320
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.1 COL 144 WIDGET-ID 52
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.1 COL 3.2 WIDGET-ID 326
     RECT-61 AT ROW 1.48 COL 143 WIDGET-ID 50
     RECT-62 AT ROW 1.48 COL 81.2 WIDGET-ID 312
     RECT-83 AT ROW 1.48 COL 2.4 WIDGET-ID 324
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COLUMN 1 ROW 1
         SIZE 189.6 BY 22.86
         DEFAULT-BUTTON bSearch WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent Files"
         HEIGHT             = 22.86
         WIDTH              = 189.6
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData fSearch fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bDelete IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bEdit IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bHistory IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bNew IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON bViewFile IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flName IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       flName:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each agentfile.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Agent Files */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Agent Files */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Agent Files */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup C-Win
ON CHOOSE OF bAgentLookup IN FRAME fMain /* agentlookup */
DO:
  define variable cAgentID  as character no-undo.
  define variable cName     as character no-undo.
    
  run dialogagentlookup.w(input flAgentID:input-value,
                          input "",      /* Selected State ID */
                          input true,    /* Allow 'ALL' */
                          output cAgentID,
                          output std-ch, /* Agent state ID */
                          output cName,
                          output std-lo).
   
  if not std-lo or flAgentID:input-value = cAgentID  
   then
    return no-apply.
     
  assign
      flAgentID:screen-value = cAgentID
      flName:screen-value    = cName
      . 
  
  resultsChanged(false).
          
  if flAgentID:input-value <> ""
   then
    run getData in this-procedure.   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEdit C-Win
ON CHOOSE OF bEdit IN FRAME fMain /* Edit */
do:
  run modifyAgentFile in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bHistory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bHistory C-Win
ON CHOOSE OF bHistory IN FRAME fMain /* History */
DO:
  run viewFileHistory in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
do:
  run newAgentFile in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
do:
  run viewFileHistory in this-procedure.  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME fMain /* Search */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGet C-Win
ON CHOOSE OF btGet IN FRAME fMain /* Get */
OR 'RETURN' of flAgentID
DO:
  if not validAgent()
   then
    return no-apply.
        
  run getData in this-procedure.           
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewFile C-Win
ON CHOOSE OF bViewFile IN FRAME fMain /* ViewFile */
DO:
  run viewAgentFile in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID C-Win
ON VALUE-CHANGED OF flAgentID IN FRAME fMain /* Agent ID */
DO:
  resultsChanged(false).
  flName:screen-value = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flFile C-Win
ON VALUE-CHANGED OF flFile IN FRAME fMain /* File */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgClosedFiles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgClosedFiles C-Win
ON VALUE-CHANGED OF tgClosedFiles IN FRAME fMain /* Include Closed Files */
DO:
  resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

on close of this-procedure 
  run disable_UI.

pause 0 before-hide.
subscribe to "closeWindow" anywhere.

subscribe to "RefreshScreensForFileNumModify" anywhere.
subscribe to "CloseScreensForFileNumModify" anywhere.

setStatusMessage("").

/* Setting button images */
bExport      :load-image            ("images/excel.bmp").
bExport      :load-image-insensitive("images/excel-i.bmp").

bnew         :load-image            ("images/add.bmp").
bnew         :load-image-insensitive("images/add-i.bmp").

bHistory     :load-image            ("images/task.bmp").
bHistory     :load-image-insensitive("images/task-i.bmp").

bEdit        :load-image            ("images/update.bmp").
bEdit        :load-image-insensitive("images/update-i.bmp").

bDelete      :load-image            ("images/delete.bmp").
bDelete      :load-image-insensitive("images/delete-i.bmp").

btGet        :load-image            ("images/Completed.bmp").              
btGet        :load-image-insensitive("images/Completed-i.bmp").

bViewFile    :load-image            ("images/open.bmp").
bViewFile    :load-image-insensitive("images/open-i.bmp").

bSearch      :load-image-up         ("images/magnifier.bmp").
bSearch      :load-image-insensitive("images/magnifier-i.bmp").

bAgentLookup :load-image("images/s-lookup.bmp").
bAgentLookup :load-image-insensitive("images/s-lookup-i.bmp").

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.
    
  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.
   
  run windowResized in this-procedure.

  apply 'entry' to flAgentID. 
  
  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseScreensForFileNumModify C-Win 
PROCEDURE CloseScreensForFileNumModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pAgentID as character.
define input parameter pOldFileID as character.
define input parameter pNewFileID as character.

if can-find(first agentfile where agentfile.agentID = pAgentID and agentfile.fileid = pOldFileID) or
   can-find(first agentfile where agentfile.agentID = pAgentID and agentfile.fileid = pNewFileID)
 then
  run closewindow.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 publish "WindowClosed" (input this-procedure).
 APPLY "CLOSE":U TO THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flAgentID flName flFile tgClosedFiles fSearch 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE btGet bSearch flAgentID bAgentLookup flFile tgClosedFiles fSearch 
         brwData RECT-61 RECT-62 RECT-83 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/                                                                                         
  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table agentfile:handle.
  run util/exporttable.p (table-handle std-ha,
                          "agentfile",
                          "for each agentfile ",
                          "agentfileID,agentID,agentname,fileID,fileNumber,stat,stage,addr1,addr2,addr3,addr4,city,countyID,state,zip,notes,closingDate,reportingDate,transactionType,insuredType,liabilityAmt,invoiceAmt,paidAmt,osAmt",
                          "Agentfile ID,Agent ID,Name,File ID,File,Status,Stage,Address1,Address2,Address3,Address4,City,CountyID,State,Zip,Notes,Closing Date,Reporting Date,Transaction Type,Insured Type,Liability Amount,Invoice Amount,Paid Amount,Remaining Amount",
                          std-ch,
                          "AgentFile-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwData.
  empty temp-table agentfile.
  
  define buffer tagentfile for tagentfile.

  do with frame {&frame-name}:
  end.
  
  for each tagentfile :
    /* test if the record contains the search text */
    if fSearch:screen-value <> "" and 
      not ( (tagentfile.fileID      matches "*" + fSearch:screen-value + "*")  or
            (tagentfile.fileNumber  matches "*" + fSearch:screen-value + "*")  or
            (tagentfile.stage       matches "*" + fSearch:screen-value + "*")  or
            (tagentfile.notes       matches "*" + fSearch:screen-value + "*"))
     then next.
   
    create agentfile.
    buffer-copy tagentfile to agentfile.
  end.
  
  open query brwData preselect each agentfile.
  
  run setWidgetState in this-procedure.
 
  setStatusCount(query brwData:num-results).
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  define buffer tagentfile for tagentfile.
  
  do with frame {&frame-name}:
  end.
  
  if tgClosedFiles:checked
   then
    do:
      message "This query could take a few minutes. Continue?" 
          view-as alert-box warning buttons yes-no update std-lo.
      if not std-lo 
       then return.
    end.
    
  close query brwData.
  
  run server/getagentfiles.p (input flAgentID:input-value,
                              input flFile:input-value,
                              input tgClosedFiles:checked,
                              output table tagentfile,
                              output std-lo,
                              output std-ch).

  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box info buttons ok.
      return.
    end.

  run filterData in this-procedure.
  
  /* Set Status count with date and time from the server */
  setStatusRecords(query brwData:num-results).
  
  apply 'value-changed' to browse brwData.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshScreensForFileNumModify C-Win 
PROCEDURE RefreshScreensForFileNumModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter pAgentID as character.
define input parameter pOldFileID as character.
define input parameter pNewFileID as character.

if can-find(first agentfile where agentfile.agentID = pAgentID and agentfile.fileid = pOldFileID) or
   can-find(first agentfile where agentfile.agentID = pAgentID and agentfile.fileid = pNewFileID)
 then
  run getData.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  assign
    /*  bNew:sensitive      = query brwData:num-results > 0
      bEdit:sensitive     = query brwData:num-results > 0 */
      bViewFile:sensitive = query brwData:num-results > 0 
      bHistory:sensitive  = query brwData:num-results > 0
      bExport:sensitive   = query brwData:num-results > 0 
      .  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .
  
  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewAgentFile C-Win 
PROCEDURE viewAgentFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   if not available agentfile 
    then
     return.

   publish "SetCurrentValue" ("FileNumber",  agentfile.fileNumber).
   publish "SetCurrentValue" ("AgentID",  agentfile.agentID).
   publish "OpenWindow" (input "wfile", 
                         input agentfile.fileNumber, 
                         input "wfile.w", 
                         input ?,                                    
                         input this-procedure).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewFileHistory C-Win 
PROCEDURE viewFileHistory :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available agentfile 
   then return.
  
  publish "OpenWindow" (input "wagentfilehistory",            /*childtype*/
                        input string(agentfile.agentfileID),  /*childid*/
                        input "wagentfilehistory.w",          /*window*/
                        input "integer|input|" + string(agentfile.agentfileID) + "^character|input|" + agentfile.fileNumber + "^character|input|" + agentfile.agentID + "^character|input|" + agentfile.agentname,   /*parameters*/                               
                        input this-procedure).                /*currentProcedure handle*/    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame fMain:width-pixels          = {&window-name}:width-pixels
      frame fMain:virtual-width-pixels  = {&window-name}:width-pixels
      frame fMain:height-pixels         = {&window-name}:height-pixels
      frame fMain:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels       = frame fmain:width-pixels - 14.5
      {&browse-name}:height-pixels      = frame fMain:height-pixels - {&browse-name}:y - 2
      .
 
  run ShowScrollBars(browse brwData:handle, no, yes).
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFileStatus C-Win 
FUNCTION getFileStatus RETURNS CHARACTER
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cStat = 'O' 
   then
    return 'Open'. 
   else if cStat = 'C' 
    then
     return 'Closed'.  
   else
    return cStat.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage("Results may not match current parameters.").
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if flAgentID:input-value = ""
   then return false. /* Function return value. */
  
  else if flAgentID:input-value = {&ALL}
   then
    flName:screen-value = {&NotApplicable}.
       
  else if flAgentID:input-value <> {&ALL}
   then
    do:
      
      publish "getAgentName" (input flAgentID:input-value,
                              output std-ch,
                              output std-lo).                                               
      if not std-lo 
       then 
        do:
          assign 
              flAgentID:screen-value = "" 
              flName:screen-value    = ""
              .
          return false. /* Function return value. */
        end.
      flName:screen-value = std-ch.
    end. 
  
  resultsChanged(false).  
  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

