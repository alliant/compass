&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------
@file dialogselectcolumn.w
@description A generic dialog to selecct the columns

@author John Oliver
@created 09.26.2018
@Modified
Date		Name	Description
----		----	-----------
07/28/20	AG	Minor UI layout changes
------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input parameter pWindow as character no-undo.
DEFINE INPUT PARAMETER pAvailableColumns AS CHARACTER no-undo.
DEFINE INPUT-OUTPUT PARAMETER pSelectedColumns AS CHARACTER NO-UNDO.
DEFINE OUTPUT PARAMETER pCancel AS LOGICAL NO-UNDO INITIAL TRUE.

/* Local Variable Definitions ---                                       */
define variable tSelectedColumns as character no-undo.
{lib/std-def.i}

/* Functions ---                                                        */
{lib/add-delimiter.i}
{lib/move-item.i}
{lib/move-item-order.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bDefault tAvailableList tSelectedList ~
bAddItem bMoveUp bRemoveItem bMoveDown bOK bCancel 
&Scoped-Define DISPLAYED-OBJECTS tAvailableList tSelectedList 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getColumnList Dialog-Frame 
FUNCTION getColumnList RETURNS CHARACTER
  ( INPUT pOnlyOne AS LOGICAL)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddItem 
     LABEL ">" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add field to selected list".

DEFINE BUTTON bCancel AUTO-GO 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bDefault 
     LABEL "Restore Default" 
     SIZE 17 BY 1.14.

DEFINE BUTTON bMoveDown 
     LABEL "^" 
     SIZE 4.8 BY 1.14 TOOLTIP "Move the selected field down".

DEFINE BUTTON bMoveUp 
     LABEL "^" 
     SIZE 4.8 BY 1.14 TOOLTIP "Move the selected field up".

DEFINE BUTTON bOK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bRemoveItem 
     LABEL "<" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete field from selected list".

DEFINE VARIABLE tAvailableList AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SORT SCROLLBAR-VERTICAL 
     LIST-ITEM-PAIRS "ALL","ALL" 
     SIZE 24 BY 5.95 NO-UNDO.

DEFINE VARIABLE tSelectedList AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SCROLLBAR-VERTICAL 
     LIST-ITEM-PAIRS "ALL","ALL" 
     SIZE 24 BY 5.95 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bDefault AT ROW 1.48 COL 34 WIDGET-ID 144
     tAvailableList AT ROW 3.81 COL 3 NO-LABEL WIDGET-ID 128
     tSelectedList AT ROW 3.81 COL 34 NO-LABEL WIDGET-ID 132
     bAddItem AT ROW 5.48 COL 28 WIDGET-ID 138
     bMoveUp AT ROW 5.48 COL 59 WIDGET-ID 124
     bRemoveItem AT ROW 6.86 COL 28 WIDGET-ID 140
     bMoveDown AT ROW 6.86 COL 59 WIDGET-ID 126
     bOK AT ROW 10.81 COL 17.2 WIDGET-ID 142
     bCancel AT ROW 10.81 COL 34.2 WIDGET-ID 136
     "Available Columns:" VIEW-AS TEXT
          SIZE 22 BY .62 AT ROW 2.81 COL 3 WIDGET-ID 130
     "Selected Columns:" VIEW-AS TEXT
          SIZE 22 BY .62 AT ROW 2.81 COL 34 WIDGET-ID 134
     SPACE(9.39) SKIP(9.08)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Select Columns" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Select Columns */
DO:
  if tSelectedColumns <> pSelectedColumns
   then {lib/confirm-close.i "Column List"}
  
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddItem
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddItem Dialog-Frame
ON CHOOSE OF bAddItem IN FRAME Dialog-Frame /* > */
DO:
  do with frame {&frame-name}:
    MoveItem(tAvailableList:handle,tSelectedList:handle,",").
  end.
  tSelectedColumns = getColumnList(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel Dialog-Frame
ON CHOOSE OF bCancel IN FRAME Dialog-Frame /* Cancel */
DO:
  if tSelectedColumns <> pSelectedColumns
   then {lib/confirm-close.i "Column List"}
  
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDefault
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDefault Dialog-Frame
ON CHOOSE OF bDefault IN FRAME Dialog-Frame /* Restore Default */
DO:
  define variable cDefault as character no-undo.
  
  publish "GetDynamicBrowseDefault" (pWindow, output cDefault).
  do with frame {&frame-name}:
    MoveItemList(tSelectedList:handle, tAvailableList:handle, getColumnList(true)).
    MoveItemList(tAvailableList:handle, tSelectedList:handle, cDefault).
  end.
   tSelectedColumns = getColumnList(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bMoveDown
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bMoveDown Dialog-Frame
ON CHOOSE OF bMoveDown IN FRAME Dialog-Frame /* ^ */
DO:
  do with frame frmBuild:
    MoveItemOrder(tSelectedList:handle,false,",").
  end.
  tSelectedColumns = getColumnList(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bMoveUp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bMoveUp Dialog-Frame
ON CHOOSE OF bMoveUp IN FRAME Dialog-Frame /* ^ */
DO:
  do with frame frmBuild:
    MoveItemOrder(tSelectedList:handle,true,",").
  end.
  tSelectedColumns = getColumnList(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOK Dialog-Frame
ON CHOOSE OF bOK IN FRAME Dialog-Frame /* Save */
DO:
  if getColumnList(false) = ""
   then
    do:
      message "Please select at least one column" view-as alert-box error buttons ok.
      return no-apply.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRemoveItem
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRemoveItem Dialog-Frame
ON CHOOSE OF bRemoveItem IN FRAME Dialog-Frame /* < */
DO:
  do with frame {&frame-name}:
    MoveItem(tSelectedList:handle,tAvailableList:handle,",").
  end.
  tSelectedColumns = getColumnList(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAvailableList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAvailableList Dialog-Frame
ON DEFAULT-ACTION OF tAvailableList IN FRAME Dialog-Frame
DO:
  APPLY "CHOOSE":U TO bAddItem.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSelectedList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSelectedList Dialog-Frame
ON DEFAULT-ACTION OF tSelectedList IN FRAME Dialog-Frame
DO:
  APPLY "CHOOSE":U TO bRemoveItem.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bMoveUp:load-image-up("images/s-up.bmp").
bMoveUp:load-image-insensitive("images/s-up-i.bmp").
bMoveDown:load-image-up("images/s-down.bmp").
bMoveDown:load-image-insensitive("images/s-down-i.bmp").
bAddItem:load-image-up("images/s-right.bmp").
bAddItem:load-image-insensitive("images/s-right-i.bmp").
bRemoveItem:load-image-up("images/s-left.bmp").
bRemoveItem:load-image-insensitive("images/s-left-i.bmp").

assign
  tAvailableList:list-item-pairs = (if pAvailableColumns > "" then pAvailableColumns else "ALL,ALL")
  tSelectedList:list-item-pairs = (if pSelectedColumns > "" then pSelectedColumns else "ALL,ALL")
  .
tAvailableList:delete("ALL").
tSelectedList:delete("ALL").
tSelectedColumns = getColumnList(false).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  
  pCancel = false.
  pSelectedColumns = getColumnList(true).
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tAvailableList tSelectedList 
      WITH FRAME Dialog-Frame.
  ENABLE bDefault tAvailableList tSelectedList bAddItem bMoveUp bRemoveItem 
         bMoveDown bOK bCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getColumnList Dialog-Frame 
FUNCTION getColumnList RETURNS CHARACTER
  ( INPUT pOnlyOne AS LOGICAL) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  DEFINE VARIABLE pList AS CHARACTER NO-UNDO.

  do with frame {&frame-name}:
    pList = "".
    do std-in = 1 to num-entries(tSelectedList:list-item-pairs) / 2:
      if pOnlyOne
       then pList = addDelimiter(pList,",") + entry(std-in * 2,tSelectedList:list-item-pairs).
       else pList = addDelimiter(pList,",") + entry(std-in * 2 - 1,tSelectedList:list-item-pairs) + "," + entry(std-in * 2,tSelectedList:list-item-pairs).
    end.
  end.
  RETURN pList.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

