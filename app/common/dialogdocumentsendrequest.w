&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

def input param pAction as char.  /* 'Share', 'Send' or 'Request' */
def input param pObjType as char. /* 'Folder' or 'File' */
def input param pObjName as char. /* Name of folder or file(s) */
def output param pTo as char.
def output param pSubject as char.
def output param pBody as char.
def output param pCCSender as log.
def output param pNotify as log.  /* Notify by email on download or upload */
def output param pSave as logical init false.

{lib/std-def.i}
{lib/validEmailAddr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tHeader tTo tSubject tBody tCCSender tNotify ~
Btn_OK Btn_Cancel tBodyLabel 
&Scoped-Define DISPLAYED-OBJECTS tHeader tTo tSubject tBody tCCSender ~
tNotify tBodyLabel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Send/Request" 
     SIZE 21 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tBody AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 100 BY 6 NO-UNDO.

DEFINE VARIABLE tHeader AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 109 BY 6 NO-UNDO.

DEFINE VARIABLE tTo AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 100 BY 3 TOOLTIP "Enter a comma-separated list of email addresses" NO-UNDO.

DEFINE VARIABLE tBodyLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Message:" 
      VIEW-AS TEXT 
     SIZE 9 BY .62 NO-UNDO.

DEFINE VARIABLE tSubject AS CHARACTER FORMAT "X(256)":U 
     LABEL "Subject" 
     VIEW-AS FILL-IN 
     SIZE 100 BY 1 NO-UNDO.

DEFINE VARIABLE tCCSender AS LOGICAL INITIAL no 
     LABEL "Send me a copy of the email" 
     VIEW-AS TOGGLE-BOX
     SIZE 67 BY .81 NO-UNDO.

DEFINE VARIABLE tNotify AS LOGICAL INITIAL yes 
     LABEL "Email me when item(s) have been downloaded/uploaded" 
     VIEW-AS TOGGLE-BOX
     SIZE 67 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     tHeader AT ROW 1.95 COL 5 NO-LABEL WIDGET-ID 22 NO-TAB-STOP 
     tTo AT ROW 8.38 COL 14 NO-LABEL WIDGET-ID 8
     tSubject AT ROW 11.71 COL 12 COLON-ALIGNED WIDGET-ID 12
     tBody AT ROW 13.14 COL 14 NO-LABEL WIDGET-ID 14
     tCCSender AT ROW 19.62 COL 14 WIDGET-ID 16
     tNotify AT ROW 20.81 COL 14 WIDGET-ID 18
     Btn_OK AT ROW 22.91 COL 40
     Btn_Cancel AT ROW 22.91 COL 63.2
     tBodyLabel AT ROW 13.14 COL 2.2 COLON-ALIGNED NO-LABEL WIDGET-ID 24
     "To:" VIEW-AS TEXT
          SIZE 3 BY .62 AT ROW 8.38 COL 10 WIDGET-ID 10
     SPACE(104.79) SKIP(15.75)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Share/Send/Request Folder/File"
         DEFAULT-BUTTON Btn_Cancel CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       tHeader:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Share/Send/Request Folder/File */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Send/Request */
DO:
  if tTo:screen-value = "" or tTo:screen-value = ? then
  do:
    message "You must enter at least one email address."
      view-as alert-box.
    return no-apply.
  end.
  
  if pAction <> "Share" 
  and (tSubject:screen-value = "" or tSubject:screen-value = ?) then
  do:
    message "Subject cannot be blank."
      view-as alert-box.
    return no-apply.
  end.

  assign tTo tSubject tBody tCCSender tNotify.

  /* Validate structure of email addresses */
  std-ch = "".
  do std-in = 1 to num-entries(tTo):
    if not validEmailAddr(entry(std-in, tTo)) then
    do:
      message "Email address '" + entry(std-in, tTo) + "' is not valid."
        view-as alert-box.
      return no-apply.
    end.
    std-ch = std-ch + trim(entry(std-in, tTo)) + ",".
  end.
  tTo = trim(std-ch,",").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
  ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  if lookup(pAction,"Share,Send,Request") = 0 then
  do:
    message "Invalid action '" + pAction + "' passed."
      view-as alert-box error.
    return.
  end.

  if lookup(pObjType,"Folder,File") = 0 then
  do:
    message "Invalid object type '" + pObjType + "' passed."
      view-as alert-box error.
    return.
  end.

  RUN enable_UI.

  frame {&frame-name}:title = pAction + " " + pObjType + "(s)".
  Btn_OK:label = pAction + " " + pObjType + "(s)".
  
  if pAction = "Share" or pAction = "Send" then
  tHeader:screen-value = pAction + " " + pObjType + "(s): " + pObjName.
  else
  tHeader:screen-value = pAction + " " + pObjType + "(s) to folder '" + pObjName + "'".
  
  if pAction = "Share" or pAction = "Send" then
  do:
    if num-entries(pObjName, chr(10)) > 2 then
    tSubject:screen-value = pAction + " " + pObjType + "(s): <Mulitple Files>".
    else
    tSubject:screen-value = pAction + " " + pObjType + ": " + pObjName.
  end.
  else
  tSubject:screen-value = pAction + " " + pObjType + "(s) to folder '" + pObjName + "'".

  if pAction = "Share" then
  tNotify:label = "Email me when item(s) have been shared".
  else
  if pAction = "Send" then
  tNotify:label = "Email me when item(s) have been downloaded".
  else
  tNotify:label = "Email me when item(s) have been uploaded".
  
  if pAction = "Share" then
  do:
    tSubject:hidden = yes.
    tBodyLabel:hidden = yes.
    tBody:hidden = yes.
    tCCSender:hidden = yes.
    tNotify:hidden = yes.
  end.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  
  assign  
    pTo = tTo
    pSubject = tSubject
    pBody = tBody
    pCCSender = tCCSender
    pNotify = tNotify
    pSave = true.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tHeader tTo tSubject tBody tCCSender tNotify tBodyLabel 
      WITH FRAME Dialog-Frame.
  ENABLE tHeader tTo tSubject tBody tCCSender tNotify Btn_OK Btn_Cancel 
         tBodyLabel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

