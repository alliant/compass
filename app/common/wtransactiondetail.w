&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wtransactiondetail.w

  Description: Window for showing child records of a posted transaction record

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Anjly

  Created: 08.09.2019
  Modified: 
  Date      Name Comments
  07/02/21  SB   Modified to add write-off functionality
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */

define input parameter ipiArTranID as integer.
define input parameter ipiTranID   as integer.
define input parameter ipcType     as character.

{lib/std-def.i}  
{lib/ar-def.i}
{lib/winlaunch.i} 
{lib/winshowscrollbars.i}
{lib/ar-getsourcetype.i} /* Include funcion: getSourceType */

/* Temp-table Definition */
{tt/artran.i}
{tt/artran.i &tablealias=tartran}
{tt/artran.i &tablealias=ttartran}
{tt/arpmt.i &tablealias=ttarpmt}
{tt/armisc.i &tablealias=ttarMisc}

define variable cAppCode as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwArtran

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES artran

/* Definitions for BROWSE brwArtran                                     */
&Scoped-define FIELDS-IN-QUERY-brwArtran artran.trandate artran.tranID artran.filenumber artran.reference getSourcetype(artran.sourceType) @ artran.sourceType artran.sourceID getTransType(artran.seq,artran.type) @ artran.type artran.revenuetype artran.tranamt artran.accumbalance artran.duedate artran.notes   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwArtran   
&Scoped-define SELF-NAME brwArtran
&Scoped-define QUERY-STRING-brwArtran for each artran
&Scoped-define OPEN-QUERY-brwArtran open query {&SELF-NAME} for each artran.
&Scoped-define TABLES-IN-QUERY-brwArtran artran
&Scoped-define FIRST-TABLE-IN-QUERY-brwArtran artran


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwArtran}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwArtran bExport 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getTransType C-Win 
FUNCTION getTransType RETURNS CHARACTER
  ( iSeq as integer,
    cTranType as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport AUTO-GO  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export".

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwArtran FOR 
      artran SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwArtran
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwArtran C-Win _FREEFORM
  QUERY brwArtran DISPLAY
      artran.trandate                         label  "Tran Date"     format "99/99/99" width 11       
artran.tranID                           label  "Invoice ID"                format "x(15)"     
artran.filenumber                       label  "File Number"               format "x(30)" width 15 
artran.reference                        column-label  "Check/!Reference"   format "x(30)" width 15
getSourcetype(artran.sourceType)     @ artran.sourceType label "Source"    format "x(25)" width 12 
artran.sourceID                         label  "Source ID"                 format "x(30)" width 12
getTransType(artran.seq,artran.type) @ artran.type       label  "Type"     format "x(10)" 
artran.revenuetype                      label  "Revenue"                   format "x(14)"              
artran.tranamt                          label  "Transaction"               format "->>>,>>>,>>9.99"
artran.accumbalance                     label  "Balance"                   format "->>>,>>>,>>9.99"
artran.duedate                          label  "Due Date"                  format "99/99/99 "            
artran.notes                            label  "Notes"                     format "x(200)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 178 BY 14.52 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     brwArtran AT ROW 1.43 COL 7 WIDGET-ID 200
     bExport AT ROW 1.43 COL 2.2 WIDGET-ID 404 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 235 BY 24.67 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Transaction Details"
         HEIGHT             = 15.05
         WIDTH              = 186
         MAX-HEIGHT         = 33.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwArtran 1 DEFAULT-FRAME */
ASSIGN 
       brwArtran:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwArtran:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwArtran
/* Query rebuild information for BROWSE brwArtran
     _START_FREEFORM
open query {&SELF-NAME} for each artran.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwArtran */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Transaction Details */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Transaction Details */
DO:  
  /* This event will close the window and terminate the procedure.  */ 
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Transaction Details */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwArtran
&Scoped-define SELF-NAME brwArtran
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArtran C-Win
ON DEFAULT-ACTION OF brwArtran IN FRAME DEFAULT-FRAME
DO:
  if available arTran
   then
    do:
      if arTran.type = "P" or
         arTran.type = {&Apply}
       then
        do:
          find first ttartran where ttartran.type = {&Payment} and
                                    ttartran.seq   = 0 no-error.
          
          if not available ttartran
           then 
            return no-apply.
            
          run server\getpayment.p (input ttartran.arTranID,
                                   input 0,  /* ArpmtID */
                                   output table ttarpmt,
                                   output std-lo,
                                   output std-ch).
          
          if not std-lo
           then
            do:
              message std-ch 
                  view-as alert-box error buttons ok.
              return no-apply.  
            end.
        
          run dialogmodifypayment.w (input {&View},
                                     input-output table ttarpmt,
                                     output std-lo).
                                                  
        end.
        
      if arTran.SourceType = "C"
       then
        do:
          run server\gettransactions.p (input int(arTran.SourceID), /* ArtranID */
                                        input "",                   /* Entity */
                                        input "",                   /* Entity ID*/                                        
                                        input false,                /* Include Files */
                                        input false,                /* Include Fully Paid Files */
                                        input false,                /* Include Invoices  */
                                        input false,                /* Include Void Invoices  */
                                        input false,                /* Include Fully Paid Invoices */                                        
                                        input false,                /* Include Credits */
                                        input false,                /* Include Void Credits */
                                        input false,                /* Include Fully Apply Credit */                                        
                                        input false,                /* Include Payments */
                                        input false,                /* Include Void Payments */                                        
                                        input false,                /* Include Fully Apply Payment */                                        
                                        input false,                /* IncludeRefund */
                                        input false,                /* IncludeVoidRefund */
                                        input ?,                    /* From Date */
                                        input ?,                    /* To Date */
                                        output table tartran,                                        
                                        output std-lo,
                                        output std-ch).
          
          if not std-lo
           then
            do:
              message std-ch 
                  view-as alert-box error buttons ok.
              return no-apply.  
            end.
            
          for each tartran:
            create ttarMisc.
            buffer-copy tartran to ttarMisc.
            ttarMisc.entityID  = tartran.entityID. 
            ttarMisc.agentName = tartran.entityName.
          end.
          
          run dialoginvoice.w (input-output table ttarMisc,
                               input {&Credit},
                               input {&View}, /* View */
                               output std-lo).
                                                  
        end.               
        
      if arTran.SourceType = "B"
       then
        do: 
           run batchdetail in this-procedure.
        end.                                            
    end.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArtran C-Win
ON ROW-DISPLAY OF brwArtran IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}  
    
  assign
      artran.tranamt :fgcolor in browse brwArtran      = if artran.tranamt      < 0 then 12 else 0
      artran.accumbalance :fgcolor in browse brwArtran = if artran.accumbalance < 0 then 12 else 0
      .    
  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.


/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow" anywhere.
subscribe to "RefreshScreensForFileNumModify" anywhere.
subscribe to "CloseScreensForFileNumModify" anywhere.

setStatusMessage("").

/* Get Application Code */
publish "GetAppCode" (output cAppCode).

/* Fetch data before window is visible */
run getData in this-procedure.
bExport    :load-image             ("images/s-excel.bmp").
bExport    :load-image-insensitive ("images/s-excel-i.bmp").
   
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
  run enable_UI.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE batchdetail C-Win 
PROCEDURE batchdetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
   publish "SetCurrentValue" ("AppName",  cAppCode).
   
   publish "OpenWindow" (input "wBatchDetail", 
                         input arTran.SourceID, 
                         input "wbatchdetail.w", 
                         input "character|input|" + arTran.SourceID + "^character|input|" + arTran.fileNumber + "^character|input|" + arTran.entityid,                                   
                         input this-procedure). 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseScreensForFileNumModify C-Win 
PROCEDURE CloseScreensForFileNumModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pAgentID as character.
 define input parameter pOldFileID as character.
 define input parameter pNewFileID as character.

 if can-find(first artran where artran.entityID = pAgentID and artran.fileid = pOldFileID) or
    can-find(first artran where artran.entityID = pAgentID and artran.fileid = pNewFileID) 
  then
   run closeWindow.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE brwArtran bExport 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwARTran:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
    
  empty temp-table tARTran.
  
  for each artran:
    create tARTran.
    buffer-copy artran to tARTran.
    assign
        tARTran.sourceType = getSourcetype(artran.sourceType)
        tARTran.type       = getTransType(artran.seq,artran.type)
        .
  end.
  
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table tARTran:handle.
  run util/exporttable.p (table-handle std-ha,
                          "tARTran",
                          "for each tARTran by artranID by seq",
                          "trandate,tranID,fileNumber,reference,sourceType,sourceID,type,revenuetype,tranamt,accumbalance,duedate,notes",
                          "Tran Date,Invoice ID,File Number,Check/Reference,Source,Source ID,Type,Revenue,Transaction,Balance,Due Date,Notes" ,
                          std-ch,
                          "Transaction Details-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable deBalance as decimal no-undo.
  
  define buffer ttartran for ttartran.
  
  empty temp-table ttartran.
  empty temp-table artran.
  
  run server\gettransaction.p (input ipiArTranID,
                               input ipiTranID,
                               input ipcType,
                               output table ttartran,
                               output std-lo,
                               output std-ch).
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
    
  for each ttartran by ttartran.artranID by ttartran.seq:
    create artran.
    buffer-copy ttartran to artran.
    artran.accumbalance = artran.tranamt + deBalance.
    deBalance = artran.accumbalance.
  end.
  
  apply 'value-changed' to browse brwArtran.
  
  open query brwArtran preselect each artran.
  
  bExport:sensitive = query brwArtran:num-results > 0 .
  
   /* Set Status count with date and time from the server */
  setStatusRecords(query brwArtran:num-results).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshScreensForFileNumModify C-Win 
PROCEDURE RefreshScreensForFileNumModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pAgentID as character.
 define input parameter pOldFileID as character.
 define input parameter pNewFileID as character.
 
 if can-find(first artran where artran.entityID = pAgentID and artran.fileid = pOldFileID) or
    can-find(first artran where artran.entityID = pAgentID and artran.fileid = pNewFileID)
  then
   run getData.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 42
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 3
      .
   
  run ShowScrollBars(frame {&frame-name}:handle, no, no).  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getTransType C-Win 
FUNCTION getTransType RETURNS CHARACTER
  ( iSeq as integer,
    cTranType as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cTranType = 'I' 
   then
    return 'Invoice'. 
  else if cTranType = 'D' 
   then
    return 'Debit'.
  else if cTranType = 'C' 
   then
    return 'Credit'.  
  else if cTranType = 'F' 
   then
    return 'File'.  
  else if cTranType = 'A' 
   then
    return 'Apply'.  
  else if cTranType = 'R' 
   then
    return 'Reprocess'. 
  else if cTranType = 'M' 
   then
    return 'Miscellaneous'.
  else if cTranType = 'W' 
   then
    return 'Write-off'.
  else if (iSeq = 0 and cTranType = 'P') 
   then
    return 'Payment'.    
  else if (iSeq > 0 and cTranType = 'P')
   then
    return 'Refund'.
  else
    return cTranType.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

