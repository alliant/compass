&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
CREATE WIDGET-POOL.

{tt/batchform.i}
{tt/batchform.i &tableAlias="ttbatchform"}
{tt/openwindow.i}
{lib/std-def.i}
{lib/ar-def.i}

define input parameter ipiBatchID     as integer no-undo.
define input parameter ipcFileNumber  as character no-undo.
define input parameter ipcAgentID     as character no-undo.

{lib/winlaunch.i}
define variable hpopupMenu            as handle    no-undo.
define variable hpopupMenuItemFile    as handle    no-undo.
define variable hpopupMenuItemPolicy  as handle    no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES batchform

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData batchform.seq batchform.fileNumber batchform.zipcode batchform.policyID batchform.formType batchForm.revenueType batchform.formID batchform.formCount batchform.statCode batchform.effDate batchform.liabilityAmount batchform.grossPremium batchform.netPremium batchform.retentionPremium batchform.grossDelta batchform.netDelta batchform.retentionDelta batchform.reprocess batchform.validform batchform.validMsg   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH batchform
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH batchform.
&Scoped-define TABLES-IN-QUERY-brwData batchform
&Scoped-define FIRST-TABLE-IN-QUERY-brwData batchform


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwData bExport bRefresh fAgent bViewAgent ~
fBatch fAgentName cbFileNumber rdView RECT-2 RECT-3 RECT-4 
&Scoped-Define DISPLAYED-OBJECTS fAgent fBatch fAgentName cbFileNumber ~
rdView 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh data".

DEFINE BUTTON bViewAgent  NO-FOCUS
     LABEL "A" 
     SIZE 4.8 BY 1.14 TOOLTIP "View agent".

DEFINE VARIABLE cbFileNumber AS CHARACTER 
     LABEL "File Number" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "ALL" 
     DROP-DOWN
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE fAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fAgentName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 47.6 BY 1 NO-UNDO.

DEFINE VARIABLE fBatch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Batch" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE rdView AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "File", "f",
"Policy", "p"
     SIZE 10 BY 1.67 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 42.2 BY 2.91.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 45.4 BY 2.91.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 60 BY 2.91.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      batchform SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      batchform.seq        column-label "Seq"                  format "zzzz9"
batchform.fileNumber       column-label "File"                 format "x(30)" width 19
batchform.zipcode          column-label "Zip Code"             format "x(11)"
batchform.policyID         column-label "Policy"               format "zzzzzzzzz"
batchform.formType         column-label "Type"
batchForm.revenueType      column-label "Revenue!Type" format "x(12)"
batchform.formID           column-label "Form"                 format "x(10)"
batchform.formCount        column-label "Form!Count"
batchform.statCode         column-label "STAT!Code"            format "x(10)"
batchform.effDate          column-label "Effective!Date"       format "99/99/9999"
batchform.liabilityAmount  column-label "Liability"            format "-zzz,zzz,zz9.99"
batchform.grossPremium     column-label "Processed!Gross"      format "-z,zzz,zz9.99"
batchform.netPremium       column-label "Processed!Net"        format "-z,zzz,zz9.99"
batchform.retentionPremium column-label "Processed!Retention"  format "-z,zzz,zz9.99"
batchform.grossDelta       column-label "Accounting!Gross"     format "-z,zzz,zz9.99"
batchform.netDelta         column-label "Accounting!Net"       format "-z,zzz,zz9.99"
batchform.retentionDelta   column-label "Accounting!Retention" format "-z,zzz,zz9.99"
batchform.reprocess        column-label "Redo"                 format "Yes/No" width 7    
batchform.validform        column-label "Verified"             format "x(9)"
batchform.validMsg         column-label "Messages"             format "x(100)" width 30
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 239.8 BY 16.24 ROW-HEIGHT-CHARS .86 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwData AT ROW 4.86 COL 2.2 WIDGET-ID 200
     bExport AT ROW 2.14 COL 113.4 WIDGET-ID 406 NO-TAB-STOP 
     bRefresh AT ROW 2.14 COL 106.4 WIDGET-ID 404 NO-TAB-STOP 
     fAgent AT ROW 1.95 COL 9.4 COLON-ALIGNED WIDGET-ID 426
     bViewAgent AT ROW 1.86 COL 25.6 WIDGET-ID 326
     fBatch AT ROW 1.95 COL 43 COLON-ALIGNED WIDGET-ID 424
     fAgentName AT ROW 3.1 COL 9.4 COLON-ALIGNED WIDGET-ID 432
     cbFileNumber AT ROW 2.48 COL 74.8 COLON-ALIGNED WIDGET-ID 2
     rdView AT ROW 2.1 COL 136.8 NO-LABEL WIDGET-ID 408
     "Actions" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 1.24 COL 104.8 WIDGET-ID 416
     "Filters" VIEW-AS TEXT
          SIZE 5.4 BY .62 AT ROW 1.29 COL 63 WIDGET-ID 418
     "Default View:" VIEW-AS TEXT
          SIZE 13 BY .62 AT ROW 2.14 COL 123.2 WIDGET-ID 434
     RECT-2 AT ROW 1.57 COL 61.8 WIDGET-ID 414
     RECT-3 AT ROW 1.57 COL 103.6 WIDGET-ID 420
     RECT-4 AT ROW 1.57 COL 2.2 WIDGET-ID 430
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 242.2 BY 20.14 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Batch Details"
         HEIGHT             = 20.14
         WIDTH              = 242.2
         MAX-HEIGHT         = 47.86
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 47.86
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

ASSIGN 
       fAgent:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       fAgentName:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       fBatch:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH batchform.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Batch Details */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Batch Details */
DO: 
  run closeWindow in this-procedure.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Batch Details */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
do:
  run getData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  apply 'value-changed' to browse brwData.
  
  if rdView:screen-value = "f" 
   then
    do:
       publish "SetCurrentValue" ("FileNumber",  batchform.fileNumber).
       publish "SetCurrentValue" ("AgentID",  ipcAgentID).
       publish "OpenWindow" (input "wfile", 
                             input batchform.fileNumber, 
                             input "wfile.w", 
                             input ?,                                    
                             input this-procedure).  
   
    end.
   else if rdView:screen-value = "p" 
    then
     do:
        if batchform.policyID ne 0 
         then
          do:
             publish "SetCurrentValue" ("PolicyID",  batchform.policyID).
             publish "OpenWindow" (input "wpolicy", 
                                   input batchform.policyID, 
                                   input "wpolicy.w", 
                                   input ?,                                   
                                   input this-procedure).    
          end.
     end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
    run createPopupMenu.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewAgent C-Win
ON CHOOSE OF bViewAgent IN FRAME fMain /* A */
DO:
 run openagentsummary. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbFileNumber
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbFileNumber C-Win
ON VALUE-CHANGED OF cbFileNumber IN FRAME fMain /* File Number */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
       
subscribe to "GetAutoView"                    anywhere.
subscribe to "RefreshScreensForFileNumModify" anywhere.
subscribe to "CloseScreensForFileNumModify"   anywhere.

setStatusMessage("").

bExport:load-image               ("images/excel.bmp").
bExport:load-image-insensitive   ("images/excel-i.bmp").
bRefresh:load-image              ("images/Refresh.bmp").
bRefresh:load-image-insensitive  ("images/Refresh-i.bmp").
bViewAgent:load-image            ("images/s-lookup.bmp").
bViewAgent:load-image-insensitive("images/s-lookup-i.bmp").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

if ipiBatchID > 0 
 then
  run getData in this-procedure. 

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  RUN enable_UI.
  
  /* Hide Agent button when screen launches from AMD */
  publish "GetCurrentValue" ("AppName", output std-ch).
  
  bViewAgent:hidden = not (std-ch = "ARM" or std-ch = "OPS").
    
  fBatch:screen-value = string(ipiBatchID). 
  fAgent:screen-value = string(ipcAgentID).
  publish "getAgentName" (input  ipcAgentID,
                          output std-ch,
                          output std-lo).
                          
  fAgentName:screen-value =  std-ch.    
  
  if ipiBatchID > 0 
   then
    do:    
      cbFileNumber :screen-value = if ipcFileNumber > "" then ipcFileNumber else {&ALL} no-error. 
      run filterData in this-procedure. 
      /* Display no. of records with date and time on status bar */
      setStatusRecords(query brwData:num-results).
    end.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseScreensForFileNumModify C-Win 
PROCEDURE CloseScreensForFileNumModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID   as character.
  define input parameter pOldFileID as character.
  define input parameter pNewFileID as character.
  
  do with frame {&frame-name}:
  end.
 
  if valid-handle(this-procedure) and
     fAgent:screen-value = pAgentID and
    (can-find(first ttbatchform where ttbatchform.fileid = pOldFileID) or
     can-find(first ttbatchform where ttbatchform.fileid = pNewFileID) )
  then
   run closeWindow.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
publish "WindowClosed" (input this-procedure).
apply "CLOSE":U to this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE createPopupMenu C-Win 
PROCEDURE createPopupMenu :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available batchform
   then return.
     
  if valid-handle(hpopupMenuItemFile) 
   then
    delete object hpopupMenuItemFile.

  if valid-handle(hpopupMenuItemPolicy) 
   then
    delete object hpopupMenuItemPolicy.  
      
  if valid-handle(hpopupMenu) 
   then
    delete object hpopupMenu.
              
  create menu hpopupMenu.
  assign
      hpopupMenu:popup-only = true
      hpopupMenu:title      = "Browser menu"
      .

  if batchform.formtype = "p" or
     batchform.formtype = "E"
   then
    do:
      create menu-item hpopupMenuItemFile
      assign
          parent = hpopupMenu
          label  = "File"
          name   = "File"
                  
       triggers:
         on choose persistent run openFileDetail in this-procedure. 
       end triggers.
      create menu-item hpopupMenuItemPolicy
       assign 
           label  = "Policy"
           name   = "Policy"
           parent = hpopupMenu
       triggers:
         on choose persistent run openPolicyDetail in this-procedure. 
       end triggers.
          
       self:popup-menu = hpopupMenu.
    end.
   else if (batchform.formtype = "C" or
            batchform.formtype = "T")
    then
     do:
      create menu-item hpopupMenuItemFile
      assign
          parent = hpopupMenu
          label  = "File"
          name   = "File"
                  
       triggers:
         on choose persistent run openFileDetail in this-procedure. 
       end triggers.
    
       self:popup-menu = hpopupMenu.
     end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fAgent fBatch fAgentName cbFileNumber rdView 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwData bExport bRefresh fAgent bViewAgent fBatch fAgentName 
         cbFileNumber rdView RECT-2 RECT-3 RECT-4 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
    
  publish "GetReportDir" (output std-ch).

  std-ha = temp-table batchform:handle.
  run util/exporttable.p (table-handle std-ha,
                          "batchform",
                          "for each batchform",
                          "seq,fileNumber,zipcode,policyID,formType,revenueType,formID,formCount,statCode,effDate,liabilityAmount,grossPremium,netPremium,retentionPremium,grossDelta,netDelta,retentionDelta,reprocess,validform,validMsg",
                          "Seq,File ID,Zip Code,Policy ID,Type,Revenue Type,Form,Form Count,STAT Code,Effective Date,Liability,Processed Gross,Processed Net,Processed Retention,Accounting Gross,Accounting Net,Accounting Retention,Redo,Verified,Messages",
                          std-ch,
                          "Batchform-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
                                                       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end. 
      
  empty temp-table batchform.
  close query brwData.
 
  /* Set the filters based on the data returned, with ALL as the first option */
  if cbFileNumber:input-value = {&ALL}
   then
    for each ttbatchform:
      create batchform.
      buffer-copy ttbatchform to batchform.         
    end.                             
  else  
    for each ttbatchform 
      where ttbatchform.fileNumber begins cbFileNumber:input-value:      
      create batchform.
      buffer-copy ttbatchform to batchform.         
    end.                             
  
  open query brwData preselect each batchform. 
  
  std-lo = can-find(first batchForm where formCount > 1). 
  std-ha = brwData:first-column. 
  
  BLK:
  do while valid-handle(std-ha):                   
    if std-ha:name = "FormCount" 
     then 
      do:
        if std-lo then view std-ha.                         
        else hide std-ha.                        
            
        leave BLK.
      end.                  
    std-ha = std-ha:next-column.                   
  end. 
    
  /* Display no. of records on status bar */
  setStatusCount(query brwData:num-results).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAutoView C-Win 
PROCEDURE GetAutoView :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define output parameter pView as logical.
 pView = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define buffer ttbatchform for ttbatchform.
 
 do with frame {&frame-name}:
 end.
 
 empty temp-table ttbatchform.
 
 /* If "getBatchforms" is not subscribed in datasrv then 
    server call is made to get all batchforms of a batch */
 publish "GetBatchForms" (input ipiBatchID,
                          output table ttbatchform).
 
 if not can-find(first ttbatchform)
  then
   do:
     run server/getbatchforms.p (input  ipiBatchID,
                                 input  0, /* sequence - zero for all */
                                 output table ttbatchform,
                                 output std-lo,
                                 output std-ch).

     if not std-lo
      then message std-ch view-as alert-box error buttons ok.
   end.
                              
 /* Set the filters based on the data returned, with ALL as the first option */
 for each ttbatchform 
   break by ttbatchform.FileNumber:
   if first-of(ttbatchform.FileNumber)
    then 
     cbfileNumber:add-last(ttbatchform.FileNumber).         
 end.                              
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openagentsummary C-Win 
PROCEDURE openagentsummary :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo.
  define variable hFileDataSrv as handle no-undo.
  define variable cAgentWindow as character no-undo.
  define buffer openwindow for openwindow.
  
  define variable pAgentID as character no-undo.
  define variable pType    as character no-undo.
  define variable pFile    as character no-undo.
  
  do with frame {&frame-name}:
  
  end.
  
  pAgentID = fAgent:screen-value.
  pType = "dashboard".
  pFile = "dialogagentsummary.w".
  
  
  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  
  assign
    hWindow = ?
    hFileDataSrv = ?
    cAgentWindow = pAgentID + " " + pType
    .
    
  publish "OpenAgent" (pAgentID, output hFileDataSrv).
  
  for first openwindow no-lock
      where openwindow.procFile = cAgentWindow:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow)
   then
    do:
      run value(pFile) persistent set hWindow (hFileDataSrv).
      create openwindow.
      assign
        openwindow.procFile = cAgentWindow
        openwindow.procHandle = hWindow
        .
    end.

  run ShowWindow in hWindow no-error.

  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openFileDetail C-Win 
PROCEDURE openFileDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if not available (batchform) 
  then
   return.
   
 publish "SetCurrentValue" ("FileNumber",  batchform.fileNumber).
 publish "SetCurrentValue" ("AgentID",  ipcAgentID).
 publish "OpenWindow" (input "wfile", 
                       input batchform.fileNumber, 
                       input "wfile.w", 
                       input ?,                                    
                       input this-procedure). 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openPolicyDetail C-Win 
PROCEDURE openPolicyDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if not available (batchform) 
  then
   return.
  if batchform.policyID ne 0 
    then
     do:
        publish "SetCurrentValue" ("PolicyID",  batchform.policyID).
        publish "OpenWindow" (input "wpolicy", 
                              input batchform.policyID, 
                              input "wpolicy.w", 
                              input ?,                                   
                              input this-procedure).    
     end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshScreensForFileNumModify C-Win 
PROCEDURE RefreshScreensForFileNumModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pAgentID as character.
 define input parameter pOldFileID as character.
 define input parameter pNewFileID as character.
 do with frame {&frame-name}:
 end.   
 if fAgent:screen-value = pAgentID and
    (can-find(first ttbatchform where ttbatchform.fileid = pOldFileID) or
    can-find(first ttbatchform where ttbatchform.fileid = pNewFileID) )
  then
   do:
      ipcFileNumber = pNewFileID. 
      run getdata.
   end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 assign
     frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
     frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
     frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
     frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
     /* {&frame-name} components */
     {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 10
     {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 5
     .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

