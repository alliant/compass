&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Temp Table Definitions ---                                           */
{tt/sysqueue.i}
{tt/ini.i &tableAlias="paramitem"}
{tt/ini.i &tableAlias="destitem"}

/* Parameters Definitions ---                                           */
define input parameter table for sysqueue.

/* Local Variable Definitions ---                                       */
define variable tItem as character no-undo.
{lib/std-def.i}
{lib/brw-multi-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fDialog
&Scoped-define BROWSE-NAME brwDests

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES destitem paramitem

/* Definitions for BROWSE brwDests                                      */
&Scoped-define FIELDS-IN-QUERY-brwDests destitem.id destitem.val   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDests   
&Scoped-define SELF-NAME brwDests
&Scoped-define QUERY-STRING-brwDests for each destitem
&Scoped-define OPEN-QUERY-brwDests open query {&SELF-NAME} for each destitem.
&Scoped-define TABLES-IN-QUERY-brwDests destitem
&Scoped-define FIRST-TABLE-IN-QUERY-brwDests destitem


/* Definitions for BROWSE brwParams                                     */
&Scoped-define FIELDS-IN-QUERY-brwParams paramitem.id paramitem.val   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwParams   
&Scoped-define SELF-NAME brwParams
&Scoped-define QUERY-STRING-brwParams for each paramitem
&Scoped-define OPEN-QUERY-brwParams open query {&SELF-NAME} for each paramitem.
&Scoped-define TABLES-IN-QUERY-brwParams paramitem
&Scoped-define FIRST-TABLE-IN-QUERY-brwParams paramitem


/* Definitions for FRAME fDialog                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fDialog ~
    ~{&OPEN-QUERY-brwDests}~
    ~{&OPEN-QUERY-brwParams}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-79 tQueue tStatus tFault tAction ~
brwParams brwDests 
&Scoped-Define DISPLAYED-OBJECTS tQueue tStatus tFault tAction 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE VARIABLE tFault AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 43 BY 2.14 NO-UNDO.

DEFINE VARIABLE tAction AS CHARACTER FORMAT "X(256)":U 
     LABEL "Action" 
     VIEW-AS FILL-IN 
     SIZE 33 BY 1 NO-UNDO.

DEFINE VARIABLE tQueue AS INTEGER FORMAT ">>>>9":U INITIAL 0 
     LABEL "Queue" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE tStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-79
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 97 BY 3.57.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwDests FOR 
      destitem SCROLLING.

DEFINE QUERY brwParams FOR 
      paramitem SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwDests
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDests C-Win _FREEFORM
  QUERY brwDests DISPLAY
      destitem.id width 10
destitem.val
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 48 BY 7.86
         TITLE "Destinations" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwParams
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwParams C-Win _FREEFORM
  QUERY brwParams DISPLAY
      paramitem.id width 10
paramitem.val
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 48 BY 7.86
         TITLE "Parameters" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fDialog
     tQueue AT ROW 2.19 COL 10 COLON-ALIGNED WIDGET-ID 8
     tStatus AT ROW 2.19 COL 27 COLON-ALIGNED WIDGET-ID 10
     tFault AT ROW 2.19 COL 52 NO-LABEL WIDGET-ID 12
     tAction AT ROW 3.38 COL 10 COLON-ALIGNED WIDGET-ID 6
     brwParams AT ROW 5.29 COL 2 WIDGET-ID 200
     brwDests AT ROW 5.29 COL 51 WIDGET-ID 300
     "Fault:" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 2.38 COL 46.4 WIDGET-ID 14
     "Queue Item" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 4
     RECT-79 AT ROW 1.48 COL 2 WIDGET-ID 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 98.8 BY 12.43 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "View System Queue"
         HEIGHT             = 12.43
         WIDTH              = 98.8
         MAX-HEIGHT         = 22.86
         MAX-WIDTH          = 135.4
         VIRTUAL-HEIGHT     = 22.86
         VIRTUAL-WIDTH      = 135.4
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fDialog
   FRAME-NAME                                                           */
/* BROWSE-TAB brwParams tAction fDialog */
/* BROWSE-TAB brwDests brwParams fDialog */
ASSIGN 
       brwDests:ALLOW-COLUMN-SEARCHING IN FRAME fDialog = TRUE
       brwDests:COLUMN-RESIZABLE IN FRAME fDialog       = TRUE.

ASSIGN 
       brwParams:ALLOW-COLUMN-SEARCHING IN FRAME fDialog = TRUE
       brwParams:COLUMN-RESIZABLE IN FRAME fDialog       = TRUE.

ASSIGN 
       tAction:READ-ONLY IN FRAME fDialog        = TRUE.

ASSIGN 
       tFault:READ-ONLY IN FRAME fDialog        = TRUE.

ASSIGN 
       tQueue:READ-ONLY IN FRAME fDialog        = TRUE.

ASSIGN 
       tStatus:READ-ONLY IN FRAME fDialog        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDests
/* Query rebuild information for BROWSE brwDests
     _START_FREEFORM
open query {&SELF-NAME} for each destitem.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwDests */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwParams
/* Query rebuild information for BROWSE brwParams
     _START_FREEFORM
open query {&SELF-NAME} for each paramitem.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwParams */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* View System Queue */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* View System Queue */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDests
&Scoped-define SELF-NAME brwDests
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDests C-Win
ON ROW-DISPLAY OF brwDests IN FRAME fDialog /* Destinations */
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDests C-Win
ON START-SEARCH OF brwDests IN FRAME fDialog /* Destinations */
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwParams
&Scoped-define SELF-NAME brwParams
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwParams C-Win
ON ROW-DISPLAY OF brwParams IN FRAME fDialog /* Parameters */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwParams C-Win
ON START-SEARCH OF brwParams IN FRAME fDialog /* Parameters */
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDests
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-show.i}
{lib/win-close.i}
{lib/brw-main-multi.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN enable_UI.
  for first sysqueue no-lock:
    assign
      tQueue:screen-value  = string(sysqueue.queueID)
      tStatus:screen-value = sysqueue.itemStatDesc
      tAction:screen-value = sysqueue.actionDesc
      tFault:screen-value  = sysqueue.itemFault
      .

    /* add the parameters */
    do std-in = 1 to num-entries(sysqueue.parameters, {&msg-dlm}):
      tItem = entry(std-in, sysqueue.parameters, {&msg-dlm}).
      create paramitem.
      assign
        paramitem.id  = entry(1, tItem, "=")
        paramitem.val = entry(2, tItem, "=")
        .
    end.
    {&OPEN-QUERY-brwParams}

    /* add the destinations */
    do std-in = 1 to num-entries(sysqueue.destinations, {&msg-dlm}):
      tItem = entry(std-in, sysqueue.destinations, {&msg-dlm}).
      publish "GetSysPropDesc" ("SYS", "Destination", "Type", entry(1, tItem, "="), output std-ch).
      create paramitem.
      assign
        paramitem.id  = std-ch
        paramitem.val = entry(2, tItem, "=")
        .
    end.
    {&OPEN-QUERY-brwDests}
  end.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tQueue tStatus tFault tAction 
      WITH FRAME fDialog IN WINDOW C-Win.
  ENABLE RECT-79 tQueue tStatus tFault tAction brwParams brwDests 
      WITH FRAME fDialog IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fDialog}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

