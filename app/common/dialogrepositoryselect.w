&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fDocuments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fDocuments 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{tt/repository.i &tableAlias="tempfile"}
/* Parameters Definitions ---                                           */
define input  parameter pEntity     as character no-undo.
define input  parameter pEntityId   as character no-undo.
define input  parameter pOnlyPublic as logical   no-undo.
define output parameter table for tempfile.
define output parameter pCancel     as logical   no-undo initial true.

/* Local Variable Definitions ---                                       */
{lib/std-def.i}
{lib/brw-multi-def.i}

/* Temp Table ---                                                       */
{tt/repository.i &tableAlias="orderfolder"}
{tt/repository.i &tableAlias="orderfile"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fDocuments
&Scoped-define BROWSE-NAME brwFiles

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES orderfile orderfolder

/* Definitions for BROWSE brwFiles                                      */
&Scoped-define FIELDS-IN-QUERY-brwFiles orderfile.displayName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFiles   
&Scoped-define SELF-NAME brwFiles
&Scoped-define QUERY-STRING-brwFiles FOR EACH orderfile by orderfile.displayName
&Scoped-define OPEN-QUERY-brwFiles OPEN QUERY {&SELF-NAME} FOR EACH orderfile by orderfile.displayName.
&Scoped-define TABLES-IN-QUERY-brwFiles orderfile
&Scoped-define FIRST-TABLE-IN-QUERY-brwFiles orderfile


/* Definitions for BROWSE brwFolders                                    */
&Scoped-define FIELDS-IN-QUERY-brwFolders orderfolder.displayName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFolders
&Scoped-define SELF-NAME brwFolders
&Scoped-define QUERY-STRING-brwFolders FOR EACH orderfolder by orderfolder.displayName
&Scoped-define OPEN-QUERY-brwFolders OPEN QUERY {&SELF-NAME} FOR EACH orderfolder by orderfolder.displayName.
&Scoped-define TABLES-IN-QUERY-brwFolders orderfolder
&Scoped-define FIRST-TABLE-IN-QUERY-brwFolders orderfolder


/* Definitions for DIALOG-BOX fDocuments                                */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwFolders brwFiles bOK bCancel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setupDocuments fDocuments 
FUNCTION setupDocuments RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwFiles 
       MENU-ITEM m_Download_File LABEL "Download File" .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bOK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwFiles FOR 
      orderfile SCROLLING.

DEFINE QUERY brwFolders FOR 
      orderfolder SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwFiles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFiles fDocuments _FREEFORM
  QUERY brwFiles DISPLAY
      orderfile.displayName column-label "File Name"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET SIZE 52 BY 7.86
         TITLE "Files" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwFolders
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFolders fDocuments _FREEFORM
  QUERY brwFolders DISPLAY
      orderfolder.displayName column-label "Folder Name"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET SIZE 34 BY 7.86
         TITLE "Folders" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fDocuments
     brwFolders AT ROW 1.24 COL 2 WIDGET-ID 1500
     brwFiles AT ROW 1.24 COL 37 WIDGET-ID 1600
     bOK AT ROW 9.33 COL 31
     bCancel AT ROW 9.33 COL 47
     SPACE(28.19) SKIP(0.43)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Select a document"
         DEFAULT-BUTTON bOK CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fDocuments
   FRAME-NAME                                                           */
/* BROWSE-TAB brwFolders 1 fDocuments */
/* BROWSE-TAB brwFiles brwFolders fDocuments */
ASSIGN 
       FRAME fDocuments:SCROLLABLE       = FALSE
       FRAME fDocuments:HIDDEN           = TRUE.

ASSIGN 
       brwFiles:POPUP-MENU IN FRAME fDocuments             = MENU POPUP-MENU-brwFiles:HANDLE
       brwFiles:ALLOW-COLUMN-SEARCHING IN FRAME fDocuments = TRUE
       brwFiles:COLUMN-RESIZABLE IN FRAME fDocuments       = TRUE
       brwFiles:COLUMN-MOVABLE IN FRAME fDocuments         = TRUE.

ASSIGN 
       brwFolders:ALLOW-COLUMN-SEARCHING IN FRAME fDocuments = TRUE
       brwFolders:COLUMN-RESIZABLE IN FRAME fDocuments       = TRUE
       brwFolders:COLUMN-MOVABLE IN FRAME fDocuments         = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFiles
/* Query rebuild information for BROWSE brwFiles
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH orderfile by orderfile.displayName.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwFiles */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFolders
/* Query rebuild information for BROWSE brwFolders
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH orderfolder by orderfolder.fullpath.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwFolders */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fDocuments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDocuments fDocuments
ON WINDOW-CLOSE OF FRAME fDocuments /* Select a document */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFiles
&Scoped-define SELF-NAME brwFiles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFiles fDocuments
ON DEFAULT-ACTION OF brwFiles IN FRAME fDocuments /* Files */
DO:
  apply "CHOOSE" to bOK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFiles fDocuments
ON ROW-DISPLAY OF brwFiles IN FRAME fDocuments /* Files */
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFiles fDocuments
ON START-SEARCH OF brwFiles IN FRAME fDocuments /* Files */
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFolders
&Scoped-define SELF-NAME brwFolders
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFolders fDocuments
ON ROW-DISPLAY OF brwFolders IN FRAME fDocuments /* Folders */
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFolders fDocuments
ON START-SEARCH OF brwFolders IN FRAME fDocuments /* Folders */
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFolders fDocuments
ON VALUE-CHANGED OF brwFolders IN FRAME fDocuments /* Folders */
DO:
  if not available orderfolder
   then return.

  define variable tWhereClause as character no-undo.
  tWhereClause = "where category = ~"" + orderfolder.category + "~"".
  {lib/brw-startSearch-multi.i &browseName=brwFiles &whereClause=tWhereClause}
  apply "VALUE-CHANGED" to browse brwFiles.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Download_File
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Download_File fDocuments
ON CHOOSE OF MENU-ITEM m_Download_File /* Download File */
DO:
  if not available orderfile
   then return.
   
  define variable tFile as character no-undo.
  run server/downloadarcrepositoryfile.p (orderfile.identifier, output tFile, output std-lo, output std-ch).
  if not std-lo
   then message std-ch view-as alert-box error buttons ok.
   else run util/openfile.p (tFile).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFiles
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fDocuments 


/* ***************************  Main Block  *************************** */

{lib/brw-main-multi.i &browse-list="brwFiles,brwFolders" &noSort="brwFolders"}

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
  RUN enable_UI.
  empty temp-table orderfile.
  run server/getarcrepositoryfiles.p (pEntity, pEntityId, pOnlyPublic, output table orderfile, output std-lo, output std-ch).
  if not std-lo
   then message std-ch view-as alert-box error buttons ok.
   else setupDocuments().
   
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  
  if available orderfile
   then
    do:
      pCancel = false.
      create tempfile.
      buffer-copy orderfile to tempfile.
    end.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE createRepositoryFolder wWin 
PROCEDURE createRepositoryFolder :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pCategory as character no-undo.
  
  define variable tFolder as character no-undo.
  define variable tUserID as character no-undo.
  define variable tRowID  as rowid     no-undo.
   
    
  do with frame fDocuments:
    create orderfolder.
    assign
      orderfolder.displayName = pCategory
      orderfolder.category    = pCategory
      .
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fDocuments  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fDocuments.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fDocuments  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE brwFolders brwFiles bOK bCancel 
      WITH FRAME fDocuments.
  VIEW FRAME fDocuments.
  {&OPEN-BROWSERS-IN-QUERY-fDocuments}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setupDocuments fDocuments 
FUNCTION setupDocuments RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
  close query brwFolders.
  close query brwFiles.
  empty temp-table orderfolder.
  for each orderfile exclusive-lock
  break by orderfile.category:

    if first-of(orderfile.category)
     then run createRepositoryFolder in this-procedure (orderfile.category).
     
    publish "GetSysUserName" (orderfile.createdBy, output orderfile.createdByDesc).
  end.
  {&OPEN-QUERY-brwFolders}
  apply "VALUE-CHANGED" to browse brwFolders.
  apply "VALUE-CHANGED" to browse brwFiles.
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData wWin 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

