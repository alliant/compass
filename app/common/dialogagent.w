&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
@name View Agent (dialogagent.w)
@notes
Rahul 06/13/2019 Added new field "legalName"
Rahul 08/08/2019 Modified to remove all existing status buttons and add 
                 new button for status change.
Vikas 08/30/2019 Modified to set invoice due date for an agent.                 
Vikas 12/26/2019 Modified to set ARCashGLRef ARCachGLDesc ARGLRef ARGLDesc an agent.                 
AG    06/19/2020 Added compress image and delete image feature 
SB    12/16/2020 Task-86695-Agent Manager Type Implementation.
SA    03/30/2022 Task #86699 Modified to update linking of agent and org.
SA    06/10/2022 Task #93485 Added fields "Region" and "Domain"
VR    07/28/2022 Modified to support clob field changes
SR    08/09/2022 Task #96974  fixed the regions combo-box error.
VR    10/07/2022 Task # 114127 Default Agent Legal Name as a Organization name while
                 linking an agent with New Organization.
SB    11/29/2024 Task# 117467 Modified to remove active qualifications grid as it is of no use.
SC    11/29/2024 Task# 117302 Prepopulate the GL Ref numbers in Accounts Receivable screen 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input parameter hFileDataSrv as handle    no-undo.
define input parameter ipcAction    as character no-undo.

/* Local Variable Definitions ---                                       */
define variable cManagerUID     as character no-undo.
define variable cMethod         as character no-undo.
define variable iDueInDays      as integer   no-undo.
define variable iDueOnDay       as integer   no-undo.
define variable cCashAcc        as character no-undo.
define variable cCashAccDesc    as character no-undo.
define variable cARAcc          as character no-undo.
define variable cARAccDesc      as character no-undo.
define variable cRefundAcc      as character no-undo.
define variable cRefundAccDesc  as character no-undo.
define variable cWroffARAcc     as character no-undo.
define variable cWroffARAccDesc as character no-undo.
define variable cStateID        as character no-undo.
define variable cRegionID       as character no-undo.

/* window */
define variable hManager             as handle    no-undo.
define variable hCopy                as handle    no-undo.
define variable procname             as character no-undo.
define variable decdmptr             as memptr    no-undo.
define variable IMAGE-2              as handle    no-undo.
define variable cAgentStatus         as character no-undo.
define variable chPath               as character no-undo.
define variable lImage               as longchar  no-undo.
define variable chimage              as longchar  no-undo.
define variable lLogoChange          as logical   no-undo.
define variable dcolumnwidth         as decimal   no-undo.
define variable cOldOrgID            as character no-undo.
define variable outimgloc            as character no-undo.
define variable lModifyOrganization  as logical   no-undo.

{lib/std-def.i}
{lib/add-delimiter.i}
{tt/agent.i}
{tt/agentmanager.i}
{tt/agentmanager.i &tableAlias="tagentmanager"}
{tt/agentmanager.i &tableAlias="ttagentmanager"}
{tt/state.i}
{tt/organization.i}
{tt/organization.i &tableAlias="tOrganization"}
{lib/set-status.i}
{lib/com-def.i}
{lib/validstate.i}
{lib/winshowscrollbars.i}
{lib/winlaunch.i}
{tt/imageinfo.i}
{tt/region.i}

define temp-table tempagent like agent.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fAgent

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bManager tName bdeleteImag tLegalName ~
tOrganization tAddr1 tAddr2 tCity tState tZip tPhone BtnUrl tFax tEmail ~
tWebsite fDomain cbSwVendor tStateOper bInvDate cbRegion tContractID ~
tRemitType tContractDate tLiabilityLimit tRemitValue tRemitAlert ~
tPolicyLimit tProspectDate tActiveDate tWithdrawnDate tClosedDate ~
tReviewDate tCancelDate mName mAddr1 tAgentID mAddr2 mCity mState mZip ~
bEmail btnSameAsAbove fNPN tAltaUID tStateUID tCorporation bOrgLookup ~
bAddImg RECT-34 RECT-35 RECT-36 
&Scoped-Define DISPLAYED-OBJECTS tStat tName tLegalName tOrganization ~
tAddr1 tAddr2 tCity tState tZip tPhone tFax tEmail tWebsite fDomain ~
cbSwVendor tStateOper cbRegion tContractID tRemitType tContractDate ~
tLiabilityLimit tRemitValue tRemitAlert tPolicyLimit tProspectDate ~
tActiveDate tWithdrawnDate tClosedDate tReviewDate tCancelDate mName mAddr1 ~
tAgentID mAddr2 mCity mState mZip fNPN tAltaUID tStateUID tCorporation ~
fManager 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD agentStat C-Win 
FUNCTION agentStat RETURNS CHARACTER
  ( input cStat as character /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doModify C-Win 
FUNCTION doModify RETURNS LOGICAL
  ( input pModify as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getauth C-Win 
FUNCTION getauth returns character ( cAuth as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getOrgName C-Win 
FUNCTION getOrgName RETURNS CHARACTER
  ( ipcOrgID as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VARIABLE C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddImg  NO-FOCUS
     LABEL "Add Logo" 
     SIZE 4.6 BY 1.14 TOOLTIP "Add Agent Logo".

DEFINE BUTTON bCancel AUTO-GO  NO-FOCUS
     LABEL "Cancel" 
     SIZE 4.6 BY 1.14 TOOLTIP "Cancel".

DEFINE BUTTON bCopy AUTO-GO  NO-FOCUS
     LABEL "Copy" 
     SIZE 4.6 BY 1.14 TOOLTIP "Copy".

DEFINE BUTTON bdeleteImag  NO-FOCUS
     LABEL "Delete" 
     SIZE 4.6 BY 1.14 TOOLTIP "Delete Agent Logo".

DEFINE BUTTON bEmail  NO-FOCUS
     LABEL "Email" 
     SIZE 4.8 BY 1.14 TOOLTIP "Send email".

DEFINE BUTTON bInvDate AUTO-GO  NO-FOCUS
     LABEL "InvDateMethod" 
     SIZE 4.6 BY 1.14 TOOLTIP "Accounts Receivables Information".

DEFINE BUTTON bManager  NO-FOCUS
     LABEL "Manager" 
     SIZE 4.6 BY 1.14 TOOLTIP "Manager".

DEFINE BUTTON bOrgLookup  NO-FOCUS
     LABEL "Lookup" 
     SIZE 4.6 BY 1.14 TOOLTIP "Organization lookup".

DEFINE BUTTON bSave AUTO-GO  NO-FOCUS
     LABEL "Save" 
     SIZE 4.6 BY 1.14 TOOLTIP "Save".

DEFINE BUTTON bStatusChange AUTO-GO  NO-FOCUS
     LABEL "Status Change" 
     SIZE 4.6 BY 1.14 TOOLTIP "Change Status".

DEFINE BUTTON btnSameAsAbove 
     LABEL "Same as Above" 
     SIZE 20 BY 1.14.

DEFINE BUTTON BtnUrl  NO-FOCUS
     LABEL "URL" 
     SIZE 4.6 BY 1.14 TOOLTIP "Open URL".

DEFINE VARIABLE cbRegion AS CHARACTER FORMAT "X(256)" 
     LABEL "Region" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 18.8 BY 1 NO-UNDO.

DEFINE VARIABLE cbSwVendor AS CHARACTER FORMAT "X(256)" 
     LABEL "Software" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 47 BY 1 TOOLTIP "Title company software vendor" NO-UNDO.

DEFINE VARIABLE mState AS CHARACTER FORMAT "X(256)" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tContractID AS CHARACTER FORMAT "X(256)" 
     LABEL "Contract" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 18.8 BY 1 TOOLTIP "Contract" NO-UNDO.

DEFINE VARIABLE tState AS CHARACTER FORMAT "X(256)" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE tStateOper AS CHARACTER FORMAT "X(256)" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 18.8 BY 1 NO-UNDO.

DEFINE VARIABLE fDomain AS CHARACTER FORMAT "X(256)":U 
     LABEL "Domain" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE fManager AS CHARACTER FORMAT "X(256)":U 
     LABEL "Manager" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 43.2 BY 1 NO-UNDO.

DEFINE VARIABLE fNPN AS CHARACTER FORMAT "X(256)":U 
     LABEL "NPN" 
     VIEW-AS FILL-IN 
     SIZE 48 BY 1 NO-UNDO.

DEFINE VARIABLE mAddr1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE mAddr2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE mCity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 17.8 BY 1 TOOLTIP "City" NO-UNDO.

DEFINE VARIABLE mName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE mZip AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 TOOLTIP "Zipcode" NO-UNDO.

DEFINE VARIABLE tActiveDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Active" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tAddr1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE tAddr2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER FORMAT "X(10)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN 
     SIZE 17.8 BY 1 NO-UNDO.

DEFINE VARIABLE tAltaUID AS CHARACTER FORMAT "X(200)":U 
     LABEL "ALTA UID" 
     VIEW-AS FILL-IN 
     SIZE 48 BY 1 NO-UNDO.

DEFINE VARIABLE tCancelDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Cancelled" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tCity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 17.8 BY 1 TOOLTIP "City" NO-UNDO.

DEFINE VARIABLE tClosedDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Closed" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tContractDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Signed" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tCorporation AS CHARACTER FORMAT "X(256)":U 
     LABEL "Company" 
     VIEW-AS FILL-IN 
     SIZE 48 BY 1 NO-UNDO.

DEFINE VARIABLE tEmail AS CHARACTER FORMAT "X(256)":U 
     LABEL "Email" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE tFax AS CHARACTER FORMAT "X(256)":U 
     LABEL "Fax" 
     VIEW-AS FILL-IN 
     SIZE 19.2 BY 1 NO-UNDO.

DEFINE VARIABLE tLegalName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Legal Name" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE tLiabilityLimit AS DECIMAL FORMAT ">>,>>>,>>9":U INITIAL 0 
     LABEL "Limit($)" 
     VIEW-AS FILL-IN 
     SIZE 18.8 BY 1 TOOLTIP "Maximum coverage w/o prior approval" NO-UNDO.

DEFINE VARIABLE tName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE tOrganization AS CHARACTER FORMAT "X(256)":U 
     LABEL "Organization" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE tPhone AS CHARACTER FORMAT "X(256)":U 
     LABEL "Phone" 
     VIEW-AS FILL-IN 
     SIZE 21.8 BY 1 NO-UNDO.

DEFINE VARIABLE tPolicyLimit AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Limit(#)" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 TOOLTIP "Policy Limit" NO-UNDO.

DEFINE VARIABLE tProspectDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Prospect" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tRemitAlert AS DECIMAL FORMAT ">>>,>>>,>>9":U INITIAL 0 
     LABEL "Alert" 
     VIEW-AS FILL-IN 
     SIZE 14.4 BY 1 TOOLTIP "Processing threshold for review" NO-UNDO.

DEFINE VARIABLE tRemitValue AS DECIMAL FORMAT "9.99":U INITIAL 0 
     LABEL "Remits" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 TOOLTIP "Percentage, then value is % of gross :: Liability, then value is rate/1000" NO-UNDO.

DEFINE VARIABLE tReviewDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Reviewed" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Last review" NO-UNDO.

DEFINE VARIABLE tStat AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20.2 BY 1 NO-UNDO.

DEFINE VARIABLE tStateUID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State UID" 
     VIEW-AS FILL-IN 
     SIZE 48 BY 1 NO-UNDO.

DEFINE VARIABLE tWebsite AS CHARACTER FORMAT "X(256)":U 
     LABEL "Website" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE tWithdrawnDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Withdrawn" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tZip AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 TOOLTIP "Zipcode" NO-UNDO.

DEFINE VARIABLE tRemitType AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Percentage", "P",
"Liability", "L"
     SIZE 18 BY 1.71 NO-UNDO.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 66 BY 6.76.

DEFINE RECTANGLE RECT-34
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 6 BY 3.76.

DEFINE RECTANGLE RECT-35
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 6 BY 1.52.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 6 BY 1.52.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 66 BY 6.52.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 66 BY 6.52.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 66 BY 6.76.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fAgent
     bManager AT ROW 23.81 COL 132.8 WIDGET-ID 320 NO-TAB-STOP 
     tStat AT ROW 1.43 COL 48 COLON-ALIGNED WIDGET-ID 242
     tName AT ROW 2.62 COL 21.2 COLON-ALIGNED WIDGET-ID 4
     bdeleteImag AT ROW 2.57 COL 75.8 WIDGET-ID 324 NO-TAB-STOP 
     tLegalName AT ROW 3.81 COL 21.2 COLON-ALIGNED WIDGET-ID 254
     tOrganization AT ROW 5 COL 21.2 COLON-ALIGNED WIDGET-ID 262
     tAddr1 AT ROW 6.19 COL 21.2 COLON-ALIGNED WIDGET-ID 6
     tAddr2 AT ROW 7.38 COL 21.2 COLON-ALIGNED NO-LABEL WIDGET-ID 8
     tCity AT ROW 8.57 COL 21.2 COLON-ALIGNED NO-LABEL WIDGET-ID 10
     tState AT ROW 8.57 COL 39.6 COLON-ALIGNED NO-LABEL WIDGET-ID 150
     tZip AT ROW 8.57 COL 58.2 COLON-ALIGNED NO-LABEL WIDGET-ID 148
     tPhone AT ROW 9.81 COL 21.2 COLON-ALIGNED WIDGET-ID 142
     BtnUrl AT ROW 7.43 COL 136.6 WIDGET-ID 92 NO-TAB-STOP 
     tFax AT ROW 9.81 COL 49 COLON-ALIGNED WIDGET-ID 140
     tEmail AT ROW 6.38 COL 87 COLON-ALIGNED WIDGET-ID 20
     tWebsite AT ROW 7.52 COL 87 COLON-ALIGNED WIDGET-ID 22
     fDomain AT ROW 8.67 COL 87 COLON-ALIGNED WIDGET-ID 384
     cbSwVendor AT ROW 9.81 COL 87 COLON-ALIGNED WIDGET-ID 240
     tStateOper AT ROW 11.95 COL 21.2 COLON-ALIGNED WIDGET-ID 104
     bInvDate AT ROW 6.62 COL 2.2 WIDGET-ID 256 NO-TAB-STOP 
     cbRegion AT ROW 11.95 COL 50.2 COLON-ALIGNED WIDGET-ID 382
     tContractID AT ROW 13.14 COL 21.2 COLON-ALIGNED WIDGET-ID 252
     tRemitType AT ROW 13.38 COL 52.4 NO-LABEL WIDGET-ID 152
     tContractDate AT ROW 14.33 COL 21.2 COLON-ALIGNED WIDGET-ID 36
     tLiabilityLimit AT ROW 15.52 COL 21.2 COLON-ALIGNED WIDGET-ID 40
     tRemitValue AT ROW 15.52 COL 50.2 COLON-ALIGNED WIDGET-ID 38
     tRemitAlert AT ROW 16.71 COL 21 COLON-ALIGNED WIDGET-ID 158
     tPolicyLimit AT ROW 16.71 COL 50.2 COLON-ALIGNED WIDGET-ID 380
     tProspectDate AT ROW 13.14 COL 87.2 COLON-ALIGNED WIDGET-ID 48
     tActiveDate AT ROW 13.14 COL 121.2 COLON-ALIGNED WIDGET-ID 50
     tWithdrawnDate AT ROW 14.33 COL 87.2 COLON-ALIGNED WIDGET-ID 246
     tClosedDate AT ROW 14.33 COL 121.2 COLON-ALIGNED WIDGET-ID 52
     tReviewDate AT ROW 15.52 COL 87.2 COLON-ALIGNED WIDGET-ID 46
     tCancelDate AT ROW 15.52 COL 121.2 COLON-ALIGNED WIDGET-ID 54
     mName AT ROW 19 COL 21.2 COLON-ALIGNED WIDGET-ID 172
     mAddr1 AT ROW 20.19 COL 21.2 COLON-ALIGNED WIDGET-ID 166
     tAgentID AT ROW 1.43 COL 21.2 COLON-ALIGNED WIDGET-ID 2 NO-TAB-STOP 
     mAddr2 AT ROW 21.38 COL 21.2 COLON-ALIGNED NO-LABEL WIDGET-ID 168
     mCity AT ROW 22.57 COL 21.2 COLON-ALIGNED NO-LABEL WIDGET-ID 170
     mState AT ROW 22.57 COL 40.2 COLON-ALIGNED NO-LABEL WIDGET-ID 174
     mZip AT ROW 22.57 COL 58.2 COLON-ALIGNED NO-LABEL WIDGET-ID 176
     bEmail AT ROW 6.29 COL 136.6 WIDGET-ID 318 NO-TAB-STOP 
     btnSameAsAbove AT ROW 23.81 COL 32.2 WIDGET-ID 188
     fNPN AT ROW 19 COL 87.2 COLON-ALIGNED WIDGET-ID 214
     tAltaUID AT ROW 20.19 COL 87.2 COLON-ALIGNED WIDGET-ID 186
     tStateUID AT ROW 21.38 COL 87.2 COLON-ALIGNED WIDGET-ID 210
     tCorporation AT ROW 22.57 COL 87.2 COLON-ALIGNED WIDGET-ID 212
     fManager AT ROW 23.81 COL 87.2 COLON-ALIGNED WIDGET-ID 322
     bStatusChange AT ROW 5.24 COL 2.2 WIDGET-ID 222 NO-TAB-STOP 
     bCancel AT ROW 2.71 COL 2.2 WIDGET-ID 216 NO-TAB-STOP 
     bOrgLookup AT ROW 4.91 COL 70.6 WIDGET-ID 264 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COLUMN 1.2 ROW 1.05
         SIZE 142.6 BY 33.52 WIDGET-ID 100.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME fAgent
     bCopy AT ROW 3.86 COL 2.2 WIDGET-ID 226 NO-TAB-STOP 
     bAddImg AT ROW 1.38 COL 75.8 WIDGET-ID 234 NO-TAB-STOP 
     bSave AT ROW 1.57 COL 2.2 WIDGET-ID 224 NO-TAB-STOP 
     "Activity" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 11.14 COL 77 WIDGET-ID 138
          FONT 6
     "Operating Agreement" VIEW-AS TEXT
          SIZE 24.6 BY .62 AT ROW 11.19 COL 9.6 WIDGET-ID 78
          FONT 6
     "Other" VIEW-AS TEXT
          SIZE 6.8 BY .62 AT ROW 18.14 COL 77 WIDGET-ID 184
          FONT 6
     "Mailing Address" VIEW-AS TEXT
          SIZE 18 BY .62 AT ROW 18.14 COL 10 WIDGET-ID 180
          FONT 6
     "Remit:" VIEW-AS TEXT
          SIZE 6.4 BY .62 AT ROW 14 COL 45.8 WIDGET-ID 156
     RECT-4 AT ROW 11.48 COL 8 WIDGET-ID 74
     RECT-5 AT ROW 11.48 COL 75.2 WIDGET-ID 136
     RECT-3 AT ROW 18.52 COL 8 WIDGET-ID 178
     RECT-6 AT ROW 18.52 COL 75.2 WIDGET-ID 182
     RECT-34 AT ROW 1.43 COL 1.6 WIDGET-ID 248
     RECT-35 AT ROW 5.1 COL 1.6 WIDGET-ID 250
     RECT-36 AT ROW 6.48 COL 1.6 WIDGET-ID 258
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COLUMN 1.2 ROW 1.05
         SIZE 142.6 BY 33.52 WIDGET-ID 100.

DEFINE FRAME FRAME-A
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COLUMN 81.2 ROW 1.43
         SIZE 60 BY 4.76
         BGCOLOR 15 FGCOLOR 15  WIDGET-ID 300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "New Agent"
         HEIGHT             = 24.57
         WIDTH              = 143.6
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME FRAME-A:FRAME = FRAME fAgent:HANDLE.

/* SETTINGS FOR FRAME fAgent
   FRAME-NAME Custom                                                    */
/* SETTINGS FOR BUTTON bCancel IN FRAME fAgent
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bCopy IN FRAME fAgent
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bSave IN FRAME fAgent
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bStatusChange IN FRAME fAgent
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fManager IN FRAME fAgent
   NO-ENABLE                                                            */
ASSIGN 
       fManager:READ-ONLY IN FRAME fAgent        = TRUE.

/* SETTINGS FOR RECTANGLE RECT-3 IN FRAME fAgent
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-4 IN FRAME fAgent
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-5 IN FRAME fAgent
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-6 IN FRAME fAgent
   NO-ENABLE                                                            */
ASSIGN 
       tAgentID:READ-ONLY IN FRAME fAgent        = TRUE.

ASSIGN 
       tOrganization:READ-ONLY IN FRAME fAgent        = TRUE.

/* SETTINGS FOR FILL-IN tStat IN FRAME fAgent
   NO-ENABLE                                                            */
ASSIGN 
       tStat:READ-ONLY IN FRAME fAgent        = TRUE.

/* SETTINGS FOR FRAME FRAME-A
                                                                        */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* New Agent */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* New Agent */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* New Agent */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddImg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddImg C-Win
ON CHOOSE OF bAddImg IN FRAME fAgent /* Add Logo */
do:
  /* Initially false */ 
  std-lo = false.

  system-dialog get-file procname title "Select the Logo File"  
    filters "PNG (*.PNG)"   "*.PNG",
            "JPG (*.JPG)"   "*.JPG",
            "GIF (*.GIF)"   "*.GIF",
            "ICO (*.ICO)"   "*.ICO",
            "JPEG (*.JPEG)"   "*.JPEG",
            "BITMAP FILES (*.BMP)"  "*.BMP"        
    must-exist   
    use-filename 
    update std-lo.

  if not std-lo 
   then
    return. 
  

  copy-lob from file procname to decdmptr.
      
  copy-lob from decdmptr to file chPath.
  run setLogo.

  os-delete value(chPath).
  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME fAgent /* Cancel */
do:
  run SetFields in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCopy C-Win
ON CHOOSE OF bCopy IN FRAME fAgent /* Copy */
do:
  if valid-handle(hCopy) then
    run ShowWindow in hCopy.
  else
    run dialogagent.w persistent set hCopy (input hFileDataSrv, input "C").
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bdeleteImag
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bdeleteImag C-Win
ON CHOOSE OF bdeleteImag IN FRAME fAgent /* Delete */
do:
  run server\deleteagentlogo.p( input tAgentID:screen-value,
                                output std-lo,
                                output std-ch).
  if not std-lo 
   then
    do:
       message std-ch
        view-as alert-box information buttons ok. 
       return no-apply.
    end. 
    
  IMAGE-2:load-image("") no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEmail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEmail C-Win
ON CHOOSE OF bEmail IN FRAME fAgent /* Email */
do:
  if tEmail:input-value eq "" 
   then
    return.

  run openURL("mailto:" + tEmail:input-value).   
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bInvDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bInvDate C-Win
ON CHOOSE OF bInvDate IN FRAME fAgent /* InvDateMethod */
do: 
   run dialogAgentArInfo.w(input-output cMethod,
                           input-output iDueInDays,
                           input-output iDueOnDay,
                           input-output cCashAcc,
                           input-output cCashAccDesc,
                           input-output cARAcc,
                           input-output cARAccDesc,
                           input-output cRefundAcc,                                     
                           input-output cRefundAccDesc,          
                           input-output cWroffARAcc,                           
                           input-output cWroffARAccDesc,
                           output std-lo).    
   doModify(bSave:sensitive or std-lo).                       
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bManager
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bManager C-Win
ON CHOOSE OF bManager IN FRAME fAgent /* Manager */
DO:
  if valid-handle(hManager)
   then 
    run ShowWindow in hManager.
   else
    do:
      std-lo = false.
      if valid-handle(hFileDataSrv)
       then
        run CanCommission in hFileDataSrv (output std-lo). 
        
      run dialogagentmanager.w persistent set hManager (input this-procedure, input hFileDataSrv, input tName:screen-value, input std-lo,input ipcAction, input table agentmanager).
    end.
    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOrgLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOrgLookup C-Win
ON CHOOSE OF bOrgLookup IN FRAME fAgent /* Lookup */
do:
  run openDialog in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME fAgent /* Save */
do:
  run SaveAgent in this-procedure (output std-lo).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bStatusChange
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bStatusChange C-Win
ON CHOOSE OF bStatusChange IN FRAME fAgent /* Status Change */
do: 
  define variable cToStatus as character  no-undo.
  
  run getChangedStatus in this-procedure (output cToStatus,
                                          output std-lo). 
  if not std-lo
   then
    return no-apply.
    
  run setAgentStatus in this-procedure (input cToStatus).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnSameAsAbove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnSameAsAbove C-Win
ON CHOOSE OF btnSameAsAbove IN FRAME fAgent /* Same as Above */
DO:
  do with frame {&frame-name}:
    assign
        mName:screen-value  = tName:screen-value
        mAddr1:screen-value = tAddr1:screen-value
        mAddr2:screen-value = tAddr2:screen-value
        mCity:screen-value  = tCity:screen-value
        mState:screen-value = tState:screen-value
        mZip:screen-value   = tZip:screen-value
        .
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnUrl
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnUrl C-Win
ON CHOOSE OF BtnUrl IN FRAME fAgent /* URL */
do:
  if tWebsite:screen-value <> "" 
   then
    run ShellExecuteA in this-procedure (0,
                                         "open",
                                         tWebsite:screen-value,
                                         "",
                                         "",
                                         1,
                                         output std-in).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tName C-Win
ON LEAVE OF tName IN FRAME fAgent /* Name */
DO:
  if tLegalName:screen-value = ""
   then
    tLegalName:screen-value = tName:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tName C-Win
ON VALUE-CHANGED OF tName IN FRAME fAgent /* Name */
DO:
  if tStateOper:screen-value = "ALL" and tName:screen-value <> ""
   then
    doModify(false).
  else
    doModify(true).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tState C-Win
ON VALUE-CHANGED OF tState IN FRAME fAgent
DO:
  do with frame {&frame-name}:
    if lookup(tState:screen-value, tStateOper:list-item-pairs) > 0 and ipcAction <> "E"
     then tStateOper:screen-value = tState:screen-value.
     apply 'value-changed' to tStateOper.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateOper
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateOper C-Win
ON VALUE-CHANGED OF tStateOper IN FRAME fAgent /* State */
DO:
  cStateID = tStateOper:screen-value.
  
  for first state where state.stateID = cStateID:
   cRegionID = state.region.
  end.
  
  if cStateID = "ALL"
   then
    cbRegion:screen-value = "ALL".
  else
    cbRegion:screen-value = cRegionID.
    
  if tStateOper:screen-value <> "ALL" and tName:screen-value = ""
   then
    doModify(false).
  else
    doModify(true).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i} 
/* {lib/brw-main.i} */

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Used to initialize window handle in set-staus.i */
initializeStatusWindow({&window-name}:handle).

subscribe to "AgentDataChanged" anywhere.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bsave:load-image("images/s-save.bmp").
bsave:load-image-insensitive("images/s-save-i.bmp").
bCancel:load-image("images/s-cancel.bmp").
bCancel:load-image-insensitive("images/s-cancel-i.bmp").
bCopy:load-image("images/s-copy.bmp").
bCopy:load-image-insensitive("images/s-copy-i.bmp").
bAddImg:load-image("images/s-photo.bmp").
bdeleteImag:load-image("images/s-delete.bmp").
bStatusChange:load-image("images/s-swap.bmp").
bStatusChange:load-image-insensitive("images/s-swap-i.bmp").
BtnUrl  :load-image ("images/s-url.bmp").
BtnUrl  :load-image-insensitive("images/s-url-i.bmp").
bInvDate:load-image ("images/s-calendar.bmp").
bOrgLookup:load-image             ("images/s-update.bmp").
bOrgLookup:load-image-insensitive ("images/s-update-i.bmp").
bEmail:load-image ("images/s-email.bmp").
bEmail:load-image-insensitive("images/s-email-i.bmp").
bManager:load-image             ("images/s-update.bmp").
bManager:load-image-insensitive ("images/s-update-i.bmp").


/* Populate "cbSwVendor" combo-box */
{lib/get-syscode-list.i &combo=cbSwVendor &codeType="'TitleSoftware'" &d="'UNK'"}

/* Populate "tContractID" combo-box */
{lib/get-syscode-list.i &combo=tContractID &codeType="'AgentContract'"}

/* Populate "cbRegion" combo-box */
{lib/get-syscode-list.i &combo=cbRegion &codeType="'Region'"}


/* set the operating state list */
{lib/get-state-list.i &combo=tStateOper}
 
/* set the state list */
{lib/get-state-list.i &combo=tState &useFilter=false &onlyActive=false}
{lib/get-state-list.i &combo=mState &useFilter=false &onlyActive=false}

/* enable the Save button when anything changed */
on 'VALUE-CHANGED':U anywhere 
do:
  if self:name matches "{&browse-name}"
   then return.
   
  doModify(true).
end.
    
/* get the agent */
if valid-handle(hFileDataSrv) and (ipcAction = "E" or ipcAction = "C")then
do:
  run GetAgent in hFileDataSrv (output table agent).
  if ipcAction = "E"
   then run GetAgentManagers in hFileDataSrv (output table agentmanager).
  
end.

cAgentStatus = "P".

for first agent no-lock:
  
  if ipcAction = "E" then
  do:
    run GetAgentLogo in hFileDataSrv (output chimage).
    if chimage > ""
     then lImage = chimage.
    
    assign 
        {&window-name}:title    = agent.name
        cAgentStatus            = agent.stat
        bStatusChange:sensitive = true.
        .
  end.
    
  if ipcAction = "C" then
  do:
    assign {&window-name}:title = "New Agent from " + agent.name
           std-ch               = "".
  end.
end.

std-ch = "".
publish "GetTempDir" (output std-ch).
chPath = std-ch + "\agentlogo".

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.       
    /* create the Organization combo */

  if ipcAction <> "N" then
  do:
    if ipcAction = "E" then
    do:
      enable bCopy with frame {&frame-name}.
      disable tStateOper with frame {&frame-name}.
    end.
    if ipcAction = "C" then
      enable tStateOper with frame {&frame-name}.

    run SetFields in this-procedure.
  end.
  else 
  do:
    disable bCopy with frame {&frame-name}.
    enable tStateOper with frame {&frame-name}.
    tStat:screen-value = agentStat(cAgentStatus).
    
    if tEmail:input-value = "" 
     then
      bEmail:sensitive = false.
     else
      bEmail:sensitive = true.
      
    if tWebsite:input-value = "" 
     then
      BtnUrl:sensitive = false.
     else
      BtnUrl:sensitive = true.
      
    /*{lib/set-current-value.i &state=tState} */
  end.
  
/*state combo-box default value All*/
 tStateOper:list-item-pairs in frame {&frame-name} = "Select State,ALL," + tStateOper:list-item-pairs.
 tStateOper:screen-value = "ALL".
 
 /*region combo-box default value All*/
 cbRegion:list-item-pairs in frame {&frame-name} = "Select Region,ALL," + cbRegion:list-item-pairs.
 cbRegion:screen-value = "ALL".
  
  
  /* Set Status of the screen */
  setStatus("As of " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss AM")).

  /* Display status of the screen on the status bar */
  displayStatus().

  doModify(ipcAction = "C").

  run windowResized in this-procedure.

  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentDataChanged C-Win 
PROCEDURE AgentDataChanged :
/*------------------------------------------------------------------------------
@description A change was made in the application
------------------------------------------------------------------------------*/
  if valid-handle(hFileDataSrv)
   then
    do:
      run LoadAgent in hFileDataSrv.
      run GetAgent in hFileDataSrv (output table agent).
      run LoadAgentManagers in hFileDataSrv.
      run GetAgentManagers in hFileDataSrv (output table agentmanager).
      run SetFields in this-procedure.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
@desctiption Closes the window
------------------------------------------------------------------------------*/
  std-lo = true.
  if bSave:sensitive in frame {&frame-name} 
   then
    do:
      if ipcAction = "N" or ipcAction = "C"
       then
        message "The agent is currently being created. Do you want to continue?" view-as alert-box question buttons yes-no update std-lo.
      else if ipcAction = "E" 
       then
        message "The agent is currently being modified. Do you want to continue?" view-as alert-box question buttons yes-no update std-lo.
    end.
    
  if std-lo
   then
    do:
      if valid-handle(hManager)
       then run closeWindow in hManager.
      apply "close":u to this-procedure.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE createOrganization C-Win 
PROCEDURE createOrganization :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter cOrgID as character no-undo.
  
  empty temp-table tOrganization.
  
  if cOrgID ne '' and cOrgID ne ?
   then
    do:
      create tOrganization.
      assign
          tOrganization.orgID       = cOrgID
          tOrganization.name        = agent.legalName
          tOrganization.addr1       = agent.addr1
          tOrganization.addr2       = agent.addr2
          tOrganization.city        = agent.city
          tOrganization.state       = agent.state
          tOrganization.zip         = agent.zip
          tOrganization.email       = agent.email
          tOrganization.website     = agent.website
          tOrganization.fax         = agent.fax
          tOrganization.phone       = agent.phone
          tOrganization.stat        = {&ActiveStat}
          .
          
      create organization.
      buffer-copy tOrganization to organization.
      
      publish "newOrganizationAgent" (input table tOrganization).
    end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteManager C-Win 
PROCEDURE DeleteManager :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   define input parameter pManagerID as integer no-undo.
   define output parameter pSuccess  as logical no-undo.
   
   find first agentmanager where managerID =  pManagerID no-error.
   if not avail agentmanager 
    then
     return.
    else 
     do:
       delete agentmanager.
       pSuccess = true.
     end.
     
   cManagerUID = "".  
   for first agentmanager no-lock
       where agentmanager.isPrimary = true:
  
     assign
       std-ch = ""
       cManagerUID = agentmanager.uid
       .
     publish "GetSysUserName" (cManagerUID, output std-ch).
     do with frame {&frame-name}:
       assign fManager:screen-value = std-ch.
     end.
   end.
   if cManagerUID = ""
    then fManager:screen-value  = "None".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tStat tName tLegalName tOrganization tAddr1 tAddr2 tCity tState tZip 
          tPhone tFax tEmail tWebsite fDomain cbSwVendor tStateOper cbRegion 
          tContractID tRemitType tContractDate tLiabilityLimit tRemitValue 
          tRemitAlert tPolicyLimit tProspectDate tActiveDate tWithdrawnDate 
          tClosedDate tReviewDate tCancelDate mName mAddr1 tAgentID mAddr2 mCity 
          mState mZip fNPN tAltaUID tStateUID tCorporation fManager 
      WITH FRAME fAgent IN WINDOW C-Win.
  ENABLE bManager tName bdeleteImag tLegalName tOrganization tAddr1 tAddr2 
         tCity tState tZip tPhone BtnUrl tFax tEmail tWebsite fDomain 
         cbSwVendor tStateOper bInvDate cbRegion tContractID tRemitType 
         tContractDate tLiabilityLimit tRemitValue tRemitAlert tPolicyLimit 
         tProspectDate tActiveDate tWithdrawnDate tClosedDate tReviewDate 
         tCancelDate mName mAddr1 tAgentID mAddr2 mCity mState mZip bEmail 
         btnSameAsAbove fNPN tAltaUID tStateUID tCorporation bOrgLookup bAddImg 
         RECT-34 RECT-35 RECT-36 
      WITH FRAME fAgent IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fAgent}
  VIEW FRAME FRAME-A IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-A}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getChangedStatus C-Win 
PROCEDURE getChangedStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter ocToStatus      as character no-undo.
  define output parameter olStatusChange  as logical   no-undo.
  
  do with frame {&frame-name}:
    std-ch = tName:screen-value + " (" + tAgentID:screen-value + ")".
  end.
  
  run dialogchangestatus.w (input  std-ch,
                            input  cAgentStatus,
                            input  {&Agent},
                            output ocToStatus,
                            output olStatusChange).
                            
   

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getManagerData C-Win 
PROCEDURE getManagerData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter table for tagentmanager.
  if ipcAction = "E"
   then
    run getManagerData in hFileDataSrv (output table tagentmanager).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openDialog C-Win 
PROCEDURE openDialog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
  define variable cPrevOrgID as character no-undo.
  define variable cOrgID     as character no-undo.
  define variable cName      as character no-undo.
  
  if available agent 
   then
    cPrevOrgID = agent.orgID. 
    
  run dialogorganizationlookup.w(input  cPrevOrgID,
                                 input  true,
                                 input  true,
                                 input  false,
                                 output cOrgID,
                                 output cName,
                                 output std-lo).
   
  if not std-lo 
   then
    return no-apply.
    
  if cPrevOrgID = cOrgID 
   then
    doModify(false).
  if corgID = "-99"
   then
    doModify(true).
  
  tOrganization:screen-value = if cOrgID = "" then "" else cName . 
  tOrganization:private-data = if cOrgID = "" then "" else cOrgID.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveAgent C-Win 
PROCEDURE SaveAgent :
/*------------------------------------------------------------------------------
@description Saves the agent
------------------------------------------------------------------------------*/
  define output parameter pSuccess as logical no-undo.
  
  define variable hQuery   as handle    no-undo.
  define variable hBuffer  as handle    no-undo.
  define variable lcompare as logical   no-undo.
  define variable cOrgID   as character no-undo.
  define variable cOrg     as character no-undo.
  define variable cAgentID as character no-undo.
  define variable lSuccess as logical   no-undo.
  define variable cMsg     as character no-undo.
 
  /*validation for state corresponding region*/
  if tStateOper:screen-value in frame {&frame-name} = "ALL"
   then
    do:
      MESSAGE "Please select operating state."
      VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.
      apply "entry" to tStateOper.
      return no-apply.
    end.
  
  find first state where state.stateID = tStateOper:screen-value in frame {&frame-name} no-error.
  if available state
   then
    do:
      if state.region <> cbRegion:screen-value 
       then
        cMsg = "Operating State does not belong to region.".
    end.
    
  if cManagerUID = ""
   then cMsg = cMsg + chr(10) + "No manager has been assigned.".
  
  std-lo = true.
  if cMsg <> ""
   then 
    message cMsg + chr(10) + "Are you sure you want to continue?" view-as alert-box question buttons yes-no update std-lo.
    
  if not std-lo
   then 
    return.
  
  do with frame {&frame-name}:
    
    if tAgentID:input-value ne "" and ipcAction ne "E"
       then
        ipcAction = "E".
        
    if ipcAction = "N" or ipcAction = "C" then
    do:
      empty temp-table agent.
      create agent.
      agent.stat = "P".
    end.
    if tOrganization:private-data = "-99"
     then 
      do:
        cOrg = "-99".
        lModifyOrganization = true.
        message "A new organization will also be created." view-as alert-box information button ok.  
      end.
    else
     cOrg = tOrganization:private-data.
    
    /* We need to close the dialog even if we save the changes in the agent logo. */
    pSuccess = true.

       assign /* agent and address */
            agent.agentID            = tAgentID:input-value
            agent.name               = tName:input-value
            agent.legalName          = tLegalName:input-value
            agent.addr1              = tAddr1:input-value
            agent.addr2              = tAddr2:input-value
            agent.city               = tCity:input-value
            agent.state              = tState:input-value
            agent.region             = cbRegion:input-value
            agent.zip                = tZip:input-value
            /* status, phone, fax, email, website, software, and version */
            agent.phone              = tPhone:input-value
            agent.fax                = tFax:input-value
            agent.email              = tEmail:input-value
            agent.website            = tWebsite:input-value
            agent.domain             = fDomain:input-value
            agent.swVendor           = cbSwVendor:input-value
            /* Agent Agreement */
            agent.stateID            = tStateOper:input-value
            agent.contractID         = tContractID:input-value
            agent.contractDate       = tContractDate:input-value
            agent.remitType          = tRemitType:input-value
            agent.remitValue         = tRemitValue:input-value
            agent.remitAlert         = tRemitAlert:input-value
            agent.liabilityLimit     = tLiabilityLimit:input-value
            agent.policyLimit        = tPolicyLimit:input-value
            /* Activity */
            agent.prospectDate       = tProspectDate:input-value
            agent.closedDate         = tClosedDate:input-value
            agent.activeDate         = tActiveDate:input-value
            agent.cancelDate         = tCancelDate:input-value
            agent.reviewDate         = tReviewDate:input-value
            agent.withdrawndate      = tWithdrawnDate:input-value
            /* Mailing Address */
            agent.mName              = mName:input-value
            agent.mAddr1             = mAddr1:input-value
            agent.mAddr2             = mAddr2:input-value
            agent.mCity              = mCity:input-value
            agent.mState             = mState:input-value
            agent.mZip               = mZip:input-value
            /* Other */
            agent.altaUID            = tAltaUID:input-value
            agent.stateUID           = tStateUID:input-value
            agent.corporationID      = tCorporation:input-value
            agent.manager            = cManagerUID
            agent.NPN                = fNPN:input-value
            /* AR Related information */
            agent.invoiceDue         = cMethod
            agent.dueInDays          = iDueInDays
            agent.dueOnDays          = iDueOnDay
            agent.ARCashGLRef        = cCashAcc                
            agent.ARCashGLDesc       = cCashAccDesc
            agent.ARGLRef            = cARAcc                
            agent.ARGLDesc           = cARAccDesc
            agent.ARRefundGLRef      = cRefundAcc                
            agent.ARRefundGLDesc     = cRefundAccDesc
            agent.ARWriteOffGLRef    = cWroffARAcc                
            agent.ARWriteOffGLDesc   = cWroffARAccDesc
            .
            
    if cOrg <> ?
     then
      agent.orgID = cOrg.
    
    empty temp-table ttagentmanager.
    for each agentmanager:
      create ttagentmanager.
      buffer-copy agentmanager to ttagentmanager.
    end.
   
   if ipcAction = "N" or ipcAction = "C" then
    do:
      publish "NewAgent" (input table agent,
                          output cOrgID,
                          output std-ha,
                          output cAgentID).
      
      assign
          pSuccess = valid-handle(std-ha)
          hFileDataSrv = std-ha
          .
      
      if cAgentID <> "" 
       then
        assign
            tAgentID:screen-value = cAgentID 
            {&window-name}:title  = agent.name
            .
        
      if valid-handle(std-ha) and can-find(first agentmanager) 
       then
        run ModifyAgentManager in std-ha (table agentmanager,input false,output lSuccess).
        
      if cOrg = "-99" and (cOrgID <> "" or cOrgID <> ?)
       then
        run createOrganization in this-procedure(input cOrgID).
        
      if cOrgID <> "" 
       then
        do:
          agent.orgID = cOrgID.  
        end.
      tOrganization:screen-value = getOrgName(agent.orgID).
      tOrganization:private-data = agent.orgID.
      
      if agent.manager ne ""
       then
        do:
          publish "GetSysUserName" (agent.manager, output std-ch).
          fManager:screen-value = std-ch.
        end.
            
    end.
   else
    do:
      run ModifyAgent in hFileDataSrv (input  table agent, 
                                       input  cOldOrgID,
                                       output cOrgID, 
                                       output pSuccess).
                                       
      if pSuccess then
      do:
        if lModifyOrganization 
         then
          publish "modifyOrganizationCheck" (input lModifyOrganization). 
        
        if cOrg = "-99" and (cOrgID <> "" or cOrgID <> ?)
         then
          run createOrganization in this-procedure(input cOrgID).
          
        run ModifyAgentManager in hFileDataSrv (table ttagentmanager,input false,output pSuccess).
        run GetAgentManagers in hFileDataSrv (output table agentmanager).
        run SetFields in this-procedure.
        run ShowWindow in this-procedure.
      end.
    end.
    
    /* image upload will now be a stand alone API 
    if lLogoChange 
     then
      do:
        run SetAgentLogo in hFileDataSrv (input agent.agentID, input chimage, output std-lo).
        if ipcAction = "E" and std-lo then
        do:
          run SetFields in this-procedure.    
          run ShowWindow in this-procedure.  
        end.
      end. */
  end.
  doModify(not(pSuccess)).
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setAgentStatus C-Win 
PROCEDURE setAgentStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pStat as character no-undo.
  
  define variable lSuccess as logical   no-undo.
  define variable cStatus  as character no-undo.
  
  run value("SetAgentStatus" + pStat) in hFileDataSrv (tAgentID:screen-value in frame {&frame-name}, output lSuccess).
  if lSuccess
   then
    do:
      if agent.orgID ne ""
       then
        publish "roleComplianceModified" (input agent.orgID).
     
      if cAgentStatus eq "A" and pStat ne "A" then
        message "The status of the agent is changed from Active to " +  agentStat(pStat) + ". Compliance will not be tracked."
          view-as alert-box information buttons ok.

      else if cAgentStatus ne "A" and pStat = "A" then
        message "The status of the agent is changed from " + agentStat(cAgentStatus) + " to Active. Compliance will be tracked."
          view-as alert-box information buttons ok.
      
      
      cAgentStatus = pStat.                

      run ShowWindow in this-procedure.
    end.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetFields C-Win 
PROCEDURE SetFields :
/*------------------------------------------------------------------------------
@description Sets the fields
------------------------------------------------------------------------------*/
  define variable hQuery          as handle    no-undo.
  define variable hBuffer         as handle    no-undo.
  define variable chvendorversion as character no-undo.


  empty temp-table tempagent.

  do with frame {&frame-name}:
    /* used to turn null values into blanks */
    create buffer hBuffer for table "agent".
    create query hQuery.
    hQuery:set-buffers(hBuffer).
    hQuery:query-prepare("for each agent").
    hQuery:query-open().
    hQuery:get-first().
    repeat while not hQuery:query-off-end:
      do std-in = 1 to hBuffer:num-fields:
        std-ha = hBuffer:buffer-field(std-in).
       
        if (std-ha:buffer-value() = "?" or std-ha:buffer-value() = ?) then 
          case std-ha:data-type:
            when "character" then std-ha:buffer-value() = "".
            when "decimal"   then std-ha:buffer-value() = 0.
        end.
      end.
      hQuery:get-next().
    end.

    hQuery:query-close().
    delete object hQuery no-error.
    /* used to turn null values into blanks */
     
    for each agent:
      create tempagent.
      buffer-copy agent to tempagent.
    end.

    if can-find(first agent) then
    do:
      for first agent no-lock:
        assign
          /* agent and address */
          tAgentID:screen-value           = if ipcAction = "C" then "" else agent.agentID 
          tName:screen-value              = agent.name
          tLegalName:screen-value         = agent.legalname
          tAddr1:screen-value             = agent.addr1
          tAddr2:screen-value             = agent.addr2
          tCity:screen-value              = agent.city
          tState:screen-value             = agent.state
          cbRegion:screen-value           = agent.region
          tZip:screen-value               = agent.zip
          /* status, phone, fax, email, website, software, and version */
          tPhone:screen-value             = agent.phone
          tFax:screen-value               = agent.fax
          tEmail:screen-value             = agent.email
          tWebsite:screen-value           = agent.website
          fDomain:screen-value            = agent.domain
          /*tSwVersion:screen-value       = agent.swversion*/
          tStat:screen-value              = if ipcAction = "C" then agentStat("P") else agentStat(agent.stat)
          /* Agent Agreement */          
          tStateOper:screen-value         = (if lookup(agent.stateID,tStateOper:list-item-pairs) > 0 then agent.stateID else "")
          /*tContractID:screen-value        = agent.contractID*/
          tContractDate:screen-value      = string(agent.contractDate)
          tRemitType:screen-value         = agent.remitType
          tRemitValue:screen-value        = string(agent.remitValue)
          tRemitAlert:screen-value        = string(agent.remitAlert)
          tLiabilityLimit:screen-value    = string(agent.liabilityLimit)
          tPolicyLimit:screen-value       = string(agent.policyLimit)
          /* Activity */
          tProspectDate:screen-value      = string(agent.prospectDate)
          tClosedDate:screen-value        = string(agent.closedDate)
          tActiveDate:screen-value        = string(agent.activeDate)
          tCancelDate:screen-value        = string(agent.cancelDate)
          tReviewDate:screen-value        = string(agent.reviewDate)
          tWithdrawnDate:screen-value     = string(agent.withdrawndate)
          /* Mailing Address */
          mName:screen-value               = agent.mName
          mAddr1:screen-value              = agent.mAddr1
          mAddr2:screen-value              = agent.mAddr2
          mCity:screen-value               = agent.mCity
          mState:screen-value              = agent.mState
          mZip:screen-value                = agent.mZip
          /* Other */
          tAltaUID:screen-value            = agent.altaUID
          tStateUID:screen-value           = agent.stateUID
          tCorporation:screen-value        = agent.corporationID
          fNPN:screen-value                = agent.NPN
          fManager:screen-value            = "None"
          cMethod                          = agent.invoiceDue
          iDueInDays                       = agent.dueInDays
          iDueOnDay                        = agent.dueOnDays
          cCashAcc                         = agent.ARCashGLRef
          cCashAccDesc                     = agent.ARCashGLDesc
          cARAcc                           = agent.ARGLRef
          cARAccDesc                       = agent.ARGLDesc
          cRefundAcc                       = agent.ARRefundGLRef                      
          cRefundAccDesc                   = agent.ARRefundGLDesc     
          cWroffARAcc                      = agent.ARWriteOffGLRef                      
          cWroffARAccDesc                  = agent.ARWriteOffGLDesc
          .
          
        if ipcAction = "E" 
         then
          if agent.orgID > "0" 
           then
            do:
              cOldOrgID = agent.orgID.             
              tOrganization:screen-value = getOrgName(agent.orgID).
              tOrganization:private-data = string(agent.orgID).
            end.
           else
            do:
              tOrganization:screen-value = "".
              tOrganization:private-data = "".
            end.
   
                          
        if agent.swVendor <> "" then
          chvendorversion = agent.swVendor.

        if lookup(chvendorversion, cbSwVendor:list-item-pairs) = 0 then
        do:
          if chvendorversion = "" and lookup("none", cbSwVendor:list-item-pairs) <> 0 then
            cbSwVendor:screen-value = entry(lookup("none", cbSwVendor:list-item-pairs) + 1, cbSwVendor:list-item-pairs).
          else if chvendorversion = "" then
          do:
            cbSwVendor:add-last("None", "NONE").
            cbSwVendor:screen-value = "NONE".
          end.
          else 
          do:
            cbSwVendor:add-last(chvendorversion, chvendorversion).
            cbSwVendor:screen-value = chvendorversion.
          end.
        end.
        else
        do: 
          cbSwVendor:screen-value = chvendorversion no-error.
          
          if cbSwVendor:screen-value = ? 
           then
            cbSwVendor:screen-value = entry(lookup(chvendorversion, cbSwVendor:list-item-pairs) + 1, cbSwVendor:list-item-pairs).
        end.

        if lookup(agent.contractID, tContractID:list-item-pairs) = 0 
         then
          do:
            if agent.contractID = "" 
             then
              do:            
                tContractID:list-item-pairs = "," + "," + tContractID:list-item-pairs.
                tContractID:screen-value = "".
              end.
            else 
             do:          
               tContractID:add-last(agent.contractID, agent.contractID).
               tContractID:screen-value = agent.contractID.
             end.
          end.
        else
         do:
           tContractID:screen-value = agent.contractID no-error.
          
           if tContractID:screen-value = ? 
            then
             tContractID:screen-value = entry(lookup(agent.contractID, tContractID:list-item-pairs) + 1, tContractID:list-item-pairs).            
         end.

        if ipcAction = "E"
         then
          do:
            cManagerUID = "".
            for first agentmanager no-lock
                where agentmanager.isPrimary = true:
            
              assign
                std-ch = ""
                cManagerUID = agentmanager.uid
                .
              publish "GetSysUserName" (cManagerUID, output std-ch).
              do with frame {&frame-name}:
                assign fManager:screen-value  = std-ch.
              end.
            end.
          end.
        
        /* Handling all the errors that might come while storing the image in lImage variable. */
        if lImage <> ""  and 
           lImage <> "?" and 
           lImage <> ?   and 
           lImage <> "!ISO8859-1!?" 
         then
          do:
            decdmptr = base64-decode(lImage).
            copy-lob from decdmptr to file chPath.
            run setLogo.
            os-delete value(chPath).
          end.
      end.
    end.
  end.
  
  if tEmail:input-value = "" 
   then
    bEmail:sensitive = false.
   else
    bEmail:sensitive = true.
  if tWebsite:input-value = "" 
   then
    BtnUrl:sensitive = false.
   else
    BtnUrl:sensitive = true.
    
  doModify(ipcAction = "C").
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setLogo C-Win 
PROCEDURE setLogo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iImgheight as integer no-undo.
  define variable iImgwidth  as integer no-undo.

  if valid-handle(IMAGE-2) then
    delete object IMAGE-2.

  create image IMAGE-2.
  IMAGE-2:frame = frame FRAME-A:handle.
  IMAGE-2:load-image(chPath) no-error.
  image-2:transparent = true.

  assign iImgheight = IMAGE-2:width-pixel
         iImgwidth  = IMAGE-2:height-pixel.
  
  IMAGE-2:load-image("") no-error.

  if iImgheight > frame FRAME-A:width-pixel then
    IMAGE-2:width-pixel = frame FRAME-A:width-pixel - 5.
  else
    image-2:x = image-2:frame-x + ( frame FRAME-A:width-pixels / 2 ) - ( image-2:width-pixels / 2 ) - 2 no-error.
  
  if iImgwidth > frame FRAME-A:height-pixel then
    IMAGE-2:height-pixel = frame FRAME-A:height-pixel - 5.
  else
    image-2:y = image-2:frame-y + ( frame FRAME-A:height-pixels / 2 ) - ( image-2:height-pixels / 2 ) - 2 no-error.

  IMAGE-2:load-image(chPath) no-error.

  assign IMAGE-2:stretch-to-fit = true
         IMAGE-2:resizable      = true
         IMAGE-2:hidden         = false no-error.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetManager C-Win 
PROCEDURE SetManager :
/*------------------------------------------------------------------------------
@description Sets the primary manager
------------------------------------------------------------------------------*/
  define input  parameter table for agentmanager.
  define output parameter pSuccess  as logical   no-undo.
  
  cManagerUID = "".
  for first agentmanager no-lock
      where agentmanager.isPrimary = true:
  
    assign
      std-ch = ""
      cManagerUID = agentmanager.uid
      .
    publish "GetSysUserName" (cManagerUID, output std-ch).
    do with frame {&frame-name}:
      assign fManager:screen-value = std-ch.
    end.
  end.
  
  if ipcAction = "E" 
   then
    do:
      run ModifyAgentManager in hFileDataSrv (table agentmanager,input true,output pSuccess).
      doModify(true).
      if cManagerUID = ""
       then fManager:screen-value  = "None".
    end.
   else bSave:sensitive = true.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
@description Shows the window
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign frame {&frame-name}:width-pixels                   = {&window-name}:width-pixels      - 10
         frame {&frame-name}:virtual-width-pixels           = {&window-name}:width-pixels      - 10
         frame {&frame-name}:height-pixels                  = {&window-name}:height-pixels     - 2
         frame {&frame-name}:virtual-height-pixels          = {&window-name}:height-pixels     - 2
         .

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION agentStat C-Win 
FUNCTION agentStat RETURNS CHARACTER
  ( input cStat as character /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable pReturn as character no-undo.
  
  publish "GetSysPropDesc" ("AMD", "Agent", "Status", cStat, output pReturn).
  return pReturn.
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doModify C-Win 
FUNCTION doModify RETURNS LOGICAL
  ( input pModify as logical ) :
/*------------------------------------------------------------------------------
@description Disables/Enables the save buttons
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign 
      bSave:sensitive   = pModify
      bCancel:sensitive = pModify.
  end.
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getauth C-Win 
FUNCTION getauth returns character ( cAuth as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cAuth = {&ThirdParty} then
    return {&Third-Party}.   /* Function return value. */
  else if cAuth = {&FirstParty} then
    return {&First-Party}.
  else
    return cAuth.

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getOrgName C-Win 
FUNCTION getOrgName RETURNS CHARACTER
  ( ipcOrgID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable opcName as character no-undo.
  
  publish "getOrganizationName" (input  ipcOrgID,
                                 output opcName,
                                 output std-lo).     

  if std-lo 
   then 
    return opcName.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

