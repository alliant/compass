&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI ADM2
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWin
{adecomm/appserv.i}
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/* wagentgroup.w
@desc Window to view consolidated agents
@author Sachin Chaturvedi
@Modified
*/

CREATE WIDGET-POOL.

{tt/agent.i &tableAlias=cAgent}
{tt/consolidate.i &tableAlias=tConsolidate}
{tt/consolidate.i &tableAlias=ttSelectedConsolidate}

define input parameter table for tConsolidate.
define input parameter table for cAgent.

{src/adm2/widgetprto.i}

{lib/std-def.i}
{lib/amd-def.i} 
{lib/open-window.i} /* Contain function: openWindowForAgent */
{lib/winlaunch.i}
{lib/brw-multi-def.i}
{lib/brw-totalData-def.i}
{lib/set-button-def.i &noEnable=true}

define variable activePageNumber as integer no-undo.

define variable lAgentDetail as logical init no no-undo.
define variable lBatches     as logical init no no-undo.
define variable lAr          as logical init no no-undo.
define variable lClaim       as logical init no no-undo.
define variable lAudit       as logical init no no-undo.
define variable lCompliance  as logical init no no-undo.
define variable lAlert       as logical init no no-undo.
define variable lActivity    as logical init no no-undo.

define variable pStateID          as character no-undo.
define variable pYearOfSign       as integer   no-undo.
define variable pSoftware         as character no-undo.
define variable pCompany          as character no-undo.
define variable pOrganization     as character no-undo.
define variable pManager          as character no-undo. 
define variable pTagList          as character no-undo.
define variable pAffiliationList  as character no-undo.

/* Temp-table definitions*/
{tt/claimcostssummary.i}
{tt/claimcostssummary.i &tableAlias=tclaimcostssummary}

{tt/batch.i}
{tt/batch.i &tableAlias=tBatch} 
{tt/reqfulfill.i}
{tt/reqfulfill.i &tableAlias=treqfulfill}

{tt/alert.i}
{tt/alert.i &tableAlias="ttAlert"}

{tt/qaraudit.i}
{tt/action.i}
{tt/finding.i}
{tt/action.i &tableAlias="tempaction"}
{tt/finding.i &tableAlias="tempfinding"}
{tt/action.i &tableAlias="taction"}
{tt/finding.i &tableAlias="tfinding"}
{tt/reportclaimsummary.i &tableAlias="agentClaim"}
{tt/reportclaimsummary.i &tableAlias="tagentClaim"}
{tt/araging.i}
{tt/araging.i &tableAlias="ttArAging"}
{tt/araging.i &tableAlias="tArAging"}
{tt/araging.i &tableAlias="tArAgingSummTotal"}
{tt/qaraudit.i &tableAlias="taudit"}
{tt/qaraudit.i &tableAlias="ttaudit"}

{tt/staterequirement.i}
{tt/staterequirement.i &tableAlias="tStateRequirement"}

{tt/statereqqual.i}

{tt/qualification.i}
{tt/qualification.i &tableAlias="tqualification"}

{tt/activity.i &tableAlias=ttactivity}
{tt/activity.i &tableAlias=ttactivityMetrics}

define temp-table activity like ttactivity
  field showLevel as integer   {&nodeType}
  field showID    as character {&nodeType} column-label "Agent"      format "x(500)"
  field showName  as character {&nodeType} column-label "Agent Name" format "x(500)"
  field seq       as integer   {&nodeType}
  .
define temp-table tempActivity like activity.
define temp-table tActivity like activity.
define temp-table tCActivity like activity.  
define temp-table tMetricsActivity like activity.
define temp-table activityMetrics like activity.
  
define temp-table openQars
 field qarid     as integer
 field hInstance as handle. 

define variable lRefreshClaims          as logical   no-undo.
define variable lRefreshReqFulFillment  as logical   no-undo.
define variable lRefreshQualifications  as logical   no-undo.
define variable iCount         as integer no-undo.
define variable cCurrUser      as character no-undo.
define variable cCurrentUID    as character no-undo.

define variable cAppCode          as character no-undo.
define variable cLastAlertCode    as character no-undo.
define variable cLastAlertOwner   as character no-undo.
define variable cLastAgentID      as character no-undo.
define variable cLastRemitClicked as character no-undo.

define variable cFormat         as character no-undo.
define variable cFormat1        as character no-undo.
define variable cFormat2        as character no-undo.

define variable iMonth          as integer   no-undo.
define variable hColumn         as handle    no-undo.

define variable iCreateComparision    as integer   no-undo.
define variable cArAgentEmail         as character no-undo.
define variable cLastAgingClicked     as character no-undo.
define variable lRefreshClaimsSummary as logical   no-undo. 
define variable lRefresh              as logical   no-undo.
define variable lRefreshAR            as logical   no-undo.
define variable lArAging              as logical   no-undo.
define variable lRefreshAlerts        as logical   no-undo.
define variable lBatch                as logical   no-undo.
define variable lClaimcostssummary    as logical   no-undo.
define variable lEnableCbAgents       as logical   no-undo.
define variable iLastPage             as integer   no-undo.
define variable cAgentList            as character no-undo.
define variable cAgentLIPairs         as character no-undo.
define variable cDispName             as character no-undo.

define variable iNumAgentRecords      as integer   no-undo.
define variable iActiveAgent          as integer   no-undo.
define variable iCancelledAgent       as integer   no-undo.
define variable iClosedAgent          as integer   no-undo.
define variable iProspectAgent        as integer   no-undo.
define variable iWaivedAgent          as integer   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

&Scoped-define ADM-SUPPORTED-LINKS Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwAction

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES action tActivity tMetricsActivity alert ~
tArAging batch agentclaim cAgent audit qualification reqfulfill

/* Definitions for BROWSE brwAction                                     */
&Scoped-define FIELDS-IN-QUERY-brwAction action.sourceID action.dueDate action.ownerDesc action.comments   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAction   
&Scoped-define SELF-NAME brwAction
&Scoped-define QUERY-STRING-brwAction for each action
&Scoped-define OPEN-QUERY-brwAction open query {&SELF-NAME} for each action.
&Scoped-define TABLES-IN-QUERY-brwAction action
&Scoped-define FIRST-TABLE-IN-QUERY-brwAction action


/* Definitions for BROWSE brwActivityBatch                              */
&Scoped-define FIELDS-IN-QUERY-brwActivityBatch tActivity.typeDesc tActivity.month1 tActivity.month2 tActivity.month3 tActivity.month4 tActivity.month5 tActivity.month6 tActivity.month7 tActivity.month8 tActivity.month9 tActivity.month10 tActivity.month11 tActivity.month12 tActivity.total tActivity.yrTotal   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwActivityBatch   
&Scoped-define SELF-NAME brwActivityBatch
&Scoped-define QUERY-STRING-brwActivityBatch FOR each tActivity
&Scoped-define OPEN-QUERY-brwActivityBatch OPEN QUERY {&SELF-NAME} FOR each tActivity.
&Scoped-define TABLES-IN-QUERY-brwActivityBatch tActivity
&Scoped-define FIRST-TABLE-IN-QUERY-brwActivityBatch tActivity


/* Definitions for BROWSE brwActivityCPL                                */
&Scoped-define FIELDS-IN-QUERY-brwActivityCPL tMetricsActivity.year tMetricsActivity.typeDesc tMetricsActivity.month1 tMetricsActivity.month2 tMetricsActivity.month3 tMetricsActivity.month4 tMetricsActivity.month5 tMetricsActivity.month6 tMetricsActivity.month7 tMetricsActivity.month8 tMetricsActivity.month9 tMetricsActivity.month10 tMetricsActivity.month11 tMetricsActivity.month12 tMetricsActivity.Total tMetricsActivity.yrTotal   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwActivityCPL   
&Scoped-define SELF-NAME brwActivityCPL
&Scoped-define QUERY-STRING-brwActivityCPL FOR EACH tMetricsActivity
&Scoped-define OPEN-QUERY-brwActivityCPL OPEN QUERY {&SELF-NAME} FOR EACH tMetricsActivity.
&Scoped-define TABLES-IN-QUERY-brwActivityCPL tMetricsActivity
&Scoped-define FIRST-TABLE-IN-QUERY-brwActivityCPL tMetricsActivity


/* Definitions for BROWSE brwActivityPolicies                           */
&Scoped-define FIELDS-IN-QUERY-brwActivityPolicies tMetricsActivity.year tMetricsActivity.typeDesc tMetricsActivity.month1 tMetricsActivity.month2 tMetricsActivity.month3 tMetricsActivity.month4 tMetricsActivity.month5 tMetricsActivity.month6 tMetricsActivity.month7 tMetricsActivity.month8 tMetricsActivity.month9 tMetricsActivity.month10 tMetricsActivity.month11 tMetricsActivity.month12 tMetricsActivity.Total tMetricsActivity.yrTotal   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwActivityPolicies   
&Scoped-define SELF-NAME brwActivityPolicies
&Scoped-define QUERY-STRING-brwActivityPolicies FOR EACH tMetricsActivity
&Scoped-define OPEN-QUERY-brwActivityPolicies OPEN QUERY {&SELF-NAME} FOR EACH tMetricsActivity.
&Scoped-define TABLES-IN-QUERY-brwActivityPolicies tMetricsActivity
&Scoped-define FIRST-TABLE-IN-QUERY-brwActivityPolicies tMetricsActivity


/* Definitions for BROWSE brwAlerts                                     */
&Scoped-define FIELDS-IN-QUERY-brwAlerts alert.dateCreated "Created" alert.agentID "Agent ID" alert.processCodeDesc "Alert Type" alert.thresholdRange "Threshold" alert.scoreDesc alert.severityDesc "Severity" alert.ownerDesc alert.description "Description"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAlerts   
&Scoped-define SELF-NAME brwAlerts
&Scoped-define QUERY-STRING-brwAlerts for each alert
&Scoped-define OPEN-QUERY-brwAlerts open query {&SELF-NAME} for each alert.
&Scoped-define TABLES-IN-QUERY-brwAlerts alert
&Scoped-define FIRST-TABLE-IN-QUERY-brwAlerts alert


/* Definitions for BROWSE brwArAging                                    */
&Scoped-define FIELDS-IN-QUERY-brwArAging tArAging.fileNumber tArAging.agentID tArAging.due0_30 tArAging.due31_60 tArAging.due61_90 tArAging.due91_ tArAging.balance   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwArAging   
&Scoped-define SELF-NAME brwArAging
&Scoped-define QUERY-STRING-brwArAging for each tArAging
&Scoped-define OPEN-QUERY-brwArAging open query {&SELF-NAME} for each tArAging.
&Scoped-define TABLES-IN-QUERY-brwArAging tArAging
&Scoped-define FIRST-TABLE-IN-QUERY-brwArAging tArAging


/* Definitions for BROWSE brwBatch                                      */
&Scoped-define FIELDS-IN-QUERY-brwBatch batch.agentID batch.receivedDate batch.invoiceDate batch.batchID "Batch ID" batch.reference batch.fileCount batch.policyCount batch.endorsementCount batch.netPremiumReported batch.netPremiumDelta getBatchStatus(batch.stat) @ batch.stat   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwBatch   
&Scoped-define SELF-NAME brwBatch
&Scoped-define QUERY-STRING-brwBatch FOR EACH batch
&Scoped-define OPEN-QUERY-brwBatch OPEN QUERY {&SELF-NAME} FOR EACH batch.
&Scoped-define TABLES-IN-QUERY-brwBatch batch
&Scoped-define FIRST-TABLE-IN-QUERY-brwBatch batch


/* Definitions for BROWSE brwClaims                                     */
&Scoped-define FIELDS-IN-QUERY-brwClaims agentclaim.agentID agentclaim.claimID agentclaim.statDesc agentclaim.agentErrorDesc agentclaim.assignedToDesc agentclaim.dateOpened agentclaim.laeReserve agentclaim.laeLTD agentclaim.lossReserve agentclaim.lossLTD agentclaim.recoveries agentclaim.costsPaid   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwClaims   
&Scoped-define SELF-NAME brwClaims
&Scoped-define QUERY-STRING-brwClaims FOR EACH agentclaim by agentclaim.agentID by agentclaim.claimID
&Scoped-define OPEN-QUERY-brwClaims OPEN QUERY {&SELF-NAME} FOR EACH agentclaim by agentclaim.agentID by agentclaim.claimID.
&Scoped-define TABLES-IN-QUERY-brwClaims agentclaim
&Scoped-define FIRST-TABLE-IN-QUERY-brwClaims agentclaim


/* Definitions for BROWSE brwConsolidate                                */
&Scoped-define FIELDS-IN-QUERY-brwConsolidate cAgent.agentID agentstat(cAgent.stat) cAgent.name cAgent.address cAgent.stateID getManagerName(cAgent.manager)   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwConsolidate   
&Scoped-define SELF-NAME brwConsolidate
&Scoped-define QUERY-STRING-brwConsolidate FOR EACH cAgent
&Scoped-define OPEN-QUERY-brwConsolidate OPEN QUERY {&SELF-NAME} FOR EACH cAgent.
&Scoped-define TABLES-IN-QUERY-brwConsolidate cAgent
&Scoped-define FIRST-TABLE-IN-QUERY-brwConsolidate cAgent


/* Definitions for BROWSE brwQAR                                        */
&Scoped-define FIELDS-IN-QUERY-brwQAR audit.agentID audit.stat audit.audittype audit.cQarID audit.errtype audit.auditStartDate audit.auditFinishDate audit.score audit.grade audit.auditor audit.comments   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQAR   
&Scoped-define SELF-NAME brwQAR
&Scoped-define QUERY-STRING-brwQAR for each audit no-lock by audit.auditStartDate desc
&Scoped-define OPEN-QUERY-brwQAR open query {&SELF-NAME} for each audit no-lock by audit.auditStartDate desc .
&Scoped-define TABLES-IN-QUERY-brwQAR audit
&Scoped-define FIRST-TABLE-IN-QUERY-brwQAR audit


/* Definitions for BROWSE brwQualification                              */
&Scoped-define FIELDS-IN-QUERY-brwQualification qualification.activ qualification.entityID qualification.stateID qualification.Qualification qualification.QualificationNumber qualification.stat qualification.effectiveDate qualification.expirationDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQualification   
&Scoped-define SELF-NAME brwQualification
&Scoped-define QUERY-STRING-brwQualification for each qualification  by Qualification.qualificationId
&Scoped-define OPEN-QUERY-brwQualification open query brwQualification for each qualification  by Qualification.qualificationId.
&Scoped-define TABLES-IN-QUERY-brwQualification qualification
&Scoped-define FIRST-TABLE-IN-QUERY-brwQualification qualification


/* Definitions for BROWSE brwReqFul                                     */
&Scoped-define FIELDS-IN-QUERY-brwReqFul reqfulfill.reqMet reqfulfill.reqdesc "Requirement" reqfulfill.entityID "Agent ID" reqfulfill.qualification "Qualification" reqfulfill.stat "Status" reqfulfill.appliesTo "Fulfilled By" reqfulfill.entityname "Name"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwReqFul   
&Scoped-define SELF-NAME brwReqFul
&Scoped-define QUERY-STRING-brwReqFul for each reqfulfill
&Scoped-define OPEN-QUERY-brwReqFul open query {&SELF-NAME} for each reqfulfill.
&Scoped-define TABLES-IN-QUERY-brwReqFul reqfulfill
&Scoped-define FIRST-TABLE-IN-QUERY-brwReqFul reqfulfill


/* Definitions for FRAME frActivity                                     */

/* Definitions for FRAME frAlert                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-frAlert ~
    ~{&OPEN-QUERY-brwAlerts}

/* Definitions for FRAME frAr                                           */
&Scoped-define OPEN-BROWSERS-IN-QUERY-frAr ~
    ~{&OPEN-QUERY-brwArAging}

/* Definitions for FRAME frAudit                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-frAudit ~
    ~{&OPEN-QUERY-brwAction}~
    ~{&OPEN-QUERY-brwQAR}

/* Definitions for FRAME frBatch                                        */

/* Definitions for FRAME frClaim                                        */

/* Definitions for FRAME frCompliance                                   */
&Scoped-define OPEN-BROWSERS-IN-QUERY-frCompliance ~
    ~{&OPEN-QUERY-brwQualification}~
    ~{&OPEN-QUERY-brwReqFul}

/* Definitions for FRAME frDetail                                       */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bSaveConsolidation RECT-68 eParams eResults ~
bViewParams bRefresh 
&Scoped-Define DISPLAYED-OBJECTS eParams eResults 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD agentStat wWin 
FUNCTION agentStat RETURNS CHARACTER
  ( input cStat as character /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD checkActivity wWin 
FUNCTION checkActivity RETURNS CHARACTER
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD checkActivityMetrics wWin 
FUNCTION checkActivityMetrics RETURNS CHARACTER
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createActivity wWin 
FUNCTION createActivity RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createActivityMetrics wWin 
FUNCTION createActivityMetrics RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createActivityRow wWin 
FUNCTION createActivityRow RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer,
    input table for tempActivity )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createActivityRowMetrics wWin 
FUNCTION createActivityRowMetrics RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer,
    input table for tempActivity )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayAgents wWin 
FUNCTION displayAgents RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayAR wWin 
FUNCTION displayAR RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayClaim wWin 
FUNCTION displayClaim RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayHeader wWin 
FUNCTION displayHeader RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getActivityComparedData wWin 
FUNCTION getActivityComparedData RETURNS LOGICAL
  (input ipiYear as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getActivityComparedDataMetrics wWin 
FUNCTION getActivityComparedDataMetrics RETURNS LOGICAL
  (input ipiYear as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getActivityTotal wWin 
FUNCTION getActivityTotal RETURNS DECIMAL
  ( input table for tempactivity,
    input pType as character  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getActivityType wWin 
FUNCTION getActivityType RETURNS CHARACTER
  ( input ipcType as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getAuditStatus wWin 
FUNCTION getAuditStatus RETURNS CHARACTER
  ( input ipcStatus as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getAuditType wWin 
FUNCTION getAuditType RETURNS CHARACTER
  ( input ipcAuditType as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getBatchStatus wWin 
FUNCTION getBatchStatus RETURNS CHARACTER
  ( input ipcStatus as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getCategoryDesc wWin 
FUNCTION getCategoryDesc RETURNS CHARACTER
  ( input ipcCategory as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getErrorType wWin 
FUNCTION getErrorType RETURNS CHARACTER
  ( input ipcErrType as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getManagerName wWin 
FUNCTION getManagerName RETURNS CHARACTER
  ( input cManagerUID as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getMonthName wWin 
FUNCTION getMonthName RETURNS CHARACTER
  (INPUT iMonth AS INTEGER)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getPeriodID wWin 
FUNCTION getPeriodID RETURNS INTEGER
  ( input ipiMonth as integer,
    input ipiYear as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getQualificationDesc wWin 
FUNCTION getQualificationDesc RETURNS CHARACTER
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openActivity wWin 
FUNCTION openActivity RETURNS LOGICAL
  ( input ipiYear         as integer,
    input ipcCategoryList as character,
    input iplRefresh      as logical)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openActivityMetrics wWin 
FUNCTION openActivityMetrics RETURNS LOGICAL
  ( input ipiYear         as integer,
    input ipcCategoryList as character,
    input iplRefresh      as logical)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openAlerts wWin 
FUNCTION openAlerts RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openAr wWin 
FUNCTION openAr RETURNS LOGICAL
  (input iplRefresh as logical)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openAudit wWin 
FUNCTION openAudit RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openBatchesYearly wWin 
FUNCTION openBatchesYearly RETURNS LOGICAL
  (input ipcYear as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openclaim wWin 
FUNCTION openclaim RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openQualification wWin 
FUNCTION openQualification RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openReqFulFillment wWin 
FUNCTION openReqFulFillment RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openURL wWin 
FUNCTION openURL RETURNS LOGICAL PRIVATE
  ( input pURL as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setPageState wWin 
FUNCTION setPageState RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int, input pState as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwQAR 
       MENU-ITEM m_Mail_auditor LABEL "Mail auditor"  .

DEFINE MENU POPUP-MENU-brwQAR-2 
       MENU-ITEM m_Mail_Action_Owner LABEL "Mail Action Owner".


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_folder AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh All".

DEFINE BUTTON bSaveConsolidation  NO-FOCUS
     LABEL "Save Consolidation" 
     SIZE 7.2 BY 1.71 TOOLTIP "Save Consolidation Parameters".

DEFINE BUTTON bViewParams  NO-FOCUS
     LABEL "View Params" 
     SIZE 7.2 BY 1.71 TOOLTIP "View Parameters".

DEFINE VARIABLE eParams AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 73.8 BY 2.19 NO-UNDO.

DEFINE VARIABLE eResults AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 53 BY 2.19 NO-UNDO.

DEFINE RECTANGLE RECT-68
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 25.2 BY 2.67.

DEFINE BUTTON bActivityExp  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bActivityExpCPL  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bActivityMetricsRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh data".

DEFINE BUTTON bActivityMetricsRefreshCPL  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh data".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bRefreshAlert  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh Alerts".

DEFINE VARIABLE cbAgentsAlert AS CHARACTER FORMAT "X(256)" 
     LABEL "Agents" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 50.2 BY 1 TOOLTIP "Agents" NO-UNDO.

DEFINE VARIABLE fCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Alert Type" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 44 BY 1 NO-UNDO.

DEFINE VARIABLE fOwner AS CHARACTER FORMAT "X(256)":U 
     LABEL "Owner" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 44 BY 1 NO-UNDO.

DEFINE BUTTON b30to60  NO-FOCUS
     LABEL "" 
     SIZE 23.6 BY 1.14.

DEFINE BUTTON b60to90  NO-FOCUS
     LABEL "" 
     SIZE 23.6 BY 1.14.

DEFINE BUTTON bbalance  NO-FOCUS
     LABEL "" 
     SIZE 23.6 BY 1.14.

DEFINE BUTTON bCurrentaging  NO-FOCUS
     LABEL "" 
     SIZE 23.6 BY 1.14.

DEFINE BUTTON bover90  NO-FOCUS
     LABEL "" 
     SIZE 23.6 BY 1.14.

DEFINE BUTTON btArMail AUTO-GO  NO-FOCUS
     LABEL "Mail" 
     SIZE 4.6 BY 1.14 TOOLTIP "Mail".

DEFINE BUTTON btFileExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export".

DEFINE BUTTON btFileOpen  NO-FOCUS
     LABEL "Open" 
     SIZE 4.6 BY 1.14 TOOLTIP "View Transaction Details".

DEFINE BUTTON btRefreshAr  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh data".

DEFINE VARIABLE cbAgentsAR AS CHARACTER FORMAT "X(256)" 
     LABEL "Agents" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 50.2 BY 1 TOOLTIP "Agents" NO-UNDO.

DEFINE BUTTON btActionExport AUTO-GO  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export".

DEFINE BUTTON btActionMail AUTO-GO  NO-FOCUS
     LABEL "Mail" 
     SIZE 4.6 BY 1.14 TOOLTIP "Mail".

DEFINE BUTTON btActionOpen AUTO-GO  NO-FOCUS
     LABEL "Open" 
     SIZE 4.6 BY 1.14 TOOLTIP "Open".

DEFINE BUTTON btActionRefresh AUTO-GO  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh".

DEFINE BUTTON btAuditExport AUTO-GO  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export".

DEFINE BUTTON btAuditMail AUTO-GO  NO-FOCUS
     LABEL "Mail" 
     SIZE 4.6 BY 1.14 TOOLTIP "Mail".

DEFINE BUTTON btAuditOpen AUTO-GO  NO-FOCUS
     LABEL "Open" 
     SIZE 4.6 BY 1.14 TOOLTIP "Open".

DEFINE BUTTON btAuditRefresh AUTO-GO  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh".

DEFINE VARIABLE cbAgentsAudit AS CHARACTER FORMAT "X(256)" 
     LABEL "Agents" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 50.2 BY 1 TOOLTIP "Agents" NO-UNDO.

DEFINE BUTTON bActivityBatchExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bActivityBatchRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh data".

DEFINE BUTTON bBatchExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bBatchOpen  NO-FOCUS
     LABEL "Open" 
     SIZE 4.6 BY 1.14 TOOLTIP "Open batch detail".

DEFINE BUTTON bBatchRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh data".

DEFINE BUTTON bYear1  NO-FOCUS
     LABEL "Year1" 
     SIZE 9.6 BY 1 TOOLTIP "Refresh batch details for the year".

DEFINE BUTTON bYear2  NO-FOCUS
     LABEL "Year2" 
     SIZE 9.6 BY 1 TOOLTIP "Refresh batch details for the year".

DEFINE BUTTON bYear3  NO-FOCUS
     LABEL "Year3" 
     SIZE 9.6 BY 1 TOOLTIP "Refresh batch details for the year".

DEFINE VARIABLE cbAgents AS CHARACTER FORMAT "X(256)" 
     LABEL "Agents" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 50.2 BY 1 TOOLTIP "Agents" NO-UNDO.

DEFINE BUTTON btClaimExport AUTO-GO  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export".

DEFINE BUTTON btClaimMail AUTO-GO  NO-FOCUS
     LABEL "Mail" 
     SIZE 4.6 BY 1.14 TOOLTIP "Mail".

DEFINE BUTTON btClaimOpen AUTO-GO  NO-FOCUS
     LABEL "Open" 
     SIZE 4.6 BY 1.14 TOOLTIP "Open".

DEFINE BUTTON btClaimRefresh AUTO-GO  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh".

DEFINE VARIABLE cbAgentsClm AS CHARACTER FORMAT "X(256)" 
     LABEL "Agents" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 50.2 BY 1 TOOLTIP "Agents" NO-UNDO.

DEFINE VARIABLE costfromLTD AS INTEGER FORMAT "$-z,zzz,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE costnetLTD AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 7.4 BY .62 NO-UNDO.

DEFINE VARIABLE errorfromLTD AS INTEGER FORMAT "$-z,zzz,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE errornetLTD AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 7.4 BY .62 NO-UNDO.

DEFINE VARIABLE fLAEL AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16.4 BY 1 NO-UNDO.

DEFINE VARIABLE fLAER AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16.4 BY 1 NO-UNDO.

DEFINE VARIABLE fLossL AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16.4 BY 1 NO-UNDO.

DEFINE VARIABLE fLossR AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16.4 BY 1 NO-UNDO.

DEFINE VARIABLE fRecoveriesR AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16.4 BY 1 NO-UNDO.

DEFINE VARIABLE fTotal AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 21.2 BY .62 NO-UNDO.

DEFINE VARIABLE fTotalCostL AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16.4 BY 1 NO-UNDO.

DEFINE VARIABLE netfromLTD AS INTEGER FORMAT "$-z,zzz,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE BUTTON bReqFulExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export to a CSV File".

DEFINE BUTTON btComQualExp  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export to a CSV File".

DEFINE BUTTON btComQualRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh data".

DEFINE BUTTON btComReqFulRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh data".

DEFINE BUTTON btComViewQual  NO-FOCUS
     LABEL "View Qualification" 
     SIZE 4.6 BY 1.14 TOOLTIP "View qualification".

DEFINE BUTTON btComViewReq  NO-FOCUS
     LABEL "View requirement" 
     SIZE 4.6 BY 1.14 TOOLTIP "View requirement".

DEFINE VARIABLE cbAgentsQual AS CHARACTER FORMAT "X(256)" 
     LABEL "Agents" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 50.2 BY 1 TOOLTIP "Agents" NO-UNDO.

DEFINE VARIABLE tShowInactiveQuals AS LOGICAL INITIAL yes 
     LABEL "Show inactive Qualifications" 
     VIEW-AS TOGGLE-BOX
     SIZE 31 BY .81 TOOLTIP "Show inactive Qualifications" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAction FOR 
      action SCROLLING.

DEFINE QUERY brwActivityBatch FOR 
      tActivity SCROLLING.

DEFINE QUERY brwActivityCPL FOR 
      tMetricsActivity SCROLLING.

DEFINE QUERY brwActivityPolicies FOR 
      tMetricsActivity SCROLLING.

DEFINE QUERY brwAlerts FOR 
      alert SCROLLING.

DEFINE QUERY brwArAging FOR 
      tArAging SCROLLING.

DEFINE QUERY brwBatch FOR 
      batch SCROLLING.

DEFINE QUERY brwClaims FOR 
      agentclaim SCROLLING.

DEFINE QUERY brwConsolidate FOR 
      cAgent SCROLLING.

DEFINE QUERY brwQAR FOR 
      audit SCROLLING.

DEFINE QUERY brwQualification FOR 
      qualification SCROLLING.

DEFINE QUERY brwReqFul FOR 
      reqfulfill SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAction wWin _FREEFORM
  QUERY brwAction DISPLAY
      action.sourceID label "Source ID"                   width 12    
action.dueDate label "Due"                   width 12
action.ownerDesc     label "Owner"  format "x(40)" width 30
action.comments      label "Action" format "x(350)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 191.8 BY 7.81
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Open Corrective Actions" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwActivityBatch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwActivityBatch wWin _FREEFORM
  QUERY brwActivityBatch DISPLAY
      tActivity.typeDesc width 13          
tActivity.month1 width 11
tActivity.month2 width 11
tActivity.month3 width 11
tActivity.month4 width 11
tActivity.month5 width 11
tActivity.month6 width 11
tActivity.month7 width 11
tActivity.month8 width 11
tActivity.month9 width 11
tActivity.month10 width 11
tActivity.month11 width 11
tActivity.month12 width 11
tActivity.total   width 15
tActivity.yrTotal label "YTD Jul" width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-SCROLLBAR-VERTICAL SIZE 188.6 BY 6.76
         TITLE "Net Premium" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwActivityCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwActivityCPL wWin _FREEFORM
  QUERY brwActivityCPL DISPLAY
      tMetricsActivity.year format "ZZZZ" width 7
tMetricsActivity.typeDesc width 14          
tMetricsActivity.month1 width 11
tMetricsActivity.month2 width 11
tMetricsActivity.month3 width 11
tMetricsActivity.month4 width 11
tMetricsActivity.month5 width 11
tMetricsActivity.month6 width 11
tMetricsActivity.month7 width 11
tMetricsActivity.month8 width 11
tMetricsActivity.month9 width 11
tMetricsActivity.month10 width 11
tMetricsActivity.month11 width 11
tMetricsActivity.month12 width 11
tMetricsActivity.Total width 14 label 'Total'
tMetricsActivity.yrTotal width 14 label 'YTD Total'
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-SCROLLBAR-VERTICAL SIZE 192.6 BY 6.76
         TITLE "Closing Protection Letters" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwActivityPolicies
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwActivityPolicies wWin _FREEFORM
  QUERY brwActivityPolicies DISPLAY
      tMetricsActivity.year format "ZZZZ" width 7
tMetricsActivity.typeDesc width 14          
tMetricsActivity.month1 width 11
tMetricsActivity.month2 width 11
tMetricsActivity.month3 width 11
tMetricsActivity.month4 width 11
tMetricsActivity.month5 width 11
tMetricsActivity.month6 width 11
tMetricsActivity.month7 width 11
tMetricsActivity.month8 width 11
tMetricsActivity.month9 width 11
tMetricsActivity.month10 width 11
tMetricsActivity.month11 width 11
tMetricsActivity.month12 width 11
tMetricsActivity.Total   width 14 label 'Total'
tMetricsActivity.yrTotal width 14 label 'YTD Total'
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-SCROLLBAR-VERTICAL SIZE 192.6 BY 6.76
         TITLE "Policies" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwAlerts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAlerts wWin _FREEFORM
  QUERY brwAlerts DISPLAY
      alert.dateCreated                    label        "Created"       format "99/99/9999"  width 15
      alert.agentID                        label        "Agent ID"      format "x(10)"       width 15 
alert.processCodeDesc                      label        "Alert Type"    format "x(100)"      width 27 
alert.thresholdRange                       label        "Threshold"     format "x(100)"      width 17
alert.scoreDesc                            column-label "Measure"       format "x(20)"       width 12
alert.severityDesc                         label        "Severity"      format "x(10)"       width 10
alert.ownerDesc                            column-label "Owner"         format "x(15)"       width 15
alert.description                          label        "Description"   format "x(100)"      width 54
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 192.8 BY 17.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwArAging
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwArAging wWin _FREEFORM
  QUERY brwArAging DISPLAY
      tArAging.fileNumber  label "File Number" format "x(30)"  width 30
      tArAging.agentID  label "Agent ID" format "x(10)"  width 10
tArAging.due0_30   label "Current"                      width 28
tArAging.due31_60  label "31 - 60 Days"                 width 28
tArAging.due61_90  label "61 - 90 Days"                 width 28
tArAging.due91_    label "Over 90 Days"                 width 28
tArAging.balance   label "Balance"                      width 28
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 189.8 BY 16.81
         TITLE "Select the Aging Category" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwBatch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwBatch wWin _FREEFORM
  QUERY brwBatch DISPLAY
      batch.agentID            column-label "Agent ID"        format "x(10)"           width 10    
     batch.receivedDate         column-label "Received"         format "99/99/9999"
 batch.invoiceDate          column-label "Completed"        format "99/99/9999"
 batch.batchID              label        "Batch ID"         format " >>>>>>>>>9  " 
 batch.reference            column-label "Reference"        format "x(63)"           width 53
 batch.fileCount            column-label "Files"            format "zzz,zzz"         width 11
 batch.policyCount          column-label "Policies"         format "zzz,zzz"         width 11
 batch.endorsementCount     column-label "Endorsements"     format "zzz,zzz"         width 15
 batch.netPremiumReported   column-label "Reported!Net"     format "$->>,>>>,>>9.99"
 batch.netPremiumDelta      column-label "Accounting!Net"   format "$->>,>>>,>>9.99"
 getBatchStatus(batch.stat) @ batch.stat column-label "Status"  format "x(11)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 193.2 BY 10.43
         TITLE "Batches" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwClaims
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwClaims wWin _FREEFORM
  QUERY brwClaims DISPLAY
      agentclaim.agentID column-label "Agent ID" width 10      
agentclaim.claimID column-label "Claim ID" format "99999999"  width 12
agentclaim.statDesc column-label "Status" format "x(10)" width 9
agentclaim.agentErrorDesc COLUMN-LABEL "Agent!Error" FORMAT "x(20)" WIDTH 12
agentclaim.assignedToDesc column-label "Assigned To" format "x(100)" width 24
agentclaim.dateOpened column-label "Opened" format "99/99/9999" width 13
agentclaim.laeReserve column-label "LAE!Reserve" format "(>>,>>>,>>>,>>Z)" width 16
agentclaim.laeLTD column-label "LAE!LTD" format "(>>,>>>,>>>,>>Z)" width 16
agentclaim.lossReserve column-label "Loss!Reserve" format "(>>,>>>,>>>,>>Z)" width 16
agentclaim.lossLTD column-label "Losses!LTD" format "(>>,>>>,>>>,>>Z)" width 16
agentclaim.recoveries column-label "Recoveries!Received" format "(>>,>>>,>>>,>>Z)" width 16

agentclaim.costsPaid column-label "Total Costs!LTD" format "(>>,>>>,>>>,>>Z)" width 16
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 191 BY 12.43
         TITLE "All Claims" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwConsolidate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwConsolidate wWin _FREEFORM
  QUERY brwConsolidate DISPLAY
      cAgent.agentID label "AgentID"    format "x(20)" width 12
      agentstat(cAgent.stat)  column-label "Status"      format "x(10)"    width 12
      cAgent.name label "Agent"      format "x(100)"   width 50
      cAgent.address label "Address"      format "x(100)"   width 65
      cAgent.stateID label "State ID"      format "x(2)"   width 12
      getManagerName(cAgent.manager) label "Manager"      format "x(50)"  width 20
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 188.6 BY 18.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwQAR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQAR wWin _FREEFORM
  QUERY brwQAR DISPLAY
      audit.agentID      label "Agent ID"            format "x(12)"    width 12    
      audit.stat      label "Status"            format "x(12)"     width 12      
 audit.audittype column-label "Audit!Type" format "x(12)"     width 10
 audit.cQarID column-label "Audit ID" format "x(12)"     width 10
 audit.errtype   label "Type"              format "x(18)"     width 18
audit.auditStartDate                            label "Start"             format "99/99/99"  width 12
audit.auditFinishDate                           label "Finish"            format "99/99/99"  width 12
audit.score                                     label "Points"            format "zzz "      width 8
audit.grade                                     label "Score%"            format "zzz "      width 9
audit.auditor                                   label "Auditor"           format "x(100)"    width 25
audit.comments                                  label "Comments"          format "x(150)"    width 30
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 191.8 BY 9.43
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Audits" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwQualification
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQualification wWin _FREEFORM
  QUERY brwQualification DISPLAY
      qualification.activ          column-label "Active"                                       width 10 view-as toggle-box 
      qualification.entityID          column-label "Organization ID"    format "x(10)"         width 18
qualification.stateID                     label "State ID"              format "x(5)"          width 10
qualification.Qualification               label "Qualification"         format "x(50)"         width 66
qualification.QualificationNumber         label "Number"                format "x(10)"         width 18
qualification.stat                        label "Status"                format "x(15)"         width 20
qualification.effectiveDate               label "Effective"             format "99/99/9999"    width 23
qualification.expirationDate              label "Expiration"            format "99/99/9999"    width 23
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 192 BY 6.81
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Qualifications" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwReqFul
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwReqFul wWin _FREEFORM
  QUERY brwReqFul DISPLAY
      reqfulfill.reqMet                                          column-label "Met"         width 6    view-as toggle-box
reqfulfill.reqdesc                                           label        "Requirement"   format "x(50)"      width 45
reqfulfill.entityID                                          label        "Agent ID"      format "X(10)"      width 10
reqfulfill.qualification                                     label        "Qualification" format "x(40)"      width 45
reqfulfill.stat                                              label        "Status"        format "X(16)"      width 15
reqfulfill.appliesTo                                         label        "Fulfilled By"  format "x(15)"      width 13
reqfulfill.entityname                                        label        "Name"          format "x(30)"      width 25
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 192 BY 9.81
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Requirements and Fulfillments" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bSaveConsolidation AT ROW 1.95 COL 19.6 WIDGET-ID 408 NO-TAB-STOP 
     eParams AT ROW 1.95 COL 28.2 NO-LABEL WIDGET-ID 404
     eResults AT ROW 1.95 COL 101.8 NO-LABEL WIDGET-ID 410
     bViewParams AT ROW 1.95 COL 12.2 WIDGET-ID 406 NO-TAB-STOP 
     bRefresh AT ROW 1.95 COL 4.8 WIDGET-ID 382 NO-TAB-STOP 
     "Results" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 1.24 COL 102.8 WIDGET-ID 412
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.24 COL 29.4 WIDGET-ID 402
     "Actions" VIEW-AS TEXT
          SIZE 7.2 BY .62 AT ROW 1.24 COL 4.8 WIDGET-ID 374
     RECT-68 AT ROW 1.52 COL 3.4 WIDGET-ID 372
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 205.4 BY 27.95 WIDGET-ID 100.

DEFINE FRAME frDetail
     brwConsolidate AT ROW 2.14 COL 7.4 WIDGET-ID 3500
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "Consolidation" WIDGET-ID 900.

DEFINE FRAME frCompliance
     bReqFulExport AT ROW 13.29 COL 2 WIDGET-ID 416 NO-TAB-STOP 
     cbAgentsQual AT ROW 2.48 COL 13.8 COLON-ALIGNED WIDGET-ID 252
     tShowInactiveQuals AT ROW 3.91 COL 8 WIDGET-ID 422
     brwQualification AT ROW 4.91 COL 8 WIDGET-ID 3200
     brwReqFul AT ROW 12.14 COL 8 WIDGET-ID 1200
     btComQualExp AT ROW 6.05 COL 2.2 WIDGET-ID 412 NO-TAB-STOP 
     btComQualRefresh AT ROW 4.91 COL 2.2 WIDGET-ID 410 NO-TAB-STOP 
     btComReqFulRefresh AT ROW 12.14 COL 2 WIDGET-ID 414 NO-TAB-STOP 
     btComViewQual AT ROW 7.19 COL 2.2 WIDGET-ID 418 NO-TAB-STOP 
     btComViewReq AT ROW 14.43 COL 2 WIDGET-ID 420 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "Compliance" WIDGET-ID 1100.

DEFINE FRAME frAlert
     bRefreshAlert AT ROW 3.86 COL 2.2 WIDGET-ID 318 NO-TAB-STOP 
     cbAgentsAlert AT ROW 2.1 COL 13 COLON-ALIGNED WIDGET-ID 252
     fCode AT ROW 2.1 COL 86.8 COLON-ALIGNED WIDGET-ID 324
     fOwner AT ROW 2.1 COL 153.4 COLON-ALIGNED WIDGET-ID 326
     brwAlerts AT ROW 3.91 COL 7 WIDGET-ID 1500
     bExport AT ROW 5 COL 2.2 WIDGET-ID 316 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "Alerts" WIDGET-ID 2300.

DEFINE FRAME frAudit
     btActionExport AT ROW 14.57 COL 2.8 WIDGET-ID 406 NO-TAB-STOP 
     cbAgentsAudit AT ROW 2.19 COL 13.6 COLON-ALIGNED WIDGET-ID 252
     brwQAR AT ROW 3.52 COL 7.6 WIDGET-ID 400
     brwAction AT ROW 13.48 COL 7.6 WIDGET-ID 1800
     btActionMail AT ROW 16.86 COL 2.8 WIDGET-ID 398 NO-TAB-STOP 
     btActionOpen AT ROW 15.71 COL 2.8 WIDGET-ID 400 NO-TAB-STOP 
     btActionRefresh AT ROW 13.43 COL 2.8 WIDGET-ID 402 NO-TAB-STOP 
     btAuditExport AT ROW 4.62 COL 2.8 WIDGET-ID 404 NO-TAB-STOP 
     btAuditMail AT ROW 6.91 COL 2.8 WIDGET-ID 392 NO-TAB-STOP 
     btAuditOpen AT ROW 5.76 COL 2.8 WIDGET-ID 394 NO-TAB-STOP 
     btAuditRefresh AT ROW 3.48 COL 2.8 WIDGET-ID 396 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "Audits" WIDGET-ID 1700.

DEFINE FRAME frClaim
     btClaimExport AT ROW 8.91 COL 3 WIDGET-ID 382 NO-TAB-STOP 
     netfromLTD AT ROW 3.14 COL 119.4 RIGHT-ALIGNED NO-LABEL WIDGET-ID 440 NO-TAB-STOP 
     btClaimMail AT ROW 11.19 COL 3 WIDGET-ID 384 NO-TAB-STOP 
     costfromLTD AT ROW 4.1 COL 95.4 COLON-ALIGNED NO-LABEL WIDGET-ID 442 NO-TAB-STOP 
     errorfromLTD AT ROW 5 COL 95.4 COLON-ALIGNED NO-LABEL WIDGET-ID 444 NO-TAB-STOP 
     btClaimRefresh AT ROW 7.76 COL 3 WIDGET-ID 380 NO-TAB-STOP 
     cbAgentsClm AT ROW 6.57 COL 14.2 COLON-ALIGNED WIDGET-ID 252
     brwClaims AT ROW 7.81 COL 8 WIDGET-ID 2000
     fLAER AT ROW 20.71 COL 93.4 COLON-ALIGNED NO-LABEL WIDGET-ID 526 NO-TAB-STOP 
     fLAEL AT ROW 20.71 COL 110.4 COLON-ALIGNED NO-LABEL WIDGET-ID 538 NO-TAB-STOP 
     fLossR AT ROW 20.71 COL 127.2 COLON-ALIGNED NO-LABEL WIDGET-ID 540 NO-TAB-STOP 
     fLossL AT ROW 20.71 COL 144 COLON-ALIGNED NO-LABEL WIDGET-ID 542 NO-TAB-STOP 
     fRecoveriesR AT ROW 20.71 COL 161 COLON-ALIGNED NO-LABEL WIDGET-ID 544 NO-TAB-STOP 
     fTotalCostL AT ROW 20.71 COL 177.8 COLON-ALIGNED NO-LABEL WIDGET-ID 546 NO-TAB-STOP 
     btClaimOpen AT ROW 10.05 COL 3 WIDGET-ID 390 NO-TAB-STOP 
     costnetLTD AT ROW 4.24 COL 119 COLON-ALIGNED NO-LABEL WIDGET-ID 478
     errornetLTD AT ROW 5.19 COL 119 COLON-ALIGNED NO-LABEL WIDGET-ID 480
     fTotal AT ROW 20.95 COL 71 COLON-ALIGNED NO-LABEL WIDGET-ID 524
     "Agent Error Total Costs:" VIEW-AS TEXT
          SIZE 23 BY .62 AT ROW 5.14 COL 73.4 WIDGET-ID 438
     "Life-to-Date" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 2.19 COL 103 WIDGET-ID 420
     "Net Premium:" VIEW-AS TEXT
          SIZE 13 BY .62 AT ROW 3.24 COL 83.2 WIDGET-ID 434
     "Total Cost Paid:" VIEW-AS TEXT
          SIZE 15.4 BY .62 AT ROW 4.24 COL 80.8 WIDGET-ID 492
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "Claims" WIDGET-ID 1900.

DEFINE FRAME frAr
     btArMail AT ROW 8.52 COL 4 WIDGET-ID 384 NO-TAB-STOP 
     btRefreshAr AT ROW 5.1 COL 4 WIDGET-ID 474 NO-TAB-STOP 
     b30to60 AT ROW 2.38 COL 94.8 WIDGET-ID 478
     b60to90 AT ROW 2.38 COL 120.4 WIDGET-ID 480
     bbalance AT ROW 2.38 COL 171.4 WIDGET-ID 484
     bCurrentaging AT ROW 2.38 COL 69 WIDGET-ID 476
     bover90 AT ROW 2.38 COL 146 WIDGET-ID 482
     btFileExport AT ROW 6.24 COL 4 WIDGET-ID 414 NO-TAB-STOP 
     btFileOpen AT ROW 7.38 COL 4 WIDGET-ID 428 NO-TAB-STOP 
     brwArAging AT ROW 5.14 COL 9 WIDGET-ID 1500
     cbAgentsAR AT ROW 2.43 COL 15.2 COLON-ALIGNED WIDGET-ID 252
     "Over 90 Days" VIEW-AS TEXT
          SIZE 14 BY .62 AT ROW 1.71 COL 151.4 WIDGET-ID 22
     "61 to 90 Days" VIEW-AS TEXT
          SIZE 14 BY .62 AT ROW 1.71 COL 125.6 WIDGET-ID 20
     "Total" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1.71 COL 180 WIDGET-ID 24
     "31 to 60 Days" VIEW-AS TEXT
          SIZE 14 BY .62 AT ROW 1.71 COL 100.2 WIDGET-ID 18
     "Current" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.71 COL 77.4 WIDGET-ID 16
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "A/R" WIDGET-ID 2600.

DEFINE FRAME frActivity
     brwActivityPolicies AT ROW 2 COL 7.4 WIDGET-ID 2900
     brwActivityCPL AT ROW 11.52 COL 7.4 WIDGET-ID 3400
     bActivityExp AT ROW 3.05 COL 2.4 WIDGET-ID 566 NO-TAB-STOP 
     bActivityExpCPL AT ROW 12.57 COL 2.4 WIDGET-ID 590 NO-TAB-STOP 
     bActivityMetricsRefresh AT ROW 1.95 COL 2.4 WIDGET-ID 588 NO-TAB-STOP 
     bActivityMetricsRefreshCPL AT ROW 11.48 COL 2.4 WIDGET-ID 592 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "Metrics" WIDGET-ID 2800.

DEFINE FRAME frBatch
     brwActivityBatch AT ROW 2.1 COL 12 WIDGET-ID 3300
     cbAgents AT ROW 10.1 COL 13.6 COLON-ALIGNED WIDGET-ID 252
     brwBatch AT ROW 11.33 COL 7.6 WIDGET-ID 200
     bActivityBatchExport AT ROW 2.05 COL 6.8 WIDGET-ID 580 NO-TAB-STOP 
     bActivityBatchRefresh AT ROW 2.05 COL 2.4 WIDGET-ID 588 NO-TAB-STOP 
     bYear1 AT ROW 5.91 COL 2.4 WIDGET-ID 572
     bYear2 AT ROW 4.91 COL 2.4 WIDGET-ID 574
     bYear3 AT ROW 3.91 COL 2.4 WIDGET-ID 576
     bBatchRefresh AT ROW 11.29 COL 2.6 WIDGET-ID 578 NO-TAB-STOP 
     bBatchExport AT ROW 12.43 COL 2.6 WIDGET-ID 566 NO-TAB-STOP 
     bBatchOpen AT ROW 13.57 COL 2.6 WIDGET-ID 568 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "Remittances" WIDGET-ID 1000.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Container Links: Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source
   Design Page: 9
   Other Settings: APPSERVER
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent Consolidation"
         HEIGHT             = 27.95
         WIDTH              = 205.4
         MAX-HEIGHT         = 47.19
         MAX-WIDTH          = 296
         VIRTUAL-HEIGHT     = 47.19
         VIRTUAL-WIDTH      = 296
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB wWin 
/* ************************* Included-Libraries *********************** */

{src/adm2/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME frActivity:FRAME = FRAME fMain:HANDLE
       FRAME frAlert:FRAME = FRAME fMain:HANDLE
       FRAME frAr:FRAME = FRAME fMain:HANDLE
       FRAME frAudit:FRAME = FRAME fMain:HANDLE
       FRAME frBatch:FRAME = FRAME fMain:HANDLE
       FRAME frClaim:FRAME = FRAME fMain:HANDLE
       FRAME frCompliance:FRAME = FRAME fMain:HANDLE
       FRAME frDetail:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
ASSIGN 
       eParams:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       eResults:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FRAME frActivity
                                                                        */
/* BROWSE-TAB brwActivityPolicies 1 frActivity */
/* BROWSE-TAB brwActivityCPL brwActivityPolicies frActivity */
/* SETTINGS FOR BUTTON bActivityExp IN FRAME frActivity
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bActivityExpCPL IN FRAME frActivity
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bActivityMetricsRefresh IN FRAME frActivity
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bActivityMetricsRefreshCPL IN FRAME frActivity
   NO-ENABLE                                                            */
/* SETTINGS FOR BROWSE brwActivityCPL IN FRAME frActivity
   NO-ENABLE                                                            */
ASSIGN 
       brwActivityCPL:COLUMN-RESIZABLE IN FRAME frActivity       = TRUE
       brwActivityCPL:COLUMN-MOVABLE IN FRAME frActivity         = TRUE.

/* SETTINGS FOR BROWSE brwActivityPolicies IN FRAME frActivity
   NO-ENABLE                                                            */
ASSIGN 
       brwActivityPolicies:COLUMN-RESIZABLE IN FRAME frActivity       = TRUE
       brwActivityPolicies:COLUMN-MOVABLE IN FRAME frActivity         = TRUE.

/* SETTINGS FOR FRAME frAlert
                                                                        */
/* BROWSE-TAB brwAlerts fOwner frAlert */
ASSIGN 
       FRAME frAlert:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bExport IN FRAME frAlert
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bRefreshAlert IN FRAME frAlert
   NO-ENABLE                                                            */
ASSIGN 
       brwAlerts:ALLOW-COLUMN-SEARCHING IN FRAME frAlert = TRUE
       brwAlerts:COLUMN-RESIZABLE IN FRAME frAlert       = TRUE.

/* SETTINGS FOR COMBO-BOX cbAgentsAlert IN FRAME frAlert
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX fCode IN FRAME frAlert
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX fOwner IN FRAME frAlert
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frAr
   Custom                                                               */
/* BROWSE-TAB brwArAging btFileOpen frAr */
ASSIGN 
       brwArAging:ALLOW-COLUMN-SEARCHING IN FRAME frAr = TRUE
       brwArAging:COLUMN-RESIZABLE IN FRAME frAr       = TRUE
       brwArAging:COLUMN-MOVABLE IN FRAME frAr         = TRUE.

/* SETTINGS FOR BUTTON btArMail IN FRAME frAr
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btFileExport IN FRAME frAr
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btFileOpen IN FRAME frAr
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btRefreshAr IN FRAME frAr
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbAgentsAR IN FRAME frAr
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frAudit
                                                                        */
/* BROWSE-TAB brwQAR cbAgentsAudit frAudit */
/* BROWSE-TAB brwAction brwQAR frAudit */
ASSIGN 
       brwAction:POPUP-MENU IN FRAME frAudit             = MENU POPUP-MENU-brwQAR-2:HANDLE
       brwAction:ALLOW-COLUMN-SEARCHING IN FRAME frAudit = TRUE
       brwAction:COLUMN-RESIZABLE IN FRAME frAudit       = TRUE.

ASSIGN 
       brwQAR:POPUP-MENU IN FRAME frAudit             = MENU POPUP-MENU-brwQAR:HANDLE
       brwQAR:ALLOW-COLUMN-SEARCHING IN FRAME frAudit = TRUE
       brwQAR:COLUMN-RESIZABLE IN FRAME frAudit       = TRUE.

/* SETTINGS FOR BUTTON btActionExport IN FRAME frAudit
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btActionMail IN FRAME frAudit
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btActionOpen IN FRAME frAudit
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btActionRefresh IN FRAME frAudit
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btAuditExport IN FRAME frAudit
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btAuditMail IN FRAME frAudit
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btAuditOpen IN FRAME frAudit
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btAuditRefresh IN FRAME frAudit
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbAgentsAudit IN FRAME frAudit
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frBatch
   NOT-VISIBLE                                                          */
/* BROWSE-TAB brwActivityBatch 1 frBatch */
/* BROWSE-TAB brwBatch cbAgents frBatch */
ASSIGN 
       FRAME frBatch:HIDDEN           = TRUE
       FRAME frBatch:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bActivityBatchExport IN FRAME frBatch
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bActivityBatchRefresh IN FRAME frBatch
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBatchExport IN FRAME frBatch
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBatchOpen IN FRAME frBatch
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBatchRefresh IN FRAME frBatch
   NO-ENABLE                                                            */
/* SETTINGS FOR BROWSE brwActivityBatch IN FRAME frBatch
   NO-ENABLE                                                            */
ASSIGN 
       brwActivityBatch:COLUMN-RESIZABLE IN FRAME frBatch       = TRUE
       brwActivityBatch:COLUMN-MOVABLE IN FRAME frBatch         = TRUE.

ASSIGN 
       brwBatch:COLUMN-RESIZABLE IN FRAME frBatch       = TRUE
       brwBatch:COLUMN-MOVABLE IN FRAME frBatch         = TRUE.

/* SETTINGS FOR COMBO-BOX cbAgents IN FRAME frBatch
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frClaim
                                                                        */
/* BROWSE-TAB brwClaims cbAgentsClm frClaim */
ASSIGN 
       brwClaims:ALLOW-COLUMN-SEARCHING IN FRAME frClaim = TRUE
       brwClaims:COLUMN-MOVABLE IN FRAME frClaim         = TRUE.

/* SETTINGS FOR BUTTON btClaimExport IN FRAME frClaim
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btClaimMail IN FRAME frClaim
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btClaimOpen IN FRAME frClaim
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btClaimRefresh IN FRAME frClaim
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbAgentsClm IN FRAME frClaim
   NO-ENABLE                                                            */
ASSIGN 
       costfromLTD:READ-ONLY IN FRAME frClaim        = TRUE
       costfromLTD:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       errorfromLTD:READ-ONLY IN FRAME frClaim        = TRUE
       errorfromLTD:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       fLAEL:READ-ONLY IN FRAME frClaim        = TRUE
       fLAEL:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       fLAER:READ-ONLY IN FRAME frClaim        = TRUE
       fLAER:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       fLossL:READ-ONLY IN FRAME frClaim        = TRUE
       fLossL:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       fLossR:READ-ONLY IN FRAME frClaim        = TRUE
       fLossR:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       fRecoveriesR:READ-ONLY IN FRAME frClaim        = TRUE
       fRecoveriesR:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       fTotalCostL:READ-ONLY IN FRAME frClaim        = TRUE
       fTotalCostL:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

/* SETTINGS FOR FILL-IN netfromLTD IN FRAME frClaim
   ALIGN-R                                                              */
ASSIGN 
       netfromLTD:READ-ONLY IN FRAME frClaim        = TRUE
       netfromLTD:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

/* SETTINGS FOR FRAME frCompliance
   NOT-VISIBLE                                                          */
/* BROWSE-TAB brwQualification tShowInactiveQuals frCompliance */
/* BROWSE-TAB brwReqFul brwQualification frCompliance */
ASSIGN 
       FRAME frCompliance:HIDDEN           = TRUE
       FRAME frCompliance:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bReqFulExport IN FRAME frCompliance
   NO-ENABLE                                                            */
ASSIGN 
       brwQualification:ALLOW-COLUMN-SEARCHING IN FRAME frCompliance = TRUE
       brwQualification:COLUMN-RESIZABLE IN FRAME frCompliance       = TRUE.

ASSIGN 
       brwReqFul:ALLOW-COLUMN-SEARCHING IN FRAME frCompliance = TRUE
       brwReqFul:COLUMN-RESIZABLE IN FRAME frCompliance       = TRUE.

/* SETTINGS FOR BUTTON btComQualExp IN FRAME frCompliance
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btComQualRefresh IN FRAME frCompliance
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btComReqFulRefresh IN FRAME frCompliance
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btComViewQual IN FRAME frCompliance
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btComViewReq IN FRAME frCompliance
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbAgentsQual IN FRAME frCompliance
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frDetail
   Custom                                                               */
/* BROWSE-TAB brwConsolidate 1 frDetail */
ASSIGN 
       brwConsolidate:ALLOW-COLUMN-SEARCHING IN FRAME frDetail = TRUE
       brwConsolidate:COLUMN-RESIZABLE IN FRAME frDetail       = TRUE
       brwConsolidate:COLUMN-MOVABLE IN FRAME frDetail         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAction
/* Query rebuild information for BROWSE brwAction
     _START_FREEFORM
open query {&SELF-NAME} for each action
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAction */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwActivityBatch
/* Query rebuild information for BROWSE brwActivityBatch
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR each tActivity.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwActivityBatch */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwActivityCPL
/* Query rebuild information for BROWSE brwActivityCPL
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tMetricsActivity.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwActivityCPL */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwActivityPolicies
/* Query rebuild information for BROWSE brwActivityPolicies
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tMetricsActivity.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwActivityPolicies */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAlerts
/* Query rebuild information for BROWSE brwAlerts
     _START_FREEFORM
open query {&SELF-NAME} for each alert.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAlerts */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwArAging
/* Query rebuild information for BROWSE brwArAging
     _START_FREEFORM
open query {&SELF-NAME} for each tArAging.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwArAging */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwBatch
/* Query rebuild information for BROWSE brwBatch
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH batch.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwBatch */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwClaims
/* Query rebuild information for BROWSE brwClaims
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH agentclaim by agentclaim.agentID by agentclaim.claimID.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwClaims */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwConsolidate
/* Query rebuild information for BROWSE brwConsolidate
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH cAgent.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwConsolidate */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQAR
/* Query rebuild information for BROWSE brwQAR
     _START_FREEFORM
open query {&SELF-NAME} for each audit no-lock by audit.auditStartDate desc .
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQAR */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQualification
/* Query rebuild information for BROWSE brwQualification
     _START_FREEFORM
open query brwQualification for each qualification  by Qualification.qualificationId.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQualification */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwReqFul
/* Query rebuild information for BROWSE brwReqFul
     _START_FREEFORM
open query {&SELF-NAME} for each reqfulfill.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwReqFul */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frAlert
/* Query rebuild information for FRAME frAlert
     _Query            is NOT OPENED
*/  /* FRAME frAlert */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frBatch
/* Query rebuild information for FRAME frBatch
     _Query            is NOT OPENED
*/  /* FRAME frBatch */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frCompliance
/* Query rebuild information for FRAME frCompliance
     _Query            is NOT OPENED
*/  /* FRAME frCompliance */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frDetail
/* Query rebuild information for FRAME frDetail
     _Query            is NOT OPENED
*/  /* FRAME frDetail */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin /* Agent Consolidation */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin /* Agent Consolidation */
DO:
  run exitObject in this-procedure.
  
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
/*   APPLY "CLOSE":U TO THIS-PROCEDURE. */
/*   RETURN NO-APPLY.                   */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-RESIZED OF wWin /* Agent Consolidation */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAr
&Scoped-define SELF-NAME b30to60
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL b30to60 wWin
ON CHOOSE OF b30to60 IN FRAME frAr
DO:
  if not available tArAgingSummTotal 
   then
    return.
  cLastAgingClicked = "30to60".
  lArAging = true.
  if tArAgingSummTotal.due31_60 = 0 
   then
    do:
      run filterArAging in this-procedure(input cLastAgingClicked).
      return.
    end.
  run getAr in this-procedure(input cLastAgingClicked).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME b60to90
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL b60to90 wWin
ON CHOOSE OF b60to90 IN FRAME frAr
DO:
  if not available tArAgingSummTotal 
   then
    return.
  cLastAgingClicked = "60to90".
  if tArAgingSummTotal.due61_90 = 0 
   then
    do:
      run filterArAging in this-procedure(input "60to90").
      return.
    end.
  run getAr in this-procedure(input "60to90").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBatch
&Scoped-define SELF-NAME bActivityBatchExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActivityBatchExport wWin
ON CHOOSE OF bActivityBatchExport IN FRAME frBatch /* Export */
do:
  run exportData in this-procedure (input browse brwActivityBatch:handle, /* Borwser handle */
                                    input temp-table tActivity:handle,    /* Temp-table handle */
                                    input "tActivity",                    /* Table name */
                                    input "BatchActivities",              /* CSV filename */
                                    input "for each tActivity by tActivity.year by tActivity.seq").          /* Query */  
                                
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bActivityBatchRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActivityBatchRefresh wWin
ON CHOOSE OF bActivityBatchRefresh IN FRAME frBatch /* Refresh */
do:
  run refreshActivityBatch in this-procedure.                                            
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frActivity
&Scoped-define SELF-NAME bActivityExp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActivityExp wWin
ON CHOOSE OF bActivityExp IN FRAME frActivity /* Export */
DO:
  run exportData in this-procedure (input browse brwActivityPolicies:handle,     /* Borwser handle */
                                    input temp-table tMetricsActivity:handle,    /* Temp-table handle */
                                    input "tMetricsActivity",                    /* Table name */
                                    input "MetricsActivityPolicies",                    /* CSV filename */
                                    input "for each tMetricsActivity where tMetricsActivity.category = 'P' ").          /* Query */  
                                
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bActivityExpCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActivityExpCPL wWin
ON CHOOSE OF bActivityExpCPL IN FRAME frActivity /* Export */
DO:
  run exportData in this-procedure (input browse brwActivityPolicies:handle,     /* Borwser handle */
                                    input temp-table tMetricsActivity:handle,    /* Temp-table handle */
                                    input "tMetricsActivity",                    /* Table name */
                                    input "MetricsActivityCPL",                    /* CSV filename */
                                    input "for each tMetricsActivity where tMetricsActivity.category = 'T' ").          /* Query */  
                                
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bActivityMetricsRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActivityMetricsRefresh wWin
ON CHOOSE OF bActivityMetricsRefresh IN FRAME frActivity /* Refresh */
DO:
  if  cAgentList ne ""
   then
    do:
      run refreshActivityMetrics in this-procedure.
    end.
    
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bActivityMetricsRefreshCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActivityMetricsRefreshCPL wWin
ON CHOOSE OF bActivityMetricsRefreshCPL IN FRAME frActivity /* Refresh */
DO:
  apply 'choose' to bActivityMetricsRefresh.                                             
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAr
&Scoped-define SELF-NAME bbalance
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bbalance wWin
ON CHOOSE OF bbalance IN FRAME frAr
DO:
  if not available tArAgingSummTotal 
   then
    return.
  cLastAgingClicked = "balance".
  lArAging = true.
  if tArAgingSummTotal.balance = 0 
   then
    do:
      run filterArAging in this-procedure(input cLastAgingClicked).
      return.
    end.
  run getAr in this-procedure (input cLastAgingClicked).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBatch
&Scoped-define SELF-NAME bBatchExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBatchExport wWin
ON CHOOSE OF bBatchExport IN FRAME frBatch /* Export */
DO:
  empty temp-table tBatch.
  
  for each batch no-lock:
    create tBatch.
    buffer-copy batch to tBatch.
    tBatch.stat =  getBatchStatus(batch.stat).
  end.
  run exportData in this-procedure (input browse brwBatch:handle, /* Borwser handle */
                                    input temp-table tBatch:handle,    /* Temp-table handle */
                                    input "tBatch",                    /* Table name */
                                    input "Batches",              /* CSV filename */
                                    input "for each tBatch").          /* Query */  
        
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBatchOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBatchOpen wWin
ON CHOOSE OF bBatchOpen IN FRAME frBatch /* Open */
DO:
  run openBatchDetails in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBatchRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBatchRefresh wWin
ON CHOOSE OF bBatchRefresh IN FRAME frBatch /* Refresh */
DO:
  if cLastRemitClicked = ""
   then
    cLastRemitClicked = string(year(today)). 
    
  openBatchesYearly(input cLastRemitClicked).
  
  brwBatch:title = "Batches for " + cLastRemitClicked.
  run enableButtons in this-procedure.
  lBatch = true.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAr
&Scoped-define SELF-NAME bCurrentaging
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCurrentaging wWin
ON CHOOSE OF bCurrentaging IN FRAME frAr
DO:
  if not available tArAgingSummTotal 
   then
    return.
  cLastAgingClicked = "Current".  
  lArAging = true.
  if tArAgingSummTotal.due0_30 = 0 
   then
    do:
      run filterArAging in this-procedure(input cLastAgingClicked).
      return.
    end. 

  run getAr in this-procedure(input cLastAgingClicked).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAlert
&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport wWin
ON CHOOSE OF bExport IN FRAME frAlert /* Export */
DO:
  run exportData in this-procedure (input browse brwAlerts:handle, /* Borwser handle */
                                    input temp-table alert:handle, /* Temp-table handle */
                                    input "alert",                 /* Table name */
                                    input "Alerts",                /* CSV filename */
                                    input "for each alert").       /* Query */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAr
&Scoped-define SELF-NAME bover90
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bover90 wWin
ON CHOOSE OF bover90 IN FRAME frAr
DO:
  if not available tArAgingSummTotal 
   then
    return.
  cLastAgingClicked = "over90".
  lArAging = true.
  if tArAgingSummTotal.due91_ = 0 
   then
    do:
      run filterArAging in this-procedure(input cLastAgingClicked).
      return.
    end.
  run getAr in this-procedure(input cLastAgingClicked).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh wWin
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  run refreshData in this-procedure(input activePageNumber).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAlert
&Scoped-define SELF-NAME bRefreshAlert
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefreshAlert wWin
ON CHOOSE OF bRefreshAlert IN FRAME frAlert /* Refresh */
DO:
  lRefreshAlerts = true.
  
  run loadAlerts   in this-procedure.
  run getAlertDesc in this-procedure.
  run setAlertCombos in this-procedure.
  run filterAlerts in this-procedure.
  
  lRefreshAlerts = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frCompliance
&Scoped-define SELF-NAME bReqFulExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReqFulExport wWin
ON CHOOSE OF bReqFulExport IN FRAME frCompliance /* Export */
do:
  run exportData in this-procedure (input browse brwReqful:handle,   /* Borwser handle */
                                  input temp-table reqfulfill:handle, /* Temp-table handle */
                                  input "reqfulfill",                 /* Table name */
                                  input "ComplianceLogs",             /* CSV filename */
                                  input "for each reqfulfill").       /* Query */  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAction
&Scoped-define FRAME-NAME frAudit
&Scoped-define SELF-NAME brwAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAction wWin
ON DEFAULT-ACTION OF brwAction IN FRAME frAudit /* Open Corrective Actions */
DO:
  apply 'choose' to btActionOpen.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAction wWin
ON ROW-DISPLAY OF brwAction IN FRAME frAudit /* Open Corrective Actions */
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAction wWin
ON START-SEARCH OF brwAction IN FRAME frAudit /* Open Corrective Actions */
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwAction}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwActivityBatch
&Scoped-define FRAME-NAME frBatch
&Scoped-define SELF-NAME brwActivityBatch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwActivityBatch wWin
ON ROW-DISPLAY OF brwActivityBatch IN FRAME frBatch /* Net Premium */
DO:

  for first brw where brw.name = self:name:
    rowColor = if tActivity.type = "A" then ? else 32 . 
    do std-in = 1 to num-entries(brw.colHandleList):
      colHandle = handle(entry(std-in, brw.colHandleList)).
      if valid-handle(colHandle)
       then
        do:
          colHandle:bgcolor = rowColor. 
          if std-in = num-entries(brw.colHandleList) and tActivity.year = year(today) 
           then
            colHandle:font = 8. 
        end.              
    end.
  end.    
  
  assign
      tActivity.month1:fgcolor in browse {&browse-name} = if (tActivity.month1 < 0)   then 12 else ?
      tActivity.month2:fgcolor  = if (tActivity.month2 < 0)   then 12 else ?
      tActivity.month3:fgcolor  = if (tActivity.month3 < 0)   then 12 else ?                                                                   
      tActivity.month4:fgcolor  = if (tActivity.month4 < 0)   then 12 else ?                                                    
      tActivity.month5:fgcolor  = if (tActivity.month5 < 0)   then 12 else ?                                                   
      tActivity.month6:fgcolor  = if (tActivity.month6 < 0)   then 12 else ?                                                   
      tActivity.month7:fgcolor  = if (tActivity.month7 < 0)   then 12 else ?                                                  
      tActivity.month8:fgcolor  = if (tActivity.month8 < 0)   then 12 else ?                                                   
      tActivity.month9:fgcolor  = if (tActivity.month9 < 0)   then 12 else ?                                                   
      tActivity.month10:fgcolor = if (tActivity.month10 < 0)  then 12 else ?                                                   
      tActivity.month11:fgcolor = if (tActivity.month11 < 0)  then 12 else ?                                                   
      tActivity.month12:fgcolor = if (tActivity.month12 < 0)  then 12 else ?
      tActivity.yrTotal:fgcolor = if (tActivity.yrTotal < 0)  then 12 else ?  
      .
          
  /* change the column format */
  assign
      cFormat  = "(>>>,>>>,>>9)% "
      cFormat1 = "(>>>,>>>,>>>)"
      cFormat2 = "(>>>,>>>,>>9)"
      .

  if (tActivity.type = "C" or tActivity.type = "A") and tActivity.year = year(today)
   then
    do:
      assign
          tActivity.month1:format in browse {&browse-name} = if month(today) >= 1 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month2:format in browse {&browse-name} = if month(today) >= 2 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month3:format in browse {&browse-name} = if month(today) >= 3 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month4:format in browse {&browse-name} = if month(today) >= 4 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month5:format in browse {&browse-name} = if month(today) >= 5 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month6:format in browse {&browse-name} = if month(today) >= 6 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month7:format in browse {&browse-name} = if month(today) >= 7 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month8:format in browse {&browse-name} = if month(today) >= 8 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month9:format in browse {&browse-name} = if month(today) >= 9 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month10:format in browse {&browse-name} = if month(today) >= 10 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month11:format in browse {&browse-name} = if month(today) >= 11 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month12:format in browse {&browse-name} = if month(today) >= 12 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.total:format in browse {&browse-name} = if tActivity.type = "C" then cFormat else cFormat2
          tActivity.yrTotal:format in browse {&browse-name} = if tActivity.type = "C" then cFormat else cFormat2
          .
    end. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwActivityCPL
&Scoped-define FRAME-NAME frActivity
&Scoped-define SELF-NAME brwActivityCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwActivityCPL wWin
ON ROW-DISPLAY OF brwActivityCPL IN FRAME frActivity /* Closing Protection Letters */
DO:
  if tMetricsActivity.type <> "A" and tMetricsActivity.year = year(today)
   then
    tMetricsActivity.year:screen-value in browse {&browse-name} = "" /* string(tMetricsActivity.year) */.   
  
  for first brw where brw.name = self:name:
    rowColor = if tMetricsActivity.type = "A" then ? else 32 . 
    do std-in = 1 to num-entries(brw.colHandleList):
      colHandle = handle(entry(std-in, brw.colHandleList)).
      if valid-handle(colHandle)
       then
        do:
          colHandle:bgcolor = rowColor. 
          if std-in = num-entries(brw.colHandleList) and tMetricsActivity.year = year(today) 
           then
            colHandle:font = 8. 
        end.              
    end.
  end.
      
  
  assign
      tMetricsActivity.month1:fgcolor in browse {&browse-name} = if (tMetricsActivity.month1 < 0)   then 12 else ?
      tMetricsActivity.month2:fgcolor  = if (tMetricsActivity.month2 < 0)   then 12 else ?
      tMetricsActivity.month3:fgcolor  = if (tMetricsActivity.month3 < 0)   then 12 else ?                                                                   
      tMetricsActivity.month4:fgcolor  = if (tMetricsActivity.month4 < 0)   then 12 else ?                                                    
      tMetricsActivity.month5:fgcolor  = if (tMetricsActivity.month5 < 0)   then 12 else ?                                                   
      tMetricsActivity.month6:fgcolor  = if (tMetricsActivity.month6 < 0)   then 12 else ?                                                   
      tMetricsActivity.month7:fgcolor  = if (tMetricsActivity.month7 < 0)   then 12 else ?                                                  
      tMetricsActivity.month8:fgcolor  = if (tMetricsActivity.month8 < 0)   then 12 else ?                                                   
      tMetricsActivity.month9:fgcolor  = if (tMetricsActivity.month9 < 0)   then 12 else ?                                                   
      tMetricsActivity.month10:fgcolor = if (tMetricsActivity.month10 < 0)  then 12 else ?                                                   
      tMetricsActivity.month11:fgcolor = if (tMetricsActivity.month11 < 0)  then 12 else ?                                                   
      tMetricsActivity.month12:fgcolor = if (tMetricsActivity.month12 < 0)  then 12 else ?
      tMetricsActivity.yrTotal:fgcolor = if (tMetricsActivity.yrTotal < 0)  then 12 else ?  
      .
          
  /* change the column format */
  assign
      cFormat  = ">>>,>>>,>>9% "
      cFormat1 = "(>>>,>>>,>>>)"
      cFormat2 = "(>>>,>>>,>>9)"
      . 
      
  if (tMetricsActivity.type = "C" or tMetricsActivity.type = "A") and tMetricsActivity.year = year(today)
   then
    do:
      assign
          tMetricsActivity.month1:format in browse {&browse-name} = if month(today) >= 1 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month2:format in browse {&browse-name} = if month(today) >= 2 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month3:format in browse {&browse-name} = if month(today) >= 3 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month4:format in browse {&browse-name} = if month(today) >= 4 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month5:format in browse {&browse-name} = if month(today) >= 5 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month6:format in browse {&browse-name} = if month(today) >= 6 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month7:format in browse {&browse-name} = if month(today) >= 7 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month8:format in browse {&browse-name} = if month(today) >= 8 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month9:format in browse {&browse-name} = if month(today) >= 9 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month10:format in browse {&browse-name} = if month(today) >= 10 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month11:format in browse {&browse-name} = if month(today) >= 11 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month12:format in browse {&browse-name} = if month(today) >= 12 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.total:format in browse {&browse-name} = if tMetricsActivity.type = "C" then cFormat else cFormat2
          tMetricsActivity.yrTotal:format in browse {&browse-name} = if tMetricsActivity.type = "C" then cFormat else cFormat2
          .
    end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwActivityPolicies
&Scoped-define SELF-NAME brwActivityPolicies
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwActivityPolicies wWin
ON ROW-DISPLAY OF brwActivityPolicies IN FRAME frActivity /* Policies */
DO:
  if tMetricsActivity.type <> "I" and tMetricsActivity.year = year(today)
   then
    tMetricsActivity.year:screen-value in browse {&browse-name} = "" /* string(tMetricsActivity.year) */.   
  
  for first brw where brw.name = self:name:
    rowColor = if tMetricsActivity.type = "A" or tMetricsActivity.type = "I"  then ? else 32 . 
    do std-in = 1 to num-entries(brw.colHandleList):
      colHandle = handle(entry(std-in, brw.colHandleList)).
      if valid-handle(colHandle)
       then
        do:
          colHandle:bgcolor = rowColor. 
          if std-in = num-entries(brw.colHandleList) and tMetricsActivity.year = year(today) 
           then
            colHandle:font = 8. 
        end.              
    end.
  end.
      
  
  assign
      tMetricsActivity.month1:fgcolor in browse {&browse-name} = if (tMetricsActivity.month1 < 0)   then 12 else ?
      tMetricsActivity.month2:fgcolor  = if (tMetricsActivity.month2 < 0)   then 12 else ?
      tMetricsActivity.month3:fgcolor  = if (tMetricsActivity.month3 < 0)   then 12 else ?                                                                   
      tMetricsActivity.month4:fgcolor  = if (tMetricsActivity.month4 < 0)   then 12 else ?                                                    
      tMetricsActivity.month5:fgcolor  = if (tMetricsActivity.month5 < 0)   then 12 else ?                                                   
      tMetricsActivity.month6:fgcolor  = if (tMetricsActivity.month6 < 0)   then 12 else ?                                                   
      tMetricsActivity.month7:fgcolor  = if (tMetricsActivity.month7 < 0)   then 12 else ?                                                  
      tMetricsActivity.month8:fgcolor  = if (tMetricsActivity.month8 < 0)   then 12 else ?                                                   
      tMetricsActivity.month9:fgcolor  = if (tMetricsActivity.month9 < 0)   then 12 else ?                                                   
      tMetricsActivity.month10:fgcolor = if (tMetricsActivity.month10 < 0)  then 12 else ?                                                   
      tMetricsActivity.month11:fgcolor = if (tMetricsActivity.month11 < 0)  then 12 else ?                                                   
      tMetricsActivity.month12:fgcolor = if (tMetricsActivity.month12 < 0)  then 12 else ?
      tMetricsActivity.yrTotal:fgcolor = if (tMetricsActivity.yrTotal < 0)  then 12 else ?  
      .
          
  /* change the column format */
  assign
      cFormat  = ">>>,>>>,>>9% "
      cFormat1 = "(>>>,>>>,>>>)"
      cFormat2 = "(>>>,>>>,>>9)"
      .   
    
  if (tMetricsActivity.type = "C" or tMetricsActivity.type = "I") and tMetricsActivity.year = year(today)
   then
    do:
      assign
          tMetricsActivity.month1:format in browse {&browse-name} = if month(today) >= 1 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month2:format in browse {&browse-name} = if month(today) >= 2 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month3:format in browse {&browse-name} = if month(today) >= 3 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month4:format in browse {&browse-name} = if month(today) >= 4 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month5:format in browse {&browse-name} = if month(today) >= 5 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month6:format in browse {&browse-name} = if month(today) >= 6 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month7:format in browse {&browse-name} = if month(today) >= 7 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month8:format in browse {&browse-name} = if month(today) >= 8 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month9:format in browse {&browse-name} = if month(today) >= 9 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month10:format in browse {&browse-name} = if month(today) >= 10 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month11:format in browse {&browse-name} = if month(today) >= 11 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month12:format in browse {&browse-name} = if month(today) >= 12 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.total:format in browse {&browse-name} = if tMetricsActivity.type = "C" then cFormat else cFormat2
          tMetricsActivity.yrTotal:format in browse {&browse-name} = if tMetricsActivity.type = "C" then cFormat else cFormat2
          .
    end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAlerts
&Scoped-define FRAME-NAME frAlert
&Scoped-define SELF-NAME brwAlerts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAlerts wWin
ON ROW-DISPLAY OF brwAlerts IN FRAME frAlert
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAlerts wWin
ON START-SEARCH OF brwAlerts IN FRAME frAlert
do:
  {lib/brw-startSearch-multi.i &browse-name = brwAlerts}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwArAging
&Scoped-define FRAME-NAME frAr
&Scoped-define SELF-NAME brwArAging
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArAging wWin
ON ROW-DISPLAY OF brwArAging IN FRAME frAr /* Select the Aging Category */
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArAging wWin
ON START-SEARCH OF brwArAging IN FRAME frAr /* Select the Aging Category */
do:
  {lib/brw-startSearch-multi.i &browse-name = brwArAging}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwBatch
&Scoped-define FRAME-NAME frBatch
&Scoped-define SELF-NAME brwBatch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBatch wWin
ON DEFAULT-ACTION OF brwBatch IN FRAME frBatch /* Batches */
DO:
  run openBatchDetails in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBatch wWin
ON ROW-DISPLAY OF brwBatch IN FRAME frBatch /* Batches */
DO:
 {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBatch wWin
ON START-SEARCH OF brwBatch IN FRAME frBatch /* Batches */
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwBatch}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwClaims
&Scoped-define FRAME-NAME frClaim
&Scoped-define SELF-NAME brwClaims
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwClaims wWin
ON ROW-DISPLAY OF brwClaims IN FRAME frClaim /* All Claims */
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwClaims wWin
ON START-SEARCH OF brwClaims IN FRAME frClaim /* All Claims */
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwClaims}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwConsolidate
&Scoped-define FRAME-NAME frDetail
&Scoped-define SELF-NAME brwConsolidate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwConsolidate wWin
ON DEFAULT-ACTION OF brwConsolidate IN FRAME frDetail
DO:
  define variable hFileDataSrv as handle    no-undo. 
  if not available cAgent
   then return.
   
  publish "OpenAgent" (cAgent.AgentID, output hFileDataSrv).
  
  publish "OpenWindow" (input "wAgent", 
                         input cAgent.AgentID, 
                         input "wAgent.w", 
                         input "handle|input|" + string(hFileDataSrv) + "^character|input|E",                                   
                         input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwConsolidate wWin
ON ROW-DISPLAY OF brwConsolidate IN FRAME frDetail
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwConsolidate wWin
ON START-SEARCH OF brwConsolidate IN FRAME frDetail
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwConsolidate}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQAR
&Scoped-define FRAME-NAME frAudit
&Scoped-define SELF-NAME brwQAR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQAR wWin
ON DEFAULT-ACTION OF brwQAR IN FRAME frAudit /* Audits */
DO:
  find current audit.
  if available audit
   then
    run selectAudit in this-procedure (audit.qarid).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQAR wWin
ON ROW-DISPLAY OF brwQAR IN FRAME frAudit /* Audits */
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQAR wWin
ON START-SEARCH OF brwQAR IN FRAME frAudit /* Audits */
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwQAR}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQualification
&Scoped-define FRAME-NAME frCompliance
&Scoped-define SELF-NAME brwQualification
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification wWin
ON DEFAULT-ACTION OF brwQualification IN FRAME frCompliance /* Qualifications */
DO:
  if btComViewQual:sensitive
   then
    apply "choose" to btComViewQual.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification wWin
ON ROW-DISPLAY OF brwQualification IN FRAME frCompliance /* Qualifications */
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification wWin
ON START-SEARCH OF brwQualification IN FRAME frCompliance /* Qualifications */
do:
{lib/brw-startSearch-multi.i &browse-name = brwQualification}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwReqFul
&Scoped-define SELF-NAME brwReqFul
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReqFul wWin
ON DEFAULT-ACTION OF brwReqFul IN FRAME frCompliance /* Requirements and Fulfillments */
do:
  if btComViewReq:sensitive
   then
    apply "choose" to btComViewReq.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReqFul wWin
ON ROW-DISPLAY OF brwReqFul IN FRAME frCompliance /* Requirements and Fulfillments */
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReqFul wWin
ON START-SEARCH OF brwReqFul IN FRAME frCompliance /* Requirements and Fulfillments */
do:
  {lib/brw-startSearch-multi.i &browse-name = brwReqFul}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bSaveConsolidation
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSaveConsolidation wWin
ON CHOOSE OF bSaveConsolidation IN FRAME fMain /* Save Consolidation */
DO:
  define variable cUpdatedName    as character no-undo.
  
  empty temp-table ttSelectedConsolidate.
  for each tConsolidate where tConsolidate.isChecked = true:
    create ttSelectedConsolidate.
    buffer-copy tConsolidate to ttSelectedConsolidate.
  end.
  
  run dialogsaveconsolidationparams.w (input "S",
                                       input 0 ,
                                       input "",
                                       input table ttSelectedConsolidate,
                                       output cUpdatedName).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAudit
&Scoped-define SELF-NAME btActionExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btActionExport wWin
ON CHOOSE OF btActionExport IN FRAME frAudit /* Export */
DO:
  run exportData in this-procedure (input browse brwAction:handle,  /* Borwser handle */
                                    input temp-table action:handle, /* Temp-table handle */
                                    input "action",                 /* Table name */
                                    input "Action",                  /* CSV filename */
                                    input "for each action").       /* Query */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btActionMail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btActionMail wWin
ON CHOOSE OF btActionMail IN FRAME frAudit /* Mail */
DO:
  if available action and action.uid ne "" 
   then 
    openURL("mailto:" + action.uid).     
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btActionOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btActionOpen wWin
ON CHOOSE OF btActionOpen IN FRAME frAudit /* Open */
DO:
  empty temp-table tempaction.
  empty temp-table tempfinding.
  
  if not available action
   then 
    return.
  
  create tempaction.
  buffer-copy action to tempaction.
  find first tfinding where tfinding.findingid = action.findingid no-error.
  if available tfinding 
   then
    do:
      create tempfinding.
      buffer-copy tfinding to tempfinding.
    end.
    
  run dialogagentaction.w(input table tempaction,
                          input table tempfinding).

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btActionRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btActionRefresh wWin
ON CHOOSE OF btActionRefresh IN FRAME frAudit /* Refresh */
DO:
  run refreshaction in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAr
&Scoped-define SELF-NAME btArMail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btArMail wWin
ON CHOOSE OF btArMail IN FRAME frAr /* Mail */
DO:
  if available tArAging 
   then 
    openURL("mailto:" + cArAgentEmail).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAudit
&Scoped-define SELF-NAME btAuditExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btAuditExport wWin
ON CHOOSE OF btAuditExport IN FRAME frAudit /* Export */
DO:
  run exportData in this-procedure (input browse brwQAR:handle,    /* Borwser handle */
                                    input temp-table audit:handle, /* Temp-table handle */
                                    input "audit",                 /* Table name */
                                    input "Audit",                 /* CSV filename */ 
                                    input "for each audit").       /* Query */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btAuditMail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btAuditMail wWin
ON CHOOSE OF btAuditMail IN FRAME frAudit /* Mail */
DO:
  find current audit.
  openURL("mailto:" + audit.uid).   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btAuditOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btAuditOpen wWin
ON CHOOSE OF btAuditOpen IN FRAME frAudit /* Open */
DO:
  find current audit.
   run selectAudit in this-procedure (audit.qarid).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btAuditRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btAuditRefresh wWin
ON CHOOSE OF btAuditRefresh IN FRAME frAudit /* Refresh */
DO:
  run refreshaudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frClaim
&Scoped-define SELF-NAME btClaimExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClaimExport wWin
ON CHOOSE OF btClaimExport IN FRAME frClaim /* Export */
DO:
  run exportData in this-procedure (input browse brwClaims:handle,      /* Borwser handle */
                                    input temp-table agentclaim:handle, /* Temp-table handle */
                                    input "agentclaim",                 /* Table name */
                                    input "Agentclaim",                 /* CSV filename */
                                    input "for each agentclaim")        /* Query */
                                    .

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btClaimMail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClaimMail wWin
ON CHOOSE OF btClaimMail IN FRAME frClaim /* Mail */
DO:
  find current agentclaim.
  openURL("mailto:" + agentclaim.assignedto).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btClaimRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClaimRefresh wWin
ON CHOOSE OF btClaimRefresh IN FRAME frClaim /* Refresh */
do: 
  run refreshclaim in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frCompliance
&Scoped-define SELF-NAME btComQualExp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btComQualExp wWin
ON CHOOSE OF btComQualExp IN FRAME frCompliance /* Export */
do:
  run exportData in this-procedure (input browse brwQualification:handle,   /* Borwser handle */
                                  input temp-table qualification:handle, /* Temp-table handle */
                                  input "qualification",              /* Table name */
                                  input "Qualification",              /* CSV filename */
                                  input "for each qualification").   /* Query */  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btComQualRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btComQualRefresh wWin
ON CHOOSE OF btComQualRefresh IN FRAME frCompliance /* Refresh */
do:
  lRefreshQualifications = true.
  openQualification().
  lRefreshQualifications = false.
  run enableButtons in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btComReqFulRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btComReqFulRefresh wWin
ON CHOOSE OF btComReqFulRefresh IN FRAME frCompliance /* Refresh */
do:
  lRefreshReqFulFillment = true.
  openReqFulFillment().
  lRefreshReqFulFillment = false.
  run enableButtons in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btComViewQual
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btComViewQual wWin
ON CHOOSE OF btComViewQual IN FRAME frCompliance /* View Qualification */
do:
   run viewComQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btComViewReq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btComViewReq wWin
ON CHOOSE OF btComViewReq IN FRAME frCompliance /* View requirement */
do:
  run viewComRequirement in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAr
&Scoped-define SELF-NAME btFileExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btFileExport wWin
ON CHOOSE OF btFileExport IN FRAME frAr /* Export */
DO:
  run exportData in this-procedure (input browse brwArAging:handle,   /* Borwser handle */
                                    input temp-table tArAging:handle, /* Temp-table handle */
                                    input "tArAging",                 /* Table name */
                                    input "ArAging",                  /* CSV filename */
                                    input "for each tArAging" ).      /* Query */

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btFileOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btFileOpen wWin
ON CHOOSE OF btFileOpen IN FRAME frAr /* Open */
DO:
  run openTranDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btRefreshAr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btRefreshAr wWin
ON CHOOSE OF btRefreshAr IN FRAME frAr /* Refresh */
DO:
  lRefreshAR = true.
  run refreshAR in this-procedure.
  lRefreshAR = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bViewParams
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewParams wWin
ON CHOOSE OF bViewParams IN FRAME fMain /* View Params */
DO:
  run dialogagentsgroup.w(input table tConsolidate,
                              input false /* for editable browse */).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBatch
&Scoped-define SELF-NAME bYear1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bYear1 wWin
ON CHOOSE OF bYear1 IN FRAME frBatch /* Year1 */
or 'CHOOSE' of bYear1
or 'CHOOSE' of bYear2
or 'CHOOSE' of bYear3
DO:
  openBatchesYearly(input self:label).
  cLastRemitClicked = self:label.
  brwBatch:title = "Batches for " + self:label.
  run enableButtons in this-procedure.
  lBatch = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAgents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgents wWin
ON VALUE-CHANGED OF cbAgents IN FRAME frBatch /* Agents */
DO:
  run filterBatches in this-procedure.
  close query brwBatch.      
  open query brwBatch for each batch by batch.agentid by batch.batchid.
  run enableButtons in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAr
&Scoped-define SELF-NAME cbAgentsAR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgentsAR wWin
ON VALUE-CHANGED OF cbAgentsAR IN FRAME frAr /* Agents */
DO:
  displayAR().
  
  if not available tArAgingSummTotal
   then
    return.
    
 run filterArAging in this-procedure(input cLastAgingClicked).   

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAudit
&Scoped-define SELF-NAME cbAgentsAudit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgentsAudit wWin
ON VALUE-CHANGED OF cbAgentsAudit IN FRAME frAudit /* Agents */
DO:
  run filterAudits in this-procedure.
  run filterActions in this-procedure.
  close query brwQAR.
  close query brwAction.
  open query brwQAR for each audit no-lock by audit.agentid by audit.auditStartDate desc. 
  open query brwAction for each action. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frClaim
&Scoped-define SELF-NAME cbAgentsClm
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgentsClm wWin
ON VALUE-CHANGED OF cbAgentsClm IN FRAME frClaim /* Agents */
DO:
  run filterClaims in this-procedure.
  close query brwClaims.
  open query brwClaims for each agentclaim by agentclaim.agentID by agentclaim.claimID.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frCompliance
&Scoped-define SELF-NAME cbAgentsQual
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgentsQual wWin
ON VALUE-CHANGED OF cbAgentsQual IN FRAME frCompliance /* Agents */
DO:
  run filterReqfulfillments in this-procedure.
  run filterQualifications  in this-procedure.
  close query brwqualification.
  close query brwReqFul.
  open query brwqualification for each qualification by qualification.entityID.
  open query brwReqFul for each reqfulfill by reqfulfill.orgRoleID.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAlert
&Scoped-define SELF-NAME fOwner
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fOwner wWin
ON VALUE-CHANGED OF fOwner IN FRAME frAlert /* Owner */
or 'value-changed' of fCode
or 'value-changed' of cbAgentsAlert
DO:
  assign
      cLastAlertOwner = fOwner:input-value
      cLastAlertCode  = fCode:input-value
      cLastAgentID    = cbAgentsAlert:input-value
      .
  run filterAlerts in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frCompliance
&Scoped-define SELF-NAME tShowInactiveQuals
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tShowInactiveQuals wWin
ON VALUE-CHANGED OF tShowInactiveQuals IN FRAME frCompliance /* Show inactive Qualifications */
DO:
  run filterQualifications in this-procedure.
  close query brwqualification.
  open query brwqualification for each qualification by qualification.entityID. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwAction
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm2/windowmn.i}
{lib/win-main.i}
{lib/win-status.i}
{lib/brw-main-multi.i &browse-list="brwConsolidate,brwQAR,brwBatch,brwClaims,brwAlerts,brwReqFul,brwAction,brwActivityBatch,brwActivityPolicies,brwActivityCPL,brwInteraction,brwSentiments,brwObjectives,brwArAging,brwQualification"}

{lib/pbupdate.i "'Retrieving data...'" 5}
{lib/pbupdate.i "'Formatting screen...'" 80}

run initializeObject in this-procedure.

{lib/pbupdate.i "'Finalizing screen...'" 98}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects wWin  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE currentPage  AS INTEGER NO-UNDO.

  ASSIGN currentPage = getCurrentPage().

  CASE currentPage: 

    WHEN 0 THEN DO:
       RUN constructObject (
             INPUT  'adm2/folder.w':U ,
             INPUT  FRAME fMain:HANDLE ,
             INPUT  'FolderLabels':U + 'Consolidation|Remittances|Metrics|A/R|Claims|Audits|Compliance|Alerts' + 'FolderTabWidth20FolderFont-1HideOnInitnoDisableOnInitnoObjectLayout':U ,
             OUTPUT h_folder ).
       RUN repositionObject IN h_folder ( 5.00 , 1.80 ) NO-ERROR.
       RUN resizeObject IN h_folder ( 23.62 , 203.00 ) NO-ERROR.

       /* Links to SmartFolder h_folder. */
       RUN addLink ( h_folder , 'Page':U , THIS-PROCEDURE ).

       /* Adjust the tab order of the smart objects. */
       RUN adjustTabOrder ( h_folder ,
             eResults:HANDLE IN FRAME fMain , 'AFTER':U ).
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changePage wWin 
PROCEDURE changePage :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
  RUN SUPER.
 
  {get CurrentPage activePageNumber}.
  
  iLastPage = activePageNumber.

  case activePageNumber:
  when 1 
   then
    do:   
      view frame frDetail.
      if not lAgentDetail then
      do:           
         displayAgents().
         lAgentDetail = yes.                                               
      end.
      enable all except with frame frDetail.
      setPageState(activePageNumber, "Enabled").        
      hide frame frBatch.        
      hide frame frAr.
      hide frame frClaim.
      hide frame frAudit.
      hide frame frCompliance.
      hide frame frAlert.
      hide frame frActivity.
    end.
   
  when 2
   then
    do:
      hide frame frDetail.        
      view frame frBatch. 
      if not lBatches 
       then
        do:
          run refreshActivityBatch in this-procedure.
          bBatchRefresh:sensitive        = false.
        end. /* if not lBatches */
      enable all except brwActivityBatch bBatchRefresh with frame frBatch.
      setPageState(activePageNumber, "Enabled").
      hide frame frAr.
      hide frame frClaim.
      hide frame frAudit.
      hide frame frCompliance.
      hide frame frAlert.
      hide frame frActivity.
    end.
  when 3
   then
    do:       
      hide frame frDetail.        
      hide frame frBatch.        
      hide frame frAr.        
      hide frame frClaim.        
      hide frame frAudit.                
      hide frame frCompliance.                
      hide frame frAlert.
      view frame frActivity.
      if not lActivity 
       then
        run refreshActivityMetrics in this-procedure.
      enable all except brwActivityPolicies with frame frActivity.
      setPageState(activePageNumber, "Enabled"). 
      brwActivityPolicies:deselect-focused-row() .
      brwActivityCPL:deselect-focused-row() .
    end.  
  when 4
   then
    do:  
      hide frame frDetail.        
      hide frame frBatch.        
      view frame frAr.
      if not lAr 
       then
        do:
         openAr(input false).
         displayAr().                                                   
         lAr = yes.                                               
        end.
      enable all except btRefreshAr with frame frAr .
      setPageState(activePageNumber, "Enabled").
    
      hide frame frClaim.
      hide frame frAudit.        
      hide frame frCompliance.
      hide frame frAffiliation.
      hide frame frActivity. 
      find first arAging no-error.
    end.    
  when 5
   then
    do:
      hide frame frDetail.        
      hide frame frBatch.        
      hide frame frAr.        
      view frame frClaim.
      if not lClaim 
       then
        do:
          if cAgentList ne ""
           then
            do:
              displayClaim().
              openClaim().                                                    
              lClaim = yes.
            end.                                                 
        end.
      enable all except btClaimOpen fLAER with frame frClaim.
      setPageState(activePageNumber, "Enabled").

      hide frame frAudit.        
      hide frame frCompliance.
      hide frame frAlert.
      hide frame frActivity.
    end.
  when 6
   then
    do:
      hide frame frDetail.        
      hide frame frBatch.        
      hide frame frAr.        
      hide frame frClaim.        
      view frame frAudit.
      if not lAudit 
       then
        do:
          if cAgentList ne ""
           then
            do:
              openAudit().
              lAudit = yes.  
            end.
        end.
      enable all with frame frAudit.
      setPageState(activePageNumber, "Enabled").
      hide frame frCompliance.
      hide frame frAlert.
      hide frame frActivity. 
    end.  
   when 7
   then
    do:  
      hide frame frDetail.        
      hide frame frBatch.        
      hide frame frAr.        
      hide frame frClaim.        
      hide frame frAudit.
      view frame frCompliance.
      if not lCompliance 
       then
        do:
          if cAgentList ne ""
           then
            do:
              openQualification().
              openReqFulFillment().                                                     
              lCompliance = yes. 
            end.     
        end.
      enable all with frame frCompliance.
      setPageState(activePageNumber, "Enabled").
      hide frame frAlert.
      hide frame frActivity. 
    end.
   when 8
   then
    do:
      hide frame frDetail.        
      hide frame frBatch.        
      hide frame frAr.        
      hide frame frClaim.        
      hide frame frAudit.                
      hide frame frCompliance.                
      view frame frAlert.
      if not lAlert 
       then
        do:
          if cAgentList ne ""
           then
            do:
              openAlerts().                                                   
              lAlert = yes.
            end.                                                
        end.
      enable all except fCode fOwner with frame frAlert.
      setPageState(activePageNumber, "Enabled").
      hide frame frActivity.
      assign
          fOwner:sensitive    = query brwAlerts:num-results > 0
          fCode:sensitive     = query brwAlerts:num-results > 0
          bExport:sensitive   = query brwAlerts:num-results > 0
          .         
    end.
  end case.
  
  run enableButtons       in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE checkOpenQar wWin 
PROCEDURE checkOpenQar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  define input parameter pAuditID as character no-undo.
  define output parameter qaropen as logical   no-undo.
  define output parameter qarsummaryhandle as handle.

  find first openQars
   where openQars.QarId = integer(pAuditID) no-error.
  if available openQars and valid-handle(openQars.hInstance)
   then
  do:
    assign
        qaropen          = yes
        qarsummaryhandle = openQars.hInstance
        .
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow wWin 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: Procedure used to close window from parent window      
------------------------------------------------------------------------------*/

  publish "windowClosed" (input this-procedure). 
   
  apply "CLOSE":U to this-procedure.
  return.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE createAgentLIPairs wWin 
PROCEDURE createAgentLIPairs :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  for each cAgent:
    assign
        cDispName = if length(cAgent.name)>37 then (substring(cAgent.name,1,37)) else cAgent.name
        cAgentLIPairs = cAgentLIPairs + if cAgentLIPairs = "" then (cDispName + "(" + cAgent.agentID + ")|" + cAgent.agentID) else ("|" + cDispName + "(" + cAgent.agentID + ")|" + cAgent.agentID).
  end.
  cAgentLIPairs = "ALL|ALL" + "|" + cAgentLIPairs.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE emptyTempTable wWin 
PROCEDURE emptyTempTable :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Details Tab */
  lAgentDetail = false.
  empty temp-table cAgent.

  /*Remittance Tab*/
  lBatches = false.
  empty temp-table tActivity.
  empty temp-table Batch.
  close query brwActivityBatch.
  close query brwBatch.
  
  /* Metrics Tab */
  lActivity = false.
  empty temp-table activityMetrics.
  empty temp-table tMetricsActivity.
  close query brwActivityPolicies. 
  close query brwActivityCPL.      
  
  /*AR Tab*/
  lAr = false.
  empty temp-table arAging.
  empty temp-table ttArAging.
  empty temp-table tArAging.
  close query brwArAging. 
  
  /*Claims Tab*/
  lClaim = false.
  close query brwClaims.
  empty temp-table agentclaim.
  empty temp-table claimcostssummary.
  
  /*Audits Tab*/
  lAudit = false.
  empty temp-table audit.
  empty temp-table action.
  empty temp-table finding.
  
  /*Compliance Tab*/
  lCompliance = false.
  empty temp-table reqfulfill.  
  empty temp-table qualification.

  /*Alerts Tab*/
  lAlert = false.
  empty temp-table alert.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableButtons wWin 
PROCEDURE enableButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  case activePageNumber:
  
   when 2 
    then 
     do with frame frBatch:
       assign
           bBatchExport:sensitive         = if brwBatch:num-iterations <= 0 then false else true
           bbatchopen  :sensitive         = bBatchExport:sensitive
           cbAgents:sensitive             = can-find(first tBatch)
           bActivityBatchExport:sensitive = if brwActivityBatch:num-iterations <= 0 then false else true
           .
     end.
    
   when 3
    then
     do with frame frActivity:
       assign
           bActivityExp   :sensitive = if brwActivityPolicies:num-iterations <= 0 then false else true
           bActivityExpCPL:sensitive = if brwActivityCPL     :num-iterations <= 0 then false else true
           .
     end.
   when 4
    then
     do with frame frAr:
       assign
           btFileExport:sensitive = if brwArAging:num-iterations <= 0 then false else true
           btFileOpen  :sensitive = btFileExport:sensitive
           btArMail    :sensitive = btFileExport:sensitive
           cbAgentsAR:sensitive     = can-find(first ttARAging) 
           .
     end.
     
   when 5 
    then
     do with frame frClaim:
       assign 
           btClaimExport:sensitive = if brwClaims:num-iterations <= 0 then false else true
           btClaimMail  :sensitive = btClaimExport:sensitive
           cbAgentsClm:sensitive = can-find(first tAgentclaim)
           .
     end.
    
   when 6 
    then  
     do with frame frAudit:
       assign
           btAuditExport:sensitive = if brwQAR:num-iterations <= 0 then false else true
           btAuditOpen  :sensitive = btAuditExport:sensitive
           btAuditMail  :sensitive = btAuditExport:sensitive
           cbAgentsAudit:sensitive =   can-find(first tAudit)
         
           btActionExport:sensitive = if brwAction:num-iterations <= 0 then false else true
           btActionOpen  :sensitive = btActionExport:sensitive
           btActionMail  :sensitive = btActionExport:sensitive
           .
     end.
   when 7
    then
     do with frame frCompliance:
       if not can-find(first tqualification) 
        then
         tShowInactiveQuals:sensitive = false.
        else
         tShowInactiveQuals:sensitive = true.
         
       assign
            BtComViewQual:sensitive  = if brwQualification:num-iterations <= 0 then false else true
            btComQualExp:sensitive   = BtComViewQual:sensitive
            cbAgentsQual:sensitive   = can-find(first tqualification)
            
            BtComViewReq:sensitive   = if brwReqful:num-iterations <= 0 then false else true
            bReqFulExport:sensitive  = BtComViewReq:sensitive
            .
                 
     end.
   when 8
    then
     do with frame frAlert:
       assign
            cbAgentsAlert:sensitive   = can-find(first ttAlert)
            fCode:sensitive    = cbAgentsAlert:sensitive
            fOwner:sensitive   = cbAgentsAlert:sensitive
            .
                 
     end.  
  end case.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableObject wWin 
PROCEDURE enableObject :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
  RUN SUPER.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY eParams eResults 
      WITH FRAME fMain IN WINDOW wWin.
  ENABLE bSaveConsolidation RECT-68 eParams eResults bViewParams bRefresh 
      WITH FRAME fMain IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW FRAME frActivity IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frActivity}
  DISPLAY cbAgentsAlert fCode fOwner 
      WITH FRAME frAlert IN WINDOW wWin.
  ENABLE brwAlerts 
      WITH FRAME frAlert IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frAlert}
  FRAME frAlert:SENSITIVE = NO.
  DISPLAY cbAgentsAR 
      WITH FRAME frAr IN WINDOW wWin.
  ENABLE b30to60 b60to90 bbalance bCurrentaging bover90 brwArAging 
      WITH FRAME frAr IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frAr}
  DISPLAY cbAgentsAudit 
      WITH FRAME frAudit IN WINDOW wWin.
  ENABLE brwQAR brwAction 
      WITH FRAME frAudit IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frAudit}
  DISPLAY cbAgents 
      WITH FRAME frBatch IN WINDOW wWin.
  ENABLE brwBatch bYear1 bYear2 bYear3 
      WITH FRAME frBatch IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frBatch}
  FRAME frBatch:SENSITIVE = NO.
  DISPLAY netfromLTD costfromLTD errorfromLTD cbAgentsClm fLAER fLAEL fLossR 
          fLossL fRecoveriesR fTotalCostL costnetLTD errornetLTD fTotal 
      WITH FRAME frClaim IN WINDOW wWin.
  ENABLE netfromLTD costfromLTD errorfromLTD brwClaims fLAER fLAEL fLossR 
         fLossL fRecoveriesR fTotalCostL costnetLTD errornetLTD fTotal 
      WITH FRAME frClaim IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frClaim}
  DISPLAY cbAgentsQual tShowInactiveQuals 
      WITH FRAME frCompliance IN WINDOW wWin.
  ENABLE tShowInactiveQuals brwQualification brwReqFul 
      WITH FRAME frCompliance IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frCompliance}
  FRAME frCompliance:SENSITIVE = NO.
  ENABLE brwConsolidate 
      WITH FRAME frDetail IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frDetail}
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exitObject wWin 
PROCEDURE exitObject PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:  Window-specific override of this procedure which destroys 
            its contents and itself.
    Notes:  
------------------------------------------------------------------------------*/

  /* Close the window and its all child instances */
  run closeWindow in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData wWin 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter haBrowser  as handle    no-undo.
  define input parameter haTable    as handle    no-undo.
  define input parameter cTableName as character no-undo.
  define input parameter cFileName  as character no-undo.
  define input parameter cQuery     as character no-undo.
  
  define variable cFields    as character no-undo.
  define variable cFieldName as character no-undo.
  
  /* Get column and field name of a browser */
  do std-in = 1 to haBrowser:num-columns:
    std-ha = haBrowser:get-browse-column(std-in).
    assign
        cFields    = cFields    + (if cFields    > "" then "," else "") + std-ha:name
        cFieldName = cFieldName + (if cFieldName > "" then "," else "") + replace(std-ha:label,"!"," ")
        .
  end.

  if haBrowser:query:num-results = ? or haBrowser:query:num-results = 0
   then
    do:
      message "There is nothing to export"
        view-as alert-box warning buttons ok.
      return.
    end.
  
  publish "GetReportDir" (output std-ch).

  run util/exporttable.p (table-handle haTable,
                          cTableName,
                          cQuery,
                          cFields,
                          cFieldName,
                          std-ch,
                          cFileName + "-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterActions wWin 
PROCEDURE filterActions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* get filtered actions */
   empty temp-table action.
   for each tFinding where tFinding.entityID = if cbAgentsAudit:input-value in frame frAudit = "ALL" then tFinding.entityID else cbAgentsAudit:input-value:
     for each tAction where tAction.sourceID = tFinding.sourceID and
                            tAction.stat = 'O':
       if not can-find(first action where action.actionID = tAction.actionID)
        then
         do:
           create action.
           buffer-copy tAction to action.
         end.
     end.
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterAlerts wWin 
PROCEDURE filterAlerts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttAlert for ttAlert.
  
  do with frame frAlert:
  end.
  
  close query brwAlerts.
  empty temp-table alert.

  for each ttAlert
    where ttAlert.owner = (if fOwner:screen-value = "ALL" then ttAlert.owner
                           else fOwner:screen-value)       and
          ttAlert.processCode = (if fCode:screen-value = "ALL" then ttAlert.processCode
                           else fCode:screen-value)        and
          ttAlert.agentID = if cbAgentsAlert:input-value in frame frAlert = "ALL" then ttAlert.agentID else cbAgentsAlert:input-value:
    create alert.
    buffer-copy ttAlert to alert.    
  end.

  open query brwAlerts for each alert by Alert.agentID by Alert.dateCreated.
  
  bExport:sensitive   = query brwAlerts:num-results > 0.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterArAging wWin 
PROCEDURE filterArAging :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcBucket as character no-undo.

  empty temp-table tArAging.
  
  do with frame frAr:
  end.
  
  
  case ipcBucket:
    when "Current" 
     then
      do:
        brwArAging:title = "Files in Current Category".
        for each ttArAging 
          where ttArAging.agentID = (if cbAgentsAR:input-value in frame frAR = "ALL" then ttArAging.agentID else cbAgentsAR:input-value)     and
                ttArAging.due0_30 <> 0:
          create tArAging.
          buffer-copy ttArAging to tArAging.
          if ttaraging.type = "P" and ttArAging.fileNumber = "" 
           then
            tArAging.fileNumber = "Unapplied Payment".
        end.
      end.
    when "30to60" 
     then
      do:
        brwArAging:title = "Files in 31-60 Category".
        for each ttArAging 
          where ttArAging.agentID = (if cbAgentsAR:input-value in frame frAR = "ALL" then ttArAging.agentID else cbAgentsAR:input-value)     and 
                ttArAging.due31_60 <> 0:
          create tArAging.
          buffer-copy ttArAging to tArAging.
          if ttaraging.type = "P" and ttArAging.fileNumber = "" 
           then
            tArAging.fileNumber = "UnApplied Payment".
        end.
      end.  
    when "60to90" 
     then
      do:
        brwArAging:title = "Files in 61-90 Category".
        for each ttArAging 
          where ttArAging.agentID = (if cbAgentsAR:input-value in frame frAR = "ALL" then ttArAging.agentID else cbAgentsAR:input-value)     and 
                ttArAging.due61_90 <> 0:
          create tArAging.
          buffer-copy ttArAging to tArAging.
          if ttaraging.type = "P" and ttArAging.fileNumber = "" 
           then
            tArAging.fileNumber = "UnApplied Payment".
        end.
      end.
    when "over90" 
     then
      do:
        brwArAging:title = "Files in Over 90 Category".
        for each ttArAging 
          where ttArAging.agentID = (if cbAgentsAR:input-value in frame frAR = "ALL" then ttArAging.agentID else cbAgentsAR:input-value)     and 
                ttArAging.due91_ <> 0:
          create tArAging.
          buffer-copy ttArAging to tArAging.
          if ttaraging.type = "P" and ttArAging.fileNumber = "" 
           then
            tArAging.fileNumber = "UnApplied Payment".  
        end.
      end.
    when "balance" 
     then
      do:
        brwArAging:title = "Files in All Category".
        for each ttArAging 
          where ttArAging.agentID = (if cbAgentsAR:input-value in frame frAR = "ALL" then ttArAging.agentID else cbAgentsAR:input-value)     and 
                ttArAging.balance <> 0:
          create tArAging.
          buffer-copy ttArAging to tArAging.
          if ttaraging.type = "P" and ttArAging.fileNumber = "" 
           then
            tArAging.fileNumber = "UnApplied Payment".
        end.
      end.
  end case.

  open query brwArAging for each tArAging. 
  
  run enableButtons in this-procedure.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterAudits wWin 
PROCEDURE filterAudits :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Update the local copy of data. */ 
  empty temp-table audit.
  for each taudit where tAudit.agentID = if cbAgentsAudit:input-value in frame frAudit = "ALL" then tAudit.agentID else cbAgentsAudit:input-value:
    create audit.
    buffer-copy taudit to audit.
    assign
        audit.stat      = getAuditStatus(audit.stat)
        audit.audittype = getAuditType(audit.audittype)
        audit.errtype   = getErrorType(audit.errtype)
        .
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterBatches wWin 
PROCEDURE filterBatches :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table batch.
  
  for each tBatch where tBatch.agentid = (if cbAgents:input-value in frame frBatch = "ALL" then tBatch.agentid else cbAgents:input-value):
    create batch.
    buffer-copy tBatch to batch.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterClaims wWin 
PROCEDURE filterClaims :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iLAER              as integer no-undo.
  define variable iLAEL              as integer no-undo.
  define variable iLossR             as integer no-undo.
  define variable iLossL             as integer no-undo.
  define variable iRecoveriesR       as integer no-undo.
  define variable iTotalCostL        as integer no-undo.
  define variable iCount             as integer no-undo.
  
  empty temp-table agentclaim.
  /* Update the local copy of data. */ 
  iCount = 0.

  for each tAgentclaim where tAgentclaim.agentID = if cbAgentsClm:input-value in frame frClaim = "ALL" then tAgentclaim.agentID else cbAgentsClm:input-value:
    create agentclaim.
    buffer-copy tAgentclaim to agentclaim.
    
    publish "getSysUserName" (input agentclaim.assignedTo, output agentclaim.assignedToDesc).
    agentClaim.costsPaid = agentclaim.laeLTD + agentclaim.lossLTD - agentclaim.recoveries.
    assign
        iCount       = iCount + 1
        iLAER        = iLAER        + agentclaim.laeReserve   
        iLAEL        = iLAEL        + agentclaim.laeLTD       
        iLossR       = iLossR       + agentclaim.lossReserve  
        iLossL       = iLossL       + agentclaim.lossLTD      
        iRecoveriesR = iRecoveriesR + agentclaim.recoveries   
        iTotalCostL  = iTotalCostL  + agentclaim.costsPaid    
        .
  end.
       
  
  open query brwClaims for each agentclaim by agentclaim.agentID by agentclaim.claimID.

  assign
      fTotal      :screen-value in frame frclaim = "Total of " + string(iCount) + " Claim(s)"
      fLAER       :screen-value = if iLAER        = 0 then "" else string(iLAER)
      fLAEL       :screen-value = if iLAEL        = 0 then "" else string(iLAEL)
      fLossR      :screen-value = if iLossR       = 0 then "" else string(iLossR)
      fLossL      :screen-value = if iLossL       = 0 then "" else string(iLossL)
      fRecoveriesR:screen-value = if iRecoveriesR = 0 then "" else string(iRecoveriesR)
      fTotalCostL :screen-value = if iTotalCostL  = 0 then "" else string(iTotalCostL)
      .   
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterQualifications wWin 
PROCEDURE filterQualifications :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table qualification.
  
  for each cAgent no-lock where cAgent.agentID = if cbAgentsQual:input-value in frame frCompliance = "ALL" then cAgent.agentID else cbAgentsQual:input-value:
    for each tqualification where tqualification.entityID = cAgent.orgID  and
                                  tqualification.stateId = cAgent.stateID and
                                  tQualification.activ   = (if tShowInactiveQuals:input-value in frame frCompliance eq yes then tQualification.activ else yes):
      create qualification.
      buffer-copy tqualification to qualification.
      qualification.stat = getQualificationDesc(qualification.stat).  
    end.
  end.
  
  run enableButtons in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterReqfulfillments wWin 
PROCEDURE filterReqfulfillments :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table reqfulfill.    
  for each cAgent no-lock where cAgent.agentID = if cbAgentsQual:input-value in frame frCompliance = "ALL" then cAgent.agentID else cbAgentsQual:input-value:
    for each treqfulfill no-lock where treqfulfill.orgRoleID = cAgent.orgRoleID:
      create reqfulfill.
      buffer-copy treqfulfill to reqfulfill.
    
      reqfulfill.stat = getQualificationDesc(reqfulfill.stat).
     
      if reqfulfill.appliesTo = {&OrganizationCode}
       then
        reqfulfill.appliesTo = {&Organization}.   
       else if reqfulfill.appliesTo = {&personcode}
        then
        reqfulfill.appliesTo = {&Person}.
    end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getAgentActivity wWin 
PROCEDURE getAgentActivity :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter iplRefresh    as logical   no-undo.
  
  define variable        iYear         as integer   no-undo.
  define variable        cCategoryList as character no-undo.
  
  empty temp-table activityMetrics. 
  empty temp-table tMetricsActivity. 
  
  cCategoryList = "T,P". /* T = CPL Issued net,
                            P = policies Issued */ 
  
  /* Populating year data in widgets */
  iYear = year(today).   
  openActivityMetrics(iYear,cCategoryList,iplRefresh).
  
  for each activityMetrics by activityMetrics.year:
    if can-do( "A,I,C,P",activityMetrics.type) 
     then
    do:
      create tMetricsActivity.
      buffer-copy activityMetrics to tMetricsActivity.
    end.
  end.

  open query brwActivityPolicies for each tMetricsActivity where tMetricsActivity.category = "P" by tMetricsActivity.year by tMetricsActivity.seq.
  open query brwActivityCPL      for each tMetricsActivity where tMetricsActivity.category = "T" by tMetricsActivity.year by tMetricsActivity.seq.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getAlertDesc wWin 
PROCEDURE getAlertDesc :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cValue as character no-undo.
  
  for each ttAlert:
    publish "GetSysCodeDesc" ("Alert", ttAlert.processcode, output cValue).
    ttAlert.processCodeDesc = cValue.
    publish "GetSysPropDesc" ("AMD", "Alert", "Owner", ttAlert.owner, output cValue).
    ttAlert.ownerDesc = cValue.
    publish "GetSysUserName" (ttAlert.createdBy, output cValue).
    ttAlert.createdByDesc = cValue.
    /* set the severity */
    publish "GetSysPropDesc" ("AMD", "Alert", "Severity", ttAlert.severity, output ttalert.severityDesc).
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getAr wWin 
PROCEDURE getAr :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcBucket as character no-undo.
  
  define variable tLoadedARAgingDetailed as logical no-undo.
 
  find first ttArAging no-error.
  if not available ttArAging 
   then
    do:
      run server/queryagentgrpfileaging.p (input datetime(today),
                                           input "D",
                                           input no,
                                           input pStateID,
                                           input pYearOfSign,
                                           input pSoftware,
                                           input pCompany,
                                           input pOrganization,
                                           input pManager,
                                           input pTagList,
                                           input pAffiliationList,
                                           output table ttARAging,
                                           output cArAgentEmail,
                                           output tLoadedARAgingDetailed,
                                           output std-ch).


      if not tLoadedARAgingDetailed
       then
        do:
           message std-ch
            view-as alert-box.
           return. 
        end.
        
      btRefreshAr:sensitive in frame frAr = true.  
    end.
    
  run filterArAging in this-procedure(input ipcBucket).
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initializeObject wWin 
PROCEDURE initializeObject PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/     
  {&window-name}:max-width-pixels = session:width-pixels.
  {&window-name}:max-height-pixels = session:height-pixels.
  {&window-name}:min-width-pixels = {&window-name}:width-pixels.
  {&window-name}:min-height-pixels = {&window-name}:height-pixels.

  {lib/set-button.i &frame-name=fMain &label="Refresh"      &toggle=false}
  {lib/set-button.i &frame-name=fMain &b=bViewParams  &image="images/magnifier.bmp"     &inactive="images/magnifier-i.bmp"}
  {lib/set-button.i &frame-name=fMain &b=bSaveConsolidation  &image="images/save.bmp"     &inactive="images/save-i.bmp"}
  
  /* Remittance */                                                                                             
  {lib/set-button.i &frame-name=frBatch &label="ActivityBatchRefresh" &image="images/s-refresh.bmp"   &toggle=false}
  {lib/set-button.i &frame-name=frBatch &label="ActivityBatchExport"  &image="images/s-excel.bmp"     &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frBatch &label="BatchExport"          &image="images/s-excel.bmp"     &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frBatch &label="BatchOpen"            &image="images/s-magnifier.bmp" &inactive="images/s-magnifier-i.bmp"}
  {lib/set-button.i &frame-name=frBatch &label="BatchRefresh"         &image="images/s-refresh.bmp"   &inactive="images/s-refresh-i.bmp" &toggle=false}

  /* AR */                                                                                                     
  {lib/set-button.i &frame-name=frAr &b=btFileExport &image="images/s-excel.bmp"     &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frAr &b=btFileOpen   &image="images/s-magnifier.bmp" &inactive="images/s-magnifier-i.bmp"}
  {lib/set-button.i &frame-name=frAr &b=btArMail     &image="images/s-email.bmp"     &inactive="images/s-email-i.bmp"}
  {lib/set-button.i &frame-name=frAr &b=btRefreshAr  &image="images/s-refresh.bmp"   &inactive="images/s-refresh-i.bmp" &toggle=false}

  /* Claim */                                                                                                  
  {lib/set-button.i &frame-name=frClaim &b=btClaimRefresh  &image="images/s-refresh.bmp"   &inactive="images/s-refresh-i.bmp" &toggle=false}
  {lib/set-button.i &frame-name=frClaim &b=btClaimExport  &image="images/s-excel.bmp"     &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frClaim &b=btClaimOpen    &image="images/s-magnifier.bmp" &inactive="images/s-magnifier-i.bmp"}
  {lib/set-button.i &frame-name=frClaim &b=btClaimMail    &image="images/s-email.bmp"     &inactive="images/s-email-i.bmp"}


  /* Audit */                                                                                                  
  {lib/set-button.i &frame-name=frAudit &b=btAuditRefresh   &image="images/s-refresh.bmp"   &inactive="images/s-refresh-i.bmp" &toggle=false}
  {lib/set-button.i &frame-name=frAudit &b=btActionRefresh  &image="images/s-refresh.bmp"   &inactive="images/s-refresh-i.bmp" &toggle=false}
  {lib/set-button.i &frame-name=frAudit &b=btAuditExport    &image="images/s-excel.bmp"     &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frAudit &b=btActionExport   &image="images/s-excel.bmp"     &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frAudit &b=btAuditMail      &image="images/s-email.bmp"     &inactive="images/s-email-i.bmp"}
  {lib/set-button.i &frame-name=frAudit &b=btActionMail     &image="images/s-email.bmp"     &inactive="images/s-email-i.bmp"}
  {lib/set-button.i &frame-name=frAudit &b=btAuditOpen      &image="images/s-magnifier.bmp" &inactive="images/s-magnifier-i.bmp"}
  {lib/set-button.i &frame-name=frAudit &b=btActionOpen     &image="images/s-magnifier.bmp" &inactive="images/s-magnifier-i.bmp"}

  /* Compliance */                                                                                             
  {lib/set-button.i &frame-name=frCompliance &b=btComReqFulRefresh &image="images/s-refresh.bmp"   &inactive="images/s-refresh-i.bmp" &toggle=false}
  {lib/set-button.i &frame-name=frCompliance &b=btComQualRefresh   &image="images/s-refresh.bmp"   &inactive="images/s-refresh-i.bmp" &toggle=false}
  {lib/set-button.i &frame-name=frCompliance &b=bReqFulExport      &image="images/s-excel.bmp"     &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frCompliance &b=btComQualExp       &image="images/s-excel.bmp"     &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frCompliance &b=btComViewreq       &image="images/s-magnifier.bmp" &inactive="images/s-magnifier-i.bmp"}
  {lib/set-button.i &frame-name=frCompliance &b=btComViewQual      &image="images/s-magnifier.bmp" &inactive="images/s-magnifier-i.bmp"}
  {lib/set-button.i &frame-name=frCompliance &b=bOrgLookup         &image="images/s-update.bmp"    &inactive="images/s-update-i.bmp"}

  /* Alert */                                                                                           
  {lib/set-button.i &frame-name=frAlert &b=bRefreshAlert &image="images/s-refresh.bmp"   &toggle=false}
  {lib/set-button.i &frame-name=frAlert &label="Export"  &useSmall=true}
  {lib/set-button.i &frame-name=frAlert &label="Modify"  &useSmall=true}
  {lib/set-button.i &frame-name=frAlert &label="NewNote" &useSmall=true}
  
  /* Activity */                                                                                           
  {lib/set-button.i &frame-name=frActivity &label="ActivityMetricsRefresh"    &image="images/s-refresh.bmp" &toggle=false}
  {lib/set-button.i &frame-name=frActivity &label="ActivityMetricsRefreshCPL" &image="images/s-refresh.bmp" &toggle=false}
  {lib/set-button.i &frame-name=frActivity &label="ActivityExp"               &image="images/s-excel.bmp"   &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frActivity &label="ActivityExpCPL"            &image="images/s-excel.bmp"   &inactive="images/s-excel-i.bmp"}
    
  setButtons().
      
  publish "GetAppCode" (output cAppCode).
  publish "GetCredentialsName" (output cCurrUser).
  publish "GetCredentialsID" (output cCurrentUID).

  /* Populating year data in widgets */
  std-in = year(today).   
  do iCount = 0 to 2:
    case iCount:
      when 0 then bYear1:label = string(std-in - iCount).
      when 1 then bYear2:label = string(std-in - iCount).
      when 2 then bYear3:label = string(std-in - iCount).
    end case.  
  end.
  {lib/pbupdate.i "'Getting users...'" 86}

  {lib/pbupdate.i "'Displaying file data...'" 96}
  
  lAgentDetail = yes .

  displayAgents().

  run createAgentLIPairs in this-procedure.

  run windowResized in this-procedure. 
  run super. 
  run selectPage(1).
  displayHeader().  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE loadAlerts wWin 
PROCEDURE loadAlerts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tLoadedAlerts as logical no-undo.
  define buffer ttAlert for ttAlert.
  
  do with frame frAlert:
  end.
  
  run server/getagentgroupalerts.p( 
                               input "",    /* Code, "" for All */
                               input "O",    /* (O)pen Status */
                               input pStateID,
                               input pYearOfSign,
                               input pSoftware,
                               input pCompany,
                               input pOrganization,
                               input pManager,
                               input pTagList,
                               input pAffiliationList,
                               output table ttAlert,
                               output tLoadedAlerts,
                               output std-ch
                              ).
                                   
  if not tLoadedAlerts
   then
    do:
      message std-ch 
        view-as alert-box.
      return.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openBatchDetails wWin 
PROCEDURE openBatchDetails :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available batch 
   then return.
  
  publish "SetCurrentValue" ("AppName",  cAppCode).
  publish "OpenWindow" (input "wBatchDetail",
                        input string(batch.batchID),
                        input "wbatchdetail.w",
                        input "character|input|" + string(batch.batchID) + "^character|input|" + "" + "^character|input|" + batch.agentID,                                   
                        input this-procedure).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openTranDetail wWin 
PROCEDURE openTranDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  if not available tArAging
   then 
    return.
  
  publish "OpenWindow" (input "wtransactiondetail",     /*childtype*/
                         input string(/* artran */ tArAging.artranID),   /*childid*/
                         input "wtransactiondetail.w",    /*window*/
                         input "integer|input|" + string(tArAging.artranID)  + "^integer|input|0^character|input|", /*parameters*/                               
                         input this-procedure).           /*currentProcedure handle*/ 
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshaction wWin 
PROCEDURE refreshaction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer taction for taction.
  define buffer tFinding for tFinding.
  
  define variable tLoadedActions as logical no-undo.
  
  run server\getagentgroupactions.p (input   0 ,  /*piFindingID */
                           input   "",            /*pcStat -- Only 'closed' findings */
                           input   "O",           /*pcStatus -- Action stat*/
                           input   "C",           /* pcAction -- Action type */
                           input   0,             /* piYear */
                           input   "Agent",       /* entity */
                           input   cAgentList,    /* entityID */
                           input   "",            /*pcOwner*/
                           output  table  taction,
                           output  table  tfinding,
                           output tLoadedActions,
                           output std-ch).
                                   
  if not tLoadedActions
   then
    do:
      message std-ch 
        view-as alert-box.
      return.  
    end.
  
  run filterActions in this-procedure.
  
  open query brwAction for each action.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshActivityBatch wWin 
PROCEDURE refreshActivityBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if cAgentList ne ""
   then
    do:
      empty temp-table tActivity.
      empty temp-table activity.
      empty temp-table ttActivity.
      
      std-in = year(today).
      openActivity(input std-in, input "N", false).
      
      for each activity:
        create tActivity.
        buffer-copy activity to tActivity.
      end.
              
      iMonth = month(today).
       
      hColumn = brwActivityBatch:get-browse-column(15) in frame frBatch.
              
      if valid-handle(hColumn)
       then
        hColumn:label  = "YTD " + getMonthName(iMonth).  
      
      open query brwActivityBatch for each tActivity by tActivity.year by tActivity.seq.
              
      run enableButtons in this-procedure.
      
      assign
          cbAgents:delimiter in frame frBatch = "|"
          cbAgents:list-item-pairs  = cAgentLIPairs
          cbAgents:screen-value = "ALL"
          .  
      lBatches = yes.
    end. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshActivityMetrics wWin 
PROCEDURE refreshActivityMetrics :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
      run getAgentActivity in this-procedure(input true).
      iMonth = month(today).
      
      hColumn = brwActivityPolicies:get-browse-column(16) in frame frActivity.
          
      if valid-handle(hColumn)
       then
        hColumn:label  = "YTD " + getMonthName(iMonth).
        
      hColumn = brwActivityCPL:get-browse-column(16).
          
      if valid-handle(hColumn)
       then
        hColumn:label  = "YTD " + getMonthName(iMonth).      
          
      lActivity = yes.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshAR wWin 
PROCEDURE refreshAR :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    
  if cLastAgingClicked = ""
   then
    cLastAgingClicked = "Current".
    
  openAr(input true).
  displayAR().
  
  if not available tArAgingSummTotal 
   then
    return.
    
  if lArAging
   then
    do:
      empty temp-table ttArAging.
      run getAr in this-procedure(input cLastAgingClicked).                                    
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshaudit wWin 
PROCEDURE refreshaudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer taudit for taudit.
  define variable tLoadedAudits as logical no-undo.
                              
  run server/getagentgroupallaudits.p (
                             input 0,         /* year */
                             input "ALL",     /* auditor */
                             input "ALL",     /* status */
                             input 0,         /* start gap */
                             input 0,         /* audit gap */
                             input pStateID,
                             input pYearOfSign,
                             input pSoftware,
                             input pCompany,
                             input pOrganization,
                             input pManager,
                             input pTagList,
                             input pAffiliationList,
                             output table taudit,
                             output tLoadedAudits,
                             output std-ch
                             ).                              
                                    
   if not tLoadedAudits
    then
     do:
       message std-ch 
         view-as alert-box.
       return.  
     end.
     
  for each tAudit:
     taudit.cQarID = string(taudit.qarID).
   end.   
   
  run filterAudits in this-procedure.
  
  open query brwQAR for each audit no-lock by audit.agentid by audit.auditStartDate desc.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshclaim wWin 
PROCEDURE refreshclaim :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  lRefreshClaims = true.

  openClaim().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData wWin 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipiActivePage as integer no-undo.

  run emptyTempTable in this-procedure.
  
  run server/getConsolidatedAgents.p(input table tConsolidate,
                                     output std-in,
                                     output table cAgent,
                                     output std-lo,
                                     output std-ch
                                     ).    
  case ipiActivePage:
    when 1
     then
      do with frame frDetail:
        lRefresh = true.
      end.
    when 2
     then
      do with frame frBatch:
        apply 'choose' to bActivityBatchRefresh.
        if lBatch 
         then
          apply 'choose' to bBatchRefresh .
      end.
    when 3
     then
      do with frame frActivity:
        apply 'choose' to bActivityMetricsRefresh.
      end.
    when 4
     then
      do with frame frAr:
        apply 'choose' to btRefreshAr.
      end.
    when 5
     then
      do with frame frClaim: 
        lRefreshClaimsSummary = true.
        displayclaim().
        apply 'choose' to btClaimRefresh.
      end.
    when 6
     then
      do with frame frAudit:
        apply 'choose' to btAuditRefresh.
        apply 'choose' to btActionRefresh.
      end.
    when 7
     then
      do with frame frCompliance:
        apply 'choose' to btComQualRefresh.
        apply 'choose' to btComReqFulRefresh.
      end.
    when 8
     then
      do with frame frAlert:
        apply 'choose' to bRefreshAlert.
      end.
  end case.
  lRefresh = false.
  run enableButtons in this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE selectAudit wWin 
PROCEDURE selectAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter iQarID as integer no-undo.
  define variable qarsummaryhandle as handle no-undo.
  
  find first audit where audit.QarId = iQarID no-error.
  if not available audit 
   then
    return.
            
  create ttaudit.
  assign
    ttaudit.QarID  =      audit.QarID  
    ttaudit.agentID =     audit.agentID  
    ttaudit.stat =        audit.stat
    ttaudit.auditScore =  audit.auditScore
    ttaudit.name =        audit.name 
    ttaudit.addr =        audit.addr 
    ttaudit.city =        audit.city 
    ttaudit.state =       audit.state 
    ttaudit.zip =         audit.zip 
    .

  run checkOpenQar(input audit.QarId,
                          output std-lo,
                          output qarsummaryhandle ).
  if std-lo
   then
    do:
       run ViewWindow in qarsummaryhandle no-error.
      empty temp-table ttaudit.
      return.
    end.
  else
    do:
      
      if audit.audittype = "U" then
        run QURSummary.w persistent ( input table ttaudit ).
      else
        run QARSummary.w persistent ( input table ttaudit ).
       run ViewWindow in qarsummaryhandle no-error.   
      
      empty temp-table ttaudit.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE selectPage wWin 
PROCEDURE selectPage :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter piPageNum as integer no-undo.
  iLastPage = piPageNum.
  
  RUN SUPER( INPUT piPageNum).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setAlertCombos wWin 
PROCEDURE setAlertCombos :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frAlert:
  end.
  
  /* Alert type combo */
  fCode:list-item-pairs = "ALL,ALL".

  for each ttAlert break by ttAlert.processcode:
    if first-of(ttAlert.processcode) 
     then
      fCode:list-item-pairs = fCode:list-item-pairs + "," + ttAlert.processCodeDesc + "," + ttAlert.processcode.
  end.
  fCode:screen-value = if cLastAlertCode = ? then "ALL" else cLastAlertCode.

  
  /* Owner combo */
  fOwner:list-item-pairs = "ALL,ALL".

  for each ttAlert break by ttAlert.owner:
    if first-of(ttAlert.owner) 
     then
      fOwner:list-item-pairs = fOwner:list-item-pairs + "," + ttAlert.ownerDesc + "," + ttAlert.owner.
  end.
  fOwner:screen-value = if cLastAlertOwner = ? then "ALL" else cLastAlertOwner.
  
  /*AgentID*/
  assign
      cbAgentsAlert:delimiter in frame frAlert    = "|"
      cbAgentsAlert:list-item-pairs               = cAgentLIPairs
      cbAgentsAlert:screen-value                  = if cLastAgentID = ? then "ALL" else cLastAgentID
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setContainerState wWin 
PROCEDURE setContainerState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  define input parameter pContainer as handle no-undo.
  define input parameter pState     as character no-undo.
  
  define variable hObject as handle no-undo.
  define variable hChild  as handle no-undo.

  if valid-handle(pContainer)
   then
    run setWidgetState (pContainer, pState).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState wWin 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pObject as handle    no-undo.
  define input parameter pState  as character no-undo.
  
  define variable hNextObject    as handle    no-undo.

  /* Drill down child object chain first */
  hNextObject = pObject:first-child no-error.
  
  if valid-handle(hNextObject)
   then
    run setWidgetState (hNextObject, pState).
    
  /* Check for siblings and their children.
  */
  hNextObject = pObject:next-sibling no-error.
  if valid-handle(hNextObject) 
   then
    do:
      /* Do not process any sibling frames or their children */
      if hNextObject:type <> "FRAME" then
      run setWidgetState (hNextObject, pState).
    end.
  
  /* This object does not have any children or siblings, or the children or
  siblings have already been processed in the recursive call. Process
  object information now.
  */
  
  if pObject:type <> ? 
   then /* Should never be the case, but just to be safe */
    case pState:
      when "Enabled" then
       do:
         if lookup(pObject:type,"EDITOR,FILL-IN") <> 0 then
         do:        
           if pObject:private-data = "READ-ONLY" then
           assign
             pObject:sensitive = yes
             pObject:read-only = yes.
           else        
           assign
             pObject:sensitive = yes
             pObject:read-only = no.
         end.
         
         if lookup(pObject:type,"COMBO-BOX,RADIO-SET,SELECTION-LIST,TOGGLE-BOX") <> 0 then
         do:
           if pObject:private-data = "READ-ONLY" then
           assign
             pObject:sensitive = no.
           else
           assign
             pObject:sensitive = yes.
         end.
       end.
      when "Disabled" then
       do:
         if lookup(pObject:type,"EDITOR,FILL-IN") <> 0 then
         do:
           assign
             pObject:sensitive = yes
             pObject:read-only = yes.
         end.
         
         if lookup(pObject:type,"COMBO-BOX,RADIO-SET,SELECTION-LIST,TOGGLE-BOX") <> 0 then
         do:
           assign
             pObject:sensitive = no.
         end.
       end.
      when "ReadOnly" then
       do:
         if pObject:type = "BUTTON" then
         do:
           if pObject:private-data <> "STATIC" then
           pObject:sensitive = no.
         end.
         
         if lookup(pObject:type,"EDITOR,FILL-IN") <> 0 then
         do:
           assign
             pObject:sensitive = yes
             pObject:read-only = yes.
         end.
         
         if lookup(pObject:type,"COMBO-BOX,RADIO-SET,SELECTION-LIST,TOGGLE-BOX") <> 0 then
         do:
           assign
             pObject:sensitive = no.
         end.
       end.
    end case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow wWin 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData wWin 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewComQualification wWin 
PROCEDURE viewComQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frcompliance:
  end.

  if not available qualification
     then
      do:
        message "Internal Error of missing data."
            view-as alert-box error buttons ok.
        return.
      end.
      
  run dialogviewqualification-agent.w(input qualification.entity,
                                      input qualification.EntityId,
                                      input qualification.StateId,
                                      input qualification.qualificationID)
                                      .

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewComRequirement wWin 
PROCEDURE viewComRequirement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable oplsuccess as logical   no-undo.
  define variable opcMsg     as character no-undo.
  
  do with frame {&frame-name}:
  end.
 
  if not available reqfulfill
   then
    do:
      message "Internal Error of missing data."
          view-as alert-box error buttons ok.
      return.
    end.
  
  /* Get the records in temp-table, so that in case any error comes, then the local data instance is not deleted. */
  run server/getrequirements.p (input  reqfulfill.stateID,
                                input  reqfulfill.requirementID,
                                output table StateRequirement,
                                output table StateReqQual,
                                output oplSuccess,
                                output opcMsg).

  if not oplSuccess 
   then
    do:
      message opcMsg 
          view-as alert-box error buttons ok.
      return.
    end.
                            
  /* Populate the return temp-table from the cached data. */
  for each stateRequirement where stateRequirement.requirementID = reqfulfill.requirementId:
    create tStateRequirement.
    buffer-copy stateRequirement to tStateRequirement.
  end.
  
  run dialogviewrequirement-agent.w(input table tStateRequirement).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized wWin 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  frame fMain:width-pixels = {&window-name}:width-pixels.
  frame fMain:virtual-width-pixels = frame fMain:width-pixels.

  frame fMain:height-pixels = {&window-name}:height-pixels.
  frame fMain:virtual-height-pixels = frame fMain:height-pixels.

  run showCurrentPage in h_folder(activePageNumber).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION agentStat wWin 
FUNCTION agentStat RETURNS CHARACTER
  ( input cStat as character /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable pReturn as character no-undo.
  
  publish "GetSysPropDesc" ("AMD", "Agent", "Status", cStat, output pReturn).
  return pReturn.
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION checkActivity wWin 
FUNCTION checkActivity RETURNS CHARACTER
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define buffer ttactivity for ttactivity.
  
  empty temp-table tempactivity. 
  
  if not can-find (first ttactivity where ttactivity.year = pYear 
                                      and ttActivity.category = pCategory
                                      and ttactivity.type = pType)
   then
    do:
      for first ttActivity:
        create tempactivity.
        buffer-copy ttactivity to tempactivity.
        createActivityRow(pSeq, pType, pCategory, pYear, table tempactivity).
      end.
    end.
  return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION checkActivityMetrics wWin 
FUNCTION checkActivityMetrics RETURNS CHARACTER
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define buffer ttactivityMetrics for ttactivityMetrics.
  
  empty temp-table tempactivity. 
  
  if not can-find (first ttactivityMetrics where ttactivityMetrics.year = pYear 
                                             and ttActivityMetrics.category = pCategory
                                             and ttactivityMetrics.type = pType)
   then
    do:
      for first ttActivityMetrics:
        create tempactivity.
        buffer-copy ttactivityMetrics to tempactivity.
        createActivityRowMetrics(pSeq, pType, pCategory, pYear, table tempactivity).
      end.
    end.
  return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createActivity wWin 
FUNCTION createActivity RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  empty temp-table tCActivity.
    create tCActivity.
    assign
        tCActivity.activityID    = 0
        tCActivity.seq           = pSeq
        tCActivity.type          = pType
        tCActivity.category      = pCategory
        tCActivity.year          = pYear
        tCActivity.month1        = 0
        tCActivity.month2        = 0
        tCActivity.month3        = 0
        tCActivity.month4        = 0
        tCActivity.month5        = 0
        tCActivity.month6        = 0
        tCActivity.month7        = 0
        tCActivity.month8        = 0
        tCActivity.month9        = 0
        tCActivity.month10       = 0
        tCActivity.month11       = 0
        tCActivity.month12       = 0
        tCActivity.qtr1          = 0
        tCActivity.qtr2          = 0
        tCActivity.qtr3          = 0
        tCActivity.qtr4          = 0
        tCActivity.yrTotal       = 0
        tCActivity.typeDesc      = getActivityType(pType)
        .    
  for first tCActivity:
    create ttActivity.
    buffer-copy tCActivity to ttActivity.
  end.
  return true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createActivityMetrics wWin 
FUNCTION createActivityMetrics RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  empty temp-table tCActivity.
    create tCActivity.
    assign
        tCActivity.activityID    = 0
        tCActivity.seq           = pSeq
        tCActivity.type          = pType
        tCActivity.category      = pCategory
        tCActivity.year          = pYear
        tCActivity.month1        = 0
        tCActivity.month2        = 0
        tCActivity.month3        = 0
        tCActivity.month4        = 0
        tCActivity.month5        = 0
        tCActivity.month6        = 0
        tCActivity.month7        = 0
        tCActivity.month8        = 0
        tCActivity.month9        = 0
        tCActivity.month10       = 0
        tCActivity.month11       = 0
        tCActivity.month12       = 0
        tCActivity.qtr1          = 0
        tCActivity.qtr2          = 0
        tCActivity.qtr3          = 0
        tCActivity.qtr4          = 0
        tCActivity.yrTotal       = 0
        tCActivity.typeDesc      = getActivityType(pType)
        .    
  for first tCActivity:
    create ttActivityMetrics.
    buffer-copy tCActivity to ttActivityMetrics.
  end.
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createActivityRow wWin 
FUNCTION createActivityRow RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer,
    input table for tempActivity ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  empty temp-table tCActivity.
  for first tempActivity no-lock:
    create tCActivity.
    assign
        tCActivity.activityID    = integer(tempActivity.agentID)
        tCActivity.seq           = pSeq
        tCActivity.agentID       = tempActivity.agentID
        tCActivity.showID        = tempActivity.showID
        tCActivity.stateID       = tempActivity.stateID
        tCActivity.corporationID = tempActivity.corporationID
        tCActivity.name          = tempActivity.name
        tCActivity.showName      = tempActivity.showName
        tCActivity.type          = pType
        tCActivity.category      = pCategory
        tCActivity.year          = pYear
        tCActivity.month1        = 0
        tCActivity.month2        = 0
        tCActivity.month3        = 0
        tCActivity.month4        = 0
        tCActivity.month5        = 0
        tCActivity.month6        = 0
        tCActivity.month7        = 0
        tCActivity.month8        = 0
        tCActivity.month9        = 0
        tCActivity.month10       = 0
        tCActivity.month11       = 0
        tCActivity.month12       = 0
        tCActivity.qtr1          = 0
        tCActivity.qtr2          = 0
        tCActivity.qtr3          = 0
        tCActivity.qtr4          = 0
        tCActivity.yrTotal       = 0
        tCActivity.typeDesc      = getActivityType(pType)
        .    
  end.
  for first tCActivity:
    create ttActivity.
    buffer-copy tCActivity to ttActivity.
  end.
  return true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createActivityRowMetrics wWin 
FUNCTION createActivityRowMetrics RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer,
    input table for tempActivity ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  empty temp-table tCActivity.
  for first tempActivity no-lock:
    create tCActivity.
    assign
        tCActivity.activityID    = integer(tempActivity.agentID)
        tCActivity.seq           = pSeq
        tCActivity.agentID       = tempActivity.agentID
        tCActivity.showID        = tempActivity.showID
        tCActivity.stateID       = tempActivity.stateID
        tCActivity.corporationID = tempActivity.corporationID
        tCActivity.name          = tempActivity.name
        tCActivity.showName      = tempActivity.showName
        tCActivity.type          = pType
        tCActivity.category      = pCategory
        tCActivity.year          = pYear
        tCActivity.month1        = 0
        tCActivity.month2        = 0
        tCActivity.month3        = 0
        tCActivity.month4        = 0
        tCActivity.month5        = 0
        tCActivity.month6        = 0
        tCActivity.month7        = 0
        tCActivity.month8        = 0
        tCActivity.month9        = 0
        tCActivity.month10       = 0
        tCActivity.month11       = 0
        tCActivity.month12       = 0
        tCActivity.qtr1          = 0
        tCActivity.qtr2          = 0
        tCActivity.qtr3          = 0
        tCActivity.qtr4          = 0
        tCActivity.yrTotal       = 0
        tCActivity.typeDesc      = getActivityType(pType)
        .    
  end.
  for first tCActivity:
    create ttActivityMetrics.
    buffer-copy tCActivity to ttActivityMetrics.
  end.
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayAgents wWin 
FUNCTION displayAgents RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  

  define variable tAddress   as character no-undo.
  
  for each cAgent:
    cAgentList = cAgentList + (if cAgent.AgentID = "" then "" else "," + cAgent.AgentID).
    tAddress = "".
    assign
        tAddress =  if (cAgent.addr1 = "?") or (cAgent.addr1 = "") then "" else cAgent.addr1
        tAddress =  tAddress + if (cAgent.addr2 = "?") or (cAgent.addr2 = "") then "" else  ", " + cAgent.addr2
        tAddress =  tAddress + if (cAgent.addr3 = "?") or (cAgent.addr3 = "") then "" else  ", " + cAgent.addr3
        tAddress =  tAddress + if (cAgent.addr4 = "?") or (cAgent.addr4 = "") then "" else  ", " + cAgent.addr4
        tAddress =  tAddress + if (cAgent.city = "?")  or (cAgent.city = "")  then "" else  ", " + cAgent.city
        tAddress =  tAddress + if (cAgent.state = "?") or (cAgent.state = "") then "" else  ", " +  cAgent.state
        tAddress =  tAddress + if (cAgent.zip = "?")   or (cAgent.zip = "")   then "" else  " " +  cAgent.zip
        tAddress =  trim(tAddress,',')
        cAgent.Address  =  trim(tAddress)
        iNumAgentRecords = iNumAgentRecords + 1
        .
    if cAgent.stat = "A" 
     then
      iActiveAgent = iActiveAgent + 1.

    if cAgent.stat = "X" 
     then
      iCancelledAgent = iCancelledAgent + 1.
   
    if cAgent.stat = "P" 
     then
      iProspectAgent = iProspectAgent + 1.

    if cAgent.stat = "W" 
     then
      iWaivedAgent = iWaivedAgent + 1.
      
    if cAgent.stat = "C" 
     then
      iClosedAgent = iClosedAgent + 1.
      
  end.
  
  cAgentList = trim(cAgentList,",").
  
  open query brwConsolidate for each cAgent by cAgent.agentID.
 
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayAR wWin 
FUNCTION displayAR RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 define variable due0_30All  as decimal  no-undo initial 0.
 define variable due31_60All as decimal  no-undo initial 0.
 define variable due61_90All as decimal  no-undo initial 0.
 define variable due91_All   as decimal  no-undo initial 0.
 define variable balanceAll  as decimal  no-undo initial 0.
  
 do with frame frAr:
 
   for each arAging no-lock where arAging.agentID = if cbAgentsAR:input-value in frame frAR = "ALL" then arAging.agentID else cbAgentsAR:input-value:
     assign
         due0_30All  =  due0_30All +  araging.due0_30
         due31_60All =  due31_60All + araging.due31_60
         due61_90All =  due61_90All +  araging.due61_90
         due91_All   =  due91_All + araging.due91_
         balanceAll  =  balanceAll + balance
         .
   end.
   
   empty temp-table tArAgingSummTotal.
   create  tArAgingSummTotal.
   assign
        tArAgingSummTotal.due0_30  =  due0_30All
        tArAgingSummTotal.due31_60 =  due31_60All
        tArAgingSummTotal.due61_90 =  due61_90All
        tArAgingSummTotal.due91_   =  due91_All
        tArAgingSummTotal.balance  =  balanceAll
        .
            
   assign
       bCurrentaging:label = "$" + string(tArAgingSummTotal.due0_30 , "->,>>>,>>>,>>9")  
       b30to60:label       = "$" + string(tArAgingSummTotal.due31_60, "->,>>>,>>>,>>9")   
       b60to90:label       = "$" + string(tArAgingSummTotal.due61_90, "->,>>>,>>>,>>9")   
       bover90:label       = "$" + string(tArAgingSummTotal.due91_  , "->,>>>,>>>,>>9")     
       bbalance:label      = "$" + string(tArAgingSummTotal.balance , "->,>>>,>>>,>>9")
       .
   assign 
       bCurrentaging:bgcolor = if tArAgingSummTotal.due0_30  >= 0 then 2 else 12
       b30to60:bgcolor       = if tArAgingSummTotal.due31_60 >= 0 then 2 else 12
       b60to90:bgcolor       = if tArAgingSummTotal.due61_90 >= 0 then 2 else 12
       bover90:bgcolor       = if tArAgingSummTotal.due91_   >= 0 then 2 else 12
       bbalance:bgcolor      = if tArAgingSummTotal.balance  >= 0 then 2 else 12
       .
 end.
  
 return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayClaim wWin 
FUNCTION displayClaim RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define buffer claimcostssummary  for claimcostssummary.
  define buffer tClaimcostssummary for tClaimcostssummary.
  
  define variable dNetPremium    as decimal  no-undo.
  define variable dClaimCosts    as decimal  no-undo.
  define variable dCostRatio     as decimal  no-undo.
  define variable dClaimCostsAE  as decimal  no-undo.
  define variable dCostRatioAE   as decimal  no-undo.
  
  do with frame frClaim :
  end.
 
  /* Client Server Call */

  run server/getagentgrpclmcostssummary.p ( input ?, /* enddate */
                                            input pStateID,
                                            input pYearOfSign,
                                            input pSoftware,
                                            input pCompany,
                                            input pOrganization,
                                            input pManager,
                                            input pTagList,
                                            input pAffiliationList,
                                            output table tClaimcostssummary,
                                            output lClaimcostssummary,
                                            output std-ch).

  if not lClaimcostssummary 
   then
    do:
      message "LoadedClaimCostsSummary failed: " std-ch
             view-as alert-box error.
      return false.
    end.
             
  empty temp-table claimcostssummary.
  
  /* Update the local copy of data. */  
  for each tClaimcostssummary:
    create claimcostssummary.
    buffer-copy tClaimcostssummary to claimcostssummary.
  end.

  empty temp-table tClaimcostssummary.  

  for each claimcostssummary:
    assign
        dNetPremium   =  dNetPremium + claimcostssummary.netpremium
        dClaimCosts   =  dClaimCosts + claimcostssummary.claimCosts
        dCostRatio    =  dCostRatio +  claimcostssummary.costRatio
        dClaimCostsAE =  dClaimCostsAE + claimcostssummary.claimCostsAE 
        dCostRatioAE  =  dCostRatioAE + claimcostssummary.costRatioAE
        .  
  end.
  
  assign
      netfromLTD  :screen-value = string(dNetPremium) 
      costfromLTD :screen-value = string(dClaimCosts)
      errorfromLTD:screen-value = string(dClaimCostsAE)
      costnetLTD  :screen-value = if ((dClaimCosts * 100)/(dNetPremium) <= 0) or ((dClaimCosts * 100)/(dNetPremium) = ?) then "" else string((dClaimCosts * 100)/(dNetPremium), "zzz")   + "%" 
      errornetLTD :screen-value = if ((dClaimCostsAE * 100)/(dNetPremium) <= 0) or ((dClaimCostsAE * 100)/(dNetPremium) = ?) then "" else string((dClaimCostsAE * 100)/(dNetPremium), "zzz")   + "%"
      .
      
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayHeader wWin 
FUNCTION displayHeader RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cAffiliationList as character no-undo.
  define variable cTagList         as character no-undo.
  define variable cParameterList   as character no-undo.
  define variable cDeleimiter      as character no-undo.

  cDeleimiter = chr(10).

  do with frame fMain:
  
    for each tConsolidate where tConsolidate.isChecked = true:

      case tConsolidate.fldKey:
        when "State"
         then
          do:
            assign
                cParameterList = cParameterList + cDeleimiter + "State = " + tConsolidate.fldDesc
                pStateID       = tConsolidate.fldValue
                .
          end.   
          
        when "Year of Signing"
         then
          do:
            assign
                cParameterList = cParameterList + cDeleimiter + "Year of Signing = " +  tConsolidate.fldDesc
                pYearOfSign    = integer(tConsolidate.fldValue)
                .
          end.
    
        when "Software"
         then
          do:
            assign
                cParameterList = cParameterList + cDeleimiter + "Software = " + tConsolidate.fldDesc
                pSoftware      =  tConsolidate.fldValue
                .
          end.
          
        when "Organization"
         then
          do:
            assign
                cParameterList = cParameterList + cDeleimiter + "Organization = " + tConsolidate.fldDesc
                pOrganization  =  tConsolidate.fldValue
                .
          end.
          
        when "Company"
         then
          do:
            assign
                cParameterList = cParameterList + cDeleimiter + "Company = " +  tConsolidate.fldDesc
                pCompany       =   tConsolidate.fldValue
                .
          end.
          
        when "Manager"
         then
          do:
            assign
                cParameterList = cParameterList + cDeleimiter + "Manager = " +  tConsolidate.fldDesc
                pManager =  tConsolidate.fldValue
                . 
          end.
          
        when "Tag"
         then
          do:
            assign
                cTagList = if cTagList = "" 
                            then 
                             tConsolidate.fldValue 
                            else   
                             cTagList + "," + tConsolidate.fldValue
                pTagList = cTagList            
                . 
          end.
          
        when "Affiliation"
         then
          do:
            assign
                cAffiliationList = if cAffiliationList = "" 
                                    then 
                                     tConsolidate.fldDesc 
                                    else   
                                     cAffiliationList + "," + tConsolidate.fldDesc
                pAffiliationList = if pAffiliationList = "" 
                                    then 
                                     tConsolidate.fldValue 
                                    else   
                                     pAffiliationList + "," + tConsolidate.fldValue                    
                . 
          end.      
      end case.  
    end.
  
    assign
        pTagList         = trim(pTagList,',')
        pAffiliationList = trim(pAffiliationList,',')
        cParameterList   = cParameterList +  (if cTagList ne "" then (cDeleimiter + "Tag = " + cTagList) else "")  
                                          +  (if cAffiliationList ne "" then (cDeleimiter + "Affiliation = " + cAffiliationList) else "")
        cParameterList   = trim(cParameterList,cDeleimiter)
        cParameterList   = trim(cParameterList)
        .
        
    eParams:screen-value = cParameterList.
    eResults:screen-value =       "Total Agents = " + string(iNumAgentRecords)  
                            + chr(10) + "Active           = " + string(iActiveAgent)     
                            + chr(10) + "Prospect       = " + string(iProspectAgent)   
                            + chr(10) + "Closed          = " + string(iClosedAgent)      
                            + chr(10) + "Withdrawn    = " + string(iWaivedAgent)
                            + chr(10) + "Cancelled     = " + string(iCancelledAgent)
                            .
  end.
   
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getActivityComparedData wWin 
FUNCTION getActivityComparedData RETURNS LOGICAL
  (input ipiYear as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable dMonthPlan   as decimal extent 18 no-undo.
  define variable dMonthActual as decimal extent 18 no-undo.

  define variable iSeq as integer no-undo.
  
  define buffer ttactivity for ttactivity.
  
  checkActivity(1, "A", "N", ipiYear - 2).
  checkActivity(1, "A", "N", ipiYear - 1 ).
  checkActivity(1, "A", "N", ipiYear).
  checkActivity(2, "P", "N", ipiYear).
  
  for each ttactivity 
    by ttactivity.category: 
    if ttActivity.year = ipiYear 
     then
      do:       
        create activity.
        buffer-copy ttactivity to activity.
        assign
            activity.showID       = ""
            activity.showName     = ""
            activity.categoryDesc = getCategoryDesc(ttActivity.category)
            .
        
        if ttactivity.type = "A" or ttactivity.type = "I" 
         then
          do:
            activity.seq = 1.
            activity.typeDesc  = getActivityType("A" /* ttActivity.type */).
            assign
                dMonthActual[1] = ttactivity.month1
                dMonthActual[2] = ttactivity.month2
                dMonthActual[3] = ttactivity.month3
                dMonthActual[4] = ttactivity.month4
                dMonthActual[5] = ttactivity.month5
                dMonthActual[6] = ttactivity.month6
                dMonthActual[7] = ttactivity.month7
                dMonthActual[8] = ttactivity.month8
                dMonthActual[9] = ttactivity.month9
                dMonthActual[10] = ttactivity.month10
                dMonthActual[11] = ttactivity.month11
                dMonthActual[12] = ttactivity.month12
                dMonthActual[13] = ttactivity.qtr1
                dMonthActual[14] = ttactivity.qtr2
                dMonthActual[15] = ttactivity.qtr3
                dMonthActual[16] = ttactivity.qtr4
                dMonthActual[17] = ttactivity.yrTotal
                dMonthActual[18] = ttactivity.total
                .
          end.
        if ttactivity.type = "P"
         then
          do:
            iCreateComparision = iCreateComparision + 1.
            activity.seq = 2.
            activity.typeDesc  = getActivityType(ttactivity.type).
            assign
                dMonthPlan[1] = ttactivity.month1
                dMonthPlan[2] = ttactivity.month2
                dMonthPlan[3] = ttactivity.month3
                dMonthPlan[4] = ttactivity.month4
                dMonthPlan[5] = ttactivity.month5
                dMonthPlan[6] = ttactivity.month6
                dMonthPlan[7] = ttactivity.month7
                dMonthPlan[8] = ttactivity.month8
                dMonthPlan[9] = ttactivity.month9
                dMonthPlan[10] = ttactivity.month10
                dMonthPlan[11] = ttactivity.month11
                dMonthPlan[12] = ttactivity.month12
                dMonthPlan[13] = ttactivity.qtr1
                dMonthPlan[14] = ttactivity.qtr2
                dMonthPlan[15] = ttactivity.qtr3
                dMonthPlan[16] = ttactivity.qtr4
                dMonthPlan[17] = ttactivity.yrTotal
                dMonthPlan[18] = ttactivity.total
                .
          end.
      end.
    else
     do:
       if (ttActivity.category = "N" and ttActivity.type = "A")
        then
         do:
           create activity.
           buffer-copy ttactivity to activity.
           assign
               activity.showID       = ""
               activity.showName     = ""
               activity.seq          = 1
               activity.typeDesc     = getActivityType("A" /* ttActivity.type */)
               activity.categoryDesc = getCategoryDesc(ttActivity.category)
               .
         end.
     end. 
  end.

  /* build the comparison row */
  for each ttactivity where ttactivity.year = ipiYear break by ttactivity.category:
    if first-of(ttactivity.category) 
     then
      do:
        create activity.    
        assign
            activity.month1  = (dMonthActual[1]  * 100 ) / dMonthPlan[1] 
            activity.month2  = (dMonthActual[2]  * 100 ) / dMonthPlan[2]   
            activity.month3  = (dMonthActual[3]  * 100 ) / dMonthPlan[3] 
            activity.month4  = (dMonthActual[4]  * 100 ) / dMonthPlan[4] 
            activity.month5  = (dMonthActual[5]  * 100 ) / dMonthPlan[5] 
            activity.month6  = (dMonthActual[6]  * 100 ) / dMonthPlan[6] 
            activity.month7  = (dMonthActual[7]  * 100 ) / dMonthPlan[7] 
            activity.month8  = (dMonthActual[8]  * 100 ) / dMonthPlan[8] 
            activity.month9  = (dMonthActual[9]  * 100 ) / dMonthPlan[9] 
            activity.month10 = (dMonthActual[10] * 100 ) / dMonthPlan[10]
            activity.month11 = (dMonthActual[11] * 100 ) / dMonthPlan[11]
            activity.month12 = (dMonthActual[12] * 100 ) / dMonthPlan[12]
            activity.qtr1    = (dMonthActual[13] * 100 ) / dMonthPlan[13]
            activity.qtr2    = (dMonthActual[14] * 100 ) / dMonthPlan[14]
            activity.qtr3    = (dMonthActual[15] * 100 ) / dMonthPlan[15]
            activity.qtr4    = (dMonthActual[16] * 100 ) / dMonthPlan[16]
            activity.yrTotal = (dMonthActual[17] * 100 ) / dMonthPlan[17]
            activity.total   = (dMonthActual[18] * 100 ) / dMonthPlan[18]
            .
        
        assign
            activity.seq           = 3
            activity.agentID       = ""
            activity.stateID       = ttactivity.stateID
            activity.corporationID = ttactivity.corporationID
            activity.name          = ttactivity.name
            activity.type          = "C"
            activity.typeDesc      = "% of Planned"
            activity.category      = ttactivity.category
            activity.categoryDesc  = getCategoryDesc(ttActivity.category)
            activity.year          = ttactivity.year
            activity.month1        = if activity.month1 = ? then 0 else activity.month1
            activity.month2        = if activity.month2 = ? then 0 else activity.month2
            activity.month3        = if activity.month3 = ? then 0 else activity.month3
            activity.month4        = if activity.month4 = ? then 0 else activity.month4
            activity.month5        = if activity.month5 = ? then 0 else activity.month5
            activity.month6        = if activity.month6 = ? then 0 else activity.month6
            activity.month7        = if activity.month7 = ? then 0 else activity.month7
            activity.month8        = if activity.month8 = ? then 0 else activity.month8
            activity.month9        = if activity.month9 = ? then 0 else activity.month9
            activity.month10       = if activity.month10 = ? then 0 else activity.month10
            activity.month11       = if activity.month11 = ? then 0 else activity.month11
            activity.month12       = if activity.month12 = ? then 0 else activity.month12
            activity.qtr1          = if activity.qtr1 = ? then 0 else activity.qtr1
            activity.qtr2          = if activity.qtr2 = ? then 0 else activity.qtr2
            activity.qtr3          = if activity.qtr3 = ? then 0 else activity.qtr3
            activity.qtr4          = if activity.qtr4 = ? then 0 else activity.qtr4
            activity.yrTotal       = if activity.yrTotal = ? then 0 else activity.yrTotal
            activity.total         = if activity.total = ? then 0 else activity.total
            .
      end.
  end.
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getActivityComparedDataMetrics wWin 
FUNCTION getActivityComparedDataMetrics RETURNS LOGICAL
  (input ipiYear as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable dMonthPlanP   as decimal extent 18 no-undo.
  define variable dMonthActualP as decimal extent 18 no-undo.
  define variable dMonthPlanT   as decimal extent 18 no-undo.
  define variable dMonthActualT as decimal extent 18 no-undo.
  
  define variable iSeq as integer no-undo.
  
  define buffer ttactivityMetrics for ttactivityMetrics.
  
  checkActivityMetrics(1, "I", "P", ipiYear - 2).
  checkActivityMetrics(1, "A", "T", ipiYear - 2).
  checkActivityMetrics(1, "I", "P", ipiYear - 1).
  checkActivityMetrics(1, "A", "T", ipiYear - 1).
  checkActivityMetrics(1, "I", "P", ipiYear).
  checkActivityMetrics(1, "A", "T", ipiYear).
  checkActivityMetrics(2, "P", "P", ipiYear).
  checkActivityMetrics(2, "P", "T", ipiYear).
  
  for each ttactivityMetrics 
    by ttactivityMetrics.category: 
    if ttactivityMetrics.year = ipiYear 
     then
      do:       
        create activityMetrics.
        buffer-copy ttactivityMetrics to activityMetrics.
        activityMetrics.categoryDesc = getCategoryDesc(ttactivityMetrics.category).
        
        if ttactivityMetrics.type = "I" and ttactivityMetrics.category = 'P' 
         then
          do:
            iCreateComparision = iCreateComparision + 1.
            activityMetrics.seq = 1.
            activityMetrics.typeDesc  = getActivityType(ttactivityMetrics.type).
            assign
                dMonthActualP[1] = ttactivityMetrics.month1
                dMonthActualP[2] = ttactivityMetrics.month2
                dMonthActualP[3] = ttactivityMetrics.month3
                dMonthActualP[4] = ttactivityMetrics.month4
                dMonthActualP[5] = ttactivityMetrics.month5
                dMonthActualP[6] = ttactivityMetrics.month6
                dMonthActualP[7] = ttactivityMetrics.month7
                dMonthActualP[8] = ttactivityMetrics.month8
                dMonthActualP[9] = ttactivityMetrics.month9
                dMonthActualP[10] = ttactivityMetrics.month10
                dMonthActualP[11] = ttactivityMetrics.month11
                dMonthActualP[12] = ttactivityMetrics.month12
                dMonthActualP[13] = ttactivityMetrics.qtr1
                dMonthActualP[14] = ttactivityMetrics.qtr2
                dMonthActualP[15] = ttactivityMetrics.qtr3
                dMonthActualP[16] = ttactivityMetrics.qtr4
                dMonthActualP[17] = ttactivityMetrics.yrTotal
                dMonthActualP[18] = ttactivityMetrics.total
                .
          end.
       
        if ttactivityMetrics.category = 'P' and ttactivityMetrics.type = "P"
         then
          do:
            iCreateComparision = iCreateComparision + 1.
            activityMetrics.seq = 2.
            activityMetrics.typeDesc  = getActivityType(ttactivityMetrics.type).
            assign
                dMonthPlanP[1] = ttactivityMetrics.month1
                dMonthPlanP[2] = ttactivityMetrics.month2
                dMonthPlanP[3] = ttactivityMetrics.month3
                dMonthPlanP[4] = ttactivityMetrics.month4
                dMonthPlanP[5] = ttactivityMetrics.month5
                dMonthPlanP[6] = ttactivityMetrics.month6
                dMonthPlanP[7] = ttactivityMetrics.month7
                dMonthPlanP[8] = ttactivityMetrics.month8
                dMonthPlanP[9] = ttactivityMetrics.month9
                dMonthPlanP[10] = ttactivityMetrics.month10
                dMonthPlanP[11] = ttactivityMetrics.month11
                dMonthPlanP[12] = ttactivityMetrics.month12
                dMonthPlanP[13] = ttactivityMetrics.qtr1
                dMonthPlanP[14] = ttactivityMetrics.qtr2
                dMonthPlanP[15] = ttactivityMetrics.qtr3
                dMonthPlanP[16] = ttactivityMetrics.qtr4
                dMonthPlanP[17] = ttactivityMetrics.yrTotal
                dMonthPlanP[18] = ttactivityMetrics.total
                .
          end.
        if ttactivityMetrics.type = "A" and ttactivityMetrics.category = 'T' 
         then
          do:
            iCreateComparision = iCreateComparision + 1.
            activityMetrics.seq = 1.
            activityMetrics.typeDesc  = getActivityType("I").
            assign
                dMonthActualT[1] = ttactivityMetrics.month1
                dMonthActualT[2] = ttactivityMetrics.month2
                dMonthActualT[3] = ttactivityMetrics.month3
                dMonthActualT[4] = ttactivityMetrics.month4
                dMonthActualT[5] = ttactivityMetrics.month5
                dMonthActualT[6] = ttactivityMetrics.month6
                dMonthActualT[7] = ttactivityMetrics.month7
                dMonthActualT[8] = ttactivityMetrics.month8
                dMonthActualT[9] = ttactivityMetrics.month9
                dMonthActualT[10] = ttactivityMetrics.month10
                dMonthActualT[11] = ttactivityMetrics.month11
                dMonthActualT[12] = ttactivityMetrics.month12
                dMonthActualT[13] = ttactivityMetrics.qtr1
                dMonthActualT[14] = ttactivityMetrics.qtr2
                dMonthActualT[15] = ttactivityMetrics.qtr3
                dMonthActualT[16] = ttactivityMetrics.qtr4
                dMonthActualT[17] = ttactivityMetrics.yrTotal
                dMonthActualT[18] = ttactivityMetrics.total
                .
          end.
        if ttactivityMetrics.type = "P" and ttactivityMetrics.category = 'T'
         then
          do:
            iCreateComparision = iCreateComparision + 1.
            activityMetrics.seq = 2.
            activityMetrics.typeDesc  = getActivityType(ttactivityMetrics.type).
            assign
                dMonthPlanT[1] = ttactivityMetrics.month1
                dMonthPlanT[2] = ttactivityMetrics.month2
                dMonthPlanT[3] = ttactivityMetrics.month3
                dMonthPlanT[4] = ttactivityMetrics.month4
                dMonthPlanT[5] = ttactivityMetrics.month5
                dMonthPlanT[6] = ttactivityMetrics.month6
                dMonthPlanT[7] = ttactivityMetrics.month7
                dMonthPlanT[8] = ttactivityMetrics.month8
                dMonthPlanT[9] = ttactivityMetrics.month9
                dMonthPlanT[10] = ttactivityMetrics.month10
                dMonthPlanT[11] = ttactivityMetrics.month11
                dMonthPlanT[12] = ttactivityMetrics.month12
                dMonthPlanT[13] = ttactivityMetrics.qtr1
                dMonthPlanT[14] = ttactivityMetrics.qtr2
                dMonthPlanT[15] = ttactivityMetrics.qtr3
                dMonthPlanT[16] = ttactivityMetrics.qtr4
                dMonthPlanT[17] = ttactivityMetrics.yrTotal
                dMonthPlanT[18] = ttactivityMetrics.total
                .
          end.
      end.
    else
     do:
       if (ttactivityMetrics.category = "T" and ttactivityMetrics.type = "A") or 
          (ttactivityMetrics.category = "P" and ttactivityMetrics.type = "I") or
          (ttactivityMetrics.category = "N" and ttactivityMetrics.type = "A")
        then
         do:
           create activityMetrics.
           buffer-copy ttactivityMetrics to activityMetrics.
           assign
               activityMetrics.seq          = 1
               activityMetrics.typeDesc     = getActivityType("I")
               activityMetrics.categoryDesc = getCategoryDesc(ttactivityMetrics.category)
               .
         end.
     end. 
  end.

  /* build the comparison row */
  for each ttactivityMetrics where ttactivityMetrics.year = ipiYear break by ttactivityMetrics.category:
    if first-of(ttactivityMetrics.category)
     then
      do:
        create activityMetrics.
        if ttactivityMetrics.category = 'P'
         then
          do: 
            assign
                activityMetrics.month1  = (dMonthActualP[1]  * 100 ) / dMonthPlanP[1] 
                activityMetrics.month2  = (dMonthActualP[2]  * 100 ) / dMonthPlanP[2]   
                activityMetrics.month3  = (dMonthActualP[3]  * 100 ) / dMonthPlanP[3] 
                activityMetrics.month4  = (dMonthActualP[4]  * 100 ) / dMonthPlanP[4] 
                activityMetrics.month5  = (dMonthActualP[5]  * 100 ) / dMonthPlanP[5] 
                activityMetrics.month6  = (dMonthActualP[6]  * 100 ) / dMonthPlanP[6] 
                activityMetrics.month7  = (dMonthActualP[7]  * 100 ) / dMonthPlanP[7] 
                activityMetrics.month8  = (dMonthActualP[8]  * 100 ) / dMonthPlanP[8] 
                activityMetrics.month9  = (dMonthActualP[9]  * 100 ) / dMonthPlanP[9] 
                activityMetrics.month10 = (dMonthActualP[10] * 100 ) / dMonthPlanP[10]
                activityMetrics.month11 = (dMonthActualP[11] * 100 ) / dMonthPlanP[11]
                activityMetrics.month12 = (dMonthActualP[12] * 100 ) / dMonthPlanP[12]
                activityMetrics.qtr1    = (dMonthActualP[13] * 100 ) / dMonthPlanP[13]
                activityMetrics.qtr2    = (dMonthActualP[14] * 100 ) / dMonthPlanP[14]
                activityMetrics.qtr3    = (dMonthActualP[15] * 100 ) / dMonthPlanP[15]
                activityMetrics.qtr4    = (dMonthActualP[16] * 100 ) / dMonthPlanP[16]
                activityMetrics.yrTotal = (dMonthActualP[17] * 100 ) / dMonthPlanP[17]
                activityMetrics.total   = (dMonthActualP[18] * 100 ) / dMonthPlanP[18]
                .
          end.
        if ttactivityMetrics.category = 'T'
         then
          do:  
            assign
                activityMetrics.month1  = (dMonthActualT[1]  * 100 ) / dMonthPlanT[1] 
                activityMetrics.month2  = (dMonthActualT[2]  * 100 ) / dMonthPlanT[2]   
                activityMetrics.month3  = (dMonthActualT[3]  * 100 ) / dMonthPlanT[3] 
                activityMetrics.month4  = (dMonthActualT[4]  * 100 ) / dMonthPlanT[4] 
                activityMetrics.month5  = (dMonthActualT[5]  * 100 ) / dMonthPlanT[5] 
                activityMetrics.month6  = (dMonthActualT[6]  * 100 ) / dMonthPlanT[6] 
                activityMetrics.month7  = (dMonthActualT[7]  * 100 ) / dMonthPlanT[7] 
                activityMetrics.month8  = (dMonthActualT[8]  * 100 ) / dMonthPlanT[8] 
                activityMetrics.month9  = (dMonthActualT[9]  * 100 ) / dMonthPlanT[9] 
                activityMetrics.month10 = (dMonthActualT[10] * 100 ) / dMonthPlanT[10]
                activityMetrics.month11 = (dMonthActualT[11] * 100 ) / dMonthPlanT[11]
                activityMetrics.month12 = (dMonthActualT[12] * 100 ) / dMonthPlanT[12]
                activityMetrics.qtr1    = (dMonthActualT[13] * 100 ) / dMonthPlanT[13]
                activityMetrics.qtr2    = (dMonthActualT[14] * 100 ) / dMonthPlanT[14]
                activityMetrics.qtr3    = (dMonthActualT[15] * 100 ) / dMonthPlanT[15]
                activityMetrics.qtr4    = (dMonthActualT[16] * 100 ) / dMonthPlanT[16]
                activityMetrics.yrTotal = (dMonthActualT[17] * 100 ) / dMonthPlanT[17]
                activityMetrics.total   = (dMonthActualT[18] * 100 ) / dMonthPlanT[18]
                .
          end.
        assign
            activityMetrics.seq           = 3
            activityMetrics.agentID       = ttactivityMetrics.agentID
            activityMetrics.stateID       = ttactivityMetrics.stateID
            activityMetrics.corporationID = ttactivityMetrics.corporationID
            activityMetrics.name          = ttactivityMetrics.name
            activityMetrics.type          = "C"
            activityMetrics.typeDesc      = "% of Planned"
            activityMetrics.category      = ttactivityMetrics.category
            activityMetrics.categoryDesc  = getCategoryDesc(ttactivityMetrics.category)
            activityMetrics.year          = ttactivityMetrics.year
            activityMetrics.month1        = if activityMetrics.month1 = ? then 0 else activityMetrics.month1
            activityMetrics.month2        = if activityMetrics.month2 = ? then 0 else activityMetrics.month2
            activityMetrics.month3        = if activityMetrics.month3 = ? then 0 else activityMetrics.month3
            activityMetrics.month4        = if activityMetrics.month4 = ? then 0 else activityMetrics.month4
            activityMetrics.month5        = if activityMetrics.month5 = ? then 0 else activityMetrics.month5
            activityMetrics.month6        = if activityMetrics.month6 = ? then 0 else activityMetrics.month6
            activityMetrics.month7        = if activityMetrics.month7 = ? then 0 else activityMetrics.month7
            activityMetrics.month8        = if activityMetrics.month8 = ? then 0 else activityMetrics.month8
            activityMetrics.month9        = if activityMetrics.month9 = ? then 0 else activityMetrics.month9
            activityMetrics.month10       = if activityMetrics.month10 = ? then 0 else activityMetrics.month10
            activityMetrics.month11       = if activityMetrics.month11 = ? then 0 else activityMetrics.month11
            activityMetrics.month12       = if activityMetrics.month12 = ? then 0 else activityMetrics.month12
            activityMetrics.qtr1          = if activityMetrics.qtr1 = ? then 0 else activityMetrics.qtr1
            activityMetrics.qtr2          = if activityMetrics.qtr2 = ? then 0 else activityMetrics.qtr2
            activityMetrics.qtr3          = if activityMetrics.qtr3 = ? then 0 else activityMetrics.qtr3
            activityMetrics.qtr4          = if activityMetrics.qtr4 = ? then 0 else activityMetrics.qtr4
            activityMetrics.yrTotal       = if activityMetrics.yrTotal = ? then 0 else activityMetrics.yrTotal
            activityMetrics.total         = if activityMetrics.total = ? then 0 else activityMetrics.total
            .
      end.
  end.
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getActivityTotal wWin 
FUNCTION getActivityTotal RETURNS DECIMAL
  ( input table for tempactivity,
    input pType as character  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable dReturn as decimal no-undo.
  define variable iMonth as integer no-undo.
  define buffer tempactivity for tempactivity.
  
  iMonth = month(today).
  for first tempactivity no-lock:
    case pType:
     when "Year" 
      then 
       do:
         dReturn = tempactivity.month1 + tempactivity.month2 + tempactivity.month3 + 
                   tempactivity.month4 + tempactivity.month5 + tempactivity.month6 +
                   tempactivity.month7 + tempactivity.month8 + tempactivity.month9 +
                   tempactivity.month10 + tempactivity.month11 + tempactivity.month12.
       end.
     when "Current" then
      do:
        dReturn = (if iMonth >= 1  then tempactivity.month1  else 0)
                + (if iMonth >= 2  then tempactivity.month2  else 0)
                + (if iMonth >= 3  then tempactivity.month3  else 0)
                + (if iMonth >= 4  then tempactivity.month4  else 0)
                + (if iMonth >= 5  then tempactivity.month5  else 0)
                + (if iMonth >= 6  then tempactivity.month6  else 0)
                + (if iMonth >= 7  then tempactivity.month7  else 0)
                + (if iMonth >= 8  then tempactivity.month8  else 0)
                + (if iMonth >= 9  then tempactivity.month9  else 0)
                + (if iMonth >= 10 then tempactivity.month10 else 0)
                + (if iMonth >= 11 then tempactivity.month11 else 0)
                + (if iMonth >= 12 then tempactivity.month12 else 0).
      end.
    end case.
  end.
  return dReturn.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getActivityType wWin 
FUNCTION getActivityType RETURNS CHARACTER
  ( input ipcType as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("AMD", "Activity", "Type", ipcType, output std-ch).
  return std-ch.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getAuditStatus wWin 
FUNCTION getAuditStatus RETURNS CHARACTER
  ( input ipcStatus as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/   
  std-ch = "".
  publish "GetSysPropDesc" ("QAR", "Audit", "Status", ipcStatus, output std-ch).
  return std-ch.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getAuditType wWin 
FUNCTION getAuditType RETURNS CHARACTER
  ( input ipcAuditType as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("QAR", "Audit", "Type", ipcAuditType, output std-ch).
  return std-ch.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getBatchStatus wWin 
FUNCTION getBatchStatus RETURNS CHARACTER
  ( input ipcStatus as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("OPS", "Batch", "Status", ipcStatus, output std-ch).
  return std-ch.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getCategoryDesc wWin 
FUNCTION getCategoryDesc RETURNS CHARACTER
  ( input ipcCategory as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("AMD", "Activity", "Category", ipcCategory, output std-ch).
  return std-ch.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getErrorType wWin 
FUNCTION getErrorType RETURNS CHARACTER
  ( input ipcErrType as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("QAR", "ERR", "Type", ipcErrType, output std-ch).
  return std-ch.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getManagerName wWin 
FUNCTION getManagerName RETURNS CHARACTER
  ( input cManagerUID as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  publish "GetSysUserName" (cManagerUID, output std-ch).
  return std-ch.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getMonthName wWin 
FUNCTION getMonthName RETURNS CHARACTER
  (INPUT iMonth AS INTEGER) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cMonths as character no-undo initial "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".
  IF iMonth lt 1 or iMonth gt 12 
   then
    return "".
  return entry(iMonth,cMonths).
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getPeriodID wWin 
FUNCTION getPeriodID RETURNS INTEGER
  ( input ipiMonth as integer,
    input ipiYear as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable iPeriodID as integer no-undo.
  
  publish "GetPeriodID" (input ipiMonth,
                         input ipiYear,
                         output iPeriodID).
                         
  RETURN iPeriodID.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getQualificationDesc wWin 
FUNCTION getQualificationDesc RETURNS CHARACTER
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  publish "GetSysPropDesc" ("COM", "Qualification", "Status", cStat, output std-ch).
  return std-ch.   /* Function return value. */

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openActivity wWin 
FUNCTION openActivity RETURNS LOGICAL
  ( input ipiYear         as integer,
    input ipcCategoryList as character,
    input iplRefresh      as logical) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/     
  define buffer ttactivity for ttactivity.
  define variable dAgentTotalMonthly as decimal extent 18 no-undo.
  define variable categoryTotal as decimal no-undo.
  define variable typeTotal   as decimal no-undo.
  
  empty temp-table activity.
  empty temp-table ttactivity.
  
  do with frame frActivity:
  end.

  if ipiYear = year(today) 
   then
    do:
       run server/queryagentgroupactivity.p( input ipiYear ,
                                             input ipcCategoryList ,
                                             input pStateID,
                                             input pYearOfSign,
                                             input pSoftware,
                                             input pCompany,
                                             input pOrganization,
                                             input pManager,
                                             input pTagList,
                                             input pAffiliationList,
                                             output table ttactivity,
                                             output std-lo,
                                             output std-ch).
       if not std-lo
        then
         do:
           message "LoadAgentActivities failed: " + std-ch view-as alert-box info.
           return false.   /* Function return value. */
         end.                                       
                                          
    end. /* if ipiYear = year(today) */

  empty temp-table tempactivity.
  for each ttactivity no-lock break /* by ttactivity.category */ by ttactivity.type by ttactivity.year:
    if first-of( ttactivity.year ) then
      do:
        assign
            dAgentTotalMonthly[1] = 0
            dAgentTotalMonthly[2] = 0
            dAgentTotalMonthly[3] = 0
            dAgentTotalMonthly[4] = 0
            dAgentTotalMonthly[5] = 0
            dAgentTotalMonthly[6] = 0
            dAgentTotalMonthly[7] = 0
            dAgentTotalMonthly[8] = 0
            dAgentTotalMonthly[9] = 0
            dAgentTotalMonthly[10] = 0
            dAgentTotalMonthly[11] = 0
            dAgentTotalMonthly[12] = 0
            .
      end.  
      
    assign
            dAgentTotalMonthly[1] = dAgentTotalMonthly[1] + ttactivity.month1
            dAgentTotalMonthly[2] = dAgentTotalMonthly[2] + ttactivity.month2
            dAgentTotalMonthly[3] = dAgentTotalMonthly[3] + ttactivity.month3
            dAgentTotalMonthly[4] = dAgentTotalMonthly[4] + ttactivity.month4
            dAgentTotalMonthly[5] = dAgentTotalMonthly[5] + ttactivity.month5
            dAgentTotalMonthly[6] = dAgentTotalMonthly[6] + ttactivity.month6
            dAgentTotalMonthly[7] = dAgentTotalMonthly[7] + ttactivity.month7
            dAgentTotalMonthly[8] = dAgentTotalMonthly[8] + ttactivity.month8
            dAgentTotalMonthly[9] = dAgentTotalMonthly[9] + ttactivity.month9
            dAgentTotalMonthly[10] = dAgentTotalMonthly[10] + ttactivity.month10
            dAgentTotalMonthly[11] = dAgentTotalMonthly[11] + ttactivity.month11
            dAgentTotalMonthly[12] = dAgentTotalMonthly[12] + ttactivity.month12
            .
    
    if last-of( ttactivity.year ) then
      do:
         create tempactivity.
         buffer-copy ttactivity to tempactivity.
         assign
             tempactivity.month1 = dAgentTotalMonthly[1]
            tempactivity.month2 = dAgentTotalMonthly[2]
            tempactivity.month3 = dAgentTotalMonthly[3]
            tempactivity.month4 = dAgentTotalMonthly[4]
            tempactivity.month5 = dAgentTotalMonthly[5]
            tempactivity.month6 = dAgentTotalMonthly[6]
            tempactivity.month7 = dAgentTotalMonthly[7]
            tempactivity.month8 = dAgentTotalMonthly[8]
            tempactivity.month9 = dAgentTotalMonthly[9]
            tempactivity.month10 = dAgentTotalMonthly[10]
            tempactivity.month11 = dAgentTotalMonthly[11]
            tempactivity.month12 = dAgentTotalMonthly[12]
            .
            
      end.

  end.
  /* Copy totalled data in ttActivity */
  empty temp-table ttActivity.
  for each tempActivity:
    create ttActivity.
    buffer-copy tempActivity to ttActivity.
  end.   
     
  for each ttactivity:
    ttactivity.typeDesc = getActivityType(ttactivity.type).
    
    /* get totals */
    empty temp-table tempactivity.  
    create tempactivity.
    buffer-copy ttactivity to tempactivity.
    assign
        ttactivity.Total   = getActivityTotal(table tempactivity, "Year")
        ttactivity.yrTotal = getActivityTotal(table tempactivity, "Current")
        .
  end.
  
  getActivityComparedData(input ipiYear).

  find first ttActivity no-error.
  if not available ttActivity
   then
    do:
      createActivity(1 , "A", "N", ipiYear - 2).
      createActivity(1 , "A", "N", ipiYear - 1).
      getActivityComparedData(input ipiYear).
    end.
  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openActivityMetrics wWin 
FUNCTION openActivityMetrics RETURNS LOGICAL
  ( input ipiYear         as integer,
    input ipcCategoryList as character,
    input iplRefresh      as logical) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/     
  define buffer ttactivityMetrics for ttactivityMetrics.
  
  define variable dAgentTotalMonthly as decimal extent 18 no-undo.
  
  empty temp-table activityMetrics.
  empty temp-table ttactivityMetrics.
  
  do with frame frActivity:
  end.
 
  if ipiYear = year(today) 
   then
    do:
      run server/queryagentgroupactivity.p(  input ipiYear ,
                                             input ipcCategoryList ,
                                             input pStateID,
                                             input pYearOfSign,
                                             input pSoftware,
                                             input pCompany,
                                             input pOrganization,
                                             input pManager,
                                             input pTagList,
                                             input pAffiliationList,
                                             output table ttactivityMetrics,
                                             output std-lo,
                                             output std-ch). 
                                             
      if not std-lo
       then
        do:
          message "LoadAgentActivities failed: " + std-ch view-as alert-box info.
          return false.   /* Function return value. */
        end.                                                    
    end. /* if ipiYear = year(today) */

  empty temp-table tempactivity.
  for each ttactivityMetrics no-lock break by ttactivityMetrics.category by ttactivityMetrics.type by ttactivityMetrics.year:

    if first-of( ttactivityMetrics.year ) then
      do:
        assign
            dAgentTotalMonthly[1] = 0
            dAgentTotalMonthly[2] = 0
            dAgentTotalMonthly[3] = 0
            dAgentTotalMonthly[4] = 0
            dAgentTotalMonthly[5] = 0
            dAgentTotalMonthly[6] = 0
            dAgentTotalMonthly[7] = 0
            dAgentTotalMonthly[8] = 0
            dAgentTotalMonthly[9] = 0
            dAgentTotalMonthly[10] = 0
            dAgentTotalMonthly[11] = 0
            dAgentTotalMonthly[12] = 0
            .
      end.  

    assign
            dAgentTotalMonthly[1] = dAgentTotalMonthly[1] + ttactivityMetrics.month1
            dAgentTotalMonthly[2] = dAgentTotalMonthly[2] + ttactivityMetrics.month2
            dAgentTotalMonthly[3] = dAgentTotalMonthly[3] + ttactivityMetrics.month3
            dAgentTotalMonthly[4] = dAgentTotalMonthly[4] + ttactivityMetrics.month4
            dAgentTotalMonthly[5] = dAgentTotalMonthly[5] + ttactivityMetrics.month5
            dAgentTotalMonthly[6] = dAgentTotalMonthly[6] + ttactivityMetrics.month6
            dAgentTotalMonthly[7] = dAgentTotalMonthly[7] + ttactivityMetrics.month7
            dAgentTotalMonthly[8] = dAgentTotalMonthly[8] + ttactivityMetrics.month8
            dAgentTotalMonthly[9] = dAgentTotalMonthly[9] + ttactivityMetrics.month9
            dAgentTotalMonthly[10] = dAgentTotalMonthly[10] + ttactivityMetrics.month10
            dAgentTotalMonthly[11] = dAgentTotalMonthly[11] + ttactivityMetrics.month11
            dAgentTotalMonthly[12] = dAgentTotalMonthly[12] + ttactivityMetrics.month12
            .
    
    if last-of( ttactivityMetrics.year ) then
      do:
         create tempactivity.
         buffer-copy ttactivityMetrics to tempactivity.
         assign
             tempactivity.month1 = dAgentTotalMonthly[1]
            tempactivity.month2 = dAgentTotalMonthly[2]
            tempactivity.month3 = dAgentTotalMonthly[3]
            tempactivity.month4 = dAgentTotalMonthly[4]
            tempactivity.month5 = dAgentTotalMonthly[5]
            tempactivity.month6 = dAgentTotalMonthly[6]
            tempactivity.month7 = dAgentTotalMonthly[7]
            tempactivity.month8 = dAgentTotalMonthly[8]
            tempactivity.month9 = dAgentTotalMonthly[9]
            tempactivity.month10 = dAgentTotalMonthly[10]
            tempactivity.month11 = dAgentTotalMonthly[11]
            tempactivity.month12 = dAgentTotalMonthly[12]
            .
            
      end.

  end.
  
  /* Copy totalled data in ttactivityMetrics */
  empty temp-table ttactivityMetrics.
  for each tempActivity:
    create ttactivityMetrics.
    buffer-copy tempActivity to ttactivityMetrics.
  end.

  for each ttactivityMetrics:
    ttactivityMetrics.typeDesc = getActivityType(ttactivityMetrics.type).
    
    /* get totals */
    empty temp-table tempactivity.  
    create tempactivity.
    buffer-copy ttactivityMetrics to tempactivity.
    assign
        ttactivityMetrics.Total   = getActivityTotal(table tempactivity, "Year")
        ttactivityMetrics.yrTotal = getActivityTotal(table tempactivity, "Current")
        .

  end.
  
  getActivityComparedDataMetrics(input ipiYear).

  find first ttActivityMetrics no-error.
  if not available ttActivityMetrics
   then
    do:
         createActivityMetrics(1 , "I", "P", ipiYear - 2).
         createActivityMetrics(1 , "A", "T", ipiYear - 2).
         createActivityMetrics(1 , "I", "P", ipiYear - 1).
         createActivityMetrics(1 , "A", "T", ipiYear - 1).
         getActivityComparedDataMetrics(input ipiYear).
      
    end.
  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openAlerts wWin 
FUNCTION openAlerts RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  run loadAlerts   in this-procedure.
  run getAlertDesc in this-procedure.
  assign
      cLastAlertOwner = "ALL"
      cLastAlertCode  = "ALL"
      cLastAgentID    = "ALL"
      .
      
  run setAlertCombos in this-procedure.
  run filterAlerts in this-procedure.   
 
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openAr wWin 
FUNCTION openAr RETURNS LOGICAL
  (input iplRefresh as logical) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
  define variable tLoadedARAging as logical no-undo.
  define buffer  arAging for arAging.

  run server/queryagentgrpfileaging.p ( input datetime(today),
                                       input "S", 
                                       input no,
                                       input pStateID,
                                       input pYearOfSign,
                                       input pSoftware,
                                       input pCompany,
                                       input pOrganization,
                                       input pManager,
                                       input pTagList,
                                       input pAffiliationList,
                                       output table aRAging,
                                       output cArAgentEmail,
                                       output tLoadedARAging,
                                       output std-ch).


    if not tLoadedARAging
     then
      do:
        if lookup("is not active.",std-ch," ") = 0
         then
          message std-ch
           view-as alert-box.
         return false. 
      end.
      
  if not lRefreshAR 
   then
    assign
        cbAgentsAR:delimiter in frame frAR    = "|"
        cbAgentsAR:list-item-pairs            = cAgentLIPairs
        cbAgentsAR:screen-value               = "All"
        .

  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openAudit wWin 
FUNCTION openAudit RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
  define buffer taudit for taudit.
  define buffer taction for taction.
  define buffer tFinding for tFinding.
  
  define variable tLoadedAudits as logical no-undo.
  define variable tLoadedActions as logical no-undo.

  run server/getagentgroupallaudits.p (
                             input 0,         /* year */
                             input "ALL",     /* auditor */
                             input "ALL",     /* status */
                             input 0,         /* start gap */
                             input 0,         /* audit gap */
                             input pStateID,
                             input pYearOfSign,
                             input pSoftware,
                             input pCompany,
                             input pOrganization,
                             input pManager,
                             input pTagList,
                             input pAffiliationList,
                             output table taudit,
                             output tLoadedAudits,
                             output std-ch
                             ).
                                   
  if not tLoadedAudits
   then
    do:
      message std-ch 
        view-as alert-box.
      return false.  
    end.
   
   for each tAudit:
     taudit.cQarID = string(taudit.qarID).
   end.
   
   assign
      cbAgentsAudit:delimiter in frame frAudit    = "|"
      cbAgentsAudit:list-item-pairs               = cAgentLIPairs
      cbAgentsAudit:screen-value in frame frAudit = "All"
      .
  

  run filterAudits in this-procedure.
 
  open query brwQAR for each audit no-lock by audit.agentid by audit.auditStartDate desc.

  
  run server\getagentgroupactions.p (input   0 ,  /*piFindingID */
                           input   "",            /*pcStat -- Only 'closed' findings */
                           input   "O",           /*pcStatus -- Action stat*/
                           input   "C",           /* pcAction -- Action type */
                           input   0,             /* piYear */
                           input   "Agent",       /* entity */
                           input   cAgentList,    /* entityID */
                           input   "",            /*pcOwner*/
                           output  table  taction,
                           output  table  tfinding,
                           output tLoadedActions,
                           output std-ch).
                                   
  if not tLoadedActions
   then
    do:
      message std-ch 
        view-as alert-box.
      return false.  
    end.
  
  run filterActions in this-procedure.
  open query brwAction for each action. 

  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openBatchesYearly wWin 
FUNCTION openBatchesYearly RETURNS LOGICAL
  (input ipcYear as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
  define variable ipiYear as integer no-undo.

  do with frame frBatch:
  end.
  
  if ipcYear = "" 
   then return false.
  
  ipiYear = integer(ipcYear) no-error.
  if error-status:error
   then return false.
  
  if  cAgentList = ""
   then
    return false.
  
  empty temp-table Batch.
  
  run server/getagentgroupbatchesyearly.p(input cAgentList,
                                         input ipiYear,
                                         output table tBatch,
                                         output std-lo,
                                         output std-ch
                                         ).        
  if not std-lo
       then
        do:
          message std-ch
            view-as alert-box.
          return  false.
        end.
  
  run filterBatches in this-procedure.
  close query brwBatch.      
  open query brwBatch for each batch by batch.agentid by batch.batchid.
  
  assign
    bBatchRefresh:sensitive        = true
/*     cbAgents:sensitive             = if brwBatch:num-iterations <= 0 then false else true */
    .
    
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openclaim wWin 
FUNCTION openclaim RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
  define buffer tAgentclaim for tAgentclaim.
  
  define variable tLoadedAgentClaim  as logical no-undo.

  run server/getclaimsbyagentgroup.p ( input 0,
                                       input "",
                                       input "",
                                       input ?,
                                       input ?,
                                       input pStateID,
                                       input pYearOfSign,
                                       input pSoftware,
                                       input pCompany,
                                       input pOrganization,
                                       input pManager,
                                       input pTagList,
                                       input pAffiliationList,
                                       output table tAgentclaim,
                                       output tLoadedAgentClaim,
                                       output std-ch).
  if not tLoadedAgentClaim
     then
      do:
        message std-ch 
          view-as alert-box.
        return false.  
      end.
  
  if not lRefreshClaims 
   then
    assign
        cbAgentsClm:delimiter in frame frClaim    = "|"
        cbAgentsClm:list-item-pairs               = cAgentLIPairs
        cbAgentsClm:screen-value in frame frClaim = "All"
        .
  run filterClaims in this-procedure.
      
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openQualification wWin 
FUNCTION openQualification RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tLoadedQuals as logical no-undo.
  define buffer tqualification for tqualification.

  run server/getagentgroupqualifications.p (input pStateID,
                                            input pYearOfSign,
                                            input pSoftware,
                                            input pCompany,
                                            input pOrganization,
                                            input pManager,
                                            input pTagList,
                                            input pAffiliationList,
                                            output table tqualification,
                                            output tLoadedQuals,
                                            output std-ch).
      
  if not tLoadedQuals
   then 
    do:
      if lookup("is not active.",std-ch," ") = 0
       then
        message std-ch view-as alert-box error buttons ok.
      return false.
    end. 
  
  if not lRefreshQualifications
   then
    assign
        cbAgentsQual:delimiter in frame frCompliance    = "|"
        cbAgentsQual:list-item-pairs               = cAgentLIPairs
        cbAgentsQual:screen-value in frame frCompliance = "All"
        .
  run filterQualifications in this-procedure.
  
  open query brwqualification for each qualification by qualification.entityID. 
  
  return true.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openReqFulFillment wWin 
FUNCTION openReqFulFillment RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  define variable tLoadedReqfulfill as logical no-undo.
  define buffer treqfulfill for treqfulfill.

  run server/getagentgroupfulfillments.p(input cAgentList,
                                   output table treqfulfill,
                                   output tLoadedReqfulfill,
                                   output std-ch
                                  ).
  if not tLoadedReqfulfill
   then
    do:
      if lookup("is not active.",std-ch," ") = 0
       then
        message std-ch
         view-as alert-box.
      return false.   
    end.
  
/*   if not lRefreshReqFulFillment                                    */
/*    then                                                            */
/*     assign                                                         */
/*         cbAgentsQual:delimiter in frame frCompliance    = "|"      */
/*         cbAgentsQual:list-item-pairs               = cAgentLIPairs */
/*         cbAgentsQual:screen-value in frame frCompliance = "All"    */
/*         .                                                          */
      
  run filterReqfulfillments in this-procedure.
  
  open query brwReqFul for each reqfulfill by reqfulfill.orgRoleID.
    
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openURL wWin 
FUNCTION openURL RETURNS LOGICAL PRIVATE
  ( input pURL as char ) :


  run ShellExecuteA in this-procedure (0,
                                       "open",
                                       pURL,
                                       "",
                                       "",
                                       1,
                                       output std-in).

 return true.

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setPageState wWin 
FUNCTION setPageState RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int, input pState as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  case pPageNumber:
    when 1 then
    run setContainerState(frame frDetail:handle, pState).
    when 2 then
    run setContainerState(frame frBatch:handle, pState).
    when 3 then
    run setContainerState(frame frActivity:handle, pState).
    when 4 then
    run setContainerState(frame frAR:handle, pState).    
    when 5 then
    run setContainerState(frame frClaim:handle, pState).
    when 6 then
    run setContainerState(frame frAudit:handle, pState).
    when 7 then
    run setContainerState(frame frCompliance:handle, pState). 
    when 8 then
    run setContainerState(frame frAlert:handle, pState).
  end case.

  return false.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

