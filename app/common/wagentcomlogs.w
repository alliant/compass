&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wagentcomlogs.w

  Description: Window of system logs

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gurvindar

  Created: 09.25.2018

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
   by this procedure. This is a good default which assures
   that this procedure's triggers and internal procedures 
   will execute in this procedure's storage, and that proper
   cleanup will occur on deletion of the procedure. */

create widget-pool.
define input parameter hFileDataSrv as handle no-undo.
{lib/std-def.i}
{lib/sys-def.i}
{lib/com-def.i}
{tt/agent.i}
{tt/comLog.i}
{tt/comLog.i &tableAlias=ttComLog}
{lib/winshowscrollbars.i}
{lib/get-column.i} 
/*------------------------Temp table Definitions--------------------*/
{tt/sysLog.i}

define temp-table ttSyslog no-undo like syslog
  field clogID as character.

/* Variable Definition */
define variable dColumnWidth   as decimal   no-undo.
define variable  cRoles as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwComLog

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES comLog

/* Definitions for BROWSE brwComLog                                     */
&Scoped-define FIELDS-IN-QUERY-brwComLog getComStat(comLog.sequence) @ comLog.sequence comLog.role comLog.logdate comLog.username comLog.notes   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwComLog   
&Scoped-define SELF-NAME brwComLog
&Scoped-define QUERY-STRING-brwComLog for each comLog by comLog.logdate descending
&Scoped-define OPEN-QUERY-brwComLog open query {&SELF-NAME} for each comLog by comLog.logdate descending.
&Scoped-define TABLES-IN-QUERY-brwComLog comLog
&Scoped-define FIRST-TABLE-IN-QUERY-brwComLog comLog


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwComLog}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bComLogOpen dtStartDate dtEndDate bLogExport ~
bGetLogs brwComLog RECT-55 RECT-56 RECT-54 
&Scoped-Define DISPLAYED-OBJECTS cbRole dtStartDate dtEndDate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getComStat C-Win 
FUNCTION getComStat RETURNS CHARACTER
  ( input cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openComLog C-Win 
FUNCTION openComLog RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validateData C-Win 
FUNCTION validateData RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bComLogOpen  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open log".

DEFINE BUTTON bGetLogs  NO-FOCUS
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE BUTTON bLogExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE VARIABLE cbRole AS CHARACTER FORMAT "X(256)":U 
     LABEL "Role" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 23.8 BY 1 NO-UNDO.

DEFINE VARIABLE dtEndDate AS DATE FORMAT "99/99/99":U 
     LABEL "End Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE dtStartDate AS DATE FORMAT "99/99/99":U 
     LABEL "Start Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-54
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 38 BY 2.48.

DEFINE RECTANGLE RECT-55
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 66 BY 2.48.

DEFINE RECTANGLE RECT-56
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 19.2 BY 2.48.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwComLog FOR 
      comLog SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwComLog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwComLog C-Win _FREEFORM
  QUERY brwComLog DISPLAY
      getComStat(comLog.sequence)  @ comLog.sequence label ""        format "x(2)"                 width 2   
comLog.role                                    label "Role"    format "x(25)"                width 18
comLog.logdate                                 label "Update"  format "99/99/9999 HH:MM:SS"  width 23
comLog.username                                label "User ID" format "x(33)"                width 25
comLog.notes                                   label "Notes"   format "x(200)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 148 BY 15.95
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bComLogOpen AT ROW 2.67 COL 107.6 WIDGET-ID 266 NO-TAB-STOP 
     cbRole AT ROW 2.95 COL 75.2 COLON-ALIGNED WIDGET-ID 270
     dtStartDate AT ROW 3.05 COL 12.6 COLON-ALIGNED WIDGET-ID 110
     dtEndDate AT ROW 3.05 COL 39.4 COLON-ALIGNED WIDGET-ID 112
     bLogExport AT ROW 2.67 COL 114.8 WIDGET-ID 388 NO-TAB-STOP 
     bGetLogs AT ROW 2.67 COL 58.4 WIDGET-ID 382 NO-TAB-STOP 
     brwComLog AT ROW 5.91 COL 2.2 WIDGET-ID 2400
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.95 COL 106.6 WIDGET-ID 386
     "Parameters" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 1.95 COL 3.2 WIDGET-ID 108
     "Filters" VIEW-AS TEXT
          SIZE 5.4 BY .62 AT ROW 2 COL 69.2 WIDGET-ID 272
     RECT-55 AT ROW 2.29 COL 2 WIDGET-ID 106
     RECT-56 AT ROW 2.29 COL 105.4 WIDGET-ID 384
     RECT-54 AT ROW 2.29 COL 67.6 WIDGET-ID 92
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 150.4 BY 21.14 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Compliance Logs"
         HEIGHT             = 21.95
         WIDTH              = 150.4
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwComLog bGetLogs fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwComLog:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwComLog:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwComLog:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR COMBO-BOX cbRole IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwComLog
/* Query rebuild information for BROWSE brwComLog
     _START_FREEFORM
open query {&SELF-NAME} for each comLog by comLog.logdate descending.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwComLog */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Compliance Logs */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Compliance Logs */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Compliance Logs */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bComLogOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bComLogOpen C-Win
ON CHOOSE OF bComLogOpen IN FRAME fMain /* Open */
do:
  run openLog in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGetLogs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGetLogs C-Win
ON CHOOSE OF bGetLogs IN FRAME fMain /* Get */
do:
  /* Validation for start and end date. */
  run validateComLogDate in this-procedure (output std-lo).  
  
  /* if error is returned from the validate procedure, then return. */
  if std-lo 
   then
    return no-apply.
  
  openComLog().
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLogExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLogExport C-Win
ON CHOOSE OF bLogExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwComLog
&Scoped-define SELF-NAME brwComLog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwComLog C-Win
ON DEFAULT-ACTION OF brwComLog IN FRAME fMain
DO:
  run openLog in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwComLog C-Win
ON ROW-DISPLAY OF brwComLog IN FRAME fMain
do:
  {lib/brw-rowDisplay.i}

  /* "3" = Red, 2 = "Yellow", 1 = "Green" */
  case comLog.sequence:
    when "3" then  
      comLog.sequence:bgcolor in browse brwComLog = 12.

    when "2" then 
      comLog.sequence:bgcolor in browse brwComLog = 14.

    when "1" then
      comLog.sequence:bgcolor in browse brwComLog =  2.
  end case.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwComLog C-Win
ON START-SEARCH OF brwComLog IN FRAME fMain
do:
  define variable hsort as handle no-undo.

  hsort = browse {&browse-name}:current-column.

  /* Standard browse sorting does not work if the column label is blank.
     Therefore, set the label of first column,
     sort the browse, then reset the label. */

  if hsort:label = "" then
    hsort:label = "Comstat".
  
  {lib/brw-startSearch.i}
  
  if hsort:label = "Comstat" then
    hsort:label = "".
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRole C-Win
ON VALUE-CHANGED OF cbRole IN FRAME fMain /* Role */
do:
  run filterData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/****************************  Main Block  ****************************/

{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

assign 
  current-window                = {&window-name} 
  this-procedure:current-window = {&window-name}
  .

{lib/win-main.i}
{lib/win-status.i}

on close of this-procedure 
  run disable_UI.

setStatusMessage("").

pause 0 before-hide.

bGetLogs    :load-image("images/refresh.bmp").
bGetLogs    :load-image-insensitive("images/refresh-i.bmp").
bComLogOpen :load-image("images/magnifier.bmp").
bComLogOpen :load-image-insensitive("images/magnifier-i.bmp").
bLogExport  :load-image("images/excel.bmp").
bLogExport  :load-image-insensitive("images/excel-i.bmp").
    

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

  run enable_UI.  

  /* get the column width */
  {lib/get-column-width.i &col="'Action'"  &var=dColumnWidth} 
  
  /* procedure to set default start and end date */
  run setData in this-procedure.

  run showWindow in this-procedure.

  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbRole dtStartDate dtEndDate 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bComLogOpen dtStartDate dtEndDate bLogExport bGetLogs brwComLog 
         RECT-55 RECT-56 RECT-54 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

/*   if query brwData:num-results = 0                                                                                                                                                                                                                                 */
/*    then                                                                                                                                                                                                                                                            */
/*     do:                                                                                                                                                                                                                                                            */
/*       message "There is nothing to export"                                                                                                                                                                                                                         */
/*           view-as alert-box warning buttons ok.                                                                                                                                                                                                                    */
/*       return.                                                                                                                                                                                                                                                      */
/*     end.                                                                                                                                                                                                                                                           */
/*                                                                                                                                                                                                                                                                    */
/*   publish "GetReportDir" (output std-ch).                                                                                                                                                                                                                          */
/*                                                                                                                                                                                                                                                                    */
/*                                                                                                                                                                                                                                                                    */
/*   std-ha = temp-table ttsyslog:handle.                                                                                                                                                                                                                             */
/*   run util/exporttable.p (table-handle std-ha,                                                                                                                                                                                                                     */
/*                           "ttsyslog",                                                                                                                                                                                                                              */
/*                           "for each ttsyslog ",                                                                                                                                                                                                                    */
/*                           "clogID,createDate,action,uid,applicationID,addr,role,progExec,duration,isError,msg,faultCode,refType,refNum,sysMsg",                                                                                                                    */
/*                           "From " + fStartDate:screen-value + " To " + fEndDate:screen-value + chr(10) + "LogID,Create Date,Action,UID,Module ID,Address,Role,Program execution,Duration,Error,Message,Fault Code,Reference Type,Reference Number,System Message", */
/*                           std-ch,                                                                                                                                                                                                                                  */
/*                           "SystemLog-" + replace(string(now,"99-99-99"),"-","") + "-" + replace(string(time,"HH:MM:SS"),":","") + ".csv",                                                                                                                          */
/*                           true,                                                                                                                                                                                                                                    */
/*                           output std-ch,                                                                                                                                                                                                                           */
/*                           output std-in).                                                                                                                                                                                                                          */
/*                                                                                                                                                                                                                                                                    */
/*   if std-ch <> ""                                                                                                                                                                                                                                                  */
/*    then                                                                                                                                                                                                                                                            */
/*     do:                                                                                                                                                                                                                                                            */
/*       message std-ch                                                                                                                                                                                                                                               */
/*           view-as alert-box warning buttons ok.                                                                                                                                                                                                                    */
/*       return.                                                                                                                                                                                                                                                      */
/*     end.                                                                                                                                                                                                                                                           */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*   define variable iCount  as integer   no-undo.                              */
/*   define variable cStatus as character no-undo.                              */
/*                                                                              */
/*   define buffer syslog for syslog.                                           */
/*                                                                              */
/*   close query brwData.                                                       */
/*   empty temp-table ttsyslog.                                                 */
/*                                                                              */
/*   do with frame {&frame-name}:                                               */
/*   end.                                                                       */
/*                                                                              */
/*   for each syslog:                                                           */
/*     /* test if the record contains the search text */                        */
/*     if fSearch:input-value <> "" and                                         */
/*       not ( (syslog.action      matches "*" + fSearch:input-value + "*")  or */
/*             (syslog.progExec    matches "*" + fSearch:input-value + "*")  or */
/*             (syslog.UID         matches "*" + fSearch:input-value + "*")     */
/*              )                                                               */
/*      then                                                                    */
/*       next.                                                                  */
/*                                                                              */
/*     create ttsyslog.                                                         */
/*     assign ttsyslog.clogID = string(syslog.logID) .                          */
/*     buffer-copy syslog to ttsyslog.                                          */
/*   end.                                                                       */
/*                                                                              */
/*   open query brwData preselect each ttsyslog by ttsyslog.createDate desc.    */
/*                                                                              */
/*   if (query brwData:num-results) > 0                                         */
/*    then                                                                      */
/*     assign                                                                   */
/*       bExport:sensitive = true                                               */
/*       bView:sensitive   = true.                                              */
/*    else                                                                      */
/*     assign                                                                   */
/*       bExport:sensitive = false                                              */
/*       bView:sensitive   = false.                                             */
/*                                                                              */
/*   setStatusCount(query brwData:num-results).                                 */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  run server/getsyslogs.p(input {&CalledAction}, /* (C)alledAction, for all actions */
                          input "All",
                          input "All",
                          dtStartDate:input-value,
                          dtendDate:input-value,
                          output table sysLog,
                          output std-lo,
                          output std-ch).
  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.

  run filterData.

/*   if (query brwData:num-results) > 0           */
/*    then                                        */
/*     assign                                     */
/*       fSearch:sensitive = true                 */
/*       bSearch:sensitive = true                 */
/*       .                                        */
/*    else                                        */
/*     assign                                     */
/*       fSearch:sensitive = false                */
/*       bSearch:sensitive = false                */
/*       .                                        */
/*                                                */
/*   setStatusRecords(query brwData:num-results). */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData C-Win 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Get default no. of days from dialog config to initialise start date */
  publish "getDefaultdays" (output std-in).

  assign
    dtStartDate:screen-value = string(today - std-in)
    dtEndDate:screen-value   = string(today)
    .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showSysLog C-Win 
PROCEDURE showSysLog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table syslog.
  
  if available ttsyslog
   then
    do:
      create syslog.
      buffer-copy ttsyslog to syslog.
      run dialogsyslog.w (input table syslog).
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized
   then
    {&window-name}:window-state = window-normal .
  
  c-Win:move-to-top(). 

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i}   

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
    frame fMain:width-pixels          = {&window-name}:width-pixels
    frame fMain:virtual-width-pixels  = {&window-name}:width-pixels
    frame fMain:height-pixels         = {&window-name}:height-pixels
    frame fMain:virtual-height-pixels = {&window-name}:height-pixels

    /* fMain Components */
/*     brwData:width-pixels              = frame fmain:width-pixels  - 13 */
/*     brwData:height-pixels             = frame fMain:height-pixels - 70 */
    .

  {lib/resize-column.i &col="'createDate,Action,UID'" &var=dColumnWidth}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getComStat C-Win 
FUNCTION getComStat RETURNS CHARACTER
  ( input cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
case cStat:
   when "3" 
    then 
     return {&Rcode}.

   when "1" 
    then
     return {&Gcode}.

   when "2" 
    then
     return {&Ycode}.
 end case.
 
 return "".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openComLog C-Win 
FUNCTION openComLog RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  find first agent no-error.
  if not available agent
   then
    return false.
  
  /* Get comlog record from server */
  run server/getcompliancelogs.p (input "A" ,                 /* Agent/Attorney/Person/Company */
                                  input agent.agentID,                /* Entity ID   */
                                  input dtStartDate:input-value in frame {&frame-name},  /*  Start Date */
                                  input dtEndDate:input-value in frame {&frAme-name},    /*  End Date   */
                                  input "ALL",                    /* State ID    */
                                  output table ttComLog,
                                  output std-lo ,                 /* Success or not */           
                                  output std-ch).
  if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box.
      return false.
    end.
  
  cRoles = "".
  for each ttComLog break by ttComlog.objRef:
    if first-of(ttComlog.objRef) 
     then
      cRoles = cRoles + "," + ttComlog.objRef. 
  end.
  cRoles = trim(cRoles,",").
  cbRole:list-items   = if cRoles = "" then {&ALL} else {&ALL} + "," + cRoles no-error.
  cbRole:screen-value = {&ALL} no-error.
  
  run filterComLogs in this-procedure.     

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validateData C-Win 
FUNCTION validateData RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if interval(dtEndDate:input-value, dtStartDate:input-value, "days") > 30 
   then
    do:
      message "The date range is greater than 30 days, the report will be slow. Are you sure you want to continue?" 
        view-as alert-box warning buttons yes-no update std-lo.
     
      if not std-lo 
       then
        return false.
    end.
 
  if interval(dtEndDate:input-value, dtStartDate:input-value, "years") > 0
  then
    do:
      message "Date range cannot be more than a year." 
        view-as alert-box error buttons ok.
     
      return false.
    end.

  return true.   /* function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

