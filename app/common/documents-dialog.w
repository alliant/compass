&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input  parameter pApp          as character no-undo. /* Application ID */
define input  parameter pServerPath   as character no-undo.
define input  parameter pDesc         as character no-undo. /* UI Transaction description */
define input  parameter pValidActions as character no-undo. /* new,delete,open,modify,send,request */

/* Used to call Compass server to hold reference that docs exist */
define input  parameter pEntityType   as character no-undo.
define input  parameter pEntityID     as character no-undo.
define input  parameter pEntitySeq    as integer   no-undo.

/* return the item id and a link to the file */
define output parameter pItemID       as character no-undo.
define output parameter pLink         as character no-undo.

/* Local Variable Definitions ---                                       */
{lib/std-def.i}
{lib/brw-multi-def.i}
{lib/set-button-def.i}

define variable tRootFolder    as character no-undo.
define variable tCurrentFolder as character no-undo initial ".". /* Set to "." to initialize first load of folder list */
define variable tDisable       as logical   no-undo initial false.

/* temp tables */
{tt/doc.i}
{tt/doc.i &tableAlias="folder"}
{tt/doc.i &tableAlias="root"}
{tt/docuser.i}

/* functions and procedures */
{lib/add-delimiter.i}
{lib/urlencode.i}
{lib/winlaunch.i}
{lib/repository-procedures.i}

/* menus */
DEFINE MENU folderpopmenu TITLE "Folder Actions"
  menu-item m_PopNew label "New..."
  menu-item m_PopRename label "Rename..."
  rule
  menu-item m_PopDelete label "Delete..."
  .

ON 'choose':U OF menu-item m_PopNew in menu folderpopmenu
DO:
  run ActionNewFolder in this-procedure.
  RETURN.
END.

ON 'choose':U OF menu-item m_PopRename in menu folderpopmenu
DO:
  run ActionRenameFolder in this-procedure.
  RETURN.
END.

ON 'choose':U OF menu-item m_PopDelete in menu folderpopmenu
DO:
  run ActionDeleteFolder in this-procedure.
  RETURN.
END.

DEFINE MENU docpopmenu TITLE "File Actions"
  menu-item m_PopOpen LABEL "Open"
  rule 
  menu-item m_PopNew label "Upload..."
  menu-item m_PopNotes label "Edit Notes..."
  rule
  menu-item m_PopDelete label "Delete..."
  .

ON 'choose':U OF menu-item m_PopOpen in menu docpopmenu
DO:
  run ActionOpenFile in this-procedure (output std-lo).
  RETURN.
END.

ON 'choose':U OF menu-item m_PopNew in menu docpopmenu
DO:
  run ActionNewFile in this-procedure.
  RETURN.
END.

ON 'choose':U OF menu-item m_PopNotes in menu docpopmenu
DO:
  run ActionEditNotes in this-procedure.
  RETURN.
END.

ON 'choose':U OF menu-item m_PopDelete in menu docpopmenu
DO:
  run ActionDeleteFile in this-procedure.
  RETURN.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwDocs

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES document folder

/* Definitions for BROWSE brwDocs                                       */
&Scoped-define FIELDS-IN-QUERY-brwDocs document.displayName document.createdDate document.filetype   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDocs   
&Scoped-define SELF-NAME brwDocs
&Scoped-define QUERY-STRING-brwDocs FOR EACH document by document.displayName
&Scoped-define OPEN-QUERY-brwDocs OPEN QUERY {&SELF-NAME} FOR EACH document by document.displayName.
&Scoped-define TABLES-IN-QUERY-brwDocs document
&Scoped-define FIRST-TABLE-IN-QUERY-brwDocs document


/* Definitions for BROWSE brwFolders                                    */
&Scoped-define FIELDS-IN-QUERY-brwFolders folder.displayName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFolders   
&Scoped-define SELF-NAME brwFolders
&Scoped-define QUERY-STRING-brwFolders FOR EACH folder by folder.displayName
&Scoped-define OPEN-QUERY-brwFolders OPEN QUERY {&SELF-NAME} FOR EACH folder by folder.displayName.
&Scoped-define TABLES-IN-QUERY-brwFolders folder
&Scoped-define FIRST-TABLE-IN-QUERY-brwFolders folder


/* Definitions for DIALOG-BOX Dialog-Frame                              */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwFolders brwDocs bOK bCancel bRefresh ~
bFileRefresh 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFileType C-Win 
FUNCTION getFileType RETURNS CHARACTER
  ( input pFileName as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshDocs C-Win 
FUNCTION refreshDocs RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshFolders C-Win 
FUNCTION refreshFolders RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD uploadFile C-Win 
FUNCTION uploadFile RETURNS LOGICAL PRIVATE
  ( input pFile as character,
    output pMsg as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bFileDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete selected document(s)".

DEFINE BUTTON bFileDownload  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open the selected document(s)".

DEFINE BUTTON bFileModify  NO-FOCUS
     LABEL "Edit File" 
     SIZE 7.2 BY 1.71 TOOLTIP "Edit document filename and notes".

DEFINE BUTTON bFileRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh the list of documents".

DEFINE BUTTON bFileUpload  NO-FOCUS
     LABEL "Upload" 
     SIZE 7.2 BY 1.71 TOOLTIP "Upload a new document".

DEFINE BUTTON bFolderDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete selected folder(s)".

DEFINE BUTTON bFolderNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Create a new folder".

DEFINE BUTTON bFolderRename  NO-FOCUS
     LABEL "Rename" 
     SIZE 7.2 BY 1.71 TOOLTIP "Rename selected folder".

DEFINE BUTTON bOK 
     LABEL "OK" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh the list of folders".

DEFINE RECTANGLE rButtons
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 38.2 BY 2.05.

DEFINE RECTANGLE rButtons-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 30.8 BY 2.05.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwDocs FOR 
      document SCROLLING.

DEFINE QUERY brwFolders FOR 
      folder SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwDocs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDocs Dialog-Frame _FREEFORM
  QUERY brwDocs DISPLAY
      document.displayName format "x(256)" width 37 label "File Name"
document.createdDate format "x(24)" width 28 label "Date Saved"
document.filetype format "x(12)" label "Type"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET SIZE 86 BY 10.86
         TITLE "Documents" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwFolders
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFolders Dialog-Frame _FREEFORM
  QUERY brwFolders DISPLAY
      folder.displayName format "x(256)" width 50 label "Folder Name"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET SIZE 30.8 BY 10.86
         TITLE "Folders" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bFolderNew AT ROW 1.38 COL 2.6 WIDGET-ID 44
     brwFolders AT ROW 3.52 COL 2 WIDGET-ID 300
     brwDocs AT ROW 3.52 COL 34 WIDGET-ID 200
     bOK AT ROW 14.57 COL 45.6 WIDGET-ID 64
     bCancel AT ROW 14.57 COL 61.6 WIDGET-ID 66
     bFileModify AT ROW 1.38 COL 49.4 WIDGET-ID 36
     bRefresh AT ROW 1.38 COL 24.8 WIDGET-ID 54
     bFileDelete AT ROW 1.38 COL 56.8 WIDGET-ID 6
     bFolderRename AT ROW 1.38 COL 10 WIDGET-ID 52
     bFolderDelete AT ROW 1.38 COL 17.4 WIDGET-ID 46
     bFileDownload AT ROW 1.38 COL 34.6 WIDGET-ID 24
     bFileRefresh AT ROW 1.38 COL 64.2 WIDGET-ID 32
     bFileUpload AT ROW 1.38 COL 42 WIDGET-ID 4
     rButtons AT ROW 1.24 COL 34 WIDGET-ID 22
     rButtons-2 AT ROW 1.24 COL 2 WIDGET-ID 48
     SPACE(88.19) SKIP(12.75)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Select Document" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
/* BROWSE-TAB brwFolders bFolderNew Dialog-Frame */
/* BROWSE-TAB brwDocs brwFolders Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bFileDelete IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       bFileDelete:PRIVATE-DATA IN FRAME Dialog-Frame     = 
                "Delete".

/* SETTINGS FOR BUTTON bFileDownload IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       bFileDownload:PRIVATE-DATA IN FRAME Dialog-Frame     = 
                "Open".

/* SETTINGS FOR BUTTON bFileModify IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       bFileModify:PRIVATE-DATA IN FRAME Dialog-Frame     = 
                "Email".

ASSIGN 
       bFileRefresh:PRIVATE-DATA IN FRAME Dialog-Frame     = 
                "Email".

/* SETTINGS FOR BUTTON bFileUpload IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       bFileUpload:PRIVATE-DATA IN FRAME Dialog-Frame     = 
                "New".

/* SETTINGS FOR BUTTON bFolderDelete IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       bFolderDelete:PRIVATE-DATA IN FRAME Dialog-Frame     = 
                "Delete".

/* SETTINGS FOR BUTTON bFolderNew IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       bFolderNew:PRIVATE-DATA IN FRAME Dialog-Frame     = 
                "New".

/* SETTINGS FOR BUTTON bFolderRename IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       bFolderRename:PRIVATE-DATA IN FRAME Dialog-Frame     = 
                "Email".

ASSIGN 
       bRefresh:PRIVATE-DATA IN FRAME Dialog-Frame     = 
                "Email".

ASSIGN 
       brwDocs:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE
       brwDocs:COLUMN-MOVABLE IN FRAME Dialog-Frame         = TRUE.

ASSIGN 
       brwFolders:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE
       brwFolders:COLUMN-MOVABLE IN FRAME Dialog-Frame         = TRUE.

/* SETTINGS FOR RECTANGLE rButtons IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE rButtons-2 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDocs
/* Query rebuild information for BROWSE brwDocs
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH document by document.displayName.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwDocs */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFolders
/* Query rebuild information for BROWSE brwFolders
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH folder by folder.displayName.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwFolders */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Select Document */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel Dialog-Frame
ON CHOOSE OF bCancel IN FRAME Dialog-Frame /* Cancel */
DO:
  apply "WINDOW-CLOSE" to frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileDelete Dialog-Frame
ON CHOOSE OF bFileDelete IN FRAME Dialog-Frame /* Del */
DO:
  run ActionDeleteFile in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileDownload
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileDownload Dialog-Frame
ON CHOOSE OF bFileDownload IN FRAME Dialog-Frame /* Open */
DO:
   run ActionOpenFile in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileModify Dialog-Frame
ON CHOOSE OF bFileModify IN FRAME Dialog-Frame /* Edit File */
DO:
  run ActionEditNotes in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileRefresh Dialog-Frame
ON CHOOSE OF bFileRefresh IN FRAME Dialog-Frame /* Refresh */
DO:
  refreshDocs().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileUpload
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileUpload Dialog-Frame
ON CHOOSE OF bFileUpload IN FRAME Dialog-Frame /* Upload */
DO:
  run ActionNewFile in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFolderDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFolderDelete Dialog-Frame
ON CHOOSE OF bFolderDelete IN FRAME Dialog-Frame /* Del */
DO:
  run ActionDeleteFolder in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFolderNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFolderNew Dialog-Frame
ON CHOOSE OF bFolderNew IN FRAME Dialog-Frame /* New */
DO:
  run ActionNewFolder in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFolderRename
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFolderRename Dialog-Frame
ON CHOOSE OF bFolderRename IN FRAME Dialog-Frame /* Rename */
DO:
  run ActionRenameFolder in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOK Dialog-Frame
ON CHOOSE OF bOK IN FRAME Dialog-Frame /* OK */
DO:
  if not available document
   then
    do:
      message "Please select a file" view-as alert-box warning buttons ok.
      return.
    end.
   else
    do:
      pItemID = document.identifier.
      run GetRepositoryItemLink (document.fullpath, output pLink, output std-lo, output std-ch).
      if not std-lo
       then message std-ch view-as alert-box error buttons ok.
       else apply "CHOOSE" to bCancel.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh Dialog-Frame
ON CHOOSE OF bRefresh IN FRAME Dialog-Frame /* Refresh */
DO:
  refreshFolders().
  refreshDocs().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDocs
&Scoped-define SELF-NAME brwDocs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs Dialog-Frame
ON DEFAULT-ACTION OF brwDocs IN FRAME Dialog-Frame /* Documents */
DO:
   apply "CHOOSE" to bOK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs Dialog-Frame
ON DROP-FILE-NOTIFY OF brwDocs IN FRAME Dialog-Frame /* Documents */
DO:
  run ActionFilesDropped in this-procedure (self:handle).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs Dialog-Frame
ON ROW-DISPLAY OF brwDocs IN FRAME Dialog-Frame /* Documents */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs Dialog-Frame
ON START-SEARCH OF brwDocs IN FRAME Dialog-Frame /* Documents */
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs Dialog-Frame
ON VALUE-CHANGED OF brwDocs IN FRAME Dialog-Frame /* Documents */
DO:
  if brwDocs:num-selected-rows > 0 
   then assign
          bFileDownload:sensitive = lookup("open", pValidActions) > 0
          bFileDelete:sensitive = lookup("delete", pValidActions) > 0
          bFileModify:sensitive = lookup("modify", pValidActions) > 0 
          menu-item m_PopOpen:sensitive in menu docpopmenu = bFileDownload:sensitive
          menu-item m_PopDelete:sensitive in menu docpopmenu = bFileDelete:sensitive
          menu-item m_PopNotes:sensitive in menu docpopmenu = bFileModify:sensitive
          .
   else assign
          bFileDownload:sensitive = false
          bFileDelete:sensitive = false
          bFileModify:sensitive = false
          menu-item m_PopOpen:sensitive in menu docpopmenu = false
          menu-item m_PopDelete:sensitive in menu docpopmenu = false
          menu-item m_PopNotes:sensitive in menu docpopmenu = false
          .
  bFileUpload:sensitive in frame {&frame-name} = lookup("new", pValidActions) > 0.
  menu-item m_PopNew:sensitive in menu docpopmenu = bFileUpload:sensitive.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFolders
&Scoped-define SELF-NAME brwFolders
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFolders Dialog-Frame
ON DEFAULT-ACTION OF brwFolders IN FRAME Dialog-Frame /* Folders */
DO:
   /*run ActionOpen in this-procedure (output std-lo).*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFolders Dialog-Frame
ON DROP-FILE-NOTIFY OF brwFolders IN FRAME Dialog-Frame /* Folders */
DO:
  run ActionFoldersDropped in this-procedure (self:handle).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFolders Dialog-Frame
ON ROW-DISPLAY OF brwFolders IN FRAME Dialog-Frame /* Folders */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFolders Dialog-Frame
ON START-SEARCH OF brwFolders IN FRAME Dialog-Frame /* Folders */
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFolders Dialog-Frame
ON VALUE-CHANGED OF brwFolders IN FRAME Dialog-Frame /* Folders */
DO:
  if brwFolders:num-selected-rows > 0 
   then assign
          bFolderNew:sensitive = lookup("new", pValidActions) > 0
          bFolderRename:sensitive = lookup("modify", pValidActions) > 0 and folder.name <> ""
          bFolderDelete:sensitive = lookup("delete", pValidActions) > 0 and folder.name <> ""
          menu-item m_PopNew:sensitive in menu folderpopmenu = bFolderNew:sensitive
          menu-item m_PopRename:sensitive in menu folderpopmenu = bFolderRename:sensitive
          menu-item m_PopDelete:sensitive in menu folderpopmenu = bFolderDelete:sensitive
          .
   else assign
          bFolderNew:sensitive = lookup("new", pValidActions) > 0
          bFolderRename:sensitive = false 
          bFolderDelete:sensitive = false
          menu-item m_PopNew:sensitive in menu folderpopmenu = false
          menu-item m_PopRename:sensitive in menu folderpopmenu = false
          menu-item m_PopDelete:sensitive in menu folderpopmenu = false
          .
  
  if tCurrentFolder <> folder.name
   then
    do:
      tCurrentFolder = "/" + folder.name.
      refreshDocs().
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDocs
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

{lib/brw-main-multi.i &browse-list="brwDocs,brwFolders"}

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

if pDesc > "" 
 then frame {&frame-name}:title = frame {&frame-name}:title + " for " + pDesc.

{lib/set-button.i &label="FileDownload" &image="images/download.bmp"   &inactive="images/download-i.bmp"}
{lib/set-button.i &label="FileUpload"   &image="images/upload.bmp"     &inactive="images/upload-i.bmp"}
{lib/set-button.i &label="FileDelete"   &image="images/erase.bmp"      &inactive="images/erase-i.bmp"}
{lib/set-button.i &label="FileRefresh"  &image="images/sync.bmp"       &inactive="images/sync-i.bmp"}
{lib/set-button.i &label="FileModify"   &image="images/update.bmp"     &inactive="images/update-i.bmp"}
{lib/set-button.i &label="FolderNew"    &image="images/folder-add.bmp" &inactive="images/folder-add-i.bmp"}
{lib/set-button.i &label="FolderRename" &image="images/update.bmp"     &inactive="images/update-i.bmp"}
{lib/set-button.i &label="FolderDelete" &image="images/erase.bmp"      &inactive="images/erase-i.bmp"}
{lib/set-button.i &label="Refresh"      &image="images/sync.bmp"       &inactive="images/sync-i.bmp"}
setButtons().

browse brwFolders:POPUP-MENU = MENU folderpopmenu:HANDLE.
browse brwDocs:POPUP-MENU = MENU docpopmenu:HANDLE.
       

/* remove the last / or \ */
pServerPath = right-trim(pServerPath, "~\").
pServerPath = right-trim(pServerPath, "~/").

/* Grab the root folder name for display in the browse */
if pServerPath begins "~/" and num-entries(pServerPath, "~/") > 1
 then tRootFolder = entry(num-entries(pServerPath, "~/"), pServerPath, "~/").
 else 
  if pServerPath begins "~\"  and num-entries(pServerPath, "~\") > 1 
   then tRootFolder = entry(num-entries(pServerPath,"~\"), pServerPath, "~\").
   else tRootFolder = "Root".

/* Prepend root folder based on config setting (dev versus prod) */
std-ch = "".
publish "GetRepositoryRoot" (output std-ch).
pServerPath = std-ch + pServerPath.

{lib/pbshow.i "'Initializing window, please wait...'"}
{lib/pbupdate.i "'Initializing window'" 10}

/* login */
/* The reason for no uid or password is because the publish "SetRepositoryToken" will */
/* make a server call to log the user in */
setUserToken("", "", output std-ch). 

/* create the new folder in case it's not created */
run NewRepositoryFolder (pServerPath, output std-lo, output std-ch).
if not std-lo
 then
  do:
    message "Folder not found and could not create folder: " + std-ch view-as alert-box information buttons ok.
    tDisable = true.
  end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  if not tDisable
   then 
    do:
      {lib/pbupdate.i "'Initializing window'" 80}
      refreshFolders().
      apply "value-changed" to brwFolders.
    end.
   else
    assign
      bFileDownload:sensitive  = false
      bFileUpload:sensitive    = false
      bFileDelete:sensitive    = false
      bFileRefresh:sensitive   = false
      bFileModify:sensitive    = false
      bFolderNew:sensitive     = false
      bFolderDelete:sensitive  = false
      bFolderRename:sensitive  = false
      bRefresh:sensitive       = false
      .
  RUN enable_UI.
  
  {lib/pbhide.i}
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDeleteFile C-Win 
PROCEDURE ActionDeleteFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tFileCnt as integer   no-undo.
  define variable tMsg     as character no-undo.
  define variable tGoodCnt as integer   no-undo.
  
  if lookup("delete", pValidActions) = 0
   then return.
  
  if browse brwDocs:num-selected-rows = 0
   then 
    do: bFileDelete:sensitive in frame {&frame-name} = false.
        menu-item m_PopDelete:sensitive in menu docpopmenu = false.
        return.
    end.
  
  {lib/confirm-delete.i "Document(s)"}
  
  do tFileCnt = 1 to browse brwDocs:num-selected-rows:
    browse brwDocs:fetch-selected-row(tFileCnt).
    
    if not available document 
     then next.
    
    if document.identifier = ""
     then
      do: 
        tMsg = tMsg + (if tMsg > "" then chr(19) else "") + document.fullpath + " is invalid.".
        next.
      end.
    
    run DeleteRepositoryItem (document.fullpath, output std-lo, output std-ch).
                                  
    if not std-lo 
     then tMsg = tMsg + (if tMsg > "" then chr(19) else "") + "Unable to remove " + document.name + " .".
     else tGoodCnt = tGoodCnt + 1.
  end.
  
  if tMsg > "" or tGoodCnt <> browse brwDocs:num-selected-rows
   then MESSAGE "Failed to remove " (browse brwDocs:num-selected-rows - tGoodCnt) " files." skip(1) tMsg skip(2) "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
   else refreshDocs().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDeleteFolder C-Win 
PROCEDURE ActionDeleteFolder :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tFileCnt as integer   no-undo.
  define variable tMsg     as character no-undo.
  define variable tGoodCnt as integer   no-undo.
  
  if lookup("delete", pValidActions) = 0
   then return.
  
  if browse brwFolders:num-selected-rows = 0
   then 
    do: bFileDelete:sensitive in frame {&frame-name} = false.
        menu-item m_PopDelete:sensitive in menu docpopmenu = false.
        return.
    end.
  
  {lib/confirm-delete.i "Folder and its contents"}
  
  do tFileCnt = 1 to browse brwFolders:num-selected-rows:
    browse brwFolders:fetch-selected-row(tFileCnt).
    
    if not available folder 
     then next.
    
    if folder.identifier = ""
     then
      do: 
        tMsg = tMsg + (if tMsg > "" then chr(19) else "") + folder.fullpath + " is invalid.".
          next.
      end.
    
    run DeleteRepositoryItem (folder.fullpath, output std-lo, output std-ch).
    
    if not std-lo 
     then tMsg = tMsg + (if tMsg > "" then chr(19) else "") + "Unable to remove " + folder.name + " .".
     else tGoodCnt = tGoodCnt + 1.
  end.
  
  if tMsg > "" or tGoodCnt <> browse brwFolders:num-selected-rows
   then MESSAGE "Failed to remove " (browse brwFolders:num-selected-rows - tGoodCnt) " files." skip(1) tMsg skip(2) "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
   else refreshFolders().
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionEditNotes C-Win 
PROCEDURE ActionEditNotes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tName  as character no-undo.
  define variable tNotes as character no-undo.
  
  if lookup("modify", pValidActions) = 0
   then return.
  
  if browse brwDocs:num-selected-rows = 0
   then return.
  
  if not available document
   then return.
  
  if document.identifier = ""
   then
    do: 
      message document.fullpath + " is invalid." view-as alert-box error.
      return.
    end.
  
  tName  = document.displayName.
  tNotes = document.details.

  run dialogdocumentfileedit.w (input-output tName, input-output tNotes, output std-lo).
  if not std-lo
   then return.

  run ModifyRepositoryItem (document.fullpath, tName, tNotes, output std-lo, output std-ch).
  
  if not std-lo
   then
    do: 
      MESSAGE "Failed to edit " + document.displayName + "." skip std-ch skip "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
      return.
    end.
 
  refreshDocs().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionEmail C-Win 
PROCEDURE ActionEmail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tTo         as character no-undo.
  define variable tSubject    as character no-undo.
  define variable tMsg        as character no-undo.
  define variable tExpiration as integer   no-undo.
  define variable tNotify     as logical   no-undo.
  define variable tDownloads  as integer   no-undo initial 2.
  
  if lookup("email", pValidActions) = 0
   then return.
  
  if browse brwDocs:num-selected-rows = 0
   then return.
  
  if document.identifier = ""
   then
    do: 
      MESSAGE "Document record is corrupted.  Please contact the System Administrator." VIEW-AS ALERT-BOX INFO BUTTONS OK.
      return.
    end.
  
  MAIL-BLOCK:
  repeat:
    /* Prompt for email attributes */
    run dialogdocumentemail.w (input document.displayName,
                               input-output tTo,
                               input-output tSubject,
                               input-output tMsg,
                               input-output tExpiration,
                               input-output tNotify,
                               input-output tDownloads,
                               output std-lo).
    if not std-lo 
     then leave MAIL-BLOCK.
    
    run RequestRepositoryDownload 
        (document.fullpath,
         tTo,
         tSubject,
         tMsg,
         "",
         false,
         true,
         tExpiration,
         tNotify,
         false,
         tDownloads,
         false,
         output std-lo,
         output std-ch).
    
    if std-lo 
     then 
      do: 
        publish "AddContact" (tTo).
        leave MAIL-BLOCK.
      end.
    
    MESSAGE std-ch VIEW-AS ALERT-BOX warning BUTTONS OK.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionFilesDropped C-Win 
PROCEDURE ActionFilesDropped :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pWidget as handle.

 def var tFileCnt as int no-undo.
 def var tMsg as character no-undo.

 if not valid-handle(pWidget) 
  then return.

 if lookup("new", pValidActions) = 0
  then
   do:  pWidget:end-file-drop().
        return.
   end.

 FILE-LOOP:
 do tFileCnt = 1 to pWidget:num-dropped-files:
  std-ch = pWidget:get-dropped-file(tFileCnt).
  std-lo = uploadFile(std-ch, output tMsg).
  if not std-lo 
   then
    do:
        MESSAGE tMsg skip(2) "File uploads interrupted."
         VIEW-AS ALERT-BOX error BUTTONS OK.
        leave FILE-LOOP.
    end.
 end.
 pWidget:end-file-drop().

 refreshDocs().
 if can-find(first document) 
  then apply "value-changed" to brwDocs in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionFoldersDropped C-Win 
PROCEDURE ActionFoldersDropped :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

/*
 def input parameter pWidget as handle.

 def var tFileCnt as int no-undo.
 def var tMsg as character no-undo.

 if not valid-handle(pWidget) 
  then return.

 if lookup("new", pValidActions) = 0
  then
   do:  pWidget:end-file-drop().
        return.
   end.

 FILE-LOOP:
 do tFileCnt = 1 to pWidget:num-dropped-files:
  std-ch = pWidget:get-dropped-file(tFileCnt).
  std-lo = uploadFile(std-ch, output tMsg).
  if not std-lo 
   then
    do:
        MESSAGE tMsg skip(2)
                "File uploads interrupted."
         VIEW-AS ALERT-BOX error BUTTONS OK.
        leave FILE-LOOP.
    end.
 end.
 pWidget:end-file-drop().

 refreshDocs().
 if can-find(first document) 
  then apply "value-changed" to brwDocs in frame {&frame-name}.
*/  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNewFile C-Win 
PROCEDURE ActionNewFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tFile as character no-undo.

 if lookup("new", pValidActions) = 0
  then return.

 system-dialog get-file tFile
  must-exist 
  title "Document to Upload"
  update std-lo.
 if not std-lo 
  then return.
 
 publish "GetConfirmFileUpload" (output std-lo).
 if std-lo 
  then
   do:
       MESSAGE tFile " will be uploaded.  Continue?"
        VIEW-AS ALERT-BOX question BUTTONS YES-NO title "Confirmation" update std-lo.
       if not std-lo 
        then return.
   end.

 std-lo = uploadFile(tFile, output std-ch).
 if not std-lo 
  then
   do:
       MESSAGE std-ch
        VIEW-AS ALERT-BOX warning BUTTONS OK.
       return.
   end.

 refreshDocs().
 if can-find(first document) 
  then apply "value-changed" to brwDocs in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNewFolder C-Win 
PROCEDURE ActionNewFolder :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tFolder as character no-undo.
  
  if lookup("new", pValidActions) = 0
  then return.
  
  run dialogdocumentnewfolder.w (output tFolder, output std-lo).
  if not std-lo
  then return.
  
  if tFolder = "" or tFolder = ? 
  then return.

  run NewRepositoryFolder (pServerPath + "/" + tFolder, output std-lo, output std-ch).
  
  if not std-lo
   then
    do:
      MESSAGE "Failed to create folder '" + tFolder + "'." skip std-ch skip "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
      return.
    end.
 
  refreshFolders().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionOpenFile C-Win 
PROCEDURE ActionOpenFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter pSuccess as logical initial false no-undo.
  
  define variable tLocalPath as character no-undo.
  define variable tMsg       as character no-undo.
  define variable tFile      as character no-undo.
  define variable tFileCnt   as integer   no-undo.
  define variable tGoodCnt   as integer   no-undo.
  
  if lookup("open", pValidActions) = 0
   then return.
  
  if browse brwDocs:num-selected-rows = 0
   then return.
  
  /* das:ActionOpen - handle multiple */
  
  publish "GetTempDir" (output tLocalPath).
  
  do tFileCnt = 1 to browse brwDocs:num-selected-rows:
    browse brwDocs:fetch-selected-row(tFileCnt).
    
    if not available document 
     then next.
    
    if document.identifier = ""
     then
      do: 
        tMsg = addDelimiter(tMsg, chr(19)) + document.fullpath + " is invalid.".
        next.
      end.
    
    tFile = tLocalPath + document.name.
    if search(tFile) <> ? 
     then os-delete value(tFile).
    
    run DownloadRepositoryFile (document.fullpath, tFile, output std-lo, output std-ch).
    
    if not std-lo 
     then
      do: 
        tMsg = addDelimiter(tMsg, chr(19)) + "Unable to download " + document.name + " .".
        next.
      end.
    
    if search(tFile) = ? 
     then
      do: 
        tMsg = addDelimiter(tMsg, chr(19)) + document.name + " failed to download.".
        next.
      end.
    
    publish "AddTempFile" (document.name, tFile).
    run util/openfile.p (tFile).
    tGoodCnt = tGoodCnt + 1.
  end.
  if tMsg > "" or tGoodCnt <> browse brwDocs:num-selected-rows
   then MESSAGE "Failed to download " (browse brwDocs:num-selected-rows - tGoodCnt) " files." skip tMsg skip "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
   else pSuccess = true.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionRenameFolder C-Win 
PROCEDURE ActionRenameFolder :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tFolder as character no-undo.
  
  if lookup("modify", pValidActions) = 0
   then return.

  if browse brwFolders:num-selected-rows = 0
   then return.
  
  if not available folder
   then return.
  
  if folder.identifier = ""
   then
    do: 
      message folder.fullpath + " is invalid." view-as alert-box error.
      return.
    end.
  
  tFolder = folder.displayName.
  run dialogdocumentrenamefolder.w (input-output tFolder, output std-lo).
  
  if not std-lo
   then return.
  
  if tFolder = "" or tFolder = ? 
   then return.

  run ModifyRepositoryItem (folder.fullpath, tFolder, "", output std-lo, output std-ch).
  
  if not std-lo
   then
    do:
      MESSAGE "Failed to rename folder '" + folder.displayName + "'." skip(2) std-ch skip "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
      return.
  end.
 
  refreshFolders().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE brwFolders brwDocs bOK bCancel bRefresh bFileRefresh 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFileType C-Win 
FUNCTION getFileType RETURNS CHARACTER
  ( input pFileName as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  def var tFileExt as character init "".
  def var tFileType as character no-undo.

  if num-entries(pFileName,".") > 1 then
  tFileExt = trim(entry(num-entries(pFileName,"."), pFileName, ".")).

  if tFileEXt <> "" then
  do:
    case tFileExt:
      when "doc" or when "docx" then
      tFileType = "Word".
      when "xls" or when "xlsx" then
      tFileType = "Excel".
      when "ppt" or when "pptx" then
      tFileType = "PowerPoint".
      when "msg" then
      tFileType = "Email".
      otherwise
      tFileType = caps(tFileExt).
    end case.
  end.

  RETURN tFileType.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshDocs C-Win 
FUNCTION refreshDocs RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define buffer document for document.
  
  close query brwDocs.
  empty temp-table document.
  
  run GetRepositoryFolder (input pServerPath + tCurrentFolder, output table document, output std-lo, output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch view-as alert-box warning.
      return false.
    end.
  
  for each document where document.type <> "file":
    delete document.
  end.
  
  for each document:
    document.filetype = getFileType(document.displayName).
  end.
   
  {lib/brw-startSearch-multi.i &browseName="brwDocs" &sortClause="'by displayName'"}
  apply "value-changed" to brwDocs in frame {&frame-name}.
  
    if not can-find(first document)
     then 
      do:
        run server/unlinkdocument.p (pEntityType, pEntityID, pEntitySeq, output std-lo, output std-ch).
        {&window-name}:move-to-top().
      end.
  
  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshFolders C-Win 
FUNCTION refreshFolders RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
/* def buffer folder for folder.  */

  close query brwFolders.
  empty temp-table folder.
  
  run GetRepositoryFolder (pServerPath, output table folder, output std-lo, output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch view-as alert-box warning.
      return false.
    end.
  
  /* Get 'root' folder */
  run GetRepositoryItem (pServerPath, output table root, output std-lo, output std-ch).
  for first root exclusive-lock:
    assign
      root.name        = ""
      root.displayName = ".<" + tRootFolder + ">"
      .
    create folder.
    buffer-copy root to folder.
  end.
   
  for each folder where folder.type <> "folder":
    delete folder.
  end.
  
  open query brwFolders for each folder by folder.displayName.
  apply "value-changed" to brwFolders in frame {&frame-name}.
  apply "entry" to brwFolders in frame {&frame-name}.

RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION uploadFile C-Win 
FUNCTION uploadFile RETURNS LOGICAL PRIVATE
  ( input pFile as character,
    output pMsg as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tFileID as character no-undo.
  
  if not available folder
   then
    do:
      pMsg = "No folder is selected.".
      return false.
    end.
   
  if folder.fullPath = "" or folder.fullPath = ?
   then
    do:
      pMsg = "Folder path is blank or unknown.".
      return false.
    end.
  
  file-info:file-name = pFile.
  if file-info:full-pathname = ?
    or index(file-info:file-type, "F") = 0
    or index(file-info:file-type, "R") = 0
   then 
    do:
      pMsg = pFile + " is not accessible".
      return false.
    end.
  
  {lib/pbshow.i "'Uploading document, please wait...'"}
  {lib/pbupdate.i "'Uploading document...'" 10}
  
  setUserToken("", "", output std-ch).
  run UploadRepositoryFile (pFile, folder.fullPath, output std-lo, output pMsg).
  
  if not std-lo 
   then
    do:
      if pMsg = ""
       then pMsg = "Could not upload file".
      {lib/pbhide.i}
      return false.
    end.
  
  {lib/pbupdate.i "'Uploading document...'" 80}
  
  if pEntityType > ""
   then run server/linkdocument.p (pEntityType, pEntityID, pEntitySeq, output std-lo, output std-ch).
   else std-lo = true.
  {&window-name}:move-to-top().
  
  {lib/pbupdate.i "'Upload complete'" 100 &delay=1}
  {lib/pbhide.i}
  
  RETURN std-lo.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

