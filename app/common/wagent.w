&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI ADM2
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWin
{adecomm/appserv.i}
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/* wagentview.w
@desc Window to view a file
@author V.Jain
@Modified
Shefali  12/16/2020 Task-86695-Agent Manager Type Implementation.
SA       08/13/2021 Task 83510 modified to add points and score
SA       09/24/2021 Task#:86696 Defects raised by QA
SA       03/30/2022 Task #86699 Modified to update linking of agent and org.
SA       06/10/2022 Task #93485 Added new fields "Region" and "Domain".
VR       07/28/2022 Modified to support clob field changes.
SA       08/16/2022 Task #96812 Modified to add functionality for personagent.
VR       04/04/2023 Modified to add Agent Journals related chagnes
SB       06/26/2023 Task #104008 Modified to implement tags for person
Sagar K  07/04/2023 Task #103751 added modify person button for  personagent.
AG       08/16/2023 Task #106690 Enable the save button if "Same as above" 
                    button is clicked.
SK       09/29/2023 Task #105743 changes tooltip of deactivateperson button 
SB       02/16/2024 Task #110389 Modified to fix the modification of person associated with agent. 
SK       02/29/2024 Tag related changes.
SB       04/02/2024 Task #111689 Modified to use new dialog to create a new person or link existing person to an agent
SRK      04/04/2024 Modified related to update person associated with agent
VR       07/03/2024 Task # 114127 Default Agent Legal Name as a Organization Name while 
                    linking an agent with New Organization
*/

CREATE WIDGET-POOL.

define input parameter hFileDataSrv as handle    no-undo.
define input parameter pAction      as character no-undo.
                      
{src/adm2/widgetprto.i}

{lib/std-def.i}
{lib/amd-def.i} 
{lib/open-window.i} /* Contain function: openWindowForAgent */
{lib/winlaunch.i}
{lib/brw-multi-def.i}
{lib/brw-totalData-def.i}
{lib/set-button-def.i &noEnable=true}
{tt/imageinfo.i}
{tt/region.i}

define variable hAlertDataSrv as handle     no-undo.
define variable ipcAgentID    as character  no-undo.
define variable ipcOrgID      as character  no-undo.
define variable cPersonID     as character  no-undo.
define variable ipcCategory   as integer    no-undo.

define variable tWinHandle       as handle.
define variable activePageNumber as integer no-undo.
define variable dataChanged      as logical no-undo init no.
define variable newRecord        as logical no-undo init no.

define variable lAgentDetail as logical init no no-undo.
define variable lBatches     as logical init no no-undo.
define variable lAr          as logical init no no-undo.
define variable lClaim       as logical init no no-undo.
define variable lAudit       as logical init no no-undo.
define variable lCompliance  as logical init no no-undo.
define variable lpersonagent as logical init no no-undo.
define variable lObjective   as logical init no no-undo.
define variable lEvent       as logical init no no-undo.
define variable lAlert       as logical init no no-undo.
define variable lActivity    as logical init no no-undo.

define variable iTimeFrame as integer no-undo.
define variable dstartDate as date no-undo.
define variable dEndDate   as date no-undo.

define variable outimgloc as character no-undo.
{tt/person.i}
{tt/person.i &tableAlias="tPerson"}
{tt/person.i &tableAlias="ttPerson"}

{tt/agentcontact.i}
{tt/agentcontact.i &tableAlias="tAgentContact"}

{tt/tag.i}
{tt/tag.i &tableAlias=temptag}   

{tt/claimcostssummary.i}

{tt/agent.i}                     
{tt/agentmanager.i}
{tt/agentmanager.i &tableAlias=tagentmanager}  
{tt/batch.i}
{tt/batch.i &tableAlias=tBatch} 
{tt/batch.i &tableAlias=ttBatch}
{tt/reqfulfill.i}

{tt/affiliation.i}
{tt/Affiliation.i &tableAlias="tAffiliation"}
{tt/personagent.i}
{tt/personagent.i &tableAlias="tPersonAgent"}
{tt/personagent.i &tableAlias="ttPersonAgent"}
{tt/alert.i}
{tt/alert.i &tableAlias="ttAlert"}
{tt/alert.i &tableAlias="tempAlert"}
{tt/event.i}
{tt/event.i &tableAlias="tlEvent"}  /* for timeline events */
{tt/event.i &tableAlias="tempEvent"}
{tt/objective.i}
{tt/objective.i &tableAlias="tempObjective"}
{tt/sentiment.i}
{tt/sentiment.i &tableAlias="tempSentiment"}
{tt/organization.i}
{tt/organization.i &tableAlias=torganization}
{tt/role.i}

{tt/qaraudit.i}
{tt/action.i}
{tt/finding.i}
{tt/action.i &tableAlias="tempaction"}
{tt/finding.i &tableAlias="tempfinding"}
{tt/reportclaimsummary.i &tableAlias="agentClaim"}
{tt/araging.i}
{tt/araging.i &tableAlias="ttArAging"}
{tt/araging.i &tableAlias="tArAging"}
{tt/qaraudit.i &tableAlias="ttaudit"}

/* Temp-table definition*/
{tt/comLog.i}
{tt/comlog.i &tableAlias="ttcomlog"}

{tt/staterequirement.i}
{tt/staterequirement.i &tableAlias="tStateRequirement"}

{tt/statereqqual.i}

{tt/qualification.i}
{tt/qualification.i &tableAlias="tqualification"}

{tt/activity.i &tableAlias=ttactivity}
{tt/activity.i &tableAlias=ttactivityMetrics}
{tt/state.i}

{tt/consolidate.i &tableAlias=tConsolidate}

define temp-table activity like ttactivity
  field showLevel as integer   {&nodeType}
  field showID    as character {&nodeType} column-label "Agent"      format "x(500)"
  field showName  as character {&nodeType} column-label "Agent Name" format "x(500)"
  field seq       as integer   {&nodeType}
  .
define temp-table tempActivity like activity.
define temp-table tActivity like activity.
define temp-table tCActivity like activity.  
define temp-table tMetricsActivity like activity.
define temp-table activityMetrics like activity.
  
define temp-table openQars
 field qarid     as integer
 field hInstance as handle.

define variable chimage      as longchar  no-undo.  
define variable chPath       as character no-undo.
define variable lImage       as longchar  no-undo.
define variable IMAGE-2      as handle    no-undo.
define variable procname     as character no-undo.   
define variable decdmptr     as memptr    no-undo. 
define variable lLogoChange  as logical   no-undo. 

define variable cMethod      as character no-undo.
define variable iDueInDays   as integer   no-undo.
define variable iDueOnDay    as integer   no-undo.
define variable cCashAcc     as character no-undo.
define variable cCashAccDesc as character no-undo.
define variable cARAcc       as character no-undo.
define variable cARAccDesc   as character no-undo.
define variable cRefARAcc       as character no-undo.
define variable cRefARAccDesc   as character no-undo.
define variable cWroffARAcc     as character no-undo.
define variable cWroffARAccDesc as character no-undo.
define variable cAgentStatus as character no-undo.
define variable iTagID       as integer   no-undo.
define variable iEventID     as integer   no-undo.
define variable iObjectiveID as integer   no-undo.

define variable hManager     as handle    no-undo.
define variable cManagerUID  as character no-undo.
define variable lModifyOrganization     as logical   no-undo.
define variable lRefreshEvents          as logical   no-undo.
define variable lRefreshtpersonagents    as logical   no-undo.
define variable lRefreshClaims          as logical   no-undo.
define variable lRefreshReqFulFillment  as logical   no-undo.
define variable lRefreshQualifications  as logical   no-undo.
define variable iCount         as integer no-undo.
define variable cCurrUser      as character no-undo.
define variable cCurrentUID    as character no-undo.
define variable iOldOrgID      as integer   no-undo.

define variable cList             as character no-undo.
define variable cAppCode          as character no-undo.
define variable cLastAlertCode    as character no-undo.
define variable cLastAlertOwner   as character no-undo.
define variable cLastRemitClicked as character no-undo.

define variable cOrgID          as character no-undo.
define variable cOrgName        as character no-undo.

define variable cFormat         as character no-undo.
define variable cFormat1        as character no-undo.
define variable cFormat2        as character no-undo.

define variable iMonth          as integer   no-undo.
define variable iPlanned        as integer   no-undo.
define variable iActual         as integer   no-undo.
define variable iComp           as integer   no-undo.
define variable hColumn         as handle    no-undo.

define variable iCreateComparision    as integer   no-undo.
define variable cArAgentEmail         as character no-undo.
define variable cLastAgingClicked     as character no-undo.
define variable lRefreshClaimsSummary as logical   no-undo. 
define variable lRefresh              as logical   no-undo.
define variable lArAging              as logical   no-undo.
define variable lBatch                as logical   no-undo.
define variable iLastPage             as integer   no-undo.
define variable cOrigOrg              as character no-undo.

define variable cFile                 as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

&Scoped-define ADM-SUPPORTED-LINKS Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwAction

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES action tActivity tMetricsActivity alert ~
tArAging batch agentclaim tlEvent tpersonagent audit qualification ~
reqfulfill tag

/* Definitions for BROWSE brwAction                                     */
&Scoped-define FIELDS-IN-QUERY-brwAction action.dueDate action.ownerDesc action.comments   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAction   
&Scoped-define SELF-NAME brwAction
&Scoped-define QUERY-STRING-brwAction for each action
&Scoped-define OPEN-QUERY-brwAction open query {&SELF-NAME} for each action.
&Scoped-define TABLES-IN-QUERY-brwAction action
&Scoped-define FIRST-TABLE-IN-QUERY-brwAction action


/* Definitions for BROWSE brwActivityBatch                              */
&Scoped-define FIELDS-IN-QUERY-brwActivityBatch tActivity.typeDesc tActivity.month1 tActivity.month2 tActivity.month3 tActivity.month4 tActivity.month5 tActivity.month6 tActivity.month7 tActivity.month8 tActivity.month9 tActivity.month10 tActivity.month11 tActivity.month12 tActivity.total tActivity.yrTotal   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwActivityBatch   
&Scoped-define SELF-NAME brwActivityBatch
&Scoped-define QUERY-STRING-brwActivityBatch FOR each tActivity
&Scoped-define OPEN-QUERY-brwActivityBatch OPEN QUERY {&SELF-NAME} FOR each tActivity.
&Scoped-define TABLES-IN-QUERY-brwActivityBatch tActivity
&Scoped-define FIRST-TABLE-IN-QUERY-brwActivityBatch tActivity


/* Definitions for BROWSE brwActivityCPL                                */
&Scoped-define FIELDS-IN-QUERY-brwActivityCPL tMetricsActivity.year tMetricsActivity.typeDesc tMetricsActivity.month1 tMetricsActivity.month2 tMetricsActivity.month3 tMetricsActivity.month4 tMetricsActivity.month5 tMetricsActivity.month6 tMetricsActivity.month7 tMetricsActivity.month8 tMetricsActivity.month9 tMetricsActivity.month10 tMetricsActivity.month11 tMetricsActivity.month12 tMetricsActivity.Total tMetricsActivity.yrTotal   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwActivityCPL   
&Scoped-define SELF-NAME brwActivityCPL
&Scoped-define QUERY-STRING-brwActivityCPL FOR EACH tMetricsActivity
&Scoped-define OPEN-QUERY-brwActivityCPL OPEN QUERY {&SELF-NAME} FOR EACH tMetricsActivity.
&Scoped-define TABLES-IN-QUERY-brwActivityCPL tMetricsActivity
&Scoped-define FIRST-TABLE-IN-QUERY-brwActivityCPL tMetricsActivity


/* Definitions for BROWSE brwActivityPolicies                           */
&Scoped-define FIELDS-IN-QUERY-brwActivityPolicies tMetricsActivity.year tMetricsActivity.typeDesc tMetricsActivity.month1 tMetricsActivity.month2 tMetricsActivity.month3 tMetricsActivity.month4 tMetricsActivity.month5 tMetricsActivity.month6 tMetricsActivity.month7 tMetricsActivity.month8 tMetricsActivity.month9 tMetricsActivity.month10 tMetricsActivity.month11 tMetricsActivity.month12 tMetricsActivity.Total tMetricsActivity.yrTotal   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwActivityPolicies   
&Scoped-define SELF-NAME brwActivityPolicies
&Scoped-define QUERY-STRING-brwActivityPolicies FOR EACH tMetricsActivity
&Scoped-define OPEN-QUERY-brwActivityPolicies OPEN QUERY {&SELF-NAME} FOR EACH tMetricsActivity.
&Scoped-define TABLES-IN-QUERY-brwActivityPolicies tMetricsActivity
&Scoped-define FIRST-TABLE-IN-QUERY-brwActivityPolicies tMetricsActivity


/* Definitions for BROWSE brwAlerts                                     */
&Scoped-define FIELDS-IN-QUERY-brwAlerts alert.dateCreated "Created" alert.processCodeDesc "Alert Type" alert.thresholdRange "Threshold" alert.scoreDesc alert.severityDesc "Severity" alert.ownerDesc alert.description "Description"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAlerts   
&Scoped-define SELF-NAME brwAlerts
&Scoped-define QUERY-STRING-brwAlerts for each alert
&Scoped-define OPEN-QUERY-brwAlerts open query {&SELF-NAME} for each alert.
&Scoped-define TABLES-IN-QUERY-brwAlerts alert
&Scoped-define FIRST-TABLE-IN-QUERY-brwAlerts alert


/* Definitions for BROWSE brwArAging                                    */
&Scoped-define FIELDS-IN-QUERY-brwArAging tArAging.fileNumber tArAging.due0_30 tArAging.due31_60 tArAging.due61_90 tArAging.due91_ tArAging.balance   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwArAging   
&Scoped-define SELF-NAME brwArAging
&Scoped-define QUERY-STRING-brwArAging for each tArAging
&Scoped-define OPEN-QUERY-brwArAging open query {&SELF-NAME} for each tArAging.
&Scoped-define TABLES-IN-QUERY-brwArAging tArAging
&Scoped-define FIRST-TABLE-IN-QUERY-brwArAging tArAging


/* Definitions for BROWSE brwBatch                                      */
&Scoped-define FIELDS-IN-QUERY-brwBatch batch.receivedDate batch.invoiceDate batch.batchID "Batch ID" batch.reference batch.fileCount batch.policyCount batch.endorsementCount batch.netPremiumReported batch.netPremiumDelta getBatchStatus(batch.stat) @ batch.stat   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwBatch   
&Scoped-define SELF-NAME brwBatch
&Scoped-define QUERY-STRING-brwBatch FOR EACH batch
&Scoped-define OPEN-QUERY-brwBatch OPEN QUERY {&SELF-NAME} FOR EACH batch.
&Scoped-define TABLES-IN-QUERY-brwBatch batch
&Scoped-define FIRST-TABLE-IN-QUERY-brwBatch batch


/* Definitions for BROWSE brwClaims                                     */
&Scoped-define FIELDS-IN-QUERY-brwClaims agentclaim.claimID agentclaim.statDesc agentclaim.agentErrorDesc agentclaim.assignedToDesc agentclaim.dateOpened agentclaim.laeReserve agentclaim.laeLTD agentclaim.lossReserve agentclaim.lossLTD agentclaim.recoveries agentclaim.costsPaid   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwClaims   
&Scoped-define SELF-NAME brwClaims
&Scoped-define QUERY-STRING-brwClaims FOR EACH agentclaim
&Scoped-define OPEN-QUERY-brwClaims OPEN QUERY {&SELF-NAME} FOR EACH agentclaim.
&Scoped-define TABLES-IN-QUERY-brwClaims agentclaim
&Scoped-define FIRST-TABLE-IN-QUERY-brwClaims agentclaim


/* Definitions for BROWSE brwEvent                                      */
&Scoped-define FIELDS-IN-QUERY-brwEvent tlevent.date tlevent.name tlevent.description tlevent.days   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwEvent   
&Scoped-define SELF-NAME brwEvent
&Scoped-define QUERY-STRING-brwEvent for each tlEvent by tlEvent.date
&Scoped-define OPEN-QUERY-brwEvent open query {&SELF-NAME} for each tlEvent by tlEvent.date.
&Scoped-define TABLES-IN-QUERY-brwEvent tlEvent
&Scoped-define FIRST-TABLE-IN-QUERY-brwEvent tlEvent


/* Definitions for BROWSE brwpersonagent                                */
&Scoped-define FIELDS-IN-QUERY-brwpersonagent tpersonagent.active tpersonagent.personName tpersonagent.jobTitle tpersonagent.phone tpersonagent.email tpersonagent.notes tpersonagent.doNotCall tpersonagent.isCtrlPerson tpersonagent.linkedToCompliance tpersonagent.personID   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwpersonagent   
&Scoped-define SELF-NAME brwpersonagent
&Scoped-define QUERY-STRING-brwpersonagent for each tpersonagent by tpersonagent.active desc by tpersonagent.personName
&Scoped-define OPEN-QUERY-brwpersonagent open query {&SELF-NAME} for each tpersonagent by tpersonagent.active desc by tpersonagent.personName.
&Scoped-define TABLES-IN-QUERY-brwpersonagent tpersonagent
&Scoped-define FIRST-TABLE-IN-QUERY-brwpersonagent tpersonagent


/* Definitions for BROWSE brwQAR                                        */
&Scoped-define FIELDS-IN-QUERY-brwQAR audit.stat audit.audittype audit.errtype audit.auditStartDate audit.auditFinishDate audit.score audit.grade audit.auditor audit.comments   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQAR   
&Scoped-define SELF-NAME brwQAR
&Scoped-define QUERY-STRING-brwQAR for each audit no-lock by audit.auditStartDate desc
&Scoped-define OPEN-QUERY-brwQAR open query {&SELF-NAME} for each audit no-lock by audit.auditStartDate desc .
&Scoped-define TABLES-IN-QUERY-brwQAR audit
&Scoped-define FIRST-TABLE-IN-QUERY-brwQAR audit


/* Definitions for BROWSE brwQualification                              */
&Scoped-define FIELDS-IN-QUERY-brwQualification qualification.activ qualification.stateID qualification.Qualification qualification.QualificationNumber qualification.stat qualification.effectiveDate qualification.expirationDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQualification   
&Scoped-define SELF-NAME brwQualification
&Scoped-define QUERY-STRING-brwQualification for each qualification  by Qualification.qualificationId
&Scoped-define OPEN-QUERY-brwQualification open query brwQualification for each qualification  by Qualification.qualificationId.
&Scoped-define TABLES-IN-QUERY-brwQualification qualification
&Scoped-define FIRST-TABLE-IN-QUERY-brwQualification qualification


/* Definitions for BROWSE brwReqFul                                     */
&Scoped-define FIELDS-IN-QUERY-brwReqFul reqfulfill.reqMet reqfulfill.reqdesc "Requirement" reqfulfill.qualification "Qualification" reqfulfill.stat "Status" reqfulfill.appliesTo "Fulfilled By" reqfulfill.entityname "Name"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwReqFul   
&Scoped-define SELF-NAME brwReqFul
&Scoped-define QUERY-STRING-brwReqFul for each reqfulfill
&Scoped-define OPEN-QUERY-brwReqFul open query {&SELF-NAME} for each reqfulfill.
&Scoped-define TABLES-IN-QUERY-brwReqFul reqfulfill
&Scoped-define FIRST-TABLE-IN-QUERY-brwReqFul reqfulfill


/* Definitions for BROWSE brwTags                                       */
&Scoped-define FIELDS-IN-QUERY-brwTags tag.name   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwTags   
&Scoped-define SELF-NAME brwTags
&Scoped-define QUERY-STRING-brwTags FOR EACH tag
&Scoped-define OPEN-QUERY-brwTags OPEN QUERY {&SELF-NAME} FOR EACH tag.
&Scoped-define TABLES-IN-QUERY-brwTags tag
&Scoped-define FIRST-TABLE-IN-QUERY-brwTags tag


/* Definitions for FRAME frActivity                                     */

/* Definitions for FRAME frAlert                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-frAlert ~
    ~{&OPEN-QUERY-brwAlerts}

/* Definitions for FRAME frAr                                           */
&Scoped-define OPEN-BROWSERS-IN-QUERY-frAr ~
    ~{&OPEN-QUERY-brwArAging}

/* Definitions for FRAME frAudit                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-frAudit ~
    ~{&OPEN-QUERY-brwAction}~
    ~{&OPEN-QUERY-brwQAR}

/* Definitions for FRAME frBatch                                        */

/* Definitions for FRAME frClaim                                        */

/* Definitions for FRAME frCompliance                                   */
&Scoped-define OPEN-BROWSERS-IN-QUERY-frCompliance ~
    ~{&OPEN-QUERY-brwQualification}~
    ~{&OPEN-QUERY-brwReqFul}

/* Definitions for FRAME frDetail                                       */

/* Definitions for FRAME frEvent                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-frEvent ~
    ~{&OPEN-QUERY-brwEvent}

/* Definitions for FRAME frpersonagent                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-frpersonagent ~
    ~{&OPEN-QUERY-brwpersonagent}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bAgentJournals bConsolidate RECT-68 ~
hdAgentID hdAgentName fStatus hdLAgentName bPDF bRefresh bStatusChange ~
bARInfo bFollow bNotes 
&Scoped-Define DISPLAYED-OBJECTS hdAgentID hdAgentName fStatus hdLAgentName 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD adjustFormattedValue wWin 
FUNCTION adjustFormattedValue RETURNS LOGICAL
  ( input ipcAmount as character,
    input haWidget  as handle)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD agentStat wWin 
FUNCTION agentStat RETURNS CHARACTER
  ( input cStat as character /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD checkActivity wWin 
FUNCTION checkActivity RETURNS CHARACTER
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD checkActivityMetrics wWin 
FUNCTION checkActivityMetrics RETURNS CHARACTER
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD checkDataChanged wWin 
FUNCTION checkDataChanged RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD checkOpenPeriod wWin 
FUNCTION checkOpenPeriod RETURNS LOGICAL
  ( input ipiMonth as integer,
    input ipiYear as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createActivity wWin 
FUNCTION createActivity RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createActivityMetrics wWin 
FUNCTION createActivityMetrics RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createActivityRow wWin 
FUNCTION createActivityRow RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer,
    input table for tempActivity )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createActivityRowMetrics wWin 
FUNCTION createActivityRowMetrics RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer,
    input table for tempActivity )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD discardPage wWin 
FUNCTION discardPage RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayAlert wWin 
FUNCTION displayAlert RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayAR wWin 
FUNCTION displayAR RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayAudit wWin 
FUNCTION displayAudit RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayClaim wWin 
FUNCTION displayClaim RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayCompliance wWin 
FUNCTION displayCompliance RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayDetail wWin 
FUNCTION displayDetail RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayEvent wWin 
FUNCTION displayEvent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayHeader wWin 
FUNCTION displayHeader RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayObjective wWin 
FUNCTION displayObjective RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doModify wWin 
FUNCTION doModify RETURNS LOGICAL
  ( input pModify as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getActivityComparedData wWin 
FUNCTION getActivityComparedData RETURNS LOGICAL
  (input ipiYear as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getActivityComparedDataMetrics wWin 
FUNCTION getActivityComparedDataMetrics RETURNS LOGICAL
  (input ipiYear as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getActivityTotal wWin 
FUNCTION getActivityTotal RETURNS DECIMAL
  ( input table for tempactivity,
    input pType as character  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getActivityType wWin 
FUNCTION getActivityType RETURNS CHARACTER
  ( input ipcType as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getAuditStatus wWin 
FUNCTION getAuditStatus RETURNS CHARACTER
  ( input ipcStatus as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getAuditType wWin 
FUNCTION getAuditType RETURNS CHARACTER
  ( input ipcAuditType as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getauth wWin 
FUNCTION getauth returns character ( cAuth as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getBatchStatus wWin 
FUNCTION getBatchStatus RETURNS CHARACTER
  ( input ipcStatus as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getCategoryDesc wWin 
FUNCTION getCategoryDesc RETURNS CHARACTER
  ( input ipcCategory as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getComStat wWin 
FUNCTION getComStat returns character
 ( input cStat as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getcomstatus wWin 
FUNCTION getcomstatus RETURNS CHARACTER
  ( input cStat as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getEntity wWin 
FUNCTION getEntity RETURNS CHARACTER
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getEntityID wWin 
FUNCTION getEntityID RETURNS CHARACTER
  ( cPartnerBType as character,
    cPartnerA     as character,
    cPartnerB     as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getEntityName wWin 
FUNCTION getEntityName RETURNS CHARACTER
  ( cPartnerB     as character,
    cPartnerBType as character,
    cPartnerAName as character,
    cPartnerBName as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getErrorType wWin 
FUNCTION getErrorType RETURNS CHARACTER
  ( input ipcErrType as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getMonthName wWin 
FUNCTION getMonthName RETURNS CHARACTER
  (INPUT iMonth AS INTEGER)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getMonthWiseYearTrend wWin 
FUNCTION getMonthWiseYearTrend RETURNS LOGICAL
  ( input ipiPeriodID as integer,
    input ipiYear     as integer,
    input ipiYearSeq  as integer,
    input ipdeAmount  as decimal)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getNature wWin 
FUNCTION getNature RETURNS CHARACTER
  ( cPartnerB     as character,
    cPartnerBType as character,
    cNatureAtoB   as character,
    cNatureBtoA   as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getOrgName wWin 
FUNCTION getOrgName RETURNS CHARACTER
  ( ipcOrgID as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getPeriodID wWin 
FUNCTION getPeriodID RETURNS INTEGER
  ( input ipiMonth as integer,
    input ipiYear as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getQualificationDesc wWin 
FUNCTION getQualificationDesc RETURNS CHARACTER
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getQualificationStatus wWin 
FUNCTION getQualificationStatus RETURNS CHARACTER
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getTranType wWin 
FUNCTION getTranType RETURNS CHARACTER
  (input ipcTranType as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openActivity wWin 
FUNCTION openActivity RETURNS LOGICAL
  ( input ipiYear         as integer,
    input ipcCategoryList as character,
    input iplRefresh      as logical)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openActivityMetrics wWin 
FUNCTION openActivityMetrics RETURNS LOGICAL
  ( input ipiYear         as integer,
    input ipcCategoryList as character,
    input iplRefresh      as logical)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openAlerts wWin 
FUNCTION openAlerts RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openAr wWin 
FUNCTION openAr RETURNS LOGICAL
  (input iplRefresh as logical)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openAudit wWin 
FUNCTION openAudit RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openBatchesYearly wWin 
FUNCTION openBatchesYearly RETURNS LOGICAL
  (input ipcYear as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openclaim wWin 
FUNCTION openclaim RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openEvents wWin 
FUNCTION openEvents RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openObjectives wWin 
FUNCTION openObjectives RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openPersonAgents wWin 
FUNCTION openPersonAgents RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openQualification wWin 
FUNCTION openQualification RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openReqFulFillment wWin 
FUNCTION openReqFulFillment RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openTags wWin 
FUNCTION openTags RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openURL wWin 
FUNCTION openURL RETURNS LOGICAL PRIVATE
  ( input pURL as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged wWin 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveDetail wWin 
FUNCTION saveDetail RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD savePage wWin 
FUNCTION savePage RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveStateDisable wWin 
FUNCTION saveStateDisable RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveStateEnable wWin 
FUNCTION saveStateEnable RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setHeaderState wWin 
FUNCTION setHeaderState RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setLockState wWin 
FUNCTION setLockState RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setPageState wWin 
FUNCTION setPageState RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int, input pState as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setZeroForClosedPeriod wWin 
FUNCTION setZeroForClosedPeriod RETURNS LOGICAL
  ( input ipiYear     as integer,
    input ipiYearSeq  as integer)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_Activity 
       MENU-ITEM m_Notes        LABEL "Notes"          ACCELERATOR "ALT-S"
       MENU-ITEM m_AR_Information LABEL "A/R Information" ACCELERATOR "ALT-A"
       MENU-ITEM m_Following    LABEL "Following"      ACCELERATOR "ALT-F".

DEFINE SUB-MENU m_Reports 
       MENU-ITEM m_summary      LABEL "Summary"        ACCELERATOR "ALT-M"
       RULE
       MENU-ITEM m_remittances  LABEL "Remittances"    ACCELERATOR "ALT-C"
       MENU-ITEM m_claims       LABEL "Claims"         ACCELERATOR "ALT-T"
       MENU-ITEM m_accounting   LABEL "Accounting"    
       RULE
       MENU-ITEM m_Historical_Objectives LABEL "Historical Objectives" ACCELERATOR "ALT-O"
       RULE
       MENU-ITEM m_Compliance_Log LABEL "Compliance Logs".

DEFINE MENU MENU-BAR-wWin MENUBAR
       SUB-MENU  m_Activity     LABEL "Activity"      
       SUB-MENU  m_Reports      LABEL "Reports"       .

DEFINE MENU POPUP-MENU-brwQAR 
       MENU-ITEM m_Mail_auditor LABEL "Mail auditor"  .

DEFINE MENU POPUP-MENU-brwQAR-2 
       MENU-ITEM m_Mail_Action_Owner LABEL "Mail Action Owner".


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_folder AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentJournals  NO-FOCUS
     LABEL "Agent Journals" 
     SIZE 7.2 BY 1.71 TOOLTIP "View the Agent Journals".

DEFINE BUTTON bARInfo AUTO-GO  NO-FOCUS
     LABEL "AR Info" 
     SIZE 7.2 BY 1.71 TOOLTIP "Accounts Receivables Information".

DEFINE BUTTON bConsolidate  NO-FOCUS
     LABEL "Consolidate" 
     SIZE 7.2 BY 1.71 TOOLTIP "Consolidate".

DEFINE BUTTON bFollow  NO-FOCUS
     LABEL "Following" 
     SIZE 7.2 BY 1.71 TOOLTIP "Start Following".

DEFINE BUTTON bNotes  NO-FOCUS
     LABEL "Notes" 
     SIZE 7.2 BY 1.71 TOOLTIP "View the notes of the selected agent".

DEFINE BUTTON bPDF  NO-FOCUS
     LABEL "PDF" 
     SIZE 7.2 BY 1.71 TOOLTIP "Generate PDF".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh All".

DEFINE BUTTON bStatusChange AUTO-GO  NO-FOCUS
     LABEL "Status Change" 
     SIZE 4.6 BY 1.14 TOOLTIP "Change Status".

DEFINE VARIABLE fStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE hdAgentID AS CHARACTER FORMAT "X(10)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE hdAgentName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 54 BY 1 NO-UNDO.

DEFINE VARIABLE hdLAgentName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Legal Name" 
     VIEW-AS FILL-IN 
     SIZE 54 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-68
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 52.2 BY 2.57.

DEFINE BUTTON bActivityExp  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bActivityExpCPL  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bActivityMetricsRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh data".

DEFINE BUTTON bActivityMetricsRefreshCPL  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh data".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 4.6 BY 1.14 TOOLTIP "Get Alerts".

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 4.6 BY 1.14 TOOLTIP "Modify the agent alert".

DEFINE BUTTON bNewNote  NO-FOCUS
     LABEL "Note" 
     SIZE 4.6 BY 1.14 TOOLTIP "Create a new note for the agent alert".

DEFINE VARIABLE fCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Alert Type" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 44 BY 1 NO-UNDO.

DEFINE VARIABLE fOwner AS CHARACTER FORMAT "X(256)":U 
     LABEL "Owner" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 44 BY 1 NO-UNDO.

DEFINE BUTTON b30to60  NO-FOCUS
     LABEL "" 
     SIZE 23.6 BY 1.14.

DEFINE BUTTON b60to90  NO-FOCUS
     LABEL "" 
     SIZE 23.6 BY 1.14.

DEFINE BUTTON bbalance  NO-FOCUS
     LABEL "" 
     SIZE 23.6 BY 1.14.

DEFINE BUTTON bCurrentaging  NO-FOCUS
     LABEL "" 
     SIZE 23.6 BY 1.14.

DEFINE BUTTON bover90  NO-FOCUS
     LABEL "" 
     SIZE 23.6 BY 1.14.

DEFINE BUTTON btArMail AUTO-GO  NO-FOCUS
     LABEL "Mail" 
     SIZE 4.6 BY 1.14 TOOLTIP "Mail".

DEFINE BUTTON btFileExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export".

DEFINE BUTTON btFileOpen  NO-FOCUS
     LABEL "Open" 
     SIZE 4.6 BY 1.14 TOOLTIP "View Transaction Details".

DEFINE BUTTON btRefreshAr  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh data".

DEFINE BUTTON btActionExport AUTO-GO  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export".

DEFINE BUTTON btActionMail AUTO-GO  NO-FOCUS
     LABEL "Mail" 
     SIZE 4.6 BY 1.14 TOOLTIP "Mail".

DEFINE BUTTON btActionOpen AUTO-GO  NO-FOCUS
     LABEL "Open" 
     SIZE 4.6 BY 1.14 TOOLTIP "Open".

DEFINE BUTTON btActionRefresh AUTO-GO  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh".

DEFINE BUTTON btAuditExport AUTO-GO  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export".

DEFINE BUTTON btAuditMail AUTO-GO  NO-FOCUS
     LABEL "Mail" 
     SIZE 4.6 BY 1.14 TOOLTIP "Mail".

DEFINE BUTTON btAuditOpen AUTO-GO  NO-FOCUS
     LABEL "Open" 
     SIZE 4.6 BY 1.14 TOOLTIP "Open".

DEFINE BUTTON btAuditRefresh AUTO-GO  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh".

DEFINE BUTTON bActivityBatchExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bActivityBatchRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh data".

DEFINE BUTTON bBatchExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bBatchOpen  NO-FOCUS
     LABEL "Open" 
     SIZE 4.6 BY 1.14 TOOLTIP "Open batch detail".

DEFINE BUTTON bBatchRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh data".

DEFINE BUTTON bYear1  NO-FOCUS
     LABEL "Year1" 
     SIZE 9.6 BY 1 TOOLTIP "Refresh batch details for the year".

DEFINE BUTTON bYear2  NO-FOCUS
     LABEL "Year2" 
     SIZE 9.6 BY 1 TOOLTIP "Refresh batch details for the year".

DEFINE BUTTON bYear3  NO-FOCUS
     LABEL "Year3" 
     SIZE 9.6 BY 1 TOOLTIP "Refresh batch details for the year".

DEFINE BUTTON btClaimExport AUTO-GO  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export".

DEFINE BUTTON btClaimMail AUTO-GO  NO-FOCUS
     LABEL "Mail" 
     SIZE 4.6 BY 1.14 TOOLTIP "Mail".

DEFINE BUTTON btClaimOpen AUTO-GO  NO-FOCUS
     LABEL "Open" 
     SIZE 4.6 BY 1.14 TOOLTIP "Open".

DEFINE BUTTON btClaimRefresh AUTO-GO  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh".

DEFINE VARIABLE costfrom12 AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 17.4 BY 1 NO-UNDO.

DEFINE VARIABLE costfrom24 AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 17.4 BY 1 NO-UNDO.

DEFINE VARIABLE costfrom36 AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 17.4 BY 1 NO-UNDO.

DEFINE VARIABLE costfromLTD AS INTEGER FORMAT "$-z,zzz,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE costnet12 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 7.4 BY .62 NO-UNDO.

DEFINE VARIABLE costnet24 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 7.4 BY .62 NO-UNDO.

DEFINE VARIABLE costnet36 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 7.4 BY .62 NO-UNDO.

DEFINE VARIABLE costnetLTD AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 7.4 BY .62 NO-UNDO.

DEFINE VARIABLE errorfrom12 AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 17.4 BY 1 NO-UNDO.

DEFINE VARIABLE errorfrom24 AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 17.4 BY 1 NO-UNDO.

DEFINE VARIABLE errorfromLTD AS INTEGER FORMAT "$-z,zzz,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE errornet12 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 7.4 BY .62 NO-UNDO.

DEFINE VARIABLE errornet24 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 7.8 BY .62 NO-UNDO.

DEFINE VARIABLE errornet36 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 7.4 BY .62 NO-UNDO.

DEFINE VARIABLE errornetLTD AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 7.4 BY .62 NO-UNDO.

DEFINE VARIABLE errortfrom36 AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 17.4 BY 1 NO-UNDO.

DEFINE VARIABLE fLAEL AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16.4 BY 1 NO-UNDO.

DEFINE VARIABLE fLAER AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16.4 BY 1 NO-UNDO.

DEFINE VARIABLE fLossL AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16.4 BY 1 NO-UNDO.

DEFINE VARIABLE fLossR AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16.4 BY 1 NO-UNDO.

DEFINE VARIABLE fRecoveriesR AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16.4 BY 1 NO-UNDO.

DEFINE VARIABLE from12 AS CHARACTER FORMAT "X(256)":U INITIAL "From 04/2019" 
      VIEW-AS TEXT 
     SIZE 14 BY .62 NO-UNDO.

DEFINE VARIABLE from24 AS CHARACTER FORMAT "X(256)":U INITIAL "From 04/2018" 
      VIEW-AS TEXT 
     SIZE 14 BY .62 NO-UNDO.

DEFINE VARIABLE from36 AS CHARACTER FORMAT "X(256)":U INITIAL "From 04/2017" 
      VIEW-AS TEXT 
     SIZE 14 BY .62 NO-UNDO.

DEFINE VARIABLE fTotal AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 21.2 BY .62 NO-UNDO.

DEFINE VARIABLE fTotalCostL AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16.4 BY 1 NO-UNDO.

DEFINE VARIABLE netfrom12 AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 17.4 BY 1 NO-UNDO.

DEFINE VARIABLE netfrom24 AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 17.4 BY 1 NO-UNDO.

DEFINE VARIABLE netfrom36 AS INTEGER FORMAT "$-z,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 17.4 BY 1 NO-UNDO.

DEFINE VARIABLE netfromLTD AS INTEGER FORMAT "$-z,zzz,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE BUTTON bOrgLookup  NO-FOCUS
     LABEL "Lookup" 
     SIZE 4.6 BY 1.14 TOOLTIP "Organization lookup".

DEFINE BUTTON bReqFulExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export to a CSV File".

DEFINE BUTTON btComQualExp  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export to a CSV File".

DEFINE BUTTON btComQualRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh data".

DEFINE BUTTON btComReqFulRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh data".

DEFINE BUTTON btComViewQual  NO-FOCUS
     LABEL "View Qualification" 
     SIZE 4.6 BY 1.14 TOOLTIP "View qualification".

DEFINE BUTTON btComViewReq  NO-FOCUS
     LABEL "View requirement" 
     SIZE 4.6 BY 1.14 TOOLTIP "View requirement".

DEFINE VARIABLE tOrganization AS CHARACTER FORMAT "X(256)":U 
     LABEL "Organization ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE tOrgName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 160.4 BY 1 NO-UNDO.

DEFINE VARIABLE tShowInactiveQuals AS LOGICAL INITIAL yes 
     LABEL "Show inactive Qualifications" 
     VIEW-AS TOGGLE-BOX
     SIZE 31 BY .81 TOOLTIP "Show inactive Qualifications" NO-UNDO.

DEFINE BUTTON bAddImg  NO-FOCUS
     LABEL "Add Logo" 
     SIZE 4.6 BY 1.14 TOOLTIP "Add Agent Logo".

DEFINE BUTTON bAgentRefresh AUTO-GO  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh data".

DEFINE BUTTON bCancel AUTO-GO  NO-FOCUS
     LABEL "Cancel" 
     SIZE 4.6 BY 1.14 TOOLTIP "Cancel".

DEFINE BUTTON bdeleteImag  NO-FOCUS
     LABEL "Delete" 
     SIZE 4.6 BY 1.14 TOOLTIP "Delete Agent Logo".

DEFINE BUTTON bDelTag  NO-FOCUS
     LABEL "Delete tag" 
     SIZE 4.6 BY 1.14 TOOLTIP "Delete tag".

DEFINE BUTTON bEditManager  NO-FOCUS
     LABEL "Edit" 
     SIZE 4.6 BY 1.14 TOOLTIP "Update Manager".

DEFINE BUTTON bEditTag  NO-FOCUS
     LABEL "Edit" 
     SIZE 4.6 BY 1.14 TOOLTIP "Update tag".

DEFINE BUTTON bNewTag  NO-FOCUS
     LABEL "New" 
     SIZE 4.6 BY 1.14 TOOLTIP "Add a tag".

DEFINE BUTTON bSave AUTO-GO  NO-FOCUS
     LABEL "Save" 
     SIZE 4.6 BY 1.14 TOOLTIP "Save".

DEFINE BUTTON bTagRefresh AUTO-GO  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh data".

DEFINE BUTTON btnAgentMail  NO-FOCUS
     LABEL "Mail" 
     SIZE 4.6 BY 1.14 TOOLTIP "Mail".

DEFINE BUTTON btnSameAsAbove 
     LABEL "Same as Above" 
     SIZE 20 BY 1.14.

DEFINE BUTTON BtnUrl  NO-FOCUS
     LABEL "URL" 
     SIZE 4.6 BY 1.14 TOOLTIP "Open URL".

DEFINE VARIABLE cbContractID AS CHARACTER FORMAT "X(256)" 
     LABEL "Contract" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 18.8 BY 1 TOOLTIP "Contract" NO-UNDO.

DEFINE VARIABLE cbMState AS CHARACTER FORMAT "X(256)" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Co","CO"
     DROP-DOWN-LIST
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE cbRegion AS CHARACTER FORMAT "X(256)" 
     LABEL "Region" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Item1","Item1"
     DROP-DOWN-LIST
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "CO","CO"
     DROP-DOWN-LIST
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE cbStateOper AS CHARACTER FORMAT "X(256)" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "CO","CO"
     DROP-DOWN-LIST
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE cbSwVendor AS CHARACTER FORMAT "X(256)" 
     LABEL "Software" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 47 BY 1 TOOLTIP "Title company software vendor" NO-UNDO.

DEFINE VARIABLE fDomain AS CHARACTER FORMAT "X(256)":U 
     LABEL "Domain" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE flActiveDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Active" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flAddr1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE flAddr2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE flAgentName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE flCancelDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Cancelled" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flCity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 17.8 BY 1 TOOLTIP "City" NO-UNDO.

DEFINE VARIABLE flClosedDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Closed" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flContractDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Signed" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flEmail AS CHARACTER FORMAT "X(256)":U 
     LABEL "Email" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1.

DEFINE VARIABLE flFax AS CHARACTER FORMAT "X(256)":U 
     LABEL "Fax" 
     VIEW-AS FILL-IN 
     SIZE 19.2 BY 1 NO-UNDO.

DEFINE VARIABLE flLAgentName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Legal Name" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE flLiabilityLimit AS DECIMAL FORMAT ">>,>>>,>>9":U INITIAL 0 
     LABEL "Limit($)" 
     VIEW-AS FILL-IN 
     SIZE 18.8 BY 1 TOOLTIP "Maximum coverage w/o prior approval" NO-UNDO.

DEFINE VARIABLE flMAddr1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE flMAddr2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE flMCity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 17.8 BY 1 TOOLTIP "City" NO-UNDO.

DEFINE VARIABLE flMName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE flMZip AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 TOOLTIP "Zipcode" NO-UNDO.

DEFINE VARIABLE flOAltaUID AS CHARACTER FORMAT "X(200)":U 
     LABEL "ALTA UID" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE flOCorporation AS CHARACTER FORMAT "X(256)":U 
     LABEL "Company" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE flOManager AS CHARACTER FORMAT "X(256)":U 
     LABEL "Manager" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 42.2 BY 1 NO-UNDO.

DEFINE VARIABLE flONPN AS CHARACTER FORMAT "X(256)":U 
     LABEL "NPN" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE flOStateUID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State UID" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE flPhone AS CHARACTER FORMAT "X(256)":U 
     LABEL "Phone" 
     VIEW-AS FILL-IN 
     SIZE 21.8 BY 1 NO-UNDO.

DEFINE VARIABLE flPolicyLimit AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Limit(#)" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 TOOLTIP "Policy Limit" NO-UNDO.

DEFINE VARIABLE flProspectDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Prospect" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flRemitAlert AS DECIMAL FORMAT ">>>,>>>,>>9":U INITIAL 0 
     LABEL "Alert" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 TOOLTIP "Processing threshold for review" NO-UNDO.

DEFINE VARIABLE flRemitValue AS DECIMAL FORMAT "9.99":U INITIAL 0 
     LABEL "Remits" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 TOOLTIP "Percentage, then value is % of gross :: Liability, then value is rate/1000" NO-UNDO.

DEFINE VARIABLE flReviewDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Reviewed" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Last review" NO-UNDO.

DEFINE VARIABLE flWebsite AS CHARACTER FORMAT "X(256)":U 
     LABEL "Website" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE flWithdrawnDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Withdrawn" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flZip AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 TOOLTIP "Zipcode" NO-UNDO.

DEFINE VARIABLE rdRemitType AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Percentage", "P",
"Liability", "L"
     SIZE 18 BY 1.71 NO-UNDO.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 62 BY 7.

DEFINE RECTANGLE RECT-34
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 6 BY 3.76.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 62 BY 6.62.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 63.6 BY 6.62.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 63.6 BY 7.

DEFINE BUTTON bBwdNinety AUTO-GO  NO-FOCUS
     LABEL "Backward90" 
     SIZE 7.2 BY 1.71 TOOLTIP "Move Backward 90 days".

DEFINE BUTTON bBwdThirty AUTO-GO  NO-FOCUS
     LABEL "Backward30" 
     SIZE 7.2 BY 1.71 TOOLTIP "Move Backward 30 days".

DEFINE BUTTON bCancelObj  NO-FOCUS
     LABEL "Cancel" 
     SIZE 4.6 BY 1.14 TOOLTIP "Cancel Objective".

DEFINE BUTTON bCancelObjTh  NO-FOCUS
     LABEL "Cancel" 
     SIZE 4.6 BY 1.14 TOOLTIP "Cancel Objective".

DEFINE BUTTON bCollapse AUTO-GO  NO-FOCUS
     LABEL "-" 
     SIZE 10.4 BY 1.14 TOOLTIP "Collapse the time horizon"
     FONT 10.

DEFINE BUTTON bEditEvent  NO-FOCUS
     LABEL "Edit" 
     SIZE 4.6 BY 1.14 TOOLTIP "Update Event".

DEFINE BUTTON bEditObj  NO-FOCUS
     LABEL "Edit" 
     SIZE 4.6 BY 1.14 TOOLTIP "Update Objective".

DEFINE BUTTON bEditObjTh  NO-FOCUS
     LABEL "Edit" 
     SIZE 4.6 BY 1.14 TOOLTIP "Update Objective".

DEFINE BUTTON bEventAf AUTO-GO  NO-FOCUS
     LABEL "EventAf" 
     SIZE 7.2 BY 1.71 TOOLTIP "More Events are there after".

DEFINE BUTTON bEventBf AUTO-GO  NO-FOCUS
     LABEL "EventBf" 
     SIZE 7.2 BY 1.71 TOOLTIP "More Events are there before".

DEFINE BUTTON bEventExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export".

DEFINE BUTTON bEventRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh".

DEFINE BUTTON bExpand AUTO-GO  NO-FOCUS
     LABEL "+" 
     SIZE 10.4 BY 1.14 TOOLTIP "Expand the time horizon"
     FONT 10.

DEFINE BUTTON bFwdNinety AUTO-GO  NO-FOCUS
     LABEL "Forward90" 
     SIZE 7.2 BY 1.71 TOOLTIP "Move Forward 90 days".

DEFINE BUTTON bFwdThirty AUTO-GO  NO-FOCUS
     LABEL "Forward30" 
     SIZE 7.2 BY 1.71 TOOLTIP "Move Forward 30 days".

DEFINE BUTTON bNewEvent  NO-FOCUS
     LABEL "New" 
     SIZE 4.6 BY 1.14 TOOLTIP "Add an Event".

DEFINE BUTTON bNewObj  NO-FOCUS
     LABEL "New" 
     SIZE 4.6 BY 1.14.

DEFINE BUTTON bNewObjTh  NO-FOCUS
     LABEL "New" 
     SIZE 4.6 BY 1.14 TOOLTIP "Add an Objective".

DEFINE BUTTON bSnmtOurObj  NO-FOCUS
     LABEL "SentimentO" 
     SIZE 4.6 BY 1.14.

DEFINE BUTTON bSnmtThObj  NO-FOCUS
     LABEL "SentimentT" 
     SIZE 4.6 BY 1.14.

DEFINE BUTTON bTimeLine AUTO-GO  NO-FOCUS
     LABEL "" 
     SIZE 31.2 BY 1.71.

DEFINE BUTTON bToday AUTO-GO  NO-FOCUS
     LABEL "Today" 
     SIZE 10.4 BY 1.14 TOOLTIP "Move to time window containing today"
     FONT 4.

DEFINE VARIABLE edObj AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 85.2 BY 3.33 NO-UNDO.

DEFINE VARIABLE edObjTh AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 85.2 BY 3.33 NO-UNDO.

DEFINE VARIABLE fCreateDtO AS DATE FORMAT "99/99/99":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 13.8 BY 1 NO-UNDO.

DEFINE VARIABLE fCreateDtTh AS DATE FORMAT "99/99/99":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 13.8 BY 1 NO-UNDO.

DEFINE VARIABLE fNoSentO AS CHARACTER FORMAT "X(256)":U INITIAL "No Sentiment Recorded" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE fNoSentTh AS CHARACTER FORMAT "X(256)":U INITIAL "No Sentiment Recorded" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 25 BY 1 NO-UNDO.

DEFINE BUTTON bPersonAgentAdd  NO-FOCUS
     LABEL "PersonAgentAdd" 
     SIZE 4.6 BY 1.14 TOOLTIP "Add person".

DEFINE BUTTON bPersonAgentDeactivate  NO-FOCUS
     LABEL "PersonAgentDeactivate" 
     SIZE 4.6 BY 1.14 TOOLTIP "Deactivate personagent".

DEFINE BUTTON bPersonAgentExport  NO-FOCUS
     LABEL "PersonAgentExport" 
     SIZE 4.6 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bpersonagentlookup  NO-FOCUS
     LABEL "personagentlookup" 
     SIZE 4.6 BY 1.14 TOOLTIP "lookup person".

DEFINE BUTTON bPersonAgentUpdate  NO-FOCUS
     LABEL "PersonAgentUpdate" 
     SIZE 4.6 BY 1.14 TOOLTIP "Modify Job Title".

DEFINE BUTTON btnAgentPersonMail  NO-FOCUS
     LABEL "AgentPersonMail" 
     SIZE 4.6 BY 1.14 TOOLTIP "Mail".

DEFINE BUTTON btPersonAgentRefresh AUTO-GO  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14 TOOLTIP "Refresh person".

DEFINE VARIABLE AffType AS CHARACTER INITIAL "B" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Active", "A":U,
"Inactive", "I":U,
"Both", "B":U
     SIZE 35 BY 1.38 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAction FOR 
      action SCROLLING.

DEFINE QUERY brwActivityBatch FOR 
      tActivity SCROLLING.

DEFINE QUERY brwActivityCPL FOR 
      tMetricsActivity SCROLLING.

DEFINE QUERY brwActivityPolicies FOR 
      tMetricsActivity SCROLLING.

DEFINE QUERY brwAlerts FOR 
      alert SCROLLING.

DEFINE QUERY brwArAging FOR 
      tArAging SCROLLING.

DEFINE QUERY brwBatch FOR 
      batch SCROLLING.

DEFINE QUERY brwClaims FOR 
      agentclaim SCROLLING.

DEFINE QUERY brwEvent FOR 
      tlEvent SCROLLING.

DEFINE QUERY brwpersonagent FOR 
      tpersonagent SCROLLING.

DEFINE QUERY brwQAR FOR 
      audit SCROLLING.

DEFINE QUERY brwQualification FOR 
      qualification SCROLLING.

DEFINE QUERY brwReqFul FOR 
      reqfulfill SCROLLING.

DEFINE QUERY brwTags FOR 
      tag SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAction wWin _FREEFORM
  QUERY brwAction DISPLAY
      action.dueDate label "Due"                   width 12
action.ownerDesc     label "Owner"  format "x(40)" width 30
action.comments      label "Action" format "x(350)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 191.8 BY 7.81
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Open Corrective Actions" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwActivityBatch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwActivityBatch wWin _FREEFORM
  QUERY brwActivityBatch DISPLAY
      tActivity.typeDesc width 13          
tActivity.month1 width 11
tActivity.month2 width 11
tActivity.month3 width 11
tActivity.month4 width 11
tActivity.month5 width 11
tActivity.month6 width 11
tActivity.month7 width 11
tActivity.month8 width 11
tActivity.month9 width 11
tActivity.month10 width 11
tActivity.month11 width 11
tActivity.month12 width 11
tActivity.total   width 15
tActivity.yrTotal label "YTD Jul" width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-SCROLLBAR-VERTICAL SIZE 188.6 BY 6.76
         TITLE "Net Premium" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwActivityCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwActivityCPL wWin _FREEFORM
  QUERY brwActivityCPL DISPLAY
      tMetricsActivity.year format "ZZZZ" width 7
tMetricsActivity.typeDesc width 14          
tMetricsActivity.month1 width 11
tMetricsActivity.month2 width 11
tMetricsActivity.month3 width 11
tMetricsActivity.month4 width 11
tMetricsActivity.month5 width 11
tMetricsActivity.month6 width 11
tMetricsActivity.month7 width 11
tMetricsActivity.month8 width 11
tMetricsActivity.month9 width 11
tMetricsActivity.month10 width 11
tMetricsActivity.month11 width 11
tMetricsActivity.month12 width 11
tMetricsActivity.Total width 14 label 'Total'
tMetricsActivity.yrTotal width 14 label 'YTD Total'
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-SCROLLBAR-VERTICAL SIZE 192.6 BY 6.76
         TITLE "Closing Protection Letters" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwActivityPolicies
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwActivityPolicies wWin _FREEFORM
  QUERY brwActivityPolicies DISPLAY
      tMetricsActivity.year format "ZZZZ" width 7
tMetricsActivity.typeDesc width 14          
tMetricsActivity.month1 width 11
tMetricsActivity.month2 width 11
tMetricsActivity.month3 width 11
tMetricsActivity.month4 width 11
tMetricsActivity.month5 width 11
tMetricsActivity.month6 width 11
tMetricsActivity.month7 width 11
tMetricsActivity.month8 width 11
tMetricsActivity.month9 width 11
tMetricsActivity.month10 width 11
tMetricsActivity.month11 width 11
tMetricsActivity.month12 width 11
tMetricsActivity.Total   width 14 label 'Total'
tMetricsActivity.yrTotal width 14 label 'YTD Total'
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-SCROLLBAR-VERTICAL SIZE 192.6 BY 6.76
         TITLE "Policies" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwAlerts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAlerts wWin _FREEFORM
  QUERY brwAlerts DISPLAY
      alert.dateCreated                    label        "Created"       format "99/99/9999"  width 15      
alert.processCodeDesc                      label        "Alert Type"    format "x(100)"      width 30 
alert.thresholdRange                       label        "Threshold"     format "x(100)"      width 20
alert.scoreDesc                            column-label "Measure"       format "x(20)"       width 15
alert.severityDesc                         label        "Severity"      format "x(10)"       width 10
alert.ownerDesc                            column-label "Owner"         format "x(15)"       width 15
alert.description                          label        "Description"   format "x(100)"      width 55
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 192.8 BY 17.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwArAging
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwArAging wWin _FREEFORM
  QUERY brwArAging DISPLAY
      tArAging.fileNumber  label "File Number" format "x(30)"  width 30
tArAging.due0_30   label "Current"                      width 30
tArAging.due31_60  label "31 - 60 Days"                 width 30
tArAging.due61_90  label "61 - 90 Days"                 width 30
tArAging.due91_    label "Over 90 Days"                 width 30
tArAging.balance   label "Balance"                      width 30
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 189.8 BY 17.81
         TITLE "Select the Aging Category" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwBatch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwBatch wWin _FREEFORM
  QUERY brwBatch DISPLAY
      batch.receivedDate         column-label "Received"         format "99/99/9999"
 batch.invoiceDate          column-label "Completed"        format "99/99/9999"
 batch.batchID              label        "Batch ID"         format " >>>>>>>>>9  " 
 batch.reference            column-label "Reference"        format "x(63)"           width 63
 batch.fileCount            column-label "Files"            format "zzz,zzz"         width 11
 batch.policyCount          column-label "Policies"         format "zzz,zzz"         width 11
 batch.endorsementCount     column-label "Endorsements"     format "zzz,zzz"         width 15
 batch.netPremiumReported   column-label "Reported!Net"     format "$->>,>>>,>>9.99"
 batch.netPremiumDelta      column-label "Accounting!Net"   format "$->>,>>>,>>9.99"
 getBatchStatus(batch.stat) @ batch.stat column-label "Status"  format "x(11)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 193.2 BY 11.43
         TITLE "Batches" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwClaims
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwClaims wWin _FREEFORM
  QUERY brwClaims DISPLAY
      agentclaim.claimID column-label "Claim ID" format "99999999"  width 14
agentclaim.statDesc column-label "Status" format "x(10)" width 11
agentclaim.agentErrorDesc COLUMN-LABEL "Agent!Error" FORMAT "x(20)" WIDTH 14
agentclaim.assignedToDesc column-label "Assigned To" format "x(100)" width 30
agentclaim.dateOpened column-label "Opened" format "99/99/9999" width 14
agentclaim.laeReserve column-label "LAE!Reserve" format "(>>,>>>,>>>,>>Z)" width 16
agentclaim.laeLTD column-label "LAE!LTD" format "(>>,>>>,>>>,>>Z)" width 16
agentclaim.lossReserve column-label "Loss!Reserve" format "(>>,>>>,>>>,>>Z)" width 16
agentclaim.lossLTD column-label "Losses!LTD" format "(>>,>>>,>>>,>>Z)" width 16
agentclaim.recoveries column-label "Recoveries!Received" format "(>>,>>>,>>>,>>Z)" width 16

agentclaim.costsPaid column-label "Total Costs!LTD" format "(>>,>>>,>>>,>>Z)" width 16
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 191 BY 13.43
         TITLE "All Claims" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwEvent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwEvent wWin _FREEFORM
  QUERY brwEvent DISPLAY
      tlevent.date               label "Date"              format "99/99/9999"    width 15  
   tlevent.name                  label "Event"             format "x(20)"         width 25
   tlevent.description           label "Description"       format "x(100)"        width 140
   tlevent.days                  label "Days "             format "->>>>"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 193.2 BY 9.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwpersonagent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwpersonagent wWin _FREEFORM
  QUERY brwpersonagent DISPLAY
      tpersonagent.active       column-label "Active"                             width 10      view-as toggle-box        
tpersonagent.personName         label "Name"                        format "x(30)"    width 24  
tpersonagent.jobTitle           label "Job Title"                   format "x(30)"    width 17
tpersonagent.phone              label "Phone"                       format "x(20)"    width 16
tpersonagent.email              label "Email"                       format "x(50)"    width 26
tpersonagent.notes              label "Notes"                       format "x(50)"    width 49
tpersonagent.doNotCall          column-label "DNC"       width 8 view-as toggle-box 
tpersonagent.isCtrlPerson       column-label "CP"       width 8 view-as toggle-box 
tpersonagent.linkedToCompliance column-label "COM" width 8 view-as toggle-box
tpersonagent.personID           label "Person ID"                   format "x(30)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 191 BY 19.52
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwQAR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQAR wWin _FREEFORM
  QUERY brwQAR DISPLAY
      audit.stat      label "Status"            format "x(12)"     width 12      
 audit.audittype column-label "Audit!Type" format "x(12)"     width 12     
 audit.errtype   label "Type"              format "x(18)"     width 18
audit.auditStartDate                            label "Start"             format "99/99/99"  width 12
audit.auditFinishDate                           label "Finish"            format "99/99/99"  width 12
audit.score                                     label "Points"            format "zzz "      width 8
audit.grade                                     label "Score%"            format "zzz "      width 9
audit.auditor                                   label "Auditor"           format "x(100)"    width 30
audit.comments                                  label "Comments"          format "x(150)"    width 45
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 191.8 BY 10.43
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Audits" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwQualification
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQualification wWin _FREEFORM
  QUERY brwQualification DISPLAY
      qualification.activ          column-label "Active"                                       width 10 view-as toggle-box   
qualification.stateID                     label "State ID"              format "x(5)"          width 20
qualification.Qualification               label "Qualification"         format "x(50)"         width 70
qualification.QualificationNumber         label "Number"                format "x(10)"         width 20
qualification.stat                        label "Status"                format "x(15)"         width 20
qualification.effectiveDate               label "Effective"             format "99/99/9999"    width 25
qualification.expirationDate              label "Expiration"            format "99/99/9999"    width 25
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 192 BY 6.81
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Qualifications" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwReqFul
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwReqFul wWin _FREEFORM
  QUERY brwReqFul DISPLAY
      reqfulfill.reqMet                                          column-label "Met"         width 6    view-as toggle-box
reqfulfill.reqdesc                                           label        "Requirement"   format "x(50)"      width 45
reqfulfill.qualification                                     label        "Qualification" format "x(40)"      width 45
reqfulfill.stat                                              label        "Status"        format "X(16)"      width 15
reqfulfill.appliesTo                                         label        "Fulfilled By"  format "x(15)"      width 13
reqfulfill.entityname                                        label        "Name"          format "x(30)"      width 35
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 192 BY 9.81
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Requirements and Fulfillments" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwTags
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwTags wWin _FREEFORM
  QUERY brwTags DISPLAY
      tag.name label "Tag"    format "x(20)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 50.8 BY 13.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bAgentJournals AT ROW 2.05 COL 152.2 WIDGET-ID 388 NO-TAB-STOP 
     bConsolidate AT ROW 2.05 COL 138.2 WIDGET-ID 384 NO-TAB-STOP 
     hdAgentID AT ROW 1.95 COL 14.6 COLON-ALIGNED WIDGET-ID 360 NO-TAB-STOP 
     hdAgentName AT ROW 1.95 COL 47.8 COLON-ALIGNED WIDGET-ID 364 NO-TAB-STOP 
     fStatus AT ROW 3.19 COL 14.6 COLON-ALIGNED WIDGET-ID 362 NO-TAB-STOP 
     hdLAgentName AT ROW 3.19 COL 47.8 COLON-ALIGNED WIDGET-ID 366 NO-TAB-STOP 
     bPDF AT ROW 2.05 COL 145.2 WIDGET-ID 386 NO-TAB-STOP 
     bRefresh AT ROW 2.05 COL 110.2 WIDGET-ID 382 NO-TAB-STOP 
     bStatusChange AT ROW 3.14 COL 29 WIDGET-ID 380 NO-TAB-STOP 
     bARInfo AT ROW 2.05 COL 124.2 WIDGET-ID 262 NO-TAB-STOP 
     bFollow AT ROW 2.05 COL 131.2 WIDGET-ID 376 NO-TAB-STOP 
     bNotes AT ROW 2.05 COL 117.2 WIDGET-ID 368 NO-TAB-STOP 
     "Actions" VIEW-AS TEXT
          SIZE 7.2 BY .62 AT ROW 1.33 COL 110.2 WIDGET-ID 374
     RECT-68 AT ROW 1.62 COL 108.8 WIDGET-ID 372
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 205.4 BY 28 WIDGET-ID 100.

DEFINE FRAME frpersonagent
     bpersonagentlookup AT ROW 8.14 COL 4 WIDGET-ID 172 NO-TAB-STOP 
     AffType AT ROW 1.1 COL 28.4 NO-LABEL WIDGET-ID 358
     brwpersonagent AT ROW 2.48 COL 9 WIDGET-ID 2700
     bPersonAgentUpdate AT ROW 5.86 COL 4 WIDGET-ID 344 NO-TAB-STOP 
     bPersonAgentAdd AT ROW 4.71 COL 4 WIDGET-ID 342 NO-TAB-STOP 
     bPersonAgentDeactivate AT ROW 7 COL 4 WIDGET-ID 408 NO-TAB-STOP 
     bPersonAgentExport AT ROW 3.57 COL 4 WIDGET-ID 326 NO-TAB-STOP 
     btnAgentPersonMail AT ROW 9.29 COL 4 WIDGET-ID 378 NO-TAB-STOP 
     btPersonAgentRefresh AT ROW 2.43 COL 4 WIDGET-ID 402 NO-TAB-STOP 
     "Show association :" VIEW-AS TEXT
          SIZE 18.8 BY .86 AT ROW 1.38 COL 9 WIDGET-ID 362
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "People" WIDGET-ID 3000.

DEFINE FRAME frAlert
     bExport AT ROW 5 COL 2.2 WIDGET-ID 316 NO-TAB-STOP 
     fCode AT ROW 2.1 COL 32.2 COLON-ALIGNED WIDGET-ID 324
     fOwner AT ROW 2.1 COL 107 COLON-ALIGNED WIDGET-ID 326
     brwAlerts AT ROW 3.91 COL 7 WIDGET-ID 1500
     bGo AT ROW 3.86 COL 2.2 WIDGET-ID 318 NO-TAB-STOP 
     bModify AT ROW 6.14 COL 2.2 WIDGET-ID 320 NO-TAB-STOP 
     bNewNote AT ROW 7.29 COL 2.2 WIDGET-ID 322 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "Alerts" WIDGET-ID 2300.

DEFINE FRAME frCompliance
     tOrganization AT ROW 2.1 COL 22 COLON-ALIGNED WIDGET-ID 282
     bReqFulExport AT ROW 13.29 COL 2 WIDGET-ID 416 NO-TAB-STOP 
     tOrgName AT ROW 2.1 COL 37.4 COLON-ALIGNED NO-LABEL WIDGET-ID 404
     tShowInactiveQuals AT ROW 3.91 COL 8 WIDGET-ID 422
     brwQualification AT ROW 4.91 COL 8 WIDGET-ID 3200
     brwReqFul AT ROW 12.14 COL 8 WIDGET-ID 1200
     btComQualExp AT ROW 6.05 COL 2.2 WIDGET-ID 412 NO-TAB-STOP 
     btComQualRefresh AT ROW 4.91 COL 2.2 WIDGET-ID 410 NO-TAB-STOP 
     btComReqFulRefresh AT ROW 12.14 COL 2 WIDGET-ID 414 NO-TAB-STOP 
     btComViewQual AT ROW 7.19 COL 2.2 WIDGET-ID 418 NO-TAB-STOP 
     btComViewReq AT ROW 14.43 COL 2 WIDGET-ID 420 NO-TAB-STOP 
     bOrgLookup AT ROW 2 COL 34.2 WIDGET-ID 264 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "Compliance" WIDGET-ID 1100.

DEFINE FRAME frAudit
     brwQAR AT ROW 2 COL 7.6 WIDGET-ID 400
     brwAction AT ROW 13.48 COL 7.6 WIDGET-ID 1800
     btActionExport AT ROW 14.57 COL 2.8 WIDGET-ID 406 NO-TAB-STOP 
     btActionMail AT ROW 16.86 COL 2.8 WIDGET-ID 398 NO-TAB-STOP 
     btActionOpen AT ROW 15.71 COL 2.8 WIDGET-ID 400 NO-TAB-STOP 
     btActionRefresh AT ROW 13.43 COL 2.8 WIDGET-ID 402 NO-TAB-STOP 
     btAuditExport AT ROW 3.1 COL 2.8 WIDGET-ID 404 NO-TAB-STOP 
     btAuditMail AT ROW 5.38 COL 2.8 WIDGET-ID 392 NO-TAB-STOP 
     btAuditOpen AT ROW 4.24 COL 2.8 WIDGET-ID 394 NO-TAB-STOP 
     btAuditRefresh AT ROW 1.95 COL 2.8 WIDGET-ID 396 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "Audits" WIDGET-ID 1700.

DEFINE FRAME frClaim
     btClaimExport AT ROW 7.57 COL 3 WIDGET-ID 382 NO-TAB-STOP 
     btClaimMail AT ROW 9.86 COL 3 WIDGET-ID 384 NO-TAB-STOP 
     netfrom36 AT ROW 3.14 COL 85.4 COLON-ALIGNED NO-LABEL WIDGET-ID 446 NO-TAB-STOP 
     netfrom24 AT ROW 3.14 COL 122.4 COLON-ALIGNED NO-LABEL WIDGET-ID 452 NO-TAB-STOP 
     netfrom12 AT ROW 3.14 COL 159.4 COLON-ALIGNED NO-LABEL WIDGET-ID 516 NO-TAB-STOP 
     btClaimRefresh AT ROW 6.43 COL 3 WIDGET-ID 380 NO-TAB-STOP 
     netfromLTD AT ROW 3.19 COL 68.2 RIGHT-ALIGNED NO-LABEL WIDGET-ID 440 NO-TAB-STOP 
     costfrom36 AT ROW 4.1 COL 85.4 COLON-ALIGNED NO-LABEL WIDGET-ID 448 NO-TAB-STOP 
     costfrom24 AT ROW 4.1 COL 122.4 COLON-ALIGNED NO-LABEL WIDGET-ID 454 NO-TAB-STOP 
     costfrom12 AT ROW 4.1 COL 159.4 COLON-ALIGNED NO-LABEL WIDGET-ID 518 NO-TAB-STOP 
     costfromLTD AT ROW 4.14 COL 44.2 COLON-ALIGNED NO-LABEL WIDGET-ID 442 NO-TAB-STOP 
     errorfromLTD AT ROW 5.05 COL 44.2 COLON-ALIGNED NO-LABEL WIDGET-ID 444 NO-TAB-STOP 
     btClaimOpen AT ROW 8.71 COL 3 WIDGET-ID 390 NO-TAB-STOP 
     errortfrom36 AT ROW 5.05 COL 85.4 COLON-ALIGNED NO-LABEL WIDGET-ID 450 NO-TAB-STOP 
     errorfrom24 AT ROW 5.05 COL 122.4 COLON-ALIGNED NO-LABEL WIDGET-ID 460 NO-TAB-STOP 
     errorfrom12 AT ROW 5.05 COL 159.4 COLON-ALIGNED NO-LABEL WIDGET-ID 520 NO-TAB-STOP 
     brwClaims AT ROW 6.48 COL 8 WIDGET-ID 2000
     fLAER AT ROW 20.43 COL 93.4 COLON-ALIGNED NO-LABEL WIDGET-ID 526 NO-TAB-STOP 
     fLAEL AT ROW 20.43 COL 110.4 COLON-ALIGNED NO-LABEL WIDGET-ID 538 NO-TAB-STOP 
     fLossR AT ROW 20.43 COL 127.2 COLON-ALIGNED NO-LABEL WIDGET-ID 540 NO-TAB-STOP 
     fLossL AT ROW 20.43 COL 144 COLON-ALIGNED NO-LABEL WIDGET-ID 542 NO-TAB-STOP 
     fRecoveriesR AT ROW 20.43 COL 161 COLON-ALIGNED NO-LABEL WIDGET-ID 544 NO-TAB-STOP 
     fTotalCostL AT ROW 20.43 COL 177.8 COLON-ALIGNED NO-LABEL WIDGET-ID 546 NO-TAB-STOP 
     from12 AT ROW 1.62 COL 160 COLON-ALIGNED NO-LABEL WIDGET-ID 504
     from36 AT ROW 1.67 COL 86.2 COLON-ALIGNED NO-LABEL WIDGET-ID 430
     from24 AT ROW 1.67 COL 124 COLON-ALIGNED NO-LABEL WIDGET-ID 432
     costnetLTD AT ROW 4.29 COL 67.8 COLON-ALIGNED NO-LABEL WIDGET-ID 478
     costnet36 AT ROW 4.29 COL 103.4 COLON-ALIGNED NO-LABEL WIDGET-ID 476
     costnet24 AT ROW 4.29 COL 140.4 COLON-ALIGNED NO-LABEL WIDGET-ID 486
     costnet12 AT ROW 4.29 COL 177.4 COLON-ALIGNED NO-LABEL WIDGET-ID 508
     errornet12 AT ROW 5.19 COL 177.4 COLON-ALIGNED NO-LABEL WIDGET-ID 510
     errornetLTD AT ROW 5.24 COL 67.8 COLON-ALIGNED NO-LABEL WIDGET-ID 480
     errornet36 AT ROW 5.24 COL 103.2 COLON-ALIGNED NO-LABEL WIDGET-ID 482
     errornet24 AT ROW 5.24 COL 140.4 COLON-ALIGNED NO-LABEL WIDGET-ID 484
     fTotal AT ROW 20.67 COL 71 COLON-ALIGNED NO-LABEL WIDGET-ID 524
     "Agent Error Total Costs:" VIEW-AS TEXT
          SIZE 23 BY .62 AT ROW 5.19 COL 22.2 WIDGET-ID 438
     "Life-to-Date" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 2.24 COL 51.8 WIDGET-ID 420
     "(12 Months)" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 2.19 COL 163 WIDGET-ID 506
     "Total Cost Paid:" VIEW-AS TEXT
          SIZE 15.4 BY .62 AT ROW 4.29 COL 29.6 WIDGET-ID 492
     "Net Premium:" VIEW-AS TEXT
          SIZE 13 BY .62 AT ROW 3.29 COL 32 WIDGET-ID 434
     "(24 Months)" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 2.24 COL 127.4 WIDGET-ID 422
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3 WIDGET-ID 1900.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME frClaim
     "(36 Months)" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 2.24 COL 89.6 WIDGET-ID 424
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "Claims" WIDGET-ID 1900.

DEFINE FRAME frAr
     btArMail AT ROW 7.24 COL 4 WIDGET-ID 384 NO-TAB-STOP 
     b30to60 AT ROW 2.38 COL 65.6 WIDGET-ID 478
     b60to90 AT ROW 2.38 COL 90.2 WIDGET-ID 480
     bbalance AT ROW 2.38 COL 139.2 WIDGET-ID 484
     bCurrentaging AT ROW 2.38 COL 40.8 WIDGET-ID 476
     bover90 AT ROW 2.38 COL 114.8 WIDGET-ID 482
     btRefreshAr AT ROW 3.81 COL 4 WIDGET-ID 474 NO-TAB-STOP 
     btFileExport AT ROW 4.95 COL 4 WIDGET-ID 414 NO-TAB-STOP 
     btFileOpen AT ROW 6.1 COL 4 WIDGET-ID 428 NO-TAB-STOP 
     brwArAging AT ROW 3.86 COL 9 WIDGET-ID 1500
     "Over 90 Days" VIEW-AS TEXT
          SIZE 14 BY .62 AT ROW 1.71 COL 120.2 WIDGET-ID 22
     "61 to 90 Days" VIEW-AS TEXT
          SIZE 14 BY .62 AT ROW 1.71 COL 95.4 WIDGET-ID 20
     "Total" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1.71 COL 147.8 WIDGET-ID 24
     "31 to 60 Days" VIEW-AS TEXT
          SIZE 14 BY .62 AT ROW 1.71 COL 71 WIDGET-ID 18
     "Current" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.71 COL 49.2 WIDGET-ID 16
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "A/R" WIDGET-ID 2600.

DEFINE FRAME frActivity
     brwActivityPolicies AT ROW 2 COL 7.4 WIDGET-ID 2900
     brwActivityCPL AT ROW 11.52 COL 7.4 WIDGET-ID 3400
     bActivityExp AT ROW 3.05 COL 2.4 WIDGET-ID 566 NO-TAB-STOP 
     bActivityExpCPL AT ROW 12.57 COL 2.4 WIDGET-ID 590 NO-TAB-STOP 
     bActivityMetricsRefreshCPL AT ROW 11.48 COL 2.4 WIDGET-ID 592 NO-TAB-STOP 
     bActivityMetricsRefresh AT ROW 1.95 COL 2.4 WIDGET-ID 588 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "Metrics" WIDGET-ID 2800.

DEFINE FRAME frBatch
     brwActivityBatch AT ROW 2.1 COL 12 WIDGET-ID 3300
     brwBatch AT ROW 10.05 COL 7.6 WIDGET-ID 200
     bActivityBatchExport AT ROW 2.05 COL 6.8 WIDGET-ID 580 NO-TAB-STOP 
     bActivityBatchRefresh AT ROW 2.05 COL 2.4 WIDGET-ID 588 NO-TAB-STOP 
     bYear1 AT ROW 5.91 COL 2.4 WIDGET-ID 572
     bYear2 AT ROW 4.91 COL 2.4 WIDGET-ID 574
     bYear3 AT ROW 3.91 COL 2.4 WIDGET-ID 576
     bBatchRefresh AT ROW 10 COL 2.6 WIDGET-ID 578 NO-TAB-STOP 
     bBatchExport AT ROW 11.14 COL 2.6 WIDGET-ID 566 NO-TAB-STOP 
     bBatchOpen AT ROW 12.29 COL 2.6 WIDGET-ID 568 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "Remittances" WIDGET-ID 1000.

DEFINE FRAME frEvent
     bCancelObj AT ROW 4.48 COL 110 WIDGET-ID 204 NO-TAB-STOP 
     bCancelObjTh AT ROW 4.38 COL 2 WIDGET-ID 446 NO-TAB-STOP 
     edObjTh AT ROW 2.14 COL 6.8 NO-LABEL WIDGET-ID 448
     edObj AT ROW 2.24 COL 114.8 NO-LABEL WIDGET-ID 434
     bEditObj AT ROW 3.33 COL 110 WIDGET-ID 184 NO-TAB-STOP 
     fNoSentTh AT ROW 5.67 COL 23.2 COLON-ALIGNED NO-LABEL WIDGET-ID 462
     fCreateDtTh AT ROW 5.67 COL 23.2 COLON-ALIGNED NO-LABEL WIDGET-ID 456
     fNoSentO AT ROW 5.76 COL 131.6 COLON-ALIGNED NO-LABEL WIDGET-ID 464
     bEditObjTh AT ROW 3.24 COL 2 WIDGET-ID 442 NO-TAB-STOP 
     fCreateDtO AT ROW 5.76 COL 131.6 COLON-ALIGNED NO-LABEL WIDGET-ID 326
     brwEvent AT ROW 11 COL 6.8 WIDGET-ID 2500
     bSnmtOurObj AT ROW 5.67 COL 128.6 WIDGET-ID 452 NO-TAB-STOP 
     bSnmtThObj AT ROW 5.57 COL 20.2 WIDGET-ID 454 NO-TAB-STOP 
     bNewObj AT ROW 2.19 COL 110 WIDGET-ID 186 NO-TAB-STOP 
     bNewObjTh AT ROW 2.1 COL 2 WIDGET-ID 440 NO-TAB-STOP 
     bEventRefresh AT ROW 10.95 COL 2 WIDGET-ID 376 NO-TAB-STOP 
     bCollapse AT ROW 7.76 COL 97.8 WIDGET-ID 392 NO-TAB-STOP 
     bExpand AT ROW 7.76 COL 108 WIDGET-ID 396 NO-TAB-STOP 
     bToday AT ROW 7.76 COL 87.4 WIDGET-ID 406 NO-TAB-STOP 
     bEventExport AT ROW 12.1 COL 2 WIDGET-ID 432 NO-TAB-STOP 
     bBwdNinety AT ROW 8.91 COL 72.6 WIDGET-ID 388 NO-TAB-STOP 
     bEventAf AT ROW 8.91 COL 133.6 WIDGET-ID 428 NO-TAB-STOP 
     bEventBf AT ROW 8.91 COL 65.2 WIDGET-ID 430 NO-TAB-STOP 
     bTimeLine AT ROW 8.91 COL 87.4 WIDGET-ID 426 NO-TAB-STOP 
     bBwdThirty AT ROW 8.91 COL 80 WIDGET-ID 390 NO-TAB-STOP 
     bFwdNinety AT ROW 8.91 COL 126.2 WIDGET-ID 398 NO-TAB-STOP 
     bFwdThirty AT ROW 8.91 COL 118.8 WIDGET-ID 400 NO-TAB-STOP 
     bEditEvent AT ROW 14.38 COL 2 WIDGET-ID 394 NO-TAB-STOP 
     bNewEvent AT ROW 13.24 COL 2 WIDGET-ID 404 NO-TAB-STOP 
     "My Sentiment:" VIEW-AS TEXT
          SIZE 13.6 BY .62 AT ROW 5.86 COL 115.2 WIDGET-ID 458
     "My Sentiment:" VIEW-AS TEXT
          SIZE 13.6 BY .62 AT ROW 5.76 COL 6.8 WIDGET-ID 460
     "Our Objective" VIEW-AS TEXT
          SIZE 13.2 BY .62 AT ROW 1.57 COL 115 WIDGET-ID 436
     "Their Objective" VIEW-AS TEXT
          SIZE 15 BY .62 AT ROW 1.48 COL 6.8 WIDGET-ID 450
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "Timeline" WIDGET-ID 2200.

DEFINE FRAME frDetail
     brwTags AT ROW 7.91 COL 150 WIDGET-ID 2000
     flAgentName AT ROW 1.86 COL 20.4 COLON-ALIGNED WIDGET-ID 364
     flLAgentName AT ROW 2.95 COL 20.4 COLON-ALIGNED WIDGET-ID 366
     flAddr1 AT ROW 4.05 COL 20.4 COLON-ALIGNED WIDGET-ID 6
     flAddr2 AT ROW 5.14 COL 20.4 COLON-ALIGNED NO-LABEL WIDGET-ID 8
     bDelTag AT ROW 11.14 COL 145 WIDGET-ID 208 NO-TAB-STOP 
     flCity AT ROW 6.24 COL 20.4 COLON-ALIGNED NO-LABEL WIDGET-ID 10
     cbState AT ROW 6.24 COL 38.8 COLON-ALIGNED NO-LABEL WIDGET-ID 150
     flZip AT ROW 6.24 COL 57.4 COLON-ALIGNED NO-LABEL WIDGET-ID 148
     flPhone AT ROW 1.86 COL 88.8 COLON-ALIGNED WIDGET-ID 142
     flFax AT ROW 1.86 COL 116.6 COLON-ALIGNED WIDGET-ID 140
     flEmail AT ROW 2.95 COL 88.8 COLON-ALIGNED WIDGET-ID 20
     flWebsite AT ROW 4.05 COL 88.8 COLON-ALIGNED WIDGET-ID 22
     bEditTag AT ROW 10.05 COL 145 WIDGET-ID 272 NO-TAB-STOP 
     cbSwVendor AT ROW 5.14 COL 88.8 COLON-ALIGNED WIDGET-ID 240
     fDomain AT ROW 6.24 COL 88.8 COLON-ALIGNED WIDGET-ID 384
     bNewTag AT ROW 8.95 COL 145 WIDGET-ID 274 NO-TAB-STOP 
     cbRegion AT ROW 8.38 COL 49.4 COLON-ALIGNED WIDGET-ID 382
     cbContractID AT ROW 9.57 COL 20.4 COLON-ALIGNED WIDGET-ID 252
     rdRemitType AT ROW 9.81 COL 51.4 NO-LABEL WIDGET-ID 152
     flContractDate AT ROW 10.76 COL 20.4 COLON-ALIGNED WIDGET-ID 36
     flLiabilityLimit AT ROW 11.95 COL 20.4 COLON-ALIGNED WIDGET-ID 40
     flRemitValue AT ROW 11.95 COL 49.4 COLON-ALIGNED WIDGET-ID 38
     flRemitAlert AT ROW 13.14 COL 20.4 COLON-ALIGNED WIDGET-ID 158
     flPolicyLimit AT ROW 13.14 COL 49.4 COLON-ALIGNED WIDGET-ID 380
     bEditManager AT ROW 20.43 COL 133.4 WIDGET-ID 374 NO-TAB-STOP 
     flProspectDate AT ROW 9.57 COL 88.8 COLON-ALIGNED WIDGET-ID 48
     flActiveDate AT ROW 9.57 COL 121.6 COLON-ALIGNED WIDGET-ID 50
     btnAgentMail AT ROW 2.86 COL 138.2 WIDGET-ID 378 NO-TAB-STOP 
     flWithdrawnDate AT ROW 10.76 COL 88.8 COLON-ALIGNED WIDGET-ID 246
     flClosedDate AT ROW 10.76 COL 121.6 COLON-ALIGNED WIDGET-ID 52
     flReviewDate AT ROW 11.95 COL 88.8 COLON-ALIGNED WIDGET-ID 46
     flCancelDate AT ROW 11.95 COL 121.6 COLON-ALIGNED WIDGET-ID 54
     btnSameAsAbove AT ROW 15.62 COL 35.8 WIDGET-ID 188
     flMName AT ROW 16.95 COL 20.4 COLON-ALIGNED WIDGET-ID 172
     flMAddr1 AT ROW 18.14 COL 20.4 COLON-ALIGNED WIDGET-ID 166
     flMAddr2 AT ROW 19.33 COL 20.4 COLON-ALIGNED NO-LABEL WIDGET-ID 168
     flMCity AT ROW 20.52 COL 20.4 COLON-ALIGNED NO-LABEL WIDGET-ID 170
     cbMState AT ROW 20.52 COL 39.4 COLON-ALIGNED NO-LABEL WIDGET-ID 174
     flMZip AT ROW 20.52 COL 57.4 COLON-ALIGNED NO-LABEL WIDGET-ID 176
     flONPN AT ROW 15.76 COL 88.8 COLON-ALIGNED WIDGET-ID 214
     flOAltaUID AT ROW 16.95 COL 88.8 COLON-ALIGNED WIDGET-ID 186
     flOStateUID AT ROW 18.14 COL 88.8 COLON-ALIGNED WIDGET-ID 210
     flOCorporation AT ROW 19.33 COL 88.8 COLON-ALIGNED WIDGET-ID 212
     flOManager AT ROW 20.52 COL 88.8 COLON-ALIGNED WIDGET-ID 372
     cbStateOper AT ROW 8.38 COL 20.4 COLON-ALIGNED WIDGET-ID 104 NO-TAB-STOP 
     bAddImg AT ROW 1.86 COL 145 WIDGET-ID 284 NO-TAB-STOP 
     bAgentRefresh AT ROW 2 COL 2.8 WIDGET-ID 368 NO-TAB-STOP 
     bTagRefresh AT ROW 7.86 COL 145 WIDGET-ID 370 NO-TAB-STOP 
     BtnUrl AT ROW 4.05 COL 138.2 WIDGET-ID 92 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3 WIDGET-ID 900.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME frDetail
     bCancel AT ROW 4.29 COL 2.8 WIDGET-ID 216 NO-TAB-STOP 
     bSave AT ROW 3.14 COL 2.8 WIDGET-ID 224 NO-TAB-STOP 
     bdeleteImag AT ROW 3.05 COL 145 WIDGET-ID 324 NO-TAB-STOP 
     "Mailing Address" VIEW-AS TEXT
          SIZE 18 BY .62 AT ROW 14.71 COL 12.2 WIDGET-ID 180
          FONT 6
     "Other" VIEW-AS TEXT
          SIZE 6.8 BY .62 AT ROW 14.71 COL 79 WIDGET-ID 184
          FONT 6
     "Remit:" VIEW-AS TEXT
          SIZE 6.2 BY .62 AT ROW 10.43 COL 44.8 WIDGET-ID 156
     "Activity" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 7.57 COL 79 WIDGET-ID 138
          FONT 6
     "Operating Agreement" VIEW-AS TEXT
          SIZE 24.6 BY .62 AT ROW 7.62 COL 12.2 WIDGET-ID 78
          FONT 6
     RECT-4 AT ROW 7.91 COL 11 WIDGET-ID 74
     RECT-5 AT ROW 7.91 COL 77.4 WIDGET-ID 136
     RECT-3 AT ROW 15.05 COL 11 WIDGET-ID 178
     RECT-6 AT ROW 15.05 COL 77.4 WIDGET-ID 182
     RECT-34 AT ROW 1.86 COL 2.2 WIDGET-ID 248
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 2.8 ROW 6.1
         SIZE 201 BY 22.3
         TITLE "Details" WIDGET-ID 900.

DEFINE FRAME frLogo
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 150 ROW 1.86
         SIZE 50.8 BY 5.38 WIDGET-ID 300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Container Links: Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source
   Design Page: 9
   Other Settings: APPSERVER
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent"
         HEIGHT             = 28
         WIDTH              = 205.4
         MAX-HEIGHT         = 47.19
         MAX-WIDTH          = 296
         VIRTUAL-HEIGHT     = 47.19
         VIRTUAL-WIDTH      = 296
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-wWin:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB wWin 
/* ************************* Included-Libraries *********************** */

{src/adm2/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME frActivity:FRAME = FRAME fMain:HANDLE
       FRAME frAlert:FRAME = FRAME fMain:HANDLE
       FRAME frAr:FRAME = FRAME fMain:HANDLE
       FRAME frAudit:FRAME = FRAME fMain:HANDLE
       FRAME frBatch:FRAME = FRAME fMain:HANDLE
       FRAME frClaim:FRAME = FRAME fMain:HANDLE
       FRAME frCompliance:FRAME = FRAME fMain:HANDLE
       FRAME frDetail:FRAME = FRAME fMain:HANDLE
       FRAME frEvent:FRAME = FRAME fMain:HANDLE
       FRAME frLogo:FRAME = FRAME frDetail:HANDLE
       FRAME frpersonagent:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
ASSIGN 
       fStatus:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       hdAgentID:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       hdAgentName:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       hdLAgentName:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FRAME frActivity
                                                                        */
/* BROWSE-TAB brwActivityPolicies 1 frActivity */
/* BROWSE-TAB brwActivityCPL brwActivityPolicies frActivity */
/* SETTINGS FOR BUTTON bActivityExp IN FRAME frActivity
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bActivityExpCPL IN FRAME frActivity
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bActivityMetricsRefresh IN FRAME frActivity
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bActivityMetricsRefreshCPL IN FRAME frActivity
   NO-ENABLE                                                            */
/* SETTINGS FOR BROWSE brwActivityCPL IN FRAME frActivity
   NO-ENABLE                                                            */
ASSIGN 
       brwActivityCPL:COLUMN-RESIZABLE IN FRAME frActivity       = TRUE
       brwActivityCPL:COLUMN-MOVABLE IN FRAME frActivity         = TRUE.

/* SETTINGS FOR BROWSE brwActivityPolicies IN FRAME frActivity
   NO-ENABLE                                                            */
ASSIGN 
       brwActivityPolicies:COLUMN-RESIZABLE IN FRAME frActivity       = TRUE
       brwActivityPolicies:COLUMN-MOVABLE IN FRAME frActivity         = TRUE.

/* SETTINGS FOR FRAME frAlert
                                                                        */
/* BROWSE-TAB brwAlerts fOwner frAlert */
ASSIGN 
       FRAME frAlert:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bExport IN FRAME frAlert
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bModify IN FRAME frAlert
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bNewNote IN FRAME frAlert
   NO-ENABLE                                                            */
ASSIGN 
       brwAlerts:ALLOW-COLUMN-SEARCHING IN FRAME frAlert = TRUE
       brwAlerts:COLUMN-RESIZABLE IN FRAME frAlert       = TRUE.

/* SETTINGS FOR COMBO-BOX fCode IN FRAME frAlert
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX fOwner IN FRAME frAlert
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frAr
   Custom                                                               */
/* BROWSE-TAB brwArAging btFileOpen frAr */
ASSIGN 
       brwArAging:ALLOW-COLUMN-SEARCHING IN FRAME frAr = TRUE
       brwArAging:COLUMN-RESIZABLE IN FRAME frAr       = TRUE
       brwArAging:COLUMN-MOVABLE IN FRAME frAr         = TRUE.

/* SETTINGS FOR BUTTON btArMail IN FRAME frAr
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btFileExport IN FRAME frAr
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btFileOpen IN FRAME frAr
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btRefreshAr IN FRAME frAr
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frAudit
                                                                        */
/* BROWSE-TAB brwQAR 1 frAudit */
/* BROWSE-TAB brwAction brwQAR frAudit */
ASSIGN 
       brwAction:POPUP-MENU IN FRAME frAudit             = MENU POPUP-MENU-brwQAR-2:HANDLE
       brwAction:ALLOW-COLUMN-SEARCHING IN FRAME frAudit = TRUE
       brwAction:COLUMN-RESIZABLE IN FRAME frAudit       = TRUE.

ASSIGN 
       brwQAR:POPUP-MENU IN FRAME frAudit             = MENU POPUP-MENU-brwQAR:HANDLE
       brwQAR:ALLOW-COLUMN-SEARCHING IN FRAME frAudit = TRUE
       brwQAR:COLUMN-RESIZABLE IN FRAME frAudit       = TRUE.

/* SETTINGS FOR BUTTON btActionExport IN FRAME frAudit
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btActionMail IN FRAME frAudit
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btActionOpen IN FRAME frAudit
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btActionRefresh IN FRAME frAudit
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btAuditExport IN FRAME frAudit
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btAuditMail IN FRAME frAudit
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btAuditOpen IN FRAME frAudit
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btAuditRefresh IN FRAME frAudit
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frBatch
   NOT-VISIBLE                                                          */
/* BROWSE-TAB brwActivityBatch 1 frBatch */
/* BROWSE-TAB brwBatch brwActivityBatch frBatch */
ASSIGN 
       FRAME frBatch:HIDDEN           = TRUE
       FRAME frBatch:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bActivityBatchExport IN FRAME frBatch
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bActivityBatchRefresh IN FRAME frBatch
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBatchExport IN FRAME frBatch
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBatchOpen IN FRAME frBatch
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBatchRefresh IN FRAME frBatch
   NO-ENABLE                                                            */
/* SETTINGS FOR BROWSE brwActivityBatch IN FRAME frBatch
   NO-ENABLE                                                            */
ASSIGN 
       brwActivityBatch:COLUMN-RESIZABLE IN FRAME frBatch       = TRUE
       brwActivityBatch:COLUMN-MOVABLE IN FRAME frBatch         = TRUE.

ASSIGN 
       brwBatch:COLUMN-RESIZABLE IN FRAME frBatch       = TRUE
       brwBatch:COLUMN-MOVABLE IN FRAME frBatch         = TRUE.

/* SETTINGS FOR FRAME frClaim
                                                                        */
/* BROWSE-TAB brwClaims errorfrom12 frClaim */
ASSIGN 
       brwClaims:ALLOW-COLUMN-SEARCHING IN FRAME frClaim = TRUE
       brwClaims:COLUMN-MOVABLE IN FRAME frClaim         = TRUE.

/* SETTINGS FOR BUTTON btClaimExport IN FRAME frClaim
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btClaimMail IN FRAME frClaim
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btClaimOpen IN FRAME frClaim
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btClaimRefresh IN FRAME frClaim
   NO-ENABLE                                                            */
ASSIGN 
       costfrom12:READ-ONLY IN FRAME frClaim        = TRUE
       costfrom12:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       costfrom24:READ-ONLY IN FRAME frClaim        = TRUE
       costfrom24:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       costfrom36:READ-ONLY IN FRAME frClaim        = TRUE
       costfrom36:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       costfromLTD:READ-ONLY IN FRAME frClaim        = TRUE
       costfromLTD:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       errorfrom12:READ-ONLY IN FRAME frClaim        = TRUE
       errorfrom12:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       errorfrom24:READ-ONLY IN FRAME frClaim        = TRUE
       errorfrom24:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       errorfromLTD:READ-ONLY IN FRAME frClaim        = TRUE
       errorfromLTD:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       errortfrom36:READ-ONLY IN FRAME frClaim        = TRUE
       errortfrom36:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       fLAEL:READ-ONLY IN FRAME frClaim        = TRUE
       fLAEL:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       fLAER:READ-ONLY IN FRAME frClaim        = TRUE
       fLAER:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       fLossL:READ-ONLY IN FRAME frClaim        = TRUE
       fLossL:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       fLossR:READ-ONLY IN FRAME frClaim        = TRUE
       fLossR:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       fRecoveriesR:READ-ONLY IN FRAME frClaim        = TRUE
       fRecoveriesR:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       fTotalCostL:READ-ONLY IN FRAME frClaim        = TRUE
       fTotalCostL:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       netfrom12:READ-ONLY IN FRAME frClaim        = TRUE
       netfrom12:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       netfrom24:READ-ONLY IN FRAME frClaim        = TRUE
       netfrom24:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

ASSIGN 
       netfrom36:READ-ONLY IN FRAME frClaim        = TRUE
       netfrom36:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

/* SETTINGS FOR FILL-IN netfromLTD IN FRAME frClaim
   ALIGN-R                                                              */
ASSIGN 
       netfromLTD:READ-ONLY IN FRAME frClaim        = TRUE
       netfromLTD:PRIVATE-DATA IN FRAME frClaim     = 
                "READ-ONLY".

/* SETTINGS FOR FRAME frCompliance
   NOT-VISIBLE                                                          */
/* BROWSE-TAB brwQualification tShowInactiveQuals frCompliance */
/* BROWSE-TAB brwReqFul brwQualification frCompliance */
ASSIGN 
       FRAME frCompliance:HIDDEN           = TRUE
       FRAME frCompliance:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bReqFulExport IN FRAME frCompliance
   NO-ENABLE                                                            */
ASSIGN 
       brwQualification:ALLOW-COLUMN-SEARCHING IN FRAME frCompliance = TRUE
       brwQualification:COLUMN-RESIZABLE IN FRAME frCompliance       = TRUE.

ASSIGN 
       brwReqFul:ALLOW-COLUMN-SEARCHING IN FRAME frCompliance = TRUE
       brwReqFul:COLUMN-RESIZABLE IN FRAME frCompliance       = TRUE.

/* SETTINGS FOR BUTTON btComQualExp IN FRAME frCompliance
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btComQualRefresh IN FRAME frCompliance
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btComReqFulRefresh IN FRAME frCompliance
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btComViewQual IN FRAME frCompliance
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btComViewReq IN FRAME frCompliance
   NO-ENABLE                                                            */
ASSIGN 
       tOrganization:READ-ONLY IN FRAME frCompliance        = TRUE.

ASSIGN 
       tOrgName:READ-ONLY IN FRAME frCompliance        = TRUE.

/* SETTINGS FOR TOGGLE-BOX tShowInactiveQuals IN FRAME frCompliance
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frDetail
   Custom                                                               */
/* BROWSE-TAB brwTags 1 frDetail */
/* SETTINGS FOR BUTTON bDelTag IN FRAME frDetail
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bEditManager IN FRAME frDetail
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bEditTag IN FRAME frDetail
   NO-ENABLE                                                            */
ASSIGN 
       brwTags:ALLOW-COLUMN-SEARCHING IN FRAME frDetail = TRUE
       brwTags:COLUMN-RESIZABLE IN FRAME frDetail       = TRUE
       brwTags:COLUMN-MOVABLE IN FRAME frDetail         = TRUE.

ASSIGN 
       cbStateOper:PRIVATE-DATA IN FRAME frDetail     = 
                "Read-Only".

/* SETTINGS FOR FILL-IN flOManager IN FRAME frDetail
   NO-ENABLE                                                            */
ASSIGN 
       flOManager:READ-ONLY IN FRAME frDetail        = TRUE.

/* SETTINGS FOR FRAME frEvent
                                                                        */
/* BROWSE-TAB brwEvent fCreateDtO frEvent */
ASSIGN 
       FRAME frEvent:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bCancelObj IN FRAME frEvent
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bCancelObjTh IN FRAME frEvent
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bEditObj IN FRAME frEvent
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bEditObjTh IN FRAME frEvent
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bEventExport IN FRAME frEvent
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bEventRefresh IN FRAME frEvent
   NO-ENABLE                                                            */
ASSIGN 
       brwEvent:ALLOW-COLUMN-SEARCHING IN FRAME frEvent = TRUE
       brwEvent:COLUMN-RESIZABLE IN FRAME frEvent       = TRUE.

/* SETTINGS FOR BUTTON bSnmtOurObj IN FRAME frEvent
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bSnmtThObj IN FRAME frEvent
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR edObj IN FRAME frEvent
   NO-ENABLE                                                            */
ASSIGN 
       edObj:READ-ONLY IN FRAME frEvent        = TRUE.

/* SETTINGS FOR EDITOR edObjTh IN FRAME frEvent
   NO-ENABLE                                                            */
ASSIGN 
       edObjTh:READ-ONLY IN FRAME frEvent        = TRUE.

/* SETTINGS FOR FILL-IN fCreateDtO IN FRAME frEvent
   NO-ENABLE                                                            */
ASSIGN 
       fCreateDtO:READ-ONLY IN FRAME frEvent        = TRUE.

/* SETTINGS FOR FILL-IN fCreateDtTh IN FRAME frEvent
   NO-ENABLE                                                            */
ASSIGN 
       fCreateDtTh:READ-ONLY IN FRAME frEvent        = TRUE.

/* SETTINGS FOR FILL-IN fNoSentO IN FRAME frEvent
   NO-ENABLE                                                            */
ASSIGN 
       fNoSentO:READ-ONLY IN FRAME frEvent        = TRUE.

/* SETTINGS FOR FILL-IN fNoSentTh IN FRAME frEvent
   NO-ENABLE                                                            */
ASSIGN 
       fNoSentTh:READ-ONLY IN FRAME frEvent        = TRUE.

/* SETTINGS FOR FRAME frLogo
   NOT-VISIBLE                                                          */
ASSIGN 
       FRAME frLogo:SENSITIVE        = FALSE.

/* SETTINGS FOR FRAME frpersonagent
   NOT-VISIBLE                                                          */
/* BROWSE-TAB brwpersonagent AffType frpersonagent */
ASSIGN 
       FRAME frpersonagent:HIDDEN           = TRUE
       FRAME frpersonagent:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bPersonAgentDeactivate IN FRAME frpersonagent
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPersonAgentExport IN FRAME frpersonagent
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPersonAgentUpdate IN FRAME frpersonagent
   NO-ENABLE                                                            */
ASSIGN 
       brwpersonagent:ALLOW-COLUMN-SEARCHING IN FRAME frpersonagent = TRUE
       brwpersonagent:COLUMN-RESIZABLE IN FRAME frpersonagent       = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAction
/* Query rebuild information for BROWSE brwAction
     _START_FREEFORM
open query {&SELF-NAME} for each action
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAction */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwActivityBatch
/* Query rebuild information for BROWSE brwActivityBatch
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR each tActivity.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwActivityBatch */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwActivityCPL
/* Query rebuild information for BROWSE brwActivityCPL
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tMetricsActivity.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwActivityCPL */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwActivityPolicies
/* Query rebuild information for BROWSE brwActivityPolicies
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tMetricsActivity.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwActivityPolicies */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAlerts
/* Query rebuild information for BROWSE brwAlerts
     _START_FREEFORM
open query {&SELF-NAME} for each alert.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAlerts */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwArAging
/* Query rebuild information for BROWSE brwArAging
     _START_FREEFORM
open query {&SELF-NAME} for each tArAging.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwArAging */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwBatch
/* Query rebuild information for BROWSE brwBatch
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH batch.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwBatch */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwClaims
/* Query rebuild information for BROWSE brwClaims
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH agentclaim.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwClaims */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwEvent
/* Query rebuild information for BROWSE brwEvent
     _START_FREEFORM
open query {&SELF-NAME} for each tlEvent by tlEvent.date.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwEvent */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwpersonagent
/* Query rebuild information for BROWSE brwpersonagent
     _START_FREEFORM
open query {&SELF-NAME} for each tpersonagent by tpersonagent.active desc by tpersonagent.personName.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwpersonagent */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQAR
/* Query rebuild information for BROWSE brwQAR
     _START_FREEFORM
open query {&SELF-NAME} for each audit no-lock by audit.auditStartDate desc .
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQAR */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQualification
/* Query rebuild information for BROWSE brwQualification
     _START_FREEFORM
open query brwQualification for each qualification  by Qualification.qualificationId.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQualification */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwReqFul
/* Query rebuild information for BROWSE brwReqFul
     _START_FREEFORM
open query {&SELF-NAME} for each reqfulfill.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwReqFul */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwTags
/* Query rebuild information for BROWSE brwTags
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tag.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwTags */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frAlert
/* Query rebuild information for FRAME frAlert
     _Query            is NOT OPENED
*/  /* FRAME frAlert */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frBatch
/* Query rebuild information for FRAME frBatch
     _Query            is NOT OPENED
*/  /* FRAME frBatch */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frCompliance
/* Query rebuild information for FRAME frCompliance
     _Query            is NOT OPENED
*/  /* FRAME frCompliance */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frDetail
/* Query rebuild information for FRAME frDetail
     _Query            is NOT OPENED
*/  /* FRAME frDetail */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frEvent
/* Query rebuild information for FRAME frEvent
     _Query            is NOT OPENED
*/  /* FRAME frEvent */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frLogo
/* Query rebuild information for FRAME frLogo
     _Query            is NOT OPENED
*/  /* FRAME frLogo */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frpersonagent
/* Query rebuild information for FRAME frpersonagent
     _Query            is NOT OPENED
*/  /* FRAME frpersonagent */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin /* Agent */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin /* Agent */
DO:
  run exitObject in this-procedure.
  
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
/*   APPLY "CLOSE":U TO THIS-PROCEDURE. */
/*   RETURN NO-APPLY.                   */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-RESIZED OF wWin /* Agent */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frpersonagent
&Scoped-define SELF-NAME AffType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL AffType wWin
ON VALUE-CHANGED OF AffType IN FRAME frpersonagent
DO:
  empty temp-table tpersonagent. 
  if AffType:input-value = "A" 
   then
    do:
      for each personagent where personagent.active = true :
        create tpersonagent. 
        buffer-copy personagent to tpersonagent.
      end.
     end.
   else if AffType:input-value = "I" 
    then
     do:
       for each personagent where personagent.active = false :
         create tpersonagent. 
         buffer-copy personagent to tpersonagent.
       end.
     end.
   else
    do:
      for each personagent:
        create tpersonagent. 
        buffer-copy personagent to tpersonagent.
      end.
    end.
    
  open query brwpersonagent for each tpersonagent by tpersonagent.active desc by tpersonagent.personName.
  apply "value-changed" to brwpersonagent. 
  if query brwPersonAgent:num-results = 0
   then
    assign
        bpersonagentUpdate:sensitive in frame frpersonagent = false
        bpersonagentdeactivate:sensitive in frame frpersonagent = false
        bpersonagentExport:sensitive in frame frpersonagent = false
        btnAgentPersonMail:sensitive in frame frpersonagent = false
        bpersonagentlookup:sensitive in frame frpersonagent = false.
  else
   assign
       bpersonagentUpdate:sensitive in frame frpersonagent = true
       bpersonagentdeactivate:sensitive in frame frpersonagent = true
       bpersonagentExport:sensitive in frame frpersonagent = true
       bpersonagentlookup:sensitive in frame frpersonagent = true
       btnAgentPersonMail:sensitive in frame frpersonagent = if tpersonagent.email = '' or tpersonagent.doNotCall then false  else true
       .
      
   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAr
&Scoped-define SELF-NAME b30to60
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL b30to60 wWin
ON CHOOSE OF b30to60 IN FRAME frAr
DO:
  if not available arAging 
   then
    return.
  cLastAgingClicked = "30to60".
  lArAging = true.
  if araging.due31_60 = 0 
   then
    do:
      run filterArAging in this-procedure(input cLastAgingClicked).
      return.
    end.
  run getAr in this-procedure(input cLastAgingClicked).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME b60to90
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL b60to90 wWin
ON CHOOSE OF b60to90 IN FRAME frAr
DO:
  if not available arAging 
   then
    return.
  cLastAgingClicked = "60to90".
  if araging.due61_90 = 0 
   then
    do:
      run filterArAging in this-procedure(input "60to90").
      return.
    end.
  run getAr in this-procedure(input "60to90").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBatch
&Scoped-define SELF-NAME bActivityBatchExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActivityBatchExport wWin
ON CHOOSE OF bActivityBatchExport IN FRAME frBatch /* Export */
do:
  run exportData in this-procedure (input browse brwActivityBatch:handle, /* Borwser handle */
                                    input temp-table tActivity:handle,    /* Temp-table handle */
                                    input "tActivity",                    /* Table name */
                                    input "BatchActivities",              /* CSV filename */
                                    input "for each tActivity by tActivity.year by tActivity.seq").          /* Query */  
                                
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bActivityBatchRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActivityBatchRefresh wWin
ON CHOOSE OF bActivityBatchRefresh IN FRAME frBatch /* Refresh */
DO:
  find first agent no-error.
  if available agent
   then
    do:
      empty temp-table tActivity.
      empty temp-table activity.
      empty temp-table ttActivity.
      std-in = year(today).
      openActivity(input std-in, input "N" , true).
      for each activity:
        create tActivity.
        buffer-copy activity to tActivity.
      end.
      
      iMonth = month(today).
  
      hColumn = brwActivityBatch:get-browse-column(15).
      
      if valid-handle(hColumn)
       then
        assign
            hColumn:label  = "YTD " + getMonthName(iMonth).
            .  
      open query brwActivityBatch for each tActivity by tActivity.year by tActivity.seq.
              
      lBatches = yes.
    end. /* if available agent */                                              
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frActivity
&Scoped-define SELF-NAME bActivityExp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActivityExp wWin
ON CHOOSE OF bActivityExp IN FRAME frActivity /* Export */
DO:
  run exportData in this-procedure (input browse brwActivityPolicies:handle,     /* Borwser handle */
                                    input temp-table tMetricsActivity:handle,    /* Temp-table handle */
                                    input "tMetricsActivity",                    /* Table name */
                                    input "MetricsActivityPolicies",                    /* CSV filename */
                                    input "for each tMetricsActivity where tMetricsActivity.category = 'P' ").          /* Query */  
                                
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bActivityExpCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActivityExpCPL wWin
ON CHOOSE OF bActivityExpCPL IN FRAME frActivity /* Export */
DO:
  run exportData in this-procedure (input browse brwActivityPolicies:handle,     /* Borwser handle */
                                    input temp-table tMetricsActivity:handle,    /* Temp-table handle */
                                    input "tMetricsActivity",                    /* Table name */
                                    input "MetricsActivityCPL",                    /* CSV filename */
                                    input "for each tMetricsActivity where tMetricsActivity.category = 'T' ").          /* Query */  
                                
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bActivityMetricsRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActivityMetricsRefresh wWin
ON CHOOSE OF bActivityMetricsRefresh IN FRAME frActivity /* Refresh */
DO:
  find first agent no-error.
  if available agent
   then
    do:
      run getAgentActivity in this-procedure(input true).
      iMonth = month(today).
      
      hColumn = brwActivityPolicies:get-browse-column(16).
          
      if valid-handle(hColumn)
       then
        hColumn:label  = "YTD " + getMonthName(iMonth).
        
      hColumn = brwActivityCPL:get-browse-column(16).
          
      if valid-handle(hColumn)
       then
        hColumn:label  = "YTD " + getMonthName(iMonth).      
          
      lActivity = yes.
    end. /* if available agent */                                              
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bActivityMetricsRefreshCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActivityMetricsRefreshCPL wWin
ON CHOOSE OF bActivityMetricsRefreshCPL IN FRAME frActivity /* Refresh */
DO:
  apply 'choose' to bActivityMetricsRefresh.                                             
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frDetail
&Scoped-define SELF-NAME bAddImg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddImg wWin
ON CHOOSE OF bAddImg IN FRAME frDetail /* Add Logo */
do:
  run addImage in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bAgentJournals
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentJournals wWin
ON CHOOSE OF bAgentJournals IN FRAME fMain /* Agent Journals */
DO:
  if not available agent
   then return.
   
  run ActionAgentJournals in this-procedure (agent.agentID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frDetail
&Scoped-define SELF-NAME bAgentRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentRefresh wWin
ON CHOOSE OF bAgentRefresh IN FRAME frDetail /* Refresh */
DO:
  run agentDataChanged in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bARInfo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bARInfo wWin
ON CHOOSE OF bARInfo IN FRAME fMain /* AR Info */
do:   
  run dialogAgentArInfo.w(input-output cMethod,
                          input-output iDueInDays,
                          input-output iDueOnDay,
                          input-output cCashAcc,
                          input-output cCashAccDesc,
                          input-output cARAcc,
                          input-output cARAccDesc,
                          input-output cRefARAcc,
                          input-output cRefARAccDesc,
                          input-output cWroffARAcc,
                          input-output cWroffARAccDesc,
                          output std-lo).  
  if not std-lo
   then
    return no-apply.
  
  run updateARAccounts in hFileDataSrv (hdAgentID:screen-value in frame {&frame-name}, 
                                        input cMethod,
                                        input iDueInDays,
                                        input iDueOnDay,
                                        input cCashAcc,
                                        input cCashAccDesc,
                                        input cARAcc,
                                        input cARAccDesc,
                                        input cRefARAcc,
                                        input cRefARAccDesc,
                                        input cWroffARAcc,
                                        input cWroffARAccDesc,
                                        output std-lo).
  if std-lo
   then
    do:
      dataChanged = false.      
      doModify(false).
      run ShowWindow in this-procedure.
    end.   
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAr
&Scoped-define SELF-NAME bbalance
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bbalance wWin
ON CHOOSE OF bbalance IN FRAME frAr
DO:
  if not available arAging 
   then
    return.
  cLastAgingClicked = "balance".
  lArAging = true.
  if araging.balance = 0 
   then
    do:
      run filterArAging in this-procedure(input cLastAgingClicked).
      return.
    end.
  run getAr in this-procedure (input cLastAgingClicked).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBatch
&Scoped-define SELF-NAME bBatchExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBatchExport wWin
ON CHOOSE OF bBatchExport IN FRAME frBatch /* Export */
DO:
  for each batch no-lock:
    create tBatch.
    buffer-copy batch to tBatch.
    tBatch.stat =  getBatchStatus(batch.stat).
  end.
  run exportData in this-procedure (input browse brwBatch:handle, /* Borwser handle */
                                    input temp-table tBatch:handle,    /* Temp-table handle */
                                    input "tBatch",                    /* Table name */
                                    input "Batches",              /* CSV filename */
                                    input "for each tBatch").          /* Query */  
        
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBatchOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBatchOpen wWin
ON CHOOSE OF bBatchOpen IN FRAME frBatch /* Open */
DO:
  run openBatchDetails in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBatchRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBatchRefresh wWin
ON CHOOSE OF bBatchRefresh IN FRAME frBatch /* Refresh */
DO:
  find first agent no-error.
  if not available agent
   then
    return.

  if cLastRemitClicked = ""
   then
    cLastRemitClicked = string(year(today)). 
  run refreshAgentBatches in hFileDataSrv.
  openBatchesYearly(input cLastRemitClicked).
  
  brwBatch:title = "Batches for " + cLastRemitClicked.
  run enableButtons in this-procedure.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frEvent
&Scoped-define SELF-NAME bBwdNinety
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBwdNinety wWin
ON CHOOSE OF bBwdNinety IN FRAME frEvent /* Backward90 */
do:
  assign 
      dEndDate   = dstartDate - 1
      dstartDate = dEndDate - 90
      .
     
  run showTimelineEvents in this-procedure(input dstartDate,
                                           input dEndDate
                                          )
                                          .
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBwdThirty
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBwdThirty wWin
ON CHOOSE OF bBwdThirty IN FRAME frEvent /* Backward30 */
do:
  assign 
      dEndDate   = dstartDate - 1
      dstartDate = dEndDate - 30
      .
     
   run showTimelineEvents in this-procedure(input dstartDate,
                                            input dEndDate
                                           ).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frDetail
&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel wWin
ON CHOOSE OF bCancel IN FRAME frDetail /* Cancel */
do:
  displayDetail().
  saveStateDisable(activePageNumber).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frEvent
&Scoped-define SELF-NAME bCancelObj
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancelObj wWin
ON CHOOSE OF bCancelObj IN FRAME frEvent /* Cancel */
DO:
  message "The Objective will be marked as Cancelled. Continue?" view-as alert-box warning buttons yes-no update std-lo.
  if not std-lo
   then
    return.
    
  find first objective where objective.stat = {&ActiveStat} and 
                             objective.type = 'O' no-error.
  if  available objective
   then
    do:
      empty temp-table tempObjective.
      create tempObjective.
      buffer-copy objective except stat to tempObjective.
      tempObjective.stat = 'X'.
      
      run modifyObjective in hFileDataSrv (input table tempObjective,
                                             output std-lo). 
      if not std-lo
       then
        return no-apply.

      find first objective where objective.objectiveID = tempObjective.objectiveID no-error.
      if available objective
       then
        objective.stat = 'X'.
      
       run displayObjWidgets in this-procedure.

    end.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancelObjTh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancelObjTh wWin
ON CHOOSE OF bCancelObjTh IN FRAME frEvent /* Cancel */
DO:
  message "The Objective will be marked as Cancelled. Continue?" view-as alert-box warning buttons yes-no update std-lo.
  if not std-lo
   then
    return.
    
  find first objective where objective.stat = {&ActiveStat} and 
                             objective.type = 'T' no-error.
  if  available objective
   then
    do:
      empty temp-table tempObjective.
      create tempObjective.
      buffer-copy objective except stat to tempObjective.
      tempObjective.stat = 'X'.
      
      run modifyObjective in hFileDataSrv (input table tempObjective,
                                             output std-lo). 
      if not std-lo
       then
        return no-apply.

      find first objective where objective.objectiveID = tempObjective.objectiveID no-error.
      if available objective
       then
        objective.stat = 'X'.
      
       run displayObjWidgets in this-procedure.

    end.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCollapse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCollapse wWin
ON CHOOSE OF bCollapse IN FRAME frEvent /* - */
do:
  run changeTimeHorizon in this-procedure(input -1).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bConsolidate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bConsolidate wWin
ON CHOOSE OF bConsolidate IN FRAME fMain /* Consolidate */
DO:
  run populateConsolidateTable in this-procedure. 
  if can-find(first tConsolidate) 
   then
    do:
      run dialogagentsgroup.w(input table tConsolidate,
                              input true /* for editable browse */).
    end.

    

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAr
&Scoped-define SELF-NAME bCurrentaging
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCurrentaging wWin
ON CHOOSE OF bCurrentaging IN FRAME frAr
DO:
  if not available arAging 
   then
    return.
  cLastAgingClicked = "Current".  
  lArAging = true.
  if araging.due0_30 = 0 
   then
    do:
      run filterArAging in this-procedure(input cLastAgingClicked).
      return.
    end. 

  run getAr in this-procedure(input cLastAgingClicked).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frDetail
&Scoped-define SELF-NAME bdeleteImag
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bdeleteImag wWin
ON CHOOSE OF bdeleteImag IN FRAME frDetail /* Delete */
do:
  run deleteAgentLogo in hFileDataSrv( output std-lo,
                                       output std-ch).
  if not std-lo 
   then
    do:
       message std-ch
        view-as alert-box information button ok.
       return no-apply.
    end. 
  bdeleteImag:sensitive = not std-lo.  
  image-2:load-image("") no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelTag
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelTag wWin
ON CHOOSE OF bDelTag IN FRAME frDetail /* Delete tag */
DO:
  if not available tag
   then return.
   
  empty temp-table temptag.
  create temptag.
  assign
      temptag.tagID      = tag.tagID
      temptag.entity     = "A"
      temptag.entityID   = tag.entityID
      temptag.uid        = tag.uid
      temptag.name       = tag.name
      .
  
  message "Highlighted tag will be deleted." skip "Do you want to continue?"
    view-as alert-box question buttons yes-no
    title "Delete Tag"
    update lContinue as logical.
  
  if not lContinue
   then
    return.
  
  run untagentity in hFileDataSrv (input table temptag,
                                   output std-lo).
  if not std-lo
   then
    return no-apply.
    
  openTags().
  
  apply 'value-changed' to brwTags in frame frDetail.
  
  empty temp-table temptag.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frEvent
&Scoped-define SELF-NAME bEditEvent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditEvent wWin
ON CHOOSE OF bEditEvent IN FRAME frEvent /* Edit */
DO:
  if available agent and 
     available tlEvent
   then
    do:
      empty temp-table tempEvent.
      create tempEvent.
      buffer-copy tlEvent to tempEvent.
      
      /* Assign description without anniversary number as prefix */
      if tlEvent.reminder   and
         tlEvent.year ne 0  and       
         tlEvent.year lt year(today)  
       then
        do:
          find first event where event.eventID = tlEvent.eventID no-error.
          if available event 
           then
            tempEvent.description = event.description.
        end.
        
      run dialogEvent.w(input hFiledataSrv,
                      input "M",
                      input table tempEvent,
                      output iEventID,
                      output std-lo).
      if not std-lo
       then
        return no-apply.
      
      openEvents().
      
      /* Reposition to the record. */
      find first tlEvent where tlEvent.eventID = iEventID no-error.
      if available tlEvent
       then std-ro = rowid(tlEvent).

      reposition brwEvent to rowid std-ro no-error.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frDetail
&Scoped-define SELF-NAME bEditManager
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditManager wWin
ON CHOOSE OF bEditManager IN FRAME frDetail /* Edit */
DO:
  if valid-handle(hManager)
   then run ShowWindow in hManager.
   else
    do:
      std-lo = false.
      run CanCommission in hFileDataSrv (output std-lo). 
      run dialogagentmanager.w  persistent set hManager (input this-procedure, input hFileDataSrv, input hdAgentName:screen-value in frame fMain, input std-lo,input pAction, input table agentmanager ).
    end. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frEvent
&Scoped-define SELF-NAME bEditObj
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditObj wWin
ON CHOOSE OF bEditObj IN FRAME frEvent /* Edit */
DO:
  find first objective where objective.stat = {&ActiveStat} and 
                             objective.type = 'O' no-error.
  if  available objective
   then
    do:
      empty temp-table tempObjective.
      create tempObjective.
      buffer-copy objective to tempObjective.

      run dialogobjective.w(input hFileDataSrv,
                            input "M",
                            input table tempObjective,
                            output iObjectiveID,
                            output std-lo).
      if not std-lo
       then
        return no-apply.

      run getObjective in hFileDataSrv(input objective.ObjectiveID,
                                       output table tempObjective).
                                       
      for first tempObjective:
        find first objective where objective.objectiveID = tempObjective.objectiveID no-error.
        if available objective
         then
          buffer-copy tempObjective to objective. 
      end.
      
       run displayObjWidgets in this-procedure.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEditObjTh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditObjTh wWin
ON CHOOSE OF bEditObjTh IN FRAME frEvent /* Edit */
DO:
  find first objective where objective.stat = {&ActiveStat} and 
                             objective.type = 'T' no-error.
  if  available objective
   then
    do:
      empty temp-table tempObjective.
      create tempObjective.
      buffer-copy objective to tempObjective.

      run dialogobjective.w(input hFileDataSrv,
                            input "M",
                            input table tempObjective,
                            output iObjectiveID,
                            output std-lo).
      if not std-lo
       then
        return no-apply.

      run getObjective in hFileDataSrv(input objective.ObjectiveID,
                                       output table tempObjective).
                                       
      for first tempObjective:
        find first objective where objective.objectiveID = tempObjective.objectiveID no-error.
        if available objective
         then
          buffer-copy tempObjective to objective. 
      end.
      
       run displayObjWidgets in this-procedure.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frDetail
&Scoped-define SELF-NAME bEditTag
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditTag wWin
ON CHOOSE OF bEditTag IN FRAME frDetail /* Edit */
DO:
  if available agent and 
     available tag
   then
    do:
      empty temp-table temptag.
      create temptag.
      buffer-copy tag to temptag.
      run dialogtag.w(input hFiledataSrv,
                      input "M",
                      input-output table temptag,
                      output std-lo).
      if not std-lo
       then
        return no-apply.
      
      openTags().
      
      /* Reposition to the record. */
      for first temptag:
        find first tag where tag.tagID = temptag.tagID no-error.
        if available tag 
         then std-ro = rowid(tag).
      end.
  
      reposition brwTags to rowid std-ro no-error.
      apply "value-changed" to brwTags.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frEvent
&Scoped-define SELF-NAME bEventExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEventExport wWin
ON CHOOSE OF bEventExport IN FRAME frEvent /* Export */
DO:
  run exportData in this-procedure (input browse brwEvent:handle,               /* Borwser handle */
                                    input temp-table tlEvent:handle,            /* Temp-table handle */
                                    input "tlEvent",                            /* Table name */
                                    input "Events",                             /* CSV filename */
                                    input "for each tlEvent by tlEvent.date").  /* Query */              

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEventRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEventRefresh wWin
ON CHOOSE OF bEventRefresh IN FRAME frEvent /* Refresh */
DO:
  lRefreshEvents = true.
  openEvents().
  lRefreshEvents = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExpand
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExpand wWin
ON CHOOSE OF bExpand IN FRAME frEvent /* + */
do:
   run changeTimeHorizon in this-procedure(input 1).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAlert
&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport wWin
ON CHOOSE OF bExport IN FRAME frAlert /* Export */
DO:
  run exportData in this-procedure (input browse brwAlerts:handle, /* Borwser handle */
                                    input temp-table alert:handle, /* Temp-table handle */
                                    input "alert",                 /* Table name */
                                    input "Alerts",                /* CSV filename */
                                    input "for each alert").       /* Query */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bFollow
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFollow wWin
ON CHOOSE OF bFollow IN FRAME fMain /* Following */
DO:
  if available agent
   then
    run agentFollow in hFileDataSrv (input agent.agentID,
                                     input agent.isFavorite,
                                     output std-lo).
                                     
  if std-lo
   then
    do:
      assign
          menu-item m_Following:label in menu m_Activity = (if agent.isFavorite then "Stop Following" else "Start Following")
          bFollow:tooltip                                =  menu-item m_Following:label in menu m_Activity
          .
    end.
    
    

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frEvent
&Scoped-define SELF-NAME bFwdNinety
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFwdNinety wWin
ON CHOOSE OF bFwdNinety IN FRAME frEvent /* Forward90 */
do:
  assign 
      dstartDate = dEndDate + 1
      dEndDate   = dstartDate + 90
      .
     
   run showTimelineEvents in this-procedure(input dstartDate,
                                            input dEndDate
                                           ). 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFwdThirty
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFwdThirty wWin
ON CHOOSE OF bFwdThirty IN FRAME frEvent /* Forward30 */
do:
  assign 
      dstartDate = dEndDate + 1
      dEndDate   = dstartDate + 30
      .
     
   run showTimelineEvents in this-procedure(input dstartDate,
                                            input dEndDate
                                           ). 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAlert
&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo wWin
ON CHOOSE OF bGo IN FRAME frAlert /* Go */
DO: 
  run getAlerts in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify wWin
ON CHOOSE OF bModify IN FRAME frAlert /* Modify */
DO:  
  if not available alert
   then return.
 
  publish "OpenAlert" (alert.alertID, output hAlertDataSrv).
  publish "OpenWindow" (input "dialogalertmodify",
                        input string(alert.alertID),
                        input "dialogalertmodify.w",
                        input "handle|input|" + string(hAlertDataSrv),
                        input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frEvent
&Scoped-define SELF-NAME bNewEvent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewEvent wWin
ON CHOOSE OF bNewEvent IN FRAME frEvent /* New */
DO:
  if available agent
   then
    do:
      empty temp-table tempEvent.
      create tempEvent.
      assign
        tempEvent.entity     = {&AgentCode}
        tempEvent.entityID   = agent.agentID
        tempEvent.uid        = cCurrentUID
        .
      run dialogevent.w(input hFileDataSrv,
                      input "N",
                      input table tempEvent,
                      output iEventID,
                      output std-lo).
      if not std-lo
       then
        return no-apply.

      openEvents().

      find first tlEvent where tlEvent.eventID = iEventID no-error.
      if available tlEvent
       then std-ro = rowid(tlEvent).

      reposition brwEvent to rowid std-ro no-error.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAlert
&Scoped-define SELF-NAME bNewNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewNote wWin
ON CHOOSE OF bNewNote IN FRAME frAlert /* Note */
DO: 
  if not available alert
   then return.
   
  publish "OpenAlert" (alert.alertID, output hAlertDataSrv).
  publish "OpenWindow" (input "dialogalertnoteadd",
                        input string(alert.alertID),
                        input "dialogalertnoteadd.w",
                        input "handle|input|" + string(hAlertDataSrv),
                        input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frEvent
&Scoped-define SELF-NAME bNewObj
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewObj wWin
ON CHOOSE OF bNewObj IN FRAME frEvent /* New */
DO:
  run addCompleteOurObj in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNewObjTh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewObjTh wWin
ON CHOOSE OF bNewObjTh IN FRAME frEvent /* New */
DO:
  run addCompleteTheirObj in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frDetail
&Scoped-define SELF-NAME bNewTag
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewTag wWin
ON CHOOSE OF bNewTag IN FRAME frDetail /* New */
DO: 
  if available agent
   then
    do:
      empty temp-table temptag.
      create temptag.
      assign
        temptag.entity     = "A"
        temptag.entityID   = agentID
        temptag.uid        = cCurrentUID
        temptag.username   = cCurrUser        
        temptag.entityname = agent.name        
        .
      run dialogtag.w(input hFileDataSrv,
                      input "N",
                      input-output table temptag,
                      output std-lo).
      if not std-lo
       then
        return no-apply.
      
      openTags().
      
      for first temptag:
        find first tag where tag.tagID = temptag.tagID no-error.
        if available tag 
         then std-ro = rowid(tag).
      end.
  
      reposition brwTags to rowid std-ro no-error.
      apply "value-changed" to brwTags.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNotes wWin
ON CHOOSE OF bNotes IN FRAME fMain /* Notes */
DO:
  if not available agent
   then return.
   
  run ActionNotes in this-procedure (agent.agentID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frCompliance
&Scoped-define SELF-NAME bOrgLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOrgLookup wWin
ON CHOOSE OF bOrgLookup IN FRAME frCompliance /* Lookup */
do:
  run updateOrganization in this-procedure.          
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAr
&Scoped-define SELF-NAME bover90
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bover90 wWin
ON CHOOSE OF bover90 IN FRAME frAr
DO:
  if not available arAging 
   then
    return.
  cLastAgingClicked = "over90".
  lArAging = true.
  if araging.due91_ = 0 
   then
    do:
      run filterArAging in this-procedure(input cLastAgingClicked).
      return.
    end.
  run getAr in this-procedure(input cLastAgingClicked).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bPDF
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPDF wWin
ON CHOOSE OF bPDF IN FRAME fMain /* PDF */
DO:
  if not available agent 
   then 
    return. 
                          
  publish "openAgentPDF" (input "wagentsummarypdf",
                          input string(agent.agentID),
                          input string(agent.name),
                          input "wagentsummarypdf.w",
                          input "character|input|" + agent.agentID + "^character|input|" + agent.name,
                          input this-procedure).

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frpersonagent
&Scoped-define SELF-NAME bPersonAgentAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPersonAgentAdd wWin
ON CHOOSE OF bPersonAgentAdd IN FRAME frpersonagent /* PersonAgentAdd */
do:
  run addpersonagent in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPersonAgentDeactivate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPersonAgentDeactivate wWin
ON CHOOSE OF bPersonAgentDeactivate IN FRAME frpersonagent /* PersonAgentDeactivate */
do:
  if not available tpersonagent
   then
    return.
    
  run deactivatepersonagent in this-procedure.
  
/*   apply 'value-changed' to brwPersonAgent. */
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPersonAgentExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPersonAgentExport wWin
ON CHOOSE OF bPersonAgentExport IN FRAME frpersonagent /* PersonAgentExport */
do:
  run exportData in this-procedure (input browse brwPersonAgent:handle,      /* Borwser handle */
                                    input temp-table tpersonagent:handle,    /* Temp-table handle */
                                    input "tpersonagent",                    /* Table name */
                                    input "Agent_Persons",                    /* CSV filename */
                                    input "for each tpersonagent").          /* Query */
                                
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bpersonagentlookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bpersonagentlookup wWin
ON CHOOSE OF bpersonagentlookup IN FRAME frpersonagent /* personagentlookup */
do:
  if not available tpersonagent
   then
    return.
    
  run modifypersonagent in this-procedure.

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPersonAgentUpdate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPersonAgentUpdate wWin
ON CHOOSE OF bPersonAgentUpdate IN FRAME frpersonagent /* PersonAgentUpdate */
do:
  run actionModify in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh wWin
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  run refreshData in this-procedure(input activePageNumber).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frCompliance
&Scoped-define SELF-NAME bReqFulExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReqFulExport wWin
ON CHOOSE OF bReqFulExport IN FRAME frCompliance /* Export */
do:
  run exportData in this-procedure (input browse brwReqful:handle,   /* Borwser handle */
                                  input temp-table reqfulfill:handle, /* Temp-table handle */
                                  input "reqfulfill",                 /* Table name */
                                  input "ComplianceLogs",             /* CSV filename */
                                  input "for each reqfulfill").       /* Query */  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAction
&Scoped-define FRAME-NAME frAudit
&Scoped-define SELF-NAME brwAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAction wWin
ON DEFAULT-ACTION OF brwAction IN FRAME frAudit /* Open Corrective Actions */
DO:
  apply 'choose' to btActionOpen.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAction wWin
ON ROW-DISPLAY OF brwAction IN FRAME frAudit /* Open Corrective Actions */
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAction wWin
ON START-SEARCH OF brwAction IN FRAME frAudit /* Open Corrective Actions */
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwAction}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwActivityBatch
&Scoped-define FRAME-NAME frBatch
&Scoped-define SELF-NAME brwActivityBatch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwActivityBatch wWin
ON ROW-DISPLAY OF brwActivityBatch IN FRAME frBatch /* Net Premium */
DO:

  for first brw where brw.name = self:name:
    rowColor = if tActivity.type = "A" then ? else 32 . 
    do std-in = 1 to num-entries(brw.colHandleList):
      colHandle = handle(entry(std-in, brw.colHandleList)).
      if valid-handle(colHandle)
       then
        do:
          colHandle:bgcolor = rowColor. 
          if std-in = num-entries(brw.colHandleList) and tActivity.year = year(today) 
           then
            colHandle:font = 8. 
        end.              
    end.
  end.    
  
  assign
      tActivity.month1:fgcolor in browse {&browse-name} = if (tActivity.month1 < 0)   then 12 else ?
      tActivity.month2:fgcolor  = if (tActivity.month2 < 0)   then 12 else ?
      tActivity.month3:fgcolor  = if (tActivity.month3 < 0)   then 12 else ?                                                                   
      tActivity.month4:fgcolor  = if (tActivity.month4 < 0)   then 12 else ?                                                    
      tActivity.month5:fgcolor  = if (tActivity.month5 < 0)   then 12 else ?                                                   
      tActivity.month6:fgcolor  = if (tActivity.month6 < 0)   then 12 else ?                                                   
      tActivity.month7:fgcolor  = if (tActivity.month7 < 0)   then 12 else ?                                                  
      tActivity.month8:fgcolor  = if (tActivity.month8 < 0)   then 12 else ?                                                   
      tActivity.month9:fgcolor  = if (tActivity.month9 < 0)   then 12 else ?                                                   
      tActivity.month10:fgcolor = if (tActivity.month10 < 0)  then 12 else ?                                                   
      tActivity.month11:fgcolor = if (tActivity.month11 < 0)  then 12 else ?                                                   
      tActivity.month12:fgcolor = if (tActivity.month12 < 0)  then 12 else ?
      tActivity.yrTotal:fgcolor = if (tActivity.yrTotal < 0)  then 12 else ?  
      .
          
  /* change the column format */
  assign
      cFormat  = "(>>>,>>>,>>9)% "
      cFormat1 = "(>>>,>>>,>>>)"
      cFormat2 = "(>>>,>>>,>>9)"
      .

  if (tActivity.type = "C" or tActivity.type = "A") and tActivity.year = year(today)
   then
    do:
      assign
          tActivity.month1:format in browse {&browse-name} = if month(today) >= 1 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month2:format in browse {&browse-name} = if month(today) >= 2 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month3:format in browse {&browse-name} = if month(today) >= 3 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month4:format in browse {&browse-name} = if month(today) >= 4 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month5:format in browse {&browse-name} = if month(today) >= 5 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month6:format in browse {&browse-name} = if month(today) >= 6 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month7:format in browse {&browse-name} = if month(today) >= 7 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month8:format in browse {&browse-name} = if month(today) >= 8 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month9:format in browse {&browse-name} = if month(today) >= 9 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month10:format in browse {&browse-name} = if month(today) >= 10 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month11:format in browse {&browse-name} = if month(today) >= 11 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.month12:format in browse {&browse-name} = if month(today) >= 12 then if tActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tActivity.total:format in browse {&browse-name} = if tActivity.type = "C" then cFormat else cFormat2
          tActivity.yrTotal:format in browse {&browse-name} = if tActivity.type = "C" then cFormat else cFormat2
          .
    end. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwActivityCPL
&Scoped-define FRAME-NAME frActivity
&Scoped-define SELF-NAME brwActivityCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwActivityCPL wWin
ON ROW-DISPLAY OF brwActivityCPL IN FRAME frActivity /* Closing Protection Letters */
DO:
  if tMetricsActivity.type <> "A" and tMetricsActivity.year = year(today)
   then
    tMetricsActivity.year:screen-value in browse {&browse-name} = "" /* string(tMetricsActivity.year) */.   
  
  for first brw where brw.name = self:name:
    rowColor = if tMetricsActivity.type = "A" then ? else 32 . 
    do std-in = 1 to num-entries(brw.colHandleList):
      colHandle = handle(entry(std-in, brw.colHandleList)).
      if valid-handle(colHandle)
       then
        do:
          colHandle:bgcolor = rowColor. 
          if std-in = num-entries(brw.colHandleList) and tMetricsActivity.year = year(today) 
           then
            colHandle:font = 8. 
        end.              
    end.
  end.
      
  
  assign
      tMetricsActivity.month1:fgcolor in browse {&browse-name} = if (tMetricsActivity.month1 < 0)   then 12 else ?
      tMetricsActivity.month2:fgcolor  = if (tMetricsActivity.month2 < 0)   then 12 else ?
      tMetricsActivity.month3:fgcolor  = if (tMetricsActivity.month3 < 0)   then 12 else ?                                                                   
      tMetricsActivity.month4:fgcolor  = if (tMetricsActivity.month4 < 0)   then 12 else ?                                                    
      tMetricsActivity.month5:fgcolor  = if (tMetricsActivity.month5 < 0)   then 12 else ?                                                   
      tMetricsActivity.month6:fgcolor  = if (tMetricsActivity.month6 < 0)   then 12 else ?                                                   
      tMetricsActivity.month7:fgcolor  = if (tMetricsActivity.month7 < 0)   then 12 else ?                                                  
      tMetricsActivity.month8:fgcolor  = if (tMetricsActivity.month8 < 0)   then 12 else ?                                                   
      tMetricsActivity.month9:fgcolor  = if (tMetricsActivity.month9 < 0)   then 12 else ?                                                   
      tMetricsActivity.month10:fgcolor = if (tMetricsActivity.month10 < 0)  then 12 else ?                                                   
      tMetricsActivity.month11:fgcolor = if (tMetricsActivity.month11 < 0)  then 12 else ?                                                   
      tMetricsActivity.month12:fgcolor = if (tMetricsActivity.month12 < 0)  then 12 else ?
      tMetricsActivity.yrTotal:fgcolor = if (tMetricsActivity.yrTotal < 0)  then 12 else ?  
      .
          
  /* change the column format */
  assign
      cFormat  = ">>>,>>>,>>9% "
      cFormat1 = "(>>>,>>>,>>>)"
      cFormat2 = "(>>>,>>>,>>9)"
      . 
      
  if (tMetricsActivity.type = "C" or tMetricsActivity.type = "A") and tMetricsActivity.year = year(today)
   then
    do:
      assign
          tMetricsActivity.month1:format in browse {&browse-name} = if month(today) >= 1 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month2:format in browse {&browse-name} = if month(today) >= 2 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month3:format in browse {&browse-name} = if month(today) >= 3 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month4:format in browse {&browse-name} = if month(today) >= 4 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month5:format in browse {&browse-name} = if month(today) >= 5 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month6:format in browse {&browse-name} = if month(today) >= 6 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month7:format in browse {&browse-name} = if month(today) >= 7 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month8:format in browse {&browse-name} = if month(today) >= 8 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month9:format in browse {&browse-name} = if month(today) >= 9 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month10:format in browse {&browse-name} = if month(today) >= 10 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month11:format in browse {&browse-name} = if month(today) >= 11 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month12:format in browse {&browse-name} = if month(today) >= 12 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.total:format in browse {&browse-name} = if tMetricsActivity.type = "C" then cFormat else cFormat2
          tMetricsActivity.yrTotal:format in browse {&browse-name} = if tMetricsActivity.type = "C" then cFormat else cFormat2
          .
    end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwActivityPolicies
&Scoped-define SELF-NAME brwActivityPolicies
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwActivityPolicies wWin
ON ROW-DISPLAY OF brwActivityPolicies IN FRAME frActivity /* Policies */
DO:
  if tMetricsActivity.type <> "I" and tMetricsActivity.year = year(today)
   then
    tMetricsActivity.year:screen-value in browse {&browse-name} = "" /* string(tMetricsActivity.year) */.   
  
  for first brw where brw.name = self:name:
    rowColor = if tMetricsActivity.type = "A" or tMetricsActivity.type = "I"  then ? else 32 . 
    do std-in = 1 to num-entries(brw.colHandleList):
      colHandle = handle(entry(std-in, brw.colHandleList)).
      if valid-handle(colHandle)
       then
        do:
          colHandle:bgcolor = rowColor. 
          if std-in = num-entries(brw.colHandleList) and tMetricsActivity.year = year(today) 
           then
            colHandle:font = 8. 
        end.              
    end.
  end.
      
  
  assign
      tMetricsActivity.month1:fgcolor in browse {&browse-name} = if (tMetricsActivity.month1 < 0)   then 12 else ?
      tMetricsActivity.month2:fgcolor  = if (tMetricsActivity.month2 < 0)   then 12 else ?
      tMetricsActivity.month3:fgcolor  = if (tMetricsActivity.month3 < 0)   then 12 else ?                                                                   
      tMetricsActivity.month4:fgcolor  = if (tMetricsActivity.month4 < 0)   then 12 else ?                                                    
      tMetricsActivity.month5:fgcolor  = if (tMetricsActivity.month5 < 0)   then 12 else ?                                                   
      tMetricsActivity.month6:fgcolor  = if (tMetricsActivity.month6 < 0)   then 12 else ?                                                   
      tMetricsActivity.month7:fgcolor  = if (tMetricsActivity.month7 < 0)   then 12 else ?                                                  
      tMetricsActivity.month8:fgcolor  = if (tMetricsActivity.month8 < 0)   then 12 else ?                                                   
      tMetricsActivity.month9:fgcolor  = if (tMetricsActivity.month9 < 0)   then 12 else ?                                                   
      tMetricsActivity.month10:fgcolor = if (tMetricsActivity.month10 < 0)  then 12 else ?                                                   
      tMetricsActivity.month11:fgcolor = if (tMetricsActivity.month11 < 0)  then 12 else ?                                                   
      tMetricsActivity.month12:fgcolor = if (tMetricsActivity.month12 < 0)  then 12 else ?
      tMetricsActivity.yrTotal:fgcolor = if (tMetricsActivity.yrTotal < 0)  then 12 else ?  
      .
          
  /* change the column format */
  assign
      cFormat  = ">>>,>>>,>>9% "
      cFormat1 = "(>>>,>>>,>>>)"
      cFormat2 = "(>>>,>>>,>>9)"
      .   
    
  if (tMetricsActivity.type = "C" or tMetricsActivity.type = "I") and tMetricsActivity.year = year(today)
   then
    do:
      assign
          tMetricsActivity.month1:format in browse {&browse-name} = if month(today) >= 1 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month2:format in browse {&browse-name} = if month(today) >= 2 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month3:format in browse {&browse-name} = if month(today) >= 3 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month4:format in browse {&browse-name} = if month(today) >= 4 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month5:format in browse {&browse-name} = if month(today) >= 5 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month6:format in browse {&browse-name} = if month(today) >= 6 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month7:format in browse {&browse-name} = if month(today) >= 7 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month8:format in browse {&browse-name} = if month(today) >= 8 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month9:format in browse {&browse-name} = if month(today) >= 9 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month10:format in browse {&browse-name} = if month(today) >= 10 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month11:format in browse {&browse-name} = if month(today) >= 11 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.month12:format in browse {&browse-name} = if month(today) >= 12 then if tMetricsActivity.type = "C" then cFormat else cFormat2 else cFormat1
          tMetricsActivity.total:format in browse {&browse-name} = if tMetricsActivity.type = "C" then cFormat else cFormat2
          tMetricsActivity.yrTotal:format in browse {&browse-name} = if tMetricsActivity.type = "C" then cFormat else cFormat2
          .
    end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAlerts
&Scoped-define FRAME-NAME frAlert
&Scoped-define SELF-NAME brwAlerts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAlerts wWin
ON ROW-DISPLAY OF brwAlerts IN FRAME frAlert
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAlerts wWin
ON START-SEARCH OF brwAlerts IN FRAME frAlert
do:
  {lib/brw-startSearch-multi.i &browse-name = brwAlerts}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwArAging
&Scoped-define FRAME-NAME frAr
&Scoped-define SELF-NAME brwArAging
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArAging wWin
ON ROW-DISPLAY OF brwArAging IN FRAME frAr /* Select the Aging Category */
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwArAging wWin
ON START-SEARCH OF brwArAging IN FRAME frAr /* Select the Aging Category */
do:
  {lib/brw-startSearch-multi.i &browse-name = brwArAging}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwBatch
&Scoped-define FRAME-NAME frBatch
&Scoped-define SELF-NAME brwBatch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBatch wWin
ON DEFAULT-ACTION OF brwBatch IN FRAME frBatch /* Batches */
DO:
  run openBatchDetails in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBatch wWin
ON ROW-DISPLAY OF brwBatch IN FRAME frBatch /* Batches */
DO:
 {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBatch wWin
ON START-SEARCH OF brwBatch IN FRAME frBatch /* Batches */
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwBatch}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwClaims
&Scoped-define FRAME-NAME frClaim
&Scoped-define SELF-NAME brwClaims
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwClaims wWin
ON ROW-DISPLAY OF brwClaims IN FRAME frClaim /* All Claims */
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwClaims wWin
ON START-SEARCH OF brwClaims IN FRAME frClaim /* All Claims */
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwClaims}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwEvent
&Scoped-define FRAME-NAME frEvent
&Scoped-define SELF-NAME brwEvent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwEvent wWin
ON ROW-DISPLAY OF brwEvent IN FRAME frEvent
do:
  {lib/brw-rowdisplay-multi.i}

  if date(tlevent.date) = today  
   then
    do std-in = 1 to num-entries(brw.colHandleList):
      colHandle = handle(entry(std-in, brw.colHandleList)).
      if valid-handle(colHandle)
       then 
        colHandle:bgcolor = 10.
    end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwEvent wWin
ON START-SEARCH OF brwEvent IN FRAME frEvent
do:
  {lib/brw-startSearch-multi.i &browse-name = brwEvent}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwEvent wWin
ON VALUE-CHANGED OF brwEvent IN FRAME frEvent
do:
  find current tlevent no-error.
  if available tlEvent 
   then
    do:
      if tlevent.name = 'Today' 
       then 
        bEditEvent:sensitive = false.
       else 
        bEditEvent:sensitive = true.
    end.
  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwpersonagent
&Scoped-define FRAME-NAME frpersonagent
&Scoped-define SELF-NAME brwpersonagent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwpersonagent wWin
ON DEFAULT-ACTION OF brwpersonagent IN FRAME frpersonagent
do:
  if bPersonAgentUpdate:sensitive
   then
    apply "choose" to bPersonAgentUpdate.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwpersonagent wWin
ON ROW-DISPLAY OF brwpersonagent IN FRAME frpersonagent
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwpersonagent wWin
ON START-SEARCH OF brwpersonagent IN FRAME frpersonagent
do:
  {lib/brw-startSearch-multi.i &browse-name = brwPersonAgent}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwpersonagent wWin
ON VALUE-CHANGED OF brwpersonagent IN FRAME frpersonagent
DO:
  if not available tpersonagent
   then
    return.

 if tpersonagent.email = '' or tpersonagent.doNotCall = true
  then
   btnAgentPersonMail:sensitive = false. 
 else
   btnAgentPersonMail:sensitive = true. 
    
    
  if not tpersonagent.active
   then
    do:                                                            
      bPersonAgentDeactivate    :load-image("images/s-flag_green.bmp").
      bPersonAgentDeactivate    :tooltip   = "Activate association".
    end.
  else 
   do:
      bPersonAgentDeactivate    :load-image("images/s-flag_red.bmp").
      bPersonAgentDeactivate    :tooltip   = "Deactivate association".
   end.
   
         
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQAR
&Scoped-define FRAME-NAME frAudit
&Scoped-define SELF-NAME brwQAR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQAR wWin
ON DEFAULT-ACTION OF brwQAR IN FRAME frAudit /* Audits */
DO:
  find current audit.
  if available audit
   then
    run selectAudit in this-procedure (audit.qarid).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQAR wWin
ON ROW-DISPLAY OF brwQAR IN FRAME frAudit /* Audits */
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQAR wWin
ON START-SEARCH OF brwQAR IN FRAME frAudit /* Audits */
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwQAR}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQualification
&Scoped-define FRAME-NAME frCompliance
&Scoped-define SELF-NAME brwQualification
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification wWin
ON DEFAULT-ACTION OF brwQualification IN FRAME frCompliance /* Qualifications */
DO:
  if btComViewQual:sensitive
   then
    apply "choose" to btComViewQual.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification wWin
ON ROW-DISPLAY OF brwQualification IN FRAME frCompliance /* Qualifications */
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification wWin
ON START-SEARCH OF brwQualification IN FRAME frCompliance /* Qualifications */
do:
{lib/brw-startSearch-multi.i &browse-name = brwQualification}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwReqFul
&Scoped-define SELF-NAME brwReqFul
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReqFul wWin
ON DEFAULT-ACTION OF brwReqFul IN FRAME frCompliance /* Requirements and Fulfillments */
do:
  if btComViewReq:sensitive
   then
    apply "choose" to btComViewReq.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReqFul wWin
ON ROW-DISPLAY OF brwReqFul IN FRAME frCompliance /* Requirements and Fulfillments */
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReqFul wWin
ON START-SEARCH OF brwReqFul IN FRAME frCompliance /* Requirements and Fulfillments */
do:
  {lib/brw-startSearch-multi.i &browse-name = brwReqFul}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwTags
&Scoped-define FRAME-NAME frDetail
&Scoped-define SELF-NAME brwTags
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTags wWin
ON ROW-DISPLAY OF brwTags IN FRAME frDetail
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTags wWin
ON START-SEARCH OF brwTags IN FRAME frDetail
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwTags}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTags wWin
ON VALUE-CHANGED OF brwTags IN FRAME frDetail
DO:
  assign
    bEditTag:sensitive = (available tag and tag.uid = cCurrentUID)
    bDelTag:sensitive  = (available tag and tag.uid = cCurrentUID)
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave wWin
ON CHOOSE OF bSave IN FRAME frDetail /* Save */
do:
  run actionSave in this-procedure.
  run refreshTags in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frEvent
&Scoped-define SELF-NAME bSnmtOurObj
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSnmtOurObj wWin
ON CHOOSE OF bSnmtOurObj IN FRAME frEvent /* SentimentO */
DO:
  run createSentiment in this-procedure(input {&Our}). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSnmtThObj
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSnmtThObj wWin
ON CHOOSE OF bSnmtThObj IN FRAME frEvent /* SentimentT */
DO:
  run createSentiment in this-procedure(input {&Their}). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bStatusChange
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bStatusChange wWin
ON CHOOSE OF bStatusChange IN FRAME fMain /* Status Change */
do: 
  define variable cToStatus as character  no-undo.
  
  run getChangedStatus in this-procedure (output cToStatus,
                                          output std-lo). 
  if not std-lo
   then
    return no-apply.
    
  run setAgentStatus in this-procedure (input cToStatus).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAudit
&Scoped-define SELF-NAME btActionExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btActionExport wWin
ON CHOOSE OF btActionExport IN FRAME frAudit /* Export */
DO:
  run exportData in this-procedure (input browse brwAction:handle,  /* Borwser handle */
                                    input temp-table action:handle, /* Temp-table handle */
                                    input "action",                 /* Table name */
                                    input "Audit",                  /* CSV filename */
                                    input "for each action").       /* Query */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btActionMail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btActionMail wWin
ON CHOOSE OF btActionMail IN FRAME frAudit /* Mail */
DO:
  if available action and action.uid ne "" 
   then 
    openURL("mailto:" + action.uid).     
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btActionOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btActionOpen wWin
ON CHOOSE OF btActionOpen IN FRAME frAudit /* Open */
DO:
  empty temp-table tempaction.
  empty temp-table tempfinding.
  
  if not available action
   then 
    return.
  
  create tempaction.
  buffer-copy action to tempaction.
  find first finding where finding.findingid = action.findingid no-error.
  if available finding 
   then
    do:
      create tempfinding.
      buffer-copy finding to tempfinding.
    end.
    
  run dialogagentaction.w(input table tempaction,
                          input table tempfinding).

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btActionRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btActionRefresh wWin
ON CHOOSE OF btActionRefresh IN FRAME frAudit /* Refresh */
DO:
  run refreshaction in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frDetail
&Scoped-define SELF-NAME bTagRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bTagRefresh wWin
ON CHOOSE OF bTagRefresh IN FRAME frDetail /* Refresh */
DO:
  run refreshTags in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAr
&Scoped-define SELF-NAME btArMail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btArMail wWin
ON CHOOSE OF btArMail IN FRAME frAr /* Mail */
DO:
  if available tArAging
   then 
    openURL("mailto:" + cArAgentEmail).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAudit
&Scoped-define SELF-NAME btAuditExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btAuditExport wWin
ON CHOOSE OF btAuditExport IN FRAME frAudit /* Export */
DO:
  run exportData in this-procedure (input browse brwQAR:handle,    /* Borwser handle */
                                    input temp-table audit:handle, /* Temp-table handle */
                                    input "audit",                 /* Table name */
                                    input "Audit",                 /* CSV filename */ 
                                    input "for each audit").       /* Query */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btAuditMail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btAuditMail wWin
ON CHOOSE OF btAuditMail IN FRAME frAudit /* Mail */
DO:
  find current audit.
  if audit.uid ne "" 
   then
    openURL("mailto:" + audit.uid).   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btAuditOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btAuditOpen wWin
ON CHOOSE OF btAuditOpen IN FRAME frAudit /* Open */
DO:
  find current audit.
   run selectAudit in this-procedure (audit.qarid).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btAuditRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btAuditRefresh wWin
ON CHOOSE OF btAuditRefresh IN FRAME frAudit /* Refresh */
DO:
  run refreashaudit.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frClaim
&Scoped-define SELF-NAME btClaimExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClaimExport wWin
ON CHOOSE OF btClaimExport IN FRAME frClaim /* Export */
DO:
  run exportData in this-procedure (input browse brwClaims:handle,      /* Borwser handle */
                                    input temp-table agentclaim:handle, /* Temp-table handle */
                                    input "agentclaim",                 /* Table name */
                                    input "Agentclaim",                 /* CSV filename */
                                    input "for each agentclaim")        /* Query */
                                    .

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btClaimMail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClaimMail wWin
ON CHOOSE OF btClaimMail IN FRAME frClaim /* Mail */
DO:
  find current agentclaim.
  openURL("mailto:" + agentclaim.assignedto).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btClaimRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClaimRefresh wWin
ON CHOOSE OF btClaimRefresh IN FRAME frClaim /* Refresh */
do: 
  run refreshclaim in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frCompliance
&Scoped-define SELF-NAME btComQualExp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btComQualExp wWin
ON CHOOSE OF btComQualExp IN FRAME frCompliance /* Export */
do:
  run exportData in this-procedure (input browse brwQualification:handle,   /* Borwser handle */
                                  input temp-table qualification:handle, /* Temp-table handle */
                                  input "qualification",              /* Table name */
                                  input "Qualification",              /* CSV filename */
                                  input "for each qualification").   /* Query */  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btComQualRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btComQualRefresh wWin
ON CHOOSE OF btComQualRefresh IN FRAME frCompliance /* Refresh */
do:
  lRefreshQualifications = true.
  openQualification().
  lRefreshQualifications = false.
  run enableButtons in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btComReqFulRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btComReqFulRefresh wWin
ON CHOOSE OF btComReqFulRefresh IN FRAME frCompliance /* Refresh */
do:
  lRefreshReqFulFillment = true.
  openReqFulFillment().
  lRefreshReqFulFillment = false.
  run enableButtons in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btComViewQual
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btComViewQual wWin
ON CHOOSE OF btComViewQual IN FRAME frCompliance /* View Qualification */
do:
   run viewComQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btComViewReq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btComViewReq wWin
ON CHOOSE OF btComViewReq IN FRAME frCompliance /* View requirement */
do:
  run viewComRequirement in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAr
&Scoped-define SELF-NAME btFileExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btFileExport wWin
ON CHOOSE OF btFileExport IN FRAME frAr /* Export */
DO:
  run exportData in this-procedure (input browse brwArAging:handle,   /* Borwser handle */
                                    input temp-table tArAging:handle, /* Temp-table handle */
                                    input "tArAging",                 /* Table name */
                                    input "ArAging",                  /* CSV filename */
                                    input "for each tArAging" ).      /* Query */

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btFileOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btFileOpen wWin
ON CHOOSE OF btFileOpen IN FRAME frAr /* Open */
DO:
  run openTranDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frDetail
&Scoped-define SELF-NAME btnAgentMail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnAgentMail wWin
ON CHOOSE OF btnAgentMail IN FRAME frDetail /* Mail */
do:
  openURL("mailto:" + flEmail:screen-value). 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frpersonagent
&Scoped-define SELF-NAME btnAgentPersonMail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnAgentPersonMail wWin
ON CHOOSE OF btnAgentPersonMail IN FRAME frpersonagent /* AgentPersonMail */
do:
 if not available tpersonagent
  then
   return.
   
 openURL("mailto:" + tpersonagent.email). 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frDetail
&Scoped-define SELF-NAME btnSameAsAbove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnSameAsAbove wWin
ON CHOOSE OF btnSameAsAbove IN FRAME frDetail /* Same as Above */
DO:
  do with frame {&frame-name}:
    assign
        flMName:screen-value  = hdAgentName:screen-value in frame fMain
        flMAddr1:screen-value = flAddr1:screen-value
        flMAddr2:screen-value = flAddr2:screen-value
        flMCity:screen-value  = flCity:screen-value
        cbMState:screen-value = cbState:screen-value
        flMZip:screen-value   = flZip:screen-value
        .
  end.
  apply 'value-changed':U to flMName. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnUrl
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnUrl wWin
ON CHOOSE OF BtnUrl IN FRAME frDetail /* URL */
do:
  if flWebsite:screen-value eq "" 
   then
    return.

  openURL(flWebsite:screen-value).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frEvent
&Scoped-define SELF-NAME bToday
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bToday wWin
ON CHOOSE OF bToday IN FRAME frEvent /* Today */
do:
   assign
       iTimeFrame = 0
       dstartDate = today - day(today) + 1
       dEndDate   = add-interval( dstartDate, 1, "month" ) - 1 
       . 
     
   run showTimelineEvents in this-procedure(input dstartDate,
                                            input dEndDate
                                           ). 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frpersonagent
&Scoped-define SELF-NAME btPersonAgentRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btPersonAgentRefresh wWin
ON CHOOSE OF btPersonAgentRefresh IN FRAME frpersonagent /* Refresh */
do:
  lRefreshtpersonagents = true.
  openpersonagents().
  lRefreshtpersonagents = false.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAr
&Scoped-define SELF-NAME btRefreshAr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btRefreshAr wWin
ON CHOOSE OF btRefreshAr IN FRAME frAr /* Refresh */
DO:
  run refreshAR in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBatch
&Scoped-define SELF-NAME bYear1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bYear1 wWin
ON CHOOSE OF bYear1 IN FRAME frBatch /* Year1 */
or 'CHOOSE' of bYear1
or 'CHOOSE' of bYear2
or 'CHOOSE' of bYear3
DO:
  openBatchesYearly(input self:label).
  cLastRemitClicked = self:label.
  brwBatch:title = "Batches for " + self:label.
  run enableButtons in this-procedure.
  lBatch = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAlert
&Scoped-define SELF-NAME fOwner
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fOwner wWin
ON VALUE-CHANGED OF fOwner IN FRAME frAlert /* Owner */
or 'value-changed' of fCode
DO:
  assign
      cLastAlertOwner = fOwner:input-value
      cLastAlertCode  = fCode:input-value
      .
  run filterAlerts in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_accounting
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_accounting wWin
ON CHOOSE OF MENU-ITEM m_accounting /* Accounting */
DO:
  run rpt/agentaccounting.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_AR_Information
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_AR_Information wWin
ON CHOOSE OF MENU-ITEM m_AR_Information /* A/R Information */
DO:
  apply 'choose' to bARInfo in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_claims
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_claims wWin
ON CHOOSE OF MENU-ITEM m_claims /* Claims */
DO:
  run rpt/claimsbyagent.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Compliance_Log
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Compliance_Log wWin
ON CHOOSE OF MENU-ITEM m_Compliance_Log /* Compliance Logs */
DO:
  run openLog in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Following
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Following wWin
ON CHOOSE OF MENU-ITEM m_Following /* Following */
DO:
  apply 'choose' to bFollow in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Historical_Objectives
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Historical_Objectives wWin
ON CHOOSE OF MENU-ITEM m_Historical_Objectives /* Historical Objectives */
DO:                                        
  if not available agent 
   then return.
  
  publish "OpenWindow" (input "wHistoricalObjectives",
                        input string(agent.agentID),
                        input "wHistoricalObjectives.w",
                        input "character|input|" + agent.agentID + "^character|input|" + agent.name + "^handle|input|" + string(hFileDataSrv),                                   
                        input this-procedure).                                         
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Notes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Notes wWin
ON CHOOSE OF MENU-ITEM m_Notes /* Notes */
DO:
  apply 'choose' to bNotes in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_remittances
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_remittances wWin
ON CHOOSE OF MENU-ITEM m_remittances /* Remittances */
DO:
  run rpt/agentbatchactivity.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_summary
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_summary wWin
ON CHOOSE OF MENU-ITEM m_summary /* Summary */
DO:
  
  if not available agent 
   then return.
  
  publish "OpenWindow" (input "dialogagentsummary",
                        input string(agent.agentID),
                        input "dialogagentsummary.w",
                        input "handle|input|" + string(hFileDataSrv),                                   
                        input this-procedure). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frCompliance
&Scoped-define SELF-NAME tShowInactiveQuals
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tShowInactiveQuals wWin
ON VALUE-CHANGED OF tShowInactiveQuals IN FRAME frCompliance /* Show inactive Qualifications */
DO:
  run filterQualifications in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwAction
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm2/windowmn.i}
{lib/win-main.i}
{lib/win-status.i}
{lib/brw-main-multi.i &browse-list="brwQAR,brwTags,brwEvent,brwBatch,brwClaims,brwAlerts,brwReqFul,brwAction,brwActivityBatch,brwActivityPolicies,brwActivityCPL,brwPersonAgent,brwInteraction,brwSentiments,brwObjectives,brwArAging,brwQualification"}
  
subscribe to "AgentDataChanged" anywhere.
subscribe to "AlertDataChanged" anywhere.
subscribe to "refreshAgentTimeline" anywhere.
subscribe to "refreshAgentPerson" anywhere.
subscribe to "refreshTags" anywhere.

std-ch = "".
publish "GetTempDir" (output std-ch).
chPath = std-ch + "\agentlogo".

/* Trigger to detect changes by the user */
ON 'value-changed':U anywhere 
DO:
  assign
    std-ch = ""
    std-ha = focus.
  do while std-ha <> ?:
    if std-ha:type = "DIALOG-BOX" or std-ha:type = "WINDOW" then
    do:
      tWinHandle = std-ha.
      leave.
    end.
    std-ha = std-ha:parent.
  end.
  if valid-handle(tWinHandle) then
  do:
    if tWinHandle:instantiating-procedure = this-procedure
    and focus:type <> "BROWSE"
    and focus:type <> "TOGGLE-BOX"
    and focus:name <> "cbCategory"
    and avail agent then
    do:
      if dataChanged = no then
      saveStateEnable(activePageNumber).    
    end.
    
  end.
  run enableButtons in this-procedure.
END.

{lib/pbupdate.i "'Retrieving data...'" 5}
{lib/pbupdate.i "'Formatting screen...'" 80}

run initializeObject in this-procedure.

{lib/pbupdate.i "'Finalizing screen...'" 98}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionAgentJournals wWin 
PROCEDURE ActionAgentJournals :
/*------------------------------------------------------------------------------
@description View the notes for the agent     
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  openWindowForAgent(pAgentID, "agentJournals", "wagentjournals.w").
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionModify wWin 
PROCEDURE actionModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
  
  define variable iCount            as integer   no-undo.
  define variable cpersonID         as character no-undo.
  
  if not available tpersonagent
   then
    return .
   else
    do with frame {&frame-name}:
      cpersonID = tpersonagent.personID.
      do iCount = 1 to brwpersonagent:num-iterations IN FRAME frpersonagent:
        if brwpersonagent:is-row-selected(iCount) 
         then leave.
      end.
    end.

  run dialogmodifypersonagent.w(input hdAgentID:screen-value,
                                input tpersonagent.personID,
                                input tpersonagent.personName,
                                input tpersonagent.jobTitle ,
                                input tpersonagent.notes,
                                output std-lo).     

  if not std-lo
   then
    return.

  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  if std-ch = "ARM"
   then
    run LoadAgentPersons in hFileDataSrv.    
  /* get the person data */
  run getAgentPersons in hFileDataSrv(output table personagent).
  empty temp-table tpersonagent. 
  for each personagent:
    if (date(personagent.effectivedate) <= today or date(personagent.effectivedate) = ? ) and (date(personagent.expirationdate) > today or date(personagent.expirationdate) = ?)
     then
      personagent.active = true.
    else
     personagent.active = false.
      
    create tpersonagent.
    buffer-copy personagent to tpersonagent.
   end.

   apply 'value-changed' to AffType in frame frpersonagent.
   apply 'value-changed' to brwPersonAgent in frame frpersonagent.
  
   open query brwpersonagent for each tpersonagent by tpersonagent.active desc by tpersonagent.personName.
  
   for first tpersonagent 
    where tpersonagent.personID = cpersonID no-lock:
     std-ro = rowid(tpersonagent).
   end.

   if std-ro <> ? and iCount > 0
    then
     do with frame {&frame-name}:
      brwpersonagent:set-repositioned-row(iCount, "ALWAYS") no-error.
      reposition brwpersonagent to rowid std-ro no-error.
     end.   
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNotes wWin 
PROCEDURE ActionNotes :
/*------------------------------------------------------------------------------
@description View the notes for the agent     
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  publish "GetNoteWindow" (output std-ch).
  if std-ch = "B" 
   then openWindowForAgent(pAgentID, "notes", "wagentnotesbrowse.w").
   else openWindowForAgent(pAgentID, "notes", "wagentnotes.w").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionSave wWin 
PROCEDURE actionSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

std-lo = savePage(activePageNumber).
if not std-lo 
 then
  return.

saveStateDisable(activePageNumber).
                 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addAffiliation wWin 
PROCEDURE addAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table tAffiliation.
  
  do with frame frAffiliation:
  end.
                                          
  run dialogaffiliation-otop-agent.w(input hFileDataSrv,
                                   input table tpersonagent,
                                   input cOrgID,
                                   input corgName,
                                   output std-lo).                                          
  if not std-lo 
   then
    return.

  /* Clearing Browser */
  close query brwPersonAgent.
  empty temp-table tpersonagent.
  /* Get updated data of affiliated persons with an organization */
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addCompleteOurObj wWin 
PROCEDURE addCompleteOurObj :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if self:label = "NewObj" 
   then
    do:
      if available agent
       then
        do:
          empty temp-table tempObjective.
          create tempObjective.
          assign
              tempObjective.entity     = {&AgentCode}
              tempObjective.entityID   = agent.agentID
              tempObjective.uid        = cCurrentUID
              tempObjective.stat       = {&ActiveStat}
              tempObjective.type       = 'O'
              .
          run dialogobjective.w(input hFileDataSrv,
                                input "N",
                                input table tempObjective,
                                output iObjectiveID,
                                output std-lo).
          if not std-lo
           then
            return no-apply.

          run getObjective in hFileDataSrv(input iObjectiveID,
                                       output table tempObjective).
          for first tempObjective:
            create objective.
            buffer-copy tempObjective to objective. 
          end.
        end.
    end.
   else
    do:
      message "The Objective will be marked as Completed. Continue?" view-as alert-box question buttons yes-no update std-lo.
      if not std-lo
       then
        return.
 
      find first objective where objective.stat = {&ActiveStat} and 
                                 objective.type = 'O' no-error.
      if  available objective
       then
        do:
          empty temp-table tempObjective.
          create tempObjective.
          buffer-copy objective except stat to tempObjective.
          tempObjective.stat = 'C'.
      
          run modifyObjective in hFileDataSrv (input table tempObjective,
                                               output std-lo). 
          if not std-lo
           then
            return no-apply.

          find first objective where objective.objectiveID = tempObjective.objectiveID no-error.
          if available objective
           then
            objective.stat = 'C'.
        end. 
    end.
   
  run displayObjWidgets in this-procedure. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addCompleteTheirObj wWin 
PROCEDURE addCompleteTheirObj :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if self:label = "NewObjTh"
   then
    do:
      if available agent
       then
        do:
          empty temp-table tempObjective.
          create tempObjective.
          assign
              tempObjective.entity     = {&AgentCode}
              tempObjective.entityID   = agent.agentID
              tempObjective.uid        = cCurrentUID
              tempObjective.stat       = {&ActiveStat}
              tempObjective.type       = 'T'
              .
          run dialogobjective.w(input hFileDataSrv,
                                input "N",
                                input table tempObjective,
                                output iObjectiveID,
                                output std-lo).
          if not std-lo
           then
            return no-apply.

          run getObjective in hFileDataSrv(input iObjectiveID,
                                           output table tempObjective).
          for first tempObjective:
            create objective.
            buffer-copy tempObjective to objective. 
          end.
          
        end.
    end.
   else
   do:
     message "The Objective will be marked as Completed. Continue?" view-as alert-box question buttons yes-no update std-lo.
     if not std-lo
      then
       return.
    
     find first objective where objective.stat = {&ActiveStat} and 
                                objective.type = 'T' no-error.
     if  available objective
      then
       do:
         empty temp-table tempObjective.
         create tempObjective.
         buffer-copy objective except stat to tempObjective.
         tempObjective.stat = 'C'.
      
         run modifyObjective in hFileDataSrv (input table tempObjective,
                                              output std-lo). 
         if not std-lo
          then
           return no-apply.

         find first objective where objective.objectiveID = tempObjective.objectiveID no-error.
         if available objective
          then
           objective.stat = 'C'.
       end.
   end.

   run displayObjWidgets in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addImage wWin 
PROCEDURE addImage :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable mFile       as memptr    no-undo.
  define variable lBase64     as longchar  no-undo.
  define variable cCurrentImg as longchar no-undo.
 
  /* Initially false */ 
  std-lo = false.

  system-dialog get-file procname title "Select the Logo File"  
    filters "JPG (*.JPG)"   "*.JPG",
            "JPEG (*.JPEG)"   "*.JPEG"   
            
    must-exist   
    use-filename 
    update std-lo.  

  if not std-lo 
   then
    return.
    
  /* If current image same as previous one, do not upload */
  copy-lob from file procname to mFile.
 
  lBase64 = base64-encode(mFile).   
  cCurrentImg = lBase64.
 
  run GetAgentLogo in hFileDataSrv (output chimage).                                
 
  if cCurrentImg = chimage 
   then
    do:
      message "Current Logo is same as previous Logo."
       view-as alert-box information button ok.
     return.
   end.
        
  run loadImage in hFileDataSrv(input procname,
                                input "A",
                                input hdAgentID:screen-value in frame fMain,
                                output std-lo,
                                output std-ch).
 
  if not std-lo 
   then
    do:
       message std-ch
         view-as alert-box information button ok.
       return no-apply.
    end.    

  if cCurrentImg > '' then do:
    decdmptr = base64-decode(lBase64).
    
    copy-lob decdmptr to file chPath.
    
    run setLogo.
    
    bdeleteImag:sensitive in frame frDetail = std-lo. 
    
    os-delete value(chPath) no-error.
  end.   
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addpersonagent wWin 
PROCEDURE addpersonagent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var olsuccess as logical no-undo.
  define variable cPersonId as character no-undo.  

  run dialognewperson.w(input hdAgentID:screen-value in frame fMain,
                        output cPersonId,
                        output olsuccess).  

  if not olsuccess and cPersonID = ''
   then
    return.
  
  /* Clearing Browser */
  close query brwPersonAgent.
  empty temp-table tpersonagent.
  
  lRefreshtpersonagents = true.
  /* Get updated data of persons for an agent */
  openpersonagents().
  

    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects wWin  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE currentPage  AS INTEGER NO-UNDO.

  ASSIGN currentPage = getCurrentPage().

  CASE currentPage: 

    WHEN 0 THEN DO:
       RUN constructObject (
             INPUT  'adm2/folder.w':U ,
             INPUT  FRAME fMain:HANDLE ,
             INPUT  'FolderLabels':U + 'Details|People|Timeline|Remittances|Metrics|A/R|Claims|Audits|Compliance|Alerts' + 'FolderTabWidth20FolderFont-1HideOnInitnoDisableOnInitnoObjectLayout':U ,
             OUTPUT h_folder ).
       RUN repositionObject IN h_folder ( 5.00 , 1.80 ) NO-ERROR.
       RUN resizeObject IN h_folder ( 23.62 , 203.00 ) NO-ERROR.

       /* Links to SmartFolder h_folder. */
       RUN addLink ( h_folder , 'Page':U , THIS-PROCEDURE ).

       /* Adjust the tab order of the smart objects. */
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE affiliateOrganization wWin 
PROCEDURE affiliateOrganization :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  empty temp-table tpersonagent.
  
  do with frame frpersonagent:
  end.
                                          
  run dialogaffiliation-OtoO-agent.w(input table tpersonagent,
                               input cOrgID,
                               input corgName,
                               output std-lo).                                          
  if not std-lo 
   then
    return.

  /* Clearing Browser */
  close query brwPersonAgent.
  empty temp-table tpersonagent.

  /* Get updated data of affiliated persons with an organization */

  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE agentDataChanged wWin 
PROCEDURE agentDataChanged :
/*------------------------------------------------------------------------------
@description A change was made in the application
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.  
  if valid-handle(hFileDataSrv) 
   then    
    do:
      if not lRefresh 
       then
        run LoadAgent in hFileDataSrv.
      run GetAgent in hFileDataSrv (output table agent).
      
      run GetAgentLogo in hFileDataSrv (output chimage).
      if chimage > ""
       then 
        lImage = chimage.
      run LoadAgentManagers in hFileDataSrv.
      run GetAgentManagers in hFileDataSrv (output table agentmanager).
      setHeaderState(). 
      displayHeader().  
      displayDetail(). 
      run ShowWindow in this-procedure.
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE alertDataChanged wWin 
PROCEDURE alertDataChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frAlert:
  end.
  
  find first agent no-error.
  if not available agent
   then
    return.
    
  find current alert no-error.
  if available alert
   then
    do:
      run server/getagentalerts.p( input alert.alertID, /* AlertID, 0 for All */
                                   input agent.stateID,
                                   input agent.agentID,
                                   input "",    /* Code, "" for All */
                                   input "O",    /* (O)pen Status */
                                   output table tempAlert,
                                   output std-lo,
                                   output std-ch
                                 ).
      if not std-lo
       then
        do:
          message std-ch 
           view-as alert-box.
          return.
        end.
        
      for first tempAlert:
        for first ttAlert where ttAlert.alertID = tempAlert.alertID:
          buffer-copy tempAlert to ttAlert.
        end.
      end.
          
      run getAlertDesc   in this-procedure.
      run setAlertCombos in this-procedure.
      run filterAlerts   in this-procedure. 
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changePage wWin 
PROCEDURE changePage :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
  RUN SUPER.
 
  {get CurrentPage activePageNumber}.
  
  iLastPage = activePageNumber.

  case activePageNumber:
  when 1 
   then
    do:
        hide frame frBatch.        
        hide frame frAr.
        hide frame frClaim.
        hide frame frAudit.
        hide frame frCompliance.
        hide frame frPersonagent.
        hide frame frEvent.
        hide frame frAlert.
        hide frame frActivity.
        view frame frDetail.
        if not lAgentDetail then
        do:           
           openTags().
           displayDetail().                                                   
           lAgentDetail = yes.                                               
        end.
        enable all except flOManager with frame frDetail.
        setPageState(activePageNumber, "Enabled").         
    end.
  when 2 
   then
    do:
        hide frame frDetail.
        hide frame frBatch.
        hide frame frAr.
        hide frame frClaim.
        hide frame frAudit.
        hide frame frCompliance.
        hide frame frEvent.
        hide frame frAlert.
        hide frame frActivity.
        view frame frpersonagent.
        
        if not lpersonagent then
        do:
           bBatchRefresh:sensitive = false.
           openpersonagents().
           lpersonagent = yes.
        end.
        enable all except btPersonAgentRefresh with frame frpersonagent.
        setPageState(activePageNumber, "Enabled").
        
        AffType:screen-value = "A".
        
        apply 'value-changed' to AffType.
        
        publish "GetCurrentValue" ("ApplicationCode", output std-ch).
        
        if std-ch = 'COM' or std-ch = 'AMD' 
         then
          bPersonAgentAdd:sensitive = true.
        else
         bPersonAgentAdd:sensitive = false.
        
    end.
  when 3
   then
    do:
        hide frame frDetail.        
        hide frame frBatch.        
        hide frame frAr.        
        hide frame frClaim.        
        hide frame frAudit.                
        hide frame frCompliance.                
        hide frame frpersonagent.
        hide frame frAlert.
        hide frame frActivity.
        view frame frEvent.
        if not lEvent then
        do:           
           openEvents().                                                   
           lEvent = yes.                                               
        end.
        enable all except bCollapse bEventBf bEventAf bEditEvent bEventExport bEditObj bCancelObj bNewObjTh bEditObjTh bCancelObjTh fNoSentTh fNoSentO with frame frEvent.
        setPageState(activePageNumber, "Enabled").
    end.
    
  when 4
   then
    do: 
        hide frame frDetail.
        hide frame frAr.
        hide frame frClaim.
        hide frame frAudit.
        hide frame frCompliance.
        hide frame frpersonagent.
        hide frame frEvent.
        hide frame frAlert.
        hide frame frActivity.
        view frame frBatch.
        if not lBatches 
         then
          do:
            find first agent no-error.
            if available agent
             then
              do:
                empty temp-table tActivity.
                std-in = year(today).
                openActivity(input std-in, input "N", false).
                for each activity:
                  create tActivity.
                  buffer-copy activity to tActivity.
                end.
                
                iMonth = month(today).
         
                hColumn = brwActivityBatch:get-browse-column(15).
                
                if valid-handle(hColumn)
                 then
                  hColumn:label  = "YTD " + getMonthName(iMonth).  
                open query brwActivityBatch for each tActivity by tActivity.year by tActivity.seq.
                
                run enableButtons in this-procedure.        
                
                lBatches = yes.
              end. /* if available agent */                                              
          end. /* if not lBatches */
        enable all except brwActivityBatch bBatchRefresh with frame frBatch.
        setPageState(activePageNumber, "Enabled").        
    end.
  when 5
   then
    do:        
        hide frame frDetail.        
        hide frame frBatch.        
        hide frame frAr.        
        hide frame frClaim.        
        hide frame frAudit.                
        hide frame frCompliance.                
        hide frame frpersonagent.        
        hide frame frEvent.        
        hide frame frAlert.
        view frame frActivity.
        if not lActivity 
         then
          do:
            run getAgentActivity in this-procedure(input false).    
            iMonth = month(today).
         
            hColumn = brwActivityPolicies:get-browse-column(16).
                
            if valid-handle(hColumn)
             then
              hColumn:label  = "YTD " + getMonthName(iMonth).
              
            hColumn = brwActivityCPL:get-browse-column(16).
                
            if valid-handle(hColumn)
             then
              hColumn:label  = "YTD " + getMonthName(iMonth).      
                
            lActivity = yes.
           end.
        enable all except brwActivityPolicies brwActivityCPL with frame frActivity.
        setPageState(activePageNumber, "Enabled"). 
        brwActivityPolicies:deselect-focused-row() .
        brwActivityCPL:deselect-focused-row() .
    end.  
  when 6
   then
    do:
        hide frame frDetail.        
        hide frame frBatch.
        hide frame frClaim.
        hide frame frAudit.        
        hide frame frCompliance.
        hide frame frpersonagent.
        hide frame frEvent.
        hide frame frAlert.
        hide frame frActivity.
        view frame frAr.
        if not lAr then
        do:
           openAr(input false).
           displayAr().                                                   
           lAr = yes.                                               
        end.
        enable all except btRefreshAr with frame frAr .
        setPageState(activePageNumber, "Enabled").  
        find first arAging no-error.
    end.    
  when 7
   then
    do:
        hide frame frDetail.        
        hide frame frBatch.        
        hide frame frAr.
        hide frame frAudit.        
        hide frame frCompliance.
        hide frame frpersonagent.
        hide frame frEvent.
        hide frame frAlert.
        hide frame frActivity.
        view frame frClaim.
        if not lClaim then
        do:
           displayClaim().
           openClaim().                                                   
           lClaim = yes.                                               
        end.
        enable all except btClaimOpen fLAER with frame frClaim.
        setPageState(activePageNumber, "Enabled").
    end.
  when 8
   then
    do:
        hide frame frDetail.        
        hide frame frBatch.        
        hide frame frAr.        
        hide frame frClaim.
        hide frame frCompliance.
        hide frame frpersonagent.
        hide frame frEvent.
        hide frame frAlert.
        hide frame frActivity.
        view frame frAudit.
        if not lAudit then
        do:
           openAudit().
           displayAudit().                                                   
           lAudit = yes.                                               
        end.
        enable all with frame frAudit.
        setPageState(activePageNumber, "Enabled").  
    end.  
   when 9
   then
    do: 
        hide frame frDetail.        
        hide frame frBatch.        
        hide frame frAr.        
        hide frame frClaim.        
        hide frame frAudit.
        hide frame frpersonagent.
        hide frame frEvent.
        hide frame frAlert.
        hide frame frActivity.
        view frame frCompliance.
        if not lCompliance then
        do:
           displayCompliance().  
           openQualification().
           openReqFulFillment().                                                   
           lCompliance = yes.      
        end.
        enable all with frame frCompliance.
        setPageState(activePageNumber, "Enabled").  
    end.
   when 10
   then
    do:
        hide frame frDetail.        
        hide frame frBatch.        
        hide frame frAr.        
        hide frame frClaim.        
        hide frame frAudit.                
        hide frame frCompliance.                
        hide frame frpersonagent.        
        hide frame frEvent.        
        view frame frAlert.
        if not lAlert 
         then
          do:           
             openAlerts().                                                   
             lAlert = yes.                                               
          end.
        enable all except fCode fOwner with frame frAlert.
        setPageState(activePageNumber, "Enabled").
        hide frame frActivity.
        assign
            fOwner:sensitive    = query brwAlerts:num-results > 0
            fCode:sensitive     = query brwAlerts:num-results > 0
            bExport:sensitive   = query brwAlerts:num-results > 0
            bModify:sensitive   = query brwAlerts:num-results > 0
            bNewNote:sensitive  = query brwAlerts:num-results > 0
            .         
    end.
  end case.
  
  run customizeWidgets in this-procedure.
  run enableButtons       in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changeTimeHorizon wWin 
PROCEDURE changeTimeHorizon :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipiCount     as integer  no-undo.
  
  define variable tempStDate as date no-undo.
  define variable tempFnDate as date no-undo.
  
  iTimeFrame = iTimeFrame + ipiCount.

  case iTimeFrame:
   when 0 
    then
     do:
       assign 
           tempStDate   = dstartDate
           tempFnDate   = dEndDate 
           .
     end.
     
   when 1 
    then
     do:
       assign 
           tempStDate   = dstartDate - 30
           tempFnDate   = dEndDate + 30 
           .
     end.
     
   when 2 
    then
     do:
       assign 
           tempStDate = dstartDate - 75
           tempFnDate   = dEndDate + 75 
           .
     end.
     
   when 3 
    then
     do:
       assign 
           tempStDate = dstartDate - 167
           tempFnDate   = dEndDate + 168
           .
     end.  
  
  end case.

  run showTimelineEvents in this-procedure(input tempStDate,
                                           input tempFnDate
                                          )
                                          .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE checkOpenQar wWin 
PROCEDURE checkOpenQar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  define input parameter pAuditID as character no-undo.
  define output parameter qaropen as logical   no-undo.
  define output parameter qarsummaryhandle as handle.

  find first openQars
   where openQars.QarId = integer(pAuditID) no-error.
  if available openQars and valid-handle(openQars.hInstance)
   then
  do:
    assign
        qaropen          = yes
        qarsummaryhandle = openQars.hInstance
        .
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow wWin 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: Procedure used to close window from parent window      
------------------------------------------------------------------------------*/
  /* close children windows */
  if valid-handle(hManager)
   then run closeWindow in hManager.      

  publish "windowClosed" (input this-procedure). 
   
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE createOrganization wWin 
PROCEDURE createOrganization :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter cOrgID as character no-undo.
  
  empty temp-table tOrganization.
  
  if cOrgID ne '' and cOrgID ne ?
   then
    do:
      create tOrganization.
      assign
          tOrganization.orgID       = string(cOrgID)
          tOrganization.name        = agent.legalName
          tOrganization.addr1       = agent.addr1
          tOrganization.addr2       = agent.addr2
          tOrganization.city        = agent.city
          tOrganization.state       = agent.state
          tOrganization.zip         = agent.zip
          tOrganization.email       = agent.email
          tOrganization.website     = agent.website
          tOrganization.fax         = agent.fax
          tOrganization.phone       = agent.phone
          tOrganization.stat        = {&ActiveStat}
          .
      create organization.
      buffer-copy tOrganization to organization.
      run newOrganizationAgent in hFileDataSrv (input table tOrganization).
      publish "newOrganizationAgent" (input table tOrganization). /* for organization lookup */
    end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE createSentiment wWin 
PROCEDURE createSentiment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcType as character no-undo.
  
  define variable iSentimentID as integer no-undo.
  
  do with frame frEvent:
  end.
      
  find first objective where objective.stat = {&ActiveStat} and 
                             objective.type = ipcType no-error.

  if not available objective
   then
    return.

  create tempSentiment.

  tempSentiment.objectiveID  = objective.objectiveID.

  run dialogsentiment.w(input hFileDataSrv,
                        input objective.description,
                        input table tempSentiment,
                        output iSentimentID,
                        output std-lo).
  if not std-lo
   then
    return no-apply.
         
  run getSentiment in hFileDataSrv(input iSentimentID,
                                   output table tempSentiment).
  for first tempSentiment:
    create sentiment.
    buffer-copy tempSentiment to sentiment. 
  end.
      
  for last sentiment where sentiment.objectiveID = objective.objectiveID by sentiment.createDate:
    if ipcType = {&Their} 
     then
      do:
        assign
            bSnmtThObj:tooltip       = sentiment.notes
            fCreateDtTh:screen-value = string(sentiment.createDate)
            fCreateDtTh:visible       = true
            fNoSentTh:visible         = false
            .
            
        case sentiment.type:
           when 1 
           then
            bSnmtThObj:load-image("images/s-too sad-16").
          when 2 
           then
            bSnmtThObj:load-image("images/s-sad-16").
          when 3 
           then
            bSnmtThObj:load-image("images/s-neutral-16").
          when 4 
           then
            bSnmtThObj:load-image("images/s-smiling-16").
          otherwise 
            bSnmtThObj:load-image("images/s-lovingit-16"). 
        end case.    
      end.
     else
      do:
        assign
            bSnmtOurObj:tooltip     = sentiment.notes
            fCreateDtO:screen-value = string(sentiment.createDate)
            fCreateDtO:visible      = true
            fNoSentO:visible        = false
            .
        case sentiment.type:
           when 1 
           then
            bSnmtOurObj:load-image("images/s-too sad-16").
          when 2 
           then
            bSnmtOurObj:load-image("images/s-sad-16").
          when 3 
           then
            bSnmtOurObj:load-image("images/s-neutral-16").
          when 4 
           then
            bSnmtOurObj:load-image("images/s-smiling-16").
          otherwise 
            bSnmtOurObj:load-image("images/s-lovingit-16"). 
        end case.    
      end.     
  end.

  empty temp-table tempSentiment.
  
  if available agent 
   then
    publish "refreshHistoricalObjs" (input agent.agentID,
                                     input ipcType).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE customizeWidgets wWin 
PROCEDURE customizeWidgets :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  assign 
      flOManager            :sensitive in frame frDetail     = false
      tOrganization         :sensitive in frame frCompliance = false
      tOrgName              :sensitive in frame frCompliance = false
      edObj                 :sensitive in frame frEvent      = false
      edObjTh               :sensitive in frame frEvent      = false
      fCreateDtO            :sensitive in frame frEvent      = false
      fCreateDtTh           :sensitive in frame frEvent      = false
      fNoSentO              :sensitive in frame frEvent      = false
      fNoSentTh             :sensitive in frame frEvent      = false
      .
  case activePageNumber:
    when 3
     then
      do:
        run displayObjWidgets in this-procedure.
      end.
  end case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deactivatepersonagent wWin 
PROCEDURE deactivatepersonagent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var pipersonagentID as integer no-undo.
 def var olsuccess as logical no-undo.
 
empty temp-table ttpersonagent.
 
 if not available tpersonagent
  then
   return.
 
 pipersonagentID = tpersonagent.personagentID . 
 
 create ttpersonagent.
 buffer-copy tpersonagent to ttpersonagent.
    
 if tpersonagent.expirationdate = ?  
   then
    do:
    run deactivatepersonagent in hFileDataSrv (input pipersonagentID,
                                               input today,
                                               output olsuccess,
                                               output std-ch).
    if olsuccess
     then
      tpersonagent.expirationdate = today.                                               
                                            
    end.
  else
   do:
   
     run deactivatepersonagent in hFileDataSrv (input pipersonagentID,
                                                input ?,
                                                output olsuccess,
                                                output std-ch).
     if olsuccess
      then
       tpersonagent.expirationdate = ?.                                                
                                            
   end.
   
 

 if not olsuccess
   then
    return.
    
  publish "updateAgentPersonDate" (input tPersonagent.personagentID, input tpersonagent.expirationdate).
  publish "updateAffiliationAgent".
  
  /* Clearing Browser */
  close query brwPersonAgent.
  empty temp-table tpersonagent.

  /* Get updated data of affiliated persons with an organization */
  openpersonagents().
  
  find first tpersonagent where tpersonagent.personagentID = pipersonagentID no-error.
  if not available ttpersonagent
   then
    do:
      message "Internal Error of missing data."
          view-as alert-box error buttons ok.
      return.
    end.
    
  std-ro = rowid(tpersonagent).
  
  apply 'value-changed' to AffType in frame frpersonagent.
  
  reposition brwPersonAgent to rowid std-ro no-error.
  
  apply 'value-changed' to brwPersonAgent in frame frpersonagent.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteAffiliation wWin 
PROCEDURE deleteAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lcheck     as logical   no-undo. 
  define variable cComStat   as character no-undo.
  
  define buffer tpersonagent for tpersonagent.
  
  if not available tpersonagent 
   then
    do:
      message "Internal Error of missing data."
          view-as alert-box error buttons ok.
      return.
    end.
    
  message "Highlighted tpersonagent will be deleted." skip "Do you want to continue?"
      view-as alert-box question buttons yes-no title "Delete tpersonagent" update lcheck.
   
  if not lcheck 
   then
    return.

  if not std-lo 
   then
    return.
  
  /* Clearing Browser */
  close query brwPersonAgent.
  empty temp-table tpersonagent.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteManager wWin 
PROCEDURE DeleteManager :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   define input parameter pManagerID as integer no-undo.
   define output parameter pSuccess  as logical no-undo.
  
   find first agentmanager where managerID =  pManagerID no-error.
   if not avail agentmanager 
    then
     return.
    else 
     do:
       delete agentmanager.
       pSuccess = true.
     end.
     
   cManagerUID = "".  
   for first agentmanager no-lock
       where agentmanager.isPrimary = true:
  
     assign
       std-ch = ""
       cManagerUID = agentmanager.uid
       .
     publish "GetSysUserName" (cManagerUID, output std-ch).
     do with frame frDetail:
       assign flOManager:screen-value = std-ch.
     end.
   end.
   if cManagerUID = ""
    then 
     do with frame frDetail:
       flOManager:screen-value  = "None".
     end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayManager wWin 
PROCEDURE displayManager :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   cManagerUID = "".
   for first agentmanager no-lock
     where agentmanager.isPrimary = true:

     assign
       std-ch = ""
       cManagerUID = agentmanager.uid
       .

     publish "GetSysUserName" (cManagerUID, output std-ch).

     assign
         flOManager:screen-value in frame frDetail = std-ch
         flOManager:sensitive = false
         .
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayObjWidgets wWin 
PROCEDURE displayObjWidgets :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frEvent:
  end.

  find first objective where objective.stat = {&ActiveStat} and 
                             objective.type = 'O' no-error.
  if available objective
   then
    do:
      assign
          bEditObj:sensitive     = true
          bCancelObj:sensitive   = true
          bNewObj:label          = "CompleteObj"
          bNewObj:tooltip        = "Complete Objective"
          edObj:screen-value     = objective.description
          .   
      bNewObj:load-image("images/s-completed.bmp").
      
      if can-find(first sentiment where sentiment.objectiveID = objective.objectiveID and 
                                        sentiment.uid         = cCurrentUID) 
       then
        assign
            fCreateDtO:visible = true
            fNoSentO:visible     = false
            .
       else
        do:
          assign
            fCreateDtO:visible = false
            fNoSentO:visible     = true
            . 
          bSnmtOurObj:load-image("images/s-nosentiment-neutral-16").
        end.

        

      for last sentiment where sentiment.objectiveID = objective.objectiveID and 
                               sentiment.uid         = cCurrentUID 
                                by sentiment.createDate:
        assign
            bSnmtOurObj:tooltip    = sentiment.notes
            fCreateDtO:screen-value = string(sentiment.createDate)
            .
        case sentiment.type:
           when 1 
           then
            bSnmtOurObj:load-image("images/s-too sad-16").
          when 2 
           then
            bSnmtOurObj:load-image("images/s-sad-16").
          when 3 
           then
            bSnmtOurObj:load-image("images/s-neutral-16").
          when 4 
           then
            bSnmtOurObj:load-image("images/s-smiling-16").
          otherwise 
            bSnmtOurObj:load-image("images/s-lovingit-16").     
        end case.
           
      end.
      
    end.
   else
    do:
      assign
          bEditObj:sensitive     = false
          bCancelObj:sensitive   = false
          bNewObj:label          = "NewObj"
          bNewObj:tooltip        = "New Objective"
          edObj:screen-value     = ""
          
          fCreateDtO:visible      = false
          fNoSentO:visible        = true
          bSnmtOurObj:tooltip     = "Create New Sentiment"
          .
          
      bSnmtOurObj:load-image("images/s-nosentiment-neutral-16").
      bNewObj:load-image("images/s-add.bmp").    
    end.
    
  /* 'Their' Objective */
  find first objective where objective.stat = {&ActiveStat} and 
                             objective.type = 'T' no-error.
  if available objective
   then
    do:
      assign
          bEditObjTh:sensitive     = true
          bCancelObjTh:sensitive   = true
          bNewObjTh:label          = "CompleteObjTh"
          bNewObjTh:tooltip        = "Complete Objective"
          edObjTh:screen-value     = objective.description
          .
      bNewObjTh:load-image("images/s-completed.bmp").
      
      if can-find(first sentiment where sentiment.objectiveID = objective.objectiveID and 
                                        sentiment.uid         = cCurrentUID) 
       then
        assign
            fCreateDtTh:visible = true
            fNoSentTh:visible     = false
            .
       else
         do:
           assign
            fCreateDtTh:visible = false
            fNoSentTh:visible     = true
            bSnmtThObj:tooltip    = "Create New Sentiment"
            .
           bSnmtThObj:load-image("images/s-nosentiment-neutral-16"). 
         end.
        
      
      for last sentiment where sentiment.objectiveID = objective.objectiveID and 
                               sentiment.uid         = cCurrentUID 
                                by sentiment.createDate:
        assign
            bSnmtThObj:tooltip    = sentiment.notes
            fCreateDtTh:screen-value = string(sentiment.createDate)
            .
        case sentiment.type:
          when 1 
           then
            bSnmtThObj:load-image("images/s-too sad-16").
          when 2 
           then
            bSnmtThObj:load-image("images/s-sad-16").
          when 3 
           then
            bSnmtThObj:load-image("images/s-neutral-16").
          when 4 
           then
            bSnmtThObj:load-image("images/s-smiling-16").
          otherwise  
            bSnmtThObj:load-image("images/s-lovingit-16").      
        end case.   
      end. 
      
    end.
   else
    do:
      assign
          bEditObjTh:sensitive     = false
          bCancelObjTh:sensitive   = false
          bNewObjTh:label          = "NewObjTh"
          bNewObjTh:tooltip        = "New Objective"
          edObjTh:screen-value     = ""
          
          fCreateDtTh:visible      = false
          fNoSentTh:visible        = true
          bSnmtThObj:tooltip       = "Create New Sentiment"
          .
          
      bSnmtThObj:load-image("images/s-nosentiment-neutral-16").
      bNewObjTh:load-image("images/s-add.bmp").
    end.
          
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayYearTrends wWin 
PROCEDURE displayYearTrends :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipiYear    as integer no-undo.
  define input parameter ipiYearSeq as integer no-undo.
  
  define variable deAmount as decimal no-undo.
  
  do with frame frBatch:
  end.
  
  find first agent no-error.
  if not available agent
   then
    return.
  
  empty temp-table ttBatch.
  
  run getagentbatchesyearly in hFileDataSrv (input ipiYear,
                                             output table ttBatch).
  
    
  for each ttBatch break by ttBatch.periodID:
  
    accumulate ttbatch.netpremiumreported (total by ttBatch.periodID).
  
    if last-of(ttBatch.periodID) 
     then
      do:
        deAmount = accum total by ttBatch.periodID ttbatch.netpremiumreported. 
      
        getMonthWiseYearTrend(input ttBatch.periodID,
                              input ipiYear,
                              input ipiYearSeq,
                              input deAmount).
      end.  
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE emptyTempTable wWin 
PROCEDURE emptyTempTable :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Details Tab */
  lAgentDetail = false.
  empty temp-table agent.
  empty temp-table agentmanager.
  chImage = "".
  empty temp-table tag.
  close query brwTags.
  displayDetail().

  /*tpersonagent Tab*/ 
  lpersonagent = false.
  empty temp-table organization. 
  empty temp-table tpersonagent. 
  close query brwPersonAgent.
/*   AffEditor:screen-value in frame frpersonagent = "". */
  
  /*Timeline Tab*/
  lEvent = false.
  empty temp-table tempObjective.
  empty temp-table tempSentiment.
  empty temp-table event.
  close query brwEvent.
  run showTimelineEvents in this-procedure(input dstartDate,
                                          input dEndDate
                                         ).    
  
  /*Remittance Tab*/
  lBatches = false.
  empty temp-table tActivity.
  empty temp-table Batch.
  close query brwActivityBatch.
  close query brwBatch.
  
  /* Metrics Tab */
  lActivity = false.
  empty temp-table activityMetrics.
  empty temp-table tMetricsActivity.
  close query brwActivityPolicies. 
  close query brwActivityCPL.      
  
  /*AR Tab*/
  lAr = false.
  empty temp-table arAging.
  empty temp-table ttArAging.
  empty temp-table tArAging.
  close query brwArAging. 
  
  /*Claims Tab*/
  lClaim = false.
  close query brwClaims.
  empty temp-table agentclaim.
  empty temp-table claimcostssummary.
  
  /*Audits Tab*/
  lAudit = false.
  empty temp-table audit.
  empty temp-table action.
  empty temp-table finding.
  
  /*Compliance Tab*/
  lCompliance = false.
  empty temp-table role.
  empty temp-table reqfulfill.  
  empty temp-table qualification.

  /*Alerts Tab*/
  lAlert = false.
  empty temp-table alert.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableButtons wWin 
PROCEDURE enableButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  case activePageNumber:
  
   when 1
    then
     do with frame frDetail:
       assign
           btnAgentMail:sensitive =  if flEmail   :screen-value = "" then false else true
           BtnUrl      :sensitive =  if flWebsite :screen-value = "" then false else true
           bdeleteImag :sensitive =  (chImage ne "")
           .
     end.
    
   when 3
    then
     do with frame frEvent:
       assign
         bEditEvent:sensitive   = can-find(first tlEvent where tlEvent.name ne "Today")
         bEventExport:sensitive = bEditEvent:sensitive
         .
       apply 'value-changed' to brwEvent.    
     end.  
    
   when 4 
    then 
     do with frame frBatch:
       assign
           bBatchExport:sensitive = if brwBatch:num-iterations <= 0 then false else true
           bbatchopen  :sensitive = bBatchExport:sensitive
           bActivityBatchExport:sensitive = if brwActivityBatch:num-iterations <= 0 then false else true
           .
     end.
    
   when 5
    then
     do with frame frActivity:
       assign
           bActivityExp   :sensitive = if brwActivityPolicies:num-iterations <= 0 then false else true
           bActivityExpCPL:sensitive = if brwActivityCPL     :num-iterations <= 0 then false else true
           .
     end.
   when 6
    then
     do with frame frAr:
       assign
           btFileExport:sensitive = if brwArAging:num-iterations <= 0 then false else true
           btFileOpen  :sensitive = btFileExport:sensitive
           btArMail    :sensitive = btFileExport:sensitive
           .
     end.
     
   when 7 
    then
     do with frame frClaim:
       assign 
           btClaimExport:sensitive = if brwClaims:num-iterations <= 0 then false else true
           btClaimMail  :sensitive = btClaimExport:sensitive
           .
     end.
    
   when 8 
    then  
     do with frame frAudit:
       assign
           btAuditExport:sensitive = if brwQAR:num-iterations <= 0 then false else true
           btAuditOpen  :sensitive = btAuditExport:sensitive
           btAuditMail  :sensitive = btAuditExport:sensitive
         
           btActionExport:sensitive = if brwAction:num-iterations <= 0 then false else true
           btActionOpen  :sensitive = btActionExport:sensitive
           btActionMail  :sensitive = btActionExport:sensitive
           .
     end.
   when 9
    then
     do with frame frCompliance:
       if not can-find(first tqualification) 
        then
         tShowInactiveQuals:sensitive = false.
        else
         tShowInactiveQuals:sensitive = true.
         
       assign
            BtComViewQual:sensitive  = if brwQualification:num-iterations <= 0 then false else true
            btComQualExp:sensitive   = BtComViewQual:sensitive
            BtComViewReq:sensitive   = if brwReqful:num-iterations <= 0 then false else true
            bReqFulExport:sensitive  = BtComViewReq:sensitive
            btComQualRefresh:sensitive   = tOrganization:screen-value ne "" 
            btComReqFulRefresh:sensitive     = tOrganization:screen-value ne ""
            .
                 
     end.
  end case.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableObject wWin 
PROCEDURE enableObject :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
  RUN SUPER.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY hdAgentID hdAgentName fStatus hdLAgentName 
      WITH FRAME fMain IN WINDOW wWin.
  ENABLE bAgentJournals bConsolidate RECT-68 hdAgentID hdAgentName fStatus 
         hdLAgentName bPDF bRefresh bStatusChange bARInfo bFollow bNotes 
      WITH FRAME fMain IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  {&OPEN-BROWSERS-IN-QUERY-frLogo}
  FRAME frLogo:SENSITIVE = NO.
  DISPLAY fCode fOwner 
      WITH FRAME frAlert IN WINDOW wWin.
  ENABLE brwAlerts bGo 
      WITH FRAME frAlert IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frAlert}
  FRAME frAlert:SENSITIVE = NO.
  ENABLE b30to60 b60to90 bbalance bCurrentaging bover90 brwArAging 
      WITH FRAME frAr IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frAr}
  ENABLE brwQAR brwAction 
      WITH FRAME frAudit IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frAudit}
  ENABLE brwBatch bYear1 bYear2 bYear3 
      WITH FRAME frBatch IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frBatch}
  FRAME frBatch:SENSITIVE = NO.
  DISPLAY netfrom36 netfrom24 netfrom12 netfromLTD costfrom36 costfrom24 
          costfrom12 costfromLTD errorfromLTD errortfrom36 errorfrom24 
          errorfrom12 fLAER fLAEL fLossR fLossL fRecoveriesR fTotalCostL from12 
          from36 from24 costnetLTD costnet36 costnet24 costnet12 errornet12 
          errornetLTD errornet36 errornet24 fTotal 
      WITH FRAME frClaim IN WINDOW wWin.
  ENABLE netfrom36 netfrom24 netfrom12 netfromLTD costfrom36 costfrom24 
         costfrom12 costfromLTD errorfromLTD errortfrom36 errorfrom24 
         errorfrom12 brwClaims fLAER fLAEL fLossR fLossL fRecoveriesR 
         fTotalCostL from12 from36 from24 costnetLTD costnet36 costnet24 
         costnet12 errornet12 errornetLTD errornet36 errornet24 fTotal 
      WITH FRAME frClaim IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frClaim}
  DISPLAY tOrganization tOrgName tShowInactiveQuals 
      WITH FRAME frCompliance IN WINDOW wWin.
  ENABLE tOrganization tOrgName brwQualification brwReqFul bOrgLookup 
      WITH FRAME frCompliance IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frCompliance}
  FRAME frCompliance:SENSITIVE = NO.
  DISPLAY flAgentName flLAgentName flAddr1 flAddr2 flCity cbState flZip flPhone 
          flFax flEmail flWebsite cbSwVendor fDomain cbRegion cbContractID 
          rdRemitType flContractDate flLiabilityLimit flRemitValue flRemitAlert 
          flPolicyLimit flProspectDate flActiveDate flWithdrawnDate flClosedDate 
          flReviewDate flCancelDate flMName flMAddr1 flMAddr2 flMCity cbMState 
          flMZip flONPN flOAltaUID flOStateUID flOCorporation flOManager 
          cbStateOper 
      WITH FRAME frDetail IN WINDOW wWin.
  ENABLE brwTags flAgentName flLAgentName flAddr1 flAddr2 flCity cbState flZip 
         flPhone flFax flEmail flWebsite cbSwVendor fDomain bNewTag cbRegion 
         cbContractID rdRemitType flContractDate flLiabilityLimit flRemitValue 
         flRemitAlert flPolicyLimit flProspectDate flActiveDate btnAgentMail 
         flWithdrawnDate flClosedDate flReviewDate flCancelDate btnSameAsAbove 
         flMName flMAddr1 flMAddr2 flMCity cbMState flMZip flONPN flOAltaUID 
         flOStateUID flOCorporation cbStateOper bAddImg bAgentRefresh 
         bTagRefresh BtnUrl bCancel bSave bdeleteImag RECT-4 RECT-5 RECT-3 
         RECT-6 RECT-34 
      WITH FRAME frDetail IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frDetail}
  DISPLAY edObjTh edObj fNoSentTh fCreateDtTh fNoSentO fCreateDtO 
      WITH FRAME frEvent IN WINDOW wWin.
  ENABLE brwEvent bNewObj bNewObjTh bCollapse bExpand bToday bBwdNinety 
         bEventAf bEventBf bTimeLine bBwdThirty bFwdNinety bFwdThirty 
         bEditEvent bNewEvent 
      WITH FRAME frEvent IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frEvent}
  FRAME frEvent:SENSITIVE = NO.
  VIEW FRAME frActivity IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frActivity}
  DISPLAY AffType 
      WITH FRAME frpersonagent IN WINDOW wWin.
  ENABLE bpersonagentlookup AffType brwpersonagent bPersonAgentAdd 
         btnAgentPersonMail btPersonAgentRefresh 
      WITH FRAME frpersonagent IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frpersonagent}
  FRAME frpersonagent:SENSITIVE = NO.
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exitObject wWin 
PROCEDURE exitObject PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:  Window-specific override of this procedure which destroys 
            its contents and itself.
    Notes:  
------------------------------------------------------------------------------*/
  if dataChanged then
  do:
    std-lo = no.
    message
      "Data has been modified." skip
      "Are you sure you want to exit without saving?"
      view-as alert-box warning buttons yes-no
      update std-lo.
    if std-lo = no 
     then return.
  end.

  /* Close the window and its all child instances */
  run closeWindow in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData wWin 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter haBrowser  as handle    no-undo.
  define input parameter haTable    as handle    no-undo.
  define input parameter cTableName as character no-undo.
  define input parameter cFileName  as character no-undo.
  define input parameter cQuery     as character no-undo.
  
  define variable cFields    as character no-undo.
  define variable cFieldName as character no-undo.
  
  /* Get column and field name of a browser */
  do std-in = 1 to haBrowser:num-columns:
    std-ha = haBrowser:get-browse-column(std-in).
  
    assign
        cFields    = cFields    + (if cFields    > "" then "," else "") + std-ha:name
        cFieldName = cFieldName + (if cFieldName > "" then "," else "") + replace(std-ha:label,"!"," ")
        .
  end.
  
  if haBrowser:query:num-results = ? or haBrowser:query:num-results = 0
   then
    do:
      message "There is nothing to export"
        view-as alert-box warning buttons ok.
      return.
    end.
  
  publish "GetReportDir" (output std-ch).
  
  run util/exporttable.p (table-handle haTable,
                          cTableName,
                          cQuery,
                          cFields,
                          cFieldName,
                          std-ch,
                          cFileName + "-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterAlerts wWin 
PROCEDURE filterAlerts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttAlert for ttAlert.
  
  do with frame frAlert:
  end.
  
  close query brwAlerts.
  empty temp-table alert.

  for each ttAlert
    where ttAlert.owner = (if fOwner:screen-value = "ALL" then ttAlert.owner
                           else fOwner:screen-value)       and
          ttAlert.processCode = (if fCode:screen-value = "ALL" then ttAlert.processCode
                           else fCode:screen-value)                :
    create alert.
    buffer-copy ttAlert to alert.    
  end.

  open query brwAlerts for each alert.
  
  assign
      bExport:sensitive   = query brwAlerts:num-results > 0
      bModify:sensitive   = query brwAlerts:num-results > 0
      bNewNote:sensitive  = query brwAlerts:num-results > 0
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterArAging wWin 
PROCEDURE filterArAging :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcBucket as character no-undo.

  empty temp-table tArAging.
  
  do with frame frAr:
  end.

  case ipcBucket:
    when "Current" 
     then
      do:
        brwArAging:title = "Files in Current Category".
        for each ttArAging 
          where ttArAging.due0_30 <> 0:
          create tArAging.
          buffer-copy ttArAging to tArAging.
          if ttaraging.type = "P" and ttArAging.fileNumber = "" 
           then
            tArAging.fileNumber = "Unapplied Payment".
        end.
      end.
    when "30to60" 
     then
      do:
        brwArAging:title = "Files in 31-60 Category".
        for each ttArAging 
          where ttArAging.due31_60 <> 0:
          create tArAging.
          buffer-copy ttArAging to tArAging.
          if ttaraging.type = "P" and ttArAging.fileNumber = "" 
           then
            tArAging.fileNumber = "UnApplied Payment".
        end.
      end.  
    when "60to90" 
     then
      do:
        brwArAging:title = "Files in 61-90 Category".
        for each ttArAging 
          where ttArAging.due61_90 <> 0:
          create tArAging.
          buffer-copy ttArAging to tArAging.
          if ttaraging.type = "P" and ttArAging.fileNumber = "" 
           then
            tArAging.fileNumber = "UnApplied Payment".
        end.
      end.
    when "over90" 
     then
      do:
        brwArAging:title = "Files in Over 90 Category".
        for each ttArAging 
          where ttArAging.due91_ <> 0:
          create tArAging.
          buffer-copy ttArAging to tArAging.
          if ttaraging.type = "P" and ttArAging.fileNumber = "" 
           then
            tArAging.fileNumber = "UnApplied Payment".  
        end.
      end.
    when "balance" 
     then
      do:
        brwArAging:title = "Files in All Category".
        for each ttArAging 
          where ttArAging.balance <> 0:
          create tArAging.
          buffer-copy ttArAging to tArAging.
          if ttaraging.type = "P" and ttArAging.fileNumber = "" 
           then
            tArAging.fileNumber = "UnApplied Payment".
        end.
      end.
  end case.

  open query brwArAging for each tArAging. 
  
  run enableButtons in this-procedure.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterComLogs wWin 
PROCEDURE filterComLogs :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frCompliance:
  end.
 
  empty temp-table ttComLog.
  
  for each ComLog:
    create ttcomLog.
    buffer-copy ComLog to ttcomLog.
  end.
  
  open query brwComLogs for each ttcomLog by ttcomLog.logdate descending.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterQualifications wWin 
PROCEDURE filterQualifications :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table qualification.
  
  for each tqualification where tqualification.stateId = agent.stateID and
                                tQualification.activ   = (if tShowInactiveQuals:input-value in frame frCompliance eq yes then tQualification.activ else yes):
    create qualification.
    buffer-copy tqualification to qualification.
    qualification.stat = getQualificationDesc(qualification.stat).  
  end.
  
  open query brwqualification for each qualification. 
  
  run enableButtons in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getAgentActivity wWin 
PROCEDURE getAgentActivity :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter iplRefresh    as logical   no-undo.
  
  define variable        iYear         as integer   no-undo.
  define variable        cCategoryList as character no-undo.
  
  empty temp-table activityMetrics. 
  empty temp-table tMetricsActivity. 
  
  cCategoryList = "T,P". /* T = CPL Issued net,
                            P = policies Issued */ 
  
  /* Populating year data in widgets */
  iYear = year(today).   
  openActivityMetrics(iYear,cCategoryList,iplRefresh).
  
  for each activityMetrics by activityMetrics.year:
    if can-do( "A,I,C,P",activityMetrics.type) 
     then
    do:
        
    
    create tMetricsActivity.
    buffer-copy activityMetrics to tMetricsActivity.
    end.
  end.

  open query brwActivityPolicies for each tMetricsActivity where tMetricsActivity.category = "P" by tMetricsActivity.year by tMetricsActivity.seq.
  open query brwActivityCPL      for each tMetricsActivity where tMetricsActivity.category = "T" by tMetricsActivity.year by tMetricsActivity.seq. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getAlertDesc wWin 
PROCEDURE getAlertDesc :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cValue as character no-undo.
  
  for each ttAlert:
    publish "GetSysCodeDesc" ("Alert", ttAlert.processcode, output cValue).
    ttAlert.processCodeDesc = cValue.
    publish "GetSysPropDesc" ("AMD", "Alert", "Owner", ttAlert.owner, output cValue).
    ttAlert.ownerDesc = cValue.
    publish "GetSysUserName" (ttAlert.createdBy, output cValue).
    ttAlert.createdByDesc = cValue.
    /* set the severity */
    publish "GetSysPropDesc" ("AMD", "Alert", "Severity", ttAlert.severity, output ttalert.severityDesc).
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getAlerts wWin 
PROCEDURE getAlerts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run loadAgentAlerts in hFileDataSrv.
  run getAgentAlerts in hFileDataSrv(output table ttAlert).
  
  run getAlertDesc in this-procedure.
  run setAlertCombos in this-procedure.
  run filterAlerts in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getAr wWin 
PROCEDURE getAr :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcBucket as character no-undo.
  
  find first agent no-error.
  if not available agent
   then
    return.
  
  find first ttArAging no-error.
  if not available ttArAging 
   then
    do:
      run getARAgingDetailed in hFileDataSrv (output table ttArAging,
                                              output cArAgentEmail).
      btRefreshAr:sensitive in frame frAr = true.                                                 
    end.
  run filterArAging in this-procedure(input ipcBucket).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getChangedStatus wWin 
PROCEDURE getChangedStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter ocToStatus      as character no-undo.
  define output parameter olStatusChange  as logical   no-undo.
  
  do with frame {&frame-name}:
    std-ch = hdAgentName:screen-value + " (" + hdAgentID:screen-value + ")".
  end.
  
  run dialogchangestatus.w (input  std-ch,
                            input  cAgentStatus,
                            input  {&Agent},
                            output ocToStatus,
                            output olStatusChange).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getManagerData wWin 
PROCEDURE getManagerData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter table for tagentmanager.
  run getManagerData in hFileDataSrv (output table tagentmanager).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getObjectives wWin 
PROCEDURE getObjectives :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initializeObject wWin 
PROCEDURE initializeObject PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/    
  
  /* {&window-name}:window-state = 2. /* Minimized */  */ 
  {&window-name}:max-width-pixels = session:width-pixels.
  {&window-name}:max-height-pixels = session:height-pixels.
  {&window-name}:min-width-pixels = {&window-name}:width-pixels.
  {&window-name}:min-height-pixels = {&window-name}:height-pixels.

  /* Main */
  {lib/set-button.i &frame-name=fMain &label="Refresh"       &toggle=false}
  {lib/set-button.i &frame-name=fMain &label="StatusChange"  &image="images/s-update.bmp" &inactive="images/s-update-i.bmp"}
  {lib/set-button.i &frame-name=fMain &label="Notes"         &image="images/note.bmp"     &inactive="images/note-i.bmp"}
  {lib/set-button.i &frame-name=fMain &label="ARInfo"        &image="images/dollar.bmp"   &toggle=false}
  {lib/set-button.i &frame-name=fMain &label="Follow"        &image="images/star.bmp"     &inactive="images/star-i.bmp"}
  {lib/set-button.i &frame-name=fMain &label="Consolidate"   &image="images/sum.bmp"     &inactive="images/sum-i.bmp"}
  {lib/set-button.i &frame-name=fMain &label="PDF"           &image="images/pdf.bmp"     &toggle=false}
  {lib/set-button.i &frame-name=fMain &label="AgentJournals" &image="images/log.bmp"     &toggle=false}

  /* Details */                                                                                                
  {lib/set-button.i &frame-name=frDetail &label="AgentRefresh" &image="images/s-refresh.bmp" &inactive="images/s-refresh-i.bmp" &toggle=false}
  {lib/set-button.i &frame-name=frDetail &label="Save"         &image="images/s-save.bmp"    &inactive="images/s-save-i.bmp"}
  {lib/set-button.i &frame-name=frDetail &label="Cancel"       &image="images/s-cancel.bmp"  &inactive="images/s-cancel-i.bmp"}
  {lib/set-button.i &frame-name=frDetail &label="AddImg"       &image="images/s-photo.bmp"   &toggle=false}
  {lib/set-button.i &frame-name=frDetail &label="DeleteImag"   &image="images/s-delete.bmp"  &inactive="images/s-delete-i.bmp"}
  {lib/set-button.i &frame-name=frDetail &b=btnUrl             &image="images/s-url.bmp"     &inactive="images/s-url-i.bmp"}
  {lib/set-button.i &frame-name=frDetail &label="TagRefresh"   &image="images/s-refresh.bmp" &inactive="images/s-refresh-i.bmp"}
  {lib/set-button.i &frame-name=frDetail &label="NewTag"       &image="images/s-add.bmp"     &inactive="images/s-add-i.bmp"}
  {lib/set-button.i &frame-name=frDetail &label="EditTag"      &image="images/s-update.bmp"  &inactive="images/s-update-i.bmp"}
  {lib/set-button.i &frame-name=frDetail &label="DelTag"       &image="images/s-delete.bmp"  &inactive="images/s-delete-i.bmp"}
  {lib/set-button.i &frame-name=frDetail &label="EditManager"  &image="images/s-update.bmp"  &inactive="images/s-delete-i.bmp"}
  {lib/set-button.i &frame-name=frDetail &b=btnAgentMail       &image="images/s-email.bmp"   &inactive="images/s-email-i.bmp"}

  /* Remittance */                                                                                             
  {lib/set-button.i &frame-name=frBatch &label="ActivityBatchExport"  &image="images/s-excel.bmp"     &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frBatch &label="BatchExport"          &image="images/s-excel.bmp"     &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frBatch &label="BatchOpen"            &image="images/s-magnifier.bmp" &inactive="images/s-magnifier-i.bmp"}
  {lib/set-button.i &frame-name=frBatch &b=bBatchRefresh              &image="images/s-refresh.bmp"     &inactive="images/s-refresh-i.bmp" &toggle=false}
  {lib/set-button.i &frame-name=frBatch &b=bActivityBatchRefresh      &image="images/s-refresh.bmp"     &inactive="images/s-refresh-i.bmp" &toggle=false}

  /* AR */                                                                                                     
  {lib/set-button.i &frame-name=frAr &b=btRefreshAr  &image="images/s-refresh.bmp"   &inactive="images/s-refresh-i.bmp"  &toggle=false}
  {lib/set-button.i &frame-name=frAr &b=btFileExport &image="images/s-excel.bmp"     &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frAr &b=btFileOpen   &image="images/s-magnifier.bmp" &inactive="images/s-magnifier-i.bmp"}
  {lib/set-button.i &frame-name=frAr &b=btArMail     &image="images/s-email.bmp"     &inactive="images/s-email-i.bmp"}

  /* Claim */                                                                                                  
  {lib/set-button.i &frame-name=frClaim &b=btClaimRefresh &image="images/s-refresh.bmp"   &inactive="images/s-refresh-i.bmp"  &toggle=false}
  {lib/set-button.i &frame-name=frClaim &b=btClaimExport  &image="images/s-excel.bmp"     &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frClaim &b=btClaimOpen    &image="images/s-magnifier.bmp" &inactive="images/s-magnifier-i.bmp"}
  {lib/set-button.i &frame-name=frClaim &b=btClaimMail    &image="images/s-email.bmp"     &inactive="images/s-email-i.bmp"}

  /* Audit */                                                                                                  
  {lib/set-button.i &frame-name=frAudit &b=btAuditRefresh   &image="images/s-refresh.bmp"   &inactive="images/s-refresh-i.bmp"    &toggle=false}
  {lib/set-button.i &frame-name=frAudit &b=btActionRefresh  &image="images/s-refresh.bmp"   &inactive="images/s-refresh-i.bmp"    &toggle=false}
  {lib/set-button.i &frame-name=frAudit &b=btAuditExport    &image="images/s-excel.bmp"     &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frAudit &b=btActionExport   &image="images/s-excel.bmp"     &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frAudit &b=btAuditMail      &image="images/s-email.bmp"     &inactive="images/s-email-i.bmp"}
  {lib/set-button.i &frame-name=frAudit &b=btActionMail     &image="images/s-email.bmp"     &inactive="images/s-email-i.bmp"}
  {lib/set-button.i &frame-name=frAudit &b=btAuditOpen      &image="images/s-magnifier.bmp" &inactive="images/s-magnifier-i.bmp"}
  {lib/set-button.i &frame-name=frAudit &b=btActionOpen     &image="images/s-magnifier.bmp" &inactive="images/s-magnifier-i.bmp"}

  /* Compliance */                                                                                             
  {lib/set-button.i &frame-name=frCompliance &b=btComReqFulRefresh &image="images/s-refresh.bmp"   &inactive="images/s-refresh-i.bmp"  &toggle=false}
  {lib/set-button.i &frame-name=frCompliance &b=btComQualRefresh   &image="images/s-refresh.bmp"   &inactive="images/s-refresh-i.bmp"  &toggle=false}
  {lib/set-button.i &frame-name=frCompliance &b=bReqFulExport      &image="images/s-excel.bmp"     &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frCompliance &b=btComQualExp       &image="images/s-excel.bmp"     &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frCompliance &b=btComViewreq       &image="images/s-magnifier.bmp" &inactive="images/s-magnifier-i.bmp"}
  {lib/set-button.i &frame-name=frCompliance &b=btComViewQual      &image="images/s-magnifier.bmp" &inactive="images/s-magnifier-i.bmp"}
  {lib/set-button.i &frame-name=frCompliance &b=bOrgLookup         &image="images/s-update.bmp"    &inactive="images/s-update-i.bmp"}

  /* Event */                                                                                                
  {lib/set-button.i &frame-name=frEvent &label="NewObj"       &image="images/s-add.bmp"         &toggle=false}
  {lib/set-button.i &frame-name=frEvent &label="EditObj"      &image="images/s-update.bmp"      &inactive="images/s-update-i.bmp"}
  {lib/set-button.i &frame-name=frEvent &label="CancelObj"    &image="images/s-cancel.bmp"      &inactive="images/s-cancel-i.bmp"}
  {lib/set-button.i &frame-name=frEvent &label="NewObjTh"     &image="images/s-add.bmp"         &toggle=false}
  {lib/set-button.i &frame-name=frEvent &label="EditObjTh"    &image="images/s-update.bmp"      &inactive="images/s-update-i.bmp"}
  {lib/set-button.i &frame-name=frEvent &label="CancelObjTh"  &image="images/s-cancel.bmp"      &inactive="images/s-cancel-i.bmp"}
  {lib/set-button.i &frame-name=frEvent &label="NewEvent"     &image="images/s-add.bmp"         &toggle=false}
  {lib/set-button.i &frame-name=frEvent &label="EditEvent"    &image="images/s-update.bmp"      &inactive="images/s-update-i.bmp"}
  {lib/set-button.i &frame-name=frEvent &label="EventExport"  &image="images/s-excel.bmp"       &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frEvent &label="BwdNinety"    &image="images/previouspg.bmp"    &toggle=false}
  {lib/set-button.i &frame-name=frEvent &label="BwdThirty"    &image="images/previous.bmp"      &toggle=false}
  {lib/set-button.i &frame-name=frEvent &label="FwdNinety"    &image="images/nextpg.bmp"        &toggle=false}
  {lib/set-button.i &frame-name=frEvent &label="FwdThirty"    &image="images/next.bmp"          &toggle=false}
  {lib/set-button.i &frame-name=frEvent &label="EventBf"      &image="images/time-backward.bmp" &inactive="images/time-backward-i.bmp"}
  {lib/set-button.i &frame-name=frEvent &label="EventAf"      &image="images/time-forward.bmp"  &inactive="images/time-forward-i.bmp"}
  {lib/set-button.i &frame-name=frEvent &b=bEventRefresh      &image="images/s-refresh.bmp"     &inactive="images/s-refresh-i.bmp" &toggle=false}
  
  /* Alert */                                                                                           
  {lib/set-button.i &frame-name=frAlert &b=bGo      &image="images/s-refresh.bmp"     &inactive="images/s-refresh-i.bmp" &toggle=false}
  {lib/set-button.i &frame-name=frAlert &label="Export"  &useSmall=true}
  {lib/set-button.i &frame-name=frAlert &label="Modify"  &useSmall=true}
  {lib/set-button.i &frame-name=frAlert &label="NewNote" &useSmall=true}
  
  /* Activity */                                                                                           
  {lib/set-button.i &frame-name=frActivity &label="ActivityMetricsRefresh"    &image="images/s-refresh.bmp"  &inactive="images/s-refresh-i.bmp"  &toggle=false}
  {lib/set-button.i &frame-name=frActivity &label="ActivityMetricsRefreshCPL" &image="images/s-refresh.bmp"  &inactive="images/s-refresh-i.bmp"  &toggle=false}
  {lib/set-button.i &frame-name=frActivity &label="ActivityExp"               &image="images/s-excel.bmp"   &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frActivity &label="ActivityExpCPL"            &image="images/s-excel.bmp"   &inactive="images/s-excel-i.bmp"}
    
  /* Persons */                                                                                           
  {lib/set-button.i &frame-name=frpersonagent &b=btPersonAgentRefresh           &image="images/s-refresh.bmp"    &inactive="images/s-refresh-i.bmp"   &toggle=false}
  {lib/set-button.i &frame-name=frpersonagent &label="PersonAgentExport" &image="images/s-excel.bmp"      &inactive="images/s-excel-i.bmp"}
  {lib/set-button.i &frame-name=frpersonagent &label="PersonAgentAdd"    &image="images/s-add.bmp" &inactive="images/s-add-i.bmp"}
  {lib/set-button.i &frame-name=frpersonagent &label="PersonAgentUpdate" &image="images/s-update.bmp"     &inactive="images/s-update-i.bmp"}
  {lib/set-button.i &frame-name=frpersonagent &label="PersonAgentDeactivate" &image="images/s-flag_red.bmp"     &inactive="images/s-flag-i.bmp"}
  {lib/set-button.i &frame-name=frpersonagent &b=bpersonagentlookup       &image="images/s-magnifier.bmp" &inactive="images/s-magnifier-i.bmp"}
  {lib/set-button.i &frame-name=frpersonagent &b=btnAgentPersonMail          &image="images/s-email.bmp"   &inactive="images/s-email-i.bmp"}
  setButtons().
      
  publish "GetAppCode" (output cAppCode).
  publish "GetCredentialsName" (output cCurrUser).
  publish "GetCredentialsID" (output cCurrentUID).

  /* Populating year data in widgets */
  std-in = year(today).   
  do iCount = 0 to 2:
    case iCount:
      when 0 then bYear1:label = string(std-in - iCount).
      when 1 then bYear2:label = string(std-in - iCount).
      when 2 then bYear3:label = string(std-in - iCount).
    end case.  
  end.
  {lib/pbupdate.i "'Getting users...'" 86}

  
  /* get the agent */
  if valid-handle(hFileDataSrv) 
   then    
    do:
      run GetAgent in hFileDataSrv (output table agent).
      run GetAgentManagers in hFileDataSrv (output table agentmanager).
      run GetAgentLogo in hFileDataSrv (output chimage).
      if chimage > ""
       then 
        lImage = chimage.
    end.       

  /* codes */
  {lib/get-syscode-list.i &combo=cbSwVendor &codeType="'TitleSoftware'"}
  {lib/get-syscode-list.i &combo=cbContractID &codeType="'AgentContract'"}
  /* Populate "cbRegion" combo-box */
  {lib/get-syscode-list.i &combo=cbRegion &codeType="'Region'"}
  
  
  /* state */
  {lib/get-state-list.i &combo=cbStateOper}
  {lib/get-state-list.i &combo=cbState  &useFilter=false &onlyActive=false}
  {lib/get-state-list.i &combo=cbMState &useFilter=false &onlyActive=false}
  {lib/pbupdate.i "'Displaying file data...'" 96}
  
    
  assign
     lAgentDetail = yes 
     dstartDate = today - day(today) + 1
     dEndDate   = add-interval( dstartDate, 1, "month" ) - 1 
     .
          
  setHeaderState(). 
  displayHeader(). 
  openTags().  
  displayDetail().
 
  find first agent no-error.
  if available agent                                                                                                                      
   then
    assign
        wWin:title = wWin:title + " - " + agent.name + " [" + string(agent.agentID) + "]"
        menu-item m_Following:label in menu m_Activity = (if agent.isFavorite then "Stop Following" else "Start Following")
        bFollow:tooltip in frame fMain =  menu-item m_Following:label in menu m_Activity
        .
  
  run windowResized in this-procedure. 
  run super. 
  run selectPage(1).  
  run displaymanager in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyAffiliation wWin 
PROCEDURE modifyAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable itpersonagentId as integer no-undo.

  do with frame frpersonagent:
  end.
  
  if not available tpersonagent
   then
    do:
      message "Internal Error of missing data."
          view-as alert-box error buttons ok.
      return.
    end.

  itpersonagentId = tpersonagent.personagentID.

  empty temp-table tpersonagent.

  /* Clearing Browser */
  close query brwPersonAgent.
  empty temp-table tpersonagent.

  std-ro = rowid(tpersonagent).
  
  reposition brwPersonAgent to rowid std-ro no-error.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyPersonAgent wWin 
PROCEDURE modifyPersonAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var pipersonagentID as integer no-undo.
 def var olsuccess as logical no-undo.
 
 empty temp-table ttpersonagent.
 
 if not available tpersonagent
  then
   return.
 
 pipersonagentID = tpersonagent.personagentID .
 
 create ttpersonagent.
 buffer-copy tpersonagent to ttpersonagent.

 publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  
 if std-ch = "AMD" 
  then
     do:
        publish "OpenWindow" (input 'Person',
                              input tpersonagent.personID,
                              input "wpersondetail.w",
                              input "character|input|" + tpersonagent.personID + "^integer|input|" + string(6) + "^character|input|" + '',
                              input this-procedure).
     end.
  else
    do:
       publish "OpenWindow" (input 'Person',
                             input tpersonagent.personID,
                             input "wpersondetail.w",
                             input "character|input|" + tpersonagent.personID + "^integer|input|" + string(2) + "^character|input|" + '',
                             input this-procedure).
    end.
                     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openBatchDetails wWin 
PROCEDURE openBatchDetails :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available batch 
   then return.
  
  publish "SetCurrentValue" ("AppName",  cAppCode).
  publish "OpenWindow" (input "wBatchDetail",
                        input string(batch.batchID),
                        input "wbatchdetail.w",
                        input "character|input|" + string(batch.batchID) + "^character|input|" + "" + "^character|input|" + batch.agentID,                                   
                        input this-procedure).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openDialog wWin 
PROCEDURE openDialog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openLog wWin 
PROCEDURE openLog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  do with frame {&FRAME-NAME}:
  end.

  find first agent  where agent.orgid ne "" no-error.
  if available agent
   then
    publish "OpenWindow" (input "wcompliancelog-agent",
                          input agent.agentID,
                          input "wcompliancelog-agent.w",
                          input "character|input|"  + {&OrganizationCode} + "^character|input|" + agent.orgID   + "^character|input|" + agent.state +
                                "^character|input|" + {&agent}            + "^character|input|" + agent.agentid + "^character|input|" + agent.name,
                          input this-procedure). 
  else
   do:
     message "Agent is not assigned to any Organization."
         view-as alert-box information button ok.
     return.
   end.
   

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenMaintainQar wWin 
PROCEDURE OpenMaintainQar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter pAuditId as integer no-undo.
define input parameter pWindow  as handle no-undo.

find openQars 
  where openQars.QarID = pAuditId no-error.
if not available openQars 
 then
  do:
    create openQars.
    openQars.QarID = audit.QarID.
  end.
openQars.hInstance = pWindow.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openTranDetail wWin 
PROCEDURE openTranDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  if not available tArAging
   then return.
  
  publish "OpenWindow" (input "wtransactiondetail",     /*childtype*/
                         input string(/* artran */ tArAging.artranID),   /*childid*/
                         input "wtransactiondetail.w",    /*window*/
                         input "integer|input|" + string(tArAging.artranID)  + "^integer|input|0^character|input|", /*parameters*/                               
                         input this-procedure).           /*currentProcedure handle*/ 
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE populateConsolidateTable wWin 
PROCEDURE populateConsolidateTable :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table tConsolidate.
  
  if available agent
   then
    do:
      create tConsolidate.
      assign
          tConsolidate.fldKey   = "State"
          tConsolidate.fldValue = agent.stateID
          tConsolidate.fldDesc  = entry((lookup(tConsolidate.fldValue,cbStateOper:list-item-pairs in frame frDetail) - 1),cbStateOper:list-item-pairs)
          .
      create tConsolidate.
      assign
          tConsolidate.fldKey   = "Software"
          tConsolidate.fldValue = agent.SwVendor
          tConsolidate.fldDesc  = if ( lookup(tConsolidate.fldValue,cbSwVendor:list-item-pairs) modulo 2) eq 1
                                   then 
                                     entry(lookup(tConsolidate.fldValue,cbSwVendor:list-item-pairs in frame frDetail),cbSwVendor:list-item-pairs) 
                                   else 
                                    entry((lookup(tConsolidate.fldValue,cbSwVendor:list-item-pairs in frame frDetail) - 1),cbSwVendor:list-item-pairs).
          . 
      create tConsolidate.
      assign
          tConsolidate.fldKey   = "Year of Signing"
          tConsolidate.fldValue = if agent.contractDate = ? then "" else string(year(agent.contractDate)) 
          tConsolidate.fldDesc  = if agent.contractDate = ? then "" else string(year(agent.contractDate))
          .
          
      create tConsolidate.
      assign
          tConsolidate.fldKey   = "Organization"
          tConsolidate.fldValue = if integer(agent.orgID) > 0 
                                   then 
                                    (if agent.orgID = "" or agent.orgID = ? then "" else agent.orgID) 
                                   else "" 
          tConsolidate.fldDesc  = if getOrgName(agent.orgID) = ? then "" else getOrgName(agent.orgID)
          .   
         
      create tConsolidate.
      assign
          tConsolidate.fldKey   = "Company"
          tConsolidate.fldValue = string(agent.corporationID)
          tConsolidate.fldDesc  = string(agent.corporationID)
          .
     create tConsolidate.
     assign
         tConsolidate.fldKey   = "Manager"
         tConsolidate.fldValue = agent.manager   
         tConsolidate.fldDesc  = flOManager:input-value
         .
         
     for each tag:
       create tConsolidate.
       assign
           tConsolidate.fldKey   = "Tag"
           tConsolidate.fldValue = tag.name   
           tConsolidate.fldDesc  = tag.name
           .
     end.
     
/*      run getAgentpersonagents in hFileDataSrv(output table tpersonagent).   */
/*                                                                             */
/*      for each tpersonagent where tpersonagent.partnerBType = {&PersonCode}: */
/*        create tConsolidate.                                                 */
/*        assign                                                               */
/*            tConsolidate.fldKey   = "tpersonagent"                           */
/*            tConsolidate.fldValue = getEntityID(tpersonagent.partnerBType,   */
/*                                                tpersonagent.partnerA,       */
/*                                                tpersonagent.partnerB)       */
/*                                                                             */
/*            tConsolidate.fldDesc  = getEntityName(tpersonagent.partnerB,     */
/*                                                  tpersonagent.partnerBType, */
/*                                                  tpersonagent.partnerAName, */
/*                                                  tpersonagent.partnerBName) */
/*            .                                                                */
/*      end.                                                                   */

    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreashaudit wWin 
PROCEDURE refreashaudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  find first agent no-error.
  if not available agent
   then
    return .
  
  run server/getallaudits.p (input 0,         /* year */
                               input agent.stateid,  /* state */
                               input agent.agentid,  /* agent */
                               input "ALL",     /* auditor */
                               input "ALL",     /* status */
                               input 0,         /* start gap */
                               input 0,         /* audit gap */
                               output table audit,
                               output std-lo,
                               output std-ch
                               ).
 if not std-lo
   then
      message std-ch 
        view-as alert-box.
   else 
    
 for each audit:
   assign
       audit.stat      = getAuditStatus(audit.stat)
       audit.audittype = getAuditType(audit.audittype)
       audit.errtype   = getErrorType(audit.errtype)
       .
 end.
 open query brwQAR for each audit no-lock by audit.auditStartDate desc.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshaction wWin 
PROCEDURE refreshaction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run loadActions in hFileDataSrv.
  run getActions in hFileDataSrv(output  table  action,
                                 output  table  finding
                                ).
                                 
  open query brwAction for each action.    

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshAgentPerson wWin 
PROCEDURE refreshAgentPerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply 'choose' to btPersonAgentRefresh in frame frpersonagent.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshAgentTimeline wWin 
PROCEDURE refreshAgentTimeline :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcAgentID as character no-undo.
  
  if not available agent or     
     string(agent.agentID) ne ipcAgentID 
   then
    return.
  
  run getObjectives in hFileDataSrv (input {&ActiveStat},              /* (A)ctive */ 
                                     output table objective,
                                     output table sentiment).
                                     
  run displayObjWidgets in this-procedure.                                     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshAR wWin 
PROCEDURE refreshAR :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  find first agent no-error.
  if not available agent
   then
    return.
    
  if cLastAgingClicked = ""
   then
    cLastAgingClicked = "Current".
    
  openAr(input true).
  displayAR().
  
  if not available arAging 
   then
    return.
    
  if lArAging
   then
    do:
      run refreshARAgingDetailed in hFileDataSrv (output table ttArAging,
                                                  output cArAgentEmail).
                                          
      run filterArAging in this-procedure(input cLastAgingClicked).
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshclaim wWin 
PROCEDURE refreshclaim :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  lRefreshClaims = true.
  displayClaim().
  openClaim().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData wWin 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipiActivePage as integer no-undo.

  run emptyTempTable in hFileDataSrv.
  run emptyTempTable in this-procedure.
  
  run GetAgent in hFileDataSrv (output table agent). 
  
  
    
  case ipiActivePage:
    when 1
     then
      do with frame frDetail:
        lRefresh = true.
        apply 'choose' to bAgentRefresh.
        apply 'choose' to bTagRefresh  .        
      end.
    when 2
     then
      do with frame frpersonagent: 
        apply 'choose' to btPersonAgentRefresh.
      end.
    when 3
     then
      do with frame frEvent:
        run LoadObjectives in hFileDataSrv (input {&ActiveStat}).
        apply 'choose' to bEventRefresh.
      end.
    when 4
     then
      do with frame frBatch:
        apply 'choose' to bActivityBatchRefresh.
        if lBatch 
         then
          apply 'choose' to bBatchRefresh .
      end.
    when 5
     then
      do with frame frActivity:
        apply 'choose' to bActivityMetricsRefresh.
      end.
    when 6
     then
      do with frame frAr:
        apply 'choose' to btRefreshAr.
      end.
    when 7
     then
      do with frame frClaim: 
        lRefreshClaimsSummary = true.
        apply 'choose' to btClaimRefresh.
      end.
    when 8
     then
      do with frame frAudit:
        apply 'choose' to btAuditRefresh.
        apply 'choose' to btActionRefresh.
      end.
    when 9
     then
      do with frame frCompliance:
        
        displayCompliance().
        apply 'choose' to btComQualRefresh.
        apply 'choose' to btComReqFulRefresh.
      end.
    when 10
     then
      do with frame frAlert:
        apply 'choose' to bGo.
      end.
  end case.
  lRefresh = false.
  run enableButtons in this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshTags wWin 
PROCEDURE refreshTags :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  find first agent no-error.
  if not available agent
   then
    return.
    
  run LoadUserTags in hFileDataSrv(input "A", input agent.agentID).
  
  openTags().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE selectAudit wWin 
PROCEDURE selectAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter iQarID as integer.
define variable qarsummaryhandle as handle.
find first audit where audit.QarId = iQarID no-error.
if not available audit 
 then return.

              
  create ttaudit.
  assign
    ttaudit.QarID  =      audit.QarID  
    ttaudit.agentID =     audit.agentID  
    ttaudit.stat =        audit.stat
    ttaudit.auditScore =  audit.auditScore
    ttaudit.name =        audit.name 
    ttaudit.addr =        audit.addr 
    ttaudit.city =        audit.city 
    ttaudit.state =       audit.state 
    ttaudit.zip =         audit.zip 
    .

run checkOpenQar(input audit.QarId,
                        output std-lo,
                        output qarsummaryhandle ).
if std-lo
 then
  do:
     run ViewWindow in qarsummaryhandle no-error.
    empty temp-table ttaudit.
    return.
  end.
else
  do:
    
    if audit.audittype = "U" then
      run QURSummary.w persistent ( input table ttaudit ).
    else
      run QARSummary.w persistent ( input table ttaudit ).
     run ViewWindow in qarsummaryhandle no-error.   
    
    empty temp-table ttaudit.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE selectPage wWin 
PROCEDURE selectPage :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter piPageNum as integer no-undo.
  iLastPage = piPageNum.
  
  /* Code placed here will execute PRIOR to standard behavior. */

  /* 
  Prevent user from changing pages if something has been modified on the
  current page.  Prompt if they want to save or discard, then handle the
  choice accordingly.
  
  Saving changes needs to be page specific. Saving all the data on all
  pages will take too much time.
  */
  std-lo = checkDataChanged().
  if not std-lo 
   then
    return no-apply.     
  
  RUN SUPER( INPUT piPageNum).

  /* Code placed here will execute AFTER standard behavior.    */
  
  activePageNumber = piPageNum.
  
  saveStateDisable(activePageNumber).
  
  /* Disable editing if claim is closed */
  if not avail agent 
   then
    setPageState(activePageNumber, "Enabled").
    
  if piPageNum = 1 then
    apply 'value-changed' to brwTags in frame frDetail.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setAgentStatus wWin 
PROCEDURE setAgentStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pStat as character no-undo.
  
  define variable lSuccess as logical   no-undo.
  define variable cStatus  as character no-undo.
    
  run value("SetAgentStatus" + pStat) in hFileDataSrv (hdAgentID:screen-value in frame {&frame-name}, output lSuccess).
  
  if lSuccess
   then
    do:
      if cAgentStatus eq "A" and pStat ne "A" then
        message "The status of the agent is changed from Active to " +  agentStat(pStat) + ". Compliance will not be tracked."
          view-as alert-box information buttons ok.

      else if cAgentStatus ne "A" and pStat = "A" then
        message "The status of the agent is changed from " + agentStat(cAgentStatus) + " to Active. Compliance will be tracked."
          view-as alert-box information buttons ok.
      
      
      dataChanged = false.
      cAgentStatus = pStat.     
      doModify(false).
      run ShowWindow in this-procedure.
      run refreshTags in this-procedure.
    end.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setAlertCombos wWin 
PROCEDURE setAlertCombos :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*   define variable cValue as character no-undo. */
  
  do with frame frAlert:
  end.
  
  /* Alert type combo */
  fCode:list-item-pairs = "ALL,ALL".

  for each ttAlert break by ttAlert.processcode:
    if first-of(ttAlert.processcode) 
     then
      fCode:list-item-pairs = fCode:list-item-pairs + "," + ttAlert.processCodeDesc + "," + ttAlert.processcode.
  end.
  fCode:screen-value = if cLastAlertCode = ? then "ALL" else cLastAlertCode.

  
  /* Owner combo */
  fOwner:list-item-pairs = "ALL,ALL".

  for each ttAlert break by ttAlert.owner:
    if first-of(ttAlert.owner) 
     then
      fOwner:list-item-pairs = fOwner:list-item-pairs + "," + ttAlert.ownerDesc + "," + ttAlert.owner.
  end.
  fOwner:screen-value = if cLastAlertOwner = ? then "ALL" else cLastAlertOwner.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setArInfo wWin 
PROCEDURE setArInfo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  run dialogAgentArInfo.w(input-output cMethod,
                          input-output iDueInDays,
                          input-output iDueOnDay,
                          input-output cCashAcc,
                          input-output cCashAccDesc,
                          input-output cARAcc,
                          input-output cARAccDesc,
                          input-output cRefARAcc,
                          input-output cRefARAccDesc,
                          input-output cWroffARAcc,
                          input-output cWroffARAccDesc,
                          output std-lo).     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setContainerState wWin 
PROCEDURE setContainerState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  define input parameter pContainer as handle no-undo.
  define input parameter pState     as character no-undo.
  
  define variable hObject as handle no-undo.
  define variable hChild  as handle no-undo.

  if valid-handle(pContainer)
   then run setWidgetState (pContainer, pState).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setLogo wWin 
PROCEDURE setLogo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iImgheight as integer no-undo.
  define variable iImgwidth  as integer no-undo.

  if valid-handle(IMAGE-2) then
    delete object IMAGE-2.

  create image IMAGE-2.
  IMAGE-2:frame = frame frLogo:handle.
  IMAGE-2:load-image(chPath) no-error.
  image-2:transparent = true.
  
  assign iImgheight = IMAGE-2:width-pixel
         iImgwidth  = IMAGE-2:height-pixel.
  
  IMAGE-2:load-image("") no-error.

  if iImgheight > frame frLogo:width-pixel then
    IMAGE-2:width-pixel = frame frLogo:width-pixel - 5.
  else
    image-2:x = image-2:frame-x + ( frame frLogo:width-pixels / 2 ) - ( image-2:width-pixels / 2 ) - 2 no-error.
  
  if iImgwidth > frame frLogo:height-pixel then
    IMAGE-2:height-pixel = frame frLogo:height-pixel - 5.
  else
    image-2:y = image-2:frame-y + ( frame frLogo:height-pixels / 2 ) - ( image-2:height-pixels / 2 ) - 2 no-error.

  IMAGE-2:load-image(chPath) no-error.

  assign IMAGE-2:stretch-to-fit = true
         IMAGE-2:resizable      = true
         IMAGE-2:hidden         = false no-error.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetManager wWin 
PROCEDURE SetManager :
/*------------------------------------------------------------------------------
@description Sets the primary manager
------------------------------------------------------------------------------*/
  define input parameter table for agentmanager.
  define output parameter pSuccess  as logical   no-undo.  
  
  do with frame frDetail:
  end.
  cManagerUID = "".
  for first agentmanager no-lock
      where agentmanager.isPrimary = true:
  
    assign
      std-ch = ""
      cManagerUID = agentmanager.uid
      .
    publish "GetSysUserName" (cManagerUID, output std-ch).
    do with frame {&frame-name}:
      assign flOManager:screen-value  = std-ch.
    end.
  end.
  run ModifyAgentManager in hFileDataSrv (table agentmanager,input std-lo,output pSuccess). 
  if cManagerUID = ""
    then flOManager:screen-value = "None".    
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState wWin 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  define input parameter pObject as handle    no-undo.
  define input parameter pState  as character no-undo.
  
  define variable hNextObject    as handle    no-undo.

  /* Drill down child object chain first */
  hNextObject = pObject:first-child no-error.
  
  if valid-handle(hNextObject)
  then run setWidgetState (hNextObject, pState).
  /* Check for siblings and their children.
  */
  hNextObject = pObject:next-sibling no-error.
  if valid-handle(hNextObject) then
  do:
    /* Do not process any sibling frames or their children */
    if hNextObject:type <> "FRAME" then
    run setWidgetState (hNextObject, pState).
  end.
  
  /* This object does not have any children or siblings, or the children or
  siblings have already been processed in the recursive call. Process
  object information now.
  */
  
  if pObject:type <> ? then /* Should never be the case, but just to be safe */
  case pState:
    when "Enabled" then
    do:
      if lookup(pObject:type,"EDITOR,FILL-IN") <> 0 then
      do:        
        if pObject:private-data = "READ-ONLY" then
        assign
          pObject:sensitive = yes
          pObject:read-only = yes.
        else        
        assign
          pObject:sensitive = yes
          pObject:read-only = no.
      end.
      
      if lookup(pObject:type,"COMBO-BOX,RADIO-SET,SELECTION-LIST,TOGGLE-BOX") <> 0 then
      do:
        if pObject:private-data = "READ-ONLY" then
        assign
          pObject:sensitive = no.
        else
        assign
          pObject:sensitive = yes.
      end.
    end.
    when "Disabled" then
    do:
      if lookup(pObject:type,"EDITOR,FILL-IN") <> 0 then
      do:
        assign
          pObject:sensitive = yes
          pObject:read-only = yes.
      end.
      
      if lookup(pObject:type,"COMBO-BOX,RADIO-SET,SELECTION-LIST,TOGGLE-BOX") <> 0 then
      do:
        assign
          pObject:sensitive = no.
      end.
    end.
    when "ReadOnly" then
    do:
      if pObject:type = "BUTTON" then
      do:
        if pObject:private-data <> "STATIC" then
        pObject:sensitive = no.
      end.
      
      if lookup(pObject:type,"EDITOR,FILL-IN") <> 0 then
      do:
        assign
          pObject:sensitive = yes
          pObject:read-only = yes.
      end.
      
      if lookup(pObject:type,"COMBO-BOX,RADIO-SET,SELECTION-LIST,TOGGLE-BOX") <> 0 then
      do:
        assign
          pObject:sensitive = no.
      end.
    end.
  end case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showTimelineEvents wWin 
PROCEDURE showTimelineEvents :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter dStDate as date no-undo.
  define input parameter dFnDate as date no-undo.
  
  define variable cSuffix   as character no-undo.
  define variable iAnnivNum as integer   no-undo.
  define variable dEventDt  as date      no-undo.
  
  close query brwEvent.
  empty temp-table tlEvent.
  
  bTimeline:label  in frame frEvent = string(dStDate,"99/99/9999") + " - " + string(dFnDate,"99/99/9999").
      
  for each event where (date(event.date) ge dStDate    and 
                        date(event.date) le dFnDate
                       )
                       or
                       (event.reminder):
                       
    /* for recurring events, if event date lies in the timeline, create its record with updated year */                   
    if event.reminder 
     then
      do:
        dEventDt = date( (if month(event.date) < 10 then "0" else "")  + string(month(event.date))   +   
                         (if day(event.date)   < 10 then "0" else "")  + string(day(event.date))     +   
                        string(if month(event.date) ge month(dStDate) then year(dStDate) else year(dFnDate))
                       )
                       .
                       
        if (dEventDt ge dStDate) and 
           (dEventDt le dFnDate) 
         then
          do:
            create tlEvent.
            buffer-copy event except event.date to tlEvent.
            tlEvent.date = dEventDt.
            
            
            /* Add anniversary number in description if year is given but it is not current year(not to show 0th Anniversary)  */
            if tlEvent.reminder   and
               tlEvent.year ne 0  and       
               tlEvent.year lt year(today)  
             then
              do:
                assign
                    iAnnivNum         = year(tlEvent.date) - tlEvent.year
                    cSuffix           = if (integer(iAnnivNum / 10) eq 1)                        or
                                           ((iAnnivNum mod 10 ge 4) and (iAnnivNum mod 10 le 9)) or
                                           (iAnnivNum mod 10 eq 0)
                                         then "th"
                                         else if iAnnivNum mod 10 eq 1
                                          then
                                           "st"
                                         else if iAnnivNum mod 10 eq 2
                                          then
                                           "nd"
                                         else
                                          "rd"
                    tlEvent.description = string(iAnnivNum) + cSuffix + ' Anniversary.' + tlEvent.description.
              end.
          end.
    
      end.
     else
      do:
        create tlEvent.
        buffer-copy event to tlEvent.
      end.
      
    if available tlEvent
     then
      assign 
          tlEvent.days = date(tlEvent.date) - today.
        
  end.
  
  /* create record for today if there is no record with today's date in current timeline */
  if today ge dStDate     and 
     today le dFnDate     and 
     not can-find(first tlEvent where date(tlEvent.date) = today) 
   then
    do:
      create tlEvent.
      assign
          tlEvent.name     = "Today"
          tlEvent.month    = month(today)
          tlEvent.day      = day(today)
          tlEvent.year     = year(today)
          tlEvent.date     = today
          tlEvent.reminder = no
          tlEvent.days     = 0
          .
    end.
    
  open query brwEvent for each tlEvent by tlEvent.date.
  
  /* Show indicators if there are non recurring events after the end date */
  if can-find(first event where event.reminder = false and 
              date(event.date) gt dFnDate)
   then
    bEventAf:sensitive in frame frEvent = true.
   else
    bEventAf:sensitive = false.

  /* Show indicators if there are non recurring events before the start date */  
  if can-find(first event where event.reminder = false and 
              date(event.date) lt dStDate)
   then
    bEventBf:sensitive in frame frEvent = true.
   else
    bEventBf:sensitive = false.  

  if iTimeFrame = 0 
   then
    bCollapse:sensitive = false.  
   else
    bCollapse:sensitive = true.
    
    
  if iTimeFrame = 3
   then
    bExpand:sensitive = false.
   else
    bExpand:sensitive = true.

  assign
      bEditEvent:sensitive   = can-find(first tlEvent where tlEvent.name ne "Today")
      bEventExport:sensitive = bEditEvent:sensitive
      .
  apply 'value-changed' to brwEvent.  
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow wWin 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData wWin 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE updateOrganization wWin 
PROCEDURE updateOrganization :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  define variable cOrgID     as character no-undo.
  define variable cName      as character no-undo.
  define variable lSuccess   as logical   no-undo.
  define variable cPrevOrgID as character no-undo.
  define variable psuccess   as logical   no-undo.
  define variable cAction    as character no-undo.
  define variable iOrgRoleID as integer   no-undo.
  define variable oOrgID     as character no-undo.
  
  do with frame frCompliance :
  end.
  
  if available agent 
   then
    cPrevOrgID = agent.orgID. 
    
  run dialogorganizationlookup.w(input cPrevOrgID,
                                 input yes,
                                 input yes,
                                 input no,
                                 output cOrgID,
                                 output cName,
                                 output lSuccess).
   
  if not lSuccess 
   then
    return no-apply.

  if cPrevOrgID <> cOrgID and agent.agentID ne ""
   then
    do:
      if cOrgId  = "-99"
       then
        cAction = "New" .
       else
        cAction = "Modify" .
      
      run updateOrganization in hFileDataSrv( input cAction,
                                              input agent.agentID,
                                              input cOrgID,
                                              output oOrgID,
                                              output iOrgRoleID,
                                              output pSuccess ).          
      if not pSuccess 
       then
        return.
     
      if cOrgId = "-99" and (oOrgID <> "" or oOrgID <> ?)
       then
        do:
          run createOrganization in this-procedure(input oOrgID).
          assign
              cName = getOrgName(oOrgID)
              cOrgID = oOrgID
              .
        end.    
        
      assign
          agent.orgID     = cOrgID
          agent.orgRoleID = iOrgRoleID
          cOrigOrg        = cOrgID.
      
      
    end.
  
    assign
        tOrganization:screen-value = if cOrgID = ?  then "" else cOrgID 
        tOrganization:private-data = if cOrgID = ?  then "" else cOrgID
        tOrgName:screen-value      = if cName = "?" then "" else cName 
        .

   apply "CHOOSE" to btComQualRefresh   in frame frCompliance.
   apply "CHOOSE" to btComReqFulRefresh in frame frCompliance.
   if available tpersonagent or lpersonagent
    then
     apply "CHOOSE" to btPersonAgentRefresh in frame frpersonagent.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewComQualification wWin 
PROCEDURE viewComQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frcompliance:
  end.

if not available qualification
   then
    do:
      message "Internal Error of missing data."
          view-as alert-box error buttons ok.
      return.
    end.
    
  run dialogviewqualification-agent.w(input qualification.entity,
                                      input qualification.EntityId,
                                      input qualification.StateId,
                                      input qualification.qualificationID)
                                      .

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewComRequirement wWin 
PROCEDURE viewComRequirement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable oplsuccess as logical   no-undo.
  define variable opcMsg     as character no-undo.
  
  do with frame {&frame-name}:
  end.
 
  if not available reqfulfill
   then
    do:
      message "Internal Error of missing data."
          view-as alert-box error buttons ok.
      return.
    end.
  
  /* Get the records in temp-table, so that in case any error comes, then the local data instance is not deleted. */
  run server/getrequirements.p (input  reqfulfill.stateID,
                                input  reqfulfill.requirementID,
                                output table StateRequirement,
                                output table StateReqQual,
                                output oplSuccess,
                                output opcMsg).

  if not oplSuccess 
   then
    do:
      message opcMsg 
          view-as alert-box error buttons ok.
      return.
    end.
                            
  /* Populate the return temp-table from the cached data. */
  for each stateRequirement where stateRequirement.requirementID = reqfulfill.requirementId:
    create tStateRequirement.
    buffer-copy stateRequirement to tStateRequirement.
  end.
  
  run dialogviewrequirement-agent.w(input table tStateRequirement).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized wWin 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  frame fMain:width-pixels = {&window-name}:width-pixels.
  frame fMain:virtual-width-pixels = frame fMain:width-pixels.

  frame fMain:height-pixels = {&window-name}:height-pixels.
  frame fMain:virtual-height-pixels = frame fMain:height-pixels.

  run showCurrentPage in h_folder(activePageNumber).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION adjustFormattedValue wWin 
FUNCTION adjustFormattedValue RETURNS LOGICAL
  ( input ipcAmount as character,
    input haWidget  as handle) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/    
  case length(ipcAmount):
    when 1 then 
      assign
          haWidget:format = if decimal(ipcAmount) > 0 then "ZZZ,ZZZ,ZZZ,ZZZ,ZZZ" else "ZZZ,ZZZ,ZZZ,ZZZ,ZZ9"
          haWidget:x      = haWidget:x + 0.5
          .
    when 2 then 
      assign
          haWidget:format = "ZZ,ZZZ,ZZZ,ZZZ,ZZZ"
          haWidget:x      = haWidget:x - 1.5
          . 
    when 3 then 
      assign
          haWidget:format = "Z,ZZZ,ZZZ,ZZZ,ZZZ"
          haWidget:x      = haWidget:x - 1.5
          .
    when 4 then 
      assign
          haWidget:format = "ZZZ,ZZZ,ZZZ,ZZZ"
          haWidget:x      = haWidget:x + 2
          .
    when 5 then 
      assign
          haWidget:format = "ZZ,ZZZ,ZZZ,ZZZ"
          haWidget:x      = haWidget:x + 2
          . 
    when 6 then 
      assign
          haWidget:format = "Z,ZZZ,ZZZ,ZZZ"
          haWidget:x      = haWidget:x + 2
          .
    when 7 then 
      assign
          haWidget:format = "Z,ZZZ,ZZZ,ZZZ"
          haWidget:x      = haWidget:x - 1.5
          . 
    when 8 then 
      assign
          haWidget:format = "ZZZ,ZZZ,ZZZ"
          haWidget:x      = haWidget:x + 2
          . 
    when 9 then 
      assign
          haWidget:format = "ZZZ,ZZZ,ZZZ"
          haWidget:x      = haWidget:x - 1
          .  
  end case.
               
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION agentStat wWin 
FUNCTION agentStat RETURNS CHARACTER
  ( input cStat as character /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable pReturn as character no-undo.
  
  publish "GetSysPropDesc" ("AMD", "Agent", "Status", cStat, output pReturn).
  return pReturn.
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION checkActivity wWin 
FUNCTION checkActivity RETURNS CHARACTER
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define buffer ttactivity for ttactivity.
  
  empty temp-table tempactivity. 
  
  if not can-find (first ttactivity where ttactivity.year = pYear 
                                      and ttActivity.category = pCategory
                                      and ttactivity.type = pType)
   then
    do:
      for first ttActivity:
        create tempactivity.
        buffer-copy ttactivity to tempactivity.
        createActivityRow(pSeq, pType, pCategory, pYear, table tempactivity).
      end.
    end.
  return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION checkActivityMetrics wWin 
FUNCTION checkActivityMetrics RETURNS CHARACTER
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define buffer ttactivityMetrics for ttactivityMetrics.
  
  empty temp-table tempactivity. 
  
  if not can-find (first ttactivityMetrics where ttactivityMetrics.year = pYear 
                                             and ttActivityMetrics.category = pCategory
                                             and ttactivityMetrics.type = pType)
   then
    do:
      for first ttActivityMetrics:
        create tempactivity.
        buffer-copy ttactivityMetrics to tempactivity.
        createActivityRowMetrics(pSeq, pType, pCategory, pYear, table tempactivity).
      end.
    end.
  return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION checkDataChanged wWin 
FUNCTION checkDataChanged RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
  if dataChanged then
  do:
    if iLastPage = activePageNumber then
     return false.
    std-lo = ?.
    message
      "Data has been modified on the current tab." skip
      "Do you want to save the changes before continuing?"
      skip(1)
      "Choices:" skip
      "'Yes' will save the changes" skip 
      "'No' will discard the changes" skip
      "'Cancel' will take no action and remain on the current tab"
      view-as alert-box warning buttons yes-no-cancel
      update std-lo.
      
    if std-lo = ? then
    return false.
    else
    if std-lo = yes then
    savePage(activePageNumber).
    else
    if std-lo = no then
    discardPage(activePageNumber).
    else
    return false.
  end.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION checkOpenPeriod wWin 
FUNCTION checkOpenPeriod RETURNS LOGICAL
  ( input ipiMonth as integer,
    input ipiYear as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable lOpen as logical no-undo.
    
  publish "IsPeriodOpen" (input ipiMonth,
                          input ipiYear,
                          output lOpen).
                          
  return lOpen.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createActivity wWin 
FUNCTION createActivity RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  empty temp-table tCActivity.
    create tCActivity.
    assign
        tCActivity.activityID    = 0
        tCActivity.seq           = pSeq
        tCActivity.type          = pType
        tCActivity.category      = pCategory
        tCActivity.year          = pYear
        tCActivity.month1        = 0
        tCActivity.month2        = 0
        tCActivity.month3        = 0
        tCActivity.month4        = 0
        tCActivity.month5        = 0
        tCActivity.month6        = 0
        tCActivity.month7        = 0
        tCActivity.month8        = 0
        tCActivity.month9        = 0
        tCActivity.month10       = 0
        tCActivity.month11       = 0
        tCActivity.month12       = 0
        tCActivity.qtr1          = 0
        tCActivity.qtr2          = 0
        tCActivity.qtr3          = 0
        tCActivity.qtr4          = 0
        tCActivity.yrTotal       = 0
        tCActivity.typeDesc      = getActivityType(pType)
        .    
  for first tCActivity:
    create ttActivity.
    buffer-copy tCActivity to ttActivity.
  end.
  return true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createActivityMetrics wWin 
FUNCTION createActivityMetrics RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  empty temp-table tCActivity.
    create tCActivity.
    assign
        tCActivity.activityID    = 0
        tCActivity.seq           = pSeq
        tCActivity.type          = pType
        tCActivity.category      = pCategory
        tCActivity.year          = pYear
        tCActivity.month1        = 0
        tCActivity.month2        = 0
        tCActivity.month3        = 0
        tCActivity.month4        = 0
        tCActivity.month5        = 0
        tCActivity.month6        = 0
        tCActivity.month7        = 0
        tCActivity.month8        = 0
        tCActivity.month9        = 0
        tCActivity.month10       = 0
        tCActivity.month11       = 0
        tCActivity.month12       = 0
        tCActivity.qtr1          = 0
        tCActivity.qtr2          = 0
        tCActivity.qtr3          = 0
        tCActivity.qtr4          = 0
        tCActivity.yrTotal       = 0
        tCActivity.typeDesc      = getActivityType(pType)
        .    
  for first tCActivity:
    create ttActivityMetrics.
    buffer-copy tCActivity to ttActivityMetrics.
  end.
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createActivityRow wWin 
FUNCTION createActivityRow RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer,
    input table for tempActivity ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  empty temp-table tCActivity.
  for first tempActivity no-lock:
    create tCActivity.
    assign
        tCActivity.activityID    = integer(tempActivity.agentID)
        tCActivity.seq           = pSeq
        tCActivity.agentID       = tempActivity.agentID
        tCActivity.showID        = tempActivity.showID
        tCActivity.stateID       = tempActivity.stateID
        tCActivity.corporationID = tempActivity.corporationID
        tCActivity.name          = tempActivity.name
        tCActivity.showName      = tempActivity.showName
        tCActivity.type          = pType
        tCActivity.category      = pCategory
        tCActivity.year          = pYear
        tCActivity.month1        = 0
        tCActivity.month2        = 0
        tCActivity.month3        = 0
        tCActivity.month4        = 0
        tCActivity.month5        = 0
        tCActivity.month6        = 0
        tCActivity.month7        = 0
        tCActivity.month8        = 0
        tCActivity.month9        = 0
        tCActivity.month10       = 0
        tCActivity.month11       = 0
        tCActivity.month12       = 0
        tCActivity.qtr1          = 0
        tCActivity.qtr2          = 0
        tCActivity.qtr3          = 0
        tCActivity.qtr4          = 0
        tCActivity.yrTotal       = 0
        tCActivity.typeDesc      = getActivityType(pType)
        .    
  end.
  for first tCActivity:
    create ttActivity.
    buffer-copy tCActivity to ttActivity.
  end.
  return true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createActivityRowMetrics wWin 
FUNCTION createActivityRowMetrics RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input pCategory as character,
    input pYear as integer,
    input table for tempActivity ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  empty temp-table tCActivity.
  for first tempActivity no-lock:
    create tCActivity.
    assign
        tCActivity.activityID    = integer(tempActivity.agentID)
        tCActivity.seq           = pSeq
        tCActivity.agentID       = tempActivity.agentID
        tCActivity.showID        = tempActivity.showID
        tCActivity.stateID       = tempActivity.stateID
        tCActivity.corporationID = tempActivity.corporationID
        tCActivity.name          = tempActivity.name
        tCActivity.showName      = tempActivity.showName
        tCActivity.type          = pType
        tCActivity.category      = pCategory
        tCActivity.year          = pYear
        tCActivity.month1        = 0
        tCActivity.month2        = 0
        tCActivity.month3        = 0
        tCActivity.month4        = 0
        tCActivity.month5        = 0
        tCActivity.month6        = 0
        tCActivity.month7        = 0
        tCActivity.month8        = 0
        tCActivity.month9        = 0
        tCActivity.month10       = 0
        tCActivity.month11       = 0
        tCActivity.month12       = 0
        tCActivity.qtr1          = 0
        tCActivity.qtr2          = 0
        tCActivity.qtr3          = 0
        tCActivity.qtr4          = 0
        tCActivity.yrTotal       = 0
        tCActivity.typeDesc      = getActivityType(pType)
        .    
  end.
  for first tCActivity:
    create ttActivityMetrics.
    buffer-copy tCActivity to ttActivityMetrics.
  end.
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION discardPage wWin 
FUNCTION discardPage RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  displayHeader().

  case pPageNumber:
    when 1 then
    do:
      displayDetail().
    end.
    when 2 then
    do:
    end.
    when 3 then
    do:
      displayAR().
    end.
    when 4 then
    do:
      displayClaim().
    end.
    when 5 then
    do:
      displayAudit().
    end.
    when 6 then
    do:
      displayCompliance().
    end.
    when 7 then
    do:
      
    end.
    when 8 then
    do:
      displayObjective().
    end.
    when 9 then
    do:
      displayEvent().
    end.
    when 10 then
    do:
      displayAlert().
    end.
  end case.
  
  saveStateDisable(pPageNumber).
  
  RETURN true.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayAlert wWin 
FUNCTION displayAlert RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayAR wWin 
FUNCTION displayAR RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 do with frame frAr:
 
   find first araging no-error.
   if not available araging 
    then 
     return false.
    
   assign
       bCurrentaging:label = "$" + string(araging.due0_30 , (if araging.due0_30  = 0 then "->,>>>,>>>,>>9" else "->,>>>,>>>,>>9.99"))  
       b30to60:label       = "$" + string(araging.due31_60, (if araging.due31_60 = 0 then "->,>>>,>>>,>>9" else "->,>>>,>>>,>>9.99"))   
       b60to90:label       = "$" + string(araging.due61_90, (if araging.due61_90 = 0 then "->,>>>,>>>,>>9" else "->,>>>,>>>,>>9.99"))   
       bover90:label       = "$" + string(araging.due91_  , (if araging.due91_   = 0 then "->,>>>,>>>,>>9" else "->,>>>,>>>,>>9.99"))     
       bbalance:label      = "$" + string(araging.balance , (if araging.balance  = 0 then "->,>>>,>>>,>>9" else "->,>>>,>>>,>>9.99"))
       .
   assign 
       bCurrentaging:bgcolor = if araging.due0_30  >= 0 then 2 else 12
       b30to60:bgcolor       = if araging.due31_60 >= 0 then 2 else 12
       b60to90:bgcolor       = if araging.due61_90 >= 0 then 2 else 12
       bover90:bgcolor       = if araging.due91_   >= 0 then 2 else 12
       bbalance:bgcolor      = if araging.balance  >= 0 then 2 else 12
       .
 end.
  
 return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayAudit wWin 
FUNCTION displayAudit RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayClaim wWin 
FUNCTION displayClaim RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  do with frame frClaim :
  end.
  if lRefreshClaimsSummary
   then
    run loadClaimCostsSummary in hFileDatasrv.
    
  run getclaimcostssummary in hFileDatasrv(output table claimcostssummary). 
                                     
  for each claimcostssummary:
    case claimcostssummary.period:
     when 0
      then
       do:
         assign
             netfromLTD  :screen-value = string(claimcostssummary.netpremium) 
             costfromLTD :screen-value = string(claimcostssummary.claimCosts)
             errorfromLTD:screen-value = string(claimcostssummary.claimCostsAE)
             costnetLTD  :screen-value = if claimcostssummary.costRatio   <= 0 then "" else string(claimcostssummary.costRatio * 100)   + "%"
             errornetLTD :screen-value = if claimcostssummary.costRatioAE <= 0 then "" else string(claimcostssummary.costRatioAE * 100) + "%"                         
             .
       end.
     when 1                    
      then
       do:
         assign
             netfrom12  :screen-value = string(claimcostssummary.netpremium)
             costfrom12 :screen-value = string(claimcostssummary.claimCosts)
             errorfrom12:screen-value = string(claimcostssummary.claimCostsAE)
             costnet12  :screen-value = if claimcostssummary.costRatio   <= 0 then "" else string(claimcostssummary.costRatio * 100)   + "%"
             errornet12 :screen-value = if claimcostssummary.costRatioAE <= 0 then "" else string(claimcostssummary.costRatioAE * 100) + "%"                         
             from12     :screen-value = "From " + string(month(add-interval(today ,-12,'Months'))) + "/" + string(year(add-interval(today ,-12,'Months')))
             .
       end.
     when 2
      then
       do:
         assign
             netfrom24  :screen-value = string(claimcostssummary.netpremium)
             costfrom24 :screen-value = string(claimcostssummary.claimCosts)
             errorfrom24:screen-value = string(claimcostssummary.claimCostsAE)
             costnet24  :screen-value = if claimcostssummary.costRatio   <= 0 then "" else string(claimcostssummary.costRatio * 100)   + "%"
             errornet24 :screen-value = if claimcostssummary.costRatioAE <= 0 then "" else string(claimcostssummary.costRatioAE * 100) + "%"                         
             from24     :screen-value = "From " + string(month(add-interval(today ,-24,'Months'))) + "/" + string(year(add-interval(today ,-24,'Months')))
             .
       end.
     when 3
      then
       do:
         assign
             netfrom36   :screen-value = string(claimcostssummary.netpremium)
             costfrom36  :screen-value = string(claimcostssummary.claimCosts)
             errortfrom36:screen-value = string(claimcostssummary.claimCostsAE)
             costnet36   :screen-value = if claimcostssummary.costRatio   <= 0 then "" else string(claimcostssummary.costRatio * 100)   + "%"
             errornet36  :screen-value = if claimcostssummary.costRatioAE <= 0 then "" else string(claimcostssummary.costRatioAE * 100) + "%"                         
             from36      :screen-value = "From " + string(month(add-interval(today ,-36,'Months'))) + "/" + string(year(add-interval(today ,-36,'Months')))
             .
       end.
    end case.
  end.  

  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayCompliance wWin 
FUNCTION displayCompliance RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
   find first agent no-error.
   if not available agent 
    then 
     return false.
   
   do with frame frCompliance:
     if integer(agent.orgID) > 0 
      then
       do:
         assign
             iOldOrgID                  = integer(agent.orgID)             
             tOrganization:screen-value = if agent.orgID = "" or agent.orgID = ? then "" else agent.orgID
             tOrganization:private-data = agent.orgID
             tOrgName:screen-value      = if getOrgName(agent.orgID) = ? then "" else getOrgName(agent.orgID)
             .
       end.
      else
       do:
         tOrganization:screen-value = "".
         tOrganization:private-data = "".
         tOrgName:screen-value      = "".
       end.  
   end.
  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayDetail wWin 
FUNCTION displayDetail RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
define variable chvendorversion as character no-undo.

do with frame frDetail:
  find first agent no-error.
  if not available agent 
   then 
    return false.
    
  cOrigOrg = agent.orgID.  

  assign    
    flAgentName  = agent.name     
    flLAgentName = agent.legalname
    flAddr1      = agent.addr1
    flAddr2      = agent.addr2
    flCity       = agent.city
    cbState      = agent.state
    cbRegion     = agent.region
    flZip        = agent.zip
    /* status, phone, fax, email, website, software, and version */
    flPhone             = agent.phone
    flFax               = agent.fax
    flEmail             = agent.email
    flWebsite           = agent.website
    fDomain             = agent.domain
    /* Agent Agreement */          
    cbStateOper         = agent.stateID
          
    flContractDate    = agent.contractDate
    rdRemitType       = agent.remitType 
    flRemitValue      = agent.remitValue
    flRemitAlert      = agent.remitAlert
    flLiabilityLimit  = agent.liabilityLimit
    flPolicyLimit     = agent.policyLimit
          
    /* Activity */
    flProspectDate      = agent.prospectDate
    flClosedDate        = agent.closedDate
    flActiveDate        = agent.activeDate
    flCancelDate        = agent.cancelDate
    flReviewDate        = agent.reviewDate
    flWithdrawnDate     = agent.withdrawndate
    /* Mailing Address */
    flmName               = agent.mName
    flmAddr1              = agent.mAddr1
    flmAddr2              = agent.mAddr2
    flmCity               = agent.mCity
    cbMState              = agent.mState 
    flmZip                = agent.mZip
    /* Other */
    floAltaUID            = agent.altaUID
    floStateUID           = agent.stateUID
    floCorporation        = agent.corporationID
    floNPN                = agent.NPN          
    no-error.
          
   display
     /*cbOrg*/
     flAgentName
     flLAgentName
     flAddr1
     flAddr2
     flCity
     cbState
     flZip
     flPhone
     flFax
     flEmail
     flWebsite
     cbStateOper
     flContractDate
     rdRemitType
     flRemitValue
     flRemitAlert
     flLiabilityLimit
     flPolicyLimit
     flProspectDate
     flActiveDate
     flCancelDate
     flClosedDate
     flReviewDate
     flWithdrawnDate
     flmName
     flmAddr1
     flmAddr2
     cbmState
     flMCity     
     flmZip
     floAltaUID
     floStateUID     
     floCorporation
     floNPN     
     . 
   assign
     cMethod         = agent.invoiceDue
     iDueInDays      = agent.dueInDays
     iDueOnDay       = agent.dueOnDays
     cCashAcc        = agent.ARCashGLRef
     cCashAccDesc    = agent.ARCashGLDesc
     cARAcc          = agent.ARGLRef
     cARAccDesc      = agent.ARGLDesc
     cRefARAcc       = agent.ARRefundGLRef
     cRefARAccDesc   = agent.ARRefundGLDesc
     cWroffARAcc     = agent.ARWriteOffGLRef
     cWroffARAccDesc = agent.ARWriteOffGLDesc
     . 
   
   run enableButtons in this-procedure.
   if agent.swVendor <> "" 
    then
     chvendorversion = agent.swVendor.

   if lookup(chvendorversion, cbSwVendor:list-item-pairs) = 0 then
   do:
     if chvendorversion = "" and lookup("none", cbSwVendor:list-item-pairs) <> 0 then
       cbSwVendor:screen-value = entry(lookup("none", cbSwVendor:list-item-pairs) + 1, cbSwVendor:list-item-pairs).
     else if chvendorversion = "" then
     do:
       cbSwVendor:add-last("None", "NONE").
       cbSwVendor:screen-value = "NONE".
     end.
     else 
     do:
       cbSwVendor:add-last(chvendorversion, chvendorversion).
       cbSwVendor:screen-value = chvendorversion.
     end.
   end.
   else
   do: 
     cbSwVendor:screen-value = chvendorversion no-error.
        
      if cbSwVendor:screen-value = ? 
       then
        cbSwVendor:screen-value = entry(lookup(chvendorversion, cbSwVendor:list-item-pairs) + 1, cbSwVendor:list-item-pairs).
   end.
              
   if lookup(agent.contractID, cbContractID:list-item-pairs) = 0 
    then
     do:
       if agent.contractID = "" 
        then
         do:            
           cbContractID:list-item-pairs = "," + "," + cbContractID:list-item-pairs.
           cbContractID:screen-value = "".
         end.
       else 
        do:          
          cbContractID:add-last(agent.contractID, agent.contractID).
          cbContractID:screen-value = agent.contractID.
        end.
     end.
   else
    do:
      cbContractID:screen-value = agent.contractID no-error.
     
      if cbContractID:screen-value = ? 
       then
        cbContractID:screen-value = entry(lookup(agent.contractID, cbContractID:list-item-pairs) + 1, cbContractID:list-item-pairs).            
    end.
 
   find first agentmanager no-error.
   if not available agentmanager
    then
     do:
      run LoadAgentManagers in hFileDataSrv.
      run GetAgentManagers in hFileDataSrv (output table agentmanager).
     end.
     
   /* below call to displaymanager will not be useful on running first time */  
  
   run displayManager in this-procedure.
 
   run GetAgentLogo in hFileDataSrv (output chimage).
   if chimage > ""
    then 
     lImage = chimage.
      
   /* Handling all the errors that might come while storing the image in lImage variable. */
   if lImage <> ""  and 
      lImage <> "?" and 
      lImage <> ?   and 
      lImage <> "!ISO8859-1!?" 
    then
     do:
       decdmptr = base64-decode(lImage).
       copy-lob from decdmptr to file chPath.
       run setLogo in this-procedure.
       os-delete value(chPath).
     end. 
  end.
  doModify(false).
  return true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayEvent wWin 
FUNCTION displayEvent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  return false.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayHeader wWin 
FUNCTION displayHeader RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 do with frame fMain:
 
   find first agent no-error.
   if not available agent then return false.
   
   assign
     hdAgentID    = agent.agentID
     hdAgentName  = agent.name
     fStatus      = agent.statusDesc
     hdLAgentName = agent.legalname
     .
   display
     hdAgentID
     hdAgentName
     fStatus     
     hdLAgentName
     .
   cAgentStatus = agent.stat. 
   publish "SetCurrentValue" ("AgentID",  agent.agentID).
 end.
  
 return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayObjective wWin 
FUNCTION displayObjective RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  return false.    /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doModify wWin 
FUNCTION doModify RETURNS LOGICAL
  ( input pModify as logical ) :
/*------------------------------------------------------------------------------
@description Disables/Enables the save buttons
------------------------------------------------------------------------------*/
  do with frame frDetail:
    assign 
      bSave:sensitive   = pModify
      bCancel:sensitive = pModify
      datachanged       = pModify
      .
  end.
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getActivityComparedData wWin 
FUNCTION getActivityComparedData RETURNS LOGICAL
  (input ipiYear as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable dMonthPlan   as decimal extent 18 no-undo.
  define variable dMonthActual as decimal extent 18 no-undo.

  define variable iSeq as integer no-undo.
  
  define buffer ttactivity for ttactivity.
  
  checkActivity(1, "A", "N", ipiYear - 2).
  checkActivity(1, "A", "N", ipiYear - 1 ).
  checkActivity(1, "A", "N", ipiYear).
  checkActivity(2, "P", "N", ipiYear).
  
  for each ttactivity 
    by ttactivity.category: 
    if ttActivity.year = ipiYear 
     then
      do:       
        create activity.
        buffer-copy ttactivity to activity.
        assign
            activity.showID       = hdAgentID:input-value in frame fMain
            activity.showName     = hdAgentName:input-value in frame fMain 
            activity.categoryDesc = getCategoryDesc(ttActivity.category)
            .
        if ttactivity.type = "A" or ttactivity.type = "I" 
         then
          do:
            iCreateComparision = iCreateComparision + 1.
            activity.seq = 1.
            activity.typeDesc  = getActivityType("A" /* ttActivity.type */).
            assign
                dMonthActual[1] = ttactivity.month1
                dMonthActual[2] = ttactivity.month2
                dMonthActual[3] = ttactivity.month3
                dMonthActual[4] = ttactivity.month4
                dMonthActual[5] = ttactivity.month5
                dMonthActual[6] = ttactivity.month6
                dMonthActual[7] = ttactivity.month7
                dMonthActual[8] = ttactivity.month8
                dMonthActual[9] = ttactivity.month9
                dMonthActual[10] = ttactivity.month10
                dMonthActual[11] = ttactivity.month11
                dMonthActual[12] = ttactivity.month12
                dMonthActual[13] = ttactivity.qtr1
                dMonthActual[14] = ttactivity.qtr2
                dMonthActual[15] = ttactivity.qtr3
                dMonthActual[16] = ttactivity.qtr4
                dMonthActual[17] = ttactivity.yrTotal
                dMonthActual[18] = ttactivity.total
                .
          end.
        if ttactivity.type = "P"
         then
          do:
            iCreateComparision = iCreateComparision + 1.
            activity.seq = 2.
            activity.typeDesc  = getActivityType(ttactivity.type).
            assign
                dMonthPlan[1] = ttactivity.month1
                dMonthPlan[2] = ttactivity.month2
                dMonthPlan[3] = ttactivity.month3
                dMonthPlan[4] = ttactivity.month4
                dMonthPlan[5] = ttactivity.month5
                dMonthPlan[6] = ttactivity.month6
                dMonthPlan[7] = ttactivity.month7
                dMonthPlan[8] = ttactivity.month8
                dMonthPlan[9] = ttactivity.month9
                dMonthPlan[10] = ttactivity.month10
                dMonthPlan[11] = ttactivity.month11
                dMonthPlan[12] = ttactivity.month12
                dMonthPlan[13] = ttactivity.qtr1
                dMonthPlan[14] = ttactivity.qtr2
                dMonthPlan[15] = ttactivity.qtr3
                dMonthPlan[16] = ttactivity.qtr4
                dMonthPlan[17] = ttactivity.yrTotal
                dMonthPlan[18] = ttactivity.total
                .
          end.
      end.
    else
     do:
       if (ttActivity.category = "T" and ttActivity.type = "A") or 
          (ttActivity.category = "P" and ttActivity.type = "I") or
          (ttActivity.category = "N" and ttActivity.type = "A")
        then
         do:
           create activity.
           buffer-copy ttactivity to activity.
           assign
               activity.showID       = hdAgentID:input-value in frame fMain
               activity.showName     = hdAgentName:input-value in frame fMain
               activity.seq          = 1
               activity.typeDesc     = getActivityType("A" /* ttActivity.type */)
               activity.categoryDesc = getCategoryDesc(ttActivity.category)
               .
         end.
     end. 
  end.

  /* build the comparison row */
  for each ttactivity where ttactivity.year = ipiYear break by ttactivity.category:
    if first-of(ttactivity.category) 
     then
      do:
        create activity.    
        assign
            activity.month1  = (dMonthActual[1]  * 100 ) / dMonthPlan[1] 
            activity.month2  = (dMonthActual[2]  * 100 ) / dMonthPlan[2]   
            activity.month3  = (dMonthActual[3]  * 100 ) / dMonthPlan[3] 
            activity.month4  = (dMonthActual[4]  * 100 ) / dMonthPlan[4] 
            activity.month5  = (dMonthActual[5]  * 100 ) / dMonthPlan[5] 
            activity.month6  = (dMonthActual[6]  * 100 ) / dMonthPlan[6] 
            activity.month7  = (dMonthActual[7]  * 100 ) / dMonthPlan[7] 
            activity.month8  = (dMonthActual[8]  * 100 ) / dMonthPlan[8] 
            activity.month9  = (dMonthActual[9]  * 100 ) / dMonthPlan[9] 
            activity.month10 = (dMonthActual[10] * 100 ) / dMonthPlan[10]
            activity.month11 = (dMonthActual[11] * 100 ) / dMonthPlan[11]
            activity.month12 = (dMonthActual[12] * 100 ) / dMonthPlan[12]
            activity.qtr1    = (dMonthActual[13] * 100 ) / dMonthPlan[13]
            activity.qtr2    = (dMonthActual[14] * 100 ) / dMonthPlan[14]
            activity.qtr3    = (dMonthActual[15] * 100 ) / dMonthPlan[15]
            activity.qtr4    = (dMonthActual[16] * 100 ) / dMonthPlan[16]
            activity.yrTotal = (dMonthActual[17] * 100 ) / dMonthPlan[17]
            activity.total   = (dMonthActual[18] * 100 ) / dMonthPlan[18]
            .
        
        assign
            activity.seq           = 3
            activity.agentID       = ttactivity.agentID
            activity.showID        = hdAgentID:input-value in frame fMain
            activity.stateID       = ttactivity.stateID
            activity.corporationID = ttactivity.corporationID
            activity.name          = ttactivity.name
            activity.showName      = hdAgentName:input-value in frame fMain
            activity.type          = "C"
            activity.typeDesc      = "% of Planned"
            activity.category      = ttactivity.category
            activity.categoryDesc  = getCategoryDesc(ttActivity.category)
            activity.year          = ttactivity.year
            activity.month1        = if activity.month1 = ? then 0 else activity.month1
            activity.month2        = if activity.month2 = ? then 0 else activity.month2
            activity.month3        = if activity.month3 = ? then 0 else activity.month3
            activity.month4        = if activity.month4 = ? then 0 else activity.month4
            activity.month5        = if activity.month5 = ? then 0 else activity.month5
            activity.month6        = if activity.month6 = ? then 0 else activity.month6
            activity.month7        = if activity.month7 = ? then 0 else activity.month7
            activity.month8        = if activity.month8 = ? then 0 else activity.month8
            activity.month9        = if activity.month9 = ? then 0 else activity.month9
            activity.month10       = if activity.month10 = ? then 0 else activity.month10
            activity.month11       = if activity.month11 = ? then 0 else activity.month11
            activity.month12       = if activity.month12 = ? then 0 else activity.month12
            activity.qtr1          = if activity.qtr1 = ? then 0 else activity.qtr1
            activity.qtr2          = if activity.qtr2 = ? then 0 else activity.qtr2
            activity.qtr3          = if activity.qtr3 = ? then 0 else activity.qtr3
            activity.qtr4          = if activity.qtr4 = ? then 0 else activity.qtr4
            activity.yrTotal       = if activity.yrTotal = ? then 0 else activity.yrTotal
            activity.total         = if activity.total = ? then 0 else activity.total
            .
      end.
  end.
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getActivityComparedDataMetrics wWin 
FUNCTION getActivityComparedDataMetrics RETURNS LOGICAL
  (input ipiYear as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable dMonthPlanP   as decimal extent 18 no-undo.
  define variable dMonthActualP as decimal extent 18 no-undo.
  define variable dMonthPlanT   as decimal extent 18 no-undo.
  define variable dMonthActualT as decimal extent 18 no-undo.
  
  define variable iSeq as integer no-undo.
  
  define buffer ttactivityMetrics for ttactivityMetrics.
  
  checkActivityMetrics(1, "I", "P", ipiYear - 2).
  checkActivityMetrics(1, "A", "T", ipiYear - 2).
  checkActivityMetrics(1, "I", "P", ipiYear - 1).
  checkActivityMetrics(1, "A", "T", ipiYear - 1).
  checkActivityMetrics(1, "I", "P", ipiYear).
  checkActivityMetrics(1, "A", "T", ipiYear).
  checkActivityMetrics(2, "P", "P", ipiYear).
  checkActivityMetrics(2, "P", "T", ipiYear).
  
  for each ttactivityMetrics 
    by ttactivityMetrics.category: 
    if ttactivityMetrics.year = ipiYear 
     then
      do:       
        create activityMetrics.
        buffer-copy ttactivityMetrics to activityMetrics.
        assign
            activityMetrics.showID       = hdAgentID:input-value in frame fMain
            activityMetrics.showName     = hdAgentName:input-value in frame fMain 
            activityMetrics.categoryDesc = getCategoryDesc(ttactivityMetrics.category)
            .
        if ttactivityMetrics.type = "I" and ttactivityMetrics.category = 'P' 
         then
          do:
            iCreateComparision = iCreateComparision + 1.
            activityMetrics.seq = 1.
            activityMetrics.typeDesc  = getActivityType(ttactivityMetrics.type).
            assign
                dMonthActualP[1] = ttactivityMetrics.month1
                dMonthActualP[2] = ttactivityMetrics.month2
                dMonthActualP[3] = ttactivityMetrics.month3
                dMonthActualP[4] = ttactivityMetrics.month4
                dMonthActualP[5] = ttactivityMetrics.month5
                dMonthActualP[6] = ttactivityMetrics.month6
                dMonthActualP[7] = ttactivityMetrics.month7
                dMonthActualP[8] = ttactivityMetrics.month8
                dMonthActualP[9] = ttactivityMetrics.month9
                dMonthActualP[10] = ttactivityMetrics.month10
                dMonthActualP[11] = ttactivityMetrics.month11
                dMonthActualP[12] = ttactivityMetrics.month12
                dMonthActualP[13] = ttactivityMetrics.qtr1
                dMonthActualP[14] = ttactivityMetrics.qtr2
                dMonthActualP[15] = ttactivityMetrics.qtr3
                dMonthActualP[16] = ttactivityMetrics.qtr4
                dMonthActualP[17] = ttactivityMetrics.yrTotal
                dMonthActualP[18] = ttactivityMetrics.total
                .
          end.
        if ttactivityMetrics.category = 'P' and ttactivityMetrics.type = "P"
         then
          do:
            iCreateComparision = iCreateComparision + 1.
            activityMetrics.seq = 2.
            activityMetrics.typeDesc  = getActivityType(ttactivityMetrics.type).
            assign
                dMonthPlanP[1] = ttactivityMetrics.month1
                dMonthPlanP[2] = ttactivityMetrics.month2
                dMonthPlanP[3] = ttactivityMetrics.month3
                dMonthPlanP[4] = ttactivityMetrics.month4
                dMonthPlanP[5] = ttactivityMetrics.month5
                dMonthPlanP[6] = ttactivityMetrics.month6
                dMonthPlanP[7] = ttactivityMetrics.month7
                dMonthPlanP[8] = ttactivityMetrics.month8
                dMonthPlanP[9] = ttactivityMetrics.month9
                dMonthPlanP[10] = ttactivityMetrics.month10
                dMonthPlanP[11] = ttactivityMetrics.month11
                dMonthPlanP[12] = ttactivityMetrics.month12
                dMonthPlanP[13] = ttactivityMetrics.qtr1
                dMonthPlanP[14] = ttactivityMetrics.qtr2
                dMonthPlanP[15] = ttactivityMetrics.qtr3
                dMonthPlanP[16] = ttactivityMetrics.qtr4
                dMonthPlanP[17] = ttactivityMetrics.yrTotal
                dMonthPlanP[18] = ttactivityMetrics.total
                .
          end.
        if ttactivityMetrics.type = "A" and ttactivityMetrics.category = 'T' 
         then
          do:
            iCreateComparision = iCreateComparision + 1.
            activityMetrics.seq = 1.
            activityMetrics.typeDesc  = getActivityType("I").
            assign
                dMonthActualT[1] = ttactivityMetrics.month1
                dMonthActualT[2] = ttactivityMetrics.month2
                dMonthActualT[3] = ttactivityMetrics.month3
                dMonthActualT[4] = ttactivityMetrics.month4
                dMonthActualT[5] = ttactivityMetrics.month5
                dMonthActualT[6] = ttactivityMetrics.month6
                dMonthActualT[7] = ttactivityMetrics.month7
                dMonthActualT[8] = ttactivityMetrics.month8
                dMonthActualT[9] = ttactivityMetrics.month9
                dMonthActualT[10] = ttactivityMetrics.month10
                dMonthActualT[11] = ttactivityMetrics.month11
                dMonthActualT[12] = ttactivityMetrics.month12
                dMonthActualT[13] = ttactivityMetrics.qtr1
                dMonthActualT[14] = ttactivityMetrics.qtr2
                dMonthActualT[15] = ttactivityMetrics.qtr3
                dMonthActualT[16] = ttactivityMetrics.qtr4
                dMonthActualT[17] = ttactivityMetrics.yrTotal
                dMonthActualT[18] = ttactivityMetrics.total
                .
          end.
        if ttactivityMetrics.type = "P" and ttactivityMetrics.category = 'T'
         then
          do:
            iCreateComparision = iCreateComparision + 1.
            activityMetrics.seq = 2.
            activityMetrics.typeDesc  = getActivityType(ttactivityMetrics.type).
            assign
                dMonthPlanT[1] = ttactivityMetrics.month1
                dMonthPlanT[2] = ttactivityMetrics.month2
                dMonthPlanT[3] = ttactivityMetrics.month3
                dMonthPlanT[4] = ttactivityMetrics.month4
                dMonthPlanT[5] = ttactivityMetrics.month5
                dMonthPlanT[6] = ttactivityMetrics.month6
                dMonthPlanT[7] = ttactivityMetrics.month7
                dMonthPlanT[8] = ttactivityMetrics.month8
                dMonthPlanT[9] = ttactivityMetrics.month9
                dMonthPlanT[10] = ttactivityMetrics.month10
                dMonthPlanT[11] = ttactivityMetrics.month11
                dMonthPlanT[12] = ttactivityMetrics.month12
                dMonthPlanT[13] = ttactivityMetrics.qtr1
                dMonthPlanT[14] = ttactivityMetrics.qtr2
                dMonthPlanT[15] = ttactivityMetrics.qtr3
                dMonthPlanT[16] = ttactivityMetrics.qtr4
                dMonthPlanT[17] = ttactivityMetrics.yrTotal
                dMonthPlanT[18] = ttactivityMetrics.total
                .
          end.
      end.
    else
     do:
       if (ttactivityMetrics.category = "T" and ttactivityMetrics.type = "A") or 
          (ttactivityMetrics.category = "P" and ttactivityMetrics.type = "I") or
          (ttactivityMetrics.category = "N" and ttactivityMetrics.type = "A")
        then
         do:
           create activityMetrics.
           buffer-copy ttactivityMetrics to activityMetrics.
           assign
               activityMetrics.showID       = hdAgentID:input-value in frame fMain
               activityMetrics.showName     = hdAgentName:input-value in frame fMain
               activityMetrics.seq          = 1
               activityMetrics.typeDesc     = getActivityType("I")
               activityMetrics.categoryDesc = getCategoryDesc(ttactivityMetrics.category)
               .
         end.
     end. 
  end.

  /* build the comparison row */
  for each ttactivityMetrics where ttactivityMetrics.year = ipiYear break by ttactivityMetrics.category:
    if first-of(ttactivityMetrics.category)
     then
      do:
        create activityMetrics.
        if ttactivityMetrics.category = 'P'
         then
          do: 
            assign
                activityMetrics.month1  = (dMonthActualP[1]  * 100 ) / dMonthPlanP[1] 
                activityMetrics.month2  = (dMonthActualP[2]  * 100 ) / dMonthPlanP[2]   
                activityMetrics.month3  = (dMonthActualP[3]  * 100 ) / dMonthPlanP[3] 
                activityMetrics.month4  = (dMonthActualP[4]  * 100 ) / dMonthPlanP[4] 
                activityMetrics.month5  = (dMonthActualP[5]  * 100 ) / dMonthPlanP[5] 
                activityMetrics.month6  = (dMonthActualP[6]  * 100 ) / dMonthPlanP[6] 
                activityMetrics.month7  = (dMonthActualP[7]  * 100 ) / dMonthPlanP[7] 
                activityMetrics.month8  = (dMonthActualP[8]  * 100 ) / dMonthPlanP[8] 
                activityMetrics.month9  = (dMonthActualP[9]  * 100 ) / dMonthPlanP[9] 
                activityMetrics.month10 = (dMonthActualP[10] * 100 ) / dMonthPlanP[10]
                activityMetrics.month11 = (dMonthActualP[11] * 100 ) / dMonthPlanP[11]
                activityMetrics.month12 = (dMonthActualP[12] * 100 ) / dMonthPlanP[12]
                activityMetrics.qtr1    = (dMonthActualP[13] * 100 ) / dMonthPlanP[13]
                activityMetrics.qtr2    = (dMonthActualP[14] * 100 ) / dMonthPlanP[14]
                activityMetrics.qtr3    = (dMonthActualP[15] * 100 ) / dMonthPlanP[15]
                activityMetrics.qtr4    = (dMonthActualP[16] * 100 ) / dMonthPlanP[16]
                activityMetrics.yrTotal = (dMonthActualP[17] * 100 ) / dMonthPlanP[17]
                activityMetrics.total   = (dMonthActualP[18] * 100 ) / dMonthPlanP[18]
                .
          end.
        if ttactivityMetrics.category = 'T'
         then
          do:  
            assign
                activityMetrics.month1  = (dMonthActualT[1]  * 100 ) / dMonthPlanT[1] 
                activityMetrics.month2  = (dMonthActualT[2]  * 100 ) / dMonthPlanT[2]   
                activityMetrics.month3  = (dMonthActualT[3]  * 100 ) / dMonthPlanT[3] 
                activityMetrics.month4  = (dMonthActualT[4]  * 100 ) / dMonthPlanT[4] 
                activityMetrics.month5  = (dMonthActualT[5]  * 100 ) / dMonthPlanT[5] 
                activityMetrics.month6  = (dMonthActualT[6]  * 100 ) / dMonthPlanT[6] 
                activityMetrics.month7  = (dMonthActualT[7]  * 100 ) / dMonthPlanT[7] 
                activityMetrics.month8  = (dMonthActualT[8]  * 100 ) / dMonthPlanT[8] 
                activityMetrics.month9  = (dMonthActualT[9]  * 100 ) / dMonthPlanT[9] 
                activityMetrics.month10 = (dMonthActualT[10] * 100 ) / dMonthPlanT[10]
                activityMetrics.month11 = (dMonthActualT[11] * 100 ) / dMonthPlanT[11]
                activityMetrics.month12 = (dMonthActualT[12] * 100 ) / dMonthPlanT[12]
                activityMetrics.qtr1    = (dMonthActualT[13] * 100 ) / dMonthPlanT[13]
                activityMetrics.qtr2    = (dMonthActualT[14] * 100 ) / dMonthPlanT[14]
                activityMetrics.qtr3    = (dMonthActualT[15] * 100 ) / dMonthPlanT[15]
                activityMetrics.qtr4    = (dMonthActualT[16] * 100 ) / dMonthPlanT[16]
                activityMetrics.yrTotal = (dMonthActualT[17] * 100 ) / dMonthPlanT[17]
                activityMetrics.total   = (dMonthActualT[18] * 100 ) / dMonthPlanT[18]
                .
          end.
        assign
            activityMetrics.seq           = 3
            activityMetrics.agentID       = ttactivityMetrics.agentID
            activityMetrics.showID        = hdAgentID:input-value in frame fMain
            activityMetrics.stateID       = ttactivityMetrics.stateID
            activityMetrics.corporationID = ttactivityMetrics.corporationID
            activityMetrics.name          = ttactivityMetrics.name
            activityMetrics.showName      = hdAgentName:input-value in frame fMain
            activityMetrics.type          = "C"
            activityMetrics.typeDesc      = "% of Planned"
            activityMetrics.category      = ttactivityMetrics.category
            activityMetrics.categoryDesc  = getCategoryDesc(ttactivityMetrics.category)
            activityMetrics.year          = ttactivityMetrics.year
            activityMetrics.month1        = if activityMetrics.month1 = ? then 0 else activityMetrics.month1
            activityMetrics.month2        = if activityMetrics.month2 = ? then 0 else activityMetrics.month2
            activityMetrics.month3        = if activityMetrics.month3 = ? then 0 else activityMetrics.month3
            activityMetrics.month4        = if activityMetrics.month4 = ? then 0 else activityMetrics.month4
            activityMetrics.month5        = if activityMetrics.month5 = ? then 0 else activityMetrics.month5
            activityMetrics.month6        = if activityMetrics.month6 = ? then 0 else activityMetrics.month6
            activityMetrics.month7        = if activityMetrics.month7 = ? then 0 else activityMetrics.month7
            activityMetrics.month8        = if activityMetrics.month8 = ? then 0 else activityMetrics.month8
            activityMetrics.month9        = if activityMetrics.month9 = ? then 0 else activityMetrics.month9
            activityMetrics.month10       = if activityMetrics.month10 = ? then 0 else activityMetrics.month10
            activityMetrics.month11       = if activityMetrics.month11 = ? then 0 else activityMetrics.month11
            activityMetrics.month12       = if activityMetrics.month12 = ? then 0 else activityMetrics.month12
            activityMetrics.qtr1          = if activityMetrics.qtr1 = ? then 0 else activityMetrics.qtr1
            activityMetrics.qtr2          = if activityMetrics.qtr2 = ? then 0 else activityMetrics.qtr2
            activityMetrics.qtr3          = if activityMetrics.qtr3 = ? then 0 else activityMetrics.qtr3
            activityMetrics.qtr4          = if activityMetrics.qtr4 = ? then 0 else activityMetrics.qtr4
            activityMetrics.yrTotal       = if activityMetrics.yrTotal = ? then 0 else activityMetrics.yrTotal
            activityMetrics.total         = if activityMetrics.total = ? then 0 else activityMetrics.total
            .
      end.
  end.
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getActivityTotal wWin 
FUNCTION getActivityTotal RETURNS DECIMAL
  ( input table for tempactivity,
    input pType as character  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable dReturn as decimal no-undo.
  define variable iMonth as integer no-undo.
  define buffer tempactivity for tempactivity.
  
  iMonth = month(today).
  for first tempactivity no-lock:
    case pType:
     when "Year" 
      then 
       do:
         dReturn = tempactivity.month1 + tempactivity.month2 + tempactivity.month3 + 
                   tempactivity.month4 + tempactivity.month5 + tempactivity.month6 +
                   tempactivity.month7 + tempactivity.month8 + tempactivity.month9 +
                   tempactivity.month10 + tempactivity.month11 + tempactivity.month12.
       end.
     when "Current" then
      do:
        dReturn = (if iMonth >= 1  then tempactivity.month1  else 0)
                + (if iMonth >= 2  then tempactivity.month2  else 0)
                + (if iMonth >= 3  then tempactivity.month3  else 0)
                + (if iMonth >= 4  then tempactivity.month4  else 0)
                + (if iMonth >= 5  then tempactivity.month5  else 0)
                + (if iMonth >= 6  then tempactivity.month6  else 0)
                + (if iMonth >= 7  then tempactivity.month7  else 0)
                + (if iMonth >= 8  then tempactivity.month8  else 0)
                + (if iMonth >= 9  then tempactivity.month9  else 0)
                + (if iMonth >= 10 then tempactivity.month10 else 0)
                + (if iMonth >= 11 then tempactivity.month11 else 0)
                + (if iMonth >= 12 then tempactivity.month12 else 0).
      end.
    end case.
  end.
  return dReturn.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getActivityType wWin 
FUNCTION getActivityType RETURNS CHARACTER
  ( input ipcType as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("AMD", "Activity", "Type", ipcType, output std-ch).
  return std-ch.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getAuditStatus wWin 
FUNCTION getAuditStatus RETURNS CHARACTER
  ( input ipcStatus as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/   
  std-ch = "".
  publish "GetSysPropDesc" ("QAR", "Audit", "Status", ipcStatus, output std-ch).
  return std-ch.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getAuditType wWin 
FUNCTION getAuditType RETURNS CHARACTER
  ( input ipcAuditType as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("QAR", "Audit", "Type", ipcAuditType, output std-ch).
  return std-ch.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getauth wWin 
FUNCTION getauth returns character ( cAuth as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  if cAuth = {&ThirdParty} then
    return {&Third-Party}.   /* Function return value. */
  else if cAuth = {&FirstParty} then
    return {&First-Party}.
  else
  return cAuth.

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getBatchStatus wWin 
FUNCTION getBatchStatus RETURNS CHARACTER
  ( input ipcStatus as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("OPS", "Batch", "Status", ipcStatus, output std-ch).
  return std-ch.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getCategoryDesc wWin 
FUNCTION getCategoryDesc RETURNS CHARACTER
  ( input ipcCategory as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("AMD", "Activity", "Category", ipcCategory, output std-ch).
  return std-ch.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getComStat wWin 
FUNCTION getComStat returns character
 ( input cStat as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 case cStat:
   when "3" 
    then 
     return {&Rcode}.

   when "1" 
    then
     return {&Gcode}.

   when "2" 
    then
     return {&Ycode}.
 end case.  

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getcomstatus wWin 
FUNCTION getcomstatus RETURNS CHARACTER
  ( input cStat as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cstat = {&Gcode}
   then 
    return {&Green}.
  else if cstat = {&Ycode}
   then
    return {&Yellow}.
  else
   return {&Red}.
    
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getEntity wWin 
FUNCTION getEntity RETURNS CHARACTER
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cStat = {&OrganizationCode}
   then
    cStat = {&Organization}.
  else
   cStat = {&Person}.
  
  return  cStat.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getEntityID wWin 
FUNCTION getEntityID RETURNS CHARACTER
  ( cPartnerBType as character,
    cPartnerA     as character,
    cPartnerB     as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cPartnerBType = {&OrganizationCode} 
   then
    do with frame frpersonagent:
      if cPartnerB = corgID 
       then
        return cPartnerA.
      else
       return cPartnerB.
    end.
  else
   return cPartnerB.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getEntityName wWin 
FUNCTION getEntityName RETURNS CHARACTER
  ( cPartnerB     as character,
    cPartnerBType as character,
    cPartnerAName as character,
    cPartnerBName as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  if cPartnerBType = {&OrganizationCode} 
   then
    do with frame frpersonagent:
      if cPartnerB = corgID 
       then
        return cPartnerAName.
      else
       return cPartnerBName.
    end.
  else
   return cPartnerBName.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getErrorType wWin 
FUNCTION getErrorType RETURNS CHARACTER
  ( input ipcErrType as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("QAR", "ERR", "Type", ipcErrType, output std-ch).
  return std-ch.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getMonthName wWin 
FUNCTION getMonthName RETURNS CHARACTER
  (INPUT iMonth AS INTEGER) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cMonths as character no-undo initial "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".
  IF iMonth lt 1 or iMonth gt 12 
   then
    return "".
  return entry(iMonth,cMonths).
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getMonthWiseYearTrend wWin 
FUNCTION getMonthWiseYearTrend RETURNS LOGICAL
  ( input ipiPeriodID as integer,
    input ipiYear     as integer,
    input ipiYearSeq  as integer,
    input ipdeAmount  as decimal) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame frBatch:
  end.
  
  return true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getNature wWin 
FUNCTION getNature RETURNS CHARACTER
  ( cPartnerB     as character,
    cPartnerBType as character,
    cNatureAtoB   as character,
    cNatureBtoA   as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cPartnerBType = {&OrganizationCode} 
   then
    do with frame frpersonagent:
      if cPartnerB = corgID 
       then
        return cNatureAtoB.
      else
       return cNatureBtoA.
    end.
  else
   return cNatureBtoA.
   
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getOrgName wWin 
FUNCTION getOrgName RETURNS CHARACTER
  ( ipcOrgID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable opcName as character no-undo.
  
  run GetOrganizationName in hFileDataSrv (input  ipcOrgID,
                                           output opcName,
                                           output std-lo).     

  if std-lo 
   then 
    return opcName.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getPeriodID wWin 
FUNCTION getPeriodID RETURNS INTEGER
  ( input ipiMonth as integer,
    input ipiYear as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable iPeriodID as integer no-undo.
  
  publish "GetPeriodID" (input ipiMonth,
                         input ipiYear,
                         output iPeriodID).
                         
  RETURN iPeriodID.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getQualificationDesc wWin 
FUNCTION getQualificationDesc RETURNS CHARACTER
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  publish "GetSysPropDesc" ("COM", "Qualification", "Status", cStat, output std-ch).
  return std-ch.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getQualificationStatus wWin 
FUNCTION getQualificationStatus RETURNS CHARACTER
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("COM", "Qualification", "Status", cStat, output std-ch).
  return std-ch.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getTranType wWin 
FUNCTION getTranType RETURNS CHARACTER
  (input ipcTranType as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  case ipcTranType:
    when "I" then return "Invoice".
    when "D" then return "Debit".
    when "C" then return "Credit".
    when "P" then return "Payment".
    when "F" then return "File".
    when "A" then return "Apply".
    when "R" then return "Reprocess".
    when "M" then return "Miscellaneous".
    otherwise return ipcTranType. /* Function return value. */
  end case.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openActivity wWin 
FUNCTION openActivity RETURNS LOGICAL
  ( input ipiYear         as integer,
    input ipcCategoryList as character,
    input iplRefresh      as logical) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/     
  define buffer ttactivity for ttactivity.
  
  empty temp-table activity.
  empty temp-table ttactivity.
  
  do with frame frActivity:
  end.
 
  if ipiYear = year(today) 
   then
    do:
      if iplRefresh 
       then
        run refreshAgentActivity in hFileDataSrv(input ipiYear ,
                                                 input ipcCategoryList ,
                                                 output table ttactivity).
       else
        run getAgentActivity in hFileDataSrv(input ipiYear ,
                                             input ipcCategoryList ,
                                             output table ttactivity).
                                          
    end. /* if ipiYear = year(today) */

  for each ttactivity:
    ttactivity.typeDesc = getActivityType(ttactivity.type).
    
    /* get totals */
    empty temp-table tempactivity.  
    create tempactivity.
    buffer-copy ttactivity to tempactivity.
    assign
        ttactivity.Total   = getActivityTotal(table tempactivity, "Year")
        ttactivity.yrTotal = getActivityTotal(table tempactivity, "Current")
        .
  end.
  
  getActivityComparedData(input ipiYear).

  find first ttActivity no-error.
  if not available ttActivity
   then
    do:
      createActivity(1 , "A", "N", ipiYear - 2).
      createActivity(1 , "A", "N", ipiYear - 1).
      getActivityComparedData(input ipiYear).
    end.
  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openActivityMetrics wWin 
FUNCTION openActivityMetrics RETURNS LOGICAL
  ( input ipiYear         as integer,
    input ipcCategoryList as character,
    input iplRefresh      as logical) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/     
  define buffer ttactivityMetrics for ttactivityMetrics.
  
  empty temp-table activityMetrics.
  empty temp-table ttactivityMetrics.
  
  do with frame frActivity:
  end.
 
  if ipiYear = year(today) 
   then
    do:
      if iplRefresh 
       then
        run refreshAgentActivityMetrics in hFileDataSrv(input ipiYear ,
                                                        input ipcCategoryList,
                                                        output table ttactivityMetrics). 
       else
        run getAgentActivityMetrics in hFileDataSrv(input ipiYear ,
                                                    input ipcCategoryList,
                                                    output table ttactivityMetrics).                                 
                                                          
    end. /* if ipiYear = year(today) */

  for each ttactivityMetrics:
    ttactivityMetrics.typeDesc = getActivityType(ttactivityMetrics.type).
    
    /* get totals */
    empty temp-table tempactivity.  
    create tempactivity.
    buffer-copy ttactivityMetrics to tempactivity.
    assign
        ttactivityMetrics.Total   = getActivityTotal(table tempactivity, "Year")
        ttactivityMetrics.yrTotal = getActivityTotal(table tempactivity, "Current")
        .

  end.
  
  getActivityComparedDataMetrics(input ipiYear).

  find first ttActivityMetrics no-error.
  if not available ttActivityMetrics
   then
    do:
         createActivityMetrics(1 , "I", "P", ipiYear - 2).
         createActivityMetrics(1 , "A", "T", ipiYear - 2).
         createActivityMetrics(1 , "I", "P", ipiYear - 1).
         createActivityMetrics(1 , "A", "T", ipiYear - 1).
         getActivityComparedDataMetrics(input ipiYear).
      
    end.
  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openAlerts wWin 
FUNCTION openAlerts RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame frAlert:
  end.

  run getAgentAlerts in hFileDataSrv(output table ttAlert).  

  run getAlertDesc in this-procedure.
  assign
      cLastAlertOwner = "ALL"
      cLastAlertCode  = "ALL"
      .
  run setAlertCombos in this-procedure.
  run filterAlerts in this-procedure.   
 
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openAr wWin 
FUNCTION openAr RETURNS LOGICAL
  (input iplRefresh as logical) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
  if iplRefresh
   then
    run refreshARAging in hFileDataSrv(output table arAging,
                                       output cArAgentEmail).
   else
    run getARAging in hFileDataSrv(output table arAging,
                                   output cArAgentEmail). 

  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openAudit wWin 
FUNCTION openAudit RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
  find first agent no-error.
  if not available agent
   then
    return false.
 
  run getAllAudits in hFileDataSrv(output table audit).

  for each audit:
    assign
        audit.stat      = getAuditStatus(audit.stat)
        audit.audittype = getAuditType(audit.audittype)
        audit.errtype   = getErrorType(audit.errtype)
        .
  end.
 
  open query brwQAR for each audit no-lock by audit.auditStartDate desc.    

 /* only showing open corrective actions */ 
 
 run getActions in hFileDataSrv(output  table  action,
                                output  table  finding
                                ).
                                
  open query brwAction for each action. 

  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openBatchesYearly wWin 
FUNCTION openBatchesYearly RETURNS LOGICAL
  (input ipcYear as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
  define variable ipiYear as integer no-undo.

  do with frame frBatch:
  end.
  
  if ipcYear = "" 
   then return false.
  
  ipiYear = integer(ipcYear) no-error.
  if error-status:error
   then return false.
  
  find first agent no-error.
  if not available agent
   then
    return false.
  
  empty temp-table batch.
  
  run getagentbatchesyearly in hFileDataSrv (input ipiYear,
                                             output table batch).
          

  open query brwBatch for each batch.
  bBatchRefresh:sensitive = true.
  
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openclaim wWin 
FUNCTION openclaim RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
  define variable iLAER        as integer no-undo.
  define variable iLAEL        as integer no-undo.
  define variable iLossR       as integer no-undo.
  define variable iLossL       as integer no-undo.
  define variable iRecoveriesR as integer no-undo.
  define variable iTotalCostL  as integer no-undo.
  define variable iCount       as integer no-undo.
  
  do with frame fMain:
  end.

  if lRefreshClaims 
   then
    run loadClaimsByAgent in hFileDataSrv.
    
  run getclaimsbyagent in hFileDataSrv (output table agentclaim).
                                 
  iCount = 0.
  for each agentclaim:
    publish "getSysUserName" (input agentclaim.assignedTo, output agentclaim.assignedToDesc).
    agentClaim.costsPaid = agentclaim.laeLTD + agentclaim.lossLTD - agentclaim.recoveries.
    assign
        iCount       = iCount + 1
        iLAER        = iLAER        + agentclaim.laeReserve   
        iLAEL        = iLAEL        + agentclaim.laeLTD       
        iLossR       = iLossR       + agentclaim.lossReserve  
        iLossL       = iLossL       + agentclaim.lossLTD      
        iRecoveriesR = iRecoveriesR + agentclaim.recoveries   
        iTotalCostL  = iTotalCostL  + agentclaim.costsPaid    
        .
  end.
  open query brwClaims for each agentclaim.

  assign
      fTotal      :screen-value in frame frclaim = "Total of " + string(iCount) + " Claim(s)"
      fLAER       :screen-value = if iLAER        = 0 then "" else string(iLAER)
      fLAEL       :screen-value = if iLAEL        = 0 then "" else string(iLAEL)
      fLossR      :screen-value = if iLossR       = 0 then "" else string(iLossR)
      fLossL      :screen-value = if iLossL       = 0 then "" else string(iLossL)
      fRecoveriesR:screen-value = if iRecoveriesR = 0 then "" else string(iRecoveriesR)
      fTotalCostL :screen-value = if iTotalCostL  = 0 then "" else string(iTotalCostL)
      .
      
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openEvents wWin 
FUNCTION openEvents RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  find first agent no-error.
  if not available agent
   then
    return false.

  run getObjectives in hFileDataSrv (input {&ActiveStat},              /* (A)ctive */ 
                                     output table objective,
                                     output table sentiment).

  if lRefreshEvents 
   then
    run loadEvents in hFileDataSrv. 
  
 run getEvents in hFileDataSrv (output table event).

 run showTimelineEvents in this-procedure(input dstartDate,
                                          input dEndDate
                                         ).                                       

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openObjectives wWin 
FUNCTION openObjectives RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openPersonAgents wWin 
FUNCTION openPersonAgents RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame frAffiliation :
  end.

  find first agent no-error.
  if not available agent
   then
    return false.
      
  if lRefreshtpersonagents
   then
    run LoadAgentPersons in hFileDataSrv.

  empty temp-table personagent.
  
  run getAgentPersons in hFileDataSrv(output table personagent).
  
  empty temp-table tpersonagent. 
  for each personagent:
    if (date(personagent.effectivedate) <= today or date(personagent.effectivedate) = ? ) and (date(personagent.expirationdate) > today or date(personagent.expirationdate) = ?)
     then
      personagent.active = true.
    else
     personagent.active = false.
      
    create tpersonagent.
    buffer-copy personagent to tpersonagent.
  end. 

  apply 'value-changed' to AffType in frame frpersonagent.
  apply 'value-changed' to brwPersonAgent in frame frpersonagent.
  
  open query brwPersonAgent  for each tpersonagent by tpersonagent.active desc by tpersonagent.personName.

  if query brwPersonAgent:num-results = 0
   then
    assign
        bpersonagentUpdate:sensitive in frame frpersonagent = false
        bpersonagentdeactivate:sensitive in frame frpersonagent = false
        bpersonagentExport:sensitive in frame frpersonagent = false
        btnAgentPersonMail:sensitive in frame frpersonagent = false.
  else
   assign
       bpersonagentUpdate:sensitive in frame frpersonagent = true
       bpersonagentdeactivate:sensitive in frame frpersonagent = true
       bpersonagentExport:sensitive in frame frpersonagent = true
       btnAgentPersonMail:sensitive in frame frpersonagent = if tpersonagent.email = '' or tpersonagent.doNotCall then false  else true
       .
   
  return true.
  
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openQualification wWin 
FUNCTION openQualification RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  empty temp-table tqualification.

  find first agent no-error.
  if not available agent 
   then 
    return false.
 
  if lRefreshQualifications
   then
    run loadAgentQualifications in hFileDataSrv.
    
  run getAgentQualifications in hFileDataSrv (output table tqualification).

  run filterQualifications in this-procedure.
  
  return true.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openReqFulFillment wWin 
FUNCTION openReqFulFillment RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  empty temp-table role.
  empty temp-table reqfulfill.
  
  find first agent no-error.
  if not available agent
   then
    return false.
  
  if lRefreshReqFulFillment
   then
    run loadOrganizationRole in hFileDataSrv. 
    
  run getOrganizationRole in hFileDataSrv(output table role,
                                          output table reqfulfill
                                         ).
  
  for each reqfulfill:
     reqfulfill.stat      = getQualificationDesc(reqfulfill.stat).
     
     if reqfulfill.appliesTo = {&OrganizationCode}
      then
       reqfulfill.appliesTo = {&Organization}.   
      else if reqfulfill.appliesTo = {&personcode}
       then
        reqfulfill.appliesTo = {&Person}.
  end.
  
  
  open query brwReqFul for each reqfulfill.
    
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openTags wWin 
FUNCTION openTags RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
  find first agent no-error.
  if not available agent
   then
    return false.
    
  run getuserTags in hFileDataSrv (input "A", input agent.agentID, output table tag). 
      
  open query brwTags for each tag where tag.entity = "A".

  apply 'value-changed' to brwTags in frame frDetail.
  
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openURL wWin 
FUNCTION openURL RETURNS LOGICAL PRIVATE
  ( input pURL as char ) :


  run ShellExecuteA in this-procedure (0,
                                       "open",
                                       pURL,
                                       "",
                                       "",
                                       1,
                                       output std-in).

 return true.

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged wWin 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveDetail wWin 
FUNCTION saveDetail RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------------
@description Saves the agent
------------------------------------------------------------------------------*/    
  define variable cOrgID   as character no-undo.
  define variable cOrg     as character no-undo.
  define variable cMsg     as character no-undo.
  std-lo = true.
  
  do with frame frDetail:
  end.
  
 find first state where state.stateID = cbStateOper:screen-value no-error.
  if available state
   then
    do:
      if state.region <> cbRegion:screen-value 
       then
        cMsg = "Operating State does not belong to region.".
    end.
    
  if cManagerUID = ""
   then cMsg = cMsg + chr(10) + "No manager has been assigned.".
  
  std-lo = true.
  if cMsg <> ""
   then 
    message cMsg + chr(10) + "Are you sure you want to continue?" view-as alert-box question buttons yes-no update std-lo.
   
  if not std-lo
   then 
    return false.
  
  do with frame {&frame-name}:  

    assign /* agent and address */
      agent.agentID            = hdAgentID:input-value 
      agent.name               = flAgentName:input-value
      agent.legalName          = flLAgentName:input-value
      agent.addr1              = flAddr1:input-value
      agent.addr2              = flAddr2:input-value
      agent.city               = flCity:input-value
      agent.state              = cbState:input-value
      agent.region             = cbRegion:input-value
      agent.zip                = flZip:input-value
      /* status, phone, fax, email, website, software, and version */
      agent.phone              = flPhone:input-value
      agent.fax                = flFax:input-value
      agent.email              = flEmail:input-value
      agent.website            = flWebsite:input-value
      agent.domain             = fDomain:input-value
      agent.swVendor           = cbSwVendor:input-value
/*       Agent Agreement */
      agent.stateID            = cbStateOper:input-value
      agent.contractID         = cbContractID:input-value
      agent.contractDate       = flContractDate:input-value
      agent.remitType          = rdRemitType:input-value
      agent.remitValue         = flRemitValue:input-value
      agent.remitAlert         = flRemitAlert:input-value
      agent.liabilityLimit     = flLiabilityLimit:input-value
      agent.policyLimit        = flPolicyLimit:input-value
      /* Activity */
      agent.prospectDate       = flProspectDate:input-value
      agent.closedDate         = flClosedDate:input-value
      agent.activeDate         = flActiveDate:input-value
      agent.cancelDate         = flCancelDate:input-value
      agent.reviewDate         = flReviewDate:input-value
      agent.withdrawndate      = flWithdrawnDate:input-value
      /* Mailing Address */
      agent.mName              = flMName:input-value
      agent.mAddr1             = flMAddr1:input-value
      agent.mAddr2             = flMAddr2:input-value
      agent.mCity              = flMCity:input-value
      agent.mState             = cbMstate:input-value
      agent.mZip               = flMZip:input-value
      /* Other */
      agent.altaUID            = flOAltaUID:input-value
      agent.stateUID           = flOStateUID:input-value
      agent.corporationID      = flOCorporation:input-value
      agent.manager            = cManagerUID
      agent.NPN                = flONPN:input-value
      /* AR Related information */
      agent.invoiceDue         = cMethod
      agent.dueInDays          = iDueInDays
      agent.dueOnDays          = iDueOnDay
      agent.ARCashGLRef        = cCashAcc
      agent.ARCashGLDesc       = cCashAccDesc
      agent.ARGLRef            = cARAcc
      agent.ARGLDesc           = cARAccDesc
      agent.ARRefundGLRef      = cRefARAcc
      agent.ARRefundGLDesc     = cRefARAccDesc
      agent.ARWriteOffGLRef    = cWroffARAcc
      agent.ARWriteOffGLDesc   = cWroffARAccDesc
      agent.orgID              = cOrigOrg
      .
      
    run ModifyAgent in hFileDataSrv (input  table agent, 
                                     input  iOldOrgID,
                                     output cOrgID, 
                                     output std-lo).
     if std-lo then
      do:
        if lModifyOrganization 
         then
          run modifyOrganizationCheck in hFileDataSrv (INPUT lModifyOrganization). 
            
        run GetAgentManagers   in hFileDataSrv (output table agentmanager).
        displayHeader().
        displayDetail(). 
      end.
    if lLogoChange
     then
      do:
        run SetAgentLogo in hFileDataSrv (input agent.agentID, input chimage, output std-lo).     
        displayHeader().
        displayDetail().
      end.                   
  end.  
  
  doModify(false).
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION savePage wWin 
FUNCTION savePage RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  case pPageNumber:
    when 1 then
    do:
      std-lo = saveDetail().
      if not std-lo then
      return false.
    end.
    when 2 then
    do:
      /* Remittance page is read only */
    end.
    when 3 then
    do:
      /* A/R page is read only */
    end.
    when 4 then
    do:
      /* Claim page is read only */
    end.
    when 5 then
    do:
      /* Audit page is read only */
    end.
    when 6 then
    do:
      /* Compliance page is read only */
    end.
    when 7 then
    do:      
    end.
    when 8 then
    do:      
    end.
    when 9 then
    do:      
    end.
    when 10 then
    do:      
    end.
  end case.
  
  saveStateDisable(pPageNumber).

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveStateDisable wWin 
FUNCTION saveStateDisable RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
      
  case pPageNumber:
    when 1 then
    do with frame frDetail:
      bSave:sensitive = no.
      bCancel:sensitive = no.            
    end. 
  
  end case.
  
  dataChanged = no.  
     
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveStateEnable wWin 
FUNCTION saveStateEnable RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  case pPageNumber:
    when 1 then
    do with frame frDetail:
      bSave:sensitive = yes.
      bCancel:sensitive = yes.
    end.
     
  end case.
   
  /* only flag that the data was changed if the tab is not on the accounting screen */
  dataChanged = yes.

  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setHeaderState wWin 
FUNCTION setHeaderState RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  do with frame fMain:
  end.
      
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setLockState wWin 
FUNCTION setLockState RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setPageState wWin 
FUNCTION setPageState RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int, input pState as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  case pPageNumber:
    when 1 then
    run setContainerState(frame frDetail:handle, pState).
    when 2 then
    run setContainerState(frame frpersonagent:handle, pState).
    when 3 then
    run setContainerState(frame frEvent:handle, pState).
    when 4 then
    run setContainerState(frame frBatch:handle, pState).
    when 5 then
    run setContainerState(frame frActivity:handle, pState).
    when 6 then
    run setContainerState(frame frAR:handle, pState).    
    when 7 then
    run setContainerState(frame frClaim:handle, pState).
    when 8 then
    run setContainerState(frame frAudit:handle, pState).
    when 9 then
    run setContainerState(frame frCompliance:handle, pState). 
    when 10 then
    run setContainerState(frame frAlert:handle, pState).
  end case.

  return false.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setZeroForClosedPeriod wWin 
FUNCTION setZeroForClosedPeriod RETURNS LOGICAL
  ( input ipiYear     as integer,
    input ipiYearSeq  as integer) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable iCount as integer no-undo.
  
  do with frame frBatch:
  end.

  return true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

