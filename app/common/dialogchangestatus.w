&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File:dialogchangestatus.w

  Description:Dialog to change the status of agent. 

  Input Parameters:
  pText is simply to display some textual context in the fill-in
  pFrom - From status  

  Output Parameters:
  pTo -Selected value by the user
  pCancel - Whether the user wants to save or not
      
  Author:Rahul Sharma

  Created:08.01.2019 
  @Modified
  Date         Name            Description 
  03-27-2020   Archana Gupta   Modified code for Phase 2
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* --Parameters Definitions ---                                         */
define input  parameter ipcText        as character no-undo.
define input  parameter ipcFrom        as character no-undo.
define input  parameter ipcType        as character no-undo.  /* Agent or Attorney */
define output parameter ipcTo          as character no-undo.
define output parameter oplCancel      as logical   no-undo init true.

{lib/std-def.i}
{lib/com-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbStatus bCancel 
&Scoped-Define DISPLAYED-OBJECTS fiText fiStatusFrom cbStatus 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Cancel".

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14 TOOLTIP "Create".

DEFINE VARIABLE cbStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 25.6 BY 1 NO-UNDO.

DEFINE VARIABLE fiStatusFrom AS CHARACTER FORMAT "X(256)":U 
     LABEL "Change Status From" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE fiText AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 80.4 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fiText AT ROW 1.48 COL 6.2 COLON-ALIGNED NO-LABEL WIDGET-ID 50
     fiStatusFrom AT ROW 2.86 COL 26.4 COLON-ALIGNED WIDGET-ID 34
     cbStatus AT ROW 2.86 COL 51.4 COLON-ALIGNED WIDGET-ID 40
     bSave AT ROW 4.19 COL 32.6
     bCancel AT ROW 4.19 COL 49.2
     SPACE(31.59) SKIP(0.46)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Change Status"
         DEFAULT-BUTTON bSave CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bSave IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fiStatusFrom IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fiText IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fiText:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Change Status */
do:
  oplCancel = false.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel Dialog-Frame
ON CHOOSE OF bCancel IN FRAME Dialog-Frame /* Cancel */
do:
  oplCancel = false.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave Dialog-Frame
ON CHOOSE OF bSave IN FRAME Dialog-Frame /* Save */
do:
  ipcTo = cbStatus:screen-value.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbStatus Dialog-Frame
ON VALUE-CHANGED OF cbStatus IN FRAME Dialog-Frame /* To */
do:
  bSave:sensitive = true.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINdoW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? 
 then
  frame {&frame-name}:parent = active-window.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and end-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

  run enable_UI.
  run setdata.
  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fiText fiStatusFrom cbStatus 
      WITH FRAME Dialog-Frame.
  ENABLE cbStatus bCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData Dialog-Frame 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if ipcType = {&Agent}
   then    
    publish "GetStatusList" ("AMD", "Agent", "Status" ,input ipcFrom , output std-ch).
  else
   publish "GetStatusList" ("COM", "Attorney", "Status" ,input ipcFrom , output std-ch). 
  
  std-ch = replace(trim(std-ch),"^",",").
  cbStatus:list-item-pairs = std-ch.

  /* If list has only one entry then select it as default. */
  if num-entries(std-ch) = 2
   then 
    assign
        cbStatus:screen-value = entry(2,std-ch)
        bSave:sensitive       = true.      
  
  if ipcType = {&Agent}
   then
    publish "GetSysPropDesc" ("AMD", "Agent", "Status", ipcFrom, output std-ch).    
  else
   publish "GetSysPropDesc" ("COM", "Attorney", "Status", ipcFrom, output std-ch).
    
  assign fiText:screen-value       = ipcText
         fiStatusFrom:screen-value = std-ch.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

