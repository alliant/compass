&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/

define input parameter pApp          as character no-undo. /* Application ID */
define input parameter pServerPath   as character no-undo.
define input parameter pDesc         as character no-undo. /* UI Transaction description */
define input parameter pValidActions as character no-undo. /* new,delete,open,modify,email */

/* Used to call Compass server to hold reference that docs exist */
define input parameter pEntityType   as character no-undo.
define input parameter pEntityID     as character no-undo.
define input parameter pEntitySeq    as integer no-undo.

/* variables used to see if the folder is available (and could be created if not) */
define variable tDisable as logical no-undo initial false.

/* variables used to store original values for resize */
define variable dColumnWidth   as decimal no-undo.


CREATE WIDGET-POOL.

{lib/std-def.i}
{lib/set-button-def.i}

{tt/doc.i}
{tt/doc.i &tableAlias="tempdoc"}

{lib/add-delimiter.i}
{lib/urlencode.i}
{lib/winlaunch.i}
{lib/repository-procedures.i}

 DEFINE MENU docpopmenu TITLE "Actions"
  MENU-ITEM m_PopOpen LABEL "Open"
  RULE
  menu-item m_PopDelete label "Delete..."
  menu-item m_PopModify label "Categorize..."
  RULE
  menu-item m_PopNew label "Upload..."
  .

 ON 'choose':U OF menu-item m_PopOpen in menu docpopmenu
 DO:
  run ActionOpen in this-procedure (output std-lo).
  RETURN.
 END.

 ON 'choose':U OF menu-item m_PopDelete in menu docpopmenu
 DO:
  run ActionDelete in this-procedure.
  RETURN.
 END.

 ON 'choose':U OF menu-item m_PopModify in menu docpopmenu
 DO:
  run ActionCategorize in this-procedure.
  RETURN.
 END.

 ON 'choose':U OF menu-item m_PopNew in menu docpopmenu
 DO:
  run ActionNew in this-procedure.
  RETURN.
 END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwDocs

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES document

/* Definitions for BROWSE brwDocs                                       */
&Scoped-define FIELDS-IN-QUERY-brwDocs document.displayName document.details document.createdDate document.fileSizeDesc document.createdBy   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDocs   
&Scoped-define SELF-NAME brwDocs
&Scoped-define QUERY-STRING-brwDocs FOR EACH document by document.displayName
&Scoped-define OPEN-QUERY-brwDocs OPEN QUERY {&SELF-NAME} FOR EACH document by document.displayName.
&Scoped-define TABLES-IN-QUERY-brwDocs document
&Scoped-define FIRST-TABLE-IN-QUERY-brwDocs document


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tCategory brwDocs bFileRefresh 
&Scoped-Define DISPLAYED-OBJECTS tCategory 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshData C-Win 
FUNCTION refreshData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD uploadFile C-Win 
FUNCTION uploadFile RETURNS LOGICAL PRIVATE
  ( input pFile as character,
    output pMsg as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bFileModify  NO-FOCUS
     LABEL "Categorize" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify the category of the selected document(s)".

DEFINE BUTTON bFileDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 7.2 BY 1.71 TOOLTIP "Remove the previously saved document(s)".

DEFINE BUTTON bFileDownload  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open the selected document(s)".

DEFINE BUTTON bFileRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh the list of documents".

DEFINE BUTTON bFileUpload  NO-FOCUS
     LABEL "Upload" 
     SIZE 7.2 BY 1.71 TOOLTIP "Upload a new document".

DEFINE VARIABLE tCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Default Category" 
     VIEW-AS FILL-IN 
     SIZE 34 BY 1 TOOLTIP "Category associated to newly added documents" NO-UNDO.

DEFINE RECTANGLE rButtons
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 38.2 BY 2.05.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwDocs FOR 
      document SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwDocs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDocs C-Win _FREEFORM
  QUERY brwDocs DISPLAY
      document.displayName format "x(256)" width 50 label "File"
 document.details format "x(200)" width 25 label "Category"
 document.createdDate format "x(25)" label "Date Saved"
 document.fileSizeDesc format "x(15)" width 10 label "Size"
       document.createdBy format "x(25)" label "Saved By"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE DROP-TARGET SIZE 147.6 BY 13.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bFileDownload AT ROW 1.38 COL 2.6 WIDGET-ID 24
     tCategory AT ROW 1.76 COL 59 COLON-ALIGNED WIDGET-ID 40
     brwDocs AT ROW 3.38 COL 2 WIDGET-ID 200
     bFileModify AT ROW 1.38 COL 24.8 WIDGET-ID 36
     bFileRefresh AT ROW 1.38 COL 32.2 WIDGET-ID 32
     bFileUpload AT ROW 1.38 COL 10 WIDGET-ID 4
     bFileDelete AT ROW 1.38 COL 17.4 WIDGET-ID 6
     rButtons AT ROW 1.24 COL 2 WIDGET-ID 22
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 149.2 BY 16.52 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Documents"
         HEIGHT             = 16.52
         WIDTH              = 149.2
         MAX-HEIGHT         = 19.05
         MAX-WIDTH          = 160.6
         VIRTUAL-HEIGHT     = 19.05
         VIRTUAL-WIDTH      = 160.6
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwDocs tCategory fMain */
/* SETTINGS FOR BUTTON bFileModify IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bFileModify:PRIVATE-DATA IN FRAME fMain     = 
                "Email".

/* SETTINGS FOR BUTTON bFileDelete IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bFileDelete:PRIVATE-DATA IN FRAME fMain     = 
                "Delete".

/* SETTINGS FOR BUTTON bFileDownload IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bFileDownload:PRIVATE-DATA IN FRAME fMain     = 
                "Open".

ASSIGN 
       bFileRefresh:PRIVATE-DATA IN FRAME fMain     = 
                "Email".

/* SETTINGS FOR BUTTON bFileUpload IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bFileUpload:PRIVATE-DATA IN FRAME fMain     = 
                "New".

ASSIGN 
       brwDocs:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwDocs:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR RECTANGLE rButtons IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDocs
/* Query rebuild information for BROWSE brwDocs
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH document by document.displayName.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwDocs */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Documents */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Documents */
DO:
  publish "WindowClosed" ("DOCUMENTS").

  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Documents */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileModify C-Win
ON CHOOSE OF bFileModify IN FRAME fMain /* Categorize */
DO:
  run ActionCategorize in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileDelete C-Win
ON CHOOSE OF bFileDelete IN FRAME fMain /* Del */
DO:
  run ActionDelete in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileDownload
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileDownload C-Win
ON CHOOSE OF bFileDownload IN FRAME fMain /* Open */
DO:
   run ActionOpen in this-procedure (output std-lo).
   if std-lo 
    then apply "WINDOW-CLOSE" to frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileRefresh C-Win
ON CHOOSE OF bFileRefresh IN FRAME fMain /* Refresh */
DO:
  refreshData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileUpload
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileUpload C-Win
ON CHOOSE OF bFileUpload IN FRAME fMain /* Upload */
DO:
  run ActionNew in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDocs
&Scoped-define SELF-NAME brwDocs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON DEFAULT-ACTION OF brwDocs IN FRAME fMain
DO:
   run ActionOpen in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON DROP-FILE-NOTIFY OF brwDocs IN FRAME fMain
DO:
  run ActionFilesDropped in this-procedure (self:handle).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON ROW-DISPLAY OF brwDocs IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON START-SEARCH OF brwDocs IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON VALUE-CHANGED OF brwDocs IN FRAME fMain
DO:

  if brwDocs:num-selected-rows > 0 
   then assign
          bFileDownload:sensitive = lookup("open", pValidActions) > 0
          bFileDelete:sensitive = lookup("delete", pValidActions) > 0
          bFileModify:sensitive = lookup("modify", pValidActions) > 0 
          menu-item m_PopOpen:sensitive in menu docpopmenu = bFileDownload:sensitive
          menu-item m_PopDelete:sensitive in menu docpopmenu = bFileDelete:sensitive
          menu-item m_PopModify:sensitive in menu docpopmenu = bFileModify:sensitive
          .
   else assign
          bFileDownload:sensitive = false
          bFileDelete:sensitive = false
          bFileModify:sensitive = false
          menu-item m_PopOpen:sensitive in menu docpopmenu = false
          menu-item m_PopDelete:sensitive in menu docpopmenu = false
          menu-item m_PopModify:sensitive in menu docpopmenu = false
          .
          
  if available document
   then tCategory:screen-value = document.details.
  
  bFileUpload:sensitive in frame {&frame-name} = lookup("new", pValidActions) > 0.
  menu-item m_PopNew:sensitive in menu docpopmenu = bFileUpload:sensitive.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-show.i}
{lib/win-close.i}
{lib/brw-main.i}

if pDesc > "" 
 then {&window-name}:title = {&window-name}:title + " for " + pDesc.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.


{lib/set-button.i &label="FileDownload" &image="images/download.bmp"   &inactive="images/download-i.bmp"}
{lib/set-button.i &label="FileUpload"   &image="images/upload.bmp"     &inactive="images/upload-i.bmp"}
{lib/set-button.i &label="FileDelete"   &image="images/erase.bmp"      &inactive="images/erase-i.bmp"}
{lib/set-button.i &label="FileRefresh"  &image="images/sync.bmp"       &inactive="images/sync-i.bmp"}
{lib/set-button.i &label="FileModify"   &image="images/update.bmp"     &inactive="images/update-i.bmp"}
setButtons().

browse brwDocs:POPUP-MENU = MENU docpopmenu:HANDLE.

/* Tell the parent window we are open even though we haven't pulled 
   directory data yet */
publish "WindowOpened" ("DOCUMENTS").

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
       
{&window-name}:window-state = 2.

/* Prepend root folder based on config setting (dev versus prod) */
std-ch = "".
publish "GetRepositoryRoot" (output std-ch).
pServerPath = std-ch + pServerPath.

{lib/pbshow.i "'Initializing window, please wait...'"}
{lib/pbupdate.i "'Initializing window'" 10}

/* login */
/* The reason for no uid or password is because the publish "SetRepositoryToken" will */
/* make a server call to log the user in */
setUserToken("", "", output std-ch). 

/* create the new folder in case it's not created */
run NewRepositoryFolder (pServerPath, output std-lo, output std-ch).
if not std-lo
 then
  do:
    message "Folder not found and could not create folder: " + std-ch view-as alert-box information buttons ok.
    assign
      tDisable = true
      std-ch   = pServerPath 
      .
  end.
 else  std-ch = pServerPath 
              + "  (" 
              + (if lookup("new", pValidActions) > 0 then "C" else "")
              + (if lookup("open", pValidActions) > 0 then "R" else "")
              + (if lookup("modify", pValidActions) > 0 then "U" else "")
              + (if lookup("delete", pValidActions) > 0 then "D" else "")
              + ")".
status input std-ch in window {&window-name}.
status default std-ch in window {&window-name}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  RUN enable_UI.
  if not tDisable
   then 
    do:
      {lib/pbupdate.i "'Initializing window'" 80}
      refreshData().
      /* upload */
      bFileUpload:sensitive = lookup("new", pValidActions) > 0.
      menu-item m_PopNew:sensitive in menu docpopmenu = lookup("new", pValidActions) > 0.
      /* modify */
      bFileModify:sensitive = lookup("modify", pValidActions) > 0.
      menu-item m_PopModify:sensitive in menu docpopmenu = lookup("modify", pValidActions) > 0.
    end.
   else
    assign
      bFileDelete:sensitive   = false
      bFileDownload:sensitive = false
      bFileModify:sensitive   = false
      bFileRefresh:sensitive  = false
      bFileUpload:sensitive   = false
      .
   
  {lib/get-column-width.i &browse-name=brwDocs &col="'details'" &var=dColumnWidth}
  run windowResized in this-procedure.
  
  {lib/pbhide.i}
  {&window-name}:window-state = 3.
      
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionCategorize C-Win 
PROCEDURE ActionCategorize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tFileCnt     as integer   no-undo.
  define variable tGoodCnt     as integer   no-undo.
  define variable tMsg         as character no-undo.
  define variable tNewCategory as character no-undo.
  
  if lookup("modify", pValidActions) = 0
   then return.
  
  if browse brwDocs:num-selected-rows = 0
   then return.
  
  run dialogdocumentcategory.w (output tNewCategory, output std-lo).
  if not std-lo
   then return.
  

  do tFileCnt = 1 to browse brwDocs:num-selected-rows:
    browse brwDocs:fetch-selected-row(tFileCnt).
    
    if not available document
     then next.
    
    if document.identifier = ""
     then
      do: 
        tMsg = addDelimiter(tMsg, chr(19)) + document.fullpath + " is invalid.".
        next.
      end.
    
    session:set-wait-state("general").
    run ModifyRepositoryItem (document.fullpath, document.name, tNewCategory, output std-lo, output std-ch).
    session:set-wait-state("").
    
    if not std-lo
     then tMsg = addDelimiter(tMsg, chr(19)) + "Unable to edit " + document.name + ".".
     else tGoodCnt = tGoodCnt + 1.
  end.
 
  if tMsg > "" or tGoodCnt <> browse brwDocs:num-selected-rows
   then MESSAGE "Failed to edit " (browse brwDocs:num-selected-rows - tGoodCnt) " files." skip(1) tMsg skip(2) "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
  
  if tGoodCnt > 0
   then refreshData().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDelete C-Win 
PROCEDURE ActionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tFileCnt as integer no-undo.
 def var tMsg as character no-undo.
 def var tGoodCnt as integer no-undo.

 if lookup("delete", pValidActions) = 0
  then return.

 if browse brwDocs:num-selected-rows = 0
  then 
   do: bFileDelete:sensitive in frame {&frame-name} = false.
       menu-item m_PopDelete:sensitive in menu docpopmenu = false.
       return.
   end.

 std-lo = true.
 publish "GetConfirmDelete" (output std-lo).
 if std-lo
  then
   do:
     std-lo = false.
     MESSAGE "Document(s) will be permanently removed. Continue?" VIEW-AS ALERT-BOX question BUTTONS Yes-No update std-lo.
     if not std-lo 
      then return.
   end.

 do tFileCnt = 1 to browse brwDocs:num-selected-rows:
  browse brwDocs:fetch-selected-row(tFileCnt).

  if not available document 
   then next.

  if document.identifier = ""
   then
    do: tMsg = tMsg + (if tMsg > "" then chr(19) else "") 
           + document.fullpath + " is invalid.".
        next.
    end.

  session:set-wait-state("general").
  run DeleteRepositoryItem (document.fullpath, output std-lo, output std-ch).
  session:set-wait-state("").

  if not std-lo 
   then tMsg = addDelimiter(tMsg, chr(19)) + "Unable to remove " + document.name + " .".
   else tGoodCnt = tGoodCnt + 1.
 end.

 if tMsg > ""
   or tGoodCnt <> browse brwDocs:num-selected-rows
  then
   MESSAGE "Failed to remove " (browse brwDocs:num-selected-rows - tGoodCnt) " files." skip tMsg skip "Contact the System Administrator." VIEW-AS ALERT-BOX warning BUTTONS OK.
  else refreshData().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionEmail C-Win 
PROCEDURE ActionEmail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 
 def var tTo as character no-undo.
 def var tSubject as character no-undo.
 def var tMsg as character no-undo.
 def var tExpiration as integer init 30 no-undo.
 def var tNotify as logical init true no-undo.
 def var tDownloads as integer init 2 no-undo.

 if lookup("email", pValidActions) = 0
  then return.

 if browse brwDocs:num-selected-rows = 0
  then return.

 if document.identifier = ""
  then
   do: 
    MESSAGE "Document record is corrupted.  Please contact the System Administrator."
     VIEW-AS ALERT-BOX INFO BUTTONS OK.
   end.

 MAIL-BLOCK:
 repeat:
  /* Prompt for email attributes */
  run dialogdocumentemail.w (input document.displayName,
                             input-output tTo,
                             input-output tSubject,
                             input-output tMsg,
                             input-output tExpiration,
                             input-output tNotify,
                             input-output tDownloads,
                             output std-lo).
  if not std-lo 
   then leave MAIL-BLOCK.
 
  run RequestRepositoryDownload 
      (document.fullpath,
       tTo,
       tSubject,
       tMsg,
       "",
       false,
       true,
       tExpiration,
       tNotify,
       false,
       tDownloads,
       false,
       output std-lo,
       output std-ch).
 
  if std-lo 
   then 
    do: publish "AddContact" (tTo).
        leave MAIL-BLOCK.
    end.

  MESSAGE std-ch
   VIEW-AS ALERT-BOX warning BUTTONS OK.
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionFilesDropped C-Win 
PROCEDURE ActionFilesDropped :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pWidget as handle.

 def var tFileCnt as integer no-undo.
 def var tMsg as character no-undo.

 if not valid-handle(pWidget) 
  then return.

 if lookup("new", pValidActions) = 0
  then
   do:  pWidget:end-file-drop().
        return.
   end.

 FILE-LOOP:
 do tFileCnt = 1 to pWidget:num-dropped-files:
  std-ch = pWidget:get-dropped-file(tFileCnt).
  std-lo = uploadFile(std-ch, output tMsg).
  if not std-lo 
   then
    do:
        MESSAGE tMsg skip(2)
                "File uploads interrupted."
         VIEW-AS ALERT-BOX error BUTTONS OK.
        leave FILE-LOOP.
    end.
 end.
 pWidget:end-file-drop().

 refreshData().
 if can-find(first document) 
  then apply "value-changed" to brwDocs in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNew C-Win 
PROCEDURE ActionNew :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tFile as character no-undo.

 if lookup("new", pValidActions) = 0
  then return.

 system-dialog get-file tFile
  must-exist 
  title "Document to Upload"
  update std-lo.
 if not std-lo 
  then return.
 
 publish "GetConfirmFileUpload" (output std-lo).
 if std-lo 
  then
   do:
       MESSAGE tFile " will be uploaded.  Continue?"
        VIEW-AS ALERT-BOX question BUTTONS YES-NO title "Confirmation" update std-lo.
       if not std-lo 
        then return.
   end.

 std-lo = uploadFile(tFile, output std-ch).
 if not std-lo 
  then
   do:
       MESSAGE std-ch
        VIEW-AS ALERT-BOX warning BUTTONS OK.
       return.
   end.

 refreshData().
 if can-find(first document) 
  then apply "value-changed" to brwDocs in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionOpen C-Win 
PROCEDURE ActionOpen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter pSuccess as logical initial false no-undo.
  
  define variable tLocalPath as character no-undo.
  define variable tMsg as character no-undo.
  define variable tFile as character no-undo.
  define variable tFileCnt as integer no-undo.
  define variable tGoodCnt as integer no-undo.
  
  if lookup("open", pValidActions) = 0
   then return.
  
  if browse brwDocs:num-selected-rows = 0
   then return.
  
  /* das:ActionOpen - handle multiple */
  
  publish "GetTempDir" (output tLocalPath).
  
  do tFileCnt = 1 to browse brwDocs:num-selected-rows:
    browse brwDocs:fetch-selected-row(tFileCnt).
    
    if not available document 
     then next.
    
    if document.identifier = ""
     then
      do: tMsg = tMsg + (if tMsg > "" then chr(19) else "") 
             + document.fullpath + " is invalid.".
          next.
      end.
    
    tFile = tLocalPath + document.name.
    if search(tFile) <> ? 
     then os-delete value(tFile).
    
    session:set-wait-state("general").
    run DownloadRepositoryFile (document.fullpath, tFile, output std-lo, output std-ch).
    session:set-wait-state("").
    
    if not std-lo 
     then
      do: tMsg = addDelimiter(tMsg, chr(19)) + "Unable to download " + document.name + " .".
          next.
      end.
    
    if search(tFile) = ? 
     then
      do: tMsg = addDelimiter(tMsg, chr(19)) + document.name + " failed to download.".
          next.
      end.
    
    publish "AddTempFile" (document.name, tFile).
    run util/openfile.p (tFile).
    tGoodCnt = tGoodCnt + 1.
  end.
  if tMsg > ""
    or tGoodCnt <> browse brwDocs:num-selected-rows
   then
    MESSAGE "Failed to download " (browse brwDocs:num-selected-rows - tGoodCnt) " files." skip(1)
             tMsg skip(2)
            "Contact the System Administrator."
     VIEW-AS ALERT-BOX warning BUTTONS OK.
   else pSuccess = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tCategory 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE tCategory brwDocs bFileRefresh 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/brw-sortdata.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

 /* fMain components */
 {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5. 
    
  {lib/resize-column.i &browse-name=brwDocs &col="'details'" &var=dColumnWidth}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshData C-Win 
FUNCTION refreshData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define buffer document for document.
  
  close query brwDocs.
  empty temp-table document.
  empty temp-table tempdoc.
  
  run GetRepositoryFolder (pServerPath, output table document, output std-lo, output std-ch).
  
  if not std-lo 
   then
    do:
        message std-ch
          view-as alert-box warning.
        return false.
    end.
  
  dataSortBy = "".
  dataSortDesc = false.
  run sortData in this-procedure ("displayName").
  apply "value-changed" to brwDocs in frame {&frame-name}.
  
  if not can-find(first document) 
   then run server/unlinkdocument.p (pEntityType, pEntityID, pEntitySeq, output std-lo, output std-ch).
   else
    for first document no-lock
        where document.details > "":
        
      tCategory:screen-value = document.details.
      run server/linkdocument.p (pEntityType, pEntityID, pEntitySeq, output std-lo, output std-ch).
      {&window-name}:move-to-top().
    end.
  
  RETURN true.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION uploadFile C-Win 
FUNCTION uploadFile RETURNS LOGICAL PRIVATE
  ( input pFile as character,
    output pMsg as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tFileID   as character no-undo.
  define variable tFileName as character no-undo.
  
  file-info:file-name = pFile.
  if file-info:full-pathname = ?
    or index(file-info:file-type, "F") = 0
    or index(file-info:file-type, "R") = 0
   then 
    do:
      pMsg = pFile + " is not accessible".
      return false.
    end.
   else tFileName = substring(pFile, r-index(pFile, "\") + 1).
  
  {lib/pbshow.i "'Uploading document, please wait...'"}
  {lib/pbupdate.i "'Uploading document...'" 10}
  
  setUserToken("", "", output std-ch).
  run UploadRepositoryFile (pFile, pServerPath, output std-lo, output pMsg).
  
  if not std-lo 
   then
    do:
      if pMsg = ""
       then pMsg = "Could not upload file".
      {lib/pbhide.i}
      return false.
    end.
  
  {lib/pbupdate.i "'Uploading document...'" 80}
  
  if pEntityType > ""
   then run server/linkdocument.p (pEntityType, pEntityID, pEntitySeq, output std-lo, output std-ch).
   
  std-lo = true.
  
  /* Set the file attributes for sorting */
  if tCategory:screen-value in frame {&frame-name} > ""
   then 
    do:
      run GetRepositoryItemID (pServerPath + tFileName, output tFileID, output std-lo, output pMsg).
      if std-lo
       then run ModifyRepositoryItem (tFileID, "" , tCategory:screen-value in frame {&frame-name}, output std-lo, output pMsg).
    end.
      
  
  run ShowWindow.
  {lib/pbupdate.i "'Upload complete'" 100 &delay=1}
  {lib/pbhide.i}
  
  RETURN std-lo.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

