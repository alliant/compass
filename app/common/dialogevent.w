&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogevent.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Sachin Chaturvedi

  Created:05/08/2020
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{tt/event.i}
{tt/event.i &tableAlias=ttevent}


/* Parameters Definitions ---                                           */
define input  parameter iphfileDataSrv  as handle.
define input  parameter ipcAction       as  character  no-undo.
define input  parameter table         for event.
define output parameter opieventID        as  integer    no-undo.
define output parameter oplSuccess      as  logical    no-undo.


/* Local Variable Definitions ---                                       */
define variable cTrackName     as character no-undo.
define variable cTrackMonth    as character no-undo.
define variable cTrackDay      as character no-undo.
define variable cTrackYear     as character no-undo.
define variable lTrackReminder as logical no-undo.
define variable lTrackPrivate  as logical no-undo.
define variable cTrackContactID  as character no-undo.
define variable cTrackDesc     as character no-undo.
define variable cPersonID      as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bPrsnLookup flName cbMonth cbDay cbYear ~
tReminder tPrivate flContact edDesc Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS flName cbMonth cbDay cbYear tReminder ~
tPrivate flContact edDesc 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bPrsnLookup  NO-FOCUS
     LABEL "Lookup" 
     SIZE 4.6 BY 1.14 TOOLTIP "Person lookup".

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 12 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 12 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cbDay AS CHARACTER FORMAT "X(256)" 
     LABEL "Day" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 8.6 BY 1 NO-UNDO.

DEFINE VARIABLE cbMonth AS INTEGER FORMAT ">9":U INITIAL 1 
     LABEL "Month" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "January",1,
                     "February",2,
                     "March",3,
                     "April",4,
                     "May",5,
                     "June",6,
                     "July",7,
                     "August",8,
                     "September",9,
                     "October",10,
                     "November",11,
                     "December",12
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE cbYear AS CHARACTER FORMAT "X(256)":U 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 14.2 BY 1 NO-UNDO.

DEFINE VARIABLE edDesc AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 80 BY 7.71 NO-UNDO.

DEFINE VARIABLE flContact AS CHARACTER FORMAT "X(256)":U 
     LABEL "Contact" 
     VIEW-AS FILL-IN 
     SIZE 75.8 BY 1 NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Event" 
     VIEW-AS FILL-IN 
     SIZE 80 BY 1 NO-UNDO.

DEFINE VARIABLE tPrivate AS LOGICAL INITIAL no 
     LABEL "Private" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.

DEFINE VARIABLE tReminder AS LOGICAL INITIAL no 
     LABEL "Recurring" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bPrsnLookup AT ROW 13.67 COL 94.4 WIDGET-ID 292 NO-TAB-STOP 
     flName AT ROW 3.38 COL 16 COLON-ALIGNED WIDGET-ID 42
     cbMonth AT ROW 2 COL 16 COLON-ALIGNED WIDGET-ID 46
     cbDay AT ROW 2 COL 51.2 COLON-ALIGNED WIDGET-ID 90
     cbYear AT ROW 2 COL 81.8 COLON-ALIGNED WIDGET-ID 62
     tReminder AT ROW 12.76 COL 18.2 WIDGET-ID 58
     tPrivate AT ROW 12.76 COL 51 WIDGET-ID 294
     flContact AT ROW 13.76 COL 16 COLON-ALIGNED WIDGET-ID 56 NO-TAB-STOP 
     edDesc AT ROW 4.76 COL 18 NO-LABEL WIDGET-ID 52
     Btn_OK AT ROW 15.76 COL 38.8
     Btn_Cancel AT ROW 15.76 COL 53
     "Notes:" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 4.71 COL 11.2 WIDGET-ID 54
     SPACE(85.19) SKIP(11.95)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Event"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flContact:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Event */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrsnLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrsnLookup Dialog-Frame
ON CHOOSE OF bPrsnLookup IN FRAME Dialog-Frame /* Lookup */
do:
  run openDialog in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  run saveEvent in this-procedure.
  if not oplSuccess 
   then
    return no-apply.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbDay
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbDay Dialog-Frame
ON VALUE-CHANGED OF cbDay IN FRAME Dialog-Frame /* Day */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbMonth Dialog-Frame
ON VALUE-CHANGED OF cbMonth IN FRAME Dialog-Frame /* Month */
DO:
  define variable cCurrDayVal as character no-undo.
  cCurrDayVal = cbDay:input-value.
  
  run getDayCombo in this-procedure.
  
  cbDay:screen-value = if can-do(cbDay:list-items,cCurrDayVal) then cCurrDayVal else entry(1,cbDay:list-items).
  
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbYear Dialog-Frame
ON VALUE-CHANGED OF cbYear IN FRAME Dialog-Frame /* Year */
DO:
  define variable cCurrDayVal as character no-undo.
  cCurrDayVal = cbDay:input-value.
  
  run getDayCombo in this-procedure.
  
  cbDay:screen-value = if can-do(cbDay:list-items,cCurrDayVal) then cCurrDayVal else entry(1,cbDay:list-items).
  
  if cbYear:screen-value = "Unknown" 
   then
    assign
        tReminder:checked   = true
        tReminder:sensitive = false
        .
   else if ipcAction = "N"  /* Disable in case of edit */
    then
     tReminder:sensitive = true.
    
  run enableDisableSave in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edDesc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edDesc Dialog-Frame
ON VALUE-CHANGED OF edDesc IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flName Dialog-Frame
ON VALUE-CHANGED OF flName IN FRAME Dialog-Frame /* Event */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tPrivate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tPrivate Dialog-Frame
ON VALUE-CHANGED OF tPrivate IN FRAME Dialog-Frame /* Private */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tReminder
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tReminder Dialog-Frame
ON VALUE-CHANGED OF tReminder IN FRAME Dialog-Frame /* Recurring */
DO:
  define variable cCurrYrVal as character no-undo.
  cCurrYrVal = cbYear:input-value.
  
  run updateYearCombo in this-procedure.
  
  cbYear:screen-value = if can-do(cbYear:list-items,cCurrYrVal) then cCurrYrVal else string(year(today)).
  
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bPrsnLookup:load-image("images/s-lookup.bmp").
bPrsnLookup:load-image-insensitive("images/s-lookup-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  run displayData in this-procedure.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  find first event no-error.
  if not available event
   then 
    return.

  assign
    flName:screen-value     = event.name
    flContact:screen-value  = event.personName
    tPrivate:checked        = event.isPrivate
    edDesc:screen-value     = event.description
    tReminder:checked       = if ipcAction = "N" then true else event.reminder
    opiEventID              = event.eventID
    cPersonID               = if event.personID ne "" then event.personID else "" 
    .
  
  run getYearCombo    in this-procedure.
  run updateYearCombo in this-procedure.
  
  if ipcAction = "N"
   then
    assign
      frame dialog-frame:title = "New Event"
      Btn_OK :label            = "Create" 
      Btn_OK :tooltip          = "Create"
      cbYear:screen-value      = string(year(today))
      .
   else       
     assign
       frame dialog-frame:title = "Edit Event"
       Btn_OK :label            = "Save" 
       Btn_OK :tooltip          = "Save"
       cbMonth:screen-value     = string(event.month)
       cbDay:screen-value       = string(event.day)
       cbYear:screen-value      = if string(event.year) = "0" then "Unknown" else string(event.year)
       tReminder:sensitive      = false 
       .

  assign
      cTrackName       = flName:input-value
      cTrackMonth      = cbMonth:input-value
      cTrackDay        = cbDay:input-value
      cTrackYear       = cbYear:input-value
      lTrackReminder   = tReminder:checked
      lTrackPrivate    = tPrivate:checked
      cTrackContactID    = cPersonID
      cTrackDesc       = edDesc:input-value
      .
      
  apply 'value-changed' to cbYear.   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  if ipcAction = "N"
   then
    Btn_OK:sensitive = (flName:input-value <> "").
  else
   Btn_OK:sensitive = (flName:input-value <> "") and
                       (flName:input-value <> cTrackName or
                        cbMonth:input-value <> cTrackMonth or
                        cbDay:input-value <> cTrackDay or
                        cbYear:input-value <> cTrackYear or
                        tReminder:checked <> lTrackReminder or
                        tPrivate:checked <> lTrackPrivate  or
                        cPersonID <> cTrackContactID or
                        edDesc:input-value <> cTrackDesc
                        )
                        .   
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flName cbMonth cbDay cbYear tReminder tPrivate flContact edDesc 
      WITH FRAME Dialog-Frame.
  ENABLE bPrsnLookup flName cbMonth cbDay cbYear tReminder tPrivate flContact 
         edDesc Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getDayCombo Dialog-Frame 
PROCEDURE getDayCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCountDays as integer   no-undo. 
  define variable cMonths30  as character no-undo initial "4,6,9,11".
  define variable cMonths31  as character no-undo initial "1,3,5,7,8,10,12".
   
  do with frame {&frame-name}:
  end.
  
  cbDay:list-items = " ".
  
  if lookup(string(cbMonth:input-value),cMonths31) gt 0
   then
    do iCountDays = 1 to 31:
      cbDay:list-items = cbDay:list-items + "," + string(iCountDays).
    end.
   else if lookup(string(cbMonth:input-value),cMonths30) gt 0
    then
     do iCountDays = 1 to 30:
      cbDay:list-items = cbDay:list-items + "," + string(iCountDays).
    end.
   else
    do:
      if cbYear:input-value = "Unknown"   or
         cbYear:input-value modulo 4 eq 0     
       then
        do iCountDays = 1 to 29:
          cbDay:list-items = cbDay:list-items + "," + string(iCountDays).
        end.
       else
        do iCountDays = 1 to 28:
          cbDay:list-items = cbDay:list-items + "," + string(iCountDays).
        end.
    end.

  assign
      cbDay:list-items = trim(cbDay:list-items) 
      cbDay:list-items = trim(cbDay:list-items,",")
      cbDay:screen-value = entry(1,cbDay:list-items)
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getYearCombo Dialog-Frame 
PROCEDURE getYearCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iBaseYear as integer no-undo. 
  assign
      std-ch    = ""
      iBaseYear = 1950
      .
 
  do std-in = - 1 to (year(today) - iBaseYear) with frame {&frame-name}:
    std-ch = std-ch + "," + string(year(today) - std-in).
  end.
  
  assign
      cbYear:list-items    = trim(std-ch,",")
/*       cbYear:screen-value  = string(year(today)) */
      .
 
  std-ch = "".
  
  run getDayCombo in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openDialog Dialog-Frame 
PROCEDURE openDialog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
  define variable cCurrPersonID     as character no-undo.
  define variable cName             as character no-undo.
  
  for first event:
    cCurrPersonID = event.personID.
  end.
    
    
  run dialogpersonlookup.w(input  cCurrPersonID,
                           input  false,        /* True if add "New" */
                           input  false,        /* True if add "Blank" */
                           input  false,        /* True if add "ALL" */
                           output cPersonID,
                           output cName,
                           output std-lo).  
  if not std-lo 
   then
    return no-apply.
    
  if cName = "All"
   then
    flContact:screen-value = cName.
   else
    flContact:screen-value = if cPersonID = "" then "" else cName.
  
  run enableDisableSave in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveEvent Dialog-Frame 
PROCEDURE saveEvent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  empty temp-table ttevent.  
  for first event:
    create ttevent.
    buffer-copy event to ttevent.
  end.

  assign
    ttevent.name        = flName:input-value
    ttevent.month       = cbMonth:input-value
    ttevent.day         = integer(cbDay:input-value)
    ttevent.year        = if cbYear:input-value = "Unknown" then 0 else integer(cbYear:input-value)
    ttevent.personID    = if cPersonID ne "" then cPersonID else ""
    ttevent.reminder    = tReminder:checked
    ttevent.description = edDesc:input-value
    ttevent.personName  = flContact:input-value
    .

  if tPrivate:checked
   then 
    do:
      define variable cCurrentUID as character no-undo.
      publish "GetCredentialsID"   (output cCurrentUID).
      ttevent.userIDs = cCurrentUID.
    end.
   else
    ttevent.userIDs = "*". 
  
  if ipcAction = "M"
   then
    run modifyEvent in iphFileDataSrv (input table ttEvent,
                                       output oplSuccess).

   else
    run newEvent in iphFileDataSrv (input table ttevent,
                                    output opiEventID,
                                    output oplSuccess).      

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE updateYearCombo Dialog-Frame 
PROCEDURE updateYearCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if tReminder:checked and lookup("Unknown",cbYear:list-items) eq 0 
   then
    cbYear:list-items    = "Unknown" + "," + cbYear:list-items.
   else if (not tReminder:checked) and lookup("Unknown",cbYear:list-items) gt 0 
    then
     cbYear:delete("Unknown").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

