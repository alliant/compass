&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : lib/brw-startSearch.i
    Author      : John Oliver
    Date        : 02/19/2016
    
    Purpose     : Event code for browse 'start-search' event for 
                  column sorting. Use in multi browse window
    
    Usage       : Place this include file in the 'start-search' event
                  for the browse control.
                  Requires lib/std-def.i, lib/brw-main-multi.i, 
                  and lib/brw-sortData-mulit.i
------------------------------------------------------------------------*/

&IF DEFINED(sortClause) = 0
&THEN
  &SCOPED-DEFINE sortClause ""
&ENDIF

&IF DEFINED(whereClause) = 0
&THEN
  &SCOPED-DEFINE whereClause ""
&ENDIF

&IF DEFINED(browseName) = 0
&THEN
  &SCOPED-DEFINE browseName {&browse-name}
&ENDIF

do:
  assign
    cSearchWhereClause = {&whereClause}
    cSearchSortClause = {&sortClause}
    std-ha = browse {&browseName}:handle
    hSortColumn = browse {&browseName}:current-column
    .
  if valid-handle(hSortColumn)
   then 
    assign
      hQueryHandle = std-ha:query
      hSearchBuf = hQueryHandle:get-buffer-handle(1)
      std-ch = hSearchBuf:table + "." + hSortColumn:name
      .
   else std-ch = "".

  run sortData(std-ha, std-ch, cSearchWhereClause, cSearchSortClause).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 3.1
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


