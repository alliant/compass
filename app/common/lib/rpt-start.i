&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : lib/rpt-start.i
    Purpose     : standard RePorT START procedure (startReport)
    Author(s)   : D.Sinclair
    Created     : 9.12.2011
    Notes       :
    Parameters  : &logo=filename (with a relative path; default="images\alliant.jpg")
    Modified    :
                 Date        Name       Description
                 11/07/2023   S Chandu   Task #:107834 Updated default logo="images\ANlogoblueNew"
  ----------------------------------------------------------------------*/

&IF DEFINED(logo)=0 &THEN
&SCOPED-DEFINE logo images\alliant.jpg
&ENDIF

procedure startReport:
 RUN pdf_new ({&rpt}, activeFilename).
 
 RUN pdf_set_Orientation ({&rpt}, {&rptOrientation}).

 run pdf_load_image ({&rpt}, "logo", search("{&logo}")).
 
 RUN pdf_set_TopMargin ({&rpt}, 70).
 RUN pdf_set_BottomMargin ({&rpt}, 40).
 pdf_PageHeader({&rpt}, this-procedure, "reportHeader").
 pdf_PageFooter({&rpt}, this-procedure, "reportFooter").
 &if defined(no-startpage) = 0 &then
 newPage().
 &endif
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


