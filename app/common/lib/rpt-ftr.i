&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : lib/rpt-ftr.i
    Purpose     : standard RePorT FooTeR procedure (reportFooter)
    Author(s)   : D.Sinclair
    Created     : 9.12.2011
    Notes       : no-datetime:  Don't display date/time in left corner
                  no-trademark: Don't display company name/rights rsvd in center
                  no-pages:     Don't display page numbers in right corner
  ----------------------------------------------------------------------*/
  def var tTotalPages as char.

  tTotalPages = dynamic-function("pdf_TotalPages", {&rpt}).

  run pdf_text_color ({&rpt}, 0.0, 0.0, 0.0).
  run pdf_set_font ({&rpt}, "Courier", 7.0).

  &IF defined(no-datetime) = 0 &THEN
    run pdf_text_xy ({&rpt}, string(now, "99/99/9999 HH:MM:SS"), 10, 20).
  &ENDIF
  
  &IF defined(no-trademark) = 0 &THEN
    run pdf_text_xy ({&rpt}, 
                     chr(169) + string(year(today), "9999") + 
                     " Alliant National Title Insurance Company, Inc.  All Rights Reserved. ", 
                     if rptOrientation = "Landscape" then 240 else 160, 
                     20).
  &ENDIF
  
  &IF defined(no-pages) = 0 &THEN
    run pdf_text_xy ({&rpt}, string(iPage, "zz9") + " of " + tTotalPages, 
                     if rptOrientation = "Landscape" then 740 else 560, 
                     20).
  &ENDIF

  setFont(activeFont, activeSize). /* reset */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


