&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : validEmailAddr.i
    Purpose     : To validate an email address entered by the user

    Syntax      :

    Description :

    Author(s)   : B.Johnson
    Created     : 04/09/2015
    Modifications:
    Date        Name      Comments
    06/06/24    Sachin C  Task# 113416 Removed leading and trailing white spaces while validating emailID
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validEmailAddr Include 
FUNCTION validEmailAddr RETURNS LOGICAL
  ( input pEmailAddr as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validEmailAddr Include 
FUNCTION validEmailAddr RETURNS LOGICAL
  ( input pEmailAddr as char ) :

  def var tBadEmailChars as char init "/?<>\:*|~"~~~'~`!#$%^&~{~}[];," no-undo.
  def var i as int no-undo.
  def var tEmailOK as log no-undo init yes.
  def var tEmail as character no-undo.
  def var loop as integer no-undo.
  
                                                                   
   do loop = 1 to num-entries(pEmailAddr, ";"):
     if not tEmailOK
      then next.
     tEmail = trim(entry(loop, pEmailAddr, ";")).

     do i = 1 to length(tEmail):
      if index(tBadEmailChars, substring(tEmail,i,1)) > 0 then
    do:
      tEmailOK = no.
      leave.
    end.
  end.
  
  if tEmailOK then
   if num-entries(tEmail, "@") <> 2 then
    tEmailOK = no. /*eamil address must contain '@'*/
     
  if tEmailOK then
   if num-entries(tEmail, ".") < 2 then
    tEmailOK = no.

  if tEmailOK then
   if num-entries(tEmail, " ") > 1 then
    tEmailOK = no.

  if tEmailOK then
     do i = 1 to num-entries(tEmail, "@"):
       if entry(i, tEmail, "@") = "" or entry(i, tEmail, "@") = "?" then
    do:
      tEmailOK = no.
      leave.
    end.
  end.
  
  if tEmailOK then
  do i = 1 to num-entries(pEmailAddr, "."):
    if entry(i, pEmailAddr, ".") = "" or entry(i, pEmailAddr, ".") = "?" then
    do:
      tEmailOK = no.
      leave.
    end.
  end.
   end.
  
  RETURN tEmailOK.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

