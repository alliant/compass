&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : lib/rpt-hdr.i
    Purpose     : standard RePorT HeaDeR procedure (reportHeader)
    Author(s)   : D.Sinclair
    Created     : 9.12.2011
    Notes       : title:    Contains textual title
                  title2:   Contains 2nd title line (optional)
                  title-x:  X position in points
                  title-y:  Y position in points
                  no-logo:  Non-blank to not display default logo                
    Modified    :
                 Date        Name       Description
                 11/07/2023   S Chandu   Task #:107834 Updated to new logo in pdf.
   
  ----------------------------------------------------------------------*/
  
 &if defined(titleSize) = 0 &then
 &scoped-define titleSize 11.0
 &endif
 
 def var rgbRed as decimal.
 def var rgbGreen as decimal.
 def var rgbBlue as decimal.

 /* Grey */
 rgbRed = dynamic-function("getPDFcolor", 211).
 rgbGreen = rgbRed.
 rgbBlue = rgbRed.

 &IF defined(no-logo) = 0 
 &THEN
 /*run pdf_place_image ({&rpt}, "logo", 50, 65, 180, 48).*/
 /*run pdf_place_image ({&rpt}, "logo", 1, 76, 250, 56).*/
  run pdf_place_image ({&rpt}, "logo", 15, 76, 159, 56). 

 &ENDIF
 
 run pdf_text_color ({&rpt}, 0.0, 0.0, 0.0).
 run pdf_set_font ({&rpt}, "Helvetica-Bold", {&titleSize}).
 run pdf_text_xy ({&rpt}, {&title}, {&title-x}, {&title-y}).
 &IF defined(title2) <> 0 
  &THEN
 run pdf_set_font ({&rpt}, "Helvetica", 11.0).
 run pdf_text_xy ({&rpt}, {&title2}, {&title-x}, {&title-y} - 16).
 &ENDIF
 setFont(activeFont, activeSize). /* reset */
 iPage = iPage + 1.
 iLine = 1.
 newLine(1).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


