&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file group-column-value.i
@description Group the column values
@author John Oliver
------------------------------------------------------------------------*/

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

do:
  create buffer hDataBuffer for table cDataTable.
  create buffer hShowBuffer for table cShowTable.

  /* create a query to see how many rows there are */
  create query hShowQuery.
  hShowQuery:add-buffer(hShowBuffer).
  hShowQuery:query-prepare("for each " + cShowTable + " where " + cShowLevelColumn + " = 1 by " + cLowestDetailID).
  hShowQuery:query-open().
  hShowQuery:get-first().

  if {&whereClause} = ""
   then cWhereClause = "true = true".
   else cWhereClause = substring({&whereClause}, 6).

  if cShowGroupField > "" and cLowestDetailID <> "{&dataField}"
   then
    repeat while not hShowQuery:query-off-end:
      do std-in = 1 to num-entries(cShowGroupField):
        cGroupColumn = entry(std-in, cShowGroupField).
        create query hDataQuery.
        hDataQuery:add-buffer(hDataBuffer).
        hDataQuery:query-prepare("for each " + cDataTable + " where {&dataField} = '" + escapeQuote(hShowBuffer:buffer-field(cShowIDColumn):buffer-value()) + "' and " + cWhereClause).
        hDataQuery:query-open().
        hDataQuery:get-first().
        assign
          iDataCounter = 0
          cDataValue   = ""
          .
        repeat while not hDataQuery:query-off-end:
          iDataCounter = iDataCounter + 1.
          if lookup(hDataBuffer:buffer-field(cGroupColumn):buffer-value(), cDataValue) = 0
           then cDataValue = addDelimiter(cDataValue,",") + hDataBuffer:buffer-field(cGroupColumn):buffer-value().
          hDataQuery:get-next().
        end.
        hDataQuery:query-close().
        delete object hDataQuery no-error.
        /* if more than 1, then put multiple */
        if num-entries(cDataValue) > 1
         then cDataValue = "Multiple".
        hShowBuffer:buffer-field(cGroupColumn):buffer-value() = cDataValue.
      end.
      hShowQuery:get-next().
    end.
  hShowQuery:query-close().
  delete object hShowQuery no-error.
  /* sort the browse */
  &if defined(noSort) = 0 &then
  run value(cSortProcedure) in this-procedure ("").
  &endif
end.
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


