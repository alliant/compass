&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file set-buttons.i
@description Functions for maintaining the buttons

@author John Oliver
@created 9/17/2018
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{lib/find-widget.i}

/* table for the various buttons */
define temp-table setButton
  field setButtonHandle as handle
  field setButtonLabel  as character
  field setButtonImage  as character
  .
  
/* variable to hold the prefix */
define variable tSetButtonPrefix as character no-undo.

/* constant to hold the buttons to enable/disable */
/* generally, the list contains buttons if data is available */
&scoped-define toggleList "Export,Print,Docs,Save,Modify,Delete,Copy,Filter,FilterClear"

&if defined(exclude) = 0 &then
&scoped-define exclude ""
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD enableButtons Include 
FUNCTION enableButtons RETURNS LOGICAL
  ( input pEnable as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* the Refresh button */
create setButton.
setButton.setButtonLabel = "Refresh".
setButton.setButtonImage = "sync".
release setButton.

/* the Go button */
create setButton.
setButton.setButtonLabel = "Go".
setButton.setButtonImage = "completed".
release setButton.

/* the Export button */
create setButton.
setButton.setButtonLabel = "Export".
setButton.setButtonImage = "excel".
release setButton.

/* the Print button */
create setButton.
setButton.setButtonLabel = "Print".
setButton.setButtonImage = "pdf".
release setButton.

/* the Close button */
create setButton.
setButton.setButtonLabel = "Close".
setButton.setButtonImage = "close".
release setButton.

/* the Clear button */
create setButton.
setButton.setButtonLabel = "Clear".
setButton.setButtonImage = "erase".
release setButton.

/* the Filter button */
create setButton.
setButton.setButtonLabel = "Filter".
setButton.setButtonImage = "filter".
release setButton.

/* the Clear Filter button */
create setButton.
setButton.setButtonLabel = "FilterClear".
setButton.setButtonImage = "erase".
release setButton.

/* the Configuration button */
create setButton.
setButton.setButtonLabel = "Config".
setButton.setButtonImage = "gear".
release setButton.

/* the New button */
create setButton.
setButton.setButtonLabel = "New".
setButton.setButtonImage = "new".
release setButton.

/* the Modify button */
create setButton.
setButton.setButtonLabel = "Modify".
setButton.setButtonImage = "update".
release setButton.

/* the Delete button */
create setButton.
setButton.setButtonLabel = "Delete".
setButton.setButtonImage = "delete".
release setButton.

/* the Cancel button */
create setButton.
setButton.setButtonLabel = "Cancel".
setButton.setButtonImage = "cancel".
release setButton.

/* the Configuration button */
create setButton.
setButton.setButtonLabel = "Config".
setButton.setButtonImage = "gear".
release setButton.

/* the Copy button */
create setButton.
setButton.setButtonLabel = "Copy".
setButton.setButtonImage = "copy".
release setButton.

/* the Notes button */
create setButton.
setButton.setButtonLabel = "Notes".
setButton.setButtonImage = "note".
release setButton.

/* the New Note button */
create setButton.
setButton.setButtonLabel = "NewNote".
setButton.setButtonImage = "blank-add".
release setButton.

/* the Document button */
create setButton.
setButton.setButtonLabel = "Docs".
setButton.setButtonImage = "docs".
release setButton.

/* the Save button */
create setButton.
setButton.setButtonLabel = "Save".
setButton.setButtonImage = "save".
release setButton.

/* the Search button */
create setButton.
setButton.setButtonLabel = "Search".
setButton.setButtonImage = "magnifier".
release setButton.

/* loop through the temp table to see if the button exists */
for each setButton exclusive-lock:
  /* only if it's not excluded */
  if index({&exclude}, setButton.setButtonLabel) = 0
   then
    do:
      /* find the right widget */
      setButton.setButtonHandle = GetWidgetByName(frame {&frame-name}:handle, "b" + setButton.setButtonLabel).
      if valid-handle(setButton.setButtonHandle)
       then
        do:
          /* if valid, get the prefix */
          if setButton.setButtonHandle:width-chars = 7.2
           then tSetButtonPrefix = "".
           else tSetButtonPrefix = "s-".
          /* load the image on the button */
          setButton.setButtonHandle:load-image("images/" + tSetButtonPrefix + setButton.setButtonImage + ".bmp").
          setButton.setButtonHandle:load-image-insensitive("images/" + tSetButtonPrefix + setButton.setButtonImage + "-i.bmp").
        end.
       else delete setButton.
    end.
end.

enableButtons(false).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION enableButtons Include 
FUNCTION enableButtons RETURNS LOGICAL
  ( input pEnable as logical ) :
/*------------------------------------------------------------------------------
@description Enable/disable the buttons
------------------------------------------------------------------------------*/
  for each setButton no-lock:
    /* enable/disable only certian buttons */
    if valid-handle(setButton.setButtonHandle) and lookup(setButton.setButtonLabel, {&toggleList}) > 0
     then setButton.setButtonHandle:sensitive = pEnable.
  end.

  RETURN pEnable.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

