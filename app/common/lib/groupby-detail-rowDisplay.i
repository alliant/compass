&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file groupby-detail.i
@description Gets the detail rows for the grouped row
@author John Oliver
------------------------------------------------------------------------*/

/* Local Variable Definitions ---                                       */
{lib/std-def.i}

&if defined(showTable) = 0 &then
&scoped-define showTable showdata
&endif

&if defined(showIDColumn) = 0 &then
&scoped-define showIDColumn showID
&endif

&if defined(showLevelColumn) = 0 &then
&scoped-define showLevelColumn showLevel
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

for first groupdetail no-lock
    where groupdetail.id = {&showTable}.{&showIDColumn}:
    
  std-ch = (if groupdetail.isOpen then "-" else "+").
  if groupdetail.detailGroup <> cLowestDetailID and {&showTable}.{&showLevelColumn} = 1
   then {&showTable}.{&showIDColumn}:screen-value in browse {&browse-name} = std-ch.
end.

{lib/brw-rowdisplay.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


