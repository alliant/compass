&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/questiondef.i
  ----------------------------------------------------------------------*/
&if defined(ex-toggleset) = 0 &then
def temp-table toggleset
  field seq as int
  field togglebox as widget-handle
  field haswarning as widget-handle
 index pi-q seq.
&global-define ex-toggleset true
&endif

def var n{&seq}  as widget-handle no-undo.
def var t{&seq}  as widget-handle no-undo.
/* def var t{&seq}A as widget-handle no-undo. */
def var q{&seq}  as widget-handle no-undo.
def var q{&seq}Y as widget-handle no-undo.
def var q{&seq}N as widget-handle no-undo.
def var q{&seq}A as widget-handle no-undo.
def var q{&seq}U as widget-handle no-undo.
def var b{&seq}  as widget-handle no-undo.
def var w{&seq}  as widget-handle no-undo.
def var r{&seq}  as widget-handle no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


