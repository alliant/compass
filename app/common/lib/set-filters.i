&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file set-filters.i
@description Functions for maintaining the filters

@param tableName;char;The name of the table (default is "data")
@param labelName;char;The labels for the filters (comma-delimited list)
@param columnName;char;The columns that filter the data in the temmp table

@author John Oliver
@created 10/12/2018

@notes Place this include in the MAIN-BLOCK section. Below is an example 
       of how to call the include:
       
       {lib/set-filters.i &tableName="'data'" &labelName="'Status'" &columnName="'stat'"}

       In the getData procedure, call the function below to set the 
       filters based on the returned data:
       
       setFilterCombos("ALL").
       
       In the sortData procedure, call the function below to create the query:
       
       define variable tWhereClause as char no-undo.
       tWhereClause = doFilterSort().
       {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}
       
       For the functions to work, the object names of the filters must be
       f + {&labelName} meaning that if there was a filter for Status, then
       the object name must be "fStatus". You can only have a maximum of
       ten filters
	   
@modification
    date         Name           Description
    06/10/2022   SA             Task 93485 - Modified logic in 'setFilterRegionCombo'
	                            to get regionDesc from 'GetSysCodeDesc'
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{lib/add-delimiter.i}
{lib/find-widget.i}
{lib/getstatename.i}
{lib/do-wait.i}

&if defined(entity) = 0 &then
&scoped-define entity ""
&endif

&if defined(tableName) = 0 &then
&scoped-define tableName "data"
&endif

&if defined(labelName) = 0 &then
&scoped-define labelName ""
&endif

&if defined(columnName) = 0 &then
&scoped-define columnName ""
&endif

&if defined(useSingle) = 0 &then
&scoped-define useSingle ""
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doFilterSort Include 
FUNCTION doFilterSort RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setFilterCombos Include 
FUNCTION enableFilters RETURNS LOGICAL
  ( input pEnable as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setFilterCombos Include 
FUNCTION setFilterCombos RETURNS LOGICAL
  ( INPUT pName AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

enableFilters(false).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterActionEntityCombo Include 
PROCEDURE setFilterActionEntityCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.

  define variable hBuffer as handle no-undo.
  define variable hQuery as handle no-undo.
  define variable hField as handle no-undo.
  define variable hFilter as handle no-undo.
  define variable cValue as character no-undo.
  
  hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + replace(entry(pIndex, {&labelName}), " ", "")).
  if not valid-handle(hFilter)
   then return.
  
  hFilter:list-item-pairs = "ALL" + hFilter:delimiter + "ALL".
  
  /* query the table */
  create buffer hBuffer for table {&tableName}.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + {&tableName} + " no-lock " + doFilterSort()).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hField = hBuffer:buffer-field(entry(pIndex, {&columnName})).
    if valid-handle(hField) and lookup(hField:buffer-value(), hFilter:list-item-pairs) = 0
     then hFilter:list-item-pairs = addDelimiter(hFilter:list-item-pairs, hFilter:delimiter) + hField:buffer-value() + hFilter:delimiter + hField:buffer-value().
    hQuery:get-next().
  end.
  hQuery:query-close().

  delete object hQuery no-error.
  delete object hBuffer no-error.
  delete object hField no-error.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterActionOwnerCombo Include 
PROCEDURE setFilterActionOwnerCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.

  define variable hBuffer as handle no-undo.
  define variable hQuery  as handle no-undo.
  define variable hField  as handle no-undo.
  define variable hFilter as handle no-undo.
  
  define variable cUID    as character no-undo.
  define variable cValue  as character no-undo.
  
  define variable cFilter as character no-undo.
  define variable i       as integer no-undo.
  define variable j       as integer no-undo.
  
  hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + replace(entry(pIndex, {&labelName}), " ", "")) no-error.
  if not valid-handle(hFilter)
   then return.
  
  hFilter:list-item-pairs = "ALL" + hFilter:delimiter + "ALL".
  
  /* query the table */
  create buffer hBuffer for table {&tableName}.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + {&tableName} + " no-lock " + doFilterSort() + " by ownerID").
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    cFilter = entry(pIndex, {&columnName}).
    do i = 1 to num-entries(cFilter, ":"):
      hField = hBuffer:buffer-field(entry(i, cFilter, ":")).
      if valid-handle(hField) and lookup(hField:buffer-value(), hFilter:list-item-pairs) = 0 and hField:buffer-value() > ""
       then
        do j = 1 to num-entries(hField:buffer-value()):
          cUID = entry(j, hField:buffer-value()).
          publish "GetSysUserName" (cUID, output cValue).
          if cValue = ""
           then cValue = cUID.
          hFilter:list-item-pairs = addDelimiter(hFilter:list-item-pairs, hFilter:delimiter) + cValue + hFilter:delimiter + cUID.
        end.
    end.
    hQuery:get-next().
  end.
  hQuery:query-close().

  delete object hQuery no-error.
  delete object hBuffer no-error.
  delete object hField no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterActionSeverityCombo Include 
PROCEDURE setFilterActionSeverityCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.

  define variable hBuffer as handle no-undo.
  define variable hQuery as handle no-undo.
  define variable hField as handle no-undo.
  define variable hFilter as handle no-undo.
  define variable cValue as character no-undo.
  
  hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + replace(entry(pIndex, {&labelName}), " ", "")).
  if not valid-handle(hFilter)
   then return.
  
  hFilter:list-item-pairs = "ALL" + hFilter:delimiter + "ALL".
  
  /* query the table */
  create buffer hBuffer for table {&tableName}.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + {&tableName} + " no-lock " + doFilterSort()).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hField = hBuffer:buffer-field(entry(pIndex, {&columnName})).
    if valid-handle(hField) and lookup(hField:buffer-value(), hFilter:list-item-pairs) = 0
     then
      do:
        cValue = hField:buffer-value().
        publish "GetSysPropDesc" ("CAM", "Finding", "Severity", hField:buffer-value(), output cValue).
        hFilter:list-item-pairs = addDelimiter(hFilter:list-item-pairs, hFilter:delimiter) + cValue + hFilter:delimiter + hField:buffer-value().
      end.
    hQuery:get-next().
  end.
  hQuery:query-close().

  delete object hQuery no-error.
  delete object hBuffer no-error.
  delete object hField no-error.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterActionStatusCombo Include 
PROCEDURE setFilterActionStatusCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.

  define variable hBuffer as handle no-undo.
  define variable hQuery as handle no-undo.
  define variable hField as handle no-undo.
  define variable hFilter as handle no-undo.
  define variable cValue as character no-undo.
  
  hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + replace(entry(pIndex, {&labelName}), " ", "")).
  if not valid-handle(hFilter)
   then return.
  
  hFilter:list-item-pairs = "ALL" + hFilter:delimiter + "ALL".
  
  /* query the table */
  create buffer hBuffer for table {&tableName}.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + {&tableName} + " no-lock " + doFilterSort()).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hField = hBuffer:buffer-field(entry(pIndex, {&columnName})).
    if valid-handle(hField) and lookup(hField:buffer-value(), hFilter:list-item-pairs) = 0
     then
      do:
        cValue = hField:buffer-value().
        publish "GetSysPropDesc" ("CAM", "Action", "Status", hField:buffer-value(), output cValue).
        hFilter:list-item-pairs = addDelimiter(hFilter:list-item-pairs, hFilter:delimiter) + cValue + hFilter:delimiter + hField:buffer-value().
      end.
    hQuery:get-next().
  end.
  hQuery:query-close().

  delete object hQuery no-error.
  delete object hBuffer no-error.
  delete object hField no-error.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterAgentAppManagerCombo Include 
PROCEDURE setFilterAgentAppManagerCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.

  run setFilterManagerCombo in this-procedure (pIndex).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterAgentAppRegionCombo Include 
PROCEDURE setFilterAgentAppRegionCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.

  run setFilterRegionCombo in this-procedure (pIndex).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterAgentAppStateCombo Include 
PROCEDURE setFilterAgentAppStateCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.

  run setFilterStateCombo in this-procedure (pIndex).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterAgentAppStatusCombo Include 
PROCEDURE setFilterAgentAppStatusCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.

  define variable hBuffer as handle no-undo.
  define variable hQuery as handle no-undo.
  define variable hField as handle no-undo.
  define variable hFilter as handle no-undo.
  define variable cValue as character no-undo.
  
  hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + replace(entry(pIndex, {&labelName}), " ", "")).
  if not valid-handle(hFilter)
   then return.
  
  hFilter:list-item-pairs = "ALL" + hFilter:delimiter + "ALL".
  
  /* query the table */
  create buffer hBuffer for table {&tableName}.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + {&tableName} + " no-lock " + doFilterSort()).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hField = hBuffer:buffer-field(entry(pIndex, {&columnName})).
    if valid-handle(hField) and lookup(hField:buffer-value(), hFilter:list-item-pairs) = 0
     then
      do:
        publish "GetSysPropDesc" ("AMD", "Application", "Status", hField:buffer-value(), output cValue).
        hFilter:list-item-pairs = addDelimiter(hFilter:list-item-pairs, hFilter:delimiter) + cValue + hFilter:delimiter + hField:buffer-value().
      end.
    hQuery:get-next().
  end.
  hQuery:query-close().

  delete object hQuery no-error.
  delete object hBuffer no-error.
  delete object hField no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterActivityCategoryCombo Include 
PROCEDURE setFilterActivityCategoryCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.

  define variable hBuffer as handle no-undo.
  define variable hQuery as handle no-undo.
  define variable hField as handle no-undo.
  define variable hFilter as handle no-undo.
  define variable cValue as character no-undo.
  
  hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + replace(entry(pIndex, {&labelName}), " ", "")).
  if not valid-handle(hFilter)
   then return.
  
  hFilter:list-item-pairs = "ALL" + hFilter:delimiter + "ALL".
  
  /* query the table */
  create buffer hBuffer for table {&tableName}.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + {&tableName} + " no-lock " + doFilterSort()).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hField = hBuffer:buffer-field(entry(pIndex, {&columnName})).
    if valid-handle(hField) and lookup(hField:buffer-value(), hFilter:list-item-pairs) = 0
     then
      do:
        cValue = hField:buffer-value().
        publish "GetSysPropDesc" ("AMD", "Activity", "Category", hField:buffer-value(), output cValue).
        hFilter:list-item-pairs = addDelimiter(hFilter:list-item-pairs, hFilter:delimiter) + cValue + hFilter:delimiter + hField:buffer-value().
      end.
    hQuery:get-next().
  end.
  hQuery:query-close().

  delete object hQuery no-error.
  delete object hBuffer no-error.
  delete object hField no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterActivityTypeCombo Include 
PROCEDURE setFilterActivityTypeCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.

  define variable hBuffer as handle no-undo.
  define variable hQuery as handle no-undo.
  define variable hField as handle no-undo.
  define variable hFilter as handle no-undo.
  define variable cValue as character no-undo.
  
  hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + replace(entry(pIndex, {&labelName}), " ", "")).
  if not valid-handle(hFilter)
   then return.
  
  hFilter:list-item-pairs = "ALL" + hFilter:delimiter + "ALL".
  
  /* query the table */
  create buffer hBuffer for table {&tableName}.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + {&tableName} + " no-lock " + doFilterSort()).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hField = hBuffer:buffer-field(entry(pIndex, {&columnName})).
    if valid-handle(hField) and lookup(hField:buffer-value(), hFilter:list-item-pairs) = 0
     then
      do:
        cValue = hField:buffer-value().
        publish "GetSysPropDesc" ("AMD", "Activity", "Type", hField:buffer-value(), output cValue).
        hFilter:list-item-pairs = addDelimiter(hFilter:list-item-pairs, hFilter:delimiter) + cValue + hFilter:delimiter + hField:buffer-value().
      end.
    hQuery:get-next().
  end.
  hQuery:query-close().

  delete object hQuery no-error.
  delete object hBuffer no-error.
  delete object hField no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterAdminCombo Include 
PROCEDURE setFilterAdminCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.

  define variable hBuffer as handle no-undo.
  define variable hQuery as handle no-undo.
  define variable hField as handle no-undo.
  define variable hFilter as handle no-undo.
  define variable cValue as character no-undo.
  
  hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + replace(entry(pIndex, {&labelName}), " ", "")).
  if not valid-handle(hFilter)
   then return.
  
  hFilter:list-item-pairs = "ALL" + hFilter:delimiter + "ALL".
  
  /* query the table */
  create buffer hBuffer for table {&tableName}.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + {&tableName} + " no-lock " + doFilterSort()).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hField = hBuffer:buffer-field(entry(pIndex, {&columnName})).
    if valid-handle(hField) and lookup(hField:buffer-value(), hFilter:list-item-pairs) = 0
     then
      do:
        cValue = hField:buffer-value().
        publish "GetSysUserName" (hField:buffer-value(), output cValue).
        hFilter:list-item-pairs = addDelimiter(hFilter:list-item-pairs, hFilter:delimiter) + cValue + hFilter:delimiter + hField:buffer-value().
      end.
    hQuery:get-next().
  end.
  hQuery:query-close().

  delete object hQuery no-error.
  delete object hBuffer no-error.
  delete object hField no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterAlertCodeCombo Include 
PROCEDURE setFilterAlertCodeCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.

  define variable hBuffer as handle no-undo.
  define variable hQuery as handle no-undo.
  define variable hField as handle no-undo.
  define variable hFilter as handle no-undo.
  define variable cValue as character no-undo.
  
  hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + replace(entry(pIndex, {&labelName}), " ", "")).
  if not valid-handle(hFilter)
   then return.
  
  hFilter:list-item-pairs = "ALL" + hFilter:delimiter + "ALL".
  
  /* query the table */
  create buffer hBuffer for table {&tableName}.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + {&tableName} + " no-lock " + doFilterSort()).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hField = hBuffer:buffer-field(entry(pIndex, {&columnName})).
    if valid-handle(hField) and lookup(hField:buffer-value(), hFilter:list-item-pairs) = 0
     then
      do:
        cValue = hField:buffer-value().
        publish "GetSysCodeDesc" ("Alert", hField:buffer-value(), output cValue).
        hFilter:list-item-pairs = addDelimiter(hFilter:list-item-pairs, hFilter:delimiter) + cValue + hFilter:delimiter + hField:buffer-value().
      end.
    hQuery:get-next().
  end.
  hQuery:query-close().

  delete object hQuery no-error.
  delete object hBuffer no-error.
  delete object hField no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterAlertCombo Include 
PROCEDURE setFilterAlertCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.

  define variable hBuffer as handle no-undo.
  define variable hQuery as handle no-undo.
  define variable hField as handle no-undo.
  define variable hFilter as handle no-undo.
  define variable cValue as character no-undo.
  
  hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + replace(entry(pIndex, {&labelName}), " ", "")).
  if not valid-handle(hFilter)
   then return.
  
  hFilter:list-item-pairs = "ALL" + hFilter:delimiter + "ALL".
  
  /* query the table */
  create buffer hBuffer for table {&tableName}.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + {&tableName} + " no-lock " + doFilterSort()).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hField = hBuffer:buffer-field(entry(pIndex, {&columnName})).
    if valid-handle(hField) and lookup(hField:buffer-value(), hFilter:list-item-pairs) = 0
     then
      do:
        cValue = hField:buffer-value().
        publish "GetSysPropDesc" ("AMD", "Alert", "Severity", hField:buffer-value(), output cValue).
        hFilter:list-item-pairs = addDelimiter(hFilter:list-item-pairs, hFilter:delimiter) + cValue + hFilter:delimiter + hField:buffer-value().
      end.
    hQuery:get-next().
  end.
  hQuery:query-close().

  delete object hQuery no-error.
  delete object hBuffer no-error.
  delete object hField no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterAlertOwnerCombo Include 
PROCEDURE setFilterAlertOwnerCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.

  define variable hBuffer as handle no-undo.
  define variable hQuery as handle no-undo.
  define variable hField as handle no-undo.
  define variable hFilter as handle no-undo.
  define variable cValue as character no-undo.
  
  hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + replace(entry(pIndex, {&labelName}), " ", "")).
  if not valid-handle(hFilter)
   then return.
  
  hFilter:list-item-pairs = "ALL" + hFilter:delimiter + "ALL".
  
  /* query the table */
  create buffer hBuffer for table {&tableName}.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + {&tableName} + " no-lock " + doFilterSort()).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hField = hBuffer:buffer-field(entry(pIndex, {&columnName})).
    if valid-handle(hField) and lookup(hField:buffer-value(), hFilter:list-item-pairs) = 0
     then
      do:
        cValue = hField:buffer-value().
        publish "GetSysPropDesc" ("AMD", "Alert", "Owner", hField:buffer-value(), output cValue).
        hFilter:list-item-pairs = addDelimiter(hFilter:list-item-pairs, hFilter:delimiter) + cValue + hFilter:delimiter + hField:buffer-value().
      end.
    hQuery:get-next().
  end.
  hQuery:query-close().

  delete object hQuery no-error.
  delete object hBuffer no-error.
  delete object hField no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterManagerCombo Include 
PROCEDURE setFilterManagerCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.

  define variable hBuffer as handle no-undo.
  define variable hQuery  as handle no-undo.
  define variable hField  as handle no-undo.
  define variable hFilter as handle no-undo.
  
  define variable cUID    as character no-undo.
  define variable cValue  as character no-undo.
  
  define variable cFilter as character no-undo.
  define variable i       as integer no-undo.
  define variable j       as integer no-undo.
  
  hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + replace(entry(pIndex, {&labelName}), " ", "")) no-error.
  if not valid-handle(hFilter)
   then return.
  
  hFilter:list-item-pairs = "ALL" + hFilter:delimiter + "ALL".
  
  /* query the table */
  create buffer hBuffer for table {&tableName}.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + {&tableName} + " no-lock " + doFilterSort() + " by manager").
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    cFilter = entry(pIndex, {&columnName}).
    do i = 1 to num-entries(cFilter, ":"):
      hField = hBuffer:buffer-field(entry(i, cFilter, ":")).
      if valid-handle(hField) and lookup(hField:buffer-value(), hFilter:list-item-pairs) = 0 and hField:buffer-value() > ""
       then
        do j = 1 to num-entries(hField:buffer-value()):
          cUID = entry(j, hField:buffer-value()).
          publish "GetSysUserName" (cUID, output cValue).
          if cValue = ""
           then cValue = cUID.
          hFilter:list-item-pairs = addDelimiter(hFilter:list-item-pairs, hFilter:delimiter) + cValue + hFilter:delimiter + cUID.
        end.
    end.
    hQuery:get-next().
  end.
  hQuery:query-close().

  delete object hQuery no-error.
  delete object hBuffer no-error.
  delete object hField no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterRegionCombo Include 
PROCEDURE setFilterRegionCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.

  define variable hBuffer as handle no-undo.
  define variable hQuery as handle no-undo.
  define variable hField as handle no-undo.
  define variable hFilter as handle no-undo.
  define variable cValue as character no-undo.
  
  hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + replace(entry(pIndex, {&labelName}), " ", "")).
  if not valid-handle(hFilter)
   then return.
  
  hFilter:list-item-pairs = "ALL" + hFilter:delimiter + "ALL".
  
  /* query the table */
  create buffer hBuffer for table {&tableName}.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + {&tableName} + " no-lock " + doFilterSort()).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hField = hBuffer:buffer-field(entry(pIndex, {&columnName})).
    if valid-handle(hField) and lookup(hField:buffer-value(), hFilter:list-item-pairs) = 0
     then
      do:
        cValue = hField:buffer-value().
        publish "GetSysCodeDesc" ("Region", hField:buffer-value(), output cValue).
        hFilter:list-item-pairs = addDelimiter(hFilter:list-item-pairs, hFilter:delimiter) + cValue + hFilter:delimiter + hField:buffer-value().
      end.
    hQuery:get-next().
  end.
  hQuery:query-close().

  delete object hQuery no-error.
  delete object hBuffer no-error.
  delete object hField no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterStateCombo Include 
PROCEDURE setFilterStateCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.

  define variable hBuffer as handle no-undo.
  define variable hQuery  as handle no-undo.
  define variable hField  as handle no-undo.
  define variable hFilter as handle no-undo.
  define variable cLabel  as character no-undo.
  define variable cValue  as character no-undo.
  
  cLabel = replace(entry(pIndex, {&labelName}), " ", "").
  hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + cLabel).
  if not valid-handle(hFilter)
   then return.

  if lookup(cLabel, {&useSingle}) > 0
   then hFilter:list-items = "ALL".
   else hFilter:list-item-pairs = "ALL" + hFilter:delimiter + "ALL".
  
  /* query the table */
  create buffer hBuffer for table {&tableName}.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + {&tableName} + " no-lock " + doFilterSort()).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hField = hBuffer:buffer-field(entry(pIndex, {&columnName})).
    if lookup(cLabel, {&useSingle}) > 0
     then
      do:
        if valid-handle(hField) and lookup(hField:buffer-value(), hFilter:list-items) = 0
         then hFilter:list-items = addDelimiter(hFilter:list-items, hFilter:delimiter) + hField:buffer-value().
      end.
     else
      do:
        cValue = getStateName(hField:buffer-value()).
        if valid-handle(hField) and lookup(hField:buffer-value(), hFilter:list-item-pairs) = 0
         then hFilter:list-item-pairs = addDelimiter(hFilter:list-item-pairs, hFilter:delimiter) + cValue + hFilter:delimiter + hField:buffer-value().
      end.
    hQuery:get-next().
  end.
  hQuery:query-close().

  delete object hQuery no-error.
  delete object hBuffer no-error.
  delete object hField no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterStatusCombo Include 
PROCEDURE setFilterStatusCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pIndex as integer no-undo.
  
  define variable hBuffer as handle no-undo.
  define variable hQuery as handle no-undo.
  define variable hField as handle no-undo.
  define variable hFilter as handle no-undo.
  define variable cValue as character no-undo.
  
  hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + replace(entry(pIndex, {&labelName}), " ", "")).
  if not valid-handle(hFilter)
   then return.
  
  hFilter:list-item-pairs = "ALL" + hFilter:delimiter + "ALL".
  
  /* query the table */
  create buffer hBuffer for table {&tableName}.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + {&tableName} + " no-lock " + doFilterSort()).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hField = hBuffer:buffer-field(entry(pIndex, {&columnName})).
    if valid-handle(hField) and lookup(hField:buffer-value(), hFilter:list-item-pairs) = 0
     then
      do:
        cValue = hField:buffer-value().
        publish "GetSysPropDesc" ("AMD", "Agent", "Status", hField:buffer-value(), output cValue).
        hFilter:list-item-pairs = addDelimiter(hFilter:list-item-pairs, hFilter:delimiter) + cValue + hFilter:delimiter + hField:buffer-value().
      end.
    hQuery:get-next().
  end.
  hQuery:query-close().

  delete object hQuery no-error.
  delete object hBuffer no-error.
  delete object hField no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&if defined(noSort) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doFilterSort Include 
FUNCTION doFilterSort RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo initial "".
  define variable tInnerClause as character no-undo.
  
  define variable cColumn as character no-undo.
  define variable hFilter as handle no-undo.
  define variable i as integer no-undo.
  define variable j as integer no-undo.
  /* set the values to what they were */
  do i = 1 to num-entries({&labelName}):
    hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + replace(entry(i, {&labelName}), " ", "")).
    if valid-handle(hFilter) and hFilter:screen-value <> "ALL" and hFilter:screen-value <> ?
     then
      do:
        cColumn = entry(i, {&columnName}).
        if num-entries(cColumn, ":") > 1
         then
          do:
            do j = 1 to num-entries(cColumn, ":"):
              tInnerClause = addDelimiter(tInnerClause," or ") + entry(j, cColumn, ":") + " matches '*" + hFilter:screen-value + "*'".
            end.
            if tInnerClause > ""
             then tWhereClause = addDelimiter(tWhereClause, " and ") + "(" + tInnerClause + ")".
          end.
         else 
          case hFilter:data-type:
           when "INTEGER" or
           when "DECIMAL" then tWhereClause = addDelimiter(tWhereClause, " and ") + cColumn + " = " + hFilter:screen-value.
           otherwise tWhereClause = addDelimiter(tWhereClause, " and ") + cColumn + " = '" + hFilter:screen-value + "'".
          end case.
      end.
  end.
  if tWhereClause > ""
   then tWhereClause = "where " + tWhereClause.
   
  RETURN tWhereClause.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION enableFilters Include 
FUNCTION enableFilters RETURNS LOGICAL
  ( input pEnable as logical ) :
/*------------------------------------------------------------------------------
@description Enable/disable the filters
------------------------------------------------------------------------------*/
  define variable cFullList as character     no-undo.
  define variable hWidget   as widget-handle no-undo.
  define variable i         as integer       no-undo.
  
  cFullList = {&labelName}.
  &if defined(include) <> 0 &then
  cFullList = cFullList + "," + {&include}.
  &endif
  
  /* all other filters */
  do i = 1 to num-entries(cFullList):
    hWidget = GetWidgetByName(frame {&frame-name}:handle, "f" + replace(entry(i, cFullList), " ", "")).
    if valid-handle(hWidget)
     then
      case hWidget:type:
       when "BUTTON" then hWidget:sensitive = pEnable.
       when "FILL-IN" then hWidget:read-only = not pEnable.
       when "SELECTION-LIST" then hWidget:sensitive = pEnable.
       when "COMBO-BOX" then hWidget:sensitive = pEnable.
       when "TOGGLE-BOX" then hWidget:sensitive = pEnable.
       when "EDITOR" then hWidget:read-only = not pEnable.
      end case.
  end.

  RETURN pEnable.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setFilterCombos Include 
FUNCTION setFilterCombos RETURNS LOGICAL
  ( INPUT pName AS CHARACTER ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable i as integer no-undo.
  define variable cLabel as character no-undo.
  define variable cFilter as character no-undo.
  define variable hFilter as handle no-undo.
  define variable cParams as character no-undo extent 10.
  
  /* initialize array */
  do i = 1 to 10:
    cParams[i] = "".
  end.

  doWait(true).
  if {&labelName} > "" and {&columnName} > ""
   then
    do:
      /* get the filter values as they are currently */
      do i = 1 to num-entries({&labelName}):
        cLabel = entry(i, {&labelName}).
        cFilter = replace(cLabel, " ", "").
        hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + cFilter).
        if valid-handle(hFilter) and hFilter:type <> "FILL-IN"
         then cParams[i] = hFilter:screen-value.
      end.
      /* call the correct procedure to set the data */
      do i = 1 to num-entries({&labelName}):
        cLabel = entry(i, {&labelName}).
        cFilter = replace(cLabel, " ", "").
        hFilter = GetWidgetByName(frame {&frame-name}:handle,"f" + cFilter).
        if valid-handle(hFilter) and (hFilter:type = "COMBO-BOX" or hFilter:type = "SELECTION-LIST")
         then
          do:
            if pName = "ALL" or (pName <> "ALL" and pName <> cLabel)
             then
              do:
                run value("setFilter" + {&entity} + cFilter + "Combo") in this-procedure (i).
                if lookup(cParams[i], if lookup(cLabel, {&useSingle}) > 0 then hFilter:list-items else hFilter:list-item-pairs) = 0
                 then hFilter:screen-value = "ALL".
                 else hFilter:screen-value = cParams[i].
                if lookup("Multiple", if lookup(cLabel, {&useSingle}) > 0 then hFilter:list-items else hFilter:list-item-pairs) > 0
                 then hFilter:sensitive = false.
                 else hFilter:sensitive = true.
              end.
          end.
      end.
    end.
  doWait(false).
  
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

