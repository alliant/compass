&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/configsf.i
   contains routines related to the Document service
   
   Based on Citrix Document API documentation
   http://api.sharefile.com/https.aspx
    
   D.Sinclair
   4.4.2013
   
   8.21.2015 jpo : Changed the GetSfUsername to get the email address
   9.12.2019 jpo : Modified to use v3 Document API
   
   https://api.sharefile.com/rest/
 */

/* These vars are to maintain session credentials during a single
   running instance of an application.  They are not stored to the
   config file */

define variable tRepositoryToken     as character no-undo.
define variable tRepositoryStartTime as datetime  no-undo.
{tt/ini.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 20.19
         WIDTH              = 67.4.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* get/set authorization tokens */
subscribe to "GetRepositoryToken"         anywhere.
subscribe to "SetRepositoryToken"         anywhere.
                                          
/* Document interface URLs */             
subscribe to "GetARCRepositoryURL"        anywhere.
subscribe to "GetRepositoryURL"           anywhere.
                                          
/* Field data */                          
subscribe to "GetRepositoryClientID"      anywhere.
subscribe to "GetRepositorySecret"        anywhere.
subscribe to "GetRepositoryRoot"          anywhere.

/* load the data */
subscribe to "LoadARCRepositoryEndpoints" anywhere.
subscribe to "LoadRepositoryFields"       anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetARCRepositoryURL Include 
PROCEDURE GetARCRepositoryURL :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pID  as character no-undo.
  define output parameter pVal as character no-undo.
  
  /* check if sharefile info available */
  if not can-find(first ini where ini.id matches "*" + pID)
   then run LoadARCRepositoryEndpoints.
   
  for first ini no-lock
      where ini.id matches "*" + pID:
      
    pVal = ini.val.
  end.
  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetRepositoryClientID Include 
PROCEDURE GetRepositoryClientID :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter pVal as character no-undo.
  
  /* check if sharefile info available */
  if not can-find(first ini where id = "RepositoryClientID")
   then run LoadRepositoryFields.
   
  for first ini no-lock
      where ini.id = "RepositoryClientID":
      
    pVal = ini.val.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetRepositoryInterval Include 
PROCEDURE GetRepositoryInterval :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter pVal as integer no-undo initial 0.
  
  /* check if sharefile info available */
  if not can-find(first ini where id = "RepositoryInterval")
   then run LoadRepositoryFields.
   
  for first ini no-lock
      where ini.id = "RepositoryInterval":
      
    pVal = integer(ini.val) no-error.
    if error-status:error
     then pVal = 0.
  end.
  
  /* if the id isn't found, then the default is 25,200 seconds (7 hours) */
  if pVal = 0
   then pVal = 25200.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetRepositoryRoot Include 
PROCEDURE GetRepositoryRoot :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter pVal as character no-undo.
  
  /* check if sharefile info available */
  if not can-find(first ini where id = "RepositoryRootFolder")
   then run LoadRepositoryFields.
   
  for first ini no-lock
      where ini.id = "RepositoryRootFolder":
      
    pVal = ini.val.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetRepositorySecret Include 
PROCEDURE GetRepositorySecret :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter pVal as character no-undo.
  
  /* check if sharefile info available */
  if not can-find(first ini where id = "RepositorySecret")
   then run LoadRepositoryFields.
   
  for first ini no-lock
      where ini.id = "RepositorySecret":
      
    pVal = ini.val.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetRepositoryToken Include 
PROCEDURE GetRepositoryToken :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter pToken as character no-undo.
  
  define variable tInterval as integer no-undo.
  
  run GetRepositoryInterval in this-procedure (output tInterval).
  if tRepositoryToken = "" or interval(now, tRepositoryStartTime, "seconds") > tInterval
   then run SetRepositoryToken in this-procedure.
   
  pToken = tRepositoryToken.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetRepositoryURL Include 
PROCEDURE GetRepositoryURL :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pID  as character no-undo.
  define output parameter pVal as character no-undo.
  
  /* check if sharefile info available */
  if not can-find(first ini where id = pID)
   then run LoadRepositoryFields.
   
  for first ini no-lock
      where ini.id = pID:
      
    pVal = ini.val.
  end.
  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadRepositoryFields Include 
PROCEDURE LoadARCRepositoryEndpoints :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* call the server */
  empty temp-table ini.
  run server/getarcrepositoryendpoints.p (output table ini,
                                          output std-lo,
                                          output std-ch).
                                 
  /* if unsuccessful, show a message if debug is on */
  if not std-lo
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadARCRepositoryEndpoints failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadRepositoryFields Include 
PROCEDURE LoadRepositoryFields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* call the server */
  empty temp-table ini.
  run server/getrepositoryfields.p (output table ini,
                                    output std-lo,
                                    output std-ch).
                                 
  /* if unsuccessful, show a message if debug is on */
  if not std-lo
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadRepositoryFields failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetRepositoryToken Include 
PROCEDURE SetRepositoryToken :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tUserToken  as character no-undo.
  define variable tAdminToken as character no-undo.
  
  run server/getrepositorytoken.p (output tRepositoryToken,
                                   output std-lo,
                                   output std-ch).
  
  if not std-lo
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "SetRepositoryToken failed: " + std-ch view-as alert-box warning.
    end.
   else tRepositoryStartTime = now.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
