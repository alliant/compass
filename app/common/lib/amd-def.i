&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : lib/amd-def.i
Purpose     :

Syntax      :

Description :

Author(s)   : Sachin Chaturvedi
Created     : 07-14-2020
Notes       :

Modification:                              
----------------------------------------------------------------------*/
/*----------Definition of Actions -------------------*/
&global-define Agent             "Agent"
&global-define AgentCode         "A"
&global-define Organization      "Organization"
&global-define OrganizationCode  "O"
&global-define Person            "Person"
&global-define PersonCode        "P"
&global-define Attorney          "T"
&global-define Employee          "E" 
&global-define New               "N"
&global-define Modify            "M"
&global-define Copy              "C"
&global-define Active            "Active"
&global-define ActiveStat        "A"
&global-define All               "ALL"
&global-define Our               "O"
&global-define Their             "T"
&global-define InUse             "InUse"

&global-define Third-Party       "Third-Party"
&global-define ThirdParty        "T"

&global-define First-Party       "First-Party"
&global-define FirstParty        "C"

/*------------------colours----------------------*/
&global-define Gcode             "G"
&global-define Green             "Green"
&global-define Rcode             "R"
&global-define Red               "Red"
&global-define Ycode             "Y"
&global-define Yellow            "Yellow"

/*----------window status bar messages----------------*/
&global-define ResultNotMatch                "Results may not match current parameters."
&global-define ResultNotMatchSearchString    "Results may not match current search string."

/*----------Statistics Type----------------*/

&global-define Action        "Action"

&global-define NatureOfAffiliation  "NatureOfAffiliation"
&global-define OrganizationCode  "O"
&global-define PersonCode        "P"
/*------------Mandatory Field symbol----------*/
&global-define Mandatory        "*"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


