&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file groupby.i
@description Group the table by a field and sum up the fields
@author John Oliver
------------------------------------------------------------------------*/

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD escapeQoute Include 
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */
do:
  &if defined(dataTable) = 0 &then
  &scoped-define dataTable data
  &endif

  DoWait(true).
  empty temp-table groupdata.
  create buffer hDataBuffer for table "{&dataTable}".
  create buffer hGroupBuffer for table "groupdata".
  create buffer hGroupCopyBuffer for table "groupdata".
  create buffer hShowBuffer for table cShowTable.

  /* create a query to see how many rows there are */
  std-ch = "for each {&dataTable} no-lock " + {&whereClause} + " by {&firstField}".
  &if defined(secondField) <> 0 &then
  std-ch = std-ch + " by {&secondField}".
  &endif
  &if defined(thirdField) <> 0 &then
  std-ch = std-ch + " by {&thirdField}".
  &endif
  create query hGroupQuery.
  hGroupQuery:add-buffer(hDataBuffer).
  hGroupQuery:query-prepare(std-ch).
  hGroupQuery:query-open().
  hGroupQuery:get-first().
  cLastGroup = "".
  iGroupCounter = 0.
  /* get the totals for each column group */
  repeat while not hGroupQuery:query-off-end:
    if cLastGroup <> hDataBuffer:buffer-field("{&firstField}"):buffer-value()
     then iGroupCounter = iGroupCounter + 1.
    /* find if the group row is in the table already */
    std-ch = "for each groupdata no-lock where {&firstField} = '" + escapeQuote(hDataBuffer:buffer-field("{&firstField}"):buffer-value()) + "'".
    &if defined(secondField) <> 0 &then
    std-ch = std-ch + " and {&secondField} = '" + escapeQuote(hDataBuffer:buffer-field("{&secondField}"):buffer-value()) + "'".
    &endif
    &if defined(thirdField) <> 0 &then
    std-ch = std-ch = std-ch + " and {&thirdField} = '" + escapeQuote(hDataBuffer:buffer-field("{&thirdField}"):buffer-value()) + "'".
    &endif
    create query hGroupCopyQuery.
    hGroupCopyQuery:add-buffer(hGroupCopyBuffer).
    hGroupCopyQuery:query-prepare(std-ch).
    hGroupCopyQuery:query-open().
    hGroupCopyQuery:get-first().
    std-in = 0.
    repeat while not hGroupCopyQuery:query-off-end:
      std-in = std-in + 1.
      hGroupCopyQuery:get-next().
    end.
    hGroupCopyQuery:query-close().
    /* std-in of 0 means that the group isn't in the table yet */
    if std-in = 0
     then
      do:
        cLastGroup = hDataBuffer:buffer-field("{&firstField}"):buffer-value().
        hGroupBuffer:buffer-create.
        hGroupBuffer:buffer-copy(hDataBuffer).
        assign
          hGroupBuffer:buffer-field("groupID"):buffer-value()    = hDataBuffer:buffer-field("{&firstField}"):buffer-value()
          hGroupBuffer:buffer-field("groupName"):buffer-value()  = hDataBuffer:buffer-field("{&groupName}"):buffer-value()
          hGroupBuffer:buffer-field("groupLevel"):buffer-value() = 1
          hGroupBuffer:buffer-field("sortOrder"):buffer-value()  = string(iGroupCounter, "99999999")
          .
        do std-in = 1 to hGroupBuffer:num-fields:
          hShowField = hGroupBuffer:buffer-field(std-in).
          if valid-handle(hShowField) and hShowField:name = "groupLevel"
           then next.
           
          if valid-handle(hShowField) and (hShowField:data-type = "INTEGER" or hShowField:data-type = "DECIMAL")
           then hShowField:buffer-value() = 0.
        end.
      end.
    /* add up only integer and decimal fields for the group */
    do std-in = 1 to hDataBuffer:num-fields:
      hGroupField = hDataBuffer:buffer-field(std-in).
      hShowField = hGroupBuffer:buffer-field(std-in).
      if valid-handle(hGroupField) and valid-handle(hShowField) and (hGroupField:data-type = "INTEGER" or hGroupField:data-type = "DECIMAL")
       then
        do:
          if valid-handle(hGroupField) and hGroupField:name = "groupLevel"
           then next.
           
          hShowField:buffer-value() = hShowField:buffer-value() + hGroupField:buffer-value().
        end.
    end.
    /* decide if the group by has a formula */
    for each groupformula no-lock:
      case groupformula.formulaType:
       when "percent" then
        do:
          if hGroupBuffer:buffer-field(groupformula.denominator):buffer-value() = 0
           then hGroupBuffer:buffer-field(groupformula.outcome):buffer-value() = groupformula.onError.
           else hGroupBuffer:buffer-field(groupformula.outcome):buffer-value() = (hGroupBuffer:buffer-field(groupformula.numerator):buffer-value() / hGroupBuffer:buffer-field(groupformula.denominator):buffer-value()) * 100.
        end.
      end case.
    end.
    hGroupQuery:get-next().
  end.
  hGroupQuery:query-close().
  delete object hGroupCopyQuery no-error.
  delete object hGroupQuery no-error.
  /* load the group table */
  empty temp-table groupdetail.
  for each groupdata no-lock:
    create groupdetail.
    assign
      groupdetail.id          = groupdata.groupID
      groupdetail.isOpen      = false
      groupdetail.detailGroup = "{&firstField}"
      groupdetail.detailValue = groupdata.{&firstField}
      groupdetail.sortOrder   = groupdata.sortOrder
      .
  end.
  /* copy the data from the group table to the show table */
  hShowBuffer:empty-temp-table().
  create query hShowQuery.
  hShowQuery:add-buffer(hGroupBuffer).
  hShowQuery:query-prepare("for each groupdata by sortOrder").
  hShowQuery:query-open().
  hShowQuery:get-first().
  repeat while not hShowQuery:query-off-end:
    hShowBuffer:buffer-create().
    hShowBuffer:buffer-copy(hGroupBuffer).
    hShowBuffer:buffer-field(cShowIDColumn):buffer-value() = hGroupBuffer:buffer-field("groupID"):buffer-value().
    hShowBuffer:buffer-field(cShowNameColumn):buffer-value() = hGroupBuffer:buffer-field("groupName"):buffer-value().
    hShowBuffer:buffer-field(cShowLevelColumn):buffer-value() = hGroupBuffer:buffer-field("groupLevel"):buffer-value().
    hShowQuery:get-next().
  end.
  /* set group column values */
  &if defined(noSort) = 0 &then
  {lib/groupby-column-value.i &dataField={&firstField} &whereClause={&whereClause}}
  &else
  {lib/groupby-column-value.i &dataField={&firstField} &whereClause={&whereClause} &noSort=true}
  &endif
  /* change the column names */
  std-ha = getColumn({&browse-name}:handle, cShowNameColumn).
  IF VALID-HANDLE(std-ha)
   THEN std-ha:label = {&columnLabel}.
  DoWait(false).
end.
