&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/dispfileanswer.i

   case:  Other case statement conditions...
   seq: Number sequence of checkbox definitions q<seq>Y, etc.
 */
/*------------------------------------------------------------------------
    File        :   lib/dispfileanswer.i
    Modification:
    Date          Name      Description
    11/14/2016    AC        Implement question priority = 0 functionality.
	02/13/2017    AC        Implement local inspect functionality.
  ----------------------------------------------------------------------*/

{lib/unsetquestion.i {&*}}

&IF defined(exclude-if-define) = 0 &then
  define variable questionpriority as integer no-undo.
  define variable pAuditStatus as character no-undo. 
  &global-define exclude-if-define true 
&ENDIF

publish "GetQuestionPriority" (input {&seq}, output questionpriority).
publish "getFileQuestionAnswer" (input activeFileID, 
                                 input {&seq}, 
                                 output std-ch).
/* Toggle-boxes */
case std-ch:
 {&case}
  when "Y" then 
    do: 
        if valid-handle(q{&seq}Y) 
         then q{&seq}Y:checked  = true.
        if valid-handle(w{&seq}) 
           and w{&seq}:private-data = "Y"
         then w{&seq}:visible = true.
    end.
  when "N" then
    do: 

        if valid-handle(q{&seq}N) 
         then q{&seq}N:checked = true.
        if valid-handle(w{&seq}) 
           and w{&seq}:private-data = "N"
         then w{&seq}:visible = true.
    end.
  when "A" then 
    do: 
        if valid-handle(q{&seq}A) 
         then q{&seq}A:checked = true.
        if valid-handle(w{&seq}) 
           and w{&seq}:private-data = "A"
         then w{&seq}:visible = true.
    end.
  when "U" then 
    do: 
        if valid-handle(q{&seq}U) 
         then q{&seq}U:checked = true.
        if valid-handle(w{&seq}) 
           and w{&seq}:private-data = "U"
         then w{&seq}:visible = true.
    end.
end case.

/* Fill-in */
if valid-handle(q{&seq}) 
 then q{&seq}:screen-value = std-ch.

/* Finding/BP button */
if valid-handle(b{&seq}) 
 then
  do: std-lo = false.
      publish "GetHasFinding" ({&seq}, output std-lo).
      if not std-lo 
       then publish "GetHasBestPractice" ({&seq}, output std-lo).
      if std-lo 
       then b{&seq}:load-image("images/notes.bmp").
       else b{&seq}:load-image("images/notesblank.bmp").
      if questionpriority eq 0 then
       b{&seq}:sensitive = false.      
      else
       b{&seq}:sensitive = true.
  end.

publish "GetAuditStatus" (output pAuditStatus).
if pAuditStatus = "C"then
do:
   if valid-handle(q{&seq}Y)then
     q{&seq}Y:sensitive = false.
   if valid-handle(q{&seq}N) then
     q{&seq}N:sensitive = false.
   if valid-handle(q{&seq}A) then
     q{&seq}A:sensitive = false .
   if valid-handle(q{&seq}U) then
     q{&seq}U:sensitive = false.
   if valid-handle(q{&seq}) then
     q{&seq}:sensitive  = false.

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


