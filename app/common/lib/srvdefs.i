&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
lib/srvdefs.i
standard DEFinitionS section for a SeRVer integration procedure
D.Sinclair
2.8.2014

10.06.2015 jpo Changed xml/xmlparse.i to lib/xmlparse.i
*/


def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

{lib/std-def.i}

&IF defined(exclude-filevars) = 0
 &THEN

def var tPostFile as char no-undo.
def var tTraceFile as char no-undo.
def var tResponseFile as char no-undo.
def var contentType as char initial "text/json" no-undo.
def var clientResponse as util.HttpResponse no-undo.

/* These are used inside xml/xmlparse.i */
&SCOP traceFile tTraceFile
&SCOP xmlFile tResponseFile

&ENDIF

{lib/xmlparse.i &exclude-startElement=true}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


