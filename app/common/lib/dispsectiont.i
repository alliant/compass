&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*dispsectiont.i
  DISPlay the SECTION Title
  File        :   lib/dispsectiont.i
  Modification:
  Date          Name      Description
  09/24/2021    SA        Task#:86696 Defects raised by QA and set section 
                          text width
 */
  /*local variables*/
  define variable tsectionDisplay as character no-undo. 
  define variable tsectiontitleWidth as integer no-undo.  

&if defined(hdr-frame) = 0 &then
&scoped-define hdr-frame fMain
&endif

publish "GetSectionAttribute" ("{&id}", "Description", output std-ch).
tsectiontitleWidth = integer(title1:width-chars in frame {&hdr-frame}) - 24.
 
if  (length(std-ch) > tsectiontitleWidth - 35)
 then
  do:
    tsectionDisplay = substring(std-ch, 1,  r-index(std-ch, " ", tsectiontitleWidth - 22  ) - 1 ).
    tsectionDisplay = trim(tsectionDisplay + "" + fill(".", 3)).
    title1:screen-value = replace(tsectionDisplay,'&','&&') .
    title1:tooltip = "..." + substring(replace(std-ch,'&','&&'), r-index(std-ch, " ", tsectiontitleWidth - 22 ),length(std-ch) ).
 end.
else
 do:
   assign
        title1:screen-value = replace(std-ch,'&','&&')
        title1:tooltip = ""
        .
 end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


