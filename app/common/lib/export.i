&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file export.i
@description Export the report to Excel

@author John Oliver
@created 12.13.2018
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

&if defined(isMulti) = 0 &then
&scoped-define isMulti false
&endif

&if defined(reportName) = 0 &then
&scoped-define reportName ""
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

define variable cExportReportName as character no-undo.

if not {&isMulti}
 then
  do:
    if query {&browse-name}:num-results = 0 
     then
      do: 
        MESSAGE "There is nothing to export" VIEW-AS ALERT-BOX warning BUTTONS OK.
        return.
      end.

    cExportReportName = {&reportName} + "-" + string(today,"99999999").
    
    std-ch = "C".
    publish "GetExportType" (output std-ch).
    if std-ch = "X" 
     then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), cExportReportName).
     else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), cExportReportName).
  end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


