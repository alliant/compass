&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : lib/brw-sortData.i
    Author      : B.Johnson
    Date        : 08.27/2014
    
    Purpose     : Browse procedure for default column sorting.
    
    Usage       : Place this include file in a procedure called 
                  'sortData' in a window that contains a browse 
                  control.
                  
                  Procedure syntax:
                    
                    {lib/brw-sortData.i}
                    END PRODEDURE.
                    
                  Requires lib/std-def.i and lib/brw-main.i

11.12.2014 das    Added pre-by-clause and post-by-clause parameters to allow 
                  for granular control including secondary sorting
10.15.2015 das    Added pre-code option at the beginning of the block.
------------------------------------------------------------------------*/

def input parameter pName as char.

{&pre-code}

hQueryHandle = browse {&browse-name}:query.
hQueryHandle:query-close().
browse {&browse-name}:clear-sort-arrows().

if pName = dataSortBy 
  then dataSortDesc = not dataSortDesc.

{&pre-querystring}
tQueryString = "preselect each {&first-table-in-query-{&browse-name}} no-lock " + 
                {&pre-by-clause}
               (if pName <> ""
                then " by " + pName + (if dataSortDesc then " descending" else "")
                else "")
               {&post-by-clause}.
{&post-querystring}

do std-in = 1 to browse {&browse-name}:num-columns:
  std-ha = browse {&browse-name}:get-browse-column(std-in).
  if std-ha:name = pName 
    then browse {&browse-name}:set-sort-arrow(std-in, not dataSortDesc).
end.
dataSortBy = pName.

&if defined(ExcludeOpenQuery) = 0 &then 
hQueryHandle:query-prepare(tQueryString).
hQueryHandle:query-open().  
&endif

apply 'entry' to browse {&browse-name}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 11.76
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


