&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file alertdatasrv.i
@description Data Server file for the agent alerts

@param syscode;char;Comma-delimited list of syscodes to get

@author John Oliver
@created 04-19-2019
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}

/* tables */
{tt/alert.i}
{tt/esbmsg.i &tableAlias="alertesbmsg"}
{tt/sysprop.i &tableAlias="alertsysprop"}
define temp-table openAlert
  field alertID as integer
  field alertHandle as handle
  .

/* temp tables */
{tt/alert.i &tableAlias="tempalert"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshAlert Procedure
FUNCTION refreshAlert RETURNS logical
  ( input pAlertID as integer ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* main procedures */
subscribe to "AlertChanged"       anywhere.
subscribe to "GetAlert"           anywhere.
subscribe to "GetAlerts"          anywhere.
subscribe to "LoadAlerts"         anywhere.
subscribe to "NewAlert"           anywhere.

/* used for opening/closing the agent data server */
subscribe to "CloseAlert"         anywhere.
subscribe to "OpenAlert"          anywhere.

/* questions */
subscribe to "IsAlertUserDefined" anywhere.

/* used for the messaging system */
subscribe to "TIMER"              anywhere run-procedure "AlertTimer".
subscribe to "ExternalEsbMessage" anywhere run-procedure "AlertESB".

/* used to dump the data file */
subscribe to "DataDump"           anywhere run-procedure "AlertDataDump".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AlertChanged Include 
PROCEDURE AlertChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pAlertID as integer no-undo.
  
  /* refresh the alert */
  refreshAlert(pAlertID).
  
  /* publish that the data changed */
  publish "AlertDataChanged".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AlertDataDump Include 
PROCEDURE AlertDataDump :
/*------------------------------------------------------------------------------
@description Dump the alert and tempalert temp-tables into files
------------------------------------------------------------------------------*/
  /* variables */
  define variable tPrefix as character no-undo.
  
  /* get the prefix */
  publish "GetAppCode" (output tPrefix).
  tPrefix = tPrefix + "_".
  
  /* delete the current temp files */
  publish "DeleteTempFile" ("DataAlert", output std-lo).
  publish "DeleteTempFile" ("DataTempAlert", output std-lo).
  
  /* Active temp-tables */
  temp-table alert:write-xml("file", tPrefix + "Alert.xml").
  publish "AddTempFile" ("DataAlert", tPrefix + "Alert.xml").
  
  /* "Temp" temp-tables */
  temp-table tempalert:write-xml("file", tPrefix + "TempAlert.xml").
  publish "AddTempFile" ("DataAlert", tPrefix + "TempAlert.xml").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AlertESB Procedure 
PROCEDURE AlertESB :
/*------------------------------------------------------------------------------
@description Runs when an esb message is received
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey as character no-undo.
  
  /* exit if not correct entity */
  if pEntity <> "Alert"
   then next.
  
  /* get the next number */
  std-in = 1.
  for each alertesbmsg no-lock:
    std-in = std-in + 1.
  end.
  
  /* create the esb message */
  create alertesbmsg.
  assign
    alertesbmsg.seq    = std-in
    alertesbmsg.rcvd   = now
    alertesbmsg.entity = pEntity
    alertesbmsg.action = pAction
    alertesbmsg.keyID  = pKey
    .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AlertTimer Include 
PROCEDURE AlertTimer :
/*------------------------------------------------------------------------------
@description Triggered by appstart.i running on a timer
------------------------------------------------------------------------------*/
  /* buffers */
  define buffer alertesbmsg for alertesbmsg.

  /* loop through the esb messages */
  for each alertesbmsg exclusive-lock
  break by alertesbmsg.keyID
        by alertesbmsg.rcvd descending:

    /* if the alert is loaded and the we have a messages */
    if first-of(alertesbmsg.keyID)
     then run AlertChanged (alertesbmsg.keyID).
     
    /* delete the esb message and set the last sync */
    delete alertesbmsg.
    publish "SetLastSync".
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseAlert Include 
PROCEDURE CloseAlert :
/*------------------------------------------------------------------------------
@description Closes the alert data server procedure
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pAlertID as integer no-undo.
  
  /* buffers */
  define buffer openAlert for openAlert.
  
  /* get the correct alert data server record */
  for first openAlert exclusive-lock
      where openAlert.alertID = pAlertID:
      
    /* delete the proceudre to the data server */
    if valid-handle(openAlert.alertHandle)
     then delete object openAlert.alertHandle.
    
    /* delete the alert data server record */
    delete openAlert.
  end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAlert Include 
PROCEDURE GetAlert :
/*------------------------------------------------------------------------------
@description Gets an alert
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pAlertID as integer no-undo.
  define output parameter table for tempalert.
  
  /* buffers */
  define buffer alert for alert.

  /* copy the alert */
  empty temp-table tempalert.
  for first alert no-lock
      where alert.alertID = pAlertID:
    
    create tempalert.
    buffer-copy alert to tempalert.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAlerts Include 
PROCEDURE GetAlerts :
/*------------------------------------------------------------------------------
@description Get the alerts
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for tempalert.
  
  define buffer alert for alert.
  
  /* assume that the criteria match and only copy those rows */
  empty temp-table tempalert.
  std-lo = true.
  for each alert no-lock:     
    create tempalert.
    buffer-copy alert to tempalert.
  end.
  
  /* get the descriptions of fields */
  for each tempalert exclusive-lock:
    /* set the created by name */
    tempalert.createdByDesc = tempalert.createdBy.
    publish "GetSysUserName" (tempalert.createdBy, output tempalert.createdByDesc).
    /* set the status */
    tempalert.statDesc = tempalert.stat.
    publish "GetSysPropDesc" ("AMD", "Alert", "Status", tempalert.stat, output tempalert.statDesc).
    /* set the alert type */
    tempalert.processCodeDesc = tempalert.processCode.
    publish "GetSysCodeDesc" ("Alert", tempalert.processCode, output tempalert.processCodeDesc).
    /* set the severity */
    tempalert.severityDesc = string(tempalert.severity).
    publish "GetSysPropDesc" ("AMD", "Alert", "Severity", tempalert.severity, output tempalert.severityDesc).
    /* set the owner */
    tempalert.ownerDesc = tempalert.owner.
    publish "GetSysPropDesc" ("AMD", "Alert", "Owner", tempalert.owner, output tempalert.ownerDesc).
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsAlertUserDefined Include 
PROCEDURE IsAlertUserDefined :
/*------------------------------------------------------------------------------
@description Checks if the alert is user defined
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pCode        as character no-undo.
  define output parameter pUserDefined as logical no-undo initial false.
  
  /* see if the system properties are loaded */
  publish "GetSysProp" ("AMD", "Alert", "Threshold", output table alertsysprop).
  
  /* if not available, load the alert sysprops */
  if not can-find(first alertsysprop)
   then run server/getsysprops.p (input  "AMD",
                                  input  "Alert",
                                  input  "Threshold",
                                  input  "",
                                  input  "",
                                  output table alertsysprop,
                                  output std-lo,
                                  output std-ch
                                  ).
  
  pUserDefined = can-find(first alertsysprop where objID = pCode and objValue = "User Defined").
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadAlerts Include 
PROCEDURE LoadAlerts :
/*------------------------------------------------------------------------------
@description Loads the alerts
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pStateID as character no-undo.
  define input parameter pAgentID as character no-undo.
  define input parameter pCode    as character no-undo.
  define input parameter pStatus  as character no-undo.
     
  /* call the server */
  empty temp-table alert.
  run server/getagentalerts.p (input  0,
                               input  pStateID,
                               input  pAgentID,
                               input  pCode,
                               input  pStatus,
                               output table alert,
                               output std-lo,
                               output std-ch).
  
  /* if unsuccessful, show a message if debug */
  if not std-lo
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadAlerts failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenAlert C-Win 
PROCEDURE OpenAlert :
/*------------------------------------------------------------------------------
@description Loads the agent into the agentdatasrv.p file
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pAlertID as integer no-undo.
  define output parameter pHandle as handle no-undo.
  
  /* buffers */
  define buffer openAlert for openAlert.
  
  /* if available, get the handle to the alert data server */
  if can-find(first openAlert where alertID = pAlertID)
   then 
    for first openAlert no-lock
        where openAlert.alertID = pAlertID:
        
      pHandle = openAlert.alertHandle.
    end.
   else 
    do:
      /* otherwise run the alert data server and create the record */
      run alertdatasrv.p persistent set pHandle (pAlertID).
      create openAlert.
      assign
        openAlert.alertID = pAlertID
        openAlert.alertHandle = pHandle
        .
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewAlert C-Win 
PROCEDURE NewAlert :
/*------------------------------------------------------------------------------
@description Creates a new alert
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter table for tempalert.
  define output parameter pHandle as handle no-undo.
  
  /* variables */
  define variable tNewAlertID as integer no-undo.
  
  /* buffers */
  define buffer tempalert for tempalert.
  
  run server/newagentalert.p (input table tempalert,
                              output tNewAlertID,
                              output std-lo,
                              output std-ch).
                                     
  /* if unsuccessful, show a message */
  if not std-lo
   then message std-ch view-as alert-box error.
   else
    do:
      /* otherwise, add the new alert details to the list of alerts */
      for first tempalert no-lock:
        create alert.
        buffer-copy tempalert to alert.
        alert.alertID = tNewAlertID.
      end.
      run OpenAlert (tNewAlertID, output pHandle).
      publish "AgentDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshSysCode Include 
FUNCTION refreshAlert RETURNS logical
  ( input pAlertID as integer ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* variables */
  define variable lSuccess as logical no-undo.
  
  /* buffers */
  define buffer tempalert for tempalert.
  define buffer alert for alert.
  
  /* call the server */
  empty temp-table tempalert.
  run server/getagentalerts.p (input  pAlertID,
                               input  "ALL",
                               input  "ALL",
                               input  "ALL",
                               input  "ALL",
                               output table tempalert,
                               output lSuccess,
                               output std-ch).

  /* if unsuccessful, show a message if debug is on */
  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshAlert failed: " + std-ch view-as alert-box warning.
    end.
   else
    /* otherwise, copy the alert details into the alert records */
    do:
      find tempalert where tempalert.alertID = pAlertID no-error.
      find alert where alert.alertID = pAlertID no-error.
      if not available tempalert and available alert /* delete */
       then delete alert.
      if available tempalert and not available alert /* new */
       then create alert.
      if available tempalert and available alert /* modify */
       then buffer-copy tempalert to alert.
    end.
  
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
