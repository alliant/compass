&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file reference-config.i
@description Procedures to allow users to choose their own columns
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
/* temp tables */
{tt/listfield.i}

/* functions and procedures */
{lib/find-widget.i}
{lib/build-browse.i &noTitle=true}

/* variables */
{lib/std-def.i}
define variable dReferenceColumnWidth as decimal no-undo.

&if defined(integerOnly) = 0 &then
&scoped-define integerOnly false
&endif

&if defined(excludeColumn) = 0 &then
&scoped-define excludeColumn ""
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD areDefaultColumns Include 
FUNCTION areDefaultColumns RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getBrowseHandle Include 
FUNCTION getBrowseHandle RETURNS WIDGET-HANDLE
  ( input pFrame as handle )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

dReferenceColumnWidth = 0.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE BuildBrowseColumns Include 
PROCEDURE BuildBrowseColumns :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* variables */
  define variable hBuffer        as handle    no-undo.
  define variable hField         as handle    no-undo.
  define variable cLabel         as character no-undo case-sensitive.
  define variable cUserList      as character no-undo.
  
  /* get the reference colums from the config */
  publish "GetReferenceColumns" ({&entity}, output cUserList).
  
  create buffer hBuffer for table {&table}.
  /* get the selected column list */
  empty temp-table listfield.
  do std-in = 1 to num-entries(cUserList):
    hField = hBuffer:buffer-field(entry(std-in, cUserList)).
    if valid-handle(hField)
     then
      do:
        cLabel = hField:column-label.
        if cLabel = hField:name
         then cLabel = hField:label.
        
        create listfield.
        assign
          listfield.columnBuffer = hField:name
          listfield.displayName = cLabel
          listfield.dataType = hField:data-type
          listfield.columnFormat = hField:format
          .
        case hField:data-type:
         when "datetime"  then listfield.columnWidth = 15.
         when "decimal"   then listfield.columnWidth = 15.
         when "integer"   then listfield.columnWidth = 10.
         when "logical"   then listfield.columnWidth = 10.
         when "character" then listfield.columnWidth = 25.
         otherwise listfield.columnWidth = 25.
        end case.
      end.
  end.

  delete object hBuffer no-error.
  delete object hField no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetReferenceColumns Include 
PROCEDURE GetReferenceColumns :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE INPUT PARAMETER pDoConfig AS LOGICAL NO-UNDO.

  define variable hBuffer as handle no-undo.
  define variable hField as handle no-undo.
  define variable cLabel as character no-undo case-sensitive.
  define variable cUserList as character no-undo.

  define variable pAvailableList as character no-undo.
  define variable pSelectedList as character no-undo.
  define variable pCancel as logical no-undo.
  
  publish "GetReferenceColumns" ({&entity}, output cUserList).
  if cUserList = ""
   then publish "GetReferenceColumns" ({&entity} + "Default", output cUserList).
  
  create buffer hBuffer for table {&table}.
  /* get the selected column list */
  empty temp-table listfield.
  do std-in = 1 to num-entries(cUserList):
    hField = hBuffer:buffer-field(entry(std-in, cUserList)).
    if valid-handle(hField)
     then
      do:
        cLabel = hField:column-label.
        if cLabel = hField:name
         then cLabel = hField:label.
        
        pSelectedList = addDelimiter(pSelectedList, ",") + replace(cLabel, "!", " ") + "," + hField:name.
        create listfield.
        assign
          listfield.columnBuffer = hField:name
          listfield.displayName = cLabel
          listfield.dataType = hField:data-type
          listfield.columnFormat = hField:format
          .
        case hField:data-type:
         when "datetime" then listfield.columnWidth = 15.
         when "decimal" then listfield.columnWidth = 15.
         when "integer" then listfield.columnWidth = 10.
         when "logical" then listfield.columnWidth = 10.
         when "character" then listfield.columnWidth = 25.
         otherwise listfield.columnWidth = 25.
        end case.
      end.
  end.
  /* get the rest of the columns */
  do std-in = 1 to hBuffer:num-fields:
    hField = hBuffer:buffer-field(std-in).
    if valid-handle(hField)
     then
      do:
        cLabel = hField:column-label.
        if cLabel = hField:name
         then cLabel = hField:label.
        
        if cLabel = hField:name
         then next.
        
        /* exclude fields */
        &if defined(excludeField) <> 0 &then
        if lookup(hField:name, {&excludeField}) > 0
         then next.
        &endif
        
        if lookup(hField:name, cUserList) = 0
         then pAvailableList = addDelimiter(pAvailableList, ",") + replace(cLabel, "!", " ") + "," + hField:name.
      end.
  end.
  
  IF pDoConfig
   THEN
    DO:
      run dialogcolumnselect.w (input {&entity}, input pAvailableList, input-output pSelectedList, output pCancel).
      
      if not pCancel
       then publish "SetReferenceColumns" ({&entity}, pSelectedList).
    END.

  delete object hBuffer no-error.
  delete object hField no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetReferenceData Include 
PROCEDURE GetReferenceData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE INPUT PARAMETER pFrame AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER pQuery AS CHARACTER NO-UNDO.

  define variable hQuery as handle no-undo.
  define variable hBuffer as handle no-undo.
  define variable hTable as handle no-undo.
  define variable hTableBuffer as handle no-undo.

  create buffer hBuffer for table {&table}.
  create temp-table hTable.
  hTable:add-new-field("keyfield","character",0,"x(50)").
  if not can-find(first listfield)
   then run GetReferenceColumns (false).
  for each listfield no-lock:
    hTable:add-new-field(listfield.columnBuffer,listfield.dataType,0,listfield.columnFormat).
  end.
  hTable:temp-table-prepare({&table}).
  hTableBuffer = hTable:default-buffer-handle.

  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare(pQuery).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hTableBuffer:buffer-create().
    hTableBuffer:buffer-copy(hBuffer).
    hTableBuffer:buffer-field("keyfield"):buffer-value() = hBuffer:buffer-field({&keyfield}):buffer-value().
    hQuery:get-next().
  end.
  hQuery:query-close().
  run BuildBrowse in this-procedure (hTable, temp-table listfield:handle, pFrame, {&entity}).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResizeColumns Include 
PROCEDURE ResizeColumns :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pFrame  as handle no-undo.
  define input parameter pColumn as character no-undo.
  
  define variable dTotalWidth as decimal no-undo.
  define variable dAddWidth   as decimal no-undo.
  define variable dFrameWidth as decimal no-undo.
  define variable dColAvail   as integer no-undo.
  define variable iCol        as integer no-undo.
  define variable hBrowse     as handle no-undo.
  define variable hColumn     as handle no-undo.
  
  define buffer listfield for listfield.
  
  /* get the original column widths */
  for each listfield no-lock:
    dTotalWidth = dTotalWidth + (listfield.columnWidth * session:pixels-per-column + 5.9).
  end.
  
  /* resize the column only if there is room to spare */
  hBrowse = getBrowseHandle(pFrame).
  dFrameWidth = pFrame:width-pixels.
  if dTotalWidth < dFrameWidth
   then 
    do:
      do iCol = 1 to hBrowse:num-columns:
        hColumn = hBrowse:get-browse-column(iCol).
        if valid-handle(hColumn) and lookup(hColumn:name, pColumn) > 0
         then dColAvail = dColAvail + 1.
      end.
      dAddWidth = (dFrameWidth - dTotalWidth) / dColAvail.
    end.
   else dAddWidth = 0.
  
  if valid-handle(hBrowse) and hBrowse:type = "BROWSE"
   then
    do:
      hColumn = hBrowse:first-column.
      do while valid-handle(hColumn):
        for first listfield no-lock
            where listfield.columnBuffer = hColumn:name:
            
          hColumn:width = listfield.columnWidth.
        end.
        do iCol = 1 to num-entries(pColumn):
          if hColumn:name = entry(iCol, pColumn)
           then hColumn:width-pixels  = dReferenceColumnWidth + dAddWidth.
        end.
        hColumn = hColumn:next-column.
      end.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetBrowseColumns Include 
PROCEDURE SetBrowseColumns :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* variables */
  define variable hBuffer        as handle    no-undo.
  define variable hField         as handle    no-undo.
  define variable cLabel         as character no-undo case-sensitive.
  define variable pAvailableList as character no-undo.
  define variable pSelectedList  as character no-undo.
  
  /* build the current column list if not done so already */
  if not can-find(first listfield)
   then run BuildBrowseColumns.
  
  /* get the selected column list */
  for each listfield no-lock:
    pSelectedList = addDelimiter(pSelectedList, ",") + listfield.displayName + "," + listfield.columnBuffer.
  end.
  
  create buffer hBuffer for table {&table}.
  /* get the rest of the columns */
  do std-in = 1 to hBuffer:num-fields:
    hField = hBuffer:buffer-field(std-in).
    if valid-handle(hField)
     then
      do:
        cLabel = hField:column-label.
        if cLabel = hField:name
         then cLabel = hField:label.
        
        if cLabel = hField:name
         then next.
        
        /* exclude fields */
        &if defined(excludeField) <> 0 &then
        if lookup(hField:name, {&excludeField}) > 0
         then next.
        &endif
        
        if not can-find(first listfield where columnBuffer = hField:name)
         then pAvailableList = addDelimiter(pAvailableList, ",") + replace(cLabel, "!", " ") + "," + hField:name.
      end.
  end.
  /* cleanup handles */
  delete object hBuffer no-error.
  delete object hField no-error.
  
  std-lo = false.
  run dialogcolumnselect.w (input {&entity}, input pAvailableList, input-output pSelectedList, output std-lo).
  
  if not std-lo and areDefaultColumns()
   then publish "SetReferenceColumns" ({&entity}, pSelectedList).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetTotalData Include 
PROCEDURE SetTotalData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pFrame as handle no-undo.
  define input parameter pQuery as character no-undo.
  define input parameter pShow  as logical no-undo.
  
  define variable i        as integer no-undo.
  define variable j        as integer no-undo.
  define variable tempDec  as decimal no-undo.
  define variable dMin     as decimal no-undo initial -9999999.
  define variable lDefault as logical no-undo initial false.
  define variable dColSum  as decimal no-undo.
  
  define variable dTotal   as decimal no-undo extent.
  define variable iCol     as decimal no-undo.
  define variable iRow     as decimal no-undo.
  define variable iWidth   as decimal no-undo.
  define variable iPos     as integer no-undo.
  define variable dataType as character no-undo.
  define variable scrValue as character no-undo.
  define variable svFormat as character no-undo.
  define variable hToolTip as character no-undo.

  define variable hBrowse  as handle no-undo.
  define variable hField   as handle no-undo.
  define variable hQuery   as handle no-undo.
  define variable hBuffer  as handle no-undo.
  
  /* drop and recreate a "pool" to hold the widgets */
  delete widget-pool "referenceTotal" no-error.
  create widget-pool "referenceTotal" persistent.
  
  /* get the browse */
  hBrowse = getBrowseHandle(pFrame).
  if not valid-handle(hBrowse) or hBrowse:type <> "BROWSE"
   then return.
  
  /* set the array extent to the number of columns */
  if extent(dTotal) = ?
   then extent(dTotal) = hBrowse:num-columns.

  /* used to see if we have default columns */
  lDefault = areDefaultColumns().

  /* set the browse height for the total row */
  hBrowse:height-pixels = hBrowse:height-pixels - 23.
  
  /* clear our the array and remove existing total row */
  do i = 1 to extent(dTotal):
    dTotal[i] = 0.
  end.

  /* get the totals for each column */
  create buffer hBuffer for table {&table}.
  create query hQuery.
  hQuery:add-buffer(hBuffer).
  hQuery:query-prepare(pQuery).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    do i = 1 to hBrowse:num-columns:
      hField = hBuffer:buffer-field(hBrowse:get-browse-column(i):name).
      if valid-handle(hField)
       then
        if lookup(string(i),"{&excludeColumn}",",") = 0 and lDefault
         then
          do:
            tempDec = decimal(hField:buffer-value) no-error.
            if dTotal[i] <> dMin and not error-status:error and hField:buffer-value <> ""
             then dTotal[i] = dTotal[i] + tempDec.
             else dTotal[i] = dMin.
          end.
         else dTotal[i] = dMin.
    end.
    hQuery:get-next().
  end.
  
  /* create the widget */
  do i = 1 to extent(dTotal):
    /* skip to the next widget if not default columns */
    if i > 1 and not lDefault
     then next.
  
    hField = hBrowse:get-browse-column(i).
    iCol = hField:column + hBrowse:col.
    iWidth = hField:width-chars - 0.1.
    hToolTip = "".
    
    /* first row */
    if i = 1
     then
      assign
        scrValue = "Totals"
        dataType = "CHARACTER"
        svFormat = "x(8)"
        .
     else
      /* check if valid row */
      if dTotal[i] = dMin or dTotal[i] = 0
       then
        assign
          scrValue = ""
          dataType = "CHARACTER"
          svFormat = "x(8)"
          .
       else
        do:
          scrValue = string(dTotal[i]).
          dataType = "DECIMAL".
          svFormat = "".
          
          /* account for negative numbers */
          if dTotal[i] < 0
           then svFormat = svFormat + "(".
          
          /* loop through the absolute value of the number cast as an int64 */
          do j = length(string(int64(absolute(dTotal[i])))) to 1 by -1:
            if j modulo 3 = 0
             then svFormat = svFormat + (if j = length(string(int64(absolute(dTotal[i])))) then ">" else ",>").
             else svFormat = svFormat + (if j = 1 then "Z" else ">").
          end.
       
          /* if the number had a decimal value */
          if index(scrValue, ".") > 0 and not {&integerOnly}
           then svFormat = svFormat + ".99".
           
          /* account for negative numbers */
          if dTotal[i] < 0
           then svFormat = svFormat + ")".
          
          iPos = hField:width-pixels - font-table:get-text-width-pixels(svFormat, 1).
          iPos = iPos / font-table:get-text-width-pixels(" ", 1).
          
          if iPos < 2
           then
            assign
              hToolTip = string(scrValue,replace(svFormat,"Z","9"))
              scrValue = "*"
              dataType = "CHARACTER"
              svFormat = "x(8)"
              .
           else svFormat = fill(" ", iPos) + svFormat.
        end.
        
    /* if the user doesn't want to show the total row, just set every field to blank */
    if i > 1 and not pShow
     then
      assign
        scrValue = ""
        dataType = "CHARACTER"
        svFormat = "x(8)"
        .
        
    hField = ?.
    create text hField in widget-pool "referenceTotal" assign
      frame         = pFrame
      row           = hBrowse:row + hBrowse:height-chars + 0.1
      column        = iCol
      data-type     = dataType
      format        = svFormat
      screen-value  = scrValue
      visible       = true
      width         = iWidth
      font          = 1
      name          = "totalRow" + string(i)
      .
    
    if hToolTip <> ""
     then hField:tooltip = hToolTip.
  end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StoreColumnWidth Include 
PROCEDURE StoreColumnWidth :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pFrame  as handle no-undo.
  define input parameter pColumn as character no-undo.
  
  define variable hWindow as handle no-undo.
  define variable hBrowse as handle no-undo.
  define variable hColumn as handle no-undo.
  
  hBrowse = getBrowseHandle(pFrame).
  if valid-handle(hBrowse) and hBrowse:type = "BROWSE"
   then
    do:
      hColumn = hBrowse:first-column.
      do while valid-handle(hColumn):
        if hColumn:name = pColumn
         then dReferenceColumnWidth = hColumn:width-pixels.
        hColumn = hColumn:next-column.
      end.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION areDefaultColumns Include 
FUNCTION areDefaultColumns RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cDefault as character no-undo.
  define variable cUser    as character no-undo.
  
  publish "GetReferenceColumns" ({&entity}, output cUser).
  publish "GetReferenceColumnsDefault" ({&entity}, output cDefault).
  
  RETURN cUser = "" or cUser = cDefault.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getBrowseHandle Include 
FUNCTION getBrowseHandle RETURNS WIDGET-HANDLE
  ( input pFrame as handle ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable hWidget as widget-handle no-undo.
  define variable lFound  as logical no-undo initial false.

  hWidget = pFrame:first-child:first-child.
  repeat while valid-handle(hWidget) and not lFound:
    if hWidget:type = "BROWSE"
     then lFound = true.
     else hWidget = hWidget:next-sibling.
  end.
  RETURN hWidget.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

