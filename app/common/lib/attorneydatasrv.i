&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file attorneydatasrv.i
@description Data Server file for managing the attorney

@author John Oliver
@created 07-11-2018
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}
define variable tLoadedAttorneys as logical initial false no-undo.

/* tables */
{tt/attorney.i}
{tt/esbmsg.i &tableAlias="attorneyesbmsg"}
define temp-table openAttorney
  field attorneyID as character
  field attorneyHandle as handle
  .

/* temp tables */
{tt/attorney.i &tableAlias="tempattorney"}

/* functions */
{lib/add-delimiter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshAttorney Include 
FUNCTION refreshAttorney RETURNS LOGICAL
  ( input pAttorneyID as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* attorney procedures */
subscribe to "AttorneyChanged"      anywhere.
subscribe to "CloseAttorney"        anywhere.
subscribe to "DeleteAttorney"       anywhere.
subscribe to "GetAttorney"          anywhere.
subscribe to "GetAttorneys"         anywhere.
subscribe to "GetProspectAttorneys" anywhere.
subscribe to "LoadAttorneys"        anywhere.
subscribe to "NewAttorney"          anywhere.
subscribe to "OpenAttorney"         anywhere.

/* used for the messaging system */
subscribe to "TIMER"                anywhere run-procedure "AttorneyTimer".
subscribe to "ExternalEsbMessage"   anywhere run-procedure "AttorneyESB".
                                    
/* used to dump the data file */
subscribe to "DataDump"             anywhere run-procedure "AttorneyDataDump".

std-lo = true.
publish "SetSplashStatus".
publish "GetLoadAttorneys" (output std-lo).
if std-lo
 then run LoadAttorneys.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AttorneyChanged Include 
PROCEDURE AttorneyChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pAttorneyID as character no-undo.
  
  /* refresh the attorney */
  refreshAttorney(pAttorneyID).
  
  /* publish that the data changed */
  publish "AttorneyDataChanged".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AttorneyDataDump Include 
PROCEDURE AttorneyDataDump :
/*------------------------------------------------------------------------------
@description Dump the attorney and tempattorney temp-tables into files
------------------------------------------------------------------------------*/
  /* variables */
  define variable tPrefix as character no-undo.
  
  /* get the prefix */
  publish "GetAppCode" (output tPrefix).
  tPrefix = tPrefix + "_".
  
  /* delete the current temp files */
  publish "DeleteTempFile" ("DataAttorney", output std-lo).
  publish "DeleteTempFile" ("DataTempAttorney", output std-lo).
  
  /* Active temp-tables */
  temp-table attorney:write-xml("file", tPrefix + "Attorney.xml").
  publish "AddTempFile" ("DataAttorney", tPrefix + "Attorney.xml").
  
  /* "Temp" temp-tables */
  temp-table tempattorney:write-xml("file", tPrefix + "TempAttorney.xml").
  publish "AddTempFile" ("DataAttorney", tPrefix + "TempAttorney.xml").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AttorneyESB Procedure 
PROCEDURE AttorneyESB :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey as character no-undo.
  
  /* exit if not correct entity */
  if pEntity <> "Attorney"
   then next.
  
  /* get the next number */
  std-in = 1.
  for each attorneyesbmsg no-lock:
    std-in = std-in + 1.
  end.
  
  /* create the esb message */
  create attorneyesbmsg.
  assign
    attorneyesbmsg.seq    = std-in
    attorneyesbmsg.rcvd   = now
    attorneyesbmsg.entity = pEntity
    attorneyesbmsg.action = pAction
    attorneyesbmsg.keyID  = pKey
    .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AttorneyTimer Procedure 
PROCEDURE AttorneyTimer :
/*------------------------------------------------------------------------------
  Purpose:     Called when the application is idle
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* buffers */
  define buffer attorneyesbmsg for attorneyesbmsg.

  /* loop through the esb messages */
  for each attorneyesbmsg exclusive-lock
  break by attorneyesbmsg.keyID
        by attorneyesbmsg.rcvd descending:

    /* if the agent is loaded and the we have a messages */
    if tLoadedAttorneys and first-of(attorneyesbmsg.keyID) 
     then run AttorneyChanged (attorneyesbmsg.keyID).
     
    /* delete the esb message and set the last sync */
    delete attorneyesbmsg.
    publish "SetLastSync".
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseAttorney Include 
PROCEDURE CloseAttorney :
/*------------------------------------------------------------------------------
@description Closes the attorney data server procedure
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pAttorneyID as character no-undo.
  
  /* buffers */
  define buffer openAttorney for openAttorney.
  
  /* get the correrct attorney record */
  for first openAttorney exclusive-lock
      where openAttorney.attorneyID = pAttorneyID:
      
    /* delete the proceudre to the data server */
    if valid-handle(openAttorney.attorneyHandle)
     then delete object openAttorney.attorneyHandle.
    
    /* delete the agent data server record */
    delete openAttorney.
  end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteAttorney Include 
PROCEDURE DeleteAttorney :
/*------------------------------------------------------------------------------
@description Delete an attorney
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pAttorneyID as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  
  /* buffers */
  define buffer attorney for attorney.
  
  /* call the server */
  run server/deleteattorney.p (input pAttorneyID,
                            output pSuccess,
                            output std-ch).

  /* if unsuccessful, show a message */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    /* otherwise, close the attorney data server and delete the attorney */
    for first attorney exclusive-lock
        where attorney.attorneyID = pAttorneyID:
      
      run CloseAttorney (attorney.attorneyID).
      delete attorney.
      publish "AttorneyDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAttorney Include 
PROCEDURE GetAttorney :
/*------------------------------------------------------------------------------
@description Gets an attorney
------------------------------------------------------------------------------*/
  define input parameter pAttorneyID as character no-undo.
  define output parameter table for tempattorney.
  define buffer tempattorney for tempattorney.

  /* delete all agents that aren't the one passed in */
  empty temp-table tempattorney.
  run GetAttorneys (output table tempattorney).

  /* delete all attorneys that aren't the one passed in */
  for each tempattorney exclusive-lock
     where tempattorney.attorneyID <> pAttorneyID:
    
    delete tempattorney.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAttorneys Include 
PROCEDURE GetAttorneys :
/*------------------------------------------------------------------------------
@description Gets the attorneys
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for attorney.

  /* if not loaded, load the attorneys */
  if not tLoadedAttorneys
   then run LoadAttorneys.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&IF DEFINED(EXCLUDE-GetProspectAttorneys) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetProspectAttorneys Procedure 
PROCEDURE GetProspectAttorneys :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for tempattorney.
   
  /* buffers */
  define buffer tempattorney for tempattorney.

  /* get the attorneys */
  empty temp-table tempattorney.
  publish "GetAttorneys" (output table tempattorney).
  
  /* delete the list of attorneys that are not prospects */
  for each tempattorney no-lock
     where tempattorney.stat <> "P":
    
    delete tempattorney.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadAttorneys Include 
PROCEDURE LoadAttorneys :
/*------------------------------------------------------------------------------
@description Loads the attorneys
------------------------------------------------------------------------------*/
  /* call the server */
  empty temp-table attorney.
  run server/getattorneys.p ("",
                             "P" ,  /* Person  */
                             output table attorney,
                             output tLoadedAttorneys,
                             output std-ch).

  /* if unsuccessful, show a message if debug is on */
  if not tLoadedAttorneys
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadAttorneys failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewAttorney Include 
PROCEDURE NewAttorney :
/*------------------------------------------------------------------------------
@description Creates a new attorney
------------------------------------------------------------------------------*/
  define input parameter table for tempattorney.
  define output parameter pHandle as handle no-undo.
  
  define variable tNewAttorneyID as character no-undo.
  define variable opcOrgID       as character no-undo.
  define buffer tempattorney for tempattorney.
  
  run server/newattorney.p (input table tempattorney,
                            output tNewAttorneyID,
                            output opcOrgID,
                            output std-lo,
                            output std-ch).
                         
  if not std-lo
   then message std-ch view-as alert-box error.
   else
    do:
      for first tempattorney no-lock:
        create attorney.
        buffer-copy tempattorney to attorney.
        attorney.attorneyID = tNewAttorneyID.
      end.
      run OpenAttorney (tNewAttorneyID, output pHandle).
      publish "AttorneyDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenAttorney Include 
PROCEDURE OpenAttorney :
/*------------------------------------------------------------------------------
@description Loads the attorney into the attorneydatasrv.p file
------------------------------------------------------------------------------*/
  define input parameter pAttorneyID as character no-undo.
  define output parameter pHandle as handle no-undo.
  
  define buffer openAttorney for openAttorney.
  
  if can-find(first openAttorney where attorneyID = pAttorneyID)
   then 
    for first openAttorney no-lock
        where openAttorney.attorneyID = pAttorneyID:
        
      pHandle = openAttorney.attorneyHandle.
    end.
   else 
    do:
      /*run attorneydatasrv.p persistent set pHandle (pAttorneyID).*/
      create openAttorney.
      assign
        openAttorney.attorneyID = pAttorneyID
        openAttorney.attorneyHandle = pHandle
        .
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshAttorney Include 
FUNCTION refreshAttorney RETURNS LOGICAL
  ( input pAttorneyID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* variables */
  define variable lSuccess as logical no-undo.
  
  /* buffers */
  define buffer tempattorney for tempattorney.
  define buffer attorney for attorney.
  
  /* call the server */
  empty temp-table tempattorney.
  run server/getattorneys.p (pAttorneyID,
                             "P" ,  /* Person  */
                             output table tempattorney,
                             output lSuccess,
                             output std-ch).

  /* if unsuccessful, show a message if debug is on */
  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshAgent failed: " + std-ch view-as alert-box warning.
    end.
   else
    /* otherwise, copy the attorney details into the attorney records */
    do:
      find tempattorney where tempattorney.attorneyID = pAttorneyID no-error.
      find attorney where attorney.attorneyID = pAttorneyID no-error.
      if not available tempattorney and available attorney /* delete */
       then delete attorney.
      if available tempattorney and not available attorney /* new */
       then create attorney.
      if available tempattorney and available attorney /* modify */
       then buffer-copy tempattorney to attorney.
    end.
  
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

