&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/docheckboxf.i 
  Modification:
    Date          Name      Description
    12/26/2016    AG        Enable save button if any answer is changed,
                            implement AutoSave functionality.
*/
def input parameter pQuestionSeq as int.

def var tNewWarning as widget-handle.
def var tOldWarning as widget-handle.
def var tSetting as char.

if focus:private-data <> ""
 then tSetting = focus:private-data.
 else tSetting = substring(focus:name, length(focus:name)).

publish "SetFileQuestionAnswer" (activeFileID,
                                 pQuestionSeq, 
                                 if focus:checked
                                   then tSetting
                                   else "").

if error-status:error 
 then 
  do: focus:checked = not focus:checked.
      return.
  end.

for each toggleset
  where toggleset.seq = pQuestionSeq:
 if toggleset.togglebox <> focus
   then 
    do: toggleset.togglebox:checked = false.
        if valid-handle(toggleset.haswarning)
         then tOldWarning = toggleset.haswarning.
    end.
   else tNewWarning = toggleset.haswarning.
end.

if valid-handle(tNewWarning)
  then tNewWarning:visible = focus:checked.
 if valid-handle(tOldWarning)
  then tOldWarning:visible = false.

 publish "EnableSave".
 publish "SaveAfterEveryAnswer" .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


