&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file activitydatasrv.i
@description Data Server file for the agent activities

@author John Oliver
@created 04-19-2019
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}
define variable tYearList as character no-undo.

/* tables */
{tt/activity.i}
{tt/esbmsg.i &tableAlias="activityesbmsg"}
{tt/sysprop.i &tableAlias="activitysysprop"}
define temp-table openActivity
  field activityID as integer
  field activityHandle as handle
  .

/* temp tables */
{tt/activity.i &tableAlias="tempactivity"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshActivity Procedure
FUNCTION refreshActivity RETURNS logical
  ( input pActivityID as integer ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* main procedures */
subscribe to "ActivityChanged"     anywhere.
subscribe to "DeleteActivity"      anywhere.
subscribe to "FinalizeActivities"  anywhere.
subscribe to "GetActivity"         anywhere.
subscribe to "GetActivityYearList" anywhere.
subscribe to "GetActivities"       anywhere.
subscribe to "LoadActivities"      anywhere.
subscribe to "NewActivity"         anywhere.

/* used for opening/closing the agent data server */
subscribe to "CloseActivity"       anywhere.
subscribe to "OpenActivity"        anywhere.

/* used for the messaging system */
subscribe to "TIMER"               anywhere run-procedure "ActivityTimer".
subscribe to "ExternalEsbMessage"  anywhere run-procedure "ActivityESB".

/* used to dump the data file */
subscribe to "DataDump"            anywhere run-procedure "ActivityDataDump".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActivityChanged Include 
PROCEDURE ActivityChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pActivityID as integer no-undo.
  
  /* refresh the activity */
  refreshActivity(pActivityID).
  
  /* publish that the data changed */
  publish "ActivityDataChanged".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActivityDataDump Include 
PROCEDURE ActivityDataDump :
/*------------------------------------------------------------------------------
@description Dump the activity and tempactivity temp-tables into files
------------------------------------------------------------------------------*/
  /* variables */
  define variable tPrefix as character no-undo.
  
  /* get the prefix */
  publish "GetAppCode" (output tPrefix).
  tPrefix = tPrefix + "_".
  
  /* delete the current temp files */
  publish "DeleteTempFile" ("DataActivity", output std-lo).
  publish "DeleteTempFile" ("DataTempActivity", output std-lo).
  
  /* Active temp-tables */
  temp-table activity:write-xml("file", tPrefix + "Activity.xml").
  publish "AddTempFile" ("DataActivity", tPrefix + "Activity.xml").
  
  /* "Temp" temp-tables */
  temp-table tempactivity:write-xml("file", tPrefix + "TempActivity.xml").
  publish "AddTempFile" ("DataActivity", tPrefix + "TempActivity.xml").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActivityESB Procedure 
PROCEDURE ActivityESB :
/*------------------------------------------------------------------------------
@description Runs when an esb message is received
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey as character no-undo.
  
  /* exit if not correct entity */
  if pEntity <> "Activity"
   then next.
  
  /* get the next number */
  std-in = 1.
  for each activityesbmsg no-lock:
    std-in = std-in + 1.
  end.
  
  /* create the esb message */
  create activityesbmsg.
  assign
    activityesbmsg.seq    = std-in
    activityesbmsg.rcvd   = now
    activityesbmsg.entity = pEntity
    activityesbmsg.action = pAction
    activityesbmsg.keyID  = pKey
    .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActivityTimer Include 
PROCEDURE ActivityTimer :
/*------------------------------------------------------------------------------
@description Triggered by appstart.i running on a timer
------------------------------------------------------------------------------*/
  /* buffers */
  define buffer activityesbmsg for activityesbmsg.

  /* loop through the esb messages */
  for each activityesbmsg exclusive-lock
  break by activityesbmsg.keyID
        by activityesbmsg.rcvd descending:

    /* if the activity is loaded and the we have a messages */
    if first-of(activityesbmsg.keyID)
     then run ActivityChanged (activityesbmsg.keyID).
     
    /* delete the esb message and set the last sync */
    delete activityesbmsg.
    publish "SetLastSync".
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseActivity Include 
PROCEDURE CloseActivity :
/*------------------------------------------------------------------------------
@description Closes the activity data server procedure
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pActivityID as integer no-undo.
  
  /* buffers */
  define buffer openActivity for openActivity.
  
  /* get the correct activity data server record */
  for first openActivity exclusive-lock
      where openActivity.activityID = pActivityID:
      
    /* delete the proceudre to the data server */
    if valid-handle(openActivity.activityHandle)
     then delete object openActivity.activityHandle.
    
    /* delete the activity data server record */
    delete openActivity.
  end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteActivity Include 
PROCEDURE DeleteActivity :
/*------------------------------------------------------------------------------
@description Deletes the activity
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pActivityID as integer no-undo.
  define output parameter pSuccess    as logical no-undo.
  
  /* buffers */
  define buffer activity for activity.
  
  /* call the server */
  run server/deleteagentactivity.p (input  pActivityID,
                                    output pSuccess,
                                    output std-ch).
                
  /* if unsuccessful, show a message */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    /* otherwise, refresh the activity */
    for first activity exclusive-lock
        where activity.activityID = pActivityID:
     
      delete activity.
      publish "ActivityDataChanged".
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FinalizeActivities Include 
PROCEDURE FinalizeActivities :
/*------------------------------------------------------------------------------
@description Finalizes the activities
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pActivityList as character no-undo.
  define output parameter pSuccess      as logical   no-undo.
  
  /* buffers */
  define buffer activity for activity.
  
  /* call the server */
  run server/finalizeagentactivities.p (input pActivityList,
                                        output pSuccess,
                                        output std-ch).
  
  /* if unsuccessful,  */
  if not pSuccess
   then message std-ch view-as alert-box warning.
   else
    do:
      for each activity exclusive-lock:
        if lookup(string(activity.activityID), pActivityList) = 0
         then next.
         
        activity.stat = "C".
      end.
      publish "ActivityDataChanged".
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetActivitys Include 
PROCEDURE GetActivities :
/*------------------------------------------------------------------------------
@description Get the activities
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pYear    as integer   no-undo.
  define input  parameter pName    as character no-undo.
  define input  parameter pAgentID as character no-undo.
  define input  parameter pStateID as character no-undo.
  define input  parameter pDecide  as integer   no-undo.
  define output parameter table for tempactivity.
  
  run LoadActivities (pYear, pName, pAgentID, pStateID, pDecide).
  
  /* assume that the criteria match and only copy those rows */
  empty temp-table tempactivity.
  std-lo = true.
  for each activity no-lock:
    if pYear > 0 and activity.year <> pYear
     then std-lo = false.
     
    if pAgentID <> "ALL" and activity.agentID <> pAgentID
     then std-lo = false.
     
    if pStateID <> "ALL" and activity.stateID <> pStateID
     then std-lo = false.
     
    if pName > "" and activity.name <> pName
     then std-lo = false.
     
    if std-lo
     then
      do:
        create tempactivity.
        buffer-copy activity to tempactivity.
      end.
  end.
  
  /* get the descriptions of fields */
  for each tempactivity exclusive-lock:
    /* set the status */
    tempactivity.statDesc = tempactivity.stat.
    publish "GetSysPropDesc" ("AMD", "Activity", "Status", tempactivity.stat, output tempactivity.statDesc).
    /* set the type */
    tempactivity.typeDesc = tempactivity.type.
    publish "GetSysPropDesc" ("AMD", "Activity", "Type", tempactivity.type, output tempactivity.typeDesc).
    /* set the category */
    tempactivity.categoryDesc = tempactivity.category.
    publish "GetSysPropDesc" ("AMD", "Activity", "Category", tempactivity.category, output tempactivity.categoryDesc).
    /* set the quarter and year numbers */
    assign
      tempactivity.qtr1    = truncate(tempactivity.month1,0) +
                             truncate(tempactivity.month2,0) +
                             truncate(tempactivity.month3,0)
      tempactivity.qtr2    = truncate(tempactivity.month4,0) +
                             truncate(tempactivity.month5,0) +
                             truncate(tempactivity.month6,0)
      tempactivity.qtr3    = truncate(tempactivity.month7,0) +
                             truncate(tempactivity.month8,0) +
                             truncate(tempactivity.month9,0)
      tempactivity.qtr4    = truncate(tempactivity.month10,0) +
                             truncate(tempactivity.month11,0) +
                             truncate(tempactivity.month12,0)
      tempactivity.yrTotal = truncate(tempactivity.month1,0) +
                             truncate(tempactivity.month2,0) +
                             truncate(tempactivity.month3,0) +
                             truncate(tempactivity.month4,0) +
                             truncate(tempactivity.month5,0) +
                             truncate(tempactivity.month6,0) +
                             truncate(tempactivity.month7,0) +
                             truncate(tempactivity.month8,0) +
                             truncate(tempactivity.month9,0) +
                             truncate(tempactivity.month10,0) +
                             truncate(tempactivity.month11,0) +
                             truncate(tempactivity.month12,0)
                             .
    /* delete the categories R and W */
    if tempactivity.category = "R" or tempactivity.category = "W"
     then delete tempactivity.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetActivity Include 
PROCEDURE GetActivity :
/*------------------------------------------------------------------------------
@description Gets an activity
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pActivityID as integer no-undo.
  define output parameter table for tempactivity.
  
  /* buffers */
  define buffer activity     for activity.
  define buffer tempactivity for tempactivity.

  /* get all the activitys */
  empty temp-table tempactivity.
  if not can-find(first activity where activityID = pActivityID)
   then refreshActivity(pActivityID).

  /* copy the activity */
  empty temp-table tempactivity.
  for first activity no-lock
      where activity.ActivityID = pActivityID:
    
    create tempactivity.
    buffer-copy activity to tempactivity.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetActivityYearList Include 
PROCEDURE GetActivityYearList :
/*------------------------------------------------------------------------------
@description Gets the activity year list
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter pList as character no-undo.
  
  /* find out if the year has been saved previously */
  if tYearList = ""
   then 
    do:
      /* call the server */
      run server/getagentactivityyears.p (output pList,
                                          output std-lo,
                                          output std-ch).

      /* if unsuccessful, show a message */
      if not std-lo
       then message std-ch view-as alert-box warning.
       else tYearList = pList.
    end.
   else pList = tYearList.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadActivitys Include 
PROCEDURE LoadActivities :
/*------------------------------------------------------------------------------
@description Loads the activities
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pYear    as integer   no-undo.
  define input  parameter pName    as character no-undo.
  define input  parameter pAgentID as character no-undo.
  define input  parameter pStateID as character no-undo.
  define input  parameter pDecide  as integer   no-undo.
     
  /* call the server */
  empty temp-table activity.
  run server/getagentactivities.p (input  0,
                                   input  pYear,
                                   input  pName,
                                   input  pAgentID,
                                   input  pStateID,
                                   input  pDecide,
                                   output table tempactivity,
                                   output std-lo,
                                   output std-ch).
  
  /* if unsuccessful, show a message if debug */
  if not std-lo
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadActivities failed: " + std-ch view-as alert-box warning.
    end.
   else
    do:
      /* copy the temp activitys to the activitys table */
      for each tempactivity:
        create activity.
        buffer-copy tempactivity to activity.
      end.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenActivity C-Win 
PROCEDURE OpenActivity :
/*------------------------------------------------------------------------------
@description Loads the agent into the agentdatasrv.p file
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pActivityID as integer no-undo.
  define output parameter pHandle as handle no-undo.
  
  /* buffers */
  define buffer openActivity for openActivity.
  
  /* if available, get the handle to the activity data server */
  if can-find(first openActivity where activityID = pActivityID)
   then 
    for first openActivity no-lock
        where openActivity.activityID = pActivityID:
        
      pHandle = openActivity.activityHandle.
    end.
   else 
    do:
      /* otherwise run the activity data server and create the record */
      run activitydatasrv.p persistent set pHandle (pActivityID).
      create openActivity.
      assign
        openActivity.activityID = pActivityID
        openActivity.activityHandle = pHandle
        .
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewActivity C-Win 
PROCEDURE NewActivity :
/*------------------------------------------------------------------------------
@description Creates a new activity
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter table for tempactivity.
  define output parameter pHandle as handle no-undo.
  
  /* variables */
  define variable tNewActivityID as integer no-undo.
  
  /* buffers */
  define buffer tempactivity for tempactivity.
  
  run server/newagentactivity.p (input table tempactivity,
                                 output tNewActivityID,
                                 output std-lo,
                                 output std-ch).
                                     
  /* if unsuccessful, show a message */
  if not std-lo
   then message std-ch view-as alert-box error.
   else
    do:
      /* otherwise, get the details of the new activity from the server */
      run OpenActivity (tNewActivityID, output pHandle).
      publish "ActivityDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshActivity Include 
FUNCTION refreshActivity RETURNS logical
  ( input pActivityID as integer ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* variables */
  define variable lSuccess as logical no-undo.
  
  /* buffers */
  define buffer tempactivity for tempactivity.
  define buffer activity for activity.
  
  /* call the server */
  empty temp-table tempactivity.
  run server/getagentactivities.p (input  pActivityID,
                                   input  0,
                                   input  "",
                                   input  "ALL",
                                   input  "ALL",
                                   input  0,
                                   output table tempactivity,
                                   output lSuccess,
                                   output std-ch).

  /* if unsuccessful, show a message if debug is on */
  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshActivity failed: " + std-ch view-as alert-box warning.
    end.
   else
    /* otherwise, copy the activity details into the activity records */
    do:
      find tempactivity where tempactivity.activityID = pActivityID no-error.
      find activity where activity.activityID = pActivityID no-error.
      if not available tempactivity and available activity /* delete */
       then delete activity.
      if available tempactivity and not available activity /* new */
       then create activity.
      if available tempactivity and available activity /* modify */
       then buffer-copy tempactivity to activity.
    end.
  
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
