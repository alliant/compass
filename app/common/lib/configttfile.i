&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/configttfile.i
   Routines related to the CONFIGuration FILE that are generally
     used in multiple applications.
   D.Sinclair
   9.18.2012
   
    lib/configttdef.i must precede this routine
              
   Parameters:
    &config      Configuration Filename (no path)
 */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD loadConfiguration Include 
FUNCTION loadConfiguration RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

subscribe to "GetConfigFile" anywhere.
subscribe to "GetSettingsFile" anywhere.

subscribe to "QUIT" anywhere.

loadConfiguration().

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetConfigFile Include 
PROCEDURE GetConfigFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pFile as char.
pFile = search("{&config}").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSettingsFile Include 
PROCEDURE GetSettingsFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pFile as char.
 
 pFile = os-getenv("appdata") + "\Alliant\{&config}".
 if search(pFile) = ?
  then 
   do: os-create-dir value(os-getenv("appdata") + "\Alliant").
       return.
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE QUIT Include 
PROCEDURE QUIT :
/*------------------------------------------------------------------------------
  Purpose:     Save the user-settable fields into the user configuration file
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 run GetSettingsFile in this-procedure (output std-ch).
 std-ha = temp-table config:handle.
 std-ha:write-xml("file", std-ch).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION loadConfiguration Include 
FUNCTION loadConfiguration RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def var tTempFile as char no-undo.

 /* Load "system" configuration */
 run GetConfigFile in this-procedure (output tTempFile).
 if search(tTempFile) = ?
  then return false. /* Installation failure. Don't try to load anything */

 std-ha = temp-table setting:handle.
 std-ha:read-xml("file", tTempFile, "empty", "", false) no-error.

 /* Load user config (unless it's the first run... */
 run GetSettingsFile in this-procedure (output tTempFile).
 if search(tTempFile) = ? 
  then return true.

 std-ha = temp-table config:handle.
 std-ha:read-xml("file", tTempFile, "empty", "", false) no-error.

 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

