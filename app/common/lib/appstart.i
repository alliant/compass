&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : lib/appstart.i
    Purpose     : Command-line STARTup routine for a gui client APPlication
    Syntax      : -param ... for configuration options

                  config        (for configuration data-source)
                  startup       (internal procedure)
                  subscribe-esb (comma-delimited list of Topics to listen)
                  timeout       (non-blank integer in seconds for the wait-for
                                 to fall-out periodically for "TIMER" event)
                  splash        (integer for number of calls to Splash counter) 

    Author(s)   : D.Sinclair

    11.24.2014 D.Sinclair  Removed extraneous timer fields and procedures.
    11.26.2014 D.Sinclair  Added splash option to set denominator for the 
                            progress bar.
    09.04.2015 J.Oliver    Added a check to see if the password has or
                            is about to expire.
  ----------------------------------------------------------------------*/

def var appRunning as logical no-undo init true.
def var appCode as char no-undo.

{lib/std-def.i}

ON "U1" of this-procedure
do:
  appRunning = false.
end.


&IF defined(splash) = 0 &then
  &SCOPED-DEFINE splash 6
&endif


/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 10.33
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

 /* Trying to avoid screen flashing...
    No good workaround according to Progress */
 default-window:hidden = true.
 default-window:window-state = 2.
 default-window:sensitive = false.
 default-window:visible = false.

 session:data-entry-return = true.

 /* Attempt to create the local directory for config/options files */
 /* Failure is behind the scenes and doesn't materially impact startup */
 os-create-dir value(os-getenv("appdata") + "\Alliant").


 /* Interface used by main application window to close.  By having the
    wait-for external to the main window, we avoid some screen flashing */
 subscribe to "ExitApplication" anywhere.

 /* Shared interface for optimization of client-side objects */
 subscribe to "AboutApplication" anywhere.

 /* Continuing the application startup */
 subscribe to "ContinueStartup" anywhere.

 /* Run the persistent routine to read config and options file(s) */
 run {&config} persistent.

 publish "GetAppCode" (output appCode).
 
 /* Prevent run of a duplicate module instance 
    (if required and AdminService is running) */
 publish "GetAppAdmin" (output std-lo).
 if std-lo
  then
   do: std-lo = false.
       publish "GetAppSingleton" (output std-lo).
       if std-lo
        then
         do: run sys/modadmmsg.p (appCode, "VIEW", output std-lo, output std-ch).
             if std-lo 
              then QUIT.
         end.
   end.

/* Save the app code and ID prior to any data calls */
publish "SetCurrentValue" ("ApplicationCode", appCode).
publish "SetCurrentValue" ("ApplicationID", guid(generate-uuid)).

/* Create the datastore to manage the credentials file and requests */
run sys/authconfig.p persistent.

/* Create the datastore to hold records of all temp files created */
run sys/tempfiledata.p persistent.

/* Optionally run the Splash to authenticate */
publish "GetAppAuthenticate" (output std-lo).
if std-lo
 then 
  do:
    publish "LoadArbingerTips".
    RUN sys/login.w persistent.
    publish "SetSplashCounter" ({&splash}).
  end.
 else 
  do:
    publish "LoadCredentials" (output std-lo).
    run ContinueStartup in this-procedure.
  end.

 /* Main blocking loop/statement for the client application.  By having it here,
    we avoid a lot of screen flashing.
  */
WAIT-BLOCK:
do while appRunning
   on stop undo, leave
   on error undo, leave
   on endkey undo, leave:

  &IF defined(timeout) = 0
   &THEN
  &scoped-define timeout 5
  &ENDIF
  
  WAIT-FOR "U1" of this-procedure pause {&timeout}.
  if appRunning 
   then publish "TIMER".
end. 

  /* Let any listening persistent routines know we're leaving so they can 
    perform any needed clean-up. */
  publish "QUIT".

  QUIT.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AboutApplication Include 
PROCEDURE AboutApplication :
/*------------------------------------------------------------------------------
  Purpose:     Handler for common "About" dialog-box
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 std-lo = false.
 publish "IsAboutOpen" (output std-lo).
 if not std-lo 
  then run sys/wabout.w persistent.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ContinueStartup Include 
PROCEDURE ContinueStartup :
/*------------------------------------------------------------------------------
  Purpose:     Continue the startup of the application
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Create the datastore for logging HTTP GET and POST requests */
  publish "SetSplashStatus".
  run sys/eventdata.p persistent.

  /* Create the datastore to manage tasks */
  publish "SetSplashStatus".
  std-lo = false.
  publish "GetAppTask" (output std-lo).
  if std-lo 
  then run sys/taskdata.p persistent.

  /* Create the AdminService capabilities (localhost:port listener) */
  publish "SetSplashStatus".
  std-lo = false.
  publish "GetAppAdmin" (output std-lo).
  if std-lo 
  then run sys/modadmsrv.p persistent.

  /* Create the MQ client connection and datastore of received messages */
  publish "SetSplashStatus".
  std-lo = false.
  publish "GetAppESB" (output std-lo).
  if std-lo 
   then run sys/esbdata.p persistent ("{&subscribe-esb}").

  /* Run the main application window and data-model (optionally) */
  publish "SetSplashStatus".
  run {&startup} in this-procedure.

  /* Optionally display various windows */
  /* Task window */
  std-lo = false.
  publish "GetAppTask" (output std-lo).
  if std-lo
   then
    do:
      std-lo = false.
      publish "GetAppTaskLaunch" (output std-lo).
      if std-lo 
       then run sys/wtasks.w persistent.
    end.

  /* Esb window */
  std-lo = false.
  publish "GetAppEsb" (output std-lo).
  if std-lo
   then
    do:
      std-lo = false.
      publish "GetAppEsbLaunch" (output std-lo).
      if std-lo 
      then run sys/wesbmsg.w persistent.
    end.

  /* Event viewer window */
  std-lo = false.
  publish "GetAppRequestLaunch" (output std-lo).
  if std-lo 
   then run sys/eventviewer.w persistent.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExitApplication Include 
PROCEDURE ExitApplication :
/*------------------------------------------------------------------------------
  Purpose:     Notification procedure for common mechanism to end wait-for
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /* Let any listening persistent routines know we're leaving so they can 
    perform any needed clean-up.
  */
  apply "U1" to this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

