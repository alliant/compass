&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file lib/brw-totalData-setup.i
@author J.Oliver
@date 08.17.2017
    
@description Used to initialize all of the variables and functions used in
             the brw-totalData.i include
------------------------------------------------------------------------*/

{lib/count-rows.i}
{lib/find-widget.i}
{lib/get-column.i}

/* variables needed for the browse total */
define variable totalCol as decimal extent no-undo.
define variable totalAdded as logical no-undo initial false.

&ANALYZE-RESUME


