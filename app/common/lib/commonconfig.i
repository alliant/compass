&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* dialogconfig.p
   Manage common configuration elements
 */

{lib/configttdef.i}
{lib/configttcur.i}
{lib/configtthost.i}
{lib/configttlogo.i}
{lib/configttapp.i}
{lib/configttrepo.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 30.57
         WIDTH              = 68.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 

/* ***************************  Main Block  *************************** */ 

{lib/confirmconfig.i}
{lib/contactconfig.i}
{lib/directoryconfig.i}
{lib/loadconfig.i}
{lib/optionconfig.i}

/* columns */
subscribe to "GetDynamicBrowseColumns"  anywhere.
subscribe to "GetDynamicBrowseDefault"  anywhere.
subscribe to "SetDynamicBrowseColumns"  anywhere.

/* liability ranges */
subscribe to "GetLiabilityDistribution" anywhere.
subscribe to "SetLiabilityDistribution" anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-GetDynamicBrowseColumns) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetDynamicBrowseColumns Procedure 
PROCEDURE GetDynamicBrowseColumns :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pWindow as character no-undo.
  define output parameter pList   as character no-undo.
  pList = getOption(pWindow).
  if pList = ""
   then pList = getSetting(pWindow).
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetDynamicBrowseDefault) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetDynamicBrowseDefault Procedure 
PROCEDURE GetDynamicBrowseDefault :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pWindow as character no-undo.
  define output parameter pList   as character no-undo.
  pList = getSetting(pWindow).
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLiabilityDistribution) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLiabilityDistribution Procedure 
PROCEDURE GetLiabilityDistribution :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-ch as character no-undo.
  std-ch = getOption("LiabilityDistribution").
  if std-ch = "" 
   then std-ch = getSetting("LiabilityDistribution").
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetDynamicBrowseColumns) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetDynamicBrowseColumns Procedure 
PROCEDURE SetDynamicBrowseColumns :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pWindow as character no-undo.
  define input parameter pList   as character no-undo.  
  setOption(pWindow, pList).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLiabilityDistribution) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLiabilityDistribution Procedure 
PROCEDURE SetLiabilityDistribution :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-ch as character no-undo.
  if std-ch = ? 
   then std-ch = "".
  setOption("LiabilityDistribution", std-ch).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

