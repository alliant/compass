/*------------------------------------------------------------------------
    File        : lib/rpt-dialogDestination.i
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : MK
    Created     : 06/03/21
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

&if defined(rpt-format) = 0 &then
&scoped-define rpt-format rpt-format
&endif

&if defined(rpt-destinationType) = 0 &then
&scoped-define rpt-destinationType rpt-destinationType
&endif

&if defined(rpt-behavior) = 0 &then
&scoped-define rpt-behavior rpt-behavior
&endif

&if defined(rpt-notifyRequestor) = 0 &then
&scoped-define rpt-notifyRequestor rpt-notifyRequestor
&endif

&if defined(rpt-destination) = 0 &then
&scoped-define rpt-destination rpt-destination 
&endif

&if defined(rpt-email) = 0 &then
&scoped-define rpt-email rpt-email
&endif

&if defined(rpt-printer) = 0 &then
&scoped-define rpt-printer rpt-printer 
&endif                                     

&if defined(rpt-repository) = 0 &then
&scoped-define rpt-repository rpt-repository
&endif

&if defined(rpt-outputFormat) = 0 &then
&scoped-define rpt-outputFormat rpt-outputFormat
&endif

&if defined(rpt-cancel) = 0 &then
&scoped-define rpt-cancel rpt-cancel
&endif

/* ***************************  Main Block  *************************** */

RUN util/rpt-dialogDestination.w(INPUT {&rpt-format},
                                 INPUT {&rpt-destinationType},
                                 OUTPUT {&rpt-behavior},
                                 OUTPUT {&rpt-notifyRequestor},
                                 OUTPUT {&rpt-destination},
                                 OUTPUT {&rpt-email},
                                 OUTPUT {&rpt-printer},
                                 OUTPUT {&rpt-repository},
                                 OUTPUT {&rpt-outputFormat},
                                 OUTPUT {&rpt-cancel}
                                 ).
                                 
&if defined(exclude-default-cancel) = 0 &then
 if {&rpt-cancel}
 then
  return.
&endif
