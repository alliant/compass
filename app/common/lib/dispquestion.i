&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/dispquestion.i
  exclude-vars: Non-blank to exclude variable definitions
  var-desc:     Variable to hold description of question (default tDesc)
  var-priority: Variable to hold priority of question (default tPriority)
  var-ans:      Variable to hold ans to question (default tAns)
  extend-desc:  If t<seq>a are defined in frame and should be used (90 chars max)
  q:            QuestionID (for example, "8.1.1"
  disp-q:       Override to separate database questionId from visual questionID
  seq:          Number of visual variable to hold results
  questionType: Leave blank for default                                                                    
  ----------------------------------------------------------------------*/

&if defined(exclude-vars) = 0 &then
def var tDesc as char.
def var tPriority as int.
def var tAns as char.
&global-define exclude-vars true
&endif

&IF defined(var-desc) = 0 &THEN
&scoped-define var-desc tDesc
&ENDIF

&IF defined(var-priority) = 0 &THEN
&scoped-define var-priority tPriority
&ENDIF

&IF defined(var-ans) = 0 &THEN
&scoped-define var-ans tAns
&ENDIF

&if defined(sectionFrame) = 0 &then
&scoped-define sectionFrame in frame fSection
&endif

&IF defined(questionType) = 0 &THEN
&scoped-define questionType Question
&ENDIF

publish "Get{&questionType}Attributes" (input "{&q}",
                                 output {&var-desc}, 
                                 output {&var-priority}).
n{&seq}:font {&sectionFrame} = 15 + {&var-priority}.
&IF defined(q-disp) = 0 &THEN
n{&seq}:screen-value {&sectionFrame} = "{&q}".
&else
n{&seq}:screen-value {&sectionFrame} = "{&q-disp}".
&ENDIF
t{&seq}:font  {&sectionFrame} = 15 + {&var-priority}.
t{&seq}:screen-value  {&sectionFrame} = (if length({&var-desc}) > 90
              then substring({&var-desc}, 1, r-index({&var-desc}, " ", 90))
              else {&var-desc})
&IF defined(extend-desc) = 0 &THEN
                                   + " " 
                                   + fill("*", {&var-priority} - 1)
&ENDIF
                                   .
&IF defined(extend-desc) <> 0 &THEN
t{&seq}a:font  {&sectionFrame} = 15 + {&var-priority}.
t{&seq}a:screen-value  {&sectionFrame} = substring({&var-desc}, r-index({&var-desc}, " ", 90) + 1) 
                                   + " " 
                                   + fill("*", {&var-priority} - 1).
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


