/*------------------------------------------------------------------------
    File        : lib/rpt-setparams.i   
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : MK
    Created     : 06/03/21
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* ***************************  Main Block  *************************** */
input rpt-behavior,
input rpt-notifyRequestor,
input rpt-destination,
input rpt-email,
input rpt-printer,
input rpt-repository,
input rpt-outputFormat
