&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* ***************************  Definitions  ************************** */

&if defined(appCode) = 0 &then
&scoped-define appCode ""
&endif

&if defined(objAction) = 0 &then
&scoped-define objAction ""
&endif

&if defined(objProperty) = 0 &then
&scoped-define objProperty ""
&endif

&if defined(addAll) = 0 &then
&scoped-define addAll false
&endif

&if defined(addFirst) = 0 &then
&scoped-define addFirst false
&endif

&if defined(firstValue) = 0 &then
&scoped-define firstValue "ALL,ALL"
&endif

&if defined(out) = 0 &then
&scoped-define out std-lo
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

if {&combo}:type = "COMBO-BOX"
 then {&combo}:inner-lines = 10.
{&combo}:list-item-pairs = entry(1, {&firstValue}) + {&combo}:delimiter + entry(2, {&firstValue}).

/* get the properties */
std-ch = "".
&if defined(minus) = 0 &then
publish "GetSysPropList" ({&appCode}, {&objAction}, {&objProperty}, output std-ch).
&else
publish "GetSysPropListMinusID" ({&appCode}, {&objAction}, {&objProperty}, {&minus}, output std-ch).
&endif
{&out} = false.
if std-ch > ""
 then 
  assign
    std-ch                   = replace(std-ch, {&msg-dlm}, {&combo}:delimiter)
    {&combo}:list-item-pairs = {&combo}:list-item-pairs + {&combo}:delimiter + std-ch
    {&out}                   = true
    .

if {&out} and not {&addAll} and not {&addFirst}
 then {&combo}:delete(1).

&if defined(d) = 0 &then
{&combo}:screen-value = entry(2, {&combo}:list-item-pairs, {&combo}:delimiter).
&else
if lookup({&d}, {&combo}:list-item-pairs, {&combo}:delimiter) > 0
 then {&combo}:screen-value = {&d}.
 else {&combo}:screen-value = entry(2, {&combo}:list-item-pairs, {&combo}:delimiter).
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


