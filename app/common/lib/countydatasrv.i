&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file commondatasrv.i
@description Data Server file for common data elements

@author John Oliver
@created 04-17-2019
@notes Remade from commondatasrv.i
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}
define variable tLoadedCounties as logical initial false no-undo.

/* tables */
{tt/county.i}
{tt/esbmsg.i &tableAlias="countyesbmsg"}

/* temp tables */
{tt/county.i &tableAlias="tempcounty"}

/* functions */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshCounty Include 
FUNCTION refreshCounty RETURNS logical
  ( input pStateID as character,
    input pCountyID as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

subscribe to "CountyChanged"        anywhere.
subscribe to "DeleteCounty"         anywhere.
subscribe to "GetCounties"          anywhere.
subscribe to "GetCountyDescription" anywhere.
subscribe to "LoadCounties"         anywhere.
subscribe to "ModifyCounty"         anywhere.
subscribe to "NewCounty"            anywhere.

/* used for the messaging system */
subscribe to "TIMER"                anywhere run-procedure "CountyTimer".
subscribe to "ExternalEsbMessage"   anywhere run-procedure "CountyESB".

/* used to dump the data file */
subscribe to "DataDump"             anywhere run-procedure "CountyDataDump".

/* load counties */
std-lo = true.
publish "SetSplashStatus".
publish "GetLoadCounties" (output std-lo).
if std-lo 
 then run LoadCounties.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CountyChanged Include 
PROCEDURE CountyChanged :
/*------------------------------------------------------------------------------
@description Reloads a county
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pStateID as character no-undo.
  define input parameter pCountyID as character no-undo.
  
  /* refresh the data */
  refreshCounty(pStateID, pCountyID).
  
  /* publish that the data changed */
  publish "CountyDataChanged".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CountyDataDump Include 
PROCEDURE CountyDataDump :
/*------------------------------------------------------------------------------
@description Dump the county and tempcounty temp-tables into files
------------------------------------------------------------------------------*/
  /* variables */
  define variable tPrefix as character no-undo.
  
  /* get the prefix */
  publish "GetAppCode" (output tPrefix).
  tPrefix = tPrefix + "_".
  
  /* delete the current temp files */
  publish "DeleteTempFile" ("DataCounty", output std-lo).
  publish "DeleteTempFile" ("DataTempCounty", output std-lo).
  
  /* Active temp-tables */
  temp-table county:write-xml("file", tPrefix + "County.xml").
  publish "AddTempFile" ("DataCounty", tPrefix + "County.xml").
  
  /* "Temp" temp-tables */
  temp-table tempcounty:write-xml("file", tPrefix + "TempCounty.xml").
  publish "AddTempFile" ("DataCounty", tPrefix + "TempCounty.xml").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CountyESB Procedure 
PROCEDURE CountyESB :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey as character no-undo.
  
  /* exit if not correct entity */
  if pEntity <> "County"
   then next.
  
  /* get the next number */
  std-in = 1.
  for each countyesbmsg no-lock:
    std-in = std-in + 1.
  end.
  
  /* create the esb message */
  create countyesbmsg.
  assign
    countyesbmsg.seq    = std-in
    countyesbmsg.rcvd   = now
    countyesbmsg.entity = pEntity
    countyesbmsg.action = pAction
    countyesbmsg.keyID  = pKey
    .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CountyTimer Procedure 
PROCEDURE CountyTimer :
/*------------------------------------------------------------------------------
  Purpose:     Called when the application is idle
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* buffers */
  define buffer countyesbmsg for countyesbmsg.

  /* loop through the esb messages */
  for each countyesbmsg exclusive-lock
  break by countyesbmsg.keyID
        by countyesbmsg.rcvd descending:

    /* if the agent is loaded and the we have a messages */
    if tLoadedCounties and first-of(countyesbmsg.keyID) 
     then run CountyChanged (entry(1, countyesbmsg.keyID, {&id-dlm}), entry(2, countyesbmsg.keyID, {&id-dlm})).
     
    /* delete the esb message and set the last sync */
    delete countyesbmsg.
    publish "SetLastSync".
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteCounty Include 
PROCEDURE DeleteCounty :
/*------------------------------------------------------------------------------
@description Delete a county
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pStateID  as character no-undo.
  define input  parameter pCountyID as character no-undo.
  define output parameter pSuccess  as logical no-undo initial false.
  
  /* buffers */
  define buffer county for county.
  
  /* call the server */
  run server/deletecounty.p (input pStateID,
                             input pCountyID,
                             output pSuccess,
                             output std-ch).

  /* if unsuccessful, show a message */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    /* otherwise, delete the county */
    for first county exclusive-lock
        where county.stateID = pStateID
          and county.countyID = pCountyID:
     
      delete county.
      publish "CountyDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCountyDescription Include 
PROCEDURE GetCountyDescription :
/*------------------------------------------------------------------------------
@description Gets the county description
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pCountyID   as character no-undo.
  define output parameter pCountyDesc as character no-undo initial "Unknown".

  /* get all the counties */
  empty temp-table tempcounty.
  run GetCounties (output table tempcounty).
  
  /* get the description for the county */
  define buffer county for county.
  for first county no-lock
      where county.countyID = pCountyID:
      
    pCountyDesc = county.description.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCounties Include 
PROCEDURE GetCounties :
/*------------------------------------------------------------------------------
@description Gets the counties
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for county.

  /* if not done, load the counties */
  if not tLoadedCounties 
   then run LoadCounties.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadCounties Include 
PROCEDURE LoadCounties :
/*------------------------------------------------------------------------------
@description Loads the counties
------------------------------------------------------------------------------*/  
  /* call the server */
  empty temp-table county.  
  run server/getcounties.p (input  "", /* stateID, blank=All */
                            input  "", /* countyID, blank=All */
                            output table county,
                            output tLoadedCounties,
                            output std-ch).
                            
  /* if unsuccessful, show a message if debug is on */
  if not tLoadedCounties
   then 
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadCounties failed: " + std-ch view-as alert-box warning.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyCounty Include 
PROCEDURE ModifyCounty :
/*------------------------------------------------------------------------------
@description Modifies a county
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pStateID     as character no-undo.
  define input  parameter pCountyID    as character no-undo.
  define input  parameter pDescription as character no-undo.
  define output parameter pSuccess     as logical no-undo initial false.
  
  /* buffers */
  define buffer county for county.
  
  /* call the server */
  run server/modifycounty.p (input pStateID,
                             input pCountyID,
                             input pDescription,
                             output pSuccess,
                             output std-ch).

  /* if unsuccessful, show a message */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    /* otherwise, set the county description */
    for first county exclusive-lock
        where county.stateID = pStateID
          and county.countyID = pCountyID:
      
      county.description = pDescription.
      publish "CountyDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewCounty Include 
PROCEDURE NewCounty :
/*------------------------------------------------------------------------------
@description Creates a new county
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pStateID     as character no-undo.
  define input  parameter pCountyID    as character no-undo.
  define input  parameter pDescription as character no-undo.
  define output parameter pSuccess     as logical no-undo initial false.

  /* call the server */
  run server/newcounty.p (input pStateID,
                          input pCountyID,
                          input pDescription,
                          output pSuccess,
                          output std-ch).

  /* if unsuccessful, show a message if debug is on */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    do:
      /* otherwise, create a new county */
      create county.
      assign
        county.stateID = pStateID
        county.countyID = pCountyID
        county.description = pDescription
        .
      publish "CountyDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshCounty Include 
FUNCTION refreshCounty RETURNS logical
  ( input pStateID as character,
    input pCountyID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* variables */
  define variable lSuccess as logical no-undo.
  
  /* buffers */
  define buffer tempcounty for tempcounty.
  define buffer county for county.
  
  /* call the server */
  empty temp-table tempcounty.
  run server/getcounties.p (pStateID,
                            pCountyID,
                            output table tempcounty,
                            output lSuccess,
                            output std-ch).

  /* if unsuccessful, show a message if debug is on */
  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshSysCode failed: " + std-ch view-as alert-box warning.
    end.
   else
    /* otherwise, copy the county details into the county records */
    do:
      find tempcounty where tempcounty.stateID = pStateID and tempcounty.countyID = pCountyID no-error.
      find county where county.stateID = pStateID and county.countyID = pCountyID no-error.
      if not available tempcounty and available county /* delete */
       then delete county.
      if available tempcounty and not available county /* new */
       then create county.
      if available tempcounty and available county /* modify */
       then buffer-copy tempcounty to county.
    end.
  
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

