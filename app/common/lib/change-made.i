&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*---------------------------------------------------------------------
@name change-made.i
@description Function, changeMade, that will detect if a change is made
             to the particular field given the table, column, and value

@author John Oliver
@version 1.0
@created 04/10/2016
@notes The assumption is that there is only one row in the table. If
       there are multiple rows, then the function will not work.
---------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD changeMade Include 
FUNCTION changeMade RETURNS LOGICAL
  ( input pNewValue as character,
    input pColumn as character,
    input pTable as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION changeMade Include 
FUNCTION changeMade RETURNS LOGICAL
  ( input pNewValue as character,
    input pColumn as character,
    input pTable as character ) :
/*------------------------------------------------------------------------------
@description Determines if a change was made between the two values
@param NewValue;character;The new value of the field
@param Column;character;The field that the value belongs to
@param Table;character;The table that the field belongs to
------------------------------------------------------------------------------*/
  define variable lDiff as logical no-undo initial false.
  define variable iCounter as int no-undo.
  define variable hQuery as handle no-undo.
  define variable hBuffer as handle no-undo.
  define variable hField as handle no-undo.

  /* query the table */
  create buffer hBuffer for table pTable.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + pTable).
  hQuery:query-open().
  hQuery:get-first().
  do iCounter = 1 to hBuffer:num-fields:
    hField = hBuffer:buffer-field(iCounter).
    if hField:name = pColumn
     then
      do:
        case hField:data-type:
         when "datetime" then pNewValue = string(datetime(pNewValue)).
         when "decimal" then pNewValue = string(decimal(pNewValue)).
        end case.
        assign
          lDiff = (trim(string(hField:buffer-value())) <> trim(pNewValue))
          iCounter = hBuffer:num-fields
          .
      end.
  end.
  hQuery:query-close().
  delete object hQuery no-error.
   
  return lDiff.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

