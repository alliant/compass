/*------------------------------------------------------------------------
    File        : ar-gettrantype.i
    Purpose     : Returns transaction type of artran records

    Syntax      :

    Description :

    Author(s)   : Rahul Sharma
    Created     : 04/15/2020
    Notes       :
    Modified
    MK    03/09/2021      "Added 'RB' tranType for refund"
    SA    06/01/2021      "Added 'VCR' and 'VPR' trantype "
    SB    07/02/2021      "Added 'W' sourceType for write-off"
  ----------------------------------------------------------------------*/
&IF DEFINED(TranType) = 0
&THEN

FUNCTION getTranType RETURNS CHARACTER
  ( cTranType as character )  FORWARD.

FUNCTION getTranType RETURNS CHARACTER
  ( cTranType as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cTranType = 'I' 
   then
    return 'Invoice'. 
  else if cTranType = 'D' 
   then
    return 'Deposit'.
  else if cTranType = 'C' 
   then
    return 'Credit'.  
  else if cTranType = 'P' 
   then
    return 'Payment'.  
  else if cTranType = 'F' 
   then
    return 'File'.  
  else if cTranType = 'A' 
   then
    return 'Apply'.  
 else if cTranType = 'RF' 
   then
    return 'Refund'. 
  else if cTranType = 'R' 
   then
    return 'Reprocess'. 
  else if cTranType = 'M' 
   then
    return 'Miscellaneous'. 
  else if cTranType = 'B' 
   then
    return 'Batch'. 
  else if cTranType = 'RB' 
   then
    return 'Refund'.
  else if cTranType = 'VCR' 
   then
    return 'Refund'.
  else if cTranType = 'VPR' 
   then
    return 'Refund'.
  else if cTranType = 'W' 
   then
    return 'Write-off'.
  else
    return cTranType.   /* Function return value. */

END FUNCTION.
&GLOBAL-DEFINE TranType true
&ENDIF
