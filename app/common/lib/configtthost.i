&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/configtthost.i
   contains routines related to the server HOST parameter that are generally
   used in multiple applications.
   D.Sinclair
   9.18.2012
 */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* Used by every call to the server (httppost.p httpget.p) */
subscribe to "GetServiceAddress" anywhere.
subscribe to "GetServiceTimeout" anywhere.
subscribe to "GetAlfrescoAddress" anywhere.
subscribe to "GetEsbAddress" anywhere.

/* Used to allow per application session override (testing) */
subscribe to "SetServiceAddress" anywhere.
subscribe to "SetServiceTimeout" anywhere.
subscribe to "SetAlfrescoAddress" anywhere.
subscribe to "SetEsbAddress" anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAlfrescoAddress Include 
PROCEDURE GetAlfrescoAddress :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pAddr as char.
 pAddr = getSetting("AlfrescoAddress").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetEsbAddress Include 
PROCEDURE GetEsbAddress :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pAddr as char.
 pAddr = getSetting("EsbAddress").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetServiceAddress Include 
PROCEDURE GetServiceAddress :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pAddr as char.
 pAddr = getSetting("ServiceAddress").

 if pAddr > "" 
  then return.

 pAddr = getSetting("ServiceProtocol").
 pAddr = pAddr + "//" + getSetting("ServiceHost").

 std-ch = getSetting("ServicePort").
 if std-ch > "" 
  then pAddr = pAddr + ":" + std-ch.

 pAddr = pAddr + "/" + getSetting("ServiceCGI").
 pAddr = pAddr + "/" + getSetting("ServiceMessenger").

 std-ch = getSetting("ServiceName").

 if std-ch > "" 
  then pAddr = pAddr + "/WService=" + std-ch.

 pAddr = pAddr + "/" + getSetting("ServiceCommand").
 setSetting("ServiceAddress", pAddr).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetServiceTimeout Include 
PROCEDURE GetServiceTimeout :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pTimeout as char.
 pTimeout = getSetting("ServiceTimeout").

 if pTimeout > "" 
  then return.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetAlfrescoAddress Include 
PROCEDURE SetAlfrescoAddress :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAddr as char.

 if pAddr = "" or pAddr = ? 
  then return.
 setSetting("AlfrescoAddress", pAddr).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetEsbAddress Include 
PROCEDURE SetEsbAddress :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAddr as char.

 if pAddr = "" or pAddr = ? 
  then return.
 setSetting("EsbAddress", pAddr).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetServiceAddress Include 
PROCEDURE SetServiceAddress :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAddr as char.

 if pAddr = "" or pAddr = ? 
  then return.
 setSetting("ServiceAddress", pAddr).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetServiceTimeout Include 
PROCEDURE SetServiceTimeout :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pTimeout as char.

 if pTimeout = "" or pTimeout = ? 
  then return.
 setSetting("ServiceTimeout", pTimeout).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

