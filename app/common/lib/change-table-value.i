&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file change-table-value.i
@description Function to change a table's value
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD changeTableValue Include 
FUNCTION changeTableValue RETURNS LOGICAL
  ( input pTable  as character,
    input pColumn as character,
    input pValue  as character,
    input pWhere  as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION changeTableValue Include 
FUNCTION changeTableValue RETURNS LOGICAL
  ( input pTable  as character,
    input pColumn as character,
    input pValue  as character,
    input pWhere  as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* variables */
  define variable pSuccess as logical   no-undo initial false.
  define variable hTable   as handle    no-undo.
  define variable hBuffer  as handle    no-undo.
  define variable hQuery   as handle    no-undo.
  define variable hField   as handle    no-undo.

  /* create buffer */
  create buffer hBuffer for table pTable.
  
  /* add the where statement if not done */
  if index(pWhere, "where") = 0
   then pWhere = "where " + pWhere.
  
  /* query the data */
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each " + pTable + " no-lock " + pWhere).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hField = hBuffer:buffer-field(pColumn).
    if valid-handle(hField)
     then
      case hField:data-type:
        when "INTEGER"   then hField:buffer-value() = integer(pValue) no-error.
        when "DECIMAL"   then hField:buffer-value() = decimal(pValue) no-error.
        when "LOGICAL"   then hField:buffer-value() = logical(pValue) no-error.
        when "DATETIME"  then hField:buffer-value() = datetime(pValue) no-error.
        when "CHARACTER" then hField:buffer-value() = pValue.
      end case.
    hQuery:get-next().
  end.
  hQuery:query-close().

  return pSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

