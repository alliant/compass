
&IF defined(validstate) = 0
 &THEN
 FUNCTION validState RETURNS LOGICAL (input pcState as char):
  return lookup(pcState, "AL,AK,AZ,AR,CA,CO,CT,DC,DE,FL,GA,HI,ID,IL,IN,IA,KS,KY,LA,ME,MD,MA,MI,MN,MS,MO,MT,NE,NV,NH,NJ,NM,NY,NC,ND,OH,OK,OR,PA,RI,SC,SD,TN,TX,UT,VT,VA,WA,WV,WI,WY") > 0.
 END FUNCTION.  /* validState */

&global-define validstate true
&ENDIF
