&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/dispsection.i
  id:                    SectionID
  exclude-section-vars:  Non-blank to avoid variable definition
  sectionScore:          Variable to hold score string
  sectionComments:       Variable to hold comments string
  score:                 Field to display result
  comments:              Field to display result
  ----------------------------------------------------------------------*/
&IF defined(exclude-section-vars) = 0 &THEN
def var tSectionComments as char.
def var tSectionScore as char.

&global-define exclude-section-vars = true
&global-define sectionComments tSectionComments
&global-define sectionScore tSectionScore
&ENDIF

&IF defined(score) = 0 &THEN
&scoped-define score tScore
&ENDIF
&IF defined(comments) = 0 &THEN
&scoped-define comments tComments
&ENDIF
&if defined(hdr-frame) = 0 &then
&scoped-define hdr-frame fMain
&endif

publish "getSectionAnswer" (input "{&id}", 
                            output {&sectionScore},
                            output {&sectionComments}).

assign
  {&score}:screen-value in frame {&hdr-frame} = {&sectionScore}
  {&comments}:screen-value in frame {&hdr-frame} = {&sectionComments}
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


