&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* ***************************  Definitions  ************************** */

{lib/find-widget.i}
{lib/add-delimiter.i}

&if defined(get-agent-filter-list-def) = 0 &then
define variable hCombo     as handle    no-undo.
define variable tComboList as character no-undo.
&global-define get-agent-filter-list-def true
&endif

&if defined(innerLines) = 0 &then
&scoped-define innerLines 20
&endif

&if defined(maxLines) = 0 &then
&scoped-define maxLines 100
&endif

&if defined(t) = 0 &then
&scoped-define t agent
&endif

&if defined(n) = 0 &then
&scoped-define n name
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

&if defined(names) <> 0 or defined(combo) <> 0 &then

&if defined(combo) <> 0 &then
{&combo}:delimiter = {&msg-dlm}.
tComboList = {&combo}:name.
&endif

&if defined(names) <> 0 &then
tComboList = {&names}.
&endif

/* create the text box */
do std-in = 1 to num-entries(tComboList):
  hCombo = GetWidgetByName(frame {&frame-name}:handle, entry(std-in, tComboList)).
  hCombo:tab-stop = false.
  hCombo:move-to-bottom().
  create fill-in std-ha assign
    frame           = hCombo:frame
    row             = hCombo:row
    column          = hCombo:column
    width           = hCombo:width
    format          = "x(500)"
    screen-value    = ""
    sensitive       = true
    read-only       = false
    visible         = true
    name            = hCombo:name + "AgentText"
    tab-stop        = true
    triggers:
      on value-changed run AgentComboFilterList in this-procedure (hCombo:name, self:screen-value).
      on return apply "return" to hCombo.
      on mouse-select-click
      do:
        if not self:read-only
         then apply "VALUE-CHANGED" to self.
      end.
      on any-key 
      do:
        do with frame {&frame-name}:
          hCombo:list-item-pairs = "ALL" + hCombo:delimiter + "ALL".
          hCombo:delete(1).           
        end.
      end.
    end triggers
    .
    
  /* create a selection list widget (if not already there) */
  if hCombo:type = "COMBO-BOX"
   then
    do:
      create selection-list std-ha assign
        frame              = hCombo:frame
        row                = hCombo:row + hCombo:height
        column             = hCombo:column
        width              = hCombo:width
        inner-lines        = 1
        scrollbar-vertical = true
        sensitive          = true
        visible            = false
        name               = hCombo:name + "AgentSelection"
        tab-stop           = false
        triggers:
          on return apply "mouse-select-click" to self.
          on mouse-select-click
          do:
            if not self:screen-value = "..."
             then run AgentComboFilterSet in this-procedure (hCombo:name, self:screen-value).
          end.
        end triggers
        .
      std-ha:delimiter = {&msg-dlm}.
      std-ha:list-item-pairs = "ALL" {&msg-add} "ALL".
      std-ha:delete(1). 
    end.
   else
    do:
      assign
        std-ha = hCombo:handle
        std-ha:row = std-ha:row + 1.3
        std-ha:inner-lines = std-ha:inner-lines - 2
        .
    end.
  /* trigger to close the agent selection list */
  &if defined(noCloseTrigger) = 0 &then
  on entry anywhere
  do:
    run AgentComboFilterClose in this-procedure (hCombo:name).
  end.
  on mouse-select-down of frame {&frame-name}
  do:
    apply "ENTRY" to self.
  end.
  &endif
end.
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _procedure AgentComboFilterClear Include 
PROCEDURE AgentComboFilterClear :
/*------------------------------------------------------------------------------
@description Clears the selected agent from the agent list
------------------------------------------------------------------------------*/
  define input parameter pCombo as character no-undo.
  
  define variable hCombo as handle no-undo.
  
  hCombo = GetWidgetByName(frame {&frame-name}:handle, pCombo).
  if not valid-handle(hCombo)
   then return.

  do with frame {&frame-name}:
    std-ha = GetWidgetByName(hCombo:frame, pCombo + "AgentText").
    if valid-handle(std-ha)
     then 
      do:
        std-ha:screen-value = "".
        apply "ENTRY" to std-ha.
      end.
    
    hCombo:list-item-pairs = "ALL" + hCombo:delimiter + "ALL".
    run AgentComboFilterClose in this-procedure (pCombo).
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _procedure AgentComboFilterClose Include 
PROCEDURE AgentComboFilterClose :
/*------------------------------------------------------------------------------
@description Closes the agent list
------------------------------------------------------------------------------*/
  define input parameter pCombo as character no-undo.
  
  define variable hText      as handle no-undo.
  define variable hSelection as handle no-undo.
  define variable hCombo     as handle no-undo.
  
  hCombo = GetWidgetByName(frame {&frame-name}:handle, pCombo).
  if not valid-handle(hCombo)
   then return.
  
  hSelection = GetWidgetByName(hCombo:frame, pCombo + "AgentSelection").
  hText = GetWidgetByName(hCombo:frame, pCombo + "AgentText").
  if valid-handle(hSelection) and valid-handle(hText)
   then
    if not self:name = pCombo + "AgentText" and not self:name = pCombo + "AgentSelection"
     then hSelection:visible = false.
     else
      do:
        if hSelection:list-item-pairs > "" and not hSelection:visible
         then hSelection:visible = true.
      end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _procedure AgentComboFilterDebug Include 
PROCEDURE AgentComboFilterDebug :
/*------------------------------------------------------------------------------
@description Messages the information
------------------------------------------------------------------------------*/
  define input parameter pCombo as character no-undo.

  define variable hText      as handle no-undo.
  define variable hSelection as handle no-undo.
  define variable hCombo     as handle no-undo.
  
  hCombo = GetWidgetByName(frame {&frame-name}:handle, pCombo).
  if not valid-handle(hCombo)
   then return.
  
  do with frame {&frame-name}:
    hText = GetWidgetByName(hCombo:frame, pCombo + "AgentText").
    hSelection = GetWidgetByName(hCombo:frame, pCombo + "AgentSelection").
    if valid-handle(hText) and valid-handle(hSelection)
     then message "Text Value: " + hText:screen-value skip
                  "Visible: " + string(hSelection:visible) skip
                  "List: " + hSelection:list-item-pairs view-as alert-box information buttons ok.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _procedure AgentComboFilterEnable Include 
PROCEDURE AgentComboFilterEnable :
/*------------------------------------------------------------------------------
@description Enables/Disables the agent fill-in
------------------------------------------------------------------------------*/
  define input parameter pCombo  as character no-undo.
  define input parameter pEnable as logical   no-undo.
  
  define variable hCombo as handle no-undo.
  
  hCombo = GetWidgetByName(frame {&frame-name}:handle, pCombo).
  if not valid-handle(hCombo)
   then return.
  
  do with frame {&frame-name}:
    std-ha = GetWidgetByName(hCombo:frame, pCombo + "AgentText").
    if valid-handle(std-ha)
     then
      do:
        std-ha:read-only = not pEnable.
        std-ha = GetWidgetByName(hCombo:frame, pCombo + "AgentSelection").
        if valid-handle(std-ha)
         then std-ha:visible = false.
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentComboFilterGetName Include 
PROCEDURE AgentComboFilterGetName :
/*------------------------------------------------------------------------------
@description Get the agent name 
------------------------------------------------------------------------------*/
  define input  parameter pCombo     as character no-undo.
  define input  parameter piAgentID  as character no-undo.
  define output parameter poAgentID  as character no-undo.
  define output parameter pAgentName as character no-undo.
  
  assign
    poAgentID  = ""
    pAgentName = ""
    .
  for first {&t} no-lock
      where {&t}.agentID = piAgentID:
    
    assign
      poAgentID  = {&t}.agentID
      pAgentName = {&t}.{&n}
      .
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _procedure AgentComboFilterHide Include 
PROCEDURE AgentComboFilterHide :
/*------------------------------------------------------------------------------
@description Hides/Shows the agent fill-in
------------------------------------------------------------------------------*/
  define input parameter pCombo as character no-undo.
  define input parameter pHide  as logical no-undo.
  
  define variable hCombo as handle no-undo.
  
  hCombo = GetWidgetByName(frame {&frame-name}:handle, pCombo).
  if not valid-handle(hCombo)
   then return.
  
  do with frame {&frame-name}:
    std-ha = GetWidgetByName(hCombo:frame, pCombo + "AgentText").
    if valid-handle(std-ha)
     then
      do:
        std-ha:visible = not pHide.
        std-ha = GetWidgetByName(hCombo:frame, pCombo + "AgentSelection").
        if valid-handle(std-ha)
         then std-ha:visible = false.
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentComboFilterList Include 
PROCEDURE AgentComboFilterList :
/*------------------------------------------------------------------------------
@description Get the agent list
------------------------------------------------------------------------------*/
  define input parameter pCombo       as character no-undo.
  define input parameter pAgentFilter as character no-undo.
  
  define buffer hBuf for {&t}.
  
  define variable cFirst     as character no-undo.
  define variable cList      as character no-undo.
  define variable iAgents    as integer   no-undo.
  define variable iFilter    as integer   no-undo.
  define variable hSelection as handle    no-undo.
  define variable hCombo     as handle    no-undo.
  
  hCombo = GetWidgetByName(frame {&frame-name}:handle, pCombo).
  if not valid-handle(hCombo)
   then return.

  do with frame {&frame-name}:
    std-lo = hCombo:type = "COMBO-BOX".
    if std-lo
     then hSelection = GetWidgetByName(hCombo:frame, pCombo + "AgentSelection").
     else hSelection = hCombo:handle.
    
    if valid-handle(hSelection)
     then
      do:
        assign
          hSelection:visible = true
          cList        = ""
          cFirst       = "ALL" + hSelection:delimiter + "ALL"
          iAgents      = 1
          .
          
        for each hBuf no-lock
              by hBuf.{&n}:
          
          if pAgentFilter > "" and not (hBuf.{&n} matches "*" + pAgentFilter + "*" or hBuf.agentID begins pAgentFilter)
           then next.
          
          if lookup(hBuf.agentID, cList, hCombo:delimiter) > 0
           then next.
          
          if hBuf.{&n} = "Unknown Prospects"
           then next.
          
          &if defined(state) <> 0 &then
          if {&state}:screen-value <> "ALL" and {&state}:screen-value <> "Unknown" and {&state}:screen-value <> hBuf.stateID
           then next.
          &endif
          
          &if defined(stat) <> 0 &then
          if {&stat}:screen-value <> "ALL" and {&stat}:screen-value <> hBuf.stat
           then next.
          &endif
          
          &if defined(manager) <> 0 &then
          if {&manager}:screen-value <> "ALL" and {&manager}:screen-value <> hBuf.manager
           then next.
          &endif
          
          &if defined(region) <> 0 &then
          if {&region}:screen-value <> "ALL" and {&region}:screen-value <> hBuf.regionID
           then next.
          &endif
          
          iAgents = iAgents + 1.
          if iAgents > {&maxLines} - 2
           then next.
          
          cList = addDelimiter(cList, hSelection:delimiter) + hBuf.{&n} + " (" + hBuf.agentID + ")" + hSelection:delimiter + hBuf.agentID.
        end.

        /* add the ... option */
        if iAgents > {&maxLines} - 2
         then
          do:
            cList = addDelimiter(cList, hSelection:delimiter) + "..." + hSelection:delimiter + "...".
            if hCombo:type = "COMBO-BOX"
             then hSelection:inner-lines = minimum(iAgents, minimum({&innerLines}, integer(hCombo:frame:height - hCombo:row - hCombo:height))).
          end.
         else
          do:
            if hCombo:type = "COMBO-BOX"
             then
              do:
                hSelection:inner-lines = minimum(iAgents, minimum({&innerLines}, integer(hCombo:frame:height - hCombo:row - hCombo:height))).
                if iAgents = 0
                 then hSelection:visible = false.
              end.
          end.

        /* add the ALL option */
        if cList > ""
         then cList = cFirst + hSelection:delimiter + cList.
         else cList = cFirst.
         
        if cList > ""
         then hSelection:list-item-pairs = cList.
      end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentComboFilterResize Include 
PROCEDURE AgentComboFilterResize :
/*------------------------------------------------------------------------------
@description The user resizes the agent selection
------------------------------------------------------------------------------*/
  define input parameter pCombo as character no-undo.
  
  define variable hText      as handle no-undo.
  define variable hSelection as handle no-undo.
  define variable hCombo     as handle    no-undo.
  
  hCombo = GetWidgetByName(frame {&frame-name}:handle, pCombo).
  if not valid-handle(hCombo)
   then return.

  do with frame {&frame-name}:
    hText = GetWidgetByName(hCombo:frame, pCombo + "AgentText").
    hSelection = GetWidgetByName(hCombo:frame, pCombo + "AgentSelection").
    if valid-handle(hText)
     then
      do:
        assign
          hText:row    = hCombo:row
          hText:column = hCombo:column
          hText:width  = hCombo:width
          .
        hText:move-to-top().
      end.
    if valid-handle(hSelection)
     then
      assign
        hSelection:row    = hCombo:row + hCombo:height
        hSelection:column = hCombo:column
        hSelection:width  = hCombo:width
        .
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentComboFilterSet Include 
PROCEDURE AgentComboFilterSet :
/*------------------------------------------------------------------------------
@description Set an agent in both the drop down and text box
------------------------------------------------------------------------------*/
  define input parameter pCombo   as character no-undo.
  define input parameter pAgentID as character no-undo.
  
  define variable cAgentID   as character no-undo.
  define variable cAgentName as character no-undo.
  define variable lFound     as logical no-undo initial false.
  define variable hCombo     as handle    no-undo.
  
  hCombo = GetWidgetByName(frame {&frame-name}:handle, pCombo).
  if not valid-handle(hCombo)
   then return.
  
  do with frame {&frame-name}:
    hCombo:list-item-pairs = "ALL" + hCombo:delimiter + "ALL".
    for first {&t} no-lock
        where {&t}.agentID = pAgentID:
      
      assign
        lFound = true
        cAgentID = {&t}.agentID
        cAgentName = {&t}.{&n}
        .
    end.
    /* set the main combo */
    if lFound
     then
      assign
        hCombo:list-item-pairs = cAgentName + hCombo:delimiter + cAgentID
        hCombo:screen-value = cAgentID
        .
     else hCombo:screen-value = "ALL".
    
    /* the textbox */
    std-ha = GetWidgetByName(hCombo:frame, pCombo + "AgentText").
    if valid-handle(std-ha)
     then std-ha:screen-value = (if lFound then cAgentName + " (" + cAgentID + ")" else "ALL").
    
    /* the selection list */
    std-ha = GetWidgetByName(hCombo:frame, pCombo + "AgentSelection").
    if valid-handle(std-ha)
     then std-ha:visible = false.
    
    &if defined(noApply) = 0 &then
    if pAgentID > ""
     then apply "VALUE-CHANGED" to hCombo.
    &endif
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentComboFilterState Include 
PROCEDURE AgentComboFilterState :
/*------------------------------------------------------------------------------
@description The user selected a new state
------------------------------------------------------------------------------*/
  define input parameter pCombo as character no-undo.
  
  define variable hText      as handle no-undo.
  define variable hSelection as handle no-undo.
  define variable hCombo     as handle no-undo.
  
  hCombo = GetWidgetByName(frame {&frame-name}:handle, pCombo).
  if not valid-handle(hCombo)
   then return.

  do with frame {&frame-name}:
    hText = GetWidgetByName(hCombo:frame, pCombo + "AgentText").
    hSelection = GetWidgetByName(hCombo:frame, pCombo + "AgentSelection").
    if valid-handle(hText)
     then
      do:
        run AgentComboFilterClear in this-procedure.
        hText:screen-value = "".
        hSelection:visible = false.
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

