&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* ***************************  Definitions  ************************** */

{tt/sysuser.i}

&if defined(firstOption) = 0 &then
&scoped-define firstOption "ALL"
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

if {&combo}:type = "COMBO-BOX"
 then {&combo}:inner-lines = 10.

{&combo}:list-item-pairs = {&firstOption} + {&combo}:delimiter + {&firstOption}.

/* get the properties */
std-ch = "".
publish "GetClmUsers" (output table sysuser).
std-lo = false.
for each sysuser no-lock: 
  {&combo}:add-last(sysuser.name, sysuser.uid).
  std-lo = true.
end.

if not std-lo
 then {&combo}:list-item-pairs = "None" + {&combo}:delimiter + "None".

&if defined(d) = 0 &then
{&combo}:screen-value = entry(2, {&combo}:list-item-pairs, {&combo}:delimiter).
&else
if lookup({&d}, {&combo}:list-item-pairs) > 0
 then {&combo}:screen-value = {&d}.
 else {&combo}:screen-value = entry(2, {&combo}:list-item-pairs, {&combo}:delimiter).
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


