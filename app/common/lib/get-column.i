&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file get-column.i
@description Retrieves the handle for a particular column in a browse widget
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getColumn Include 
FUNCTION getColumn RETURNS HANDLE
  ( INPUT pBrowse AS HANDLE,
    INPUT pColName AS CHARACTER  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&if defined(getColumn-defined) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getColumn Include 
FUNCTION getColumn RETURNS HANDLE
  ( INPUT pBrowse AS HANDLE,
    INPUT pColName AS CHARACTER  ) :
/*------------------------------------------------------------------------------
@description Loops through the all the columns of the browse to get the column handle
             given the corresponding column in the underlying table
------------------------------------------------------------------------------*/
  def var hColumn as handle no-undo.
  def var lFound as logical no-undo init false.
  def var iCounter as int no-undo init 1.
    
  if valid-handle(pBrowse)
   then
    do:
      hColumn = pBrowse:first-column.
      repeat while valid-handle(hColumn) and not lFound:
        if index(hColumn:name,pColName) > 0
         then lFound = true.
         else hColumn = hColumn:next-column.
      end.
    end.
  
  if not lFound
   then hColumn = ?.
  
  return hColumn.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&global-define getColumn-defined
&endif

