&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file find-widget.i
@description Functions to find a widget in the current window
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GetWidgetBeginName Include 
FUNCTION GetWidgetBeginName RETURNS WIDGET-HANDLE
  ( input pParent  as widget-handle,
    input pSibling as widget-handle,
    input pName    as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GetWidgetByName Include 
FUNCTION GetWidgetByName RETURNS WIDGET-HANDLE
  ( input pParent as widget-handle,
    input pName   as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GetWidgetByType Include 
FUNCTION GetWidgetByType RETURNS WIDGET-HANDLE
  ( input pParent as widget-handle,
    input pSibling as widget-handle,
    input pType   as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&if defined(find-widget-def) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GetWidgetBeginName Include 
FUNCTION GetWidgetBeginName RETURNS WIDGET-HANDLE
  ( input pParent  as widget-handle,
    input pSibling as widget-handle,
    input pName    as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable hScanWidget  as widget-handle no-undo.
  define variable hChildWidget as widget-handle no-undo.

  if valid-handle(pParent)
   then
    if valid-handle(pSibling)
     then hScanWidget = pSibling:next-sibling.
     else hScanWidget = pParent:first-child.

  do while valid-handle(hScanWidget):
    if hScanWidget:name begins pName
     then return hScanWidget.

    if hScanWidget:type = "WINDOW" or hScanWidget:type = "FRAME" or hScanWidget:type = "FIELD-GROUP"
     then
      do:
        hChildWidget = GetWidgetBeginName(hScanWidget, pSibling, pName).
        if valid-handle(hChildWidget)
         then return hChildWidget.
      end.

    hScanWidget = hScanWidget:next-sibling.
  end.

  RETURN ?.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GetWidgetByName Include 
FUNCTION GetWidgetByName RETURNS WIDGET-HANDLE
  ( input pParent as widget-handle,
    input pName   as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable hScanWidget  as widget-handle no-undo.
  define variable hChildWidget as widget-handle no-undo.

  if valid-handle(pParent)
   then hScanWidget = pParent:first-child.

  do while valid-handle(hScanWidget):     
    if hScanWidget:name = pName
     then return hScanWidget.

    if hScanWidget:type = "WINDOW" or hScanWidget:type = "FRAME" or hScanWidget:type = "FIELD-GROUP"
     then
      do:
        hChildWidget = GetWidgetByName(hScanWidget, pName).         
        if valid-handle(hChildWidget)
         then return hChildWidget.
      end.

    hScanWidget = hScanWidget:next-sibling.
  end.

  RETURN ?.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GetWidgetByType Include 
FUNCTION GetWidgetByType RETURNS WIDGET-HANDLE
  ( input pParent  as widget-handle,
    input pSibling as widget-handle,
    input pType    as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable hScanWidget  as widget-handle no-undo.
  define variable hChildWidget as widget-handle no-undo.

  if valid-handle(pParent)
   then
    if valid-handle(pSibling)
     then hScanWidget = pSibling:next-sibling.
     else hScanWidget = pParent:first-child.

  do while valid-handle(hScanWidget):
    if hScanWidget:type = pType
     then return hScanWidget.

    if hScanWidget:type = "WINDOW" or hScanWidget:type = "FRAME" or hScanWidget:type = "FIELD-GROUP"
     then
      do:
        hChildWidget = GetWidgetByType(hScanWidget, pSibling, pType).
        if valid-handle(hChildWidget)
         then return hChildWidget.
      end.

    hScanWidget = hScanWidget:next-sibling.
  end.

  RETURN ?.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&global-define find-widget-def true
&endif

