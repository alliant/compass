&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file directoryconfig.i
@description Procedures to get and set the diretory configurations
  ----------------------------------------------------------------------*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 30.57
         WIDTH              = 68.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include


/* ***************************  Main Block  *************************** */ 

subscribe to "GetTempDir"   anywhere.
subscribe to "SetTempDir"   anywhere.

subscribe to "GetReportDir" anywhere.
subscribe to "SetReportDir" anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-GetReportDir) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetReportDir Procedure 
PROCEDURE GetReportDir :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-ch as character no-undo.
  std-ch = getOption("ReportDir").
  if std-ch = ""
   then std-ch = getSetting("ReportDir").
  
  if std-ch = "" 
   then
    do:
      std-ch = os-getenv("TMP").
      run SetReportDir in this-procedure (std-ch).
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetTempDir) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetTempDir Include 
PROCEDURE GetTempDir :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-ch as character no-undo.
  std-ch = getOption("TemporaryDirectory").
  
  if std-ch = "" 
   then
    do:
      std-ch = os-getenv("TMP").
      run SetTempDir (std-ch).
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetReportDir) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetReportDir Procedure 
PROCEDURE SetReportDir :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-ch as character no-undo.
  
  file-info:file-name = std-ch.
  if file-info:full-pathname = ?
    or index(file-info:file-type, "D") = 0 
    or index(file-info:file-type, "W") = 0
   then return.
  
  std-ch = file-info:full-pathname.
  if substring(std-ch, length(std-ch), 1) <> "\" 
   then std-ch = std-ch + "\".
  
  setOption("ReportDir", std-ch).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetTempDir) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetTempDir Include 
PROCEDURE SetTempDir :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-ch as character no-undo.
  
  file-info:file-name = std-ch.
  if file-info:full-pathname = ?
    or index(file-info:file-type, "D") = 0 
    or index(file-info:file-type, "W") = 0
   then return.
  
  std-ch = file-info:full-pathname.
  if substring(std-ch, length(std-ch), 1) <> "\" 
   then std-ch = std-ch + "\".
  
  setOption("TemporaryDirectory", std-ch).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

