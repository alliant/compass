&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@modified
Date          Name      Description
03/30/2022    Shefali   Task# 86699 Modified to get attorneys on the basis of attorneyType parameter.
  ----------------------------------------------------------------------*/
  
/* ***************************  Definitions  ************************** */

{lib/find-widget.i}

&if defined(addAll) = 0 &then
&scoped-define addAll false
&endif

&if defined(setEnable) = 0 &then
&scoped-define setEnable true
&endif

&if defined(innerLines) = 0 &then
&scoped-define innerLines 20
&endif

&if defined(maxLines) = 0 &then
&scoped-define maxLines 100
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

&if defined(combo) <> 0 &then
{&combo}:delimiter = {&msg-dlm}.
&if defined(noPublish) = 0 &then
publish "GetAttorneys" (output table attorney).
&endif
/* create the text box */
create fill-in std-ha assign
  frame           = {&combo}:frame
  row             = {&combo}:row
  column          = {&combo}:column
  width           = {&combo}:width
  format          = "x(500)"
  screen-value    = ""
  sensitive       = true
  read-only       = not {&setEnable}
  visible         = true
  name            = "{&combo}AttorneyText"
  triggers:
    on value-changed run AttorneyComboList in this-procedure (self:screen-value).
    on return apply "return" to {&combo} in frame {&frame-name}.
    on mouse-select-click
    do:
      if not self:read-only
       then apply "VALUE-CHANGED" to self.
    end.
    on any-key 
    do:
      do with frame {&frame-name}:
        {&combo}:list-item-pairs = "ALL" + {&combo}:delimiter + "ALL".
        {&combo}:delete(1).
      end.
    end.
  end triggers
  .
/* create a selection list widget (if not already there) */
if {&combo}:type = "COMBO-BOX"
 then
  do:
    create selection-list std-ha assign
      frame              = {&combo}:frame
      row                = {&combo}:row + {&combo}:height
      column             = {&combo}:column
      width              = {&combo}:width
      inner-lines        = 1
      scrollbar-vertical = true
      sensitive          = true
      visible            = false
      name               = "{&combo}AttorneySelection"
      triggers:
        on return apply "mouse-select-click" to self.
        on mouse-select-click
        do:
          if not self:screen-value = "..."
           then run AttorneyComboSet in this-procedure (self:screen-value).
        end.
      end triggers
      .
    std-ha:delimiter = {&msg-dlm}.
    std-ha:list-item-pairs = "ALL" {&msg-add} "ALL".
    std-ha:delete(1).
  end.
 else
  do:
    assign
      std-ha = {&combo}:handle
      std-ha:row = std-ha:row + 1.3
      std-ha:inner-lines = std-ha:inner-lines - 2
      .
  end.
/* trigger to close the agent selection list */
on entry anywhere
do:
  define variable hSelection as handle no-undo.
  define variable hText as handle no-undo.
  
  hSelection = GetWidgetByName({&combo}:frame in frame {&frame-name}, "{&combo}AttorneySelection").
  hText = GetWidgetByName({&combo}:frame in frame {&frame-name}, "{&combo}AttorneyText").
  if valid-handle(hSelection) and valid-handle(hText)
   then
    if not self:name = "{&combo}AttorneyText" and not self:name = "{&combo}AttorneySelection"
     then hSelection:visible = false.
     else
      do:
        if hSelection:list-item-pairs > "" and not hSelection:visible
         then hSelection:visible = true.
      end.
end.
on mouse-select-down of frame {&frame-name}
do:
  apply "ENTRY" to self.
end.
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _procedure AttorneyComboClear Include 
PROCEDURE AttorneyComboClear :
/*------------------------------------------------------------------------------
@description Clears the selected agent from the agent list
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    std-ha = GetWidgetByName({&combo}:frame, "{&combo}AttorneyText").
    if valid-handle(std-ha)
     then std-ha:screen-value = "".
    
    {&combo}:list-item-pairs = "ALL" + {&combo}:delimiter + "ALL".
    {&combo}:delete(1).
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _procedure AttorneyComboDebug Include 
PROCEDURE AttorneyComboDebug :
/*------------------------------------------------------------------------------
@description Messages the information
------------------------------------------------------------------------------*/
  define variable hText as handle no-undo.
  define variable hSelection as handle no-undo.
  
  do with frame {&frame-name}:
    hText = GetWidgetByName({&combo}:frame, "{&combo}AttorneyText").
    hSelection = GetWidgetByName({&combo}:frame, "{&combo}AttorneySelection").
    if valid-handle(hText) and valid-handle(hSelection)
     then message "Text Value: " + hText:screen-value skip
                  "Visible: " + string(hSelection:visible) skip
                  "List: " + hSelection:list-item-pairs view-as alert-box information buttons ok.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _procedure AttorneyComboEnable Include 
PROCEDURE AttorneyComboEnable :
/*------------------------------------------------------------------------------
@description Enables/Disables the agent fill-in
------------------------------------------------------------------------------*/
  define input parameter pEnable as logical no-undo.
  
  do with frame {&frame-name}:
    std-ha = GetWidgetByName({&combo}:frame, "{&combo}AttorneyText").
    if valid-handle(std-ha)
     then
      do:
        std-ha:read-only = not pEnable.
        std-ha = GetWidgetByName({&combo}:frame, "{&combo}AttorneySelection").
        if valid-handle(std-ha)
         then std-ha:visible = false.
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _procedure AttorneyComboHide Include 
PROCEDURE AttorneyComboHide :
/*------------------------------------------------------------------------------
@description Hides/Shows the agent fill-in
------------------------------------------------------------------------------*/
  define input parameter pHide as logical no-undo.
  
  do with frame {&frame-name}:
    std-ha = GetWidgetByName({&combo}:frame, "{&combo}AttorneyText").
    if valid-handle(std-ha)
     then
      do:
        std-ha:visible = not pHide.
        std-ha = GetWidgetByName({&combo}:frame, "{&combo}AttorneySelection").
        if valid-handle(std-ha)
         then std-ha:visible = false.
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AttorneyComboList Include 
PROCEDURE AttorneyComboList :
/*------------------------------------------------------------------------------
@description Get the agent list
------------------------------------------------------------------------------*/
  define input parameter pAgentFilter as character no-undo.
  define variable iAgents as integer no-undo.
  define variable iFilter as integer no-undo.
  define variable hBox as handle no-undo.

  do with frame {&frame-name}:
    std-lo = {&combo}:type = "COMBO-BOX".
    if std-lo
     then hBox = GetWidgetByName({&combo}:frame, "{&combo}AttorneySelection").
     else hBox = {&combo}:handle.
    
    if valid-handle(hBox)
     then
      do:
        assign
          hBox:visible = true
          hBox:list-item-pairs = "ALL" + hBox:delimiter + "ALL"
          iAgents = (if {&addAll} then 1 else 0)
          .
        
        for each attorney no-lock
              by attorney.name1:
          
          if pAgentFilter > "" and not attorney.name1 matches "*" + pAgentFilter + "*"
           then next.
          
          &if defined(attorneyType) <> 0 &then
           if {&attorneyType}:screen-value = {&OrganizationCode} then
            do:
              if attorney.personID ne ""
               then next.
            end.
           else if {&attorneyType}:screen-value = {&PersonCode} then
            do:
              if attorney.orgID ne ""
               then next.
            end.
          &endif
          
          &if defined(state) <> 0 &then
          if {&state}:screen-value <> "ALL" and lookup(attorney.stateID, {&state}:screen-value) = 0 /* as stateList field is no longer present, replacing it with stateID */
           then next.
          &endif
          
          &if defined(stat) <> 0 &then
          if {&stat}:screen-value <> "ALL" and {&stat}:screen-value <> attorney.stat
           then next.
          &endif
          
          iAgents = iAgents + 1.
          if iAgents > {&maxLines} - (if {&addAll} then 2 else 1)
           then next.
          
          hBox:add-last( if attorney.name1 ne "" then attorney.name1 else attorney.firmname + " (" + attorney.attorneyID + ")", attorney.attorneyID).
        end.

        /* add the ... option */
        if iAgents > {&maxLines} - (if {&addAll} then 2 else 1)
         then
          do:
            hBox:add-last("...","...").
            if {&combo}:type = "COMBO-BOX"
             then hBox:inner-lines = minimum(iAgents, minimum({&innerLines}, integer({&combo}:frame:height - {&combo}:row))).
          end.
         else
          do:
            if {&combo}:type = "COMBO-BOX"
             then
              do:
                hBox:inner-lines = minimum(iAgents, minimum({&innerLines}, integer({&combo}:frame:height - {&combo}:row))).
                if iAgents = 0
                 then hBox:visible = false.
              end.
          end.

        /* delete the ALL option */
        if not {&addAll}
         then hBox:delete(1).
      end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AttorneyComboSet Include 
PROCEDURE AttorneyComboSet :
/*------------------------------------------------------------------------------
@description Set an agent in both the drop down and text box
------------------------------------------------------------------------------*/
  define input parameter pAttorneyID as character no-undo.
  define variable cAttorneyID as character no-undo.
  define variable cAttorneyName as character no-undo.
  define variable lFound as logical no-undo initial false.
  
  do with frame {&frame-name}:
    {&combo}:list-item-pairs = "ALL" + {&combo}:delimiter + "ALL".
    for first attorney no-lock
        where attorney.attorneyID = pAttorneyID:
      
      assign
        lFound = true
        cAttorneyID = attorney.attorneyID
        cAttorneyName = if attorney.name1 ne "" then attorney.name1 else attorney.firmname.
        .
    end.
    /* set the main combo */
    if lFound
     then
      assign
        {&combo}:list-item-pairs = cAttorneyName + {&combo}:delimiter + cAttorneyID
        {&combo}:screen-value = cAttorneyID
        .
     else {&combo}:screen-value = "ALL".
    
    /* the textbox */
    std-ha = GetWidgetByName({&combo}:frame, "{&combo}AttorneyText").
    if valid-handle(std-ha)
     then std-ha:screen-value = (if lFound then cAttorneyName + " (" + cAttorneyID + ")" else (if {&addAll} then "ALL" else std-ha:screen-value)).
    
    /* the selection list */
    std-ha = GetWidgetByName({&combo}:frame, "{&combo}AttorneySelection").
    if valid-handle(std-ha)
     then std-ha:visible = false.
    
    &if defined(noApply) = 0 &then
    if pAttorneyID > ""
     then apply "VALUE-CHANGED" to {&combo}.
    &endif
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _procedure AttorneyComboState Include 
PROCEDURE AttorneyComboState :
/*------------------------------------------------------------------------------
@description The user selected a new state
------------------------------------------------------------------------------*/
  define variable hText as handle no-undo.
  define variable hSelection as handle no-undo.

  do with frame {&frame-name}:
    hText = GetWidgetByName({&combo}:frame, "{&combo}AttorneyText").
    hSelection = GetWidgetByName({&combo}:frame, "{&combo}AttorneySelection").
    if valid-handle(hText)
     then
      do:
        run AttorneyComboClear in this-procedure.
        hText:screen-value = "".
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

