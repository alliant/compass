&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/rcm-std-def.i
   Anjly Chanana
   08-08-2018
   Scratch variables used in many routines of rcm application.
   Modification :
   Date          Name        Comments
   10/25/2021    Shubham     Task#85099 - Added "WI" in "StateWithScenarios"
                             and set "500000" in "WisconsinJuniorMaxLimit".
   01/24/2021    Vignesh     Task#88826 - Added "UT" in "StateWithScenarios"
                             and added the Maximum amounts for UT state. 
   09/15/2022    Chandu      Task#92602 - Added "NV" in "StateWithScenarios"
                             and added the Maximun amounts for NV state. 
   09/14/2023    SChandu     Task#107100 - Added "NM" in "StateWithScenarios"
                             and added the Maximun amounts for NM state.
  ----------------------------------------------------------------------*/
&IF defined(rcm-std-def-defined) = 0 &THEN

  /*States*/
  &GLOBAL-DEFINE Alabama                "AL"
  &GLOBAL-DEFINE Georgia                "GA"
  &GLOBAL-DEFINE GeorgiaID              "GA"
  &GLOBAL-DEFINE Colorado               "CO"
  &GLOBAL-DEFINE SouthCarolina          "SC"
  &GLOBAL-DEFINE Minnesota              "MN"
  &GLOBAL-DEFINE Mississippi            "MS"
  &GLOBAL-DEFINE Missouri               "MO"
  &GLOBAL-DEFINE Florida                "FL"
  &GLOBAL-DEFINE Kansas                 "KS"
  &GLOBAL-DEFINE Texas                  "TX"
  &GLOBAL-DEFINE Wisconsin              "WI"
  &GLOBAL-DEFINE Utah                   "UT"
  &GLOBAL-DEFINE Nevada                 "NV"  
  &GLOBAL-DEFINE NewMexico              "NM"
 /*Rate Calulator UI default applicable to all states*/
  &GLOBAL-DEFINE NoRatetype             "none"                                   
  &GLOBAL-DEFINE none                   "none"
  &GLOBAL-DEFINE SelectType             "--Select Type--"
  &GLOBAL-DEFINE NaUI                   "NA"
  &GLOBAL-DEFINE DefaultPropertyType    "R"
  &GLOBAL-DEFINE null                   "null"
  &GLOBAL-DEFINE all                    "ALL"
  
  /* UseCase Types abbriviations */
  &GLOBAL-DEFINE Owners                 "O"
  &GLOBAL-DEFINE Lenders                "L"
  &GLOBAL-DEFINE SecondLoan             "S"
  &GLOBAL-DEFINE SecondLenders          "S"
  &GLOBAL-DEFINE LenderCPL              "O"
  &GLOBAL-DEFINE Lenders                "L"
  &GLOBAL-DEFINE SecondLoan             "S"
  &GLOBAL-DEFINE SecondLenders          "S"
  
  /*Attribute*/ 
  &GLOBAL-DEFINE PostAll                "PostAll"
  &GLOBAL-DEFINE PreAll                 "PreAll"
  
  /*Defind Rules*/ 
  &GLOBAL-DEFINE Add                    "Add"       
  &GLOBAL-DEFINE Subtract               "Subtract"  
  &GLOBAL-DEFINE Multiply               "Multiply"   
  &GLOBAL-DEFINE Divide                 "Divide"    
  &GLOBAL-DEFINE Minimum                "Minimum"   
  &GLOBAL-DEFINE Maximum                "Maximum"   
  &GLOBAL-DEFINE Base                   "Base"      
  &GLOBAL-DEFINE Percent                "Percent"   
  &GLOBAL-DEFINE Fixed                  "Fixed"     
  &GLOBAL-DEFINE CardRange              "CardRange" 
  &GLOBAL-DEFINE CardRules              "CardRules" 
  &GLOBAL-DEFINE RoundUp                "RoundUp"   
  &GLOBAL-DEFINE RoundDown              "RoundDown" 
  &GLOBAL-DEFINE Round                  "Round"
  &GLOBAL-DEFINE Default                "Default"
  
  /* Grouping category Logs*/     
  &GLOBAL-DEFINE  GroupRule             "Rule"
  &GLOBAL-DEFINE  GroupRuleError        "Rule Error"
  &GLOBAL-DEFINE  GroupRange            "Range"
  &GLOBAL-DEFINE  GroupRangeError       "Range Error"
  &GLOBAL-DEFINE  GroupReference        "Reference"
  &GLOBAL-DEFINE  GroupReferenceError   "Reference Error"
  &GLOBAL-DEFINE  Validate              "Validate"
  &GLOBAL-DEFINE  CardSelection         "Card Selection"
  &GLOBAL-DEFINE  CardSelectionError    "Card Selection Error"
  &GLOBAL-DEFINE  PremiumResult         "Resulted Premium"
  &GLOBAL-DEFINE  GroupError            "Error"
  &GLOBAL-DEFINE  GrandTotal            "Grand Total"
  
  
   /* Log Calculation for*/                     
  &GLOBAL-DEFINE OwnersPolicy           "Owner Policy"
  &GLOBAL-DEFINE LendersPolicy          "Loan Policy"
  &GLOBAL-DEFINE SecondLendersPolicy    "Second Loan Policy"
  &GLOBAL-DEFINE SimultaneousCalc       "Simultaneous" 
  &GLOBAL-DEFINE OwnersEndors           "Owner endorsement"
  &GLOBAL-DEFINE LendersEndors          "Loan endorsement"
  &GLOBAL-DEFINE SecondLendersEndors    "Second Loan endorsement"
  &GLOBAL-DEFINE Scenario               "Scenario"
  
  /* Log Typeofamount*/ 
  &GLOBAL-DEFINE Face                   "Face"
  &GLOBAL-DEFINE Excess                 "Excess"
  &GLOBAL-DEFINE Endorsements           "Endorsements" 
  
  /* Log Typeofamount*/ 
  &GLOBAL-DEFINE Residential            "R"
  &GLOBAL-DEFINE Commercial             "M"
  &GLOBAL-DEFINE Construction           "C" 
  &GLOBAL-DEFINE CommConstruction       "MC"
  &GLOBAL-DEFINE ResiConstruction       "RC" 
  &GLOBAL-DEFINE CommBuilder            "MB"
  &GLOBAL-DEFINE ResiBuilder            "RB" 
  &GLOBAL-DEFINE Charity                "Ch" 
  &GLOBAL-DEFINE Builder                "B" 
  &GLOBAL-DEFINE Developer              "D" 
  &GLOBAL-DEFINE PropTypes              "R,Residential,M,Commercial,C,Construction,MC,CommConstruction,RC,ResiConstruction,MB,CommBuilder,RB,ResiBuilder,Ch,Charity,B,Builder,D,Developer"
  &GLOBAL-DEFINE StateWithScenarios     "KS,TX,WI,UT,NV,NM"  
  &GLOBAL-DEFINE StateWithCommitments   "NM"  
  &GLOBAL-DEFINE KansasJuniorMaxLimit     500000
  &GLOBAL-DEFINE KansasJuniorFixAmtLimit  150000
  &GLOBAL-DEFINE WisconsinJuniorMaxLimit  500000
  &GLOBAL-DEFINE UtahJuniorMaxLimit       250000
  &GLOBAL-DEFINE UtahHomeEquityMaxLimit   100000
  &GLOBAL-DEFINE NevadaJuniorMaxLimit          250000
  &GLOBAL-DEFINE NevadaBulidersParcelMinLimit  2
  &GLOBAL-DEFINE NewMexicoLimitedMortagageMaxLimit  20000000

&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 8.48
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


