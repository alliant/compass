&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file set-buttons.i
@description Functions for maintaining the buttons

@author John Oliver
@created 9/17/2018
Modifications:
Date         Name     Description
02/06/2023   Shefali  Task-102072 - Implement print,PDF, up and down.

  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

&if defined(exclude) = 0 &then
&scoped-define exclude false
&endif

&if defined(toggle) = 0 &then
&scoped-define toggle true
&endif

&if defined(useSmall) = 0 &then
&scoped-define useSmall false
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

create uiButton.
assign
  uiButton.uiButtonLabel    = "{&label}"
  uiButton.uiButtonImage    = "{&image}"
  uiButton.uiButtonInactive = "{&inactive}"
  uiButton.uiButtonExclude  = {&exclude}
  uiButton.uiButtonToggle   = {&toggle}
  uiButton.uiButtonUseSmall = {&useSmall}
  .
  
&if defined(b) = 0 &then
uiButton.uiButtonWidgetName = "b" + uiButton.uiButtonLabel.
&else
uiButton.uiButtonWidgetName = "{&b}".
&endif
uiButton.uiButtonWidget = GetWidgetByName(frame {&frame-name}:handle, uiButton.uiButtonWidgetName).
if valid-handle(uiButton.uiButtonWidget) and uiButton.uiButtonWidget:width-chars = 4.8
 then uiButton.uiButtonUseSmall = true.
  
/* if the image isn't defined, then let the program decide if possible */
if uiButton.uiButtonImage = ""
 then
  case uiButton.uiButtonLabel:
   when "New" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-new.bmp"   else "images/new.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-new-i.bmp" else "images/new-i.bmp"
      .
   when "Add" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-add.bmp"   else "images/add.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-add-i.bmp" else "images/add-i.bmp"
      .
   when "Refresh" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-sync.bmp"   else "images/sync.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-sync-i.bmp" else "images/sync-i.bmp"
      .
   when "Go" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-completed.bmp"   else "images/completed.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-completed-i.bmp" else "images/completed-i.bmp"
      .
   when "Export" or
   when "Excel"  then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-excel.bmp"   else "images/excel.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-excel-i.bmp" else "images/excel-i.bmp"
      .
   when "Print" or
   when "PDF"   then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-pdf.bmp"   else "images/pdf.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-pdf-i.bmp" else "images/pdf-i.bmp"
      .
   when "Close" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-close.bmp"   else "images/close.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-close-i.bmp" else "images/close-i.bmp"
      .
   when "Clear" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-erase.bmp"   else "images/erase.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-erase-i.bmp" else "images/erase-i.bmp"
      .
   when "Filter" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-filter.bmp"   else "images/filter.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-filter-i.bmp" else "images/filter-i.bmp"
      .
   when "FilterClear" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-erase.bmp"   else "images/filtererase.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-erase-i.bmp" else "images/filtererase-i.bmp"
      .
   when "Config" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-gear.bmp"   else "images/gear.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-gear-i.bmp" else "images/gear-i.bmp"
      .
   when "Edit"   or
   when "Modify" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-update.bmp"   else "images/update.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-update-i.bmp" else "images/update-i.bmp"
      .
   when "Delete" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-delete.bmp"   else "images/delete.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-delete-i.bmp" else "images/delete-i.bmp"
      .
   when "Cancel" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-cancel.bmp"   else "images/cancel.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-cancel-i.bmp" else "images/cancel-i.bmp"
      .
   when "Copy" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-copy.bmp"   else "images/copy.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-copy-i.bmp" else "images/copy-i.bmp"
      .
   when "Notes" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-note.bmp"   else "images/note.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-note-i.bmp" else "images/note-i.bmp"
      .
   when "NewNote" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-note.bmp"   else "images/blank-add.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-note-i.bmp" else "images/blank-add-i.bmp"
      .
   when "Docs" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-docs.bmp"   else "images/docs.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-docs-i.bmp" else "images/docs-i.bmp"
      .
   when "Save" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-save.bmp"   else "images/save.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-save-i.bmp" else "images/save-i.bmp"
      .
   when "Search" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-magnifier.bmp"   else "images/magnifier.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-magnifier-i.bmp" else "images/magnifier-i.bmp"
      .
   when "Undo" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-undo.bmp"   else "images/undo.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-undo-i.bmp" else "images/undo-i.bmp"
      .
   when "Up" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-up.bmp"   else "images/up.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-up-i.bmp" else "images/up-i.bmp"
      .
   when "Down" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-down.bmp"   else "images/down.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-down-i.bmp" else "images/down-i.bmp"
      .
   when "Task"  or
   when "Tasks" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-task.bmp"   else "images/task.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-task-i.bmp" else "images/task-i.bmp"
      .
   when "View" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-open.bmp"   else "images/open.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-open-i.bmp" else "images/open-i.bmp"
      .
   when "Upload" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-upload.bmp"   else "images/upload.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-upload-i.bmp" else "images/upload-i.bmp"
      .
   when "Download" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-download.bmp"   else "images/download.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-download-i.bmp" else "images/download-i.bmp"
      .
   when "Email" then
    assign
      uiButton.uiButtonImage    = if uiButton.uiButtonUseSmall then "images/s-email.bmp"   else "images/email.bmp"
      uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-email-i.bmp" else "images/email-i.bmp"
      .
  end case.
  
/* if the image isn't found */
if search(uiButton.uiButtonImage) = ?
 then uiButton.uiButtonImage = if uiButton.uiButtonUseSmall then "images/s-question.bmp" else "images/question.bmp".
  
/* if the image isn't found */
if search(uiButton.uiButtonInactive) = ?
 then uiButton.uiButtonInactive = if uiButton.uiButtonUseSmall then "images/s-question-i.bmp" else "images/question-i.bmp".

release uiButton.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

