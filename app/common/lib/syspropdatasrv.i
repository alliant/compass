&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file syspropdatasrv.i
@description Data Server file for the system properties

@param sysprop;char;Comma-delimited list of sysprops to get. The props are
                    separated by colon (:). The first is the appCode,
                    second is the objAction, and third is the objProperty

@author John Oliver
@created 04-19-2019
@notes Remade from commondatasrv.i
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}
define variable tLoadedSysProps as logical initial false no-undo.

/* tables */
{tt/sysprop.i}
{tt/esbmsg.i &tableAlias="syspropesbmsg"}

/* temp tables */
{tt/sysprop.i &tableAlias="tempsysprop"}

/* functions */
{lib/add-delimiter.i}

/* preprocessors */
&if defined(sysprop) = 0 &then
std-ch = "".
publish "GetAppCode" (output std-ch).
&scoped-define sysprop std-ch
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshSysProp Include 
FUNCTION refreshSysProp RETURNS LOGICAL
  ( input pAppCode     as character,
    input pObjAction   as character,
    input pObjProperty as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* sysprop procedures */
subscribe to "SysPropChanged" anywhere.
subscribe to "GetSysProp" anywhere.
subscribe to "GetSysProps" anywhere.
subscribe to "GetSysPropDesc" anywhere.
subscribe to "GetSysPropList" anywhere.
subscribe to "GetSysPropListMinusID" anywhere.
subscribe to "LoadSysProps" anywhere.
subscribe to "GetSysPropByAppCode" anywhere.

/* used for the messaging system */
subscribe to "TIMER"              anywhere run-procedure "SysPropTimer".
subscribe to "ExternalEsbMessage" anywhere run-procedure "SysPropESB".

/* used to dump the data file */
subscribe to "DataDump"           anywhere run-procedure "SysPropDataDump".

/* load system properties */
std-lo = true.
publish "SetSplashStatus".
publish "GetLoadSysProps" (output std-lo).
if std-lo 
 then run LoadSysProps.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysPropGet Include 
PROCEDURE GetSysProp :
/*------------------------------------------------------------------------------
@description Gets the system property
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pAppCode  as character no-undo.
  define input  parameter pAction   as character no-undo.
  define input  parameter pProperty as character no-undo.
  define output parameter table for tempsysprop.
  
  /* buffers */
  define buffer tempsysprop for tempsysprop.
  define buffer sysprop for sysprop.

  /* if the appCode is blank, then get the code from the XML file */
  if pAppCode = ""
   then publish "GetAppCode" (output pAppCode).
  
  /* if the system property can not be found */
  empty temp-table tempsysprop.
  if not can-find(first sysprop where appCode = pAppCode and objAction = pAction and objProperty = pProperty)
   then
    do:
      /* call the server */
      run server/getsysprops.p (input  pAppCode,
                                input  pAction,
                                input  pProperty,
                                input  "",
                                input  "",
                                output table tempsysprop,
                                output std-lo,
                                output std-ch
                                ).

      /* if unsuccessful, show a message */
      if not std-lo
       then message "LoadSysProps (" + pAppCode + ", " + pAction + ", " + pProperty + ") failed: " + std-ch view-as alert-box error.
       else
        /* otherwise, add the newly retrieved system property to the system property records */
        for each tempsysprop:
          create sysprop.
          buffer-copy tempsysprop to sysprop.
        end.
    end.
   
  /* load the table */
  empty temp-table tempsysprop.
  for each sysprop no-lock
     where sysprop.appCode = pAppCode
       and sysprop.objAction = pAction
       and sysprop.objProperty = pProperty
        by sysprop.objRef
        by sysprop.objID:
    
    create tempsysprop.
    buffer-copy sysprop to tempsysprop.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSysPropByAppCode Include 
PROCEDURE GetSysPropByAppCode :
/*------------------------------------------------------------------------------
@description Gets the system property
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pAppCode  as character no-undo.
  define output parameter table for tempsysprop.
  
  /* buffers */
  define buffer tempsysprop for tempsysprop.
  define buffer sysprop for sysprop.
       
  /* load the table */
  empty temp-table tempsysprop.
  for each sysprop no-lock
     where sysprop.appCode = pAppCode:    
    create tempsysprop.
    buffer-copy sysprop to tempsysprop.   
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSysPropDesc Include 
PROCEDURE GetSysPropDesc :
/*------------------------------------------------------------------------------
@description Gets a system property description
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pAppCode  as character no-undo.
  define input  parameter pAction   as character no-undo.
  define input  parameter pProperty as character no-undo.
  define input  parameter pID       as character no-undo.
  define output parameter pDesc     as character no-undo.
  
  std-ch = "Active^A^Inactive^I^Closed^C^Prospect^P^Withdrawn^W^Cancelled^X".

  if pAppCode = "AMD" and pAction = "AgentManager" and pProperty = "Status"
   then
    do:
      pDesc = if lookup(pID, std-ch, {&msg-dlm}) = 0 then "" else entry(lookup(pID, std-ch, {&msg-dlm}) - 1, std-ch, {&msg-dlm}).
      return.
    end.
    
  if pAppCode = "AMD" and pAction = "Agent" and pProperty = "Status"
   then
    do:
      pDesc = if lookup(pID, std-ch, {&msg-dlm}) = 0 then "" else entry(lookup(pID, std-ch, {&msg-dlm}) - 1, std-ch, {&msg-dlm}).
      return.
    end.


  /* get the system properties list */
  std-ch = "".
  run GetSysPropList (pAppCode, pAction, pProperty, output std-ch).

  /* if list found and ID is not blank */
  pDesc = pID.
  if std-ch > "" and pID > ""
   then
    do:
      /* lookup the value, if available */
      if lookup(pID, std-ch, {&msg-dlm}) > 0 
       then pDesc = entry(lookup(pID, std-ch, {&msg-dlm}) - 1, std-ch, {&msg-dlm}).
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysPropGetList Include 
PROCEDURE GetSysPropList :
/*------------------------------------------------------------------------------
@description Gets a list of system properties
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pAppCode  as character no-undo.
  define input  parameter pAction   as character no-undo.
  define input  parameter pProperty as character no-undo.
  define output parameter pList     as character no-undo.
  
  /* buffers */
  define buffer tempsysprop for tempsysprop.

  /* get the system property */
  empty temp-table tempsysprop.
  run GetSysProp (pAppCode, pAction, pProperty, output table tempsysprop).

  /* create a list of system properties */
  pList = "".
  for each tempsysprop no-lock
     where tempsysprop.appCode = pAppCode
       and tempsysprop.objAction = pAction
       and tempsysprop.objProperty = pProperty
        by tempsysprop.objRef
        by tempsysprop.objID:
    
    pList = addDelimiter(pList, {&msg-dlm}) + tempsysprop.objValue {&msg-add} tempsysprop.objID.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSysPropListMinusID Procedure 
PROCEDURE GetSysPropListMinusID :
/*------------------------------------------------------------------------------
@description Get a list of system properties minus the ID passed in
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pAppCode  as character no-undo.
  define input  parameter pAction   as character no-undo.
  define input  parameter pProperty as character no-undo.
  define input  parameter pMinusID  as character no-undo.
  define output parameter pList     as character no-undo.
  
  /* variables */
  define variable tList  as character no-undo.
  define variable tID    as character no-undo.
  define variable tValue as character no-undo.
  
  /* get the system property list */
  run GetSysPropList (pAppCode, pAction, pProperty, output tList).
  
  /* divide the list and take out the ID */
  do std-in = 1 to num-entries(tList, {&msg-dlm}) / 2:
    assign
      tValue = entry(std-in * 2 - 1, tList, {&msg-dlm})
      tID    = entry(std-in * 2, tList, {&msg-dlm})
      .
    if tID = pMinusID
     then next.
     
    pList = addDelimiter(pList, {&msg-dlm}) + tValue {&msg-add} tID.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysPropGet Include 
PROCEDURE GetSysProps :
/*------------------------------------------------------------------------------
@description Gets the system properties
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for sysprop.
  
  /* if not loaded, load the system properties */
  if not tLoadedSysProps
   then run LoadSysProps.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadSysProps Include 
PROCEDURE LoadSysProps :
/*------------------------------------------------------------------------------
@description Loads the System Properties
------------------------------------------------------------------------------*/
  /* variables */
  define variable tSysProp as character no-undo.
  define variable tAppCode as character no-undo.
  define variable tObjAction as character no-undo.
  define variable tObjProperty as character no-undo.

  /* buffers */
  define buffer sysprop for sysprop.
  define buffer tempsysprop for tempsysprop.

  /* load the system properties from the preprocessor */
  empty temp-table sysprop.
  do std-in = 1 to num-entries({&sysprop}):
    tSysProp = entry(std-in, {&sysprop}).
    tAppCode = entry(1, tSysProp, ":").
    if num-entries(tSysProp, ":") = 2
     then tObjAction = entry(2, tSysProp, ":").
    if num-entries(tSysProp, ":") = 3
     then tObjProperty = entry(3, tSysProp, ":").
     
    /* call the server */
    empty temp-table tempsysprop.
    run server/getsysprops.p (input tAppCode,
                              input tObjAction,
                              input tObjProperty,
                              input "",
                              input "",
                              output table tempsysprop,
                              output std-lo,
                              output std-ch
                              ).
                              
    /* if unsuccessful, show a message */
    if not std-lo 
     then
      do:
        std-lo = false.
        publish "GetAppDebug" (output std-lo).
        if std-lo 
         then message "LoadSysProps (" + replace(tSysProp, ":", ", ") + ") failed: " + std-ch view-as alert-box warning.
      end.
     else
      /* otherwise, add the newly retrieved system property to the system property records */
      for each tempsysprop:
        create sysprop.
        buffer-copy tempsysprop to sysprop.
      end.
  end.
  
  /*** ALWAYS LOAD YOUR OWN MODULE PROPERTIES ***/
  tAppCode = "".
  publish "GetAppCode" (output tAppCode).
  if not can-find(first sysprop where appCode = tAppCode)
   then
    do:
      empty temp-table tempsysprop.
      run server/getsysprops.p (input  tAppCode,
                                input  "",
                                input  "",
                                input  "",
                                input  "",
                                output table tempsysprop,
                                output std-lo,
                                output std-ch
                                ).
    
      if std-lo 
       then
        for each tempsysprop:
          create sysprop.
          buffer-copy tempsysprop to sysprop.
        end.
       else
        do: 
          std-lo = false.
          publish "GetAppDebug" (output std-lo).
          if std-lo 
           then message "LoadSysProps (" + tAppCode + ") failed: " + std-ch view-as alert-box warning.
        end.
    end.
    
  /*** LOAD SYSPROPS NEEDED FOR COMMON ELEMENTS ***/
  /* agent status */
  if not can-find(first sysprop where appCode = "AMD" and objAction = "Agent" and objProperty = "Status")
   then
    do:
      empty temp-table tempsysprop.
      run server/getsysprops.p (input "AMD",
                                input "Agent",
                                input "Status",
                                input "",
                                input "",
                                output table tempsysprop,
                                output std-lo,
                                output std-ch
                                ).
    
      if std-lo 
       then
        for each tempsysprop:
          create sysprop.
          buffer-copy tempsysprop to sysprop.
        end.
       else
        do: 
          std-lo = false.
          publish "GetAppDebug" (output std-lo).
          if std-lo 
           then message "LoadSysProps (AMD, Agent, Status) failed: " + std-ch view-as alert-box warning.
        end.
    end.
  tLoadedSysProps = true.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysPropChanged Include 
PROCEDURE SysPropChanged :
/*------------------------------------------------------------------------------
@description Reloads a property
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pAppCode as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pProperty as character no-undo.
  
  /* refresh the agent */
  refreshSysProp(pAppCode, pAction, pProperty).
  
  /* publish that the data changed */
  publish "SysPropDataChanged".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysPropDataDump Include 
PROCEDURE SysPropDataDump :
/*------------------------------------------------------------------------------
@description Dump the sysprop and tempsysprop temp-tables into files
------------------------------------------------------------------------------*/
  /* variables */
  define variable tPrefix as character no-undo.
  
  /* get the prefix */
  publish "GetAppCode" (output tPrefix).
  tPrefix = tPrefix + "_".
  
  /* delete the current temp files */
  publish "DeleteTempFile" ("DataSysProp", output std-lo).
  publish "DeleteTempFile" ("DataTempSysProp", output std-lo).
  
  /* Active temp-tables */
  temp-table sysprop:write-xml("file", tPrefix + "SysProp.xml").
  publish "AddTempFile" ("DataSysProp", tPrefix + "SysProp.xml").
  
  /* "Temp" temp-tables */
  temp-table tempsysprop:write-xml("file", tPrefix + "TempSysProp.xml").
  publish "AddTempFile" ("DataSysProp", tPrefix + "TempSysProp.xml").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysPropESB Procedure 
PROCEDURE SysPropESB :
/*------------------------------------------------------------------------------
@description Runs when an esb message is received
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey as character no-undo.
  
  /* exit if not correct entity */
  if pEntity <> "SysProp"
   then next.
  
  /* get the next number */
  std-in = 1.
  for each syspropesbmsg no-lock:
    std-in = std-in + 1.
  end.
  
  /* create the esb message */
  create syspropesbmsg.
  assign
    syspropesbmsg.seq    = std-in
    syspropesbmsg.rcvd   = now
    syspropesbmsg.entity = pEntity
    syspropesbmsg.action = pAction
    syspropesbmsg.keyID  = pKey
    .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysPropTimer Include 
PROCEDURE SysPropTimer :
/*------------------------------------------------------------------------------
@description Triggered by appstart.i running on a timer
------------------------------------------------------------------------------*/
  /* buffers */
  define buffer syspropesbmsg for syspropesbmsg.

  /* loop through the esb messages */
  for each syspropesbmsg exclusive-lock
  break by syspropesbmsg.keyID
        by syspropesbmsg.rcvd descending:

    /* if the sysprop is loaded and the we have a messages */
    if tLoadedSysProps and first-of(syspropesbmsg.keyID) 
     then run SysPropChanged (syspropesbmsg.keyID).
     
    /* delete the esb message and set the last sync */
    delete syspropesbmsg.
    publish "SetLastSync".
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshSysProp Include 
FUNCTION refreshSysProp RETURNS LOGICAL
  ( input pAppCode     as character,
    input pObjAction   as character,
    input pObjProperty as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* variables */
  define variable lSuccess as logical no-undo.
  
  /* buffers */
  define buffer tempsysprop for tempsysprop.
  define buffer sysprop for sysprop.
  
  /* call the server */
  empty temp-table tempsysprop.
  run server/getsysprops.p (input  pAppCode,
                            input  pObjAction,
                            input  pObjProperty,
                            input  "",
                            input  "",
                            output table tempsysprop,
                            output lSuccess,
                            output std-ch
                            ).

  /* if unsuccessful, show a message if debug is on */
  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshSysProp failed: " + std-ch view-as alert-box warning.
    end.
   else
    /* delete the sysprop records */
    for each sysprop exclusive-lock
       where sysprop.appCode = pAppCode 
         and sysprop.objAction = pObjAction 
         and sysprop.objProperty = pObjProperty:
         
      delete sysprop.
    end.
    /* add the tempsysprop records */
    for each tempsysprop no-lock:
      create sysprop.
      buffer-copy tempsysprop to sysprop.
    end.
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

