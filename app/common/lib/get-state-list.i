&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* ***************************  Definitions  ************************** */

&if defined(addAll) = 0 &then
&scoped-define addAll false
&endif

&if defined(onlyActive) = 0 &then
&scoped-define onlyActive true
&endif

&if defined(useSingle) = 0 &then
&scoped-define useSingle false
&endif

&if defined(useFilter) = 0 &then
&scoped-define useFilter true
&endif

&if defined(useFullName) = 0 &then
&scoped-define useFullName true
&endif

&if defined(firstValue) = 0 &then
&scoped-define firstValue "ALL,ALL"
&endif

&if defined(addUnknown) = 0 &then
&scoped-define addUnknown false
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* get the states */
if {&combo}:type = "COMBO-BOX"
 then {&combo}:inner-lines = 10.
if {&useSingle}
 then {&combo}:list-items = entry(1, {&firstValue}).
 else {&combo}:list-item-pairs = entry(1, {&firstValue}) + {&combo}:delimiter + entry(2, {&firstValue}).

if {&addUnknown}
 then
  do:
    if {&useSingle}
     then {&combo}:add-last("Unknown").
     else {&combo}:add-last("Unknown","Unknown").
  end.    
 
 
std-in = 0.
&if defined(noPublish) = 0 &then
empty temp-table state.
if {&useFilter}
 then publish "GetStates" (output table state).
 else publish "GetAllStates" (output table state).
&endif
for each state no-lock:
  std-in = std-in + 1.
  if {&onlyActive} and not state.active
   then next.
  
  if {&useSingle}
   then {&combo}:add-last(state.stateID).
   else
    if {&useFullName}
     then {&combo}:add-last(state.description, state.stateID).
     else {&combo}:add-last(state.stateID, state.stateID).
end.

if std-in > 0 and not {&addAll}
 then {&combo}:delete(1).

{&combo}:screen-value = entry(2, (if {&useSingle} then {&combo}:list-items else {&combo}:list-item-pairs), {&combo}:delimiter).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


