&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file persondatasrvs.i
@description Data Server file for managing the person

@author Sachin Anthwal
@created 09-09-2022
@Modification
  Date            Name        Description
  11/07/2022     S Chandu     Modified searchPersons to populate Roles parameter to server call. 
  02/27/2022     SB           Task# 102649 Parameterize the Person Screen.
  06/26/2023     SB           Task #104008 Modified to implement tags for person
  09/28/2023     SK           Task #105743 Modified to show fulladdress
  02/16/2024     SB           Task #110389 Modified to fix the creation of new person associated with agent.
  04/03/2024     SB           Task #111689 Modified to get the duplicates based on name,email and mobile
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}

define variable loadPersons       as logical initial false no-undo.

/* temp tables */
{tt/person.i}
{tt/person.i   &tableAlias="tperson"}
{tt/person.i   &tableAlias="tcachePerson"}
{tt/personagentdetails.i}
{tt/tag.i}
{tt/tag.i &tableAlias=temptag}
{tt/tag.i &tableAlias=removedtag}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 18.71
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* person procedures */
subscribe to "getPersons"                         anywhere.
subscribe to "refreshPersons"                     anywhere.
subscribe to "getDuplicatePersons"                anywhere.
subscribe to "NewPerson"                          anywhere.
subscribe to "getPerson"                          anywhere.
subscribe to "deletePerson"                       anywhere.

subscribe to "ManagePersonsCache"                 anywhere.

subscribe to "searchPersons"                      anywhere.
subscribe to "newPersonAttorney"                  anywhere.
subscribe to "validPerson"                        anywhere.
subscribe to "getPersonName"                      anywhere.

subscribe to "newusertag"                         anywhere.
subscribe to "untagentity"                        anywhere.


/* std-lo = true.                            */
/* publish "SetSplashStatus".                */
/* publish "GetLoadpersons" (output std-lo). */
/* if std-lo                                 */
/*  then run Loadpersons.                    */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deletePerson Include 
PROCEDURE deletePerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define input  parameter ipcPersonID as character no-undo.
  define output parameter oplSuccess  as logical   no-undo.
  define output parameter opcMsg      as character no-undo.

  define buffer tcachePerson for tcachePerson.

  /* client Server Call */
  run server/deletePerson.p (input ipcPersonID,
                             output oplSuccess,
                             output opcMsg).

  if not oplSuccess 
   then
    return.

  find first tcachePerson where tcachePerson.personID = ipcPersonID no-error.
  
  if available tcachePerson 
   then
    delete tcachePerson.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getduplicatepersons Include 
PROCEDURE getduplicatepersons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define input  parameter ipcPersonName    as character no-undo.
  define input  parameter ipcPersonEmail   as character no-undo.
  define input  parameter ipcPersonMobile  as character no-undo.
  define output parameter table for tPerson.
  define output parameter table for personagentdetails.
  define output parameter oplDupPerson     as logical no-undo.
  define output parameter oplCanLinkAent   as logical no-undo.
  define output parameter oplSuccess       as logical   no-undo.
  define output parameter opcMsg           as character no-undo.
 
  empty temp-table tPerson.
  
  /* Server Call */
  run server/getduplicatepersons.p(input  ipcPersonName,
                                   input  ipcPersonEmail,
                                   input  ipcPersonMobile,
                                   output table tPerson,
                                   output table personagentdetails,
                                   output oplDupPerson,
                                   output oplCanLinkAent,
                                   output oplSuccess,
                                   output opcMsg).
 
  if not oplSuccess 
   then
    return.
                               
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getPerson Include 
PROCEDURE getPerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipPersonID as character no-undo.
  define output parameter table for tPerson. 

  empty temp-table tPerson.

  if ipPersonID <> ""  
   then      
    do:
      find first tcachePerson where tcachePerson.personID = ipPersonID no-error.
      if available tcachePerson
       then
        do:
          create tPerson.
          buffer-copy tcachePerson to tPerson no-error.
        end.
    end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getPersonName Include 
PROCEDURE getPersonName :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter  ipcPersonID as character no-undo.
  define output parameter  opcName     as character no-undo.
  define output parameter  oplSuccess  as logical   no-undo.

  /* Buffer */
  define buffer tcachePerson for tcachePerson.

  /* Client Server Call */
  if not can-find(first tcachePerson)
   then
    do:    
      run server/getPersons.p (output table tcachePerson,
                               output std-lo,
                               output std-ch).
      if not std-lo 
       then 
        /* message not sent back to UI, so displayed here. */
        message "Load Persons failed: " std-ch
                 view-as alert-box error.
    end.

  find first tcachePerson where tcachePerson.personID = ipcPersonID no-error.
  if available tcachePerson
   then
    do:
      assign
          opcName      = tcachePerson.dispName
          oplSuccess   = true
          .
    end.    
  else
   oplSuccess = false.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getPersons Include 
PROCEDURE getPersons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define output parameter table for tcachePerson.
 
 /* Buffer */
  define buffer tcachePerson for tcachePerson.

  /* Client Server Call */
  if not loadPersons
   then
    do:    
      run server/getPersons.p (output table tcachePerson,
                               output std-lo,
                               output std-ch).
      if not std-lo 
       then 
        /* message not sent back to UI, so displayed here. */
        message "Load Persons failed: " std-ch
                 view-as alert-box error.
      
      loadPersons  = std-lo.
    end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ManagePersonsCache Include 
PROCEDURE ManagePersonsCache :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter table for tPerson.
  
  /* Buffer */
  define buffer tPerson      for tPerson.
  define buffer tCachePerson for tCachePerson.
  
  for first tPerson no-lock:
    find first tCachePerson where tCachePerson.personID = tPerson.personID no-lock no-error.
    if not avail tCachePerson 
	 then
       create tCachePerson.
	   
    buffer-copy tPerson to tCachePerson.
  end.
  
  empty temp-table tPerson.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newPerson Include 
PROCEDURE newPerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter table for tPerson.
  define output parameter opcPersonID as character no-undo.
  define output parameter oplSuccess  as logical   no-undo.
  define output parameter opcMsg      as character no-undo.

  define buffer tPerson       for tPerson.
  define buffer tcacheperson  for tcacheperson.

  /* Check if input table is empty then return */
  find first tPerson no-error.
  if not available tPerson
   then
    return.

  /* Create temporary record in main table regarding new person as 
     input temprary table for person will be empty after server call 
     in case the person is not duplicate */
  /* Caching of data */
  create tcacheperson.
  buffer-copy tPerson except personID to tcacheperson.
  tcacheperson.personID = "-1". /* assign temporary Person ID */

  /* Server Call */
  run server/newperson.p (input table tPerson, /* I/P - new person record, O/P - All duplicate recods matching the input person record. */
                          output opcPersonID,         /* In case there is no error, then ID of newly created person record. */
                          output oplSuccess,
                          output opcMsg).

  /* In case sever call is not sucessful or 
     in case duplicate person is found, then no need to keep the record. */
  if (not oplSuccess) or 
      (oplSuccess and (opcPersonID = "0" or opcPersonID = "" or opcPersonID = ?)) /*Scenario is applicable when existing person is associated with current agent*/
   then
    do:
      /* If there is no success on server then delete the temprary created person*/
      find first tcacheperson where tcacheperson.personID = "-1" no-error. /* find temporary person created */
      if available tcacheperson 
       then
        delete tcacheperson.
      return.
    end. 

  /* In case of success and person is not duplicate then replace temporary Persion ID with permanent ID */
  /* Caching of data */
  find first tcacheperson where tcacheperson.personID = "-1" no-error. /* find temp person created */
  if available tcacheperson 
   then
    assign
        tcacheperson.personID     = opcPersonID
        tcacheperson.active       = if tcacheperson.linkedToCompliance = true then true else false
        tcacheperson.contactPhone = tPerson.contactPhone + (if tPerson.phExtension = '' then '' else (',' + tPerson.phExtension)).
        
  empty temp-table tPerson.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newPersonAttorney Include 
PROCEDURE newPersonAttorney :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter table for tPerson.
  
  for first tPerson:
    find first tcachePerson 
      where tcachePerson.personId = tperson.personID no-error.
    if not available tcachePerson
     then
      do:
        create tcachePerson.
        buffer-copy tperson to tcachePerson.
      end.
  end.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newusertag Include 
PROCEDURE newusertag :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input-output parameter table for tag.
  define output parameter loTag  as logical.
  
  define buffer tag     for tag.
  define buffer temptag for temptag.

  run server/newusertag.p (input-output table tag,
                           output loTag,
                           output std-ch).
  if not loTag
   then
    do:
      message std-ch
        view-as alert-box.
      return.
    end.
    
  for each tag:
    create temptag.
    buffer-copy tag except tag.name to temptag.
    assign
        temptag.tagID = tag.tagID 
        temptag.name  = tag.name
        .    
  end.
  
/*   empty temp-table tag. */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshPersons Include 
PROCEDURE refreshPersons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define output parameter oplSuccess as logical    no-undo.
  define output parameter opcMsg     as character  no-undo.
  
  define buffer tcachePerson  for tcachePerson.
  define buffer tPerson       for tPerson.

  empty temp-table tPerson.
  
  /* Get the records in temp-table, so that in case any error comes, then the local data instance is not deleted. */
  /* client Server Call */
  run server/getPersons.p (output table tPerson,
                           output oplSuccess,
                           output opcMsg).
 
  if not oplSuccess 
   then
    return.
  
 /* publish "SetCurrentValue" ("RefreshActions", true). */

  empty temp-table tcachePerson.

  /* Update the local copy of data. */  
  for each tPerson:
    create tcachePerson.
    buffer-copy tPerson to tcachePerson.
  end.

  empty temp-table tPerson.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE searchPersons Include 
PROCEDURE searchPersons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pstateID      as character no-undo.
  define input  parameter pSearchString as character no-undo.
  define input  parameter pcNameType    as character no-undo.
  define input  parameter pcNameBegins  as character no-undo.
  define output parameter table for person.
  define output parameter oplSuccess    as logical   no-undo.
  define output parameter opcMsg        as character no-undo.

  define buffer tPerson for tPerson.
  define buffer person  for person.

  empty temp-table tPerson.

  /* Person caching is yet to implement. */
  /* Get the records in temp-table, so that in case any error comes, then the local data instance is not deleted. */
  run server/searchPersons.p (input  pstateID ,
                              input  pSearchString ,
                              input  "", /* when the personrole type is not given pass it has empty string - "" */
                              input  pcNameType,
                              input  pcNameBegins,
                              output table tPerson,
                              output oplSuccess,
                              output opcMsg).

  if not oplSuccess
   then
    return.

  empty temp-table person.

  for each tPerson:
    create person.
    buffer-copy tPerson to person.
    person.fulladdress  =  if (tperson.address1 = "?") or (tperson.address1 = "") then "" else tperson.address1 +
                           if (tperson.address2 = "?") or (tperson.address2 = "") then "" else ", " + tperson.address2   +
                           if (tperson.city  = "?") or (tperson.city = "")  then "" else ", " + tperson.city   +
                           if (tperson.state = "?") or (tperson.state = "") then "" else ", " +  tperson.state  +
                           if (tperson.zip   = "?") or (tperson.zip = "")   then "" else "," +  tperson.zip  .
    person.fulladdress  =  trim(person.fulladdress,',')   .
    person.fulladdress  =  trim(person.fulladdress).  
  end.

  empty temp-table tPerson.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE untagentity Include 
PROCEDURE untagentity :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter table for tag.
  define output parameter table for removedtag.
  define output parameter loTag  as logical no-undo.
  define output parameter opSucessMsg as character no-undo.
  
  define buffer tag     for tag.
  define buffer temptag for temptag.
  
  empty temp-table removedtag.
    
  run server/untagentity.p (input table tag,
                            output table removedtag,
                            output opSucessMsg,
                            output loTag,
                            output std-ch).
  if not loTag 
   then
    do:
      message std-ch
        view-as alert-box.
      return.  
    end.
   
  for each removedtag:
    for first temptag 
      where temptag.entityID = removedtag.entityID and 
            temptag.name     = removedtag.name:
      delete temptag.
    end.
  end.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validPerson Include 
PROCEDURE validPerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter  ipcPersonID as character no-undo.
  define output parameter oplSuccess  as logical   no-undo.

  define buffer tcachePerson for tcachePerson.
   
  find first tcachePerson where tcachePerson.PersonID = ipcPersonID no-error.
  if not available tcachePerson
   then
    do:
      message "Invalid Person ID"  
        view-as alert-box info buttons ok. 
      return.
    end.

  oplSuccess   = true.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

