&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : lib/brw-sortData-multi.i
    Author      : John Oliver
    Date        : 02/19/2016
    
    Purpose     : Browse procedure for default column sorting.
    
    Usage       : Place this include file in a procedure called 
                  'sortData' in a window that contains a browse 
                  control.
                  
                  Procedure syntax:
                    
                    {lib/brw-sortData-multi.i}
                    END PRODEDURE.
                    
                  Requires lib/std-def.i and lib/brw-main-multi.i

11.12.2014 das    Added pre-by-clause and post-by-clause parameters to allow 
                  for granular control including secondary sorting
10.15.2015 das    Added pre-code option at the beginning of the block.
------------------------------------------------------------------------*/

def input parameter pHandle as handle no-undo.
def input parameter pName as char no-undo.
def input parameter pWhere as char no-undo.
def input parameter pSort as char no-undo.

def var hBuf as handle no-undo.
def var cSortClause as char no-undo init "".

hQueryHandle = pHandle:query.
hQueryHandle:query-close().
pHandle:clear-sort-arrows().

if pName = dataSortBy 
 then dataSortDesc = not dataSortDesc.
 
if pName <> ""
 then cSortClause = "by " + pName + (if dataSortDesc then " descending " else " ").
 
cSortClause = cSortClause + (if pSort <> "" then pSort else "").
 
hBuf = hQueryHandle:get-buffer-handle(1).

tQueryString = "preselect each " + hBuf:table + " no-lock " + pWhere + " " + cSortClause.

do std-in = 1 to pHandle:num-columns:
  std-ha = pHandle:get-browse-column(std-in).
  if pName <> "" and hBuf:table + "." + std-ha:name = pName 
    then pHandle:set-sort-arrow(std-in, not dataSortDesc).
end.
 
dataSortBy = pName.

&if defined(ExcludeOpenQuery) = 0 &then 
hQueryHandle:query-prepare(tQueryString).
hQueryHandle:query-open().  
&endif

apply 'entry' to pHandle.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 2.86
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


