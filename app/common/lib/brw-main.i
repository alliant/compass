&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : lib/brw-main.i
    Author      : B.Johnson
    Date        : 08.27/2014
    
    Purpose     : Browse routines for row highlighting and column
                  sorting.  
    
    Usage       : Include this file in the main block of a window
                  that contains a browse control.
                  Requires lib/std-def.i
------------------------------------------------------------------------*/

&if defined (browse-name) <> 0 &then
    
    /* Get list of browse column-handles for setting row colors */
    colHandleList = "".
    colHandle = browse {&browse-name}:first-column.
    do while valid-handle(colHandle):
      colHandleList = colHandleList + string(colHandle) + ",".
      colHandle = colHandle:next-column.
    end.
    if colHandleList <> "" then
    colHandleList = trim(colHandleList,",").
    
    /* Enable column searching so that column sorting works */
    &if defined(noSort) = 0 &then
    {&browse-name}:column-movable = no.
    {&browse-name}:allow-column-searching = yes.
    &endif
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 2.95
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


