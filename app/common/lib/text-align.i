&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file text-align.i
@description Functions to align text
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD centerText Include 
FUNCTION centerText RETURNS LOGICAL
  ( input pWidget as handle )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD rightAlignText Include 
FUNCTION rightAlignText RETURNS LOGICAL
  ( input pWidget as handle )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION centerText Include 
FUNCTION centerText RETURNS LOGICAL
  ( input pWidget as handle ) :
/*------------------------------------------------------------------------------
@description Centers text within the widget
------------------------------------------------------------------------------*/
  DEFINE VARIABLE reps AS INTEGER NO-UNDO.
  reps = (pWidget:WIDTH-PIXELS - FONT-TABLE:GET-TEXT-WIDTH-PIXELS(TRIM(pWidget:SCREEN-VALUE),pWidget:FONT)) / FONT-TABLE:GET-TEXT-WIDTH-PIXELS(' ',pWidget:FONT).
  reps = (reps / 2).
  pWidget:SCREEN-VALUE = FILL(' ',reps) + TRIM(pWidget:SCREEN-VALUE).
  RETURN yes.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION rightAlignText Include 
FUNCTION rightAlignText RETURNS LOGICAL
  ( input pWidget as handle ) :
/*------------------------------------------------------------------------------
@description Aligns the text to the right
------------------------------------------------------------------------------*/
  DEFINE VARIABLE reps AS INTEGER NO-UNDO.
  reps = (pWidget:WIDTH-PIXELS - FONT-TABLE:GET-TEXT-WIDTH-PIXELS(TRIM(pWidget:SCREEN-VALUE),pWidget:FONT)) / FONT-TABLE:GET-TEXT-WIDTH-PIXELS(' ',pWidget:FONT).
  reps = reps - 1.
  pWidget:SCREEN-VALUE = FILL(' ',reps) + TRIM(pWidget:SCREEN-VALUE).
  RETURN yes.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

