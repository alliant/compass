&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file groupby-detail.i
@description Gets the detail rows for the grouped row
@author John Oliver
------------------------------------------------------------------------*/

/* Local Variable Definitions ---                                       */
{lib/std-def.i}

&if defined(whereClause) = 0 &then
&scoped-define whereClause "true = true"
&endif

&if defined(useClick) = 0 &then
&scoped-define useClick false
&endif

&if defined(expandAll) = 0 &then
&scoped-define expandAll false
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* create buffers */
create buffer hGroupBuffer for table "groupdata".
create buffer hDataBuffer  for table cDataTable.
create buffer hShowBuffer  for table cShowTable.

if {&whereClause} = ""
 then cWhereClause = "true = true".
 else cWhereClause = substring({&whereClause}, 6).

/* get the details */
DoWait(true).
for first groupdetail exclusive-lock
    where groupdetail.id = {&groupID}:
 
  if {&useClick} /* occuring when user double clicks row */
   then
    if not groupdetail.isOpen
     then
      do:
        create query hDataQuery.
        hDataQuery:add-buffer(hDataBuffer).
        hDataQuery:query-prepare("for each " + cDataTable + " no-lock where " + groupdetail.detailGroup  + " = '" + escapeQuote({&groupID}) + "' and " + cWhereClause).
        hDataQuery:query-open().
        hDataQuery:get-first().
        iDataCounter = 0.
        repeat while not hDataQuery:query-off-end:
          hGroupBuffer:buffer-create.
          hGroupBuffer:buffer-copy(hDataBuffer).
          assign
            iDataCounter = iDataCounter + 1
            hGroupBuffer:buffer-field("groupID"):buffer-value()    = hDataBuffer:buffer-field(cLowestDetailID):buffer-value()
            hGroupBuffer:buffer-field("groupName"):buffer-value()  = hDataBuffer:buffer-field(cLowestDetailName):buffer-value()
            hGroupBuffer:buffer-field("groupLevel"):buffer-value() = 2
            hGroupBuffer:buffer-field("sortOrder"):buffer-value()  = groupdetail.sortOrder + "." + string(iDataCounter, "99999999")
            .
          hDataQuery:get-next().
        end.
        hDataQuery:query-close().
        groupdetail.isOpen = true.
      end.
     else
      do:
        create query hGroupQuery.
        hGroupQuery:add-buffer(hGroupBuffer).
        hGroupQuery:query-prepare("for each groupdata no-lock where " + groupdetail.detailGroup  + " = '" + escapeQuote({&groupID}) + "' and groupLevel = 2").
        hGroupQuery:query-open().
        hGroupQuery:get-first().
        repeat while not hGroupQuery:query-off-end:
          hGroupBuffer:buffer-delete().
          hGroupQuery:get-next().
        end.
        hGroupQuery:query-close().
        groupdetail.isOpen = false.
      end.
   else
    do:
      if {&expandAll} and not groupdetail.isOpen
       then
        do:
          create query hDataQuery.
          hDataQuery:add-buffer(hDataBuffer).
          hDataQuery:query-prepare("for each " + cDataTable + " no-lock where " + groupdetail.detailGroup  + " = '" + escapeQuote({&groupID}) + "' and " + cWhereClause).
          hDataQuery:query-open().
          hDataQuery:get-first().
          iDataCounter = 0.
          repeat while not hDataQuery:query-off-end:
            hGroupBuffer:buffer-create.
            hGroupBuffer:buffer-copy(hDataBuffer).
            assign
              iDataCounter = iDataCounter + 1
              hGroupBuffer:buffer-field("groupID"):buffer-value()    = hDataBuffer:buffer-field(cLowestDetailID):buffer-value()
              hGroupBuffer:buffer-field("groupName"):buffer-value()  = hDataBuffer:buffer-field(cLowestDetailName):buffer-value()
              hGroupBuffer:buffer-field("groupLevel"):buffer-value() = 2
              hGroupBuffer:buffer-field("sortOrder"):buffer-value()  = groupdetail.sortOrder + "." + string(iDataCounter, "99999999")
              .
            hDataQuery:get-next().
          end.
          hDataQuery:query-close().
          groupdetail.isOpen = true.
        end.
        
      if not {&expandAll} and groupdetail.isOpen
       then
        do:
          create query hGroupQuery.
          hGroupQuery:add-buffer(hGroupBuffer).
          hGroupQuery:query-prepare("for each groupdata no-lock where " + groupdetail.detailGroup  + " = '" + escapeQuote({&groupID}) + "' and groupLevel = 2").
          hGroupQuery:query-open().
          hGroupQuery:get-first().
          repeat while not hGroupQuery:query-off-end:
            hGroupBuffer:buffer-delete().
            hGroupQuery:get-next().
          end.
          hGroupQuery:query-close().
          groupdetail.isOpen = false.
        end.
    end.
    
  /* copy group buffer to show buffer */
  hShowBuffer:empty-temp-table().
  create query hShowQuery.
  hShowQuery:add-buffer(hGroupBuffer).
  hShowQuery:query-prepare("for each groupdata by sortOrder").
  hShowQuery:query-open().
  hShowQuery:get-first().
  repeat while not hShowQuery:query-off-end:
    hShowBuffer:buffer-create().
    hShowBuffer:buffer-copy(hGroupBuffer).
    hShowBuffer:buffer-field(cShowIDColumn):buffer-value() = hGroupBuffer:buffer-field("groupID"):buffer-value().
    hShowBuffer:buffer-field(cShowNameColumn):buffer-value() = hGroupBuffer:buffer-field("groupName"):buffer-value().
    hShowBuffer:buffer-field(cShowLevelColumn):buffer-value() = hGroupBuffer:buffer-field("groupLevel"):buffer-value().
    hShowQuery:get-next().
  end.
end.
DoWait(false).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

