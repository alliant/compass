&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file loadconfig.i
@description Procedures to get and set the load configurations
  ----------------------------------------------------------------------*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 30.57
         WIDTH              = 68.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include


/* ***************************  Main Block  *************************** */ 

subscribe to "GetLoad"                    anywhere.
subscribe to "SetLoad"                    anywhere.

subscribe to "GetLoadAccounts"            anywhere.
subscribe to "SetLoadAccounts"            anywhere.

subscribe to "GetLoadAgents"              anywhere.
subscribe to "SetLoadAgents"              anywhere.

subscribe to "GetLoadAttorneys"           anywhere.
subscribe to "SetLoadAttorneys"           anywhere.

subscribe to "GetLoadContents"            anywhere.
subscribe to "SetLoadContents"            anywhere.

subscribe to "GetLoadCounties"            anywhere.
subscribe to "SetLoadCounties"            anywhere.

subscribe to "GetLoadDefaultEndorsements" anywhere.
subscribe to "SetLoadDefaultEndorsements" anywhere.

subscribe to "GetLoadOffices"             anywhere.
subscribe to "SetLoadOffices"             anywhere.

subscribe to "GetLoadPeriods"             anywhere.
subscribe to "SetLoadPeriods"             anywhere.

subscribe to "GetLoadRegions"             anywhere.
subscribe to "SetLoadRegions"             anywhere.

subscribe to "GetLoadStatCodes"           anywhere.
subscribe to "SetLoadStatCodes"           anywhere.

subscribe to "GetLoadStates"              anywhere.
subscribe to "SetLoadStates"              anywhere.

subscribe to "GetLoadStateForms"          anywhere.
subscribe to "SetLoadStateForms"          anywhere.

subscribe to "GetLoadSysCodes"            anywhere.
subscribe to "SetLoadSysCodes"            anywhere.

subscribe to "GetLoadSysProps"            anywhere.
subscribe to "SetLoadSysProps"            anywhere.

subscribe to "GetLoadSysUsers"            anywhere.
subscribe to "SetLoadSysUsers"            anywhere.

subscribe to "GetLoadTitleSoftware"       anywhere.
subscribe to "SetLoadTitleSoftware"       anywhere.

subscribe to "GetLoadVendors"             anywhere.
subscribe to "SetLoadVendors"             anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-GetLoad) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadAccounts Procedure 
PROCEDURE GetLoad :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pName as character no-undo.
  define output parameter pLoad as logical   no-undo.

  std-ch = getOption(pName).
  if std-ch = "" 
   then std-ch = getSetting(pName).
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then pLoad = false.
   else pLoad = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadAccounts) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadAccounts Procedure 
PROCEDURE GetLoadAccounts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadAccounts").
  if std-ch = "" 
   then std-ch = getSetting("LoadAccounts").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadActivities) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadActivities Procedure 
PROCEDURE GetLoadActivities :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadActivities", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadAgentApps) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadAgentApps Procedure 
PROCEDURE GetLoadAgentApps :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadAgentApps").
  if std-ch = "" 
   then std-ch = getSetting("LoadAgentApps").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadAgents) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadAgents Procedure 
PROCEDURE GetLoadAgents :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadAgents").
  if std-ch = "" 
   then std-ch = getSetting("LoadAgents").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadAlerts) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadAlerts Procedure 
PROCEDURE GetLoadAlerts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadAlerts").
  if std-ch = "" 
   then std-ch = getSetting("LoadAlerts").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadAttorneys) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadAttorneys Procedure 
PROCEDURE GetLoadAttorneys :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadAttorneys").
  if std-ch = "" 
   then std-ch = getSetting("LoadAttorneys").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadContents) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadContents Procedure 
PROCEDURE GetLoadContents :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadContentCodes").
  if std-ch = "" 
   then std-ch = getSetting("LoadContentCodes").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 or std-ch = "" 
   then std-lo = false.
   else std-lo = true.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadCounties) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadCounties Procedure 
PROCEDURE GetLoadCounties :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadCounties").
  if std-ch = "" 
   then std-ch = getSetting("LoadCounties").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadEndorsements) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadEndorsements Procedure 
PROCEDURE GetLoadEndorsements :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadDefaultEndorsements").
  if std-ch = "" 
   then std-ch = getSetting("LoadDefaultEndorsements").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadOffices) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadOffices Procedure 
PROCEDURE GetLoadOffices :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadOffices").
  if std-ch = "" 
   then std-ch = getSetting("LoadOffices").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadPeriods) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadPeriods Procedure 
PROCEDURE GetLoadPeriods :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadPeriods").
  if std-ch = "" 
   then std-ch = getSetting("LoadPeriods").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadRegions) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadRegions Procedure 
PROCEDURE GetLoadRegions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadRegions").
  if std-ch = "" 
   then std-ch = getSetting("LoadRegions").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadStatCodes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadStatCodes Procedure 
PROCEDURE GetLoadStatCodes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadStatCodes").
  if std-ch = "" 
   then std-ch = getSetting("LoadStatCodes").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadStates) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadStates Procedure 
PROCEDURE GetLoadStates :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadStates").
  if std-ch = "" 
   then std-ch = getSetting("LoadStates").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadStateForms) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadStateForms Procedure 
PROCEDURE GetLoadStateForms :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadStateForms").
  if std-ch = "" 
   then std-ch = getSetting("LoadStateForms").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadSysCodes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadSysCodes Procedure 
PROCEDURE GetLoadSysCodes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadSysCodes").
  if std-ch = "" 
   then std-ch = getSetting("LoadSysCodes").
  
  if std-ch = ""
   then
    do:
      std-ch = getOption("LoadCodes").
      if std-ch = "" 
       then std-ch = getSetting("LoadCodes").
    end.
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadSysProps) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadSysProps Procedure 
PROCEDURE GetLoadSysProps :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadSysProps").
  if std-ch = "" 
   then std-ch = getSetting("LoadSysProps").
  
  if std-ch = ""
   then
    do:
      std-ch = getOption("LoadProperties").
      if std-ch = "" 
       then std-ch = getSetting("LoadProperties").
    end.
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadSysUsers) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadSysUsers Procedure 
PROCEDURE GetLoadSysUsers :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadSysUsers").
  if std-ch = "" 
   then std-ch = getSetting("LoadSysUsers").
  
  if std-ch = ""
   then
    do:
      std-ch = getOption("LoadUsers").
      if std-ch = "" 
       then std-ch = getSetting("LoadUsers").
    end.
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadTitleSoftware) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadTitleSoftware Procedure 
PROCEDURE GetLoadTitleSoftware :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadTitleSoftware").
  if std-ch = "" 
   then std-ch = getSetting("LoadTitleSoftware").
  
  if std-ch = ""
   then
    do:
      std-ch = getOption("LoadTitleSoftware").
      if std-ch = "" 
       then std-ch = getSetting("LoadTitleSoftware").
    end.
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLoadVendors) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLoadVendors Procedure 
PROCEDURE GetLoadVendors :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical.
  std-ch = getOption("LoadVendors").
  if std-ch = "" 
   then std-ch = getSetting("LoadVendors").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoad) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoad Procedure 
PROCEDURE SetLoad :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pName as character no-undo.
  define input parameter pLoad as logical no-undo.
  
  setOption(pName, string(pLoad)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadAccounts) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadAccounts Procedure 
PROCEDURE SetLoadAccounts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadAccounts", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadActivities) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadActivities Procedure 
PROCEDURE SetLoadActivities :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadActivities", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadAgentApps) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadAgentApps Procedure 
PROCEDURE SetLoadAgentApps :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadAgentApps", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadAgents) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadAgents Procedure 
PROCEDURE SetLoadAgents :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadAgents", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadAlerts) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadAlerts Procedure 
PROCEDURE SetLoadAlerts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadAlerts", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadAttorneys) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadAttorneys Procedure 
PROCEDURE SetLoadAttorneys :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadAttorneys", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadContents) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadContents Procedure 
PROCEDURE SetLoadContents :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadContentCodes", string(std-lo)).
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadCounties) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadCounties Procedure 
PROCEDURE SetLoadCounties :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadCounties", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadEndorsements) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadEndorsements Procedure 
PROCEDURE SetLoadEndorsements :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadDefaultEndorsements", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadOffices) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadOffices Procedure 
PROCEDURE SetLoadOffices :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadOffices", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadPeriods) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadPeriods Procedure 
PROCEDURE SetLoadPeriods :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadPeriods", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadRegions) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadRegions Procedure 
PROCEDURE SetLoadRegions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadRegions", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadStatCodes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadStatCodes Procedure 
PROCEDURE SetLoadStatCodes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadStatCodes", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadStates) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadStates Procedure 
PROCEDURE SetLoadStates :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadStates", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadStateForms) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadStateForms Procedure 
PROCEDURE SetLoadStateForms :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadStateForms", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadSysCodes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadSysCodes Procedure 
PROCEDURE SetLoadSysCodes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadSysCodes", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadSysProps) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadSysProps Procedure 
PROCEDURE SetLoadSysProps :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadSysProps", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadSysUsers) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadSysUsers Procedure 
PROCEDURE SetLoadSysUsers :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadSysUsers", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadTitleSoftware) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadTitleSoftware Procedure 
PROCEDURE SetLoadTitleSoftware :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadTitleSoftware", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLoadVendors) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLoadVendors Procedure 
PROCEDURE SetLoadVendors :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("LoadVendors", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

