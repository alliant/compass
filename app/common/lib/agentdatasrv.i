&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file agentsrv.i
@description Data Server file for managing the agent

@author John Oliver
@created 04-17-2019
@notes Remade from commondatasrv.i

@Modification
Date          Name            Description
06-08-2020    Archana Gupta   Modified Logic for New Organization Structure.
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}
define variable tLoadedAgents as logical no-undo initial false.

/* tables */
{tt/agent.i}
{tt/esbmsg.i &tableAlias="agentesbmsg"}
define temp-table openAgent
  field agentID as character
  field agentHandle as handle
  .

/* temp tables */
{tt/agent.i &tableAlias="tempagent"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshAgent Include 
FUNCTION refreshAgent RETURNS LOGICAL
  ( input pAgentID as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* main procedures */
subscribe to "AgentChanged"       anywhere.
subscribe to "DeleteAgent"        anywhere.
subscribe to "GetAgent"           anywhere.
subscribe to "GetAgentName"       anywhere.
subscribe to "GetAgents"          anywhere.
subscribe to "GetProspectAgents"  anywhere.
subscribe to "GetStatusList"      anywhere.
subscribe to "LoadAgents"         anywhere.
subscribe to "NewAgent"           anywhere.
subscribe to "getAgentName"       anywhere.

/* used for opening/closing the agent data server */
subscribe to "CloseAgent"         anywhere.
subscribe to "OpenAgent"          anywhere.

/* used for the messaging system */
subscribe to "TIMER"              anywhere run-procedure "AgentTimer".
subscribe to "ExternalEsbMessage" anywhere run-procedure "AgentESB".

/* used to dump the data file */
subscribe to "DataDump"           anywhere run-procedure "AgentDataDump".

/* load agents */
std-lo = true.
publish "SetSplashStatus".
publish "GetLoadAgents" (output std-lo).
if std-lo
 then run LoadAgents.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentChanged Include 
PROCEDURE AgentChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pAgentID as character no-undo.
  
  /* refresh the agent */
  refreshAgent(pAgentID).
  
  /* publish that the data changed */
  publish "AgentDataChanged".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentDataDump Include 
PROCEDURE AgentDataDump :
/*------------------------------------------------------------------------------
@description Dump the agent and tempagent temp-tables into files
------------------------------------------------------------------------------*/
  /* variables */
  define variable tPrefix as character no-undo.
  
  /* get the prefix */
  publish "GetAppCode" (output tPrefix).
  tPrefix = tPrefix + "_".
  
  /* delete the current temp files */
  publish "DeleteTempFile" ("DataAgent", output std-lo).
  publish "DeleteTempFile" ("DataTempAgent", output std-lo).
  
  /* Active temp-tables */
  temp-table agent:write-xml("file", tPrefix + "Agent.xml").
  publish "AddTempFile" ("DataAgent", tPrefix + "Agent.xml").
  
  /* "Temp" temp-tables */
  temp-table tempagent:write-xml("file", tPrefix + "TempAgent.xml").
  publish "AddTempFile" ("DataAgent", tPrefix + "TempAgent.xml").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentESB Include 
PROCEDURE AgentESB :
/*------------------------------------------------------------------------------
@description Runs when an esb message is received
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey as character no-undo.
  
  /* exit if not correct entity */
  if pEntity <> "Agent"
   then next.
  
  /* get the next number */
  std-in = 1.
  for each agentesbmsg no-lock:
    std-in = std-in + 1.
  end.
  
  /* create the esb message */
  create agentesbmsg.
  assign
    agentesbmsg.seq    = std-in
    agentesbmsg.rcvd   = now
    agentesbmsg.entity = pEntity
    agentesbmsg.action = pAction
    agentesbmsg.keyID  = pKey
    .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentTimer Include 
PROCEDURE AgentTimer :
/*------------------------------------------------------------------------------
@description Triggered by appstart.i running on a timer
------------------------------------------------------------------------------*/
  /* buffers */
  define buffer agentesbmsg for agentesbmsg.

  /* loop through the esb messages */
  for each agentesbmsg exclusive-lock
  break by agentesbmsg.keyID
        by agentesbmsg.rcvd descending:

    /* if the agent is loaded and the we have a messages */
    if tLoadedAgents and first-of(agentesbmsg.keyID) 
     then run AgentChanged (agentesbmsg.keyID).
     
    /* delete the esb message and set the last sync */
    delete agentesbmsg.
    publish "SetLastSync".
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseAgent Include 
PROCEDURE CloseAgent :
/*------------------------------------------------------------------------------
@description Closes the agent data server procedure
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pAgentID as character no-undo.
  
  /* buffers */
  define buffer openAgent for openAgent.
  
  /* get the correct agent data server record */
  for first openAgent exclusive-lock
      where openAgent.agentID = pAgentID:
      
    /* delete the proceudre to the data server */
    if valid-handle(openAgent.agentHandle)
     then delete object openAgent.agentHandle.
    
    /* delete the agent data server record */
    delete openAgent.
  end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteAgent Include 
PROCEDURE DeleteAgent :
/*------------------------------------------------------------------------------
@description Delete an agent
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pAgentID as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  
  /* buffers */
  define buffer agent for agent.
  
  /* call the server */
  run server/deleteagent.p (input pAgentID,
                            output pSuccess,
                            output std-ch).

  /* if unsuccessful, show a message */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    /* otherwise, close the agent data server and delete the agent */
    for first agent exclusive-lock
        where agent.agentID = pAgentID:
      
      run CloseAgent (agent.agentID).
      delete agent.
      publish "AgentDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAgent Include 
PROCEDURE GetAgent :
/*------------------------------------------------------------------------------
@description Gets an agent
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pAgentID as character no-undo.
  define output parameter table for tempagent.
  
  /* buffers */
  define buffer tempagent for tempagent.

  /* get all the agents */
  empty temp-table tempagent.
  run GetAgents (output table tempagent).

  /* delete all agents that aren't the one passed in */
  for each tempagent exclusive-lock
     where tempagent.agentID <> pAgentID:
    
    delete tempagent.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAgentName Include 
PROCEDURE GetAgentName :
/*------------------------------------------------------------------------------
@description Gets an agent's name
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pAgentID as character no-undo.
  define output parameter pName    as character no-undo initial "Unknown".
  define output parameter pSuccess as logical   no-undo initial false.
  
  /* buffers */
  define buffer tempagent for tempagent.

  /* get all the agents */
  empty temp-table tempagent.
  run GetAgents (output table tempagent).

  /* delete all agents that aren't the one passed in */
  for first tempagent exclusive-lock
      where tempagent.agentID = pAgentID:
    
    pName = tempagent.name.
    pSuccess = true.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAgents Include 
PROCEDURE GetAgents :
/*------------------------------------------------------------------------------
@description Gets the agents
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for agent.

  /* if not loaded, load the agents */
  if not tLoadedAgents
   then run LoadAgents.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetProspectAgents Include 
PROCEDURE GetProspectAgents :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for tempagent.
   
  /* buffers */
  define buffer tempagent for tempagent.

  /* get the agents */
  empty temp-table tempagent.
  publish "GetAgents" (output table tempagent).
  
  /* delete the list of agents that are not prospects */
  for each tempagent no-lock
     where tempagent.stat <> "P":
    
    delete tempagent.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadAgents Include 
PROCEDURE LoadAgents :
/*------------------------------------------------------------------------------
@description Loads the Agents
------------------------------------------------------------------------------*/
  /* call the server */
  empty temp-table agent.
  run server/getagents.p (input  "ALL",
                          output table agent,
                          output tLoadedAgents,
                          output std-ch).

  /* if unsuccessful, show a message if debug is on */
  if not tLoadedAgents
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadAgents failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewAgent Include 
PROCEDURE NewAgent :
/*------------------------------------------------------------------------------
@description Creates a new agent
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter table for tempagent.
  define output parameter cOrgID  as character no-undo.
  define output parameter pHandle as handle    no-undo.
  define output parameter tNewAgentID as character no-undo.
  
  /* buffers */
  define buffer tempagent for tempagent.
  
  /* call the server */
  run server/newagent.p (input table tempagent,
                         output cOrgID,
                         output tNewAgentID,
                         output std-lo,
                         output std-ch).
                         
  /* if unsuccessful, show a message */
  if not std-lo
   then message std-ch view-as alert-box error.
   else
    do:
      /* otherwise, add the new agent details to the list of agents */
      for first tempagent no-lock:
        create agent.
        buffer-copy tempagent to agent.
        agent.agentID = tNewAgentID.
      end.
      run OpenAgent (tNewAgentID, output pHandle).
      publish "AgentDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenAgent Include 
PROCEDURE OpenAgent :
/*------------------------------------------------------------------------------
@description Loads the agent data server procedure
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pAgentID as character no-undo.
  define output parameter pHandle  as handle no-undo.
  
  /* buffers */
  define buffer openAgent for openAgent.
  
  /* if available, get the handle to the agent data server */
  if can-find(first openAgent where agentID = pAgentID)
   then 
    for first openAgent no-lock
        where openAgent.agentID = pAgentID:
        
      pHandle = openAgent.agentHandle.
    end.
   else 
    do:
      /* otherwise run the agent data server and create the record */
      run agentdatasrv.p persistent set pHandle (pAgentID).
      create openAgent.
      assign
        openAgent.agentID = pAgentID
        openAgent.agentHandle = pHandle
        .
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshAgent Include 
FUNCTION refreshAgent RETURNS LOGICAL
  ( input pAgentID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* variables */
  define variable lSuccess as logical no-undo.
  
  /* buffers */
  define buffer tempagent for tempagent.
  define buffer agent for agent.
  
  /* call the server */
  empty temp-table tempagent.
  run server/getagents.p (pAgentID,
                          output table tempagent,
                          output lSuccess,
                          output std-ch).

  /* if unsuccessful, show a message if debug is on */
  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshAgent failed: " + std-ch view-as alert-box warning.
    end.
   else
    /* otherwise, copy the agent details to the agent records */
    do:
      find tempagent where tempagent.agentID = pAgentID no-error.
      find agent where agent.agentID = pAgentID no-error.
      if not available tempagent and available agent /* delete */
       then delete agent.
      if available tempagent and not available agent /* new */
       then create agent.
      if available tempagent and available agent /* modify */
       then buffer-copy tempagent to agent.
    end.

  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

