&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------------
@description Moves an item order up or down in the same list
@returns True or false depending on the success of the move

@notes The function assumes that the widget uses list-item-pairs and not list-items
------------------------------------------------------------------------------*/
&IF DEFINED(MoveItemOrder) = 0 &THEN

FUNCTION MoveItemOrder RETURNS LOGICAL
  ( input hSelection as handle,
    input lOrderUp as logical,
    input pDelim as character ) :
/*------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
  define variable cValue as character no-undo.
  define variable cLabel as character no-undo.
  define variable cSelected as character no-undo.
  define variable iSelected as integer no-undo.
  define variable iPosition as integer no-undo.
  
  /* get the screen value from the box */
  cSelected = hSelection:screen-value.
  
  /* make sure that a value is present or present an error */
  if cSelected = ?
   then return false.
   
  if lOrderUp
   then
    do:
      /* don't move any item if the first item is in the selection */
      iPosition = 1.
      if not hSelection:lookup(entry(iPosition,cSelected,pDelim)) = 1
       then
        /* loop through all the selected entries */
        do iSelected = 1 to num-entries(cSelected,pDelim):
          /* get the entry */
          assign
            cValue = entry(iSelected,cSelected,pDelim)
            iPosition = hSelection:lookup(cValue)
            cLabel = entry(iPosition * 2 - 1,hSelection:list-item-pairs,pDelim)
            .
          
          hSelection:delete(iPosition).
          hSelection:insert(cLabel,cValue,iPosition - 1).
        end.
    end.
   else
    do:
      /* don't move any item if the last item is in the selection */
      iPosition = num-entries(cSelected,pDelim).
      if not hSelection:lookup(entry(iPosition,cSelected,pDelim)) = hSelection:num-items
       then
        /* loop through all the selected entries */
        do iSelected = num-entries(cSelected,pDelim) to 1 by -1:
          /* get the entry */
          assign
            cValue = entry(iSelected,cSelected,pDelim)
            iPosition = hSelection:lookup(cValue)
            cLabel = entry(iPosition * 2 - 1,hSelection:list-item-pairs,pDelim)
            .
          
          hSelection:delete(iPosition).
          hSelection:insert(cLabel,cValue,iPosition + 1).
        end.
    end.
   
  /* make the items selected again */
  hSelection:screen-value = cSelected.
  return true.   /* Function return value. */

END FUNCTION.
&GLOBAL-DEFINE MoveItemOrder true
&ENDIF


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


