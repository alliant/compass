&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* ***************************  Definitions  ************************** */

define variable lAddNewOrg as logical init true no-undo.

{lib/find-widget.i}

&if defined(addAll) = 0 &then
&scoped-define addAll false
&endif

&if defined(setEnable) = 0 &then
&scoped-define setEnable true
&endif

&if defined(innerLines) = 0 &then
&scoped-define innerLines 20
&endif

&if defined(maxLines) = 0 &then
&scoped-define maxLines 100
&endif

&if defined(addNew) = 0 &then
&scoped-define addNew false
lAddNewOrg = false.
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

&if defined(combo) <> 0 &then
{&combo}:delimiter = {&msg-dlm}.
&if defined(noPublish) = 0 &then
publish "Getorganizations" (output table organization).
&endif

/* create the text box */
create fill-in std-ha assign
  frame           = {&combo}:frame
  row             = {&combo}:row
  column          = {&combo}:column
  width           = {&combo}:width
  format          = "x(500)"
  screen-value    = ""
  sensitive       = true
  read-only       = not {&setEnable}
  visible         = true
  name            = "{&combo}organizationText"
  triggers:
    on value-changed run organizationComboList in this-procedure (self:screen-value).
    on return apply "return" to {&combo} in frame {&frame-name}.
    on mouse-select-click
    do:
      if not self:read-only
       then apply "VALUE-CHANGED" to self.
    end.
    on any-key 
    do:
      do with frame {&frame-name}:
        {&combo}:list-item-pairs = "ALL" + {&combo}:delimiter + "ALL".
        {&combo}:delete(1).
        if lAddNewOrg
         then
          {&combo}:list-item-pairs = "New Organization" + {&combo}:delimiter + "New Organization". 
      end.
    end.
  end triggers
  .
/* create a selection list widget (if not already there) */
if {&combo}:type = "COMBO-BOX"
 then
  do:
    create selection-list std-ha assign
      frame              = {&combo}:frame
      row                = {&combo}:row + {&combo}:height
      column             = {&combo}:column
      width              = {&combo}:width
      inner-lines        = 1
      scrollbar-vertical = true
      sensitive          = true
      visible            = false
      name               = "{&combo}organizationSelection"
      triggers:
        on return apply "mouse-select-click" to self.
        on mouse-select-click
        do:
          if not self:screen-value = "..."
           then run organizationComboSet in this-procedure (self:screen-value).
        end.
      end triggers
      .
    std-ha:delimiter = {&msg-dlm}.
    std-ha:list-item-pairs = "ALL" {&msg-add} "ALL".
    std-ha:delete(1).
    if lAddNewOrg
     then
      {&combo}:list-item-pairs = "New Organization" + {&combo}:delimiter + "New Organization".     
  end.
 else
  do:
    assign
      std-ha = {&combo}:handle
      std-ha:row = std-ha:row + 1.3
      std-ha:inner-lines = std-ha:inner-lines - 2
      .
  end.
/* trigger to close the organization selection list */
on entry anywhere
do:
  define variable hSelection as handle no-undo.
  define variable hText as handle no-undo.
  
  hSelection = GetWidgetByName({&combo}:frame in frame {&frame-name}, "{&combo}organizationSelection").
  hText = GetWidgetByName({&combo}:frame in frame {&frame-name}, "{&combo}organizationText").
  if valid-handle(hSelection) and valid-handle(hText)
   then
    if not self:name = "{&combo}organizationText" and not self:name = "{&combo}organizationSelection"
     then hSelection:visible = false.
     else
      do:
        if hSelection:list-item-pairs > "" and not hSelection:visible
         then hSelection:visible = true.
      end.
end.
on mouse-select-down of frame {&frame-name}
do:
  apply "ENTRY" to self.
end.
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE organizationComboClear Include 
PROCEDURE organizationComboClear :
/*------------------------------------------------------------------------------
@description Clears the selected organization from the organization list
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    std-ha = GetWidgetByName({&combo}:frame, "{&combo}organizationText").
    if valid-handle(std-ha)
     then std-ha:screen-value = "".
    
    {&combo}:list-item-pairs = "ALL" + {&combo}:delimiter + "ALL".
    {&combo}:delete(1).
    if lAddNewOrg
     then
      {&combo}:list-item-pairs = "New Organization" + {&combo}:delimiter + "New Organization".     
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE organizationComboDebug Include 
PROCEDURE organizationComboDebug :
/*------------------------------------------------------------------------------
@description Messages the information
------------------------------------------------------------------------------*/
  define variable hText as handle no-undo.
  define variable hSelection as handle no-undo.
  
  do with frame {&frame-name}:
    hText = GetWidgetByName({&combo}:frame, "{&combo}organizationText").
    hSelection = GetWidgetByName({&combo}:frame, "{&combo}organizationSelection").
    if valid-handle(hText) and valid-handle(hSelection)
     then message "Text Value: " + hText:screen-value skip
                  "Visible: " + string(hSelection:visible) skip
                  "List: " + hSelection:list-item-pairs view-as alert-box information buttons ok.
  end.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE organizationComboDeleteItem Include 
PROCEDURE organizationComboDeleteItem :
/*------------------------------------------------------------------------------
@description Set an organization in both the drop down and text box
------------------------------------------------------------------------------*/
define input parameter pNew as logical no-undo.

  lAddNewOrg =  pNew.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE organizationComboEnable Include 
PROCEDURE organizationComboEnable :
/*------------------------------------------------------------------------------
@description Enables/Disables the organization fill-in
------------------------------------------------------------------------------*/
  define input parameter pEnable as logical no-undo.
  
  do with frame {&frame-name}:
    std-ha = GetWidgetByName({&combo}:frame, "{&combo}organizationText").
    if valid-handle(std-ha)
     then
      do:
        std-ha:read-only = not pEnable.
        std-ha = GetWidgetByName({&combo}:frame, "{&combo}organizationSelection").
        if valid-handle(std-ha)
         then std-ha:visible = false.
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE organizationComboHide Include 
PROCEDURE organizationComboHide :
/*------------------------------------------------------------------------------
@description Hides/Shows the organization fill-in
------------------------------------------------------------------------------*/
  define input parameter pHide as logical no-undo.
  
  do with frame {&frame-name}:
    std-ha = GetWidgetByName({&combo}:frame, "{&combo}organizationText").
    if valid-handle(std-ha)
     then
      do:
        std-ha:visible = not pHide.
        std-ha = GetWidgetByName({&combo}:frame, "{&combo}organizationSelection").
        if valid-handle(std-ha)
         then std-ha:visible = false.
      end.
  end.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE organizationComboList Include 
PROCEDURE organizationComboList :
/*------------------------------------------------------------------------------
@description Get the organization list
------------------------------------------------------------------------------*/
  define input parameter porganizationFilter as character no-undo.
  define variable iorganizations as integer no-undo.
  define variable iFilter as integer no-undo.
  define variable hBox as handle no-undo.

  do with frame {&frame-name}:
    std-lo = {&combo}:type = "COMBO-BOX".
    if std-lo
     then hBox = GetWidgetByName({&combo}:frame, "{&combo}organizationSelection").
     else hBox = {&combo}:handle.
    
    if valid-handle(hBox)
     then
      do:
        assign
          hBox:visible = true
          hBox:list-item-pairs = "ALL" + hBox:delimiter + "ALL"
          iorganizations = (if {&addAll} then 1 else 0)
          .
        if lAddNewOrg
         then
          hBox:add-last("New Organization","New Organization").         
        for each organization no-lock
              by organization.Name:
          
          if porganizationFilter > "" and not organization.name matches "*" + porganizationFilter + "*"
           then next.
          
          &if defined(state) <> 0 &then
          if {&state}:screen-value <> "ALL" and {&state}:screen-value <> organization.stateID
           then next.
          &endif
          
          
          iorganizations = iorganizations + 1.
          if iorganizations > {&maxLines} - (if {&addAll} then 2 else 1)
           then next.
          
          hBox:add-last(organization.name + " (" + organization.orgID + ")", organization.orgID).
        end.

        /* add the ... option */
        if iorganizations > {&maxLines} - (if {&addAll} then 2 else 1)
         then
          do:
            hBox:add-last("...","...").
            if {&combo}:type = "COMBO-BOX"
             then hBox:inner-lines = minimum(iorganizations, minimum({&innerLines}, integer({&combo}:frame:height - {&combo}:row))).
          end.
         else
          do:
            if {&combo}:type = "COMBO-BOX"
             then
              do:
                hBox:inner-lines = minimum(iorganizations, minimum({&innerLines}, integer({&combo}:frame:height - {&combo}:row))).
                if iorganizations = 0
                 then hBox:visible = false.
              end.
          end.

        /* delete the ALL option */
        if not {&addAll}
         then hBox:delete(1).
      end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE organizationComboSet Include 
PROCEDURE organizationComboSet :
/*------------------------------------------------------------------------------
@description Set an organization in both the drop down and text box
------------------------------------------------------------------------------*/
  define input parameter porgID as character no-undo.
  define variable corgID as character no-undo.
  define variable corganizationName as character no-undo.
  define variable lFound as logical no-undo initial false.
  
  do with frame {&frame-name}:
    {&combo}:list-item-pairs = "ALL" + {&combo}:delimiter + "ALL".
    for first organization no-lock
      where organization.orgID = porgID:
      assign
        lFound = true
        corgID = organization.orgID
        corganizationName = organization.name
        .
    end.
    /* set the main combo */
    if lFound
     then
      assign
        {&combo}:list-item-pairs = corganizationName + {&combo}:delimiter + corgID
        {&combo}:screen-value = corgID
        .
     else if lAddNewOrg
      then
       assign
         {&combo}:list-item-pairs = "New Organization" + {&combo}:delimiter + "New Organization"
         {&combo}:screen-value    = "New Organization".
         
         
     else {&combo}:screen-value = "ALL".
  
    /* the textbox */
    std-ha = GetWidgetByName({&combo}:frame, "{&combo}organizationText").
    if valid-handle(std-ha)
     then std-ha:screen-value = (if lFound then corganizationName + " (" + corgID + ")" else (if {&addAll} then "ALL" else (if lAddNewOrg then "New Organization" else std-ha:screen-value))).
    
    /* the selection list */
    std-ha = GetWidgetByName({&combo}:frame, "{&combo}organizationSelection").
    if valid-handle(std-ha)
     then std-ha:visible = false.
    
    &if defined(noApply) = 0 &then
    if porgID > ""
     then apply "VALUE-CHANGED" to {&combo}.
    &endif
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE organizationComboState Include 
PROCEDURE organizationComboState :
/*------------------------------------------------------------------------------
@description The user selected a new state
------------------------------------------------------------------------------*/
  define variable hText as handle no-undo.
  define variable hSelection as handle no-undo.

  do with frame {&frame-name}:
    hText = GetWidgetByName({&combo}:frame, "{&combo}organizationText").
    hSelection = GetWidgetByName({&combo}:frame, "{&combo}organizationSelection").
    if valid-handle(hText)
     then
      do:
        run organizationComboClear in this-procedure.
        hText:screen-value = "".
      end.
  end.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

