&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/std-def.i
   D.Sinclair
   7.4.2012
   Scratch variables used in many routines.
   Do NOT assume std-* values persistent across include files.
   
   09.04.2015 jpo : Added std-dt for datetime scratch variable
   09.25.2019 Rahul : Added std-pb for progress bar
  ----------------------------------------------------------------------*/

&IF defined(std-def-defined) = 0 &THEN
def var std-ch as char no-undo.
def var std-lo as logical no-undo.
def var std-da as date no-undo.
def var std-dt as datetime no-undo.
def var std-in as integer no-undo.
def var std-de as decimal no-undo.
def var std-ha as handle no-undo.
def var std-ro as rowid no-undo.
def var std-lc as longchar no-undo.

/* Global min/max variables */
def var lc-max as char init "" no-undo.         /* Was "~177" but SQL didn't like it */
 assign lc-max = fill("z",255).
def var lc-min as char init "~000" no-undo.
def var li-max as int  init  999999999 no-undo.
def var li-min as int  init -999999999 no-undo.
def var lf-max as dec  init  999999999999 no-undo.
def var lf-min as dec  init -999999999999 no-undo.
def var ld-max as date init 12/31/32767 no-undo.
def var ld-min as date init 01/01/-32767 no-undo.

/* handles to hold the window and frame for includes */
def var hWindow as handle no-undo.
def var hFrame as handle no-undo.

/* Variables for browse row highlighting, column sorting, and row totaling */
def var colHandleList   as char    no-undo.
def var colHandle       as handle  no-undo.
def var rowColor        as int     no-undo.
def var hSortColumn     as widget-handle.
def var hQueryHandle    as handle  no-undo. 
def var tQueryString    as char    no-undo.
def var dataSortBy      as char    init "".
def var dataSortDesc    as log     init false.
def var tCanDo          as log no-undo init false.

/* to hold the nextkey.i stored-procedure handle */
def var iProcHandle     as integer no-undo.

/* Variable for progress bar */
define variable std-pb as handle no-undo.


&GLOBAL-DEFINE msg-dlm "^"
&GLOBAL-DEFINE msg-add + "^" +

&GLOBAL-DEFINE id-dlm "^"
&GLOBAL-DEFINE id-add + "^" +

&GLOBAL-DEFINE evenColor 32
&GLOBAL-DEFINE oddColor ?

&GLOBAL-DEFINE std-def-defined true
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 2.43
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


