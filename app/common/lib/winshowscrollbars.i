&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/winshowscrollbars.i
 */

/* 4GL wrapper procedure for ease of use */
PROCEDURE ShowScrollbars:
    DEFINE INPUT PARAMETER ip-Frame      AS HANDLE  NO-UNDO.
    DEFINE INPUT PARAMETER ip-horizontal AS LOGICAL NO-UNDO.
    DEFINE INPUT PARAMETER ip-vertical   AS LOGICAL NO-UNDO.

    DEFINE       VARIABLE  iv-retint     AS INTEGER NO-UNDO.

    &scoped-define SB_HORZ 0
    &scoped-define SB_VERT 1
    &scoped-define SB_BOTH 3
    &scoped-define SB_THUMBPOSITION 4

    RUN ShowScrollBar ( ip-Frame:HWND,
                        {&SB_HORZ},
                        IF ip-horizontal THEN -1 ELSE 0,
                        OUTPUT iv-retint ).

    RUN ShowScrollBar ( ip-Frame:HWND,
                        {&SB_VERT},
                        IF ip-vertical  THEN -1 ELSE 0,
                        OUTPUT iv-retint ).
    &undefine SB_HORZ
    &undefine SB_VERT
    &undefine SB_BOTH
    &undefine SB_THUMBPOSITION
END PROCEDURE.

/* Windows API entry point */
PROCEDURE ShowScrollBar EXTERNAL "user32":
    DEFINE INPUT  PARAMETER hwnd        AS LONG.
    DEFINE INPUT  PARAMETER fnBar       AS LONG.
    DEFINE INPUT  PARAMETER fShow       AS LONG.
    DEFINE RETURN PARAMETER ReturnValue AS LONG.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


