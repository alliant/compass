&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file notesTIMER.i
@description Procedures used control the notes history
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

&IF defined(entity) = 0 &THEN
&scoped-define entity ""
&ENDIF

&IF defined(entityID) = 0 &THEN
&scoped-define entityID ""
&ENDIF

&IF defined(subject) = 0 &THEN
&scoped-define subject "No Subject Assigned"
&ENDIF

&IF defined(category) = 0 &THEN
&scoped-define category "No Category Defined"
&ENDIF

&if defined(OCX) = 0 &then
&scoped-define OCX CtrlFrame
&endif

&if defined(Timer) = 0 &then
&scoped-define Timer PSTimer
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

subscribe to "TIMER" anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE TIMER Include 
PROCEDURE {&OCX}.{&Timer}.Tick :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  &IF defined(notes) = 0 &THEN
  return.
  &ENDIF
  
  publish "SetNoteHistory" ({&entity}, string({&entityID}), {&notes}, {&subject}, {&category}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveNoteHistory Include 
PROCEDURE CompleteNoteHistory :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  &IF defined(notes) = 0 &THEN
  return.
  &ENDIF
  
  publish "SetNoteHistory" ({&entity}, string({&entityID}), {&notes}, {&subject}, {&category}).
  publish "CompleteNoteHistory" ({&entity}, string({&entityID}), {&notes}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteNoteHistory Include 
PROCEDURE DeleteNoteHistory :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  publish "DeleteNoteHistory" ({&entity}, string({&entityID}), 0).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveNoteHistory Include 
PROCEDURE SaveNoteHistory :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  run {&OCX}.{&Timer}.Tick.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

