&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* ***************************  Definitions  ************************** */

&if defined(addAll) = 0 &then
&scoped-define addAll false
&endif

&if defined(firstValue) = 0 &then
&scoped-define firstValue "ALL,ALL"
&endif

&if defined(useFilter) = 0 &then
&scoped-define useFilter true
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* get the states */
if {&combo}:type = "COMBO-BOX"
 then {&combo}:inner-lines = 10.
{&combo}:list-item-pairs = entry(1, {&firstValue}) + {&combo}:delimiter + entry(2, {&firstValue}).

std-in = 0.
&if defined(noPublish) = 0 &then
publish "GetRegions" (output table region).
&endif
if {&useFilter}
 then
  do:
    std-ch = "".
    publish "GetAgentFilterStateList" (output std-ch). /* used to get only the states allowed to see */
    for each region exclusive-lock
    break by region.regionID:
      
      if first-of(region.regionID)
       then
        do:
          std-in = std-in + 1.
          if std-ch > "" and not can-find(first region where lookup(region.stateID, std-ch) > 0)
           then next.
          
          publish "GetRegionDescription" (region.regionID, output region.regionDesc).
          {&combo}:add-last(region.regionDesc, region.regionID).
        end.
    end.
  end.
 else
  for each region exclusive-lock
  break by region.regionID:
    
    if first-of(region.regionID)
     then
      do:
        std-in = std-in + 1.
        publish "GetSysPropDesc" ("AMD", "Region", "Category", region.regionID, output region.regionDesc).
        {&combo}:add-last(region.regionDesc, region.regionID).
      end.
  end.

if std-in > 0 and not {&addAll}
 then {&combo}:delete(1).

{&combo}:screen-value = entry(2, {&combo}:list-item-pairs, {&combo}:delimiter).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


