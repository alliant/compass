&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/win-main.i
   D.Sinclair
   9.21.2012
  */
  
{windows.i}
if {&window-name}:title = "" 
 then 
  do: publish "GetAppName" (output std-ch).
      {&window-name}:title = std-ch.
  end.
if index({&window-name}:title, ":|:") = 0 
 then 
  do:
    publish "GetServiceAddress" (output std-ch).
    if std-ch <> "" then
    do:
      if (std-ch matches "*WService=dvlp*" or std-ch matches "*WService=test*")
      then {&window-name}:title = "Compass (Test) :|: " + {&window-name}:title.
      else
      if (std-ch matches "*WService=beta*")
      then {&window-name}:title = "Compass (Beta) :|: " + {&window-name}:title.
      else {&window-name}:title = "Compass :|: " + {&window-name}:title.
    end.
    else {&window-name}:title = "Compass :|: " + {&window-name}:title.
  end.

publish "GetSmallLogo" (output std-ch).
if search(std-ch) <> ? 
 then {&window-name}:load-small-icon(std-ch).

std-lo = false.
publish "GetAppDebug" (output std-lo).
if std-lo 
 then {&window-name}:title = {&window-name}:title + " (" + this-procedure:file-name + ")".

/* Center window on screen */
assign
  {&window-name}:x = integer((session:width-pixels - {&window-name}:width-pixels) / 2)
  {&window-name}:y = integer((session:height-pixels - {&window-name}:height-pixels - 36) / 2)
  no-error.

/* Make this the default window for alert-boxes, dialogs, etc. launched from this program */
this-procedure:current-window = {&window-name}:handle.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


