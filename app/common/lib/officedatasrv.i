&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file sysinidatasrv.i
@description Data Server file for getting the server ini file

@author John Oliver
@created 04-22-2019
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}
define variable tLoadedOffices as logical initial false no-undo.

/* tables */
{tt/office.i}
{tt/esbmsg.i &tableAlias="officeesbmsg"}

/* temp tables */
{tt/office.i &tableAlias="tempoffice"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* main procedures */
subscribe to "OfficeChanged"     anywhere.
subscribe to "GetOffice"         anywhere.
subscribe to "GetOfficeName"     anywhere.
subscribe to "GetOffices"        anywhere.
subscribe to "LoadOffices"       anywhere.

/* used for the messaging system */
subscribe to "TIMER"              anywhere run-procedure "OfficeTimer".
subscribe to "ExternalEsbMessage" anywhere run-procedure "OfficeESB".

/* used to dump the data file */
subscribe to "DataDump"           anywhere run-procedure "OfficeDataDump".

/* load system users */
std-lo = true.
publish "SetSplashStatus".
publish "GetLoadOffices" (output std-lo).
if std-lo 
 then run LoadOffices.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetOffice Procedure 
PROCEDURE GetOffice :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pOfficeID as integer no-undo.
  define output parameter table for tempoffice.

  /* buffers */
  define buffer office for office.

  /* get the offices */
  empty temp-table tempoffice.
  run GetOffices (output table tempoffice).

  /* get correct office */
  for each tempoffice exclusive-lock
     where tempoffice.OfficeId <> pOfficeID:
     
    delete tempoffice.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetOfficeName Procedure 
PROCEDURE GetOfficeName :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pOfficeID   as integer   no-undo.
  define output parameter pOfficeName as character no-undo.

  /* buffers */
  define buffer office for office.

  /* get the offices */
  empty temp-table tempoffice.
  run GetOffices (output table tempoffice).

  /* get correct office */
  for first tempoffice exclusive-lock
      where tempoffice.OfficeId = pOfficeID:
     
    pOfficeName = tempoffice.officeName.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetOffices Include 
PROCEDURE GetOffices :
/*------------------------------------------------------------------------------
@description Gets the system ini records
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for office.

  /* load the ini records if not done already  */
  if not tLoadedOffices
   then run LoadOffices.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadOffices Include 
PROCEDURE LoadOffices :
/*------------------------------------------------------------------------------
@description Loads the offices
------------------------------------------------------------------------------*/
  /* call the server */
  empty temp-table office.
  run server/getoffices.p (output table office,
                           output tLoadedOffices,
                           output std-ch).

  /* if unsuccessful, show a message if debug */
  if not tLoadedOffices
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadOffices failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OfficeChanged Include 
PROCEDURE OfficeChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pOfficeID as character no-undo.
  
  /* refresh the alert */
  run LoadOffices.
  
  /* publish that the data changed */
  publish "OfficeDataChanged".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OfficeDataDump Include 
PROCEDURE OfficeDataDump :
/*------------------------------------------------------------------------------
@description Dump the office and tempoffice temp-tables into files
------------------------------------------------------------------------------*/
  /* variables */
  define variable tPrefix as character no-undo.
  
  /* get the prefix */
  publish "GetAppOffice" (output tPrefix).
  tPrefix = tPrefix + "_".
  
  /* delete the current temp files */
  publish "DeleteTempFile" ("DataOffice", output std-lo).
  publish "DeleteTempFile" ("DataTempOffice", output std-lo).
  
  /* Active temp-tables */
  temp-table office:write-xml("file", tPrefix + "Office.xml").
  publish "AddTempFile" ("DataOffice", tPrefix + "Office.xml").
  
  /* "Temp" temp-tables */
  temp-table tempoffice:write-xml("file", tPrefix + "TempOffice.xml").
  publish "AddTempFile" ("DataOffice", tPrefix + "TempOffice.xml").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OfficeESB Procedure 
PROCEDURE OfficeESB :
/*------------------------------------------------------------------------------
@description Runs when an esb message is received
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey as character no-undo.
  
  /* exit if not correct entity */
  if pEntity <> "Office"
   then next.
  
  /* get the next number */
  std-in = 1.
  for each officeesbmsg no-lock:
    std-in = std-in + 1.
  end.
  
  /* create the esb message */
  create officeesbmsg.
  assign
    officeesbmsg.seq    = std-in
    officeesbmsg.rcvd   = now
    officeesbmsg.entity = pEntity
    officeesbmsg.action = pAction
    officeesbmsg.keyID  = pKey
    .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OfficeTimer Include 
PROCEDURE OfficeTimer :
/*------------------------------------------------------------------------------
@description Triggered by appstart.i running on a timer
------------------------------------------------------------------------------*/
  /* buffers */
  define buffer officeesbmsg for officeesbmsg.

  /* loop through the esb messages */
  for each officeesbmsg exclusive-lock
  break by officeesbmsg.keyID
        by officeesbmsg.rcvd descending:

    /* if the office is loaded and the we have a messages */
    if tLoadedOffices and first-of(officeesbmsg.keyID) 
     then run OfficeChanged (officeesbmsg.keyID).
     
    /* delete the esb message and set the last sync */
    delete officeesbmsg.
    publish "SetLastSync".
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */
