&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file set-filter-def.i
@description Variables and functions for maintaining the filters

@author John Oliver
@created 10/12/2018
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* functions */
{lib/find-widget.i}
{lib/add-delimiter.i}
{lib/do-wait.i}

&if defined(tableName) = 0 &then
&scoped-define tableName data
&endif

/* variables */
{lib/std-def.i}

/* used to hold the fiters */
define temp-table filter
  field filterLabel       as character format "x(80)"
  field filterWidget      as handle           
  field filterWidgetName  as character format "x(80)"
  field filterColumnID    as character format "x(80)"
  field filterColumnValue as character format "x(80)"
  field filterUseSingle   as logical          
  field filterCurrValue   as character format "x(80)"
  field filterAddOption   as character format "x(1000)"
  field filterSet         as logical          
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setFilterCombos Include 
FUNCTION enableFilters RETURNS LOGICAL
  ( input pEnable as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getWhereClause Include 
FUNCTION getWhereClause RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setFilterCombos Include 
FUNCTION setFilterCombos RETURNS LOGICAL
  ( INPUT pName AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

empty temp-table filter.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION enableFilters Include 
FUNCTION enableFilters RETURNS LOGICAL
  ( input pEnable as logical ) :
/*------------------------------------------------------------------------------
@description Enable/disable the filters
------------------------------------------------------------------------------*/
  define variable hFilter as widget-handle no-undo.
  
  /* all other filters */
  for each filter no-lock:
    hFilter = filter.filterWidget.
    if valid-handle(hFilter)
     then
      case hFilter:type:
       when "FILL-IN" or
       when "EDITOR" then hFilter:read-only = not pEnable.
       otherwise hFilter:sensitive          = pEnable.
      end case.
  end.

  RETURN pEnable.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&if defined(noSort) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getWhereClause Include 
FUNCTION getWhereClause RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo initial "".
  define variable tInnerClause as character no-undo.
  
  define variable cColumn      as character no-undo.
  define variable hFilter      as handle no-undo.
  define variable i            as integer no-undo.
  
  /* set the values to what they were */
  for each filter no-lock:
    hFilter = filter.filterWidget.
    if valid-handle(hFilter) and hFilter:screen-value <> "ALL" and hFilter:screen-value <> ?
     then
      do:
        cColumn = filter.filterColumnID.
        if hFilter:type = "FILL-IN" or hFilter:type = "EDITOR"
         then
          do:
            do i = 1 to num-entries(cColumn, ":"):
              tInnerClause = addDelimiter(tInnerClause," or ") + "string(" + entry(i, cColumn, ":") + ") matches '*" + hFilter:screen-value + "*'".
            end.
            if tInnerClause > ""
             then tWhereClause = addDelimiter(tWhereClause, " and ") + "(" + tInnerClause + ")".
          end.
         else
          do:
            case hFilter:data-type:
             when "INTEGER"   or
             when "DECIMAL"   then tWhereClause = addDelimiter(tWhereClause, " and ") + cColumn + " = " + hFilter:screen-value.
             when "CHARACTER" then
              if hFilter:screen-value = ""
               then tWhereClause = addDelimiter(tWhereClause, " and ") + "(" + cColumn + " = ? or " + cColumn + " = '')".
               else tWhereClause = addDelimiter(tWhereClause, " and ") + cColumn + " = '" + hFilter:screen-value + "'".
             otherwise tWhereClause = addDelimiter(tWhereClause, " and ") + cColumn + " = '" + hFilter:screen-value + "'".
            end case.
          end.
      end.
  end.
  if tWhereClause > ""
   then tWhereClause = "where " + tWhereClause.
   
  RETURN tWhereClause.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setFilterCombos Include 
FUNCTION setFilterCombos RETURNS LOGICAL
  ( INPUT pName AS CHARACTER ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* filter buffer */
  define buffer filter for filter.

  /* handles */
  define variable hBuffer as handle    no-undo.
  define variable hQuery  as handle    no-undo.
  define variable hFilter as handle    no-undo.
  
  /* used to hold the values from the data */
  define variable cID     as character no-undo.
  define variable cValue  as character no-undo.
  define variable i       as integer   no-undo.

  doWait(true).  
  for each filter exclusive-lock:
    hFilter = filter.filterWidget.
    if valid-handle(hFilter)
     then
      do:
        filter.filterCurrValue = hFilter:screen-value.
        if filter.filterSet and (pName = "ALL" or (pName <> "ALL" and pName <> filter.filterLabel))
         then
          do:
            /* the first option is always ALL for a filter */
            if filter.filterUseSingle
             then hFilter:list-items = "ALL".
             else hFilter:list-item-pairs = "ALL" + hFilter:delimiter + "ALL".
             
            if filter.filterAddOption <> ""
             then
              do:
                if filter.filterUseSingle
                 then
                  do i = 1 to num-entries(filter.filterAddOption, ":"):
                    cID = entry(i, filter.filterAddOption, ":").
                    hFilter:add-last(cID).
                  end.
                 else
                  do i = 1 to num-entries(filter.filterAddOption, ":"):
                    assign
                      cID    = entry(2, entry(i, filter.filterAddOption, ":"))
                      cValue = entry(1, entry(i, filter.filterAddOption, ":"))
                      .
                    hFilter:add-last(cValue, cID).
                  end.
              end.
            
            /* query the data */
            create buffer hBuffer for table "{&tableName}".
            create query hQuery.
            hQuery:set-buffers(hBuffer).
            hQuery:query-prepare("for each {&tableName} no-lock " + getWhereClause() + " by " + filter.filterColumnID).
            hQuery:query-open().
            hQuery:get-first().
            repeat while not hQuery:query-off-end:
              do i = 1 to num-entries(filter.filterColumnID, ":"):
                if filter.filterUseSingle
                 then
                  do:
                    cID = hBuffer:buffer-field(entry(i, filter.filterColumnID, ":")):buffer-value().
                    if lookup(cID, hFilter:list-items) = 0 and cID <> "" and cID <> ?
                     then hFilter:add-last(cID).
                  end.
                 else
                  do:
                    assign
                      cID    = hBuffer:buffer-field(entry(i, filter.filterColumnID, ":")):buffer-value()
                      cValue = hBuffer:buffer-field(entry(i, filter.filterColumnValue, ":")):buffer-value()
                      .
                    if lookup(cID, hFilter:list-item-pairs) = 0 and cID <> "" and cID <> ? and cValue <> "" and cValue <> ?
                     then hFilter:add-last(cValue, cID).
                  end.
              end.
              hQuery:get-next().
            end.
            hQuery:query-close().
            
            /* set the current value */
            if filter.filterUseSingle
             then
              do:
                if lookup(filter.filterCurrValue, hFilter:list-items) = 0
                 then hFilter:screen-value = "ALL".
                 else hFilter:screen-value = filter.filterCurrValue.
                if lookup("Multiple", hFilter:list-items) > 0
                 then hFilter:sensitive = false.
                 else hFilter:sensitive = true.
              end.
             else
              do:
                if lookup(filter.filterCurrValue, hFilter:list-item-pairs) = 0
                 then hFilter:screen-value = "ALL".
                 else hFilter:screen-value = filter.filterCurrValue.
                if lookup("Multiple", hFilter:list-item-pairs) > 0
                 then hFilter:sensitive = false.
                 else hFilter:sensitive = true.
              end.
          end. /* if filterSet */
      end. /* if valid-handle */
  end. /* for each filter */
  output close.
  doWait(false).
  
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

