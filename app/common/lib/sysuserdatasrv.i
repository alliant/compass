&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file commondatasrv.i
@description Data Server file for common data elements

@param sysprop;char;Comma-delimited list of sysprops to get. The props are
                    separated by semicolon (:). The first is the appCode,
                    second is the objAction, and third is the objProperty
@param syscode;char;Comma-delimited list of syscodes to get
@param period;char;Either "Accounting" or "Batch"

@author John Oliver
@created 07-11-2018
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}
define variable tLoadedSysUsers as logical initial false no-undo.

/* tables */
{tt/sysuser.i}
{tt/esbmsg.i &tableAlias="sysuseresbmsg"}

/* temp tables */
{tt/sysuser.i &tableAlias="tempsysuser"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshSysUser Procedure
FUNCTION refreshSysUser RETURNS logical
  ( input pUserID as character ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* main procedures */
subscribe to "GetSysUser"         anywhere.
subscribe to "GetSysUsers"        anywhere.
subscribe to "GetSysUserEmail"    anywhere.
subscribe to "GetSysUserName"     anywhere.
subscribe to "GetSysUserRole"     anywhere.
subscribe to "LoadSysUsers"       anywhere.

/* questions */
subscribe to "IsAdmin"            anywhere.

/* used for the messaging system */
subscribe to "TIMER"              anywhere run-procedure "SysUserTimer".
subscribe to "ExternalEsbMessage" anywhere run-procedure "SysUserESB".

/* used to dump the data file */
subscribe to "DataDump"           anywhere run-procedure "SysUserDataDump".

/* load system users */
std-lo = true.
publish "SetSplashStatus".
publish "GetLoadSysUsers" (output std-lo).
if std-lo 
 then run LoadSysUsers.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSysUser Include 
PROCEDURE GetSysUser :
/*------------------------------------------------------------------------------
@description Gets the user
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pUID as character no-undo.
  define output parameter table for tempsysuser.
  
  /* buffers */
  define buffer tempsysuser for tempsysuser.

  /* get the users */
  empty temp-table tempsysuser.
  run GetSysUsers (output table tempsysuser).
  
  /* get the correct user */
  for each tempsysuser exclusive-lock
     where tempsysuser.uid <> pUID:
    
    delete tempsysuser.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSysUserEmail Include 
PROCEDURE GetSysUserEmail :
/*------------------------------------------------------------------------------
@description Gets the user's email
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pUID   as character no-undo.
  define output parameter pEmail as character no-undo initial "".
  
  /* buffers */
  define buffer tempsysuser for tempsysuser.

  /* get the user */
  empty temp-table tempsysuser.
  run GetSysUser (pUID, output table tempsysuser).

  /* get the user's email address */
  for first tempsysuser no-lock:
    pEmail = tempsysuser.email.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSysUserName Include 
PROCEDURE GetSysUserName :
/*------------------------------------------------------------------------------
@description Gets the user's name
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pUID  as character no-undo.
  define output parameter pName as character no-undo initial "".
  
  /* buffers */
  define buffer tempsysuser for tempsysuser.

  /* get the user */
  empty temp-table tempsysuser.
  run GetSysUser (pUID, output table tempsysuser).

  /* get the user's name */
  for first tempsysuser no-lock:
    pName = tempsysuser.name.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSysUserRole Include 
PROCEDURE GetSysUserRole :
/*------------------------------------------------------------------------------
@description Gets the user's role
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pUID  as character no-undo.
  define output parameter pRole as character no-undo initial "".
  
  /* buffers */
  define buffer tempsysuser for tempsysuser.

  /* get the user */
  empty temp-table tempsysuser.
  run GetSysUser (pUID, output table tempsysuser).

  /* get the user's role */
  for first tempsysuser no-lock:
    pRole = tempsysuser.role.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSysUsers Include 
PROCEDURE GetSysUsers :
/*------------------------------------------------------------------------------
@description Gets the system users
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for sysuser.

  /* get all the users */
  if not tLoadedSysUsers
   then run LoadSysUsers.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsAdmin Include 
PROCEDURE IsAdmin :
/*------------------------------------------------------------------------------
@description Checks if the current user is an admin
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter pIsAdmin as logical no-undo initial false.
  
  /* buffers */
  define buffer sysuser for sysuser.

  /* get the current user */
  std-ch = "".
  publish "GetCredentialsID" (output std-ch).

  /* check if the user is an admin */
  for first sysuser no-lock
      where sysuser.uid = std-ch:
    
    pIsAdmin = can-do(sysuser.role, "Administrator").
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadSysUsers Include 
PROCEDURE LoadSysUsers :
/*------------------------------------------------------------------------------
@description Loads the Compass users
------------------------------------------------------------------------------*/
  /* call the server */
  empty temp-table sysuser.
  run server/getsysusers.p (input  "B",    /* ActionType (A)ctive,(I)nactive,(B)oth */
                            input  "ALL",  /* UID */
                            input  ?,      /* StartDate*/
                            input  ?,      /* EndDate */
                            input "" ,     /* fieldList delimiter ","  */
                            output table sysuser,
                            output tLoadedSysUsers,
                            output std-ch).

  /* if unsuccessful, show a message if debug */
  if not tLoadedSysUsers
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadSysUsers failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysUserChanged Include 
PROCEDURE SysUserChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pUserID as character no-undo.
  
  /* refresh the alert */
  refreshSysUser(pUserID).
  
  /* publish that the data changed */
  publish "UserDataChanged".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysUserDataDump Include 
PROCEDURE SysUserDataDump :
/*------------------------------------------------------------------------------
@description Dump the sysuser and tempsysuser temp-tables into files
------------------------------------------------------------------------------*/
  /* variables */
  define variable tPrefix as character no-undo.
  
  /* get the prefix */
  publish "GetAppUser" (output tPrefix).
  tPrefix = tPrefix + "_".
  
  /* delete the current temp files */
  publish "DeleteTempFile" ("DataSysUser", output std-lo).
  publish "DeleteTempFile" ("DataTempSysUser", output std-lo).
  
  /* Active temp-tables */
  temp-table sysuser:write-xml("file", tPrefix + "SysUser.xml").
  publish "AddTempFile" ("DataSysUser", tPrefix + "SysUser.xml").
  
  /* "Temp" temp-tables */
  temp-table tempsysuser:write-xml("file", tPrefix + "TempSysUser.xml").
  publish "AddTempFile" ("DataSysUser", tPrefix + "TempSysUser.xml").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysUserESB Procedure 
PROCEDURE SysUserESB :
/*------------------------------------------------------------------------------
@description Runs when an esb message is received
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey as character no-undo.
  
  /* exit if not correct entity */
  if pEntity <> "SysUser"
   then next.
  
  /* get the next number */
  std-in = 1.
  for each sysuseresbmsg no-lock:
    std-in = std-in + 1.
  end.
  
  /* create the esb message */
  create sysuseresbmsg.
  assign
    sysuseresbmsg.seq    = std-in
    sysuseresbmsg.rcvd   = now
    sysuseresbmsg.entity = pEntity
    sysuseresbmsg.action = pAction
    sysuseresbmsg.keyID  = pKey
    .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysUserTimer Include 
PROCEDURE SysUserTimer :
/*------------------------------------------------------------------------------
@description Triggered by appstart.i running on a timer
------------------------------------------------------------------------------*/
  /* buffers */
  define buffer sysuseresbmsg for sysuseresbmsg.

  /* loop through the esb messages */
  for each sysuseresbmsg exclusive-lock
  break by sysuseresbmsg.keyID
        by sysuseresbmsg.rcvd descending:

    /* if the sysuser is loaded and the we have a messages */
    if tLoadedSysUsers and first-of(sysuseresbmsg.keyID) 
     then run SysUserChanged (sysuseresbmsg.keyID).
     
    /* delete the esb message and set the last sync */
    delete sysuseresbmsg.
    publish "SetLastSync".
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshSysUser Include 
FUNCTION refreshSysUser RETURNS logical
  ( input pUserID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* parameters */
  define variable lSuccess as logical no-undo.
  
  /* buffers */
  define buffer tempsysuser for tempsysuser.
  define buffer sysuser for sysuser.
  
  /* call the server */
  empty temp-table tempsysuser.
  run server/getsysusers.p (input  pUserID, 
                            input  "ALL",  /* UID */
                            input  ?,      /* StartDate*/
                            input  ?,      /* EndDate */
                            input "" ,     /* fieldList delimiter ","  */
                            output table tempsysuser,
                            output lSuccess,
                            output std-ch).

  /* if unsuccessful, show a message if debug is on */
  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshSysUser failed: " + std-ch view-as alert-box warning.
    end.
   else
    /* otherwise, copy the system code details into the system code records */
    do:
      find tempsysuser where tempsysuser.uid = pUserID no-error.
      find sysuser where sysuser.uid = pUserID no-error.
      if not available tempsysuser and available sysuser /* delete */
       then delete sysuser.
      if available tempsysuser and not available sysuser /* new */
       then create sysuser.
      if available tempsysuser and available sysuser /* modify */
       then buffer-copy tempsysuser to sysuser.
    end.
  
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

