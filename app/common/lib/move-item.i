&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------------
@description Moves an item between two selection boxes
@returns True or false depending on the success of the move

@notes The function assumes that the widget uses list-item-pairs and not list-items
------------------------------------------------------------------------------*/
&IF DEFINED(MoveItem) = 0 &THEN
FUNCTION MoveItem RETURNS LOGICAL
  ( input hFrom as handle,
    input hTo as handle,
    input pDelim as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cLabel as character no-undo.
  define variable cValue as character no-undo.
  define variable cSelected as character no-undo.
  define variable iSelected as int no-undo.
  define variable iCounter as int no-undo initial 0.
  define variable lFound as logical no-undo initial false.
  
  /* get the screen value from the box */
  cSelected = hFrom:screen-value.
  
  /* make sure that a value is present or present an error */
  if cSelected = ?
   then 
    do:
      MESSAGE "Please select a field to move over" VIEW-AS ALERT-BOX INFO BUTTONS OK.
      return false.
    end.
   else
    /* loop through all the selected entries (the entries are comma pDelimeted) */
    do iSelected = 1 to num-entries(cSelected,pDelim):
      /* get the entry */
      assign
        cValue = entry(iSelected,cSelected,pDelim)
        iCounter = 0
        lFound = false
        .
      /* because the list is comprised of a label and a value, the */
      /* dividing by two is the items in the list                  */
      repeat while iCounter < num-entries(hFrom:list-item-pairs,pDelim) / 2 and not lFound:
        iCounter = iCounter + 1.
        /* if the loop is on the first iteration, the screen value would be the */
        /* iteration value * 2 and the label would be iteration value * 2 - 1   */
        if cValue = entry(iCounter * 2, hFrom:list-item-pairs,pDelim)
         then 
          assign
            cLabel = entry(iCounter * 2 - 1, hFrom:list-item-pairs,pDelim)
            lFound = true
            .
      end.
      hTo:add-last(cLabel,cValue).
      hFrom:delete(iCounter).
    end.
  return true.   /* Function return value. */

END FUNCTION.
&GLOBAL-DEFINE MoveItem true
&ENDIF

&IF DEFINED(MoveItemList) = 0 &THEN
FUNCTION MoveItemList RETURNS LOGICAL
  ( input hFrom as handle,
    input hTo as handle,
    input pList as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cLabel as character no-undo.
  define variable cValue as character no-undo.
  define variable iSelected as int no-undo.
  define variable iCounter as int no-undo initial 0.
  define variable lFound as logical no-undo initial false.
  
  /* loop through all the selected entries (the entries are comma hFrom:delimitereted) */
  do iSelected = 1 to num-entries(pList, hFrom:delimiter):
    /* get the entry */
    assign
      cValue = entry(iSelected, pList, hFrom:delimiter)
      iCounter = 0
      lFound = false
      .
    /* because the list is comprised of a label and a value, the */
    /* dividing by two is the items in the list                  */
    repeat while iCounter < num-entries(hFrom:list-item-pairs, hFrom:delimiter) / 2 and not lFound:
      iCounter = iCounter + 1.
      /* if the loop is on the first iteration, the screen value would be the */
      /* iteration value * 2 and the label would be iteration value * 2 - 1   */
      if cValue = entry(iCounter * 2, hFrom:list-item-pairs, hFrom:delimiter)
       then 
        assign
          cLabel = entry(iCounter * 2 - 1, hFrom:list-item-pairs, hFrom:delimiter)
          lFound = true
          .
    end.
    hTo:add-last(cLabel,cValue).
    hFrom:delete(iCounter).
  end.
  return true.   /* Function return value. */

END FUNCTION.
&GLOBAL-DEFINE MoveItemList true
&ENDIF


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


