&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file get-temp-table.i
@description Gets the data from the server XML

@param t;char;The name of the table to get the buffer from

@author John Oliver
@created 10.16.2018
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

&if defined(get-temp-table-vars) = 0 &then
define variable setTableField  as integer no-undo.
define variable setTableAttr   as integer no-undo.
define variable setTableHandle as handle no-undo.
{lib/srvstartelement.i}
&global-define get-temp-table-vars true
&endif

&if defined(setParam) = 0 &then
&scoped-define setParam "{&t}"
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

setTableHandle = temp-table {&t}:DEFAULT-BUFFER-HANDLE.

if qName = {&setParam}
 then
  do:
    setTableHandle:buffer-create().
    /* we must loop through the XML attributes */
    do setTableField = 1 to setTableHandle:num-fields:
      do setTableAttr = 1 to attributes:num-items:
        if setTableHandle:buffer-field(setTableField):name = attributes:get-qname-by-index(setTableAttr)
         then
          do:
            fldvalue = attributes:get-value-by-index(setTableAttr).
            case setTableHandle:buffer-field(setTableField):data-type:
             when "character" then setTableHandle:buffer-field(setTableField):buffer-value() = if (fldvalue = "?" or fldvalue = ?) then ""   else decodexml(fldvalue).  
             when "integer"   then setTableHandle:buffer-field(setTableField):buffer-value() = if (fldvalue = "?" or fldvalue = ?) then 0    else integer(fldvalue)  no-error.
             when "decimal"   then setTableHandle:buffer-field(setTableField):buffer-value() = if (fldvalue = "?" or fldvalue = ?) then 0    else decimal(fldvalue)  no-error.
             when "logical"   then setTableHandle:buffer-field(setTableField):buffer-value() = if (fldvalue = "?" or fldvalue = ?) then no   else logical(fldvalue)  no-error. 
             when "datetime"  then setTableHandle:buffer-field(setTableField):buffer-value() = if (fldvalue = "?" or fldvalue = ?) then ?    else datetime(fldvalue) no-error. 
             when "date"      then setTableHandle:buffer-field(setTableField):buffer-value() = if (fldvalue = "?" or fldvalue = ?) then ?    else date(fldvalue)     no-error. 
            end case.
          end.
      end.
    end.
  end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


