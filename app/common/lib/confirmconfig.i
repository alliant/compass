&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file directoryconfig.i
@description Procedures to set the confirm configurations
  ----------------------------------------------------------------------*/


/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 30.57
         WIDTH              = 68.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include


/* ***************************  Main Block  *************************** */ 

subscribe to "GetConfirmClose"        anywhere.
subscribe to "SetConfirmClose"        anywhere.

subscribe to "GetConfirmDelete"       anywhere.
subscribe to "SetConfirmDelete"       anywhere.

subscribe to "GetConfirmExit"         anywhere.
subscribe to "SetConfirmExit"         anywhere.

subscribe to "GetConfirmFileUpload"   anywhere.
subscribe to "SetConfirmFileUpload"   anywhere.

subscribe to "GetConfirmStatusChange" anywhere.
subscribe to "SetConfirmStatusChange" anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-GetConfirmClose) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetConfirmClose Procedure 
PROCEDURE GetConfirmClose :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical no-undo.
  std-ch = getOption("ConfirmClose").
  if std-ch = "" 
   then std-ch = getSetting("ConfirmClose").
 
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetConfirmDelete) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetConfirmDelete Procedure 
PROCEDURE GetConfirmDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical no-undo.
  std-ch = getOption("ConfirmDelete").
  if std-ch = "" 
   then std-ch = getSetting("ConfirmDelete").
 
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetConfirmExit) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetConfirmExit Procedure 
PROCEDURE GetConfirmExit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical no-undo.
  std-ch = getOption("ConfirmExit").
  if std-ch = "" 
   then std-ch = getSetting("ConfirmExit").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetConfirmFileUpload) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetConfirmFileUpload Procedure 
PROCEDURE GetConfirmFileUpload :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical no-undo.
  std-ch = getOption("ConfirmFileUpload").
  if std-ch = "" 
   then std-ch = getSetting("ConfirmFileUpload").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetConfirmStatusChange) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetConfirmStatusChange Procedure 
PROCEDURE GetConfirmStatusChange :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo as logical no-undo.
  std-ch = getOption("ConfirmStatusChange").
  if std-ch = "" 
   then std-ch = getSetting("ConfirmStatusChange").
  
  if lookup(std-ch, "no,N,False,F,0") > 0 
   then std-lo = false.
   else std-lo = true.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetConfirmClose) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetConfirmClose Procedure 
PROCEDURE SetConfirmClose :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("ConfirmClose", string(std-lo)).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetConfirmDelete) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetConfirmDelete Procedure 
PROCEDURE SetConfirmDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("ConfirmDelete", string(std-lo)).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetConfirmExit) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetConfirmExit Procedure 
PROCEDURE SetConfirmExit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("ConfirmExit", string(std-lo)).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetConfirmFileUpload) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetConfirmFileUpload Procedure 
PROCEDURE SetConfirmFileUpload :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("ConfirmFileUpload", string(std-lo)).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetConfirmStatusChange) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetConfirmStatusChange Procedure 
PROCEDURE SetConfirmStatusChange :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter std-lo as logical no-undo.
  setOption("ConfirmStatusChange", string(std-lo)).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF
