&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/resizequestion.i
   Move the fields around for the specified sequence based on a window
   resize event.  The preprocessors below are defined in lib/question.i
   or lib/questionf.i 

   seq:    Number for variable q<seq>Y, etc.

    Modification :
    Date          Name      Description
    11/17/2016    AG        Implement resize question description and tooltip
                            functionality
    01/17/2017    AG        Fixed width char of question 20000  
    09/24/2021    SA        Task#:86696 Defects raised by QA and set question 
                            text width
  ----------------------------------------------------------------------*/
   
/* added to get question description  */   
&IF defined(exclude-it) = 0 &then
  def var tQuestionID as char no-undo.
  def var tQuestionDesc as char no-undo.
  def var tQuestionPriority as int no-undo.
  def var tQuestionWarningAnswer as char no-undo.
  def var tQuestionWarningMsg as char no-undo.
  def var tQuestionWarningTip as char no-undo.
  def var tQuestionFrameHeight as int no-undo.
  def var questionpriority as integer no-undo .
  def var tQuestiontoDisplay as char no-undo.
  def var qsize as integer no-undo.
&global-define exclude-it true 
&ENDIF  

&IF defined(var-id) = 0 &THEN
  &scoped-define var-id tQuestionID
&ENDIF

&IF defined(var-desc) = 0 &THEN
  &global-define var-desc tQuestionDesc
&ENDIF

&IF defined(var-priority) = 0 &THEN
  &scoped-define var-priority tQuestionPriority
&ENDIF

&IF defined(var-warninganswer) = 0 &THEN
  &SCOPED-DEFINE var-warninganswer tQuestionWarningAnswer
&ENDIF

&IF defined(var-warningmsg) = 0 &THEN
  &SCOPED-DEFINE var-warningmsg tQuestionWarningMsg
&ENDIF

&IF defined(var-warningtip) = 0 &THEN
  &SCOPED-DEFINE var-warningtip tQuestionWarningTip
&ENDIF

&IF defined(questionType) = 0 &THEN
  &scoped-define questionType Question
&ENDIF

publish "Get{&questionType}Attributes" (input {&seq},
                                 output {&var-id},
                                 output {&var-desc},
                                 output {&var-priority},
                                 output std-lo,
                                 output {&var-warninganswer},
                                 output {&var-warningmsg},
                                 output {&var-warningtip}).

{&var-desc} = replace({&var-desc},'&','&&').

if {&var-priority} eq 0 then
 {&var-desc} = trim({&var-desc} + "" + fill("*", {&var-priority})).
else
 {&var-desc} = trim({&var-desc} + "" + fill("*", {&var-priority} - 1)).

/* Question text */
{&t{&seq}Resize}

/* Warning text */
{&w{&seq}Resize}

/* Fill-in field  (lib/questionf.i) */
{&q{&seq}Resize}

/* Toggle-boxes (Y)es, (N)o, N/(A), and (U)nknown */
{&q{&seq}yResize}
{&q{&seq}nResize}
{&q{&seq}aResize}
{&q{&seq}uResize}

/* Finding/best practice button */
{&b{&seq}Resize}

/* Rectangle */
{&r{&seq}Resize}

qsize = integer( t{&seq}:width-chars).
if  (length({&var-desc}) > qsize - 36 )
 then
  do:
    if  ( {&var-priority} eq 0 ) or ( {&var-priority} eq 1 ) then
    do:
      tQuestiontoDisplay = substring({&var-desc}, 1,  r-index({&var-desc}, " ", qsize - 15 ) - 1 ).
      tQuestiontoDisplay = trim(tQuestiontoDisplay + "" + fill(".", 3)).
      t{&seq}:screen-value = tQuestiontoDisplay .
      t{&seq}:tooltip = "..." + substring({&var-desc}, r-index({&var-desc}, " ", qsize - 15 ),length({&var-desc}) ).
    end.
      
    if ( {&var-priority} eq 2 )  then
    do:
      tQuestiontoDisplay = substring({&var-desc}, 1,  r-index({&var-desc}, " ", qsize - 17 ) - 1 ).
      tQuestiontoDisplay = trim(tQuestiontoDisplay + "" + fill(".", 3)).
      t{&seq}:screen-value = tQuestiontoDisplay .
      t{&seq}:tooltip =  "..." + substring({&var-desc}, r-index({&var-desc}, " ", qsize - 17 ) ,length({&var-desc}) ).
    end.
     
    if ( {&var-priority} eq 3 )  then
    do:
      tQuestiontoDisplay = substring({&var-desc}, 1,  r-index({&var-desc}, " ", qsize - 30 ) - 1 ).
      tQuestiontoDisplay = trim(tQuestiontoDisplay + "" + fill(".", 3)).
      t{&seq}:screen-value = tQuestiontoDisplay .
      t{&seq}:tooltip = "..." +  substring({&var-desc}, r-index({&var-desc}, " ", qsize - 30 ) ,length({&var-desc}) ).
    end.
  end.
 else 
  do:
    t{&seq}:screen-value = {&var-desc}.
    t{&seq}:tooltip = "" .
    {&t{&seq}Resize}
  end.
  
if {&window-name}:window-state = 1 or {&window-name}:window-state = 3
 then
  do:
    if {&seq} = 20000 then
     t{&seq}:width-chars = 30.
  end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


