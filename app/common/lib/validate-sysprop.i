&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{lib/std-def.i}
{lib/add-delimiter.i}

&IF defined(appCode) = 0 &THEN
&scoped-define appCode ""
&ENDIF

&IF defined(objAction) = 0 &THEN
&scoped-define objAction ""
&ENDIF

&IF defined(objProperty) = 0 &THEN
&scoped-define objProperty ""
&ENDIF

&IF defined(err) = 0 &THEN
&scoped-define err std-lo
&ENDIF

&IF defined(out) = 0 &THEN
&scoped-define out std-ch
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

{&err} = false.
{&out} = "".
for each sysprop no-lock
   where sysprop.appCode = {&appCode}
     and sysprop.objAction = {&objAction}
     and sysprop.objProperty = {&objProperty}:
  
  /* create the error message */
  std-in = index(sysprop.objValue, sysprop.objID).
  if std-in = 0
   then {&out} = addDelimiter({&out}, ", ") + "(" + sysprop.objID + ")" + sysprop.objValue.
   else {&out} = addDelimiter({&out}, ", ") + substring(sysprop.objValue, 1, std-in - 1) + "(" + sysprop.objID + ")" + substring(sysprop.objValue, length(sysprop.objID) + 1).
  
  /* check if valid */
  if sysprop.objID = {&objID}
   then {&err} = true.
end.

std-in = r-index({&out}, ",").
{&out} = substring({&out}, 1, std-in) + " or" + substring({&out}, std-in + 1).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


