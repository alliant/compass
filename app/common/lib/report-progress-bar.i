&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file report-progress-bar.i
@description Procedures to manage a report progress bar

@param counter;int;The maximum counter (defaults to 2)

@author John Oliver
@created 9/17/2018
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* for the progress bar */
define variable iCounter as integer no-undo initial 0.

&IF defined(counter) = 0 &THEN
&scoped-define counter 2
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbMinStatus Include 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbUpdateStatus Include 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( input pPercentage as decimal,
    input pPauseSeconds as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetProgressEnd Include 
PROCEDURE SetProgressEnd :
/*------------------------------------------------------------------------------
@description Makes the progress bar minimum after setting to 100
------------------------------------------------------------------------------*/
  iCounter = 0.
  pbUpdateStatus(100, 1).
  pbMinStatus().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetProgressStatus Include 
PROCEDURE SetProgressStatus :
/*------------------------------------------------------------------------------
@description Updates the progress bar
------------------------------------------------------------------------------*/
  iCounter = iCounter + 1.
  pbUpdateStatus(int(iCounter / {&counter} * 100), 0).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbMinStatus Include 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
@description Sets the progress bar back to the minimum value
------------------------------------------------------------------------------*/
  chCtrlFrame:ProgressBar:VALUE = chCtrlFrame:ProgressBar:MIN.
  RETURN TRUE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbUpdateStatus Include 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( input pPercentage as decimal,
    input pPauseSeconds as integer ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  {&WINDOW-NAME}:move-to-top().
  do with frame {&frame-name}:
    if chCtrlFrame:ProgressBar:VALUE <> pPercentage then
    assign
      chCtrlFrame:ProgressBar:VALUE = pPercentage.
      
    if pPauseSeconds > 0
     then
      do:
        session:set-wait-state("general").
        pause pPauseSeconds no-message.
        session:set-wait-state("").
      end.
  end.
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

