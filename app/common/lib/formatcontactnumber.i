/*------------------------------------------------------------------------
    File        : lib/formatcontactnumber.i
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : Sachin Chaturvedi
    Created     : 12/21/22
    Notes       : Make sure to reset the format of the contact number
                  fill-in on entry trigger.
                  
                  Use &noMessage = true when using this .i while displaying data
                  Use &includedBefore = true when using it second or more time in a procedure.
    Modify     
    Name             Date                comments
    SRK              04/09/24            Changed to function to return formatted value.
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

function Validatenumber returns character 
 (input ipccontactnumber as char , output opcvalidcontactnumber as char, output oplerror as logical ):

define variable fPh-Count           as integer   no-undo.
define variable fPh-ValidCharsPhone as character no-undo.
define variable fPh-TempChar        as character no-undo.
define variable fPh-FirstLetter     as character no-undo.

define variable icount           as integer   no-undo.
define variable cvalue           as character no-undo.
define variable cvalue1           as character no-undo.
define variable cvalue2           as character no-undo.
define variable cnumber           as character no-undo.



assign
    fPh-ValidCharsPhone = "0,1,2,3,4,5,6,7,8,9"
    fPh-FirstLetter     = substring(ipccontactnumber,1,1)
    fPh-Count           = 1
    fPh-TempChar        = ''
    .

/* contact number is not blank and first letter should be + */
if ipccontactnumber = '' or fPh-FirstLetter eq '+' 
   then
    opcvalidcontactnumber = ipccontactnumber.
   else
    do:
      /*removing unformatting to formatting */
      do fPh-Count = 1 to length(ipccontactnumber):
        fPh-TempChar = substring(ipccontactnumber,fPh-Count,1).
        if lookup(fPh-TempChar,fPh-ValidCharsPhone,',') > 0  
         then
          opcvalidcontactnumber = opcvalidcontactnumber + fPh-TempChar.
      end.
      
      fPh-FirstLetter  = substring(opcvalidcontactnumber,1,1)   .
      
      if fPh-FirstLetter = '1'
       then
        opcvalidcontactnumber = substring(opcvalidcontactnumber,2,(length(opcvalidcontactnumber) - 1)).
    
      /*validate contact number*/
      if length(opcvalidcontactnumber) ne 10 
       then
        do:
          oplerror  = true.
          return "".
        end.
        
       opcvalidcontactnumber = "(" + substring(opcvalidcontactnumber,1,3) + ") " + substring(opcvalidcontactnumber,4,3) + "-" + substring(opcvalidcontactnumber,7,4).        
   end.
   
   

 
  return "".
 end function.
  


