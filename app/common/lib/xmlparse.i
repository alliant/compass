&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* xml/xmlparse.i
   PARSE an XML response from the Compass server
   Author(s)   : D.Sinclair
   Created     : 1.24.2012
   Notes       : &xmlFile
                 &traceFile
                 &parseAction  :  when clause of element name
                 &include-trace:  non-blank to call trace() function

                 Uses parseStatus to indicate success/failure of parse.
                 Uses parseMsg to return the msg to be presented to the user.
                 Uses parser to hold the handle to the SAX parser.
 */

def var parseCode as char no-undo.
def var parseStatus as logical no-undo.
def var parseMsg as char no-undo.
def var parser as handle no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-decodeUrl) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD decodeUrl Procedure 
FUNCTION decodeUrl RETURNS CHARACTER
  ( input cValue as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-decodeXml) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD decodeXml Procedure 
FUNCTION decodeXml RETURNS CHARACTER PRIVATE
  ( pEncoded as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-encodeUrl) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD encodeUrl Procedure 
FUNCTION encodeUrl RETURNS CHARACTER
  ( input cValue as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-encodeXml) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD encodeXml Procedure 
FUNCTION encodeXml RETURNS CHARACTER PRIVATE
  (pUnencoded as char)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parseXml) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD parseXml Procedure 
FUNCTION parseXml RETURNS LOGICAL PRIVATE
  ( output pMsg as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-trace) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD trace Procedure 
FUNCTION trace RETURNS LOGICAL PRIVATE
  ( input pTraceMsg as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* NOTE:  The assumption is that parseStatus is set to true and this means
          that parsing was successful.  If you need to indicate that it's 
          failed, you must set parseStatus = false.
 */
 
&IF defined(exclude-startElement) = 0 &THEN
 procedure startElement:
  DEFINE INPUT PARAMETER namespaceURI AS CHARACTER.
  DEFINE INPUT PARAMETER localName AS CHARACTER.
  DEFINE INPUT PARAMETER qName AS CHARACTER.
  DEFINE INPUT PARAMETER attributes AS HANDLE.
 
  DEF VAR i AS INT.
  DEF VAR fldvalue AS CHAR.
 
  case qName: 
   when "Fault" 
    then 
     do:
         DO  i = 1 TO  attributes:NUM-ITEMS:
          fldvalue = attributes:get-value-by-index(i).
          CASE attributes:get-qname-by-index(i):
              WHEN "code" THEN
                  parseCode = fldvalue.
              WHEN "message" THEN
                  parseMsg = fldvalue.
          END CASE.
         END.

         &IF defined(include-trace) > 0 &THEN
         trace(parseMsg + "(" + parseCode + ")").
         &ENDIF

         parseStatus = false.
     end.
   {&parseAction}
  end case.
 END PROCEDURE.
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-error) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE error Procedure 
PROCEDURE error :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  pErr as char
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pErr as char.

 assign
   parseCode = "998"
   parseStatus = false
   parseMsg = pErr
   .

 &IF defined(include-trace) > 0 &THEN
 trace(pErr + " (SAX Error)").
 &ENDIF

 return error. /* Stop parsing */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-fatalError) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fatalError Procedure 
PROCEDURE fatalError :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  pErr as char
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pErr as char.

 assign
   parseCode = "999"
   parseStatus = false
   parseMsg = pErr
   .

 &IF defined(include-trace) > 0 &THEN
 trace(pErr + " (SAX Fatal Error)").
 &ENDIF

 return error. /* Stop parsing */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-decodeUrl) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION decodeUrl Procedure 
FUNCTION decodeUrl RETURNS CHARACTER
  ( input cValue as char ) :

/**************************************************************************** 
Description: Decodes unsafe characters in a URL as per RFC 1738 section 2.2. 
<URL:http://ds.internic.net/rfc/rfc1738.txt>, 2.2 
Input Parameters: Encoded character string to decode, 
In addition, all characters specified in the variable cUnsafe plus ASCII values 0 <= x <= 31 and 127 <= x <= 255 are considered unsafe. 
Returns: Decoded string (unkown value is returned as blank) 
****************************************************************************/ 
DEFINE VARIABLE cHex        AS CHARACTER   NO-UNDO INITIAL "0123456789ABCDEF":U. 
DEFINE VARIABLE iCounter    AS INTEGER     NO-UNDO. 
DEFINE VARIABLE cChar       AS INTEGER     NO-UNDO. 
define variable cTriplet    as character   no-undo.

/* Unsafe characters that must be encoded in URL's.  See RFC 1738 Sect 2.2. */
DEFINE VARIABLE cUnsafe   AS CHARACTER NO-UNDO INITIAL " <>~"#%~{}|~\^~~[]`":U.

/* Reserved characters that normally are not encoded in URL's */
DEFINE VARIABLE cReserved AS CHARACTER NO-UNDO INITIAL "~;/?:@=&":U.

/* Don't bother with blank or unknown */ 
IF LENGTH(cValue) EQ 0 OR 
   cValue         EQ ? THEN 
  RETURN "". 

/* Loop through entire input string */ 
iCounter = 0. 
DO WHILE TRUE: 
  ASSIGN iCounter = iCounter + 1 
         /* ASCII value of character using single byte codepage */ 
         cChar = ASC(SUBSTRING(cValue, iCounter, 1, "RAW":U), 
                     "1252":U, 
                     "1252":U). 

  if chr(cChar) = "%" then
  do:
    assign 
        cTriplet = substring(cValue, iCounter, 3, "RAW":U)
        cChar    = ((index(cHex,substring(cTriplet,2,1)) - 1) * 16) + /* high hex digit */
                   (index(cHex,substring(cTriplet,3,1)) - 1).         /* low hex digit */
                   
    substring(cValue, iCounter, 3, "RAW":U) = chr(cChar).    
  end.
  
  IF iCounter = LENGTH(cValue,"RAW":U) THEN LEAVE. 
END. 

return cValue.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-decodeXml) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION decodeXml Procedure 
FUNCTION decodeXml RETURNS CHARACTER PRIVATE
  ( pEncoded as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 pEncoded = replace(pEncoded, "&amp;", "&").
 pEncoded = replace(pEncoded, "&#9;", chr(9)).
 /*pEncoded = replace(pEncoded, "&br;", "  ").*/
 /*pEncoded = replace(pEncoded, "&br;", chr(10)).*/
 pEncoded = replace(pEncoded, "&br;", chr(13) + chr(10)).
 pEncoded = replace(pEncoded, "&apos;", "'").
 pEncoded = replace(pEncoded, "&quot;", "'").
 pEncoded = replace(pEncoded, "&gt;", ">").
 pEncoded = replace(pEncoded, "&lt;", "<").
 pEncoded = replace(pEncoded, "&#13;", chr(10)). 

 RETURN pEncoded.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-encodeUrl) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION encodeUrl Procedure 
FUNCTION encodeUrl RETURNS CHARACTER
  ( input cValue as char ) :

/**************************************************************************** 
Description: Encodes unsafe characters in a URL as per RFC 1738 section 2.2. 
<URL:http://ds.internic.net/rfc/rfc1738.txt>, 2.2 
Input Parameters: Character string to encode, Encoding option where "query", 
"cookie", "default" or any specified string of characters are valid. 
In addition, all characters specified in the variable cUnsafe plus ASCII values 0 <= x <= 31 and 127 <= x <= 255 are considered unsafe. 
Returns: Encoded string (unkown value is returned as blank) 
Variables: cUnsafe, cReserved 
****************************************************************************/ 
DEFINE VARIABLE cHex        AS CHARACTER   NO-UNDO INITIAL "0123456789ABCDEF":U. 
DEFINE VARIABLE cEncodeList AS CHARACTER   NO-UNDO.
DEFINE VARIABLE cEnctype    AS CHARACTER   NO-UNDO INITIAL "query".
DEFINE VARIABLE iCounter    AS INTEGER     NO-UNDO. 
DEFINE VARIABLE cChar       AS INTEGER     NO-UNDO. 

/* Unsafe characters that must be encoded in URL's.  See RFC 1738 Sect 2.2. */
DEFINE VARIABLE cUnsafe   AS CHARACTER NO-UNDO INITIAL " <>~"#%~{}|~\^~~[]`":U.

/* Reserved characters that normally are not encoded in URL's */
DEFINE VARIABLE cReserved AS CHARACTER NO-UNDO INITIAL "~;/?:@=&":U.

/* Don't bother with blank or unknown */ 
IF LENGTH(cValue) EQ 0 OR 
   cValue         EQ ? THEN 
  RETURN "". 

/* What kind of encoding should be used? */ 
CASE cEnctype: 
  WHEN "query":U THEN /* QUERY_STRING name=value parts */ 
    cEncodeList = cUnsafe + cReserved + "+":U. 
  WHEN "cookie":U THEN /* Persistent Cookies */ 
    cEncodeList = cUnsafe + " ,~;":U. 
  WHEN "default":U OR WHEN "" THEN /* Standard URL encoding */ 
    cEncodeList = cUnsafe. 
  OTHERWISE 
    cEncodeList = cUnsafe + cEnctype. /* user specified ... */ 
END CASE. 

/* Loop through entire input string */ 
iCounter = 0. 
DO WHILE TRUE: 
  ASSIGN iCounter = iCounter + 1 
         /* ASCII value of character using single byte codepage */ 
         cChar = ASC(SUBSTRING(cValue, iCounter, 1, "RAW":U), 
                     "1252":U, 
                     "1252":U). 

  IF cChar LE 31  OR 
     cChar GE 127 OR 
     INDEX(cEncodeList, CHR(cChar)) GT 0 THEN DO: 
    /* Replace character with %hh hexidecimal triplet */ 
    SUBSTRING(cValue, iCounter, 1, "RAW":U) = "%":U + 
      SUBSTRING(cHex, INTEGER(TRUNCATE(cChar / 16, 0)) + 1, 1, "RAW":U) + /* high */ 
      SUBSTRING(cHex, cChar MODULO 16 + 1, 1, "RAW":U). /* low digit */ 

    iCounter = iCounter + 2. /* skip over hex triplet just inserted */ 
  END. 
  IF iCounter EQ LENGTH(cValue,"RAW":U) THEN LEAVE. 
END. 

return cValue.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-encodeXml) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION encodeXml Procedure 
FUNCTION encodeXml RETURNS CHARACTER PRIVATE
  (pUnencoded as char) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  pUnencoded = replace(pUnencoded, "&", "&amp;").
  /* The & would have been replaced in the original up above */
  pUnencoded = replace(pUnencoded, "&amp;#10;", "&#10;").
  
  pUnencoded = replace(pUnencoded, "'", "&apos;").
  pUnencoded = replace(pUnencoded, '"', "&quot;").
  pUnencoded = replace(pUnencoded, ">", "&gt;").
  pUnencoded = replace(pUnencoded, "<", "&lt;").

  pUnencoded = replace(pUnencoded, chr(9), "&#9;").
  /*pUnencoded = replace(pUnencoded, chr(13), "  ").*/
  /*pUnencoded = replace(pUnencoded, chr(13), "&br;").*/
  /*pUnencoded = replace(pUnencoded, chr(10), "&br;").*/
  pUnencoded = replace(pUnencoded, chr(13) + chr(10), "&br;").
  pUnencoded = replace(pUnencoded, chr(10), "&br;").
  pUnencoded = replace(pUnencoded, chr(145), "&apos;").
  pUnencoded = replace(pUnencoded, chr(146), "&apos;").
  pUnencoded = replace(pUnencoded, chr(147), "&quot;").
  pUnencoded = replace(pUnencoded, chr(148), "&quot;").

  RETURN pUnencoded.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parseXml) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION parseXml Procedure 
FUNCTION parseXml RETURNS LOGICAL PRIVATE
  ( output pMsg as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 parseStatus = true. /* Assume it works! */
 
 file-info:file-name = {&xmlFile}.
 if file-info:full-pathname <> ? 
  then
   do:
       create sax-reader parser.
       parser:handler = this-procedure.
       parser:SET-INPUT-SOURCE("FILE", {&xmlFile}).
       &if defined(has-namespace) = 0 &then
       parser:suppress-namespace-processing = true.
       &endif
       parser:SAX-PARSE() NO-ERROR.
      
       DELETE OBJECT parser.
      
       &IF defined(include-trace) > 0 &THEN
       if not parseStatus 
        then trace("Parse failed in " + {&xmlFile} + ": " + parseMsg).
       &ENDIF

       pMsg = parseMsg.
   end.
  else 
   do: parseStatus = false.
       pMsg = "No response data found".
   end.
 
 RETURN parseStatus.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-trace) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION trace Procedure 
FUNCTION trace RETURNS LOGICAL PRIVATE
  ( input pTraceMsg as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

&IF defined(include-trace) > 0 &THEN
 output to value({&traceFile}) append.
 put unformatted pTraceMsg skip.
 output close.
&ENDIF

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

