/*------------------------------------------------------------------------
    File        : lib/rpt-getparams.i    
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : MK
    Created     : 06/03/21
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
define input parameter rpt-behavior        as character no-undo.
define input parameter rpt-notifyRequestor as character no-undo.
define input parameter rpt-destination     as character no-undo.
define input parameter rpt-email           as character no-undo.
define input parameter rpt-printer         as character no-undo.
define input parameter rpt-repository      as character no-undo.
define input parameter rpt-outputFormat    as character no-undo.

