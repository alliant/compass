&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file syspropdatasrv.i
@description Data Server file for the system codes

@param syscode;char;Comma-delimited list of syscodes to get

@author John Oliver
@created 04-19-2019
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}
define variable tLoadedSysCodes as logical initial false no-undo.

/* tables */
{tt/syscode.i}
{tt/esbmsg.i &tableAlias="syscodeesbmsg"}

/* temp tables */
{tt/syscode.i &tableAlias="tempsyscode"}

/* functions */
{lib/add-delimiter.i}

/* preprocessors */
&if defined(syscode) = 0 &then
&scoped-define syscode ""
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshSysCode Include 
FUNCTION refreshSysCode RETURNS logical
  ( input pCodeType as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* main procedures */
subscribe to "SysCodeChanged"     anywhere.
subscribe to "GetSysCodeDesc"     anywhere.
subscribe to "GetSysCodeList"     anywhere.
subscribe to "GetCodes"           anywhere run-procedure "GetSysCodes".
subscribe to "GetSysCodes"        anywhere.
subscribe to "LoadSysCodes"       anywhere.

/* used for the messaging system */
subscribe to "TIMER"              anywhere run-procedure "SysCodeTimer".
subscribe to "ExternalEsbMessage" anywhere run-procedure "SysCodeESB".

/* used to dump the data file */
subscribe to "DataDump"           anywhere run-procedure "SysCodeDataDump".

/* load system codes */
std-lo = true.
publish "SetSplashStatus".
publish "GetLoadSysCodes" (output std-lo).
if std-lo
 then run LoadSysCodes.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSysCodeDesc Include 
PROCEDURE GetSysCodeDesc :
/*------------------------------------------------------------------------------
@description Gets a system code description
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pCodeType as character no-undo.
  define input  parameter pCode     as character no-undo.
  define output parameter pDesc     as character no-undo.
  
  std-ch = "Gross Premium^G^Net Premium^N^P&L^P" .
  
  if pCodeType = "Commission"
   then
    do:
      pDesc = if lookup(pCode, std-ch, {&msg-dlm}) = 0 then "" else entry(lookup(pCode, std-ch, {&msg-dlm}) - 1, std-ch, {&msg-dlm}).
      return.
    end.

  /* get the list of system codes */
  std-ch = "".
  run GetSysCodeList (pCodeType, output std-ch).

  /* if list found */
  if std-ch > ""
   then
    do:
      /* lookup the value, if available */
      if lookup(pCode, std-ch, {&msg-dlm}) = 0 
       then pDesc = "".
       else pDesc = entry(lookup(pCode, std-ch, {&msg-dlm}) - 1, std-ch, {&msg-dlm}).
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSysCodeList Include 
PROCEDURE GetSysCodeList :
/*------------------------------------------------------------------------------
@description Gets a list of system codes
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pCodeType as character no-undo.
  define output parameter pList     as character no-undo.
  
  /* buffers */
  define buffer tempsyscode for tempsyscode.

  /* get the system codes */
  empty temp-table tempsyscode.
  run GetSysCodes (pCodeType, output table tempsyscode).

  /* create a list of system codes */
  pList = "".
  for each tempsyscode no-lock
     where tempsyscode.codeType = pCodeType
        by tempsyscode.description:
        
     pList = addDelimiter(pList, {&msg-dlm}) + tempsyscode.description {&msg-add} tempsyscode.code.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSysCodes Include 
PROCEDURE GetSysCodes :
/*------------------------------------------------------------------------------
@description Gets the system codes
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pCodeType as character no-undo.
  define output parameter table for tempsyscode.
  
  define variable cRegionList as character init "Caribbean,1,CentralWest,2,GreatLakes,3,SouthEast,4,SouthWest,5" no-undo.
  define variable iCounter    as integer no-undo.

  /* if the system property can not be found */
  if not can-find(first syscode where syscode.codeType = pCodeType)
   then
    do:
      /* call the server */
      empty temp-table tempsyscode.
      if pCodeType = "Region" 
       then
        do:
          do iCounter = 1 to num-entries(cRegionList) / 2:
            create tempsyscode.
            assign 
                tempsyscode.codeType    = pCodeType             
                tempsyscode.code        = string(iCounter)
                tempsyscode.description = if lookup(string(iCounter), cRegionList) = 0 then "" 
                                          else entry(lookup(string(iCounter), cRegionList) - 1, cRegionList).                   
          end.
          assign std-lo = true.
        end.
       else 
        run server/getsyscodes.p (input  pCodeType,
                                  output table tempsyscode,
                                  output std-lo,
                                  output std-ch).
      
      /* if unsuccessful, show a message */
      if not std-lo 
       then message "LoadSysCodes for " + pCodeType + " failed: " + std-ch view-as alert-box error.
       else
        /* otherwise, add the newly retrieved system code to the system code records */
        for each tempsyscode:
          create syscode.
          buffer-copy tempsyscode to syscode.
        end.
    end.
  
  /* load the table */
  empty temp-table tempsyscode.
  for each syscode no-lock
     where syscode.codeType = pCodeType:
    
    create tempsyscode.
    buffer-copy syscode to tempsyscode.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadSysCodes Include 
PROCEDURE LoadSysCodes :
/*------------------------------------------------------------------------------
@description Loads the System Codes
------------------------------------------------------------------------------*/
  /* variables */
  define variable tCodeType as character no-undo.
  
  /* buffers */
  define buffer tempsyscode for tempsyscode.

  /* load the system codes from the preprocessor */
  empty temp-table syscode.
  do std-in = 1 to num-entries({&syscode}):
    tCodeType = entry(std-in, {&syscode}).
     
    /* call the server */
    empty temp-table tempsyscode.
    run server/getsyscodes.p (input  tCodeType,
                              output table tempsyscode,
                              output std-lo,
                              output std-ch).
  
    /* if unsuccessful, show a message */
    if not std-lo 
     then
      do: 
        std-lo = false.
        publish "GetAppDebug" (output std-lo).
        if std-lo 
         then message "LoadSysCodes for " + tCodeType + " failed: " + std-ch view-as alert-box warning.
      end.
     else
      /* otherwise, add the newly retrieved system codes to the system code records */
      for each tempsyscode:
        create syscode.
        buffer-copy tempsyscode to syscode.
      end.
  end.
  tLoadedSysCodes = true. /* Always set to true as there may not be any codes to load at startup */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysCodeChanged Include 
PROCEDURE SysCodeChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pCodeType as character no-undo.
  
  /* refresh the alert */
  refreshSysCode(pCodeType).
  
  /* publish that the data changed */
  publish "SysCodeDataChanged".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysCodeDataDump Include 
PROCEDURE SysCodeDataDump :
/*------------------------------------------------------------------------------
@description Dump the syscode and tempsyscode temp-tables into files
------------------------------------------------------------------------------*/
  /* variables */
  define variable tPrefix as character no-undo.
  
  /* get the prefix */
  publish "GetAppCode" (output tPrefix).
  tPrefix = tPrefix + "_".
  
  /* delete the current temp files */
  publish "DeleteTempFile" ("DataSysCode", output std-lo).
  publish "DeleteTempFile" ("DataTempSysCode", output std-lo).
  
  /* Active temp-tables */
  temp-table syscode:write-xml("file", tPrefix + "SysCode.xml").
  publish "AddTempFile" ("DataSysCode", tPrefix + "SysCode.xml").
  
  /* "Temp" temp-tables */
  temp-table tempsyscode:write-xml("file", tPrefix + "TempSysCode.xml").
  publish "AddTempFile" ("DataSysCode", tPrefix + "TempSysCode.xml").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysCodeESB Procedure 
PROCEDURE SysCodeESB :
/*------------------------------------------------------------------------------
@description Runs when an esb message is received
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey as character no-undo.
  
  /* exit if not correct entity */
  if pEntity <> "SysCode"
   then next.
  
  /* get the next number */
  std-in = 1.
  for each syscodeesbmsg no-lock:
    std-in = std-in + 1.
  end.
  
  /* create the esb message */
  create syscodeesbmsg.
  assign
    syscodeesbmsg.seq    = std-in
    syscodeesbmsg.rcvd   = now
    syscodeesbmsg.entity = pEntity
    syscodeesbmsg.action = pAction
    syscodeesbmsg.keyID  = pKey
    .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysCodeTimer Include 
PROCEDURE SysCodeTimer :
/*------------------------------------------------------------------------------
@description Triggered by appstart.i running on a timer
------------------------------------------------------------------------------*/
  /* buffers */
  define buffer syscodeesbmsg for syscodeesbmsg.

  /* loop through the esb messages */
  for each syscodeesbmsg exclusive-lock
  break by syscodeesbmsg.keyID
        by syscodeesbmsg.rcvd descending:

    /* if the syscode is loaded and the we have a messages */
    if tLoadedSysCodes and first-of(syscodeesbmsg.keyID) 
     then run SysCodeChanged (syscodeesbmsg.keyID).
     
    /* delete the esb message and set the last sync */
    delete syscodeesbmsg.
    publish "SetLastSync".
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshSysCode Include 
FUNCTION refreshSysCode RETURNS logical
  ( input pCodeType as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* parameters */
  define variable lSuccess as logical no-undo.
  
  /* buffers */
  define buffer tempsyscode for tempsyscode.
  define buffer syscode for syscode.
  
  /* call the server */
  empty temp-table tempsyscode.
  run server/getsyscodes.p (input  pCodeType,
                            output table tempsyscode,
                            output lSuccess,
                            output std-ch).

  /* if unsuccessful, show a message if debug is on */
  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshSysCode failed: " + std-ch view-as alert-box warning.
    end.
   else
    /* otherwise, copy the system code details into the system code records */
    do:
       empty temp-table syscode.
       for each tempsyscode no-lock where tempsyscode.codeType = pCodeType:
       create syscode.
       buffer-copy tempsyscode to syscode.
       end.

  
    end.
  
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
