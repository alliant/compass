/*---------------------------------------------------------------------
@name getclaimreservelimit.i
@description Get the user's approval limit of what they can raise
             the reserve

@param {1} is the claim ID (int)
@param {2} is the reference (char)
@param {3) is the user id (char)
@param {4} is the return variable (decimal)

@author John Oliver
@version 1.0
@created 08/6/16
@notes The formula is:
       Limit = UserDefinedReserveLimit - TotalReserve
---------------------------------------------------------------------*/
{lib/get-reserve-type.i}

define variable dReserveLimit as decimal.
define variable dReserveAmount as decimal.
define variable cReserveType as character no-undo.
assign
  dReserveLimit = 0.0
  dReserveAmount = 0.0
  {4} = 0.0
  .
{lib/get-sysprop.i &appCode="'CLM'" &objAction="'PayableInvoice'" &objProperty="'Category'" &objID={2} &out=cReserveType}
FOR FIRST sysprop NO-LOCK
    WHERE sysprop.appCode = "CLM"
      AND sysprop.objAction = "ClaimAdjustmentRequest"
      AND sysprop.objProperty = cReserveType + "ReserveLimit"
      AND sysprop.objID = {3}:
      
  dReserveLimit = DECIMAL(sysprop.objValue) NO-ERROR.
END.
/* get the reserve total */
{lib/getclaimreservetotal.i {1} {2} dReserveAmount}

if dReserveLimit = -1
 then {4} = lf-max.
 else {4} = dReserveLimit - ABSOLUTE(dReserveAmount).
 
if {4} < 0
 then {4} = 0.