&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/configdef.i
   D.Sinclair
   7.4.2012
   Temp-table definitions to hold settings/options
   &exclude-subscriptions To prevent subscribing to Get* publish events
  ----------------------------------------------------------------------*/

{lib/std-def.i}

{tt/sysconfig.i &tableAlias=setting}
{tt/sysconfig.i &tableAlias=config}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getOption Include 
FUNCTION getOption RETURNS CHARACTER PRIVATE
 ( pName as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getSetting Include 
FUNCTION getSetting RETURNS CHARACTER PRIVATE
 ( pName as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setOption Include 
FUNCTION setOption RETURNS LOGICAL PRIVATE
 ( pName as char,
   pVal as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setSetting Include 
FUNCTION setSetting RETURNS LOGICAL PRIVATE
 ( pName as char,
   pVal as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


&IF defined(exclude-subscriptions) = 0 
 &THEN
subscribe to "GetSettings" anywhere.
subscribe to "GetOptions" anywhere.
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetOptions Include 
PROCEDURE GetOptions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter table for config.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSettings Include 
PROCEDURE GetSettings :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter table for setting.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getOption Include 
FUNCTION getOption RETURNS CHARACTER PRIVATE
 ( pName as char ) :
/*------------------------------------------------------------------------------
 Purpose:  
   Notes:  
------------------------------------------------------------------------------*/
  find config
    where config.f = pName no-error.
  if available config 
   then return config.v.
  RETURN "".   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getSetting Include 
FUNCTION getSetting RETURNS CHARACTER PRIVATE
 ( pName as char ) :
/*------------------------------------------------------------------------------
 Purpose:  
   Notes:  
------------------------------------------------------------------------------*/
 find setting
   where setting.f = pName no-error.
 if available setting 
  then return setting.v.
 RETURN "".   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setOption Include 
FUNCTION setOption RETURNS LOGICAL PRIVATE
 ( pName as char,
   pVal as char ) :
/*------------------------------------------------------------------------------
 Purpose:  
   Notes:  
------------------------------------------------------------------------------*/
  find config
    where config.f = pName no-error.
  if not available config 
   then 
    do: create config.
        config.f = pName.
     end.
  config.v = pVal.
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setSetting Include 
FUNCTION setSetting RETURNS LOGICAL PRIVATE
 ( pName as char,
   pVal as char ) :
/*------------------------------------------------------------------------------
 Purpose:  
   Notes:  
------------------------------------------------------------------------------*/
  find setting
    where setting.f = pName no-error.
  if not available setting 
   then 
    do: create setting.
        setting.f = pName.
     end.
  setting.v = pVal.
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

