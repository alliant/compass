&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*------------------------------------------------------------------------
@file userfield.i
@description Functions related to user fields in text

@notes The word is default so it must be first (and the option probably
       selected the most)
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */


&if defined(user-field-def) = 0 &then
/* needed for multiple functions */
{lib/add-delimiter.i}

/* beginning, delimiting, and ending characters for the userfield */
&scoped-define UserFieldStartChar    "~~~{"
&scoped-define UserFieldEndChar      "~~~}"
&scoped-define UserFieldDelimiter    "|"

/* used for the names of the user field datatype options */
&scoped-define UserFieldDisplayList  "Word"      {&msg-add} "Currency"           {&msg-add} "Date"       {&msg-add} "Number"
&scoped-define UserFieldDatatypeList "character" {&msg-add} "decimal"            {&msg-add} "date"       {&msg-add} "integer"
&scoped-define UserFieldFormatList   "x(1000)"   {&msg-add} ">>>,>>>,>>>,>>9.99" {&msg-add} "99/99/9999" {&msg-add} "9(100)"

&global-define user-field-def true
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-GetUserField) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GetUserField Method-Library 
FUNCTION GetUserField RETURNS CHARACTER
  ( input pWord as character,
    input pType as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetUserFieldDatatype) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GetUserFieldDatatype Method-Library 
FUNCTION GetUserFieldDatatype RETURNS CHARACTER
  ( input pUserField as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetUserFieldDatatypeList) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GetUserFieldDatatypeList Method-Library 
FUNCTION GetUserFieldDatatypeList RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetUserFieldDisplay) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GetUserFieldDisplay Method-Library 
FUNCTION GetUserFieldDisplay RETURNS CHARACTER
  ( input pUserField as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetUserFieldDisplayFromDatatype) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GetUserFieldDisplayFromDatatype Method-Library 
FUNCTION GetUserFieldDisplayFromDatatype RETURNS CHARACTER
  ( INPUT pDatatype AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetUserFieldDisplayList) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GetUserFieldDisplayList Method-Library 
FUNCTION GetUserFieldDisplayList RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetUserFieldFormat) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GetUserFieldFormat Method-Library 
FUNCTION GetUserFieldFormat RETURNS CHARACTER
  ( input pUserField as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetUserFieldPlaceholders) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GetUserFieldPlaceholders Method-Library 
FUNCTION GetUserFieldPlaceholders RETURNS CHARACTER
  ( input pText      as character,
    input pDelimiter as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ReplaceUserFields) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD ReplaceUserFields Method-Library 
FUNCTION ReplaceUserFields RETURNS CHARACTER
  ( input pText         as character,
    input pPlaceholders as character,
    input pPlacevalues  as character,
    input pDelimiter    as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ValidateUserField) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD ValidateUserField Method-Library 
FUNCTION ValidateUserField RETURNS CHARACTER
  ( input pText     as character,
    input pDatatype as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ValidateUserFieldSelection) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD ValidateUserFieldSelection Method-Library 
FUNCTION ValidateUserFieldSelection RETURNS LOGICAL
  ( input pSelect as longchar )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-GetUserField) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GetUserField Method-Library 
FUNCTION GetUserField RETURNS CHARACTER
  ( input pWord as character,
    input pType as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  RETURN {&UserFieldStartChar} + pWord + {&UserFieldDelimiter} + pType + {&UserFieldEndChar}.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetUserFieldDatatype) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GetUserFieldDatatype Method-Library 
FUNCTION GetUserFieldDatatype RETURNS CHARACTER
  ( input pUserField as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tIndex    as integer   no-undo.
  define variable tDatatype as character no-undo initial "".

  if num-entries(pUserField, {&UserFieldDelimiter}) = 2
   then
    do:
      tIndex = lookup(entry(2, pUserField, {&UserFieldDelimiter}), {&UserFieldDatatypeList}, {&msg-dlm}).
      if tIndex > 0
       then tDatatype = entry(tIndex, {&UserFieldDatatypeList}, {&msg-dlm}).
    end.
  
  /* derfault to the character datatype */
  if tDatatype = ""
   then tDatatype = entry(1, {&UserFieldDatatypeList}, {&msg-dlm}).

  RETURN tDatatype.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetUserFieldDatatypeList) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GetUserFieldDatatypeList Method-Library 
FUNCTION GetUserFieldDatatypeList RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  RETURN {&UserFieldDatatypeList}.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetUserFieldDisplay) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GetUserFieldDisplay Method-Library 
FUNCTION GetUserFieldDisplay RETURNS CHARACTER
  ( input pUserField as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tDisplay as character no-undo.

  if num-entries(pUserField, {&UserFieldDelimiter}) = 2
   then tDisplay = entry(2, pUserField, {&UserFieldDelimiter}).
  
  /* derfault to the character datatype */
  if tDisplay = ""
   then tDisplay = entry(1, {&UserFieldDisplayList}, {&msg-dlm}).

  RETURN tDisplay.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetUserFieldDisplayFromDatatype) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GetUserFieldDisplayFromDatatype Method-Library 
FUNCTION GetUserFieldDisplayFromDatatype RETURNS CHARACTER
  ( input pDatatype as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tIndex   as integer   no-undo.
  define variable tDisplay as character no-undo.

  tIndex = lookup(pDatatype, {&UserFieldDatatypeList}, {&msg-dlm}).
  if tIndex > 0
   then tDisplay = entry(tIndex, {&UserFieldDisplayList}, {&msg-dlm}).
  
  /* derfault to the character format */
  if tDisplay = ""
   then tDisplay = entry(1, {&UserFieldDisplayList}, {&msg-dlm}).

  RETURN tDisplay.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetUserFieldDisplayList) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GetUserFieldDisplayList Method-Library 
FUNCTION GetUserFieldDisplayList RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  RETURN {&UserFieldDisplayList}.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetUserFieldFormat) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GetUserFieldFormat Method-Library 
FUNCTION GetUserFieldFormat RETURNS CHARACTER
  ( input pUserField as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tIndex  as integer   no-undo.
  define variable tFormat as character no-undo initial "".
    
  if num-entries(pUserField, {&UserFieldDelimiter}) = 2
   then
    do:
      tIndex = lookup(entry(2, pUserField, {&UserFieldDelimiter}), {&UserFieldDisplayList}, {&msg-dlm}).
      if tIndex > 0
       then tFormat = entry(tIndex, {&UserFieldFormatList}, {&msg-dlm}).
    end.
  
  /* derfault to the character format */
  if tFormat = ""
   then tFormat = entry(1, {&UserFieldFormatList}, {&msg-dlm}).

  RETURN tFormat.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetUserFieldPlaceholders) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GetUserFieldPlaceholders Method-Library 
FUNCTION GetUserFieldPlaceholders RETURNS CHARACTER
  ( input pText      as character,
    input pDelimiter as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* variables */
  define variable iCount    as integer   no-undo.
  define variable tWord     as character no-undo.
  define variable tSubText  as character no-undo.
  define variable iStartPos as integer   no-undo.
  define variable iEndPos   as integer   no-undo.
  define variable tFound    as logical   no-undo initial true.
  define variable tReturn   as character no-undo.

  tSubText = pText.
  if tSubText <> ""
   then
    do:
      do while tFound:
        /* get the start and end position */
        assign
          iStartPos = index(tSubText, {&UserFieldStartChar})
          iEndPos   = index(tSubText, {&UserFieldEndChar})
          tWord     = substring(tSubText, iStartPos + 1, iEndPos - iStartPos - 1)
          .
          
        /* add the word */
        if index(tReturn, tWord) = 0 and iStartPos > 0 and iEndPos > 0
         then tReturn = addDelimiter(tReturn, pDelimiter) + tWord.
         
        /* set the subtext */
        assign
          tSubText = substring(tSubText, iEndPos + 1)
          tFound   = (index(tSubText, {&UserFieldStartChar}) > 0)
          .
      end.
    end.

  RETURN tReturn.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ReplaceUserFields) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION ReplaceUserFields Method-Library 
FUNCTION ReplaceUserFields RETURNS CHARACTER
  ( input pText         as character,
    input pPlaceholders as character,
    input pPlacevalues  as character,
    input pDelimiter    as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable iCount       as integer   no-undo.
  define variable tPlaceholder as character no-undo.
  define variable tPlacevalue  as character no-undo.

  do iCount = 1 to num-entries(pPlaceholders, pDelimiter):
    assign
      tPlaceholder = entry(iCount, pPlaceholders, pDelimiter)
      tPlacevalue  = entry(iCount, pPlacevalues, pDelimiter)
      .

    pText = replace(pText, {&UserFieldStartChar} + tPlaceholder + {&UserFieldEndChar}, tPlacevalue).
  end.

  RETURN pText.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ValidateUserField) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION ValidateUserField Method-Library 
FUNCTION ValidateUserField RETURNS CHARACTER
  ( input pText     as character,
    input pDatatype as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tError   as character no-undo.
  define variable tDate    as date      no-undo.
  define variable tDecimal as decimal   no-undo.
  define variable tInteger as integer   no-undo.
  
  case pDatatype:
   when "DECIMAL" then
    do:
      tDecimal = decimal(pText) no-error.
      if error-status:error or pText = ""
       then tError = "Not a valid dollar amount".
    end.
   when "INTEGER" then
    do:
      tInteger = integer(pText) no-error.
      if error-status:error or pText = ""
       then tError = "Not a whole number".
    end.
   when "DATE"    then
    do:
      tDate = date(pText) no-error.
      if error-status:error or pText = ""
       then tError = "Please format the date as MM/DD/YYYY".
    end.
  end case.
  RETURN tError.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ValidateUserFieldSelection) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION ValidateUserFieldSelection Method-Library 
FUNCTION ValidateUserFieldSelection RETURNS LOGICAL
  ( input pSelect as longchar ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
/* if the entire selection is a replacable word */
  if substring(pSelect, 1, 1) = {&UserFieldStartChar} and substring(pSelect, length(pSelect), 1) = {&UserFieldEndChar}
   then return true.
  
  /* the selection text can't contain a replacement character */
  if index(pSelect, {&UserFieldStartChar}) > 0 or index(pSelect, {&UserFieldEndChar}) > 0
   then return false.

  /* the selection text can't contain the user delimiter */
  if index(pSelect, {&UserFieldDelimiter}) > 0
   then return false.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

