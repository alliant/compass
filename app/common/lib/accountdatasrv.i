&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file sysinidatasrv.i
@description Data Server file for getting the server ini file

@author John Oliver
@created 04-22-2019
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}
define variable tLoadedAccounts as logical initial false no-undo.

/* tables */
{tt/account.i}
{tt/esbmsg.i &tableAlias="accountesbmsg"}

/* temp tables */
{tt/account.i &tableAlias="tempaccount"}

/* functions */
{lib/getstatename.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshAccount Procedure
FUNCTION refreshAccount RETURNS logical
  ( input pAccountID as character ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* main procedures */
subscribe to "AccountChanged"     anywhere.
subscribe to "GetAccount"         anywhere.
subscribe to "GetAccountName"     anywhere.
subscribe to "GetAccounts"        anywhere.
subscribe to "LoadAccounts"       anywhere.

/* used for the messaging system */
subscribe to "TIMER"              anywhere run-procedure "AccountTimer".
subscribe to "ExternalEsbMessage" anywhere run-procedure "AccountESB".

/* used to dump the data file */
subscribe to "DataDump"           anywhere run-procedure "AccountDataDump".

/* load system users */
std-lo = true.
publish "SetSplashStatus".
publish "GetLoadAccounts" (output std-lo).
if std-lo 
 then run LoadAccounts.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AccountChanged Include 
PROCEDURE AccountChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pAccountID as character no-undo.
  
  /* refresh the alert */
  refreshAccount(pAccountID).
  
  /* publish that the data changed */
  publish "AccountDataChanged".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AccountDataDump Include 
PROCEDURE AccountDataDump :
/*------------------------------------------------------------------------------
@description Dump the account and tempaccount temp-tables into files
------------------------------------------------------------------------------*/
  /* variables */
  define variable tPrefix as character no-undo.
  
  /* get the prefix */
  publish "GetAppAccount" (output tPrefix).
  tPrefix = tPrefix + "_".
  
  /* delete the current temp files */
  publish "DeleteTempFile" ("DataAccount", output std-lo).
  publish "DeleteTempFile" ("DataTempAccount", output std-lo).
  
  /* Active temp-tables */
  temp-table account:write-xml("file", tPrefix + "Account.xml").
  publish "AddTempFile" ("DataAccount", tPrefix + "Account.xml").
  
  /* "Temp" temp-tables */
  temp-table tempaccount:write-xml("file", tPrefix + "TempAccount.xml").
  publish "AddTempFile" ("DataAccount", tPrefix + "TempAccount.xml").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AccountESB Procedure 
PROCEDURE AccountESB :
/*------------------------------------------------------------------------------
@description Runs when an esb message is received
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey as character no-undo.
  
  /* exit if not correct entity */
  if pEntity <> "Account"
   then next.
  
  /* get the next number */
  std-in = 1.
  for each accountesbmsg no-lock:
    std-in = std-in + 1.
  end.
  
  /* create the esb message */
  create accountesbmsg.
  assign
    accountesbmsg.seq    = std-in
    accountesbmsg.rcvd   = now
    accountesbmsg.entity = pEntity
    accountesbmsg.action = pAction
    accountesbmsg.keyID  = pKey
    .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AccountTimer Include 
PROCEDURE AccountTimer :
/*------------------------------------------------------------------------------
@description Triggered by appstart.i running on a timer
------------------------------------------------------------------------------*/
  /* buffers */
  define buffer accountesbmsg for accountesbmsg.

  /* loop through the esb messages */
  for each accountesbmsg exclusive-lock
  break by accountesbmsg.keyID
        by accountesbmsg.rcvd descending:

    /* if the account is loaded and the we have a messages */
    if tLoadedAccounts and first-of(accountesbmsg.keyID) 
     then run AccountChanged (accountesbmsg.keyID).
     
    /* delete the esb message and set the last sync */
    delete accountesbmsg.
    publish "SetLastSync".
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAccount Procedure 
PROCEDURE GetAccount :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pAccountID as character no-undo.
  define output parameter table for tempaccount.

  /* buffers */
  define buffer account for account.

  /* get the accounts */
  empty temp-table tempaccount.
  run GetAccounts (output table tempaccount).

  /* get correct account */
  for each tempaccount exclusive-lock
     where tempaccount.acct <> pAccountID:
     
    delete tempaccount.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAccountName Procedure 
PROCEDURE GetAccountName :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pAccountID   as character no-undo.
  define output parameter pAccountName as character no-undo.

  /* buffers */
  define buffer account for account.

  /* get the accounts */
  empty temp-table tempaccount.
  run GetAccounts (output table tempaccount).

  /* get correct account */
  for first tempaccount exclusive-lock
      where tempaccount.acct = pAccountID:
     
    pAccountName = tempaccount.acctname.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAccount Include 
PROCEDURE GetAccounts :
/*------------------------------------------------------------------------------
@description Gets the system ini records
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for account.
  
  /* buffers */
  define buffer account for account.

  /* load the ini records if not done already  */
  if not tLoadedAccounts
   then run LoadAccounts.
   
  /* add the state's name to the records */
  for each account exclusive-lock:
    account.stateName = getStateName(account.state).
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadAccounts Include 
PROCEDURE LoadAccounts :
/*------------------------------------------------------------------------------
@description Loads the accounts
------------------------------------------------------------------------------*/
  /* call the server */
  empty temp-table account.
  run server/getaccounts.p (input  "ALL",
                            output table account,
                            output tLoadedAccounts,
                            output std-ch).

  /* if unsuccessful, show a message if debug */
  if not tLoadedAccounts
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadAccounts failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshAccount Include 
FUNCTION refreshAccount RETURNS logical
  ( input pAccountID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* parameters */
  define variable lSuccess as logical no-undo.
  
  /* buffers */
  define buffer tempaccount for tempaccount.
  define buffer account for account.
  
  /* call the server */
  empty temp-table tempaccount.
  run server/getaccounts.p (input  pAccountID,
                            output table tempaccount,
                            output lSuccess,
                            output std-ch).

  /* if unsuccessful, show a message if debug is on */
  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshAccount failed: " + std-ch view-as alert-box warning.
    end.
   else
    /* otherwise, copy the alert details into the alert records */
    do:
      find tempaccount where tempaccount.acct = pAccountID no-error.
      find account where account.acct = pAccountID no-error.
      if not available tempaccount and available account /* delete */
       then delete account.
      if available tempaccount and not available account /* new */
       then create account.
      if available tempaccount and available account /* modify */
       then buffer-copy tempaccount to account.
    end.
  
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
