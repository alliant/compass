/*---------------------------------------------------------------------
@name getclaimpolicylimit.i
@description Gets the policy limit for a loss policy

@param {1} is the policyID (integer)
@param {2} is the return variable (decimal)

@author John Oliver
@version 1.0
@created 08/29/16
@notes
---------------------------------------------------------------------*/
{2} = 0.0.
for first policy no-lock
    where policy.policyid = {1}:
    
  {2} = policy.liabilityamount.
end.
