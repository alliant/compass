&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file parseFileFromXML.i
@description Gets the data from the server XML

@author MK
@created 04.06.2021
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/srvstartelement.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */
 
if qName = "File"
 then
  do:
    create file.
    DO i = 1 TO attributes:NUM-ITEMS:
      fldvalue = attributes:get-value-by-index(i).
      CASE attributes:get-qname-by-index(i):
       when "filename" then file.filename = decodeFromXml(fldvalue).
       when "base64" THEN file.base64 = decodeFromXml(fldvalue).
       when "order" then file.order = integer(fldvalue) no-error.
      END CASE.
    END.
    release file.
  end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


