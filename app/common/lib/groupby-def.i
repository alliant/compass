&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file groupby.i
@description Creates the groupBy function for an agent group
@author John Oliver
------------------------------------------------------------------------*/

/* Local Variable Definitions ---                                       */
{lib/std-def.i}

/* functions */
{lib/do-wait.i}

&if defined(dataTable) = 0 &then
&scoped-define dataTable data
&endif

&if defined(showTable) = 0 &then
&scoped-define showTable showdata
&endif

&if defined(showIDColumn) = 0 &then
&scoped-define showIDColumn showID
&endif

&if defined(showNameColumn) = 0 &then
&scoped-define showNameColumn showName
&endif

&if defined(showLevelColumn) = 0 &then
&scoped-define showLevelColumn showLevel
&endif

&if defined(lowestDetailID) = 0 &then
&scoped-define lowestDetailID agentID
&endif

&if defined(lowestDetailName) = 0 &then
&scoped-define lowestDetailName agentName
&endif

&if defined(sortProcedure) = 0 &then
&scoped-define sortProcedure sortData
&endif


&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif

/* handles for the queries/buffers */
define variable hDataQuery            as handle    no-undo.
define variable hDataBuffer           as handle    no-undo.
define variable hGroupQuery           as handle    no-undo.
define variable hGroupBuffer          as handle    no-undo.
define variable hGroupCopyQuery       as handle    no-undo.
define variable hGroupCopyBuffer      as handle    no-undo.
define variable hShowQuery            as handle    no-undo.
define variable hShowBuffer           as handle    no-undo.

/* handles for the buffer fields */
define variable hGroupField           as handle    no-undo.
define variable hShowField            as handle    no-undo.

/* handle for the formula */
define variable hHasFormula           as handle    no-undo.
define variable hFormulaOper          as handle    no-undo.
define variable hFormulaOne           as handle    no-undo.
define variable hFormulaTwo           as handle    no-undo.
define variable hFormulaPost          as handle    no-undo.

/* used to tell if there is a new main group */
define variable iGroupCounter         as integer   no-undo.
define variable cLastGroup            as character no-undo.

/* get the data for the lowest detail */
define variable cLowestDetailID       as character no-undo.
define variable cLowestDetailName     as character no-undo.
                                      
/* hold the subgroup values */
define variable cSecondGroup          as character no-undo.
define variable cThirdGroup           as character no-undo.

/* variable to hold procedure name used to sort */
define variable cSortProcedure        as character no-undo.

/* data table */
define variable cDataTable            as character no-undo.
define variable cDataValue            as character no-undo.
define variable iDataCounter          as integer   no-undo.

/* table and columns that actually holds the end data */
define variable cShowTable            as character no-undo.
define variable cShowIDColumn         as character no-undo.
define variable cShowNameColumn       as character no-undo.
define variable cShowLevelColumn        as character no-undo.

/* used to roll multiple status when grouped */
define variable cShowGroupField       as character no-undo.
define variable cWhereClause          as character no-undo.
define variable cGroupColumn          as character no-undo.

/* to allow/disallow sorting */
define variable lGroupDetailSortAllow as logical   no-undo.

/* temp table to hold the grouping */
define temp-table groupdata like {&dataTable}
  field groupLevel as integer   {&nodeType}
  field groupID    as character {&nodeType} format "x(500)"
  field groupName  as character {&nodeType} format "x(500)"
  field sortOrder  as character {&nodeType}
  .

/* temp table for grouping details */
define temp-table groupdetail
  field id          as character {&nodeType} format "x(500)"
  field detailValue as character {&nodeType} format "x(500)"
  field detailGroup as character {&nodeType} format "x(500)"
  field isOpen      as logical   {&nodeType}
  field sortOrder   as character {&nodeType} format "x(100)"
  .
  
/* used for the group by formula */
define temp-table groupformula
  field formulaType as character {&nodeType} format "x(500)"
  field numerator   as character {&nodeType} format "x(500)"
  field denominator as character {&nodeType} format "x(500)"
  field outcome     as character {&nodeType} format "x(500)"
  field onError     as integer   {&nodeType}
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&if defined(exclude-escapeQuote) = 0 &then
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD escapeQuote Include 
FUNCTION escapeQuote RETURNS CHARACTER
  ( input pWord as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
&endif

/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

assign
  cDataTable        = "{&dataTable}"
  cShowTable        = "{&showTable}"
  cShowIDColumn     = "{&showIDColumn}"
  cShowNameColumn   = "{&showNameColumn}"
  cShowLevelColumn  = "{&showLevelColumn}"
  cShowGroupField   = "{&group-column-fields}"
  cLowestDetailID   = "{&lowestDetailID}"
  cLowestDetailName = "{&lowestDetailName}"
  cSortProcedure    = "{&sortProcedure}"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION escapeQuote Include 
FUNCTION escapeQuote RETURNS CHARACTER
  ( input pWord as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  RETURN replace(pWord, "'", "~~'").   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
