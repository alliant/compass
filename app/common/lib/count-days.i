&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file count-days.i
@description Count calendar or business days (Monday thru Friday)
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

&IF defined(business) = 0 &THEN
&scoped-define business true
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&if defined(count-days-defined) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION countDays Include 
FUNCTION countDays RETURNS INTEGER
  ( input pDate1 as datetime,
    input pDate2 as datetime ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable iReturn as integer no-undo initial 0.
  define variable iCount as integer no-undo.
  define variable i as integer no-undo.
  define variable dStart as date no-undo.
  define variable dEnd as date no-undo.
  define variable dTemp as date no-undo.

  if pDate1 < pDate2
   then
    assign
      dStart = date(pDate1)
      dEnd = date(pDate2)
      .
   else
    assign
      dStart = date(pDate2)
      dEnd = date(pDate1)
      .
  
  iCount = interval(dEnd, dStart, "days") + 1.
  if {&business}
   then
    do i = 1 to iCount:
      dTemp = add-interval(dStart, i - 1, "days").
      if weekday(dTemp) > 1 and weekday(dTemp) < 7
       then iReturn = iReturn + 1.
    end.
   else iReturn = iCount.

  RETURN iReturn.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&global-define count-days-defined true
&endif


