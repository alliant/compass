/*------------------------------------------------------------------------
    File        : getperiodname.i
    Purpose     : Return period i.e. month + year                 

    Syntax      :

    Description :

    Author(s)   : Rahul Sharma
    Created     : 04/15/2020
    Notes       :
  ----------------------------------------------------------------------*/
&IF DEFINED(PeriodName) = 0
&THEN

FUNCTION getPeriodName RETURNS CHARACTER
  (input pMonth as int,
   input pYear as int)  FORWARD.

FUNCTION getPeriodName RETURNS CHARACTER
  (input pMonth as int,
   input pYear as int) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 if pMonth < 1 or pMonth > 12 
  then return "Unknown Month".
  
 if pYear < 2000 or pYear > 3000 
  then return "Unknown Year".

 return
  entry(pMonth, "January,February,March,April,May,June,July,August,September,October,November,December")
             + " " + string(pYear, "9999").

END FUNCTION.
&GLOBAL-DEFINE PeriodName true
&ENDIF
