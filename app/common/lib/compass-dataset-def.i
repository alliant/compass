/*------------------------------------------------------------------------
@file load-dataset-def.i
@description Loads (loadDataset) or creates (saveDataset) a dataset from 
             or for the server

@param buffers;character;The buffers for the dataset
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

define variable dsCompassHandle as handle                    no-undo.
define variable hParamHandle     as handle                    no-undo.
define variable dsCompassBuffer as handle                    no-undo.
define variable tBufferCount    as integer                   no-undo.


/* ************************  Function Prototypes ********************** */

function getparameter returns character 
  ( input keyvalue as character ) forward.


FUNCTION loadDataset RETURNS LOGICAL
  ( output pMsg          as character  )  FORWARD.

FUNCTION saveDataset RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.


/* ***************************  Main Block  *************************** */

create dataset dsCompassHandle.
dsCompassHandle:serialize-name = 'data'.
&if defined(buffers) &then
dsCompassHandle:set-buffers({&buffers}).
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */


function getparameter returns character 
  ( input keyvalue as character):
/*------------------------------------------------------------------------------
 Purpose:
 Notes:
------------------------------------------------------------------------------*/
  define variable icount as integer no-undo.
  define variable hField as handle.
  if valid-handle(hParamHandle)
   then
    do:
      hParamHandle:default-buffer-handle:find-first ("",no-lock) no-error.
      if hParamHandle:default-buffer-handle:available then
        hField = hParamHandle:default-buffer-handle:buffer-field (keyvalue) no-error.
      
      if valid-handle(hField) then
        return hParamHandle:default-buffer-handle:buffer-field (keyvalue):buffer-value().    
    end.
end function.
    
FUNCTION loadDataset RETURNS LOGICAL
  ( output pMsg          as character  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  define variable lSuccess as logical no-undo.
  define variable iCount as integer no-undo.
  /* checking for faults in the response before reading body of the respone to the dataset*/
  if not clientResponse:compassStatus
   then
    do:
      pMsg = clientResponse:compassMessage.
      return false.
    end.
  /* checking if there is anything (data) to be read from the bodyfile*/  
  if not clientResponse:hasBody
  then
    do:
      pMsg = clientResponse:compassMessage.
      return true.
    end.
   
  &if defined(paramfields) &then
     create temp-table hParamHandle.
     do icount = 1 to num-entries("{&paramfields}"):
      hParamHandle:add-new-field (entry(iCount, "{&paramfields}"), "character").
     end.
     hParamHandle:temp-table-prepare ("parameters").
     hParamHandle:serialize-name = "parameter".
     dsCompassHandle:add-buffer (hParamHandle:default-buffer-handle) no-error.
  &endif
   
  if clientResponse:contentType = 'text/json'
   then
    lSuccess = dsCompassHandle:read-json("file", clientResponse:bodyFilePath, "EMPTY") no-error.
  else if clientResponse:contentType = 'text/xml'
   then
    lSuccess = dsCompassHandle:read-xml("file", clientResponse:bodyFilePath, "EMPTY", ?, ?, ?, ?) no-error.

  if not lSuccess or error-status:error
   then 
    do:   
      do iCount = 1 to error-status:num-messages:
         pMsg = pMsg + error-status:get-message(iCount) + ' '. 
      end.
  end.
  
  return lSuccess.     
END FUNCTION.

FUNCTION saveDataset RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable pFile as character no-undo. 
  
  &if defined(content-type) &then
  contentType = "{&content-type}".
  &endif

  publish "GetTempDir" (output pFile).
  pFile = pFile + guid.
  if contentType = "text/json"
  then
   do:
     pFile = PFile + ".json".
     dsCompassHandle:write-json("file", pFile, true) no-error.
   end.
  else
   do:
     pFile = pFile + ".xml".
     dsCompassHandle:write-xml("file", pFile, true) no-error.
   end. 
   
  publish "AddTempFile" (pFile, search(pFile)).
  RETURN pFile.   /* Function return value. */
END FUNCTION.

