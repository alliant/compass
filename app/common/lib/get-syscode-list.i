&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* ***************************  Definitions  ************************** */

&if defined(codeType) = 0 &then
&scoped-define codeType ""
&endif

&if defined(addAll) = 0 &then
&scoped-define addAll false
&endif

&if defined(addFirst) = 0 &then
&scoped-define addFirst false
&endif

&if defined(firstValue) = 0 &then
&scoped-define firstValue "ALL,ALL"
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

if {&combo}:type = "COMBO-BOX"
 then {&combo}:inner-lines = 10.
{&combo}:list-item-pairs = entry(1, {&firstValue}) + {&combo}:delimiter + entry(2, {&firstValue}).

/* get the codes */
std-ch = "".
publish "GetSysCodeList" ({&codeType}, output std-ch).
std-lo = false.
if std-ch > ""
 then 
  assign
    std-ch                   = replace(std-ch, {&msg-dlm}, {&combo}:delimiter)
    {&combo}:list-item-pairs = {&combo}:list-item-pairs + {&combo}:delimiter + std-ch
    std-lo = true
    .

if std-lo and not {&addAll} and not {&addFirst}
 then {&combo}:delete(1).

&if defined(d) = 0 &then
{&combo}:screen-value = entry(2, {&combo}:list-item-pairs, {&combo}:delimiter).
&else
if index({&combo}:list-item-pairs, {&d}) > 0
 then {&combo}:screen-value = {&d}.
 else {&combo}:screen-value = entry(2, {&combo}:list-item-pairs, {&combo}:delimiter).
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


