&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file sysinidatasrv.i
@description Data Server file for getting the server ini file

@author John Oliver
@created 04-22-2019
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}
define variable tLoadedSysIni as logical initial false no-undo.

/* tables */
{tt/ini.i}
{tt/esbmsg.i &tableAlias="iniesbmsg"}

/* temp tables */
{tt/ini.i &tableAlias="tempini"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* main procedures */
subscribe to "GetSysIni"         anywhere.
subscribe to "GetSysIniDesc"     anywhere.
subscribe to "LoadSysIniFile"    anywhere.

/* used for the messaging system */
subscribe to "TIMER"              anywhere run-procedure "SysIniTimer".
subscribe to "ExternalEsbMessage" anywhere run-procedure "SysIniESB".

/* used to dump the data file */
subscribe to "DataDump"           anywhere run-procedure "SysIniDataDump".

/* load system users */
std-lo = true.
publish "SetSplashStatus".
if std-lo 
 then run LoadSysIniFile.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSysIni Include 
PROCEDURE GetSysIni :
/*------------------------------------------------------------------------------
@description Gets the system ini records
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for ini.

  /* load the ini records if not done already  */
  if not tLoadedSysIni
   then run LoadSysIniFile.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSysIniDesc Include 
PROCEDURE GetSysIniDesc :
/*------------------------------------------------------------------------------
@description Gets the system ini value
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pKey   as character no-undo.
  define output parameter pValue as character no-undo.
  
  /* buffers */
  define buffer ini for ini.

  /* get the ini records */
  empty temp-table tempini.
  run GetSysIni (output table tempini).
  
  /* get the correct value for the key passed in */
  empty temp-table tempini.
  for first ini no-lock
      where ini.id = pKey:
    
    pValue = ini.val.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadSysIniFile Include 
PROCEDURE LoadSysIniFile :
/*------------------------------------------------------------------------------
@description Loads the system inis
------------------------------------------------------------------------------*/
  /* call the server */
  empty temp-table ini.
  run server/getini.p (output table ini,
                       output tLoadedSysIni,
                       output std-ch).

  /* if unsuccessful, show a message if debug */
  if not tLoadedSysIni
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadSysIniFile failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysIniChanged Include 
PROCEDURE SysIniChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pIniID as character no-undo.
  
  /* refresh the alert */
  run LoadSysIniFile.
  
  /* publish that the data changed */
  publish "IniDataChanged".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysIniDataDump Include 
PROCEDURE SysIniDataDump :
/*------------------------------------------------------------------------------
@description Dump the ini and tempini temp-tables into files
------------------------------------------------------------------------------*/
  /* variables */
  define variable tPrefix as character no-undo.
  
  /* get the prefix */
  publish "GetAppIni" (output tPrefix).
  tPrefix = tPrefix + "_".
  
  /* delete the current temp files */
  publish "DeleteTempFile" ("DataSysIni", output std-lo).
  publish "DeleteTempFile" ("DataTempSysIni", output std-lo).
  
  /* Active temp-tables */
  temp-table ini:write-xml("file", tPrefix + "SysIni.xml").
  publish "AddTempFile" ("DataSysIni", tPrefix + "SysIni.xml").
  
  /* "Temp" temp-tables */
  temp-table tempini:write-xml("file", tPrefix + "TempSysIni.xml").
  publish "AddTempFile" ("DataSysIni", tPrefix + "TempSysIni.xml").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysIniESB Procedure 
PROCEDURE SysIniESB :
/*------------------------------------------------------------------------------
@description Runs when an esb message is received
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey as character no-undo.
  
  /* exit if not correct entity */
  if pEntity <> "SysIni"
   then next.
  
  /* get the next number */
  std-in = 1.
  for each iniesbmsg no-lock:
    std-in = std-in + 1.
  end.
  
  /* create the esb message */
  create iniesbmsg.
  assign
    iniesbmsg.seq    = std-in
    iniesbmsg.rcvd   = now
    iniesbmsg.entity = pEntity
    iniesbmsg.action = pAction
    iniesbmsg.keyID  = pKey
    .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysIniTimer Include 
PROCEDURE SysIniTimer :
/*------------------------------------------------------------------------------
@description Triggered by appstart.i running on a timer
------------------------------------------------------------------------------*/
  /* buffers */
  define buffer iniesbmsg for iniesbmsg.

  /* loop through the esb messages */
  for each iniesbmsg exclusive-lock
  break by iniesbmsg.keyID
        by iniesbmsg.rcvd descending:

    /* if the ini is loaded and the we have a messages */
    if tLoadedSysIni and first-of(iniesbmsg.keyID) 
     then run SysIniChanged (iniesbmsg.keyID).
     
    /* delete the esb message and set the last sync */
    delete iniesbmsg.
    publish "SetLastSync".
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

