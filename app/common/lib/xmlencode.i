&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : xmlencode.i
    Purpose     : PARSE an XML response from the AES server
    Description :
    Author(s)   : D.Sinclair
    Created     : 1.24.2012
    Notes       : 
  ----------------------------------------------------------------------*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-decodeFromXml) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD decodeFromXml Procedure 
FUNCTION decodeFromXml RETURNS CHARACTER PRIVATE
  ( pEncoded as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-encodeForXml) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD encodeForXml Procedure 
FUNCTION encodeForXml RETURNS CHARACTER PRIVATE
  (pUnencoded as char)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-decodeFromXml) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION decodeFromXml Procedure 
FUNCTION decodeFromXml RETURNS CHARACTER PRIVATE
  ( pEncoded as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def var pResult as char no-undo.
 def var tCnt as int no-undo.
 def var tChar as char no-undo.

 pEncoded = replace(pEncoded, "&apos;", "'").
 pEncoded = replace(pEncoded, "&quot;", '"').
 pEncoded = replace(pEncoded, "&lsquo;", "'").
 pEncoded = replace(pEncoded, "&rsquo;", "'").
 pEncoded = replace(pEncoded, "&ldquo;", '"').
 pEncoded = replace(pEncoded, "&rdquo;", '"').
 pEncoded = replace(pEncoded, "&gt;", ">").
 pEncoded = replace(pEncoded, "&lt;", "<").
 pEncoded = replace(pEncoded, "&amp;", "&").
 pEncoded = replace(pEncoded, "&br;", "&#10;").

 pResult = pEncoded.
 do tCnt = 1 to length(pEncoded):
  tChar = substring(pEncoded, tCnt, 1).
  if asc(tChar) < 32
   then pResult = replace(pResult, tChar, ""). 
 end.

 /* At this point, we have & and &#10; both with an ampersand */
 RETURN pResult.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-encodeForXml) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION encodeForXml Procedure 
FUNCTION encodeForXml RETURNS CHARACTER PRIVATE
  (pUnencoded as char) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  pUnencoded = replace(pUnencoded, "&", "&amp;").
  /* The & would have been replaced in the original up above */
  pUnencoded = replace(pUnencoded, "&amp;#10;", "&#10;").
  
  pUnencoded = replace(pUnencoded, "'", "&apos;").
  pUnencoded = replace(pUnencoded, '"', "&quot;").
  pUnencoded = replace(pUnencoded, ">", "&gt;").
  pUnencoded = replace(pUnencoded, "<", "&lt;").

  pUnencoded = replace(pUnencoded, chr(13), "  ").
  pUnencoded = replace(pUnencoded, chr(10), "&#13;").
  pUnencoded = replace(pUnencoded, chr(145), "&apos;").
  pUnencoded = replace(pUnencoded, chr(146), "&apos;").
  pUnencoded = replace(pUnencoded, chr(147), "&quot;").
  pUnencoded = replace(pUnencoded, chr(148), "&quot;").

  RETURN pUnencoded.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

