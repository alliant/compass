&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file confirm-file-upload.i
@description Confirm if the user wants to upload the file

@author John Oliver
@created 9/26/2018
  ----------------------------------------------------------------------*/


/* ***************************  Definitions  ************************** */

&IF DEFINED(combo) = 0 &THEN
&SCOPED-DEFINE combo tFile
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

do:
  publish "GetConfirmFileUpload" (output std-lo).
  if std-lo and {&combo}:screen-value > ""
   then
    do:
      std-lo = false.
      MESSAGE "The current file will be replaced. Continue?" VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO TITLE "Confirmation" UPDATE std-lo.
      if not std-lo 
       then return no-apply.
    end.
end.