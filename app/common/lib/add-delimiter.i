&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------------
@description Adds a delimiter to the string
@param String;char;The string to make delimited
@param Delimiter;char;The delimiter to use
@returns The string with the delimiter if more than one
@notes Call the function: std-ch = addDelimiter(std-ch,",") + "string". When used
       in a loop, the varaiable std-ch will be "string,string,string". If just once,
       the variable std-ch will be "string".
------------------------------------------------------------------------------*/

&IF DEFINED(addDelimiter) = 0
&THEN

&IF DEFINED(EXCLUDE-addDelimiter) = 0 &THEN
FUNCTION addDelimiter RETURNS CHARACTER
  ( INPUT pString AS CHARACTER,
    INPUT pDelimiter AS CHARACTER )  FORWARD.
&ENDIF

FUNCTION addDelimiter RETURNS CHARACTER
  ( INPUT pString AS CHARACTER,
    INPUT pDelimiter AS CHARACTER ) :
  
  def var cDelimitedString as char no-undo.
  cDelimitedString = pString + (if pString <> "" then pDelimiter else "").
  
  return cDelimitedString.
  
END FUNCTION.
&GLOBAL-DEFINE addDelimiter true
&ENDIF


&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


