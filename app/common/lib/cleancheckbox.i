
for each {&hTemp}:
  IF VALID-HANDLE({&hTemp}.hToggle) THEN
    DELETE WIDGET {&hTemp}.hToggle.

  IF VALID-HANDLE({&hTemp}.hEditor) THEN
    DELETE WIDGET {&hTemp}.hEditor.
end.
