&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file excel.i
@description Constants for the Excel com-handle
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */


/* cell formats */
&if defined(excel-defined) = 0 &then
  &global-define excel-defined true
  /* excel com-handles */
  define variable chExcel      as com-handle no-undo.
  define variable chWorksheet  as com-handle no-undo.
  define variable chWorkbook   as com-handle no-undo.
  define variable chQueryTable as com-handle no-undo.
  define variable chChart      as com-handle no-undo.
  define variable chShape      as com-handle no-undo.
  /* excel row and column */
  define variable iRow         as integer no-undo initial 0.
  define variable iCol         as integer no-undo initial 0.
  define variable iTotalRow    as integer no-undo.
  define variable iTotalCol    as integer no-undo.
  define variable cCell        as character no-undo.
  define variable cLine        as character no-undo.
  define variable cValue       as character no-undo case-sensitive.
  /* hold the data types used */
  define variable dataTypes    as character no-undo.
  /* EXCEL CONSTANTS */
  /* formatting */
  &global-define xlFormatAccounting "_($* #,##0.00_);[RED]_($* (#,##0.00);_($* -??_);_(@_)"
  &global-define xlFormatNumber "#,##0_);[Red](#,##0);-_)"
  &global-define xlFormatPercentage "0.00%"
  &global-define xlFormatText "@"
  &global-define xlFormatDate "m/d/yy"
  &global-define xlFormatDateTime "m/d/yy [$-en-US]h:mm:ss AM/PM"
  /* window states */
  &global-define xlWindowMaximized -4137
  &global-define xlWindowMinimized -4140
  &global-define xlWindowNormal -4143	
  /* horizontal allignment */
  &global-define xlHAlignCenter -4108
  &global-define xlHAlignCenterAcrossSelection 7
  &global-define xlHAlignDistributed -4117
  &global-define xlHAlignFill 5
  &global-define xlHAlignGeneral 1
  &global-define xlHAlignJustify -4130
  &global-define xlHAlignLeft -4131
  &global-define xlHAlignRight -4152
  /* chart types */
  &global-define xlChartTypeBar 54
  &global-define xlChartTypeScatter 74
  &global-define xlChartTypeLine 65	
  /* border */
  &global-define xlBorderLeft 7
  &global-define xlBorderRight 10
  &global-define xlBorderTop 8
  &global-define xlBorderBottom 9
  &global-define xlBorderStyleSingle 1
  &global-define xlBorderStyleDouble -4119
  &global-define xlBorderWeightThin 2
  &global-define xlBorderWeightMedium 3
  &global-define xlBorderWeightThick 4
  /* colors */
  &global-define xlColorBlack 1
  &global-define xlColorWhite 2
  &global-define xlColorRed 3
  &global-define xlColorBrightGreen 4
  &global-define xlColorBlue 5
  &global-define xlColorYellow 6
  &global-define xlColorPink 7
  &global-define xlColorTurquoise 8
  &global-define xlColorDarkRed 9
  &global-define xlColorGreen 10
  &global-define xlColorDarkBlue 11
  &global-define xlColorDarkYellow 12
  &global-define xlColorViolet 13
  &global-define xlColorTeal 14
  &global-define xlColorGray25 15
  &global-define xlColorGray50 16
  &global-define xlColorPeriwinkle 17
  &global-define xlColorIvoryLite 19
  &global-define xlColorTurquoise 20
  &global-define xlColorDarkPurple 21
  &global-define xlColorCoral 22
  &global-define xlColorOceanBlue 23
  &global-define xlColorIceBlue 24
  &global-define xlColorSkyBlue 33
  &global-define xlColorLightTurquoise 34
  &global-define xlColorLightGreen 35
  &global-define xlColorLightYellow 36
  &global-define xlColorPaleBlue 37
  &global-define xlColorRose 38
  &global-define xlColorLavender 39
  &global-define xlColorTan 40
  &global-define xlColorLightBlue 41
  &global-define xlColorAqua 42
  &global-define xlColorLime 43
  &global-define xlColorGold 44
  &global-define xlColorLightOrange 45
  &global-define xlColorOrange 46
  &global-define xlColorBlueGray 47
  &global-define xlColorGray40 48
  &global-define xlColorDarkTeal 49
  &global-define xlColorSeaGreen 50
  &global-define xlColorDarkGreen 51
  &global-define xlColorOliveGreen 52
  &global-define xlColorBrown 53
  &global-define xlColorPlum 54
  &global-define xlColorIndigo 55
  &global-define xlColorGray80 56
  /* consolidation function */
  &global-define xlAverage -4106
  &global-define xlCount -4112
  &global-define xlMax -4136
  &global-define xlMin -4139
  &global-define xlProduct -4149
  &global-define xlSum -4157
  /* colors (easier to use) */
  &global-define xlHeaderBackColor {&xlColorDarkTeal}
  &global-define xlHeaderFontColor {&xlColorWhite}
  &global-define xlFooterBackColor {&xlColorOceanBlue}
  &global-define xlFooterFontColor {&xlColorWhite}
  &global-define xlEvenRow {&xlColorIvoryLite}
  &global-define xlOddRow {&xlColorWhite}
  /* cell columns */
  &global-define xlColumn "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,AA,AB,AC,AD,AE,AF,AG,AH,AI,AJ,AK,AL,AM,AN,AO,AP,AQ,AR,AS,AT,AU,AV,AW,AX,AY,AZ,BA,BB,BC,BD,BE,BF,BG,BH,BI,BJ,BK,BL,BM,BN,BO,BP,BQ,BR,BS,BT,BU,BV,BW,BX,BY,BZ"
&endif


/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


