&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file statedatasrv.i
@description Data Server file for managing the state

@author John Oliver
@created 04-17-2019
@notes Remade from commondatasrv.i
@modification
   date         Name           Description
   06/22/2022   SA             Task 93485 - added 'UpdateStateDetails'
                                procedure to update temp-table.
   07/18/2023   SK             Change in Getallstates.p
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}
define variable tLoadedStates as logical no-undo initial false.

/* tables */
{tt/state.i}
{tt/state.i &tableAlias="allstate"}
{tt/stateevent.i}
{tt/esbmsg.i &tableAlias="stateesbmsg"}

/* temp tables */
{tt/state.i &tableAlias="tempstate"}
{tt/stateevent.i &tableAlias="tempstateevent"}

/* functions */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshState Procedure
FUNCTION refreshState RETURNS logical
  ( input pStateID as character ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

subscribe to "ActivateState"       anywhere.
subscribe to "DeactivateState"     anywhere.
subscribe to "DeleteState"         anywhere.
subscribe to "GetActiveStates"     anywhere.
subscribe to "GetAllStates"        anywhere.
subscribe to "GetState"            anywhere.
subscribe to "GetStateDescription" anywhere.
subscribe to "GetStates"           anywhere.
subscribe to "LoadStates"          anywhere.
subscribe to "ModifyState"         anywhere.
subscribe to "StateChanged"        anywhere.
subscribe to "UpdateStateDetails"  anywhere.

/* state event */
subscribe to "DeleteStateEvent"    anywhere.
subscribe to "GetStateEvent"       anywhere.
subscribe to "GetStateEvents"      anywhere.
subscribe to "ModifyStateEvent"    anywhere.
subscribe to "NewStateEvent"       anywhere.
subscribe to "StateEventChanged"   anywhere.

/* used for the messaging system */
subscribe to "TIMER"               anywhere run-procedure "StateTimer".
subscribe to "ExternalEsbMessage"  anywhere run-procedure "StateESB".

/* used to dump the data file */
subscribe to "DataDump"            anywhere run-procedure "StateDataDump".

std-lo = true.
publish "SetSplashStatus".
publish "GetLoadStates" (output std-lo).
if std-lo
 then run LoadStates.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateActivate Procedure 
PROCEDURE ActivateState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pStateID as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  
  /* buffers */
  define buffer state for state.

  /* call to activate the state */
  run server/activatestate.p(input  pStateID,
                             output pSuccess,
                             output std-ch).

  /* if not successful, show the message */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else run StateChanged in this-procedure (pStateID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeactivateState Procedure 
PROCEDURE DeactivateState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pStateID as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  
  /* buffers */
  define buffer state for state.

  /* call to deactivate the state */
  run server/deactivatestate.p(input  pStateID,
                               output pSuccess,
                               output std-ch).

  /* if not successful, show the message */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else run StateChanged in this-procedure (pStateID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteState Include 
PROCEDURE DeleteState :
/*------------------------------------------------------------------------------
@description Deletes a state
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pStateID as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  
  /* buffers */
  define buffer state for state.
  
  /* call to delete the state */
  run server/deletestate.p (input  pStateID,
                            output pSuccess,
                            output std-ch).

  /* if not successful, show a message */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else run StateChanged in this-procedure (pStateID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteStateEvent Include 
PROCEDURE DeleteStateEvent :
/*------------------------------------------------------------------------------
@description Deletes a state event
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pStateID  as character   no-undo.
  define input  parameter pSeq      as integer     no-undo.
  define output parameter pSuccess  as logical     no-undo.
  
  /* buffers */
  define buffer stateevent for stateevent.

  /* call to delete the state event */
  run server/deletestateevent.p (input  pStateID,
                                 input  pSeq,
                                 output pSuccess,
                                 output std-ch).
  
  /* if not successful, show a message */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else run StateChanged in this-procedure (pStateID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetActiveStates Include 
PROCEDURE GetActiveStates :
/*------------------------------------------------------------------------------
@description Get the states that are active
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for tempstate.
  
  /* buffers */
  define buffer tempstate for tempstate.

  /* get all states */
  empty temp-table tempstate.
  run GetStates (output table tempstate).
   
  /* delete all states that aren't active */
  for each tempstate exclusive-lock
     where tempstate.active = false:
     
    delete tempstate.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetStates Include 
PROCEDURE GetAllStates :
/*------------------------------------------------------------------------------
@description Get the states
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for allstate.

  /* load the states if not loaded */ 
  if not tLoadedStates  
   then run LoadStates. 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetState Include 
PROCEDURE GetState :
/*------------------------------------------------------------------------------
@description Gets a state
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pStateID as character no-undo.
  define output parameter table for tempstate.
  
  /* buffers */
  define buffer tempstate for tempstate.

  /* get all states */
  empty temp-table tempstate.
  run GetStates (output table tempstate).

  /* delete all states that aren't the one the user wants */
  for each tempstate exclusive-lock
     where tempstate.stateID <> pStateID:
    
    delete tempstate.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetStateDescription Include 
PROCEDURE GetStateDescription :
/*------------------------------------------------------------------------------
@description Gets a state
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pStateID   as character no-undo.
  define output parameter pStateName as character no-undo.
  
  /* buffers */
  define buffer tempstate for tempstate.

  /* get the state information */
  empty temp-table tempstate.
  run GetState (pStateID, output table tempstate).

  /* get the state name if known, otherwise the name is "Unknown" */
  pStateName = "Unknown".
  for first tempstate no-lock:
    pStateName = tempstate.description.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetStateEvent Include 
PROCEDURE GetStateEvent :
/*------------------------------------------------------------------------------
@description Get a state event
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pStateID as character no-undo.
  define output parameter table for tempstateevent.

  /* get the state events */
  empty temp-table tempstateevent.
  run GetStateEvents (output table tempstateevent).

  /* delete all state events that aren't the from the state the user wants */
  for each tempstateevent exclusive-lock
     where tempstateevent.stateID <> pStateID:
    
    delete tempstateevent.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetStateEvent Include 
PROCEDURE GetStateEvents :
/*------------------------------------------------------------------------------
@description Get a state event
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for stateevent.

  /* load the states if not loaded */
  if not tLoadedStates
   then run LoadStates.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetStates Include 
PROCEDURE GetStates :
/*------------------------------------------------------------------------------
@description Get the states
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for state.

  /* load the states if not loaded */
  if not tLoadedStates 
   then run LoadStates.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadStates Include 
PROCEDURE LoadStates :
/*------------------------------------------------------------------------------
@description Loads the states and the state events
------------------------------------------------------------------------------*/
  /* call the server to get the state and state events */
  empty temp-table state.
  empty temp-table allstate.
  run server/getstatedetails.p (input  "",
                                output table state,
                                output table stateevent,
                                output tLoadedStates,
                                output std-ch).
  
  if tLoadedStates 
   then run server/getallstates.p (input  "", 
                                   output table allstate, 
                                   output tLoadedStates, 
                                   output std-ch). 

  /* if unsuccessful, show a message if debug is on */
  if not tLoadedStates
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadStates failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyState Include 
PROCEDURE ModifyState :
/*------------------------------------------------------------------------------
@description Modifies a state
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter table for tempstate.
  define output parameter pSuccess as logical no-undo initial false.
  
  /* buffers */
  define buffer tempstate for tempstate.
  define buffer state for state.
  
  /* call the server to modify the state */
  run server/modifystate.p (input-output table tempstate,
                            output pSuccess,
                            output std-ch).

  /* if unsuccessful, show a message */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    /* otherwise, copy the modified state data to the state record */
    for first tempstate no-lock:
      run StateChanged (tempstate.stateID).
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyStateEvent Include 
PROCEDURE ModifyStateEvent :
/*------------------------------------------------------------------------------
@description Modifies a state event
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter table for tempstateevent.
  define output parameter pSuccess as logical no-undo initial false.
  
  /* buffers */
  define buffer tempstateevent for tempstateevent.
  define buffer stateevent for stateevent.
  
  /* call the server to modify the state event */
  run server/modifystateevent.p (input-output table tempstateevent,
                                 output pSuccess,
                                 output std-ch).

  /* if unsuccessful, show a message */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    /* otherwise, copy the modified state event data to the state event record */
    for first tempstateevent no-lock:
      run StateChanged (tempstateevent.stateID).
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewStateEvent Include 
PROCEDURE NewStateEvent :
/*------------------------------------------------------------------------------
@description Creates a new state event
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter table for tempstateevent.
  define output parameter pSuccess as logical no-undo initial false.
  
  /* buffers */
  define buffer stateevent for stateevent.

  /* call the server to create a new state event */
  run server/newstateevent.p (input-output table tempstateevent,
                              output pSuccess,
                              output std-ch).

  /* if unsuccessful, show a message */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    /* otherwise, create a new state event */
    for first tempstateevent no-lock:
      run StateChanged (tempstateevent.stateID).
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateChanged Include 
PROCEDURE StateChanged :
/*------------------------------------------------------------------------------
@description Reloads a state
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pStateID as character no-undo.
  
  /* refresh the state */
  refreshState(pStateID).
  
  /* publish that the state data changed */
  publish "StateDataChanged".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateChanged Include 
PROCEDURE StateEventChanged :
/*------------------------------------------------------------------------------
@description Reloads a state
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pStateID as character no-undo.
  
  /* call the state change procedure */
  run StateChanged (pStateID).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateDataDump Include 
PROCEDURE StateDataDump :
/*------------------------------------------------------------------------------
@description Dump the state and tempstate temp-tables into files
------------------------------------------------------------------------------*/
  /* variables */
  define variable tPrefix as character no-undo.
  
  /* get the prefix */
  publish "GetAppCode" (output tPrefix).
  tPrefix = tPrefix + "_".
  
  /* delete the current temp files */
  publish "DeleteTempFile" ("DataState", output std-lo).
  publish "DeleteTempFile" ("DataTempState", output std-lo).
  
  /* Active temp-tables */
  temp-table state:write-xml("file", tPrefix + "State.xml").
  publish "AddTempFile" ("DataState", tPrefix + "State.xml").
  
  /* "Temp" temp-tables */
  temp-table tempstate:write-xml("file", tPrefix + "TempState.xml").
  publish "AddTempFile" ("DataState", tPrefix + "TempState.xml").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateESB Procedure 
PROCEDURE StateESB :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey    as character no-undo.
  
  /* exit if not correct entity */
  if pEntity <> "State" or pEntity <> "StateEvent"
   then next.
  
  /* get the next number */
  std-in = 1.
  for each stateesbmsg no-lock:
    std-in = std-in + 1.
  end.
  
  /* create the esb message */
  create stateesbmsg.
  assign
    stateesbmsg.seq    = std-in
    stateesbmsg.rcvd   = now
    stateesbmsg.entity = pEntity
    stateesbmsg.action = pAction
    stateesbmsg.keyID  = pKey
    .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateTimer Procedure 
PROCEDURE StateTimer :
/*------------------------------------------------------------------------------
  Purpose:     Called when the application is idle
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* buffers */
  define buffer stateesbmsg for stateesbmsg.

  /* loop through the esb messages */
  for each stateesbmsg exclusive-lock
  break by stateesbmsg.keyID
        by stateesbmsg.rcvd descending:

    /* if the state is loaded and the we have a messages */
    if tLoadedStates and first-of(stateesbmsg.keyID) 
     then run StateChanged (stateesbmsg.keyID).
     
    /* delete the esb message and set the last sync */
    delete stateesbmsg.
    publish "SetLastSync".
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE UpdateStateDetails Include 
PROCEDURE UpdateStateDetails :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter cRegionList  as character no-undo.
  
  /*local variables*/
  define  variable i as integer    no-undo.
  
  do i = 1 to num-entries(cRegionList):
   if i mod 2 = 0 
    then
     next.
   else 
     do:    /*adding the regions*/
       for first state where stateID = entry(i,cRegionList):
         state.region = entry(i + 1,cRegionList).
       end.
     end.
    
  end.
  
  /* removing the regions*/
  for each state no-lock:
    if lookup(state.stateID,cRegionList) = 0 
     then
      state.region = "".
  end.

  /* publish that the state data changed */
  publish "StateDataChanged".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshState Include 
FUNCTION refreshState RETURNS logical
  ( input pStateID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* variables */
  define variable lSuccess as logical no-undo.
  
  /* buffers */
  define buffer tempstate for tempstate.
  define buffer state for state.
  
  /* call the server */
  empty temp-table tempstate.
  run server/getstatedetails.p (pStateID,
                                output table tempstate,
                                output table tempstateevent,
                                output lSuccess,
                                output std-ch).

  /* if unsuccessful, show a message if debug is on */
  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshState failed: " + std-ch view-as alert-box warning.
    end.
   else
    /* otherwise, copy the state details to the state records */
    do:
      /* state refresh */
      find tempstate where tempstate.stateID = pStateID no-error.
      find state where state.stateID = pStateID no-error.
      if not available tempstate and available state /* delete */
       then delete state.
      if available tempstate and not available state /* new */
       then create state.
      if available tempstate and available state /* modify */
       then buffer-copy tempstate to state.
      /* allstate refresh */
      find tempstate where tempstate.stateID = pStateID no-error.
      find allstate where allstate.stateID = pStateID no-error.
      if not available tempstate and available allstate /* delete */
       then delete allstate.
      if available tempstate and not available allstate /* new */
       then create allstate.
      if available tempstate and available allstate /* modify */
       then buffer-copy tempstate to allstate.
      /* state event refresh */
      for each stateevent exclusive-lock
         where stateevent.stateID = pStateID:
         
        delete stateevent.
      end.
      for each tempstateevent exclusive-lock
         where tempstateevent.stateID = pStateID:
        
        create stateevent.
        buffer-copy tempstateevent to stateevent.
      end.
    end.
  
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

