&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file perioddatasrv.i
@description Data Server file for managing the period

@param period;char;Either "Accounting" or "Batch"

@author John Oliver
@created 07-11-2018
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}
define variable tLoadedPeriods as logical initial false no-undo.

/* tables */
{tt/period.i}
{tt/esbmsg.i &tableAlias="periodesbmsg"}

/* temp tables */
{tt/period.i &tableAlias="tempperiod"}

/* preprocessors */
&if defined(period) = 0 &then
&scoped-define period "Batch"
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshPeriod Procedure
FUNCTION refreshPeriod RETURNS logical
  ( input pPeriodID as integer ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* main procedures */
subscribe to "ClosePeriod"        anywhere.
subscribe to "DeletePeriod"       anywhere.
subscribe to "GetPeriodID"        anywhere.
subscribe to "GetPeriods"         anywhere.
subscribe to "LoadPeriods"        anywhere.
subscribe to "NewPeriod"          anywhere.
subscribe to "OpenPeriod"         anywhere.
subscribe to "PeriodChanged"      anywhere.

/* questions */
subscribe to "IsPeriodOpen"       anywhere.

/* used for the messaging system */
subscribe to "TIMER"              anywhere run-procedure "PeriodTimer".
subscribe to "ExternalEsbMessage" anywhere run-procedure "PeriodESB".

/* used to dump the data file */
subscribe to "DataDump"           anywhere run-procedure "PeriodDataDump".

/* load system users */
std-lo = true.
publish "SetSplashStatus".
publish "GetLoadPeriods" (output std-lo).
if std-lo 
 then run LoadPeriods.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ClosePeriod Include 
PROCEDURE ClosePeriod :
/*------------------------------------------------------------------------------
@description Closes a period
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pPeriodID as integer no-undo.
  
  /* buffers */
  define buffer period for period.

  /* call the server */
  std-lo = false.
  case {&period}:
   when "Batch" then run server/closeperiod.p (pPeriodID,
                                               output std-lo,
                                               output std-ch).
   when "Accounting" then run server/closeaccountingperiod.p (pPeriodID,
                                                              output std-lo,
                                                              output std-ch).
  end case.
  
  /* if unsuccessful, show a message */
  if not std-lo
   then message std-ch view-as alert-box error.
   else
    /* otherwise, set the acive flag */
    for first period exclusive-lock
        where period.periodID = pPeriodID:
      
      case {&period}:
       when "Batch" then period.active = no.
       when "Accounting" then period.activeAccounting = no.
      end case.
      publish "PeriodDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeletePeriod Include 
PROCEDURE DeletePeriod :
/*------------------------------------------------------------------------------
@description Deletes a period
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pPeriodID as integer no-undo.
  define output parameter pSuccess  as logical no-undo initial false.
  
  /* buffers */
  define buffer period for period.

  /* call the server */
  run server/deleteperiod.p (input pPeriodID,
                             output pSuccess,
                             output std-ch).

  /* if unsuccessful, show a message */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    /* otherwise, delete the period */
    for first period exclusive-lock
        where period.periodID = pPeriodID:
      
      delete period.
      publish "PeriodDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPeriodID Include 
PROCEDURE GetPeriodID :
/*------------------------------------------------------------------------------
@description Gets the period id
------------------------------------------------------------------------------*/
  /* parameters */
  define input param pMonth as integer no-undo.
  define input param pYear as integer no-undo.
  define output param pPeriodID as integer no-undo initial 0.
  
  /* buffers */
  define buffer period for period.

  /* get the periods */
  publish "GetPeriods" (output table period).
  
  /* get the correct period */
  for first period no-lock
      where period.periodMonth = pMonth
        and period.periodYear = pYear:

    pPeriodID = period.periodID.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPeriods Include 
PROCEDURE GetPeriods :
/*------------------------------------------------------------------------------
@description Gets the periods
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for period.

  /* if the periods are not loaded, then load them */
  if not tLoadedPeriods
   then publish "LoadPeriods".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsPeriodOpen Include 
PROCEDURE IsPeriodOpen :
/*------------------------------------------------------------------------------
@description Checks if the period is open or not
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pMonth as integer no-undo.
  define input  parameter pYear  as integer no-undo.
  define output parameter pOpen  as logical initial false.
  
  /* buffers */
  define buffer tempperiod for tempperiod.

  /* get all the periods */
  empty temp-table tempperiod.
  run GetPeriods (output table tempperiod).

  /* determine if the period is open for the month and year */
  for first tempperiod no-lock
      where tempperiod.periodMonth = pMonth
        and tempperiod.periodYear = pYear:
    
    case {&period}:
     when "Batch" then pOpen = (tempperiod.active = yes).
     when "Accounting" then pOpen = (tempperiod.activeAccounting = yes).
    end case.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadPeriods Include 
PROCEDURE LoadPeriods :
/*------------------------------------------------------------------------------
@description Loads the periods
------------------------------------------------------------------------------*/
  /* call the server */
  empty temp-table period.
  case {&period}:
   when "Batch" then run server/getperiods.p (input  0,
                                              output table period,
                                              output tLoadedPeriods,
                                              output std-ch).
   when "Accounting" then run server/getaccountingperiods.p (input  0,
                                                             output table period,
                                                             output tLoadedPeriods,
                                                             output std-ch).
  end case.

  /* if unsuccessful, show a message if debug */
  if not tLoadedPeriods
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadPeriods failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewPeriod Include 
PROCEDURE NewPeriod :
/*------------------------------------------------------------------------------
@description Creates a new period
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter pSuccess as logical no-undo initial false.
  
  /* buffers */
  define buffer tempperiod for tempperiod.
  
  /* call the server */
  empty temp-table tempperiod.
  run server/newperiod.p (output table tempperiod,
                          output pSuccess,
                          output std-ch).

  /* if unsuccessful, show the message why */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    /* otherwise, create the period */
    for first tempperiod no-lock:
      create period.
      buffer-copy tempperiod to period.
      publish "PeriodDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PeriodChanged Include 
PROCEDURE PeriodChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pPeriodID as integer no-undo.
  
  /* refresh the alert */
  refreshPeriod(pPeriodID).
  
  /* publish that the data changed */
  publish "PeriodDataChanged".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PeriodDataDump Include 
PROCEDURE PeriodDataDump :
/*------------------------------------------------------------------------------
@description Dump the period and tempperiod temp-tables into files
------------------------------------------------------------------------------*/
  /* variables */
  define variable tPrefix as character no-undo.
  
  /* get the prefix */
  publish "GetAppUser" (output tPrefix).
  tPrefix = tPrefix + "_".
  
  /* delete the current temp files */
  publish "DeleteTempFile" ("DataPeriod", output std-lo).
  publish "DeleteTempFile" ("DataTempPeriod", output std-lo).
  
  /* Active temp-tables */
  temp-table period:write-xml("file", tPrefix + "Period.xml").
  publish "AddTempFile" ("DataPeriod", tPrefix + "Period.xml").
  
  /* "Temp" temp-tables */
  temp-table tempperiod:write-xml("file", tPrefix + "TempPeriod.xml").
  publish "AddTempFile" ("DataPeriod", tPrefix + "TempPeriod.xml").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PeriodESB Procedure 
PROCEDURE PeriodESB :
/*------------------------------------------------------------------------------
@description Runs when an esb message is received
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey as character no-undo.
  
  /* exit if not correct entity */
  if pEntity <> "Period"
   then next.
  
  /* get the next number */
  std-in = 1.
  for each periodesbmsg no-lock:
    std-in = std-in + 1.
  end.
  
  /* create the esb message */
  create periodesbmsg.
  assign
    periodesbmsg.seq    = std-in
    periodesbmsg.rcvd   = now
    periodesbmsg.entity = pEntity
    periodesbmsg.action = pAction
    periodesbmsg.keyID  = pKey
    .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PeriodTimer Include 
PROCEDURE PeriodTimer :
/*------------------------------------------------------------------------------
@description Triggered by appstart.i running on a timer
------------------------------------------------------------------------------*/
  /* buffers */
  define buffer periodesbmsg for periodesbmsg.

  /* loop through the esb messages */
  for each periodesbmsg exclusive-lock
  break by periodesbmsg.keyID
        by periodesbmsg.rcvd descending:

    /* if the period is loaded and the we have a messages */
    if tLoadedPeriods and first-of(periodesbmsg.keyID) 
     then run PeriodChanged (periodesbmsg.keyID).
     
    /* delete the esb message and set the last sync */
    delete periodesbmsg.
    publish "SetLastSync".
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenPeriod Include 
PROCEDURE OpenPeriod :
/*------------------------------------------------------------------------------
@description Opens a period
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pPeriodID as integer no-undo.
  
  /* buffers */
  define buffer period for period.

  /* open the period */
  std-lo = false.
  case {&period}:
   when "Batch" then run server/openperiod.p (pPeriodID,
                                               output std-lo,
                                               output std-ch).
   when "Accounting" then run server/openaccountingperiod.p (pPeriodID,
                                                              output std-lo,
                                                              output std-ch).
  end case.
  if not std-lo
   then message std-ch view-as alert-box error.
   else
    for first period exclusive-lock
        where period.periodID = pPeriodID:
      
      case {&period}:
       when "Batch" then period.active = yes.
       when "Accounting" then period.activeAccounting = yes.
      end case.
      publish "PeriodDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshPeriod Include 
FUNCTION refreshPeriod RETURNS logical
  ( input pPeriodID as integer ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* parameters */
  define variable lSuccess as logical no-undo.
  
  /* buffers */
  define buffer tempperiod for tempperiod.
  define buffer period for period.
  
  /* call the server */
  empty temp-table tempperiod.
  case {&period}:
   when "Batch" then run server/getperiods.p (pPeriodID,
                                              output table period,
                                              output lSuccess,
                                              output std-ch).
   when "Accounting" then run server/getaccountingperiods.p (pPeriodID,
                                                             output table period,
                                                             output lSuccess,
                                                             output std-ch).
  end case.

  /* if unsuccessful, show a message if debug is on */
  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshPeriod failed: " + std-ch view-as alert-box warning.
    end.
   else
    /* otherwise, copy the period details into the period records */
    do:
      find tempperiod where tempperiod.periodID = pperiodID no-error.
      find period where period.periodID = pperiodID no-error.
      if not available tempperiod and available period /* delete */
       then delete period.
      if available tempperiod and not available period /* new */
       then create period.
      if available tempperiod and available period /* modify */
       then buffer-copy tempperiod to period.
    end.
  
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

