&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/dobutton.i 
    Modification:
    Date          Name      Description
    03/07/2016    AG        Launch FindBp window from progress menu
*/

 def input parameter pSeq as int.
 def var tHasFinding as logical.
 def var tButtonHandle as handle.

 tButtonHandle = focus.
 if not valid-handle(tButtonHandle) 
    or (valid-handle(tButtonHandle) and tButtonHandle:type <> "BUTTON")
  then return.

 publish "ViewFinding" (pSeq, "Section", output tHasFinding).
 /* das
 if tHasFinding 
  then tButtonHandle:load-image("images/notes.bmp").
  else tButtonHandle:load-image("images/notesblank.bmp").
   */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


