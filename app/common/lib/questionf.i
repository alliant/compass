&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/questionf.i
  seq:          Unique question identifier (Mandatory)  
  exclude-vars: Non-blank to exclude variable definitions
  var-id:       Variable to hold question ID (default tQuestionID)
  var-desc:     Variable to hold description of question (default tDesc)
  var-priority: Variable to hold priority of question (default tPriority)
  questionType: Name of event to publish (default "Question")
  r:            Row where widgets should be placed in the frame
  questionHeight:Pixel height of the question (default = 53)
  ----------------------------------------------------------------------*/
 /*------------------------------------------------------------------------
    File        :   lib/questionf.i
    Modification:
    Date          Name      Description
    11/14/2016    AC        Implement question priority = 0 functionality
    11/17/2016    AC        On resize of window, set question text width as original.
    02/08/2017    AG        Modified to send the current screen value of the widget.
  ----------------------------------------------------------------------*/

&IF defined(exclude-question-vars) = 0 &then
def var tQuestionID as char no-undo.
def var tQuestionDesc as char no-undo.
def var tQuestionPriority as int no-undo.
def var tQuestionWarningAnswer as char no-undo.
def var tQuestionWarningMsg as char no-undo.
def var tQuestionWarningTip as char no-undo.
def var tQuestionFrameHeight as int no-undo.
define variable tQuestionDisplay as character no-undo.
define variable tQuestionSize as integer no-undo.
define variable questionpriority as integer no-undo.
&global-define exclude-question-vars true
&ENDIF

&IF defined(var-id) = 0 &THEN
&scoped-define var-id tQuestionID
&ENDIF

&IF defined(var-desc) = 0 &THEN
&scoped-define var-desc tQuestionDesc
&ENDIF

&IF defined(var-priority) = 0 &THEN
&scoped-define var-priority tQuestionPriority
&ENDIF

&IF defined(var-warninganswer) = 0 &THEN
&SCOPED-DEFINE var-warninganswer tQuestionWarningAnswer
&ENDIF

&IF defined(var-warningmsg) = 0 &THEN
&SCOPED-DEFINE var-warningmsg tQuestionWarningMsg
&ENDIF

&IF defined(var-warningtip) = 0 &THEN
&SCOPED-DEFINE var-warningtip tQuestionWarningTip
&ENDIF

&if defined(questionType) = 0 &then
&scoped-define questionType Question
&endif


&IF defined(ex-frameresize) = 0 &then
tQuestionFrameHeight = tQuestionFrameHeight + 
  &IF defined(questionHeight) = 0 &THEN 53 &ELSE {&questionHeight} &ENDIF.
frame fSection:virtual-height-pixels = max(frame fSection:height-pixels, tQuestionFrameHeight).
&ENDIF

publish "Get{&questionType}Attributes" (input {&seq},
                                        output {&var-id},
                                        output {&var-desc}, 
                                        output {&var-priority},
                                        output std-lo,
                                        output {&var-warninganswer},
                                        output {&var-warningmsg},
                                        output {&var-warningtip}).

/* Taking care of adding "*" in questions, when question priority is 0 */
if {&var-priority} eq 0 then
  {&var-desc} = trim({&var-desc} + "" + fill("*", {&var-priority})).
else                                   
  {&var-desc} = trim({&var-desc} + " " + fill("*", {&var-priority} - 1)).


/* Question number */
create text n{&seq}
 assign
   name = "n{&seq}"
   frame = frame fSection:handle
   column = 2.0
   row = {&r}
   width-chars = 11.0
   height-chars = 1.0
   sensitive = false
   visible = true
   format = "x(10)"
   .
/* Taking care of font of question sequence, when question priority is 0 */
if {&var-priority} eq 0 then
  n{&seq}:font = 15 + {&var-priority} + 1 .
else
  n{&seq}:font = 15 + {&var-priority}.

n{&seq}:screen-value = {&var-id}.


/* Question text */
create text t{&seq}
 assign
   name = "t{&seq}"
   frame = frame fSection:handle
   column = 14.0
   row = {&r}
   width-chars = 107.0
   height-chars = 1.0
   sensitive = false
   visible = true
   format = "x(200)"
   .
/* Taking care of font of question text, when question priority is 0 */
if {&var-priority} eq 0 then
  t{&seq}:font = 15 + {&var-priority} + 1.
else
  t{&seq}:font = 15 + {&var-priority}.

t{&seq}:screen-value = {&var-desc}.
&global-define t{&seq}Resize t{&seq}:width-chars = frame fSection:width-chars - 57 . 


/* Fill-in field */
create fill-in q{&seq}
 assign
   name = "q{&seq}"
   frame = frame fSection:handle
   column = 118.0
   row = {&r}
   width-chars = 28.0
   height-chars = 1.0
   sensitive = true
 &if defined(bg-color) <> 0 &then
   bgcolor = {&bg-color}
 &endif
   visible = true
   format = "x(256)"
   triggers:
    ON leave persistent run doFillin in this-procedure ({&seq},q{&seq}:screen-value).
   end triggers.
   .
&global-define q{&seq}Resize q{&seq}:x = frame fSection:width-pixels - 205.


/* Finding/Best Practice button */
create button b{&seq}
  assign
    name = {&var-id}
    frame = frame fSection:handle
    column = 149
    row = {&r}
    sensitive = true
    visible = true
    width = 5.0
    height = 1.14
    tab-stop = false
   triggers:
    ON choose persistent run doButton in this-procedure ({&seq}).
   end triggers.
b{&seq}:load-image("images/notesblank.bmp").
b{&seq}:load-image-insensitive("images/notesdisabled.bmp").

/* Disable 'finding/best practice button when question priority is 0 */
if {&var-priority} eq 0 then
  b{&seq}:sensitive = false .

&global-define b{&seq}Resize b{&seq}:x = frame fSection:width-pixels - 50.


/* Dividing line rectangle */
&IF defined(ex-r) = 0 &THEN
create rectangle r{&seq}
 assign
   name = "r{&seq}"
   frame = frame fSection:handle
   x = 5
   row = {&r} + 1.7
   width-pixels = 780
   height-pixels = 1
   graphic-edge = true
   sensitive = false
   visible = true
   filled = true
   fgcolor = 8
   .
&global-define r{&seq}Resize r{&seq}:width-pixels = frame fSection:width-pixels - 10.
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 14.91
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


