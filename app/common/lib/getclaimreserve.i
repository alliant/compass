/*---------------------------------------------------------------------
@name getclaimreserve.i
@description Get the reserve balance for the reference category

@param {1} is the claim ID (int)
@param {2} is the reference (char)
@param {3} is the return variable (decimal)

@author John Oliver
@version 1.0
@created 08/6/16
@notes The formula is:
       AllReserve - PostedPayableTransactions
---------------------------------------------------------------------*/

{3} = 0.0.
FOR EACH claimadjtrx NO-LOCK
   WHERE claimadjtrx.claimID = {1}
     AND claimadjtrx.refCategory = {2}:
  
  /* add the reserve amounts */
  {3} = {3} + claimadjtrx.transAmount.
END.
FOR EACH aptrx NO-LOCK
   WHERE aptrx.refID = STRING({1})
     AND aptrx.refCategory = {2}:
     
  /* subtract the complete invoices */
  {3} = {3} - aptrx.transAmount.
END.
