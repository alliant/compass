&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------------
@description Additional server calls for the ap module

@changelog
------------------------------------------------------------------------------
Date        Author  Change
------------------------------------------------------------------------------
2017-02-22  JO      Added the procedure GetPayableInvoiceApprovedDate
2017-05-04  JO      Changed the order of events when modifying/creating a
                    payable to upload the document until last. Also, uploading
                    the document has no bearing on whether the modify/create 
                    succeeds or not.
------------------------------------------------------------------------------*/

&IF DEFINED(refType) = 0 &THEN
&SCOPED-DEFINE refType
&ENDIF

&IF DEFINED(refID) = 0 &THEN
&SCOPED-DEFINE refID refID
&ENDIF

&IF DEFINED(refSeq) = 0 &THEN
&SCOPED-DEFINE refSeq refSeq
&ENDIF

define variable refID as character no-undo initial "".
define variable refSeq as integer no-undo initial 0.
define variable cAccountUser as character no-undo.
define variable cAccountName as character no-undo.
define variable cAccountPass as character no-undo.

define variable lPayableLoaded as logical no-undo initial false.
define variable lReceivableLoaded as logical no-undo initial false.

{tt/apinv.i}
{tt/apinva.i}
{tt/apinvd.i}
{tt/aptrx.i}
{tt/arinv.i}
{tt/artrx.i}
{tt/checkrequest.i}
{tt/ini.i}
{tt/apinv.i &tableAlias="tempapinv"}
{tt/apinv.i &tableAlias="userapinv"}
{tt/apinva.i &tableAlias="tempapinva"}
{tt/apinvd.i &tableAlias="tempapinvd"}
{tt/aptrx.i &tableAlias="tempaptrx"}
{tt/arinv.i &tableAlias="userarinv"}
{tt/arinv.i &tableAlias="temparinv"}
{tt/artrx.i &tableAlias="tempartrx"}
{tt/sysprop.i &tableAlias="apsysprops"}

{lib/std-def.i}
{lib/winlaunch.i}
{lib/account-functions.i}
{lib/account-repository.i}
{lib/count-rows.i}
{lib/add-delimiter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 28.71
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

publish "GetCredentialsID" (output cAccountUser).
publish "GetCredentialsName" (output cAccountName).
publish "GetCredentialsPassword" (output cAccountPass).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ApprovePayableInvoice Include 
PROCEDURE ApprovePayableInvoice :
/*------------------------------------------------------------------------------
@description Approves a payable invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pAmount as decimal no-undo.
  define input parameter pNotes as character no-undo.
  define input parameter pUser as character no-undo.
  define output parameter pSuccess as logical no-undo.
  
  define buffer apinva for apinva.
  
  assign
    std-in = 0
    std-de = 0
    pSuccess = true
    .
  
  if pSuccess
   then
    for first apinv no-lock
        where apinv.apinvID = pInvoiceID:
                  
      for first apinva exclusive-lock
          where apinva.apinvID = apinv.apinvID
            and apinva.uid = pUser
            and apinva.stat = "P":
          
        std-de = 0.
        run server/approveapinvoice.p (input pInvoiceID,
                                       input apinva.seq,
                                       input pAmount,
                                       input pNotes,
                                       input apinv.refType,
                                       output std-de,
                                       output pSuccess,
                                       output std-ch
                                       ).
        if pSuccess
         THEN
          DO:
            if std-de > 0
             then run AddClaimReserve in this-procedure (INTEGER(apinv.refID), apinv.refCategory, std-de) no-error.
             
            ASSIGN
              apinva.stat = "A"
              apinva.amount = pAmount
              apinva.notes = pNotes
              apinva.dateActed = now
              apinva.uid = cAccountUser
              .
            publish "ClaimAccountingDataChanged".
            /** Auto refresh the claim note **/
            publish "RefreshNotes".
          END.
      end.
    end.
      
  if not pSuccess and std-ch > ""
   then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ApproveReceivableInvoice Include 
PROCEDURE ApproveReceivableInvoice :
/*------------------------------------------------------------------------------
@description Approves a receivable invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pSuccess as logical no-undo.
  
  define buffer arinv for arinv.
  
  assign
    pSuccess = true
    .
  
  if pSuccess
   then
    for first arinv exclusive-lock
        where arinv.arinvID = pInvoiceID:
                  
      run server/approvearinvoice.p (input pInvoiceID,
                                     output pSuccess,
                                     output std-ch
                                     ).
      if pSuccess
       THEN
        DO:
          arinv.stat = "A".
          publish "ClaimAccountingDataChanged".
        END.
  end.
      
  if not pSuccess and std-ch > ""
   then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AttachPayableDocument Include 
PROCEDURE AttachPayableDocument :
/*------------------------------------------------------------------------------
@description Attaches a document to a payable invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pUserFile as character no-undo.
  define output parameter pSuccess as logical no-undo.
  
  define variable cNewDir as character no-undo.
  define variable cFilePath as character no-undo.
  define variable cFileName as character no-undo.
  define variable cExtension as character no-undo.
  
  pSuccess = false.
  empty temp-table tempapinv.
  for first apinv exclusive-lock
      where apinv.apinvID = pInvoiceID:
      
    assign
      cNewDir    = getPayableServerPath(apinv.vendorID,apinv.invoiceDate)
      cExtension = entry(num-entries(pUserFile,"."),pUserFile,".")
      cFileName  = getPayableFilename(apinv.apinvID,apinv.invoiceNumber,apinv.refType,apinv.refID)
      cFilePath  = cFileName + "." + cExtension
      .
    /* copy the file as we changed the filename */
    os-copy value(pUserFile) value(cFilePath).
    publish "AddTempFile" ("Invoice",cFilePath).
    /* upload the file */
    run repository/newrepositoryfolder.p (cAccountUser, cAccountPass, cNewDir, output pSuccess, output std-ch).
    if pSuccess
     then run repository/uploadrepositoryfile.p (cAccountUser, cAccountPass, cFilePath, cNewDir, output pSuccess, output std-ch).
    publish "DeleteTempFile" ("Invoice", output std-lo).
    /* create a sysdoc record */
    if pSuccess
     then 
      do:
        run server/linkdocumentwithid.p (input "Invoice-AP", 
                                         input apinv.apinvID, 
                                         input 0,
                                         input cFileName,
                                         input cExtension,
                                         output std-lo,
                                         output std-ch).
                                         
        publish "ClaimAccountingDataChanged".
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AttachReceivableDocument Include 
PROCEDURE AttachReceivableDocument :
/*------------------------------------------------------------------------------
@description Attaches a document to a receivable invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pUserFile as character no-undo.
  define output parameter pSuccess as logical no-undo.
  
  define variable cNewDir as character no-undo.
  define variable cFilePath as character no-undo.
  define variable cFileName as character no-undo.
  define variable cExtension as character no-undo.
  
  pSuccess = false.
  for first arinv exclusive-lock
      where arinv.arinvID = pInvoiceID:
      
    assign
      cNewDir    = getReceivableServerPath(arinv.dateRequested)
      cExtension = entry(num-entries(pUserFile,"."),pUserFile,".")
      cFileName  = getReceivableFileName(arinv.arinvID,arinv.name,arinv.refType,arinv.refID)
      cFilePath  = cFileName + "." + cExtension
      .
    /* copy the file as we changed the filename */
    os-copy value(pUserFile) value(cFilePath).
    publish "AddTempFile" ("Invoice",cFilePath).
    /* upload the file */
    run repository/newrepositoryfolder.p (cAccountUser, cAccountPass, cNewDir, output pSuccess, output std-ch).
    if pSuccess
     then run repository/uploadrepositoryfile.p (cAccountUser, cAccountPass, cFilePath, cNewDir, output pSuccess, output std-ch).
    publish "DeleteTempFile" ("Invoice", output std-lo).
    /* create a sysdoc record */
    if pSuccess
     then 
      do:
        run server/linkdocumentwithid.p (input "Invoice-AR", 
                                         input arinv.arinvID, 
                                         input 0,
                                         input cFileName,
                                         input cExtension,
                                         output std-lo,
                                         output std-ch).
                                         
        publish "ClaimAccountingDataChanged".
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ClaimPayable Include 
PROCEDURE ClaimPayable :
/*------------------------------------------------------------------------------
@description Server call to see if any of the claim features are triggered
------------------------------------------------------------------------------*/
  define input parameter table for userapinv.
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pSuccess as logical no-undo.
  
  define variable dReserve as decimal no-undo initial 0.
  define variable iTrxID as integer no-undo initial 0.
  define variable lIsApproved as logical no-undo initial false.
  
  run server/claiminvoice.p (input table userapinv,
                             output iTrxID,
                             output dReserve,
                             output lIsApproved,
                             output pSuccess,
                             output std-ch).
  if not pSuccess
   then 
    do:
      if std-ch > ""
       then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
      return.
    end.
  
  /* if auto added reserve and in claim module */
  /* if not in claim module, publish will not do anything */
  IF dReserve > 0
   then
    for first userapinv:
      run AddClaimReserve in this-procedure (INTEGER(userapinv.refID), userapinv.refCategory, dReserve) no-error.
    end.
   
  for first apinv exclusive-lock
      where apinv.apinvID = pInvoiceID:
      
    /* if auto approved */
    if lIsApproved
     then 
      for each apinva exclusive-lock
         where apinv.apinvID = apinv.apinvID:
      
        assign
          apinva.stat = "A"
          apinva.amount = apinv.amount
          apinva.notes = "Automatically approved due to user's limit"
          apinva.dateActed = now
          .
        
        std-lo = false.
        run HasPayableInvoiceApprovedAll (pInvoiceID, output std-lo).
        if std-lo
         then apinv.stat = "A".
        /** Auto refresh the claim note **/
        publish "RefreshNotes".

      end.

    /* if auto completed */
    if iTrxID > 0
     then 
      do:
        create aptrx.
        assign
          aptrx.aptrxID = iTrxID
          aptrx.transType = "C"
          aptrx.transAmount = apinv.amount
          aptrx.transDate = now
          aptrx.apinvID = apinv.apinvID
          aptrx.refType = apinv.refType
          aptrx.refID = apinv.refID
          aptrx.refSeq = apinv.refSeq
          aptrx.refCategory = apinv.refCategory
          aptrx.uid = cAccountUser
          aptrx.username = cAccountName
          aptrx.notes = "Automatically completed due to user's limit"
          aptrx.report = true
          apinv.stat = "C"
          .
        validate aptrx.
        release aptrx.
      end.
  end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CompletePayableInvoice Include 
PROCEDURE CompletePayableInvoice :
/*------------------------------------------------------------------------------
@description Approves a payable invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pAmount as decimal no-undo.
  define input parameter pDate as datetime no-undo.
  define input parameter pNotes as character no-undo.
  define output parameter pSuccess as logical no-undo.
  define buffer apinv for apinv.
  
  assign
    pSuccess = true
    std-ch = ""
    .
  /* check for all have approved */
  run HasPayableInvoiceApprovedAll in this-procedure (pInvoiceID, output pSuccess).
  if not pSuccess
   then std-ch = addDelimiter(std-ch,"~n") + "Not all approvers have approved the invoice".
   
  /* check if vendor id is blank */
  if can-find(first apinv where apinvID = pInvoiceID and vendorID = "")
   then
    assign
      pSuccess = false
      std-ch = addDelimiter(std-ch,"~n") + "The Vendor ID may not be Blank."
      .
      
  /* check if invoice date is empty */
  if can-find(first apinv where apinvID = pInvoiceID and invoiceDate = ?)
   then
    assign
      pSuccess = false
      std-ch = addDelimiter(std-ch,"~n") + "The Invoice Date is Invalid."
      .
      
  /* check if invoice number is blank */
  if can-find(first apinv where apinvID = pInvoiceID and invoiceNumber = "")
   then
    assign
      pSuccess = false
      std-ch = addDelimiter(std-ch,"~n") + "The Invoice Number may not be Blank."
      .

  if pSuccess
   then
    for first apinv exclusive-lock
        where apinv.apinvID = pInvoiceID:
      
      std-in = 0.
      run server/completeapinvoice.p (input pInvoiceID,
                                      input pAmount,
                                      input pDate,
                                      input pNotes,
                                      true,
                                      input apinv.refType,
                                      output std-in,
                                      output pSuccess,
                                      output std-ch
                                      ).
      if std-in > 0
       then
        do:
          create aptrx.
          assign
            aptrx.aptrxID = std-in
            aptrx.transType = "A"
            aptrx.transAmount = pAmount
            aptrx.transDate = pDate
            aptrx.apinvID = pInvoiceID
            aptrx.refType = apinv.refType
            aptrx.refID = apinv.refID
            aptrx.refSeq = apinv.refSeq
            aptrx.refCategory = apinv.refCategory
            aptrx.uid = cAccountUser
            aptrx.username = cAccountName
            aptrx.notes = pNotes
            aptrx.report = true
            apinv.stat = "C"
            .
          validate aptrx.
          release aptrx.
          publish "ClaimAccountingDataChanged".
          publish "RefreshCoverages".
          pSuccess = true.
        end.
    end.
  if std-ch > ""
   then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CompleteReceivableInvoice Include 
PROCEDURE CompleteReceivableInvoice :
/*------------------------------------------------------------------------------
@description Completes a receivable invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pAmount as decimal no-undo.
  define input parameter pDate as datetime no-undo.
  define input parameter pNotes as character no-undo.
  define output parameter pSuccess as logical no-undo.
  
  define variable dRequestedAmount as decimal no-undo.
  define variable dPaidAmount as decimal no-undo.
  define buffer arinv for arinv.
  
  assign
    pSuccess = true
    std-ch = ""
    .
    
  run GetReceivableInvoiceAmount (pInvoiceID, output dRequestedAmount).
  run GetReceivableInvoiceAmountPaid (pInvoiceID, output dPaidAmount).
  if pAmount > dRequestedAmount - dPaidAmount
   then 
    do:
      std-ch = "The Paid Amount cannot be greater than the Requested Amount".
      pSuccess = false.
    end.
    
  if pSuccess
   then
    for first arinv exclusive-lock
        where arinv.arinvID = pInvoiceID:
      
      std-in = 0.
      run server/completearinvoice.p (input pInvoiceID,
                                      input pAmount,
                                      input pDate,
                                      input pNotes,
                                      output std-in,
                                      output pSuccess,
                                      output std-ch
                                      ).
      if pSuccess
       then
        do:
          create artrx.
          assign
            artrx.artrxID = std-in
            artrx.transType = "C"
            artrx.transAmount = pAmount
            artrx.transDate = pDate
            artrx.arinvID = pInvoiceID
            artrx.refType = arinv.refType
            artrx.refID = arinv.refID
            artrx.refSeq = arinv.refSeq
            artrx.refCategory = arinv.refCategory
            artrx.uid = cAccountUser
            artrx.username = cAccountName
            artrx.notes = pNotes
            .
          validate artrx.
          release artrx.
          publish "ClaimAccountingDataChanged".
        end.
    end.
  if not pSuccess and std-ch > ""
   then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DebugPayableApproval Include 
PROCEDURE DebugPayableApproval :
/*------------------------------------------------------------------------------
@description Outputs a message of the approvals
------------------------------------------------------------------------------*/

  for each apinva no-lock:
    message "invoice=" + string(apinva.apinvID) skip
            "seq=" + string(apinva.seq) skip
            "approver user=" + apinva.uid skip
            "approver role=" + apinva.role skip
            "current user=" + cAccountUser
            view-as alert-box information buttons ok.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeletePayableInvoice Include 
PROCEDURE DeletePayableInvoice :
/*------------------------------------------------------------------------------
@description Deletes the payable invoice from the database
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pSuccess as logical.
  
  for first apinv exclusive-lock
      where apinv.apinvID = pInvoiceID:
      
    run server/deleteapinvoice.p (input apinv.apinvID,
                                  output pSuccess,
                                  output std-ch
                                  ).
                                  
    if not pSuccess
     then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
     else publish "ClaimAccountingDataChanged".
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteReceivableInvoice Include 
PROCEDURE DeleteReceivableInvoice :
/*------------------------------------------------------------------------------
@description Deletes the receivable invoice from the database
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pSuccess as logical.
  
  for first arinv exclusive-lock
      where arinv.arinvID = pInvoiceID:
      
    run server/deletearinvoice.p (input arinv.arinvID,
                                  output pSuccess,
                                  output std-ch).
    
    if not pSuccess
     then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
     else publish "ClaimAccountingDataChanged".
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DetachPayableDocument Include 
PROCEDURE DetachPayableDocument :
/*------------------------------------------------------------------------------
@description Removes a payable invoice document from Sharefile
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pSuccess as logical no-undo.
  
  run server/deleteapinvoicedocument.p (input  pInvoiceID,
                                        output pSuccess,
                                        output std-ch).
                                          
  if not pSuccess
   then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
   else publish "ClaimAccountingDataChanged".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DetachReceivableDocument Include 
PROCEDURE DetachReceivableDocument :
/*------------------------------------------------------------------------------
@description Removes a receivable invoice document from Sharefile
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pSuccess as logical no-undo.
  
  run server/deletearinvoicedocument.p (input  pInvoiceID,
                                        output pSuccess,
                                        output std-ch).
                                          
  if not pSuccess
   then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
   else publish "ClaimAccountingDataChanged".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAccountReference Include 
PROCEDURE GetAccountReference :
/*------------------------------------------------------------------------------
@description Get the account name for an account number
------------------------------------------------------------------------------*/
  define output parameter pRefType as character no-undo.
  define output parameter pRefID as character no-undo.
  define output parameter pRefSeq as integer no-undo.
  
  pRefType = "{&refType}".
  pRefID = string({&refID}).
  pRefSeq = integer({&refSeq}) no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPayableDepartmentName Include 
PROCEDURE GetPayableDepartmentName :
/*------------------------------------------------------------------------------
@description Get the department name for a department
------------------------------------------------------------------------------*/
  define input parameter pDept as character no-undo.
  define output parameter pDeptName as character no-undo.
  
  run server/getapdepartmentname.p (input pDept,
                                    output pDeptName,
                                    output std-lo,
                                    output std-ch
                                    ).
                              
  if not std-lo
   then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPayableInvoice Include 
PROCEDURE GetPayableInvoice :
/*------------------------------------------------------------------------------
@description Gets the amount on an invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter table for userapinv.
  define output parameter table for tempapinva.
  define output parameter table for tempapinvd.
  
  /* buffers */
  define buffer apinv for apinv.
  define buffer apinva for apinva.
  define buffer apinvd for apinvd.
  
  /* empty temp tables */
  empty temp-table userapinv.
  empty temp-table tempapinva.
  empty temp-table tempapinvd.
  
  if not can-find(first apinv where apinvID = pInvoiceID)
   then run server/getapinvoices.p (input pInvoiceID,
                                    output table apinv,
                                    output table apinva,
                                    output table apinvd,
                                    output table aptrx,
                                    output std-lo,
                                    output std-ch).
  
  std-in = 0.
  for first apinv no-lock
      where apinv.apinvID = pInvoiceID:
      
    create userapinv.
    buffer-copy apinv to userapinv.
    
    for each apinva no-lock
       where apinva.apinvID = apinv.apinvID:
       
      create tempapinva.
      buffer-copy apinva to tempapinva.
    end.
    
    for each apinvd no-lock
       where apinvd.apinvID = apinv.apinvID:
       
      create tempapinvd.
      buffer-copy apinvd to tempapinvd.
    end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPayableInvoiceAmount Include 
PROCEDURE GetPayableInvoiceAmount :
/*------------------------------------------------------------------------------
@description Gets the amount on an invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pAmount as decimal no-undo.
  
  define buffer apinv for apinv.
  
  for first apinv no-lock
      where apinv.apinvID = pInvoiceID:
    
    assign
      pAmount = apinv.amount
      .
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPayableInvoiceApprover Include 
PROCEDURE GetPayableInvoiceApprover :
/*------------------------------------------------------------------------------
@description Get the 
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pUser as character no-undo.
  
  define buffer apinva for apinva.
  
  for each apinva no-lock
     where apinva.apinvID = pInvoiceID
       and apinva.stat = "P":
    
    pUser = addDelimiter(pUser,",") + apinva.uid.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPayableInvoiceApprovedAmount Include 
PROCEDURE GetPayableInvoiceApprovedAmount :
/*------------------------------------------------------------------------------
@description Get the lowest approved amount for the invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pAmount as decimal no-undo.
  
  define buffer apinva for apinva.
  
  assign
    pAmount = lf-max
    .
  for each apinva no-lock
     where apinva.apinvID = pInvoiceID
       and apinva.stat = "A":
    
    if apinva.amount < pAmount
     then pAmount = apinva.amount.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPayableInvoiceApprovedDate Include 
PROCEDURE GetPayableInvoiceApprovedDate :
/*------------------------------------------------------------------------------
@description Get the lowest approved date for the invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pDate as datetime no-undo.
  
  define buffer apinva for apinva.
  
  assign
    pDate = ld-max
    .
  for each apinva no-lock
     where apinva.apinvID = pInvoiceID
       and apinva.stat = "A":
    
    if apinva.dateActed < pDate
     then pDate = apinva.dateActed.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPayableInvoiceCategory Include 
PROCEDURE GetPayableInvoiceCategory :
/*------------------------------------------------------------------------------
@description Gets the category for the invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pCategory as character no-undo.
  
  define buffer apinv for apinv.
  
  for first apinv no-lock
      where apinv.apinvID = pInvoiceID:
    
    assign
      pCategory = apinv.refCategory
      .
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPayableInvoiceType Include 
PROCEDURE GetPayableInvoiceType :
/*------------------------------------------------------------------------------
@description Gets the category for the invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pType as character no-undo.
  
  define buffer apinv for apinv.
  
  for first apinv no-lock
      where apinv.apinvID = pInvoiceID:
    
    assign
      pType = apinv.refType
      .
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPayableVoidDetails Include 
PROCEDURE GetPayableVoidDetails :
/*------------------------------------------------------------------------------
@description Gets the amount on an invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter table for tempaptrx.
  
  /* buffers */
  define buffer aptrx for aptrx.
  
  /* empty temp tables */
  empty temp-table tempaptrx.
  
  if not lPayableLoaded
   then run LoadPayableInvoices in this-procedure.
  
  std-in = 0.
  for first aptrx no-lock
      where aptrx.apinvID = pInvoiceID
        and aptrx.transType = "V":
      
    create tempaptrx.
    buffer-copy aptrx to tempaptrx.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetReceivableInvoice Include 
PROCEDURE GetReceivableInvoice :
/*------------------------------------------------------------------------------
@description Gets the receivable invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter table for userarinv.
  
  /* buffers */
  define buffer arinv for arinv.
  define buffer artrx for artrx.
  
  /* empty temp tables */
  empty temp-table userarinv.
  
  if not can-find(first arinv where arinvID = pInvoiceID)
   then run server/getarinvoices.p (input pInvoiceID,
                                    output table arinv,
                                    output table artrx,
                                    output lReceivableLoaded,
                                    output std-ch).
  
  for first arinv no-lock
      where arinv.arinvID = pInvoiceID:
      
    create userarinv.
    buffer-copy arinv to userarinv.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetReceivableInvoiceAmount Include 
PROCEDURE GetReceivableInvoiceAmount :
/*------------------------------------------------------------------------------
@description Gets the receivable invoice amount
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pAmount as decimal no-undo.
  
  /* buffers */
  define buffer userarinv for userarinv.
  
  /* empty temp tables */
  empty temp-table userarinv.
  
  if not can-find(first arinv where arinvID = pInvoiceID)
   then run GetReceivableInvoice in this-procedure(pInvoiceID, output table userarinv).
  
  pAmount = 0.
  for first arinv no-lock
      where arinv.arinvID = pInvoiceID:
      
    pAmount = arinv.requestedAmount.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetReceivableInvoiceAmountPaid Include 
PROCEDURE GetReceivableInvoiceAmountPaid :
/*------------------------------------------------------------------------------
@description Gets the receivable invoice amount paid so far
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pAmount as decimal no-undo.
  
  /* buffers */
  define buffer arinv for arinv.
  
  /* empty temp tables */
  empty temp-table userarinv.
  
  if not can-find(first arinv where arinvID = pInvoiceID)
   then run GetReceivableInvoice in this-procedure(pInvoiceID, output table userarinv).
  
  pAmount = 0.
  for each artrx no-lock
     where artrx.arinvID = pInvoiceID:
      
    pAmount = pAmount + artrx.transAmount.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetReceivableTransactions Include 
PROCEDURE GetReceivableTransactions :
/*------------------------------------------------------------------------------
@description Gets the receivable invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter table for tempartrx.
  
  /* buffers */
  define buffer artrx for artrx.
  
  /* empty temp tables */
  empty temp-table userarinv.
  
  if not can-find(first artrx where arinvID = pInvoiceID)
   then run server/getarinvoices.p (input pInvoiceID,
                                    output table arinv,
                                    output table artrx,
                                    output lReceivableLoaded,
                                    output std-ch).
  
  for first artrx no-lock
      where artrx.artrxID = pInvoiceID:
      
    create tempartrx.
    buffer-copy artrx to tempartrx.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE HasPayableInvoiceApprovedAll Include 
PROCEDURE HasPayableInvoiceApprovedAll :
/*------------------------------------------------------------------------------
@description Determines if the invoice has at least one approver on it
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pHasApprovedAll as logical no-undo.
  
  pHasApprovedAll = HasAPInvoiceApprovedAll(pInvoiceID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsPayableInvoiceUsed Include 
PROCEDURE IsPayableInvoiceUsed :
/*------------------------------------------------------------------------------
@description Determines vendor/invoice combination has already been used
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pVendorID as character no-undo.
  define input parameter pInvoiceNumber as character no-undo.
  define output parameter pIsUsed as logical no-undo initial false.
  
  run server/getapusedinvoices.p (INPUT pInvoiceID,
                                  INPUT pVendorID,
                                  INPUT pInvoiceNumber,
                                  OUTPUT pIsUsed,
                                  OUTPUT std-lo,
                                  OUTPUT std-ch
                                  ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsReceivableComplete Include 
PROCEDURE IsReceivableComplete :
/*------------------------------------------------------------------------------
@description Determines if there has been at least one posted receiveable for
             the invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pIsComplete as logical no-undo initial false.
  
  /* buffers */
  define buffer artrx for artrx.
  
  std-ha = ?.
  create temp-table std-ha.
  
  if not can-find(first arinv where arinvID = pInvoiceID)
   then run GetReceivableTransactions in this-procedure(pInvoiceID, output table tempartrx).
  
  std-de = 0.
  for each artrx no-lock:
    std-de = std-de + artrx.transAmount.
  end.
  pIsComplete = (std-de > 0).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadAccountSysProps Include 
PROCEDURE LoadAccountSysProps :
/*------------------------------------------------------------------------------
@description Get the account payable system properties
------------------------------------------------------------------------------*/
  
  run server/getsysprops.p (input "AP",
                            input "",
                            input "",
                            input "",
                            input "",
                            output table apsysprops,
                            output std-lo,
                            output std-ch
                            ).
  
  if not std-lo and std-ch > ""
   then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadPayableInvoices Include 
PROCEDURE LoadPayableInvoices :
/*------------------------------------------------------------------------------
@description Load the payable invoices by a particular reference type
------------------------------------------------------------------------------*/
  run server/getapinvoicesbyreference.p (input "{&refType}",
                                         input {&refID},
                                         input {&refSeq},
                                         output table apinv,
                                         output table apinva,
                                         output table apinvd,
                                         output table aptrx,
                                         output lPayableLoaded,
                                         output std-ch
                                         ).

  if not lPayableLoaded
   then
    do:
      if std-ch > ""
       then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
      empty temp-table apinv.
      empty temp-table apinva.
      empty temp-table apinvd.
      empty temp-table aptrx.
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadReceivableInvoices Include 
PROCEDURE LoadReceivableInvoices :
/*------------------------------------------------------------------------------
@description Load the receivable invoices by a particular reference type
------------------------------------------------------------------------------*/
  run server/getarinvoicesbyreference.p (input "{&refType}",
                                         input {&refID},
                                         input {&refSeq},
                                         output table arinv,
                                         output table artrx,
                                         output lReceivableLoaded,
                                         output std-ch
                                         ).

  if not lReceivableLoaded
   then
    do:
      if std-ch > ""
       then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
      empty temp-table arinv.
      empty temp-table artrx.
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyPayableInvoice Include 
PROCEDURE ModifyPayableInvoice :
/*------------------------------------------------------------------------------
@description Modify the payable invoice
------------------------------------------------------------------------------*/
  define input parameter table for userapinv.
  define input parameter table for tempapinva.
  define input parameter table for tempapinvd.
  define output parameter pSuccess as logical.
  
  define buffer apinv for apinv.
  define buffer userapinv for userapinv.
  
  define variable cCompare as character no-undo initial "".
  
  /* find the correct invoice */
  pSuccess = true.

  {lib/pbshow.i "''"}

  TRX-BLOCK:
  for first userapinv no-lock:
    for first apinv exclusive-lock
        where apinv.apinvID = userapinv.apinvID TRANSACTION
        on error undo TRX-BLOCK, leave TRX-BLOCK:

      buffer-compare apinv except apinv.hasDocument apinv.documentFilename apinv.isFileCreated apinv.isFileEdited apinv.isFileDeleted to userapinv save result in cCompare.
      if cCompare > ""
       then
        do:
          {lib/pbupdate.i "'Modifying Payable.'" 20}
          run server/modifyapinvoice.p (input table userapinv,
                                        output pSuccess,
                                        output std-ch
                                        ).
                                              
          if not pSuccess and std-ch > ""
           then message std-ch view-as alert-box error buttons ok.
        end. /* if cCompare > "" */
        
      /***** DISTRIBUTIONS *****/
      if pSuccess
       then 
        do:
          {lib/pbupdate.i "'Modifying Distributions'" 40}
          run ModifyPayableInvoiceDistribution in this-procedure (input table tempapinvd, input userapinv.apinvID, output pSuccess).
        end.
       else leave TRX-BLOCK.

      /***** APPROVALS *****/
      if pSuccess
       then
        do:
          {lib/pbupdate.i "'Modifying Approvals'" 60}
          run ModifyPayableInvoiceApproval in this-procedure (input table tempapinva, input userapinv.apinvID, output pSuccess).
        end.
       else leave TRX-BLOCK.
       
      /***** CLAIM PAYABLES *****/
      if pSuccess and userapinv.amount > 0 and upper(userapinv.refType) = "C"
       then run ClaimPayable in this-procedure (table userapinv, userapinv.apinvID, output pSuccess).
        
      /***** DOCUMENTS *****/
      /* there was no document before and is now */
      if pSuccess and userapinv.isFileCreated
       then
        do:
          {lib/pbupdate.i "'Adding Document'" 80}
          run AttachPayableDocument in this-procedure (userapinv.apinvID, userapinv.documentFilename, output pSuccess).
          if not pSuccess
           then message "Document not uploaded." view-as alert-box warning buttons ok.
          pSuccess = true.
        end.
      /* there was a document and still is */
      if pSuccess and userapinv.isFileEdited
       then 
        do:
          define variable docType as character no-undo.
          if search(userapinv.documentFilename) = ?
           then
            do:
              {lib/pbupdate.i "'Moving/Renaming Document'" 80}
              docType = "moved".
            end.
           else
            do:
              {lib/pbupdate.i "'Replacing Document'" 80}
              docType = "replaced".
            end.
          run ReplacePayableDocument in this-procedure (table userapinv, output pSuccess).
          if not pSuccess
           then message "Document not " + docType + "." view-as alert-box warning buttons ok.
          pSuccess = true.
        end.
      /* there was a document and not now */
      if pSuccess and userapinv.isFileDeleted
       then
        do:
          {lib/pbupdate.i "'Removing Document'" 80}
          run DetachPayableDocument in this-procedure (apinv.apinvID, output pSuccess).
          if not pSuccess
           then message "Document not removed." view-as alert-box warning buttons ok.
          pSuccess = true.
        end.
    end. /* for first apinv */
  end. /* for first userapinv */
    
  if pSuccess
   then 
    do:
      publish "ClaimAccountingDataChanged".
      {lib/pbupdate.i "'Modify Complete'" 100}
    end.
   else {lib/pbupdate.i "'Modify Failed'" 100}
  {lib/pbhide.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyPayableInvoiceApproval Include 
PROCEDURE ModifyPayableInvoiceApproval PRIVATE :
/*------------------------------------------------------------------------------
@description Modify the payable invoice approvals
------------------------------------------------------------------------------*/
  define input parameter table for tempapinva.
  define input parameter pInvoiceID as integer.
  define output parameter pSuccess as logical.
  
  define variable cCompare as character no-undo init "".
  assign
    pSuccess = true
    std-ch = ""
    .
  /***** DELETED APPROVALS *****/
  for each apinva exclusive-lock
     where apinva.apinvID = pInvoiceID:
     
    /* if the row is in apinva and not in tempapinva, then the user wishes to delete the row */
    if not can-find(first tempapinva no-lock
                    where tempapinva.apinvID = apinva.apinvID
                      and tempapinva.seq = apinva.seq)
     then 
      do:
        run server/deleteapinvoiceapproval.p (input apinva.apinvID,
                                              input apinva.seq,
                                              output pSuccess,
                                              output std-ch
                                              ).
      end.
  end.
  /***** NEW AND MODIFIED APPROVALS *****/
  /* check for new and modified approvals */
  if pSuccess
   then
    for each tempapinva exclusive-lock:
    
      /* if the row is in tempapinva and not in apinva, then the row is new */
      if not can-find(first apinva no-lock
                      where apinva.apinvID = tempapinva.apinvID
                        and apinva.seq = tempapinva.seq)
       then
        do:
          run server/newapinvoiceapproval.p (input tempapinva.apinvID,
                                             input tempapinva.role,
                                             input tempapinva.uid,
                                             output pSuccess,
                                             output std-ch
                                             ).
        end. /* new */
       /* if the row is in both tables, then it's a modification */
       else /* modify */
        for each apinva no-lock
           where apinva.apinvID = tempapinva.apinvID
             and apinva.seq = tempapinva.seq:
             
          buffer-compare apinva to tempapinva save result in cCompare.
          if cCompare > ""
           then 
            do:
              if index(cCompare, "stat") > 0
               then run server/resetapinvoiceapproval.p (input tempapinva.apinvID,
                                                         output pSuccess,
                                                         output std-ch
                                                         ).
               else run server/modifyapinvoiceapproval.p (input tempapinva.apinvID,
                                                          input tempapinva.seq,
                                                          input tempapinva.role,
                                                          input tempapinva.uid,
                                                          input tempapinva.amount,
                                                          input tempapinva.notes,
                                                          output pSuccess,
                                                          output std-ch
                                                          ).
            end. /* differences */
        end. /* modify */
    end. /* if delete succeeded */
    
  if not pSuccess
   then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyPayableInvoiceDistribution Include 
PROCEDURE ModifyPayableInvoiceDistribution PRIVATE :
/*------------------------------------------------------------------------------
@description Modify the payable invoice distributions
------------------------------------------------------------------------------*/
  define input parameter table for tempapinvd.
  define input parameter pInvoiceID as integer.
  define output parameter pSuccess as logical.
  
  define variable cCompare as character no-undo init "".
  assign
    pSuccess = true
    std-ch = ""
    .
  
  /***** DELETED PayablePROVALS *****/
  for each apinvd exclusive-lock
     where apinvd.apinvID = pInvoiceID:
     
    /* if the row is in apinvd and not in tempapinvd, then the user wishes to delete the row */
    if not can-find(first tempapinvd no-lock
                    where tempapinvd.apinvID = apinvd.apinvID
                      and tempapinvd.seq = apinvd.seq)
     then 
      do:
        run server/deleteapinvoicedistribution.p (input apinvd.apinvID,
                                                  input apinvd.seq,
                                                  output pSuccess,
                                                  output std-ch
                                                  ).
      end.
  end.
  /***** NEW AND MODIFIED PayablePROVALS *****/
  /* check for new and modified approvals */
  if pSuccess
   then
    for each tempapinvd exclusive-lock:
    
      /* if the row is in tempapinvd and not in apinvd, then the row is new */
      if not can-find(first apinvd no-lock
                      where apinvd.apinvID = tempapinvd.apinvID
                        and apinvd.seq = tempapinvd.seq)
       then
        do:
          run server/newapinvoicedistribution.p (input tempapinvd.apinvID,
                                                 input tempapinvd.acct,
                                                 input tempapinvd.amount,
                                                 output pSuccess,
                                                 output std-ch
                                                 ).
        end. /* new */
       /* if the row is in both tables, then it's a modification */
       else /* modify */
        for each apinvd no-lock
           where apinvd.apinvID = tempapinvd.apinvID
             and apinvd.seq = tempapinvd.seq:
             
          buffer-compare apinvd to tempapinvd save result in cCompare.
          if cCompare > ""
           then 
            do:
              run server/modifyapinvoicedistribution.p (input tempapinvd.apinvID,
                                                        input tempapinvd.seq,
                                                        input tempapinvd.acct,
                                                        input tempapinvd.amount,
                                                        output pSuccess,
                                                        output std-ch
                                                        ).
            end. /* differences */
        end. /* modify */
    end. /* if delete succeeded */
    
  if not pSuccess
   then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyReceivableInvoice Include 
PROCEDURE ModifyReceivableInvoice :
/*------------------------------------------------------------------------------
@description Modify the receivable invoice
------------------------------------------------------------------------------*/
  define input parameter table for userarinv.
  define output parameter pSuccess as logical.
  
  define buffer arinv for arinv.
  define buffer userarinv for userarinv.
  
  define variable cCompare as character no-undo initial "".
  define variable lSharefile as logical no-undo initial false.
  
  pSuccess = true.
  {lib/pbshow.i "''"}
  {lib/pbupdate.i "'Modifying Receivable'" 33}
  
  /* find the correct invoice */
  pSuccess = true.
  for first userarinv no-lock:
  
    for first arinv exclusive-lock
        where arinv.arinvID = userarinv.arinvID:
      
      buffer-compare arinv except arinv.hasDocument arinv.documentFilename arinv.isFileCreated arinv.isFileEdited arinv.isFileDeleted to userarinv save result in cCompare.
      if cCompare > ""
       then
        do:
          run server/modifyarinvoice.p (input table userarinv,
                                        output pSuccess,
                                        output std-ch
                                        ).
          
          if not pSuccess and std-ch > ""
           then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
        end. /* if cCompare > "" */
        
      /***** DOCUMENTS *****/
      /* there was no document before and is now */
      if pSuccess and userarinv.isFileCreated
       then
        do:
          {lib/pbupdate.i "'Adding Document'" 66}
          run AttachReceivableDocument in this-procedure (userarinv.arinvID, userarinv.documentFilename, output pSuccess).
          if not pSuccess
           then message "Document not uploaded." view-as alert-box warning buttons ok.
          pSuccess = true.
        end.
      /* there was a document and still is */
      if pSuccess and userarinv.isFileEdited
       then 
        do:
          define variable docType as character no-undo.
          if search(userarinv.documentFilename) = ?
           then
            do:
              {lib/pbupdate.i "'Moving/Renaming Document'" 66}
              docType = "moved".
            end.
           else
            do:
              {lib/pbupdate.i "'Replacing Document'" 66}
              docType = "replaced".
            end.
          run ReplaceReceivableDocument in this-procedure (table userarinv, output pSuccess).
          if not pSuccess
           then message "Document not " + docType + "." view-as alert-box warning buttons ok.
          pSuccess = true.
        end.
      /* there was a document and not now */
      if pSuccess and userarinv.isFileDeleted
       then
        do:
          {lib/pbupdate.i "'Removing Document'" 66}
          run DetachReceivableDocument in this-procedure (arinv.arinvID, output pSuccess).
          if not pSuccess
           then message "Document not removed." view-as alert-box warning buttons ok.
          pSuccess = true.
        end. /***** DOCUMENTS *****/
    end. /* for each arinv */
  end. /* for first userarinv */
  
  if pSuccess
   then 
    do:
      publish "ClaimAccountingDataChanged".
      {lib/pbupdate.i "'Modify Complete'" 100}
    end.
   else {lib/pbupdate.i "'Modify Failed'" 100}
  {lib/pbhide.i}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewPayableInvoice Include 
PROCEDURE NewPayableInvoice :
/*------------------------------------------------------------------------------
@description Create a new invoice
------------------------------------------------------------------------------*/
  define input parameter table for userapinv.
  define input parameter table for tempapinva.
  define input parameter table for tempapinvd.
  define output parameter pInvoiceID as integer.
  define output parameter pSuccess as logical.
  
  define variable hProgressBar as handle no-undo.
  define variable dAmount as decimal no-undo.
  define variable cRefType as character no-undo.
  
  define buffer userapinv for userapinv.
  define buffer apsysprops for apsysprops.
  
  /* create the new invoice */
  pSuccess = false.
  {lib/pbshow.i "''"}

  TRX-BLOCK:
  for first userapinv exclusive-lock TRANSACTION
      on error undo TRX-BLOCK, leave TRX-BLOCK:
      
    run server/newapinvoice.p (input table userapinv,
                               output pInvoiceID,
                               output pSuccess,
                               output std-ch
                               ).
    if pSuccess
     then
      do: 
        assign
          dAmount = userapinv.amount
          cRefType = userapinv.refType
          userapinv.apinvID = pInvoiceID
          userapinv.stat = "O"
          .
        create apinv.
        buffer-copy userapinv except userapinv.hasDocument userapinv.documentFilename to apinv.
      end.
     else 
      if std-ch > ""
       then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
      
    /***** DISTRIBUTIONS *****/
    if pSuccess
     then
      do:
        {lib/pbupdate.i "'Creating Distributions'" 50}
        run NewPayableInvoiceDistribution in this-procedure (pInvoiceID, table tempapinvd, output pSuccess).
      end.
     else leave TRX-BLOCK.
     
    /***** APPROVALS *****/
    if pSuccess
     then
      do:
        {lib/pbupdate.i "'Creating Approvals'" 75}
        run NewPayableInvoiceApproval in this-procedure (pInvoiceID, table tempapinva, output pSuccess).
      end.
     else leave TRX-BLOCK.
     
    /***** SEND EMAIL *****/
    if pSuccess
     then
      do:
        std-lo = false.
        for each apinva no-lock
           where apinva.apinvID = pInvoiceID
             and apinva.stat = "P":
              
          if not can-find(first apsysprops)
           then run LoadAccountSysProps in this-procedure.
           
          /* automatically send an email to the approver */
          for first apsysprops no-lock
              where apsysprops.appCode = "AP"
                and apsysprops.objAction = "SendEmail"
                and apsysprops.objProperty = "PayableInvoice"
                and apsysprops.objID = apinva.uid
                and apsysprops.objValue = "TRUE":
            
            std-lo = true.
          end.
        end.
        if std-lo
         then run SendPayableInvoice in this-procedure (pInvoiceID, output std-lo).
      end.
      
    /***** CLAIM PAYABLES *****/
    if pSuccess and dAmount > 0 and cRefType = "C"
     then run ClaimPayable in this-procedure (table userapinv, pInvoiceID, output pSuccess).
     
    /***** DOCUMENTS *****/
    if pSuccess and userapinv.isFileCreated
     then 
      do:
        {lib/pbupdate.i "'Uploading Document'" 75}
        run AttachPayableDocument in this-procedure (pInvoiceID, userapinv.documentFilename, output pSuccess).
        if not pSuccess
         then message "Document not uploaded." view-as alert-box warning buttons ok.
        pSuccess = true.
      end.
  end. /* for first apinv */
     
  if pSuccess
   then
    do:
      publish "ClaimAccountingDataChanged".
      {lib/pbupdate.i "'Payable Created'" 100}
    end.
   else {lib/pbupdate.i "'Unable to Create Payable'" 100}
  {lib/pbhide.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewPayableInvoiceApproval Include 
PROCEDURE NewPayableInvoiceApproval PRIVATE :
/*------------------------------------------------------------------------------
@description Create a new invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer.
  define input parameter table for tempapinva.
  define output parameter pSuccess as logical.
  
  define buffer tempapinva for tempapinva.
  
  /* create the new invoice approval(s) */
  /* success is true even if no approvals because we can add them later */
  pSuccess = true.
  for each tempapinva exclusive-lock:
    run server/newapinvoiceapproval.p (input pInvoiceID,
                                       input tempapinva.role,
                                       input tempapinva.uid,
                                       output pSuccess,
                                       output std-ch
                                       ).
    if pSuccess
     then
      do:
        tempapinva.apinvID = pInvoiceID.
        create apinva.
        buffer-copy tempapinva to apinva.
      end.
     else MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewPayableInvoiceDistribution Include 
PROCEDURE NewPayableInvoiceDistribution PRIVATE :
/*------------------------------------------------------------------------------
@description Create a new invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer.
  define input parameter table for tempapinvd.
  define output parameter pSuccess as logical.
  
  define buffer tempapinvd for tempapinvd.
  
  /* create the new invoice distribution(s) */
  pSuccess = true.
  for each tempapinvd exclusive-lock:
    run server/newapinvoicedistribution.p (input pInvoiceID,
                                           input tempapinvd.acct,
                                           input tempapinvd.amount,
                                           output pSuccess,
                                           output std-ch
                                           ).
    if pSuccess
     then
      do:
        assign
          tempapinvd.apinvID = pInvoiceID
          .
        create apinvd.
        buffer-copy tempapinvd to apinvd.
      end.
     else MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewReceivableInvoice Include 
PROCEDURE NewReceivableInvoice :
/*------------------------------------------------------------------------------
@description Create a new invoice
------------------------------------------------------------------------------*/
  define input parameter table for userarinv.
  define output parameter pSuccess as logical.
  
  define buffer userarinv for userarinv.
  
  /* create the new invoice */
  pSuccess = false.
  {lib/pbshow.i "''"}
  {lib/pbupdate.i "'Creating Receivable'" 0}
  
  for first userarinv exclusive-lock:
    run server/newarinvoice.p (input table userarinv,
                               output std-in,
                               output pSuccess,
                               output std-ch
                               ).
    if pSuccess
     then
      do:
        assign
          userarinv.arinvID = std-in
          userarinv.stat = "O"
          userarinv.uid = cAccountUser
          .
        create arinv.
        buffer-copy userarinv to arinv.
     
        /***** DOCUMENTS *****/
        {lib/pbupdate.i "'Uploading Document'" 50}
        if userarinv.hasDocument
         then run AttachReceivableDocument in this-procedure (std-in, userarinv.documentFilename, output pSuccess).
      end.
     else 
      if std-ch > ""
       then message std-ch view-as alert-box error buttons ok.
  end. /* for first userarinv */
  
  if pSuccess
   then
    do:
      publish "ClaimAccountingDataChanged".
      {lib/pbupdate.i "'Receivable Created'" 100}
    end.
   else {lib/pbupdate.i "'Unable to Create Receivable'" 100}
  {lib/pbhide.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RejectPayableInvoice Include 
PROCEDURE RejectPayableInvoice :
/*------------------------------------------------------------------------------
@description Rejects a payable invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pNotes as character no-undo.
  define input parameter pUser as character no-undo.
  define output parameter pSuccess as logical no-undo.
  
  define buffer apinva for apinva.
  
  assign
    std-in = 0
    std-de = 0
    pSuccess = false
    .
  for first apinv exclusive-lock
      where apinv.apinvID = pInvoiceID:
      
    for first apinva exclusive-lock
        where apinva.apinvID = apinv.apinvID
          and apinva.uid = pUser
          and apinva.stat = "P":
          
      run server/rejectapinvoice.p (input pInvoiceID,
                                    input apinva.seq,
                                    input pNotes,
                                    output pSuccess,
                                    output std-ch
                                    ).
      if pSuccess
       then
        do:
          assign
            apinva.stat = "D"
            apinva.amount = 0.0
            apinva.notes = pNotes
            apinva.dateActed = now
            apinva.uid = cAccountUser
            apinv.stat = "D"
            .
          publish "ClaimAccountingDataChanged".      
        end.
    end.
  end.
      
  if not pSuccess and std-ch > ""
   then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RejectReceivableInvoice Include 
PROCEDURE RejectReceivableInvoice :
/*------------------------------------------------------------------------------
@description Rejects a receivable invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pSuccess as logical no-undo.
  
  define buffer arinv for arinv.
  
  assign
    pSuccess = true
    .
  
  if pSuccess
   then
    for first arinv exclusive-lock
        where arinv.arinvID = pInvoiceID:
                  
      run server/rejectarinvoice.p (input pInvoiceID,
                                    output pSuccess,
                                    output std-ch
                                    ).
      if pSuccess
       THEN
        DO:
          arinv.stat = "A".
          publish "ClaimAccountingDataChanged".    
        END.
  end.
      
  if not pSuccess and std-ch > ""
   then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ReplacePayableDocument Include 
PROCEDURE ReplacePayableDocument :
/*------------------------------------------------------------------------------
@description Attaches a document to a payable invoice
------------------------------------------------------------------------------*/
  define input parameter table for userapinv.
  define output parameter pSuccess as logical no-undo.
  
  define variable cOldServerPath as character no-undo.
  define variable cNewServerPath as character no-undo.
  define variable cOldFileName   as character no-undo.
  define variable cNewFileName   as character no-undo.
  define variable cFileName      as character no-undo.
  define variable cExtension     as character no-undo.
  
  pSuccess = false.
  for first userapinv no-lock:
    for first apinv exclusive-lock
        where apinv.apinvID = userapinv.apinvID:
    
        
      /* if there is a new file, then the documentFilename field of modified will be different from currentPayable */
      /* get the extension of either the new file or the old file */
      if userapinv.documentFilename <> apinv.documentFilename
       then cExtension = entry(num-entries(userapinv.documentFilename,"."),userapinv.documentFilename,".").
       else cExtension = entry(num-entries(apinv.documentFilename,"."),apinv.documentFilename,".").
       
      /* the old server path is created from the existing vendor and date */
      assign
        cOldFileName   = apinv.documentFilename
        cOldServerPath = getPayableServerPath(apinv.vendorID,apinv.invoiceDate)
        cNewFileName   = getPayableFilename(userapinv.apinvID,userapinv.invoiceNumber,userapinv.refType,userapinv.refID)
        cNewServerPath = getPayableServerPath(userapinv.vendorID, userapinv.invoiceDate)
        .
       
      /* the file was found */
      if apinv.hasDocument
       then 
        do:
          /* determine if the user selected a new file by comparing the new to old file */
          if search(userapinv.documentFilename) <> ?
           then
            do:
              /* since we have a new file from the user, we should delete the one in sharefile first */
              run DetachPayableDocument in this-procedure (apinv.apinvID, output pSuccess).
                                                    
              if pSuccess
               then run AttachPayableDocument in this-procedure (userapinv.apinvID, userapinv.documentFilename, output pSuccess).
            end.
           else
            do:
              cFileName = cNewFileName + "." + cExtension.
              /* the user didn't enter a file so we need to determine if the filename, server path, or both changed */
              /* if the files differ, then rename the file */
              if cFileName <> cOldFileName
               then run server/renameapinvoicedocument.p (cOldServerPath + cOldFileName, cFileName, output pSuccess, output std-ch).
                                   
              /* if the server paths differ, then move the file */
              if cNewServerPath <> cOldServerPath
               then 
                do:
                  run repository/newrepositoryfolder.p (input  cAccountUser,
                                                        input  cAccountPass,
                                                        input  cNewServerPath,
                                                        output pSuccess,
                                                        output std-ch).
                        
                  run server/moveapinvoicedocument.p (cOldServerPath + cFileName, cNewServerPath, output pSuccess, output std-ch).
                end.
            end.       
           
          /* update the sysdoc table by deleting the existing record and adding a new one */
          if pSuccess
           then
            do:
              run server/unlinkdocument.p (input "Invoice-AP", 
                                           input userapinv.apinvID, 
                                           input 0,
                                           output std-lo,
                                           output std-ch).
              run server/linkdocumentwithid.p (input "Invoice-AP", 
                                               input userapinv.apinvID, 
                                               input 0,
                                               input cNewFileName,
                                               input cExtension,
                                               output std-lo,
                                               output std-ch).
              pSuccess = true.
            end.
        end.
    end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ReplaceReceivableDocument Include 
PROCEDURE ReplaceReceivableDocument :
/*------------------------------------------------------------------------------
@description Attaches a document to a receivable invoice
------------------------------------------------------------------------------*/
  define input parameter table for userarinv.
  define output parameter pSuccess as logical no-undo.
  
  define variable cOldServerPath as character no-undo.
  define variable cNewServerPath as character no-undo.
  define variable cOldFileName   as character no-undo.
  define variable cNewFileName   as character no-undo.
  define variable cFileName      as character no-undo.
  define variable cExtension     as character no-undo.
  
  pSuccess = false.
  for first userarinv no-lock:
    for first arinv exclusive-lock
        where arinv.arinvID = userarinv.arinvID:
    
        
      /* if there is a new file, then the documentFilename field of modified will be different from currentPayable */
      /* get the extension of either the new file or the old file */
      if userarinv.documentFilename <> arinv.documentFilename
       then cExtension = entry(num-entries(userarinv.documentFilename,"."),userarinv.documentFilename,".").
       else cExtension = entry(num-entries(arinv.documentFilename,"."),arinv.documentFilename,".").
       
      /* the old server path is created from the existing vendor and date */
      assign
        cOldFileName   = arinv.documentFilename
        cOldServerPath = getReceivableServerPath(arinv.dateRequested)
        cNewFileName   = getReceivableFilename(userarinv.arinvID,userarinv.name,userarinv.refType,userarinv.refID)
        cNewServerPath = getReceivableServerPath(userarinv.dateRequested)
        .
       
      /* the file was found */
      if arinv.hasDocument
       then 
        do:
          /* determine if the user selected a new file by comparing the new to old file */
          if search(userarinv.documentFilename) <> ?
           then
            do:
              /* since we have a new file from the user, we should delete the one in sharefile first */
              run DetachReceivableDocument in this-procedure (arinv.arinvID, output pSuccess).
                                                    
              if pSuccess
               then run AttachReceivableDocument in this-procedure (userarinv.arinvID, userarinv.documentFilename, output pSuccess).
            end.
           else
            do:
              cFileName = cNewFileName + "." + cExtension.
              /* the user didn't enter a file so we need to determine if the filename, server path, or both changed */
              /* if the files differ, then rename the file */
              if cFileName <> cOldFileName
               then run server/renamearinvoicedocument.p (cOldServerPath + cOldFileName, cFileName, output pSuccess, output std-ch).
                                   
              /* if the server paths differ, then move the file */
              if cNewServerPath <> cOldServerPath
               then 
                do:
                  run repository/newrepositoryfolder.p (input  cAccountUser,
                                                        input  cAccountPass,
                                                        input  cNewServerPath,
                                                        output pSuccess,
                                                        output std-ch).
                        
                  run server/movearinvoicedocument.p (cOldServerPath + cFileName, cNewServerPath, output pSuccess, output std-ch).
                end.
            end.       
           
          /* update the sysdoc table by deleting the existing record and adding a new one */
          if pSuccess
           then
            do:
              run server/unlinkdocument.p (input "Invoice-AR", 
                                           input userarinv.arinvID, 
                                           input 0,
                                           output std-lo,
                                           output std-ch).
              run server/linkdocumentwithid.p (input "Invoice-Ar", 
                                               input userarinv.arinvID, 
                                               input 0,
                                               input cNewFileName,
                                               input cExtension,
                                               output std-lo,
                                               output std-ch).
              pSuccess = true.
            end.
        end.
    end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SendPayableInvoice Include 
PROCEDURE SendPayableInvoice :
/*------------------------------------------------------------------------------
@description Sends the invoice for approval
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pSuccess as logical.
  
  pSuccess = false.
  run server/sendapinvoice.p (input pInvoiceID,
                              output pSuccess,
                              output std-ch).
    
  if not pSuccess and std-ch > ""
   then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewPayableCheckRequest Include 
PROCEDURE ViewPayableCheckRequest :
/*------------------------------------------------------------------------------
@description Build the check request for a payable invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  
  define buffer apinv for apinv.
  define buffer apinva for apinva.
  
  for first apinv no-lock
      where apinv.apinvID = pInvoiceID:
    
    run GetClaimPDFDetails in this-procedure (integer(apinv.refID), apinv.refSeq, output table checkrequest) no-error.
    if error-status:error
     then publish "GetClaimPDFDetails" (integer(apinv.refID), apinv.refSeq, output table checkrequest).
    
    /* general claim and invoice info */
    for first checkrequest exclusive-lock:
      assign
        checkrequest.vendorID = apinv.vendorID
        checkrequest.vendorName = apinv.vendorName
        checkrequest.vendorAddress = apinv.vendorAddress
        checkrequest.invoiceNumber = apinv.invoiceNumber
        checkrequest.invoiceNotes = apinv.notes
        checkrequest.amount = apinv.amount
        .
      /* if the invoice is closed, get the trans amount */
      for last aptrx no-lock
         where aptrx.apinvID = apinv.apinvid
           and aptrx.transType = "C":
         
        checkrequest.amount = aptrx.transAmount.
      end.
      /* get the account data */
      for each apinvd no-lock
         where apinvd.apinvID = apinv.apinvID:
         
        assign
          checkrequest.account = apinvd.acct
          checkrequest.accountName = apinvd.acctname
          .
      end.
    end.
    /* get the approvals */
    empty temp-table tempapinva.
    for each apinva no-lock
       where apinva.apinvID = apinv.apinvID:
       
      create tempapinva.
      buffer-copy apinva to tempapinva.
    end.
    /* build and open the report */
    run createcheckrequest.p (input table checkrequest,
                              input table tempapinva,
                              (if apinv.stat = "C" or apinv.stat = "V" then true else false),
                              output std-ch
                              ).
    publish "AddTempFile" (std-ch, std-ch).
    run util/openfile.p (std-ch).
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewPayableDocument Include 
PROCEDURE ViewPayableDocument :
/*------------------------------------------------------------------------------
@description Views the Payable document from ShareFile
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  
  define variable cServerPath as character no-undo.
  define variable cFileName   as character no-undo.
  define variable cLocalPath  as character no-undo.
  define variable cLocalFile  as character no-undo.
  
  for first apinv no-lock
      where apinv.apinvID = pInvoiceID:

    /* build the directory path */
    publish "GetTempDir" (output cLocalPath).
    cServerPath = getPayableServerPath(apinv.vendorID,apinv.invoiceDate).
    cFileName = apinv.documentFilename.
    cLocalFile = cLocalPath + cFileName.
    /* delete the file if it exists */
    if search(cLocalFile) <> ?
     then os-delete value(cLocalFile).
    run repository/downloadrepositoryfile.p (cAccountUser, cAccountPass, cServerPath + cFileName, cLocalFile, output std-lo, output std-ch).
     
    if std-lo and search(cLocalFile) <> ?
     then run util/openfile.p (cLocalFile).
     else
      if std-ch > ""
       then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewReceivableCheckRequest Include 
PROCEDURE ViewReceivableCheckRequest :
/*------------------------------------------------------------------------------
@description Build the check request for a receivable invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  
  define buffer arinv for arinv.
  define buffer artrx for artrx.
  define buffer checkrequest for checkrequest.
  
  define variable iPolicyID as integer no-undo.
  define variable iClaimID as integer no-undo.
  
  for each arinv no-lock
     where arinv.arinvID = pInvoiceID:
      
    publish "GetSysIni" (output table ini).
      
    iClaimID = integer(arinv.refID) no-error.
    iPolicyID = 0.
    std-lo = false.
    run GetClaimPolicy in this-procedure (iClaimID, output iPolicyID) no-error.
    if error-status:error
     then publish "GetClaimPolicy" (iClaimID, output iPolicyID, output std-lo).
    run GetClaimPDFDetails in this-procedure (iClaimID, iPolicyID, output table checkrequest) no-error.
    if error-status:error
     then  publish "GetClaimPDFDetails" (iClaimID, iPolicyID, output table checkrequest).
    
    /* general claim and invoice info */
    for first checkrequest exclusive-lock:
      assign
        checkrequest.vendorName = arinv.name
        checkrequest.contactName = arinv.contactName
        checkrequest.contactEmail = arinv.contactEmail
        checkrequest.invoiceDate = arinv.dateRequested
        checkrequest.invoiceNotes = arinv.notes
        checkrequest.invoiceNumber = arinv.invoiceNumber
        checkrequest.invoiceDue = arinv.dueDate
        checkrequest.amount = arinv.requestedAmount
        .
      /* get the ini company */
      for each ini no-lock:
        case ini.id:
         when "CompanyTitle" then checkrequest.companyTitle = ini.val.
         when "CompanyAddr1" then checkrequest.companyAddress = ini.val.
         when "CompanyAddr2" then checkrequest.companySuite = ini.val.
         when "CompanyCity" then checkrequest.companyCity = ini.val + ", ".
         when "CompanyState" then checkrequest.companyCity = checkrequest.companyCity + ini.val + " ".
         when "CompanyZip" then checkrequest.companyCity = checkrequest.companyCity + ini.val.
        end case.
      end.
      /* get the trans amount */
      for each artrx no-lock
         where artrx.arinvID = arinv.arinvID:
         
        checkrequest.amount = checkrequest.amount - artrx.transAmount.
      end.
      /* if the invoice is waived or completed, the amount due is 0 */
      if arinv.stat = "W" or arinv.stat = "C"
       then checkrequest.amount = 0.
    end.
  end.
  
  /* build and open the report */
  run createinvoicedocument.p (input table checkrequest,
                               output std-ch
                               ).
  if std-ch > ""
   then
    do:
      publish "AddTempFile" (std-ch, std-ch).
      run util/openfile.p (std-ch).
    end.
   else message "Could not create document." view-as alert-box error buttons ok.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewReceivableDocument Include 
PROCEDURE ViewReceivableDocument :
/*------------------------------------------------------------------------------
@description Views the Payable document from ShareFile
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  
  define variable cServerPath as character no-undo.
  define variable cFileName   as character no-undo.
  define variable cLocalPath  as character no-undo.
  define variable cLocalFile  as character no-undo.
  
  for first arinv no-lock
      where arinv.arinvID = pInvoiceID:

    /* build the directory path */
    publish "GetTempDir" (output cLocalPath).
    cServerPath = getReceivableServerPath(arinv.dateRequested).
    cFileName = arinv.documentFilename.
    cLocalFile = cLocalPath + cFileName.
    /* delete the file if it exists */
    if search(cLocalFile) <> ?
     then os-delete value(cLocalFile).
    run repository/downloadrepositoryfile.p (cAccountUser, cAccountPass, cServerPath + cFileName, cLocalFile, output std-lo, output std-ch).

    if std-lo and search(cLocalFile) <> ?
     then run util/openfile.p (cLocalFile).
     else
      if std-ch > ""
       then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE VoidPayableTransaction Include 
PROCEDURE VoidPayableTransaction :
/*------------------------------------------------------------------------------
@description Voids a payable invoice transaction
------------------------------------------------------------------------------*/
  define input parameter pTransactionID as integer no-undo.
  define input parameter pNotes as character no-undo.
  define output parameter pSuccess as logical no-undo.
  
  define buffer aptrx for aptrx.
  define buffer apinv for apinv.
  
  pSuccess = false.
  for first aptrx exclusive-lock
      where aptrx.aptrxID = pTransactionID:

    run server/voidaptransaction.p (input pTransactionID,
                                    input pNotes,
                                    output pSuccess,
                                    output std-ch
                                    ).
                                   
    if pSuccess
     then
      for first apinv exclusive-lock
          where apinv.apinvID = aptrx.apinvID:
          
        create tempaptrx.
        assign
          tempaptrx.aptrxID = aptrx.aptrxID
          tempaptrx.transType = "V"
          tempaptrx.transAmount = 0 - aptrx.transAmount
          tempaptrx.transDate = aptrx.transDate
          tempaptrx.apinvID = apinv.apinvID
          tempaptrx.refType = apinv.refType
          tempaptrx.refID = apinv.refID
          tempaptrx.refSeq = apinv.refSeq
          tempaptrx.refCategory = apinv.refCategory
          tempaptrx.uid = cAccountUser
          tempaptrx.username = cAccountName
          tempaptrx.notes = pNotes
          tempaptrx.report = aptrx.report
          apinv.stat = "V"
          .
        validate tempaptrx.
        buffer-copy tempaptrx to aptrx.
        publish "ClaimAccountingDataChanged".
        publish "RefreshCoverages".
      end.
     else
      if std-ch > ""
       then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE VoidReceivableTransaction Include 
PROCEDURE VoidReceivableTransaction :
/*------------------------------------------------------------------------------
@description Voids a recievable invoice transaction
------------------------------------------------------------------------------*/
  define input parameter pTransactionID as integer no-undo.
  define input parameter pNotes as character no-undo.
  define output parameter pSuccess as logical no-undo.
  
  define buffer artrx for artrx.
  define buffer arinv for arinv.
  
  pSuccess = false.
  for first artrx exclusive-lock
      where artrx.artrxID = pTransactionID:

    run server/voidartransaction.p (input pTransactionID,
                                    input pNotes,
                                    output pSuccess,
                                    output std-ch
                                    ).
                                   
    if pSuccess
     then
      for first arinv exclusive-lock
          where arinv.arinvID = artrx.arinvID:
          
        create tempartrx.
        assign
          arinv.stat = "A"
          tempartrx.artrxID = artrx.artrxID
          tempartrx.transType = "V"
          tempartrx.transAmount = 0 - artrx.transAmount
          tempartrx.transDate = now
          tempartrx.arinvID = arinv.arinvID
          tempartrx.refType = arinv.refType
          tempartrx.refID = arinv.refID
          tempartrx.refSeq = arinv.refSeq
          tempartrx.refCategory = arinv.refCategory
          tempartrx.uid = cAccountUser
          tempartrx.username = cAccountName
          tempartrx.notes = pNotes
          .
        validate tempartrx.
        release tempartrx.
        publish "ClaimAccountingDataChanged".
      end.
     else
      if std-ch > ""
       then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  end.
  
  for first tempartrx no-lock:
    create artrx.
    buffer-copy tempartrx to artrx.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE WaiveReceivableInvoice Include 
PROCEDURE WaiveReceivableInvoice :
/*------------------------------------------------------------------------------
@description Waive the receivable invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  
  for first arinv exclusive-lock
      where arinv.arinvID = pInvoiceID:

    run server/waivearinvoice.p (input pInvoiceID,
                                 output std-lo,
                                 output std-ch
                                 ).
                                 
    if not std-lo and std-ch > ""
     then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
     else 
      do:
        arinv.stat = "W".
        publish "ClaimAccountingDataChanged".
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
