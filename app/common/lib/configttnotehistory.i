&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file configttnotehistory.i
@description Manages the temp table for history of the notes
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

&IF defined(entity) = 0 &THEN
&scoped-define entity ""
&ENDIF

&scoped-define filename noteshistory.xml

/* Temp Tables */
{tt/notehistory.i}
{tt/notehistory.i &tableAlias="entityNote"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

subscribe to "CompleteNoteHistory" anywhere.
subscribe to "DeleteNoteHistory"   anywhere.
subscribe to "GetNotesHistory"     anywhere.
subscribe to "SetNoteHistory"      anywhere.   

run LoadNotesHistory.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CompleteNoteHistory Include 
PROCEDURE CompleteNoteHistory :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pEntity   as character no-undo.
  define input parameter pEntityID as character no-undo.
  define input parameter pNotes    as character no-undo.

  if pEntity <> {&entity}
   then return.

  define variable tSeq  as integer   no-undo.
  define variable tTemp as character no-undo.

  tSeq = 1.
  for each notehistory no-lock
     where notehistory.entity = pEntity
       and notehistory.entityID = pEntityID
       and notehistory.seq <> 0
        by notehistory.seq desc:

    tSeq = tSeq + 1.
  end.

  for first notehistory
      where notehistory.entity = pEntity
        and notehistory.entityID = pEntityID
        and notehistory.seq = 0:

    assign
      notehistory.seq          = tSeq
      notehistory.noteDate     = now
      .
  end.
  run SaveNotesHistory in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteNoteHistory Include 
PROCEDURE DeleteNoteHistory :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pEntity   as character no-undo.
  define input parameter pEntityID as character no-undo.
  define input parameter pSeq      as integer   no-undo.

  if pEntity <> {&entity}
   then return.
  
  for first notehistory exclusive-lock
      where notehistory.entity = pEntity
        and notehistory.entityID = pEntityID
        and notehistory.seq = pSeq:
        
    delete notehistory.
  end.
  run SaveNotesHistory.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetNotesHistory Include 
PROCEDURE GetNotesHistory :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pEntity   as character no-undo.
  define input  parameter pEntityID as character no-undo.
  define output parameter table for entityNote.

  if pEntity <> {&entity}
   then return.
  
  empty temp-table entityNote.
  for each notehistory exclusive-lock
     where notehistory.entity = {&entity}
       and notehistory.entityID = pEntityID:
  
    /* replace the CR character */  
    create entityNote.
    buffer-copy notehistory to entityNote.
    entityNote.notes = replace(entityNote.notes, "&#13;", chr(10)).
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadNotesHistory Include 
PROCEDURE LoadNotesHistory :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tTemp as character no-undo.
  
  tTemp = os-getenv("appdata") + "\Alliant\{&filename}".
  if search(tTemp) = ?
   then 
    do: 
      os-create-dir value(os-getenv("appdata") + "\Alliant").
      return.
    end.
    
  temp-table notehistory:read-xml("file", tTemp, "empty", "", false).
  for each notehistory no-lock
     where notehistory.entity = {&entity}
       and notehistory.seq = 0:
     
    run CompleteNoteHistory in this-procedure ({&entity}, notehistory.entityID, notehistory.notes).
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveNotesHistory Include 
PROCEDURE SaveNotesHistory :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tTemp as character no-undo.
  
  /* trim the notes to hold only 14 days worth */
  for each notehistory exclusive-lock
     where notehistory.entity = {&entity}:
    
    if notehistory.noteDate <> ? and interval(now, notehistory.noteDate, "days") > 14
     then delete notehistory.
  end.
  
  tTemp = os-getenv("appdata") + "\Alliant\{&filename}".
  temp-table notehistory:write-xml("file", tTemp).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetNoteHistory Include 
PROCEDURE SetNoteHistory :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pEntity   as character no-undo.
  define input parameter pEntityID as character no-undo.
  define input parameter pNotes    as character no-undo.
  define input parameter pSubject  as character no-undo.
  define input parameter pCategory as character no-undo.

  if pEntity <> {&entity}
   then return.

  if not can-find(first notehistory where entity = pEntity and entityID = pEntityID and seq = 0)
   then
    do:
      create notehistory.
      assign
        notehistory.entity       = pEntity
        notehistory.entityID     = pEntityID
        notehistory.seq          = 0
        .
    end.

  for first notehistory exclusive-lock
      where notehistory.entity = pEntity
        and notehistory.entityID = pEntityID
        and notehistory.seq = 0:

    assign
      notehistory.notes    = replace(pNotes,chr(10),"&#13;")
      notehistory.subject  = pSubject
      notehistory.category = pCategory
      .
  end.
  run SaveNotesHistory in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

