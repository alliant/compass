&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file sysinidatasrv.i
@description Data Server file for getting the server ini file

@author John Oliver
@created 04-22-2019
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}
define variable tLoadedVendors as logical initial false no-undo.

/* tables */
{tt/vendor.i}
{tt/esbmsg.i &tableAlias="vendoresbmsg"}

/* temp tables */
{tt/vendor.i &tableAlias="tempvendor"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshVendor Procedure
FUNCTION refreshVendor RETURNS logical
  ( input pVendorID as character ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* main procedures */
subscribe to "VendorChanged"     anywhere.
subscribe to "GetVendor"         anywhere.
subscribe to "GetVendors"        anywhere.
subscribe to "LoadVendors"       anywhere.

/* used for the messaging system */
subscribe to "TIMER"              anywhere run-procedure "VendorTimer".
subscribe to "ExternalEsbMessage" anywhere run-procedure "VendorESB".

/* used to dump the data file */
subscribe to "DataDump"           anywhere run-procedure "VendorDataDump".

/* load system users */
std-lo = true.
publish "SetSplashStatus".
publish "GetLoadVendors" (output std-lo).
if std-lo 
 then run LoadVendors.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetVendor Procedure 
PROCEDURE GetVendor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pVendorID as character no-undo.
  define output parameter table for tempvendor.

  /* buffers */
  define buffer vendor for vendor.

  /* get the vendors */
  empty temp-table tempvendor.
  run GetVendors (output table tempvendor).

  /* get correct vendor */
  for each tempvendor exclusive-lock
     where tempvendor.vendorId <> pVendorID:
     
    delete tempvendor.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetVendor Include 
PROCEDURE GetVendors :
/*------------------------------------------------------------------------------
@description Gets the system ini records
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for vendor.

  /* load the ini records if not done already  */
  if not tLoadedVendors
   then run LoadVendors.
   
  for each vendor exclusive-lock:
    vendor.fulladdress = vendor.address + " " + vendor.city + ", " + vendor.state + " " + vendor.zipcode.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadVendors Include 
PROCEDURE LoadVendors :
/*------------------------------------------------------------------------------
@description Loads the vendors
------------------------------------------------------------------------------*/
  /* call the server */
  empty temp-table vendor.
  run server/getvendors.p ("",
                           "", /* All vendors */
                           output table vendor,
                           output tLoadedVendors,
                           output std-ch
                           ).

  /* if unsuccessful, show a message if debug */
  if not tLoadedVendors
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadVendors failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE VendorChanged Include 
PROCEDURE VendorChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pVendorID as character no-undo.
  
  /* refresh the alert */
  refreshVendor(pVendorID).
  
  /* publish that the data changed */
  publish "VendorDataChanged".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE VendorDataDump Include 
PROCEDURE VendorDataDump :
/*------------------------------------------------------------------------------
@description Dump the vendor and tempvendor temp-tables into files
------------------------------------------------------------------------------*/
  /* variables */
  define variable tPrefix as character no-undo.
  
  /* get the prefix */
  publish "GetAppVendor" (output tPrefix).
  tPrefix = tPrefix + "_".
  
  /* delete the current temp files */
  publish "DeleteTempFile" ("DataVendor", output std-lo).
  publish "DeleteTempFile" ("DataTempVendor", output std-lo).
  
  /* Active temp-tables */
  temp-table vendor:write-xml("file", tPrefix + "Vendor.xml").
  publish "AddTempFile" ("DataVendor", tPrefix + "Vendor.xml").
  
  /* "Temp" temp-tables */
  temp-table tempvendor:write-xml("file", tPrefix + "TempVendor.xml").
  publish "AddTempFile" ("DataVendor", tPrefix + "TempVendor.xml").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE VendorESB Procedure 
PROCEDURE VendorESB :
/*------------------------------------------------------------------------------
@description Runs when an esb message is received
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey as character no-undo.
  
  /* exit if not correct entity */
  if pEntity <> "Vendor"
   then next.
  
  /* get the next number */
  std-in = 1.
  for each vendoresbmsg no-lock:
    std-in = std-in + 1.
  end.
  
  /* create the esb message */
  create vendoresbmsg.
  assign
    vendoresbmsg.seq    = std-in
    vendoresbmsg.rcvd   = now
    vendoresbmsg.entity = pEntity
    vendoresbmsg.action = pAction
    vendoresbmsg.keyID  = pKey
    .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE VendorTimer Include 
PROCEDURE VendorTimer :
/*------------------------------------------------------------------------------
@description Triggered by appstart.i running on a timer
------------------------------------------------------------------------------*/
  /* buffers */
  define buffer vendoresbmsg for vendoresbmsg.

  /* loop through the esb messages */
  for each vendoresbmsg exclusive-lock
  break by vendoresbmsg.keyID
        by vendoresbmsg.rcvd descending:

    /* if the vendor is loaded and the we have a messages */
    if tLoadedVendors and first-of(vendoresbmsg.keyID) 
     then run VendorChanged (vendoresbmsg.keyID).
     
    /* delete the esb message and set the last sync */
    delete vendoresbmsg.
    publish "SetLastSync".
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshAccount Include 
FUNCTION refreshVendor RETURNS logical
  ( input pVendorID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* parameters */
  define variable lSuccess as logical no-undo.
  
  /* buffers */
  define buffer tempvendor for tempvendor.
  define buffer vendor for vendor.
  
  /* call the server */
  empty temp-table tempvendor.
  run server/getvendors.p (input  pVendorID,
                           output table tempvendor,
                           output lSuccess,
                           output std-ch).

  /* if unsuccessful, show a message if debug is on */
  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshVendor failed: " + std-ch view-as alert-box warning.
    end.
   else
    /* otherwise, copy the vendor details into the vendor records */
    do:
      find tempvendor where tempvendor.vendorID = pVendorID no-error.
      find vendor where vendor.vendorID = pVendorID no-error.
      if not available tempvendor and available vendor /* delete */
       then delete vendor.
      if available tempvendor and not available vendor /* new */
       then create vendor.
      if available tempvendor and available vendor /* modify */
       then buffer-copy tempvendor to vendor.
    end.
  
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME