&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/configttapp.i
   contains routines related to the APPlication parameter that are generally
   used in multiple applications.
   Created D.Sinclair 9.18.2012
   12.5.2014 D.Sinclair Added *RequestLimit to hold number of records in eventviewer
   5.20.2015 D.Sinclair Added *AppTaskLoad to optionally retrieve records to allow for filtering
 */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 32.52
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* Read-only */
subscribe to "GetAppCode" anywhere.
subscribe to "GetAppName" anywhere.
subscribe to "GetAppVersion" anywhere.
subscribe to "GetAppRevised" anywhere.
subscribe to "GetAppSharefile" anywhere.
subscribe to "GetAppAlfresco" anywhere.
subscribe to "GetAppSingleton" anywhere.
subscribe to "GetAppAuthenticate" anywhere.
subscribe to "GetAppSiteAddress" anywhere.

/* Read/Write */
/* Show Splash on startup */
subscribe to "GetAppSplash" anywhere.
subscribe to "SetAppSplash" anywhere.

/* Enable detailed debugging of calls */
subscribe to "GetAppDebug" anywhere.
subscribe to "SetAppDebug" anywhere.

/* Run AdminService on startup (running on localhost:port) */ 
subscribe to "GetAppAdmin" anywhere.
subscribe to "SetAppAdmin" anywhere.

/* Port for AdminService */
subscribe to "GetAppPort" anywhere.
subscribe to "SetAppPort" anywhere.

/* Connect to MQ Server on startup */
subscribe to "GetAppESB" anywhere.
subscribe to "SetAppESB" anywhere.

/* Esb launch on startup */
subscribe to "GetAppEsbLaunch" anywhere.
subscribe to "SetAppEsbLaunch" anywhere.

/* Request Limit for event data */
subscribe to "GetAppRequestLimit" anywhere.
subscribe to "SetAppRequestLimit" anywhere.

/* Request auto refresh for event data */
subscribe to "GetAppRequestRefresh" anywhere.
subscribe to "SetAppRequestRefresh" anywhere.

/* Request launch on startup */
subscribe to "GetAppRequestLaunch" anywhere.
subscribe to "SetAppRequestLaunch" anywhere.

/* Tasks launch on startup */
subscribe to "GetAppTaskLaunch" anywhere.
subscribe to "SetAppTaskLaunch" anywhere.

/* Tasks load on startup */
subscribe to "GetAppTaskLoad" anywhere.
subscribe to "SetAppTaskLoad" anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppAdmin Include 
PROCEDURE GetAppAdmin :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pStart as logical init false.
 std-ch = getOption("ApplicationAdmin").
 if std-ch = "" 
  then std-ch = getSetting("ApplicationAdmin").
 if lookup(std-ch, "yes,y,true,t,1") > 0 
  then pStart = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppAlfresco Include 
PROCEDURE GetAppAlfresco :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pAlfrescoActive as logical init false.

 std-ch = getSetting("ApplicationAlfresco").
 if lookup(std-ch, "yes,y,true,t,1") > 0 
  then pAlfrescoActive = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppAuthenticate Include 
PROCEDURE GetAppAuthenticate :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pCheck as logical init false.

 std-ch = getSetting("ApplicationAuthenticate").
 if lookup(std-ch, "yes,y,true,t,1") > 0 
  then pCheck = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppCode Include 
PROCEDURE GetAppCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pName as char.
pName = getSetting("ApplicationCode").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppDebug Include 
PROCEDURE GetAppDebug :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pShow as logical init false.
 std-ch = getOption("ApplicationDebug").
 if std-ch = "" 
  then std-ch = getSetting("ApplicationDebug").
 if lookup(std-ch, "yes,y,true,t,1") > 0 
  then pShow = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppESB Include 
PROCEDURE GetAppESB :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pStart as logical init false.
 std-ch = getOption("ApplicationESB").
 if std-ch = "" 
  then std-ch = getSetting("ApplicationESB").
 if lookup(std-ch, "yes,y,true,t,1") > 0 
  then pStart = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppEsbLaunch Include 
PROCEDURE GetAppEsbLaunch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pShow as logical init false.
 std-ch = getOption("ApplicationEsbLaunch").
 if std-ch = "" 
  then std-ch = getSetting("ApplicationEsbLaunch").
 if lookup(std-ch, "yes,y,true,t,1") > 0 
  then pShow = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppName Include 
PROCEDURE GetAppName :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pName as char.
pName = getSetting("ApplicationName").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppPort Include 
PROCEDURE GetAppPort :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pPort as int.
pPort = integer(getSetting("ApplicationPort")) no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppRequestLaunch Include 
PROCEDURE GetAppRequestLaunch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pAuto as logical init false.
 std-ch = getOption("ApplicationRequestLaunch").
 if std-ch = "" 
  then std-ch = getSetting("ApplicationRequestLaunch").
 if lookup(std-ch, "yes,y,true,t,1") > 0 
  then pAuto = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppRequestLimit Include 
PROCEDURE GetAppRequestLimit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pCount as int init 50.
 std-ch = getOption("ApplicationRequestLimit").
 if std-ch = "" 
  then std-ch = getSetting("ApplicationRequestLimit").
 std-in = integer(std-ch) no-error.
 if std-in <> ? and std-in > 0
  then pCount = std-in.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppRequestRefresh Include 
PROCEDURE GetAppRequestRefresh :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pAuto as logical init false.
 std-ch = getOption("ApplicationRequestRefresh").
 if std-ch = "" 
  then std-ch = getSetting("ApplicationRequestRefresh").
 if lookup(std-ch, "yes,y,true,t,1") > 0 
  then pAuto = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppRevised Include 
PROCEDURE GetAppRevised :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pRev as char.
pRev = getSetting("ApplicationRevision").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppSharefile Include 
PROCEDURE GetAppSharefile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pSfActive as logical init false.

 std-ch = getSetting("ApplicationSharefile").
 if lookup(std-ch, "yes,y,true,t,1") > 0 
  then pSfActive = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppSingleton Include 
PROCEDURE GetAppSingleton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pShow as logical init false.

 std-ch = getSetting("ApplicationSingleton").
 if lookup(std-ch, "yes,y,true,t,1") > 0 
  then pShow = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppSiteAddress Include 
PROCEDURE GetAppSiteAddress :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pName as char.
pName = getSetting("ApplicationSiteAddress").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppSplash Include 
PROCEDURE GetAppSplash :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pShow as logical init false.
 std-ch = getOption("ApplicationSplash").
 if std-ch = "" 
  then std-ch = getSetting("ApplicationSplash").
 if lookup(std-ch, "yes,y,true,t,1") > 0 
  then pShow = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppTaskLaunch Include 
PROCEDURE GetAppTaskLaunch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pShow as logical init false.
 std-ch = getOption("ApplicationTaskLaunch").
 if std-ch = "" 
  then std-ch = getSetting("ApplicationTaskLaunch").
 if lookup(std-ch, "yes,y,true,t,1") > 0 
  then pShow = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppTaskLoad Include 
PROCEDURE GetAppTaskLoad :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pShow as logical init false.
 std-ch = getOption("ApplicationTaskLoad").
 if std-ch = "" 
  then std-ch = getSetting("ApplicationTaskLoad").
 if lookup(std-ch, "yes,y,true,t,1") > 0 
  then pShow = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAppVersion Include 
PROCEDURE GetAppVersion :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pVer as char.
pVer = getSetting("ApplicationVersion").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetAppAdmin Include 
PROCEDURE SetAppAdmin :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pStart as logical.
 setOption("ApplicationAdmin", (if pStart then "yes" else "no")).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetAppDebug Include 
PROCEDURE SetAppDebug :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pShow as logical.
 setOption("ApplicationDebug", (if pShow then "yes" else "no")).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetAppESB Include 
PROCEDURE SetAppESB :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pStart as logical.
 setOption("ApplicationESB", (if pStart then "yes" else "no")).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetAppEsbLaunch Include 
PROCEDURE SetAppEsbLaunch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pShow as logical.
 setOption("ApplicationEsbLaunch", (if pShow then "yes" else "no")).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetAppPort Include 
PROCEDURE SetAppPort :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pPort as int.
 setOption("ApplicationPort", string(pPort)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetAppRequestLaunch Include 
PROCEDURE SetAppRequestLaunch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAuto as logical.
 setOption("ApplicationRequestLaunch", (if pAuto then "yes" else "no")).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetAppRequestLimit Include 
PROCEDURE SetAppRequestLimit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pCount as int.
 setOption("ApplicationRequestLimit", string(pCount)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetAppRequestRefresh Include 
PROCEDURE SetAppRequestRefresh :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAuto as logical.
 setOption("ApplicationRequestRefresh", (if pAuto then "yes" else "no")).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetAppSplash Include 
PROCEDURE SetAppSplash :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pShow as logical.
 setOption("ApplicationSplash", (if pShow then "yes" else "no")).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetAppTaskLaunch Include 
PROCEDURE SetAppTaskLaunch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pShow as logical.
 setOption("ApplicationTaskLaunch", (if pShow then "yes" else "no")).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetAppTaskLoad Include 
PROCEDURE SetAppTaskLoad :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pShow as logical.
 setOption("ApplicationTaskLoad", (if pShow then "yes" else "no")).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

