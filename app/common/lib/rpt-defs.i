/*------------------------------------------------------------------------
    File        : lib/rpt-defs.i
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : MK
    Created     : 06/03/21
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
define variable rpt-format          as character initial "P" no-undo. /* P,C */
define variable rpt-destinationType as character initial "E" no-undo. /* C,E,P,V */
define variable rpt-behavior        as character no-undo.
define variable rpt-notifyRequestor as character no-undo.
define variable rpt-destination     as character no-undo.             /* S or C */
define variable rpt-email           as character no-undo.
define variable rpt-printer         as character no-undo.
define variable rpt-repository      as character no-undo.
define variable rpt-outputFormat    as character no-undo.
define variable rpt-cancel          as logical   no-undo.

&global-define rpt-csv   "C"
&global-define rpt-pdf   "P"


&global-define rpt-destConfigured   "C"
&global-define rpt-destEmail        "E"
&global-define rpt-destPrinter      "P"
&global-define rpt-destView         "V"

