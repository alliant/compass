&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file get-period-list.i
@description used to populate the month and year combo boxes

@param mth;widget;A handle to the month widget
@param y;widget;A handle to the year widget (optional)
@param startYear;integer;The starting year
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */


/* Local Variable Definitions ---                                       */
{lib/std-def.i}

&IF defined(startYear) = 0 &THEN
&scoped-define startYear 2010
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GetMonthName Include 
FUNCTION GetMonthName RETURNS CHARACTER
  ( input pMonth as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

&if defined(mth) <> 0 &then
{&mth}:inner-lines = 12.
{&mth}:list-item-pairs = "ALL" + {&mth}:delimiter + "0".
&endif
&if defined(yr) <> 0 &then
{&yr}:inner-lines = 10.
{&yr}:list-items = "0".
&endif
publish "GetPeriods" (output table period).
for each period no-lock
   where period.periodYear >= {&startYear} 
      by period.periodID:
  
  &if defined(mth) <> 0 and defined(yr) <> 0 &then /* both month and year */
  /* month */
  if lookup(GetMonthName(period.periodMonth), {&mth}:list-item-pairs, {&mth}:delimiter) = 0
   then {&mth}:add-last(GetMonthName(period.periodMonth), period.periodMonth).
  /* year */
  if lookup(string(period.periodYear), {&yr}:list-items, {&yr}:delimiter) = 0
   then {&yr}:add-last(string(period.periodYear)).
  &elseif defined(mth) <> 0 and defined(yr) = 0 &then /* only month */
  /* combine the month and year */
    &if defined(noYear) = 0 &then
    {&mth}:inner-lines = 15.
    if lookup(string(period.periodID), {&mth}:list-item-pairs, {&mth}:delimiter) = 0
     then {&mth}:add-last(substring(GetMonthName(period.periodMonth),1,3) + " " + string(period.periodYear), period.periodID).
    &else
    if lookup(GetMonthName(period.periodMonth), {&mth}:list-item-pairs, {&mth}:delimiter) = 0
     then {&mth}:add-last(GetMonthName(period.periodMonth), period.periodMonth).
    &endif
  &elseif defined(mth) = 0 and defined(yr) <> 0 &then /* only year */
  if lookup(string(period.periodYear), {&yr}:list-items, {&yr}:delimiter) = 0
   then {&yr}:add-last(string(period.periodYear)).
  &endif
end.
&if defined(mth) <> 0 &then
{&mth}:delete(1).
if not can-find(first period)
 then {&mth}:add-last(GetMonthName(month(today)), month(today)).
{&mth}:screen-value = string(month(today)).
&endif
&if defined(yr) <> 0 &then
{&yr}:delete(1).
if not can-find(first period)
 then {&yr}:add-last(string(year(today))).
{&yr}:screen-value = string(year(today)).
&endif
   
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&if defined(GetMonthName) = 0 &then
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GetMonthName Include 
FUNCTION GetMonthName RETURNS CHARACTER
  ( input pMonth as integer ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  RETURN entry(pMonth, "January,February,March,April,May,June,July,August,September,October,November,December").   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
&global-define GetMonthName true
&endif
