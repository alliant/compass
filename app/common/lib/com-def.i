/*------------------------------------------------------------------------
File        : lib/com-def.i
Purpose     :

Syntax      :

Description :

Author(s)   : Rahul Sharma
Created     : 07-04-2018
Notes       :

Modification:
Date          Name           Description
08/02/2018    Gurvindar      Added and Removed preprocessors
09/27/2018    Naresh Chopra  Added new preprocessors "InactiveStat,PendingCancelStat"                                                          
10/04/2018    Naresh Chopra  Added new preprocessor
10/09/2018    Naresh Chopra  Removed "PendingcancelStat, InactiveStat" and 
                             added new preprocessor "withdrawnStat"
01/07/2019    Rahul          Added new preprocessors "Third-Party,First-Party"   
09/16/2019    Rahul          Added new preprocessor  "InActiveStat" 
09/18/2019    Rahul          Added new preprocessor  "Organizationrole"                    
10/04/2019    Rahul          Added new preprocessor  Organizationnew and OrganizationModify
03/17/2022    Shefali        Task# 86699 Added new preprocessors "LawFirm", "LawFirmCode", "AttorneyFirm",
                             "AttorneyFirmCode" and "PersonCopy"  
03/03/2022    Shefali        Task 102924 Added new preprocessor "Expired"
03/08/2023    S Chandu       Added new preprocessor OrganizationRenew and PersonRenew.
----------------------------------------------------------------------*/
&global-define Agent             "Agent"
&global-define AgentCode         "A"

&global-define Attorney          "Attorney"
&global-define AttorneyCode      "T"

&global-define AttorneyFirm      "Attorney Firm"
&global-define AttorneyFirmCode  "AF"

&global-define LawFirm           "Law Firm"
&global-define LawFirmCode       "L"

&global-define Organization      "Organization"
&global-define OrganizationCode  "O"
&global-define OrganizationRole  "OrganizationRole"
&global-define OrganizationRoleCode  "OR"
&global-define OrganizationCopy  "OC"
&global-define OrganizationRenew "ORN"

&global-define CompanyCode       "C"
&global-define Company           "Company"

&global-define AffiliationCode   "A"
&global-define QualificationCode "Q"

&global-define Role              "Role"
&global-define PersonRole        "PersonRole"
&global-define PersonRoleCode    "R"

&global-define Person            "Person"
&global-define PersonCode        "P"
&global-define PersonCopy        "PC"
&global-define PersonRenew       "PRN"

&global-define Third-Party       "Third-Party"
&global-define ThirdParty        "T"

&global-define First-Party       "First-Party"
&global-define FirstParty        "C"

&global-define AgentPerson       "AgentPerson"
&global-define SysCode           "sysCode"
&global-define parameter         "Parameter"
&global-define Requirement       "Requirement"
&global-define Qualification     "Qualification"
&global-define ControlPerson     "ControlPerson"

/* Agents qualifications created from the one time setup utility. */
&global-define TitleLicense      "Title License"
&global-define AgentTitleLicense "License to Produce Title"
&global-define AgentEvidenceQual "Evidence of Errors & Omissions Insurance"
&global-define AgentEvidenceReq  "Errors & Omissions Insurance"

/*----------window status bar messeges----------------*/
&global-define ResultNotMatch    "Results may not match current parameters."
&global-define NewRecordCreated  "New record has been created, please refresh your screen."

/*----------compliance status---------------------------*/
&global-define OutOfCompliance   "Out of Compliance"
&global-define InCompliance      "In Compliance"
&global-define NeedsAttention    "Needs Attention"

/*----------qual status---------------------------*/
&global-define GoodStanding      "G"
&global-define Waived            "W"
&global-define NeedsReview       "N"
&global-define Expired           "E"

/*----------agent status--------------------------*/
&global-define Active            "Active"
&global-define InActive          "Inactive"
&global-define Prospect          "Prospect"
&global-define Closed            "Closed"
&global-define Cancelled         "Cancelled"
&global-define Withdrawn         "Withdrawn"
&global-define Inactive          "Inactive"

/*----------organization status--------------------------*/
&global-define InActiveStat      "I"
&global-define ALL               "ALL"
&global-define Both              "B"
/*------------------colours----------------------*/
&global-define Gcode             "G"
&global-define Green             "Green"
&global-define Rcode             "R"
&global-define Red               "Red"
&global-define Ycode             "Y"
&global-define Yellow            "Yellow"

/*------------------Attorney Status---------------*/
&global-define ActiveStat        "A"
&global-define ProspectStat      "P"
&global-define CancelledStat     "X"
&global-define ClosedStat        "C"
&global-define WithdrawnStat     "W"

/*-----------------Person Status---------------*/
&global-define Affiliated        "F"
&global-define NotAffiliated     "N"

/*-------ReqStatus---------------------------------*/
&global-define ExpireDate        "On a date"
&global-define ExpireDays        "After number of days"
&global-define ExpireNot         "Does not expire"

&global-define InUse             "InUse"
&global-define SelectState       "--Select State--"
&global-define SelectRole        "--Select Role--"

/*-------------------Default------------------------*/
&global-define CompanyID         "0"
&global-define National          "00"
&global-define NationalState     "National,00"
&global-define DefaultUser       "compass@alliantnational.com"

&global-define ActionAdd         "A"
&global-define ActionNew         "N"
&global-define ActionEdit        "E"

&global-define PersonNew           "PN"
&global-define PersonModify        "PM"
&global-define AgentNew            "AN"
&global-define AgentModify         "AM"
&global-define CompanyNew          "CN"
&global-define CompanyModify       "CM"
&global-define OrganizationNew     "ON"
&global-define OrganizationModify  "OM"


&global-define RadioBoth         "B"
&global-define RadioMet          "M"
&global-define RadioUnMet        "U"
&global-define mandatory         "*"

&global-define NatureOfAffiliation  "NatureOfAffiliation"

&global-define ComCode           {&Requirement} + "," + {&Qualification} + "," +  {&PersonRole} + "," + {&OrganizationRole}
&global-define ComEmail          "Work" + "," + "Personal" + "," +  "Other" 
&global-define ComPhone          "Mobile" + "," + "Work" + "," + "Home" + "," +  "Main" + "," + "Fax" + "," + "Pager" + "," + "Other" 

