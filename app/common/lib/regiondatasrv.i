&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file regiondatasrv.i
@description Data Server file for managing the region

@author John Oliver
@created 04-17-2019
@notes Remade from commondatasrv.i
@modification
   date         Name           Description
   06/22/2022   SA             Task 93485 - Modified 'RegionChanged' procedure
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}
define variable tLoadedRegions as logical initial false no-undo.

/* tables */
{tt/region.i}
{tt/esbmsg.i &tableAlias="regionesbmsg"}

/* temp tables */
{tt/region.i &tableAlias="tempregion"}

/* functions */
{lib/add-delimiter.i}
{lib/getstatename.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* region procedures */
subscribe to "DeleteRegion"         anywhere.
subscribe to "GetRegion"            anywhere.
subscribe to "GetRegions"           anywhere.
subscribe to "LoadRegions"          anywhere.
subscribe to "ModifyRegion"         anywhere.
subscribe to "NewRegion"            anywhere.
subscribe to "RegionChanged"        anywhere.
subscribe to "GetRegionDescription" anywhere.

/* used for the messaging system */
subscribe to "TIMER"              anywhere run-procedure "RegionTimer".
subscribe to "ExternalEsbMessage" anywhere run-procedure "RegionESB".

/* used to dump the data file */
subscribe to "DataDump"           anywhere run-procedure "RegionDataDump".

std-lo = true.
publish "SetSplashStatus".
publish "GetLoadRegions" (output std-lo).
if std-lo
 then run LoadRegions.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteRegion Include 
PROCEDURE DeleteRegion :
/*------------------------------------------------------------------------------
@description Deletes a region
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pRegionID as character no-undo.
  define output parameter pSuccess  as logical no-undo initial false.
  
  /* buffers */
  define buffer region for region.
  
  /* call the server */
  run server/deleteregion.p (input  pRegionID,
                             output pSuccess,
                             output std-ch).

  /* if unsuccessful, show a message */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else run RegionChanged in this-procedure (pRegionID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetRegion Include 
PROCEDURE GetRegion :
/*------------------------------------------------------------------------------
@description Gets a region for the state
------------------------------------------------------------------------------*/
  /* parameters */
  define input  param pStateID  as character no-undo.
  define output param pRegionID as character no-undo initial "".

  /* get all the agents */
  run GetRegions (output table region).

  /* get the region ID for the state passed in */
  for first region no-lock
      where region.stateID = pStateID:

    pRegionID = region.regionID.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetRegionDescription Include 
PROCEDURE GetRegionDescription :
/*------------------------------------------------------------------------------
@description Gets a region for the state
------------------------------------------------------------------------------*/
  /* parameters */
  define input  param pRegionID   as character no-undo.
  define output param pRegionDesc as character no-undo initial "Unknown".

  /* get the region */
  run GetRegions (output table region).

  /* get the description of the region */
  for first region no-lock
      where region.regionID = pRegionID:
      
    pRegionDesc = region.regionDesc.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetRegions Include 
PROCEDURE GetRegions :
/*------------------------------------------------------------------------------
@description Gets the regions
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for region.

  /* if not loaded, load the regions */
  if not tLoadedRegions 
   then run LoadRegions.
  
  /* put the sequence on the regions */
  std-in = 1.
  for each region exclusive-lock break by region.regionID:
    region.seq = std-in.
    
    if last-of(region.regionID)
     then std-in = 1.
     else std-in = std-in + 1.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadRegions Include 
PROCEDURE LoadRegions :
/*------------------------------------------------------------------------------
@description Loads the Regions
------------------------------------------------------------------------------*/
  define variable cRegionList as character no-undo.
  
  /* call the server */
  empty temp-table region.
  run server/getregions.p (output table region,
                           output tLoadedRegions,
                           output std-ch).
                           

  
  /* if unsuccessful, show a message if debug is on */
  if not tLoadedRegions
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadRegions failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyRegion Include 
PROCEDURE ModifyRegion :
/*------------------------------------------------------------------------------
@description Modifies a region
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pRegionID  as character no-undo.
  define input  parameter pStateList as character no-undo.
  define output parameter pSuccess   as logical no-undo initial false.
  
  /* buffers */
  define buffer region for region.
  
  /* call the server */
  run server/modifyregion.p (input  pRegionID,
                             input  pStateList,
                             output pSuccess,
                             output std-ch).

  /* if unsuccessful, show a message */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else run RegionChanged in this-procedure (pRegionID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewRegion Include 
PROCEDURE NewRegion :
/*------------------------------------------------------------------------------
@description Creates a new region
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pRegionID   as character no-undo.
  define input  parameter pRegionDesc as character no-undo.
  define input  parameter pStateList  as character no-undo.
  define output parameter pSuccess    as logical no-undo initial false.
  
  /* buffers */
  define buffer region for region.
  
  /* call the server */
  run server/newregion.p (input  pRegionID,
                          input  pRegionDesc,
                          input  pStateList,
                          output pSuccess,
                          output std-ch).

  /* if unsuccessful, show a message */
  if not pSuccess
   then message std-ch view-as alert-box error.
   else run RegionChanged in this-procedure (pRegionID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RegionChanged Include 
PROCEDURE RegionChanged :
/*------------------------------------------------------------------------------
@description Reloads a region
------------------------------------------------------------------------------*/
  define input parameter pRegionID as character no-undo.
  define variable cRegionList as character no-undo.
    
  /* refresh the data */
  run LoadRegions in this-procedure.
  
  /* change the system properties */
  publish "SysCodeChanged" ("Region").
  
  for each region no-lock:
    cRegionList =  cRegionList + (if cRegionList = "" then "" else ",") + region.stateID + "," + region.regionID.
  end.
  /*calling to statedatasrv*/
  publish "UpdateStateDetails"(cRegionList).
  
  temp-table region:write-xml("file","region.xml").
  
  /* publish that the data changed */
  publish "RegionDataChanged".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RegionDataDump Include 
PROCEDURE RegionDataDump :
/*------------------------------------------------------------------------------
@description Dump the region and tempregion temp-tables into files
------------------------------------------------------------------------------*/
  /* variables */
  define variable tPrefix as character no-undo.
  
  /* get the prefix */
  publish "GetAppCode" (output tPrefix).
  tPrefix = tPrefix + "_".
  
  /* delete the current temp files */
  publish "DeleteTempFile" ("DataRegion", output std-lo).
  publish "DeleteTempFile" ("DataTempRegion", output std-lo).
  
  /* Active temp-tables */
  temp-table region:write-xml("file", tPrefix + "Region.xml").
  publish "AddTempFile" ("DataRegion", tPrefix + "Region.xml").
  
  /* "Temp" temp-tables */
  temp-table tempregion:write-xml("file", tPrefix + "TempRegion.xml").
  publish "AddTempFile" ("DataRegion", tPrefix + "TempRegion.xml").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RegionESB Procedure 
PROCEDURE RegionESB :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey as character no-undo.
  
  /* exit if not correct entity */
  if pEntity <> "Region"
   then next.
  
  /* get the next number */
  std-in = 1.
  for each regionesbmsg no-lock:
    std-in = std-in + 1.
  end.
  
  /* create the esb message */
  create regionesbmsg.
  assign
    regionesbmsg.seq    = std-in
    regionesbmsg.rcvd   = now
    regionesbmsg.entity = pEntity
    regionesbmsg.action = pAction
    regionesbmsg.keyID  = pKey
    .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RegionTimer Procedure 
PROCEDURE RegionTimer :
/*------------------------------------------------------------------------------
  Purpose:     Called when the application is idle
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* buffers */
  define buffer regionesbmsg for regionesbmsg.

  /* loop through the esb messages */
  for each regionesbmsg exclusive-lock
  break by regionesbmsg.keyID
        by regionesbmsg.rcvd descending:

    /* if the agent is loaded and the we have a messages */
    if tLoadedRegions and first-of(regionesbmsg.keyID) 
     then run RegionChanged (regionesbmsg.keyID).
     
    /* delete the esb message and set the last sync */
    delete regionesbmsg.
    publish "SetLastSync".
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */
