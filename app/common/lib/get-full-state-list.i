&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* ***************************  Definitions  ************************** */

&if defined(addAll) = 0 &then
&scoped-define addAll false
&endif

&if defined(useSingle) = 0 &then
&scoped-define useSingle false
&endif

&if defined(useFullName) = 0 &then
&scoped-define useFullName true
&endif

&if defined(firstValue) = 0 &then
&scoped-define firstValue "ALL,ALL"
&endif

&scoped-define StateName "Alabama,Alaska,Arizona,Arkansas,California,Colorado,Connecticut,District of Columbia,Delaware,Florida,Georgia,Hawaii,Idaho,Illinois,Indiana,Iowa,Kansas,Kentucky,Louisiana,Maine,Maryland,Massachusetts,Michigan,Minnesota,Mississippi,Missouri,Montana,Nebraska,Nevada,New Hampshire,New Jersey,New Mexico,New York,North Carolina,North Dakota,Ohio,Oklahoma,Oregon,Pennsylvania,Rhode Island,South Carolina,South Dakota,Tennessee,Texas,Utah,Vermont,Virginia,Washington,West Virginia,Wisconsin,Wyoming"
&scoped-define StateAbbr "AL,AK,AZ,AR,CA,CO,CT,DC,DE,FL,GA,HI,ID,IL,IN,IA,KS,KY,LA,ME,MD,MA,MI,MN,MS,MO,MT,NE,NV,NH,NJ,NM,NY,NC,ND,OH,OK,OR,PA,RI,SC,SD,TN,TX,UT,VT,VA,WA,WV,WI,WY"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* get the states */
if {&combo}:type = "COMBO-BOX"
 then {&combo}:inner-lines = 10.
if {&useSingle}
 then {&combo}:list-items = entry(1, {&firstValue}).
 else {&combo}:list-item-pairs = entry(1, {&firstValue}) + {&combo}:delimiter + entry(2, {&firstValue}).

do std-in = 1 to num-entries({&StateName}):
  if {&useSingle}
   then {&combo}:add-last(entry(std-in, {&StateAbbr})).
   else
    if {&useFullName}
     then {&combo}:add-last(entry(std-in, {&StateName}), entry(std-in, {&StateAbbr})).
     else {&combo}:add-last(entry(std-in, {&StateAbbr}), entry(std-in, {&StateAbbr})).
end.

if std-in > 0 and not {&addAll}
 then {&combo}:delete(1).

{&combo}:screen-value = entry(2, (if {&useSingle} then {&combo}:list-items else {&combo}:list-item-pairs), {&combo}:delimiter).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


