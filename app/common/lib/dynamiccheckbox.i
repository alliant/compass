define variable N{&seq} as handle      no-undo.
define variable Q{&seq} as handle      no-undo.

&global-define htog  N{&seq}. 
&global-define hEdit Q{&seq}. 

create toggle-box N{&seq}                                                                 
  assign  frame = frame {&frameName}:handle
          width-chars = 3
          height-chars = 0.71
          row = iNextEndors + 0.6
          visible = true
          sensitive = true
          column = 5
          label = ""
          checked = false
          triggers:
          on value-changed  persistent run doCheckBox in this-procedure .
          end triggers.

create editor Q{&seq}
  assign  column = 9
          fgcolor = 0
          frame = frame {&frameName}:handle
          word-wrap = true         
          sensitive = true
          width-chars = 60
          height-chars = 1.4
          row = iNextEndors + 0.6
          box = false
          screen-value = string({&tableName}.description) 
          visible = true
          read-only = true
          selectable = false
          triggers:
          on mouse-select-click  persistent run doEditor in this-procedure (N{&seq}).
          end triggers.
         .

create {&hTemp}.
assign 
  {&hTemp}.hseq     = string({&tableName}.endorsementCode)
  {&hTemp}.hToggle  = N{&seq}
  {&hTemp}.hEditor  = Q{&seq}
  {&hTemp}.hchecked = no.


