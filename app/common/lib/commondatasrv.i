&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file commondatasrv.i
@description Data Server file for common data elements

@param sysprop;char;Comma-delimited list of sysprops to get. The props are
                    separated by semicolon (:). The first is the appCode,
                    second is the objAction, and third is the objProperty
@param syscode;char;Comma-delimited list of syscodes to get
@param period;char;Either "Accounting" or "Batch"

@author John Oliver
@created 07-11-2018
Date          Name      Description
03/25/2019    Rahul     Modified to add input parameter for getsysusers.p
                        in procedure sysUserLoad
03/17/2022    Shefali   Task# 86699 Remove code for attorney.
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}
define variable tLoadedAccounts as logical initial false no-undo.
define variable tLoadedAgents as logical initial false no-undo.
define variable tLoadedCounties as logical initial false no-undo.
define variable tLoadedOffices as logical initial false no-undo.
define variable tLoadedPeriods as logical initial false no-undo.
define variable tLoadedRegions as logical initial false no-undo.
define variable tLoadedStates as logical initial false no-undo.
define variable tLoadedSysCodes as logical initial false no-undo.
define variable tLoadedSysIni as logical initial false no-undo.
define variable tLoadedSysProps as logical initial false no-undo.
define variable tLoadedSysUsers as logical initial false no-undo.
define variable tLoadedVendors as logical initial false no-undo.

define variable tLastEsbMsgSeq as integer no-undo initial 0.
define variable tLastSync as datetime no-undo.

/* tables */
{tt/account.i}
{tt/agent.i}
{tt/county.i}
{tt/esbmsg.i}
{tt/ini.i}
{tt/office.i}
{tt/period.i}
{tt/region.i}
{tt/reqfulfill.i}
{tt/state.i}
{tt/stateevent.i}
{tt/syscode.i}
{tt/sysprop.i}
{tt/sysuser.i}
{tt/vendor.i}
define temp-table openAgent
  field agentID as character
  field agentHandle as handle
  .

/* temp tables */
{tt/account.i &tableAlias="tempaccount"}
{tt/agent.i &tableAlias="tempagent"}
{tt/county.i &tableAlias="tempcounty"}
{tt/office.i &tableAlias="tempoffice"}
{tt/period.i &tableAlias="tempperiod"}
{tt/region.i &tableAlias="tempregion"}
{tt/state.i &tableAlias="tempstate"}
{tt/stateevent.i &tableAlias="tempstateevent"}
{tt/syscode.i &tableAlias="tempsyscode"}
{tt/sysprop.i &tableAlias="tempsysprop"}
{tt/sysuser.i &tableAlias="tempsysuser"}
{tt/vendor.i &tableAlias="tempvendor"}

/* functions */
{lib/add-delimiter.i}
{lib/getstatename.i}

/* preprocessors */
&if defined(sysprop) = 0 &then
std-ch = "".
publish "GetAppCode" (output std-ch).
&scoped-define sysprop std-ch
&endif

&if defined(syscode) = 0 &then
&scoped-define syscode ""
&endif

&if defined(period) = 0 &then
&scoped-define period "Batch"
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&if defined(excludeAgent) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshAgent Include 
FUNCTION refreshAgent RETURNS LOGICAL
  ( input pAgentID as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeCounty) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshCounty Include 
FUNCTION refreshCounty RETURNS logical
  ( input pStateID as character,
    input pCountyID as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludePeriod) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshPeriod Include 
FUNCTION refreshPeriod RETURNS logical
  ( input pPeriodID as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeRegion) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshRegion Include 
FUNCTION refreshRegion RETURNS logical
  ( input pRegionID as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeState) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshState Include 
FUNCTION refreshState RETURNS logical
  ( input pStateID as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeSysCode) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshSysCode Include 
FUNCTION refreshSysCode RETURNS logical
  ( input pCodeType as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeSysProp) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshSysProp Include 
FUNCTION refreshSysProp RETURNS LOGICAL
  ( input pAppCode as character,
    input pObjAction as character,
    input pObjProperty as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* account procedures */
subscribe to "GetAccounts" anywhere run-procedure "AccountGetAll".
subscribe to "LoadAccounts" anywhere run-procedure "AccountLoad".

/* agent procedures */
subscribe to "AgentChanged" anywhere.
subscribe to "CloseAgent" anywhere run-procedure "AgentClose".
subscribe to "DeleteAgent" anywhere run-procedure "AgentDelete".
subscribe to "GetAgent" anywhere run-procedure "AgentGet".
subscribe to "GetAgents" anywhere run-procedure "AgentGetAll".
subscribe to "LoadAgents" anywhere run-procedure "AgentLoad".
subscribe to "NewAgent" anywhere run-procedure "AgentNew".
subscribe to "OpenAgent" anywhere run-procedure "AgentOpen".


/* county procedures */
subscribe to "CountyChanged" anywhere.
subscribe to "DeleteCounty" anywhere run-procedure "CountyDelete".
subscribe to "GetCounties" anywhere run-procedure "CountyGetAll".
subscribe to "LoadCounties" anywhere run-procedure "CountyLoad".
subscribe to "ModifyCounty" anywhere run-procedure "CountyModify".
subscribe to "NewCounty" anywhere run-procedure "CountyNew".

/* office procedures */
subscribe to "GetOffices" anywhere run-procedure "OfficeGetAll".
subscribe to "LoadOffices" anywhere run-procedure "OfficeLoad".

/* period procedures */
subscribe to "PeriodChanged" anywhere.
subscribe to "ClosePeriod" anywhere run-procedure "PeriodClose".
subscribe to "DeletePeriod" anywhere run-procedure "PeriodDelete".
subscribe to "GetPeriodID" anywhere run-procedure "PeriodGet".
subscribe to "GetPeriods" anywhere run-procedure "PeriodGetAll".
subscribe to "LoadPeriods" anywhere run-procedure "PeriodLoad".
subscribe to "NewPeriod" anywhere run-procedure "PeriodNew".
subscribe to "OpenPeriod" anywhere run-procedure "PeriodOpen".

/* region procedures */
subscribe to "RegionChanged" anywhere.
subscribe to "DeleteRegion" anywhere run-procedure "RegionDelete".
subscribe to "GetRegion" anywhere run-procedure "RegionGet".
subscribe to "GetRegions" anywhere run-procedure "RegionGetAll".
subscribe to "LoadRegions" anywhere run-procedure "RegionLoad".
subscribe to "ModifyRegion" anywhere run-procedure "RegionModify".
subscribe to "NewRegion" anywhere run-procedure "RegionNew".

/* state procedures */
subscribe to "StateChanged" anywhere.
subscribe to "ActivateState" anywhere run-procedure "StateActivate".
subscribe to "DeactivateState" anywhere run-procedure "StateDeactivate".
subscribe to "DeleteState" anywhere run-procedure "StateDelete".
subscribe to "GetState" anywhere run-procedure "StateGet".
subscribe to "GetStates" anywhere run-procedure "StateGetAll".
subscribe to "LoadStates" anywhere run-procedure "StateLoad".
subscribe to "ModifyState" anywhere run-procedure "StateModify".

/* state event procedures */
subscribe to "StateEventChanged" anywhere run-procedure "EventChangedState".
subscribe to "DeleteStateEvent" anywhere run-procedure "StateEventDelete".
subscribe to "GetStateEvent" anywhere run-procedure "StateEventGet".
subscribe to "GetStateEvents" anywhere run-procedure "StateEventGetAll".
subscribe to "LoadStateEvent" anywhere run-procedure "StateLoad".
subscribe to "NewStateEvent" anywhere run-procedure "StateEventNew".

/* sycode procedures */
subscribe to "SysCodeChanged" anywhere.
subscribe to "DeleteSysCode" anywhere run-procedure "SysCodeDelete".
subscribe to "GetSysCodes" anywhere run-procedure "SysCodeGet".
subscribe to "GetSysCodeDesc" anywhere run-procedure "SysCodeGetDesc".
subscribe to "GetSysCodeList" anywhere run-procedure "SysCodeGetList".
subscribe to "LoadSysCodes" anywhere run-procedure "SysCodeLoad".
subscribe to "ModifySysCode" anywhere run-procedure "SysCodeModify".
subscribe to "NewSysCode" anywhere run-procedure "SysCodeNew".

/* system ini procedures */
subscribe to "GetSysIni" anywhere run-procedure "SysIniGet".
subscribe to "GetSystemParameter" anywhere run-procedure "SysIniGetDesc".
subscribe to "LoadSysIni" anywhere run-procedure "SysIniLoad".

/* sysprop procedures */
subscribe to "SysPropChanged" anywhere.
subscribe to "DeleteSysProp" anywhere run-procedure "SysPropDelete".
subscribe to "GetSysProps" anywhere run-procedure "SysPropGetAll".
subscribe to "GetSysPropDesc" anywhere run-procedure "SysPropGetDesc".
subscribe to "GetSysPropList" anywhere run-procedure "SysPropGetList".
subscribe to "GetSysPropListMinusID" anywhere run-procedure "SysPropGetListMinusID".
subscribe to "LoadSysProps" anywhere run-procedure "SysPropLoad".
subscribe to "ModifySysProp" anywhere run-procedure "SysPropModify".
subscribe to "NewSysProp" anywhere run-procedure "SysPropNew".

/* sysuser procedures */
subscribe to "GetSysUser" anywhere run-procedure "SysUserGet".
subscribe to "GetSysUsers" anywhere run-procedure "SysUserGetAll".
subscribe to "GetSysUserEmail" anywhere run-procedure "SysUserGetEmail".
subscribe to "GetSysUserName" anywhere run-procedure "SysUserGetName".
subscribe to "GetSysUserRole" anywhere run-procedure "SysUserGetRole".
subscribe to "LoadSysUsers" anywhere run-procedure "SysUserLoad".

/* vendor procedures */
subscribe to "GetVendors" anywhere run-procedure "VendorGetAll".
subscribe to "LoadVendors" anywhere run-procedure "VendorLoad".

/* the user filter to decide which agents to show */
subscribe to "GetUserFilter" anywhere run-procedure "UserFilterGet".
subscribe to "GetUserFilterManager" anywhere run-procedure "UserFilterGetManager".
subscribe to "GetUserFilterState" anywhere run-procedure "UserFilterGetState".

/* questions */
subscribe to "IsAdmin" anywhere.
subscribe to "IsAlertUserDefined" anywhere.
subscribe to "IsPeriodOpen" anywhere.

/* Called by esbdata.p/NewStompMessage when another user changes 
   something we're interested in (as specified in param to lib/appstart.i) */
subscribe to "ExternalEsbMessage" anywhere.

/* Used to indicate when the last time the data was synced */
subscribe to "GetLastSync" anywhere.

/* Published by lib/appstart.i when the user is idle */
subscribe to "TIMER" anywhere.

/* Published by lib/appstart.i just before quitting; used for testing */
subscribe to "QUIT" anywhere.

/* Published by sys/about.w to allow trouble-shooting of data contents */
subscribe to "DataDump" anywhere.

&if defined(noLoad) = 0 &then
  &if defined(excludeAgent) = 0 &then
  std-lo = true.
  publish "SetSplashStatus".
  publish "GetLoadAgents" (output std-lo).
  if std-lo
   then publish "LoadAgents".
  &endif

  &if defined(excludeRegion) = 0 &then
  std-lo = true.
  publish "SetSplashStatus".
  publish "GetLoadRegions" (output std-lo).
  if std-lo
   then publish "LoadRegions".
  &endif

  &if defined(excludeState) = 0 &then
  std-lo = true.
  publish "SetSplashStatus".
  publish "GetLoadStates" (output std-lo).
  if std-lo
   then publish "LoadStates".
  &endif

  &if defined(excludeSysCode) = 0 &then
  std-lo = true.
  publish "SetSplashStatus".
  publish "GetLoadSysCodes" (output std-lo).
  if std-lo
   then publish "LoadSysCodes".
  &endif

  &if defined(excludeSysProp) = 0 &then
  std-lo = true.
  publish "SetSplashStatus".
  publish "GetLoadSysProps" (output std-lo).
  if std-lo 
   then publish "LoadSysProps".
  &endif

  &if defined(excludeCounty) = 0 &then
  std-lo = true.
  publish "SetSplashStatus".
  publish "GetLoadCounties" (output std-lo).
  if std-lo 
   then publish "LoadCounties".
  &endif

  &if defined(excludePeriod) = 0 &then
  std-lo = true.
  publish "SetSplashStatus".
  publish "GetLoadPeriods" (output std-lo).
  if std-lo 
   then publish "LoadPeriods".
  &endif

  std-lo = true.
  publish "SetSplashStatus".
  if std-lo 
   then publish "LoadSysUsers".

  std-lo = true.
  publish "SetSplashStatus".
  if std-lo 
   then publish "LoadSysIni".
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&if DEFINED(excludeAccounts) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AccountGetAll Procedure 
PROCEDURE AccountGetAll :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAccountNbr as character no-undo.
  define output parameter table for tempaccount.

  define buffer account for account.

  if not tLoadedAccounts
   then publish "LoadAccounts".

  empty temp-table tempaccount.
  for each account no-lock:
    
    if pAccountNbr > "" and account.acct <> pAccountNbr
     then next.
     
    create tempaccount.
    buffer-copy account to tempaccount.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AccountLoad Procedure 
PROCEDURE AccountLoad :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer account for account.
  
  empty temp-table account.

  run server/getaccounts.p (input  "",
                            output table account,
                            output tLoadedAccounts,
                            output std-ch).
  
  if not tLoadedAccounts
   then 
    do: std-lo = false.
        publish "GetAppDebug" (output std-lo).
        if std-lo 
         then message "LoadAccounts failed: " + std-ch view-as alert-box warning.
    end.
   else
    for each account exclusive-lock:
      account.stateName = getStateName(account.state).
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeAgent) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentChanged Include 
PROCEDURE AgentChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  refreshAgent(pAgentID).
  publish "AgentDataChanged".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentClose Include 
PROCEDURE AgentClose :
/*------------------------------------------------------------------------------
@description Closes the agent data server procedure
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  
  define buffer openAgent for openAgent.
  for first openAgent exclusive-lock
        where openAgent.agentID = pAgentID:
      
    if valid-handle(openAgent.agentHandle)
     then delete object openAgent.agentHandle.
    
    delete openAgent.
  end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentDelete Include 
PROCEDURE AgentDelete :
/*------------------------------------------------------------------------------
@description Delete an agent
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  define buffer agent for agent.
  
  run server/deleteagent.p (input pAgentID,
                            output pSuccess,
                            output std-ch).

  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    for first agent exclusive-lock
        where agent.agentID = pAgentID:
      
      publish "CloseAgent" (agent.agentID).
      delete agent.
      publish "AgentDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentGet Include 
PROCEDURE AgentGet :
/*------------------------------------------------------------------------------
@description Gets an agent
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define output parameter table for tempagent.
  define buffer tempagent for tempagent.

  empty temp-table tempagent.
  publish "GetAgents" (output table tempagent).

  for each tempagent exclusive-lock
     where tempagent.agentID <> pAgentID:
    
    delete tempagent.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentGetAll Include 
PROCEDURE AgentGetAll :
/*------------------------------------------------------------------------------
@description Gets the agents
------------------------------------------------------------------------------*/
  define output parameter table for tempagent.
  define variable tState as character no-undo initial "".
  define variable tManager as character no-undo initial "".
  define buffer agent for agent.

  if not tLoadedAgents
   then publish "LoadAgents".

  publish "GetUserFilter" (output tState, output tManager).

  empty temp-table tempagent.
  for each agent no-lock:
    if tState > "" or tManager > ""
     then
      do:
        if lookup(agent.stateID, tState) > 0 or lookup(agent.manager, tManager) > 0
         then
          do:
            create tempagent.
            buffer-copy agent to tempagent.
          end.
      end.
     else
      do:
        create tempagent.
        buffer-copy agent to tempagent.
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentLoad Include 
PROCEDURE AgentLoad :
/*------------------------------------------------------------------------------
@description Loads the Agents
------------------------------------------------------------------------------*/
  empty temp-table agent.
  run server/getagents.p ("",
                          output table agent,
                          output tLoadedAgents,
                          output std-ch).

  if not tLoadedAgents
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadAgents failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentNew Include 
PROCEDURE AgentNew :
/*------------------------------------------------------------------------------
@description Creates a new agent
------------------------------------------------------------------------------*/
  define input parameter table for tempagent.
  define output parameter pHandle as handle no-undo.
  
  define variable tNewAgentID as character no-undo.
  define buffer tempagent for tempagent.
  
  run server/newagent.p (input table tempagent,
                         output tNewAgentID,
                         output std-lo,
                         output std-ch).
                         
  if not std-lo
   then message std-ch view-as alert-box error.
   else
    do:
      for first tempagent no-lock:
        create agent.
        buffer-copy tempagent to agent.
        agent.agentID = tNewAgentID.
      end.
      publish "OpenAgent" (tNewAgentID, output pHandle).
      publish "AgentDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentOpen Include 
PROCEDURE AgentOpen :
/*------------------------------------------------------------------------------
@description Loads the agent into the agentdatasrv.p file
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define output parameter pHandle as handle no-undo.
  
  define buffer openAgent for openAgent.
  
  if can-find(first openAgent where agentID = pAgentID)
   then 
    for first openAgent no-lock
        where openAgent.agentID = pAgentID:
        
      pHandle = openAgent.agentHandle.
    end.
   else 
    do:
      run agentdatasrv.p persistent set pHandle (pAgentID).
      create openAgent.
      assign
        openAgent.agentID = pAgentID
        openAgent.agentHandle = pHandle
        .
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif


&if defined(excludeCounty) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CountyChanged Include 
PROCEDURE CountyChanged :
/*------------------------------------------------------------------------------
@description Reloads a county
------------------------------------------------------------------------------*/
  define input parameter pStateID as character no-undo.
  define input parameter pCountyID as character no-undo.
  refreshCounty(pStateID, pCountyID).
  publish "CountyDataChanged".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CountyDelete Include 
PROCEDURE CountyDelete :
/*------------------------------------------------------------------------------
@description Delete a county
------------------------------------------------------------------------------*/
  define input parameter pStateID as character no-undo.
  define input parameter pCountyID as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  define buffer county for county.
  
  run server/deletecounty.p (input pStateID,
                             input pCountyID,
                             output pSuccess,
                             output std-ch).

  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    for first county exclusive-lock
        where county.stateID = pStateID
          and county.countyID = pCountyID:
      
      delete county.
      publish "CountyDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CountyGetAll Include 
PROCEDURE CountyGetAll :
/*------------------------------------------------------------------------------
@description Gets the counties
------------------------------------------------------------------------------*/
  define output parameter table for county.

  if not tLoadedCounties 
   then publish "LoadCounties".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CountyLoad Include 
PROCEDURE CountyLoad :
/*------------------------------------------------------------------------------
@description Loads the counties
------------------------------------------------------------------------------*/  
  empty temp-table county.  
  run server/getcounties.p ("", /* stateID, blank=All */
                            "", /* countyID, blank=All */
                            output table county,
                            output tLoadedCounties,
                            output std-ch).
  if not tLoadedCounties
   then 
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadCounties failed: " + std-ch view-as alert-box warning.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CountyModify Include 
PROCEDURE CountyModify :
/*------------------------------------------------------------------------------
@description Modifies a county
------------------------------------------------------------------------------*/
  define input parameter pStateID as character no-undo.
  define input parameter pCountyID as character no-undo.
  define input parameter pDescription as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  define buffer county for county.
  
  run server/modifycounty.p (input pStateID,
                             input pCountyID,
                             input pDescription,
                             output pSuccess,
                             output std-ch).

  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    for first county exclusive-lock
        where county.stateID = pStateID
          and county.countyID = pCountyID:
      
      county.description = pDescription.
      publish "CountyDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CountyNew Include 
PROCEDURE CountyNew :
/*------------------------------------------------------------------------------
@description Creates a new county
------------------------------------------------------------------------------*/
  define input parameter pStateID as character no-undo.
  define input parameter pCountyID as character no-undo.
  define input parameter pDescription as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.

  run server/newcounty.p (input pStateID,
                          input pCountyID,
                          input pDescription,
                          output pSuccess,
                          output std-ch).

  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    do:
      create county.
      assign
        county.stateID = pStateID
        county.countyID = pCountyID
        county.description = pDescription
        .
      publish "CountyDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DataDump Include 
PROCEDURE DataDump :
/*------------------------------------------------------------------------------
@description Dump the tables into files
------------------------------------------------------------------------------*/
  define variable tPrefix as character no-undo.
  publish "GetAppCode" (output tPrefix).
  tPrefix = tPrefix + "_".

  publish "DeleteTempFile" ("DataAgent", output std-lo).
  publish "DeleteTempFile" ("DataCounty", output std-lo).
  publish "DeleteTempFile" ("DataEsbMsg", output std-lo).
  publish "DeleteTempFile" ("DataPeriod", output std-lo).
  publish "DeleteTempFile" ("DataState", output std-lo).
  publish "DeleteTempFile" ("DataRegion", output std-lo).
  publish "DeleteTempFile" ("DataSysCode", output std-lo).
  publish "DeleteTempFile" ("DataSysIni", output std-lo).
  publish "DeleteTempFile" ("DataSysProp", output std-lo).
  publish "DeleteTempFile" ("DataSysUser", output std-lo).
  
  publish "DeleteTempFile" ("DataTempAgent", output std-lo).
  publish "DeleteTempFile" ("DataTempCounty", output std-lo).
  publish "DeleteTempFile" ("DataTempPeriod", output std-lo).
  publish "DeleteTempFile" ("DataTempState", output std-lo).
  publish "DeleteTempFile" ("DataTempRegion", output std-lo).
  publish "DeleteTempFile" ("DataTempSysCode", output std-lo).
  publish "DeleteTempFile" ("DataTempSysIni", output std-lo).
  publish "DeleteTempFile" ("DataTempSysProp", output std-lo).
  publish "DeleteTempFile" ("DataTempSysUser", output std-lo).
  
  /* Active temp-tables */
  temp-table agent:write-xml("file", tPrefix + "Agent.xml").
  publish "AddTempFile" ("DataAgent", tPrefix + "Agent.xml").
  
  temp-table county:write-xml("file", tPrefix + "County.xml").
  publish "AddTempFile" ("DataCounty", tPrefix + "County.xml").
  
  temp-table esbmsg:write-xml("file", tPrefix + "EsbMsg.xml").
  publish "AddTempFile" ("DataEsbMsg", tPrefix + "EsbMsg.xml").
  
  temp-table period:write-xml("file", tPrefix + "Period.xml").
  publish "AddTempFile" ("DataPeriod", tPrefix + "Period.xml").
  
  temp-table state:write-xml("file", tPrefix + "State.xml").
  publish "AddTempFile" ("DataState", tPrefix + "State.xml").
  
  temp-table region:write-xml("file", tPrefix + "Region.xml").
  publish "AddTempFile" ("DataState", tPrefix + "Region.xml").
  
  temp-table syscode:write-xml("file", tPrefix + "SysCode.xml").
  publish "AddTempFile" ("DataState", tPrefix + "SysCode.xml").
  
  temp-table ini:write-xml("file", tPrefix + "SysIni.xml").
  publish "AddTempFile" ("DataState", tPrefix + "SysIni.xml").
  
  temp-table sysprop:write-xml("file", tPrefix + "SysProp.xml").
  publish "AddTempFile" ("DataState", tPrefix + "SysProp.xml").
  
  temp-table sysuser:write-xml("file", tPrefix + "SysUser.xml").
  publish "AddTempFile" ("DataState", tPrefix + "SysUser.xml").
  
  /* "Temp" temp-tables */
  temp-table tempagent:write-xml("file", tPrefix + "TempAgent.xml").
  publish "AddTempFile" ("DataAgent", tPrefix + "TempAgent.xml").
  
  temp-table tempcounty:write-xml("file", tPrefix + "TempCounty.xml").
  publish "AddTempFile" ("DataCounty", tPrefix + "TempCounty.xml").
  
  temp-table tempperiod:write-xml("file", tPrefix + "TempPeriod.xml").
  publish "AddTempFile" ("DataPeriod", tPrefix + "TempPeriod.xml").
  
  temp-table tempstate:write-xml("file", tPrefix + "TempState.xml").
  publish "AddTempFile" ("DataState", tPrefix + "TempState.xml").
  
  temp-table tempregion:write-xml("file", tPrefix + "TempRegion.xml").
  publish "AddTempFile" ("DataState", tPrefix + "TempRegion.xml").
  
  temp-table tempsyscode:write-xml("file", tPrefix + "TempSysCode.xml").
  publish "AddTempFile" ("DataState", tPrefix + "TempSysCode.xml").
  
  temp-table tempsysprop:write-xml("file", tPrefix + "TempSysProp.xml").
  publish "AddTempFile" ("DataState", tPrefix + "TempSysProp.xml").
  
  temp-table tempsysuser:write-xml("file", tPrefix + "TempSysUser.xml").
  publish "AddTempFile" ("DataState", tPrefix + "TempSysUser.xml").

  /* run the procedure to continue the DataDump functions if necessary */
  run DataDumpEXT in this-procedure no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&if defined(excludeESB) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExternalEsbMessage Procedure 
PROCEDURE ExternalEsbMessage :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey as character no-undo.
  
  tLastEsbMsgSeq = tLastEsbMsgSeq + 1.
  
  create esbmsg.
  assign
    esbmsg.seq = tLastEsbMsgSeq
    esbmsg.rcvd = now
    esbmsg.entity = pEntity
    esbmsg.action = pAction
    esbmsg.keyID = pKey
    .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLastSync Procedure 
PROCEDURE GetLastSync :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter pSync as datetime.
  pSync = tLastSync.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsAdmin Include 
PROCEDURE IsAdmin :
/*------------------------------------------------------------------------------
@description Checks if the user is an admin
------------------------------------------------------------------------------*/
  define output parameter pIsAdmin as logical no-undo.
  define buffer sysuser for sysuser.

  /* get the current user */
  std-ch = "".
  publish "GetCredentialsID" (output std-ch).

  pIsAdmin = false.
  for first sysuser no-lock
      where sysuser.uid = std-ch:
    
    pIsAdmin = can-do(sysuser.role, "Administrator").
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsAlertUserDefined Include 
PROCEDURE IsAlertUserDefined :
/*------------------------------------------------------------------------------
@description Checks if the alert is user defined
------------------------------------------------------------------------------*/
  define input parameter pAlert as character no-undo.
  define output parameter pUserDefined as logical no-undo initial false.
  define buffer sysprop for sysprop.
  
  if not tLoadedSysProps 
   then publish "LoadSysProps".
   
  for each sysprop no-lock
     where sysprop.objAction = "Alert"
       and sysprop.objProperty = "Threshold"
        by sysprop.objID = pAlert:
     
    pUserDefined = (sysprop.objValue = "User Defined").
  end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsPeriodOpen Include 
PROCEDURE IsPeriodOpen :
/*------------------------------------------------------------------------------
@description Checks if the period is open or not
------------------------------------------------------------------------------*/
  define input parameter pMonth as int.
  define input parameter pYear as int.
  define output parameter pOpen as logical init false.
  define buffer period for period.

  if not tLoadedPeriods 
   then publish "LoadPeriods".

  for first period no-lock
      where period.periodMonth = pMonth
        and period.periodYear = pYear:
    
    case {&period}:
     when "Batch" then pOpen = (period.active = yes).
     when "Accounting" then pOpen = (period.activeAccounting = yes).
    end case.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&if DEFINED(excludeOffices) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OfficeGetAll Procedure 
PROCEDURE OfficeGetAll :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pOfficeID as integer no-undo.
  define output parameter table for tempoffice.

  define buffer office for office.

  if not tLoadedOffices
   then publish "LoadOffices".

  empty temp-table tempoffice.
  for each office no-lock:
    
    if pOfficeID > 0 and office.officeID <> pOfficeID
     then next.
     
    create tempoffice.
    buffer-copy office to tempoffice.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OfficeLoad Procedure 
PROCEDURE OfficeLoad :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer office for office.
  
  empty temp-table office.

  run server/getoffices.p (output table office,
                           output tLoadedOffices,
                           output std-ch).
  
  if not tLoadedOffices
   then 
    do: std-lo = false.
        publish "GetAppDebug" (output std-lo).
        if std-lo 
         then message "LoadedOffices failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludePeriod) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PeriodChanged Include 
PROCEDURE PeriodChanged :
/*------------------------------------------------------------------------------
@description Reloads a period
------------------------------------------------------------------------------*/
  define input parameter pPeriodID as integer no-undo.
  refreshPeriod(pPeriodID).
  publish "PeriodDataChanged".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PeriodClose Include 
PROCEDURE PeriodClose :
/*------------------------------------------------------------------------------
@description Closes a period
------------------------------------------------------------------------------*/
  define input parameter pPeriodID as integer no-undo.
  define buffer period for period.

  std-lo = false.
  case {&period}:
   when "Batch" then run server/closeperiod.p (pPeriodID,
                                               output std-lo,
                                               output std-ch).
   when "Accounting" then run server/closeaccountingperiod.p (pPeriodID,
                                                              output std-lo,
                                                              output std-ch).
  end case.
  if not std-lo
   then message std-ch view-as alert-box error.
   else
    for first period exclusive-lock
        where period.periodID = pPeriodID:
      
      case {&period}:
       when "Batch" then period.active = no.
       when "Accounting" then period.activeAccounting = no.
      end case.
      publish "PeriodDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PeriodDelete Include 
PROCEDURE PeriodDelete :
/*------------------------------------------------------------------------------
@description Deletes a period
------------------------------------------------------------------------------*/
  define input parameter pPeriodID as integer no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  define buffer period for period.

  run server/deleteperiod.p (input pPeriodID,
                             output pSuccess,
                             output std-ch).

  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    for first period exclusive-lock
        where period.periodID = pPeriodID:
      
      delete period.
      publish "PeriodDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PeriodGet Include 
PROCEDURE PeriodGet :
/*------------------------------------------------------------------------------
@description Get a period given the month and year
------------------------------------------------------------------------------*/
  define input param pMonth as integer no-undo.
  define input param pYear as integer no-undo.
  define output param pPeriodID as integer no-undo initial 0.
  define buffer period for period.
  
  publish "GetPeriods" (output table period).

  for first period no-lock
      where period.periodMonth = pMonth
        and period.periodYear = pYear:

    pPeriodID = period.periodID.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PeriodGetAll Include 
PROCEDURE PeriodGetAll :
/*------------------------------------------------------------------------------
@description Gets the periods
------------------------------------------------------------------------------*/
  define output parameter table for period.

  if not tLoadedPeriods
   then publish "LoadPeriods".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PeriodLoad Include 
PROCEDURE PeriodLoad :
/*------------------------------------------------------------------------------
@description Loads the Periods
------------------------------------------------------------------------------*/
  empty temp-table period.
  case {&period}:
   when "Batch" then run server/getperiods.p (0,
                                              output table period,
                                              output tLoadedPeriods,
                                              output std-ch).
   when "Accounting" then run server/getaccountingperiods.p (0,
                                                             output table period,
                                                             output tLoadedPeriods,
                                                             output std-ch).
  end case.

  if not tLoadedPeriods
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadPeriods failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PeriodNew Include 
PROCEDURE PeriodNew :
/*------------------------------------------------------------------------------
@description Creates a new period
------------------------------------------------------------------------------*/
  define output parameter pSuccess as logical no-undo initial false.
  define buffer tempperiod for tempperiod.
  
  empty temp-table tempperiod.
  run server/newperiod.p (output table tempperiod,
                          output pSuccess,
                          output std-ch).

  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    for first tempperiod no-lock:
      create period.
      buffer-copy tempperiod to period.
      publish "PeriodDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PeriodOpen Include 
PROCEDURE PeriodOpen :
/*------------------------------------------------------------------------------
@description Opens a period
------------------------------------------------------------------------------*/
  define input parameter pPeriodID as integer no-undo.
  define buffer period for period.

  std-lo = false.
  case {&period}:
   when "Batch" then run server/openperiod.p (pPeriodID,
                                               output std-lo,
                                               output std-ch).
   when "Accounting" then run server/openaccountingperiod.p (pPeriodID,
                                                              output std-lo,
                                                              output std-ch).
  end case.
  if not std-lo
   then message std-ch view-as alert-box error.
   else
    for first period exclusive-lock
        where period.periodID = pPeriodID:
      
      case {&period}:
       when "Batch" then period.active = yes.
       when "Accounting" then period.activeAccounting = yes.
      end case.
      publish "PeriodDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeQuit) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE QUIT Procedure 
PROCEDURE QUIT :
/*------------------------------------------------------------------------------
  Purpose:     Called just prior to terminating the application
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* for testing */

  /* run the procedure to continue the QUIT functions if necessary */
  run QUITEXT in this-procedure no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeRegion) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RegionChanged Include 
PROCEDURE RegionChanged :
/*------------------------------------------------------------------------------
@description Reloads a region
------------------------------------------------------------------------------*/
  define input parameter pRegionID as character no-undo.
  refreshRegion(pRegionID).
  publish "RegionDataChanged".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RegionDelete Include 
PROCEDURE RegionDelete :
/*------------------------------------------------------------------------------
@description Deletes a region
------------------------------------------------------------------------------*/
  define input parameter pRegionID as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  define buffer region for region.
  
  run server/deleteregion.p (input pRegionID,
                             output pSuccess,
                             output std-ch).

  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    do:
      for first region exclusive-lock
          where region.regionID = pRegionID:
        
        delete region.
      end.
      for first sysprop exclusive-lock
          where sysprop.appCode = "AMD"
            and sysprop.objAction = "Region"
            and sysprop.objProperty = "Category"
            and sysprop.objID = pRegionID:
        
        delete sysprop.
      end.
      publish "RegionDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RegionGet Include 
PROCEDURE RegionGet :
/*------------------------------------------------------------------------------
@description Gets a region for the state
------------------------------------------------------------------------------*/
  define input param pStateID as character no-undo.
  define output param pRegionID as character no-undo initial "".

  publish "GetRegions" (output table region).

  for first region no-lock
      where region.stateID = pStateID:

    pRegionID = region.regionID.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RegionGetAll Include 
PROCEDURE RegionGetAll :
/*------------------------------------------------------------------------------
@description Gets the regions
------------------------------------------------------------------------------*/
  define output parameter table for region.

  if not tLoadedRegions 
   then publish "LoadRegions".
  
  std-in = 1.
  for each region exclusive-lock break by region.regionID:
    publish "GetSysPropDesc" ("AMD", "Region", "Category", region.regionID, output region.regionDesc).
    region.stateDesc = getStateName(region.stateID).
    region.seq = std-in.
    
    if last-of(region.regionID)
     then std-in = 1.
     else std-in = std-in + 1.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RegionLoad Include 
PROCEDURE RegionLoad :
/*------------------------------------------------------------------------------
@description Loads the Regions
------------------------------------------------------------------------------*/
  empty temp-table region.
  run server/getregions.p ("",
                           output table region,
                           output tLoadedRegions,
                           output std-ch).

  if not tLoadedRegions
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadRegions failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RegionModify Include 
PROCEDURE RegionModify :
/*------------------------------------------------------------------------------
@description Modifies a region
------------------------------------------------------------------------------*/
  define input parameter pRegionID as character no-undo.
  define input parameter pStateList as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  define buffer region for region.
  
  run server/modifyregion.p (input pRegionID,
                             input pStateList,
                             output pSuccess,
                             output std-ch).

  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    do:
      for each region exclusive-lock
         where region.regionID = pRegionID:
        
        delete region.
      end.
      do std-in = 1 to num-entries(pStateList):
        create region.
        assign
          region.regionID = pRegionID
          region.stateID = entry(std-in, pStateList)
          .
      end.
      publish "RegionDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RegionNew Include 
PROCEDURE RegionNew :
/*------------------------------------------------------------------------------
@description Creates a new region
------------------------------------------------------------------------------*/
  define input parameter pRegionID as character no-undo.
  define input parameter pRegionDesc as character no-undo.
  define input parameter pStateList as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  define buffer region for region.
  
  run server/newregion.p (input pRegionID,
                          input pRegionDesc,
                          input pStateList,
                          output pSuccess,
                          output std-ch).

  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    do:
      do std-in = 1 to num-entries(pStateList):
        create region.
        assign
          region.regionID = pRegionID
          region.stateID = entry(std-in, pStateList)
          .
      end.
      publish "RegionDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeState) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateChanged Include 
PROCEDURE StateChanged :
/*------------------------------------------------------------------------------
@description Reloads a state
------------------------------------------------------------------------------*/
  define input parameter pStateID as character no-undo.
  refreshState(pStateID).
  publish "StateDataChanged".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateActivate Procedure 
PROCEDURE StateActivate :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pStateID as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.

  run server/activatestate.p(input pStateID,
                             output pSuccess,
                             output std-ch).

 if not pSuccess
  then message std-ch view-as alert-box error.
  else
   for first state exclusive-lock
       where state.stateID = pStateID:
     
     state.active = true.
   end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateDeactivate Procedure 
PROCEDURE StateDeactivate :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pStateID as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.

  run server/deactivatestate.p(input pStateID,
                               output pSuccess,
                               output std-ch).

 if not pSuccess
  then message std-ch view-as alert-box error.
  else
   for first state exclusive-lock
       where state.stateID = pStateID:
     
     state.active = false.
   end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateDelete Include 
PROCEDURE StateDelete :
/*------------------------------------------------------------------------------
@description Deletes a state
------------------------------------------------------------------------------*/
  define input parameter pStateID as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  define buffer state for state.
  
  run server/deletestate.p (input pStateID,
                            output pSuccess,
                            output std-ch).

  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    for first state exclusive-lock
        where state.stateID = pStateID:
      
      delete state.
      publish "StateDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateGet Include 
PROCEDURE StateGet :
/*------------------------------------------------------------------------------
@description Gets an agent
------------------------------------------------------------------------------*/
  define input parameter pStateID as character no-undo.
  define output parameter table for tempstate.
  define buffer tempstate for tempstate.

  empty temp-table tempstate.
  publish "GetStates" (output table tempstate).

  for each tempstate exclusive-lock
     where tempstate.stateID <> pStateID:
    
    delete tempstate.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateGetAll Include 
PROCEDURE StateGetAll :
/*------------------------------------------------------------------------------
@description Gets the states
------------------------------------------------------------------------------*/
  define output parameter table for state.

  if not tLoadedStates 
   then publish "LoadStates".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateLoad Include 
PROCEDURE StateLoad :
/*------------------------------------------------------------------------------
@description Loads the States
------------------------------------------------------------------------------*/
  empty temp-table state.
  run server/getstates.p (input "",
                          output table state,
                          output tLoadedStates,
                          output std-ch).

  if not tLoadedStates
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadStates failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateModify Include 
PROCEDURE StateModify :
/*------------------------------------------------------------------------------
@description Modifies a county
------------------------------------------------------------------------------*/
  define input parameter table for tempstate.
  define output parameter pSuccess as logical no-undo initial false.
  define buffer tempstate for tempstate.
  define buffer state for state.
  
  run server/modifystate.p (input-output table tempstate,
                            output pSuccess,
                            output std-ch).

  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    for first tempstate no-lock:
      for first state exclusive-lock
          where state.stateID = tempstate.stateID:
        
        buffer-copy tempstate to state.
      end.
      publish "StateDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeStateEvent) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateEventChanged Include 
PROCEDURE StateEventChanged :
/*------------------------------------------------------------------------------
@description Reloads a state
------------------------------------------------------------------------------*/
  define input parameter pStateID as character no-undo.
  refreshState(pStateID).
  publish "StateEventDataChanged".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateEventDelete Include 
PROCEDURE StateEventDelete :
/*------------------------------------------------------------------------------
@description Delete a state event
------------------------------------------------------------------------------*/
  define input parameter pStateID as character no-undo.
  define input parameter pSeq as integer no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  define output parameter pMsg as character no-undo.
  define buffer stateevent for stateevent.
  
  run server/deletestateevent.p (input pStateID,
                                 input pSeq,
                                 output pSuccess,
                                 output pMsg).
  
  if not pSuccess
   then message pMsg view-as alert-box info buttons ok.
   else
    for first stateevent exclusive-lock
        where stateevent.stateID = pStateID
          and stateevent.seq = pSeq:
      
      delete stateevent.
      publish "StateEventDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateEventGet Include 
PROCEDURE StateEventGet :
/*------------------------------------------------------------------------------
@description Get a state event
------------------------------------------------------------------------------*/
  define input parameter pStateID as character no-undo.
  define output parameter table for tempstateevent.
  define buffer tempstateevent for tempstateevent.

  empty temp-table tempstateevent.
  publish "GetStateEvents" (output table tempstateevent).

  for each tempstateevent exclusive-lock
     where tempstateevent.stateID <> pStateID:
    
    delete tempstateevent.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateEventGetAll Include 
PROCEDURE StateEventGetAll :
/*------------------------------------------------------------------------------
@description Get a state event
------------------------------------------------------------------------------*/
  define output parameter table for stateevent.

  if not tLoadedStates
   then publish "LoadStates".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StateEventNew Include 
PROCEDURE StateEventNew :
/*------------------------------------------------------------------------------
@description Create a new state event
------------------------------------------------------------------------------*/
  define input-output parameter table for tempstateevent.
  define variable pSuccess as logical no-undo initial false.
  
  run server/newstateevent.p (input-output table tempstateevent,
                              output pSuccess,
                              output std-ch).

  if not pSuccess 
   then message std-ch view-as alert-box info buttons ok.
   else
    for first tempstateevent no-lock:
      create stateevent.
      buffer-copy tempstateevent to stateevent.
      publish "StateEventDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeSysCode) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysCodeChanged Include 
PROCEDURE SysCodeChanged :
/*------------------------------------------------------------------------------
@description Reloads a code type
------------------------------------------------------------------------------*/
  define input parameter pCodeType as character no-undo.
  refreshSysCode(pCodeType).
  publish "SysCodeDataChanged".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysCodeDelete Include 
PROCEDURE SysCodeDelete :
/*------------------------------------------------------------------------------
@description Deletes a system code
------------------------------------------------------------------------------*/
  define input parameter pCodeType as character no-undo.
  define input parameter pCode as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  define buffer syscode for syscode.
  
  run server/deletesyscode.p (input pCodeType,
                              input pCode,
                              output pSuccess,
                              output std-ch).

  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    for first syscode exclusive-lock
        where syscode.codeType = pCodeType
          and syscode.code = pCode:
      
      delete syscode.
      publish "CodesDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysCodeGet Include 
PROCEDURE SysCodeGet :
/*------------------------------------------------------------------------------
@description Gets the system codes
------------------------------------------------------------------------------*/
  define input parameter pCodeType as character no-undo.
  define output parameter table for tempsyscode.

  if not can-find(first syscode where syscode.codeType = pCodeType)
   then
    do:
      empty temp-table tempsyscode.
      run server/getsyscodes.p (pCodeType,
                                output table tempsyscode,
                                output std-lo,
                                output std-ch).
      
      if not std-lo 
       then message "LoadSysCodes for " + pCodeType + " failed: " + std-ch view-as alert-box error.
       else
        for each tempsyscode:
          create syscode.
          buffer-copy tempsyscode to syscode.
        end.
    end.
  
  empty temp-table tempsyscode.
  for each syscode no-lock
     where syscode.codeType = pCodeType:
    
    create tempsyscode.
    buffer-copy syscode to tempsyscode.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysCodeGetDesc Include 
PROCEDURE SysCodeGetDesc :
/*------------------------------------------------------------------------------
@description Gets a system code description
------------------------------------------------------------------------------*/
  define input parameter pCodeType as character no-undo.
  define input parameter pCode as character no-undo.
  define output parameter pDesc as character no-undo.

  std-ch = "".
  publish "GetSysCodeList" (pCodeType, output std-ch).

  if std-ch > ""
   then
    do:
      if lookup(pCode,std-ch) = 0 
       then pDesc = "".
       else pDesc = entry(lookup(pCode,std-ch) - 1, std-ch).
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysCodeGetList Include 
PROCEDURE SysCodeGetList :
/*------------------------------------------------------------------------------
@description Gets a list of system codes
------------------------------------------------------------------------------*/
  define input parameter pCodeType as character no-undo.
  define output parameter pList as character no-undo.
  define buffer tempsyscode for tempsyscode.

  publish "GetSysCodes" (pCodeType, output table tempsyscode).

  pList = "".
  for each tempsyscode no-lock
     where tempsyscode.codeType = pCodeType
        by tempsyscode.description:
        
     pList = addDelimiter(pList,",") + tempsyscode.description + "," + tempsyscode.code.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysCodeLoad Include 
PROCEDURE SysCodeLoad :
/*------------------------------------------------------------------------------
@description Loads the System Codes
------------------------------------------------------------------------------*/
  define variable tCode as character no-undo.
  define buffer tempsyscode for tempsyscode.

  empty temp-table syscode.
  do std-in = 1 to num-entries({&syscode}):
    tCode = entry(std-in, {&syscode}).
    /* get the codes */
    empty temp-table tempsyscode.
    run server/getsyscodes.p (tCode,
                              output table tempsyscode,
                              output std-lo,
                              output std-ch).
  
    if std-lo 
     then
      for each tempsyscode:
        create syscode.
        buffer-copy tempsyscode to syscode.
      end.
     else
      do: 
        std-lo = false.
        publish "GetAppDebug" (output std-lo).
        if std-lo 
         then message "LoadSysCodes for " + tCode + " failed: " + std-ch view-as alert-box warning.
      end.
  end.
  tLoadedSysCodes = true. /* Always set to true as there may not be any codes to load at startup */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysCodeModify Include 
PROCEDURE SysCodeModify :
/*------------------------------------------------------------------------------
@description Modifies a syscode
------------------------------------------------------------------------------*/
  define input parameter pCodeType as character no-undo.
  define input parameter pCode as character no-undo.
  define input parameter pDescription as character no-undo.
  define input parameter pComments as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  
  define buffer syscode for syscode.
  
  define buffer tempSysCode for tempSysCode.
  empty temp-table tempSysCode.

  if not can-find( first syscode
                     where syscode.codeType = pCodeType
                       and syscode.code     = pCode)
    then
     return.
    
  create tempSysCode.
  assign
    tempSysCode.codetype     = pCodeType
    tempSysCode.code         = pCode
    tempSysCode.description  = pDescription 
    tempSysCode.comments     = pComments 
   .

  run server/modifysyscode.p (input table tempSysCode,
                              output pSuccess,
                              output std-ch).


  if not pSuccess
   then 
    message std-ch view-as alert-box error.
    
   else
    for first tempSysCode:
      for first syscode 
        where syscode.codeType = tempSysCode.codeType
          and syscode.code     = tempSysCode.code:
        
        buffer-copy tempSysCode to syscode.
      end.
      publish "CodesDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysCodeNew Include 
PROCEDURE SysCodeNew :
/*------------------------------------------------------------------------------
@description Creates a new syscode
------------------------------------------------------------------------------*/
  define input parameter pCodeType as character no-undo.
  define input parameter pCode as character no-undo.
  define input parameter pDescription as character no-undo.
  define input parameter pComments as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.

  define buffer syscode for syscode.
  define buffer tempSysCode for tempSysCode.
  empty temp-table tempSysCode.
  
  create tempSysCode.
  assign
    tempSysCode.codetype     = pCodeType
    tempSysCode.code         = pCode
    tempSysCode.description  = pDescription 
    tempSysCode.comments     = pComments 
   .
         
  
  run server/newsyscode.p (input table tempSysCode,
                           output pSuccess,
                           output std-ch).

  if not pSuccess
   then message std-ch view-as alert-box error.
   
   else
    do:
      find first tempSysCode no-error.
      if not available tempSysCode
       then
        return.

      create syscode.
      buffer-copy tempSysCode to syscode .
      
      publish "CodesDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeSysIni) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysIniGet Include 
PROCEDURE SysIniGet :
/*------------------------------------------------------------------------------
@description Gets the System INI records
------------------------------------------------------------------------------*/
  define output parameter table for ini.

  if not tLoadedSysIni 
   then publish "LoadSysIni".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysIniGetDesc Include 
PROCEDURE SysIniGetDesc :
/*------------------------------------------------------------------------------
@description Gets a Value for a Key
------------------------------------------------------------------------------*/
  define input parameter pKey as character no-undo.
  define output parameter pValue as character no-undo.
  define buffer ini for ini.

  publish "GetSysIni" (output table ini).

  pValue = "".
  for first ini
      where ini.id = pKey:
    
    pValue = ini.val.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysIniLoad Procedure 
PROCEDURE SysIniLoad :
/*------------------------------------------------------------------------------
@description Loads the System INI
------------------------------------------------------------------------------*/
  empty temp-table ini.
  run server/getini.p (output table ini,
                       output tLoadedSysIni,
                       output std-ch
                       ).
  if not tLoadedSysIni
   then 
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadSysIni failed: " + std-ch view-as alert-box warning.
      return.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeSysProp) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysPropChanged Include 
PROCEDURE SysPropChanged :
/*------------------------------------------------------------------------------
@description Reloads a property
------------------------------------------------------------------------------*/
  define input parameter pAppCode as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pProperty as character no-undo.
  refreshSysProp(pAppCode, pAction, pProperty).
  publish "SysPropDataChanged".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysPropDelete Include 
PROCEDURE SysPropDelete :
/*------------------------------------------------------------------------------
@description Deletes a system property
------------------------------------------------------------------------------*/
  define input parameter pAppCode as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pProperty as character no-undo.
  define input parameter pID as character no-undo.
  define input parameter pValue as character no-undo.
  define output parameter pSuccess as logical no-undo initial false.
  define buffer sysprop for sysprop.
  
  run server/deletesysprop.p (input pAppCode,
                              input pAction,
                              input pProperty,
                              input pID,
                              input pValue,
                              output pSuccess,
                              output std-ch).
                         
  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    for first sysprop exclusive-lock
        where sysprop.appCode = pAppCode
          and sysprop.objAction = pAction
          and sysprop.objProperty = pProperty
          and sysprop.objID = pID
          and sysprop.objValue = pValue:
      
      delete sysprop.
      publish "SysPropDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysPropGet Include 
PROCEDURE SysPropGetAll :
/*------------------------------------------------------------------------------
@description Gets the system properties
------------------------------------------------------------------------------*/
  define output parameter table for sysprop.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysPropGetDesc Include 
PROCEDURE SysPropGetDesc :
/*------------------------------------------------------------------------------
@description Gets a system property description
------------------------------------------------------------------------------*/
  define input parameter pAppCode as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pProperty as character no-undo.
  define input parameter pID as character no-undo.
  define output parameter pDesc as character no-undo.

  std-ch = "".
  publish "GetSysPropList" (pAppCode, pAction, pProperty, output std-ch).

  if std-ch > "" and pID > ""
   then
    do:
      if lookup(pID,std-ch) = 0 
       then pDesc = "".
       else pDesc = entry(lookup(pID,std-ch) - 1, std-ch).
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysPropGetList Include 
PROCEDURE SysPropGetList :
/*------------------------------------------------------------------------------
@description Gets a list of system properties
------------------------------------------------------------------------------*/
  define input parameter pAppCode as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pProperty as character no-undo.
  define output parameter pList as character no-undo.
  define buffer tempsysprop for tempsysprop.
  define buffer sysprop for sysprop.

  if pAppCode = ""
   then publish "GetAppCode" (output pAppCode).
  
  if not can-find(first sysprop where appCode = pAppCode and objAction = pAction and objProperty = pProperty)
   then
    do:
      empty temp-table tempsysprop.
      run server/getsysprops.p (input pAppCode,
                                input pAction,
                                input pProperty,
                                input "",
                                input "",
                                output table tempsysprop,
                                output std-lo,
                                output std-ch
                                ).

      if not std-lo
       then message "LoadSysProps (" + pAppCode + ", " + pAction + ", " + pProperty + ") failed: " + std-ch view-as alert-box error.
       else
        for each tempsysprop:
          create sysprop.
          buffer-copy tempsysprop to sysprop.
        end.
    end.

  pList = "".
  for each sysprop no-lock
     where sysprop.appCode = pAppCode
       and sysprop.objAction = pAction
       and sysprop.objProperty = pProperty
        by sysprop.objRef
        by sysprop.objID:
    
    pList = addDelimiter(pList,",") + sysprop.objValue + "," + sysprop.objID.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysPropGetListMinusID Procedure 
PROCEDURE SysPropGetListMinusID :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAppCode as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pProperty as character no-undo.
  define input parameter pMinusID as character no-undo.
  define output parameter pList as character no-undo.
  define buffer sysprop for sysprop.

  if pAppCode = ""
   then publish "GetAppCode" (output pAppCode).

  publish "GetSysProps" (output table sysprop).
  if not can-find(first sysprop where appCode = pAppCode and objAction = pAction and objProperty = pProperty)
   then
    do:
      empty temp-table tempsysprop.
      run server/getsysprops.p (input pAppCode,
                                input pAction,
                                input pProperty,
                                input "",
                                input "",
                                output table tempsysprop,
                                output std-lo,
                                output std-ch
                                ).

      if std-lo
       then
        for each tempsysprop:
          create sysprop.
          buffer-copy tempsysprop to sysprop.
        end.
       else
        do: 
          std-lo = false.
          publish "GetAppDebug" (output std-lo).
          if std-lo 
           then message "LoadSysProps (" + pAppCode + ", " + pAction + ", " + pProperty + ") failed: " + std-ch view-as alert-box warning.
        end.
    end.
  
  for each sysprop no-lock
     where sysprop.appCode = pAppCode
       and sysprop.objAction = pAction
       and sysprop.objProperty = pProperty
        by sysprop.objID:
     
    if lookup(sysprop.objID, pMinusID) > 0
     then next.
     
    pList = addDelimiter(pList,",") + sysprop.objValue + "," + sysprop.objID.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysPropLoad Include 
PROCEDURE SysPropLoad :
/*------------------------------------------------------------------------------
@description Loads the System Properties
------------------------------------------------------------------------------*/
  define variable tSysProp as character no-undo.
  define variable tAppCode as character no-undo.
  define variable tObjAction as character no-undo.
  define variable tObjProperty as character no-undo.

  define buffer sysprop for sysprop.
  define buffer tempsysprop for tempsysprop.

  empty temp-table sysprop.
  do std-in = 1 to num-entries({&sysprop}):
    tSysProp = entry(std-in, {&sysprop}).
    tAppCode = entry(1, tSysProp, ":").
    if index(tSysProp, ":") > 1
     then tObjAction = entry(2, tSysProp, ":").
    if index(tSysProp, ":") > 2
     then tObjProperty = entry(3, tSysProp, ":").
    /* get the properties */
    empty temp-table tempsysprop.
    run server/getsysprops.p (input tAppCode,
                              input tObjAction,
                              input tObjProperty,
                              input "",
                              input "",
                              output table tempsysprop,
                              output std-lo,
                              output std-ch
                              ).

    if std-lo 
     then
      for each tempsysprop:
        create sysprop.
        buffer-copy tempsysprop to sysprop.
      end.
     else
      do: 
        std-lo = false.
        publish "GetAppDebug" (output std-lo).
        if std-lo 
         then message "LoadSysProps (" + replace(tSysProp, ":", ", ") + ") failed: " + std-ch view-as alert-box warning.
      end.
  end.
  /*** ALWAYS LOAD YOUR OWN MODULE PROPERTIES ***/
  tAppCode = "".
  publish "GetAppCode" (output tAppCode).
  if not can-find(first sysprop where appCode = tAppCode)
   then
    do:
      empty temp-table tempsysprop.
      run server/getsysprops.p (input tAppCode,
                                input "",
                                input "",
                                input "",
                                input "",
                                output table tempsysprop,
                                output std-lo,
                                output std-ch
                                ).
    
      if std-lo 
       then
        for each tempsysprop:
          create sysprop.
          buffer-copy tempsysprop to sysprop.
        end.
       else
        do: 
          std-lo = false.
          publish "GetAppDebug" (output std-lo).
          if std-lo 
           then message "LoadSysProps (" + tAppCode + ") failed: " + std-ch view-as alert-box warning.
        end.
    end.
  /*** LOAD SYSPROPS NEEDED FOR COMMON ELEMENTS ***/
  /*** agent filters ***/
  if not can-find(first sysprop where appCode = "AMD" and objAction = "AgentFilter")
   then
    do:
      empty temp-table tempsysprop.
      run server/getsysprops.p (input "AMD",
                                input "AgentFilter",
                                input "",
                                input "",
                                input "",
                                output table tempsysprop,
                                output std-lo,
                                output std-ch
                                ).
    
      if std-lo 
       then
        for each tempsysprop:
          create sysprop.
          buffer-copy tempsysprop to sysprop.
        end.
       else
        do: 
          std-lo = false.
          publish "GetAppDebug" (output std-lo).
          if std-lo 
           then message "LoadSysProps (AMD, AgentFilter) failed: " + std-ch view-as alert-box warning.
        end.
    end.
  /* agent status */
  if not can-find(first sysprop where appCode = "AMD" and objAction = "Agent" and objProperty = "Status")
   then
    do:
      empty temp-table tempsysprop.
      run server/getsysprops.p (input "AMD",
                                input "Agent",
                                input "Status",
                                input "",
                                input "",
                                output table tempsysprop,
                                output std-lo,
                                output std-ch
                                ).
    
      if std-lo 
       then
        for each tempsysprop:
          create sysprop.
          buffer-copy tempsysprop to sysprop.
        end.
       else
        do: 
          std-lo = false.
          publish "GetAppDebug" (output std-lo).
          if std-lo 
           then message "LoadSysProps (AMD, Agent, Status) failed: " + std-ch view-as alert-box warning.
        end.
    end.
  tLoadedSysProps = can-find(first sysprop).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysPropModify Include 
PROCEDURE SysPropModify :
/*------------------------------------------------------------------------------
@description Modifies a sysprop
------------------------------------------------------------------------------*/
  define input parameter table for tempsysprop.
  define output parameter pSuccess as logical no-undo initial false.
  define buffer tempsysprop for tempsysprop.
  define buffer sysprop for sysprop.
  
  run server/newsysprop.p (input table tempsysprop,
                           output pSuccess,
                           output std-ch).

  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    for first tempsysprop no-lock:
      for first sysprop exclusive-lock
          where sysprop.appCode = tempsysprop.appCode
            and sysprop.objAction = tempsysprop.objAction
            and sysprop.objProperty = tempsysprop.objProperty
            and sysprop.objID = tempsysprop.objID:
        
        buffer-copy tempsysprop to sysprop.
      end.
      publish "SysPropDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysPropNew Include 
PROCEDURE SysPropNew :
/*------------------------------------------------------------------------------
@description Creates a new sysprop
------------------------------------------------------------------------------*/
  define input parameter table for tempsysprop.
  define output parameter pSuccess as logical no-undo initial false.
  define buffer tempsysprop for tempsysprop.
  
  run server/newsysprop.p (input table tempsysprop,
                           output pSuccess,
                           output std-ch).

  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    for first tempsysprop no-lock:
      create sysprop.
      buffer-copy tempsysprop to sysprop.
      publish "SysPropDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeSysUser) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysUserGet Include 
PROCEDURE SysUserGet :
/*------------------------------------------------------------------------------
@description Gets the user
------------------------------------------------------------------------------*/
  define input parameter pUID as character no-undo.
  define output parameter table for tempsysuser.
  define buffer sysuser for sysuser.

  publish "GetSysUsers" (output table sysuser).
  
  empty temp-table tempsysuser.
  for first sysuser no-lock
      where sysuser.uid = pUID:
    
    create tempsysuser.
    buffer-copy sysuser to tempsysuser.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysUserGetAll Include 
PROCEDURE SysUserGetAll :
/*------------------------------------------------------------------------------
@description Gets the system users
------------------------------------------------------------------------------*/
  define output parameter table for sysuser.

  if not tLoadedSysUsers
   then publish "LoadSysUsers".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysUserGetEmail Include 
PROCEDURE SysUserGetEmail :
/*------------------------------------------------------------------------------
@description Gets the user's email (if no user is passed in, the current user is used)
------------------------------------------------------------------------------*/
  define input  parameter pUID  as character.
  define output parameter pEmail as character.
  define buffer tempsysuser for tempsysuser.
  pEmail = "".

  /* get the current user */
  std-ch = "".
  publish "GetCredentialsID" (output std-ch).

  if pUID = ""
   then pUID = std-ch.

  publish "GetSysUser" (pUID, output table tempsysuser).

  for first tempsysuser no-lock:
    pEmail = tempsysuser.email.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysUserGetName Include 
PROCEDURE SysUserGetName :
/*------------------------------------------------------------------------------
@description Gets the user's name (if no user is passed in, the current user is used)
------------------------------------------------------------------------------*/
  define input  parameter pUID  as character.
  define output parameter pName as character.
  define buffer tempsysuser for tempsysuser.
  pName = "".

  publish "GetSysUser" (pUID, output table tempsysuser).

  for first tempsysuser no-lock:
    pName = tempsysuser.name.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysUserGetRole Include 
PROCEDURE SysUserGetRole :
/*------------------------------------------------------------------------------
@description Gets the user's role (if no user is passed in, the current user is used)
------------------------------------------------------------------------------*/
  define input  parameter pUID  as character.
  define output parameter pRole as character.
  define buffer tempsysuser for tempsysuser.
  pRole = "".

  /* get the current user */
  std-ch = "".
  publish "GetCredentialsID" (output std-ch).

  if pUID = ""
   then pUID = std-ch.

  publish "GetSysUser" (pUID, output table tempsysuser).

  for first tempsysuser no-lock:
    pRole = tempsysuser.role.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysUserLoad Include 
PROCEDURE SysUserLoad :
/*------------------------------------------------------------------------------
@description Loads the Compass users
------------------------------------------------------------------------------*/
  empty temp-table sysuser.
  run server/getsysusers.p (input  "B",    /* ActionType (A)ctive,(I)nactive,(B)oth */
                            input  "ALL",  /* UID */
                            input  ?,      /* StartDate*/
                            input  ? ,     /* EndDate */
                            input "" ,     /* fieldList delimiter ","  */
                            output table sysuser,
                            output tLoadedSysUsers,
                            output std-ch).

  if not tLoadedSysUsers
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadSysUsers failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeTimer) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE TIMER Procedure 
PROCEDURE TIMER :
/*------------------------------------------------------------------------------
  Purpose:     Called when the application is idle
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer esbmsg for esbmsg.

  /* Agent */
  for each esbmsg
     where esbmsg.entity = "Agent"
  break by esbmsg.keyID
        by esbmsg.rcvd descending:

   if tLoadedAgents and first-of(esbmsg.keyID) 
    then publish "AgentChanged" (esbmsg.keyID).
  end.

  /* County */
  for each esbmsg
     where esbmsg.entity = "County"
  break by esbmsg.keyID
        by esbmsg.rcvd descending:

   if tLoadedCounties and first-of(esbmsg.keyID) 
    then publish "CountyChanged" (esbmsg.keyID).
  end.

  /* Period */
  for each esbmsg
     where esbmsg.entity = "Period"
  break by esbmsg.keyID
        by esbmsg.rcvd descending:

   if tLoadedPeriods and first-of(esbmsg.keyID) 
    then publish "PeriodChanged" (esbmsg.keyID).
  end.

  /* State */
  for each esbmsg
     where esbmsg.entity = "State"
  break by esbmsg.keyID
        by esbmsg.rcvd descending:

   if tLoadedStates and first-of(esbmsg.keyID) 
    then publish "StateChanged" (esbmsg.keyID).
  end.

  /* run the procedure to continue the TIMER functions if necessary */
  run TIMEREXT in this-procedure no-error.

  tLastSync = now.
  tLastEsbMsgSeq = 0.
  empty temp-table esbmsg.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE UserFilterGet Procedure 
PROCEDURE UserFilterGet :
/*------------------------------------------------------------------------------
@description Gets the user filter
------------------------------------------------------------------------------*/
  define output parameter pState as character no-undo initial "".
  define output parameter pManager as character no-undo initial "".
  
  define buffer sysprop for sysprop.
  if not can-find(first sysprop where appCode = "AMD" and objAction = "AgentFilter")
   then
    do:
      empty temp-table tempsysprop.
      run server/getsysprops.p (input "AMD",
                                input "AgentFilter",
                                input "",
                                input "",
                                input "",
                                output table tempsysprop,
                                output std-lo,
                                output std-ch
                                ).
    
      if std-lo 
       then
        for each tempsysprop:
          create sysprop.
          buffer-copy tempsysprop to sysprop.
        end.
       else
        do: 
          std-lo = false.
          publish "GetAppDebug" (output std-lo).
          if std-lo 
           then message "LoadSysProps (AMD, AgentFilter) failed: " + std-ch view-as alert-box warning.
        end.
    end.
  
  /* get the current user */
  std-ch = "".
  publish "GetCredentialsID" (output std-ch).
  for each sysprop no-lock
     where sysprop.appCode = "AMD"
       and sysprop.objAction = "AgentFilter"
       and sysprop.objID = std-ch:
       
     if sysprop.objProperty = "State"
      then pState = sysprop.objValue.
       
     if sysprop.objProperty = "Manager"
      then pManager = sysprop.objValue.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE UserFilterGetManager Procedure 
PROCEDURE UserFilterGetManager :
/*------------------------------------------------------------------------------
@description Gets the filter as a manager list the user is allowed to see
------------------------------------------------------------------------------*/
  define output parameter pList as character no-undo.
  define buffer tempagent for tempagent.

  empty temp-table tempagent.
  publish "GetAgents" (output table tempagent).

  for each tempagent no-lock break by tempagent.manager:
    if first-of(tempagent.manager)
     then pList = addDelimiter(pList, ",") + tempagent.manager.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE UserFilterGetState Procedure 
PROCEDURE UserFilterGetState :
/*------------------------------------------------------------------------------
@description Gets the filter as a state list the user is allowed to see
------------------------------------------------------------------------------*/
  define output parameter pList as character no-undo.
  define buffer tempagent for tempagent.

  empty temp-table tempagent.
  publish "GetAgents" (output table tempagent).

  for each tempagent no-lock break by tempagent.stateID:
    if first-of(tempagent.stateID)
     then pList = addDelimiter(pList, ",") + tempagent.stateID.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&if DEFINED(excludeVendors) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE VendorGetAll Procedure 
PROCEDURE VendorGetAll :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pVendorNbr as character no-undo.
  define output parameter table for tempvendor.

  if not tLoadedVendors 
   then publish "LoadVendors".
     
  define buffer vendor for vendor.
  
  empty temp-table tempvendor.
  for each vendor no-lock:
    
    if pVendorNbr > "" and vendor.vendorID <> pVendorNbr
     then next.
    
    create tempvendor.
    buffer-copy vendor to tempvendor.
    if tempvendor.address > "" and
       tempvendor.city > "" and
       tempvendor.state > "" and
       tempvendor.zipcode > ""
     then tempvendor.fulladdress = vendor.address + " " + 
                                     vendor.city + ", " + 
                                     vendor.state + " " + 
                                     vendor.zipcode.
    release tempvendor.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE VendorLoad Procedure 
PROCEDURE VendorLoad :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer vendor for vendor.
 
  empty temp-table vendor.

  run server/getvendors.p ("",
                           "", /* All vendors */
                           output table vendor,
                           output tLoadedVendors,
                           output std-ch
                           ).
  if not tLoadedVendors
   then 
    do: std-lo = false.
        publish "GetAppDebug" (output std-lo).
        if std-lo 
         then message "LoadVendors failed: " + std-ch view-as alert-box warning.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

/* ************************  Function Implementations ***************** */

&if defined(excludeAgent) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshAgent Include 
FUNCTION refreshAgent RETURNS LOGICAL
  ( input pAgentID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable lSuccess as logical no-undo.
  define buffer tempagent for tempagent.
  define buffer agent for agent.
  
  empty temp-table tempagent.
  run server/getagents.p (pAgentID,
                          output table tempagent,
                          output lSuccess,
                          output std-ch).

  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshAgent failed: " + std-ch view-as alert-box warning.
    end.
   else
    do:
      find tempagent where tempagent.agentID = pAgentID no-error.
      find agent where agent.agentID = pAgentID no-error.
      if not available tempagent and available agent /* delete */
       then delete agent.
      if available tempagent and not available agent /* new */
       then create agent.
      if available tempagent and available agent /* modify */
       then buffer-copy tempagent to agent.
    end.
  
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeCounty) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshCounty Include 
FUNCTION refreshCounty RETURNS logical
  ( input pStateID as character,
    input pCountyID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable lSuccess as logical no-undo.
  define buffer tempcounty for tempcounty.
  define buffer county for county.
  
  empty temp-table tempcounty.
  run server/getcounties.p (pStateID,
                            pCountyID,
                            output table tempcounty,
                            output lSuccess,
                            output std-ch).

  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshSysCode failed: " + std-ch view-as alert-box warning.
    end.
   else
    do:
      find tempcounty where tempcounty.stateID = pStateID and tempcounty.countyID = pCountyID no-error.
      find county where county.stateID = pStateID and county.countyID = pCountyID no-error.
      if not available tempcounty and available county /* delete */
       then delete county.
      if available tempcounty and not available county /* new */
       then create county.
      if available tempcounty and available county /* modify */
       then buffer-copy tempcounty to county.
    end.
  
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludePeriod) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshPeriod Include 
FUNCTION refreshPeriod RETURNS logical
  ( input pPeriodID as integer ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable lSuccess as logical no-undo.
  define buffer tempperiod for tempperiod.
  define buffer period for period.
  
  empty temp-table tempperiod.
  case {&period}:
   when "Batch" then run server/getperiods.p (pPeriodID,
                                              output table period,
                                              output lSuccess,
                                              output std-ch).
   when "Accounting" then run server/getaccountingperiods.p (pPeriodID,
                                                             output table period,
                                                             output lSuccess,
                                                             output std-ch).
  end case.

  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshPeriod failed: " + std-ch view-as alert-box warning.
    end.
   else
    do:
      find tempperiod where tempperiod.periodID = pperiodID no-error.
      find period where period.periodID = pperiodID no-error.
      if not available tempperiod and available period /* delete */
       then delete period.
      if available tempperiod and not available period /* new */
       then create period.
      if available tempperiod and available period /* modify */
       then buffer-copy tempperiod to period.
    end.
  
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeRegion) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshRegion Include 
FUNCTION refreshRegion RETURNS logical
  ( input pRegionID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable lSuccess as logical no-undo.
  define buffer tempregion for tempregion.
  define buffer region for region.
  
  empty temp-table tempstate.
  run server/getregions.p (pRegionID,
                           output table tempregion,
                           output lSuccess,
                           output std-ch).

  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshRegion failed: " + std-ch view-as alert-box warning.
    end.
   else
    do:
      find tempregion where tempregion.regionID = pRegionID no-error.
      find region where region.regionID = pRegionID no-error.
      if not available tempregion and available region /* delete */
       then delete region.
      if available tempregion and not available region /* new */
       then create region.
      if available tempregion and available region /* modify */
       then buffer-copy tempregion to region.
    end.
  
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeState) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshState Include 
FUNCTION refreshState RETURNS logical
  ( input pStateID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable lSuccess as logical no-undo.
  define buffer tempstate for tempstate.
  define buffer state for state.
  
  empty temp-table tempstate.
  run server/getstates.p (pStateID,
                          output table tempstate,
                          output lSuccess,
                          output std-ch).

  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshState failed: " + std-ch view-as alert-box warning.
    end.
   else
    do:
      find tempstate where tempstate.stateID = pStateID no-error.
      find state where state.stateID = pStateID no-error.
      if not available tempstate and available state /* delete */
       then delete state.
      if available tempstate and not available state /* new */
       then create state.
      if available tempstate and available state /* modify */
       then buffer-copy tempstate to state.
    end.
  
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeSysCode) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshSysCode Include 
FUNCTION refreshSysCode RETURNS logical
  ( input pCodeType as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable lSuccess as logical no-undo.
  define buffer tempsyscode for tempsyscode.
  define buffer syscode for syscode.
  
  empty temp-table tempsyscode.
  run server/getsyscodes.p (pCodeType,
                            output table tempsyscode,
                            output lSuccess,
                            output std-ch).

  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshSysCode failed: " + std-ch view-as alert-box warning.
    end.
   else
    do:
      find tempsyscode where tempsyscode.codeType = pCodeType no-error.
      find syscode where syscode.codeType = pCodeType no-error.
      if not available tempsyscode and available syscode /* delete */
       then delete syscode.
      if available tempsyscode and not available syscode /* new */
       then create syscode.
      if available tempsyscode and available syscode /* modify */
       then buffer-copy tempsyscode to syscode.
    end.
  
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&if defined(excludeSysProp) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshSysProp Include 
FUNCTION refreshSysProp RETURNS LOGICAL
  ( input pAppCode as character,
    input pObjAction as character,
    input pObjProperty as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable lSuccess as logical no-undo.
  define buffer tempsysprop for tempsysprop.
  define buffer sysprop for sysprop.
  
  empty temp-table tempsysprop.
  run server/getsysprops.p (input pAppCode,
                            input pObjAction,
                            input pObjProperty,
                            input "",
                            input "",
                            output table tempsysprop,
                            output lSuccess,
                            output std-ch
                            ).

  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshSysProp failed: " + std-ch view-as alert-box warning.
    end.
   else
    do:
      find tempsysprop where tempsysprop.appCode = pAppCode and tempsysprop.objAction = pObjAction and tempsysprop.objProperty = pObjProperty no-error.
      find sysprop where sysprop.appCode = pAppCode and sysprop.objAction = pObjAction and sysprop.objProperty = pObjProperty no-error.
      if not available tempsysprop and available sysprop /* delete */
       then delete sysprop.
      if available tempsysprop and not available sysprop /* new */
       then create sysprop.
      if available tempsysprop and available sysprop /* modify */
       then buffer-copy tempsysprop to sysprop.
    end.
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

