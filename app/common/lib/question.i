&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/question.i
  seq:           Unique question identifier ** Mandatory  
  exclude-vars:  Non-blank to exclude variable definitions
  var-id:        Variable to hold identifier of question (Default: tQuestionID)
  var-desc:      Variable to hold description of question (Default: tQuestionDesc)
  var-priority:  Variable to hold priority of question (Default: tQuestionPriority)
  q-disp:        Override for display QuestionID to separate from db questionID
  ex-y:          EXclude the Yes option
  ex-n:          EXclude the No option
  ex-a:          EXclude the n/A option   
  ex-u:          EXclude the Unknown option
  ex-r:          Exclude the rectangle divider below the question
  questionType:  To specify which event to publish (default "Question")  
  .-label:       Specify for each checkbox to change label
  .-tip:         Specify for each checkbox for a tooltip
  r:             Row where widgets should be placed in the frame
  questionHeight:Pixel height of the question (default = 53)
  r-row-adj:     Expression adjustment to the placement of the rectangle
 */
/*------------------------------------------------------------------------
    File        :   lib/question.i
    Modification:
    Date          Name      Description
    11/14/2016    AC        Implement question priority = 0 functionality
    11/17/2016    AC        On resize of window, set question text width as
                            orignal
    09/24/2021    SA        Task#:86696 Defects raised by QA and set question 
                            text width
  ----------------------------------------------------------------------*/

&IF defined(exclude-question-vars) = 0 &then
def var tQuestionID as char no-undo.
def var tQuestionDesc as char no-undo.
def var tQuestionPriority as int no-undo.
def var tQuestionWarningAnswer as char no-undo.
def var tQuestionWarningMsg as char no-undo.
def var tQuestionWarningTip as char no-undo.
def var tQuestionFrameHeight as int no-undo.
define variable tQuestionDisplay as character no-undo.
define variable tQuestionSize as integer no-undo.
define variable questionpriority as integer no-undo.
&global-define exclude-question-vars true
&ENDIF

&IF defined(var-id) = 0 &THEN
&scoped-define var-id tQuestionID
&ENDIF

&IF defined(var-desc) = 0 &THEN
&scoped-define var-desc tQuestionDesc
&ENDIF

&IF defined(var-priority) = 0 &THEN
&scoped-define var-priority tQuestionPriority
&ENDIF

&IF defined(var-warninganswer) = 0 &THEN
&SCOPED-DEFINE var-warninganswer tQuestionWarningAnswer
&ENDIF

&IF defined(var-warningmsg) = 0 &THEN
&SCOPED-DEFINE var-warningmsg tQuestionWarningMsg
&ENDIF

&IF defined(var-warningtip) = 0 &THEN
&SCOPED-DEFINE var-warningtip tQuestionWarningTip
&ENDIF

&IF defined(questionType) = 0 &THEN
&scoped-define questionType Question
&ENDIF

&IF defined(ex-frameresize) = 0 &then
tQuestionFrameHeight = tQuestionFrameHeight + 
  &IF defined(questionHeight) = 0 &THEN 53 &ELSE {&questionHeight} &ENDIF.
frame fSection:virtual-height-pixels = max(frame fSection:height-pixels, tQuestionFrameHeight).
&ENDIF

publish "Get{&questionType}Attributes" (input {&seq}, 
                                 output {&var-id},
                                 output {&var-desc}, 
                                 output {&var-priority},                                 
                                 output std-lo,
                                 output {&var-warninganswer},
                                 output {&var-warningmsg},
                                 output {&var-warningtip}).
                                   
/* Taking care of adding "*" in questions, when question priority is 0 */
if {&var-priority} eq 0 then
  {&var-desc} = trim({&var-desc} + "" + fill("*", {&var-priority})).
else
  {&var-desc} = trim({&var-desc} + "" + fill("*", {&var-priority} - 1)).

/* Question number */
create text n{&seq}
 assign
   name = "n{&seq}"
   frame = frame fSection:handle
   column = 2.0
   row = {&r}
   width-chars = 11.0
   height-chars = 1.0
   sensitive = false
   visible = true
   format = "x(10)"
   .

/* Taking care of font of question sequence, when question priority is 0 */
if {&var-priority} eq 0 then
  n{&seq}:font = 15 + {&var-priority} + 1 .
else
  n{&seq}:font = 15 + {&var-priority}. 

&IF defined(q-disp) = 0 &THEN
n{&seq}:screen-value = {&var-id}.
&else
n{&seq}:screen-value = "{&q-disp}".
&ENDIF


/* Question text */
create text t{&seq}
 assign
   name = "t{&seq}"
   frame = frame fSection:handle
   column = 14.0
   row = {&r}
   width-chars = 105.0 /* ac : changed from 105.0 */
   height-chars = 1.0
   sensitive = true
   visible = true
   format = "x(500)"
   .
/* Taking care of font of question text, when question priority is 0 */
if {&var-priority} eq 0 then
  t{&seq}:font = 15 + {&var-priority} + 1.
else
  t{&seq}:font = 15 + {&var-priority}.
t{&seq}:screen-value = replace({&var-desc},'&','&&').
&global-define t{&seq}Resize t{&seq}:width-chars = frame fSection:width-chars - 57 .


/* Warning text */
if {&var-warninganswer} <> "" 
 then
  do:
    create text w{&seq}
     assign
       name = "w{&seq}"
       frame = frame fSection:handle
       column = 112.0
       row = {&r} + 0.9
       width-chars = 34.0
       height-chars = 0.6
       sensitive = false
       visible = true
       hidden = true
       fgcolor = 12
       format = "x(40)"
       private-data = {&var-warninganswer}
       .

     w{&seq}:x = frame fSection:width-pixels - 200.
     w{&seq}:screen-value = (if {&var-warningMsg} > "" 
                              then {&var-warningMsg}
                              else "Notify QA Manager immediately").
     if {&var-warningTip} > "" 
      then w{&seq}:tooltip = {&var-warningTip}.
  end.
&global-define w{&seq}Resize if valid-handle(w{&seq}) then w{&seq}:x = frame fSection:width-pixels - 200.


/* Yes checkbox */
&IF defined(ex-y) = 0 &THEN
create toggle-box q{&seq}y
 assign
   name = "q{&seq}y"
   frame = frame fSection:handle
   column = 119.0
   row = {&r}
   width-chars = 6.0
   height-chars = 1.0
   sensitive = true
   visible = true
  &if defined(y-label) <> 0 &then
   label = "{&y-label}"
  &else
   label = "Y"
  &endif
   tooltip = "{&y-tip}"
   tab-stop = true
   checked = false
   private-data = "Y"
   triggers:
    ON value-changed persistent run doCheckbox in this-procedure ({&seq}).
   end triggers.
   .
&global-define q{&seq}yResize q{&seq}y:x = frame fSection:width-pixels - 200.

create toggleset.
assign
  toggleset.seq = {&seq}
  toggleset.togglebox = q{&seq}y
  .
if {&var-warninganswer} = "Y" 
 then toggleset.haswarning = w{&seq}.
&ENDIF


/* No checkbox */
&if defined(ex-n) = 0 &then
create toggle-box q{&seq}N
 assign
   name = "q{&seq}N"
   frame = frame fSection:handle
   column = 126.0
   row = {&r}
   width-chars = 6.0
   height-chars = 1.0
   sensitive = true
   visible = true
  &if defined(n-label) <> 0 &then
   label = "{&n-label}"
  &else
   label = "N"
  &endif
   tooltip = "{&n-tip}"
   tab-stop = true
   checked = false
   private-data = "N"
   triggers:
    ON value-changed persistent run doCheckbox in this-procedure ({&seq}).
   end triggers.

&global-define q{&seq}nResize q{&seq}n:x = frame fSection:width-pixels - 165.

create toggleset.
assign
  toggleset.seq = {&seq}
  toggleset.togglebox = q{&seq}N
  .
if {&var-warninganswer} = "N" 
 then toggleset.haswarning = w{&seq}.
&endif


/* N/A checkbox */
&if defined(ex-a) = 0 &then
create toggle-box q{&seq}A
 assign
   name = "q{&seq}A"
   frame = frame fSection:handle
   column = 133.0
   row = {&r}
   width-chars = 8.0
   height-chars = 1.0
   sensitive = true
   visible = true
  &if defined(a-label) <> 0 &then
   label = "{&a-label}"
  &else
   label = "N/A"
  &endif
   tooltip = "{&a-tip}"
   tab-stop = true
   checked = false
   private-data = "A"
   triggers:
    ON value-changed persistent run doCheckbox in this-procedure ({&seq}).
   end triggers.

&global-define q{&seq}aResize q{&seq}a:x = frame fSection:width-pixels - 130.

create toggleset.
assign
  toggleset.seq = {&seq}
  toggleset.togglebox = q{&seq}A
  .
if {&var-warninganswer} = "A" 
 then toggleset.haswarning = w{&seq}.
&endif


/* Unknown checkbox */
&IF defined(ex-u) = 0 &THEN
create toggle-box q{&seq}U
 assign
   name = "q{&seq}U"
   frame = frame fSection:handle
   column = 142.0
   row = {&r}
   width-chars = 6.0
   height-chars = 1.0
   sensitive = true
   visible = true
  &if defined(u-label) <> 0 &then
   label = "{&u-label}"
  &else
   label = "?"
  &endif
   tooltip = "{&u-tip}"
   tab-stop = true
   checked = false
   private-data = "U"
   triggers:
    ON value-changed persistent run doCheckbox in this-procedure ({&seq}).
   end triggers.   

&global-define q{&seq}uResize q{&seq}u:x = frame fSection:width-pixels - 85.

create toggleset.
assign
  toggleset.seq = {&seq}
  toggleset.togglebox = q{&seq}U
  .
if {&var-warninganswer} = "U" 
 then toggleset.haswarning = w{&seq}.
&ENDIF


/* Finding/Best Practice button */
&IF defined(ex-b) = 0 &THEN
create button b{&seq}
  assign
    name = {&var-id}
    frame = frame fSection:handle
    column = 149
    row = {&r}
    sensitive = true
    visible = true
    width = 5.0
    height = 1.14
    tab-stop = false
   triggers:
    ON choose persistent run doButton in this-procedure ({&seq}).
   end triggers.
b{&seq}:load-image("images/notesblank.bmp").
b{&seq}:load-image-insensitive("images/notesdisabled.bmp").

/* Disable 'finding/best practice button when question priority is 0 */
if {&var-priority} eq 0 then
  b{&seq}:sensitive = false .
&global-define b{&seq}Resize b{&seq}:x = frame fSection:width-pixels - 50.
&ENDIF


/* Dividing line rectangle */
&IF defined(ex-r) = 0 &THEN
create rectangle r{&seq}
 assign
   name = "r{&seq}"
   frame = frame fSection:handle
   x = 5
   row = {&r} + 1.7 {&r-row-adj}
   width-pixels = 780
   height-pixels = 1
   graphic-edge = true
   sensitive = false
   visible = true
   filled = true
   fgcolor = 8
   .
&global-define r{&seq}Resize r{&seq}:width-pixels = frame fSection:width-pixels - 10.
&ENDIF

/* used for the tooltip */
tQuestionSize = integer( t{&seq}:width-chars).
if  (length({&var-desc}) > tQuestionSize - 20)
 then
  do:
    if  ( {&var-priority} eq 0 ) or ( {&var-priority} eq 1 ) then
    do:
      tQuestionDisplay = substring({&var-desc}, 1,  r-index({&var-desc}, " ", tQuestionSize - 20 ) - 1 ).
      tQuestionDisplay = trim(tQuestionDisplay + "" + fill(".", 3)).
      t{&seq}:screen-value = replace(tQuestionDisplay,'&','&&') .
      t{&seq}:tooltip = "..." + substring(replace({&var-desc},'&','&&'), r-index({&var-desc}, " ", tQuestionSize - 20 ),length({&var-desc}) ).
    end.
      
    if ( {&var-priority} eq 2 )  then
    do:
      tQuestionDisplay = substring({&var-desc}, 1,  r-index({&var-desc}, " ", tQuestionSize - 22 ) - 1 ). /*30*/
      tQuestionDisplay = trim(tQuestionDisplay + "" + fill(".", 3)).
      t{&seq}:screen-value = replace(tQuestionDisplay,'&','&&').
      t{&seq}:tooltip =  "..." + substring(replace({&var-desc},'&','&&'), r-index({&var-desc}, " ", tQuestionSize - 22 ) ,length({&var-desc}) ).
    end.
     
    if ( {&var-priority} eq 3 )  then
    do:
      tQuestionDisplay = substring({&var-desc}, 1,  r-index({&var-desc}, " ", tQuestionSize - 25 ) - 1 ).   /*46*/
      tQuestionDisplay = trim(tQuestionDisplay + "" + fill(".", 3)).
      t{&seq}:screen-value = replace(tQuestionDisplay,'&','&&').
      t{&seq}:tooltip = "..." +  substring(replace({&var-desc},'&','&&'), r-index({&var-desc}, " ", tQuestionSize - 25 ) ,length({&var-desc}) ).
    end.
  end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 14.91
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


