&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file win-status.i
@description Used to set the status bar for windows
@author John Oliver
@revision 11.23.2018 D.Sinclair - Renamed from set-status.i to win-status.i; removed undeclared vars; added specific functions
  ----------------------------------------------------------------------*/

&if defined(win-status-defined) = 0 &then
define variable cWinStatusMessage      as character no-undo.
define variable hWinStatusWindow       as handle no-undo.
define variable tWinStatusEntity       as character no-undo.
define variable tWinStatusEntityPlural as character no-undo.

&global-define win-status-defined true
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD appendStatus Include 
FUNCTION appendStatus RETURNS LOGICAL PRIVATE
  ( input pMsg as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearStatus Include 
FUNCTION clearStatus RETURNS LOGICAL PRIVATE
  (  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayStatus Include 
FUNCTION displayStatus RETURNS LOGICAL PRIVATE
  (  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD initializeStatusWindow Include 
FUNCTION initializeStatusWindow RETURNS LOGICAL PRIVATE
  ( input pWindow as handle )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setEntity Include 
FUNCTION setEntity RETURNS LOGICAL
  ( input pEntity       as character,
    input pEntityPlural as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setStatus Include 
FUNCTION setStatus RETURNS LOGICAL PRIVATE
  ( input pMsg as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setStatusCount Include 
FUNCTION setStatusCount RETURNS LOGICAL
  ( pCnt as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setStatusMessage Include 
FUNCTION setStatusMessage RETURNS LOGICAL PRIVATE
  ( pMsg as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setStatusRecords Include 
FUNCTION setStatusRecords RETURNS LOGICAL PRIVATE
  ( pCnt as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setStatusTime Include 
FUNCTION setStatusTime RETURNS LOGICAL PRIVATE
  ( pCnt as integer,
    pTime as datetime )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

&if defined(no-current-window) = 0 &then
if valid-handle(this-procedure:current-window) 
 then hWinStatusWindow = this-procedure:current-window.
&endif

&if defined(no-default-msg) = 0 &then
setStatus("").
displayStatus().
&endif

&if defined(entity) = 0 &then
&scoped-define entity "record"
&endif

&if defined(entityPlural) = 0 &then
&scoped-define entityPlural {&entity} + "s"
&endif

setEntity({&entity}, {&entityPlural}).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION appendStatus Include 
FUNCTION appendStatus RETURNS LOGICAL PRIVATE
  ( input pMsg as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  cWinStatusMessage = cWinStatusMessage + " " + pMsg.
  RETURN displayStatus().
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearStatus Include 
FUNCTION clearStatus RETURNS LOGICAL PRIVATE
  (  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatus("").
  return displayStatus().
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayStatus Include 
FUNCTION displayStatus RETURNS LOGICAL PRIVATE
  (  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if not valid-handle(hWinStatusWindow)
   then return false.
  
  status default cWinStatusMessage in window hWinStatusWindow.
  status input cWinStatusMessage in window hWinStatusWindow.
  RETURN true.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION initializeStatusWindow Include 
FUNCTION initializeStatusWindow RETURNS LOGICAL PRIVATE
  ( input pWindow as handle ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  hWinStatusWindow = pWindow.
  RETURN true.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setEntity Include 
FUNCTION setEntity RETURNS LOGICAL
  ( input pEntity       as character,
    input pEntityPlural as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  assign
    tWinStatusEntity       = pEntity
    tWinStatusEntityPlural = pEntityPlural
    .

  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setStatus Include 
FUNCTION setStatus RETURNS LOGICAL PRIVATE
  ( input pMsg as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if pMsg = ? then pMsg = "Unknown".
  cWinStatusMessage = pMsg.
  RETURN displayStatus().
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setStatusCount Include 
FUNCTION setStatusCount RETURNS LOGICAL
  ( pCnt as int ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatus((if pCnt = ? then "Unknown" else trim(string(pCnt,">>>,>>>,>>9"))) + " " + (if pCnt = 1 then tWinStatusEntity else tWinStatusEntityPlural) + " shown").
  return displayStatus().
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setStatusMessage Include 
FUNCTION setStatusMessage RETURNS LOGICAL PRIVATE
  ( pMsg as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 if pMsg = ? then pMsg = "Unknown".
 setStatus(pMsg).
 return displayStatus().
 
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setStatusRecords Include 
FUNCTION setStatusRecords RETURNS LOGICAL PRIVATE
  ( pCnt as int ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatus((if pCnt = ? then "Unknown" else trim(string(pCnt,">>>,>>>,>>9"))) + " " + (if pCnt = 1 then tWinStatusEntity else tWinStatusEntityPlural) + " shown as of " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss AM")).
  return displayStatus().
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setStatusTime Include 
FUNCTION setStatusTime RETURNS LOGICAL PRIVATE
  ( pCnt as integer,
    pTime as datetime ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatus((if pCnt = ? then "Unknown" else trim(string(pCnt,">>>,>>>,>>9"))) + " " + (if pCnt = 1 then tWinStatusEntity else tWinStatusEntityPlural) + " shown in " + trim(string(interval(now, pTime, "milliseconds") / 1000, ">>>,>>9.9")) + " seconds").
  return displayStatus().
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

