&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file set-button-def.i
@description Variables and functions for maintaining the buttons

@author John Oliver
@created 9/17/2018
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{lib/find-widget.i}

/* table for the various buttons */
define temp-table uiButton
  field uiButtonLabel      as character
  field uiButtonWidgetName as character
  field uiButtonWidget     as handle
  field uiButtonImage      as character
  field uiButtonInactive   as character
  field uiButtonExclude    as logical
  field uiButtonToggle     as logical
  field uiButtonUseSmall   as logical
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&if defined(noEnable) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD enableButtons Include 
FUNCTION enableButtons RETURNS LOGICAL
  ( input pEnable as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setButtons Procedure
FUNCTION setButtons RETURNS LOGICAL
  (  ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

empty temp-table uiButton.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&if defined(noEnable) = 0 &then

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION enableButtons Include 
FUNCTION enableButtons RETURNS LOGICAL
  ( input pEnable as logical ) :
/*------------------------------------------------------------------------------
@description Enable/disable the buttons
------------------------------------------------------------------------------*/
  define buffer uiButton for uiButton.
  
  for each uiButton no-lock:
    /* enable/disable only certian buttons */
    if valid-handle(uiButton.uiButtonWidget) and uiButton.uiButtonToggle
     then uiButton.uiButtonWidget:sensitive = pEnable.
  end.

  RETURN pEnable.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setButtons Include 
FUNCTION setButtons RETURNS LOGICAL
  (  ) :
/*------------------------------------------------------------------------------
@description Enable/disable the buttons
------------------------------------------------------------------------------*/
  define buffer uiButton for uiButton.
  
  for each uiButton exclusive-lock:
    /* only if it's not excluded */
    if not uiButton.uiButtonExclude
     then
      do:
        /* find the right widget */
        if valid-handle(uiButton.uiButtonWidget)
         then
          do:
            /* load the image on the button */
            uiButton.uiButtonWidget:load-image(uiButton.uiButtonImage).
            uiButton.uiButtonWidget:load-image-insensitive(uiButton.uiButtonInactive).
          end. /* if valid-handle */
      end. /* if not exclude */
  end. /* for each uiButton */

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

