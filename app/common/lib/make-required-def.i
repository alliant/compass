&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

define temp-table uiMakeRequired
  field uiRequired       as widget-handle
  field uiRequiredHandle as widget-handle
  field uiRequiredName   as character
  field uiRequiredFrame  as handle
  field uiRequiredRow    as decimal
  field uiRequiredCol    as decimal
  field uiRequiredHeight as decimal
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD repositionRequired Include 
FUNCTION repositionRequired RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setRequired Include 
FUNCTION setRequired RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD showRequired Include 
FUNCTION showRequired RETURNS LOGICAL
  ( input pWidget as widget-handle,
    input pShow   as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION repositionRequired Include 
FUNCTION repositionRequired RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  for each uiMakeRequired no-lock:
    if not (valid-handle(uiMakeRequired.uiRequired) and valid-handle(uiMakeRequired.uiRequiredHandle))
     then next.

    uiMakeRequired.uiRequiredHandle:col = uiMakeRequired.uiRequired:col + uiMakeRequired.uiRequired:width + 0.2.
  end.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setRequired Include 
FUNCTION setRequired RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  for each uiMakeRequired no-lock:
    create text uiMakeRequired.uiRequiredHandle assign
      frame         = uiMakeRequired.uiRequiredFrame
      row           = uiMakeRequired.uiRequiredRow
      column        = uiMakeRequired.uiRequiredCol
      width         = 2
      height        = uiMakeRequired.uiRequiredHeight
      data-type     = "CHARACTER"
      format        = "x(1)"
      sensitive     = false
      visible       = true
      name          = uiMakeRequired.uiRequiredName
      fgcolor       = 12
      font          = 10
      screen-value  = "*"
      .
  end.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION showRequired Include 
FUNCTION showRequired RETURNS LOGICAL
  ( input pWidget as widget-handle,
    input pShow   as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  for first uiMakeRequired exclusive-lock
      where uiMakeRequired.uiRequired = pWidget:

    if valid-handle(uiMakeRequired.uiRequiredHandle)
     then uiMakeRequired.uiRequiredHandle:visible = pShow.
  end.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

