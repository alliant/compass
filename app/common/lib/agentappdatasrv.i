&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file agentappdatasrv.i
@description Data Server file for the agent applications

@author John Oliver
@created 04-19-2019

@Modification
Date          Name            Description
06-08-2020    Shubham	      Modified Logic in 'refreshAgentApp'
06-10-2022    SA              Modified Logic in 'GetAgentApps' to get
                              regionDesc from 'GetSysCodeDesc'
09-09-2022    SC              Task#97318 AgentApp modified from Window not reflected 
                              in ModifyAgentApp dialog.                              
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}
define variable tLoadedAgentApps as logical no-undo initial false.

/* tables */
{tt/agentapp.i}
{tt/esbmsg.i &tableAlias="agentappesbmsg"}
{tt/sysprop.i &tableAlias="agentappsysprop"}
define temp-table openAgentApp
  field agentAppID     as character
  field agentAppHandle as handle
  .

/* temp tables */
{tt/agentapp.i &tableAlias="tempagentapp"}

/* functions */
{lib/count-days.i}
{lib/getstatename.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshAgentApp Procedure
FUNCTION refreshAgentApp RETURNS logical
  ( input pAgentID as character ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* main procedures */
subscribe to "AgentAppChanged"     anywhere.
subscribe to "DeleteAgentApp"      anywhere.
subscribe to "FinalizeAgentApps"   anywhere.
subscribe to "GetAgentApp"         anywhere.
subscribe to "GetAgentAppID"       anywhere.
subscribe to "GetAgentApps"        anywhere.
subscribe to "LoadAgentApps"       anywhere.
subscribe to "NewAgentApp"         anywhere.
subscribe to "updateAgentAppsData" anywhere.

/* used for opening/closing the agent data server */
subscribe to "CloseAgentApp"     anywhere.
subscribe to "OpenAgentApp"      anywhere.

/* used for the messaging system */
subscribe to "TIMER"                     anywhere run-procedure "AgentAppTimer".
subscribe to "ExternalEsbMessage"        anywhere run-procedure "AgentAppESB".

/* used to dump the data file */         
subscribe to "DataDump"                  anywhere run-procedure "AgentAppDataDump".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentAppChanged Include 
PROCEDURE AgentAppChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pAgentID as character no-undo.
  
  /* refresh the agentapp */
  refreshAgentApp(pAgentID).
  
  /* publish that the data changed */
  publish "AgentAppDataChanged".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentAppDataDump Include 
PROCEDURE AgentAppDataDump :
/*------------------------------------------------------------------------------
@description Dump the agentapp and tempagentapp temp-tables into files
------------------------------------------------------------------------------*/
  /* variables */
  define variable tPrefix as character no-undo.
  
  /* get the prefix */
  publish "GetAppCode" (output tPrefix).
  tPrefix = tPrefix + "_".
  
  /* delete the current temp files */
  publish "DeleteTempFile" ("DataAgentApp", output std-lo).
  publish "DeleteTempFile" ("DataTempAgentApp", output std-lo).
  
  /* Active temp-tables */
  temp-table agentapp:write-xml("file", tPrefix + "AgentApp.xml").
  publish "AddTempFile" ("DataAgentApp", tPrefix + "AgentApp.xml").
  
  /* "Temp" temp-tables */
  temp-table tempagentapp:write-xml("file", tPrefix + "TempAgentApp.xml").
  publish "AddTempFile" ("DataAgentApp", tPrefix + "TempAgentApp.xml").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentAppESB Procedure 
PROCEDURE AgentAppESB :
/*------------------------------------------------------------------------------
@description Runs when an esb message is received
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey as character no-undo.
  
  /* exit if not correct entity */
  if pEntity <> "AgentApp"
   then next.
  
  /* get the next number */
  std-in = 1.
  for each agentappesbmsg no-lock:
    std-in = std-in + 1.
  end.
  
  /* create the esb message */
  create agentappesbmsg.
  assign
    agentappesbmsg.seq    = std-in
    agentappesbmsg.rcvd   = now
    agentappesbmsg.entity = pEntity
    agentappesbmsg.action = pAction
    agentappesbmsg.keyID  = pKey
    .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentAppTimer Include 
PROCEDURE AgentAppTimer :
/*------------------------------------------------------------------------------
@description Triggered by appstart.i running on a timer
------------------------------------------------------------------------------*/
  /* buffers */
  define buffer agentappesbmsg for agentappesbmsg.

  /* loop through the esb messages */
  for each agentappesbmsg exclusive-lock
  break by agentappesbmsg.keyID
        by agentappesbmsg.rcvd descending:

    /* if the agentapp is loaded and the we have a messages */
    if tLoadedAgentApps and first-of(agentappesbmsg.keyID)
     then run AgentAppChanged (agentappesbmsg.keyID).
     
    /* delete the esb message and set the last sync */
    delete agentappesbmsg.
    publish "SetLastSync".
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseAgentApp Include 
PROCEDURE CloseAgentApp :
/*------------------------------------------------------------------------------
@description Closes the agentapp data server procedure
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pAgentID as character no-undo.
  define input  parameter pAppID   as integer   no-undo.
  
  /* variables */
  define variable tAgentAppID as character no-undo.
  
  /* buffers */
  define buffer openAgentApp for openAgentApp.
  
  /* get the combined agent application ID */
  run GetAgentAppID (pAgentID, pAppID, output tAgentAppID).
  
  /* get the correct agentapp data server record */
  for first openAgentApp exclusive-lock
      where openAgentApp.agentAppID = tAgentAppID:
      
    /* delete the proceudre to the data server */
    if valid-handle(openAgentApp.agentappHandle)
     then delete object openAgentApp.agentappHandle.
    
    /* delete the agentapp data server record */
    delete openAgentApp.
  end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAgentApp Include 
PROCEDURE GetAgentApp :
/*------------------------------------------------------------------------------
@description Gets an agentapp
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pAgentID as character no-undo.
  define input  parameter pAppID   as integer   no-undo.
  define output parameter table for tempagentapp.
  
  /* buffers */
  define buffer tempagentapp for tempagentapp.

  /* get all the agentapps */
  empty temp-table tempagentapp.
  run GetAgentApps (output table tempagentapp).

  /* delete all the agent apps that don't match the agentID and appID */
  for each tempagentapp exclusive-lock
     where tempagentapp.agentID <> pAgentID
        or tempagentapp.applicationID <> pAppID:
    
    delete tempagentapp.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAgentAppID Include 
PROCEDURE GetAgentAppID :
/*------------------------------------------------------------------------------
@description Gets an agentapp
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pAgentID       as character no-undo.
  define input  parameter pApplicationID as integer   no-undo.
  define output parameter pAgentAppID    as character no-undo.
  
  pAgentAppID = pAgentID {&msg-add} string(pApplicationID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAgentApps Include 
PROCEDURE GetAgentApps :
/*------------------------------------------------------------------------------
@description Get the activities
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for agentapp.
  
  /* if can't find any tempagentapp records, load from the server */
  if not tLoadedAgentApps
   then run LoadAgentApps.
  
  /* get the descriptions of fields */
  for each agentapp exclusive-lock:
    /* state name */
    agentapp.stateName = getStateName(agentapp.stateID).
    /* get reason code */
    agentapp.reasonCodeDesc = agentapp.reasonCode.
    publish "GetSysCodeDesc" ("AgentAppReasonCode", agentapp.reasonCode, output agentapp.reasonCodeDesc).
    /* get the status */
    agentapp.statDesc = agentapp.stat.
    publish "GetSysPropDesc" ("AMD", "Application", "Status", agentapp.stat, output agentapp.statDesc).
    /* get the type */
    agentapp.typeDesc = agentapp.type.
    publish "GetSysPropDesc" ("AMD", "Application", "Type", agentapp.type, output agentapp.typeDesc).
    /* get the subtype */
    agentapp.subtypeDesc = agentapp.subtype.
    publish "GetSysCodeDesc" (agentapp.typeDesc + "Type", agentapp.subtype, output agentapp.subtypeDesc).
    /* get the region */
    agentapp.regionDesc = agentapp.regionID.
    publish "GetRegionDescription" (agentapp.regionID, output agentapp.regionDesc).
    /* get the manager */
    agentapp.managerDesc = agentapp.manager.
    publish "GetSysUserName" (agentapp.manager, output agentapp.managerDesc).
    /* calculate metrics */
    assign
      agentapp.metricCreatedToSubmitted = countDays(agentapp.dateSubmitted, agentapp.dateCreated)
      agentapp.metricERRStartedToFinish = countDays(agentapp.dateReceviedERR, agentapp.dateOrderedERR)
      agentapp.metricCreatedToUnderReview = countDays(agentapp.dateUnderReview, agentapp.dateCreated)
      agentapp.metricUnderReviewToSentReview = countDays(agentapp.dateSentReview, agentapp.dateUnderReview)
      std-da = (if agentapp.stat = "D" or agentapp.stat = "X" then agentapp.dateStopped else agentapp.dateApproved)
      agentapp.metricSentReviewToApproved = countDays(std-da, agentapp.dateSentReview)
      agentapp.metricCreatedToApproved = countDays(std-da, agentapp.dateCreated)
      agentapp.metricUnderReviewToSentAgency = countDays(agentapp.dateSentAgency, agentapp.dateUnderReview)
      agentapp.metricSentAgencyToSigned = countDays(agentapp.dateSigned, agentapp.dateSentAgency)
      agentapp.metricCreatedToSigned = countDays(agentapp.dateSigned, agentapp.dateCreated)
      .
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadAgentApps Include 
PROCEDURE LoadAgentApps :
/*------------------------------------------------------------------------------
@description Loads the agent applications
------------------------------------------------------------------------------*/     
  /* call the server */
  empty temp-table agentapp.
  run server/getagentapp.p(input  "",
                           output table agentapp,
                           output tLoadedAgentApps,
                           output std-ch).
  
  /* if unsuccessful, show a message if debug */
  if not tLoadedAgentApps
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadAgentApps failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewAgentApp C-Win 
PROCEDURE NewAgentApp :
/*------------------------------------------------------------------------------
@description Creates a new agent application
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter  pType as character no-undo.
  define input parameter  pSubtype as character no-undo.
  define input parameter  pAgentID as character no-undo.
  define input parameter  pUsername as character no-undo.
  define input parameter  pPassword as character no-undo.
  define output parameter pSuccess as logical no-undo.
  
  /* call the server */
  run server/newagentapp.p (input  pType,
                            input  pSubtype,
                            input  pAgentID,
                            input  pUsername,
                            input  pPassword,
                            output pSuccess,
                            output std-ch).
  
  /* if unsuccessful, show a message */
  if not pSuccess
   then message std-ch view-as alert-box warning.
   else
    /* otherwise, refresh the data from the server */
    do:
      refreshAgentApp(pAgentID).
      publish "AgentAppDataChanged".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenAgentApp C-Win 
PROCEDURE OpenAgentApp :
/*------------------------------------------------------------------------------
@description Loads the agent application into the agentdatasrv.p file
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter pAgentID as character no-undo.
  define input  parameter pAppID   as integer   no-undo.
  define output parameter pHandle  as handle    no-undo.
  
  /* variables */
  define variable tAgentAppID as character no-undo.
  
  /* buffers */
  define buffer openAgentApp for openAgentApp.
  
  /* get the combined agent application ID */
  run GetAgentAppID (pAgentID, pAppID, output tAgentAppID).
  
  /* if available, get the handle to the agentapp data server */
  if can-find(first openAgentApp where agentAppID = tAgentAppID)
   then 
    for first openAgentApp no-lock
        where openAgentApp.agentAppID = tAgentAppID:
        
      pHandle = openAgentApp.agentAppHandle.
    end.
   else 
    do:
      /* otherwise run the agentapp data server and create the record */
      run agentappdatasrv.p persistent set pHandle (pAgentID, pAppID).
      create openAgentApp.
      assign
        openAgentApp.agentAppID     = tAgentAppID
        openAgentApp.agentAppHandle = pHandle
        .
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE updateAgentAppsData C-Win 
PROCEDURE updateAgentAppsData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter table for tempagentapp.
  
  for each tempagentapp no-lock:
      find agentapp where agentapp.agentID = tempagentapp.agentID
                          and agentapp.applicationID = tempagentapp.applicationID no-error.
      if available tempagentapp and not available agentapp /* new */
       then create agentapp.
      if available tempagentapp and available agentapp /* modify */
       then buffer-copy tempagentapp to agentapp.
    end.
  
  temp-table agentapp:write-xml("file","agentapp.xml").
  
  empty temp-table tempagentapp.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshAgentApp Include 
FUNCTION refreshAgentApp RETURNS logical
  ( input pAgentID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* variables */
  define variable lSuccess as logical no-undo.
  
  /* buffers */
  define buffer tempagentapp for tempagentapp.
  define buffer agentapp for agentapp.
  
  /* call the server */
  empty temp-table tempagentapp.
  run server/getagentapp.p (input  pAgentID,
                            output table tempagentapp,
                            output lSuccess,
                            output std-ch).

  /* if unsuccessful, show a message if debug is on */
  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshAgentApp failed: " + std-ch view-as alert-box warning.
    end.
   else
    /* otherwise, copy the agentapp details into the agentapp records */
    for each tempagentapp no-lock:
      find agentapp where agentapp.agentID = tempagentapp.agentID
	                  and agentapp.applicationID = tempagentapp.applicationID no-error.
      if available tempagentapp and not available agentapp /* new */
       then create agentapp.
      if available tempagentapp and available agentapp /* modify */
       then buffer-copy tempagentapp to agentapp.
    end.
  
  temp-table agentapp:write-xml("file","agentapp.xml").
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
