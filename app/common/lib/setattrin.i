&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
lib/setattrin.i
SET a INteger ATTRibute
----------------------------------------------------------------------*/
/*------------------------------------------------------------------------
    File            : setattrin.i
    Description     : Show questions of section1
    Modification    :
    Date          Name      Description
    11/24/2016    AC        Changed to publish correct method
    12/26/2016    AG        Implement AutoSave functionality.
	02/13/2017    AG        Modifed to publish the procedure.
  ----------------------------------------------------------------------*/

publish "SetAuditAttribute" ("{1}", 
                             input ?, 
                             input ?, 
                             input ?, 
                             input {2}, 
                             input ?).

publish "SaveAfterEveryAnswer".
publish "EnableSave".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


