&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file set-filter.i
@description Holds the variables and 

@param tableName;char;The name of the table (default is "data")
@param labelName;char;The labels for the filters (comma-delimited list)
@param columnName;char;The columns that filter the data in the temmp table

@author John Oliver
@created 10/12/2018
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

&if defined(filterColumnValue) = 0 &then
&scoped-define filterColumnValue {&filterColumnID}
&endif

&if defined(populate) = 0 &then
&scoped-define populate true
&endif

&if defined(useSingle) = 0 &then
&scoped-define useSingle false
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */


create filter.
assign
  filter.filterLabel       = "{&label}"
  filter.filterColumnID    = "{&id}"
  filter.filterColumnValue = "{&value}"
  filter.filterUseSingle   = {&useSingle}
  filter.filterSet         = {&populate}
  filter.filterAddOption   = "{&addOption}"
  .
  
&if defined(f) = 0 &then
filter.filterWidgetName = "f" + replace(replace(filter.filterLabel, " ", ""), "." , "").
&else
filter.filterWidgetName = "{&f}".
&endif

filter.filterWidget = GetWidgetByName(frame {&frame-name}:handle, filter.filterWidgetName).
if valid-handle(filter.filterWidget) and (filter.filterWidget:type = "FILL-IN" or filter.filterWidget:type = "EDITOR")
 then filter.filterSet = false.
release filter.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

