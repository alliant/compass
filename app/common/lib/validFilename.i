&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : validfilename.i
    Purpose     : To validate a filename entered by the user

    Syntax      :

    Description :

    Author(s)   : B.Johnson
    Created     : 02/13/2015
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validFilename Include 
FUNCTION validFilename RETURNS LOGICAL
  ( input pFilename as char, output pBadFileNameChars as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validFilename Include 
FUNCTION validFilenameWindows RETURNS LOGICAL
  ( input pFilename as char, output pBadFileNameChars as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilename Procedure
FUNCTION getFilename RETURNS character
  ( input pFilename as character ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validFilename Include 
FUNCTION validFilename RETURNS LOGICAL
  ( input pFilename as char, output pBadFileNameChars as char ) :

  def var tBadFileNameChars as char init "/?<>\:*|~"~~~'~`!@#$%^&~{~}[];" no-undo.
  def var i as int no-undo.
  def var tNameOK as log no-undo init yes.

  do i = 1 to length(pFilename):
    if index(tBadFileNameChars, substring(pFilename,i,1)) > 0 then
    do:
      tNameOK = no.
      leave.
    end.
  end.
  
  pBadFileNameChars = tBadFileNameChars.

  RETURN tNameOK.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&analyze-resume

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilename Include 
FUNCTION getFilename RETURNS character
  ( input pFilename as character ) :
  
  define variable cFilename as character no-undo.
  define variable cBadChars as character no-undo.
  define variable i as integer no-undo.
  define variable pos as integer no-undo.
  
  cFilename = pFilename.
  if not validFilename(pFilename, output cBadChars)
   then
    do i = 1 to length(pFilename):
      pos = index(cBadChars, substring(pFilename,i,1)).
      if pos > 0
       then cFilename = replace(cFilename,substring(cBadChars,pos,1),"").
    end.
    
  return cFilename.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validFilenameWindows Include 
FUNCTION validFilenameWindows RETURNS LOGICAL
  ( input pFilename as char, output pBadFileNameChars as char ) :

  def var tBadFileNameChars as char init "/\:*?~"<>|" no-undo.
  def var i as int no-undo.
  def var tNameOK as log no-undo init yes.

  do i = 1 to length(pFilename):
    if index(tBadFileNameChars, substring(pFilename,i,1)) > 0 then
    do:
      tNameOK = no.
      leave.
    end.
  end.
  
  pBadFileNameChars = tBadFileNameChars.

  RETURN tNameOK.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&analyze-resume
