&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file getFileFromTT.i
@description Gets the data from the server XML

@param tFileList;char;The name of the variable to get file paths list

@author MK
@created 04.05.2021
@modified 09.03.2021	MK	Framework changes
  ----------------------------------------------------------------------*/
def var tReportDir as char.
def var pOutFile   as char.
def var pBaseName  as char.
def var mFile as memptr no-undo.

/* ***************************  Definitions  ************************** */
&if defined(fileList) = 0 &then
&scoped-define fileList pFileList
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */
 for each file no-lock
  break by file.filename:
  if first-of(file.filename)
   then
    do:
     assign
        pBaseName  = ""
        pOutFile   = ""
        tReportDir = "".
        
     run server/rebuildfile.p (file.filename, table file, output mFile, output std-lo).
     publish "GetReportDir" (output tReportDir).
     if search(tReportDir) = ?
      then tReportDir = os-getenv("appdata") + "\Alliant\".
      
     assign
        pBaseName = entry(1,entry(num-entries(file.filename, "\"),file.filename,"\"), ".") + "-" + string(TIME)
        pOutFile =  tReportDir + pBaseName + "." + entry(2,entry(num-entries(file.filename, "\"),file.filename,"\"), ".").
        
     {&fileList} = {&fileList} + pOutFile + ",".
     copy-lob from mFile to file pOutFile no-error.
     
     publish "AddTempFile" (pBaseName,pOutFile).
     
    end.
 end.
 {&fileList} = trim({&fileList}, ",").

 empty temp-table file.
 
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


