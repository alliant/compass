&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file window-management.i

  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{lib/std-def.i}
{lib/add-delimiter.i}

/* Stores persistent window instances */
define temp-table openWindow
  field entityType    as character
  field entityID      as character
  field childHandle   as handle
  field isBusy        as logical
  field parentHandle  as handle
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* Event to subscribe to open a window */
subscribe to "OpenWindow"       anywhere.
subscribe to "OpenWindowNoShow" anywhere.

/* Event published when any window is closed. */
subscribe to "CloseWindow"      anywhere.

/* Event to get the open window handles */
subscribe to "GetOpenWindows"   anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseChildWindow Include 
PROCEDURE CloseChildWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pParentHandle as handle no-undo.
  
  define buffer openWindow for openWindow.
  define buffer bfOpenWindow for openWindow.

  /* Close all the childs associated with the input parent window's handle. */
  for each openWindow
     where openWindow.parentHandle = pParentHandle:

    std-ha = openWindow.childHandle.
    
    /* Deleting the child window instance from the temp-table before calling 
       closeWindow procedure. This has been done because the closeWindow 
       procedure of the child screen also calls WindowClosed procedure of 
       opstart.w. This leads to circular execution. Since it tries to 
       deletes the same instance, this generates error. */
    delete openWindow.
    
    /* Close child window created in the window*/
    if valid-handle(std-ha) 
     then run closeWindow in std-ha.
  end.
  
  /* Deleting windows instance when it is closed from 
     the cross button */
  for each openWindow
     where openWindow.childHandle = pParentHandle:
    
    delete openWindow.
  end.

  std-ha = ?.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseWindow Include 
PROCEDURE CloseWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pType as character no-undo.
  define input parameter pID   as character no-undo.
  
  define buffer openWindow for openWindow.
  define buffer bfOpenWindow for openWindow.

  /* Close all the childs associated with the input parent window's handle. */
  for each openWindow
     where openWindow.entityType = pType
       and openWindow.entityID   = pID:

    std-ha = openWindow.childHandle.
    
    /* Deleting the child window instance from the temp-table before calling 
       closeWindow procedure. This has been done because the closeWindow 
       procedure of the child screen also calls WindowClosed procedure of 
       opstart.w. This leads to circular execution. Since it tries to 
       deletes the same instance, this generates error. */
    delete openWindow.
    
    /* Close child window created in the window*/
    if valid-handle(std-ha) 
     then run closeWindow in std-ha.
  end.
  
  /* Deleting windows instance when it is closed from 
     the cross button */
  for each openWindow:
    if not valid-handle(openWindow.childHandle)
     then delete openWindow.
  end.

  std-ha = ?.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetOpenWindows Include 
PROCEDURE GetOpenWindows :
  define output parameter pHandleList as character no-undo.
  
  for each openWindow no-lock:
    pHandleList = addDelimiter(pHandleList, ",") + string(openWindow.childHandle).
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenWindow Include 
PROCEDURE OpenWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pType         as character no-undo.
  define input parameter pID           as character no-undo.
  define input parameter pProg         as character no-undo.
  define input parameter pParam        as character no-undo. /* Pass-through when running value(pProg) */
  define input parameter pParentHandle as handle    no-undo.
  
  define variable hCall    as handle    no-undo.
  define variable icount   as integer   no-undo.
  define variable datatype as character no-undo.
  define variable inout    as character no-undo.
  define variable varname  as character no-undo. 
  
  define buffer openWindow for openWindow.
  
  if pType = "" or pProg = "" 
   then return.
  
  assign
    std-ha = ?
    std-lo = false
    .
    
  for each openWindow
     where openWindow.entityType = pType
       and openWindow.entityID   = pID:

    if valid-handle(openWindow.childHandle) and not std-lo 
     then 
      do: 
        assign
          std-ha = openWindow.childHandle
          std-lo = true   /* found one */
          .
      end.
    else delete openWindow.
  end.
  
  if not std-lo 
   then
    do: 
      create call hCall.
      hCall:call-name  = pProg.
      hCall:persistent = true.
      hCall:call-type  = procedure-call-type.
      
      if num-entries(pParam, {&msg-dlm}) > 0 
       then
        do:
          hCall:num-parameters = num-entries(pParam, {&msg-dlm}).
          do icount = 1 to num-entries(pParam, {&msg-dlm}):
            datatype = entry(1,entry(icount, pParam, {&msg-dlm}),"|").
            inout    = entry(2,entry(icount, pParam, {&msg-dlm}),"|").
            varname  = entry(3,entry(icount, pParam, {&msg-dlm}),"|").
            hCall:set-parameter(icount, datatype, inout, varname).
          end.
        end.

      hCall:invoke.
      if not valid-handle(hCall) 
       then return. 
      
      create openWindow.
      assign
        openWindow.entityType   = pType
        openWindow.entityID     = pID
        /* Store the handle of current window opened. */
        openWindow.childHandle  = hCall:in-handle.
        std-ha                  = hCall:in-handle.
        /* Store the handle of parent window which is 
           responsible for opening the child window. */
        openWindow.parentHandle = pParentHandle 
        .
    end.
    
  if valid-handle(std-ha) 
   then run ShowWindow in std-ha.
  
  std-ha = ?.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenWindowNoShow Include 
PROCEDURE OpenWindowNoShow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pType         as character no-undo.
  define input  parameter pID           as character no-undo.
  define input  parameter pProg         as character no-undo.
  define input  parameter pParam        as character no-undo. /* Pass-through when running value(pProg) */
  define input  parameter pParentHandle as handle    no-undo.
  define output parameter pReturnHandle as handle    no-undo.
  
  define variable hCall    as handle    no-undo.
  define variable icount   as integer   no-undo.
  define variable datatype as character no-undo.
  define variable inout    as character no-undo.
  define variable varname  as character no-undo. 
  
  define buffer openWindow for openWindow.
  
  if pType = "" or pProg = "" 
   then return.
  
  assign
    std-ha = ?
    std-lo = false
    .
    
  for each openWindow
     where openWindow.entityType = pType
       and openWindow.entityID   = pID:

    if valid-handle(openWindow.childHandle) and not std-lo 
     then 
      do: 
        assign
          pReturnHandle = openWindow.childHandle
          std-lo        = true   /* found one */
          .
      end.
    else delete openWindow.
  end.
  
  if not std-lo 
   then
    do: 
      create call hCall.
      hCall:call-name  = pProg.
      hCall:persistent = true.
      hCall:call-type  = procedure-call-type.
      
      if num-entries(pParam, {&msg-dlm}) > 0 
       then
        do:
          hCall:num-parameters = num-entries(pParam, {&msg-dlm}).
          do icount = 1 to num-entries(pParam, {&msg-dlm}):
            datatype = entry(1,entry(icount, pParam, {&msg-dlm}),"|").
            inout    = entry(2,entry(icount, pParam, {&msg-dlm}),"|").
            varname  = entry(3,entry(icount, pParam, {&msg-dlm}),"|").
            hCall:set-parameter(icount, datatype, inout, varname).
          end.
        end.

      hCall:invoke.
      if not valid-handle(hCall) 
       then return. 
      
      create openWindow.
      assign
        openWindow.entityType   = pType
        openWindow.entityID     = pID
        /* Store the handle of current window opened. */
        openWindow.childHandle  = hCall:in-handle.
        pReturnHandle           = hCall:in-handle.
        /* Store the handle of parent window which is 
           responsible for opening the child window. */
        openWindow.parentHandle = pParentHandle 
        .
    end.
    
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

