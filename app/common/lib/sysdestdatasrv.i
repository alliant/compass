&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file sysdestdatasrv.i
@description Data Server file for common data elements

@param sysprop;char;Comma-delimited list of sysprops to get. The props are
                    separated by semicolon (:). The first is the appCode,
                    second is the objAction, and third is the objProperty
@param syscode;char;Comma-delimited list of syscodes to get
@param period;char;Either "Accounting" or "Batch"

@author John Oliver
@created 07-11-2018
@modification
  Date         Name                  Description
  12.29.2021   Sachin Chaturvedi     added new procedures getsysactionentity and 
                                     loadsysactionentity to get entity,action and
                                     destinations for parameters.
  01.13.2022   Sachin Chaturvedi     modified the getsysdests and loadsysdests with  
                                     parameter pDestination.
  10.12.2023   Sagar K               Modified related to Vendor

  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* variables */
{lib/std-def.i}
define variable tLoadedSysDest    as logical initial false no-undo.
define variable tLoadedAction     as logical initial false no-undo.
define variable tLoadedSysAction  as logical initial false no-undo.
define variable tLoadedAllVendors  as logical initial false no-undo.

/* to cache data for parameters */
define variable charEntity        as character no-undo. 
define variable charDestinations  as character no-undo.


/* tables */
{tt/sysdest.i}
{tt/sysdest.i &tableAlias="actiondest"}
{tt/esbmsg.i &tableAlias="sysdestesbmsg"}
{tt/sysaction.i &tableAlias="ttsysaction1"}

/* temp tables */
{tt/sysdest.i &tableAlias="tempsysdest"}
{tt/apvendor.i &tableAlias=allvendor}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshSysDest Include 
FUNCTION refreshSysDest RETURNS logical
  ( input pEntity as character,
    input pEntityID as character,
    input pAction as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* main procedures */
subscribe to "DeleteSysDest"      anywhere.
subscribe to "GetSysActionDests"  anywhere.
subscribe to "GetSysDest"         anywhere.
subscribe to "GetSysDests"        anywhere.
subscribe to "LoadSysDests"       anywhere.
subscribe to "ModifySysDest"      anywhere.
subscribe to "NewSysDest"         anywhere.
subscribe to "GetSysActionEntity" anywhere.
subscribe to "GetAllVendors"        anywhere.
subscribe to "LoadAllVendors"       anywhere.

/* used for the messaging system */
subscribe to "TIMER"              anywhere run-procedure "SysDestTimer".
subscribe to "ExternalEsbMessage" anywhere run-procedure "SysDestESB".

/* used to dump the data file */
subscribe to "DataDump"           anywhere run-procedure "SysDestDataDump".

/* load system users */
std-lo = false.
publish "SetSplashStatus".
publish "GetLoadDestinations" (output std-lo).
if std-lo 
 then run LoadSysDests.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteSysDest Include 
PROCEDURE DeleteSysDest :
/*------------------------------------------------------------------------------
@description Deletes the destination
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter ipiDestID    as integer   no-undo.
  define output parameter oplSuccess   as logical   no-undo.
  define output parameter opcMsg       as character no-undo.
    
  /* buffers */
  define buffer sysdest for sysdest.

  /* call the server */
  run server/deletesysdest.p (input ipiDestID,
                              output oplSuccess, 
                              output opcMsg).
  
  if not oplSuccess
   then return.
  
  /* Delete Cache records */
  find first sysdest where sysdest.destID = ipiDestID no-error.  
  if not available sysdest 
   then
    do: 
      assign 
          opcMsg     = "Internal Error of missing data."  
          oplSuccess = false
         . 
       return.
    end.
    
  delete sysdest.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAllVendors Include 
PROCEDURE GetAllVendors :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   /* parameters */
  define input parameter pStatus as character no-undo.
  define output parameter table for allvendor.
  
  /* if not done, load the vendorproducts */
  if not tLoadedAllVendors 
   then run LoadAllVendors (input pStatus).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSysActionDests Include 
PROCEDURE GetSysActionDests :
/*------------------------------------------------------------------------------
@description Gets the actions that are used as destinations
------------------------------------------------------------------------------*/
  /* parameters */
  define output parameter table for actiondest.
  
  /* Check if data is available in local cache. */
  if not tLoadedAction
   then run server/getactiondestinations.p (output table actiondest,
                                            output tLoadedAction,
                                            output std-ch).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSysActionEntity Include 
PROCEDURE GetSysActionEntity :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 /* parameters */
  define output parameter table for ttsysaction1.
  define output parameter cEntity       as character no-undo.
  define output parameter cDestinations as character no-undo.
 
  define buffer ttsysaction1 for ttsysaction1.

  /* Check if data is available in local cache. If its available, pass this,
     otherwise refresh the cache data */
  if (not can-find(first ttsysaction1) and 
      cEntity       = ""               and 
      cDestinations = "")
   then
    do:
      run LoadSysActionEntity(output table ttsysaction1,
                              output cEntity, 
                              output cDestinations)
                              .
      assign
          charEntity       = cEntity
          charDestinations = cDestinations
          .
    end.
   else
    assign
        cEntity       = charEntity
        cDestinations = charDestinations
        .
    

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSysDest Include 
PROCEDURE GetSysDest :
/*------------------------------------------------------------------------------
@description Gets the destination for the agent/action
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity   as character no-undo.
  define input parameter pEntityID as character no-undo.
  define input parameter pAction   as character no-undo.
  define output parameter table for tempsysdest.
  
  /* buffers */
  define buffer sysdest     for sysdest.
  define buffer tempsysdest for tempsysdest.

  /* get the users */
  empty temp-table tempsysdest.
  
  /* Used to set status message on screen */
  publish "SetCurrentValue" ("RefreshSysDest", false).
  
  /* Check if data is available in local cache. */
  if not can-find(first sysdest)
   then
    run GetSysDests (input "",
	                 input "",
					 input "",
	                 output table sysdest).
  
   else if not can-find(first sysdest where sysdest.entityType = pEntity
                                        and sysdest.entityID   = pEntityID
                                        and sysdest.action     = pAction)
    then
     refreshSysDest(input pEntity, input pEntityID, input pAction).
            
  /* get the desired sysdest */
  for each sysdest no-lock 
    where sysdest.entityType = pEntity
      and sysdest.entityID   = pEntityID
      and sysdest.action     = pAction:
    create tempsysdest.
    buffer-copy sysdest to tempsysdest.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSysDests Include 
PROCEDURE GetSysDests :
/*------------------------------------------------------------------------------
@description Gets the all destinations
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity      as character no-undo.
  define input parameter pDestination as character no-undo.
  define input parameter pAction      as character no-undo.
  define output parameter table for sysdest.

  /* Used to set status message on screen */
  publish "SetCurrentValue" ("RefreshSysDest", false).
  
  /* get all the users */
  
  run LoadSysDests(input pEntity,
                   input pDestination,
                   input pAction ,
                   output table sysdest).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadAllVendors Include 
PROCEDURE LoadAllVendors :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pStatus as character no-undo.
  empty temp-table allvendor.
  run server/getallvendors.p (input pStatus,
                             output table allvendor,
                             output tLoadedAllVendors,
                             output std-ch).
  
  /* if unsuccessful, show a message */
  if not tLoadedAllVendors
   then 
    do: std-lo = false.
        publish "GetAppDebug" (output std-lo).
        if std-lo 
         then message "LoadApVendor failed: " + std-ch view-as alert-box warning.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadSysActionEntity Include 
PROCEDURE LoadSysActionEntity :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* call the server */ 
  define output parameter table for sysaction.
  define output parameter cEntity       as character no-undo.
  define output parameter cDestinations as character no-undo.
  
  define buffer sysaction for sysaction.

  empty temp-table sysaction.
  run server/getsysdestentity.p ( output cEntity,
                                  output cDestinations,
                                  output table sysaction,
                                  output tLoadedSysAction,
                                  output std-ch).
  
  /* if unsuccessful, show a message if debug */
  if not tLoadedSysAction
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadSysActionEntity failed: " + std-ch view-as alert-box warning.
      return. 
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadSysDests Include 
PROCEDURE LoadSysDests :
/*------------------------------------------------------------------------------
@description Loads the destinations
------------------------------------------------------------------------------*/
  define input parameter pEntity      as character no-undo.
  define input parameter pDestination as character no-undo.
  define input parameter pAction      as character no-undo.
  define output parameter table for sysdest.
        
  /* call the server */
  empty temp-table sysdest.

  run server/getsysdest.p (input  pEntity, 
                           input "ALL",
                           input  pDestination,
                           input  pAction, 
                           output table sysdest,
                           output tLoadedSysDest,
                           output std-ch).
  
  /* if unsuccessful, show a message if debug */
  if not tLoadedSysDest
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "LoadSysDest failed: " + std-ch view-as alert-box warning.
      return. 
    end.

  /* Used to set status message on screen */
  publish "SetCurrentValue" ("RefreshSysDest", true).  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifySysDest Include 
PROCEDURE ModifySysDest :
/*------------------------------------------------------------------------------
@description Modifies the system destination
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter table      for tempsysdest.
  define output parameter oplSuccess as logical no-undo.
  define output parameter opcMsg     as character  no-undo.
    
  /* buffers */
  define buffer tempsysdest for tempsysdest.

  /* check for availability of record before making the server call. */
  find first tempsysdest no-error.
  if not available tempsysdest
   then return.
  
  /* call the server */
  run server/modifysysdest.p (input table tempsysdest, 
                              output oplSuccess, 
                              output opcMsg).
  
  if not oplSuccess
   then return.
  
  /* Caching of data */
  find first sysdest where sysdest.destID = tempsysdest.destID no-error.
  if not available sysdest
   then
    do: 
       assign 
         opcMsg     = "Internal Error of missing data."  
         oplSuccess = false
         . 
       return.
    end.
  
  buffer-copy tempsysdest to sysdest.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewSysDest Include 
PROCEDURE NewSysDest :
/*------------------------------------------------------------------------------
@description Creates a new system destination
------------------------------------------------------------------------------*/
  /* parameters */
  define input  parameter table       for tempsysdest.
  define output parameter opiDestID   as integer  no-undo.
  define output parameter oplSuccess  as logical  no-undo.
  define output parameter opcMsg      as character  no-undo.
    
  /* buffers */
  define buffer tempsysdest for tempsysdest.

  /* check for availability of record before making the server call. */
  find first tempsysdest no-error.
  if not available tempsysdest
   then return.
  
  /* call the server */
  run server/newsysdest.p (input table tempsysdest,
                           output opiDestID,
                           output oplSuccess, 
                           output opcMsg).
  
  if not oplSuccess
   then return.
  
  /* Caching of data */
  create sysdest.
  buffer-copy tempsysdest except tempsysdest.destID  to sysdest.
  sysdest.destID = opiDestID.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysDestChanged Include 
PROCEDURE SysDestChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity   as character no-undo.
  define input parameter pEntityID as character no-undo.
  define input parameter pAction   as character no-undo.
  
  /* refresh the alert */
  refreshSysDest(pEntity, pEntityID, pAction).
  
  /* publish that the data changed */
  publish "SysDestDataChanged".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysDestDataDump Include 
PROCEDURE SysDestDataDump :
/*------------------------------------------------------------------------------
@description Dump the sysdest and tempsysdest temp-tables into files
------------------------------------------------------------------------------*/
  /* variables */
  define variable tPrefix as character no-undo.
  
  /* get the prefix */
  publish "GetAppUser" (output tPrefix).
  tPrefix = tPrefix + "_".
  
  /* delete the current temp files */
  publish "DeleteTempFile" ("DataSysDest", output std-lo).
  publish "DeleteTempFile" ("DataTempSysDest", output std-lo).
  
  /* Active temp-tables */
  temp-table sysdest:write-xml("file", tPrefix + "sysdest.xml").
  publish "AddTempFile" ("DataSysDest", tPrefix + "SysDest.xml").
  
  /* "Temp" temp-tables */
  temp-table tempsysdest:write-xml("file", tPrefix + "TempSysDest.xml").
  publish "AddTempFile" ("DataSysDest", tPrefix + "TempSysDest.xml").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysDestESB Include 
PROCEDURE SysDestESB :
/*------------------------------------------------------------------------------
@description Runs when an esb message is received
------------------------------------------------------------------------------*/
  /* parameters */
  define input parameter pEntity as character no-undo.
  define input parameter pAction as character no-undo.
  define input parameter pKey as character no-undo.
  
  /* exit if not correct entity */
  if pEntity <> "sysdest"
   then next.
  
  /* get the next number */
  std-in = 1.
  for each sysdestesbmsg no-lock:
    std-in = std-in + 1.
  end.
  
  /* create the esb message */
  create sysdestesbmsg.
  assign
    sysdestesbmsg.seq    = std-in
    sysdestesbmsg.rcvd   = now
    sysdestesbmsg.entity = pEntity
    sysdestesbmsg.action = pAction
    sysdestesbmsg.keyID  = pKey
    .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SysDestTimer Include 
PROCEDURE SysDestTimer :
/*------------------------------------------------------------------------------
@description Triggered by appstart.i running on a timer
------------------------------------------------------------------------------*/
  /* buffers */
  define buffer sysdestesbmsg for sysdestesbmsg.

  /* loop through the esb messages */
  for each sysdestesbmsg exclusive-lock
  break by sysdestesbmsg.keyID
        by sysdestesbmsg.rcvd descending:

    /* if the sysdest is loaded and the we have a messages */
    if tLoadedSysDest and first-of(SysDestesbmsg.keyID) 
     then run SysDestChanged (sysdestesbmsg.keyID).
     
    /* delete the esb message and set the last sync */
    delete sysdestesbmsg.
    publish "SetLastSync".
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshSysDest Include 
FUNCTION refreshSysDest RETURNS logical
  ( input pEntity as character,
    input pEntityID as character,
    input pAction as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* parameters */
  define variable lSuccess as logical no-undo.
  
  /* buffers */
  define buffer tempsysdest for tempsysdest.
  define buffer sysdest for sysdest.
  
  /* call the server */
  empty temp-table tempsysdest.
  run server/getsysdest.p (input  pEntity, 
                           input  pEntityID,
                           input  "ALL",
                           input  pAction, 
                           output table tempsysdest,
                           output lSuccess,
                           output std-ch).

  /* if unsuccessful, show a message if debug is on */
  if not lSuccess
   then
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "refreshSysDest failed: " + std-ch view-as alert-box warning.
      RETURN lSuccess.  
    end.
  
  /* Used to set status message on screen */
  publish "SetCurrentValue" ("RefreshSysDest", true).  
  
  /* otherwise, delete all the system destination records for the agent/action */
  for each sysdest exclusive-lock
    where sysdest.entityType = pEntity
      and sysdest.entityID   = pEntityID
      and sysdest.action     = pAction:
       
    delete sysdest.
  end. 
  
  /* put all temp destinations to the system destinations back */
  for each tempsysdest no-lock:
    create sysdest.
    buffer-copy tempsysdest to sysdest.
  end.
  
  RETURN lSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

