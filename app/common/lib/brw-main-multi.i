&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : lib/brw-main-multi.i
    Author      : John Oliver
    Date        : 02/16/2016
    
    Purpose     : Browse routines for row highlighting and column
                  sorting. Used for multiple child browse widgets within
                  the frame. The widgets can be in another child frame.
    
    Usage       : Include this file in the main block of a window
                  that contains a browse control. Pass in the name
                  of the browse widgets.
                  Requires lib/std-def.i
------------------------------------------------------------------------*/

&if defined(parentFrame) = 0 &then
&scoped-define parentFrame {&frame-name}
&endif

/* loop through the window to see get any browse widgets */
do std-in = 1 to num-entries("{&browse-list}"):
  std-ch = entry(std-in,"{&browse-list}").
  std-ha = GetWidgetByName(frame {&parentFrame}:handle, std-ch).
  if valid-handle(std-ha) and std-ha:type = "BROWSE"
   then
    do:
      /* Get list of browse column-handles for setting row colors */
      colHandleList = "".
      colHandle = std-ha:first-column.
      do while valid-handle(colHandle):
        colHandleList = colHandleList + (if colHandleList <> "" then "," else "") + string(colHandle).
        colHandle = colHandle:next-column.
      end.
      /* create a browse row in the temp table */
      create brw.
      assign
        brw.name = std-ha:name
        brw.colHandleList = colHandleList.
        .
      release brw.
      /* Enable column searching so that column sorting works */
      std-ha:column-movable = no.
      &if defined(noSort) = 0 &then
        std-ha:allow-column-searching = yes.
      &else
        if lookup(std-ch, "{&noSort}") > 0
         then std-ha:allow-column-searching = no.
         else std-ha:allow-column-searching = yes.
      &endif
    end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 2.95
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


