/*------------------------------------------------------------------------
    File        : ar-getentitytype.i
    Purpose     : Returns entity of AR records

    Syntax      :

    Description :

    Author(s)   : Rahul Sharma
    Created     : 04/15/2020
    Notes       :
  ----------------------------------------------------------------------*/
&IF DEFINED(EntityType) = 0
&THEN

FUNCTION getEntityType RETURNS CHARACTER
  ( cEntityType as character )  FORWARD.
  
FUNCTION getEntityType RETURNS CHARACTER
  ( cEntityType as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cEntityType = 'A' 
   then
    return 'Agent'. 
  else if cEntityType = 'O' 
   then
    return 'Organization'.  
  else if cEntityType = 'AF' 
   then
    return 'AgentFile'.  
  else
    return cEntityType.   /* Function return value. */

END FUNCTION.
&GLOBAL-DEFINE EntityType true
&ENDIF
