&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file lib/brw-totalData-start.i
@author J.Oliver
@date 09.30.2015
    
@description Creates a total row at the bottom of a browse widget
    
@usage Place this include file in a procedure called 
       'sortData' in a window that contains a browse 
       control after the include call to brw-sortData.i.
       It should also be called in 'windowResized'.
    
       Procedure syntax:
         PROCEDURE sortData :
         {lib/brw-sortData.i}
         {lib/brw-totalData.i}
         END PRODEDURE.
    
       Calling syntax:
         RUN sortData (dataSortBy).
                   
@notes The totals will occur on any column that is a number field
       (i.e. integer or decimal). To exclude any columns, add the
       columns in a parameter called &excludeColumn in a list.
  
       {lib/brw-totalData.i &excludeColumn="4,5,7"}
  
       The above will exclude columns 4, 5, and 7 from the total row.
       This include requires the following libraries to be included
       into the main procedure:
  
       {lib/brw-totalData-setup.i}
       {lib/brw-main.i}
       {lib/std-def.i}
  
       By default, the first column in the browse will be reserved
       for the label "Totals". If the first row should be totaled
       as well, use the following preprocessor in the call:
  
       {lib/brw-totalData.i &excludeTotal=true}
       
       If you don't want the total row to display, use the following
       preprocessor in the call:
       
       {lib/brw-totalData.i &noShow=true}
       
       The format of decimal values will have a decimal point in them.
       If you want only integer formats, use the following preprocessor:
       
       {lib/brw-totalData.i &integerOnly=true}
------------------------------------------------------------------------*/

&if defined(excludeColumn) = 0 &then
&scoped-define excludeColumn ""
&endif

&if defined(dlm) = 0 &then
&scoped-define dlm ,
&endif

&if defined(excludeTotal) = 0 &then
&scoped-define excludeTotal false
&endif

&if defined(noShow) = 0 &then
&scoped-define noShow false
&endif

&if defined(integerOnly) = 0 &then
&scoped-define integerOnly false
&endif

define variable iCol as decimal no-undo.
define variable iRow as decimal no-undo.
define variable iWidth as decimal no-undo.
define variable hTotal as widget-handle.
define variable iPos as int no-undo.
define variable i as int no-undo.
define variable dataType as character no-undo.
define variable scrValue as character no-undo.
define variable svFormat as character no-undo.
define variable hToolTip as character no-undo.

/* the preprocessor variables */
define variable excludeColumn as character no-undo.
define variable excludeTotal as logical no-undo.
define variable noShow as logical no-undo.
define variable integerOnly as logical no-undo.
excludeColumn = "{&excludeColumn}".
excludeTotal = logical({&excludeTotal}) no-error.
noShow = logical({&noShow}) no-error.
integerOnly = logical({&integerOnly}) no-error.

/* the array will hold the total column values */
if extent(totalCol) = ?
 then extent(totalCol) = browse {&browse-name}:num-columns.

/* clear out the array from previous run */
do std-in = 1 to extent(totalCol):
  totalCol[std-in] = 0.
end.

if query {&browse-name}:handle:prepare-string <> ? and countRows(temp-table {&first-table-in-query-{&browse-name}}:default-buffer-handle) > 0
 then
  do:
    /* handles for the total */
    def var hTotalQuery as handle no-undo.
    def var hTotalTable as handle no-undo.
    hTotalTable = temp-table {&first-table-in-query-{&browse-name}}:default-buffer-handle.

    /* create a query to see how many rows there are */
    create query hTotalQuery.
    hTotalQuery:add-buffer(hTotalTable).
    hTotalQuery:query-prepare(query {&browse-name}:handle:prepare-string).
    hTotalQuery:query-open().
    hTotalQuery:get-first().

    /* get the totals for each column and put in array called totalCol */
    repeat while not hTotalQuery:query-off-end:
      do i = 1 to browse {&browse-name}:num-columns:
        do std-in = 1 to hTotalTable:num-fields:
          std-ha = hTotalTable:buffer-field(std-in).
          if std-ha:name = browse {&browse-name}:get-browse-column(i):name
           then
            do:
              if lookup(string(i),excludeColumn,",") = 0
               then
                do:
                  std-de = decimal(std-ha:buffer-value) no-error.
                  if totalCol[i] <> lf-min and not error-status:error and std-ha:buffer-value <> ""
                   then totalCol[i] = totalCol[i] + decimal(std-ha:buffer-value).
                   else totalCol[i] = lf-min.
                end.
               else totalCol[i] = lf-min.
            end.
        end.
      end.
      hTotalQuery:get-next().
    end.
    hTotalQuery:query-close().
    delete object hTotalQuery no-error.
  end.
 else
  do std-in = 1 to extent(totalCol):
    totalCol[std-in] = lf-min.
  end.

/* reduce the height of the browse to make room for the total fields only if making the totals for the first time */
if not totalAdded 
  then
    do:
      browse {&browse-name}:height-pixels = browse {&browse-name}:height-pixels - 23.
    end.

/* create the totals row */
iRow = browse {&browse-name}:row + browse {&browse-name}:height-chars + 0.1.

/* clean up the last total row if any */
do std-in = 1 to extent(totalCol):
  std-ha = GetWidgetByName(frame {&frame-name}:handle,"totalRow" + string(std-in)).
  if valid-handle(std-ha)
   then delete widget std-ha.
end.

do std-in = 1 to extent(totalCol):
  std-ha = browse {&browse-name}:get-browse-column(std-in).
  /*message std-ha:name + " is " + string(totalCol[std-in]) view-as alert-box information buttons ok.*/
  iCol = std-ha:column + browse {&browse-name}:col.
  iWidth = std-ha:width-chars - 0.1.
  hToolTip = "".
  
  /* first row */
  if std-in = 1 and not excludeTotal
   then
    assign
      scrValue = "Totals"
      dataType = "CHARACTER"
      svFormat = "x(8)"
      .
   else
    /* check if valid row */
    if totalCol[std-in] = lf-min or totalCol[std-in] = 0
     then
      assign
        scrValue = ""
        dataType = "CHARACTER"
        svFormat = "x(8)"
        .
     else
      do:
        scrValue = string(totalCol[std-in]).
        dataType = "DECIMAL".
        svFormat = "".
        
        /* account for negative numbers */
        if totalCol[std-in] < 0
         then svFormat = svFormat + "(".
        
        /* loop through the absolute value of the number cast as an int64 */
        do i = length(string(int64(absolute(totalCol[std-in])))) to 1 by -1:
          if i modulo 3 = 0
           then svFormat = svFormat + (if i = length(string(int64(absolute(totalCol[std-in])))) then ">" else ",>").
           else svFormat = svFormat + (if i = 1 then "Z" else ">").
        end.
     
        /* if the number had a decimal value */
        if index(scrValue, ".") > 0 and not integerOnly
         then svFormat = svFormat + ".99".
         
        /* account for negative numbers */
        if totalCol[std-in] < 0
         then svFormat = svFormat + ")".
        
        iPos = iWidth * session:pixels-per-column - font-table:get-text-width-pixels(svFormat, 1).
        iPos = iPos / font-table:get-text-width-pixels(" ", 1).
        
        if iPos < 2
         then
          assign
            hToolTip = string(scrValue,replace(replace(replace(svFormat,">","9"),"Z","9"),".","9"))
            scrValue = "*"
            dataType = "CHARACTER"
            svFormat = "x(8)"
            .
         else svFormat = fill(" ",iPos) + svFormat.
      end.
      
  /* if the user doesn't want to show the total row, just set every field to blank */
  if noShow and not (std-in = 1 and not excludeTotal)
   then
    assign
      scrValue = ""
      dataType = "CHARACTER"
      svFormat = "x(8)"
      .
  
  create text hTotal no-error assign
    frame         = frame {&frame-name}:handle
    row           = iRow
    column        = iCol
    data-type     = dataType
    format        = svFormat
    screen-value  = scrValue
    visible       = true
    width         = iWidth
    font          = 1
    name          = "totalRow" + string(std-in)
    .
  
  if hToolTip <> ""
   then hTotal:tooltip = hToolTip.
end.
totalAdded = true.
&ANALYZE-RESUME


