&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*---------------------------------------------------------------------
@name sf-def.i
@description Include for repository calls

@author John Oliver
@version 1.0
@created 11.16.2018
@notes 
---------------------------------------------------------------------*/

/* Variables ---                                                        */
define variable tAuthToken as character no-undo.
define variable tURL       as character no-undo.
define variable tPostFile  as character no-undo.
define variable tZoneID    as character no-undo.
{lib/std-def.i}

/* Used to get the individual items returned from sharefile             */
/* items */
define variable tItemID          as character no-undo.
define variable tItemType        as character no-undo.
define variable tItemName        as character no-undo.
define variable tItemDisplayName as character no-undo.
define variable tItemDescription as character no-undo.
define variable tItemSize        as integer   no-undo.
define variable tItemCreatedDate as character no-undo.
define variable tItemCreatedBy   as character no-undo.
define variable tItemPath        as character no-undo.
define variable tItemParentID    as character no-undo.
define variable tItemFileCount   as integer   no-undo.

/* users */
define variable tUserID                   as character no-undo.
define variable tUserName                 as character no-undo.
define variable tUserPrimaryEmail         as character no-undo.
define variable tUserCompany              as character no-undo.
define variable tUserEmployee             as logical   no-undo.
define variable tUserAdmin                as logical   no-undo.
define variable tUserPassword             as logical   no-undo.
define variable tUserRequireDownloadLogin as logical   no-undo.
define variable tUserCanViewMySettings    as logical   no-undo.
define variable tUserDateFormat           as character no-undo.

/* files */
define variable tDownloadLink as character no-undo.
define variable tUploadLink   as character no-undo.

/* Used to tell which call is being made                                */
define variable isAuth           as logical no-undo initial false.
define variable isItem           as logical no-undo initial false.
define variable isItemParent     as logical no-undo initial false.
define variable isFile           as logical no-undo initial false.
define variable isUser           as logical no-undo initial false.
define variable isUserPreference as logical no-undo initial false.

/* temp tables */
{tt/doc.i     &tableAlias="repository-item"}
{tt/docuser.i &tableAlias="repository-user"}

/* Procedures ---                                                       */
{lib/run-http.i}

&if defined(repo-token) = 0 &then
&global-define repo-token tAuthToken
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getURL Include 
FUNCTION getURL RETURNS CHARACTER
  ( input pURL as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setAdminToken Include 
FUNCTION setAdminToken RETURNS LOGICAL
  ( output pMsg as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setUserToken Include 
FUNCTION setUserToken RETURNS LOGICAL
  ( input pUID as character,
    input pPass as character,
    output pMsg as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getURL Include 
FUNCTION getURL RETURNS CHARACTER
  ( input pURL as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable pValue as character no-undo.

  publish "GetRepositoryURL" (pURL, output pValue).
  publish "GetSystemParameter" (pURL, output pValue).
  RETURN pValue.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setAdminToken Include 
FUNCTION setAdminToken RETURNS LOGICAL
  ( output pMsg as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tAdminID as character no-undo.
  define variable tAdminPW as character no-undo.
  define variable pSuccess as logical   no-undo.

  publish "GetSystemParameter" ("RepositoryAdminID", output tAdminID).
  publish "GetSystemParameter" ("RepositoryAdminPW", output tAdminPW).
  run GetRepositoryToken (tAdminID, tAdminPW, output pSuccess, output pMsg).

  RETURN pSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setUserToken Include 
FUNCTION setUserToken RETURNS LOGICAL
  ( input pUID as character,
    input pPass as character,
    output pMsg as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable pSuccess as logical   no-undo.

  publish "GetRepositoryToken" (output {&repo-token}). /* client-side publish */
  run GetRepositoryToken (pUID, pPass, output pSuccess, output pMsg).

  RETURN pSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

