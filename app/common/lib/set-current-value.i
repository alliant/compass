&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

&if defined(state) <> 0 &then
/* set the state */
std-ch = "".
publish "GetCurrentValue" ("StateID", OUTPUT std-ch).
if std-ch = ""
 then publish "GetCurrentValue" ("ViewState", output std-ch).
if std-ch = "" or (std-ch > "" and lookup(std-ch, {&state}:list-item-pairs, {&state}:delimiter) = 0)
 then std-ch = entry(2, {&state}:list-item-pairs, {&state}:delimiter).
{&state}:screen-value = std-ch.
&if defined(noApply) = 0 &then
apply "VALUE-CHANGED" to {&state}.
&endif
&endif

&if defined(agent) <> 0 &then
/* set the agent */
std-ch = "".
publish "GetCurrentValue" ("AgentID", output std-ch).
if std-ch = ""
 then publish "GetCurrentValue" ("ViewAgent", OUTPUT std-ch).
if std-ch > ""
 then run AgentComboSet in this-procedure (std-ch).
&if defined(noApply) = 0 &then
apply "VALUE-CHANGED" to {&agent}.
&endif
&endif

&if defined(agentmanager) <> 0 &then
/* set the county */
std-ch = "".
publish "GetCurrentValue" ("AgentManager", output std-ch).
if std-ch = "" or (std-ch > "" and lookup(std-ch, {&agentmanager}:list-item-pairs, {&agentmanager}:delimiter) = 0)
 then std-ch = entry(2, {&agentmanager}:list-item-pairs, {&agentmanager}:delimiter).
{&agentmanager}:screen-value = std-ch.
&if defined(noApply) = 0 &then
apply "VALUE-CHANGED" to {&agentmanager}.
&endif
&endif

&if defined(mth) <> 0 &then
/* set the month */
std-ch = "".
publish "GetCurrentValue" ("PeriodMonth", output std-ch).
if std-ch = "" or (std-ch > "" and index({&mth}:list-item-pairs, std-ch) = 0)
 then std-ch = string(month(today)).
if index({&mth}:list-item-pairs, std-ch) = 0
 then std-ch = entry(2, {&mth}:list-item-pairs, {&mth}:delimiter).
{&mth}:screen-value = std-ch.
&if defined(noApply) = 0 &then
apply "VALUE-CHANGED" to {&mth}.
&endif
&endif

&if defined(yr) <> 0 &then
/* set the year */
std-ch = "".
publish "GetCurrentValue" ("PeriodYear", output std-ch).
if std-ch = "" or (std-ch > "" and index({&yr}:list-items, std-ch) = 0)
 then std-ch = string(year(today)).
if index({&yr}:list-items, std-ch) = 0
 then std-ch = entry(1, {&yr}:list-items, {&yr}:delimiter).
{&yr}:screen-value = std-ch.
&if defined(noApply) = 0 &then
apply "VALUE-CHANGED" to {&yr}.
&endif
&endif

&if defined(county) <> 0 &then
/* set the county */
std-ch = "".
publish "GetCurrentValue" ("County", output std-ch).
if std-ch = "" or (std-ch > "" and lookup(std-ch, {&county}:list-item-pairs, {&county}:delimiter) = 0)
 then std-ch = entry(2, {&county}:list-item-pairs, {&county}:delimiter).
{&county}:screen-value = std-ch.
&if defined(noApply) = 0 &then
apply "VALUE-CHANGED" to {&county}.
&endif
&endif

&if defined(alerttype) <> 0 &then
/* set the county */
std-ch = "".
publish "GetCurrentValue" ("AlertType", output std-ch).
if std-ch = "" or (std-ch > "" and lookup(std-ch, {&alerttype}:list-item-pairs, {&alerttype}:delimiter) = 0)
 then std-ch = entry(2, {&alerttype}:list-item-pairs, {&alerttype}:delimiter).
{&alerttype}:screen-value = std-ch.
&if defined(noApply) = 0 &then
apply "VALUE-CHANGED" to {&alerttype}.
&endif
&endif

&if defined(stat) <> 0 &then
/* set the county */
std-ch = "".
publish "GetCurrentValue" ("VeiwStatus", output std-ch).
if std-ch = "" or (std-ch > "" and lookup(std-ch, {&stat}:list-item-pairs, {&stat}:delimiter) = 0)
 then std-ch = entry(2, {&stat}:list-item-pairs, {&stat}:delimiter).
{&stat}:screen-value = std-ch.
&if defined(noApply) = 0 &then
apply "VALUE-CHANGED" to {&stat}.
&endif
&endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* **********************  Internal Procedures  *********************** */

