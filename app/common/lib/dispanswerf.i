&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* lib/dispanswerf.i
  q:   QuestionID
  seq: Number sequence of checkbox definitions q<seq>Y, etc.
  ----------------------------------------------------------------------*/
 /*------------------------------------------------------------------------
    File        :   lib/dispanswerf.i
    Modification:
    Date          Name      Description
    11/14/2016    AC        Implement question priority = 0 functionality.
  ----------------------------------------------------------------------*/
   
&IF defined(exclude-if-define) = 0 &then
  define variable questionpriority as integer no-undo .
  &global-define exclude-if-define true 
&ENDIF

publish "GetQuestionPriority" (input {&seq}, output questionpriority).

publish "getQuestionAnswer" (input {&seq}, output std-ch).
q{&seq}:screen-value = std-ch.

if valid-handle(b{&seq}) 
then
do: std-lo = false.
    publish "GetHasFinding" ({&seq}, output std-lo).
    if not std-lo 
     then publish "GetHasBestPractice" ({&seq}, output std-lo).
    if std-lo 
     then b{&seq}:load-image("images/notes.bmp").
     else b{&seq}:load-image("images/notesblank.bmp").
    if questionpriority eq 0 then
     b{&seq}:sensitive = false.      
    else
     b{&seq}:sensitive = true.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


