/*------------------------------------------------------------------------
@name IsNumeric
@description Function to check if a string is all numeric
@author Bryan Johnson
@version 1.0 3/17/2017
@notes
----------------------------------------------------------------------*/

&IF DEFINED(IsNumericDefined) = 0 &THEN
&GLOBAL-DEFINE IsNumericDefined TRUE
FUNCTION isNumeric RETURNS LOGICAL
  ( input pValue as char ) :

  def var i as int.

  if pValue = "" or pValue = ? then 
  return false.

  do i = 1 to length(pValue):
    if index("0123456789", substring(pValue,i,1)) = 0 then
    return false.
  end.

  RETURN true.

END FUNCTION.
&ENDIF