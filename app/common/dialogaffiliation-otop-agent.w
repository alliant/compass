&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File:dialogaffiliation-otop.w 

  Description:Dialog to create/edit an affiliation from organization to person

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author:Shubham 

  Created:04.24.2020 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/
/* ***************************  Definitions  ************************** */
/* Temp-table Definitions ---                                           */
                                          

create widget-pool.

{tt/affiliation.i}
{tt/affiliation.i  &tableAlias="tAffiliation"}
{lib/std-def.i}
{lib/amd-def.i}
{lib/get-column.i}
{lib/brw-multi-def.i}

{tt/syscode.i}
{tt/syscode.i &tableAlias="tsysCode"}

{tt/tag.i}
{tt/tag.i &tableAlias="ttag"}   
{tt/tag.i &tableAlias="temptag"}
{tt/tag.i &tableAlias="tttag"}
{tt/tag.i &tableAlias="deltag"}


/* Parameters Definitions ---    */ 
define input  parameter hFileDataSrv as handle no-undo.
define input  parameter table for affiliation.
define input  parameter ipcOrgID    as character no-undo.
define input  parameter ipcOrgName  as character no-undo.
define output parameter oplSuccess  as logical   no-undo.

/* Variables defined to be used in IP enableDisableSave. */
define variable chTrackOrganization  as character no-undo.
define variable chTrackPerson        as character no-undo.
define variable chTrackOtoP          as character no-undo.
define variable chTrackPtoO          as character no-undo.
define variable loTrackPerson        as logical   no-undo.
define variable chTrackNotes         as character no-undo.
define variable cList                as character no-undo.

define variable cAtoB                as character no-undo.
define variable cBtoA                as character no-undo.
define variable iCount               as integer   no-undo.
define variable iAtoB                as integer   no-undo.
define variable iBtoA                as integer   no-undo.
define variable lAtoB                as logical   no-undo.
define variable lBtoA                as logical   no-undo.

define variable opiTagID            as integer   no-undo.
define variable cCurrUser                as character no-undo.
define variable cCurrentUID                as character no-undo. 
define variable chTag                 as character no-undo.
define variable cPersonID            as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwTags

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tag

/* Definitions for BROWSE brwTags                                       */
&Scoped-define FIELDS-IN-QUERY-brwTags tag.name   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwTags   
&Scoped-define SELF-NAME brwTags
&Scoped-define QUERY-STRING-brwTags FOR EACH tag by tag.name
&Scoped-define OPEN-QUERY-brwTags OPEN QUERY {&SELF-NAME} FOR EACH tag by tag.name.
&Scoped-define TABLES-IN-QUERY-brwTags tag
&Scoped-define FIRST-TABLE-IN-QUERY-brwTags tag


/* Definitions for DIALOG-BOX Dialog-Frame                              */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fltag bOrgLookup cbNatureOfAToB ~
cbNatureOfBToA tcPerson edNotes BtnCancel brwTags bTagDelete RECT-132 
&Scoped-Define DISPLAYED-OBJECTS fltag cbNatureOfAToB fOrganization ~
flperson cbNatureOfBToA tcPerson edNotes 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openTag Dialog-Frame 
FUNCTION openTag RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bOrgLookup DEFAULT  NO-FOCUS
     LABEL "Lookup" 
     SIZE 4.6 BY 1.14 TOOLTIP "Organization lookup".

DEFINE BUTTON bTagDelete DEFAULT  NO-FOCUS
     LABEL "Tag Delete" 
     SIZE 4.6 BY 1.14 TOOLTIP "Organization lookup".

DEFINE BUTTON bTagGo DEFAULT  NO-FOCUS
     LABEL "Tag Go" 
     SIZE 4.6 BY 1.14 TOOLTIP "Add tag".

DEFINE BUTTON BtnCancel AUTO-END-KEY DEFAULT 
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Cancel".

DEFINE BUTTON BtnOK AUTO-GO DEFAULT 
     LABEL "Create" 
     SIZE 15 BY 1.14 TOOLTIP "Create".

DEFINE BUTTON btnTurnDown  NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "to" 
     SIZE 4.6 BY 1.14.

DEFINE BUTTON btnTurnLeft  NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "is a(n)" 
     SIZE 4.6 BY 1.14.

DEFINE BUTTON btnTurnRight  NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "is a(n)" 
     SIZE 4.6 BY 1.14.

DEFINE BUTTON btnTurnUp  NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "is a(n)" 
     SIZE 4.6 BY 1.14.

DEFINE VARIABLE cbNatureOfAToB AS CHARACTER 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN
     SIZE 21.2 BY 1 NO-UNDO.

DEFINE VARIABLE cbNatureOfBToA AS CHARACTER 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN
     SIZE 21.2 BY 1 NO-UNDO.

DEFINE VARIABLE edNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 32 BY 5 NO-UNDO.

DEFINE VARIABLE flperson AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 33.4 BY 1 NO-UNDO.

DEFINE VARIABLE fltag AS CHARACTER FORMAT "X(256)":U 
     LABEL "Tag" 
     VIEW-AS FILL-IN 
     SIZE 31.6 BY 1 NO-UNDO.

DEFINE VARIABLE fOrganization AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 33.4 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-132
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 92.2 BY 8.24.

DEFINE VARIABLE tcPerson AS LOGICAL INITIAL no 
     LABEL "Control Person" 
     VIEW-AS TOGGLE-BOX
     SIZE 17.6 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwTags FOR 
      tag SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwTags
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwTags Dialog-Frame _FREEFORM
  QUERY brwTags DISPLAY
      tag.name label ""    format "x(20)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-LABELS NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 31.6 BY 4.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN TOOLTIP "Tags".


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bTagGo AT ROW 7.19 COL 89 WIDGET-ID 326 NO-TAB-STOP 
     fltag AT ROW 7.29 COL 55 COLON-ALIGNED WIDGET-ID 322
     bOrgLookup AT ROW 2.86 COL 90.6 WIDGET-ID 292 NO-TAB-STOP 
     cbNatureOfAToB AT ROW 1.71 COL 33.8 COLON-ALIGNED NO-LABEL WIDGET-ID 222
     fOrganization AT ROW 3 COL 2.6 NO-LABEL WIDGET-ID 40
     btnTurnDown AT ROW 1.86 COL 57 WIDGET-ID 304 NO-TAB-STOP 
     flperson AT ROW 3 COL 54.8 COLON-ALIGNED NO-LABEL WIDGET-ID 214
     btnTurnLeft AT ROW 4 COL 57 WIDGET-ID 306 NO-TAB-STOP 
     btnTurnRight AT ROW 1.86 COL 31.2 WIDGET-ID 300 NO-TAB-STOP 
     cbNatureOfBToA AT ROW 4.24 COL 33.8 COLON-ALIGNED NO-LABEL WIDGET-ID 224
     btnTurnUp AT ROW 4 COL 31.2 WIDGET-ID 308 NO-TAB-STOP 
     tcPerson AT ROW 6.24 COL 4.8 WIDGET-ID 28
     edNotes AT ROW 8.43 COL 4 NO-LABEL WIDGET-ID 314
     BtnOK AT ROW 14.43 COL 33.2 WIDGET-ID 208
     BtnCancel AT ROW 14.43 COL 49.8 WIDGET-ID 204
     brwTags AT ROW 8.48 COL 57 WIDGET-ID 2000
     bTagDelete AT ROW 8.43 COL 89 WIDGET-ID 324 NO-TAB-STOP 
     "of" VIEW-AS TEXT
          SIZE 3 BY .62 AT ROW 2.14 COL 62.4 WIDGET-ID 294
     "Organization" VIEW-AS TEXT
          SIZE 13 BY .62 AT ROW 2.29 COL 2.4 WIDGET-ID 310
     "Person" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 2.38 COL 81.4 RIGHT-ALIGNED WIDGET-ID 312
     "Notes:" VIEW-AS TEXT
          SIZE 6.4 BY .62 AT ROW 7.52 COL 4 WIDGET-ID 316
     "Attributes" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 5.48 COL 3.8 WIDGET-ID 330
     "is" VIEW-AS TEXT
          SIZE 2 BY .62 AT ROW 4.33 COL 62.6 WIDGET-ID 296
     "of" VIEW-AS TEXT
          SIZE 2.2 BY .62 AT ROW 4.24 COL 29.2 WIDGET-ID 298
     "is" VIEW-AS TEXT
          SIZE 2 BY .62 AT ROW 2.05 COL 29.2 WIDGET-ID 302
     RECT-132 AT ROW 5.81 COL 2.8 WIDGET-ID 328
     SPACE(1.19) SKIP(2.04)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "New Affiliation - Organization to Person"
         DEFAULT-BUTTON BtnOK CANCEL-BUTTON BtnCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwTags BtnCancel Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       brwTags:ALLOW-COLUMN-SEARCHING IN FRAME Dialog-Frame = TRUE
       brwTags:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE
       brwTags:COLUMN-MOVABLE IN FRAME Dialog-Frame         = TRUE.

/* SETTINGS FOR BUTTON bTagGo IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BtnOK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btnTurnDown IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btnTurnLeft IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btnTurnRight IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btnTurnUp IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       edNotes:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN flperson IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flperson:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fOrganization IN FRAME Dialog-Frame
   NO-ENABLE ALIGN-L                                                    */
ASSIGN 
       fOrganization:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR TEXT-LITERAL "Person"
          SIZE 7.4 BY .62 AT ROW 2.38 COL 81.4 RIGHT-ALIGNED            */

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwTags
/* Query rebuild information for BROWSE brwTags
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tag by tag.name.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwTags */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* New Affiliation - Organization to Person */
do:
  oplSuccess = false.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOrgLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOrgLookup Dialog-Frame
ON CHOOSE OF bOrgLookup IN FRAME Dialog-Frame /* Lookup */
do:
  run openDialog in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwTags
&Scoped-define SELF-NAME brwTags
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTags Dialog-Frame
ON ROW-DISPLAY OF brwTags IN FRAME Dialog-Frame
DO:
  {lib/brw-rowDisplay.i }
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTags Dialog-Frame
ON START-SEARCH OF brwTags IN FRAME Dialog-Frame
DO:
  {lib/brw-startSearch.i }
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bTagDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bTagDelete Dialog-Frame
ON CHOOSE OF bTagDelete IN FRAME Dialog-Frame /* Tag Delete */
do:
  run deletebrwTag in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bTagGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bTagGo Dialog-Frame
ON CHOOSE OF bTagGo IN FRAME Dialog-Frame /* Tag Go */
do:
  run addTag in this-procedure. 
  fltag:screen-value = "" .
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnCancel Dialog-Frame
ON CHOOSE OF BtnCancel IN FRAME Dialog-Frame /* Cancel */
do:
  oplSuccess = false.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnOK Dialog-Frame
ON CHOOSE OF BtnOK IN FRAME Dialog-Frame /* Create */
do:
  run validateaffiliation in this-procedure (output std-ch).
  if std-ch <> ""
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return no-apply.
    end.
  
  run addaffiliation in this-procedure.

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbNatureOfAToB
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbNatureOfAToB Dialog-Frame
ON VALUE-CHANGED OF cbNatureOfAToB IN FRAME Dialog-Frame
do:
  define variable cValue as character no-undo.
  iAtoB = 0.
  do iCount = 1 to num-entries(cList):
    if iCount mod 2 = 0 
     then
      do:      
        cValue = entry(iCount,cList,",").
        if cValue = cbNatureOfAToB:input-value
         then
          cbNatureOfBToA:screen-value = entry(iCount - 1,cList,",").  
      end.
  end.
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbNatureOfBToA
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbNatureOfBToA Dialog-Frame
ON VALUE-CHANGED OF cbNatureOfBToA IN FRAME Dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edNotes Dialog-Frame
ON VALUE-CHANGED OF edNotes IN FRAME Dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fltag
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fltag Dialog-Frame
ON VALUE-CHANGED OF fltag IN FRAME Dialog-Frame /* Tag */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tcPerson
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tcPerson Dialog-Frame
ON VALUE-CHANGED OF tcPerson IN FRAME Dialog-Frame /* Control Person */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */
 {lib/brw-main.i}


/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? 
 then 
  frame {&frame-name}:parent = active-window.

btnTurnRight:load-image            ("images/s-arrow-turn-right.bmp"). 
btnTurnRight:load-image-insensitive("images/s-arrow-turn-right.bmp"). 
btnTurnDown:load-image             ("images/s-arrow-turn-down.bmp"). 
btnTurnDown:load-image-insensitive ("images/s-arrow-turn-down.bmp"). 
btnTurnLeft:load-image             ("images/s-arrow-turn-left.bmp"). 
btnTurnLeft:load-image-insensitive ("images/s-arrow-turn-left.bmp").
btnTurnUp:load-image               ("images/s-arrow-turn-up.bmp"). 
btnTurnUp:load-image-insensitive   ("images/s-arrow-turn-up.bmp").
bOrgLookup:load-image              ("images/s-lookup.bmp").
bOrgLookup:load-image-insensitive  ("images/s-lookup-i.bmp").
bTagdelete:load-image              ("images/s-delete.bmp").
bTagdelete:load-image-insensitive  ("images/s-delete-i.bmp").
bTagGo:load-image                  ("images/s-add.bmp").
bTagGo:load-image-insensitive      ("images/s-add-i.bmp").

  publish "GetCredentialsName" (output cCurrUser).
  publish "GetCredentialsID" (output cCurrentUID).

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.
 
  opentag().
  run setNature   in this-procedure.
  run displayData in this-procedure.
  
  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addAffiliation Dialog-Frame 
PROCEDURE addAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tagList as character initial "" no-undo.
  
  do with frame {&frame-name}:
  end.
  
  find first affiliation no-error.
  
  if not available affiliation 
   then
    create affiliation.
  
  assign
      affiliation.partnerA       = fOrganization:private-data
      affiliation.partnerAType   = {&OrganizationCode}
      affiliation.partnerB       = flPerson:private-data
      affiliation.partnerBType   = {&PersonCode}
      affiliation.natureOfAToB   = cbNatureOfAToB:input-value
      affiliation.natureOfBToA   = cbNatureOfBToA:input-value  
      affiliation.isCtrlPerson   = tcPerson:input-value 
      affiliation.notes          = edNotes:input-value
      .
  if brwtags:num-entries ne 0
   then
    do:
      for each tag :
        if tagList = ""
         then
          tagList = tag.name.
        else
         tagList = tagList + "," + tag.name.
      end.
      assign
          affiliation.tag = ""
          affiliation.tag = tagList.  
    end.
      
  if affiliation.affiliationId = 0 
   then
    do:
      /* Save the affiliations. */
      run newaffiliation in this-procedure (input  table affiliation,
                                            output oplSuccess,
                                            output std-ch).    
      if not oplSuccess 
       then 
        do:
          if lookup("is not active.",std-ch," ") = 0
           then
            message std-ch
              view-as alert-box error buttons ok.
          return.
        end.
    end.
  else
   do:
     run modifyaffiliation in this-procedure (input  table affiliation,
                                              output oplSuccess,
                                              output std-ch).
     if not oplSuccess 
      then 
       do:
         message std-ch
             view-as alert-box error buttons ok.
         return.
       end. 
   end.
   
   if oplSuccess                                 and 
      chTrackOtoP  <> cbNatureOfAToB:input-value and
      chTrackPtoO  <> cbNatureOfBToA:input-value 
    then
     run newSysCode in this-procedure.
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addtag Dialog-Frame 
PROCEDURE addtag :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name} :
  end.
  define variable ctag as character no-undo.
  
  if fltag:screen-value = ""
   then
    do:
      message  "Please enter a tag" view-as alert-box.
      return no-apply.
    end.
  
  if flperson:screen-value = ""
   then
    do:
      message  "Please choose a person" view-as alert-box.
      return  no-apply.
    end.
  
  ctag = if fltag:screen-value begins "#" then fltag:screen-value else ("#" + fltag:screen-value) .
 
  for each tag :
   if ctag = tag.name
     then
      do :
        message "Tag already exist " view-as alert-box.
        return.
      end.
  end.
  
  create tag.
  tag.name        = ctag. 

  open query brwTags for each tag.
   
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deletebrwTag Dialog-Frame 
PROCEDURE deletebrwTag :
if not available tag
   then 
    return.
  
  message "Highlighted tag will be deleted." skip "Do you want to continue?"
    view-as alert-box question buttons yes-no
    title "Delete Tag"
    update lContinue as logical.
  
  if not lContinue
   then
    return.
  
  delete tag.
  
  open query brwTags for each tag.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deletetag Dialog-Frame 
PROCEDURE deletetag :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  define buffer deltag for deltag .
  
  
  if not can-find(first deltag )
   then
    return.
    
  for each deltag :                                   
    run untagentity in hFileDataSrv (input deltag.tagID,
                                     output std-lo).
                                     
    if not std-lo
     then
      return no-apply.               
  end. 
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  fOrganization:screen-value in frame Dialog-Frame = ipcOrgName .
  fOrganization:private-data = ipcOrgID.
  
  for first affiliation:
    frame Dialog-Frame:title = "Modify Affiliation - Organization to Person".
    BtnOK:label              = "Save".
    bOrgLookup:sensitive     = false.
    
    assign
        flperson:screen-value in frame Dialog-Frame = affiliation.partnerBName  
        flperson:private-data                       = affiliation.partnerB  
        cbNatureOfAToB:screen-value                 = affiliation.natureOfAToB     
        cbNatureOfBToA:screen-value                 = affiliation.natureOfBToA     
        tcPerson:screen-value                       = string(affiliation.isCtrlPerson)
        edNotes:screen-value                        = affiliation.notes
        tcPerson:hidden                             = integer(affiliation.partnerB) = 0
        .
  end.
  assign
      chTrackOrganization = fOrganization:private-data
      chTrackPerson       = flPerson:private-data 
      chTrackOtoP         = cbNatureOfAToB:input-value
      chTrackPtoO         = cbNatureOfBToA:input-value
      loTrackPerson       = tcPerson:input-value
      chTrackNotes        = edNotes:input-value
      chTag               = ""
      no-error.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  BtnOK:sensitive = not (chTrackOrganization = fOrganization:input-value  and
                         chTrackPerson       = flPerson:private-data      and
                         chTrackOtoP         = cbNatureOfAToB:input-value and
                         chTrackPtoO         = cbNatureOfBToA:input-value and
                         loTrackPerson       = tcPerson:input-value       and
                         chTrackNotes        = edNotes:input-value        and
                         chTag               = fltag:input-value)
                         no-error.
  bTagGo:sensitive = fltag:input-value <> "".
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fltag cbNatureOfAToB fOrganization flperson cbNatureOfBToA tcPerson 
          edNotes 
      WITH FRAME Dialog-Frame.
  ENABLE fltag bOrgLookup cbNatureOfAToB cbNatureOfBToA tcPerson edNotes 
         BtnCancel brwTags bTagDelete RECT-132 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getNatureList Dialog-Frame 
PROCEDURE getNatureList :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcType as character no-undo.
  define output parameter opcList as character no-undo.
  
  if not can-find (first syscode where syscode.type = ipcType) 
   then
    publish "GetSysCodes" ({&NatureOfAffiliation},output table syscode).
    
  for each syscode where syscode.type = ipcType:
    opcList = if opcList = "" then syscode.description else opcList + "," + syscode.description.
  end.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyaffiliation Dialog-Frame 
PROCEDURE modifyaffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define input  parameter table for tAffiliation.
  define output parameter oplSuccess  as logical   no-undo.
  define output parameter opcMsg      as character no-undo.
  
  define buffer affiliation   for affiliation.
  define buffer tAffiliation  for tAffiliation.
  
  /* check for availability of record before making the server call. */
  find first tAffiliation no-error.
  if not available tAffiliation
   then
    return.
  
  run modifyAffiliation in hFileDataSrv (input table tAffiliation,
                                         output oplSuccess,
                                         output opcMsg).
  if not oplSuccess
   then
    return.
  
  find first affiliation 
    where affiliation.affiliationID = tAffiliation.affiliationID no-error.
  if not available affiliation
   then
    do:
      assign 
          opcMsg     = "Internal Error of missing data."  
          oplSuccess = false
          . 
      empty temp-table tAffiliation.
      return.
    end.
  
  buffer-copy tAffiliation to affiliation.

  empty temp-table tAffiliation.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newAffiliation Dialog-Frame 
PROCEDURE newAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define input  parameter table for tAffiliation.
  define output parameter oplSuccess       as logical   no-undo.
  define output parameter opcMsg           as character no-undo.
 
  define buffer affiliation   for affiliation.
  define buffer tAffiliation  for tAffiliation.
  
  /* check for availability of record before making the server call. */
  find first tAffiliation no-error.
  if not available tAffiliation
   then
    return.
 
  run newAffiliation in hFileDataSrv (input-output table tAffiliation,
                                      output oplSuccess,
                                      output opcMsg).
  if not oplSuccess
   then
    do:
      message opcMsg
       view-as alert-box warning.
      return.
    end.
    
  /* Caching of data */
  for each tAffiliation:
    create affiliation.
    buffer-copy tAffiliation to affiliation.
  end.
    
  empty temp-table tAffiliation.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newCode Dialog-Frame 
PROCEDURE newCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter cType        as character no-undo.
  define input  parameter cDesc        as character no-undo.   
  define output parameter oplSuccess   as logical    no-undo.
  define output parameter opcMsg       as character  no-undo.  

  define variable cCode as character no-undo.
  
  define buffer sysCode   for sysCode.
  
  for last sysCode no-lock:
    cCode = sysCode.code.
  end.
  
  empty temp-table tSysCode.
  
  create tSysCode.
  assign
      tSysCode.codetype = {&NatureOfAffiliation}
      tSysCode.code = string(integer(cCode) + 1)
      tSysCode.type = cType
      tSysCode.description = cDesc
      .
 
  /* Server Call */
  run server/newsyscode.p (input table tSysCode,                               
                           output oplSuccess,
                           output opcMsg).

  if not oplSuccess
   then
    return.
  
  /* Caching of data */
  create sysCode.
  buffer-copy tSysCode to sysCode.  

  empty temp-table tSysCode.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newSysCode Dialog-Frame 
PROCEDURE newSysCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  lAtoB = logical(lookup(cbNatureOfAToB:input-value,cList)).
  lBtoA = logical(lookup(cbNatureOfBtoA:input-value,cList)).

  if not lAtoB or not lBtoA 
   then
    do:
      run newCode in this-procedure (input "PtoO",
                                     input cbNatureOfAToB:input-value + "," + cbNatureOfBtoA:input-value,
                                     output std-lo,
                                     output std-ch).
      if not std-lo
       then
        message std-ch
             view-as alert-box information buttons ok.
    end.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openDialog Dialog-Frame 
PROCEDURE openDialog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
  

  define variable cName                as character no-undo.
  define variable caffiliationPersonID as character no-undo.
  
  
  cPersonID = "".
  if available affiliation
   then
    caffiliationPersonID = affiliation.partnerB.
    
  run dialogpersonlookup.w(input  caffiliationPersonID,
                           input  false,        /* True if add "New" */
                           input  false,        /* True if add "Blank" */
                           input  false,        /* True if add "ALL" */
                           output cPersonID,
                           output cName,
                           output std-lo).
   
  if not std-lo 
   then
    return no-apply.
    
  flPerson:screen-value = if cPersonID = "" then "" else cName . 
  flPerson:private-data = cPersonID.
  
  if flPerson:input-value ne cPersonID
   then
    run enableDisableSave in this-procedure. 
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveTag Dialog-Frame 
PROCEDURE saveTag :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  define buffer temptag for temptag .
  define buffer ttag    for ttag    .
  
  find first temptag no-error.
  if not available temptag
   then
    return.
  
  for each temptag :
    empty temp-table ttag.
    create ttag.
    buffer-copy temptag to ttag.
  
    run newusertag in hFileDataSrv (input table ttag,
                                    output opiTagID,
                                    output oplSuccess).
                                     
    if not oplSuccess
     then
      return no-apply.
      
  end.
                                      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setNature Dialog-Frame 
PROCEDURE setNature :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  run GetNatureList in this-procedure (input "PtoO", output cList).
                        
  do iCount = 1 to num-entries(cList): 
    if iCount mod 2 <> 0 
     then
      do:
        if logical(lookup(entry(iCount,cList,","),cBtoA)) 
         then 
          next.
        cBtoA = if cBToA = "" then entry(iCount,cList,",") else cBToA + "," + entry(iCount,cList,",").
      end.
    else
     do:
       if logical(lookup(entry(iCount,cList,","),cAtoB)) 
        then 
         next.
       cAToB = if cAToB = "" then entry(iCount,cList,",") else cAToB + "," + entry(iCount,cList,",").
     end.
  end.

  cbNatureOfAToB:list-items = cAToB.
  cbNatureOfBToA:list-items = cBtoA.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validateAffiliation Dialog-Frame 
PROCEDURE validateAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-ch as character no-undo.
  
  do with frame {&frame-name}:
  end.
  
  if flPerson:private-data = ? 
   then
    std-ch = "Person cannot be blank".

  if cbNatureOfAToB:input-value = "" or cbNatureOfAToB:input-value = ? or 
     cbNatureOfBToA:input-value = "" or cbNatureOfBToA:input-value = ?
   then
    std-ch = "Please enter nature of affiliation".
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openTag Dialog-Frame 
FUNCTION openTag RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
 define variable iInc as integer no-undo.
 
 empty temp-table tag.
  
 find first affiliation no-error.
  if not available affiliation
   then
    return false.
  
  for first affiliation :
    do iInc = 1 to num-entries(affiliation.tag) : /* creating temp table from list */
          create tag.
          tag.name     = entry(iInc,affiliation.tag).
        end.  /* do iInc = 1 to num-entries(ttaffiliation.tag) */
  end.
  
  OPEN QUERY brwTags FOR EACH tag .

  apply 'value-changed' to brwTags in frame Dialog-Frame.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

