&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* referenceregion.w
   REFERENCE of REGIONs
   9.14.2018
   
  @modification
      date         Name           Description
      06/09/2022   SA             Task 93485 - removed displaying RegiogID 
                                  from browse(grid).
   */

CREATE WIDGET-POOL.

{tt/region.i}

{lib/std-def.i}
{lib/add-delimiter.i}

def var hData as handle no-undo.

define variable tCurrColor as integer no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES region

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData region.regionDesc region.stateDesc   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH region
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH region.
&Scoped-define TABLES-IN-QUERY-brwData region
&Scoped-define FIRST-TABLE-IN-QUERY-brwData region


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwData bDelete bExport bModify bNew ~
bRefresh 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete the selected Region".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify the selected Region".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add a new Region".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE RECTANGLE rActions
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 38 BY 2.05.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      region SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      region.regionDesc width 28
region.stateDesc width 28
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 73 BY 17.14 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwData AT ROW 3.38 COL 2 WIDGET-ID 200
     bDelete AT ROW 1.38 COL 32 WIDGET-ID 14 NO-TAB-STOP 
     bExport AT ROW 1.38 COL 9.8 WIDGET-ID 2 NO-TAB-STOP 
     bModify AT ROW 1.38 COL 24.6 WIDGET-ID 12 NO-TAB-STOP 
     bNew AT ROW 1.38 COL 17.2 WIDGET-ID 10 NO-TAB-STOP 
     bRefresh AT ROW 1.38 COL 2.6 WIDGET-ID 4 NO-TAB-STOP 
     rActions AT ROW 1.24 COL 2 WIDGET-ID 8
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 74.8 BY 19.71 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Regions"
         HEIGHT             = 19.71
         WIDTH              = 74.8
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 147.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 147.2
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR RECTANGLE rActions IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH region.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Regions */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Regions */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Regions */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME fMain /* Delete */
DO:
  if not available region
   then return.
   
  run actionDelete in this-procedure (region.regionID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME fMain /* Modify */
DO:
  if not available region
   then return.
   
  run actionModify in this-procedure (region.regionID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
DO:
  run actionNew in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  publish "LoadRegions".
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  apply "CHOOSE" to bModify.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  if region.seq > 1
   then
    do:
      region.regionDesc:screen-value in browse {&browse-name} = "".
    end.
   
  do std-in = 1 to num-entries(colHandleList):
    colHandle = handle(entry(std-in, colHandleList)).
    if valid-handle(colHandle)
     then colHandle:bgcolor = region.rowColor.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}
{lib/set-buttons.i}
{&browse-name}:allow-column-searching = false.

subscribe to "RegionDataChanged" anywhere.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  run getData in this-procedure.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionDelete C-Win 
PROCEDURE actionDelete PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pRegionID as character no-undo.
  
  {lib/confirm-delete.i "Region"}
  
  publish "DeleteRegion" (pRegionID, output std-lo).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionModify C-Win 
PROCEDURE actionModify PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pRegionID as character no-undo.
  
  define variable tStateList as character no-undo.
  define variable tCancel as logical no-undo.

  UPDATE-BLOCK:
  repeat:
    run dialogregionmodify.w (input pRegionID,
                              output tStateList,
                              output tCancel).
    if tCancel 
     then leave UPDATE-BLOCK.
  
    publish "ModifyRegion" (pRegionID, tStateList, output std-lo).
    if not std-lo 
     then next UPDATE-BLOCK.
    
    leave.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionNew C-Win 
PROCEDURE actionNew PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tRegionID as char no-undo.
  define variable tRegionDesc as char no-undo.
  define variable tStateList as char no-undo.
  define variable tCancel as logical no-undo.

  UPDATE-BLOCK:
  repeat:
    run dialogregionnew.w (output tRegionID,
                           output tRegionDesc,
                           output tStateList,
                           output tCancel).
    if tCancel 
     then leave UPDATE-BLOCK.
  
    std-lo = false.
    publish "NewRegion" (tRegionID, tRegionDesc, tStateList, output std-lo).
    if not std-lo 
     then next UPDATE-BLOCK.
   
    leave.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE brwData bDelete bExport bModify bNew bRefresh 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwData:num-results = 0 
   then
    do: 
      MESSAGE "There is nothing to export" VIEW-AS ALERT-BOX warning BUTTONS OK.
      return.
    end.

  &scoped-define ReportName "Regions"
 
  run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwData.
  empty temp-table region.
  
  tCurrColor = {&evenColor}.
  publish "GetRegions" (output table region).
  if not can-find(first region)
   then
    do:
      run server/getregions.p ("",
                               output table region,
                               output std-lo,
                               output std-ch).
      if not std-lo
       then message "LoadRegions failed: " std-ch view-as alert-box warning.
    end.
   
  if can-find(first region)
  then
   do:
      dataSortDesc = not dataSortDesc.
      if dataSortBy = ""
       then dataSortBy = "regionID".
      run sortData (dataSortBy).
      enableButtons(can-find(first region)).
   end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RegionDataChanged C-Win 
PROCEDURE RegionDataChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run getData in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetDataSource C-Win 
PROCEDURE SetDataSource :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter p as handle.

 if valid-handle(p) 
  then hData = p.
 run getData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var tWhereClause as char no-undo.

  do with frame {&frame-name}:
    /* put the row color */
    for each region exclusive-lock break by region.regionID:
      if first-of(region.regionID)
       then tCurrColor = (if tCurrColor = {&oddColor} then {&evenColor} else {&oddColor}).
      
      region.rowColor = tCurrColor.
    end.
    {lib/brw-sortData.i}
    {&browse-name}:clear-sort-arrows().
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

 /* fMain components */
 brwData:width-pixels = frame fmain:width-pixels - 10.
 brwData:height-pixels = frame fMain:height-pixels - 51.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

