&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/*------------------------------------------------------------------------
  File: sfemaildialog.w
  Description: 
  Author:  D.Sinclair
  Created: 7.8.2012
------------------------------------------------------------------------*/

def input parameter pFile as char.
def input-output parameter pTo as char.
def input-output parameter pSubject as char.
def input-output parameter pBody as char.
def input-output parameter pNotify as logical.
def input-output parameter pExpiration as int.
def input-output parameter pDownloads as int.
def output parameter pSend as logical init false.

{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-34 tTo tSubject tNotify tBody ~
tExpiration tDownloads bSend bDone 
&Scoped-Define DISPLAYED-OBJECTS tTo tSubject tNotify tBody tExpiration ~
tDownloads 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bDone AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSend AUTO-GO 
     LABEL "Send" 
     SIZE 13 BY 1.14.

DEFINE VARIABLE tTo AS CHARACTER 
     LABEL "To" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 10
     LIST-ITEMS "Email Address" 
     DROP-DOWN
     SIZE 49.2 BY 1 NO-UNDO.

DEFINE VARIABLE tBody AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 56 BY 5.24 NO-UNDO.

DEFINE VARIABLE tDownloads AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Max Downloads" 
     VIEW-AS FILL-IN 
     SIZE 5.4 BY 1 TOOLTIP "Maximum number of times the file can be downloaded" NO-UNDO.

DEFINE VARIABLE tExpiration AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Expires In" 
     VIEW-AS FILL-IN 
     SIZE 5.6 BY 1 TOOLTIP "Number of days" NO-UNDO.

DEFINE VARIABLE tSubject AS CHARACTER FORMAT "X(256)":U 
     LABEL "Subject" 
     VIEW-AS FILL-IN 
     SIZE 50 BY 1 TOOLTIP "Enter the subject line" NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 72.8 BY 8.57.

DEFINE RECTANGLE RECT-34
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 26 BY 8.57.

DEFINE VARIABLE tNotify AS LOGICAL INITIAL no 
     LABEL "Notify on Download" 
     VIEW-AS TOGGLE-BOX
     SIZE 21.8 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tTo AT ROW 1.86 COL 15.6 COLON-ALIGNED WIDGET-ID 40
     tSubject AT ROW 3.14 COL 15 COLON-ALIGNED WIDGET-ID 8
     tNotify AT ROW 3.29 COL 77.4 WIDGET-ID 30
     tBody AT ROW 4.57 COL 17 NO-LABEL WIDGET-ID 26
     tExpiration AT ROW 4.67 COL 91.8 COLON-ALIGNED WIDGET-ID 32
     tDownloads AT ROW 5.95 COL 92 COLON-ALIGNED WIDGET-ID 34
     bSend AT ROW 10.52 COL 39 WIDGET-ID 22
     bDone AT ROW 10.52 COL 53
     "Options" VIEW-AS TEXT
          SIZE 9.2 BY .62 AT ROW 1.24 COL 76.8 WIDGET-ID 38
          FONT 6
     "Message" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.14 COL 3 WIDGET-ID 24
          FONT 6
     "Message:" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 4.57 COL 7 WIDGET-ID 28
     RECT-1 AT ROW 1.48 COL 2 WIDGET-ID 20
     RECT-34 AT ROW 1.48 COL 75.8 WIDGET-ID 36
     SPACE(0.79) SKIP(2.37)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Email Document Link" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

/* SETTINGS FOR RECTANGLE RECT-1 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tBody:RETURN-INSERTED IN FRAME fMain  = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Email Document Link */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDownloads
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDownloads fMain
ON LEAVE OF tDownloads IN FRAME fMain /* Max Downloads */
DO:
 if self:input-value < 1 
  then
       MESSAGE "At least one download must be permitted"
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tExpiration
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tExpiration fMain
ON LEAVE OF tExpiration IN FRAME fMain /* Expires In */
DO:
  if self:input-value < 1 
   then
        MESSAGE "Expiration must be at least one day"
         VIEW-AS ALERT-BOX INFO BUTTONS OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

publish "GetContacts" (output std-ch).
if pTo > "" and lookup(pTo, std-ch) = 0 
 then std-ch = (if std-ch > "" then "," else "") + pTo.
                                             
tTo:list-items in frame {&frame-name} = std-ch.
tTo = pTo.

if pSubject = "" 
 then tSubject = "Link to document: " + pFile.
 else tSubject = pSubject.

if pBody = "" 
 then tBody = "Click the following link to download the file:".
 else tBody = pBody.

if pNotify = ? 
 then tNotify = true.
 else tNotify = pNotify.

if pExpiration <= 0 or pExpiration > 99
 then tExpiration = 5.
 else tExpiration = pExpiration.

if pDownloads <= 0 or pDownloads > 99
 then tDownloads = 2.
 else tDownloads = pDownloads.

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
repeat ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  std-ch = tTo:screen-value in frame fMain.
  if num-entries(std-ch, "@") <> 2 
    or (num-entries(std-ch, "@") = 2
         and num-entries(entry(2, std-ch, "@"), ".") < 2)
   then
    do:
        MESSAGE "The destination address is not a valid email address."
         VIEW-AS ALERT-BOX warning BUTTONS OK.
        next.
    end.

  if tDownloads:input-value < 1 
   then
    do:
        MESSAGE "At least one download must be permitted."
         VIEW-AS ALERT-BOX warning BUTTONS OK.
        next.
    end.

  if tExpiration:input-value < 1 
   then
    do:
        MESSAGE "Expiration must be at least one day."
         VIEW-AS ALERT-BOX warning BUTTONS OK.
        next.
    end.

  pTo = tTo:screen-value in frame {&frame-name}.
  pSubject = tSubject:screen-value in frame {&frame-name}.
  pBody = tBody:screen-value in frame {&frame-name}.
  pNotify = tNotify:checked in frame {&frame-name}.
  pExpiration = tExpiration:input-value in frame {&frame-name}.
  pDownloads = tDownloads:input-value in frame {&frame-name}.

  pSend = true.
  leave MAIN-BLOCK.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tTo tSubject tNotify tBody tExpiration tDownloads 
      WITH FRAME fMain.
  ENABLE RECT-34 tTo tSubject tNotify tBody tExpiration tDownloads bSend bDone 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

