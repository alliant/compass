&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@name qar.i
@description Quality Assurance Review summary datastructure
@author D.Sinclair
@created 1.24.2012
Modification:
Date          Name      Description
08/02/2017    AC        New fields schedStartDate and schedFinishDate added
07/15/2021    SA        Task 83510 add new fields grade
------------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias qar
&ENDIF

def temp-table {&tableAlias}
 field qarID as int
 field version as char
 field auditYear as int
 field agentID as char
 field name as char /* comes from agent table */
 field addr1 as char /* comes from agent table */
 field addr2 as char /* comes from agent table */
 field city as char /* comes from agent table */
 field state as char /* comes from agent table */
 field zip as char /* comes from agent table */
 field auditStartDate as datetime
 field auditFinishDate as datetime
 field schedStartDate as datetime
 field schedFinishDate as datetime
 field auditor as char
 field score as int
 field uid as char
 field contactName as char
 field contactPhone as char
 field contactFax as char
 field contactEmail as char
 field deliveredTo as char
 field comments as char
 field stateID as char
 field stat as char
 field grade as int
 
 /* child summary fields (don't change over time) */                                                         
 field section1score as int
 field section2score as int
 field section3score as int
 field section4score as int
 field section5score as int
 field section6score as int
 field section7score as int
 field section8score as int

 /* aggregate and child summary fields (change over time) */ 
 field nextFollowupDate as datetime
 field nextDueDate as datetime
 field numMajor as int
 field numInter as int
 field numMinor as int
 field numOpen as int
 field numInProcess as int
 field numCompleted as int
 field numRejected as int
 field numCorrective as int
 field numRecommendations as int
 field numSuggestions as int
 
 /* server fields reflecting original posting */
 field submittedBy as char
 field submittedAt as char
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


