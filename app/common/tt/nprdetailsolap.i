/*------------------------------------------------------------------------
File        : tt/nprdetailsolap.i
Purpose     :

Syntax      :

Description : temp-table for nprdetailsolap
                                           
Author(s)   : sagar K
Created     : 18-04-2023
Notes       :
----------------------------------------------------------------------*/
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias nprdetailsolap
&endif
&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "nprdetailsolap"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif
  
define temp-table {&tableAlias} no-undo {&serializeName}
    field AgentID               as character    {&nodeType} serialize-hidden 
    field name                  as character    {&nodeType} serialize-hidden 
	field Stat                  as character    {&nodeType} serialize-hidden
	field StateID               as character    {&nodeType} serialize-name  "State"
	field Manager               as character    {&nodeType}
    field Netpremium0monthprev  as decimal      {&nodeType} serialize-name  "Netpermium"  format "(>>>,>>>,>>9)"
	field Netpremium1monthprev  as decimal      {&nodeType}                   format "(>>>,>>>,>>9)"
	field Netpremium2monthprev  as decimal      {&nodeType}                   format "(>>>,>>>,>>9)"
	field NetPlan               as decimal      {&nodeType} serialize-hidden  format "(>>>,>>>,>>9)"
    field NetpremiumdiffNetPlan as decimal      {&nodeType} serialize-hidden  format "(>>>,>>>,>>9)"
	field NetpremiumYTD         as decimal      {&nodeType} serialize-name "YTD Actual" format "(>>>,>>>,>>9)"
    field NetPlanYTD            as decimal      {&nodeType} serialize-name "YTD Plan"   format "(>>>,>>>,>>9)"
    field NetpremiumDiffplanYTD as decimal      {&nodeType} serialize-name "YTD Actual to Plan"	format "(>>>,>>>,>>9)"
    field isFavorite            as character    {&nodeType} serialize-hidden
    field Favorite              as character    {&nodeType} 

	
	field stateName     as character {&nodeType} column-label "StateName"           format "x(50)"
    field managerDesc   as character {&nodeType} column-label "Primary Manager" format "x(30)"
    field statDesc      as character {&nodeType} column-label "Status"          format "x(20)"
	field secondManager as character {&nodeType} serialize-hidden
    .
  
