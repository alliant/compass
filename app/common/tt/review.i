&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : review.i
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : Rahul Sharma
    Created     :
    Notes       :
    Modification:
    Date          Name          Description
    04/06/18      Naresh        Add new fields
    04/13/18      Rahul Sharma  Changed field status_ to stat 
    08/29/18      Rahul Sharma  Added  new field userName
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

&if defined(tablealias) = 0 &then
&scoped-define tableAlias review
&endif
   
define temp-table {&tableAlias}
  field reviewID          as integer
  field stateID           as character
  field qualificationID   as integer
  field reviewDate        as datetime
  field reviewBy          as character
  field userName          as character
  field enforcement       as character
  field nextreviewDueDate as datetime
  field numValidDays      as integer
  field issue             as logical
  field comments          as character
  field stat              as character
  field effdate           as datetime
  field expdate           as datetime.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


