&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : scheduleQARdata.i
    Author(s)   : Anjly Chanana
    Created     : 10/03/2017
    Modified    : 07/15/2021 SA Task 83510 add new fields grade and score
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias scheduleQARdata
&ENDIF

def temp-table {&tableAlias} 
  field agentName as char
  field agentStatus as char
  field agentStatusDesc as character
  field agentAddress as character
  field agentCity as character
  field agentState as character
  field agentZip as character
  field agentManager as char
  field agentID as character
  field lastAuditor as character
  field auditor1 as char
  field auditor2 as char
  field auditor3 as character
  field lastAuditDate as datetime
  field auditFinishDate1 as datetime 
  field auditFinishDate2 as datetime 
  field auditFinishDate3 as datetime 
  field qarID1 as integer
  field qarID2 as integer
  field qarID3 as integer
  field auditType1 as character
  field auditType2 as character
  field auditType3 as character
  field auditScores as character
  field year1AuditScore as int
  field year2AuditScore as int
  field year3AuditScore as int
  field errScores as character
  field year1ERRScore as int
  field year2ERRScore as int
  field year3ERRScore as integer
  field proposedAuditType as character
  field proposedAuditDate as datetime
  field proposedAuditor as character
  field proposedAuditID as integer
  field recordChanged as logical
  field auditGrades as character
  field year1AuditGrade as int
  field year2AuditGrade as int
  field year3AuditGrade as int
.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


