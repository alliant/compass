&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : sysapplog.i
    Purpose     : General purpose system application log datastructure
    Author(s)   : B.Johnson
    Created     : 3.16.2016
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias sysapplog
&ENDIF

def temp-table {&tableAlias}
 field appCode as char
 field entityType as char
 field entityID as char
 field entitySeq as int
 field objAction as char
 field objID as char
 field objName as char
 field objDesc as char
 field objRef as char
 field logDate as datetime
 field uid as char
 field username as char /* Client Side */
 field entityname as char /* Client Side */
 .

/*
 Sample Usage:
 
 Table		Field		SQL DataType	Sample Data
 ----------------------------------------------------------
 sysapplog	appCode		varchar(20)		CLM
			entityType	varchar(50)		Claim
			entityID	varchar(50)		20150001
			entitySeq	Int	0
			objAction	varchar(100)	FolderShare
			objID		varchar(100)	<FolderGUID>
			objName		varchar(200)	Agent's File
			objDesc		varchar(max)	
			objRef		varchar(200)	bej@bellsouth.net
			logDate		datetime		4/1/2015
			uid			varchar(20)		bjohnson
 */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


