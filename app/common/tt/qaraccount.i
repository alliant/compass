&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* account.i
   D.Sinclair 10.20.2015 Added qarID
   ----------------------------------------------------------------------*/
   
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias account
&ENDIF
   
def temp-table {&tableAlias}
  field qarID as int
  field bankID as int
  field bankName as char
  field acctTitle as char
  field acctNumber as char
  field reconDate as datetime
  field reconCurrent as logical
  field reconAdjBankBal as decimal
  field reconCheckbookBal as decimal
  field reconTrialBal as decimal
  field stmtBal as decimal
  field dipBal as decimal
  field outChecksBal as decimal
  field netAdj as decimal
  field netNegAdj as decimal
  field calcBal as decimal
  field actualCheckbookBal as decimal
  field trialBal as decimal
  field reconciled as logical
  field comments as char
  index pi-acct bankID.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


