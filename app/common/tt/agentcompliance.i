&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/agentcompliance.i
Purpose     : Used in CRM module for get compliance

Syntax      :

Description : temp-table for requirement fulfillment
                join of tables 
                 staterequirement
                 statereqQual
                 qualification
               
                                           
Author(s)   : Rahul Sharma
Created     : 06-06-2019
Notes       :

Modification:
Date          Name           Description
09/12/2109    Gurvindar      name changed to fulfillment to agentcompliance
----------------------------------------------------------------------*/


&if defined(tablealias) = 0 &then
&scoped-define tableAlias agentcompliance
&endif
   
define temp-table {&tableAlias}
  field entityname       as character 
  field requirementID    as integer 
  field statereqqualID   as integer 
  field authorizedBy     as character 
  field reqdesc          as character   
  field appliesTo        as character 
  field qualStatCode     as character 
  field qualStatDesc     as character 
  field reqMet           as logical 
  field effectiveDate    as datetime 
  field expirationDate   as datetime     
  field qualification    as character             
  field comstat          as character
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


