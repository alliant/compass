/*------------------------------------------------------------------------
File        : tt/prodFile.i
Purpose     : used to fetch/store production Files 

Syntax      :

Description : Used specifically for AR app 

Author(s)   : Archana
Created     : 06-04-2024
Notes       : 

Modification:
Date         Name           Description

----------------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias prodFile
&endif
   
def temp-table {&tableAlias}
  field fileARID	    as  integer    {&nodeType}
  field agentfileID	    as  integer    {&nodeType}
  field agentID      	as  character  {&nodeType}
  field agentName       as  character  {&nodeType}
  field fileID          as  character  {&nodeType}
  field fileNumber      as  character  {&nodeType}
  field invoiceDate     as  datetime   {&nodeType}
  field invoicedAmount  as  decimal    {&nodeType}
  field cancelledAmount	as  decimal    {&nodeType}
  field appliedAmount   as  decimal    {&nodeType}
  field balance         as  decimal    {&nodeType}
  .
