/*------------------------------------------------------------------------
@file tt/idverificationsw.i
Datastructure for a idverificationsw
@created 11.27.2024
@author SRK
Modifications:
Date          Name           Description
   
---------------------------------------------------------------------- */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias idverificationsw
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "idverificationsw"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo  {&serializeName}
  field code         as  character  {&nodeType} serialize-name "Code"
  field description  as  character  {&nodeType}
  
  .

