&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/agent.i
   AGENT datastructure
   Author(s) D.Sinclair
   Created 1.24.2012
    9.19.2014 das Added "hasDocument" field
    11.7.2014 das Format Decimal and Integer fields as we're using the defaults in reports
    06.26.2018 Rahul Added "comlog" field
    10.03.2018 Naresh Chopra Added "logo", inactiveDate, pendingCancelDate fields
    10.08.2018 Naresh Chopra Remove "inactiveDate, pendingCancelDate" fields and 
                             Added "withdrawnDate" field
    10.16.2018 Naresh Chopra Removed "logo" field and code aligned.
    04.22.2019 Rahul         Added "matchPercentage" and seq fields.
    06.13.2019 Rahul         Added new field "LegalName"
    07.05.21   SB            Modified to add write-off GL account fields
    06.10.22   SA            Added new fieldS "Region" and "Domain"
    03.30.23   SB            Task#103627 serialize-hidden the fields which are not updated from UI
    06.01.23   SB            Task#105018 Unserialize-hidden the fields managerdesc, contactname, contactphone, contactemail
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias agent
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

define temp-table {&tableAlias} no-undo 
  field agentID         as character {&nodeType} column-label "Agent ID"             format "x(10)"
  field corporationID   as character {&nodeType} column-label "Company"              format "x(20)"
  field stateID         as character {&nodeType} column-label "State ID"             format "x(10)"
  field stat            as character {&nodeType} column-label "Stat"                 format "x(10)"
  field statusDesc      as character {&nodeType} format "x(30)"
  field name            as character {&nodeType} column-label "Agent"                format "x(200)"
  field Legalname       as character {&nodeType} column-label "Legal"                format "x(200)"
  field addr1           as character {&nodeType} column-label "Address"              format "x(80)"
  field addr2           as character {&nodeType} column-label "Suite"                format "x(80)"
  field addr3           as character {&nodeType} serialize-hidden                    
  field addr4           as character {&nodeType} serialize-hidden                    
  field city            as character {&nodeType} column-label "City"                 format "x(40)"
  field county          as character {&nodeType} serialize-hidden
  field state           as character {&nodeType} column-label "State"                format "x(10)"
  field region          as character {&nodeType} column-label "Region"               format "x(20)"
  field zip             as character {&nodeType} column-label "Zip Code"             format "x(15)"
  field contact         as character {&nodeType} column-label "Contact"              format "x(15)"
  field phone           as character {&nodeType} column-label "Phone"                format "x(20)"
  field fax             as character {&nodeType} column-label "Fax"                  format "x(20)"
  field email           as character {&nodeType} column-label "Email"                format "x(100)"
  field domain          as character {&nodeType} column-label "Domain"               format "x(100)"
  field website         as character {&nodeType} column-label "Website"              format "x(100)"
  field agentDate       as datetime  {&nodeType} serialize-hidden
  field contractDate    as datetime  {&nodeType} column-label "Contract!Date"        format "99/99/99"
  field reviewDate      as datetime  {&nodeType} column-label "Review!Date"          format "99/99/99"
  field manager         as character {&nodeType} column-label "Manager"              format "x(100)"
  field managerDesc     as character {&nodeType} column-label "Manager"              format "x(100)"
  field hasDocument     as logical   {&nodeType}
  field isFavorite      as logical   {&nodeType}
  field needRefresh     as logical   {&nodeType} serialize-hidden

  /* licensing information */                                                        
  field stateLicense    as character {&nodeType} serialize-hidden
  field stateLicenseEff as datetime  {&nodeType} serialize-hidden
  field stateLicenseExp as datetime  {&nodeType} serialize-hidden

  /* e&o information */                                                              
  field eoRequired      as logical   {&nodeType} column-label "E&O"                  
  field eoCompany       as character {&nodeType} serialize-hidden
  field eoPolicy        as character {&nodeType} serialize-hidden
  field eoCoverage      as decimal   {&nodeType} serialize-hidden
  field eoAggregate     as decimal   {&nodeType} serialize-hidden
  field eoStartDate     as datetime  {&nodeType} serialize-hidden
  field eoEndDate       as datetime  {&nodeType} serialize-hidden
  field isAiga          as logical   {&nodeType} serialize-hidden                    
  field isAffiliated    as logical   {&nodeType} serialize-hidden                    

  /* status dates */                                                                 
  field createDate      as datetime  {&nodeType} serialize-hidden
  field cancelDate      as datetime  {&nodeType} column-label "Cancel!Date"          format "99/99/99"
  field closedDate      as datetime  {&nodeType} column-label "Closed!Date"          format "99/99/99"
  field prospectDate    as datetime  {&nodeType} column-label "Prospect!Date"        format "99/99/99"
  field activeDate      as datetime  {&nodeType} column-label "Active!Date"          format "99/99/99"
  field withdrawnDate   as datetime  {&nodeType} column-label "Withdrawn!Date"       format "99/99/99"

  /* policy information */                                                           
  field policyLimit     as integer   {&nodeType} column-label "Policy Limit"         format "ZZ,ZZ9"
  field liabilityLimit  as decimal   {&nodeType} column-label "Liability Limit"      format "ZZZ,ZZZ,ZZ9"
  field lastRemitDate   as datetime  {&nodeType} serialize-hidden
  field lastRemitAmt    as decimal   {&nodeType} serialize-hidden
  field maxCoverage     as decimal   {&nodeType} serialize-hidden
  field remitAlert      as decimal   {&nodeType} column-label "Remit Alert"          format "-ZZZ,ZZZ,ZZ9.99"
  field remitType       as character {&nodeType} column-label "Remit Type"           format "x(50)"             /* P)ercentage of Gross and based on L)iability */
  field remitValue      as decimal   {&nodeType} column-label "Remit Value"          format "ZZZ,ZZZ,ZZ9.99"    /* If type = (P) then net percentage of gross */
                                                                                                                /* If type = (L) then net rate per 1000 of liability */

  /* audit */                                                                        
  field lastAuditScore  as integer   {&nodeType} serialize-hidden
  field lastAuditDate   as datetime  {&nodeType} serialize-hidden
  field nextAuditDue    as datetime  {&nodeType} serialize-hidden
  field lastCertDate    as datetime  {&nodeType} serialize-hidden 
  field nextCertDue     as datetime  {&nodeType} serialize-hidden 
  field comments        as character {&nodeType} serialize-hidden
  field contractID      as character {&nodeType} column-label "Contract"             format "x(50)"

  /* software and version */                                                         
  field swVendor        as character {&nodeType} column-label "Software"             format "x(100)"
  field swVersion       as character {&nodeType} column-label "Version"              format "x(100)"

  /* batch information */                                                            
  field batchCount      as integer   {&nodeType} serialize-hidden
  field batchTotal      as decimal   {&nodeType} serialize-hidden

  /* mailing address */                                                              
  field mName           as character {&nodeType} column-label "Mailing!Name"         format "x(200)"
  field mAddr1          as character {&nodeType} column-label "Mailing!Address"      format "x(80)"
  field mAddr2          as character {&nodeType} column-label "Mailing!Suite"        format "x(80)"
  field mCity           as character {&nodeType} column-label "Mailing!City"         format "x(50)"
  field mState          as character {&nodeType} column-label "Mailing!State"        format "x(10)"
  field mZip            as character {&nodeType} column-label "Mailing!Zip Code"     format "x(10)"
  
  /* agent contact information */                                                              
  field contactName     as character {&nodeType} column-label "A/R Contact"          format "x(100)"
  field contactPhone    as character {&nodeType} column-label "Phone"                format "x(20)"
  field contactEmail    as character {&nodeType} column-label "Email"                format "x(100)"

  /* miscellaneous information */                                                    
  field altaUID         as character {&nodeType} column-label "ALTA UID"             format "x(50)"
  field stateUID        as character {&nodeType} column-label "State UID"            format "x(50)"
  field NPN             as character {&nodeType} serialize-hidden                    
  field comstat         as character {&nodeType} serialize-hidden                    
  field logo            as character {&nodeType} serialize-hidden                    

  /* client side */                                                                  
  field matchPercentage as decimal   {&nodeType} serialize-hidden                    
  field seq             as integer   {&nodeType} serialize-hidden                    
  field stateName       as character {&nodeType} serialize-hidden
  field comSeq          as char      {&nodeType} serialize-hidden

  /* AR information */                                                               
  field invoiceDue      as character {&nodeType} column-label "Invoice Due"          
  field dueInDays       as integer   {&nodeType} column-label "Due In Days"          
  field dueOnDays       as integer   {&nodeType} column-label "Due On Days"          

  /* organization */                                                                 
  field orgID           as character {&nodeType} column-label "Organization!ID"      format "x(10)"
  field orgRoleID       as integer   {&nodeType}
  field orgName         as character {&nodeType} column-label "Organization!Name"    format "x(100)"
  field orgType         as character {&nodeType} serialize-hidden
  field address         as character {&nodeType} serialize-hidden 
  
  /* AR related information */
  field ARCashGLRef     as character {&nodeType} column-label "Cash Account"         format "x(50)"
  field ARGLRef         as character {&nodeType} column-label "Accounts!Receivables" format "x(50)"
  field ARCashGLDesc    as character {&nodeType} column-label "CA Description"       format "x(50)"
  field ARGLDesc        as character {&nodeType} column-label "AR Description"       format "x(50)"
  
  /* AR Refund related information */
  field ARRefundGLRef     as character {&nodeType} column-label "A/R Clrearing"        format "x(50)"
  field ARRefundGLDesc    as character {&nodeType} column-label "AR Description"       format "x(50)"
  
  /* AR Write-off related information */
  field ARWriteOffGLRef     as character {&nodeType} column-label "Write-off"             format "x(50)"
  field ARWriteOffGLDesc    as character {&nodeType} column-label "Write-off Description" format "x(50)"
  
  index agentid is primary unique
  agentID
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


