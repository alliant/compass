/* tt/statesource.i
   @created 05.03.2024
   @author Sachin
   Modifications:
   Date          Name           Description

 */
 
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias statesource
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "statesource"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
  field stateID           as character {&nodeType}
  field stateName         as character {&nodeType}
  field sourceType        as character {&nodeType}        
  field revenueType       as character {&nodeType}
  field createddate       as datetime  {&nodeType}
  field createdby         as character {&nodeType}
  field lastModifiedDate  as datetime  {&nodeType}
  field lastModifiedBy    as character {&nodeType}             
  .





