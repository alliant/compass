&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/agencyreport.i
   AGENCY REPORT datastructure
   Author(s) John Oliver
   Created 2/25/2019
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias agencyreport
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

define temp-table {&tableAlias}
  field agentID as character {&nodeType} column-label "Agent" format "x(20)"
  field agentName as character {&nodeType}
  field agentStat as character {&nodeType}
  field category as character {&nodeType}
  field manager as character {&nodeType}
  field yearCount as decimal {&nodeType} column-label "Current Year" format "(>>>,>>>,>>9)"
  field lastDay as decimal {&nodeType} column-label "Last 24 Hours" format "(>>>,>>>,>>9)"
  field month1Curr  as decimal {&nodeType} column-label "Jan" format "(>>>,>>>,>>9)"
  field month2Curr  as decimal {&nodeType} column-label "Feb" format "(>>>,>>>,>>9)"
  field month3Curr  as decimal {&nodeType} column-label "Mar" format "(>>>,>>>,>>9)"
  field month4Curr  as decimal {&nodeType} column-label "Apr" format "(>>>,>>>,>>9)"
  field month5Curr  as decimal {&nodeType} column-label "May" format "(>>>,>>>,>>9)"
  field month6Curr  as decimal {&nodeType} column-label "Jun" format "(>>>,>>>,>>9)"
  field month7Curr  as decimal {&nodeType} column-label "Jul" format "(>>>,>>>,>>9)"
  field month8Curr  as decimal {&nodeType} column-label "Aug" format "(>>>,>>>,>>9)"
  field month9Curr  as decimal {&nodeType} column-label "Sep" format "(>>>,>>>,>>9)"
  field month10Curr as decimal {&nodeType} column-label "Oct" format "(>>>,>>>,>>9)"
  field month11Curr as decimal {&nodeType} column-label "Nov" format "(>>>,>>>,>>9)"
  field month12Curr as decimal {&nodeType} column-label "Dec" format "(>>>,>>>,>>9)"
  field month1Prev  as decimal {&nodeType} column-label "Jan" format "(>>>,>>>,>>9)"
  field month2Prev  as decimal {&nodeType} column-label "Feb" format "(>>>,>>>,>>9)"
  field month3Prev  as decimal {&nodeType} column-label "Mar" format "(>>>,>>>,>>9)"
  field month4Prev  as decimal {&nodeType} column-label "Apr" format "(>>>,>>>,>>9)"
  field month5Prev  as decimal {&nodeType} column-label "May" format "(>>>,>>>,>>9)"
  field month6Prev  as decimal {&nodeType} column-label "Jun" format "(>>>,>>>,>>9)"
  field month7Prev  as decimal {&nodeType} column-label "Jul" format "(>>>,>>>,>>9)"
  field month8Prev  as decimal {&nodeType} column-label "Aug" format "(>>>,>>>,>>9)"
  field month9Prev  as decimal {&nodeType} column-label "Sep" format "(>>>,>>>,>>9)"
  field month10Prev as decimal {&nodeType} column-label "Oct" format "(>>>,>>>,>>9)"
  field month11Prev as decimal {&nodeType} column-label "Nov" format "(>>>,>>>,>>9)"
  field month12Prev as decimal {&nodeType} column-label "Dec" format "(>>>,>>>,>>9)"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

