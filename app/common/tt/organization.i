&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/organization.i
Purpose     :

Syntax      :

Description :

Author(s)   : Gurvindar 
Created     : 05-09-2019
Notes       :

Modification:
Date          Name           Description

----------------------------------------------------------------------*/


&if defined(tablealias) = 0 &then
&scoped-define tableAlias organization
&endif
   
define temp-table {&tableAlias}
  field orgID           as character
  field seq             as integer
  field name            as character 
  field addr1           as character
  field addr2           as character
  field city            as character
  field state           as character
  field zip             as character
  field email           as character
  field website         as character
  field NPN             as character
  field altaUID         as character
  field fax             as character
  field phone           as character
  field mobile          as character
  field stat            as character
  field createdDate     as datetime
  field createdBy       as character
  field comstat         as character
  field matchPercent    as decimal
  field address         as character  /* used for client only */
  field roles           as character  /* used for client only */
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 12.48
         WIDTH              = 76.6.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


