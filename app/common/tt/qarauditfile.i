&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* auditfile.i
   D.Sinclair 10.20.2015 Added qarID
   Yoke Sam C. 06/26/2017 Change "state" to "stateID";
                          change "county" to "countyID";
                          change type of "ownerPolicy"  from char to int;
                          change type of "lender1Policy" from char to int;
                          change type of "lender2Policy" from char to int;
                          added statutoryGapLimit.
   ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias agentFile
&ENDIF
   
def temp-table {&tableAlias}
  field qarID as int
  field fileID as int
  field answered as int
  field fileNumber as char format "x(30)"
  field stateID as char
  field countyID as char
  field ownerLiability as decimal
  field ownerPolicy as int
  field lender1Liability as decimal
  field lender1Policy as int
  field lender2Liability as decimal
  field lender2Policy as int
  field premiumStatement as decimal
  field rate as decimal
  field delta as decimal
  field originalDate as datetime
  field preliminaryDate as datetime
  field updateDate as datetime
  field finalDate as datetime
  field closingDate as datetime
  field fundingDate as datetime
  field recordingDate as datetime
  field statutoryGapLimit as int
  
  field ownerDeliveryDate as datetime
  field ownerDeliveryGapBusiness as int
  field ownerDeliveryGapCalendar as int
  field lender1DeliveryDate as datetime
  field lender1DeliveryGapBusiness as int
  field lender1DeliveryGapCalendar as int
  field lender2DeliveryDate as datetime
  field lender2DeliveryGapBusiness as int
  field lender2DeliveryGapCalendar as int

  field bringToDateGapCalendar as int
  field bringToDateGapBusiness as int
  
  field searchGapCalendar as int
  field searchGapBusiness as int
  field fundingGapCalendar as int
  field fundingGapBusiness as int

  field recordingGapCalendar as int
  field recordingGapBusiness as int

  index pi-file fileID ascending.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


