/*------------------------------------------------------------
   tt/personagentdetails.i
   @created 07/07/2023
   @author SK
   Modifications:
   Name          Date       Note
   ------------- ---------- -----------------------------------------------
   
   --------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias personagentdetails
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "personagentdetails"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
  field agentID        as character {&nodeType}
  field stateID        as character {&nodeType}
  field name           as character {&nodeType}
  field stat           as character {&nodeType}
  field statdesc       as character {&nodeType}
  field statedesc      as character {&nodeType}
  
  /* To link agents */
  field personID        as character {&nodeType}
  field personagentID   as integer   {&nodeType}
  field jobTitle        as character {&nodeType}
  field Notes           as character {&nodeType}
  field Agent           as character {&nodeType} /* both agentID and name */
  

  .
