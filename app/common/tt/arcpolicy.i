&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 policy.i
 POLICY data structure
 D.Sinclair 5.30.2012
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias arcpolicy
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

define temp-table {&tableAlias}
	field AgentId            as character {&nodeType}
	field OfficeId           as character {&nodeType}
	field GfNumber           as character {&nodeType}
	field PolicyId           as integer   {&nodeType}
	field LiabilityAmount    as decimal   {&nodeType} column-label "Coverage"     format ">>>,>>>,>>9.99"
	field GrossPremiumAmount as decimal   {&nodeType}
	field Source             as character {&nodeType}
	field Username           as character {&nodeType}
	field PolicyTemplateId   as integer   {&nodeType}
	field EffectiveDate      as datetime  {&nodeType} column-label "Effective"    format "99/99/9999"
	field IssueDate          as datetime  {&nodeType}
  field Residential        as logical   {&nodeType}
  
  /* fields for inserting a policy */
  field StateId            as character {&nodeType}
  field NetPremium         as decimal   {&nodeType}
  field StatCode           as character {&nodeType}
  field TransactionType    as character {&nodeType}
  field ClosingDate        as datetime  {&nodeType} column-label "Closing"      format "99/99/9999"
  field Signatory          as character {&nodeType}
  field InsuredName        as character {&nodeType} column-label "Insured Name" format "x(1000)"
  field PropertyType       as character {&nodeType} column-label "Property"     format "x(50)"
  field PolicyType         as character {&nodeType}
  
  /* client descriptions */
  field PolicyIdDesc       as character {&nodeType} column-label "Policy"       format "x(20)"
  field PolicyTypeDesc     as character {&nodeType} column-label "Type"         format "x(10)"
  field CountyDesc         as character {&nodeType}
  
  /* address fields */
  field Address1           as character {&nodeType}
  field Address2           as character {&nodeType}
  field City               as character {&nodeType}
  field State              as character {&nodeType}
  field County             as character {&nodeType}
  field Zipcode            as character {&nodeType}
  
  /* for linking the policy to the TPS party */
  field PartyId            as character {&nodeType}
 
  /* for identifying if retrieved from server */
  field Retrieved          as logical   {&nodeType}

  index iPolicyID is primary unique policyID
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Typeas Include
   Allowas 
   Framesas 0
   Add Fields toas Neither
   Other Settingsas INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 16.67
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


