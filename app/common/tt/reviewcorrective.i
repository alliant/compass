&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@name reviewclaim.i
@description Temp table to hold the REVIEW CORRECTIVE actions
@author John Oliver
@created 10/05/2017

------------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias reviewcorrective
&ENDIF

&IF DEFINED(nodeType) = 0 &THEN
&SCOPED-DEFINE nodeType XML-NODE-TYPE "attribute"
&ENDIF

&IF DEFINED(nodeName) = 0 &THEN
&SCOPED-DEFINE nodeName XML-NODE-NAME "reviewcorrective"
&ELSE
&SCOPED-DEFINE nodeName XML-NODE-NAME "{&nodeName}"
&ENDIF

define temp-table {&tableAlias} no-undo {&nodeName}
  field agentID as character {&nodeType}
  field findingID as integer {&nodeType}
  field actionID as integer  {&nodeType}
  field stat as character    {&nodeType}
  field q as character column-label "Question" {&nodeType}
  field f as character column-label "Finding" format "x(500)" {&nodeType}
  field a as character column-label "Suggested Action" format "x(500)" {&nodeType}
  field uid as character {&nodeType}
  field dateCreated as datetime {&nodeType} column-label "Created" format "99/99/9999" 
  field dateDue as datetime {&nodeType} column-label "Due" format "99/99/9999" 
  field dateCompleted as datetime {&nodeType} column-label "Completed" format "99/99/9999" 
  field comment as character column-label "Comments" format "x(1000)" {&nodeType}
  
  /* for the client */
  field uidDesc as character column-label "Owner" format "x(100)"  {&nodeType}
  field statDesc as character column-label "Status" format "x(20)" {&nodeType}
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


