&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@name alert.i
@description Datastructure to get the data in the ALERT table
@author John Oliver
@created 10/05/2017
------------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias alert
&ENDIF

def temp-table {&tableAlias}
  field alertID as integer
  field stateID as character
  field source as character
  field processCode as character column-label "Alert Type" format "x(100)"
  field threshold as decimal column-label "Threshold"
  field thresholdRange as character column-label "Threshold" format "x(100)"
  field score as decimal column-label "Measure"
  field scoreDesc as character column-label "Measure" format "x(20)"
  field description as character
  field severity as integer column-label "Severity"
  field dateCreated as datetime
  field createdBy as character
  field dateClosed as datetime
  field closedBy as character
  field effDate as datetime
  field stat as character
  field active as logical
  field owner as character column-label "Owner" format "x(100)"
  
  /* fields gathered from other tables */
  field agentID as character
  field agentName as character
  field lastNoteDate as datetime
  field note as character
  
  /* fields gathered from client */
  field processCodeDesc as character column-label "Alert Type" format "x(100)"
  field ownerDesc as character column-label "Owner" format "x(100)"
  field statDesc as character column-label "Status" format "x(20)"
  field severityDesc as character column-label "Severity" format "x(50)"
  field createdByDesc as character
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


