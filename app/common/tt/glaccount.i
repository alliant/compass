/*------------------------------------------------------------------------
File        : tt/account.i
Purpose     :

Syntax      :

Description :

Author(s)   : Anjly
Created     : 07-29-20
Notes       :

Modification:
Date          Name      Description

----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias glaccount
&endif
   
define temp-table {&tableAlias}
  field type           as character
  field ID             as character
  field description    as character
  .
