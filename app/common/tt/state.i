&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : state.i
    Purpose     : STATE datastructure
    Author(s)   : D.Sinclair
    Created     : 1.24.2012
    Notes       :
    Modification:
    Date         Name        Description
    08-23-2018   Gurvindar   Added field externalApproval 
    06-10-2022   SA          Added fields region and regionDesc
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias state
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

def temp-table {&tableAlias}
 field stateID as char {&nodeType}
 field seq as int {&nodeType}
 field description as char {&nodeType}
 field active as logical {&nodeType}
 field appDate as datetime {&nodeType}
 field license as char {&nodeType}
 field licEffDate as datetime {&nodeType}
 field licExpDate as datetime {&nodeType}
 field cancelDate as datetime {&nodeType}
 field lastAuditDate as datetime {&nodeType}
 field name as char {&nodeType}
 field addr1 as char {&nodeType}
 field addr2 as char {&nodeType}
 field addr3 as char {&nodeType}
 field addr4 as char {&nodeType}
 field city as char {&nodeType}
 field county as char {&nodeType}
 field state as char {&nodeType}
 field region as char {&nodeType}
 field zip as char {&nodeType}
 field contact as char {&nodeType}
 field phone as char {&nodeType}
 field fax as char {&nodeType}
 field email as char {&nodeType}
 field website as char {&nodeType}
 field comments as char {&nodeType}
 field hasDocument as logical {&nodeType}
 field externalApproval as logical {&nodeType}
  /* client-side */ 
 field regionDesc as character {&nodeType}
 
 index state is primary unique
  stateID
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


