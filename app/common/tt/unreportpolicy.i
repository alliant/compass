&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
  tt/unreportpolicy.i
  Unremitted policies temp table
  Author: John Oliver
  Date: 2023.05.12

*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias unreportpolicy
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF


define temp-table {&tableAlias} no-undo
  field officeID       as character {&nodeType} column-label "Office"       format "x(200)"
  field officeName     as character {&nodeType} column-label "Office Name"  format "x(200)"
  field agentID        as character {&nodeType} column-label "Agent"        format "x(200)"
  field agentName      as character {&nodeType} column-label "Agent Name"   format "x(200)"
  field agentStat      as character {&nodeType} column-label "Agent Status" format "x(20)"
  field agentStatDesc  as character {&nodeType} column-label "Agent Status" format "x(200)"
  field regionID       as character {&nodeType} column-label "Region"       format "x(200)"
  field regionDesc     as character {&nodeType} column-label "Region"       format "x(200)"
  field managerID      as character {&nodeType} column-label "Manager"      format "x(200)"
  field managerName    as character {&nodeType} column-label "Manager"      format "x(200)"
  field stateID        as character {&nodeType} column-label "State"        format "x(200)"
  field stateDesc      as character {&nodeType} column-label "State"        format "x(200)"
  field bucketLT30     as integer   {&nodeType} column-label "<=30 Days"    format ">,>>9"
  field bucket31to60   as integer   {&nodeType} column-label "31-60 Days"   format ">,>>9"
  field bucket61to90   as integer   {&nodeType} column-label "61-90 Days"   format ">,>>9"
  field bucket91to180  as integer   {&nodeType} column-label "91-180 Days"  format ">,>>9"
  field bucket181to270 as integer   {&nodeType} column-label "181-270 Days" format ">,>>9"
  field bucket271to365 as integer   {&nodeType} column-label "271-365 Days" format ">,>>9"
  field bucket366to730 as integer   {&nodeType} column-label "1-2 Years"    format ">,>>9"
  field bucketGT731    as integer   {&nodeType} column-label "2+ Years"     format ">,>>9"
  field total          as integer   {&nodeType} column-label "Total"        format ">>,>>9"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 1
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


