&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
  tt/unremitpolicy.i
  Unremitted policies temp table
  Author: John Oliver
  Date: 2023.05.12

*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias agentapp
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF


define temp-table {&tableAlias} no-undo
  field agentID                       as character {&nodeType} column-label "Agent ID"                             format "x(200)"
  field applicationID                 as integer   {&nodeType} serialize-hidden
  field stat                          as character {&nodeType} serialize-hidden
  field stateID                       as character {&nodeType} column-label "State"                                format "x(50)"
  field regionID                      as character {&nodeType} serialize-hidden
  field agentName                     as character {&nodeType} column-label "Agent"                                format "x(200)"
  field manager                       as character {&nodeType} serialize-hidden
  field username                      as character {&nodeType} serialize-hidden
  field password                      as character {&nodeType} serialize-hidden
  field reasonCode                    as character {&nodeType} serialize-hidden
  field type                          as character {&nodeType} serialize-hidden
  field subtype                       as character {&nodeType} serialize-hidden
  field notes                         as character {&nodeType} serialize-hidden column-label "App.!Notes"          format "x(4000)"
  /* dates */
  field dateCreated                   as datetime  {&nodeType} column-label "Created"                              format "99/99/99"
  field dateStarted                   as datetime  {&nodeType} column-label "User Started"                         format "99/99/99"
  field dateSubmitted                 as datetime  {&nodeType} column-label "User Submitted"                       format "99/99/99"
  field dateOrderedERR                as datetime  {&nodeType} column-label "Ordered ERR"                          format "99/99/99"
  field dateStartedERR                as datetime  {&nodeType} column-label "Started ERR"                          format "99/99/99"
  field dateReceviedERR               as datetime  {&nodeType} column-label "Received ERR"                         format "99/99/99"
  field dateUnderReview               as datetime  {&nodeType} column-label "Processing!Started"                   format "99/99/99"
  field dateSentReview                as datetime  {&nodeType} column-label "Review!Sent"                          format "99/99/99"
  field dateApproved                  as datetime  {&nodeType} column-label "Review!Approved"                      format "99/99/99"
  field dateStopped                   as datetime  {&nodeType} column-label "Review!Denied"                        format "99/99/99"
  field dateSentAgency                as datetime  {&nodeType} column-label "Agreement!Sent"                       format "99/99/99"
  field dateSigned                    as datetime  {&nodeType} column-label "Agreement!Received"                   format "99/99/99"
  /* metrics */
  field metricCreatedToSubmitted      as integer   {&nodeType} column-label "Create to Submitted"                  format ">>9"
  field metricERRStartedToFinish      as integer   {&nodeType} column-label "ERR Started to Finished"              format ">>9"
  field metricCreatedToUnderReview    as integer   {&nodeType} column-label "Created to Processed"                 format ">>9"
  field metricUnderReviewToSentReview as integer   {&nodeType} column-label "Processed to Review Sent"             format ">>9"
  field metricSentReviewToApproved    as integer   {&nodeType} column-label "Review Sent to Decision"              format ">>9"
  field metricCreatedToApproved       as integer   {&nodeType} column-label "Created to Decision"                  format ">>9"
  field metricUnderReviewToSentAgency as integer   {&nodeType} column-label "Processed to Agreement Sent"          format ">>9"
  field metricSentAgencyToSigned      as integer   {&nodeType} column-label "Agreement Sent to Agreement Received" format ">>9"
  field metricCreatedToSigned         as integer   {&nodeType} column-label "Created to Agreement Received"        format ">>9"
  /* client */
  field statDesc                      as character {&nodeType} column-label "Status"                               format "x(20)"
  field stateName                     as character {&nodeType} column-label "State"                                format "x(50)"
  field regionDesc                    as character {&nodeType} serialize-hidden column-label "Region"              format "x(50)"
  field managerDesc                   as character {&nodeType} column-label "Agency Manager"                       format "x(50)"
  field reasonCodeDesc                as character {&nodeType} column-label "Reason Code"
  field typeDesc                      as character {&nodeType} column-label "Type"                                 format "x(20)"
  field subtypeDesc                   as character {&nodeType} serialize-hidden column-label "Subtype"             format "x(20)"
  /* used for the ERR screen */
  field qarID                         as integer   {&nodeType} serialize-hidden column-label "Audit ID"            format "999999"
  field yearsActive                   as integer   {&nodeType} serialize-hidden
  field phone                         as character {&nodeType} serialize-hidden
  field email                         as character {&nodeType} serialize-hidden
  field name                          as character {&nodeType} serialize-hidden
  field addr1                         as character {&nodeType} serialize-hidden
  field addr2                         as character {&nodeType} serialize-hidden
  field city                          as character {&nodeType} serialize-hidden
  field state                         as character {&nodeType} serialize-hidden
  field zip                           as character {&nodeType} serialize-hidden
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 1
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


