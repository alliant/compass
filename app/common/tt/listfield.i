&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* 
@name tt/listfield.i
@description Temp table for the LIST builder to store the FIELDs for the table
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias listfield
&ENDIF

define temp-table {&tableAlias}
  field displayName as character
  field entityName as character
  field tableName as character
  field columnName as character
  field columnBuffer as character
  field columnWidth as decimal
  field columnFormat as character
  field columnWhere as character
  field dateFrom as character
  field dataType as character
  field queryFunction as character
  field order as int
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
