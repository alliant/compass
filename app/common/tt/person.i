&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/person.i
Purpose     :

Syntax      :

Description :

Author(s)   : Rahul Sharma
Created     : 11-17-2017
Notes       :

Modification:
Date          Name      Description
03/19/2018    Rahul     Added new fields
05/21/2018    Rahul     Corrected the name of title field
01/04/2019    Gurvindar Added index idxName
04/29/2019    Rahul     Added "matchPercentage" and "Seq" field.
05/18/2020    Shubham   Added "natureOfBToA" and "orgID".
08/16/2022    SA        Task #96812 Modified to add field related with person.
12/12/2022    SR        task #100564 Modifed to add fields related with personagent.
06/26/2023    SB        Task #104008 Modified to add fields related for tags implementation
09/28/2023    SK        Task #105743 Modified to add fulladdress field
04/03/2024    SB        Task #111689 Modified to add fields(contactEmail,contactPhone,contactMobile),createArTag field,
                        remove county and title field
----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias Person
&endif
   
def temp-table {&tableAlias}
  field personID         as character
  field NPN              as character
  field altaUID          as character
  field firstName        as character
  field middleName       as character
  field lastName         as character
  field dispName         as character
  field address1         as character
  field address2         as character
  field city             as character
  field state            as character
  field zip              as character
  field email            as character
  field phone            as character
  field mobile           as character
  field contactEmail     as character
  field contactPhone     as character
  field phExtension      as character
  field contactMobile    as character
  field active           as logical
  field notes            as character 
  field internal         as logical
  field doNotCall        as logical
  field createArTag      as logical
  
  field affToComp          as logical    /* used for client only */
  field belongingStateID   as character  /* used for client only */
  field matchPercentage    as decimal    /* used for client only */
  field seq                as integer    /* used for client only */
  field natureOfBToA       as character  /* person's relationship with organization used in AR */
  field orgID              as character  /* to create New Affiliation of Person to Organization in AR  */
  field agentID            as character  /* to store the previous agent in AR */
  field tempStat           as character  /* used for client only */
  field tagStat            as logical    /* used for client only */
  field address            as character  /* used for client only */
  field roles              as character  /* used for client only */
  field jobTitle           as character  /* used for client only */
  field linkedToAgent      as logical    /* used for client only */
  field linkedToCompliance as logical    /* used for client only */
  field linkedPerson       as character  /* used for client only */
  field personAgentID      as integer
  field expirationDate     as datetime
  field personAgentActive  as logical  /* used for client only */ 
  field tagBy              as character /* used for client only */
  field selectrecord       as logical   /* used for client only */ 
  field fulladdress        as character  /* used for client only */ 
  index idxName dispName
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


