&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        :tt/attorney.i
Purpose     :

Syntax      :

Description :

Author(s)   : Sachin Chautrvedi
Created     : 07.24.2017
Notes       :

Modification:
Date          Name      Description
03/15/2018    Rahul     Added field personID
06/18/2018    Rahul     Added field comStat
04/29/2019    Rahul     Added "matchPercentage" and "Seq" field.
----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias arcattorney
&endif

&if defined(nodetype) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif

define temp-table {&tableAlias}
  field ANAttorneyID as character {&nodeType}
  field FullName     as character {&nodeType}
  field FullName2    as character {&nodeType}
  field Address1     as character {&nodeType}
  field Address2     as character {&nodeType}
  field City         as character {&nodeType}
  field State        as character {&nodeType}
  field Zipcode      as character {&nodeType}
  field Phone        as character {&nodeType}
  field Fax          as character {&nodeType}
  field Email        as character {&nodeType}
  field Contact      as character {&nodeType}
  field Stat         as character {&nodeType}
  field StateID      as character {&nodeType}
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


