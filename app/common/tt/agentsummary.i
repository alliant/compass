&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/agent.i
   AGENT Customer Relationship Manager datastructure
   Author(s) J.Oliver
   Created 3.3.2017
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias agentsummary
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

define temp-table {&tableAlias}
  field agentID       as character {&nodeType} column-label "Agent ID"        format "x(20)"
  field stateID       as character {&nodeType} column-label "State"           format "x(20)"
  field stat          as character {&nodeType} serialize-hidden
  field name          as character {&nodeType} column-label "Agent"           format "x(200)"
  field manager       as character {&nodeType} serialize-hidden
  field secondManager as character {&nodeType} serialize-hidden
  field isFavorite    as logical   {&nodeType} column-label ""
  field hasDocument   as logical   {&nodeType} column-label "Has!Doc"
  field lastNoteDate  as datetime  {&nodeType} column-label "Last Note"       format "99/99/99"
  field qarDate       as datetime  {&nodeType} serialize-hidden
  field qarScore      as integer   {&nodeType} serialize-hidden
  field errScore      as integer   {&nodeType} serialize-hidden
  field monthBC_1     as decimal   {&nodeType} serialize-hidden
  field monthBC_2     as decimal   {&nodeType} serialize-hidden
  field monthBC_3     as decimal   {&nodeType} serialize-hidden
  field monthNP_1     as decimal   {&nodeType} serialize-hidden
  field monthNP_2     as decimal   {&nodeType} serialize-hidden
  field monthNP_3     as decimal   {&nodeType} serialize-hidden
  field monthNPP_1    as decimal   {&nodeType} serialize-hidden
  field monthNPP_2    as decimal   {&nodeType} serialize-hidden
  field monthNPP_3    as decimal   {&nodeType} serialize-hidden
  field monthGP_1     as decimal   {&nodeType} serialize-hidden
  field monthGP_2     as decimal   {&nodeType} serialize-hidden
  field monthGP_3     as decimal   {&nodeType} serialize-hidden
  field monthRP_1     as decimal   {&nodeType} serialize-hidden
  field monthRP_2     as decimal   {&nodeType} serialize-hidden
  field monthRP_3     as decimal   {&nodeType} serialize-hidden
  field monthFC_1     as decimal   {&nodeType} serialize-hidden
  field monthFC_2     as decimal   {&nodeType} serialize-hidden
  field monthFC_3     as decimal   {&nodeType} serialize-hidden
  field monthPC_1     as decimal   {&nodeType} serialize-hidden
  field monthPC_2     as decimal   {&nodeType} serialize-hidden
  field monthPC_3     as decimal   {&nodeType} serialize-hidden
  field monthPCP_1    as decimal   {&nodeType} serialize-hidden
  field monthPCP_2    as decimal   {&nodeType} serialize-hidden
  field monthPCP_3    as decimal   {&nodeType} serialize-hidden
  field monthRG_1     as decimal   {&nodeType} serialize-hidden
  field monthRG_2     as decimal   {&nodeType} serialize-hidden
  field monthRG_3     as decimal   {&nodeType} serialize-hidden
  field alertWarn     as character {&nodeType} serialize-hidden
  field alertCrit     as character {&nodeType} serialize-hidden
  
  /* for the client only */
  field month1Data    as decimal   {&nodeType} column-label "3"               format "(ZZ,ZZZ,ZZZ,ZZZ)"
  field month2Data    as decimal   {&nodeType} column-label "2"               format "(ZZ,ZZZ,ZZZ,ZZZ)"
  field month3Data    as decimal   {&nodeType} column-label "1"               format "(ZZ,ZZZ,ZZZ,ZZZ)"
  field month1AData   as decimal   {&nodeType} column-label "3"               format "(ZZ,ZZZ,ZZZ,ZZZ)"
  field month2AData   as decimal   {&nodeType} column-label "2"               format "(ZZ,ZZZ,ZZZ,ZZZ)"
  field month3AData   as decimal   {&nodeType} column-label "1"               format "(ZZ,ZZZ,ZZZ,ZZZ)"
  field month1PData   as decimal   {&nodeType} column-label "3"               format "(ZZ,ZZZ,ZZZ,ZZZ)"
  field month2PData   as decimal   {&nodeType} column-label "2"               format "(ZZ,ZZZ,ZZZ,ZZZ)"
  field month3PData   as decimal   {&nodeType} column-label "1"               format "(ZZ,ZZZ,ZZZ,ZZZ)"
  field alertWarnCnt  as integer   {&nodeType} column-label "Y"               format "ZZ"
  field alertCritCnt  as integer   {&nodeType} column-label "R"               format "ZZ"
  field alertNoneCnt  as integer   {&nodeType} column-label "G"               format "ZZ"
  field alertShow     as character {&nodeType} serialize-hidden
  field alertScore    as integer   {&nodeType} serialize-hidden
  field needRefresh   as logical   {&nodeType} serialize-hidden
  
  /* for the descriptions */
  field alertDesc     as character {&nodeType} column-label "Alert"           format "x(500)"
  field stateName     as character {&nodeType} column-label "State"           format "x(50)"
  field managerDesc   as character {&nodeType} column-label "Primary Manager" format "x(30)"
  field statDesc      as character {&nodeType} column-label "Status"          format "x(20)"

  index agentid is primary unique agentID
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


