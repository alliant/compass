/*------------------------------------------------------------------------
File        : tt/tasktopic.i
Purpose     :

Syntax      :

Description : temp-table for tasktopic
                                           
Author(s)   : Spandana
Created     : 04-13-2023
Notes       :
----------------------------------------------------------------------*/
&IF DEFINED(tableAlias) = 0 &THEN
&SCOPED-DEFINE tableAlias tasktopic
&ENDIF

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "tasktopic"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif
  
DEFINE TEMP-TABLE {&tableAlias} no-undo {&serializeName}
  FIELD topic           AS  CHARACTER {&nodeType}    
  FIELD description     AS  CHARACTER {&nodeType}
  FIELD active          AS  LOGICAL   {&nodeType}
  .

