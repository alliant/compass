&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/affiliatiedperson.i
Purpose     :

Syntax      :

Description : Used specifically for WEB CRM app 

Author(s)   : Rahul Sharma
Created     : 05-21-2019
Notes       :

Modification:
Date          Name           Description
06/03/2019    Gurvindar      Added email field
06/07/2019    Rahul Sharma   Added favorite field
09/12/2109    Gurvindar      Name changed from affiliation to affiliatedperson 
----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias affiliatiedperson
&ENDIF
   
def temp-table {&tableAlias}
  field agentId        as character  
  field name           as character
  field stat           as character
  field effectiveDate  as datetime
  field city           as character
  field state          as character
  field zip            as character
  field email          as character
  field rowNum         as character
  field favorite       as logical
  field organizationId as character 
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


