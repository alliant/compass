&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* 
@name tt/claimlosspdf.i
@description Temp table for the LIST builder
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias claimlosspdf
&ENDIF

define temp-table {&tableAlias}
  field claimID as integer
  field altaRisk as character
  field altaResponsibility as character
  field agentName as character
  field agentID as character
  field agentFileNumber as character
  field policyID as integer
  field policyAmount as decimal
  field policyDate as datetime
  field insuredName as character
  field claimSummary as character
  field amount as decimal
  field account as character
  field accountName as character
  field vendorName as character
  field vendorAddress as character
  field invoiceNotes as character
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
