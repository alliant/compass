/*------------------------------------------------------------
    tt/apvendor.i
   @created 11.21.2022
   
   @author KR
   @modified
   Name          Date       Note
   ------------- ---------- -----------------------------------------------
    --------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &then
&scoped-define tableAlias apvendor
&endif

&IF defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "apvendor"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
  field vendorID     as character   {&nodeType}
  field category     as character   {&nodeType}
  field vendorname   as character   {&nodeType}
  field active       as logical     {&nodeType}
  .
