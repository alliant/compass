&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file product.i
@description The PRODUCT temp table definition

@author John Oliver
@created 08.03.2022
@modified
Name          Date       Note
------------- ---------- -----------------------------------------------
SR          01/24/2023  Task 102000 - Changed as per new framework
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias product
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "{&tableAlias}"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} {&serializeName}
  field productID       as integer   {&nodeType}
  field stateID         as character {&nodeType}
  field description     as character {&nodeType} column-label "Product"        format "x(1000)"
  field price           as decimal   {&nodeType} column-label "Price"          format "->>>,>>>,>>9.99"
  field effectiveDate   as date      {&nodeType} column-label "Effective Date" format "99/99/9999"
  field expiryDate      as date      {&nodeType} column-label "Expiry Date"    format "99/99/9999"
  field revenueType     as character {&nodeType} column-label "Revenue Type"   format "x(100)"
  field active          as logical   {&nodeType} column-label "Active"         format "Yes/"

  /* client side description field */
  field stateName       as character {&nodeType} column-label "State"          format "x(100)"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


