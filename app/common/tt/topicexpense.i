/*------------------------------------------------------------------------
File        : tt/topicexpense.i
Purpose     :

Syntax      :

Description : temp-table for topicexpense
                                           
Author(s)   : Sagar k
Created     : 07-31-2023
Notes       :
----------------------------------------------------------------------*/
&IF DEFINED(tableAlias) = 0 &THEN
&SCOPED-DEFINE tableAlias topicexpense
&ENDIF

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "topicexpense"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif
  
DEFINE TEMP-TABLE {&tableAlias} no-undo {&serializeName}
  FIELD stateID	      AS  character {&nodeType}    
  FIELD topic         AS  character {&nodeType}
  FIELD GLAccount     AS  character {&nodeType}
  FIELD GLAccountDesc AS  character {&nodeType}
  .

