/*------------------------------------------------------------------------
File        : tt/glbatchrpt.i
Purpose     :

Syntax      :

Description :

Author(s)   : Anjly
Created     : 08-04-20
Notes       :

Modification:
Date          Name      Description

----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias glbatchrpt
&endif
   
define temp-table {&tableAlias}
  field batchID          as integer
  field fileID           as character
  field fileNum          as character
  field policyType       as character
  field policyID         as integer
  field formType         as character
  field endorsCplCount   as integer
  field liability        as decimal
  field gross            as decimal
  field retained         as decimal
  field net              as decimal
  field reciveDate       as date
  .
