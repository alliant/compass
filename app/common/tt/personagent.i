/*------------------------------------------------------------------------
File        : tt/personAgent.i
Purpose     :

Syntax      :

Description :

Author(s)   : Sachin Anthwal
Created     : 08-04-2022
Notes       :

Modification:
Date          Name           Description
10/28/2022    SA        Task #96812 Modified to add field related with personagent.
----------------------------------------------------------------------*/
&IF defined(tableAlias) = 0 &THEN 
&scoped-define tableAlias personAgent
&ENDIF
   
define temp-table {&tableAlias}
  field personAgentID      as integer
  field agentID            as character
  field personID           as character
  field effectivedate      as datetime
  field expirationDate     as datetime
  field jobTitle           as character
  field notes              as character
  
  field personName         as character /* used for client only */
  field agentName          as character /* used for client only */
  field comStat            as character /* used for client only */
  field stateID            as character /* used for client only */
  field isCtrlPerson       as logical   /* used for client only */
  field active             as logical   /* used for client only */
  field email              as character /* used for client only */
  field phone              as character /* used for client only */
  field doNotCall          as logical   /* used for client only */
  field linkedToCompliance as logical   /* used for client only */
  .
 

