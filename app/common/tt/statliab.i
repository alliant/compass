&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 statliab.i
 Temp-table definition for Statutory Liability Report (wops05-r.w)
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias statliab
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF
 
def temp-table {&tableAlias}
 field agentID as char {&nodeType}
 field name as char {&nodeType}
 field stateID as char {&nodeType}
 field batchID as int {&nodeType}
 field fileNumber as char {&nodeType}
 field ownerNum as int {&nodeType}
 field ownerLiability as dec {&nodeType}
 field lenderNum as int {&nodeType}
 field lenderLiability as dec {&nodeType}
 field grossPremium as dec {&nodeType}
 field netPremium as dec {&nodeType}
 field reservableLiability as dec {&nodeType}
 field grouped as char {&nodeType}
 field netRate as decimal {&nodeType}
 field grossRate as decimal {&nodeType}
 index pu1 is primary unique agentID batchID fileNumber
 
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


