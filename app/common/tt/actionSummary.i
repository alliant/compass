&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/actionSummary.i   
   Author(s) rahul sharma
   Created 9.26.2017
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias actionsummary
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

define temp-table {&tableAlias} 
  field iMonth               as integer   {&nodeType}
  field iAction              as integer   {&nodeType} column-label "# Created"          format ">>>>ZZ"
  field iactionPlanned       as integer   {&nodeType} column-label "# Planned"          format ">>>>ZZ"
  field iActionComplete      as integer   {&nodeType} column-label "# Completed"        format ">>>>ZZ"
  field iActionCancel        as integer   {&nodeType} column-label "# Cancelled"        format ">>>>ZZ"
  field iActionOpen          as integer   {&nodeType} column-label "# Opened"           format ">>>>ZZ"
  field iActionOverdue       as integer   {&nodeType} column-label "# Currently!Late"   format ">>>>ZZ"
  field iActionLate          as integer   {&nodeType} column-label "# Completed!Late"   format ">>>>ZZ"
  field iActionExtend        as integer   {&nodeType} column-label "# Extended"         format ">>>>ZZ"
  
  /* for the approvals */
  field approvalStatus       as character {&nodeType} column-label "Approval!Status"    format "x(20)" 
  field approvalCreateDate   as datetime  {&nodeType} column-label "Approval!Requested" format "99/99/9999" 
  field approvalCompleteDate as datetime  {&nodeType} column-label "Approval!Completed" format "99/99/9999"
  field approvalUser         as character {&nodeType} column-label "Approval!User"      format "x(50)"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


