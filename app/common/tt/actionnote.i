&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@name actionnote.i
@description Action notes datastructure
@author SC
@created 07.26.2017
------------------------------------------------------------------------*/

&if defined(tableAlias) = 0 &THEN
&scoped-define tableAlias actionnote
&endif

def temp-table {&tableAlias}
 field actionID     as integer serialize-hidden
 field seq          as integer serialize-hidden
 field category     as character serialize-hidden
 field categoryDesc as character label "Category" format "x(20)"
 field method       as character serialize-hidden
 field datecreated  as datetime label "Created" format "99/99/99"
 field uid          as character serialize-hidden
 field secured      as logical serialize-hidden
 field takenBy      as character serialize-hidden
 field takenByDesc  as character label "Taken By" format "x(100)"
 field stat         as character serialize-hidden
 field statDesc     as character label "Status" format "x(20)"
 field comments     as character label "Note" format "x(5000)"
 field UserName     as character label "User" format "x(100)"
 index actionIDSeq is primary unique
 actionID seq
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.2.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


