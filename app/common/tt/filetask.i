/* tt/filetask.i
   @created 04.06.2023
   @author Shefali
   Modifications:
   Date          Name           Description
 17/04/2023      sagar K        added topic and Category fields.
 07/21/2023      S Chandu       added posted field.
 */
 
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias filetask
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "filetask"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
  field fileTaskID     as integer      {&nodeType} column-label "Task ID"        format ">>>>>>>9"
  field id             as integer      {&nodeType}
  field topic          as character    {&nodeType} column-label "Task"           format "x(500)"              serialize-name "task"
  field agentFileId    as integer      {&nodeType}                                                            serialize-hidden
  field agent          as character    {&nodeType}
  field agentID        as character    {&nodeType}
  field agentIDDesc    as character    {&nodeType} column-label "Agent"          format "x(500)"
  field fileNumber     as character    {&nodeType} column-label "File Number"    format "x(100)"
  field address        as character    {&nodeType}
  field productDesc    as character    {&nodeType} column-label "Product"        format "x(100)"              serialize-name "product"
  field dueDateTime    as datetime     {&nodeType} column-label "Due Date"       format "99-99-9999 HH:MM AM" serialize-name "dueDate"
  field fileId         as character    {&nodeType} 
  field assignedTo     as character    {&nodeType}                                                            serialize-name "AssignedTo"
  field assignedToName as character    {&nodeType} column-label "Assigned User"  format "x(500)"              serialize-name "AssignedToName"
  field VendorName     as character    {&nodeType} 
  field VendorID       as character    {&nodeType}
  field assignedDate   as datetime     {&nodeType} column-label "Assigned Date"  format "99-99-9999 HH:MM AM"
  field queueCategory  as character    {&nodeType} column-label "Assigned Queue" format "x(100)"              serialize-name "queue"  
  field taskStatus     as character    {&nodeType}                                                            serialize-name "status"
  field ImportantNote  as character    {&nodeType} 
  field startDateTime  as datetime     {&nodeType} column-label "Start Date"     format "99-99-9999 HH:MM AM"
  field endDateTime    as datetime     {&nodeType} column-label "Completed Date" format "99-99-9999 HH:MM AM"
  field cost           as decimal      {&nodeType} 
  field stateID        as character    {&nodeType}
  field glaccount      as character    {&nodeType}
  field glaccountDesc  as character    {&nodeType}
  field posted         as logical      {&nodeType}
  /* Posted vendor Task */
  field apinvID        as integer      {&nodeType}
  field invoiceNumber  as character    {&nodeType}
  field invoiceDate    as date         {&nodeType} format "99-99-9999"
  field username       as character    {&nodeType}
  field amount         as decimal      {&nodeType}
  field refID          as character    {&nodeType}
    
  .                                       







