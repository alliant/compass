&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : ttresults.i
    Purpose     : Action datastructure
    Author(s)   : Rahul
    Created     : 7.11.2017
    Modification:
    Date          Name      Description
              
  ----------------------------------------------------------------------*/
  
&if defined(tableAlias) = 0 &THEN
&scoped-define tableAlias ttresults
&endif

def temp-table {&tableAlias}
 field actionID          as int
 field cactionID         as char
 field entity            as char
 field entityID          as char 
 field entityName        as char format "x(30)"
 field source            as char
 field sourceID          as char
 field sourceQuestion    AS char
 field targetProcess     as char
 field findingID         as int
 field cfindingID        as char
 field description       as char 
 field actionType        as char
 field severity          as int
 field comments          as char
 field OwnerId           as char
 field ownername         as char   
 field UID               as char
 field stat              as char
 field cstat             as char
 field stage             as char 
 field cstage             as char 
 field lastNote          as char  
 field dueDate           as datetime
 field startDate         as datetime
 field completeStatus    as char
 field method            as char
 field closedDate        as datetime
 field createdDate       as datetime
 field openedDate        as datetime
 field completedDate     as datetime
 field originalDueDate   as datetime 
 field extendedDate      as datetime 
 field followupDate      as datetime  
 field referenceActionID as char
 field iconSeverity      as int
 field iconMessages      as char.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


