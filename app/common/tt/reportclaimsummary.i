&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/reportclaimsummary.i
   datastructure for the CLAIMS SUMMARY REPORT
   Created John Oliver
   Date 02.01.2017
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias reportclaimsummary
&ENDIF

/* Should recoupments be against the Loss or LAE in accounting terms?
 */

define temp-table {&tableAlias}
  field claimID as integer
  field description as character
  field stage as character
  field stageDesc as character
  field type as character
  field typeDesc as character
  field action as character
  field actionDesc as character
  field difficulty as integer
  field urgency as integer
  field stateID as character
  field agentID as character
  field agentName as character
  field agentError as logical
  field agentErrorDesc as character
  field stat as character
  field statDesc as character
  field fileNumber as character
  field altaRiskCode as character
  field altaRiskDesc as character
  field altaResponsibilityCode as character
  field altaResponsibilityDesc as character
  field causeCode as character
  field causeDesc as character
  field descriptionCode as character
  field descriptionDesc as character
  field dateOpened as datetime
  field assignedTo as character
  
  field laeOpen as decimal
  field laeApproved as decimal
  field laeLTD as decimal
  field laeReserve as decimal
  field laeAdjOpen as decimal
  field laeAdjLTD as decimal
  
  field lossOpen as decimal
  field lossApproved as decimal
  field lossLTD as decimal
  field lossReserve as decimal
  field lossAdjOpen as decimal
  field lossAdjLTD as decimal
  
  field recoveries as decimal
  field pendingRecoveries as decimal
  field pendingDeductibleRecoveries as decimal
  field deductibleRecoveries as decimal
  field deductibleInvoice as character
  
  field policyID as character
  field policyDate as datetime
  field policyType as character
  field insuredName as character
  field insuredAddr as character
  field lastNote as datetime
  field noteCnt as integer
  
  /* client fields */
  field assignedToDesc as character
  field costsPaid as decimal
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


