&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 reinsurance.i
 Temp-table definition for Reinsurance Report (wops07-r.w)
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias reinsurance
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF
 
define temp-table {&tableAlias}
 field agentID       as character {&nodeType} column-label "AgentID"        format "x(10)"
 field name          as character {&nodeType} column-label "Name"           format "x(250)"
 field stateID       as character {&nodeType} column-label "State"          format "x(4)"
 field fileNumber    as character {&nodeType} column-label "File Number"    format "x(24)"
 field formID        as character {&nodeType} column-label "Form ID"        format "x(10)"
 field formCode      as character {&nodeType} serialize-hidden
 field policyID      as integer   {&nodeType} column-label "Policy ID"      format ">>>>>>>>9"
 field effDate       as date      {&nodeType} column-label "Eff Date"       format "99/99/99"
 field fullLiability as decimal   {&nodeType} column-label "Full Liability" format "-zzz,zzz,zzz,zz9.99"
 field amountCeded   as decimal   {&nodeType} column-label "Amount Ceded"   format "-zzz,zzz,zzz,zz9.99"
 field fullPremium   as decimal   {&nodeType} column-label "Full Premium"   format "-zzz,zzz,zzz,zz9.99"
 field netPremium    as decimal   {&nodeType} column-label "Net Premium"    format "-zzz,zzz,zzz,zz9.99"
 index pu1 is primary unique agentID fileNumber policyID
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


