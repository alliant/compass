&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 task.i
 TASK data structure
 D.Sinclair 10.25.2013
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias task
&ENDIF
 
&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF
 
def temp-table {&tableAlias}
 field taskID as int {&nodeType}
 field uid as char  {&nodeType}      /* creator */
 field owner as char  {&nodeType}    /* current owner */
 field userids as char {&nodeType}   /* can-do clause: !* = private, * = everyone */
 field subject as char  {&nodeType}
 field agentID as char {&nodeType}
 field agentName as char {&nodeType}
 field entityType as char {&nodeType}  /* reference to a business object type */
 field entityID as char {&nodeType}    /* reference key to a specific business object */
 field module as char {&nodeType}     /* the expected application module to resolve the task (optional) */ 
 field createDate as datetime  {&nodeType}
 field startDate as datetime  {&nodeType}
 field dueDate as datetime  {&nodeType}
 field followupDate as datetime {&nodeType}
 field completeDate as datetime {&nodeType}
 field stat as char  {&nodeType}     /* N)ot started, I)n process, C)omplete, W)aiting, D)eferred, X)cancelled */
 field priority as char {&nodeType}  /* H)igh, N)ormal, L)ow */
 field comments as char {&nodeType}
 field history as char {&nodeType}
 field secure as logical {&nodeType} /* True = system generated; False = user */
 field duration as int {&nodeType}   /* Used to calculate a dueDate */
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 16.67
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


