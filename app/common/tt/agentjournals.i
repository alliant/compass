/*
 agentjournals.i
 agentjournals data structure
 Vignesh Rajan 03.27.2023
 */

&IF defined(tableAlias) = 0 &THEN
  &scoped-define tableAlias agentjournals
&ENDIF

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "agentjournals"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

&IF defined(nodeType) = 0 &THEN
  &scoped-define nodeType xml-node-type "attribute"
&ENDIF

def temp-table {&tableAlias} NO-UNDO {&serializeName}
  field Id         as character {&nodeType}
  field OfficeId   as integer   {&nodeType}
  field OfficeName as character {&nodeType}
  field UserName   as character {&nodeType}
  field FullName   as character {&nodeType}
  field Timestamp  as character {&nodeType}
  field Content    as CLOB      {&nodeType}
  field Latest     as logical   {&nodeType}
  .
