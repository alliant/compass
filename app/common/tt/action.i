&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : action.i
    Purpose     : Action datastructure
    Author(s)   : Rahul
    Created     : 7.11.2017
    Modification:
    Date          Name      Description
              
  ----------------------------------------------------------------------*/
  
&if defined(tableAlias) = 0 &THEN
&scoped-define tableAlias action
&endif

def temp-table {&tableAlias}
  field actionID           as integer column-label "Action" format ">>>>>9"
  field source             as character column-label "Source" format "x(50)"
  field sourceID           as character column-label "Source ID" format "x(50)"
  field targetProcess      as character serialize-hidden
  field findingID          as integer column-label "Finding" format ">>>>>9"
  field actionType         as character serialize-hidden
  field actionTypeDesc     as character column-label "Type" format "x(20)"
  field comments           as character column-label "Action" format "x(200)"
  field ownerID            as character serialize-hidden
  field ownerDesc          as character column-label "Owner" format "x(200)"
  field ownername          as character serialize-hidden
  field uid                as character serialize-hidden
  field stat               as character serialize-hidden
  field statDesc           as character column-label "Status" format "x(20)"
  field dueDate            as datetime column-label "Due Date" format "99/99/9999"
  field startDate          as datetime column-label "Start Date" format "99/99/9999"
  field completeStatus     as character serialize-hidden
  field completeStatusDesc as character column-label "Complete!Status" format "x(20)"
  field method             as character serialize-hidden
  field createdDate        as datetime column-label "Created" format "99/99/9999"
  field openedDate         as datetime column-label "Opened" format "99/99/9999"
  field completedDate      as datetime column-label "Completed" format "99/99/9999"
  field cancellationDate   as datetime column-label "Cancellation" format "99/99/9999"
  field notificationDate   as datetime column-label "Notification" format "99/99/9999"
  field originalDueDate    as datetime column-label "Orig. Due" format "99/99/9999"
  field extendedDate       as datetime column-label "Extended" format "99/99/9999"
  field followupDate       as datetime column-label "Follow-Up" format "99/99/9999"
  field referenceActionID  as character serialize-hidden
  field iconMessages       as character column-label "Alert Message" format "x(500)"
  field iconSeverity       as integer serialize-hidden
  field lastNote           as character column-label "Last Note" format "x(500)"
  
  /* for the client only */
  field severity           as integer serialize-hidden
  field msg                as character column-label "Alert Message" format "x(500)"
  field hasDoc             as logical column-label "Has!Doc"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


