/*------------------------------------------------------------------------
@name common/tt/imageinfo.i
@description 

@author Archana 
@version 1.0
@created 06/19/2020
Modification:
Date        Name             Description
----------------------------------------------------------------------*/
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias imageinfo
&ENDIF

def temp-table {&tableAlias}
  field entity       as char
  field entityID     as char
  field width        as char
  field height       as char
  field imgType      as char
  field compressSize as char.

