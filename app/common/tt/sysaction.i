&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : sysaction.i
    Purpose     : SYStem ACTION datastructure

    Syntax      :

    Description :

    Author(s)   : D.Sinclair
    Created     : 1.24.2012
    Notes       : 09.03.2021	MK	Framework changes
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias sysaction
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

def temp-table {&tableAlias}
 field action as char {&nodeType}
 field description as char {&nodeType}
 field progExec as char {&nodeType}
 field isActive as logical {&nodeType}
 field isAnonymous as logical {&nodeType}
 field isSecure as logical {&nodeType}
 field isLog as logical {&nodeType}
 field isAudit as logical {&nodeType}
 field roles as char {&nodeType}
 field userids as char {&nodeType}
 field addrs as char {&nodeType}
 field createDate as datetime {&nodeType}
 field emails as char {&nodeType}
 field emailSuccess as char {&nodeType}
 field emailFailure as char {&nodeType}
 field isEventsEnabled as logical {&nodeType}
 field comments as char {&nodeType}
 field isQueueable as logical {&nodeType}
 field isDestination as logical {&nodeType}
 field name as char {&nodeType}
 field htmlEmail as char {&nodeType}
 field subject as char {&nodeType}
 field progQueue as char {&nodeType}
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


