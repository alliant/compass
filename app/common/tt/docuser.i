&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : sfuser.i
    Purpose     : Sharefile User datastructure
    Author(s)   : B.Johson
    Created     : 4.16.2015
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias docuser
&ENDIF
 
&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

def temp-table {&tableAlias}
 field ID                   as character {&nodeType}
 field Name                 as character {&nodeType}
 field accountID            as character {&nodeType}
 field primaryEmail         as character {&nodeType}
 field company              as character {&nodeType}
 field accountEmployee      as logical   {&nodeType}
 field accountAdmin         as logical   {&nodeType}
 field canResetPassword     as logical   {&nodeType}
 field dateFormat           as character {&nodeType}
 field requireDownloadLogin as logical   {&nodeType}
 field canViewMySettings    as logical   {&nodeType}
 field zoneID               as character {&nodeType}
 
 /* fields for transferring to server */
 field firstName            as character {&nodeType}
 field lastName             as character {&nodeType}
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


