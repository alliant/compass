&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* severitychanges.i
  @author Anjly Chanana
  @created 07/26/2017
  08/02/2017 Yoke Sam C. - Change qarID field type to int.
  07/15/2021 SA          - Task 83510 add new fields score and grade
   ----------------------------------------------------------------------*/
   
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias severitychanges
&ENDIF
   
def temp-table {&tableAlias}
 field qarID as int
 field version as character
 field questionID as character
 field questionDesc as character
 field originalSeverity as character
 field reportedSeverity as character
 field changeDesc as character
 field agentID as character
 field agentName as character
 field auditor as character
 field state as character
 field auditFinishDate as datetime
 field errType as character
 field auditType as character
 field score as int
 field grade as int.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


