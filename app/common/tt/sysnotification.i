&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/sysnotification.i
Purpose     :

Syntax      :

Description : Used specifically for WEB CRM app 

Author(s)   : Gurvindar 
Created     : 05-24-2019
Notes       :

Modification:
Date          Name           Description
08/09/2019    Gurvindar      Added new field entityName 
----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias sysnotification
&ENDIF
   
def temp-table {&tableAlias}
  field sysNotificationID     as integer 
  field UID                   as character    
  field entityType            as character
  field entityID              as character  
  field stat                  as character  /* (S)upress , (N)ew , (D)ismiss */
  field refSysNotificationID  as integer 
  field notificationStat      as character  /* (R)ED,(G)REEN,(Y)ellow */
  field sourceType            as character   
  field sourceID              as character
  field dismissedDate         as datetime
  field notification          as character
  field createDate            as dateTime
  field rowNum                as character
 
  field entityName            as character /* Client side only */
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


