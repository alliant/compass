&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/reportagentmanager.i
   AGENT datastructure
   Author(s) John Oliver
   Created 5.8.2018
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias reportagentltd
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

define temp-table {&tableAlias}
  field agentID          as character {&nodeType}
  field stateID          as character {&nodeType} column-label "State"             format "x(20)"
  field stat             as character {&nodeType}
  field manager          as character {&nodeType}
  field corporationID    as character {&nodeType}
  field closedDate       as datetime  {&nodeType} column-label "Closed Date"       format "99/99/9999"
  field contractDate     as datetime  {&nodeType} column-label "Contract Date"     format "99/99/9999"
  field name             as character {&nodeType}
  field periodFirstBatch as integer   {&nodeType} column-label "First Batch"       format "999999"
  field periodLastBatch  as integer   {&nodeType} column-label "Last Batch"        format "999999"
  field numPolicies      as integer   {&nodeType} column-label "No. of!Policies"   format ">>>,>>>,>>9"
  field numForms         as integer   {&nodeType} column-label "No. of!Endsmts"    format ">>>,>>>,>>9"
  field numClaims        as integer   {&nodeType} column-label "No. of!Claims"     format ">>>,>>>,>>9"
  field netPremium       as decimal   {&nodeType} column-label "Net!Premium"       format "(>>>,>>>,>>>,>>9)"
  field grossPremium     as decimal   {&nodeType} column-label "Gross!Premium"     format "(>>>,>>>,>>>,>>9)"
  field costsIncurred    as decimal   {&nodeType} column-label "Claims!Costs Paid" format "(>>>,>>>,>>>,>>9)"
  field agentLTD         as decimal   {&nodeType} column-label "Net Value"         format "(>>>,>>>,>>>,>>9)"
  field agentDate        as datetime  {&nodeType} column-label "Date Signed"       format "99/99/9999"
  field agentDateShow    as character {&nodeType} column-label "Date Signed"       format "x(20)"
  /* for client only */
  field statDesc         as character {&nodeType} column-label "Current!Status"    format "x(20)"
  field stateName        as character {&nodeType} column-label "State"             format "x(100)"
  field managerDesc      as character {&nodeType} column-label "Current!Manager"   format "x(100)"
  field netPercent       as decimal   {&nodeType} column-label "% of!Costs Paid"   format "(>>>,>>>,>>9.9) %"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


