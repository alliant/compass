/*------------------------------------------------------------------------
    File        : consolidate.i
    Purpose     : consolidate datastructure
    Author(s)   : Rahul Sharma
    Created     : 10.27.2020
    Modification:
    Date          Name      Description
              
  ----------------------------------------------------------------------*/
  
&if defined(tableAlias) = 0 &THEN
&scoped-define tableAlias consolidate
&endif

define temp-table {&tableAlias}
  field fldKey     as character
  field fldValue   as character
  field fldDesc    as character
  field isChecked  as logical
  .


