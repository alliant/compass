&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/clm01.i
@desc CLaiM datastructure for aggregation reporting
@author D.Sinclair
@date 12.28.2016
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias clm01
&ENDIF

def temp-table {&tableAlias}
 field stateID as char
 field agentID as char
 field agentName as char /* Typically set on client to export more complete data */
 field claimID as int
 field claimIDDesc as char
 field stat as char
 field fileNumber as char /* Agent file number */
 field altaRiskCode as char
 field altaRiskDesc as char /* Typically set on client to export more complete data */
 field altaResponsibilityCode as char
 field altaResponsibilityDesc as char /* Typically set on client to export more complete data */
 field causeCode as char
 field coCause as char /* Typically set on client to export more complete data */
 field descriptionCode as char
 field coDescription as char /* Typically set on client to export more complete data */
 field dateOpened as date
 field yearOpened as int /* Typically set on client to export more complete data */
 field assignedTo as char
 
 field summary as char

 field laeOpen as dec
 field laeApproved as dec
 field laeLTD as dec
 field laeReserve as dec
 field laeAdjOpen as dec
 field laeAdjLTD as dec

 field lossOpen as dec
 field lossApproved as dec
 field lossLTD as dec
 field lossReserve as dec
 field lossAdjOpen as dec
 field lossAdjLTD as dec

 field ownerPolicyID as char
 field ownerPolicyEffDate as date
 field ownerPolicyInsuredType as char
 field ownerPolicyInsuredName as char
 field ownerPolicyOrigLiability as dec
 field ownerPolicyCurrLiability as dec
 field ownerPolicyFormID as char
 field ownerPolicyFormDesc as char
 field ownerPolicyFormType as char

 field lenderPolicyID as char
 field lenderPolicyEffDate as date
 field lenderPolicyInsuredType as char
 field lenderPolicyInsuredName as char
 field lenderPolicyOrigLiability as dec
 field lenderPolicyCurrLiability as dec
 field lenderPolicyFormID as char
 field lenderPolicyFormDesc as char
 field lenderPolicyFormType as char

 field coverageID as char
 field coverageEffDate as date
 field coverageCoverageType as char
 field coverageInsuredType as char
 field coverageInsuredName as char
 field coverageOrigLiability as dec
 field coverageCurrLiability as dec
 field coverageFormID as char
 field coverageFormDesc as char
 field coverageFormType as char
 
 field insuredName as char
 field insuredAddr as char
 field description as char
 field lastNoteDate as date
 field lastNoteText as char
 field noteCnt as int
 field stage as char
 field type as char
 field action as char
 field difficulty as int
 field urgency as int
 field litigation as log
 field significant as log
 field agentError as char
 field searcherError as char
 field attorneyError as char
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


