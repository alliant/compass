&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/claimcoverage.i
   datastructure for CLAIM COVERAGE (i.e. Policy, CPL, Commitment)
   Created D.Sinclair 2.15.2015
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias claimcoverage
&ENDIF

def temp-table {&tableAlias}
 field claimID as int
 field seq as int             /* System assigned sequence number */
 field coverageID as char		  /* PolicyID, CPL#, Commitment, etc. */
 field coverageType as char		/* O)wner, L)ender, Commi(T)ment, C)PL, Ownership & (E)ncumberance, P)lat Certificate */
 field insuredType as char    /* O)wner, L)ender, S)eller, O(t)her */
 field origLiabilityAmount as dec
 field currentLiabilityAmount as dec
 field effDate as datetime
 field coverageYear as int		/* Year of effective date for policy, cpl, etc. */
 field insuredName as char
 field claimant as char
 field isPrimary as logical 	/* Indicates this is the policy # for statutory 
                                 reporting using StatCode, etc. */
                                 
 field coverageTypeDesc as char   /* not in DB - pulled from list in clmdatasrv.p */
 field insuredTypeDesc as char    /* not in DB - pulled from list in clmdatasrv.p */
 .
 
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


