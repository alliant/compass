&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
  tt/distribution.i
  distribution list table model
  Author: John Oliver
  Date: 06.28.2024

*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias distribution
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF


define temp-table {&tableAlias} no-undo
  /* distributions */
  field distName        as character {&nodeType} column-label "Group"             format "x(200)"
  field description     as character {&nodeType} column-label "Group Description" format "x(1000)"
  
  /* contacts */
  field personContactID as integer   {&nodeType} serialize-hidden
  
  /* from other tables */
  field contactName     as character {&nodeType} column-label "Name"              format "x(200)"
  field agentStateID    as character {&nodeType} serialize-hidden
  field agentStat       as character {&nodeType} serialize-hidden
  field agentID         as character {&nodeType} serialize-hidden                 format "x(200)"
  field agentName       as character {&nodeType} column-label "Agent"             format "x(200)"
  field email           as character {&nodeType} column-label "Email"             format "x(200)"
  field doNotCall       as logical   {&nodeType} serialize-hidden
  field personID        as character {&nodeType} serialize-hidden
  field address         as character {&nodeType} column-label "Address"           format "x(1000)"
  
  /* descriptions */
  field agentStateDesc  as character {&nodeType} column-label "State"             format "x(100)"
  field agentStatDesc   as character {&nodeType} column-label "Status"            format "x(100)"
  
  /* used to tell if row is selected */
  field isSelected      as logical   {&nodeType} column-label ""                  serialize-hidden
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 1
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


