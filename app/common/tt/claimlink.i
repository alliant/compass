&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/claimlink.i
   capture the relationship of one claim to another
   D.Sinclair 4.23.2015
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias claimlink
&ENDIF

def temp-table {&tableAlias}
 field fromClaimID as int
 field toClaimID as int
 field linkType as char 	  /* S)upplemental, R)elated */

 /* Not in DB */
 field linkTypeDesc as char
 field fileNumber as char   /* Pulled from claim.fileNumber */
 field stat as char         /* O)pen, C)losed - from claim.stat */
 field statDesc as char
 field type as char  		    /* N)otice, C)laim, M)atter - from claim.type */
 field typeDesc as char
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


