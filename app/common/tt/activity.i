&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/activity.i
   AGENT PLAN datastructure
   Author(s) John Oliver
   Created 2/14/2018
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias activity
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

define temp-table {&tableAlias}
  field activityID as integer {&nodeType}
  field name          as character {&nodeType} column-label "Agent Name"    format "x(200)"
  field agentID       as character {&nodeType} column-label "Agent"         format "x(20)"
  field stateID       as character {&nodeType} column-label "State"         format "x(30)"
  field corporationID as character {&nodeType} column-label "Organization"  format "x(200)"
  field year          as integer   {&nodeType} column-label "Year"
  field type          as character {&nodeType}
  field stat          as character {&nodeType}
  field category      as character {&nodeType}
  field month1        as decimal   {&nodeType} column-label "Jan "      format "(>>>,>>>,>>9)"
  field month2        as decimal   {&nodeType} column-label "Feb "      format "(>>>,>>>,>>9)"
  field month3        as decimal   {&nodeType} column-label "Mar "      format "(>>>,>>>,>>9)"
  field month4        as decimal   {&nodeType} column-label "Apr "      format "(>>>,>>>,>>9)"
  field month5        as decimal   {&nodeType} column-label "May "      format "(>>>,>>>,>>9)"
  field month6        as decimal   {&nodeType} column-label "Jun "      format "(>>>,>>>,>>9)"
  field month7        as decimal   {&nodeType} column-label "Jul "      format "(>>>,>>>,>>9)"
  field month8        as decimal   {&nodeType} column-label "Aug "      format "(>>>,>>>,>>9)"
  field month9        as decimal   {&nodeType} column-label "Sep "      format "(>>>,>>>,>>9)"
  field month10       as decimal   {&nodeType} column-label "Oct "      format "(>>>,>>>,>>9)"
  field month11       as decimal   {&nodeType} column-label "Nov "      format "(>>>,>>>,>>9)"
  field month12       as decimal   {&nodeType} column-label "Dec "      format "(>>>,>>>,>>9)"
  /* used for client only */
  field qtr1          as decimal   {&nodeType} column-label "Qtr 1 total"   format "(>>>,>>>,>>9)"
  field qtr2          as decimal   {&nodeType} column-label "Qtr 2 Total"   format "(>>>,>>>,>>9)"
  field qtr3          as decimal   {&nodeType} column-label "Qtr 3 Total"   format "(>>>,>>>,>>9)"
  field qtr4          as decimal   {&nodeType} column-label "Qtr 4 Total"   format "(>>>,>>>,>>9)"
  field yrTotal       as decimal   {&nodeType} column-label "Yearly Total"  format "(>>>,>>>,>>>,>>9)"
  field statDesc      as character {&nodeType} column-label "Status"        format "x(30)"
  field typeDesc      as character {&nodeType} column-label "Type"          format "x(100)"
  field categoryDesc  as character {&nodeType} column-label "Category"      format "x(100)"
  field rowColor      as integer   {&nodeType}
  field total         as integer   {&nodeType} column-label "Total"         format "(>>>,>>>,>>>,>>9)"
  field stateName     as character {&nodeType} column-label "State"         format "x(100)"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


