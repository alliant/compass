&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/arrevenue.i
Purpose     : used to fetch/store AR Transaction records

Syntax      :

Description : Used specifically for AR app 

Author(s)   : Rahul Sharma
Created     : 01-06-2020
Notes       : 

Modification:
Date         Name           Description

----------------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias arrevenue
&endif
   
def temp-table {&tableAlias}
  field revenueType      as character
  field grossGLRef       as character
  field grossGLDesc      as character 
  field retainedGLRef    as character
  field retainedGLDesc   as character
  field netGLRef         as character
  field netGLdesc        as character
  field notes            as character 
  field createdDate      as datetime  
  field createdBy        as character
  field modifiedDate     as datetime 
  field modifiedBy       as character
  field cUsername        as character /* Client side */
  field mUsername        as character /* Client side */
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


