/*------------------------------------------------------------------------
@file tt/idverificationrate.i
Datastructure for a idverificationrate
@created 11.27.2024
@author SRK
Modifications:
Date          Name           Description
   
---------------------------------------------------------------------- */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias idverificationrate
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "idverificationrate"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo  {&serializeName}
  field Agent   as  character  {&nodeType} 
  field Rate    as  decimal    {&nodeType}
  field Tax     as  decimal    {&nodeType}

  
  .

