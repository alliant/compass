&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------------------------
    File        : sysuser.i
    Purpose     : SYStem USER datastructure
    Author(s)   : D.Sinclair
    Created     : 1.24.2012
    Modification:
    Date            Name          Description
    10/03/2018    Rahul Sharma    Modified to add passwordSetDate and passwordExpired fields            
    10/11/2018    Rahul Sharma    Modified to add pwdExpireDays	
    03/26/2019    Rahul Sharma    Added new field "deptID"
  ------------------------------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias sysuser
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF
 
define temp-table {&tableAlias}
  field uid             as character {&nodeType} column-label "User ID"          format "x(100)"
  field name            as character {&nodeType} column-label "Name"             format "x(100)"
  field initials        as character {&nodeType} column-label "Initials"         format "x(20)"
  field email           as character {&nodeType} column-label "Email"            format "x(200)"
  field role            as character {&nodeType} column-label "Role(s)"          format "x(200)"
  field password        as character {&nodeType} serialize-hidden
  field isActive        as logical   {&nodeType} serialize-hidden
  field createDate      as datetime  {&nodeType} column-label "Create Date"      format "99/99/9999"
  field comments        as character {&nodeType} column-label "Comments"         format "x(4000)"
  field passwordSetDate as datetime  {&nodeType} serialize-hidden
  field pwdExpireDays   as integer   {&nodeType} serialize-hidden
  field passwordExpired as logical   {&nodeType} serialize-hidden
  field department      as character {&nodeType} column-label "Department"       format "x(100)"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


