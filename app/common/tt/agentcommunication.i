/*------------------------------------------------------------------------
File        : tt/agentcommunication.i
Purpose     :

Syntax      :

Description : temp-table for agentcommunication
                                           
Author(s)   : sagar K
Created     : 18-04-2023
Notes       :
----------------------------------------------------------------------*/
&if defined(tableAlias) = 0 &then
&scoped-define tableAlias agentcommunication
&endif
&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "agentcommunication"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif
  
define temp-table {&tableAlias} no-undo {&serializeName}
    field period                as character    {&nodeType} 
    field year                  as character    {&nodeType} 
    field month                 as character    {&nodeType} 
    field AgentID               as character    {&nodeType}  
    field name                  as character    {&nodeType} 
    field agent                 as character    {&nodeType}  
	field Stat                  as character    {&nodeType} 
    field statDesc              as character    {&nodeType}
	field State                 as character    {&nodeType}
    field stateName             as character    {&nodeType} 
	field Manager               as character    {&nodeType}
    field Region                as character    {&nodeType}
	field NetPlan               as decimal      {&nodeType}  format "(ZZZ,ZZZ,ZZZ)"
	field Netpremium            as decimal      {&nodeType}  format "(ZZZ,ZZZ,ZZZ)"
    field LenderpolicyReported  as character    {&nodeType}
    field OwnerPolicyReported   as character    {&nodeType}
    field CPLReported           as character    {&nodeType}
    field AgentCommunication    as integer      {&nodeType} format "(ZZZ,ZZZ,ZZZ)"
    field uwCommunication       as integer      {&nodeType} format "(ZZZ,ZZZ,ZZZ)"
    field LenderFiles           as decimal      {&nodeType} format "(ZZZ,ZZZ,ZZZ)"
    field OwnerFiles            as decimal      {&nodeType} format "(ZZZ,ZZZ,ZZZ)"
    field OwnerLenderFiles      as decimal      {&nodeType} format "(ZZZ,ZZZ,ZZZ)"
    field TotalFiles            as integer      {&nodeType} format "(ZZZ,ZZZ,ZZZ)"
    
    index AgentID AgentID
       

    
    
    .
  
