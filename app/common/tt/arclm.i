&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/arclm.i
   Temporary data structure for Claims A/R transactions
   This table combines the fields of 'arinv' and 'artrx'
   This table is not in the database
   B.Johnson 5.14.2015
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias arclm
&ENDIF

def temp-table {&tableAlias}
 field seq as int 			/* Temporary sequence for sorting */
 field artrxID as int

 field transType as char     /* A)pproved / received, V)oided */
 field transAmount as deci   /* Actual amount received */
 field transDate as datetime /* Date received/deposited */

 field arinvID as int
 field stat as char          /* O)pen, R)eceived, V)oided */

 field refType as char
 field refID as char
 field refSeq as int
 field refCategory as char

 field uid as char
 field notes as char
 
 /* The following fields will be pulled from 'arinv'. */
 field name as char
 field addr1 as char
 field addr2 as char
 field city as char
 field stateID as char
 field zipcode as char
 field contactName as char
 field contactPhone as char
 field contactEmail as char

 field invoiceNumber as character
 field dateRequested as datetime
 field dueDate as datetime
 field requestedAmount as deci
 field waivedAmount as deci
 
 field hasDocument as logical
 field documentFilename as character
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


