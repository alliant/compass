&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 cpl.i
 Closing Protection Letter data structure
 D.Sinclair 5.30.2012
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias cpl
&ENDIF
 
&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF
 
DEF TEMP-TABLE {&tableAlias}
  FIELD cplID           AS CHARACTER {&nodeType}
  FIELD formID          AS CHARACTER {&nodeType}
  FIELD stateID         AS CHARACTER {&nodeType}
  FIELD statCode        AS CHARACTER {&nodeType}
  FIELD agentID         AS CHARACTER {&nodeType}
  FIELD fileNumber      AS CHARACTER {&nodeType}
  FIELD cleanFileNumber AS CHARACTER {&nodeType}
  FIELD name            AS CHARACTER {&nodeType}
  FIELD addr1           AS CHARACTER {&nodeType}
  FIELD addr2           AS CHARACTER {&nodeType}
  FIELD addr3           AS CHARACTER {&nodeType}
  FIELD addr4           AS CHARACTER {&nodeType}
  FIELD city            AS CHARACTER {&nodeType}
  FIELD county          AS CHARACTER {&nodeType}
  FIELD state           AS CHARACTER {&nodeType}
  FIELD zip             AS CHARACTER {&nodeType}
  FIELD stat            AS CHARACTER {&nodeType} /* (I)ssued, (P)rocessed, (V)oided */
  FIELD issueDate       as datetime  {&nodeType} 
  FIELD voidDate        as datetime  {&nodeType}
  FIELD closeDate       as datetime  {&nodeType}
  FIELD liabilityAmount AS DECIMAL   {&nodeType}
  FIELD reportDate      AS date      {&nodeType}
  FIELD paidDate        AS date      {&nodeType}
  FIELD escrowBank      AS CHARACTER {&nodeType}
  FIELD escrowAcct      AS CHARACTER {&nodeType}
  FIELD liability       AS DECIMAL   {&nodeType}
  FIELD grossPremium    AS DECIMAL   {&nodeType}
  FIELD netPremium      AS DECIMAL   {&nodeType}
  FIELD retention       AS DECIMAL   {&nodeType}
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 16.67
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


