&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/reportagentmanager.i
   AGENT datastructure
   Author(s) John Oliver
   Created 5.8.2018
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias reportagentmanager
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

define temp-table {&tableAlias}
  field agentID as character {&nodeType} column-label "Agent" format "999999"
  field stateID as character {&nodeType}
  field stat as character {&nodeType}
  field manager as character {&nodeType}
  field managerType as character {&nodeType}
  field managerStat as character {&nodeType}
  field name as character {&nodeType} column-label "Agent Name" format "x(200)"
  field effDate as datetime {&nodeType} column-label "Manager!Eff. Date" format "99/99/9999"
  field numPolicies as integer {&nodeType} column-label "No. of!Policies" format "ZZZ,ZZZ,ZZ9"
  field numForms as integer {&nodeType} column-label "No. of!Endorsements" format "ZZZ,ZZZ,ZZ9"
  field netPremium as decimal {&nodeType} column-label "Net Premium" format "(ZZZ,ZZZ,ZZZ,ZZZ,ZZ9)"
  field grossPremium as decimal {&nodeType} column-label "Gross Premium" format "(ZZZ,ZZZ,ZZZ,ZZZ,ZZ9)"
  /* for client only */
  field statDesc as character {&nodeType} column-label "Status" format "x(20)"
  field stateDesc as character {&nodeType} column-label "State" format "x(50)"
  field managerDesc as character {&nodeType} column-label "Manager" format "x(100)"
  field managerTypeDesc as character {&nodeType} column-label "Manager Type" format "x(20)"
  field managerStatDesc as character {&nodeType} column-label "Manager!Status" format "x(20)"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


