&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/arpostedtran.i
Purpose     : used to fetch Posted transaction of Invoices, Payments, Credits

Syntax      :

Description : Used specifically for AR app 

Author(s)   : Rahul Sharma
Created     : 07-16-2019
Notes       : 

Modification:
Date          Name           Description
07/18/19      Rahul Sharma   Modified to add new fields
----------------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias arpostedtran
&endif
   
def temp-table {&tableAlias}

  field arpostedtranID         as integer
  field selectRecord         as logical
/*------------applicable to all-------------------*/
  field arpmtID         as integer
  field artranID        as integer
  field entity          as character
  field entityID        as character
  field description     as character
  field notes           as character 
  field createddate     as datetime
  field createdby       as character
  field invoiceamt      as decimal
  field remainingamt    as decimal  
  field appliedamt      as decimal   
  field username        as character 
  field entityname      as character  
 
 /*--------------applicable to invoice-----------------*/
  field invoiceID       as character
  field sourcetype      as character
  field sourceID        as character
  field invoicedate     as datetime
  field duedate         as datetime
  field fullypaid       as logical
  
 /*--------------applicable to credit-----------------*/
  
 /*---------applicable to invoice and credit------------*/
  field revenuetype     as character
  field type            as character
  field filenumber      as character

 /*---------applicable to payment and credit------------*/
  field void            as logical
  field voiddate        as datetime
  field voidby          as character
  
 /*--------------applicable to payment-----------------*/   
  field checknum        as character
  field receiptdate     as datetime
  field checkamt        as decimal
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


