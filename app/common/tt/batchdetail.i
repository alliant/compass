&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 batchform.i
 FORM entered during BATCH processing
 5.2.2012 D.Sinclair
 Modification:
    Date         Name        Description
    02-08-2019   Vikas Jain  Added fields formCount, revenueType   
    09-03-2019   Vikas Jain  Added field fileID
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias batchform
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF
 
define temp-table {&tableAlias}
 field batchID          as integer   {&nodeType} column-label "Batch ID"        format "99999999"
 field seq              as integer   {&nodeType} column-label "Seq"             format "ZZZZ9"
 field fileNumber       as character {&nodeType} column-label "File Number"     format "x(20)"
 field fileID           as character {&nodeType} column-label "File ID"         format "x(20)"
 field formType         as character {&nodeType} column-label "Form Type"       format "x(10)"
 field formID           as character {&nodeType} column-label "Form"            format "x(15)"               
 field policyID         as integer   {&nodeType} column-label "Policy"          format "ZZZZZZZZZ"
 field policyChar       as character {&nodeType} serialize-hidden
 field statCode         as character {&nodeType} column-label "Stat"            format "x(10)"
 field effDate          as datetime  {&nodeType} column-label "Eff. Date"       format "99/99/9999"
 field countyID         as character {&nodeType} column-label "County"          format "x(100)"               
 field muniID           as character {&nodeType} column-label "Municipal ID"    format "x(100)"         
 field cplID            as character {&nodeType} column-label "CPL ID"          format "x(20)"
 field liabilityAmount  as decimal   {&nodeType} column-label "Liability"       format "$(>>>,>>>,>>>,>>9)"
 field grossPremium     as decimal   {&nodeType} column-label "Gross Premium"   format "$(>>>,>>>,>>9.99)"
 field netPremium       as decimal   {&nodeType} column-label "Net Premium"     format "$(>>>,>>>,>>9.99)"
 field retentionPremium as decimal   {&nodeType} column-label "Ret. Premium"    format "$(>>>,>>>,>>9.99)"
 field insuredType      as character {&nodeType} column-label "Insured Type"    format "x(10)"
 field residential      as logical   {&nodeType} column-label "Residential"
 field liabilityDelta   as decimal   {&nodeType} column-label "Liability Delta" format "$(>>>,>>>,>>>,>>9.99)"
 field grossDelta       as decimal   {&nodeType} column-label "Gross Delta"     format "$(>>>,>>>,>>9.99)"
 field netDelta         as decimal   {&nodeType} column-label "Net Delta"       format "$(>>>,>>>,>>9.99)"
 field retentionDelta   as decimal   {&nodeType} column-label "Ret. Delta"      format "$(>>>,>>>,>>9.99)"
 field reprocess        as logical   {&nodeType} serialize-hidden
 field validPolicy      as logical   {&nodeType} serialize-hidden
 field validForm        as character {&nodeType} column-label "Verified"        format "x(2)"
 field validMsg         as character {&nodeType} column-label "Messages"        format "x(100)"
 field rateCode         as character {&nodeType} column-label "Rate Code"       format "x(20)"
 field createdDate      as datetime  {&nodeType} column-label "Created Date"    format "99/99/9999"
 field zipcode          as character {&nodeType} column-label "Zip Code"        format "x(20)"
 field formCount        as integer   {&nodeType} column-label "Form Count"      format ">>9"
 field revenueType      as character {&nodeType} column-label "Revenue Type"    format "x(20)"
                                                                                
 /* descriptions */                                                             
 field insuredTypeDesc  as character {&nodeType} serialize-hidden
 field formTypeDesc     as character {&nodeType} serialize-hidden
 field formIDDesc       as character {&nodeType} serialize-hidden
 
 index batchSeq is primary unique
  batchID
  seq
 index filePolicy
  fileNumber
  policyID
 index policyFile
  policyID
  fileNumber
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


