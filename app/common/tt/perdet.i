&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 perdet.i
 Temp-table definition for Period Details Report (wops08-r.w)
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias perdet
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF
 
define temp-table {&tableAlias}
  field agentID          as character {&nodeType} column-label "AgentID"           format "x(10)"
  field name             as character {&nodeType} column-label "Agent"             format "x(200)" 
  field formID           as character {&nodeType} column-label "Form ID"           format "x(8)"
  field formCode         as character {&nodeType} serialize-hidden
  field policyID         as integer   {&nodeType} column-label "Policy ID"         format ">>>>>>>>9"
  field issueDate        as date      {&nodeType} column-label "Policy!Issue Date" format "99/99/9999"
  field paidDate         as date      {&nodeType} column-label "Policy!Paid Date"  format "99/99/9999"
  field fileNumber       as character {&nodeType} column-label "File!Number"       format "x(30)"
  field postYear         as integer   {&nodeType} column-label "Year"              format "9999"
  field postMonth        as integer   {&nodeType} column-label "Month"             format "99"
  field batchID          as integer   {&nodeType} column-label "Batch ID"          format "99999999"
  field formType         as character {&nodeType} column-label "Form!Type"         format "x(4)"
  field statCode         as character {&nodeType} column-label "STAT!Code"         format "x(12)"
  field rateCode         as character {&nodeType} column-label "Rate!Code"         format "x(4)"
  field liabilityAmount  as decimal   {&nodeType} column-label "Liability"         format "-zzz,zzz,zz9.99"
  field reprocess        as logical   {&nodeType} column-label "Re-!Proc"          format "Yes/No"
  field grossPremium     as decimal   {&nodeType} column-label "Gross!Premium"     format "-zzz,zzz,zz9.99"
  field retentionPremium as decimal   {&nodeType} column-label "Retention!Premium" format "-zzz,zzz,zz9.99"
  field netPremium       as decimal   {&nodeType} column-label "Net!Premium"       format "-zzz,zzz,zz9.99"
  field stateID          as character {&nodeType} column-label "ST"                format "x(4)"
  field countyID         as character {&nodeType} column-label "County"            format "x(25)"
  field propType         as character {&nodeType} column-label "Prop!Type"         format "x(4)"
  field effDate          as date      {&nodeType} column-label "Effective!Date"    format "99/99/9999"
  
  /* description */
  field stateDesc        as character {&nodeType} serialize-hidden
  index pn1 is primary agentID
  index sn1 stateID
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


