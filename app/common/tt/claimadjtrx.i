&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/claimadjtrx.i
   ADJustment to a COST bucket
   D.Sinclair 4.23.2015
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias claimadjtrx
&ENDIF

def temp-table {&tableAlias}
 field claimID as int
 field refCategory as char       /* L)oss, LA(E), I)ncidental */
 field seq as int
 field requestedAmount as deci
 field requestedBy as char
 field dateRequested as datetime
 field notes as char

 field uid as char               /* Approver */
 field transDate as datetime     /* Date affecting accounting */
 field transAmount as deci       /* Amount to adjust the balance */
 field completeDate as datetime
 
 /* not in the database */
 field requestedUsername as char
 field username as char
 .

/* Index:  claimID, refCategory, transDate 

   This record is created when a claimadjreq is approved.
   This record cannot be modified or deleted.
*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 14.91
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


