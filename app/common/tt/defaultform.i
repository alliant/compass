/*------------------------------------------------------------------------
    File        : defaultForm.i
    Purpose     : linking an insuring FORM to an AGENT or STATE for default endorsements
    Author(s)   : D.Sinclair
    Created     : 1.24.2012
    Notes       :
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
  &scoped-define tableAlias defaultForm
&ENDIF

&IF defined(nodeType) = 0 &THEN
  &scoped-define nodeType xml-node-type "attribute"
&ENDIF

def temp-table {&tableAlias} no-undo
  field ID as char {&nodeType}             /* Contains either the AgentID or StateID (default) */
  field type as char {&nodeType}            /* D)efault, N)on-Default */
  field insuredType as char {&nodeType}     /* O)wner, L)ender */
  field formID as char {&nodeType}          /* foreign key data */

  field seq as int {&nodeType}              /* Visualization order 1-10 */

  field description as char {&nodeType}   

  field include as logical {&nodeType}      /* default, overwriteable in UI to select */
  field statCode as char {&nodeType}        /* default, overwriteable in UI */
  field grossPremium as decimal {&nodeType} /* default from stateform, overwriteable in UI */
  field netPremium as decimal {&nodeType}   /* default from stateform, overwriteable in UI */
  
  index idseq is primary unique
    ID
    insuredType
    seq
  .
