&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
@file form.i
@description The datastructure for the iFORM table
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias formtable
&ENDIF

define temp-table {&tableAlias}
  field formID as integer
  field revisionID as integer
  field stat as character /* (N)ew, (A)pproved, (P)ublished */
  field formType as character /* (C)ommitment, (E)ndorsement, (G)uarantee, (C)PL, (M)iscellaneous, (P)olicy, (S)chedule */
  field insuredType as character
  field orgName as character /* (A)LTA, (C)LTA, (I)nternal, or (S)tate */
  field orgVersion as character
  field orgEffDate as datetime
  field orgPubDate as datetime
  field orgReference as character
  field repositoryID as character
  field description as character /* F)orm or T)emplate */
  field formTitle as character
  field formHeader as character
  field formFooter as character
  field anticNumber as character
  field dateCreated as datetime
  field createdBy as character
  field dateModified as datetime
  field modifiedBy as character
  field parentID as character
  
  /* not in the database */
  field isLocked as logical
  field lockedUser as character
  field lockedDate as datetime
  field hasNotes as logical
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


