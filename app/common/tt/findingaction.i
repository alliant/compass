&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : findingaction.i
    Purpose     : Combination of action and finding
    Author(s)   : John Oliver
    Created     : 12.14.2018
    Modification:
    Date          Name      Description
              
  ----------------------------------------------------------------------*/
  
&if defined(tableAlias) = 0 &THEN
&scoped-define tableAlias findingaction
&endif

def temp-table {&tableAlias}
  /* common table elements */
  field findingactionID    as character serialize-hidden
  field entity             as character column-label "Entity" format "x(50)"
  field entityID           as character column-label "Entity ID" format "x(50)"
  field refType            as character serialize-hidden
  field refID              as character serialize-hidden
  field source             as character column-label "Source" format "x(50)"
  field sourceID           as character column-label "Source ID" format "x(50)"
  field question           as character column-label "Question" format "x(200)"
  field finding            as character column-label "Finding" format "x(200)"
  field action             as character column-label "Action" format "x(200)"
  field ownerID            as character serialize-hidden
  field ownerDesc          as character column-label "Owner" format "x(200)"
  field severity           as integer serialize-hidden
  field iconSeverity       as integer serialize-hidden
  
  /* finding table elements */
  field findingID          as integer column-label "Finding!ID" format ">>>>>9"
  field severityDesc       as character column-label "Severity" format "x(20)"
  field description        as character column-label "Description" format "x(1000)"
  field stage              as character serialize-hidden
  field stageDesc          as character column-label "Stage" format "x(20)"
  field findingUID         as character serialize-hidden
  field findingUIDDesc     as character column-label "Finding User" format "x(100)"
  field findingCreatedDate as datetime column-label "Created Date" format "99/99/9999"
  field findingClosedDate  as datetime column-label "Closed Date" format "99/99/9999"
  
  /* action table elements */
  field actionID           as integer column-label "Action!ID" format ">>>>>9"
  field targetProcess      as character serialize-hidden
  field actionType         as character serialize-hidden
  field actionTypeDesc     as character column-label "Action!Type" format "x(20)"
  field actionUID          as character serialize-hidden
  field actionUIDDesc      as character column-label "Action User" format "x(100)"
  field stat               as character serialize-hidden
  field statDesc           as character column-label "Status" format "x(20)"
  field startDate          as datetime column-label "Start Date" format "99/99/9999"
  field completeStatus     as character serialize-hidden
  field completeStatusDesc as character column-label "Complete!Status" format "x(20)"
  field method             as character serialize-hidden
  field actionCreatedDate  as datetime column-label "Created Date" format "99/99/9999"
  field openedDate         as datetime column-label "Opened Date" format "99/99/9999"
  field completedDate      as datetime column-label "Completed!Date" format "99/99/9999"
  field cancellationDate   as datetime column-label "Cancellation!Date" format "99/99/9999"
  field notificationDate   as datetime column-label "Notification!Date" format "99/99/9999"
  field originalDueDate    as datetime column-label "Original!Due Date" format "99/99/9999"
  field dueDate            as datetime column-label "Due Date" format "99/99/9999"
  field extendedDate       as datetime column-label "Extended!Date" format "99/99/9999"
  field followupDate       as datetime column-label "Follow-Up!Date" format "99/99/9999"
  field referenceActionID  as character serialize-hidden
  field iconMessages       as character column-label "Alert Message" format "x(500)"
  field lastNote           as character column-label "Last Note" format "x(500)"
  field hasDoc             as logical column-label "Has!Doc"
  
  /* notification report */
  field dueIn              as integer column-label "Due In" format "->>>>>9"
  field sendNotifications  as logical column-label "Send?"
  
  /* approval report */
  field approvalStat       as character serialize-hidden
  field approvalStatDesc   as character column-label "Approval!Status" format "x(20)"
  field approvalDate       as datetime column-label "Approval!Date" format "99/99/9999"
  field sentDate           as datetime column-label "Sent Date" format "99/99/9999"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


