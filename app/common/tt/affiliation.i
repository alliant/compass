&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/affiliation.i
Purpose     :

Syntax      :

Description :

Author(s)   : Rahul Sharma
Created     : 09-04-2019
Notes       :

Modification:
Date          Name      Description
09/16/2022    SA       Task #96812 Modified to add field related to agentperson 
----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias affiliation
&endif
   
def temp-table {&tableAlias}
  field affiliationID    as integer
  field partnerA         as character
  field partnerAType     as character
  field partnerB         as character
  field partnerBType     as character
  field natureOfAtoB     as character
  field natureOfBtoA     as character
  field isCtrlPerson     as logical
  field notes            as character
  field effDate          as datetime
  field expDate          as datetime 
                        
  field partnerAName     as character    /* used for client only */ 
  field partnerBName     as character    /* used for client only */  
  field tag              as character    /* used for client only */  
  field email            as character    /* used for client only */  
  field phone            as character    /* used for client only */  
  field jobTitle         as character    /* used for client only */  
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


