&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
  tt/unreportpolicy-detail.i
  Unremitted policies temp table
  Author: John Oliver
  Date: 2023.05.12

*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias unreportpolicy-detail
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF


define temp-table {&tableAlias} no-undo
  field officeID        as character {&nodeType} serialize-hidden
  field officeName      as character {&nodeType} serialize-hidden
  field officeAddress   as character {&nodeType} serialize-hidden
  field agentID         as character {&nodeType} serialize-hidden
  field agentName       as character {&nodeType} serialize-hidden
  field regionID        as character {&nodeType} serialize-hidden
  field regionDesc      as character {&nodeType} serialize-hidden
  field managerID       as character {&nodeType} serialize-hidden
  field managerName     as character {&nodeType} serialize-hidden
  field stateID         as character {&nodeType} serialize-hidden
  field stateDesc       as character {&nodeType} serialize-hidden
  field fileNumber      as character {&nodeType} column-label "File Number"      format "x(50)"
  field policyID        as integer   {&nodeType} column-label "Policy"           format "ZZZZZZZZ"
  field issueDate       as datetime  {&nodeType} column-label "Policy Date"      format "99/99/9999"
  field liabilityAmount as decimal   {&nodeType} column-label "Liability Amount" format "$(>>>,>>>,>>>,>>9)"
  field grossPremium    as decimal   {&nodeType} column-label "Gross Premium"    format "$(>>>,>>>,>>9.99)"
  field daysLate        as integer   {&nodeType} column-label "Days Late"        format ">>,>>9"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 1
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


