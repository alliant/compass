&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/calcOutputParameters.i
Purpose     : 

Syntax      :

Description :  

Author(s)   : Rahul Sharma
Created     : 04-28-2020
Notes       : 
----------------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias calcOutputParameters
&endif
   
def temp-table {&tableAlias}
fields scenarioID                    as integer
fields scenarioName                  as character
fields ttpremiumOwner                as character
fields ttpremiumLoan                 as character
fields ttpremiumOEndors              as character
fields ttpremiumLEndors              as character
fields ttpremiumOEndorsDetail        as character
fields ttpremiumLEndorsDetail        as character
fields ttpremiumScnd                 as character
fields ttpremiumSEndors              as character
fields ttpremiumSEndorsDetail        as character
fields ttpremiumLenderCPL            as character
fields ttpremiumBuyerCPL             as character
fields ttpremiumSellerCPL            as character
fields ttsuccess                     as character
fields tteffectiveDate               as character
fields ttlenderCPL                   as character
fields ttbuyerCPL                    as character
fields ttsellerCPL                   as character
fields ttratetypecode                as character
fields ttloanRateTypecode            as character
fields ttsecondloanRateType          as character
fields ttsecondloanCoverageAmount    as character
fields ttsecondLoanEndors            as character
.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


