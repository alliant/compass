&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/agentaudit.i
Purpose     : get agent audits

Syntax      :

Description : Used specifically for WEB CRM app 

Author(s)   : Rahul Sharma
Created     : 06-20-2019
Notes       : 

Modification:
Date          Name           Description
08/13/2021    SA             Task 83510 add new fields grade
----------------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias agentaudit
&endif
   
def temp-table {&tableAlias}
  field auditID      as character
  field auditor      as character  
  field uid          as character
  field score        as integer
  field auditType    as character
  field stat         as character
  field errType      as character
  field bestpractice as integer
  field rowNum       as character
  field auditStartDate  as datetime
  field schedStartDate  as datetime
  field auditFinishDate as datetime
  field schedFinishDate as datetime
  field grade as integer
  
  /* findings */
  field majorfinding        as integer
  field minorfinding        as integer
  field intermediatefinding as integer
  
  /* actions */
  field corrective  as integer
  field recommended as integer
  field suggested   as integer
    
  /* list of section score */
  field sectionscore as character 
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


