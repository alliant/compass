  /* bppareto.i
   Archana Gupta 06.21.2017 
   ----------------------------------------------------------------------*/  
          
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias bppareto
&ENDIF
   
def temp-table {&tableAlias}
 field cnt as int
 field pct as decimal
 field questionID as char
 field description as char.
