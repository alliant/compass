/* tt/filear.i
   Datastructure for an filear
   @created 05.01.2024
   @author Shefali 
   Modifications:
   Date          Name           Description
   11/26/2204    Chandu       Modified to add new field name 'referenceID' in
                              place of 'arTranID'.  
   12/11/2024    Chandu       Added artranID as field.                          
 */

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias filear
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "{&tableAlias}"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} {&serializeName}
  field filearID	    as  integer    {&nodeType}
  field agentfileID	    as  integer    {&nodeType}
  field sourceType      as  character  {&nodeType}
  field seq             as  integer    {&nodeType}
  field agentID      	as  character  {&nodeType}
  field agent        	as  character  {&nodeType}
  field agentName       as  character  {&nodeType}
  field fileID          as  character  {&nodeType}
  field type         	as  character  {&nodeType}
  field unpostedAmount  as  decimal    {&nodeType}
  field amount	        as  decimal    {&nodeType}
  field referenceID	    as  integer    {&nodeType}
  field artranID	    as  integer    {&nodeType}
  field invoicedAmount	as  decimal    {&nodeType}
  field cancelledAmount	as  decimal    {&nodeType}
  field paidAmount	    as  decimal    {&nodeType}
  field selectrecord    as  logical    {&nodeType}
  field cancel          as  logical    {&nodeType}
  field cancelDate      as  datetime   {&nodeType}
  field cancelReason    as  character  {&nodeType}
  field tranDate        as  datetime   {&nodeType}
  field remainingamt    as  decimal    {&nodeType}
  .
