/*
 agentjournalsfiles.i
 agentjournalsfiles data structure
 Vignesh Rajan 03.27.2023
 */


&if defined(tableAlias) = 0 &then
&scoped-define tableAlias agentJournalsFiles
&endif

&if defined(nodeType) = 0 &then
&scoped-define nodeType xml-node-type "attribute"
&else
&scoped-define nodeType xml-node-type "{&nodeType}"
&endif

&if defined(serializeName) = 0 &then
&scoped-define serializeName serialize-name "agentJournalsFiles"
&else
&scoped-define serializeName serialize-name "{&serializeName}"
&endif

define temp-table {&tableAlias} no-undo {&serializeName}
  field journalID    as character {&nodeType}
  field Id           as character {&nodeType}
  field OriginalName as character {&nodeType}
  field AltName      as character {&nodeType}
  field CreatedDate  as character {&nodeType}
  field UserName     as character {&nodeType}
  field fld          as character {&nodeType} SERIALIZE-NAME 'Field'
  field Extension    as character {&nodeType}
  field Size         as integer   {&nodeType}
   
  .
