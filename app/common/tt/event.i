&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : event.i
    Purpose     : event temp-table definition

    Syntax      :

    Description :

    Author(s)   : Sachin Chaturvedi
    Created     : 02/19/2019
    Notes       :
    Modification:
    Date          Name              Description
    05/07/2020  Sachin Chaturvedi   Added new fields
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias event
&ENDIF
define temp-table {&tableAlias}
  field eventID          as integer
  field entity           as character
  field entityID         as character
  field type             as character
  field name             as character
  field date             as datetime
  field month            as integer
  field day              as integer
  field year             as integer
  field userids          as character  
  field description      as character
  field UID              as character
  field createdDate      as datetime
  field personID         as character
  field reminder         as logical
  field isPrivate        as logical   /* client side field */ 
  field days             as integer   /* client side field */  
  field personName       as character /* client side field */
  field personPhone      as character /* client side field */
  field personMobile     as character /* client side field */
  field personEmail      as character /* client side field */
  field agentName        as character /* client side field */
  field agentAddr        as character /* client side field */
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 62.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


