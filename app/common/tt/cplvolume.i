&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 cpl.i
 Closing Protection Letter data structure
 D.Sinclair 5.30.2012
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias cplvolume
&ENDIF
 
&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF
 
DEF TEMP-TABLE {&tableAlias}
  FIELD agentID AS CHARACTER {&nodeType} column-label "Agent ID" format "x(10)"
  FIELD stateID AS CHARACTER {&nodeType} column-label "State" format "x(100)"
  FIELD agentName as character {&nodeType} column-label "Agent Name" format "x(200)"
  FIELD stat as character {&nodeType}
  FIELD manager as character {&nodeType}
  FIELD officeName AS CHARACTER {&nodeType}
  FIELD cplCount AS integer {&nodeType} column-label "Total LTD" format "-ZZ,ZZ9"
  FIELD month3 as integer {&nodeType} column-label "Month 3" format "-ZZ,ZZ9"
  FIELD month2 as integer {&nodeType} column-label "Month 2" format "-ZZ,ZZ9"
  FIELD month1 as integer {&nodeType} column-label "Month 1" format "-ZZ,ZZ9"
  FIELD threeMonthAverage AS decimal {&nodeType} column-label "3-Month Avg." format "-ZZZ,ZZ9"
  
  /* client */
  field statDesc as character column-label "Status" format "x(20)"
  field StateDesc as character column-label "State" format "x(50)"
  field managerDesc as character column-label "Manager" format "x(100)"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 16.67
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


