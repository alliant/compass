&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/apopen.i
   Generic datastructure for A/P invoices
   D.Sinclair 4.23.2015
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias apminvoice
&ENDIF

define temp-table {&tableAlias}

  field apinvID as integer
  field aptrxID as integer

  field stat as character  /* O)pen, P)osted, D)enied, V)oided */
  field invoiceType as character /* P)ayable or R)eceivable */
  field refType as character
  field refDescription as character 
  field refID as character
  field refCategory as character
  field vendorID as character
  field vendorName as character
  field invoiceNumber as character
  field age as integer
  field amount as decimal
  field transAmount as decimal
  
  field invoiceDate as datetime 
  field recDate as datetime    /* date received */
  field appDate as datetime    /* date approved */
  field transDate as datetime  /* date completed */
  
  field hasDocument as logical /* flag to tell if the invoice has a document in sharefile */
  field contention as logical
  
  field uid as character
  field approval as character
  .

/* Indexes:  apinvID (unique)
             vendorID (non-unique, foreign key)
             refType, refID, refSeq, refCategory

  The ref fields point to the originating/related transaction.

   Claims  refType = "C"
           refID = string(claimID) or "" for unknown
           refSeq = 0
           refCategory = L)oss, LA(E), I)ncidental, U)nallocated, blank = unknown 

   Claims Rules:
    1. Approving an apinv results in the creation of an aptrx for <= amount.  In other
       words, we can approve a partial payment. ???
    2. If refCategory = "U", the apinv cannot be approved if there is not at least 
       one contact associated with the file that has the role of "Administrator".
    3. If refCategory = "L" or "E", the apinv cannot be approved if the associated
       reserve balance on the file is less than the amount being approved.
    4. The apinv cannot be approved if the refID does not point to a valid claim.
    5. The apinv cannot be approved if the refCategory is blank or invalid.
    6. The sum of aptrx.transAmount must be <= apinv.amount.
    7. Apinv.amount cannot be negative. ???
 */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


