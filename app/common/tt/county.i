/*------------------------------------------------------------------------
    File        : county.i
    Purpose     : COUNTY datastructure
    Author(s)   : D.Sinclair
    Created     : 1.24.2012
    Notes       :
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
  &scoped-define tableAlias county
&ENDIF

&IF defined(nodeType) = 0 &THEN
  &scoped-define nodeType xml-node-type "attribute"
&ENDIF

def temp-table {&tableAlias} no-undo
  field stateID as char {&nodeType}
  field countyID as char {&nodeType}
  field description as char {&nodeType}
  field region as char {&nodeType}
  index county is primary unique
    stateID
    countyID
  .
