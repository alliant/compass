&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/agentfile.i
   Datastructure for an AGENT FILE
   @created 2.19.2015
   @author D.Sinclair
   Modifications:
   Date          Name           Description
   12/16/2019    Rahul Sharma   Modified to add fields related to agentfile
   1.12.2020     D.Sinclair     Added 5 description fields to avoid need for reference tables to lookup
   2.03.2022     SC             Added Origin, FileType, FirstCPLIssueDt and FirstPolicyIssueDt
  06.03.2024     Sachin         Task 112908- Changes in Agentfile related to Notes, Source and CPLAddr1, etc.
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias agentfile
&ENDIF

def temp-table {&tableAlias}
  /* New fields */
  field agentfileID      as integer
  field fileID           as character
  field stat             as character
  field stage            as character
  field addr1            as character
  field addr2            as character
  field addr3            as character
  field addr4            as character
  field city             as character
  field countyID         as character
  field state            as character
  field zip              as character
  field notes            as character
  field closingDate      as datetime
  field reportingDate    as datetime
  field transactionType  as character
  field insuredType      as character
  field liabilityAmt     as decimal
  field invoiceAmt       as decimal
  field paidAmt          as decimal
  field osAmt            as decimal
  field arNotes          as character /* Used when applying payment/credit */
  field agentname          as character
  /*new fields*/
  field origin             as character
  field firstCPLIssueDt    as datetime
  field firstPolicyIssueDt as datetime
  field fileType           as character
  
  
  /* common fields */
  field agentID          as character
  field fileNumber       as character
  
  /* Old fields used in OPS */ 
  field liabilityAmount  as decimal
  field grossPremium     as decimal
  field retentionPremium as decimal
  field netPremium       as decimal
  field invoiceDate      as datetime
  field effDate          as datetime
  field numPolicies      as integer

  field descStat            as character 
  field descCountyID        as character 
  field descTransactionType as character
  field descInsuredType     as character
  field descStage           as character
  
  field source    as character 
  field cplAddr1  as character 
  field cplCity   as character
  field cplState  as character
  field cplZip    as character
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


