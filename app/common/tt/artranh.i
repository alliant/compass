&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/artranh.i
Purpose     : used to fetch/store AR Transaction History  records

Syntax      :

Description : Used specifically for AR app 

Author(s)   : Anjly
Created     : 08-23-2019
Notes       : 
----------------------------------------------------------------------*/

&if defined(tableAlias) = 0 &then
&scoped-define tableAlias artranh
&endif
   
def temp-table {&tableAlias}
  field artranhID       as integer
  field revenuetype     as character
  field tranID          as character
  field seq             as integer
  field sourcetype      as character
  field type            as character
  field entityID        as character
  field sourceID        as character
  field filenumber      as character
  field fileID          as character
  field tranamt         as decimal
  field remainingamt    as decimal   
  field trandate        as datetime
  field void            as logical
  field voiddate        as datetime
  field voidby          as character
  field notes           as character 
  field createddate     as datetime
  field createdby       as character
  field duedate         as datetime
  field fullypaid       as logical
  field reference       as character
  field entity          as character
  field appliedamt      as decimal
  field transtype       as character
  field postdate        as datetime
  field postBy          as character  
  field selectrecord    as logical   /* Client side */
  field username        as character /* Client side */
  field agentname       as character /* Client side */ 
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


