&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : syslog.i
    Purpose     : SYStem LOG datastructure
    Author(s)   : D.Sinclair
    Created     : 1.24.2012
    
    Modification:
    Date          Name      Description    
    03/28/2019    Rahul     Added new fields "totalcount,applicationID,name"
  ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias syslog
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

def temp-table {&tableAlias}
 field logID as int {&nodeType}
 field createDate as datetime {&nodeType}
 field action as char {&nodeType}
 field uid as char {&nodeType}
 field addr as char {&nodeType}
 field role as char {&nodeType}
 field progExec as char {&nodeType}
 field duration as int {&nodeType}
 field isError as logical {&nodeType}
 field msg as char {&nodeType}
 field faultCode as char {&nodeType}
 field refType as char {&nodeType}
 field refNum as char {&nodeType}
 field sysMsg as char {&nodeType}
 field applicationID as char {&nodeType}
 
 field totalcount as int {&nodeType} /* client side only */
 field name as char {&nodeType}      /* client side only */
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


