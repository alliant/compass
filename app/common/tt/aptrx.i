&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/aptrx.i
   Generic datastructure for actual A/P payments (partial or complete)
   D.Sinclair 4.23.2015
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias aptrx
&ENDIF

def temp-table {&tableAlias}
 field aptrxID as int

 field transType as char    /* C)ompleted, V)oided */
 field transAmount as deci  /* Amount approved and recorded in accounting */
 field transDate as datetime

 field apinvID as int       /* The parent apinv */
 
 field refType as char
 field refID as char
 field refSeq as int
 field refCategory as char

 field report as log		/* Include this transaction in reports (i.e. Claims Ratio) - default is true */
 field uid as char
 field notes as char
 
 /* not in the database */
 field username as char
 .

/* Indexes:  aptrxID + transType (unique - max 2 records per aptrxID - see below)
             apinvID (non-unique, foreign key)
             refType, refID, refSeq, refCategory

   The ref fields are copied from apinv for easier reporting.  If the refType
   is non-blank, the refID cannot be blank.

   If an approved voucher is "voided", a new aptrx is created with the
   same aptrxID and a type = "V".  The transAmount is negative.  A voucher
   can be voided once and for the complete amount.  A void cannot be undone.
 */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


