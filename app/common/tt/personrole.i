&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
File        : tt/personrole.i
Purpose     :

Syntax      :

Description :

Author(s)   : Rahul Sharma
Created     : 11-17-2017
Notes       :

Modification:
Date          Name      Description
06/25/2018    Rahul     Added new field comstat
28/11/2019    Shubham   Added new field roleId 
20/08/2020    VJ        Added new field active
----------------------------------------------------------------------*/


&if defined(tablealias) = 0 &then
&scoped-define tableAlias PersonRole
&endif
   
define temp-table {&tableAlias}
  field personRoleID   as integer
  field stateID        as character
  field PersonID       as character
  field role           as character
  field effectivedate  as datetime
  field expirationDate as datetime
  field comStat        as character  
  field roleID         as character
  field active         as logical
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


