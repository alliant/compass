&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
    Modification:
    Date          Name       Description
    04/12/2018    Anjly      Corrected the field name.  
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias ratecard
&ENDIF
define temp-table {&tableAlias}
  field cardSetID        as integer
  field cardID           as integer
  field cardName         as character  /* Non-unique name of card for refName */
  field description      as character
  field region           as character
  field calcBy           as logical    /* false = Table; true = Reference */
                                       /* If we rely on another card to calculate */
                                       
  field refCardName      as character
  field refUseTable      as logical    /*Yes - Calculate Step1 in referenced card*/
                                       /*No - do not calculate step 1 in referenced card*/

  field refUseAttr       as logical    /*Yes - Perform step2 calculation in referenced card*/
                                       /*No - do nothing*/
  field refUseParentAttr as logical    /* Yes - Pass attribute sent from parent card to referenced card*/
                                       /* No � Pass attribute stored in refAttrName field to referenced card*/
  field refAttrName      as character  /*If refBLAttr if No, then pass this attribute to referenced card for step 2 calculation.*/
  
  field refUseRules       as logical    /*Yes - Calculate Step3 in referenced card*/
                                       /*No - do not calculate step 3 in referenced card*/  
  
  field comments         as character
  field lastModifiedDate as datetime
  field lastModifiedBy   as character.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 62.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


