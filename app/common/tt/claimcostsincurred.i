&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/claimcostsincurred.i
   datastructure for a CLAIM COSTS INCURRED report
   Created John Oliver
   Date 02.01.2017
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias claimcostsincurred
&ENDIF

/* Should recoupments be against the Loss or LAE in accounting terms?
 */

def temp-table {&tableAlias}
  field claimID as integer label "Claim ID" format "99999999"
  field assignedTo as character serialize-hidden
  field yearClosed as integer label "Year Closed" format "9999"
  field claimStatus as character label "Claim Status Code" format "x(50)"
  field residential as logical label "Residential?"
  field altaResponsibility as character label "Alta Resp." format "x(1000)"
  field altaRisk as character label "Alta Risk" format "x(1000)"
  field category as character label "Claim Description" format "x(1000)"
  field cause as character label "Claim Cause" format "x(1000)"
  field policyID as character label "Policy" format "x(100)"
  field policyFormID as character label "Policy Form" format "x(100)"
  field policyEffectiveDate as datetime label "Eff. Date" format "99/99/99"
  field policyLiability as decimal label "Liability" format "(>>>,>>>,>>Z)"
  field agentID as character label "Agent"
  field agentName as character label "Agent Name" format "x(1000)"
  field agentStatus as character label "Agent Status Code" format "x(50)"
  field agentCancelDate as datetime column-label "Agent Cancel!Date"
  field stateID as character label "State" format "x(100)"
  field laePosted as decimal label "LAE Posted" format "(>>>,>>>,>>Z)"
  field lossPosted as decimal label "Loss Posted" format "(>>>,>>>,>>Z)"
  field recoveries as decimal label "Recoveries" format "(>>>,>>>,>>Z)"
  field laeBalance as decimal column-label "Change in!LAE Reserve" format "(>>>,>>>,>>Z)"
  field lossBalance as decimal column-label "Change in!Loss Reserve" format "(>>>,>>>,>>Z)"
  field costsIncurred as decimal label "Costs!Incurred" format "(>>>,>>>,>>Z)"
  field laeReserve as decimal label "LAE Balance" format "(>>>,>>>,>>Z)"
  field lossReserve as decimal label "Loss Balance" format "(>>>,>>>,>>Z)"
  field laeReserveWithOpenPayments as decimal column-label "LAE Balance Less!Open Payments" format "(>>>,>>>,>>Z)"
  field lossReserveWithOpenPayments as decimal column-label "Loss Balance Less!Open Payments" format "(>>>,>>>,>>Z)"
  
  /* client only */
  field assignedToName as character label "Assigned To" format "x(200)"
  field claimStatusDesc as character label "Claim Status" format "x(50)"
  field agentStatusDesc as character label "Agent Status" format "x(50)"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


