&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 batch.i
 BATCH data structure
 D.Sinclair 5.30.2012
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias batch
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF
 
define temp-table {&tableAlias}
  field batchID                  as integer   {&nodeType} column-label "Batch ID"         format " >>>>>>>>>9  "
  field periodID                 as integer   {&nodeType} column-label "Period ID"        format "999999"
  field yearID                   as integer   {&nodeType} column-label "Year ID"          format "9999"
  field agentID                  as character {&nodeType} column-label "Agent ID"         format "x(20)"
  field state                    as character {&nodeType} serialize-hidden
  field stateID                  as character {&nodeType} column-label "State ID"         format "x(50)"
  field stateDesc                as character {&nodeType} column-label "State"            format "x(50)"
  field stat                     as character {&nodeType} column-label "Stat"             format "x(6)"
  field statDesc                 as character {&nodeType} column-label "Status"           format "x(20)"
  field createDate               as datetime  {&nodeType} column-label "Create!Date"      format "99/99/9999"
  field receivedDate             as datetime  {&nodeType} column-label "Received!Date"    format "99/99/9999"                              
                                                                                          
  field startDate                as datetime  {&nodeType} column-label "Start Date"       format "99/99/9999"
  field duration                 as integer   {&nodeType} column-label "Duration"         format ">,>>9"
  field endDate                  as datetime  {&nodeType} column-label "End Date"         format "99/99/9999"
  field acctDate                 as datetime  {&nodeType} column-label "Acct Date"        format "99/99/9999"
                                 
  field liabilityAmount          as decimal   {&nodeType} column-label "Liability!Amount" format "$->>,>>>,>>9.99"
  field liabilityDelta           as decimal   {&nodeType} column-label "Liability!Delta"  format "$->>,>>>,>>9.99"
                                 
  field grossPremiumReported     as decimal   {&nodeType} column-label "Reported!Gross"   format "$->>,>>>,>>9.99"
  field grossPremiumProcessed    as decimal   {&nodeType} serialize-hidden
  field grossPremiumDiff         as decimal   {&nodeType} serialize-hidden
  field grossPremiumDelta        as decimal   {&nodeType} column-label "Gross!Delta"      format "$->>,>>>,>>9.99"
                                 
  field netPremiumReported       as decimal   {&nodeType} column-label "Reported!Net"     format "$->>,>>>,>>9.99"
  field netPremiumProcessed      as decimal   {&nodeType} serialize-hidden
  field netPremiumDiff           as decimal   {&nodeType} serialize-hidden
  field netPremiumDelta          as decimal   {&nodeType} column-label "Net!Delta"        format "$->>,>>>,>>9.99"
  
  field retainedPremiumProcessed as decimal   {&nodeType} serialize-hidden
  field retainedPremiumDelta     as decimal   {&nodeType} column-label "Retained!Delta"   format "$->>,>>>,>>9.99"
  
  field fileCount                as integer   {&nodeType} column-label "File!Count"       format ">,>>9"
  field policyCount              as integer   {&nodeType} column-label "Policy!Count"     format ">,>>9"
  field endorsementCount         as integer   {&nodeType} column-label "Endorse!Count"    format ">,>>9"
  field cplCount                 as integer   {&nodeType} column-label "CPL!Count"        format ">,>>9"
  
  field periodMonth              as integer   {&nodeType}
  field periodYear               as integer   {&nodeType}
  
  field agentStat                as character {&nodeType} serialize-hidden
  field agentStatDesc            as character {&nodeType} column-label "Agent!Status"     format "x(30)"
  field agentName                as character {&nodeType} column-label "Agent Name"       format "x(200)"
  field agentManager             as character {&nodeType} serialize-hidden
  field reference                as character {&nodeType} column-label "Reference"        format "x(40)"
                                                                                          
  field cashReceived             as decimal   {&nodeType} column-label "Cash!Received"    format "$->>,>>>,>>9.99"
                                                                                          
  field rcvdVia                  as character {&nodeType} column-label "Received Via"     format "x(100)"
                                                                                          
  field processStat              as character {&nodeType} serialize-hidden
  field processStatDesc          as character {&nodeType} column-label "Process!Status"   format "x(100)"      
  field invoiceDate              as datetime  {&nodeType} column-label "Invoice!Date"     format "99/99/9999"
                                                                                          
  field lockedBy                 as character {&nodeType} column-label "Locked By"        format "x(100)"
  field lockedDate               as datetime  {&nodeType} column-label "Locked Date"      format "99/99/9999"
  
  field hasDocument              as logical   {&nodeType} column-label "Has!Doc"
  index batchID is primary unique batchID
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 16.67
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


