/*------------------------------------------------------------------------
File        : tt/agentfilter.i
Purpose     :

Syntax      :

Description :

Author(s)   : Rahul Sharma
Created     : 10-06-2020
Notes       : Used for filtering agents

Modification:
Date          Name      Description

----------------------------------------------------------------------*/

&if defined(tablealias) = 0 &then
&scoped-define tableAlias agentfilter
&endif
   
define temp-table {&tableAlias}
  field agentID  as character
  .


