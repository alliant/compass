&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/ops18.i
   Generic datastructure for OPS18 report (Agent Batch Year Volume)
   D.Sinclair 6.19.2015
 */


&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias ops18
&ENDIF

def temp-table {&tableAlias}
 field agentID as char
 field name as char
 field stateID as char
 field periodYear as int
 field stat as char
 field statDesc as char
 field manager as char
 field managerDesc as char
 
 field janBatches as int
 field janFiles as int
 field janPolicies as int
 field janGross as dec
 field janNet as dec
 field janRetain as dec
 field janLiab as dec
 field janGap as dec

 field febBatches as int
 field febFiles as int
 field febPolicies as int
 field febGross as dec
 field febNet as dec
 field febRetain as dec
 field febLiab as dec
 field febGap as dec

 field marBatches as int
 field marFiles as int
 field marPolicies as int
 field marGross as dec
 field marNet as dec
 field marRetain as dec
 field marLiab as dec
 field marGap as dec

 field aprBatches as int
 field aprFiles as int
 field aprPolicies as int
 field aprGross as dec
 field aprNet as dec
 field aprRetain as dec
 field aprLiab as dec
 field aprGap as dec

 field mayBatches as int
 field mayFiles as int
 field mayPolicies as int
 field mayGross as dec
 field mayNet as dec
 field mayRetain as dec
 field mayLiab as dec
 field mayGap as dec

 field junBatches as int
 field junFiles as int
 field junPolicies as int
 field junGross as dec
 field junNet as dec
 field junRetain as dec
 field junLiab as dec
 field junGap as dec

 field julBatches as int
 field julFiles as int
 field julPolicies as int
 field julGross as dec
 field julNet as dec
 field julRetain as dec
 field julLiab as dec
 field julGap as dec

 field augBatches as int
 field augFiles as int
 field augPolicies as int
 field augGross as dec
 field augNet as dec
 field augRetain as dec
 field augLiab as dec
 field augGap as dec

 field sepBatches as int
 field sepFiles as int
 field sepPolicies as int
 field sepGross as dec
 field sepNet as dec
 field sepRetain as dec
 field sepLiab as dec
 field sepGap as dec

 field octBatches as int
 field octFiles as int
 field octPolicies as int
 field octGross as dec
 field octNet as dec
 field octRetain as dec
 field octLiab as dec
 field octGap as dec

 field novBatches as int
 field novFiles as int
 field novPolicies as int
 field novGross as dec
 field novNet as dec
 field novRetain as dec
 field novLiab as dec
 field novGap as dec

 field decBatches as int
 field decFiles as int
 field decPolicies as int
 field decGross as dec
 field decNet as dec
 field decRetain as dec
 field decLiab as dec
 field decGap as dec

 index pu1 is primary unique agentID periodYear
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


