&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* tt/iclreport.i
   MONTHLY ICL report dataset
   Author(s) John Oliver
   Created 11/6/2018
 */

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias reporticl
&ENDIF

&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF

define temp-table {&tableAlias}
  field agentID as character {&nodeType} column-label "Agent" format "x(20)"
  field agentName as character {&nodeType} column-label "Agent Name" format "x(200)"
  field stateID as character {&nodeType} column-label "State" format "x(30)"
  field stateName as character {&nodeType} serialize-hidden
  field stat as character {&nodeType} serialize-hidden
  field statDesc as character {&nodeType} column-label "Agent Status" format "x(50)"
  field manager as character {&nodeType} serialize-hidden
  field managerDesc as character {&nodeType} column-label "Manager" format "x(100)"
  field regionID as character {&nodeType} serialize-hidden
  field regionDesc as character {&nodeType} label "Region" format "x(100)"
  field category as character {&nodeType} serialize-hidden
  field year as integer {&nodeType} serialize-hidden
  field prevMonth as decimal serialize-hidden
  field month1 as decimal {&nodeType} column-label "Jan" format "(ZZZ,ZZZ,ZZZ)"
  field month2 as decimal {&nodeType} column-label "Feb" format "(ZZZ,ZZZ,ZZZ)"
  field month3 as decimal {&nodeType} column-label "Mar" format "(ZZZ,ZZZ,ZZZ)"
  field month4 as decimal {&nodeType} column-label "Apr" format "(ZZZ,ZZZ,ZZZ)"
  field month5 as decimal {&nodeType} column-label "May" format "(ZZZ,ZZZ,ZZZ)"
  field month6 as decimal {&nodeType} column-label "Jun" format "(ZZZ,ZZZ,ZZZ)"
  field month7 as decimal {&nodeType} column-label "Jul" format "(ZZZ,ZZZ,ZZZ)"
  field month8 as decimal {&nodeType} column-label "Aug" format "(ZZZ,ZZZ,ZZZ)"
  field month9 as decimal {&nodeType} column-label "Sep" format "(ZZZ,ZZZ,ZZZ)"
  field month10 as decimal {&nodeType} column-label "Oct" format "(ZZZ,ZZZ,ZZZ)"
  field month11 as decimal {&nodeType} column-label "Nov" format "(ZZZ,ZZZ,ZZZ)"
  field month12 as decimal {&nodeType} column-label "Dec" format "(ZZZ,ZZZ,ZZZ)"
  field qtr1 as decimal {&nodeType} column-label "Qtr 1 total" format "(ZZZ,ZZZ,ZZZ)" serialize-hidden
  field qtr2 as decimal {&nodeType} column-label "Qtr 2 Total" format "(ZZZ,ZZZ,ZZZ)" serialize-hidden
  field qtr3 as decimal {&nodeType} column-label "Qtr 3 Total" format "(ZZZ,ZZZ,ZZZ)" serialize-hidden
  field qtr4 as decimal {&nodeType} column-label "Qtr 4 Total" format "(ZZZ,ZZZ,ZZZ)" serialize-hidden
  field yrTotal as decimal {&nodeType} column-label "Year Total" format "(ZZZ,ZZZ,ZZZ,ZZZ)"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

