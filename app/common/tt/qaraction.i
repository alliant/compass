&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : qaraction.i
    Purpose     : quality assurance ACTION datastructure
    Author(s)   : D.Sinclair
    Created     : 1.24.2012
    Modification:
    Date          Name      Description
    01/04/2017    AC        New field added to save Action  
    08/21/2017    AG        Added comments field to remove 
                            compilation error in qarnew.p                
  ----------------------------------------------------------------------*/
  
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias action
&ENDIF

def temp-table {&tableAlias}
 field qarID as int
 field actionID as int
 field priority as int
 field actionType as char
 field description as char
 field note as char
 field comments as char
 field questionSeq as int
 field questionID as char
 field questionDesc as char
 field finding as char
 field files as char
 field reference as char
 field dueDate as datetime
 field followupDate as datetime
 field stat as char
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


