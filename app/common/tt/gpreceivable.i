&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*
 gpreceivable.i
 Temp table for the ALERT RECEIVABLE report
 J.Oliver 10.2017
 */
 
&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias gpreceivable
&ENDIF
 
&IF defined(nodeType) = 0 &THEN
&scoped-define nodeType xml-node-type "attribute"
&ENDIF
 
define temp-table {&tableAlias}
  FIELD addAlert as logical {&nodeType}
  FIELD agentID AS CHARACTER {&nodeType}
  FIELD agentName as character {&nodeType}
  FIELD stateID AS CHARACTER {&nodeType}
  FIELD stat as character {&nodeType}
  FIELD manager as character {&nodeType}
  FIELD daysCurrent as decimal {&nodeType}
  FIELD days31to60 as decimal {&nodeType}
  FIELD days61to90 as decimal {&nodeType}
  FIELD daysOver90 as decimal {&nodeType}
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 16.67
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


