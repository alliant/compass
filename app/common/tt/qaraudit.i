&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/* audit.i
   D.Sinclair 10.20.2015 Added qarID
   Modification:
   Date          Name      Description
   08/02/2017    AC        New fields schedStartDate and schedFinishDate added
   10/04/2017    AC        New fields Audittype and Errtype added
   13/11/2017    RS        Added reason code for QUR audits.
   07/15/2021    SA        Task 83510 add new fields grade
   03/17/2023    K.R       Task 103305 Added new fields priorAuditDate,priorAuditScore,priorAuditType and cityState
   ----------------------------------------------------------------------*/

&IF defined(tableAlias) = 0 &THEN
&scoped-define tableAlias audit
&ENDIF
   
def temp-table {&tableAlias}
  field qarID as int
  field version as char
  field auditYear as int
  field agentID as char
  field name as char /* comes from agent table */
  field addr as char /* comes from agent table */
  field city as char /* comes from agent table */
  field state as char /* comes from agent table */
  field zip as char /* comes from agent table */
  field auditStartDate as datetime
  field auditFinishDate as datetime
  field schedStartDate as datetime
  field schedFinishDate as datetime
  field onsiteStartDate as datetime
  field onsiteFinishDate as datetime
  field auditor as char
  field auditorUsername as char
  field score as int
  field uid as char
  field contactName as char
  field contactPhone as char
  field contactFax as char
  field contactEmail as char
  field contactOther as char
  field deliveredTo as char
  field comments as char
  field stateID as char
  field stat as char
  field auditType as char
  field errType as char
  field draftReportDate as datetime
  field grade as int
  field priorAuditDate as datetime
  field priorAuditScore as int
  field priorAuditType as char
 
  /* server fields reflecting original posting */
  field submittedBy as char
  field submittedAt as datetime
  
  /* not in database */
  field auditScore as int
  field auditDate as datetime
  field parentName as char
  field parentAddr as char
  field parentCity as char
  field parentState as char
  field parentZip as char
  field contactPosition as char
  field policyType as char /* P)aper, E)Jacket, B)oth */
  field mainOffice as logical
  field numOffices as int
  field numEmployees as int
  field numUnderwriters as int
  field lastRevenue as decimal
  field ranking as int
  field services as char
  field escrowReviewer as char
  field escrowReviewDate as datetime
  field auditGap as character
  field startGap as character
  field ERRscore as int
  field cQarID as character
  field cityState as character
  
  /* description */
  field statDesc as character
  field auditTypeDesc as character
  field errTypeDesc as character
  field username as character
  field reasonCode as character
  
  
  /* used for the scheduling screen */
  field schedNewAudit as logical
  field schedReassign as logical
  field schedReschedule as logical
  field schedAuditType as logical
  
  field createDate as datetime
  field finalReportDate as datetime
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


