&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/* common/dialogmodifyfilenumber.w
   @date 1.12.2020
   @author D.Sinclair
   @purpose Simple UI to allow user to change a file number.
*/

 define input parameter ipcType      as character no-undo. /* P - Policy , F - File */
 define input parameter ipcPolicyID  as character no-undo.
 define input parameter ipcOldFileID as character no-undo.
 define input parameter ipcNewFileID as character no-undo.
 def output parameter pSection as char.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bDoNothing bRefresh bClose 
&Scoped-Define DISPLAYED-OBJECTS fInfo fInfo-2 fInfo-3 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bClose AUTO-GO 
     LABEL "Close Windows" 
     SIZE 20 BY 1.14 TOOLTIP "Unsaved changes will be lost".

DEFINE BUTTON bDoNothing AUTO-GO 
     LABEL "Do Nothing" 
     SIZE 20 BY 1.14.

DEFINE BUTTON bRefresh AUTO-GO 
     LABEL "Refresh Windows" 
     SIZE 20 BY 1.14 TOOLTIP "Refresing the screens might take some time".

DEFINE VARIABLE fInfo AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 100 BY 1
     FONT 6 NO-UNDO.

DEFINE VARIABLE fInfo-2 AS CHARACTER FORMAT "X(256)":U INITIAL "The information displayed on some of the screens is now outdated. Which action do you want to perform" 
      VIEW-AS TEXT 
     SIZE 100.6 BY 1
     FONT 4 NO-UNDO.

DEFINE VARIABLE fInfo-3 AS CHARACTER FORMAT "X(256)":U INITIAL "on the screens with outdated information?" 
      VIEW-AS TEXT 
     SIZE 92 BY 1
     FONT 4 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bDoNothing AT ROW 5.19 COL 25.2 WIDGET-ID 18
     bRefresh AT ROW 5.19 COL 46.2 WIDGET-ID 8
     bClose AT ROW 5.19 COL 67.2 WIDGET-ID 10
     fInfo AT ROW 1.29 COL 4.4 COLON-ALIGNED NO-LABEL WIDGET-ID 20
     fInfo-2 AT ROW 2.81 COL 4.4 COLON-ALIGNED NO-LABEL WIDGET-ID 22
     fInfo-3 AT ROW 3.76 COL 4.4 COLON-ALIGNED NO-LABEL WIDGET-ID 24
     SPACE(12.39) SKIP(2.09)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 1
         TITLE "File Number Modified"
         DEFAULT-BUTTON bDoNothing WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN fInfo IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fInfo:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fInfo-2 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fInfo-2:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fInfo-3 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fInfo-3:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* File Number Modified */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClose
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClose Dialog-Frame
ON CHOOSE OF bClose IN FRAME Dialog-Frame /* Close Windows */
DO:
  pSection = "2".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDoNothing
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDoNothing Dialog-Frame
ON CHOOSE OF bDoNothing IN FRAME Dialog-Frame /* Do Nothing */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh Dialog-Frame
ON CHOOSE OF bRefresh IN FRAME Dialog-Frame /* Refresh Windows */
DO:
  pSection = "1".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  if ipcType = "P"
   then
    do:
      Frame Dialog-Frame:title = "Move Policy".
      fInfo:screen-value = "Policy '" + ipcPolicyID + "'  is successfully moved from file '" + ipcOldFileID + "' to '" + ipcNewFileID + "'.".
    end.
   else
    do:
      Frame Dialog-Frame:title = "Rename File".
      fInfo:screen-value = "File '" + ipcOldFileID + "' is successfully renamed to file '" + ipcNewFileID + "'.".
    end.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fInfo fInfo-2 fInfo-3 
      WITH FRAME Dialog-Frame.
  ENABLE bDoNothing bRefresh bClose 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

