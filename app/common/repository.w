&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
@file repository.w
@description A screen to get the ARC repository files
@modified - changed the window title to 'Document Repository'
------------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input parameter pEntity   as character no-undo.
define input parameter pEntityID as character no-undo.

/* Local Variable Definitions ---                                       */
&scoped-define DefaultCategory "Default"
{lib/std-def.i}
{lib/set-button-def.i}
{lib/brw-multi-def.i}

/* to keep track of the column widths */
define variable dBrowseDocument as decimal no-undo.

/* Temp Table ---                                                       */
{tt/repository.i &tableAlias="orderfile"}
{tt/repository.i &tableAlias="temporderfile"}
{tt/repository.i &tableAlias="temporderfile2"}
{tt/repository.i &tableAlias="orderfolder"}
{tt/repository.i &tableAlias="temporderfolder"}

/* Functions and Parameters ---                                         */
{lib/add-delimiter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwFiles

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES orderfile orderfolder

/* Definitions for BROWSE brwFiles                                      */
&Scoped-define FIELDS-IN-QUERY-brwFiles orderfile.displayName orderfile.createdDate orderfile.fileTypeDesc orderfile.fileSizeDesc orderfile.isPrivate orderfile.createdByDesc   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFiles   
&Scoped-define SELF-NAME brwFiles
&Scoped-define QUERY-STRING-brwFiles FOR EACH orderfile by orderfile.displayName
&Scoped-define OPEN-QUERY-brwFiles OPEN QUERY {&SELF-NAME} FOR EACH orderfile by orderfile.displayName.
&Scoped-define TABLES-IN-QUERY-brwFiles orderfile
&Scoped-define FIRST-TABLE-IN-QUERY-brwFiles orderfile


/* Definitions for BROWSE brwFolders                                    */
&Scoped-define FIELDS-IN-QUERY-brwFolders orderfolder.displayName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFolders   
&Scoped-define SELF-NAME brwFolders
&Scoped-define QUERY-STRING-brwFolders FOR EACH orderfolder by orderfolder.category
&Scoped-define OPEN-QUERY-brwFolders OPEN QUERY {&SELF-NAME} FOR EACH orderfolder by orderfolder.category.
&Scoped-define TABLES-IN-QUERY-brwFolders orderfolder
&Scoped-define FIRST-TABLE-IN-QUERY-brwFolders orderfolder


/* Definitions for FRAME DEFAULT-FRAME                                  */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bFolderRefresh bFolderDelete bFolderRename ~
bFileRefresh bFileCreate bFileUpload bFileDownload bFileModify bFileDelete ~
brwFolders brwFiles bFileLink bFileEmail tURL 
&Scoped-Define DISPLAYED-OBJECTS tURL 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD checkDisplayNameModify C-Win 
FUNCTION checkDisplayNameModify RETURNS LOGICAL
  ( input pDisplayName as character,
    input pCategory as character,
    input pID as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD checkDisplayNameNew C-Win 
FUNCTION checkDisplayNameNew RETURNS LOGICAL
  ( input pDisplayName as character,
    input pCategory as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doModifyDocuments C-Win 
FUNCTION doModifyDocuments RETURNS LOGICAL
  ( input pEnable as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setupDocuments C-Win 
FUNCTION setupDocuments RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bFileCreate 
     LABEL "Create" 
     SIZE 7.2 BY 1.71 TOOLTIP "Create a Linked Document from a URL".

DEFINE BUTTON bFileDelete 
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete the file".

DEFINE BUTTON bFileDownload 
     LABEL "Download" 
     SIZE 7.2 BY 1.71 TOOLTIP "Download the file".

DEFINE BUTTON bFileEmail 
     LABEL "Email" 
     SIZE 4.8 BY 1.14 TOOLTIP "Email the link".

DEFINE BUTTON bFileLink 
     LABEL "Link" 
     SIZE 4.8 BY 1.14 TOOLTIP "Copy to Clipboard".

DEFINE BUTTON bFileModify 
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify the file".

DEFINE BUTTON bFileRefresh 
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh the files".

DEFINE BUTTON bFileUpload 
     LABEL "Upload" 
     SIZE 7.2 BY 1.71 TOOLTIP "Upload a file".

DEFINE BUTTON bFolderDelete 
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete the category".

DEFINE BUTTON bFolderRefresh 
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh the categories".

DEFINE BUTTON bFolderRename 
     LABEL "Rename" 
     SIZE 7.2 BY 1.71 TOOLTIP "Rename the category".

DEFINE VARIABLE tURL AS CHARACTER FORMAT "X(256)":U 
     LABEL "URL" 
     VIEW-AS FILL-IN 
     SIZE 106.4 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwFiles FOR 
      orderfile SCROLLING.

DEFINE QUERY brwFolders FOR 
      orderfolder SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwFiles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFiles C-Win _FREEFORM
  QUERY brwFiles DISPLAY
      orderfile.displayName column-label "File Name" width 40
orderfile.createdDate width 25
orderfile.fileTypeDesc width 12
orderfile.fileSizeDesc width 10
orderfile.isPrivate view-as toggle-box
orderfile.createdByDesc format "x(20)" label "Saved By" width 20
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET SIZE 122 BY 12.86
         TITLE "Files" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwFolders
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFolders C-Win _FREEFORM
  QUERY brwFolders DISPLAY
      orderfolder.displayName column-label ""
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET SIZE 50 BY 8.33
         TITLE "Categories" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bFolderRefresh AT ROW 1.24 COL 2 WIDGET-ID 6
     bFolderDelete AT ROW 1.24 COL 9.4 WIDGET-ID 8
     bFolderRename AT ROW 1.24 COL 16.8 WIDGET-ID 24
     bFileRefresh AT ROW 1.24 COL 53 WIDGET-ID 20
     bFileCreate AT ROW 1.24 COL 60.4 WIDGET-ID 28
     bFileUpload AT ROW 1.24 COL 67.8 WIDGET-ID 14
     bFileDownload AT ROW 1.24 COL 75.2 WIDGET-ID 16
     bFileModify AT ROW 1.24 COL 82.6 WIDGET-ID 22
     bFileDelete AT ROW 1.24 COL 90 WIDGET-ID 18
     brwFolders AT ROW 3.14 COL 2 WIDGET-ID 1500
     brwFiles AT ROW 3.14 COL 53 WIDGET-ID 1600
     bFileLink AT ROW 16.48 COL 165.4 WIDGET-ID 32
     bFileEmail AT ROW 16.48 COL 170.4 WIDGET-ID 34
     tURL AT ROW 16.57 COL 56.6 COLON-ALIGNED WIDGET-ID 30
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 175 BY 17.57 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Document Repository"
         HEIGHT             = 17.72
         WIDTH              = 175
         MAX-HEIGHT         = 24.1
         MAX-WIDTH          = 221.8
         VIRTUAL-HEIGHT     = 24.1
         VIRTUAL-WIDTH      = 221.8
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwFolders bFileDelete DEFAULT-FRAME */
/* BROWSE-TAB brwFiles brwFolders DEFAULT-FRAME */
ASSIGN 
       brwFiles:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwFiles:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwFiles:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

ASSIGN 
       brwFolders:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwFolders:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwFolders:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

ASSIGN 
       tURL:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFiles
/* Query rebuild information for BROWSE brwFiles
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH orderfile by orderfile.displayName.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwFiles */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFolders
/* Query rebuild information for BROWSE brwFolders
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH orderfolder by orderfolder.category.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwFolders */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Document Repository */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Document Repository */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Document Repository */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileCreate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileCreate C-Win
ON CHOOSE OF bFileCreate IN FRAME DEFAULT-FRAME /* Create */
DO:
  define variable pFileName as character no-undo.
  define variable pCategory as character no-undo.
  define variable pPrivate  as logical   no-undo.
  define variable pSave     as logical   no-undo initial false.
  define variable tUserID   as character no-undo.
  publish "GetCredentialsID" (output tUserID).
    
  run dialogrepositoryattach.w (table orderfile, output pFileName, output pCategory, output pPrivate, output pSave).
  if pSave
   then
    do with frame fMain:      
      empty temp-table temporderfile.
      empty temp-table temporderfile2.
      create temporderfile.
      assign
        temporderfile.identifier  = ""
        temporderfile.entity      = pEntity
        temporderfile.entityID    = pEntityID
        temporderfile.createdBy   = tUserID
        temporderfile.name        = entry(num-entries(pFileName, "/"), pFileName, "/")
        temporderfile.folderPath  = pFileName
        temporderfile.category    = pCategory
        temporderfile.isPrivate   = pPrivate 
        .
      run server/savearcrepositoryfile.p (table temporderfile, output table temporderfile2, output std-lo, output std-ch).
      if not std-lo
       then message std-ch view-as alert-box error buttons ok.
       else apply "CHOOSE" to bFolderRefresh.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileDelete C-Win
ON CHOOSE OF bFileDelete IN FRAME DEFAULT-FRAME /* Delete */
DO:
  if not available orderfile
   then return.
   
  std-lo = true.
  publish "GetConfirmDelete" (output std-lo).
  if std-lo
   then
    do:
      std-lo = false.
      message "File '" + orderfile.displayName + "' will be permanently removed. Continue?" view-as alert-box question buttons yes-no update std-lo.
      if not std-lo 
       then return.
    end.
    
  run server/deletearcrepositoryfile.p (orderfile.identifier, output std-lo, output std-ch).
  if not std-lo
   then message std-ch view-as alert-box error buttons ok.
   else apply "CHOOSE" to bFolderRefresh.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileDownload
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileDownload C-Win
ON CHOOSE OF bFileDownload IN FRAME DEFAULT-FRAME /* Download */
DO:
  if not available orderfile
   then return.
   
  if orderfile.filesize = 0
   then os-command silent value("start " + orderfile.link).
   else
    do:
      define variable tFile as character no-undo.
      tFile = orderfile.name.
      run server/downloadarcrepositoryfile.p (orderfile.identifier, input-output tFile, output std-lo, output std-ch).
      if not std-lo
       then message std-ch view-as alert-box error buttons ok.
       else run util/openfile.p (tFile).
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileEmail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileEmail C-Win
ON CHOOSE OF bFileEmail IN FRAME DEFAULT-FRAME /* Email */
DO:
  if tURL:screen-value = ""
   then return.
   
  if not available orderfile
   then return.
   
  run util/openURL.p ("mailto:?subject=Link%20to%20the%20file%20" + orderfile.name + "&body=" + tURL:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileLink
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileLink C-Win
ON CHOOSE OF bFileLink IN FRAME DEFAULT-FRAME /* Link */
DO:
  if tURL:screen-value = ""
   then return.
   
  clipboard:value = tURL:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileModify C-Win
ON CHOOSE OF bFileModify IN FRAME DEFAULT-FRAME /* Modify */
DO:
  if not available orderfile and not (orderfile.filesize = 0)
   then return.

  define variable pFileName as character no-undo.
  define variable pCategory as character no-undo.
  define variable pPrivate  as logical   no-undo.
  define variable pID       as character no-undo.
  define variable pSave     as logical   no-undo initial false.
  assign
    pID       = orderfile.identifier
    pFileName = orderfile.displayName
    pCategory = orderfile.category
    pPrivate  = orderfile.isPrivate
    .
    
  run dialogrepositorymodify.w (table orderfile, input pID, input-output pFileName, input-output pCategory, input-output pPrivate, output pSave).
  if pSave
   then
    do:
      /* we need to rename all the category for the files */
      empty temp-table temporderfile.
      empty temp-table temporderfile2.
      buffer-copy orderfile to temporderfile.
      assign
        temporderfile.displayName = pFileName
        temporderfile.category    = pCategory
        temporderfile.isPrivate   = pPrivate 
        .
      run server/modifyarcrepositoryfile.p (table temporderfile, output table temporderfile2, output std-lo, output std-ch).
      if not std-lo
       then message std-ch view-as alert-box error buttons ok.
       else apply "CHOOSE" to bFolderRefresh.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileRefresh C-Win
ON CHOOSE OF bFileRefresh IN FRAME DEFAULT-FRAME /* Refresh */
DO:
  apply "CHOOSE" to bFolderRefresh.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileUpload
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileUpload C-Win
ON CHOOSE OF bFileUpload IN FRAME DEFAULT-FRAME /* Upload */
DO:   
  define variable pFile     as character no-undo.
  define variable pCategory as character no-undo.
  define variable pPrivate  as logical   no-undo.
  define variable pSave     as logical   no-undo initial false.

  run dialogrepositoryupload.w (table orderfile, output pFile, output pCategory, output pPrivate, output pSave).
  if not pSave
   then return.

  run uploadRepositoryFile in this-procedure (pPrivate, pCategory, pFile, output std-lo).
  if std-lo
   then apply "CHOOSE" to bFolderRefresh.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFolderDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFolderDelete C-Win
ON CHOOSE OF bFolderDelete IN FRAME DEFAULT-FRAME /* Delete */
DO:
  if not available orderfolder
   then return.
   
  define variable tIDList as character no-undo.
   
  std-lo = true.
  publish "GetConfirmDelete" (output std-lo).
  if std-lo
   then
    do:
      std-lo = false.
      message "Category '" + orderfolder.displayName + "' and its contents will be permanently removed. Continue?" view-as alert-box question buttons yes-no update std-lo.
      if not std-lo 
       then return.
    end.
    
  /* get the list of ids */
  for each orderfile exclusive-lock
     where orderfile.category = orderfolder.category:
     
    tIDList = addDelimiter(tIDList, ",") + orderfile.identifier.
  end.
  
  run server/deletearcrepositoryfile.p (tIDList, output std-lo, output std-ch).
  if not std-lo
   then message std-ch view-as alert-box error buttons ok.
   else
    do:
      for each orderfile exclusive-lock
         where orderfile.category = orderfolder.category:
         
        delete orderfile.
      end.
      delete orderfolder.
      setupDocuments().
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFolderRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFolderRefresh C-Win
ON CHOOSE OF bFolderRefresh IN FRAME DEFAULT-FRAME /* Refresh */
DO:
  empty temp-table orderfile.
  run server/getarcrepositoryfiles.p (pEntity, pEntityId, false, output table orderfile, output std-lo, output std-ch).
  if not std-lo
   then
    do:
      doModifyDocuments(false).
      message std-ch view-as alert-box error buttons ok.
    end.
   else setupDocuments().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFolderRename
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFolderRename C-Win
ON CHOOSE OF bFolderRename IN FRAME DEFAULT-FRAME /* Rename */
DO:
  if not available orderfolder
   then return.
   
  define variable pCategory as character no-undo.
  define variable pSave     as logical   no-undo.
  pCategory = orderfolder.category.
  run dialogrepositoryrenamecategory.w (input-output pCategory, output pSave).
  if pSave
   then
    do:
      /* we need to rename all the category for the files */
      empty temp-table temporderfile.
      for each orderfile exclusive-lock
         where orderfile.category = orderfolder.category:
         
        if checkDisplayNameModify(orderfile.displayName, pCategory, orderfile.identifier)
         then 
          do:
            create temporderfile.
            buffer-copy orderfile to temporderfile.
            temporderfile.category = pCategory.
          end.
      end.
      if can-find(first temporderfile)
       then
        do:
          run server/modifyarcrepositoryfile.p (table temporderfile, output table temporderfile2, output std-lo, output std-ch).
          if not std-lo
           then message std-ch view-as alert-box error buttons ok.
           else apply "CHOOSE" to bFolderRefresh.
        end.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFiles
&Scoped-define SELF-NAME brwFiles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFiles C-Win
ON DEFAULT-ACTION OF brwFiles IN FRAME DEFAULT-FRAME /* Files */
DO:
  apply "CHOOSE" to bFileDownload.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFiles C-Win
ON DROP-FILE-NOTIFY OF brwFiles IN FRAME DEFAULT-FRAME /* Files */
DO:
  define variable tPrivate  as logical   no-undo.
  define variable tFileName as character no-undo.
  define variable tPercent  as integer   no-undo.
  define variable tCategory as character no-undo.
  
  if not available orderfolder
   then tCategory = {&DefaultCategory}.
   else tCategory = orderfolder.category.
   
  if self:num-dropped-files = 1
   then
    do:
      tFileName = self:get-dropped-file(1).
      tFileName = entry(num-entries(tFileName, "\"), tFileName, "\").
      message "Should the file " + tFileName + " be marked as private?" view-as alert-box question buttons yes-no-cancel update tPrivate.
    end.
   else message "Should all the files be marked as private?" view-as alert-box question buttons yes-no-cancel update tPrivate.
    
  if tPrivate = ?
   then
    do:
      self:end-file-drop().
      return no-apply.
    end.
  
  {lib/pbshow.i "''"}
  FILE-LOOP:
  do std-in = 1 to self:num-dropped-files:
    std-ch = self:get-dropped-file(std-in).
    tPercent = (std-in / self:num-dropped-files) * 100.
    {lib/pbupdate.i "'Uploading file ' + std-ch" tPercent}
  
    if not checkDisplayNameNew(entry(1, std-ch, "."), tCategory)
     then next.
    
    run uploadRepositoryFile in this-procedure (tPrivate, tCategory, std-ch, output std-lo).
    if not std-lo 
     then leave FILE-LOOP.
  end.
  self:end-file-drop().
  setupDocuments().
  {lib/pbhide.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFiles C-Win
ON ROW-DISPLAY OF brwFiles IN FRAME DEFAULT-FRAME /* Files */
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFiles C-Win
ON START-SEARCH OF brwFiles IN FRAME DEFAULT-FRAME /* Files */
DO:
  define variable tWhereClause as character no-undo.
  tWhereClause = "where category = ~"" + orderfolder.category + "~"".
  {lib/brw-startSearch-multi.i &browseName=brwFiles &whereClause=tWhereClause}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFiles C-Win
ON VALUE-CHANGED OF brwFiles IN FRAME DEFAULT-FRAME /* Files */
DO:
  assign
    std-lo                  = available orderfile
    bFileDelete:sensitive   = std-lo
    bFileModify:sensitive   = std-lo
    bFileDownload:sensitive = std-lo
    tURL:screen-value       = if (std-lo and not orderfile.isPrivate) then orderfile.link else ""
    bFileLink:sensitive     = std-lo and not orderfile.isPrivate
    bFileEmail:sensitive    = std-lo and not orderfile.isPrivate
    bFileDownload:sensitive = std-lo
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFolders
&Scoped-define SELF-NAME brwFolders
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFolders C-Win
ON DROP-FILE-NOTIFY OF brwFolders IN FRAME DEFAULT-FRAME /* Categories */
DO:
  define variable tPrivate     as logical   no-undo.
  define variable tCategory    as character no-undo.
  define variable tFolderName  as character no-undo.
  define variable tFolderPath  as character no-undo.
  define variable tFileName    as character no-undo.
  define variable tFileAttr    as character no-undo.
  define variable tPercent     as integer   no-undo.
  define variable tFolderCount as integer   no-undo initial 0.
  define variable tFileCount   as integer   no-undo initial 0.
   
  if self:num-dropped-files > 1
   then
    do:
      message "Please only upload one folder at a time." view-as alert-box warning buttons ok.
      self:end-file-drop().
      return no-apply.
    end.
   
  {lib/pbshow.i "''"}
  FOLDER-LOOP:
  do std-in = 1 to self:num-dropped-files:
    tFolderPath = self:get-dropped-file(std-in).
    
    file-info:file-name = tFolderPath.
    if file-info:file-type begins "D"
     then
      do:
        tFolderName = entry(num-entries(tFolderPath, "\"), tFolderPath, "\").
        if not can-find(first orderfile where category = tFolderName)
         then run createRepositoryFolder in this-procedure (tFolderName).
         
        input from os-dir(tFolderPath).
        repeat:
          import tFileName std-ch tFileAttr.
          if tFileName <> "." and tFileName <> ".."
           then tFolderCount = tFolderCount + 1.
        end.
        input close.
        
        if tFolderCount = 1
         then message "Should the file " + tFileName + " be marked as private?" view-as alert-box question buttons yes-no-cancel update tPrivate.
         else message "Should all the files be marked as private?" view-as alert-box question buttons yes-no-cancel update tPrivate.
    
        if tPrivate = ?
         then leave FOLDER-LOOP.
      
        input from os-dir(tFolderPath).
        repeat:
          import tFileName std-ch tFileAttr.
          if tFileAttr <> "F"
           then next.
           
          if not checkDisplayNameNew(entry(1, tFileName, "."), tFolderName)
           then next.
           
          tFileCount = tFileCount + 1.
          tPercent = (tFileCount / tFolderCount) * 100.
          {lib/pbupdate.i "'Uploading file ' + tFileName" tPercent}
          
          run uploadRepositoryFile in this-procedure (tPrivate, tFolderName, tFolderPath + "\" + tFileName, output std-lo).
          if not std-lo 
           then leave FOLDER-LOOP.
        end.
        input close.
      end.
  end.
  self:end-file-drop().
  {&OPEN-QUERY-brwFolders}
  apply "VALUE-CHANGED" to self.
  {lib/pbhide.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFolders C-Win
ON ROW-DISPLAY OF brwFolders IN FRAME DEFAULT-FRAME /* Categories */
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFolders C-Win
ON START-SEARCH OF brwFolders IN FRAME DEFAULT-FRAME /* Categories */
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFolders C-Win
ON VALUE-CHANGED OF brwFolders IN FRAME DEFAULT-FRAME /* Categories */
DO:
  assign
    std-lo                    = available orderfolder
    bFolderDelete:sensitive   = std-lo
    bFolderRename:sensitive   = std-lo
    .
  
  if not available orderfolder
   then return.

  define variable tWhereClause as character no-undo.
  define variable tSortClause  as character no-undo.
  tWhereClause = "where category = ~"" + orderfolder.category + "~"".
  tSortClause = "by displayName".
  {lib/brw-startSearch-multi.i &browseName=brwFiles &whereClause=tWhereClause &sortClause=tSortClause}
  apply "VALUE-CHANGED" to browse brwFiles.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFiles
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}
{lib/win-status.i}
{lib/brw-main-multi.i &browse-list="brwFiles,brwFolders"}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

{lib/set-button.i &label="FolderDelete"  &image="images/delete.bmp"      &inactive="images/delete-i.bmp"}
{lib/set-button.i &label="FolderRename"  &image="images/update.bmp"      &inactive="images/update-i.bmp"}
{lib/set-button.i &label="FolderRefresh" &image="images/sync.bmp"        &inactive="images/sync-i.bmp"}
{lib/set-button.i &label="FileCreate"    &image="images/hyperlink.bmp"   &inactive="images/hyperlink-i.bmp"}
{lib/set-button.i &label="FileDownload"  &image="images/download.bmp"    &inactive="images/download-i.bmp"}
{lib/set-button.i &label="FileUpload"    &image="images/upload.bmp"      &inactive="images/upload-i.bmp"}
{lib/set-button.i &label="FileModify"    &image="images/update.bmp"      &inactive="images/update-i.bmp"}
{lib/set-button.i &label="FileDelete"    &image="images/delete.bmp"      &inactive="images/delete-i.bmp"}
{lib/set-button.i &label="FileRefresh"   &image="images/sync.bmp"        &inactive="images/sync-i.bmp"}
{lib/set-button.i &label="FileLink"      &image="images/s-copy.bmp"      &inactive="images/s-copy-i.bmp"}
{lib/set-button.i &label="FileEmail"     &image="images/s-email.bmp"     &inactive="images/s-email-i.bmp"}
setButtons().

{lib/get-column-width.i &browse-name=brwFiles &var=dBrowseDocument &col="'displayName'"}

subscribe to "CheckDisplayName" anywhere.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{&window-name}:max-width-pixels = session:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:min-height-pixels = {&window-name}:height-pixels.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
  apply "CHOOSE" to bFolderRefresh.
  apply "VALUE-CHANGED" to brwFolders.
  apply "VALUE-CHANGED" to brwFiles.
  RUN enable_UI.
  
  run windowResized in this-procedure.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CheckDisplayName C-Win 
PROCEDURE CheckDisplayName :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pDisplayName as character no-undo.
  define input  parameter pCategory    as character no-undo.
  define input  parameter pID          as character no-undo.
  define output parameter pCheck       as logical   no-undo.

  if pID = ""
   then pCheck = checkDisplayNameNew(pDisplayName, pCategory).
   else pCheck = checkDisplayNameModify(pDisplayName, pCategory, pID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE createRepositoryFolder C-Win 
PROCEDURE createRepositoryFolder :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pCategory as character no-undo.
  
  define variable tFolder as character no-undo.
  define variable tUserID as character no-undo.
  define variable tRowID  as rowid     no-undo.
   
  empty temp-table temporderfolder.
  create temporderfolder.
  assign
    temporderfolder.displayName = pCategory
    temporderfolder.category    = pCategory
    .
  release temporderfolder.
    
  do with frame {&frame-name}:
    for first temporderfolder no-lock:
      create orderfolder.
      buffer-copy temporderfolder to orderfolder.
    end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tURL 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bFolderRefresh bFolderDelete bFolderRename bFileRefresh bFileCreate 
         bFileUpload bFileDownload bFileModify bFileDelete brwFolders brwFiles 
         bFileLink bFileEmail tURL 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE uploadRepositoryFile C-Win 
PROCEDURE uploadRepositoryFile PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pPrivate  as character no-undo.
  define input  parameter pCategory as character no-undo.
  define input  parameter pFile     as character no-undo.
  define output parameter pSuccess  as logical   no-undo.
  
  define variable tUserID as character no-undo.
  publish "GetCredentialsID" (output tUserID).
  
  empty temp-table temporderfile.
  run server/uploadarcrepositoryfile.p (tUserID, pPrivate, pEntity, pEntityId, pCategory, pFile, output table temporderfile, output pSuccess, output std-ch).
  if not pSuccess
   then message std-ch view-as alert-box error buttons ok.
   else
    for first temporderfile no-lock:
      create orderfile.
      buffer-copy temporderfile to orderfile.
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* variables */
  &scoped-define FrameMargin 7

  /* the main frame width and height */
  assign
    frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
    frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
    .
    
  assign
    /* browses */
    brwFolders:x                              = {&FrameMargin}
    brwFolders:y                              = {&FrameMargin} + bFolderRefresh:height-pixels + {&FrameMargin}
    brwFolders:width-pixels                   = 200
    brwFolders:height-pixels                  = frame {&frame-name}:height-pixels - brwFolders:y - 10
    brwFiles:x                                = brwFolders:x + brwFolders:width-pixels + 5
    brwFiles:y                                = brwFolders:y
    brwFiles:width-pixels                     = frame {&frame-name}:width-pixels - brwFiles:x - 10
    brwFiles:height-pixels                    = frame {&frame-name}:height-pixels - brwFiles:y - 52
    /* buttons */
    bFileRefresh:x                            = brwFiles:x
    bFileRefresh:y                            = bFolderRefresh:y
    bFileCreate:x                             = bFileRefresh:x + bFileRefresh:width-pixels + 2
    bFileCreate:y                             = bFileRefresh:y
    bFileUpload:x                             = bFileCreate:x + bFileCreate:width-pixels + 2
    bFileUpload:y                             = bFileCreate:y
    bFileDownload:x                           = bFileUpload:x + bFileUpload:width-pixels + 2
    bFileDownload:y                           = bFileUpload:y
    bFileModify:x                             = bFileDownload:x + bFileDownload:width-pixels + 2
    bFileModify:y                             = bFileDownload:y
    bFileDelete:x                             = bFileModify:x + bFileModify:width-pixels + 2
    bFileDelete:y                             = bFileModify:y
    /* URL */
    tUrl:side-label-handle:x                  = brwFiles:x
    tUrl:side-label-handle:y                  = brwFiles:y + brwFiles:height-pixels + ((brwFolders:height-pixels - brwFiles:height-pixels - tUrl:height-pixels) / 2)
    tUrl:x                                    = tUrl:side-label-handle:x + tUrl:side-label-handle:width-pixels + 1 
    tUrl:y                                    = tUrl:side-label-handle:y
    tUrl:width-pixels                         = frame {&frame-name}:width-pixels - tUrl:x - bFileEmail:width-pixels - bFileLink:width-pixels - 11
    bFileEmail:x                              = tUrl:x + tUrl:width-pixels + 1
    bFileEmail:y                              = tUrl:y - 2
    bFileLink:x                               = bFileEmail:x + bFileEmail:width-pixels + 1
    bFileLink:y                               = bFileEmail:y
    .

  /* adjust the virtual frames */
  ASSIGN
    frame {&frame-name}:virtual-width-pixels  = frame {&frame-name}:width-pixels
    frame {&frame-name}:virtual-height-pixels = frame {&frame-name}:height-pixels
    .
  
  {lib/resize-column.i &browse-name=brwFiles &var=dBrowseDocument &col="'displayName'"}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION checkDisplayNameModify C-Win 
FUNCTION checkDisplayNameModify RETURNS LOGICAL
  ( input pDisplayName as character,
    input pCategory as character,
    input pID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tCheck as logical no-undo initial yes.
  if can-find(first orderfile where displayName = pDisplayName and category = pCategory and not (identifier = pID))
   then message "There is already a file in category '" + pCategory + "' with the name '" + pDisplayName + "'. Do you want to override the file?" view-as alert-box question buttons yes-no update tCheck.

  RETURN tCheck.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION checkDisplayNameNew C-Win 
FUNCTION checkDisplayNameNew RETURNS LOGICAL
  ( input pDisplayName as character,
    input pCategory as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tCheck as logical no-undo initial yes.
  if can-find(first orderfile where displayName = pDisplayName and category = pCategory)
   then message "There is already a file in category '" + pCategory + "' with the name '" + pDisplayName + "'. Do you want to override the file?" view-as alert-box question buttons yes-no update tCheck.

  RETURN tCheck.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doModifyDocuments C-Win 
FUNCTION doModifyDocuments RETURNS LOGICAL
  ( input pEnable as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      bFolderRefresh:sensitive = pEnable
      bFolderDelete:sensitive  = pEnable
      bFolderRename:sensitive  = pEnable
      bFileRefresh:sensitive   = pEnable
      bFileModify:sensitive    = pEnable
      bFileDelete:sensitive    = pEnable
      bFileDownload:sensitive  = pEnable
      bFileUpload:sensitive    = pEnable
      .
  end.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setupDocuments C-Win 
FUNCTION setupDocuments RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  close query brwFolders.
  close query brwFiles.
  empty temp-table orderfolder.
  for each orderfile exclusive-lock
  break by orderfile.category:

    if first-of(orderfile.category)
     then run createRepositoryFolder in this-procedure (orderfile.category).
     
    publish "GetSysUserName" (orderfile.createdBy, output orderfile.createdByDesc).
  end.
  {&OPEN-QUERY-brwFolders}
  apply "VALUE-CHANGED" to browse brwFolders.
  apply "VALUE-CHANGED" to browse brwFiles.
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

