&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogrevenue.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul Sharma

  Created:01/06/2020
  
  Modified: 
  Date        Name     Comments
  01/21/2021  Shefali  Remove Net GL Ref validation.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/ar-def.i}
{tt/arrevenue.i}
{tt/arrevenue.i &tableAlias=ttarrevenue}


/* Parameters Definitions ---                                           */
define input  parameter ipcAction      as  character no-undo.
define input  parameter table          for ttarrevenue.
define output parameter opcRevenueType as  character  no-undo.
define output parameter oplSuccess     as  logical     no-undo.


/* Local Variable Definitions ---                                       */
define variable cRevenueType    as character no-undo.
define variable cNetGLRef       as character no-undo.
define variable cNetGLDesc      as character no-undo.
define variable cGrossGLRef     as character no-undo.
define variable cGrossGLDesc    as character no-undo.
define variable cRetainedGLRef  as character no-undo.
define variable cRetainedGLDesc as character no-undo.
define variable cNotes          as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS flGrossGLRef flGrossGLDesc flRetainedGLRef ~
flRetainedGLDesc eNotes Btn_Cancel RECT-91 RECT-92 
&Scoped-Define DISPLAYED-OBJECTS fRevenueType flGrossGLRef flGrossGLDesc ~
flRetainedGLRef flRetainedGLDesc eNotes flCreatedBy flCreatedDate ~
flModifiedBy flModifiedDate fMarkMandatory1 fMarkMandatory2 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 12 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 12 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE eNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 103.1 BY 2.81 NO-UNDO.

DEFINE VARIABLE flCreatedBy AS CHARACTER FORMAT "X(256)":U 
     LABEL "Created By" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 37 BY 1 NO-UNDO.

DEFINE VARIABLE flCreatedDate AS DATE FORMAT "99/99/99":U 
     LABEL "Created Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE flGrossGLDesc AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 39 BY 1 NO-UNDO.

DEFINE VARIABLE flGrossGLRef AS CHARACTER FORMAT "X(256)":U 
     LABEL "Revenue (Gross) GL Ref (CR)" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE flModifiedBy AS CHARACTER FORMAT "X(256)":U 
     LABEL "Modified By" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 37 BY 1 NO-UNDO.

DEFINE VARIABLE flModifiedDate AS DATE FORMAT "99/99/99":U 
     LABEL "Modified Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE flNetGLDesc AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 39 BY 1 NO-UNDO.

DEFINE VARIABLE flNetGLRef AS CHARACTER FORMAT "X(256)":U 
     LABEL "Net GL Ref (DR)" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE flRetainedGLDesc AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 39 BY 1 NO-UNDO.

DEFINE VARIABLE flRetainedGLRef AS CHARACTER FORMAT "X(256)":U 
     LABEL "Retained GL Ref (DR)" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 TOOLTIP "Enter N/A if Retained GL Reference is not applicable" NO-UNDO.

DEFINE VARIABLE fMarkMandatory1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fRevenueType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Revenue Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-91
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 103.6 BY 4.52.

DEFINE RECTANGLE RECT-92
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 35 BY 1.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fRevenueType AT ROW 1.43 COL 24.2 COLON-ALIGNED WIDGET-ID 2
     flGrossGLRef AT ROW 3.43 COL 31 COLON-ALIGNED WIDGET-ID 32
     flGrossGLDesc AT ROW 3.43 COL 64 COLON-ALIGNED WIDGET-ID 62
     flRetainedGLRef AT ROW 4.62 COL 31 COLON-ALIGNED WIDGET-ID 38
     flRetainedGLDesc AT ROW 4.62 COL 64 COLON-ALIGNED WIDGET-ID 64
     flNetGLRef AT ROW 5.86 COL 24.2 COLON-ALIGNED WIDGET-ID 46
     flNetGLDesc AT ROW 5.86 COL 64 COLON-ALIGNED WIDGET-ID 66
     eNotes AT ROW 8.19 COL 3 NO-LABEL WIDGET-ID 26
     Btn_OK AT ROW 14.57 COL 40
     Btn_Cancel AT ROW 14.57 COL 53.6
     flCreatedBy AT ROW 13.05 COL 56.2 COLON-ALIGNED WIDGET-ID 94 NO-TAB-STOP 
     flCreatedDate AT ROW 13.05 COL 26.2 COLON-ALIGNED WIDGET-ID 96 NO-TAB-STOP 
     flModifiedBy AT ROW 11.81 COL 56.2 COLON-ALIGNED WIDGET-ID 98 NO-TAB-STOP 
     flModifiedDate AT ROW 11.81 COL 26.2 COLON-ALIGNED WIDGET-ID 100 NO-TAB-STOP 
     fMarkMandatory1 AT ROW 1.67 COL 40.4 COLON-ALIGNED NO-LABEL WIDGET-ID 52 NO-TAB-STOP 
     fMarkMandatory2 AT ROW 3.67 COL 47.2 COLON-ALIGNED NO-LABEL WIDGET-ID 268 NO-TAB-STOP 
     "Notes:" VIEW-AS TEXT
          SIZE 6.4 BY .62 AT ROW 7.57 COL 2.6 WIDGET-ID 28
     "Net posted to Agent's A/R GL Ref" VIEW-AS TEXT
          SIZE 33 BY .71 AT ROW 6 COL 34 WIDGET-ID 274
     RECT-91 AT ROW 2.91 COL 2.6 WIDGET-ID 68
     RECT-92 AT ROW 5.86 COL 33 WIDGET-ID 276
     SPACE(39.19) SKIP(8.94)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Revenue"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       eNotes:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN flCreatedBy IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flCreatedBy:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN flCreatedDate IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flCreatedDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN flModifiedBy IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flModifiedBy:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN flModifiedDate IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flModifiedDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN flNetGLDesc IN FRAME Dialog-Frame
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       flNetGLDesc:HIDDEN IN FRAME Dialog-Frame           = TRUE.

/* SETTINGS FOR FILL-IN flNetGLRef IN FRAME Dialog-Frame
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       flNetGLRef:HIDDEN IN FRAME Dialog-Frame           = TRUE.

/* SETTINGS FOR FILL-IN fMarkMandatory1 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory2 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fRevenueType IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Revenue */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  run saveRevenue in this-procedure.
  if not oplSuccess 
   then
    return no-apply.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eNotes Dialog-Frame
ON VALUE-CHANGED OF eNotes IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flGrossGLDesc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flGrossGLDesc Dialog-Frame
ON VALUE-CHANGED OF flGrossGLDesc IN FRAME Dialog-Frame /* Description */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flGrossGLRef
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flGrossGLRef Dialog-Frame
ON VALUE-CHANGED OF flGrossGLRef IN FRAME Dialog-Frame /* Revenue (Gross) GL Ref (CR) */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flNetGLDesc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flNetGLDesc Dialog-Frame
ON VALUE-CHANGED OF flNetGLDesc IN FRAME Dialog-Frame /* Description */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flNetGLRef
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flNetGLRef Dialog-Frame
ON VALUE-CHANGED OF flNetGLRef IN FRAME Dialog-Frame /* Net GL Ref (DR) */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flRetainedGLDesc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flRetainedGLDesc Dialog-Frame
ON VALUE-CHANGED OF flRetainedGLDesc IN FRAME Dialog-Frame /* Description */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flRetainedGLRef
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flRetainedGLRef Dialog-Frame
ON VALUE-CHANGED OF flRetainedGLRef IN FRAME Dialog-Frame /* Retained GL Ref (DR) */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fRevenueType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fRevenueType Dialog-Frame
ON VALUE-CHANGED OF fRevenueType IN FRAME Dialog-Frame /* Revenue Type */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  run displayData in this-procedure.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cCodeTypeList as character   no-undo.

  do with frame {&frame-name}:
  end.

  if ipcAction <> {&View}
   then
    assign
        fMarkMandatory1:screen-value = (if ipcAction = {&Modify} then "" else {&Mandatory})
        fMarkMandatory2:screen-value = {&Mandatory}
        .
  
  case ipcAction:
    when {&add}
     then
      assign 
          frame dialog-frame:title = "New Revenue"
          fRevenueType:sensitive   = true
          Btn_OK :label            = {&Create} 
          Btn_OK :tooltip          = {&Create}          
          .
    when {&Modify} 
     then
      do:           
        assign
            frame dialog-frame:title  = "Edit Revenue"                                                           
            fRevenueType:sensitive    = false
            Btn_OK :label             = {&Save} 
            Btn_OK :tooltip           = {&Save}               
            .
        run setWidgetFields in this-procedure.    
      end.       
    when {&copy} 
     then
      do:          
        assign
            frame dialog-frame:title  = "Copy Revenue"
            fRevenueType:sensitive    = true
            Btn_OK :label             = {&Create} 
            Btn_OK :tooltip           = {&Create}              
            .
        run setWidgetFields in this-procedure.        
      end.
    when {&view} 
     then
      do:
        assign 
            frame dialog-frame:title    = "Revenue"
            fRevenueType:sensitive      = false
            flGrossGLRef:sensitive      = false
            flGrossGLDesc:sensitive     = false
            flRetainedGLRef:sensitive   = false
            flRetainedGLDesc:sensitive  = false
            flNetGLRef:sensitive        = false
            flNetGLDesc:sensitive       = false
            eNotes:sensitive            = false 
            btn_OK:sensitive            = false
            Btn_Cancel:sensitive        = false
            no-error. 
        run setWidgetFields in this-procedure.            
      end.      
  end case.  

  /* Modify screen on the basis of Action:New/Modify */
  run resetScreenLayout in this-procedure.
  
  /* Keep the copy of initial values. Required in enableDisableSave. */ 
  assign    
      cRevenueType    = fRevenueType:input-value
      cNetGLRef       = flNetGLRef:input-value
      cNetGLDesc      = flNetGLDesc:input-value
      cGrossGLRef     = flGrossGLRef:input-value
      cGrossGLDesc    = flGrossGLDesc:input-value
      cRetainedGLRef  = flRetainedGLRef:input-value
      cRetainedGLDesc = flRetainedGLDesc:input-value
      cNotes          = eNotes:input-value      
      .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  if ipcAction = {&add}
   then
    Btn_OK:sensitive = (fRevenueType:input-value    <> ""  and
                        flGrossGLRef:input-value    <> "")
    .
    
   else if ipcAction = {&Modify}                     
    then
     Btn_OK:sensitive = not((cNetGLRef       = flNetGLRef:input-value       and                             
                             cNetGLDesc      = flNetGLDesc:input-value      and    
                             cGrossGLRef     = flGrossGLRef:input-value     and                             
                             cGrossGLDesc    = flGrossGLDesc:input-value    and
                             cRetainedGLRef  = flRetainedGLRef:input-value  and                             
                             cRetainedGLDesc = flRetainedGLDesc:input-value and
                             cNotes          = eNotes:input-value
                            ) or
                            flGrossGLRef:input-value    = "").
   else /* Copy */   
    Btn_OK:sensitive = (fRevenueType:input-value    <> ""        and
                        cRevenueType <> fRevenueType:input-value and
                        flGrossGLRef:input-value    <> ""). 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fRevenueType flGrossGLRef flGrossGLDesc flRetainedGLRef 
          flRetainedGLDesc eNotes flCreatedBy flCreatedDate flModifiedBy 
          flModifiedDate fMarkMandatory1 fMarkMandatory2 
      WITH FRAME Dialog-Frame.
  ENABLE flGrossGLRef flGrossGLDesc flRetainedGLRef flRetainedGLDesc eNotes 
         Btn_Cancel RECT-91 RECT-92 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resetScreenLayout Dialog-Frame 
PROCEDURE resetScreenLayout :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  assign
      flModifiedDate:hidden = (ipcAction = {&Add} or ipcAction = {&Copy})
      flModifiedBy:hidden   = (ipcAction = {&Add} or ipcAction = {&Copy})
      flCreatedBy:hidden    = (ipcAction = {&Add} or ipcAction = {&Copy})
      flCreatedDate:hidden  = (ipcAction = {&Add} or ipcAction = {&Copy})
      Btn_OK:hidden         = ipcAction = {&View}
      Btn_Cancel:hidden     = ipcAction = {&View}
      .
      
  if ipcAction = {&Add} or ipcAction = {&Copy}
   then
    assign
        Btn_OK:row                = eNotes:row + eNotes:height + 0.7
        Btn_Cancel:row            = eNotes:row + eNotes:height + 0.7
        frame Dialog-Frame:height = eNotes:row + eNotes:height + 2.8 
        .
   else if ipcAction = {&Modify}
    then
     assign
         Btn_OK:row                = 14.57
         Btn_Cancel:row            = 14.57
         frame Dialog-Frame:height = Btn_OK:row + 2
         .
   else
    frame Dialog-Frame:height = flCreatedDate:row + flCreatedDate:height + 0.8.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveRevenue Dialog-Frame 
PROCEDURE saveRevenue :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  empty temp-table ttarrevenue.
  
  create ttarrevenue.
  assign 
      ttarrevenue.revenueType     = fRevenueType:input-value
      ttarrevenue.grossGLRef      = flGrossGLRef:input-value
      ttarrevenue.grossGLDesc     = flGrossGLDesc:input-value   
      ttarrevenue.retainedGLRef   = flRetainedGLRef:input-value
      ttarrevenue.retainedGLDesc  = flRetainedGLDesc:input-value
      ttarrevenue.netGLRef        = flNetGLRef:input-value
      ttarrevenue.netGLdesc       = flNetGLDesc:input-value
      ttarrevenue.notes           = eNotes:input-value
      .
    
  opcRevenueType = fRevenueType:input-value.

  case ipcAction:
    when {&Modify} 
     then
      publish "modifyRevenue" (input table ttarrevenue,
                               output oplSuccess,
                               output std-ch).

    when {&Add} 
     then
      publish "newRevenue" (input table ttarrevenue,
                            output oplSuccess,
                            output std-ch).
    otherwise /* Copy */    
      publish "newRevenue" (input table ttarrevenue,
                            output oplSuccess,
                            output std-ch).
  end case.  

  if not oplSuccess
   then 
    message std-ch
        view-as alert-box info buttons ok.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetFields Dialog-Frame 
PROCEDURE setWidgetFields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  find first ttarrevenue no-error.
  if available ttarrevenue 
   then            
    assign
        fRevenueType:screen-value     = ttarrevenue.revenueType              
        flNetGLRef:screen-value       = ttarrevenue.netGLRef
        flNetGLDesc:screen-value      = ttarrevenue.netGLdesc
        flGrossGLRef:screen-value     = ttarrevenue.grossGLRef            
        flGrossGLDesc:screen-value    = ttarrevenue.grossGLDesc
        flRetainedGLRef:screen-value  = ttarrevenue.retainedGLRef
        flRetainedGLDesc:screen-value = ttarrevenue.retainedGLDesc
        flModifiedDate:screen-value   = string(ttarrevenue.modifiedDate)
        flModifiedBy:screen-value     = ttarrevenue.mUsername
        flCreatedDate:screen-value    = string(ttarrevenue.createdDate)
        flCreatedBy:screen-value      = ttarrevenue.cUsername
        eNotes:screen-value           = ttarrevenue.notes        
        . 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

