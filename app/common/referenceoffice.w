&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* referencoffice.w
   Window of Offices
   4.23.2012
   */

CREATE WIDGET-POOL.

{tt/office.i}
{tt/agent.i}
{tt/openwindow.i}

{lib/std-def.i}
{lib/add-delimiter.i}
{lib/set-button-def.i}
{lib/set-filter-def.i &tableName="office"}

/* def var hData as handle no-undo.  */

/* {lib/winlaunch.i}  */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fAgent fState bExport bRefresh bConfig ~
rFilters rConfig 
&Scoped-Define DISPLAYED-OBJECTS fAgent fState 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createQuery C-Win 
FUNCTION createQuery RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bConfig  NO-FOCUS
     LABEL "Config" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE VARIABLE fAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 80 BY 1 NO-UNDO.

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

DEFINE RECTANGLE rActions
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 16 BY 2.05.

DEFINE RECTANGLE rConfig
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 8.6 BY 2.05.

DEFINE RECTANGLE rFilters
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 121.6 BY 2.05.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     fAgent AT ROW 1.71 COL 55 COLON-ALIGNED WIDGET-ID 80
     fState AT ROW 1.71 COL 24 COLON-ALIGNED WIDGET-ID 78
     bExport AT ROW 1.38 COL 10 WIDGET-ID 2 NO-TAB-STOP 
     bRefresh AT ROW 1.38 COL 2.6 WIDGET-ID 4 NO-TAB-STOP 
     bConfig AT ROW 1.38 COL 139.4 WIDGET-ID 84 NO-TAB-STOP 
     rActions AT ROW 1.24 COL 2 WIDGET-ID 8
     rFilters AT ROW 1.24 COL 17.6 WIDGET-ID 58
     rConfig AT ROW 1.24 COL 138.8 WIDGET-ID 82
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 147 BY 18.71 WIDGET-ID 100.

DEFINE FRAME fBrowse
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2 ROW 3.48
         SIZE 145 BY 15.95 WIDGET-ID 200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Offices"
         HEIGHT             = 18.71
         WIDTH              = 147
         MAX-HEIGHT         = 38.24
         MAX-WIDTH          = 307.2
         VIRTUAL-HEIGHT     = 38.24
         VIRTUAL-WIDTH      = 307.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fBrowse:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fBrowse
                                                                        */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR RECTANGLE rActions IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Offices */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Offices */
DO:
  for each openwindow no-lock:
    if valid-handle(openwindow.procHandle)
     then run CloseWindow in openwindow.procHandle no-error.
  end.

  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Offices */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bConfig
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bConfig C-Win
ON CHOOSE OF bConfig IN FRAME fMain /* Config */
DO:
  run SetDynamicBrowseColumns.
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run ExportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  empty temp-table office.
  run buildFieldList in this-procedure.
  run BuildDynamicBrowse (createQuery()).
  publish "LoadOffices".
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAgent C-Win
ON VALUE-CHANGED OF fAgent IN FRAME fMain /* Agent */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState C-Win
ON VALUE-CHANGED OF fState IN FRAME fMain /* State */
DO:
  run AgentComboState.
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}
{lib/build-dynamic-browse.i &table=office &entity="'Office'" &keyfield="'officeID'" &resizeColumn="'officeName,agentName'"}
{lib/set-filter.i &label="State" &id="stateID" &value="stateDesc"}
{lib/set-filter.i &label="Agent" &id="agentID" &value="agentName" &populate=false}
{lib/set-button.i &label="Refresh" &toggle=false}
{lib/set-button.i &label="Export"  &toggle=false}
{lib/set-button.i &label="Config"  &toggle=false}
setButtons().

{&window-name}:window-state = 2.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  {lib/get-agent-list.i &combo=fAgent &state=fState &addAll=true}
  
  run getData in this-procedure.
  run SetDynamicBrowseColumnWidth ("officeName").
  
  {&window-name}:window-state = 3.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE buildFieldList C-Win 
PROCEDURE buildFieldList :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  run BuildDynamicBrowseColumns.
  for each listfield exclusive-lock
     where listfield.dataType = "character":
    
    case listfield.columnBuffer:
     when "stat" then listfield.columnWidth = 14.
     when "zip" then listfield.columnWidth = 12.
     when "state" or
     when "stateDesc" or
     when "agentID" or
     when "stateID" then listfield.columnWidth = 15.
     when "email" then listfield.columnWidth = 35.
     when "website" then listfield.columnWidth = 40.
     when "fullAddr" then listfield.columnWidth = 50.
     when "officeName" or
     when "agentName" then listfield.columnWidth = 40.
     otherwise listfield.columnWidth = 25.
    end case.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fAgent fState 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE fAgent fState bExport bRefresh bConfig rFilters rConfig 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW FRAME fBrowse IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fBrowse}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExportData C-Win 
PROCEDURE ExportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hBrowse as handle no-undo.

  hBrowse = frame fBrowse:first-child:first-child.
  if valid-handle(hBrowse) and hBrowse:type = "BROWSE"
   then
    do:
      std-ha = hBrowse:query.
      if std-ha:num-results = 0
       then
        do:
          message "No results to export" view-as alert-box warning buttons ok.
          return.
        end.
    end.

  &scoped-define ReportName "Agents"

 std-ch = "C".
 publish "GetExportType" (output std-ch).
 if std-ch = "X" 
  then run util/exporttoexcelbrowse.p (string(hBrowse), {&ReportName}).
  else run util/exporttocsvbrowse.p (string(hBrowse), {&ReportName}).
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table office.
  publish "GetOffices" (output table office).

  run buildFieldList in this-procedure.
  run BuildDynamicBrowse (createQuery()).
  setFilterCombos("ALL").
  fState:screen-value in frame {&frame-name} = "ALL".
    
  enableFilters(can-find(first office)).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

  /* modify the frame for the dynamic browse */
  assign
    frame fBrowse:width-pixels          = {&window-name}:width-pixels - 10
    frame fBrowse:virtual-width-pixels  = frame fBrowse:width-pixels
    frame fBrowse:height-pixels         = {&window-name}:height-pixels - frame fBrowse:y - 5
    frame fBrowse:virtual-height-pixels = frame fBrowse:height-pixels
    .
  
  /* resize the filters */
  assign
    std-de                = frame {&frame-name}:width-pixels - {&window-name}:min-width-pixels
    rConfig:x             = 689 + std-de
    bConfig:x             = 692 + std-de
    rFilters:width-pixels = 608 + std-de
    fAgent:width-pixels   = 400 + std-de
    .
  run AgentComboResize.
  
  run getData in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createQuery C-Win 
FUNCTION createQuery RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  DEFINE VARIABLE tQuery       as CHARACTER no-undo.

  tQuery = "preselect each office " + getWhereClause() + " by officeName".
  RETURN tQuery.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

